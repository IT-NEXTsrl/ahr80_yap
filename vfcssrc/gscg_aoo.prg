* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_aoo                                                        *
*              Modello pagamento F24/2006                                      *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-14                                                      *
* Last revis.: 2011-05-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_aoo"))

* --- Class definition
define class tgscg_aoo as StdForm
  Top    = 3
  Left   = 10

  * --- Standard Properties
  Width  = 826
  Height = 419+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-03"
  HelpContextID=91609751
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=178

  * --- Constant Properties
  MOD_PAG_IDX = 0
  TRI_BUTI_IDX = 0
  AZIENDA_IDX = 0
  TITOLARI_IDX = 0
  COD_TRIB_IDX = 0
  CODI_UFF_IDX = 0
  SED_INPS_IDX = 0
  CAU_INPS_IDX = 0
  REG_PROV_IDX = 0
  ENTI_LOC_IDX = 0
  SE_INAIL_IDX = 0
  COD_PREV_IDX = 0
  SED_AEN_IDX = 0
  CAU_AEN_IDX = 0
  MODVPAG_IDX = 0
  MODCPAG_IDX = 0
  MODEPAG_IDX = 0
  VALUTE_IDX = 0
  CPUSERS_IDX = 0
  MODIPAG_IDX = 0
  SEDIAZIE_IDX = 0
  ANAG_PRO_IDX = 0
  cFile = "MOD_PAG"
  cKeySelect = "MFSERIAL"
  cKeyWhere  = "MFSERIAL=this.w_MFSERIAL"
  cKeyWhereODBC = '"MFSERIAL="+cp_ToStrODBC(this.w_MFSERIAL)';

  cKeyWhereODBCqualified = '"MOD_PAG.MFSERIAL="+cp_ToStrODBC(this.w_MFSERIAL)';

  cPrg = "gscg_aoo"
  cComment = "Modello pagamento F24/2006"
  icon = "anag.ico"
  cAutoZoom = 'GSCG_AOO'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MFSERIAL = space(10)
  o_MFSERIAL = space(10)
  w_MFMESRIF = space(2)
  w_MFANNRIF = space(4)
  w_MFVALUTA = space(3)
  w_APPCOIN = space(1)
  o_APPCOIN = space(1)
  w_MF_COINC = space(1)
  w_MFSERIAL = space(10)
  w_APPOIMP = 0
  w_MFCODUFF = space(3)
  w_MFCODATT = space(11)
  w_MESE = space(2)
  w_ANNO = space(4)
  w_GENERA = space(10)
  w_IMPORTA = .F.
  w_MFSERIAL = space(10)
  w_MFCDSED1 = space(5)
  o_MFCDSED1 = space(5)
  w_MFCCONT1 = space(5)
  o_MFCCONT1 = space(5)
  w_MFMINPS1 = space(17)
  w_APP1A = .F.
  o_APP1A = .F.
  w_APP1 = .F.
  o_APP1 = .F.
  w_MFDAMES1 = space(2)
  w_MFDAANN1 = space(4)
  w_MF_AMES1 = space(2)
  w_MFAN1 = space(4)
  w_MFIMPSD1 = 0
  w_MFIMPSC1 = 0
  w_MFCDSED2 = space(5)
  o_MFCDSED2 = space(5)
  w_MFCCONT2 = space(5)
  o_MFCCONT2 = space(5)
  w_MFMINPS2 = space(17)
  w_APP2A = .F.
  o_APP2A = .F.
  w_MFDAMES2 = space(2)
  w_MFDAANN2 = space(4)
  w_APP2 = .F.
  o_APP2 = .F.
  w_MF_AMES2 = space(2)
  w_MFAN2 = space(4)
  w_MFIMPSD2 = 0
  w_MFIMPSC2 = 0
  w_MFCDSED3 = space(5)
  o_MFCDSED3 = space(5)
  w_MFCCONT3 = space(5)
  o_MFCCONT3 = space(5)
  w_MFMINPS3 = space(17)
  w_APP3A = .F.
  o_APP3A = .F.
  w_MFDAMES3 = space(2)
  w_APP3 = .F.
  o_APP3 = .F.
  w_MFDAANN3 = space(4)
  w_MF_AMES3 = space(2)
  w_MFAN3 = space(4)
  w_MFIMPSD3 = 0
  w_MFIMPSC3 = 0
  w_MFCDSED4 = space(5)
  o_MFCDSED4 = space(5)
  w_MFCCONT4 = space(5)
  o_MFCCONT4 = space(5)
  w_MFMINPS4 = space(17)
  w_APP4A = .F.
  o_APP4A = .F.
  w_MFDAMES4 = space(2)
  w_APP4 = .F.
  o_APP4 = .F.
  w_MFDAANN4 = space(4)
  w_MF_AMES4 = space(2)
  w_MFAN4 = space(4)
  w_MFIMPSD4 = 0
  w_MFIMPSC4 = 0
  w_MFTOTDPS = 0
  w_MFTOTCPS = 0
  w_MFSALDPS = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_GENINPS = space(10)
  w_IMPINPS = .F.
  w_MFSERIAL = space(10)
  w_MFCODRE1 = space(2)
  o_MFCODRE1 = space(2)
  w_MFTRIRE1 = space(5)
  o_MFTRIRE1 = space(5)
  w_MFRATRE1 = space(4)
  w_MFANNRE1 = space(4)
  w_MFIMDRE1 = 0
  w_MFIMCRE1 = 0
  w_MFCODRE2 = space(2)
  o_MFCODRE2 = space(2)
  w_MFTRIRE2 = space(5)
  o_MFTRIRE2 = space(5)
  w_MFRATRE2 = space(4)
  w_MFANNRE2 = space(4)
  w_MFIMDRE2 = 0
  w_MFIMCRE2 = 0
  w_MFCODRE3 = space(2)
  o_MFCODRE3 = space(2)
  w_MFTRIRE3 = space(5)
  o_MFTRIRE3 = space(5)
  w_MFRATRE3 = space(4)
  w_MFANNRE3 = space(4)
  w_MFIMDRE3 = 0
  w_MFIMCRE3 = 0
  w_MFCODRE4 = space(2)
  o_MFCODRE4 = space(2)
  w_MFTRIRE4 = space(5)
  o_MFTRIRE4 = space(5)
  w_MFRATRE4 = space(4)
  w_MFANNRE4 = space(4)
  w_MFIMDRE4 = 0
  w_MFIMCRE4 = 0
  w_MFTOTDRE = 0
  w_MFTOTCRE = 0
  w_MFSALDRE = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_MFSERIAL = space(10)
  w_MFSINAI1 = space(5)
  o_MFSINAI1 = space(5)
  w_MF_NPOS1 = space(8)
  w_MF_PACC1 = space(2)
  w_MF_NRIF1 = space(6)
  w_MFCAUSA1 = space(2)
  w_MFIMDIL1 = 0
  w_MFIMCIL1 = 0
  w_MFSINAI2 = space(5)
  o_MFSINAI2 = space(5)
  w_MF_NPOS2 = space(8)
  w_MF_PACC2 = space(2)
  w_MF_NRIF2 = space(6)
  w_MFCAUSA2 = space(2)
  w_MFIMDIL2 = 0
  w_MFIMCIL2 = 0
  w_MFSINAI3 = space(5)
  o_MFSINAI3 = space(5)
  w_MF_NPOS3 = space(8)
  w_MF_PACC3 = space(2)
  w_MF_NRIF3 = space(6)
  w_MFCAUSA3 = space(2)
  w_MFIMDIL3 = 0
  w_MFIMCIL3 = 0
  w_MFTDINAI = 0
  w_MFTCINAI = 0
  w_MFSALINA = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_MFSERIAL = space(10)
  w_MFCDENTE = space(5)
  o_MFCDENTE = space(5)
  w_VRSDENT1 = space(5)
  o_VRSDENT1 = space(5)
  w_VPSDENT1 = space(5)
  o_VPSDENT1 = space(5)
  w_MFSDENT1 = space(5)
  o_MFSDENT1 = space(5)
  w_MFCCOAE1 = space(5)
  o_MFCCOAE1 = space(5)
  w_OLSDENT1 = space(5)
  w_MFCDPOS1 = space(9)
  w_MFMSINE1 = space(2)
  w_MFANINE1 = space(4)
  w_MFMSFIE1 = space(2)
  w_MFANF1 = space(4)
  w_MFIMDAE1 = 0
  w_MFIMCAE1 = 0
  w_VPSDENT2 = space(5)
  o_VPSDENT2 = space(5)
  w_VRSDENT2 = space(5)
  o_VRSDENT2 = space(5)
  w_MFSDENT2 = space(5)
  o_MFSDENT2 = space(5)
  w_MFCCOAE2 = space(5)
  o_MFCCOAE2 = space(5)
  w_OLSDENT2 = space(5)
  w_MFCDPOS2 = space(9)
  w_MFMSINE2 = space(2)
  w_MFANINE2 = space(4)
  w_MFMSFIE2 = space(2)
  w_MFANF2 = space(4)
  w_MFIMDAE2 = 0
  w_MFIMCAE2 = 0
  w_MFTDAENT = 0
  w_MFTCAENT = 0
  w_MFSALAEN = 0
  w_DESAENTE = space(30)
  w_MESE = space(2)
  w_ANNO = space(4)
  w_GENPREV = space(10)
  w_RESCHK = 0
  w_IMPPREV = .F.
  w_MFSERIAL = space(10)
  w_MFSALFIN = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_TIPOMOD = space(3)
  w_APPOIMP2 = 0
  w_ANNORIF = space(4)
  w_TIPTRI = space(10)
  w_MFDESSTA = space(5)
  o_MFDESSTA = space(5)
  w_CAP1 = space(9)
  w_LOCALI1 = space(30)
  w_INDIRI1 = space(35)
  w_PROVIN1 = space(2)
  w_CAP = space(9)
  w_LOCALI = space(30)
  w_PROVIN = space(2)
  w_INDIRI = space(35)
  w_DESSTA = space(1)
  w_CODAZIE = space(5)
  w_MFTIPMOD = space(10)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_MFSERIAL = this.W_MFSERIAL

  * --- Children pointers
  GSCG_ACF = .NULL.
  GSCG_AFQ = .NULL.
  GSCG_AVF = .NULL.
  GSCG_AIF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=8, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOD_PAG','gscg_aoo')
    stdPageFrame::Init()
    *set procedure to GSCG_ACF additive
    with this
      .Pages(1).addobject("oPag","tgscg_aooPag1","gscg_aoo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Contribuente")
      .Pages(1).HelpContextID = 159522069
      .Pages(2).addobject("oPag","tgscg_aooPag2","gscg_aoo",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Erario")
      .Pages(2).HelpContextID = 75603642
      .Pages(3).addobject("oPag","tgscg_aooPag3","gscg_aoo",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("INPS")
      .Pages(3).HelpContextID = 97398150
      .Pages(4).addobject("oPag","tgscg_aooPag4","gscg_aoo",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Regioni enti locali")
      .Pages(4).HelpContextID = 112366599
      .Pages(5).addobject("oPag","tgscg_aooPag5","gscg_aoo",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("INAIL")
      .Pages(5).HelpContextID = 176373126
      .Pages(6).addobject("oPag","tgscg_aooPag6","gscg_aoo",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Altri enti")
      .Pages(6).HelpContextID = 58721356
      .Pages(7).addobject("oPag","tgscg_aooPag7","gscg_aoo",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Estr.versamento")
      .Pages(7).HelpContextID = 98841880
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMFSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCG_ACF
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[22]
    this.cWorkTables[1]='TRI_BUTI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='TITOLARI'
    this.cWorkTables[4]='COD_TRIB'
    this.cWorkTables[5]='CODI_UFF'
    this.cWorkTables[6]='SED_INPS'
    this.cWorkTables[7]='CAU_INPS'
    this.cWorkTables[8]='REG_PROV'
    this.cWorkTables[9]='ENTI_LOC'
    this.cWorkTables[10]='SE_INAIL'
    this.cWorkTables[11]='COD_PREV'
    this.cWorkTables[12]='SED_AEN'
    this.cWorkTables[13]='CAU_AEN'
    this.cWorkTables[14]='MODVPAG'
    this.cWorkTables[15]='MODCPAG'
    this.cWorkTables[16]='MODEPAG'
    this.cWorkTables[17]='VALUTE'
    this.cWorkTables[18]='CPUSERS'
    this.cWorkTables[19]='MODIPAG'
    this.cWorkTables[20]='SEDIAZIE'
    this.cWorkTables[21]='ANAG_PRO'
    this.cWorkTables[22]='MOD_PAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(22))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_PAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_PAG_IDX,3]
  return

  function CreateChildren()
    this.GSCG_ACF = CREATEOBJECT('stdDynamicChild',this,'GSCG_ACF',this.oPgFrm.Page1.oPag.oLinkPC_1_8)
    this.GSCG_ACF.createrealchild()
    this.GSCG_AFQ = CREATEOBJECT('stdDynamicChild',this,'GSCG_AFQ',this.oPgFrm.Page2.oPag.oLinkPC_2_2)
    this.GSCG_AVF = CREATEOBJECT('stdDynamicChild',this,'GSCG_AVF',this.oPgFrm.Page7.oPag.oLinkPC_7_3)
    this.GSCG_AIF = CREATEOBJECT('stdDynamicChild',this,'GSCG_AIF',this.oPgFrm.Page4.oPag.oLinkPC_4_41)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_ACF)
      this.GSCG_ACF.DestroyChildrenChain()
      this.GSCG_ACF=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_8')
    if !ISNULL(this.GSCG_AFQ)
      this.GSCG_AFQ.DestroyChildrenChain()
      this.GSCG_AFQ=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_2')
    if !ISNULL(this.GSCG_AVF)
      this.GSCG_AVF.DestroyChildrenChain()
      this.GSCG_AVF=.NULL.
    endif
    this.oPgFrm.Page7.oPag.RemoveObject('oLinkPC_7_3')
    if !ISNULL(this.GSCG_AIF)
      this.GSCG_AIF.DestroyChildrenChain()
      this.GSCG_AIF=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_41')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_ACF.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_AFQ.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_AVF.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_AIF.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_ACF.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_AFQ.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_AVF.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_AIF.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_ACF.NewDocument()
    this.GSCG_AFQ.NewDocument()
    this.GSCG_AVF.NewDocument()
    this.GSCG_AIF.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_ACF.SetKey(;
            .w_MFSERIAL,"CFSERIAL";
            )
      this.GSCG_AFQ.SetKey(;
            .w_MFSERIAL,"EFSERIAL";
            )
      this.GSCG_AVF.SetKey(;
            .w_MFSERIAL,"VFSERIAL";
            )
      this.GSCG_AIF.SetKey(;
            .w_MFSERIAL,"IFSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_ACF.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"CFSERIAL";
             )
      .GSCG_AFQ.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"EFSERIAL";
             )
      .GSCG_AVF.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"VFSERIAL";
             )
      .GSCG_AIF.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"IFSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_ACF)
        i_f=.GSCG_ACF.BuildFilter()
        if !(i_f==.GSCG_ACF.cQueryFilter)
          i_fnidx=.GSCG_ACF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_ACF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_ACF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_ACF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_ACF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_AFQ)
        i_f=.GSCG_AFQ.BuildFilter()
        if !(i_f==.GSCG_AFQ.cQueryFilter)
          i_fnidx=.GSCG_AFQ.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AFQ.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AFQ.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AFQ.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AFQ.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_AVF)
        i_f=.GSCG_AVF.BuildFilter()
        if !(i_f==.GSCG_AVF.cQueryFilter)
          i_fnidx=.GSCG_AVF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AVF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AVF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AVF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AVF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_AIF)
        i_f=.GSCG_AIF.BuildFilter()
        if !(i_f==.GSCG_AIF.cQueryFilter)
          i_fnidx=.GSCG_AIF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AIF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AIF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AIF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AIF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MFSERIAL = NVL(MFSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_4_3_joined
    link_4_3_joined=.f.
    local link_4_12_joined
    link_4_12_joined=.f.
    local link_4_18_joined
    link_4_18_joined=.f.
    local link_4_24_joined
    link_4_24_joined=.f.
    local link_6_3_joined
    link_6_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOD_PAG where MFSERIAL=KeySet.MFSERIAL
    *
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_PAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_PAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_PAG '
      link_4_3_joined=this.AddJoinedLink_4_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_12_joined=this.AddJoinedLink_4_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_18_joined=this.AddJoinedLink_4_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_24_joined=this.AddJoinedLink_4_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_3_joined=this.AddJoinedLink_6_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_APPCOIN = space(1)
        .w_APPOIMP = 0
        .w_GENERA = Sys(2015)
        .w_IMPORTA = .F.
        .w_GENINPS = Sys(2015)
        .w_IMPINPS = .F.
        .w_DESAENTE = space(30)
        .w_GENPREV = Sys(2015)
        .w_RESCHK = 0
        .w_IMPPREV = .F.
        .w_TIPOMOD = 'NEW'
        .w_APPOIMP2 = 0
        .w_ANNORIF = '2003'
        .w_TIPTRI = space(10)
        .w_CAP1 = space(9)
        .w_LOCALI1 = space(30)
        .w_INDIRI1 = space(35)
        .w_PROVIN1 = space(2)
        .w_DESSTA = space(1)
        .w_CODAZIE = i_CODAZI
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFMESRIF = NVL(MFMESRIF,space(2))
        .w_MFANNRIF = NVL(MFANNRIF,space(4))
        .w_MFVALUTA = NVL(MFVALUTA,space(3))
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .w_MF_COINC = NVL(MF_COINC,space(1))
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCODUFF = NVL(MFCODUFF,space(3))
          * evitabile
          *.link_2_4('Load')
        .w_MFCODATT = NVL(MFCODATT,space(11))
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page2.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCDSED1 = NVL(MFCDSED1,space(5))
          * evitabile
          *.link_3_2('Load')
        .w_MFCCONT1 = NVL(MFCCONT1,space(5))
          * evitabile
          *.link_3_3('Load')
        .w_MFMINPS1 = NVL(MFMINPS1,space(17))
        .w_APP1A = !empty(.w_MFCCONT1) and alltrim(.w_MFCCONT1)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_APP1 = !empty(.w_MFCCONT1) and (alltrim(.w_MFCCONT1)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP1A)
        .w_MFDAMES1 = NVL(MFDAMES1,space(2))
        .w_MFDAANN1 = NVL(MFDAANN1,space(4))
        .w_MF_AMES1 = NVL(MF_AMES1,space(2))
        .w_MFAN1 = NVL(MFAN1,space(4))
        .w_MFIMPSD1 = NVL(MFIMPSD1,0)
        .w_MFIMPSC1 = NVL(MFIMPSC1,0)
        .w_MFCDSED2 = NVL(MFCDSED2,space(5))
          * evitabile
          *.link_3_13('Load')
        .w_MFCCONT2 = NVL(MFCCONT2,space(5))
          * evitabile
          *.link_3_14('Load')
        .w_MFMINPS2 = NVL(MFMINPS2,space(17))
        .w_APP2A = !empty(.w_MFCCONT2) and alltrim(.w_MFCCONT2)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES2 = NVL(MFDAMES2,space(2))
        .w_MFDAANN2 = NVL(MFDAANN2,space(4))
        .w_APP2 = !empty(.w_MFCCONT2) and (alltrim(.w_MFCCONT2)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBTU-EBIT-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP2A)
        .w_MF_AMES2 = NVL(MF_AMES2,space(2))
        .w_MFAN2 = NVL(MFAN2,space(4))
        .w_MFIMPSD2 = NVL(MFIMPSD2,0)
        .w_MFIMPSC2 = NVL(MFIMPSC2,0)
        .w_MFCDSED3 = NVL(MFCDSED3,space(5))
          * evitabile
          *.link_3_24('Load')
        .w_MFCCONT3 = NVL(MFCCONT3,space(5))
          * evitabile
          *.link_3_25('Load')
        .w_MFMINPS3 = NVL(MFMINPS3,space(17))
        .w_APP3A = !empty(.w_MFCCONT3) and alltrim(.w_MFCCONT3)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES3 = NVL(MFDAMES3,space(2))
        .w_APP3 = !empty(.w_MFCCONT3) and (alltrim(.w_MFCCONT3)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP3A)
        .w_MFDAANN3 = NVL(MFDAANN3,space(4))
        .w_MF_AMES3 = NVL(MF_AMES3,space(2))
        .w_MFAN3 = NVL(MFAN3,space(4))
        .w_MFIMPSD3 = NVL(MFIMPSD3,0)
        .w_MFIMPSC3 = NVL(MFIMPSC3,0)
        .w_MFCDSED4 = NVL(MFCDSED4,space(5))
          * evitabile
          *.link_3_35('Load')
        .w_MFCCONT4 = NVL(MFCCONT4,space(5))
          * evitabile
          *.link_3_36('Load')
        .w_MFMINPS4 = NVL(MFMINPS4,space(17))
        .w_APP4A = !empty(.w_MFCCONT4) and alltrim(.w_MFCCONT4)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES4 = NVL(MFDAMES4,space(2))
        .w_APP4 = !empty(.w_MFCCONT4) and (alltrim(.w_MFCCONT4)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP4A)
        .w_MFDAANN4 = NVL(MFDAANN4,space(4))
        .w_MF_AMES4 = NVL(MF_AMES4,space(2))
        .w_MFAN4 = NVL(MFAN4,space(4))
        .w_MFIMPSD4 = NVL(MFIMPSD4,0)
        .w_MFIMPSC4 = NVL(MFIMPSC4,0)
        .w_MFTOTDPS = NVL(MFTOTDPS,0)
        .w_MFTOTCPS = NVL(MFTOTCPS,0)
        .w_MFSALDPS = NVL(MFSALDPS,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCODRE1 = NVL(MFCODRE1,space(2))
          * evitabile
          *.link_4_2('Load')
        .w_MFTRIRE1 = NVL(MFTRIRE1,space(5))
          if link_4_3_joined
            this.w_MFTRIRE1 = NVL(TRCODICE403,NVL(this.w_MFTRIRE1,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI403,space(10))
          else
          .link_4_3('Load')
          endif
        .w_MFRATRE1 = NVL(MFRATRE1,space(4))
        .w_MFANNRE1 = NVL(MFANNRE1,space(4))
        .w_MFIMDRE1 = NVL(MFIMDRE1,0)
        .w_MFIMCRE1 = NVL(MFIMCRE1,0)
        .w_MFCODRE2 = NVL(MFCODRE2,space(2))
          * evitabile
          *.link_4_11('Load')
        .w_MFTRIRE2 = NVL(MFTRIRE2,space(5))
          if link_4_12_joined
            this.w_MFTRIRE2 = NVL(TRCODICE412,NVL(this.w_MFTRIRE2,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI412,space(10))
          else
          .link_4_12('Load')
          endif
        .w_MFRATRE2 = NVL(MFRATRE2,space(4))
        .w_MFANNRE2 = NVL(MFANNRE2,space(4))
        .w_MFIMDRE2 = NVL(MFIMDRE2,0)
        .w_MFIMCRE2 = NVL(MFIMCRE2,0)
        .w_MFCODRE3 = NVL(MFCODRE3,space(2))
          * evitabile
          *.link_4_17('Load')
        .w_MFTRIRE3 = NVL(MFTRIRE3,space(5))
          if link_4_18_joined
            this.w_MFTRIRE3 = NVL(TRCODICE418,NVL(this.w_MFTRIRE3,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI418,space(10))
          else
          .link_4_18('Load')
          endif
        .w_MFRATRE3 = NVL(MFRATRE3,space(4))
        .w_MFANNRE3 = NVL(MFANNRE3,space(4))
        .w_MFIMDRE3 = NVL(MFIMDRE3,0)
        .w_MFIMCRE3 = NVL(MFIMCRE3,0)
        .w_MFCODRE4 = NVL(MFCODRE4,space(2))
          * evitabile
          *.link_4_23('Load')
        .w_MFTRIRE4 = NVL(MFTRIRE4,space(5))
          if link_4_24_joined
            this.w_MFTRIRE4 = NVL(TRCODICE424,NVL(this.w_MFTRIRE4,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI424,space(10))
          else
          .link_4_24('Load')
          endif
        .w_MFRATRE4 = NVL(MFRATRE4,space(4))
        .w_MFANNRE4 = NVL(MFANNRE4,space(4))
        .w_MFIMDRE4 = NVL(MFIMDRE4,0)
        .w_MFIMCRE4 = NVL(MFIMCRE4,0)
        .w_MFTOTDRE = NVL(MFTOTDRE,0)
        .w_MFTOTCRE = NVL(MFTOTCRE,0)
        .w_MFSALDRE = NVL(MFSALDRE,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFSINAI1 = NVL(MFSINAI1,space(5))
          * evitabile
          *.link_5_2('Load')
        .w_MF_NPOS1 = NVL(MF_NPOS1,space(8))
        .w_MF_PACC1 = NVL(MF_PACC1,space(2))
        .w_MF_NRIF1 = NVL(MF_NRIF1,space(6))
        .w_MFCAUSA1 = NVL(MFCAUSA1,space(2))
        .w_MFIMDIL1 = NVL(MFIMDIL1,0)
        .w_MFIMCIL1 = NVL(MFIMCIL1,0)
        .w_MFSINAI2 = NVL(MFSINAI2,space(5))
          * evitabile
          *.link_5_9('Load')
        .w_MF_NPOS2 = NVL(MF_NPOS2,space(8))
        .w_MF_PACC2 = NVL(MF_PACC2,space(2))
        .w_MF_NRIF2 = NVL(MF_NRIF2,space(6))
        .w_MFCAUSA2 = NVL(MFCAUSA2,space(2))
        .w_MFIMDIL2 = NVL(MFIMDIL2,0)
        .w_MFIMCIL2 = NVL(MFIMCIL2,0)
        .w_MFSINAI3 = NVL(MFSINAI3,space(5))
          * evitabile
          *.link_5_16('Load')
        .w_MF_NPOS3 = NVL(MF_NPOS3,space(8))
        .w_MF_PACC3 = NVL(MF_PACC3,space(2))
        .w_MF_NRIF3 = NVL(MF_NRIF3,space(6))
        .w_MFCAUSA3 = NVL(MFCAUSA3,space(2))
        .w_MFIMDIL3 = NVL(MFIMDIL3,0)
        .w_MFIMCIL3 = NVL(MFIMCIL3,0)
        .w_MFTDINAI = NVL(MFTDINAI,0)
        .w_MFTCINAI = NVL(MFTCINAI,0)
        .w_MFSALINA = NVL(MFSALINA,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCDENTE = NVL(MFCDENTE,space(5))
          if link_6_3_joined
            this.w_MFCDENTE = NVL(CPCODICE603,NVL(this.w_MFCDENTE,space(5)))
            this.w_DESAENTE = NVL(CPDESCRI603,space(30))
          else
          .link_6_3('Load')
          endif
        .w_VRSDENT1 = space(5)
          .link_6_4('Load')
        .w_VPSDENT1 = space(2)
          .link_6_5('Load')
        .w_MFSDENT1 = NVL(MFSDENT1,space(5))
        .w_MFCCOAE1 = NVL(MFCCOAE1,space(5))
          * evitabile
          *.link_6_7('Load')
        .w_OLSDENT1 = .w_MFSDENT1
        .w_MFCDPOS1 = NVL(MFCDPOS1,space(9))
        .w_MFMSINE1 = NVL(MFMSINE1,space(2))
        .w_MFANINE1 = NVL(MFANINE1,space(4))
        .w_MFMSFIE1 = NVL(MFMSFIE1,space(2))
        .w_MFANF1 = NVL(MFANF1,space(4))
        .w_MFIMDAE1 = NVL(MFIMDAE1,0)
        .w_MFIMCAE1 = NVL(MFIMCAE1,0)
        .w_VPSDENT2 = space(2)
          .link_6_16('Load')
        .w_VRSDENT2 = space(5)
          .link_6_17('Load')
        .w_MFSDENT2 = NVL(MFSDENT2,space(5))
        .w_MFCCOAE2 = NVL(MFCCOAE2,space(5))
          * evitabile
          *.link_6_19('Load')
        .w_OLSDENT2 = .w_MFSDENT2
        .w_MFCDPOS2 = NVL(MFCDPOS2,space(9))
        .w_MFMSINE2 = NVL(MFMSINE2,space(2))
        .w_MFANINE2 = NVL(MFANINE2,space(4))
        .w_MFMSFIE2 = NVL(MFMSFIE2,space(2))
        .w_MFANF2 = NVL(MFANF2,space(4))
        .w_MFIMDAE2 = NVL(MFIMDAE2,0)
        .w_MFIMCAE2 = NVL(MFIMCAE2,0)
        .w_MFTDAENT = NVL(MFTDAENT,0)
        .w_MFTCAENT = NVL(MFTCAENT,0)
        .w_MFSALAEN = NVL(MFSALAEN,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFSALFIN = NVL(MFSALFIN,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page6.oPag.oObj_6_51.Calculate()
        .oPgFrm.Page7.oPag.oObj_7_10.Calculate()
        .w_MFDESSTA = NVL(MFDESSTA,space(5))
          .link_7_11('Load')
        .w_CAP = iif(empty(NVL(.w_MFDESSTA,'')),g_capazi,.w_CAP1)
        .w_LOCALI = UPPER(iif(EMPTY(NVL(.w_MFDESSTA,'')),g_locazi,.w_LOCALI1))
        .w_PROVIN = UPPER(iif(empty(NVL(.w_MFDESSTA,'')),g_proazi,.w_PROVIN1))
        .w_INDIRI = UPPER(iif(empty(NVL(.w_MFDESSTA,'')),g_indazi,.w_INDIRI1))
        .w_MFTIPMOD = NVL(MFTIPMOD,space(10))
        .oPgFrm.Page7.oPag.oObj_7_29.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_68.Calculate(ah_MSGFORMAT('Codice %0sede'))
        .oPgFrm.Page3.oPag.oObj_3_69.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page4.oPag.oObj_4_44.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page4.oPag.oObj_4_45.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page4.oPag.oObj_4_46.Calculate(ah_MSGFORMAT('Codice %0regione'))
        .oPgFrm.Page5.oPag.oObj_5_42.Calculate(ah_MSGFORMAT('Numero di %0riferimento'))
        .oPgFrm.Page6.oPag.oObj_6_52.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page6.oPag.oObj_6_53.Calculate(iif(.w_MFCDENTE<>'0003',ah_MSGFORMAT('Codice %0sede'),ah_MSGFORMAT('Codice %0provincia')))
        .oPgFrm.Page6.oPag.oObj_6_54.Calculate(ah_MSGFORMAT('Codice %0posizione'))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'MOD_PAG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page7.oPag.oBtn_7_28.enabled = this.oPgFrm.Page7.oPag.oBtn_7_28.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_aoo
    this.w_VPSDENT1=this.w_OLSDENT1
    this.w_VRSDENT1=this.w_OLSDENT1
    this.w_VPSDENT2=this.w_OLSDENT2
    this.w_VRSDENT2=this.w_OLSDENT2
    this.SaveDependsOn()
    this.SetControlsValue()
    if this.GSCG_AVF.w_VFDTPRES <= cp_CharToDate('31-12-2005')
      this.BlankRec()
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MFSERIAL = space(10)
      .w_MFMESRIF = space(2)
      .w_MFANNRIF = space(4)
      .w_MFVALUTA = space(3)
      .w_APPCOIN = space(1)
      .w_MF_COINC = space(1)
      .w_MFSERIAL = space(10)
      .w_APPOIMP = 0
      .w_MFCODUFF = space(3)
      .w_MFCODATT = space(11)
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_GENERA = space(10)
      .w_IMPORTA = .f.
      .w_MFSERIAL = space(10)
      .w_MFCDSED1 = space(5)
      .w_MFCCONT1 = space(5)
      .w_MFMINPS1 = space(17)
      .w_APP1A = .f.
      .w_APP1 = .f.
      .w_MFDAMES1 = space(2)
      .w_MFDAANN1 = space(4)
      .w_MF_AMES1 = space(2)
      .w_MFAN1 = space(4)
      .w_MFIMPSD1 = 0
      .w_MFIMPSC1 = 0
      .w_MFCDSED2 = space(5)
      .w_MFCCONT2 = space(5)
      .w_MFMINPS2 = space(17)
      .w_APP2A = .f.
      .w_MFDAMES2 = space(2)
      .w_MFDAANN2 = space(4)
      .w_APP2 = .f.
      .w_MF_AMES2 = space(2)
      .w_MFAN2 = space(4)
      .w_MFIMPSD2 = 0
      .w_MFIMPSC2 = 0
      .w_MFCDSED3 = space(5)
      .w_MFCCONT3 = space(5)
      .w_MFMINPS3 = space(17)
      .w_APP3A = .f.
      .w_MFDAMES3 = space(2)
      .w_APP3 = .f.
      .w_MFDAANN3 = space(4)
      .w_MF_AMES3 = space(2)
      .w_MFAN3 = space(4)
      .w_MFIMPSD3 = 0
      .w_MFIMPSC3 = 0
      .w_MFCDSED4 = space(5)
      .w_MFCCONT4 = space(5)
      .w_MFMINPS4 = space(17)
      .w_APP4A = .f.
      .w_MFDAMES4 = space(2)
      .w_APP4 = .f.
      .w_MFDAANN4 = space(4)
      .w_MF_AMES4 = space(2)
      .w_MFAN4 = space(4)
      .w_MFIMPSD4 = 0
      .w_MFIMPSC4 = 0
      .w_MFTOTDPS = 0
      .w_MFTOTCPS = 0
      .w_MFSALDPS = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_GENINPS = space(10)
      .w_IMPINPS = .f.
      .w_MFSERIAL = space(10)
      .w_MFCODRE1 = space(2)
      .w_MFTRIRE1 = space(5)
      .w_MFRATRE1 = space(4)
      .w_MFANNRE1 = space(4)
      .w_MFIMDRE1 = 0
      .w_MFIMCRE1 = 0
      .w_MFCODRE2 = space(2)
      .w_MFTRIRE2 = space(5)
      .w_MFRATRE2 = space(4)
      .w_MFANNRE2 = space(4)
      .w_MFIMDRE2 = 0
      .w_MFIMCRE2 = 0
      .w_MFCODRE3 = space(2)
      .w_MFTRIRE3 = space(5)
      .w_MFRATRE3 = space(4)
      .w_MFANNRE3 = space(4)
      .w_MFIMDRE3 = 0
      .w_MFIMCRE3 = 0
      .w_MFCODRE4 = space(2)
      .w_MFTRIRE4 = space(5)
      .w_MFRATRE4 = space(4)
      .w_MFANNRE4 = space(4)
      .w_MFIMDRE4 = 0
      .w_MFIMCRE4 = 0
      .w_MFTOTDRE = 0
      .w_MFTOTCRE = 0
      .w_MFSALDRE = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_MFSERIAL = space(10)
      .w_MFSINAI1 = space(5)
      .w_MF_NPOS1 = space(8)
      .w_MF_PACC1 = space(2)
      .w_MF_NRIF1 = space(6)
      .w_MFCAUSA1 = space(2)
      .w_MFIMDIL1 = 0
      .w_MFIMCIL1 = 0
      .w_MFSINAI2 = space(5)
      .w_MF_NPOS2 = space(8)
      .w_MF_PACC2 = space(2)
      .w_MF_NRIF2 = space(6)
      .w_MFCAUSA2 = space(2)
      .w_MFIMDIL2 = 0
      .w_MFIMCIL2 = 0
      .w_MFSINAI3 = space(5)
      .w_MF_NPOS3 = space(8)
      .w_MF_PACC3 = space(2)
      .w_MF_NRIF3 = space(6)
      .w_MFCAUSA3 = space(2)
      .w_MFIMDIL3 = 0
      .w_MFIMCIL3 = 0
      .w_MFTDINAI = 0
      .w_MFTCINAI = 0
      .w_MFSALINA = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_MFSERIAL = space(10)
      .w_MFCDENTE = space(5)
      .w_VRSDENT1 = space(5)
      .w_VPSDENT1 = space(5)
      .w_MFSDENT1 = space(5)
      .w_MFCCOAE1 = space(5)
      .w_OLSDENT1 = space(5)
      .w_MFCDPOS1 = space(9)
      .w_MFMSINE1 = space(2)
      .w_MFANINE1 = space(4)
      .w_MFMSFIE1 = space(2)
      .w_MFANF1 = space(4)
      .w_MFIMDAE1 = 0
      .w_MFIMCAE1 = 0
      .w_VPSDENT2 = space(5)
      .w_VRSDENT2 = space(5)
      .w_MFSDENT2 = space(5)
      .w_MFCCOAE2 = space(5)
      .w_OLSDENT2 = space(5)
      .w_MFCDPOS2 = space(9)
      .w_MFMSINE2 = space(2)
      .w_MFANINE2 = space(4)
      .w_MFMSFIE2 = space(2)
      .w_MFANF2 = space(4)
      .w_MFIMDAE2 = 0
      .w_MFIMCAE2 = 0
      .w_MFTDAENT = 0
      .w_MFTCAENT = 0
      .w_MFSALAEN = 0
      .w_DESAENTE = space(30)
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_GENPREV = space(10)
      .w_RESCHK = 0
      .w_IMPPREV = .f.
      .w_MFSERIAL = space(10)
      .w_MFSALFIN = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_TIPOMOD = space(3)
      .w_APPOIMP2 = 0
      .w_ANNORIF = space(4)
      .w_TIPTRI = space(10)
      .w_MFDESSTA = space(5)
      .w_CAP1 = space(9)
      .w_LOCALI1 = space(30)
      .w_INDIRI1 = space(35)
      .w_PROVIN1 = space(2)
      .w_CAP = space(9)
      .w_LOCALI = space(30)
      .w_PROVIN = space(2)
      .w_INDIRI = space(35)
      .w_DESSTA = space(1)
      .w_CODAZIE = space(5)
      .w_MFTIPMOD = space(10)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_MFMESRIF = right('00'+ALLTRIM(STR(MONTH(i_datsys))),2)
        .w_MFANNRIF = ALLTRIM(STR(YEAR(i_datsys)))
        .w_MFVALUTA = g_CODEUR
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
          .DoRTCalc(5,5,.f.)
        .w_MF_COINC = .w_APPCOIN
        .DoRTCalc(7,9,.f.)
          if not(empty(.w_MFCODUFF))
          .link_2_4('Full')
          endif
          .DoRTCalc(10,10,.f.)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_GENERA = Sys(2015)
        .oPgFrm.Page2.oPag.oObj_2_13.Calculate()
        .w_IMPORTA = .F.
        .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_MFCDSED1))
          .link_3_2('Full')
          endif
        .w_MFCCONT1 = iif(empty(.w_MFCDSED1),' ',.w_MFCCONT1)
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_MFCCONT1))
          .link_3_3('Full')
          endif
        .w_MFMINPS1 = iif(empty(.w_MFCDSED1),' ',.w_MFMINPS1)
        .w_APP1A = !empty(.w_MFCCONT1) and alltrim(.w_MFCCONT1)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_APP1 = !empty(.w_MFCCONT1) and (alltrim(.w_MFCCONT1)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP1A)
        .w_MFDAMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1A,' ',.w_MFDAMES1))
        .w_MFDAANN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1A,' ',.w_MFDAANN1))
        .w_MF_AMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,' ',.w_MF_AMES1))
        .w_MFAN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,' ',.w_MFAN1))
        .w_MFIMPSD1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSD1)
        .w_MFIMPSC1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSC1)
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_MFCDSED2))
          .link_3_13('Full')
          endif
        .w_MFCCONT2 = iif(empty(.w_MFCDSED2),' ',.w_MFCCONT2)
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_MFCCONT2))
          .link_3_14('Full')
          endif
        .w_MFMINPS2 = iif(empty(.w_MFCDSED2),' ',.w_MFMINPS2)
        .w_APP2A = !empty(.w_MFCCONT2) and alltrim(.w_MFCCONT2)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2A,' ',.w_MFDAMES2))
        .w_MFDAANN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2A,' ',.w_MFDAANN2))
        .w_APP2 = !empty(.w_MFCCONT2) and (alltrim(.w_MFCCONT2)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBTU-EBIT-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP2A)
        .w_MF_AMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,' ',.w_MF_AMES2))
        .w_MFAN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,' ',.w_MFAN2))
        .w_MFIMPSD2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSD2)
        .w_MFIMPSC2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSC2)
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_MFCDSED3))
          .link_3_24('Full')
          endif
        .w_MFCCONT3 = iif(empty(.w_MFCDSED3),' ',.w_MFCCONT3)
        .DoRTCalc(39,39,.f.)
          if not(empty(.w_MFCCONT3))
          .link_3_25('Full')
          endif
        .w_MFMINPS3 = iif(empty(.w_MFCDSED3),' ',.w_MFMINPS3)
        .w_APP3A = !empty(.w_MFCCONT3) and alltrim(.w_MFCCONT3)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3A,' ',.w_MFDAMES3))
        .w_APP3 = !empty(.w_MFCCONT3) and (alltrim(.w_MFCCONT3)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP3A)
        .w_MFDAANN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3A,' ',.w_MFDAANN3))
        .w_MF_AMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,' ',.w_MF_AMES3))
        .w_MFAN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,' ',.w_MFAN3))
        .w_MFIMPSD3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSD3)
        .w_MFIMPSC3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSC3)
        .DoRTCalc(49,49,.f.)
          if not(empty(.w_MFCDSED4))
          .link_3_35('Full')
          endif
        .w_MFCCONT4 = iif(empty(.w_MFCDSED4),' ',.w_MFCCONT4)
        .DoRTCalc(50,50,.f.)
          if not(empty(.w_MFCCONT4))
          .link_3_36('Full')
          endif
        .w_MFMINPS4 = iif(empty(.w_MFCDSED4),' ',.w_MFMINPS4)
        .w_APP4A = !empty(.w_MFCCONT4) and alltrim(.w_MFCCONT4)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4A,' ',.w_MFDAMES4))
        .w_APP4 = !empty(.w_MFCCONT4) and (alltrim(.w_MFCCONT4)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP4A)
        .w_MFDAANN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4A,' ',.w_MFDAANN4))
        .w_MF_AMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,' ',.w_MF_AMES4))
        .w_MFAN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,' ',.w_MFAN4))
        .w_MFIMPSD4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSD4)
        .w_MFIMPSC4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSC4)
        .w_MFTOTDPS = .w_MFIMPSD1+.w_MFIMPSD2+.w_MFIMPSD3+.w_MFIMPSD4
        .w_MFTOTCPS = .w_MFIMPSC1+.w_MFIMPSC2+.w_MFIMPSC3+.w_MFIMPSC4
        .w_MFSALDPS = .w_MFTOTDPS-.w_MFTOTCPS
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_GENINPS = Sys(2015)
        .w_IMPINPS = .F.
        .DoRTCalc(67,68,.f.)
          if not(empty(.w_MFCODRE1))
          .link_4_2('Full')
          endif
        .w_MFTRIRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFTRIRE1)
        .DoRTCalc(69,69,.f.)
          if not(empty(.w_MFTRIRE1))
          .link_4_3('Full')
          endif
        .w_MFRATRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFRATRE1)
        .w_MFANNRE1 = iif(empty(.w_MFCODRE1),' ',iif(.w_MFTRIRE1= '3805','0000',.w_MFANNRE1))
        .w_MFIMDRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMDRE1)
        .w_MFIMCRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMCRE1)
        .DoRTCalc(74,74,.f.)
          if not(empty(.w_MFCODRE2))
          .link_4_11('Full')
          endif
        .w_MFTRIRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFTRIRE2)
        .DoRTCalc(75,75,.f.)
          if not(empty(.w_MFTRIRE2))
          .link_4_12('Full')
          endif
        .w_MFRATRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFRATRE2)
        .w_MFANNRE2 = iif(empty(.w_MFCODRE2),' ',iif(.w_MFTRIRE2 ='3805','0000',.w_MFANNRE2))
        .w_MFIMDRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMDRE2)
        .w_MFIMCRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMCRE2)
        .DoRTCalc(80,80,.f.)
          if not(empty(.w_MFCODRE3))
          .link_4_17('Full')
          endif
        .w_MFTRIRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFTRIRE3)
        .DoRTCalc(81,81,.f.)
          if not(empty(.w_MFTRIRE3))
          .link_4_18('Full')
          endif
        .w_MFRATRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFRATRE3)
        .w_MFANNRE3 = iif(empty(.w_MFCODRE3),' ',iif(.w_MFTRIRE3 = '3805','0000',.w_MFANNRE3))
        .w_MFIMDRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMDRE3)
        .w_MFIMCRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMCRE3)
        .DoRTCalc(86,86,.f.)
          if not(empty(.w_MFCODRE4))
          .link_4_23('Full')
          endif
        .w_MFTRIRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFTRIRE4)
        .DoRTCalc(87,87,.f.)
          if not(empty(.w_MFTRIRE4))
          .link_4_24('Full')
          endif
        .w_MFRATRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFRATRE4)
        .w_MFANNRE4 = iif(empty(.w_MFCODRE4),' ',iif( .w_MFTRIRE4 = '3805','0000',.w_MFANNRE4))
        .w_MFIMDRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMDRE4)
        .w_MFIMCRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMCRE4)
        .w_MFTOTDRE = .w_MFIMDRE1+.w_MFIMDRE2+.w_MFIMDRE3+.w_MFIMDRE4
        .w_MFTOTCRE = .w_MFIMCRE1+.w_MFIMCRE2+.w_MFIMCRE3+.w_MFIMCRE4
        .w_MFSALDRE = .w_MFTOTDRE-.w_MFTOTCRE
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .DoRTCalc(97,98,.f.)
          if not(empty(.w_MFSINAI1))
          .link_5_2('Full')
          endif
        .w_MF_NPOS1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NPOS1)
        .w_MF_PACC1 = iif(empty(.w_MFSINAI1),' ',.w_MF_PACC1)
        .w_MF_NRIF1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NRIF1)
        .w_MFCAUSA1 = iif(empty(.w_MFSINAI1),' ',.w_MFCAUSA1)
        .w_MFIMDIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMDIL1)
        .w_MFIMCIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMCIL1)
        .DoRTCalc(105,105,.f.)
          if not(empty(.w_MFSINAI2))
          .link_5_9('Full')
          endif
        .w_MF_NPOS2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NPOS2)
        .w_MF_PACC2 = iif(empty(.w_MFSINAI2),' ',.w_MF_PACC2)
        .w_MF_NRIF2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NRIF2)
        .w_MFCAUSA2 = iif(empty(.w_MFSINAI2),' ',.w_MFCAUSA2)
        .w_MFIMDIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMDIL2)
        .w_MFIMCIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMCIL2)
        .DoRTCalc(112,112,.f.)
          if not(empty(.w_MFSINAI3))
          .link_5_16('Full')
          endif
        .w_MF_NPOS3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NPOS3)
        .w_MF_PACC3 = iif(empty(.w_MFSINAI3),' ',.w_MF_PACC3)
        .w_MF_NRIF3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NRIF3)
        .w_MFCAUSA3 = iif(empty(.w_MFSINAI3),' ',.w_MFCAUSA3)
        .w_MFIMDIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMDIL3)
        .w_MFIMCIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMCIL3)
        .w_MFTDINAI = .w_MFIMDIL1+.w_MFIMDIL2+.w_MFIMDIL3
        .w_MFTCINAI = .w_MFIMCIL1+.w_MFIMCIL2+.w_MFIMCIL3
        .w_MFSALINA = .w_MFTDINAI-.w_MFTCINAI
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .DoRTCalc(124,125,.f.)
          if not(empty(.w_MFCDENTE))
          .link_6_3('Full')
          endif
        .w_VRSDENT1 = space(5)
        .DoRTCalc(126,126,.f.)
          if not(empty(.w_VRSDENT1))
          .link_6_4('Full')
          endif
        .w_VPSDENT1 = space(2)
        .DoRTCalc(127,127,.f.)
          if not(empty(.w_VPSDENT1))
          .link_6_5('Full')
          endif
        .w_MFSDENT1 = iif(.w_MFCDENTE='0003',.w_VPSDENT1,.w_VRSDENT1)
        .w_MFCCOAE1 = iif(empty(.w_MFCDENTE) ,' ',.w_MFCCOAE1)
        .DoRTCalc(129,129,.f.)
          if not(empty(.w_MFCCOAE1))
          .link_6_7('Full')
          endif
        .w_OLSDENT1 = .w_MFSDENT1
        .w_MFCDPOS1 = iif(empty(.w_MFCCOAE1),' ',.w_MFCDPOS1)
        .w_MFMSINE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR',' ',.w_MFMSINE1))
        .w_MFANINE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR',' ',.w_MFANINE1))
        .w_MFMSFIE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFMSFIE1))
        .w_MFANF1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFANF1))
        .w_MFIMDAE1 = iif(empty(.w_MFCCOAE1),0,.w_MFIMDAE1)
        .w_MFIMCAE1 = iif(empty(.w_MFCCOAE1) or  .w_MFCDENTE='0003',0,.w_MFIMCAE1)
        .w_VPSDENT2 = space(2)
        .DoRTCalc(138,138,.f.)
          if not(empty(.w_VPSDENT2))
          .link_6_16('Full')
          endif
        .w_VRSDENT2 = space(5)
        .DoRTCalc(139,139,.f.)
          if not(empty(.w_VRSDENT2))
          .link_6_17('Full')
          endif
        .w_MFSDENT2 = iif(.w_MFCDENTE='0003',.w_VPSDENT2,.w_VRSDENT2)
        .w_MFCCOAE2 = iif(empty(.w_MFCDENTE),' ',.w_MFCCOAE2)
        .DoRTCalc(141,141,.f.)
          if not(empty(.w_MFCCOAE2))
          .link_6_19('Full')
          endif
        .w_OLSDENT2 = .w_MFSDENT2
        .w_MFCDPOS2 = iif(empty(.w_MFCCOAE2),' ',.w_MFCDPOS2)
        .w_MFMSINE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR',' ',.w_MFMSINE2))
        .w_MFANINE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR',' ',.w_MFANINE2))
        .w_MFMSFIE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFMSFIE2))
        .w_MFANF2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFANF2))
        .w_MFIMDAE2 = iif(empty(.w_MFCCOAE2),0,.w_MFIMDAE2)
        .w_MFIMCAE2 = iif(empty(.w_MFCCOAE2) or .w_MFCDENTE='0003',0,.w_MFIMCAE2)
        .w_MFTDAENT = .w_MFIMDAE1+.w_MFIMDAE2
        .w_MFTCAENT = .w_MFIMCAE1+.w_MFIMCAE2
        .w_MFSALAEN = .w_MFTDAENT-.w_MFTCAENT
          .DoRTCalc(153,153,.f.)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_GENPREV = Sys(2015)
          .DoRTCalc(157,157,.f.)
        .w_IMPPREV = .F.
          .DoRTCalc(159,159,.f.)
        .w_MFSALFIN = .w_APPOIMP + .w_APPOIMP2 + .w_MFSALDPS + .w_MFSALDRE + .w_MFSALINA + .w_MFSALAEN
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_TIPOMOD = 'NEW'
        .oPgFrm.Page6.oPag.oObj_6_51.Calculate()
          .DoRTCalc(164,164,.f.)
        .w_ANNORIF = '2003'
        .oPgFrm.Page7.oPag.oObj_7_10.Calculate()
        .DoRTCalc(166,167,.f.)
          if not(empty(.w_MFDESSTA))
          .link_7_11('Full')
          endif
          .DoRTCalc(168,171,.f.)
        .w_CAP = iif(empty(NVL(.w_MFDESSTA,'')),g_capazi,.w_CAP1)
        .w_LOCALI = UPPER(iif(EMPTY(NVL(.w_MFDESSTA,'')),g_locazi,.w_LOCALI1))
        .w_PROVIN = UPPER(iif(empty(NVL(.w_MFDESSTA,'')),g_proazi,.w_PROVIN1))
        .w_INDIRI = UPPER(iif(empty(NVL(.w_MFDESSTA,'')),g_indazi,.w_INDIRI1))
          .DoRTCalc(176,176,.f.)
        .w_CODAZIE = i_CODAZI
        .w_MFTIPMOD = '2006'
        .oPgFrm.Page7.oPag.oObj_7_29.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_68.Calculate(ah_MSGFORMAT('Codice %0sede'))
        .oPgFrm.Page3.oPag.oObj_3_69.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page4.oPag.oObj_4_44.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page4.oPag.oObj_4_45.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page4.oPag.oObj_4_46.Calculate(ah_MSGFORMAT('Codice %0regione'))
        .oPgFrm.Page5.oPag.oObj_5_42.Calculate(ah_MSGFORMAT('Numero di %0riferimento'))
        .oPgFrm.Page6.oPag.oObj_6_52.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page6.oPag.oObj_6_53.Calculate(iif(.w_MFCDENTE<>'0003',ah_MSGFORMAT('Codice %0sede'),ah_MSGFORMAT('Codice %0provincia')))
        .oPgFrm.Page6.oPag.oObj_6_54.Calculate(ah_MSGFORMAT('Codice %0posizione'))
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_PAG')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page7.oPag.oBtn_7_28.enabled = this.oPgFrm.Page7.oPag.oBtn_7_28.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_aoo
    * instanzio il figlio della seconda,quarta e settima pagina immediatamente
       if Upper(this.GSCG_AVF.class)='STDDYNAMICCHILD'
         This.oPgFrm.Pages[7].opag.uienable(.T.)
         This.oPgFrm.ActivePage=1
       Endif
       if Upper(this.GSCG_AIF.class)='STDDYNAMICCHILD'
         This.oPgFrm.Pages[4].opag.uienable(.T.)
         This.oPgFrm.ActivePage=1
       Endif
       if Upper(this.GSCG_AFQ.class)='STDDYNAMICCHILD'
         This.oPgFrm.Pages[2].opag.uienable(.T.)
         This.oPgFrm.ActivePage=1
       Endif
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"MODEL","i_CODAZI,w_MFSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_MFSERIAL = .w_MFSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMFSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oMFMESRIF_1_2.enabled = i_bVal
      .Page1.oPag.oMFANNRIF_1_3.enabled = i_bVal
      .Page1.oPag.oMF_COINC_1_7.enabled = i_bVal
      .Page2.oPag.oMFCODUFF_2_4.enabled = i_bVal
      .Page2.oPag.oMFCODATT_2_5.enabled = i_bVal
      .Page3.oPag.oMFCDSED1_3_2.enabled = i_bVal
      .Page3.oPag.oMFCCONT1_3_3.enabled = i_bVal
      .Page3.oPag.oMFMINPS1_3_4.enabled = i_bVal
      .Page3.oPag.oMFDAMES1_3_7.enabled = i_bVal
      .Page3.oPag.oMFDAANN1_3_8.enabled = i_bVal
      .Page3.oPag.oMF_AMES1_3_9.enabled = i_bVal
      .Page3.oPag.oMFAN1_3_10.enabled = i_bVal
      .Page3.oPag.oMFIMPSD1_3_11.enabled = i_bVal
      .Page3.oPag.oMFIMPSC1_3_12.enabled = i_bVal
      .Page3.oPag.oMFCDSED2_3_13.enabled = i_bVal
      .Page3.oPag.oMFCCONT2_3_14.enabled = i_bVal
      .Page3.oPag.oMFMINPS2_3_15.enabled = i_bVal
      .Page3.oPag.oMFDAMES2_3_17.enabled = i_bVal
      .Page3.oPag.oMFDAANN2_3_18.enabled = i_bVal
      .Page3.oPag.oMF_AMES2_3_20.enabled = i_bVal
      .Page3.oPag.oMFAN2_3_21.enabled = i_bVal
      .Page3.oPag.oMFIMPSD2_3_22.enabled = i_bVal
      .Page3.oPag.oMFIMPSC2_3_23.enabled = i_bVal
      .Page3.oPag.oMFCDSED3_3_24.enabled = i_bVal
      .Page3.oPag.oMFCCONT3_3_25.enabled = i_bVal
      .Page3.oPag.oMFMINPS3_3_26.enabled = i_bVal
      .Page3.oPag.oMFDAMES3_3_28.enabled = i_bVal
      .Page3.oPag.oMFDAANN3_3_30.enabled = i_bVal
      .Page3.oPag.oMF_AMES3_3_31.enabled = i_bVal
      .Page3.oPag.oMFAN3_3_32.enabled = i_bVal
      .Page3.oPag.oMFIMPSD3_3_33.enabled = i_bVal
      .Page3.oPag.oMFIMPSC3_3_34.enabled = i_bVal
      .Page3.oPag.oMFCDSED4_3_35.enabled = i_bVal
      .Page3.oPag.oMFCCONT4_3_36.enabled = i_bVal
      .Page3.oPag.oMFMINPS4_3_37.enabled = i_bVal
      .Page3.oPag.oMFDAMES4_3_39.enabled = i_bVal
      .Page3.oPag.oMFDAANN4_3_41.enabled = i_bVal
      .Page3.oPag.oMF_AMES4_3_42.enabled = i_bVal
      .Page3.oPag.oMFAN4_3_43.enabled = i_bVal
      .Page3.oPag.oMFIMPSD4_3_44.enabled = i_bVal
      .Page3.oPag.oMFIMPSC4_3_45.enabled = i_bVal
      .Page4.oPag.oMFCODRE1_4_2.enabled = i_bVal
      .Page4.oPag.oMFTRIRE1_4_3.enabled = i_bVal
      .Page4.oPag.oMFRATRE1_4_4.enabled = i_bVal
      .Page4.oPag.oMFANNRE1_4_5.enabled = i_bVal
      .Page4.oPag.oMFIMDRE1_4_6.enabled = i_bVal
      .Page4.oPag.oMFIMCRE1_4_7.enabled = i_bVal
      .Page4.oPag.oMFCODRE2_4_11.enabled = i_bVal
      .Page4.oPag.oMFTRIRE2_4_12.enabled = i_bVal
      .Page4.oPag.oMFRATRE2_4_13.enabled = i_bVal
      .Page4.oPag.oMFANNRE2_4_14.enabled = i_bVal
      .Page4.oPag.oMFIMDRE2_4_15.enabled = i_bVal
      .Page4.oPag.oMFIMCRE2_4_16.enabled = i_bVal
      .Page4.oPag.oMFCODRE3_4_17.enabled = i_bVal
      .Page4.oPag.oMFTRIRE3_4_18.enabled = i_bVal
      .Page4.oPag.oMFRATRE3_4_19.enabled = i_bVal
      .Page4.oPag.oMFANNRE3_4_20.enabled = i_bVal
      .Page4.oPag.oMFIMDRE3_4_21.enabled = i_bVal
      .Page4.oPag.oMFIMCRE3_4_22.enabled = i_bVal
      .Page4.oPag.oMFCODRE4_4_23.enabled = i_bVal
      .Page4.oPag.oMFTRIRE4_4_24.enabled = i_bVal
      .Page4.oPag.oMFRATRE4_4_25.enabled = i_bVal
      .Page4.oPag.oMFANNRE4_4_26.enabled = i_bVal
      .Page4.oPag.oMFIMDRE4_4_27.enabled = i_bVal
      .Page4.oPag.oMFIMCRE4_4_28.enabled = i_bVal
      .Page5.oPag.oMFSINAI1_5_2.enabled = i_bVal
      .Page5.oPag.oMF_NPOS1_5_3.enabled = i_bVal
      .Page5.oPag.oMF_PACC1_5_4.enabled = i_bVal
      .Page5.oPag.oMF_NRIF1_5_5.enabled = i_bVal
      .Page5.oPag.oMFCAUSA1_5_6.enabled = i_bVal
      .Page5.oPag.oMFIMDIL1_5_7.enabled = i_bVal
      .Page5.oPag.oMFIMCIL1_5_8.enabled = i_bVal
      .Page5.oPag.oMFSINAI2_5_9.enabled = i_bVal
      .Page5.oPag.oMF_NPOS2_5_10.enabled = i_bVal
      .Page5.oPag.oMF_PACC2_5_11.enabled = i_bVal
      .Page5.oPag.oMF_NRIF2_5_12.enabled = i_bVal
      .Page5.oPag.oMFCAUSA2_5_13.enabled = i_bVal
      .Page5.oPag.oMFIMDIL2_5_14.enabled = i_bVal
      .Page5.oPag.oMFIMCIL2_5_15.enabled = i_bVal
      .Page5.oPag.oMFSINAI3_5_16.enabled = i_bVal
      .Page5.oPag.oMF_NPOS3_5_17.enabled = i_bVal
      .Page5.oPag.oMF_PACC3_5_18.enabled = i_bVal
      .Page5.oPag.oMF_NRIF3_5_19.enabled = i_bVal
      .Page5.oPag.oMFCAUSA3_5_20.enabled = i_bVal
      .Page5.oPag.oMFIMDIL3_5_21.enabled = i_bVal
      .Page5.oPag.oMFIMCIL3_5_22.enabled = i_bVal
      .Page6.oPag.oMFCDENTE_6_3.enabled = i_bVal
      .Page6.oPag.oVRSDENT1_6_4.enabled = i_bVal
      .Page6.oPag.oVPSDENT1_6_5.enabled = i_bVal
      .Page6.oPag.oMFCCOAE1_6_7.enabled = i_bVal
      .Page6.oPag.oMFCDPOS1_6_9.enabled = i_bVal
      .Page6.oPag.oMFMSINE1_6_10.enabled = i_bVal
      .Page6.oPag.oMFANINE1_6_11.enabled = i_bVal
      .Page6.oPag.oMFMSFIE1_6_12.enabled = i_bVal
      .Page6.oPag.oMFANF1_6_13.enabled = i_bVal
      .Page6.oPag.oMFIMDAE1_6_14.enabled = i_bVal
      .Page6.oPag.oMFIMCAE1_6_15.enabled = i_bVal
      .Page6.oPag.oVPSDENT2_6_16.enabled = i_bVal
      .Page6.oPag.oVRSDENT2_6_17.enabled = i_bVal
      .Page6.oPag.oMFCCOAE2_6_19.enabled = i_bVal
      .Page6.oPag.oMFCDPOS2_6_21.enabled = i_bVal
      .Page6.oPag.oMFMSINE2_6_22.enabled = i_bVal
      .Page6.oPag.oMFANINE2_6_23.enabled = i_bVal
      .Page6.oPag.oMFMSFIE2_6_24.enabled = i_bVal
      .Page6.oPag.oMFANF2_6_25.enabled = i_bVal
      .Page6.oPag.oMFIMDAE2_6_26.enabled = i_bVal
      .Page6.oPag.oMFIMCAE2_6_27.enabled = i_bVal
      .Page7.oPag.oMFDESSTA_7_11.enabled = i_bVal
      .Page6.oPag.oBtn_6_47.enabled = i_bVal
      .Page2.oPag.oBtn_2_16.enabled = i_bVal
      .Page3.oPag.oBtn_3_67.enabled = i_bVal
      .Page7.oPag.oBtn_7_27.enabled = i_bVal
      .Page7.oPag.oBtn_7_28.enabled = .Page7.oPag.oBtn_7_28.mCond()
      .Page1.oPag.oObj_1_5.enabled = i_bVal
      .Page2.oPag.oObj_2_13.enabled = i_bVal
      .Page2.oPag.oObj_2_15.enabled = i_bVal
      .Page6.oPag.oObj_6_51.enabled = i_bVal
      .Page7.oPag.oObj_7_10.enabled = i_bVal
      .Page7.oPag.oObj_7_29.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMFSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMFSERIAL_1_1.enabled = .t.
        .Page1.oPag.oMFMESRIF_1_2.enabled = .t.
        .Page1.oPag.oMFANNRIF_1_3.enabled = .t.
      endif
    endwith
    this.GSCG_ACF.SetStatus(i_cOp)
    this.GSCG_AFQ.SetStatus(i_cOp)
    this.GSCG_AVF.SetStatus(i_cOp)
    this.GSCG_AIF.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'MOD_PAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_ACF.SetChildrenStatus(i_cOp)
  *  this.GSCG_AFQ.SetChildrenStatus(i_cOp)
  *  this.GSCG_AVF.SetChildrenStatus(i_cOp)
  *  this.GSCG_AIF.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMESRIF,"MFMESRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRIF,"MFANNRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFVALUTA,"MFVALUTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_COINC,"MF_COINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODUFF,"MFCODUFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODATT,"MFCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED1,"MFCDSED1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT1,"MFCCONT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS1,"MFMINPS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES1,"MFDAMES1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN1,"MFDAANN1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES1,"MF_AMES1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN1,"MFAN1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD1,"MFIMPSD1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC1,"MFIMPSC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED2,"MFCDSED2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT2,"MFCCONT2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS2,"MFMINPS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES2,"MFDAMES2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN2,"MFDAANN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES2,"MF_AMES2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN2,"MFAN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD2,"MFIMPSD2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC2,"MFIMPSC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED3,"MFCDSED3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT3,"MFCCONT3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS3,"MFMINPS3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES3,"MFDAMES3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN3,"MFDAANN3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES3,"MF_AMES3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN3,"MFAN3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD3,"MFIMPSD3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC3,"MFIMPSC3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED4,"MFCDSED4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT4,"MFCCONT4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS4,"MFMINPS4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES4,"MFDAMES4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN4,"MFDAANN4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES4,"MF_AMES4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN4,"MFAN4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD4,"MFIMPSD4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC4,"MFIMPSC4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTDPS,"MFTOTDPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTCPS,"MFTOTCPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALDPS,"MFSALDPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE1,"MFCODRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE1,"MFTRIRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE1,"MFRATRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE1,"MFANNRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE1,"MFIMDRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE1,"MFIMCRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE2,"MFCODRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE2,"MFTRIRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE2,"MFRATRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE2,"MFANNRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE2,"MFIMDRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE2,"MFIMCRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE3,"MFCODRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE3,"MFTRIRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE3,"MFRATRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE3,"MFANNRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE3,"MFIMDRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE3,"MFIMCRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE4,"MFCODRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE4,"MFTRIRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE4,"MFRATRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE4,"MFANNRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE4,"MFIMDRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE4,"MFIMCRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTDRE,"MFTOTDRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTCRE,"MFTOTCRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALDRE,"MFSALDRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSINAI1,"MFSINAI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NPOS1,"MF_NPOS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PACC1,"MF_PACC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NRIF1,"MF_NRIF1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUSA1,"MFCAUSA1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDIL1,"MFIMDIL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCIL1,"MFIMCIL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSINAI2,"MFSINAI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NPOS2,"MF_NPOS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PACC2,"MF_PACC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NRIF2,"MF_NRIF2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUSA2,"MFCAUSA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDIL2,"MFIMDIL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCIL2,"MFIMCIL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSINAI3,"MFSINAI3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NPOS3,"MF_NPOS3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PACC3,"MF_PACC3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NRIF3,"MF_NRIF3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUSA3,"MFCAUSA3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDIL3,"MFIMDIL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCIL3,"MFIMCIL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTDINAI,"MFTDINAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTCINAI,"MFTCINAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALINA,"MFSALINA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDENTE,"MFCDENTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSDENT1,"MFSDENT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCOAE1,"MFCCOAE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDPOS1,"MFCDPOS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSINE1,"MFMSINE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANINE1,"MFANINE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSFIE1,"MFMSFIE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANF1,"MFANF1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDAE1,"MFIMDAE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCAE1,"MFIMCAE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSDENT2,"MFSDENT2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCOAE2,"MFCCOAE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDPOS2,"MFCDPOS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSINE2,"MFMSINE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANINE2,"MFANINE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSFIE2,"MFMSFIE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANF2,"MFANF2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDAE2,"MFIMDAE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCAE2,"MFIMCAE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTDAENT,"MFTDAENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTCAENT,"MFTCAENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALAEN,"MFSALAEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALFIN,"MFSALFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDESSTA,"MFDESSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTIPMOD,"MFTIPMOD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    i_lTable = "MOD_PAG"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOD_PAG_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        do GSCG_BMF with this
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOD_PAG_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"MODEL","i_CODAZI,w_MFSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOD_PAG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_PAG')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_PAG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MFSERIAL,MFMESRIF,MFANNRIF,MFVALUTA,MF_COINC"+;
                  ",MFCODUFF,MFCODATT,MFCDSED1,MFCCONT1,MFMINPS1"+;
                  ",MFDAMES1,MFDAANN1,MF_AMES1,MFAN1,MFIMPSD1"+;
                  ",MFIMPSC1,MFCDSED2,MFCCONT2,MFMINPS2,MFDAMES2"+;
                  ",MFDAANN2,MF_AMES2,MFAN2,MFIMPSD2,MFIMPSC2"+;
                  ",MFCDSED3,MFCCONT3,MFMINPS3,MFDAMES3,MFDAANN3"+;
                  ",MF_AMES3,MFAN3,MFIMPSD3,MFIMPSC3,MFCDSED4"+;
                  ",MFCCONT4,MFMINPS4,MFDAMES4,MFDAANN4,MF_AMES4"+;
                  ",MFAN4,MFIMPSD4,MFIMPSC4,MFTOTDPS,MFTOTCPS"+;
                  ",MFSALDPS,MFCODRE1,MFTRIRE1,MFRATRE1,MFANNRE1"+;
                  ",MFIMDRE1,MFIMCRE1,MFCODRE2,MFTRIRE2,MFRATRE2"+;
                  ",MFANNRE2,MFIMDRE2,MFIMCRE2,MFCODRE3,MFTRIRE3"+;
                  ",MFRATRE3,MFANNRE3,MFIMDRE3,MFIMCRE3,MFCODRE4"+;
                  ",MFTRIRE4,MFRATRE4,MFANNRE4,MFIMDRE4,MFIMCRE4"+;
                  ",MFTOTDRE,MFTOTCRE,MFSALDRE,MFSINAI1,MF_NPOS1"+;
                  ",MF_PACC1,MF_NRIF1,MFCAUSA1,MFIMDIL1,MFIMCIL1"+;
                  ",MFSINAI2,MF_NPOS2,MF_PACC2,MF_NRIF2,MFCAUSA2"+;
                  ",MFIMDIL2,MFIMCIL2,MFSINAI3,MF_NPOS3,MF_PACC3"+;
                  ",MF_NRIF3,MFCAUSA3,MFIMDIL3,MFIMCIL3,MFTDINAI"+;
                  ",MFTCINAI,MFSALINA,MFCDENTE,MFSDENT1,MFCCOAE1"+;
                  ",MFCDPOS1,MFMSINE1,MFANINE1,MFMSFIE1,MFANF1"+;
                  ",MFIMDAE1,MFIMCAE1,MFSDENT2,MFCCOAE2,MFCDPOS2"+;
                  ",MFMSINE2,MFANINE2,MFMSFIE2,MFANF2,MFIMDAE2"+;
                  ",MFIMCAE2,MFTDAENT,MFTCAENT,MFSALAEN,MFSALFIN"+;
                  ",MFDESSTA,MFTIPMOD "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MFSERIAL)+;
                  ","+cp_ToStrODBC(this.w_MFMESRIF)+;
                  ","+cp_ToStrODBC(this.w_MFANNRIF)+;
                  ","+cp_ToStrODBC(this.w_MFVALUTA)+;
                  ","+cp_ToStrODBC(this.w_MF_COINC)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODUFF)+;
                  ","+cp_ToStrODBC(this.w_MFCODATT)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT1)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS1)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES1)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN1)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES1)+;
                  ","+cp_ToStrODBC(this.w_MFAN1)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD1)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT2)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS2)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES2)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN2)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES2)+;
                  ","+cp_ToStrODBC(this.w_MFAN2)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD2)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT3)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS3)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES3)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN3)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES3)+;
                  ","+cp_ToStrODBC(this.w_MFAN3)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD3)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED4)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT4)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS4)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES4)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN4)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES4)+;
                  ","+cp_ToStrODBC(this.w_MFAN4)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD4)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC4)+;
                  ","+cp_ToStrODBC(this.w_MFTOTDPS)+;
                  ","+cp_ToStrODBC(this.w_MFTOTCPS)+;
                  ","+cp_ToStrODBC(this.w_MFSALDPS)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE1)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE1)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE1)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE1)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE1)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE2)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE2)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE2)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE2)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE2)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE3)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE3)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE3)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE3)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE3)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE4)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE4)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE4)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE4)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE4)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE4)+;
                  ","+cp_ToStrODBC(this.w_MFTOTDRE)+;
                  ","+cp_ToStrODBC(this.w_MFTOTCRE)+;
                  ","+cp_ToStrODBC(this.w_MFSALDRE)+;
                  ","+cp_ToStrODBCNull(this.w_MFSINAI1)+;
                  ","+cp_ToStrODBC(this.w_MF_NPOS1)+;
                  ","+cp_ToStrODBC(this.w_MF_PACC1)+;
                  ","+cp_ToStrODBC(this.w_MF_NRIF1)+;
                  ","+cp_ToStrODBC(this.w_MFCAUSA1)+;
                  ","+cp_ToStrODBC(this.w_MFIMDIL1)+;
                  ","+cp_ToStrODBC(this.w_MFIMCIL1)+;
                  ","+cp_ToStrODBCNull(this.w_MFSINAI2)+;
                  ","+cp_ToStrODBC(this.w_MF_NPOS2)+;
                  ","+cp_ToStrODBC(this.w_MF_PACC2)+;
                  ","+cp_ToStrODBC(this.w_MF_NRIF2)+;
                  ","+cp_ToStrODBC(this.w_MFCAUSA2)+;
                  ","+cp_ToStrODBC(this.w_MFIMDIL2)+;
                  ","+cp_ToStrODBC(this.w_MFIMCIL2)+;
                  ","+cp_ToStrODBCNull(this.w_MFSINAI3)+;
                  ","+cp_ToStrODBC(this.w_MF_NPOS3)+;
                  ","+cp_ToStrODBC(this.w_MF_PACC3)+;
                  ","+cp_ToStrODBC(this.w_MF_NRIF3)+;
                  ","+cp_ToStrODBC(this.w_MFCAUSA3)+;
                  ","+cp_ToStrODBC(this.w_MFIMDIL3)+;
                  ","+cp_ToStrODBC(this.w_MFIMCIL3)+;
                  ","+cp_ToStrODBC(this.w_MFTDINAI)+;
                  ","+cp_ToStrODBC(this.w_MFTCINAI)+;
                  ","+cp_ToStrODBC(this.w_MFSALINA)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDENTE)+;
                  ","+cp_ToStrODBC(this.w_MFSDENT1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCOAE1)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBC(this.w_MFCDPOS1)+;
                  ","+cp_ToStrODBC(this.w_MFMSINE1)+;
                  ","+cp_ToStrODBC(this.w_MFANINE1)+;
                  ","+cp_ToStrODBC(this.w_MFMSFIE1)+;
                  ","+cp_ToStrODBC(this.w_MFANF1)+;
                  ","+cp_ToStrODBC(this.w_MFIMDAE1)+;
                  ","+cp_ToStrODBC(this.w_MFIMCAE1)+;
                  ","+cp_ToStrODBC(this.w_MFSDENT2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCOAE2)+;
                  ","+cp_ToStrODBC(this.w_MFCDPOS2)+;
                  ","+cp_ToStrODBC(this.w_MFMSINE2)+;
                  ","+cp_ToStrODBC(this.w_MFANINE2)+;
                  ","+cp_ToStrODBC(this.w_MFMSFIE2)+;
                  ","+cp_ToStrODBC(this.w_MFANF2)+;
                  ","+cp_ToStrODBC(this.w_MFIMDAE2)+;
                  ","+cp_ToStrODBC(this.w_MFIMCAE2)+;
                  ","+cp_ToStrODBC(this.w_MFTDAENT)+;
                  ","+cp_ToStrODBC(this.w_MFTCAENT)+;
                  ","+cp_ToStrODBC(this.w_MFSALAEN)+;
                  ","+cp_ToStrODBC(this.w_MFSALFIN)+;
                  ","+cp_ToStrODBCNull(this.w_MFDESSTA)+;
                  ","+cp_ToStrODBC(this.w_MFTIPMOD)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_PAG')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_PAG')
        cp_CheckDeletedKey(i_cTable,0,'MFSERIAL',this.w_MFSERIAL)
        INSERT INTO (i_cTable);
              (MFSERIAL,MFMESRIF,MFANNRIF,MFVALUTA,MF_COINC,MFCODUFF,MFCODATT,MFCDSED1,MFCCONT1,MFMINPS1,MFDAMES1,MFDAANN1,MF_AMES1,MFAN1,MFIMPSD1,MFIMPSC1,MFCDSED2,MFCCONT2,MFMINPS2,MFDAMES2,MFDAANN2,MF_AMES2,MFAN2,MFIMPSD2,MFIMPSC2,MFCDSED3,MFCCONT3,MFMINPS3,MFDAMES3,MFDAANN3,MF_AMES3,MFAN3,MFIMPSD3,MFIMPSC3,MFCDSED4,MFCCONT4,MFMINPS4,MFDAMES4,MFDAANN4,MF_AMES4,MFAN4,MFIMPSD4,MFIMPSC4,MFTOTDPS,MFTOTCPS,MFSALDPS,MFCODRE1,MFTRIRE1,MFRATRE1,MFANNRE1,MFIMDRE1,MFIMCRE1,MFCODRE2,MFTRIRE2,MFRATRE2,MFANNRE2,MFIMDRE2,MFIMCRE2,MFCODRE3,MFTRIRE3,MFRATRE3,MFANNRE3,MFIMDRE3,MFIMCRE3,MFCODRE4,MFTRIRE4,MFRATRE4,MFANNRE4,MFIMDRE4,MFIMCRE4,MFTOTDRE,MFTOTCRE,MFSALDRE,MFSINAI1,MF_NPOS1,MF_PACC1,MF_NRIF1,MFCAUSA1,MFIMDIL1,MFIMCIL1,MFSINAI2,MF_NPOS2,MF_PACC2,MF_NRIF2,MFCAUSA2,MFIMDIL2,MFIMCIL2,MFSINAI3,MF_NPOS3,MF_PACC3,MF_NRIF3,MFCAUSA3,MFIMDIL3,MFIMCIL3,MFTDINAI,MFTCINAI,MFSALINA,MFCDENTE,MFSDENT1,MFCCOAE1,MFCDPOS1,MFMSINE1,MFANINE1,MFMSFIE1,MFANF1,MFIMDAE1,MFIMCAE1,MFSDENT2,MFCCOAE2,MFCDPOS2,MFMSINE2,MFANINE2,MFMSFIE2,MFANF2,MFIMDAE2,MFIMCAE2,MFTDAENT,MFTCAENT,MFSALAEN,MFSALFIN,MFDESSTA,MFTIPMOD  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MFSERIAL;
                  ,this.w_MFMESRIF;
                  ,this.w_MFANNRIF;
                  ,this.w_MFVALUTA;
                  ,this.w_MF_COINC;
                  ,this.w_MFCODUFF;
                  ,this.w_MFCODATT;
                  ,this.w_MFCDSED1;
                  ,this.w_MFCCONT1;
                  ,this.w_MFMINPS1;
                  ,this.w_MFDAMES1;
                  ,this.w_MFDAANN1;
                  ,this.w_MF_AMES1;
                  ,this.w_MFAN1;
                  ,this.w_MFIMPSD1;
                  ,this.w_MFIMPSC1;
                  ,this.w_MFCDSED2;
                  ,this.w_MFCCONT2;
                  ,this.w_MFMINPS2;
                  ,this.w_MFDAMES2;
                  ,this.w_MFDAANN2;
                  ,this.w_MF_AMES2;
                  ,this.w_MFAN2;
                  ,this.w_MFIMPSD2;
                  ,this.w_MFIMPSC2;
                  ,this.w_MFCDSED3;
                  ,this.w_MFCCONT3;
                  ,this.w_MFMINPS3;
                  ,this.w_MFDAMES3;
                  ,this.w_MFDAANN3;
                  ,this.w_MF_AMES3;
                  ,this.w_MFAN3;
                  ,this.w_MFIMPSD3;
                  ,this.w_MFIMPSC3;
                  ,this.w_MFCDSED4;
                  ,this.w_MFCCONT4;
                  ,this.w_MFMINPS4;
                  ,this.w_MFDAMES4;
                  ,this.w_MFDAANN4;
                  ,this.w_MF_AMES4;
                  ,this.w_MFAN4;
                  ,this.w_MFIMPSD4;
                  ,this.w_MFIMPSC4;
                  ,this.w_MFTOTDPS;
                  ,this.w_MFTOTCPS;
                  ,this.w_MFSALDPS;
                  ,this.w_MFCODRE1;
                  ,this.w_MFTRIRE1;
                  ,this.w_MFRATRE1;
                  ,this.w_MFANNRE1;
                  ,this.w_MFIMDRE1;
                  ,this.w_MFIMCRE1;
                  ,this.w_MFCODRE2;
                  ,this.w_MFTRIRE2;
                  ,this.w_MFRATRE2;
                  ,this.w_MFANNRE2;
                  ,this.w_MFIMDRE2;
                  ,this.w_MFIMCRE2;
                  ,this.w_MFCODRE3;
                  ,this.w_MFTRIRE3;
                  ,this.w_MFRATRE3;
                  ,this.w_MFANNRE3;
                  ,this.w_MFIMDRE3;
                  ,this.w_MFIMCRE3;
                  ,this.w_MFCODRE4;
                  ,this.w_MFTRIRE4;
                  ,this.w_MFRATRE4;
                  ,this.w_MFANNRE4;
                  ,this.w_MFIMDRE4;
                  ,this.w_MFIMCRE4;
                  ,this.w_MFTOTDRE;
                  ,this.w_MFTOTCRE;
                  ,this.w_MFSALDRE;
                  ,this.w_MFSINAI1;
                  ,this.w_MF_NPOS1;
                  ,this.w_MF_PACC1;
                  ,this.w_MF_NRIF1;
                  ,this.w_MFCAUSA1;
                  ,this.w_MFIMDIL1;
                  ,this.w_MFIMCIL1;
                  ,this.w_MFSINAI2;
                  ,this.w_MF_NPOS2;
                  ,this.w_MF_PACC2;
                  ,this.w_MF_NRIF2;
                  ,this.w_MFCAUSA2;
                  ,this.w_MFIMDIL2;
                  ,this.w_MFIMCIL2;
                  ,this.w_MFSINAI3;
                  ,this.w_MF_NPOS3;
                  ,this.w_MF_PACC3;
                  ,this.w_MF_NRIF3;
                  ,this.w_MFCAUSA3;
                  ,this.w_MFIMDIL3;
                  ,this.w_MFIMCIL3;
                  ,this.w_MFTDINAI;
                  ,this.w_MFTCINAI;
                  ,this.w_MFSALINA;
                  ,this.w_MFCDENTE;
                  ,this.w_MFSDENT1;
                  ,this.w_MFCCOAE1;
                  ,this.w_MFCDPOS1;
                  ,this.w_MFMSINE1;
                  ,this.w_MFANINE1;
                  ,this.w_MFMSFIE1;
                  ,this.w_MFANF1;
                  ,this.w_MFIMDAE1;
                  ,this.w_MFIMCAE1;
                  ,this.w_MFSDENT2;
                  ,this.w_MFCCOAE2;
                  ,this.w_MFCDPOS2;
                  ,this.w_MFMSINE2;
                  ,this.w_MFANINE2;
                  ,this.w_MFMSFIE2;
                  ,this.w_MFANF2;
                  ,this.w_MFIMDAE2;
                  ,this.w_MFIMCAE2;
                  ,this.w_MFTDAENT;
                  ,this.w_MFTCAENT;
                  ,this.w_MFSALAEN;
                  ,this.w_MFSALFIN;
                  ,this.w_MFDESSTA;
                  ,this.w_MFTIPMOD;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- gscg_aoo
    * Forza aggiornamento del database
    this.bupdated=.t.
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOD_PAG_IDX,i_nConn)
      *
      * update MOD_PAG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_PAG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MFMESRIF="+cp_ToStrODBC(this.w_MFMESRIF)+;
             ",MFANNRIF="+cp_ToStrODBC(this.w_MFANNRIF)+;
             ",MFVALUTA="+cp_ToStrODBC(this.w_MFVALUTA)+;
             ",MF_COINC="+cp_ToStrODBC(this.w_MF_COINC)+;
             ",MFCODUFF="+cp_ToStrODBCNull(this.w_MFCODUFF)+;
             ",MFCODATT="+cp_ToStrODBC(this.w_MFCODATT)+;
             ",MFCDSED1="+cp_ToStrODBCNull(this.w_MFCDSED1)+;
             ",MFCCONT1="+cp_ToStrODBCNull(this.w_MFCCONT1)+;
             ",MFMINPS1="+cp_ToStrODBC(this.w_MFMINPS1)+;
             ",MFDAMES1="+cp_ToStrODBC(this.w_MFDAMES1)+;
             ",MFDAANN1="+cp_ToStrODBC(this.w_MFDAANN1)+;
             ",MF_AMES1="+cp_ToStrODBC(this.w_MF_AMES1)+;
             ",MFAN1="+cp_ToStrODBC(this.w_MFAN1)+;
             ",MFIMPSD1="+cp_ToStrODBC(this.w_MFIMPSD1)+;
             ",MFIMPSC1="+cp_ToStrODBC(this.w_MFIMPSC1)+;
             ",MFCDSED2="+cp_ToStrODBCNull(this.w_MFCDSED2)+;
             ",MFCCONT2="+cp_ToStrODBCNull(this.w_MFCCONT2)+;
             ",MFMINPS2="+cp_ToStrODBC(this.w_MFMINPS2)+;
             ",MFDAMES2="+cp_ToStrODBC(this.w_MFDAMES2)+;
             ",MFDAANN2="+cp_ToStrODBC(this.w_MFDAANN2)+;
             ",MF_AMES2="+cp_ToStrODBC(this.w_MF_AMES2)+;
             ",MFAN2="+cp_ToStrODBC(this.w_MFAN2)+;
             ",MFIMPSD2="+cp_ToStrODBC(this.w_MFIMPSD2)+;
             ",MFIMPSC2="+cp_ToStrODBC(this.w_MFIMPSC2)+;
             ",MFCDSED3="+cp_ToStrODBCNull(this.w_MFCDSED3)+;
             ",MFCCONT3="+cp_ToStrODBCNull(this.w_MFCCONT3)+;
             ",MFMINPS3="+cp_ToStrODBC(this.w_MFMINPS3)+;
             ",MFDAMES3="+cp_ToStrODBC(this.w_MFDAMES3)+;
             ",MFDAANN3="+cp_ToStrODBC(this.w_MFDAANN3)+;
             ",MF_AMES3="+cp_ToStrODBC(this.w_MF_AMES3)+;
             ",MFAN3="+cp_ToStrODBC(this.w_MFAN3)+;
             ",MFIMPSD3="+cp_ToStrODBC(this.w_MFIMPSD3)+;
             ",MFIMPSC3="+cp_ToStrODBC(this.w_MFIMPSC3)+;
             ",MFCDSED4="+cp_ToStrODBCNull(this.w_MFCDSED4)+;
             ",MFCCONT4="+cp_ToStrODBCNull(this.w_MFCCONT4)+;
             ",MFMINPS4="+cp_ToStrODBC(this.w_MFMINPS4)+;
             ",MFDAMES4="+cp_ToStrODBC(this.w_MFDAMES4)+;
             ",MFDAANN4="+cp_ToStrODBC(this.w_MFDAANN4)+;
             ",MF_AMES4="+cp_ToStrODBC(this.w_MF_AMES4)+;
             ",MFAN4="+cp_ToStrODBC(this.w_MFAN4)+;
             ",MFIMPSD4="+cp_ToStrODBC(this.w_MFIMPSD4)+;
             ",MFIMPSC4="+cp_ToStrODBC(this.w_MFIMPSC4)+;
             ",MFTOTDPS="+cp_ToStrODBC(this.w_MFTOTDPS)+;
             ",MFTOTCPS="+cp_ToStrODBC(this.w_MFTOTCPS)+;
             ",MFSALDPS="+cp_ToStrODBC(this.w_MFSALDPS)+;
             ",MFCODRE1="+cp_ToStrODBCNull(this.w_MFCODRE1)+;
             ",MFTRIRE1="+cp_ToStrODBCNull(this.w_MFTRIRE1)+;
             ",MFRATRE1="+cp_ToStrODBC(this.w_MFRATRE1)+;
             ",MFANNRE1="+cp_ToStrODBC(this.w_MFANNRE1)+;
             ",MFIMDRE1="+cp_ToStrODBC(this.w_MFIMDRE1)+;
             ",MFIMCRE1="+cp_ToStrODBC(this.w_MFIMCRE1)+;
             ",MFCODRE2="+cp_ToStrODBCNull(this.w_MFCODRE2)+;
             ",MFTRIRE2="+cp_ToStrODBCNull(this.w_MFTRIRE2)+;
             ",MFRATRE2="+cp_ToStrODBC(this.w_MFRATRE2)+;
             ",MFANNRE2="+cp_ToStrODBC(this.w_MFANNRE2)+;
             ",MFIMDRE2="+cp_ToStrODBC(this.w_MFIMDRE2)+;
             ",MFIMCRE2="+cp_ToStrODBC(this.w_MFIMCRE2)+;
             ",MFCODRE3="+cp_ToStrODBCNull(this.w_MFCODRE3)+;
             ",MFTRIRE3="+cp_ToStrODBCNull(this.w_MFTRIRE3)+;
             ",MFRATRE3="+cp_ToStrODBC(this.w_MFRATRE3)+;
             ",MFANNRE3="+cp_ToStrODBC(this.w_MFANNRE3)+;
             ",MFIMDRE3="+cp_ToStrODBC(this.w_MFIMDRE3)+;
             ",MFIMCRE3="+cp_ToStrODBC(this.w_MFIMCRE3)+;
             ",MFCODRE4="+cp_ToStrODBCNull(this.w_MFCODRE4)+;
             ",MFTRIRE4="+cp_ToStrODBCNull(this.w_MFTRIRE4)+;
             ",MFRATRE4="+cp_ToStrODBC(this.w_MFRATRE4)+;
             ",MFANNRE4="+cp_ToStrODBC(this.w_MFANNRE4)+;
             ",MFIMDRE4="+cp_ToStrODBC(this.w_MFIMDRE4)+;
             ",MFIMCRE4="+cp_ToStrODBC(this.w_MFIMCRE4)+;
             ",MFTOTDRE="+cp_ToStrODBC(this.w_MFTOTDRE)+;
             ",MFTOTCRE="+cp_ToStrODBC(this.w_MFTOTCRE)+;
             ",MFSALDRE="+cp_ToStrODBC(this.w_MFSALDRE)+;
             ",MFSINAI1="+cp_ToStrODBCNull(this.w_MFSINAI1)+;
             ",MF_NPOS1="+cp_ToStrODBC(this.w_MF_NPOS1)+;
             ",MF_PACC1="+cp_ToStrODBC(this.w_MF_PACC1)+;
             ",MF_NRIF1="+cp_ToStrODBC(this.w_MF_NRIF1)+;
             ",MFCAUSA1="+cp_ToStrODBC(this.w_MFCAUSA1)+;
             ",MFIMDIL1="+cp_ToStrODBC(this.w_MFIMDIL1)+;
             ",MFIMCIL1="+cp_ToStrODBC(this.w_MFIMCIL1)+;
             ",MFSINAI2="+cp_ToStrODBCNull(this.w_MFSINAI2)+;
             ",MF_NPOS2="+cp_ToStrODBC(this.w_MF_NPOS2)+;
             ",MF_PACC2="+cp_ToStrODBC(this.w_MF_PACC2)+;
             ",MF_NRIF2="+cp_ToStrODBC(this.w_MF_NRIF2)+;
             ",MFCAUSA2="+cp_ToStrODBC(this.w_MFCAUSA2)+;
             ",MFIMDIL2="+cp_ToStrODBC(this.w_MFIMDIL2)+;
             ",MFIMCIL2="+cp_ToStrODBC(this.w_MFIMCIL2)+;
             ",MFSINAI3="+cp_ToStrODBCNull(this.w_MFSINAI3)+;
             ",MF_NPOS3="+cp_ToStrODBC(this.w_MF_NPOS3)+;
             ",MF_PACC3="+cp_ToStrODBC(this.w_MF_PACC3)+;
             ",MF_NRIF3="+cp_ToStrODBC(this.w_MF_NRIF3)+;
             ",MFCAUSA3="+cp_ToStrODBC(this.w_MFCAUSA3)+;
             ",MFIMDIL3="+cp_ToStrODBC(this.w_MFIMDIL3)+;
             ",MFIMCIL3="+cp_ToStrODBC(this.w_MFIMCIL3)+;
             ",MFTDINAI="+cp_ToStrODBC(this.w_MFTDINAI)+;
             ",MFTCINAI="+cp_ToStrODBC(this.w_MFTCINAI)+;
             ",MFSALINA="+cp_ToStrODBC(this.w_MFSALINA)+;
             ",MFCDENTE="+cp_ToStrODBCNull(this.w_MFCDENTE)+;
             ",MFSDENT1="+cp_ToStrODBC(this.w_MFSDENT1)+;
             ",MFCCOAE1="+cp_ToStrODBCNull(this.w_MFCCOAE1)+;
             ",MFCDPOS1="+cp_ToStrODBC(this.w_MFCDPOS1)+;
             ""
             i_nnn=i_nnn+;
             ",MFMSINE1="+cp_ToStrODBC(this.w_MFMSINE1)+;
             ",MFANINE1="+cp_ToStrODBC(this.w_MFANINE1)+;
             ",MFMSFIE1="+cp_ToStrODBC(this.w_MFMSFIE1)+;
             ",MFANF1="+cp_ToStrODBC(this.w_MFANF1)+;
             ",MFIMDAE1="+cp_ToStrODBC(this.w_MFIMDAE1)+;
             ",MFIMCAE1="+cp_ToStrODBC(this.w_MFIMCAE1)+;
             ",MFSDENT2="+cp_ToStrODBC(this.w_MFSDENT2)+;
             ",MFCCOAE2="+cp_ToStrODBCNull(this.w_MFCCOAE2)+;
             ",MFCDPOS2="+cp_ToStrODBC(this.w_MFCDPOS2)+;
             ",MFMSINE2="+cp_ToStrODBC(this.w_MFMSINE2)+;
             ",MFANINE2="+cp_ToStrODBC(this.w_MFANINE2)+;
             ",MFMSFIE2="+cp_ToStrODBC(this.w_MFMSFIE2)+;
             ",MFANF2="+cp_ToStrODBC(this.w_MFANF2)+;
             ",MFIMDAE2="+cp_ToStrODBC(this.w_MFIMDAE2)+;
             ",MFIMCAE2="+cp_ToStrODBC(this.w_MFIMCAE2)+;
             ",MFTDAENT="+cp_ToStrODBC(this.w_MFTDAENT)+;
             ",MFTCAENT="+cp_ToStrODBC(this.w_MFTCAENT)+;
             ",MFSALAEN="+cp_ToStrODBC(this.w_MFSALAEN)+;
             ",MFSALFIN="+cp_ToStrODBC(this.w_MFSALFIN)+;
             ",MFDESSTA="+cp_ToStrODBCNull(this.w_MFDESSTA)+;
             ",MFTIPMOD="+cp_ToStrODBC(this.w_MFTIPMOD)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_PAG')
        i_cWhere = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
        UPDATE (i_cTable) SET;
              MFMESRIF=this.w_MFMESRIF;
             ,MFANNRIF=this.w_MFANNRIF;
             ,MFVALUTA=this.w_MFVALUTA;
             ,MF_COINC=this.w_MF_COINC;
             ,MFCODUFF=this.w_MFCODUFF;
             ,MFCODATT=this.w_MFCODATT;
             ,MFCDSED1=this.w_MFCDSED1;
             ,MFCCONT1=this.w_MFCCONT1;
             ,MFMINPS1=this.w_MFMINPS1;
             ,MFDAMES1=this.w_MFDAMES1;
             ,MFDAANN1=this.w_MFDAANN1;
             ,MF_AMES1=this.w_MF_AMES1;
             ,MFAN1=this.w_MFAN1;
             ,MFIMPSD1=this.w_MFIMPSD1;
             ,MFIMPSC1=this.w_MFIMPSC1;
             ,MFCDSED2=this.w_MFCDSED2;
             ,MFCCONT2=this.w_MFCCONT2;
             ,MFMINPS2=this.w_MFMINPS2;
             ,MFDAMES2=this.w_MFDAMES2;
             ,MFDAANN2=this.w_MFDAANN2;
             ,MF_AMES2=this.w_MF_AMES2;
             ,MFAN2=this.w_MFAN2;
             ,MFIMPSD2=this.w_MFIMPSD2;
             ,MFIMPSC2=this.w_MFIMPSC2;
             ,MFCDSED3=this.w_MFCDSED3;
             ,MFCCONT3=this.w_MFCCONT3;
             ,MFMINPS3=this.w_MFMINPS3;
             ,MFDAMES3=this.w_MFDAMES3;
             ,MFDAANN3=this.w_MFDAANN3;
             ,MF_AMES3=this.w_MF_AMES3;
             ,MFAN3=this.w_MFAN3;
             ,MFIMPSD3=this.w_MFIMPSD3;
             ,MFIMPSC3=this.w_MFIMPSC3;
             ,MFCDSED4=this.w_MFCDSED4;
             ,MFCCONT4=this.w_MFCCONT4;
             ,MFMINPS4=this.w_MFMINPS4;
             ,MFDAMES4=this.w_MFDAMES4;
             ,MFDAANN4=this.w_MFDAANN4;
             ,MF_AMES4=this.w_MF_AMES4;
             ,MFAN4=this.w_MFAN4;
             ,MFIMPSD4=this.w_MFIMPSD4;
             ,MFIMPSC4=this.w_MFIMPSC4;
             ,MFTOTDPS=this.w_MFTOTDPS;
             ,MFTOTCPS=this.w_MFTOTCPS;
             ,MFSALDPS=this.w_MFSALDPS;
             ,MFCODRE1=this.w_MFCODRE1;
             ,MFTRIRE1=this.w_MFTRIRE1;
             ,MFRATRE1=this.w_MFRATRE1;
             ,MFANNRE1=this.w_MFANNRE1;
             ,MFIMDRE1=this.w_MFIMDRE1;
             ,MFIMCRE1=this.w_MFIMCRE1;
             ,MFCODRE2=this.w_MFCODRE2;
             ,MFTRIRE2=this.w_MFTRIRE2;
             ,MFRATRE2=this.w_MFRATRE2;
             ,MFANNRE2=this.w_MFANNRE2;
             ,MFIMDRE2=this.w_MFIMDRE2;
             ,MFIMCRE2=this.w_MFIMCRE2;
             ,MFCODRE3=this.w_MFCODRE3;
             ,MFTRIRE3=this.w_MFTRIRE3;
             ,MFRATRE3=this.w_MFRATRE3;
             ,MFANNRE3=this.w_MFANNRE3;
             ,MFIMDRE3=this.w_MFIMDRE3;
             ,MFIMCRE3=this.w_MFIMCRE3;
             ,MFCODRE4=this.w_MFCODRE4;
             ,MFTRIRE4=this.w_MFTRIRE4;
             ,MFRATRE4=this.w_MFRATRE4;
             ,MFANNRE4=this.w_MFANNRE4;
             ,MFIMDRE4=this.w_MFIMDRE4;
             ,MFIMCRE4=this.w_MFIMCRE4;
             ,MFTOTDRE=this.w_MFTOTDRE;
             ,MFTOTCRE=this.w_MFTOTCRE;
             ,MFSALDRE=this.w_MFSALDRE;
             ,MFSINAI1=this.w_MFSINAI1;
             ,MF_NPOS1=this.w_MF_NPOS1;
             ,MF_PACC1=this.w_MF_PACC1;
             ,MF_NRIF1=this.w_MF_NRIF1;
             ,MFCAUSA1=this.w_MFCAUSA1;
             ,MFIMDIL1=this.w_MFIMDIL1;
             ,MFIMCIL1=this.w_MFIMCIL1;
             ,MFSINAI2=this.w_MFSINAI2;
             ,MF_NPOS2=this.w_MF_NPOS2;
             ,MF_PACC2=this.w_MF_PACC2;
             ,MF_NRIF2=this.w_MF_NRIF2;
             ,MFCAUSA2=this.w_MFCAUSA2;
             ,MFIMDIL2=this.w_MFIMDIL2;
             ,MFIMCIL2=this.w_MFIMCIL2;
             ,MFSINAI3=this.w_MFSINAI3;
             ,MF_NPOS3=this.w_MF_NPOS3;
             ,MF_PACC3=this.w_MF_PACC3;
             ,MF_NRIF3=this.w_MF_NRIF3;
             ,MFCAUSA3=this.w_MFCAUSA3;
             ,MFIMDIL3=this.w_MFIMDIL3;
             ,MFIMCIL3=this.w_MFIMCIL3;
             ,MFTDINAI=this.w_MFTDINAI;
             ,MFTCINAI=this.w_MFTCINAI;
             ,MFSALINA=this.w_MFSALINA;
             ,MFCDENTE=this.w_MFCDENTE;
             ,MFSDENT1=this.w_MFSDENT1;
             ,MFCCOAE1=this.w_MFCCOAE1;
             ,MFCDPOS1=this.w_MFCDPOS1;
             ,MFMSINE1=this.w_MFMSINE1;
             ,MFANINE1=this.w_MFANINE1;
             ,MFMSFIE1=this.w_MFMSFIE1;
             ,MFANF1=this.w_MFANF1;
             ,MFIMDAE1=this.w_MFIMDAE1;
             ,MFIMCAE1=this.w_MFIMCAE1;
             ,MFSDENT2=this.w_MFSDENT2;
             ,MFCCOAE2=this.w_MFCCOAE2;
             ,MFCDPOS2=this.w_MFCDPOS2;
             ,MFMSINE2=this.w_MFMSINE2;
             ,MFANINE2=this.w_MFANINE2;
             ,MFMSFIE2=this.w_MFMSFIE2;
             ,MFANF2=this.w_MFANF2;
             ,MFIMDAE2=this.w_MFIMDAE2;
             ,MFIMCAE2=this.w_MFIMCAE2;
             ,MFTDAENT=this.w_MFTDAENT;
             ,MFTCAENT=this.w_MFTCAENT;
             ,MFSALAEN=this.w_MFSALAEN;
             ,MFSALFIN=this.w_MFSALFIN;
             ,MFDESSTA=this.w_MFDESSTA;
             ,MFTIPMOD=this.w_MFTIPMOD;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCG_ACF : Saving
      this.GSCG_ACF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"CFSERIAL";
             )
      this.GSCG_ACF.mReplace()
      * --- GSCG_AFQ : Saving
      this.GSCG_AFQ.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"EFSERIAL";
             )
      this.GSCG_AFQ.mReplace()
      * --- GSCG_AVF : Saving
      this.GSCG_AVF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"VFSERIAL";
             )
      this.GSCG_AVF.mReplace()
      * --- GSCG_AIF : Saving
      this.GSCG_AIF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"IFSERIAL";
             )
      this.GSCG_AIF.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCG_ACF : Deleting
    this.GSCG_ACF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"CFSERIAL";
           )
    this.GSCG_ACF.mDelete()
    * --- GSCG_AFQ : Deleting
    this.GSCG_AFQ.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"EFSERIAL";
           )
    this.GSCG_AFQ.mDelete()
    * --- GSCG_AVF : Deleting
    this.GSCG_AVF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"VFSERIAL";
           )
    this.GSCG_AVF.mDelete()
    * --- GSCG_AIF : Deleting
    this.GSCG_AIF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"IFSERIAL";
           )
    this.GSCG_AIF.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOD_PAG_IDX,i_nConn)
      *
      * delete MOD_PAG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(1,5,.t.)
        if .o_APPCOIN<>.w_APPCOIN
            .w_MF_COINC = .w_APPCOIN
        endif
        .DoRTCalc(7,10,.t.)
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page2.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
        .DoRTCalc(13,16,.t.)
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFCCONT1 = iif(empty(.w_MFCDSED1),' ',.w_MFCCONT1)
          .link_3_3('Full')
        endif
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFMINPS1 = iif(empty(.w_MFCDSED1),' ',.w_MFMINPS1)
        endif
        if .o_MFCCONT1<>.w_MFCCONT1
            .w_APP1A = !empty(.w_MFCCONT1) and alltrim(.w_MFCCONT1)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        endif
        if .o_MFCCONT1<>.w_MFCCONT1.or. .o_APP1A<>.w_APP1A
            .w_APP1 = !empty(.w_MFCCONT1) and (alltrim(.w_MFCCONT1)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP1A)
        endif
        if .o_MFCDSED1<>.w_MFCDSED1.or. .o_APP1A<>.w_APP1A
            .w_MFDAMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1A,' ',.w_MFDAMES1))
        endif
        if .o_APP1A<>.w_APP1A.or. .o_MFCDSED1<>.w_MFCDSED1
            .w_MFDAANN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1A,' ',.w_MFDAANN1))
        endif
        if .o_MFCDSED1<>.w_MFCDSED1.or. .o_APP1<>.w_APP1
            .w_MF_AMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,' ',.w_MF_AMES1))
        endif
        if .o_MFCDSED1<>.w_MFCDSED1.or. .o_APP1<>.w_APP1
            .w_MFAN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,' ',.w_MFAN1))
        endif
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFIMPSD1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSD1)
        endif
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFIMPSC1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSC1)
        endif
        .DoRTCalc(27,27,.t.)
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFCCONT2 = iif(empty(.w_MFCDSED2),' ',.w_MFCCONT2)
          .link_3_14('Full')
        endif
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFMINPS2 = iif(empty(.w_MFCDSED2),' ',.w_MFMINPS2)
        endif
        if .o_MFCCONT2<>.w_MFCCONT2
            .w_APP2A = !empty(.w_MFCCONT2) and alltrim(.w_MFCCONT2)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        endif
        if .o_APP2A<>.w_APP2A.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MFDAMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2A,' ',.w_MFDAMES2))
        endif
        if .o_APP2A<>.w_APP2A.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MFDAANN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2A,' ',.w_MFDAANN2))
        endif
        if .o_MFCCONT2<>.w_MFCCONT2.or. .o_APP2A<>.w_APP2A
            .w_APP2 = !empty(.w_MFCCONT2) and (alltrim(.w_MFCCONT2)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBTU-EBIT-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP2A)
        endif
        if .o_APP2<>.w_APP2.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MF_AMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,' ',.w_MF_AMES2))
        endif
        if .o_APP2<>.w_APP2.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MFAN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,' ',.w_MFAN2))
        endif
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFIMPSD2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSD2)
        endif
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFIMPSC2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSC2)
        endif
        .DoRTCalc(38,38,.t.)
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFCCONT3 = iif(empty(.w_MFCDSED3),' ',.w_MFCCONT3)
          .link_3_25('Full')
        endif
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFMINPS3 = iif(empty(.w_MFCDSED3),' ',.w_MFMINPS3)
        endif
        if .o_MFCCONT3<>.w_MFCCONT3
            .w_APP3A = !empty(.w_MFCCONT3) and alltrim(.w_MFCCONT3)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        endif
        if .o_APP3A<>.w_APP3A.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MFDAMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3A,' ',.w_MFDAMES3))
        endif
        if .o_MFCCONT3<>.w_MFCCONT3.or. .o_APP3A<>.w_APP3A
            .w_APP3 = !empty(.w_MFCCONT3) and (alltrim(.w_MFCCONT3)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP3A)
        endif
        if .o_APP3A<>.w_APP3A.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MFDAANN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3A,' ',.w_MFDAANN3))
        endif
        if .o_APP3<>.w_APP3.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MF_AMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,' ',.w_MF_AMES3))
        endif
        if .o_APP3<>.w_APP3.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MFAN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,' ',.w_MFAN3))
        endif
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFIMPSD3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSD3)
        endif
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFIMPSC3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSC3)
        endif
        .DoRTCalc(49,49,.t.)
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFCCONT4 = iif(empty(.w_MFCDSED4),' ',.w_MFCCONT4)
          .link_3_36('Full')
        endif
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFMINPS4 = iif(empty(.w_MFCDSED4),' ',.w_MFMINPS4)
        endif
        if .o_MFCCONT4<>.w_MFCCONT4
            .w_APP4A = !empty(.w_MFCCONT4) and alltrim(.w_MFCCONT4)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        endif
        if .o_APP4A<>.w_APP4A.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MFDAMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4A,' ',.w_MFDAMES4))
        endif
        if .o_MFCCONT4<>.w_MFCCONT4.or. .o_APP4A<>.w_APP4A
            .w_APP4 = !empty(.w_MFCCONT4) and (alltrim(.w_MFCCONT4)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP4A)
        endif
        if .o_APP4A<>.w_APP4A.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MFDAANN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4A,' ',.w_MFDAANN4))
        endif
        if .o_APP4<>.w_APP4.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MF_AMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,' ',.w_MF_AMES4))
        endif
        if .o_APP4<>.w_APP4.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MFAN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,' ',.w_MFAN4))
        endif
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFIMPSD4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSD4)
        endif
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFIMPSC4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSC4)
        endif
            .w_MFTOTDPS = .w_MFIMPSD1+.w_MFIMPSD2+.w_MFIMPSD3+.w_MFIMPSD4
            .w_MFTOTCPS = .w_MFIMPSC1+.w_MFIMPSC2+.w_MFIMPSC3+.w_MFIMPSC4
            .w_MFSALDPS = .w_MFTOTDPS-.w_MFTOTCPS
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(65,68,.t.)
        if .o_MFCODRE1<>.w_MFCODRE1.or. .o_MFTRIRE1<>.w_MFTRIRE1
            .w_MFTRIRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFTRIRE1)
          .link_4_3('Full')
        endif
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFRATRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFRATRE1)
        endif
        if .o_MFCODRE1<>.w_MFCODRE1.or. .o_MFTRIRE1<>.w_MFTRIRE1
            .w_MFANNRE1 = iif(empty(.w_MFCODRE1),' ',iif(.w_MFTRIRE1= '3805','0000',.w_MFANNRE1))
        endif
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFIMDRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMDRE1)
        endif
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFIMCRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMCRE1)
        endif
        .DoRTCalc(74,74,.t.)
        if .o_MFCODRE2<>.w_MFCODRE2.or. .o_MFTRIRE2<>.w_MFTRIRE2
            .w_MFTRIRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFTRIRE2)
          .link_4_12('Full')
        endif
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFRATRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFRATRE2)
        endif
        if .o_MFCODRE2<>.w_MFCODRE2.or. .o_MFTRIRE2<>.w_MFTRIRE2
            .w_MFANNRE2 = iif(empty(.w_MFCODRE2),' ',iif(.w_MFTRIRE2 ='3805','0000',.w_MFANNRE2))
        endif
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFIMDRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMDRE2)
        endif
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFIMCRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMCRE2)
        endif
        .DoRTCalc(80,80,.t.)
        if .o_MFCODRE3<>.w_MFCODRE3.or. .o_MFTRIRE3<>.w_MFTRIRE3
            .w_MFTRIRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFTRIRE3)
          .link_4_18('Full')
        endif
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFRATRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFRATRE3)
        endif
        if .o_MFCODRE3<>.w_MFCODRE3.or. .o_MFTRIRE3<>.w_MFTRIRE3
            .w_MFANNRE3 = iif(empty(.w_MFCODRE3),' ',iif(.w_MFTRIRE3 = '3805','0000',.w_MFANNRE3))
        endif
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFIMDRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMDRE3)
        endif
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFIMCRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMCRE3)
        endif
        .DoRTCalc(86,86,.t.)
        if .o_MFCODRE4<>.w_MFCODRE4.or. .o_MFTRIRE4<>.w_MFTRIRE4
            .w_MFTRIRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFTRIRE4)
          .link_4_24('Full')
        endif
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFRATRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFRATRE4)
        endif
        if .o_MFCODRE4<>.w_MFCODRE4.or. .o_MFTRIRE4<>.w_MFTRIRE4
            .w_MFANNRE4 = iif(empty(.w_MFCODRE4),' ',iif( .w_MFTRIRE4 = '3805','0000',.w_MFANNRE4))
        endif
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFIMDRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMDRE4)
        endif
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFIMCRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMCRE4)
        endif
            .w_MFTOTDRE = .w_MFIMDRE1+.w_MFIMDRE2+.w_MFIMDRE3+.w_MFIMDRE4
            .w_MFTOTCRE = .w_MFIMCRE1+.w_MFIMCRE2+.w_MFIMCRE3+.w_MFIMCRE4
            .w_MFSALDRE = .w_MFTOTDRE-.w_MFTOTCRE
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(97,98,.t.)
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MF_NPOS1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NPOS1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MF_PACC1 = iif(empty(.w_MFSINAI1),' ',.w_MF_PACC1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MF_NRIF1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NRIF1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MFCAUSA1 = iif(empty(.w_MFSINAI1),' ',.w_MFCAUSA1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MFIMDIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMDIL1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MFIMCIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMCIL1)
        endif
        .DoRTCalc(105,105,.t.)
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MF_NPOS2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NPOS2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MF_PACC2 = iif(empty(.w_MFSINAI2),' ',.w_MF_PACC2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MF_NRIF2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NRIF2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MFCAUSA2 = iif(empty(.w_MFSINAI2),' ',.w_MFCAUSA2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MFIMDIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMDIL2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MFIMCIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMCIL2)
        endif
        .DoRTCalc(112,112,.t.)
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MF_NPOS3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NPOS3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MF_PACC3 = iif(empty(.w_MFSINAI3),' ',.w_MF_PACC3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MF_NRIF3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NRIF3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MFCAUSA3 = iif(empty(.w_MFSINAI3),' ',.w_MFCAUSA3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MFIMDIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMDIL3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MFIMCIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMCIL3)
        endif
            .w_MFTDINAI = .w_MFIMDIL1+.w_MFIMDIL2+.w_MFIMDIL3
            .w_MFTCINAI = .w_MFIMCIL1+.w_MFIMCIL2+.w_MFIMCIL3
            .w_MFSALINA = .w_MFTDINAI-.w_MFTCINAI
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(124,125,.t.)
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_VRSDENT1 = space(5)
          .link_6_4('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_VPSDENT1 = space(2)
          .link_6_5('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_VRSDENT1<>.w_VRSDENT1.or. .o_VPSDENT1<>.w_VPSDENT1
            .w_MFSDENT1 = iif(.w_MFCDENTE='0003',.w_VPSDENT1,.w_VRSDENT1)
        endif
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_MFCCOAE1 = iif(empty(.w_MFCDENTE) ,' ',.w_MFCCOAE1)
          .link_6_7('Full')
        endif
        if .o_MFSDENT1<>.w_MFSDENT1
            .w_OLSDENT1 = .w_MFSDENT1
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1
            .w_MFCDPOS1 = iif(empty(.w_MFCCOAE1),' ',.w_MFCDPOS1)
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1
            .w_MFMSINE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR',' ',.w_MFMSINE1))
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1
            .w_MFANINE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR',' ',.w_MFANINE1))
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1
            .w_MFMSFIE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFMSFIE1))
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1
            .w_MFANF1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFANF1))
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1
            .w_MFIMDAE1 = iif(empty(.w_MFCCOAE1),0,.w_MFIMDAE1)
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1.or. .o_MFCDENTE<>.w_MFCDENTE
            .w_MFIMCAE1 = iif(empty(.w_MFCCOAE1) or  .w_MFCDENTE='0003',0,.w_MFIMCAE1)
        endif
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_VPSDENT2 = space(2)
          .link_6_16('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_VRSDENT2 = space(5)
          .link_6_17('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_VRSDENT2<>.w_VRSDENT2.or. .o_VPSDENT2<>.w_VPSDENT2
            .w_MFSDENT2 = iif(.w_MFCDENTE='0003',.w_VPSDENT2,.w_VRSDENT2)
        endif
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_MFCCOAE2 = iif(empty(.w_MFCDENTE),' ',.w_MFCCOAE2)
          .link_6_19('Full')
        endif
        if .o_MFSDENT2<>.w_MFSDENT2
            .w_OLSDENT2 = .w_MFSDENT2
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2
            .w_MFCDPOS2 = iif(empty(.w_MFCCOAE2),' ',.w_MFCDPOS2)
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2
            .w_MFMSINE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR',' ',.w_MFMSINE2))
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2
            .w_MFANINE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR',' ',.w_MFANINE2))
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2
            .w_MFMSFIE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFMSFIE2))
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2
            .w_MFANF2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFANF2))
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2
            .w_MFIMDAE2 = iif(empty(.w_MFCCOAE2),0,.w_MFIMDAE2)
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2.or. .o_MFCDENTE<>.w_MFCDENTE
            .w_MFIMCAE2 = iif(empty(.w_MFCCOAE2) or .w_MFCDENTE='0003',0,.w_MFIMCAE2)
        endif
            .w_MFTDAENT = .w_MFIMDAE1+.w_MFIMDAE2
            .w_MFTCAENT = .w_MFIMCAE1+.w_MFIMCAE2
            .w_MFSALAEN = .w_MFTDAENT-.w_MFTCAENT
        .DoRTCalc(153,153,.t.)
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(156,159,.t.)
            .w_MFSALFIN = .w_APPOIMP + .w_APPOIMP2 + .w_MFSALDPS + .w_MFSALDRE + .w_MFSALINA + .w_MFSALAEN
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page6.oPag.oObj_6_51.Calculate()
        .oPgFrm.Page7.oPag.oObj_7_10.Calculate()
        .DoRTCalc(163,171,.t.)
        if .o_MFDESSTA<>.w_MFDESSTA.or. .o_MFSERIAL<>.w_MFSERIAL
            .w_CAP = iif(empty(NVL(.w_MFDESSTA,'')),g_capazi,.w_CAP1)
        endif
        if .o_MFDESSTA<>.w_MFDESSTA.or. .o_MFSERIAL<>.w_MFSERIAL
            .w_LOCALI = UPPER(iif(EMPTY(NVL(.w_MFDESSTA,'')),g_locazi,.w_LOCALI1))
        endif
        if .o_MFDESSTA<>.w_MFDESSTA.or. .o_MFSERIAL<>.w_MFSERIAL
            .w_PROVIN = UPPER(iif(empty(NVL(.w_MFDESSTA,'')),g_proazi,.w_PROVIN1))
        endif
        if .o_MFDESSTA<>.w_MFDESSTA.or. .o_MFSERIAL<>.w_MFSERIAL
            .w_INDIRI = UPPER(iif(empty(NVL(.w_MFDESSTA,'')),g_indazi,.w_INDIRI1))
        endif
        .oPgFrm.Page7.oPag.oObj_7_29.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_68.Calculate(ah_MSGFORMAT('Codice %0sede'))
        .oPgFrm.Page3.oPag.oObj_3_69.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page4.oPag.oObj_4_44.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page4.oPag.oObj_4_45.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page4.oPag.oObj_4_46.Calculate(ah_MSGFORMAT('Codice %0regione'))
        .oPgFrm.Page5.oPag.oObj_5_42.Calculate(ah_MSGFORMAT('Numero di %0riferimento'))
        .oPgFrm.Page6.oPag.oObj_6_52.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page6.oPag.oObj_6_53.Calculate(iif(.w_MFCDENTE<>'0003',ah_MSGFORMAT('Codice %0sede'),ah_MSGFORMAT('Codice %0provincia')))
        .oPgFrm.Page6.oPag.oObj_6_54.Calculate(ah_MSGFORMAT('Codice %0posizione'))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"MODEL","i_CODAZI,w_MFSERIAL")
          .op_MFSERIAL = .w_MFSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(176,178,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
        .oPgFrm.Page6.oPag.oObj_6_51.Calculate()
        .oPgFrm.Page7.oPag.oObj_7_10.Calculate()
        .oPgFrm.Page7.oPag.oObj_7_29.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_68.Calculate(ah_MSGFORMAT('Codice %0sede'))
        .oPgFrm.Page3.oPag.oObj_3_69.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page4.oPag.oObj_4_44.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page4.oPag.oObj_4_45.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page4.oPag.oObj_4_46.Calculate(ah_MSGFORMAT('Codice %0regione'))
        .oPgFrm.Page5.oPag.oObj_5_42.Calculate(ah_MSGFORMAT('Numero di %0riferimento'))
        .oPgFrm.Page6.oPag.oObj_6_52.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page6.oPag.oObj_6_53.Calculate(iif(.w_MFCDENTE<>'0003',ah_MSGFORMAT('Codice %0sede'),ah_MSGFORMAT('Codice %0provincia')))
        .oPgFrm.Page6.oPag.oObj_6_54.Calculate(ah_MSGFORMAT('Codice %0posizione'))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.enabled = this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.enabled = this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES1_3_7.enabled = this.oPgFrm.Page3.oPag.oMFDAMES1_3_7.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN1_3_8.enabled = this.oPgFrm.Page3.oPag.oMFDAANN1_3_8.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES1_3_9.enabled = this.oPgFrm.Page3.oPag.oMF_AMES1_3_9.mCond()
    this.oPgFrm.Page3.oPag.oMFAN1_3_10.enabled = this.oPgFrm.Page3.oPag.oMFAN1_3_10.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD1_3_11.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD1_3_11.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC1_3_12.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC1_3_12.mCond()
    this.oPgFrm.Page3.oPag.oMFCCONT2_3_14.enabled = this.oPgFrm.Page3.oPag.oMFCCONT2_3_14.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS2_3_15.enabled = this.oPgFrm.Page3.oPag.oMFMINPS2_3_15.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES2_3_17.enabled = this.oPgFrm.Page3.oPag.oMFDAMES2_3_17.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN2_3_18.enabled = this.oPgFrm.Page3.oPag.oMFDAANN2_3_18.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES2_3_20.enabled = this.oPgFrm.Page3.oPag.oMF_AMES2_3_20.mCond()
    this.oPgFrm.Page3.oPag.oMFAN2_3_21.enabled = this.oPgFrm.Page3.oPag.oMFAN2_3_21.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD2_3_22.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD2_3_22.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC2_3_23.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC2_3_23.mCond()
    this.oPgFrm.Page3.oPag.oMFCCONT3_3_25.enabled = this.oPgFrm.Page3.oPag.oMFCCONT3_3_25.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS3_3_26.enabled = this.oPgFrm.Page3.oPag.oMFMINPS3_3_26.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES3_3_28.enabled = this.oPgFrm.Page3.oPag.oMFDAMES3_3_28.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN3_3_30.enabled = this.oPgFrm.Page3.oPag.oMFDAANN3_3_30.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES3_3_31.enabled = this.oPgFrm.Page3.oPag.oMF_AMES3_3_31.mCond()
    this.oPgFrm.Page3.oPag.oMFAN3_3_32.enabled = this.oPgFrm.Page3.oPag.oMFAN3_3_32.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD3_3_33.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD3_3_33.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC3_3_34.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC3_3_34.mCond()
    this.oPgFrm.Page3.oPag.oMFCCONT4_3_36.enabled = this.oPgFrm.Page3.oPag.oMFCCONT4_3_36.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS4_3_37.enabled = this.oPgFrm.Page3.oPag.oMFMINPS4_3_37.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES4_3_39.enabled = this.oPgFrm.Page3.oPag.oMFDAMES4_3_39.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN4_3_41.enabled = this.oPgFrm.Page3.oPag.oMFDAANN4_3_41.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES4_3_42.enabled = this.oPgFrm.Page3.oPag.oMF_AMES4_3_42.mCond()
    this.oPgFrm.Page3.oPag.oMFAN4_3_43.enabled = this.oPgFrm.Page3.oPag.oMFAN4_3_43.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD4_3_44.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD4_3_44.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC4_3_45.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC4_3_45.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE1_4_3.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE1_4_3.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE1_4_4.enabled = this.oPgFrm.Page4.oPag.oMFRATRE1_4_4.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE1_4_5.enabled = this.oPgFrm.Page4.oPag.oMFANNRE1_4_5.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE1_4_6.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE1_4_6.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE1_4_7.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE1_4_7.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE2_4_12.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE2_4_12.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE2_4_13.enabled = this.oPgFrm.Page4.oPag.oMFRATRE2_4_13.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE2_4_14.enabled = this.oPgFrm.Page4.oPag.oMFANNRE2_4_14.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE2_4_15.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE2_4_15.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE2_4_16.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE2_4_16.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE3_4_18.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE3_4_18.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE3_4_19.enabled = this.oPgFrm.Page4.oPag.oMFRATRE3_4_19.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE3_4_20.enabled = this.oPgFrm.Page4.oPag.oMFANNRE3_4_20.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE3_4_21.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE3_4_21.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE3_4_22.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE3_4_22.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE4_4_24.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE4_4_24.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE4_4_25.enabled = this.oPgFrm.Page4.oPag.oMFRATRE4_4_25.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE4_4_26.enabled = this.oPgFrm.Page4.oPag.oMFANNRE4_4_26.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE4_4_27.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE4_4_27.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE4_4_28.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE4_4_28.mCond()
    this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.enabled = this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.mCond()
    this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.enabled = this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.mCond()
    this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.enabled = this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.mCond()
    this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.enabled = this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.mCond()
    this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.enabled = this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.mCond()
    this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.enabled = this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.mCond()
    this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.enabled = this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.mCond()
    this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.enabled = this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.mCond()
    this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.enabled = this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.mCond()
    this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.enabled = this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.mCond()
    this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.enabled = this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.mCond()
    this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.enabled = this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.mCond()
    this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.enabled = this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.mCond()
    this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.enabled = this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.mCond()
    this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.enabled = this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.mCond()
    this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.enabled = this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.mCond()
    this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.enabled = this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.mCond()
    this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.enabled = this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.mCond()
    this.oPgFrm.Page6.oPag.oVRSDENT1_6_4.enabled = this.oPgFrm.Page6.oPag.oVRSDENT1_6_4.mCond()
    this.oPgFrm.Page6.oPag.oVPSDENT1_6_5.enabled = this.oPgFrm.Page6.oPag.oVPSDENT1_6_5.mCond()
    this.oPgFrm.Page6.oPag.oMFCCOAE1_6_7.enabled = this.oPgFrm.Page6.oPag.oMFCCOAE1_6_7.mCond()
    this.oPgFrm.Page6.oPag.oMFCDPOS1_6_9.enabled = this.oPgFrm.Page6.oPag.oMFCDPOS1_6_9.mCond()
    this.oPgFrm.Page6.oPag.oMFMSINE1_6_10.enabled = this.oPgFrm.Page6.oPag.oMFMSINE1_6_10.mCond()
    this.oPgFrm.Page6.oPag.oMFANINE1_6_11.enabled = this.oPgFrm.Page6.oPag.oMFANINE1_6_11.mCond()
    this.oPgFrm.Page6.oPag.oMFMSFIE1_6_12.enabled = this.oPgFrm.Page6.oPag.oMFMSFIE1_6_12.mCond()
    this.oPgFrm.Page6.oPag.oMFANF1_6_13.enabled = this.oPgFrm.Page6.oPag.oMFANF1_6_13.mCond()
    this.oPgFrm.Page6.oPag.oMFIMDAE1_6_14.enabled = this.oPgFrm.Page6.oPag.oMFIMDAE1_6_14.mCond()
    this.oPgFrm.Page6.oPag.oMFIMCAE1_6_15.enabled = this.oPgFrm.Page6.oPag.oMFIMCAE1_6_15.mCond()
    this.oPgFrm.Page6.oPag.oVPSDENT2_6_16.enabled = this.oPgFrm.Page6.oPag.oVPSDENT2_6_16.mCond()
    this.oPgFrm.Page6.oPag.oVRSDENT2_6_17.enabled = this.oPgFrm.Page6.oPag.oVRSDENT2_6_17.mCond()
    this.oPgFrm.Page6.oPag.oMFCCOAE2_6_19.enabled = this.oPgFrm.Page6.oPag.oMFCCOAE2_6_19.mCond()
    this.oPgFrm.Page6.oPag.oMFCDPOS2_6_21.enabled = this.oPgFrm.Page6.oPag.oMFCDPOS2_6_21.mCond()
    this.oPgFrm.Page6.oPag.oMFMSINE2_6_22.enabled = this.oPgFrm.Page6.oPag.oMFMSINE2_6_22.mCond()
    this.oPgFrm.Page6.oPag.oMFANINE2_6_23.enabled = this.oPgFrm.Page6.oPag.oMFANINE2_6_23.mCond()
    this.oPgFrm.Page6.oPag.oMFMSFIE2_6_24.enabled = this.oPgFrm.Page6.oPag.oMFMSFIE2_6_24.mCond()
    this.oPgFrm.Page6.oPag.oMFANF2_6_25.enabled = this.oPgFrm.Page6.oPag.oMFANF2_6_25.mCond()
    this.oPgFrm.Page6.oPag.oMFIMDAE2_6_26.enabled = this.oPgFrm.Page6.oPag.oMFIMDAE2_6_26.mCond()
    this.oPgFrm.Page6.oPag.oMFIMCAE2_6_27.enabled = this.oPgFrm.Page6.oPag.oMFIMCAE2_6_27.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_47.enabled = this.oPgFrm.Page6.oPag.oBtn_6_47.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page6.oPag.oVRSDENT1_6_4.visible=!this.oPgFrm.Page6.oPag.oVRSDENT1_6_4.mHide()
    this.oPgFrm.Page6.oPag.oVPSDENT1_6_5.visible=!this.oPgFrm.Page6.oPag.oVPSDENT1_6_5.mHide()
    this.oPgFrm.Page6.oPag.oVPSDENT2_6_16.visible=!this.oPgFrm.Page6.oPag.oVPSDENT2_6_16.mHide()
    this.oPgFrm.Page6.oPag.oVRSDENT2_6_17.visible=!this.oPgFrm.Page6.oPag.oVRSDENT2_6_17.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_47.visible=!this.oPgFrm.Page6.oPag.oBtn_6_47.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_16.visible=!this.oPgFrm.Page2.oPag.oBtn_2_16.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_67.visible=!this.oPgFrm.Page3.oPag.oBtn_3_67.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_13.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_15.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_51.Event(cEvent)
      .oPgFrm.Page7.oPag.oObj_7_10.Event(cEvent)
      .oPgFrm.Page7.oPag.oObj_7_29.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_68.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_69.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_44.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_45.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_46.Event(cEvent)
      .oPgFrm.Page5.oPag.oObj_5_42.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_52.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_53.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_54.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MFCODUFF
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CODI_UFF_IDX,3]
    i_lTable = "CODI_UFF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CODI_UFF_IDX,2], .t., this.CODI_UFF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CODI_UFF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODUFF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AUF',True,'CODI_UFF')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UFCODICE like "+cp_ToStrODBC(trim(this.w_MFCODUFF)+"%");

          i_ret=cp_SQL(i_nConn,"select UFCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UFCODICE',trim(this.w_MFCODUFF))
          select UFCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODUFF)==trim(_Link_.UFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODUFF) and !this.bDontReportError
            deferred_cp_zoom('CODI_UFF','*','UFCODICE',cp_AbsName(oSource.parent,'oMFCODUFF_2_4'),i_cWhere,'GSCG_AUF',"Codici ufficio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',oSource.xKey(1))
            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODUFF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(this.w_MFCODUFF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',this.w_MFCODUFF)
            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODUFF = NVL(_Link_.UFCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODUFF = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CODI_UFF_IDX,2])+'\'+cp_ToStr(_Link_.UFCODICE,1)
      cp_ShowWarn(i_cKey,this.CODI_UFF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODUFF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED1
  func Link_3_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED1)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED1))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED1)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED1) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED1_3_2'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED1)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED1 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT1
  func Link_3_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT1))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT1)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT1) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT1_3_3'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT1)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT1 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED2
  func Link_3_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED2)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED2))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED2)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED2) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED2_3_13'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED2)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED2 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT2
  func Link_3_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT2)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT2))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT2)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT2) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT2_3_14'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT2)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT2 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED3
  func Link_3_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED3)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED3))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED3)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED3) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED3_3_24'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED3)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED3 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT3
  func Link_3_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT3)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT3))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT3)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT3) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT3_3_25'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT3)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT3 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED4
  func Link_3_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED4)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED4))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED4)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED4) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED4_3_35'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED4)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED4 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED4 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT4
  func Link_3_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT4)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT4))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT4)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT4) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT4_3_36'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT4)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT4 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT4 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCODRE1
  func Link_4_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE1)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE1))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE1)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE1) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE1_4_2'),i_cWhere,'GSCG_ARP',"Codici regione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE1)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE1 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE1 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE1
  func Link_4_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE1))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE1)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE1) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE1_4_3'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE1)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE1 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE1 = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Regione' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
        endif
        this.w_MFTRIRE1 = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_3.TRCODICE as TRCODICE403"+ ",link_4_3.TRTIPTRI as TRTIPTRI403"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_3 on MOD_PAG.MFTRIRE1=link_4_3.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_3"
          i_cKey=i_cKey+'+" and MOD_PAG.MFTRIRE1=link_4_3.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFCODRE2
  func Link_4_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE2)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE2))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE2)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE2) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE2_4_11'),i_cWhere,'GSCG_ARP',"Codici regione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE2)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE2 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE2 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE2
  func Link_4_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE2))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE2)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE2) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE2_4_12'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE2)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE2 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE2 = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Regione'  or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
        endif
        this.w_MFTRIRE2 = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_12.TRCODICE as TRCODICE412"+ ",link_4_12.TRTIPTRI as TRTIPTRI412"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_12 on MOD_PAG.MFTRIRE2=link_4_12.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_12"
          i_cKey=i_cKey+'+" and MOD_PAG.MFTRIRE2=link_4_12.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFCODRE3
  func Link_4_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE3)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE3))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE3)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE3) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE3_4_17'),i_cWhere,'GSCG_ARP',"Codici regione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE3)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE3 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE3 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE3
  func Link_4_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE3)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE3))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE3)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE3) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE3_4_18'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE3)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE3 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE3 = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Regione'   or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
        endif
        this.w_MFTRIRE3 = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_18.TRCODICE as TRCODICE418"+ ",link_4_18.TRTIPTRI as TRTIPTRI418"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_18 on MOD_PAG.MFTRIRE3=link_4_18.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_18"
          i_cKey=i_cKey+'+" and MOD_PAG.MFTRIRE3=link_4_18.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFCODRE4
  func Link_4_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE4)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE4))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE4)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE4) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE4_4_23'),i_cWhere,'GSCG_ARP',"Codici regione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE4)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE4 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE4 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE4
  func Link_4_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE4)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE4))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE4)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE4) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE4_4_24'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE4)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE4 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE4 = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Regione'  or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
        endif
        this.w_MFTRIRE4 = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_24.TRCODICE as TRCODICE424"+ ",link_4_24.TRTIPTRI as TRTIPTRI424"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_24 on MOD_PAG.MFTRIRE4=link_4_24.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_24"
          i_cKey=i_cKey+'+" and MOD_PAG.MFTRIRE4=link_4_24.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFSINAI1
  func Link_5_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_lTable = "SE_INAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2], .t., this.SE_INAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSINAI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ASI',True,'SE_INAIL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SICODICE like "+cp_ToStrODBC(trim(this.w_MFSINAI1)+"%");

          i_ret=cp_SQL(i_nConn,"select SICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SICODICE',trim(this.w_MFSINAI1))
          select SICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSINAI1)==trim(_Link_.SICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSINAI1) and !this.bDontReportError
            deferred_cp_zoom('SE_INAIL','*','SICODICE',cp_AbsName(oSource.parent,'oMFSINAI1_5_2'),i_cWhere,'GSCG_ASI',"Codici sede INAIL",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',oSource.xKey(1))
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSINAI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(this.w_MFSINAI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',this.w_MFSINAI1)
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSINAI1 = NVL(_Link_.SICODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSINAI1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])+'\'+cp_ToStr(_Link_.SICODICE,1)
      cp_ShowWarn(i_cKey,this.SE_INAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSINAI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFSINAI2
  func Link_5_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_lTable = "SE_INAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2], .t., this.SE_INAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSINAI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ASI',True,'SE_INAIL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SICODICE like "+cp_ToStrODBC(trim(this.w_MFSINAI2)+"%");

          i_ret=cp_SQL(i_nConn,"select SICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SICODICE',trim(this.w_MFSINAI2))
          select SICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSINAI2)==trim(_Link_.SICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSINAI2) and !this.bDontReportError
            deferred_cp_zoom('SE_INAIL','*','SICODICE',cp_AbsName(oSource.parent,'oMFSINAI2_5_9'),i_cWhere,'GSCG_ASI',"Codici sede INAIL",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',oSource.xKey(1))
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSINAI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(this.w_MFSINAI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',this.w_MFSINAI2)
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSINAI2 = NVL(_Link_.SICODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSINAI2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])+'\'+cp_ToStr(_Link_.SICODICE,1)
      cp_ShowWarn(i_cKey,this.SE_INAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSINAI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFSINAI3
  func Link_5_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_lTable = "SE_INAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2], .t., this.SE_INAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSINAI3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ASI',True,'SE_INAIL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SICODICE like "+cp_ToStrODBC(trim(this.w_MFSINAI3)+"%");

          i_ret=cp_SQL(i_nConn,"select SICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SICODICE',trim(this.w_MFSINAI3))
          select SICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSINAI3)==trim(_Link_.SICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSINAI3) and !this.bDontReportError
            deferred_cp_zoom('SE_INAIL','*','SICODICE',cp_AbsName(oSource.parent,'oMFSINAI3_5_16'),i_cWhere,'GSCG_ASI',"Codici sede INAIL",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',oSource.xKey(1))
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSINAI3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(this.w_MFSINAI3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',this.w_MFSINAI3)
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSINAI3 = NVL(_Link_.SICODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSINAI3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])+'\'+cp_ToStr(_Link_.SICODICE,1)
      cp_ShowWarn(i_cKey,this.SE_INAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSINAI3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDENTE
  func Link_6_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_PREV_IDX,3]
    i_lTable = "COD_PREV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2], .t., this.COD_PREV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACP',True,'COD_PREV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CPCODICE like "+cp_ToStrODBC(trim(this.w_MFCDENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select CPCODICE,CPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CPCODICE',trim(this.w_MFCDENTE))
          select CPCODICE,CPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDENTE)==trim(_Link_.CPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDENTE) and !this.bDontReportError
            deferred_cp_zoom('COD_PREV','*','CPCODICE',cp_AbsName(oSource.parent,'oMFCDENTE_6_3'),i_cWhere,'GSCG_ACP',"Codici enti previdenziali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPCODICE,CPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPCODICE',oSource.xKey(1))
            select CPCODICE,CPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPCODICE,CPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CPCODICE="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPCODICE',this.w_MFCDENTE)
            select CPCODICE,CPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDENTE = NVL(_Link_.CPCODICE,space(5))
      this.w_DESAENTE = NVL(_Link_.CPDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDENTE = space(5)
      endif
      this.w_DESAENTE = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2])+'\'+cp_ToStr(_Link_.CPCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_PREV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_PREV_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_3.CPCODICE as CPCODICE603"+ ",link_6_3.CPDESCRI as CPDESCRI603"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_3 on MOD_PAG.MFCDENTE=link_6_3.CPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_3"
          i_cKey=i_cKey+'+" and MOD_PAG.MFCDENTE=link_6_3.CPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VRSDENT1
  func Link_6_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_AEN_IDX,3]
    i_lTable = "SED_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2], .t., this.SED_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VRSDENT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MSE',True,'SED_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODSED like "+cp_ToStrODBC(trim(this.w_VRSDENT1)+"%");
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SECODENT,SECODSED","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SECODENT',this.w_MFCDENTE;
                     ,'SECODSED',trim(this.w_VRSDENT1))
          select SECODENT,SECODSED;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SECODENT,SECODSED into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VRSDENT1)==trim(_Link_.SECODSED) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VRSDENT1) and !this.bDontReportError
            deferred_cp_zoom('SED_AEN','*','SECODENT,SECODSED',cp_AbsName(oSource.parent,'oVRSDENT1_6_4'),i_cWhere,'GSCG_MSE',"Codici sede",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',oSource.xKey(1);
                       ,'SECODSED',oSource.xKey(2))
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VRSDENT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                   +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(this.w_VRSDENT1);
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',this.w_MFCDENTE;
                       ,'SECODSED',this.w_VRSDENT1)
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VRSDENT1 = NVL(_Link_.SECODSED,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VRSDENT1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])+'\'+cp_ToStr(_Link_.SECODENT,1)+'\'+cp_ToStr(_Link_.SECODSED,1)
      cp_ShowWarn(i_cKey,this.SED_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VRSDENT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPSDENT1
  func Link_6_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
    i_lTable = "ANAG_PRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2], .t., this.ANAG_PRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPSDENT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APR',True,'ANAG_PRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PRCODPRO like "+cp_ToStrODBC(trim(this.w_VPSDENT1)+"%");

          i_ret=cp_SQL(i_nConn,"select PRCODPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PRCODPRO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PRCODPRO',trim(this.w_VPSDENT1))
          select PRCODPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PRCODPRO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPSDENT1)==trim(_Link_.PRCODPRO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPSDENT1) and !this.bDontReportError
            deferred_cp_zoom('ANAG_PRO','*','PRCODPRO',cp_AbsName(oSource.parent,'oVPSDENT1_6_5'),i_cWhere,'GSAR_APR',"Elenco province",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODPRO";
                     +" from "+i_cTable+" "+i_lTable+" where PRCODPRO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODPRO',oSource.xKey(1))
            select PRCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPSDENT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODPRO="+cp_ToStrODBC(this.w_VPSDENT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODPRO',this.w_VPSDENT1)
            select PRCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPSDENT1 = NVL(_Link_.PRCODPRO,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VPSDENT1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])+'\'+cp_ToStr(_Link_.PRCODPRO,1)
      cp_ShowWarn(i_cKey,this.ANAG_PRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPSDENT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCOAE1
  func Link_6_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_AEN_IDX,3]
    i_lTable = "CAU_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2], .t., this.CAU_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCOAE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MAE',True,'CAU_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AECAUSEN like "+cp_ToStrODBC(trim(this.w_MFCCOAE1)+"%");
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AECODENT,AECAUSEN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AECODENT',this.w_MFCDENTE;
                     ,'AECAUSEN',trim(this.w_MFCCOAE1))
          select AECODENT,AECAUSEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AECODENT,AECAUSEN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCOAE1)==trim(_Link_.AECAUSEN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCOAE1) and !this.bDontReportError
            deferred_cp_zoom('CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(oSource.parent,'oMFCCOAE1_6_7'),i_cWhere,'GSCG_MAE',"Causali contributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(oSource.xKey(2));
                     +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',oSource.xKey(1);
                       ,'AECAUSEN',oSource.xKey(2))
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCOAE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                   +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(this.w_MFCCOAE1);
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',this.w_MFCDENTE;
                       ,'AECAUSEN',this.w_MFCCOAE1)
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCOAE1 = NVL(_Link_.AECAUSEN,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCOAE1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])+'\'+cp_ToStr(_Link_.AECODENT,1)+'\'+cp_ToStr(_Link_.AECAUSEN,1)
      cp_ShowWarn(i_cKey,this.CAU_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCOAE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPSDENT2
  func Link_6_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
    i_lTable = "ANAG_PRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2], .t., this.ANAG_PRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPSDENT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APR',True,'ANAG_PRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PRCODPRO like "+cp_ToStrODBC(trim(this.w_VPSDENT2)+"%");

          i_ret=cp_SQL(i_nConn,"select PRCODPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PRCODPRO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PRCODPRO',trim(this.w_VPSDENT2))
          select PRCODPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PRCODPRO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPSDENT2)==trim(_Link_.PRCODPRO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPSDENT2) and !this.bDontReportError
            deferred_cp_zoom('ANAG_PRO','*','PRCODPRO',cp_AbsName(oSource.parent,'oVPSDENT2_6_16'),i_cWhere,'GSAR_APR',"Elenco province",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODPRO";
                     +" from "+i_cTable+" "+i_lTable+" where PRCODPRO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODPRO',oSource.xKey(1))
            select PRCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPSDENT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODPRO="+cp_ToStrODBC(this.w_VPSDENT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODPRO',this.w_VPSDENT2)
            select PRCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPSDENT2 = NVL(_Link_.PRCODPRO,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VPSDENT2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])+'\'+cp_ToStr(_Link_.PRCODPRO,1)
      cp_ShowWarn(i_cKey,this.ANAG_PRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPSDENT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VRSDENT2
  func Link_6_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_AEN_IDX,3]
    i_lTable = "SED_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2], .t., this.SED_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VRSDENT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MSE',True,'SED_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODSED like "+cp_ToStrODBC(trim(this.w_VRSDENT2)+"%");
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SECODENT,SECODSED","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SECODENT',this.w_MFCDENTE;
                     ,'SECODSED',trim(this.w_VRSDENT2))
          select SECODENT,SECODSED;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SECODENT,SECODSED into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VRSDENT2)==trim(_Link_.SECODSED) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VRSDENT2) and !this.bDontReportError
            deferred_cp_zoom('SED_AEN','*','SECODENT,SECODSED',cp_AbsName(oSource.parent,'oVRSDENT2_6_17'),i_cWhere,'GSCG_MSE',"Codici sede",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',oSource.xKey(1);
                       ,'SECODSED',oSource.xKey(2))
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VRSDENT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                   +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(this.w_VRSDENT2);
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',this.w_MFCDENTE;
                       ,'SECODSED',this.w_VRSDENT2)
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VRSDENT2 = NVL(_Link_.SECODSED,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VRSDENT2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])+'\'+cp_ToStr(_Link_.SECODENT,1)+'\'+cp_ToStr(_Link_.SECODSED,1)
      cp_ShowWarn(i_cKey,this.SED_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VRSDENT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCOAE2
  func Link_6_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_AEN_IDX,3]
    i_lTable = "CAU_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2], .t., this.CAU_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCOAE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MAE',True,'CAU_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AECAUSEN like "+cp_ToStrODBC(trim(this.w_MFCCOAE2)+"%");
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AECODENT,AECAUSEN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AECODENT',this.w_MFCDENTE;
                     ,'AECAUSEN',trim(this.w_MFCCOAE2))
          select AECODENT,AECAUSEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AECODENT,AECAUSEN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCOAE2)==trim(_Link_.AECAUSEN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCOAE2) and !this.bDontReportError
            deferred_cp_zoom('CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(oSource.parent,'oMFCCOAE2_6_19'),i_cWhere,'GSCG_MAE',"Causali contributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(oSource.xKey(2));
                     +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',oSource.xKey(1);
                       ,'AECAUSEN',oSource.xKey(2))
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCOAE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                   +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(this.w_MFCCOAE2);
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',this.w_MFCDENTE;
                       ,'AECAUSEN',this.w_MFCCOAE2)
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCOAE2 = NVL(_Link_.AECAUSEN,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCOAE2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])+'\'+cp_ToStr(_Link_.AECODENT,1)+'\'+cp_ToStr(_Link_.AECAUSEN,1)
      cp_ShowWarn(i_cKey,this.CAU_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCOAE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFDESSTA
  func Link_7_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFDESSTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASE',True,'SEDIAZIE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODDES like "+cp_ToStrODBC(trim(this.w_MFDESSTA)+"%");
                   +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZIE);

          i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SECODAZI,SECODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SECODAZI',this.w_CODAZIE;
                     ,'SECODDES',trim(this.w_MFDESSTA))
          select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SECODAZI,SECODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFDESSTA)==trim(_Link_.SECODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFDESSTA) and !this.bDontReportError
            deferred_cp_zoom('SEDIAZIE','*','SECODAZI,SECODDES',cp_AbsName(oSource.parent,'oMFDESSTA_7_11'),i_cWhere,'GSAR_ASE',"Sedi aziende",'GSAR_ASI.SEDIAZIE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZIE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA";
                     +" from "+i_cTable+" "+i_lTable+" where SECODDES="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZIE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',oSource.xKey(1);
                       ,'SECODDES',oSource.xKey(2))
            select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFDESSTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA";
                   +" from "+i_cTable+" "+i_lTable+" where SECODDES="+cp_ToStrODBC(this.w_MFDESSTA);
                   +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZIE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',this.w_CODAZIE;
                       ,'SECODDES',this.w_MFDESSTA)
            select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFDESSTA = NVL(_Link_.SECODDES,space(5))
      this.w_CAP1 = NVL(_Link_.SE___CAP,space(9))
      this.w_LOCALI1 = NVL(_Link_.SELOCALI,space(30))
      this.w_PROVIN1 = NVL(_Link_.SEPROVIN,space(2))
      this.w_INDIRI1 = NVL(_Link_.SEINDIRI,space(35))
      this.w_DESSTA = NVL(_Link_.SEDESSTA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MFDESSTA = space(5)
      endif
      this.w_CAP1 = space(9)
      this.w_LOCALI1 = space(30)
      this.w_PROVIN1 = space(2)
      this.w_INDIRI1 = space(35)
      this.w_DESSTA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SECODAZI,1)+'\'+cp_ToStr(_Link_.SECODDES,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFDESSTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMFSERIAL_1_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page1.oPag.oMFSERIAL_1_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMFMESRIF_1_2.value==this.w_MFMESRIF)
      this.oPgFrm.Page1.oPag.oMFMESRIF_1_2.value=this.w_MFMESRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oMFANNRIF_1_3.value==this.w_MFANNRIF)
      this.oPgFrm.Page1.oPag.oMFANNRIF_1_3.value=this.w_MFANNRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oMF_COINC_1_7.RadioValue()==this.w_MF_COINC)
      this.oPgFrm.Page1.oPag.oMF_COINC_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMFSERIAL_2_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page2.oPag.oMFSERIAL_2_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page2.oPag.oMFCODUFF_2_4.value==this.w_MFCODUFF)
      this.oPgFrm.Page2.oPag.oMFCODUFF_2_4.value=this.w_MFCODUFF
    endif
    if not(this.oPgFrm.Page2.oPag.oMFCODATT_2_5.value==this.w_MFCODATT)
      this.oPgFrm.Page2.oPag.oMFCODATT_2_5.value=this.w_MFCODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oMESE_2_8.value==this.w_MESE)
      this.oPgFrm.Page2.oPag.oMESE_2_8.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page2.oPag.oANNO_2_9.value==this.w_ANNO)
      this.oPgFrm.Page2.oPag.oANNO_2_9.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page3.oPag.oMFSERIAL_3_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page3.oPag.oMFSERIAL_3_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED1_3_2.value==this.w_MFCDSED1)
      this.oPgFrm.Page3.oPag.oMFCDSED1_3_2.value=this.w_MFCDSED1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.value==this.w_MFCCONT1)
      this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.value=this.w_MFCCONT1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.value==this.w_MFMINPS1)
      this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.value=this.w_MFMINPS1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES1_3_7.value==this.w_MFDAMES1)
      this.oPgFrm.Page3.oPag.oMFDAMES1_3_7.value=this.w_MFDAMES1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN1_3_8.value==this.w_MFDAANN1)
      this.oPgFrm.Page3.oPag.oMFDAANN1_3_8.value=this.w_MFDAANN1
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES1_3_9.value==this.w_MF_AMES1)
      this.oPgFrm.Page3.oPag.oMF_AMES1_3_9.value=this.w_MF_AMES1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN1_3_10.value==this.w_MFAN1)
      this.oPgFrm.Page3.oPag.oMFAN1_3_10.value=this.w_MFAN1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD1_3_11.value==this.w_MFIMPSD1)
      this.oPgFrm.Page3.oPag.oMFIMPSD1_3_11.value=this.w_MFIMPSD1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC1_3_12.value==this.w_MFIMPSC1)
      this.oPgFrm.Page3.oPag.oMFIMPSC1_3_12.value=this.w_MFIMPSC1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED2_3_13.value==this.w_MFCDSED2)
      this.oPgFrm.Page3.oPag.oMFCDSED2_3_13.value=this.w_MFCDSED2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT2_3_14.value==this.w_MFCCONT2)
      this.oPgFrm.Page3.oPag.oMFCCONT2_3_14.value=this.w_MFCCONT2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS2_3_15.value==this.w_MFMINPS2)
      this.oPgFrm.Page3.oPag.oMFMINPS2_3_15.value=this.w_MFMINPS2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES2_3_17.value==this.w_MFDAMES2)
      this.oPgFrm.Page3.oPag.oMFDAMES2_3_17.value=this.w_MFDAMES2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN2_3_18.value==this.w_MFDAANN2)
      this.oPgFrm.Page3.oPag.oMFDAANN2_3_18.value=this.w_MFDAANN2
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES2_3_20.value==this.w_MF_AMES2)
      this.oPgFrm.Page3.oPag.oMF_AMES2_3_20.value=this.w_MF_AMES2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN2_3_21.value==this.w_MFAN2)
      this.oPgFrm.Page3.oPag.oMFAN2_3_21.value=this.w_MFAN2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD2_3_22.value==this.w_MFIMPSD2)
      this.oPgFrm.Page3.oPag.oMFIMPSD2_3_22.value=this.w_MFIMPSD2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC2_3_23.value==this.w_MFIMPSC2)
      this.oPgFrm.Page3.oPag.oMFIMPSC2_3_23.value=this.w_MFIMPSC2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED3_3_24.value==this.w_MFCDSED3)
      this.oPgFrm.Page3.oPag.oMFCDSED3_3_24.value=this.w_MFCDSED3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT3_3_25.value==this.w_MFCCONT3)
      this.oPgFrm.Page3.oPag.oMFCCONT3_3_25.value=this.w_MFCCONT3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS3_3_26.value==this.w_MFMINPS3)
      this.oPgFrm.Page3.oPag.oMFMINPS3_3_26.value=this.w_MFMINPS3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES3_3_28.value==this.w_MFDAMES3)
      this.oPgFrm.Page3.oPag.oMFDAMES3_3_28.value=this.w_MFDAMES3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN3_3_30.value==this.w_MFDAANN3)
      this.oPgFrm.Page3.oPag.oMFDAANN3_3_30.value=this.w_MFDAANN3
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES3_3_31.value==this.w_MF_AMES3)
      this.oPgFrm.Page3.oPag.oMF_AMES3_3_31.value=this.w_MF_AMES3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN3_3_32.value==this.w_MFAN3)
      this.oPgFrm.Page3.oPag.oMFAN3_3_32.value=this.w_MFAN3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD3_3_33.value==this.w_MFIMPSD3)
      this.oPgFrm.Page3.oPag.oMFIMPSD3_3_33.value=this.w_MFIMPSD3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC3_3_34.value==this.w_MFIMPSC3)
      this.oPgFrm.Page3.oPag.oMFIMPSC3_3_34.value=this.w_MFIMPSC3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED4_3_35.value==this.w_MFCDSED4)
      this.oPgFrm.Page3.oPag.oMFCDSED4_3_35.value=this.w_MFCDSED4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT4_3_36.value==this.w_MFCCONT4)
      this.oPgFrm.Page3.oPag.oMFCCONT4_3_36.value=this.w_MFCCONT4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS4_3_37.value==this.w_MFMINPS4)
      this.oPgFrm.Page3.oPag.oMFMINPS4_3_37.value=this.w_MFMINPS4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES4_3_39.value==this.w_MFDAMES4)
      this.oPgFrm.Page3.oPag.oMFDAMES4_3_39.value=this.w_MFDAMES4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN4_3_41.value==this.w_MFDAANN4)
      this.oPgFrm.Page3.oPag.oMFDAANN4_3_41.value=this.w_MFDAANN4
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES4_3_42.value==this.w_MF_AMES4)
      this.oPgFrm.Page3.oPag.oMF_AMES4_3_42.value=this.w_MF_AMES4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN4_3_43.value==this.w_MFAN4)
      this.oPgFrm.Page3.oPag.oMFAN4_3_43.value=this.w_MFAN4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD4_3_44.value==this.w_MFIMPSD4)
      this.oPgFrm.Page3.oPag.oMFIMPSD4_3_44.value=this.w_MFIMPSD4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC4_3_45.value==this.w_MFIMPSC4)
      this.oPgFrm.Page3.oPag.oMFIMPSC4_3_45.value=this.w_MFIMPSC4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFTOTDPS_3_55.value==this.w_MFTOTDPS)
      this.oPgFrm.Page3.oPag.oMFTOTDPS_3_55.value=this.w_MFTOTDPS
    endif
    if not(this.oPgFrm.Page3.oPag.oMFTOTCPS_3_56.value==this.w_MFTOTCPS)
      this.oPgFrm.Page3.oPag.oMFTOTCPS_3_56.value=this.w_MFTOTCPS
    endif
    if not(this.oPgFrm.Page3.oPag.oMFSALDPS_3_57.value==this.w_MFSALDPS)
      this.oPgFrm.Page3.oPag.oMFSALDPS_3_57.value=this.w_MFSALDPS
    endif
    if not(this.oPgFrm.Page3.oPag.oMESE_3_63.value==this.w_MESE)
      this.oPgFrm.Page3.oPag.oMESE_3_63.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page3.oPag.oANNO_3_64.value==this.w_ANNO)
      this.oPgFrm.Page3.oPag.oANNO_3_64.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page4.oPag.oMFSERIAL_4_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page4.oPag.oMFSERIAL_4_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE1_4_2.value==this.w_MFCODRE1)
      this.oPgFrm.Page4.oPag.oMFCODRE1_4_2.value=this.w_MFCODRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE1_4_3.value==this.w_MFTRIRE1)
      this.oPgFrm.Page4.oPag.oMFTRIRE1_4_3.value=this.w_MFTRIRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE1_4_4.value==this.w_MFRATRE1)
      this.oPgFrm.Page4.oPag.oMFRATRE1_4_4.value=this.w_MFRATRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE1_4_5.value==this.w_MFANNRE1)
      this.oPgFrm.Page4.oPag.oMFANNRE1_4_5.value=this.w_MFANNRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE1_4_6.value==this.w_MFIMDRE1)
      this.oPgFrm.Page4.oPag.oMFIMDRE1_4_6.value=this.w_MFIMDRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE1_4_7.value==this.w_MFIMCRE1)
      this.oPgFrm.Page4.oPag.oMFIMCRE1_4_7.value=this.w_MFIMCRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE2_4_11.value==this.w_MFCODRE2)
      this.oPgFrm.Page4.oPag.oMFCODRE2_4_11.value=this.w_MFCODRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE2_4_12.value==this.w_MFTRIRE2)
      this.oPgFrm.Page4.oPag.oMFTRIRE2_4_12.value=this.w_MFTRIRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE2_4_13.value==this.w_MFRATRE2)
      this.oPgFrm.Page4.oPag.oMFRATRE2_4_13.value=this.w_MFRATRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE2_4_14.value==this.w_MFANNRE2)
      this.oPgFrm.Page4.oPag.oMFANNRE2_4_14.value=this.w_MFANNRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE2_4_15.value==this.w_MFIMDRE2)
      this.oPgFrm.Page4.oPag.oMFIMDRE2_4_15.value=this.w_MFIMDRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE2_4_16.value==this.w_MFIMCRE2)
      this.oPgFrm.Page4.oPag.oMFIMCRE2_4_16.value=this.w_MFIMCRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE3_4_17.value==this.w_MFCODRE3)
      this.oPgFrm.Page4.oPag.oMFCODRE3_4_17.value=this.w_MFCODRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE3_4_18.value==this.w_MFTRIRE3)
      this.oPgFrm.Page4.oPag.oMFTRIRE3_4_18.value=this.w_MFTRIRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE3_4_19.value==this.w_MFRATRE3)
      this.oPgFrm.Page4.oPag.oMFRATRE3_4_19.value=this.w_MFRATRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE3_4_20.value==this.w_MFANNRE3)
      this.oPgFrm.Page4.oPag.oMFANNRE3_4_20.value=this.w_MFANNRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE3_4_21.value==this.w_MFIMDRE3)
      this.oPgFrm.Page4.oPag.oMFIMDRE3_4_21.value=this.w_MFIMDRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE3_4_22.value==this.w_MFIMCRE3)
      this.oPgFrm.Page4.oPag.oMFIMCRE3_4_22.value=this.w_MFIMCRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE4_4_23.value==this.w_MFCODRE4)
      this.oPgFrm.Page4.oPag.oMFCODRE4_4_23.value=this.w_MFCODRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE4_4_24.value==this.w_MFTRIRE4)
      this.oPgFrm.Page4.oPag.oMFTRIRE4_4_24.value=this.w_MFTRIRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE4_4_25.value==this.w_MFRATRE4)
      this.oPgFrm.Page4.oPag.oMFRATRE4_4_25.value=this.w_MFRATRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE4_4_26.value==this.w_MFANNRE4)
      this.oPgFrm.Page4.oPag.oMFANNRE4_4_26.value=this.w_MFANNRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE4_4_27.value==this.w_MFIMDRE4)
      this.oPgFrm.Page4.oPag.oMFIMDRE4_4_27.value=this.w_MFIMDRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE4_4_28.value==this.w_MFIMCRE4)
      this.oPgFrm.Page4.oPag.oMFIMCRE4_4_28.value=this.w_MFIMCRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTOTDRE_4_32.value==this.w_MFTOTDRE)
      this.oPgFrm.Page4.oPag.oMFTOTDRE_4_32.value=this.w_MFTOTDRE
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTOTCRE_4_33.value==this.w_MFTOTCRE)
      this.oPgFrm.Page4.oPag.oMFTOTCRE_4_33.value=this.w_MFTOTCRE
    endif
    if not(this.oPgFrm.Page4.oPag.oMFSALDRE_4_34.value==this.w_MFSALDRE)
      this.oPgFrm.Page4.oPag.oMFSALDRE_4_34.value=this.w_MFSALDRE
    endif
    if not(this.oPgFrm.Page4.oPag.oMESE_4_39.value==this.w_MESE)
      this.oPgFrm.Page4.oPag.oMESE_4_39.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page4.oPag.oANNO_4_40.value==this.w_ANNO)
      this.oPgFrm.Page4.oPag.oANNO_4_40.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSERIAL_5_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page5.oPag.oMFSERIAL_5_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSINAI1_5_2.value==this.w_MFSINAI1)
      this.oPgFrm.Page5.oPag.oMFSINAI1_5_2.value=this.w_MFSINAI1
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.value==this.w_MF_NPOS1)
      this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.value=this.w_MF_NPOS1
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.value==this.w_MF_PACC1)
      this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.value=this.w_MF_PACC1
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.value==this.w_MF_NRIF1)
      this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.value=this.w_MF_NRIF1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.value==this.w_MFCAUSA1)
      this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.value=this.w_MFCAUSA1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.value==this.w_MFIMDIL1)
      this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.value=this.w_MFIMDIL1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.value==this.w_MFIMCIL1)
      this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.value=this.w_MFIMCIL1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSINAI2_5_9.value==this.w_MFSINAI2)
      this.oPgFrm.Page5.oPag.oMFSINAI2_5_9.value=this.w_MFSINAI2
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.value==this.w_MF_NPOS2)
      this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.value=this.w_MF_NPOS2
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.value==this.w_MF_PACC2)
      this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.value=this.w_MF_PACC2
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.value==this.w_MF_NRIF2)
      this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.value=this.w_MF_NRIF2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.value==this.w_MFCAUSA2)
      this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.value=this.w_MFCAUSA2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.value==this.w_MFIMDIL2)
      this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.value=this.w_MFIMDIL2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.value==this.w_MFIMCIL2)
      this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.value=this.w_MFIMCIL2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSINAI3_5_16.value==this.w_MFSINAI3)
      this.oPgFrm.Page5.oPag.oMFSINAI3_5_16.value=this.w_MFSINAI3
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.value==this.w_MF_NPOS3)
      this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.value=this.w_MF_NPOS3
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.value==this.w_MF_PACC3)
      this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.value=this.w_MF_PACC3
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.value==this.w_MF_NRIF3)
      this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.value=this.w_MF_NRIF3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.value==this.w_MFCAUSA3)
      this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.value=this.w_MFCAUSA3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.value==this.w_MFIMDIL3)
      this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.value=this.w_MFIMDIL3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.value==this.w_MFIMCIL3)
      this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.value=this.w_MFIMCIL3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFTDINAI_5_30.value==this.w_MFTDINAI)
      this.oPgFrm.Page5.oPag.oMFTDINAI_5_30.value=this.w_MFTDINAI
    endif
    if not(this.oPgFrm.Page5.oPag.oMFTCINAI_5_31.value==this.w_MFTCINAI)
      this.oPgFrm.Page5.oPag.oMFTCINAI_5_31.value=this.w_MFTCINAI
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSALINA_5_32.value==this.w_MFSALINA)
      this.oPgFrm.Page5.oPag.oMFSALINA_5_32.value=this.w_MFSALINA
    endif
    if not(this.oPgFrm.Page5.oPag.oMESE_5_40.value==this.w_MESE)
      this.oPgFrm.Page5.oPag.oMESE_5_40.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page5.oPag.oANNO_5_41.value==this.w_ANNO)
      this.oPgFrm.Page5.oPag.oANNO_5_41.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page6.oPag.oMFSERIAL_6_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page6.oPag.oMFSERIAL_6_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCDENTE_6_3.value==this.w_MFCDENTE)
      this.oPgFrm.Page6.oPag.oMFCDENTE_6_3.value=this.w_MFCDENTE
    endif
    if not(this.oPgFrm.Page6.oPag.oVRSDENT1_6_4.value==this.w_VRSDENT1)
      this.oPgFrm.Page6.oPag.oVRSDENT1_6_4.value=this.w_VRSDENT1
    endif
    if not(this.oPgFrm.Page6.oPag.oVPSDENT1_6_5.value==this.w_VPSDENT1)
      this.oPgFrm.Page6.oPag.oVPSDENT1_6_5.value=this.w_VPSDENT1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCCOAE1_6_7.value==this.w_MFCCOAE1)
      this.oPgFrm.Page6.oPag.oMFCCOAE1_6_7.value=this.w_MFCCOAE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCDPOS1_6_9.value==this.w_MFCDPOS1)
      this.oPgFrm.Page6.oPag.oMFCDPOS1_6_9.value=this.w_MFCDPOS1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSINE1_6_10.value==this.w_MFMSINE1)
      this.oPgFrm.Page6.oPag.oMFMSINE1_6_10.value=this.w_MFMSINE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANINE1_6_11.value==this.w_MFANINE1)
      this.oPgFrm.Page6.oPag.oMFANINE1_6_11.value=this.w_MFANINE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSFIE1_6_12.value==this.w_MFMSFIE1)
      this.oPgFrm.Page6.oPag.oMFMSFIE1_6_12.value=this.w_MFMSFIE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANF1_6_13.value==this.w_MFANF1)
      this.oPgFrm.Page6.oPag.oMFANF1_6_13.value=this.w_MFANF1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMDAE1_6_14.value==this.w_MFIMDAE1)
      this.oPgFrm.Page6.oPag.oMFIMDAE1_6_14.value=this.w_MFIMDAE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMCAE1_6_15.value==this.w_MFIMCAE1)
      this.oPgFrm.Page6.oPag.oMFIMCAE1_6_15.value=this.w_MFIMCAE1
    endif
    if not(this.oPgFrm.Page6.oPag.oVPSDENT2_6_16.value==this.w_VPSDENT2)
      this.oPgFrm.Page6.oPag.oVPSDENT2_6_16.value=this.w_VPSDENT2
    endif
    if not(this.oPgFrm.Page6.oPag.oVRSDENT2_6_17.value==this.w_VRSDENT2)
      this.oPgFrm.Page6.oPag.oVRSDENT2_6_17.value=this.w_VRSDENT2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCCOAE2_6_19.value==this.w_MFCCOAE2)
      this.oPgFrm.Page6.oPag.oMFCCOAE2_6_19.value=this.w_MFCCOAE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCDPOS2_6_21.value==this.w_MFCDPOS2)
      this.oPgFrm.Page6.oPag.oMFCDPOS2_6_21.value=this.w_MFCDPOS2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSINE2_6_22.value==this.w_MFMSINE2)
      this.oPgFrm.Page6.oPag.oMFMSINE2_6_22.value=this.w_MFMSINE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANINE2_6_23.value==this.w_MFANINE2)
      this.oPgFrm.Page6.oPag.oMFANINE2_6_23.value=this.w_MFANINE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSFIE2_6_24.value==this.w_MFMSFIE2)
      this.oPgFrm.Page6.oPag.oMFMSFIE2_6_24.value=this.w_MFMSFIE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANF2_6_25.value==this.w_MFANF2)
      this.oPgFrm.Page6.oPag.oMFANF2_6_25.value=this.w_MFANF2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMDAE2_6_26.value==this.w_MFIMDAE2)
      this.oPgFrm.Page6.oPag.oMFIMDAE2_6_26.value=this.w_MFIMDAE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMCAE2_6_27.value==this.w_MFIMCAE2)
      this.oPgFrm.Page6.oPag.oMFIMCAE2_6_27.value=this.w_MFIMCAE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFTDAENT_6_35.value==this.w_MFTDAENT)
      this.oPgFrm.Page6.oPag.oMFTDAENT_6_35.value=this.w_MFTDAENT
    endif
    if not(this.oPgFrm.Page6.oPag.oMFTCAENT_6_36.value==this.w_MFTCAENT)
      this.oPgFrm.Page6.oPag.oMFTCAENT_6_36.value=this.w_MFTCAENT
    endif
    if not(this.oPgFrm.Page6.oPag.oMFSALAEN_6_37.value==this.w_MFSALAEN)
      this.oPgFrm.Page6.oPag.oMFSALAEN_6_37.value=this.w_MFSALAEN
    endif
    if not(this.oPgFrm.Page6.oPag.oDESAENTE_6_42.value==this.w_DESAENTE)
      this.oPgFrm.Page6.oPag.oDESAENTE_6_42.value=this.w_DESAENTE
    endif
    if not(this.oPgFrm.Page6.oPag.oMESE_6_45.value==this.w_MESE)
      this.oPgFrm.Page6.oPag.oMESE_6_45.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page6.oPag.oANNO_6_46.value==this.w_ANNO)
      this.oPgFrm.Page6.oPag.oANNO_6_46.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page7.oPag.oMFSERIAL_7_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page7.oPag.oMFSERIAL_7_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page7.oPag.oMFSALFIN_7_2.value==this.w_MFSALFIN)
      this.oPgFrm.Page7.oPag.oMFSALFIN_7_2.value=this.w_MFSALFIN
    endif
    if not(this.oPgFrm.Page7.oPag.oMESE_7_7.value==this.w_MESE)
      this.oPgFrm.Page7.oPag.oMESE_7_7.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page7.oPag.oANNO_7_8.value==this.w_ANNO)
      this.oPgFrm.Page7.oPag.oANNO_7_8.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page7.oPag.oMFDESSTA_7_11.value==this.w_MFDESSTA)
      this.oPgFrm.Page7.oPag.oMFDESSTA_7_11.value=this.w_MFDESSTA
    endif
    if not(this.oPgFrm.Page7.oPag.oCAP_7_16.value==this.w_CAP)
      this.oPgFrm.Page7.oPag.oCAP_7_16.value=this.w_CAP
    endif
    if not(this.oPgFrm.Page7.oPag.oLOCALI_7_17.value==this.w_LOCALI)
      this.oPgFrm.Page7.oPag.oLOCALI_7_17.value=this.w_LOCALI
    endif
    if not(this.oPgFrm.Page7.oPag.oPROVIN_7_18.value==this.w_PROVIN)
      this.oPgFrm.Page7.oPag.oPROVIN_7_18.value=this.w_PROVIN
    endif
    if not(this.oPgFrm.Page7.oPag.oINDIRI_7_19.value==this.w_INDIRI)
      this.oPgFrm.Page7.oPag.oINDIRI_7_19.value=this.w_INDIRI
    endif
    cp_SetControlsValueExtFlds(this,'MOD_PAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(not empty(.w_MFMESRIF) and val(.w_MFMESRIF)>= 1 and val(.w_MFMESRIF)<13)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMFMESRIF_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mese di riferimento non valido")
          case   not(not empty(.w_MFANNRIF) and val(.w_MFANNRIF)>1950 and val(.w_MFANNRIF)<2050)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMFANNRIF_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un anno compreso fra il 1950 e 2050")
          case   (empty(.w_MFCCONT1))  and (not empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT1_3_3.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_APP1A  Or  (!empty(.w_MFDAMES1) and 0<val(.w_MFDAMES1) and val(.w_MFDAMES1)<13))  and (not empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES1_3_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP1A  Or  (!empty(.w_MFDAANN1) and (val(.w_MFDAANN1)>=1996  and val(.w_MFDAANN1)<=2050)))  and (!empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN1_3_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(.w_APP1 Or (!empty(.w_MF_AMES1)  and 0<val(.w_MF_AMES1) and val(.w_MF_AMES1)<13))  and (!empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES1_3_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP1 Or (val(.w_MFAN1)<=2050 and(val(.w_MFDAANN1)<val(.w_MFAN1)or .w_MFDAANN1=.w_MFAN1 and val(.w_MFDAMES1)<=val(.w_MF_AMES1))))  and (!empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN1_3_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCONT2))  and (not empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT2_3_14.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_APP2A  Or  (!empty(.w_MFDAMES2) and 0<val(.w_MFDAMES2) and val(.w_MFDAMES2)<13))  and (not empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES2_3_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP2A  Or  (!empty(.w_MFDAANN2) and (val(.w_MFDAANN2)>=1996  and val(.w_MFDAANN2)<=2050)))  and (!empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN2_3_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(.w_APP2 Or (!empty(.w_MF_AMES2)  and 0<val(.w_MF_AMES2) and val(.w_MF_AMES2)<13))  and (!empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES2_3_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP2 Or (val(.w_MFAN2)<=2050 and(val(.w_MFDAANN2)<val(.w_MFAN2)or .w_MFDAANN2=.w_MFAN2 and val(.w_MFDAMES2)<=val(.w_MF_AMES2))))  and (!empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN2_3_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCONT3))  and (not empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT3_3_25.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_APP3A  Or  (!empty(.w_MFDAMES3) and 0<val(.w_MFDAMES3) and val(.w_MFDAMES3)<13))  and (not empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES3_3_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP3A  Or  (!empty(.w_MFDAANN3) and (val(.w_MFDAANN3)>=1996  and val(.w_MFDAANN3)<=2050)))  and (!empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN3_3_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(.w_APP3 Or (!empty(.w_MF_AMES3)  and 0<val(.w_MF_AMES3) and val(.w_MF_AMES3)<13))  and (!empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES3_3_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP3 Or (val(.w_MFAN3)<=2050 and(val(.w_MFDAANN3)<val(.w_MFAN3)or .w_MFDAANN3=.w_MFAN3 and val(.w_MFDAMES3)<=val(.w_MF_AMES3))))  and (!empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN3_3_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCONT4))  and (not empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT4_3_36.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT4)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_APP4A  Or  (!empty(.w_MFDAMES4) and 0<val(.w_MFDAMES4) and val(.w_MFDAMES4)<13))  and (not empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES4_3_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP4A  Or  (!empty(.w_MFDAANN4) and (val(.w_MFDAANN4)>=1996  and val(.w_MFDAANN4)<=2050)))  and (!empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN4_3_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(.w_APP4 Or (!empty(.w_MF_AMES4)  and 0<val(.w_MF_AMES4) and val(.w_MF_AMES4)<13))  and (!empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES4_3_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP4 Or (val(.w_MFAN4)<=2050 and(val(.w_MFDAANN4)<val(.w_MFAN4)or .w_MFDAANN4=.w_MFAN4 and val(.w_MFDAMES4)<=val(.w_MF_AMES4))))  and (!empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN4_3_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   ((empty(.w_MFTRIRE1)) or not(.w_TIPTRI='Regione' or empty(.w_TIPTRI)))  and (not empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE1_4_3.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
          case   not(empty(.w_MFRATRE1) or left(.w_MFRATRE1,2)<=right(.w_MFRATRE1,2) and len(alltrim(.w_MFRATRE1))=4)  and (not empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE1_4_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNRE1) and (val(.w_MFANNRE1)>=1996  and val(.w_MFANNRE1)<=2050  or val(.w_MFANNRE1)=0))  and (!empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE1_4_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   ((empty(.w_MFTRIRE2)) or not(.w_TIPTRI='Regione'  or empty(.w_TIPTRI)))  and (not empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE2_4_12.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
          case   not(empty(.w_MFRATRE2) or left(.w_MFRATRE2,2)<=right(.w_MFRATRE2,2) and len(alltrim(.w_MFRATRE2))=4)  and (not empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE2_4_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNRE2) and (val(.w_MFANNRE2)>=1996  and val(.w_MFANNRE2)<=2050   or val(.w_MFANNRE2)=0))  and (!empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE2_4_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   ((empty(.w_MFTRIRE3)) or not(.w_TIPTRI='Regione'   or empty(.w_TIPTRI)))  and (not empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE3_4_18.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE3)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
          case   not(empty(.w_MFRATRE3) or left(.w_MFRATRE3,2)<=right(.w_MFRATRE3,2) and len(alltrim(.w_MFRATRE3))=4)  and (not empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE3_4_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNRE3) and (val(.w_MFANNRE3)>=1996  and val(.w_MFANNRE3)<=2050  or val(.w_MFANNRE3)=0))  and (!empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE3_4_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   ((empty(.w_MFTRIRE4)) or not(.w_TIPTRI='Regione'  or empty(.w_TIPTRI)))  and (not empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE4_4_24.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE4)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
          case   not(empty(.w_MFRATRE4) or left(.w_MFRATRE4,2)<=right(.w_MFRATRE4,2) and len(alltrim(.w_MFRATRE4))=4)  and (not empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE4_4_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNRE4) and (val(.w_MFANNRE4)>=1996  and val(.w_MFANNRE4)<=2050   or val(.w_MFANNRE4)=0))  and (!empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE4_4_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFMSINE1) and 0< val(.w_MFMSINE1) and val(.w_MFMSINE1) < 13))  and (!empty(.w_MFCCOAE1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSINE1_6_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFANINE1) and (val(.w_MFANINE1)>=1996  and  val(.w_MFANINE1)<=2050)))  and (!empty(.w_MFCCOAE1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANINE1_6_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un anno compreso fra 1996 e 2050")
          case   not(alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFMSFIE1) and 0<val(.w_MFMSFIE1) and val(.w_MFMSFIE1)<13))  and (!empty(.w_MFCCOAE1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSFIE1_6_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFANF1)and((val(.w_MFANINE1)<val(.w_MFANF1)or .w_MFANINE1=.w_MFANF1 and val(.w_MFMSINE1)<=val(.w_MFMSFIE1)))))  and (!empty(.w_MFCCOAE1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANF1_6_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050")
          case   not(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFMSINE2) and 0<  val(.w_MFMSINE2) and val(.w_MFMSINE2) < 13))  and (!empty(.w_MFCCOAE2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSINE2_6_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFANINE2)  and (val(.w_MFANINE2)>=1996  and val(.w_MFANINE2)<=2050)))  and (!empty(.w_MFCCOAE2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANINE2_6_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un anno compreso fra 1996 e 2050")
          case   not(alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFMSFIE2)  and 0<val(.w_MFMSFIE2) and val(.w_MFMSFIE2)<13))  and (!empty(.w_MFCCOAE2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSFIE2_6_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFANF2)and((val(.w_MFANINE2)<val(.w_MFANF2)or .w_MFANINE2=.w_MFANF2 and val(.w_MFMSINE2)<=val(.w_MFMSFIE2)))))  and (!empty(.w_MFCCOAE2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANF2_6_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCG_ACF.CheckForm()
      if i_bres
        i_bres=  .GSCG_ACF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_AFQ.CheckForm()
      if i_bres
        i_bres=  .GSCG_AFQ.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_AVF.CheckForm()
      if i_bres
        i_bres=  .GSCG_AVF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=7
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_AIF.CheckForm()
      if i_bres
        i_bres=  .GSCG_AIF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gscg_aoo
      if i_bRes
        .w_RESCHK=0
        .NotifyEvent('ControllaDati')
        if .w_RESCHK<>0
          i_bRes=.f.
        endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MFSERIAL = this.w_MFSERIAL
    this.o_APPCOIN = this.w_APPCOIN
    this.o_MFCDSED1 = this.w_MFCDSED1
    this.o_MFCCONT1 = this.w_MFCCONT1
    this.o_APP1A = this.w_APP1A
    this.o_APP1 = this.w_APP1
    this.o_MFCDSED2 = this.w_MFCDSED2
    this.o_MFCCONT2 = this.w_MFCCONT2
    this.o_APP2A = this.w_APP2A
    this.o_APP2 = this.w_APP2
    this.o_MFCDSED3 = this.w_MFCDSED3
    this.o_MFCCONT3 = this.w_MFCCONT3
    this.o_APP3A = this.w_APP3A
    this.o_APP3 = this.w_APP3
    this.o_MFCDSED4 = this.w_MFCDSED4
    this.o_MFCCONT4 = this.w_MFCCONT4
    this.o_APP4A = this.w_APP4A
    this.o_APP4 = this.w_APP4
    this.o_MFCODRE1 = this.w_MFCODRE1
    this.o_MFTRIRE1 = this.w_MFTRIRE1
    this.o_MFCODRE2 = this.w_MFCODRE2
    this.o_MFTRIRE2 = this.w_MFTRIRE2
    this.o_MFCODRE3 = this.w_MFCODRE3
    this.o_MFTRIRE3 = this.w_MFTRIRE3
    this.o_MFCODRE4 = this.w_MFCODRE4
    this.o_MFTRIRE4 = this.w_MFTRIRE4
    this.o_MFSINAI1 = this.w_MFSINAI1
    this.o_MFSINAI2 = this.w_MFSINAI2
    this.o_MFSINAI3 = this.w_MFSINAI3
    this.o_MFCDENTE = this.w_MFCDENTE
    this.o_VRSDENT1 = this.w_VRSDENT1
    this.o_VPSDENT1 = this.w_VPSDENT1
    this.o_MFSDENT1 = this.w_MFSDENT1
    this.o_MFCCOAE1 = this.w_MFCCOAE1
    this.o_VPSDENT2 = this.w_VPSDENT2
    this.o_VRSDENT2 = this.w_VRSDENT2
    this.o_MFSDENT2 = this.w_MFSDENT2
    this.o_MFCCOAE2 = this.w_MFCCOAE2
    this.o_MFDESSTA = this.w_MFDESSTA
    * --- GSCG_ACF : Depends On
    this.GSCG_ACF.SaveDependsOn()
    * --- GSCG_AFQ : Depends On
    this.GSCG_AFQ.SaveDependsOn()
    * --- GSCG_AVF : Depends On
    this.GSCG_AVF.SaveDependsOn()
    * --- GSCG_AIF : Depends On
    this.GSCG_AIF.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_aooPag1 as StdContainer
  Width  = 822
  height = 419
  stdWidth  = 822
  stdheight = 419
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_1_1 as StdField with uid="INGQIDSUEM",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero modello",;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=130, Top=19, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFMESRIF_1_2 as StdField with uid="HFOOEBHDWN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MFMESRIF", cQueryName = "MFMESRIF",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Mese di riferimento non valido",;
    ToolTipText = "Mese di riferimento",;
    HelpContextID = 217052684,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=386, Top=19, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMESRIF_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_MFMESRIF) and val(.w_MFMESRIF)>= 1 and val(.w_MFMESRIF)<13)
    endwith
    return bRes
  endfunc

  add object oMFANNRIF_1_3 as StdField with uid="SPMPJCSSZI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MFANNRIF", cQueryName = "MFANNRIF",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un anno compreso fra il 1950 e 2050",;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 212350476,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=413, Top=19, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  proc oMFANNRIF_1_3.mAfter
    with this.Parent.oContained
      .w_MFANNRIF=yy2yyyy(.w_MFANNRIF)
    endwith
  endproc

  func oMFANNRIF_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_MFANNRIF) and val(.w_MFANNRIF)>1950 and val(.w_MFANNRIF)<2050)
    endwith
    return bRes
  endfunc


  add object oObj_1_5 as cp_runprogram with uid="UGGQSTDUFI",left=18, top=424, width=134,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BIM('N','ANN')",;
    cEvent = "New record",;
    nPag=1;
    , HelpContextID = 267263514

  add object oMF_COINC_1_7 as StdCheck with uid="RKATVGKMBB",rtseq=6,rtrep=.f.,left=658, top=51, caption="",;
    HelpContextID = 206629367,;
    cFormVar="w_MF_COINC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMF_COINC_1_7.RadioValue()
    return(iif(this.value =1,'N',;
    'S'))
  endfunc
  func oMF_COINC_1_7.GetRadio()
    this.Parent.oContained.w_MF_COINC = this.RadioValue()
    return .t.
  endfunc

  func oMF_COINC_1_7.SetRadio()
    this.Parent.oContained.w_MF_COINC=trim(this.Parent.oContained.w_MF_COINC)
    this.value = ;
      iif(this.Parent.oContained.w_MF_COINC=='N',1,;
      0)
  endfunc


  add object oLinkPC_1_8 as stdDynamicChildContainer with uid="VFEXZQDZFH",left=2, top=69, width=722, height=303, bOnScreen=.t.;


  add object oStr_1_9 as StdString with uid="RIMPGFGUUC",Visible=.t., Left=263, Top=51,;
    Alignment=1, Width=390, Height=15,;
    Caption="Anno di imposta non coincidente con anno solare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="EFRBFXDZBK",Visible=.t., Left=8, Top=19,;
    Alignment=1, Width=118, Height=15,;
    Caption="Numero modello:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="DNRJNKBPHF",Visible=.t., Left=227, Top=19,;
    Alignment=1, Width=154, Height=15,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="IWKPFCUQCR",Visible=.t., Left=488, Top=20,;
    Alignment=1, Width=124, Height=15,;
    Caption="Valuta di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="IZPIVVEYIL",Visible=.t., Left=617, Top=20,;
    Alignment=0, Width=34, Height=18,;
    Caption="Euro"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgscg_aooPag2 as StdContainer
  Width  = 822
  height = 419
  stdWidth  = 822
  stdheight = 419
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_2_1 as StdField with uid="ODPEQKOEGB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero modello",;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=146, Top=18, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)


  add object oLinkPC_2_2 as stdDynamicChildContainer with uid="LPQACJIYQL",left=5, top=44, width=714, height=294, bOnScreen=.t.;


  add object oMFCODUFF_2_4 as StdField with uid="RYJFAXKYGF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MFCODUFF", cQueryName = "MFCODUFF",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice ufficio",;
    HelpContextID = 252270092,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=100, Top=353, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CODI_UFF", cZoomOnZoom="GSCG_AUF", oKey_1_1="UFCODICE", oKey_1_2="this.w_MFCODUFF"

  func oMFCODUFF_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODUFF_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODUFF_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CODI_UFF','*','UFCODICE',cp_AbsName(this.parent,'oMFCODUFF_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AUF',"Codici ufficio",'',this.parent.oContained
  endproc
  proc oMFCODUFF_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AUF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UFCODICE=this.parent.oContained.w_MFCODUFF
     i_obj.ecpSave()
  endproc

  add object oMFCODATT_2_5 as StdField with uid="TNODRPVFFT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MFCODATT", cQueryName = "MFCODATT",;
    bObbl = .f. , nPag = 2, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice atto",;
    HelpContextID = 83274214,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=240, Top=353, cSayPict='"99999999999"', cGetPict='"99999999999"', InputMask=replicate('X',11)

  add object oMESE_2_8 as StdField with uid="KPWCJAHHDA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=370, Top=19, InputMask=replicate('X',2)

  add object oANNO_2_9 as StdField with uid="QEFDSTIJJN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=398, Top=19, InputMask=replicate('X',4)


  add object oObj_2_13 as cp_runprogram with uid="EVNNJKOTZA",left=13, top=444, width=189,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="Gscg_Bvi('I')",;
    cEvent = "Insert end,Update end",;
    nPag=2;
    , HelpContextID = 267263514


  add object oObj_2_15 as cp_runprogram with uid="EWTIJKBMUU",left=13, top=466, width=128,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="Gscg_Bvi('C')",;
    cEvent = "Delete end",;
    nPag=2;
    , HelpContextID = 267263514


  add object oBtn_2_16 as StdButton with uid="TWGRUZRJZH",left=732, top=365, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per visualizzare le distinte versamento di tipo I.R.PE.F.";
    , HelpContextID = 182357498;
    , tabstop=.f., caption='\<Distinte';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_16.Click()
      with this.Parent.oContained
        GSRI_BVI(this.Parent.oContained,"IR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_16.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_Rite<>'S')
     endwith
    endif
  endfunc

  add object oStr_2_6 as StdString with uid="DUVZRBXUKS",Visible=.t., Left=18, Top=19,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="EWLCKETTJZ",Visible=.t., Left=240, Top=20,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="NFEBJOXPXM",Visible=.t., Left=5, Top=356,;
    Alignment=1, Width=91, Height=15,;
    Caption="Codice ufficio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="LRITFRLPUR",Visible=.t., Left=157, Top=356,;
    Alignment=1, Width=79, Height=15,;
    Caption="Codice atto:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_afq",lower(this.oContained.GSCG_AFQ.class))=0
        this.oContained.GSCG_AFQ.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgscg_aooPag3 as StdContainer
  Width  = 822
  height = 419
  stdWidth  = 822
  stdheight = 419
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_3_1 as StdField with uid="UMTAXXYHVZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=148, Top=26, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFCDSED1_3_2 as StdField with uid="CKABJGIONW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MFCDSED1", cQueryName = "MFCDSED1",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 1157641,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=105, Top=131, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED1"

  func oMFCDSED1_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED1_3_2.ecpDrop(oSource)
    this.Parent.oContained.link_3_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED1_3_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED1_3_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED1_3_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED1
     i_obj.ecpSave()
  endproc

  add object oMFCCONT1_3_3 as StdField with uid="SPGALMZAEZ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MFCCONT1", cQueryName = "MFCCONT1",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 122857993,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=184, Top=131, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT1"

  func oMFCCONT1_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFCCONT1_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT1_3_3.ecpDrop(oSource)
    this.Parent.oContained.link_3_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT1_3_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT1_3_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT1_3_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT1
     i_obj.ecpSave()
  endproc

  add object oMFMINPS1_3_4 as StdField with uid="UZPQGAZZEX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MFMINPS1", cQueryName = "MFMINPS1",;
    bObbl = .f. , nPag = 3, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 89917961,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=266, Top=131, InputMask=replicate('X',17)

  func oMFMINPS1_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  add object oMFDAMES1_3_7 as StdField with uid="LNKWVLBIEA",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MFDAMES1", cQueryName = "MFDAMES1",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7641609,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=467, Top=131, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES1_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFDAMES1_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP1A  Or  (!empty(.w_MFDAMES1) and 0<val(.w_MFDAMES1) and val(.w_MFDAMES1)<13))
    endwith
    return bRes
  endfunc

  add object oMFDAANN1_3_8 as StdField with uid="AKJYRQPYXF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MFDAANN1", cQueryName = "MFDAANN1",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 137665033,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=494, Top=131, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN1_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFDAANN1_3_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP1A  Or  (!empty(.w_MFDAANN1) and (val(.w_MFDAANN1)>=1996  and val(.w_MFDAANN1)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMF_AMES1_3_9 as StdField with uid="UGNEEGCWGA",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MF_AMES1", cQueryName = "MF_AMES1",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7531017,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=543, Top=131, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES1_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMF_AMES1_3_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP1 Or (!empty(.w_MF_AMES1)  and 0<val(.w_MF_AMES1) and val(.w_MF_AMES1)<13))
    endwith
    return bRes
  endfunc

  add object oMFAN1_3_10 as StdField with uid="HMOIVJLGQJ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MFAN1", cQueryName = "MFAN1",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 148387270,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=572, Top=131, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN1_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFAN1_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP1 Or (val(.w_MFAN1)<=2050 and(val(.w_MFDAANN1)<val(.w_MFAN1)or .w_MFDAANN1=.w_MFAN1 and val(.w_MFDAMES1)<=val(.w_MF_AMES1))))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD1_3_11 as StdField with uid="TCTJSLGIBA",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MFIMPSD1", cQueryName = "MFIMPSD1",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243401,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=105, Top=272, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSD1_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  add object oMFIMPSC1_3_12 as StdField with uid="FPDWLHKRZP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MFIMPSC1", cQueryName = "MFIMPSC1",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243401,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=269, Top=272, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSC1_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  add object oMFCDSED2_3_13 as StdField with uid="SXZYRVNBKS",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MFCDSED2", cQueryName = "MFCDSED2",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 1157640,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=105, Top=155, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED2"

  func oMFCDSED2_3_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED2_3_13.ecpDrop(oSource)
    this.Parent.oContained.link_3_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED2_3_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED2_3_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED2_3_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED2
     i_obj.ecpSave()
  endproc

  add object oMFCCONT2_3_14 as StdField with uid="AVTFDROKIG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MFCCONT2", cQueryName = "MFCCONT2",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 122857992,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=184, Top=155, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT2"

  func oMFCCONT2_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFCCONT2_3_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT2_3_14.ecpDrop(oSource)
    this.Parent.oContained.link_3_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT2_3_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT2_3_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT2_3_14.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT2
     i_obj.ecpSave()
  endproc

  add object oMFMINPS2_3_15 as StdField with uid="TLRSSESNPI",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MFMINPS2", cQueryName = "MFMINPS2",;
    bObbl = .f. , nPag = 3, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 89917960,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=266, Top=155, InputMask=replicate('X',17)

  func oMFMINPS2_3_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  add object oMFDAMES2_3_17 as StdField with uid="HZGQCKOMWJ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_MFDAMES2", cQueryName = "MFDAMES2",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7641608,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=467, Top=155, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES2_3_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFDAMES2_3_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP2A  Or  (!empty(.w_MFDAMES2) and 0<val(.w_MFDAMES2) and val(.w_MFDAMES2)<13))
    endwith
    return bRes
  endfunc

  add object oMFDAANN2_3_18 as StdField with uid="WBAUDZABUC",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MFDAANN2", cQueryName = "MFDAANN2",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 137665032,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=494, Top=155, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN2_3_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFDAANN2_3_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP2A  Or  (!empty(.w_MFDAANN2) and (val(.w_MFDAANN2)>=1996  and val(.w_MFDAANN2)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMF_AMES2_3_20 as StdField with uid="XRSTGKETUN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_MF_AMES2", cQueryName = "MF_AMES2",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7531016,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=543, Top=155, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES2_3_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMF_AMES2_3_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP2 Or (!empty(.w_MF_AMES2)  and 0<val(.w_MF_AMES2) and val(.w_MF_AMES2)<13))
    endwith
    return bRes
  endfunc

  add object oMFAN2_3_21 as StdField with uid="LQXSVVFIUC",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MFAN2", cQueryName = "MFAN2",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 149435846,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=572, Top=155, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN2_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFAN2_3_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP2 Or (val(.w_MFAN2)<=2050 and(val(.w_MFDAANN2)<val(.w_MFAN2)or .w_MFDAANN2=.w_MFAN2 and val(.w_MFDAMES2)<=val(.w_MF_AMES2))))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD2_3_22 as StdField with uid="DPPNTFIXZF",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MFIMPSD2", cQueryName = "MFIMPSD2",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243400,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=105, Top=295, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSD2_3_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  add object oMFIMPSC2_3_23 as StdField with uid="DKBHHCVJQC",rtseq=37,rtrep=.f.,;
    cFormVar = "w_MFIMPSC2", cQueryName = "MFIMPSC2",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243400,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=269, Top=295, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSC2_3_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  add object oMFCDSED3_3_24 as StdField with uid="PMUMNAHIUI",rtseq=38,rtrep=.f.,;
    cFormVar = "w_MFCDSED3", cQueryName = "MFCDSED3",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 1157639,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=105, Top=179, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED3"

  func oMFCDSED3_3_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED3_3_24.ecpDrop(oSource)
    this.Parent.oContained.link_3_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED3_3_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED3_3_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED3_3_24.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED3
     i_obj.ecpSave()
  endproc

  add object oMFCCONT3_3_25 as StdField with uid="MUKHFMMIGC",rtseq=39,rtrep=.f.,;
    cFormVar = "w_MFCCONT3", cQueryName = "MFCCONT3",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 122857991,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=184, Top=179, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT3"

  func oMFCCONT3_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFCCONT3_3_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT3_3_25.ecpDrop(oSource)
    this.Parent.oContained.link_3_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT3_3_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT3_3_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT3_3_25.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT3
     i_obj.ecpSave()
  endproc

  add object oMFMINPS3_3_26 as StdField with uid="CPVWKREMYW",rtseq=40,rtrep=.f.,;
    cFormVar = "w_MFMINPS3", cQueryName = "MFMINPS3",;
    bObbl = .f. , nPag = 3, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 89917959,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=266, Top=179, InputMask=replicate('X',17)

  func oMFMINPS3_3_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  add object oMFDAMES3_3_28 as StdField with uid="UCGDBCLFJK",rtseq=42,rtrep=.f.,;
    cFormVar = "w_MFDAMES3", cQueryName = "MFDAMES3",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7641607,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=467, Top=179, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES3_3_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFDAMES3_3_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP3A  Or  (!empty(.w_MFDAMES3) and 0<val(.w_MFDAMES3) and val(.w_MFDAMES3)<13))
    endwith
    return bRes
  endfunc

  add object oMFDAANN3_3_30 as StdField with uid="PYVGITCREO",rtseq=44,rtrep=.f.,;
    cFormVar = "w_MFDAANN3", cQueryName = "MFDAANN3",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 137665031,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=494, Top=179, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN3_3_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFDAANN3_3_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP3A  Or  (!empty(.w_MFDAANN3) and (val(.w_MFDAANN3)>=1996  and val(.w_MFDAANN3)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMF_AMES3_3_31 as StdField with uid="QTOOIBGIKI",rtseq=45,rtrep=.f.,;
    cFormVar = "w_MF_AMES3", cQueryName = "MF_AMES3",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7531015,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=543, Top=179, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES3_3_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMF_AMES3_3_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP3 Or (!empty(.w_MF_AMES3)  and 0<val(.w_MF_AMES3) and val(.w_MF_AMES3)<13))
    endwith
    return bRes
  endfunc

  add object oMFAN3_3_32 as StdField with uid="FWZATATEDM",rtseq=46,rtrep=.f.,;
    cFormVar = "w_MFAN3", cQueryName = "MFAN3",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 150484422,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=572, Top=179, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN3_3_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFAN3_3_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP3 Or (val(.w_MFAN3)<=2050 and(val(.w_MFDAANN3)<val(.w_MFAN3)or .w_MFDAANN3=.w_MFAN3 and val(.w_MFDAMES3)<=val(.w_MF_AMES3))))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD3_3_33 as StdField with uid="NRTFEMYREN",rtseq=47,rtrep=.f.,;
    cFormVar = "w_MFIMPSD3", cQueryName = "MFIMPSD3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243399,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=105, Top=318, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSD3_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  add object oMFIMPSC3_3_34 as StdField with uid="RMAERXBXYN",rtseq=48,rtrep=.f.,;
    cFormVar = "w_MFIMPSC3", cQueryName = "MFIMPSC3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243399,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=269, Top=318, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSC3_3_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  add object oMFCDSED4_3_35 as StdField with uid="RENIINYJRG",rtseq=49,rtrep=.f.,;
    cFormVar = "w_MFCDSED4", cQueryName = "MFCDSED4",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 1157638,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=105, Top=203, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED4"

  func oMFCDSED4_3_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED4_3_35.ecpDrop(oSource)
    this.Parent.oContained.link_3_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED4_3_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED4_3_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED4_3_35.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED4
     i_obj.ecpSave()
  endproc

  add object oMFCCONT4_3_36 as StdField with uid="ZYWRGKWSCK",rtseq=50,rtrep=.f.,;
    cFormVar = "w_MFCCONT4", cQueryName = "MFCCONT4",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 122857990,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=184, Top=203, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT4"

  func oMFCCONT4_3_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFCCONT4_3_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT4_3_36.ecpDrop(oSource)
    this.Parent.oContained.link_3_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT4_3_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT4_3_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT4_3_36.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT4
     i_obj.ecpSave()
  endproc

  add object oMFMINPS4_3_37 as StdField with uid="YUDKXKBRDK",rtseq=51,rtrep=.f.,;
    cFormVar = "w_MFMINPS4", cQueryName = "MFMINPS4",;
    bObbl = .f. , nPag = 3, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 89917958,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=266, Top=203, InputMask=replicate('X',17)

  func oMFMINPS4_3_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  add object oMFDAMES4_3_39 as StdField with uid="IUDRVUKOUP",rtseq=53,rtrep=.f.,;
    cFormVar = "w_MFDAMES4", cQueryName = "MFDAMES4",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7641606,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=467, Top=203, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES4_3_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFDAMES4_3_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP4A  Or  (!empty(.w_MFDAMES4) and 0<val(.w_MFDAMES4) and val(.w_MFDAMES4)<13))
    endwith
    return bRes
  endfunc

  add object oMFDAANN4_3_41 as StdField with uid="HJXJWHWGKV",rtseq=55,rtrep=.f.,;
    cFormVar = "w_MFDAANN4", cQueryName = "MFDAANN4",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 137665030,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=494, Top=203, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN4_3_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFDAANN4_3_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP4A  Or  (!empty(.w_MFDAANN4) and (val(.w_MFDAANN4)>=1996  and val(.w_MFDAANN4)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMF_AMES4_3_42 as StdField with uid="RGSVDGJKVT",rtseq=56,rtrep=.f.,;
    cFormVar = "w_MF_AMES4", cQueryName = "MF_AMES4",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7531014,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=543, Top=203, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES4_3_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMF_AMES4_3_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP4 Or (!empty(.w_MF_AMES4)  and 0<val(.w_MF_AMES4) and val(.w_MF_AMES4)<13))
    endwith
    return bRes
  endfunc

  add object oMFAN4_3_43 as StdField with uid="CFHZNDFFLJ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_MFAN4", cQueryName = "MFAN4",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 151532998,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=572, Top=203, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN4_3_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFAN4_3_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP4 Or (val(.w_MFAN4)<=2050 and(val(.w_MFDAANN4)<val(.w_MFAN4)or .w_MFDAANN4=.w_MFAN4 and val(.w_MFDAMES4)<=val(.w_MF_AMES4))))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD4_3_44 as StdField with uid="MNPGVFYFDQ",rtseq=58,rtrep=.f.,;
    cFormVar = "w_MFIMPSD4", cQueryName = "MFIMPSD4",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243398,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=105, Top=341, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSD4_3_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  add object oMFIMPSC4_3_45 as StdField with uid="UYJTGTJJGS",rtseq=59,rtrep=.f.,;
    cFormVar = "w_MFIMPSC4", cQueryName = "MFIMPSC4",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243398,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=269, Top=341, cSayPict="'@Z '+ v_GV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSC4_3_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  add object oMFTOTDPS_3_55 as StdField with uid="XAUJQDXFLK",rtseq=60,rtrep=.f.,;
    cFormVar = "w_MFTOTDPS", cQueryName = "MFTOTDPS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 16095719,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=105, Top=367, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFTOTCPS_3_56 as StdField with uid="MGBYMLHXPA",rtseq=61,rtrep=.f.,;
    cFormVar = "w_MFTOTCPS", cQueryName = "MFTOTCPS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 32872935,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=269, Top=367, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFSALDPS_3_57 as StdField with uid="JXAPRSKVXL",rtseq=62,rtrep=.f.,;
    cFormVar = "w_MFSALDPS", cQueryName = "MFSALDPS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25405927,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=413, Top=367, cSayPict="v_PV(78)", cGetPict="v_GV(78)"

  add object oMESE_3_63 as StdField with uid="BKZRGNZFUB",rtseq=63,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=383, Top=26, InputMask=replicate('X',2)

  add object oANNO_3_64 as StdField with uid="MJMYMEQJVT",rtseq=64,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=410, Top=26, InputMask=replicate('X',4)


  add object oBtn_3_67 as StdButton with uid="LBKQCEWXSG",left=745, top=357, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per visualizzare le distinte versamento di tipo I.N.P.S.";
    , HelpContextID = 182357498;
    , tabstop=.f., caption='\<Distinte';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_67.Click()
      with this.Parent.oContained
        GSRI_BVI(this.Parent.oContained,"IN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_67.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_Rite<>'S')
     endwith
    endif
  endfunc


  add object oObj_3_68 as cp_calclbl with uid="XFLZEJFPVI",left=105, top=86, width=62,height=35,;
    caption='Codice sede',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=3;
    , HelpContextID = 249804567


  add object oObj_3_69 as cp_calclbl with uid="YEBTDQCIIE",left=184, top=86, width=60,height=35,;
    caption='Causale contributo',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=3;
    , HelpContextID = 123970517

  add object oStr_3_46 as StdString with uid="IYEELTZMSP",Visible=.t., Left=263, Top=86,;
    Alignment=2, Width=159, Height=18,;
    Caption="Matricola INPS\Codice INPS\"  ;
  , bGlobalFont=.t.

  add object oStr_3_47 as StdString with uid="YEXOAXJKLC",Visible=.t., Left=284, Top=105,;
    Alignment=2, Width=104, Height=15,;
    Caption="Filiale azienda"  ;
  , bGlobalFont=.t.

  add object oStr_3_48 as StdString with uid="MFUFBDQZKN",Visible=.t., Left=476, Top=87,;
    Alignment=2, Width=123, Height=15,;
    Caption="Periodo di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_3_49 as StdString with uid="BOQOXBLNLK",Visible=.t., Left=452, Top=104,;
    Alignment=2, Width=85, Height=18,;
    Caption="da mm\aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_3_50 as StdString with uid="YZBSWXWFTX",Visible=.t., Left=544, Top=104,;
    Alignment=2, Width=68, Height=18,;
    Caption="a mm\aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_3_51 as StdString with uid="SYTXHRSHZV",Visible=.t., Left=105, Top=249,;
    Alignment=0, Width=154, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_3_52 as StdString with uid="HOACILSNNJ",Visible=.t., Left=267, Top=249,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_3_53 as StdString with uid="LGLKGDJLHM",Visible=.t., Left=7, Top=367,;
    Alignment=1, Width=94, Height=15,;
    Caption="Totale C"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_54 as StdString with uid="TZCTEEYMBI",Visible=.t., Left=248, Top=368,;
    Alignment=0, Width=15, Height=14,;
    Caption="D"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_58 as StdString with uid="XBSXKBBESY",Visible=.t., Left=413, Top=343,;
    Alignment=2, Width=132, Height=15,;
    Caption="Saldo (C-D)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_59 as StdString with uid="ASFKUCRSCQ",Visible=.t., Left=20, Top=26,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_61 as StdString with uid="KPNHKKCGVH",Visible=.t., Left=16, Top=52,;
    Alignment=0, Width=111, Height=18,;
    Caption="Sezione INPS"  ;
  , bGlobalFont=.t.

  add object oStr_3_62 as StdString with uid="IZXDXVWWJG",Visible=.t., Left=252, Top=28,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oBox_3_60 as StdBox with uid="DBOPZLDODS",left=13, top=73, width=766,height=1
enddefine
define class tgscg_aooPag4 as StdContainer
  Width  = 822
  height = 419
  stdWidth  = 822
  stdheight = 419
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_4_1 as StdField with uid="WUJQAZLJAD",rtseq=67,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=134, Top=8, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFCODRE1_4_2 as StdField with uid="AIZVRMHYSY",rtseq=68,rtrep=.f.,;
    cFormVar = "w_MFCODRE1", cQueryName = "MFCODRE1",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 201938423,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=41, Left=26, Top=99, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE1"

  func oMFCODRE1_4_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE1_4_2.ecpDrop(oSource)
    this.Parent.oContained.link_4_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE1_4_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE1_4_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regione",'',this.parent.oContained
  endproc
  proc oMFCODRE1_4_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE1
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE1_4_3 as StdField with uid="EOPPRNCXED",rtseq=69,rtrep=.f.,;
    cFormVar = "w_MFTRIRE1", cQueryName = "MFTRIRE1",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'regione'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 207447543,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=99, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE1"

  func oMFTRIRE1_4_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFTRIRE1_4_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE1_4_3.ecpDrop(oSource)
    this.Parent.oContained.link_4_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE1_4_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE1_4_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE1_4_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE1
     i_obj.ecpSave()
  endproc

  add object oMFRATRE1_4_4 as StdField with uid="WJSZNHKTPL",rtseq=70,rtrep=.f.,;
    cFormVar = "w_MFRATRE1", cQueryName = "MFRATRE1",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 217859575,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=161, Top=99, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE1_4_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFRATRE1_4_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE1) or left(.w_MFRATRE1,2)<=right(.w_MFRATRE1,2) and len(alltrim(.w_MFRATRE1))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNRE1_4_5 as StdField with uid="MUARMFNAEO",rtseq=71,rtrep=.f.,;
    cFormVar = "w_MFANNRE1", cQueryName = "MFANNRE1",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 212350455,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=238, Top=99, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE1_4_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFANNRE1_4_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE1) and (val(.w_MFANNRE1)>=1996  and val(.w_MFANNRE1)<=2050  or val(.w_MFANNRE1)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE1_4_6 as StdField with uid="DHWEMFLHFR",rtseq=72,rtrep=.f.,;
    cFormVar = "w_MFIMDRE1", cQueryName = "MFIMDRE1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201831927,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=301, Top=99, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDRE1_4_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  add object oMFIMCRE1_4_7 as StdField with uid="UKNUQBICLX",rtseq=73,rtrep=.f.,;
    cFormVar = "w_MFIMCRE1", cQueryName = "MFIMCRE1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 200783351,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=463, Top=99, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCRE1_4_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  add object oMFCODRE2_4_11 as StdField with uid="ZYXFOZVGTH",rtseq=74,rtrep=.f.,;
    cFormVar = "w_MFCODRE2", cQueryName = "MFCODRE2",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 201938424,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=26, Top=122, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE2"

  func oMFCODRE2_4_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE2_4_11.ecpDrop(oSource)
    this.Parent.oContained.link_4_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE2_4_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE2_4_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regione",'',this.parent.oContained
  endproc
  proc oMFCODRE2_4_11.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE2
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE2_4_12 as StdField with uid="FKBLLTEVWV",rtseq=75,rtrep=.f.,;
    cFormVar = "w_MFTRIRE2", cQueryName = "MFTRIRE2",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'regione'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 207447544,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=122, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE2"

  func oMFTRIRE2_4_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFTRIRE2_4_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE2_4_12.ecpDrop(oSource)
    this.Parent.oContained.link_4_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE2_4_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE2_4_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE2_4_12.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE2
     i_obj.ecpSave()
  endproc

  add object oMFRATRE2_4_13 as StdField with uid="UQVLTAPSHS",rtseq=76,rtrep=.f.,;
    cFormVar = "w_MFRATRE2", cQueryName = "MFRATRE2",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 217859576,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=161, Top=122, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE2_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFRATRE2_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE2) or left(.w_MFRATRE2,2)<=right(.w_MFRATRE2,2) and len(alltrim(.w_MFRATRE2))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNRE2_4_14 as StdField with uid="HEIMJNJWRR",rtseq=77,rtrep=.f.,;
    cFormVar = "w_MFANNRE2", cQueryName = "MFANNRE2",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 212350456,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=238, Top=122, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE2_4_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFANNRE2_4_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE2) and (val(.w_MFANNRE2)>=1996  and val(.w_MFANNRE2)<=2050   or val(.w_MFANNRE2)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE2_4_15 as StdField with uid="XOVRSESBNM",rtseq=78,rtrep=.f.,;
    cFormVar = "w_MFIMDRE2", cQueryName = "MFIMDRE2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201831928,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=301, Top=122, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDRE2_4_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  add object oMFIMCRE2_4_16 as StdField with uid="RCLUETTAKO",rtseq=79,rtrep=.f.,;
    cFormVar = "w_MFIMCRE2", cQueryName = "MFIMCRE2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 200783352,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=463, Top=122, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCRE2_4_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  add object oMFCODRE3_4_17 as StdField with uid="ZVBRNRSCQE",rtseq=80,rtrep=.f.,;
    cFormVar = "w_MFCODRE3", cQueryName = "MFCODRE3",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 201938425,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=26, Top=145, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE3"

  func oMFCODRE3_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE3_4_17.ecpDrop(oSource)
    this.Parent.oContained.link_4_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE3_4_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE3_4_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regione",'',this.parent.oContained
  endproc
  proc oMFCODRE3_4_17.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE3
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE3_4_18 as StdField with uid="YCEJPTYMRK",rtseq=81,rtrep=.f.,;
    cFormVar = "w_MFTRIRE3", cQueryName = "MFTRIRE3",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'regione'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 207447545,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=145, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE3"

  func oMFTRIRE3_4_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFTRIRE3_4_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE3_4_18.ecpDrop(oSource)
    this.Parent.oContained.link_4_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE3_4_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE3_4_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE3_4_18.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE3
     i_obj.ecpSave()
  endproc

  add object oMFRATRE3_4_19 as StdField with uid="TWMNLHYLWV",rtseq=82,rtrep=.f.,;
    cFormVar = "w_MFRATRE3", cQueryName = "MFRATRE3",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 217859577,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=161, Top=145, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE3_4_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFRATRE3_4_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE3) or left(.w_MFRATRE3,2)<=right(.w_MFRATRE3,2) and len(alltrim(.w_MFRATRE3))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNRE3_4_20 as StdField with uid="SUHDLNHBSQ",rtseq=83,rtrep=.f.,;
    cFormVar = "w_MFANNRE3", cQueryName = "MFANNRE3",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 212350457,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=238, Top=145, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE3_4_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFANNRE3_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE3) and (val(.w_MFANNRE3)>=1996  and val(.w_MFANNRE3)<=2050  or val(.w_MFANNRE3)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE3_4_21 as StdField with uid="CYOULTQZVW",rtseq=84,rtrep=.f.,;
    cFormVar = "w_MFIMDRE3", cQueryName = "MFIMDRE3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201831929,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=301, Top=145, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDRE3_4_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  add object oMFIMCRE3_4_22 as StdField with uid="QQOFTRRZLZ",rtseq=85,rtrep=.f.,;
    cFormVar = "w_MFIMCRE3", cQueryName = "MFIMCRE3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 200783353,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=463, Top=145, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCRE3_4_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  add object oMFCODRE4_4_23 as StdField with uid="MBZNNKESOD",rtseq=86,rtrep=.f.,;
    cFormVar = "w_MFCODRE4", cQueryName = "MFCODRE4",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 201938426,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=26, Top=168, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE4"

  func oMFCODRE4_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE4_4_23.ecpDrop(oSource)
    this.Parent.oContained.link_4_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE4_4_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE4_4_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regione",'',this.parent.oContained
  endproc
  proc oMFCODRE4_4_23.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE4
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE4_4_24 as StdField with uid="MJAPDUZSJV",rtseq=87,rtrep=.f.,;
    cFormVar = "w_MFTRIRE4", cQueryName = "MFTRIRE4",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'regione'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 207447546,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=168, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE4"

  func oMFTRIRE4_4_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFTRIRE4_4_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE4_4_24.ecpDrop(oSource)
    this.Parent.oContained.link_4_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE4_4_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE4_4_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE4_4_24.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE4
     i_obj.ecpSave()
  endproc

  add object oMFRATRE4_4_25 as StdField with uid="NDAJMIEPRZ",rtseq=88,rtrep=.f.,;
    cFormVar = "w_MFRATRE4", cQueryName = "MFRATRE4",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 217859578,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=161, Top=168, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE4_4_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFRATRE4_4_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE4) or left(.w_MFRATRE4,2)<=right(.w_MFRATRE4,2) and len(alltrim(.w_MFRATRE4))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNRE4_4_26 as StdField with uid="BIEFIFIRQX",rtseq=89,rtrep=.f.,;
    cFormVar = "w_MFANNRE4", cQueryName = "MFANNRE4",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 212350458,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=238, Top=168, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE4_4_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFANNRE4_4_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE4) and (val(.w_MFANNRE4)>=1996  and val(.w_MFANNRE4)<=2050   or val(.w_MFANNRE4)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE4_4_27 as StdField with uid="BCINHDFTDU",rtseq=90,rtrep=.f.,;
    cFormVar = "w_MFIMDRE4", cQueryName = "MFIMDRE4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201831930,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=301, Top=168, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDRE4_4_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  add object oMFIMCRE4_4_28 as StdField with uid="GJHJZPGKWU",rtseq=91,rtrep=.f.,;
    cFormVar = "w_MFIMCRE4", cQueryName = "MFIMCRE4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 200783354,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=463, Top=168, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCRE4_4_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  add object oMFTOTDRE_4_32 as StdField with uid="RWVYQDEOZY",rtseq=92,rtrep=.f.,;
    cFormVar = "w_MFTOTDRE", cQueryName = "MFTOTDRE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 16095733,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=301, Top=196, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFTOTCRE_4_33 as StdField with uid="NEFFOASBPH",rtseq=93,rtrep=.f.,;
    cFormVar = "w_MFTOTCRE", cQueryName = "MFTOTCRE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 32872949,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=463, Top=196, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFSALDRE_4_34 as StdField with uid="ZPSTXWEYRU",rtseq=94,rtrep=.f.,;
    cFormVar = "w_MFSALDRE", cQueryName = "MFSALDRE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25405941,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=605, Top=196, cSayPict="v_PV(78)", cGetPict="v_GV(78)"

  add object oMESE_4_39 as StdField with uid="QDHEDGHHIW",rtseq=95,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=371, Top=9, InputMask=replicate('X',2)

  add object oANNO_4_40 as StdField with uid="EBJMOQWTXY",rtseq=96,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=398, Top=9, InputMask=replicate('X',4)


  add object oLinkPC_4_41 as stdDynamicChildContainer with uid="IJXOOZHGBN",left=2, top=220, width=818, height=198, bOnScreen=.t.;



  add object oObj_4_44 as cp_calclbl with uid="GKGLWDZFVF",left=234, top=63, width=64,height=35,;
    caption='Anno di riferimento',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=4;
    , HelpContextID = 58759029


  add object oObj_4_45 as cp_calclbl with uid="ZUOHGMEAFA",left=87, top=63, width=52,height=35,;
    caption='Cod. tributo',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=4;
    , HelpContextID = 64927313


  add object oObj_4_46 as cp_calclbl with uid="KSGOLPXVPB",left=26, top=63, width=47,height=35,;
    caption='Codice regione',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=4;
    , HelpContextID = 225151976

  add object oStr_4_8 as StdString with uid="YQGQUJLBET",Visible=.t., Left=142, Top=78,;
    Alignment=0, Width=89, Height=15,;
    Caption="Rateazione"  ;
  , bGlobalFont=.t.

  add object oStr_4_9 as StdString with uid="MVBKBDZKBM",Visible=.t., Left=301, Top=76,;
    Alignment=0, Width=154, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_4_10 as StdString with uid="NHHSLKGDGU",Visible=.t., Left=461, Top=76,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_4_29 as StdString with uid="KEBVMVBZNJ",Visible=.t., Left=202, Top=198,;
    Alignment=1, Width=93, Height=15,;
    Caption="Totale E"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_30 as StdString with uid="FIWKHDGIJY",Visible=.t., Left=605, Top=171,;
    Alignment=2, Width=132, Height=15,;
    Caption="Saldo (E-F)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_31 as StdString with uid="WDIGBVHLVI",Visible=.t., Left=442, Top=197,;
    Alignment=0, Width=18, Height=16,;
    Caption="F"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_36 as StdString with uid="WRQXUQEJZJ",Visible=.t., Left=6, Top=9,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_37 as StdString with uid="XBBOUQJYEG",Visible=.t., Left=230, Top=11,;
    Alignment=1, Width=136, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_38 as StdString with uid="DORQAGMARM",Visible=.t., Left=6, Top=31,;
    Alignment=0, Width=109, Height=18,;
    Caption="Sezione regioni"  ;
  , bGlobalFont=.t.

  add object oBox_4_35 as StdBox with uid="KHJPYHBFQV",left=3, top=55, width=778,height=1
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_aif",lower(this.oContained.GSCG_AIF.class))=0
        this.oContained.GSCG_AIF.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgscg_aooPag5 as StdContainer
  Width  = 822
  height = 419
  stdWidth  = 822
  stdheight = 419
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_5_1 as StdField with uid="UHIVYKYYLY",rtseq=97,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=141, Top=24, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFSINAI1_5_2 as StdField with uid="VJCJNRLRZM",rtseq=98,rtrep=.f.,;
    cFormVar = "w_MFSINAI1", cQueryName = "MFSINAI1",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INAIL",;
    HelpContextID = 195319287,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=9, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SE_INAIL", cZoomOnZoom="GSCG_ASI", oKey_1_1="SICODICE", oKey_1_2="this.w_MFSINAI1"

  func oMFSINAI1_5_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSINAI1_5_2.ecpDrop(oSource)
    this.Parent.oContained.link_5_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSINAI1_5_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SE_INAIL','*','SICODICE',cp_AbsName(this.parent,'oMFSINAI1_5_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ASI',"Codici sede INAIL",'',this.parent.oContained
  endproc
  proc oMFSINAI1_5_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ASI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SICODICE=this.parent.oContained.w_MFSINAI1
     i_obj.ecpSave()
  endproc

  add object oMF_NPOS1_5_3 as StdField with uid="IOZFIFMCFH",rtseq=99,rtrep=.f.,;
    cFormVar = "w_MF_NPOS1", cQueryName = "MF_NPOS1",nZero=8,;
    bObbl = .f. , nPag = 5, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Numero posizione assicurativa",;
    HelpContextID = 104196617,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=90, Top=173, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8)

  func oMF_NPOS1_5_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMF_PACC1_5_4 as StdField with uid="ZRBNXSIDVP",rtseq=100,rtrep=.f.,;
    cFormVar = "w_MF_PACC1", cQueryName = "MF_PACC1",nZero=2,;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "C.C. posizione assicurativa",;
    HelpContextID = 52685321,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=162, Top=173, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_PACC1_5_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMF_NRIF1_5_5 as StdField with uid="MNCHMZYNTX",rtseq=101,rtrep=.f.,;
    cFormVar = "w_MF_NRIF1", cQueryName = "MF_NRIF1",nZero=6,;
    bObbl = .f. , nPag = 5, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero di riferimento",;
    HelpContextID = 65672695,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=200, Top=173, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',6)

  func oMF_NRIF1_5_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFCAUSA1_5_6 as StdField with uid="UYLFQZKBVP",rtseq=102,rtrep=.f.,;
    cFormVar = "w_MFCAUSA1", cQueryName = "MFCAUSA1",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Causale",;
    HelpContextID = 32811529,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=287, Top=173, cSayPict='repl("!",1)', cGetPict='repl("!",1)', InputMask=replicate('X',2)

  func oMFCAUSA1_5_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFIMDIL1_5_7 as StdField with uid="KHMXTBZGND",rtseq=103,rtrep=.f.,;
    cFormVar = "w_MFIMDIL1", cQueryName = "MFIMDIL1",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 50836983,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=339, Top=173, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDIL1_5_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFIMCIL1_5_8 as StdField with uid="LCWCGXKFZS",rtseq=104,rtrep=.f.,;
    cFormVar = "w_MFIMCIL1", cQueryName = "MFIMCIL1",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 49788407,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=502, Top=173, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCIL1_5_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFSINAI2_5_9 as StdField with uid="CRJUFFVUOV",rtseq=105,rtrep=.f.,;
    cFormVar = "w_MFSINAI2", cQueryName = "MFSINAI2",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INAIL",;
    HelpContextID = 195319288,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=9, Top=197, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SE_INAIL", cZoomOnZoom="GSCG_ASI", oKey_1_1="SICODICE", oKey_1_2="this.w_MFSINAI2"

  func oMFSINAI2_5_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSINAI2_5_9.ecpDrop(oSource)
    this.Parent.oContained.link_5_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSINAI2_5_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SE_INAIL','*','SICODICE',cp_AbsName(this.parent,'oMFSINAI2_5_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ASI',"Codici sede INAIL",'',this.parent.oContained
  endproc
  proc oMFSINAI2_5_9.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ASI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SICODICE=this.parent.oContained.w_MFSINAI2
     i_obj.ecpSave()
  endproc

  add object oMF_NPOS2_5_10 as StdField with uid="FUEQFYRIPH",rtseq=106,rtrep=.f.,;
    cFormVar = "w_MF_NPOS2", cQueryName = "MF_NPOS2",nZero=8,;
    bObbl = .f. , nPag = 5, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Numero posizione assicurativa",;
    HelpContextID = 104196616,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=90, Top=197, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8)

  func oMF_NPOS2_5_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMF_PACC2_5_11 as StdField with uid="TUSBHCGAVC",rtseq=107,rtrep=.f.,;
    cFormVar = "w_MF_PACC2", cQueryName = "MF_PACC2",nZero=2,;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "C.C. posizione assicurativa",;
    HelpContextID = 52685320,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=162, Top=197, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_PACC2_5_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMF_NRIF2_5_12 as StdField with uid="DJFDGNQMDU",rtseq=108,rtrep=.f.,;
    cFormVar = "w_MF_NRIF2", cQueryName = "MF_NRIF2",nZero=6,;
    bObbl = .f. , nPag = 5, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero di riferimento",;
    HelpContextID = 65672696,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=200, Top=197, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',6)

  func oMF_NRIF2_5_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFCAUSA2_5_13 as StdField with uid="HNWHAKLLNT",rtseq=109,rtrep=.f.,;
    cFormVar = "w_MFCAUSA2", cQueryName = "MFCAUSA2",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Causale",;
    HelpContextID = 32811528,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=287, Top=197, cSayPict='repl("!",1)', cGetPict='repl("!",1)', InputMask=replicate('X',2)

  func oMFCAUSA2_5_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFIMDIL2_5_14 as StdField with uid="KEXJJEVGQG",rtseq=110,rtrep=.f.,;
    cFormVar = "w_MFIMDIL2", cQueryName = "MFIMDIL2",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 50836984,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=339, Top=197, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDIL2_5_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFIMCIL2_5_15 as StdField with uid="HAECOJYPAK",rtseq=111,rtrep=.f.,;
    cFormVar = "w_MFIMCIL2", cQueryName = "MFIMCIL2",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 49788408,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=502, Top=197, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCIL2_5_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFSINAI3_5_16 as StdField with uid="XBUILCQVSF",rtseq=112,rtrep=.f.,;
    cFormVar = "w_MFSINAI3", cQueryName = "MFSINAI3",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INAIL",;
    HelpContextID = 195319289,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=9, Top=221, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SE_INAIL", cZoomOnZoom="GSCG_ASI", oKey_1_1="SICODICE", oKey_1_2="this.w_MFSINAI3"

  func oMFSINAI3_5_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSINAI3_5_16.ecpDrop(oSource)
    this.Parent.oContained.link_5_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSINAI3_5_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SE_INAIL','*','SICODICE',cp_AbsName(this.parent,'oMFSINAI3_5_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ASI',"Codici sede INAIL",'',this.parent.oContained
  endproc
  proc oMFSINAI3_5_16.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ASI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SICODICE=this.parent.oContained.w_MFSINAI3
     i_obj.ecpSave()
  endproc

  add object oMF_NPOS3_5_17 as StdField with uid="KYNIYEUWFN",rtseq=113,rtrep=.f.,;
    cFormVar = "w_MF_NPOS3", cQueryName = "MF_NPOS3",nZero=8,;
    bObbl = .f. , nPag = 5, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Numero posizione assicurativa",;
    HelpContextID = 104196615,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=90, Top=221, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8)

  func oMF_NPOS3_5_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMF_PACC3_5_18 as StdField with uid="RMJFRMESCX",rtseq=114,rtrep=.f.,;
    cFormVar = "w_MF_PACC3", cQueryName = "MF_PACC3",nZero=2,;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "C.C. posizione assicurativa",;
    HelpContextID = 52685319,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=162, Top=221, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_PACC3_5_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMF_NRIF3_5_19 as StdField with uid="PAMPHMQGFW",rtseq=115,rtrep=.f.,;
    cFormVar = "w_MF_NRIF3", cQueryName = "MF_NRIF3",nZero=6,;
    bObbl = .f. , nPag = 5, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero di riferimento",;
    HelpContextID = 65672697,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=200, Top=221, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',6)

  func oMF_NRIF3_5_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFCAUSA3_5_20 as StdField with uid="BWTVBLXRGC",rtseq=116,rtrep=.f.,;
    cFormVar = "w_MFCAUSA3", cQueryName = "MFCAUSA3",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Causale",;
    HelpContextID = 32811527,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=287, Top=221, cSayPict='repl("!",1)', cGetPict='repl("!",1)', InputMask=replicate('X',2)

  func oMFCAUSA3_5_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFIMDIL3_5_21 as StdField with uid="ZSLZCWMSTL",rtseq=117,rtrep=.f.,;
    cFormVar = "w_MFIMDIL3", cQueryName = "MFIMDIL3",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 50836985,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=339, Top=221, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDIL3_5_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFIMCIL3_5_22 as StdField with uid="RWHHSNNRDK",rtseq=118,rtrep=.f.,;
    cFormVar = "w_MFIMCIL3", cQueryName = "MFIMCIL3",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 49788409,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=502, Top=221, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCIL3_5_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFTDINAI_5_30 as StdField with uid="PSBGPKLIAA",rtseq=119,rtrep=.f.,;
    cFormVar = "w_MFTDINAI", cQueryName = "MFTDINAI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 129014257,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=339, Top=250, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFTCINAI_5_31 as StdField with uid="XTGTLEVZXZ",rtseq=120,rtrep=.f.,;
    cFormVar = "w_MFTCINAI", cQueryName = "MFTCINAI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 129079793,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=502, Top=250, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFSALINA_5_32 as StdField with uid="DZWWWGTXFX",rtseq=121,rtrep=.f.,;
    cFormVar = "w_MFSALINA", cQueryName = "MFSALINA",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 209955321,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=641, Top=250, cSayPict="v_PV(78)", cGetPict="v_GV(78)"

  add object oMESE_5_40 as StdField with uid="MLVBGKPOHZ",rtseq=122,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=379, Top=25, InputMask=replicate('X',2)

  add object oANNO_5_41 as StdField with uid="JTXVHPAOPB",rtseq=123,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=408, Top=25, InputMask=replicate('X',4)


  add object oObj_5_42 as cp_calclbl with uid="EXHYHMEIKF",left=200, top=135, width=73,height=35,;
    caption='Numero di riferimento',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=5;
    , HelpContextID = 255703043

  add object oStr_5_23 as StdString with uid="TZTHCVPSZI",Visible=.t., Left=97, Top=151,;
    Alignment=2, Width=51, Height=15,;
    Caption="Numero"  ;
  , bGlobalFont=.t.

  add object oStr_5_24 as StdString with uid="FGFSRZKDQK",Visible=.t., Left=162, Top=153,;
    Alignment=0, Width=24, Height=15,;
    Caption="C.C."  ;
  , bGlobalFont=.t.

  add object oStr_5_25 as StdString with uid="XMJKQNQCQQ",Visible=.t., Left=339, Top=153,;
    Alignment=0, Width=154, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_5_26 as StdString with uid="QPZQHUEURM",Visible=.t., Left=502, Top=153,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_5_27 as StdString with uid="YCITJFFMQZ",Visible=.t., Left=7, Top=151,;
    Alignment=0, Width=71, Height=15,;
    Caption="Cod.sede"  ;
  , bGlobalFont=.t.

  add object oStr_5_28 as StdString with uid="DUQPKICCWN",Visible=.t., Left=69, Top=135,;
    Alignment=2, Width=109, Height=15,;
    Caption="Posiz.assicurativa"  ;
  , bGlobalFont=.t.

  add object oStr_5_29 as StdString with uid="GWKFEEEWGD",Visible=.t., Left=285, Top=153,;
    Alignment=0, Width=45, Height=15,;
    Caption="Caus."  ;
  , bGlobalFont=.t.

  add object oStr_5_33 as StdString with uid="CAXNCWDVNP",Visible=.t., Left=243, Top=252,;
    Alignment=1, Width=89, Height=15,;
    Caption="Totale I"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_34 as StdString with uid="YQYWHXKWQX",Visible=.t., Left=641, Top=228,;
    Alignment=2, Width=132, Height=15,;
    Caption="Saldo (I-L)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_35 as StdString with uid="HTBHHGCOXH",Visible=.t., Left=480, Top=252,;
    Alignment=0, Width=20, Height=15,;
    Caption="L"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_36 as StdString with uid="UICRRZTZSD",Visible=.t., Left=12, Top=63,;
    Alignment=0, Width=311, Height=18,;
    Caption="Altri enti previdenziali ed assicurativi - INAIL"  ;
  , bGlobalFont=.t.

  add object oStr_5_38 as StdString with uid="VXPRYYUHZU",Visible=.t., Left=13, Top=25,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_39 as StdString with uid="HJKKMLLUEV",Visible=.t., Left=247, Top=26,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oBox_5_37 as StdBox with uid="DLSNHYHKXJ",left=12, top=86, width=769,height=1
enddefine
define class tgscg_aooPag6 as StdContainer
  Width  = 822
  height = 419
  stdWidth  = 822
  stdheight = 419
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_6_1 as StdField with uid="BOVCFDHJYO",rtseq=124,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=131, Top=26, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFCDENTE_6_3 as StdField with uid="ZPTITZNOZB",rtseq=125,rtrep=.f.,;
    cFormVar = "w_MFCDENTE", cQueryName = "MFCDENTE",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente previdenziale",;
    HelpContextID = 133278197,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=114, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_PREV", cZoomOnZoom="GSCG_ACP", oKey_1_1="CPCODICE", oKey_1_2="this.w_MFCDENTE"

  func oMFCDENTE_6_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_3('Part',this)
      if .not. empty(.w_VRSDENT1)
        bRes2=.link_6_4('Full')
      endif
      if .not. empty(.w_MFCCOAE1)
        bRes2=.link_6_7('Full')
      endif
      if .not. empty(.w_VRSDENT2)
        bRes2=.link_6_17('Full')
      endif
      if .not. empty(.w_MFCCOAE2)
        bRes2=.link_6_19('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMFCDENTE_6_3.ecpDrop(oSource)
    this.Parent.oContained.link_6_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDENTE_6_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_PREV','*','CPCODICE',cp_AbsName(this.parent,'oMFCDENTE_6_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACP',"Codici enti previdenziali",'',this.parent.oContained
  endproc
  proc oMFCDENTE_6_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CPCODICE=this.parent.oContained.w_MFCDENTE
     i_obj.ecpSave()
  endproc

  add object oVRSDENT1_6_4 as StdField with uid="RWNSAXJAAF",rtseq=126,rtrep=.f.,;
    cFormVar = "w_VRSDENT1", cQueryName = "VRSDENT1",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 133209465,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=473, Top=144, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_AEN", cZoomOnZoom="GSCG_MSE", oKey_1_1="SECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="SECODSED", oKey_2_2="this.w_VRSDENT1"

  func oVRSDENT1_6_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oVRSDENT1_6_4.mHide()
    with this.Parent.oContained
      return (.w_MFCDENTE='0003')
    endwith
  endfunc

  func oVRSDENT1_6_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oVRSDENT1_6_4.ecpDrop(oSource)
    this.Parent.oContained.link_6_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVRSDENT1_6_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SED_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'SED_AEN','*','SECODENT,SECODSED',cp_AbsName(this.parent,'oVRSDENT1_6_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MSE',"Codici sede",'',this.parent.oContained
  endproc
  proc oVRSDENT1_6_4.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MSE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SECODENT=w_MFCDENTE
     i_obj.w_SECODSED=this.parent.oContained.w_VRSDENT1
     i_obj.ecpSave()
  endproc

  add object oVPSDENT1_6_5 as StdField with uid="VVYWAPAEPT",rtseq=127,rtrep=.f.,;
    cFormVar = "w_VPSDENT1", cQueryName = "VPSDENT1",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice provincia",;
    HelpContextID = 133209977,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=473, Top=145, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ANAG_PRO", cZoomOnZoom="GSAR_APR", oKey_1_1="PRCODPRO", oKey_1_2="this.w_VPSDENT1"

  func oVPSDENT1_6_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oVPSDENT1_6_5.mHide()
    with this.Parent.oContained
      return (.w_MFCDENTE<>'0003')
    endwith
  endfunc

  func oVPSDENT1_6_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPSDENT1_6_5.ecpDrop(oSource)
    this.Parent.oContained.link_6_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPSDENT1_6_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ANAG_PRO','*','PRCODPRO',cp_AbsName(this.parent,'oVPSDENT1_6_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APR',"Elenco province",'',this.parent.oContained
  endproc
  proc oVPSDENT1_6_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PRCODPRO=this.parent.oContained.w_VPSDENT1
     i_obj.ecpSave()
  endproc

  add object oMFCCOAE1_6_7 as StdField with uid="QKCNTKXASE",rtseq=129,rtrep=.f.,;
    cFormVar = "w_MFCCOAE1", cQueryName = "MFCCOAE1",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo",;
    HelpContextID = 72526345,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=544, Top=144, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_AEN", cZoomOnZoom="GSCG_MAE", oKey_1_1="AECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="AECAUSEN", oKey_2_2="this.w_MFCCOAE1"

  func oMFCCOAE1_6_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oMFCCOAE1_6_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCOAE1_6_7.ecpDrop(oSource)
    this.Parent.oContained.link_6_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCOAE1_6_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAU_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(this.parent,'oMFCCOAE1_6_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MAE',"Causali contributo",'',this.parent.oContained
  endproc
  proc oMFCCOAE1_6_7.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MAE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.AECODENT=w_MFCDENTE
     i_obj.w_AECAUSEN=this.parent.oContained.w_MFCCOAE1
     i_obj.ecpSave()
  endproc

  add object oMFCDPOS1_6_9 as StdField with uid="FVFMMSXZYE",rtseq=131,rtrep=.f.,;
    cFormVar = "w_MFCDPOS1", cQueryName = "MFCDPOS1",nZero=9,;
    bObbl = .f. , nPag = 6, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "Codice posizione",;
    HelpContextID = 104966665,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=55, Top=247, cSayPict='"999999999"', cGetPict='"999999999"', InputMask=replicate('X',9)

  func oMFCDPOS1_6_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCCOAE1))
    endwith
   endif
  endfunc

  add object oMFMSINE1_6_10 as StdField with uid="CQUCVMEFND",rtseq=132,rtrep=.f.,;
    cFormVar = "w_MFMSINE1", cQueryName = "MFMSINE1",nZero=2,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 140375543,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=146, Top=247, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSINE1_6_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE1))
    endwith
   endif
  endfunc

  func oMFMSINE1_6_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE1) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFMSINE1) and 0< val(.w_MFMSINE1) and val(.w_MFMSINE1) < 13))
    endwith
    return bRes
  endfunc

  add object oMFANINE1_6_11 as StdField with uid="BSCZHMAMEK",rtseq=133,rtrep=.f.,;
    cFormVar = "w_MFANINE1", cQueryName = "MFANINE1",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 139998711,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=174, Top=247, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANINE1_6_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE1))
    endwith
   endif
  endfunc

  func oMFANINE1_6_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE1) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFANINE1) and (val(.w_MFANINE1)>=1996  and  val(.w_MFANINE1)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMFMSFIE1_6_12 as StdField with uid="NJWCQBUUWE",rtseq=134,rtrep=.f.,;
    cFormVar = "w_MFMSFIE1", cQueryName = "MFMSFIE1",nZero=2,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 53343735,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=224, Top=247, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSFIE1_6_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE1))
    endwith
   endif
  endfunc

  func oMFMSFIE1_6_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFMSFIE1) and 0<val(.w_MFMSFIE1) and val(.w_MFMSFIE1)<13))
    endwith
    return bRes
  endfunc

  add object oMFANF1_6_13 as StdField with uid="SCAIVPDBNL",rtseq=135,rtrep=.f.,;
    cFormVar = "w_MFANF1", cQueryName = "MFANF1",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 81250874,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=252, Top=247, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANF1_6_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE1))
    endwith
   endif
  endfunc

  func oMFANF1_6_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFANF1)and((val(.w_MFANINE1)<val(.w_MFANF1)or .w_MFANINE1=.w_MFANF1 and val(.w_MFMSINE1)<=val(.w_MFMSFIE1)))))
    endwith
    return bRes
  endfunc

  add object oMFIMDAE1_6_14 as StdField with uid="SVMKLLZVBI",rtseq=136,rtrep=.f.,;
    cFormVar = "w_MFIMDAE1", cQueryName = "MFIMDAE1",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 83380745,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=304, Top=247, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDAE1_6_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE1))
    endwith
   endif
  endfunc

  add object oMFIMCAE1_6_15 as StdField with uid="WGFGYVXVVM",rtseq=137,rtrep=.f.,;
    cFormVar = "w_MFIMCAE1", cQueryName = "MFIMCAE1",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 84429321,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=466, Top=247, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCAE1_6_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE1) and .w_MFCDENTE<>'0003')
    endwith
   endif
  endfunc

  add object oVPSDENT2_6_16 as StdField with uid="XMQFIJZISL",rtseq=138,rtrep=.f.,;
    cFormVar = "w_VPSDENT2", cQueryName = "VPSDENT2",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice provincia",;
    HelpContextID = 133209976,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=473, Top=169, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ANAG_PRO", cZoomOnZoom="GSAR_APR", oKey_1_1="PRCODPRO", oKey_1_2="this.w_VPSDENT2"

  func oVPSDENT2_6_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oVPSDENT2_6_16.mHide()
    with this.Parent.oContained
      return (.w_MFCDENTE<>'0003')
    endwith
  endfunc

  func oVPSDENT2_6_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPSDENT2_6_16.ecpDrop(oSource)
    this.Parent.oContained.link_6_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPSDENT2_6_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ANAG_PRO','*','PRCODPRO',cp_AbsName(this.parent,'oVPSDENT2_6_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APR',"Elenco province",'',this.parent.oContained
  endproc
  proc oVPSDENT2_6_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PRCODPRO=this.parent.oContained.w_VPSDENT2
     i_obj.ecpSave()
  endproc

  add object oVRSDENT2_6_17 as StdField with uid="LOAJBDQCPZ",rtseq=139,rtrep=.f.,;
    cFormVar = "w_VRSDENT2", cQueryName = "VRSDENT2",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 133209464,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=473, Top=168, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_AEN", cZoomOnZoom="GSCG_MSE", oKey_1_1="SECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="SECODSED", oKey_2_2="this.w_VRSDENT2"

  func oVRSDENT2_6_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oVRSDENT2_6_17.mHide()
    with this.Parent.oContained
      return (.w_MFCDENTE='0003')
    endwith
  endfunc

  func oVRSDENT2_6_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oVRSDENT2_6_17.ecpDrop(oSource)
    this.Parent.oContained.link_6_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVRSDENT2_6_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SED_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'SED_AEN','*','SECODENT,SECODSED',cp_AbsName(this.parent,'oVRSDENT2_6_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MSE',"Codici sede",'',this.parent.oContained
  endproc
  proc oVRSDENT2_6_17.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MSE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SECODENT=w_MFCDENTE
     i_obj.w_SECODSED=this.parent.oContained.w_VRSDENT2
     i_obj.ecpSave()
  endproc

  add object oMFCCOAE2_6_19 as StdField with uid="NOTHDZQOTX",rtseq=141,rtrep=.f.,;
    cFormVar = "w_MFCCOAE2", cQueryName = "MFCCOAE2",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo",;
    HelpContextID = 72526344,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=544, Top=168, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_AEN", cZoomOnZoom="GSCG_MAE", oKey_1_1="AECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="AECAUSEN", oKey_2_2="this.w_MFCCOAE2"

  func oMFCCOAE2_6_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oMFCCOAE2_6_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCOAE2_6_19.ecpDrop(oSource)
    this.Parent.oContained.link_6_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCOAE2_6_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAU_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(this.parent,'oMFCCOAE2_6_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MAE',"Causali contributo",'',this.parent.oContained
  endproc
  proc oMFCCOAE2_6_19.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MAE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.AECODENT=w_MFCDENTE
     i_obj.w_AECAUSEN=this.parent.oContained.w_MFCCOAE2
     i_obj.ecpSave()
  endproc

  add object oMFCDPOS2_6_21 as StdField with uid="YTUJHCWFYN",rtseq=143,rtrep=.f.,;
    cFormVar = "w_MFCDPOS2", cQueryName = "MFCDPOS2",nZero=9,;
    bObbl = .f. , nPag = 6, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "Codice posizione",;
    HelpContextID = 104966664,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=55, Top=272, cSayPict='"999999999"', cGetPict='"999999999"', InputMask=replicate('X',9)

  func oMFCDPOS2_6_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCCOAE2))
    endwith
   endif
  endfunc

  add object oMFMSINE2_6_22 as StdField with uid="SHSFTEZLCU",rtseq=144,rtrep=.f.,;
    cFormVar = "w_MFMSINE2", cQueryName = "MFMSINE2",nZero=2,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 140375544,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=146, Top=272, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSINE2_6_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE2))
    endwith
   endif
  endfunc

  func oMFMSINE2_6_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE2) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFMSINE2) and 0<  val(.w_MFMSINE2) and val(.w_MFMSINE2) < 13))
    endwith
    return bRes
  endfunc

  add object oMFANINE2_6_23 as StdField with uid="MCAXPQUTQA",rtseq=145,rtrep=.f.,;
    cFormVar = "w_MFANINE2", cQueryName = "MFANINE2",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 139998712,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=174, Top=272, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANINE2_6_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE2))
    endwith
   endif
  endfunc

  func oMFANINE2_6_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE2) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFANINE2)  and (val(.w_MFANINE2)>=1996  and val(.w_MFANINE2)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMFMSFIE2_6_24 as StdField with uid="YMMTXOEUQT",rtseq=146,rtrep=.f.,;
    cFormVar = "w_MFMSFIE2", cQueryName = "MFMSFIE2",nZero=2,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 53343736,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=224, Top=272, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSFIE2_6_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE2))
    endwith
   endif
  endfunc

  func oMFMSFIE2_6_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFMSFIE2)  and 0<val(.w_MFMSFIE2) and val(.w_MFMSFIE2)<13))
    endwith
    return bRes
  endfunc

  add object oMFANF2_6_25 as StdField with uid="CXTDVEYWGH",rtseq=147,rtrep=.f.,;
    cFormVar = "w_MFANF2", cQueryName = "MFANF2",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 64473658,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=252, Top=272, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANF2_6_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE2))
    endwith
   endif
  endfunc

  func oMFANF2_6_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFANF2)and((val(.w_MFANINE2)<val(.w_MFANF2)or .w_MFANINE2=.w_MFANF2 and val(.w_MFMSINE2)<=val(.w_MFMSFIE2)))))
    endwith
    return bRes
  endfunc

  add object oMFIMDAE2_6_26 as StdField with uid="JSQAGPZNHA",rtseq=148,rtrep=.f.,;
    cFormVar = "w_MFIMDAE2", cQueryName = "MFIMDAE2",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 83380744,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=304, Top=272, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDAE2_6_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE2))
    endwith
   endif
  endfunc

  add object oMFIMCAE2_6_27 as StdField with uid="NZPTLZBXME",rtseq=149,rtrep=.f.,;
    cFormVar = "w_MFIMCAE2", cQueryName = "MFIMCAE2",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 84429320,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=466, Top=272, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCAE2_6_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE2) and .w_MFCDENTE<>'0003')
    endwith
   endif
  endfunc

  add object oMFTDAENT_6_35 as StdField with uid="UBZHZRCDWP",rtseq=150,rtrep=.f.,;
    cFormVar = "w_MFTDAENT", cQueryName = "MFTDAENT",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 19962342,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=304, Top=318, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFTCAENT_6_36 as StdField with uid="VGJIBWUKLL",rtseq=151,rtrep=.f.,;
    cFormVar = "w_MFTCAENT", cQueryName = "MFTCAENT",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 20027878,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=466, Top=318, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFSALAEN_6_37 as StdField with uid="NWKWVKNQGD",rtseq=152,rtrep=.f.,;
    cFormVar = "w_MFSALAEN", cQueryName = "MFSALAEN",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 75737580,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=615, Top=318, cSayPict="v_PV(78)", cGetPict="v_GV(78)"

  add object oDESAENTE_6_42 as StdField with uid="MHNJWSROFX",rtseq=153,rtrep=.f.,;
    cFormVar = "w_DESAENTE", cQueryName = "DESAENTE",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 133409669,;
   bGlobalFont=.t.,;
    Height=21, Width=185, Left=271, Top=114, InputMask=replicate('X',30)

  add object oMESE_6_45 as StdField with uid="BPSADYMFGH",rtseq=154,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=360, Top=27, InputMask=replicate('X',2)

  add object oANNO_6_46 as StdField with uid="XFUVSGDJBX",rtseq=155,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=387, Top=27, InputMask=replicate('X',4)


  add object oBtn_6_47 as StdButton with uid="FSUHCTGKMA",left=768, top=370, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per visualizzare le distinte versamento di tipo previdenziali";
    , HelpContextID = 182357498;
    , tabstop=.f., caption='\<Distinte';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_47.Click()
      with this.Parent.oContained
        GSRI_BVI(this.Parent.oContained,"PR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MFCDENTE))
      endwith
    endif
  endfunc

  func oBtn_6_47.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_Rite<>'S')
     endwith
    endif
  endfunc


  add object oObj_6_51 as cp_runprogram with uid="IMVHWAQFRK",left=13, top=442, width=143,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BBF('MP')",;
    cEvent = "ControllaDati",;
    nPag=6;
    , HelpContextID = 267263514


  add object oObj_6_52 as cp_calclbl with uid="HDNGOFJORS",left=544, top=108, width=62,height=35,;
    caption='Causale contributo',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=6;
    , HelpContextID = 123970517


  add object oObj_6_53 as cp_calclbl with uid="ZMENAVJMVQ",left=473, top=108, width=62,height=35,;
    caption='Codice sede\provincia',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=6;
    , HelpContextID = 16248303


  add object oObj_6_54 as cp_calclbl with uid="KQGEIDDWST",left=55, top=211, width=76,height=35,;
    caption='Codice posizione',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=6;
    , HelpContextID = 119970027

  add object oStr_6_2 as StdString with uid="XUFRHUWVCA",Visible=.t., Left=12, Top=115,;
    Alignment=1, Width=92, Height=18,;
    Caption="Codice ente:"  ;
  , bGlobalFont=.t.

  add object oStr_6_28 as StdString with uid="QDZVJIFSLW",Visible=.t., Left=151, Top=205,;
    Alignment=2, Width=123, Height=15,;
    Caption="Periodo di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_6_29 as StdString with uid="WFWPRZSWDC",Visible=.t., Left=137, Top=224,;
    Alignment=2, Width=85, Height=18,;
    Caption="da mm\aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_6_30 as StdString with uid="UMJFKCLLIF",Visible=.t., Left=225, Top=224,;
    Alignment=2, Width=68, Height=18,;
    Caption="a mm\aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_6_31 as StdString with uid="SAKCHWIMZS",Visible=.t., Left=302, Top=227,;
    Alignment=0, Width=154, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_6_32 as StdString with uid="ISMZUWQKMW",Visible=.t., Left=462, Top=227,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_6_33 as StdString with uid="TXOVKRQMIL",Visible=.t., Left=202, Top=320,;
    Alignment=1, Width=96, Height=15,;
    Caption="Totale M"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_34 as StdString with uid="ZHSIEZUFVM",Visible=.t., Left=445, Top=320,;
    Alignment=0, Width=18, Height=15,;
    Caption="N"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_38 as StdString with uid="LTURFGEZTI",Visible=.t., Left=615, Top=293,;
    Alignment=2, Width=132, Height=15,;
    Caption="Saldo (M-N)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_39 as StdString with uid="DDIXCLCYBG",Visible=.t., Left=4, Top=27,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_40 as StdString with uid="CTXZAWNSVI",Visible=.t., Left=12, Top=67,;
    Alignment=0, Width=285, Height=18,;
    Caption="Altri enti previdenziali ed assicurativi"  ;
  , bGlobalFont=.t.

  add object oStr_6_43 as StdString with uid="RVEZJNDSJX",Visible=.t., Left=182, Top=114,;
    Alignment=1, Width=84, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_6_44 as StdString with uid="JWKSCNPXLY",Visible=.t., Left=229, Top=28,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oBox_6_41 as StdBox with uid="OSEQIEOXBC",left=13, top=89, width=761,height=1
enddefine
define class tgscg_aooPag7 as StdContainer
  Width  = 822
  height = 419
  stdWidth  = 822
  stdheight = 419
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_7_1 as StdField with uid="FWNKVSYQIO",rtseq=159,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=138, Top=26, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFSALFIN_7_2 as StdField with uid="XPRIIKCUAV",rtseq=160,rtrep=.f.,;
    cFormVar = "w_MFSALFIN", cQueryName = "MFSALFIN",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 8148500,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=126, Left=577, Top=28, cSayPict="v_PV(78)", cGetPict="v_GV(78)"


  add object oLinkPC_7_3 as stdDynamicChildContainer with uid="CDRKTLRCVN",left=5, top=73, width=788, height=243, bOnScreen=.t.;


  add object oMESE_7_7 as StdField with uid="VBABMPRSRE",rtseq=161,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=364, Top=27, InputMask=replicate('X',2)

  add object oANNO_7_8 as StdField with uid="DRPQPJTNKA",rtseq=162,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=392, Top=27, InputMask=replicate('X',4)


  add object oObj_7_10 as cp_runprogram with uid="EAXHMSXTSY",left=6, top=447, width=134,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BAK('N','ANN')",;
    cEvent = "New record",;
    nPag=7;
    , HelpContextID = 267263514

  add object oMFDESSTA_7_11 as StdField with uid="ZXJMGMFFSC",rtseq=167,rtrep=.f.,;
    cFormVar = "w_MFDESSTA", cQueryName = "MFDESSTA",;
    bObbl = .f. , nPag = 7, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della sede aziendale predefinita quale destinataria di stampa attestazione di pagamento modello F24",;
    HelpContextID = 34642425,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=195, Top=334, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SEDIAZIE", cZoomOnZoom="GSAR_ASE", oKey_1_1="SECODAZI", oKey_1_2="this.w_CODAZIE", oKey_2_1="SECODDES", oKey_2_2="this.w_MFDESSTA"

  func oMFDESSTA_7_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFDESSTA_7_11.ecpDrop(oSource)
    this.Parent.oContained.link_7_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFDESSTA_7_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SEDIAZIE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZIE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZIE)
    endif
    do cp_zoom with 'SEDIAZIE','*','SECODAZI,SECODDES',cp_AbsName(this.parent,'oMFDESSTA_7_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASE',"Sedi aziende",'GSAR_ASI.SEDIAZIE_VZM',this.parent.oContained
  endproc
  proc oMFDESSTA_7_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SECODAZI=w_CODAZIE
     i_obj.w_SECODDES=this.parent.oContained.w_MFDESSTA
     i_obj.ecpSave()
  endproc

  add object oCAP_7_16 as StdField with uid="EHGCBYPICB",rtseq=172,rtrep=.f.,;
    cFormVar = "w_CAP", cQueryName = "CAP",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "CAP della sede aziendale predefinita quale destinataria di stampa attestazione di pagamento modello F24",;
    HelpContextID = 91955238,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=334, Top=334, InputMask=replicate('X',9)

  add object oLOCALI_7_17 as StdField with uid="YJQQJNWEDS",rtseq=173,rtrep=.f.,;
    cFormVar = "w_LOCALI", cQueryName = "LOCALI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� della sede aziendale predefinita quale destinataria di stampa attestazione di pagamento modello F24",;
    HelpContextID = 210018634,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=481, Top=334, InputMask=replicate('X',30)

  add object oPROVIN_7_18 as StdField with uid="KKBGTKSRJE",rtseq=174,rtrep=.f.,;
    cFormVar = "w_PROVIN", cQueryName = "PROVIN",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia della sede aziendale predefinita quale destinataria di stampa attestazione di pagamento modello F24",;
    HelpContextID = 127852042,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=195, Top=363, InputMask=replicate('X',2)

  add object oINDIRI_7_19 as StdField with uid="LLDGSFXWVI",rtseq=175,rtrep=.f.,;
    cFormVar = "w_INDIRI", cQueryName = "INDIRI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede aziendale predefinita quale destinataria di stampa attestazione di pagamento modello F24",;
    HelpContextID = 203199098,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=334, Top=363, InputMask=replicate('X',35)


  add object oBtn_7_27 as StdButton with uid="CJUEHBMWXH",left=714, top=368, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=7;
    , ToolTipText = "Premere per salvare i risultati e stampare il modello";
    , HelpContextID = 91630310;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_27.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_7_28 as StdButton with uid="VSFOQMDZOF",left=766, top=368, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=7;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 98927174;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_28.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_7_29 as cp_runprogram with uid="GFQIHJVWOZ",left=160, top=447, width=251,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BMF",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=7;
    , HelpContextID = 267263514

  add object oStr_7_4 as StdString with uid="VKEGRLLTGP",Visible=.t., Left=457, Top=31,;
    Alignment=1, Width=113, Height=15,;
    Caption="SALDO FINALE:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_5 as StdString with uid="BYCBSRREFL",Visible=.t., Left=9, Top=28,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_6 as StdString with uid="SCVOUDRHJZ",Visible=.t., Left=234, Top=29,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_7_20 as StdString with uid="YNXKDJTXGM",Visible=.t., Left=264, Top=334,;
    Alignment=1, Width=66, Height=18,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_7_21 as StdString with uid="TSJBINZCHU",Visible=.t., Left=415, Top=334,;
    Alignment=1, Width=60, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_7_22 as StdString with uid="GGEVHAPSLD",Visible=.t., Left=106, Top=363,;
    Alignment=1, Width=86, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_7_23 as StdString with uid="JLVIFDUYJQ",Visible=.t., Left=7, Top=334,;
    Alignment=1, Width=185, Height=18,;
    Caption="Sede destinatario di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_7_24 as StdString with uid="GNVKOPQKBS",Visible=.t., Left=273, Top=363,;
    Alignment=1, Width=57, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oBox_7_9 as StdBox with uid="LIMWEMRJGT",left=5, top=68, width=774,height=2
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_avf",lower(this.oContained.GSCG_AVF.class))=0
        this.oContained.GSCG_AVF.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_aoo','MOD_PAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MFSERIAL=MOD_PAG.MFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
