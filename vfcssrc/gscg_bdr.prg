* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bdr                                                        *
*              Controlli dati ritenute                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_15]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-06                                                      *
* Last revis.: 2005-09-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bdr",oParentObject)
return(i_retval)

define class tgscg_bdr as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_DOPPIO = space(15)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Utilizzo il temporaneo del figlio Ritenute per capire se � stato inserito
    *     un codice tributo ripetuto. 
    this.w_PADRE = This.oparentobject
    if this.w_PADRE.nDeferredFillRec=0
      * --- Solo se cursore significativo caso esc con due righe contributo uguali
      this.w_PADRE.MarkPos()     
      this.w_PADRE.Exec_Select("ELENCO","Count(*) As CONTA, t_DRCODTRI "," t_DRCODTRI Is Not Null "+" And Not Deleted() ","","t_DRCODTRI","CONTA>=2")     
      this.w_DOPPIO = Nvl ( Elenco.t_DRCODTRI , "" )
       
 Select ELENCO 
 Use
      if Not Empty ( this.w_DOPPIO )
        ah_ErrorMsg("Codice tributo %1 ripetuto",,"",ALLTRIM(this.w_DOPPIO))
        this.oParentObject.w_RESCHK = 1
      endif
      this.w_PADRE.RePos()     
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
