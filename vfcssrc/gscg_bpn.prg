* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bpn                                                        *
*              Stampa movimenti contabili                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_93]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-25                                                      *
* Last revis.: 2000-05-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bpn",oParentObject)
return(i_retval)

define class tgscg_bpn as StdBatch
  * --- Local variables
  w_UTENTE = 0
  * --- WorkFile variables
  CONTI_idx=0
  MASTRI_idx=0
  OUTPUTMP_idx=0
  TMP_PART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Stampa Movimenti Contabili (da GSCG_SPN)
    this.w_UTENTE = this.oParentObject.w_UTENTE1
    * --- Seleziono, applicando i principali filtri, e contrassegno le registrazioni di primanota
    * --- Seleziono le registrazioni di primanota
    * --- Create temporary table OUTPUTMP
    i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSCGASPN',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.OUTPUTMP_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Contrassegno le registrazioni che non quadrano (FLQUADRA='S')
    * --- Write into OUTPUTMP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PNSERIAL"
      do vq_exec with 'QUERY\GSCGQSPN',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OUTPUTMP_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLQUADRA ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLQUADRA');
          +i_ccchkf;
          +" from "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 set ";
      +"OUTPUTMP.FLQUADRA ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLQUADRA');
          +Iif(Empty(i_ccchkf),"",",OUTPUTMP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OUTPUTMP.PNSERIAL = t2.PNSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set (";
          +"FLQUADRA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLQUADRA')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set ";
      +"FLQUADRA ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLQUADRA');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLQUADRA ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLQUADRA');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Contrassegno le registrazioni che non hanno la causale di riga (FLCAURIG='S')
    * --- Write into OUTPUTMP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PNSERIAL,CPROWNUM"
      do vq_exec with 'QUERY\GSCGBSPN',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OUTPUTMP_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLCAURIG ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLCAURIG');
          +i_ccchkf;
          +" from "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 set ";
      +"OUTPUTMP.FLCAURIG ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLCAURIG');
          +Iif(Empty(i_ccchkf),"",",OUTPUTMP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OUTPUTMP.PNSERIAL = t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set (";
          +"FLCAURIG";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLCAURIG')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set ";
      +"FLCAURIG ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLCAURIG');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLCAURIG ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLCAURIG');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Contrassegno le registrazioni con PNFLSALI errato (FLFLSALI='S')
    * --- Write into OUTPUTMP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PNSERIAL,CPROWNUM"
      do vq_exec with 'QUERY\GSCGDSPN',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OUTPUTMP_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLFLSALI ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALI');
          +i_ccchkf;
          +" from "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 set ";
      +"OUTPUTMP.FLFLSALI ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALI');
          +Iif(Empty(i_ccchkf),"",",OUTPUTMP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OUTPUTMP.PNSERIAL = t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set (";
          +"FLFLSALI";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALI')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set ";
      +"FLFLSALI ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALI');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLFLSALI ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALI');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Contrassegno le registrazioni con PNFLSALF errato (FLFLSALF='S')
    * --- Write into OUTPUTMP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PNSERIAL,CPROWNUM"
      do vq_exec with 'QUERY\GSCGESPN',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OUTPUTMP_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLFLSALF ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALF');
          +i_ccchkf;
          +" from "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 set ";
      +"OUTPUTMP.FLFLSALF ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALF');
          +Iif(Empty(i_ccchkf),"",",OUTPUTMP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OUTPUTMP.PNSERIAL = t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set (";
          +"FLFLSALF";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALF')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set ";
      +"FLFLSALF ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALF');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLFLSALF ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALF');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Contrassegno le registrazioni con PNFLSALD errato (FLFLSALD='S')
    * --- Write into OUTPUTMP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PNSERIAL,CPROWNUM"
      do vq_exec with 'QUERY\GSCGFSPN',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OUTPUTMP_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLFLSALD ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALD');
          +i_ccchkf;
          +" from "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 set ";
      +"OUTPUTMP.FLFLSALD ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALD');
          +Iif(Empty(i_ccchkf),"",",OUTPUTMP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OUTPUTMP.PNSERIAL = t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set (";
          +"FLFLSALD";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALD')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OUTPUTMP.PNSERIAL = _t2.PNSERIAL";
              +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set ";
      +"FLFLSALD ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALD');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLFLSALD ="+cp_NullLink(cp_ToStrODBC("S"),'OUTPUTMP','FLFLSALD');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Applico ulteriori filtri alle registrazioni selezionate
    * --- Esegue eventuale filtro su Numero e Tipo Registro Iva
    if (NOT EMPTY(this.oParentObject.w_TIPO) OR this.oParentObject.w_NUMREG<>0)
      * --- Create temporary table TMP_PART
      i_nIdx=cp_AddTableDef('TMP_PART') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gscggspn',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_PART_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Delete from OUTPUTMP
      i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"1 = "+cp_ToStrODBC(1);
               )
      else
        delete from (i_cTable) where;
              1 = 1;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Insert into OUTPUTMP
      i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.OUTPUTMP_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Drop temporary table TMP_PART
      i_nIdx=cp_GetTableDefIdx('TMP_PART')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_PART')
      endif
    endif
    * --- Esegue eventuale filtro su Codice IVA (solo nel caso della stampa dettaglio iva)
    if NOT EMPTY(this.oParentObject.w_CODIVA) 
      * --- Create temporary table TMP_PART
      i_nIdx=cp_AddTableDef('TMP_PART') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gscghspn',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_PART_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Delete from OUTPUTMP
      i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"1 = "+cp_ToStrODBC(1);
               )
      else
        delete from (i_cTable) where;
              1 = 1;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Insert into OUTPUTMP
      i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.OUTPUTMP_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Drop temporary table TMP_PART
      i_nIdx=cp_GetTableDefIdx('TMP_PART')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_PART')
      endif
    endif
    * --- Elimino le registrazioni sulla base dei filtri w_QUADRA
    *     S= Movimenti Senza Quadratura
    *     R = Movimenti Incongruenti
    *     C = Movimenti Corretti
    *     E = S+ R
    *     T = S+ R + C
    * --- Delete from OUTPUTMP
    i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
      do vq_exec with 'QUERY\GSCGISPN',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Inizializzo le variabili da passare al report
    L_UTENTE=this.w_UTENTE
    L_CAUSA=this.oParentObject.w_CAUSA
    L_DATA1=this.oParentObject.w_DATA1
    L_DATA2=this.oParentObject.w_DATA2
    L_NUMER1=this.oParentObject.w_NUMER1
    L_NUMER2=this.oParentObject.w_NUMER2
    L_PARAME=this.oParentObject.w_DEFI
    L_TIPO=this.oParentObject.w_TIPO
    L_NUMREG=this.oParentObject.w_NUMREG
    L_TIPOCLF=this.oParentObject.w_TIPOCLF
    L_CODCLF=this.oParentObject.w_CODCLF
    L_CODIVA=this.oParentObject.w_CODIVA
    L_TESTATA=IIF(this.oParentObject.w_DEFI="S","Movimenti Provvisori","")
    * --- Lancio il report
    vx_exec(""+alltrim(THIS.oParentObject.w_OQRY)+", "+alltrim(THIS.oParentObject.w_orep)+"",this)
    * --- Chiudo i temporanei
    if USED("__tmp__")
      select __tmp__
      use
    endif
    * --- Drop temporary table OUTPUTMP
    i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('OUTPUTMP')
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MASTRI'
    this.cWorkTables[3]='*OUTPUTMP'
    this.cWorkTables[4]='*TMP_PART'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
