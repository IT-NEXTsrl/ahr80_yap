* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_auo                                                        *
*              Unit� organizzative                                             *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-06-25                                                      *
* Last revis.: 2009-07-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_auo"))

* --- Class definition
define class tgsar_auo as StdForm
  Top    = 0
  Left   = 6

  * --- Standard Properties
  Width  = 519
  Height = 284+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-07-02"
  HelpContextID=192317591
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  SEDIAZIE_IDX = 0
  AZIENDA_IDX = 0
  DIPENDEN_IDX = 0
  cFile = "SEDIAZIE"
  cKeySelect = "SECODAZI,SECODDES"
  cKeyWhere  = "SECODAZI=this.w_SECODAZI and SECODDES=this.w_SECODDES"
  cKeyWhereODBC = '"SECODAZI="+cp_ToStrODBC(this.w_SECODAZI)';
      +'+" and SECODDES="+cp_ToStrODBC(this.w_SECODDES)';

  cKeyWhereODBCqualified = '"SEDIAZIE.SECODAZI="+cp_ToStrODBC(this.w_SECODAZI)';
      +'+" and SEDIAZIE.SECODDES="+cp_ToStrODBC(this.w_SECODDES)';

  cPrg = "gsar_auo"
  cComment = "Unit� organizzative"
  icon = "anag.ico"
  cAutoZoom = 'GSAR_AUO'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SECODAZI = space(5)
  w_SECODDES = space(5)
  w_SENOMSED = space(40)
  w_SETELEFO = space(18)
  w_SE_EMAIL = space(50)
  w_SEUNIORG = space(1)
  w_UNIORGRA = space(1)
  w_SEPERSRE = space(5)
  w_COGNOM = space(50)
  w_NOME = space(50)
  w_SECODRAG = space(5)
  w_SEDRAG = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SEDIAZIE','gsar_auo')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_auoPag1","gsar_auo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Unit� organizzativa")
      .Pages(1).HelpContextID = 201875756
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSECODDES_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='DIPENDEN'
    this.cWorkTables[3]='SEDIAZIE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SEDIAZIE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SEDIAZIE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SECODAZI = NVL(SECODAZI,space(5))
      .w_SECODDES = NVL(SECODDES,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SEDIAZIE where SECODAZI=KeySet.SECODAZI
    *                            and SECODDES=KeySet.SECODDES
    *
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SEDIAZIE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SEDIAZIE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SEDIAZIE '
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SECODAZI',this.w_SECODAZI  ,'SECODDES',this.w_SECODDES  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_UNIORGRA = space(1)
        .w_COGNOM = space(50)
        .w_NOME = space(50)
        .w_SEDRAG = space(40)
        .w_SECODAZI = NVL(SECODAZI,space(5))
          * evitabile
          *.link_1_1('Load')
        .w_SECODDES = NVL(SECODDES,space(5))
        .w_SENOMSED = NVL(SENOMSED,space(40))
        .w_SETELEFO = NVL(SETELEFO,space(18))
        .w_SE_EMAIL = NVL(SE_EMAIL,space(50))
        .w_SEUNIORG = NVL(SEUNIORG,space(1))
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .w_SEPERSRE = NVL(SEPERSRE,space(5))
          if link_1_17_joined
            this.w_SEPERSRE = NVL(DPCODICE117,NVL(this.w_SEPERSRE,space(5)))
            this.w_COGNOM = NVL(DPCOGNOM117,space(50))
            this.w_NOME = NVL(DPNOME117,space(50))
          else
          .link_1_17('Load')
          endif
        .w_SECODRAG = NVL(SECODRAG,space(5))
          if link_1_20_joined
            this.w_SECODRAG = NVL(SECODDES120,NVL(this.w_SECODRAG,space(5)))
            this.w_SEDRAG = NVL(SENOMSED120,space(40))
            this.w_UNIORGRA = NVL(SEUNIORG120,space(1))
          else
          .link_1_20('Load')
          endif
        cp_LoadRecExtFlds(this,'SEDIAZIE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_auo
    if this.w_SEUNIORG <> 'U'
      this.BlankRec()
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SECODAZI = space(5)
      .w_SECODDES = space(5)
      .w_SENOMSED = space(40)
      .w_SETELEFO = space(18)
      .w_SE_EMAIL = space(50)
      .w_SEUNIORG = space(1)
      .w_UNIORGRA = space(1)
      .w_SEPERSRE = space(5)
      .w_COGNOM = space(50)
      .w_NOME = space(50)
      .w_SECODRAG = space(5)
      .w_SEDRAG = space(40)
      if .cFunction<>"Filter"
        .w_SECODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_SECODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,5,.f.)
        .w_SEUNIORG = 'U'
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .DoRTCalc(7,8,.f.)
          if not(empty(.w_SEPERSRE))
          .link_1_17('Full')
          endif
        .DoRTCalc(9,11,.f.)
          if not(empty(.w_SECODRAG))
          .link_1_20('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'SEDIAZIE')
    this.DoRTCalc(12,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSECODDES_1_2.enabled = i_bVal
      .Page1.oPag.oSENOMSED_1_4.enabled = i_bVal
      .Page1.oPag.oSETELEFO_1_6.enabled = i_bVal
      .Page1.oPag.oSE_EMAIL_1_8.enabled = i_bVal
      .Page1.oPag.oSEPERSRE_1_17.enabled = i_bVal
      .Page1.oPag.oSECODRAG_1_20.enabled = i_bVal
      .Page1.oPag.oSEDRAG_1_21.enabled = i_bVal
      .Page1.oPag.oObj_1_16.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSECODDES_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSECODDES_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SEDIAZIE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SECODAZI,"SECODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SECODDES,"SECODDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SENOMSED,"SENOMSED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SETELEFO,"SETELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SE_EMAIL,"SE_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SEUNIORG,"SEUNIORG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SEPERSRE,"SEPERSRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SECODRAG,"SECODRAG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    i_lTable = "SEDIAZIE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SEDIAZIE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SEDIAZIE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SEDIAZIE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SEDIAZIE')
        i_extval=cp_InsertValODBCExtFlds(this,'SEDIAZIE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SECODAZI,SECODDES,SENOMSED,SETELEFO,SE_EMAIL"+;
                  ",SEUNIORG,SEPERSRE,SECODRAG "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_SECODAZI)+;
                  ","+cp_ToStrODBC(this.w_SECODDES)+;
                  ","+cp_ToStrODBC(this.w_SENOMSED)+;
                  ","+cp_ToStrODBC(this.w_SETELEFO)+;
                  ","+cp_ToStrODBC(this.w_SE_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_SEUNIORG)+;
                  ","+cp_ToStrODBCNull(this.w_SEPERSRE)+;
                  ","+cp_ToStrODBCNull(this.w_SECODRAG)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SEDIAZIE')
        i_extval=cp_InsertValVFPExtFlds(this,'SEDIAZIE')
        cp_CheckDeletedKey(i_cTable,0,'SECODAZI',this.w_SECODAZI,'SECODDES',this.w_SECODDES)
        INSERT INTO (i_cTable);
              (SECODAZI,SECODDES,SENOMSED,SETELEFO,SE_EMAIL,SEUNIORG,SEPERSRE,SECODRAG  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SECODAZI;
                  ,this.w_SECODDES;
                  ,this.w_SENOMSED;
                  ,this.w_SETELEFO;
                  ,this.w_SE_EMAIL;
                  ,this.w_SEUNIORG;
                  ,this.w_SEPERSRE;
                  ,this.w_SECODRAG;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SEDIAZIE_IDX,i_nConn)
      *
      * update SEDIAZIE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SEDIAZIE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SENOMSED="+cp_ToStrODBC(this.w_SENOMSED)+;
             ",SETELEFO="+cp_ToStrODBC(this.w_SETELEFO)+;
             ",SE_EMAIL="+cp_ToStrODBC(this.w_SE_EMAIL)+;
             ",SEUNIORG="+cp_ToStrODBC(this.w_SEUNIORG)+;
             ",SEPERSRE="+cp_ToStrODBCNull(this.w_SEPERSRE)+;
             ",SECODRAG="+cp_ToStrODBCNull(this.w_SECODRAG)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SEDIAZIE')
        i_cWhere = cp_PKFox(i_cTable  ,'SECODAZI',this.w_SECODAZI  ,'SECODDES',this.w_SECODDES  )
        UPDATE (i_cTable) SET;
              SENOMSED=this.w_SENOMSED;
             ,SETELEFO=this.w_SETELEFO;
             ,SE_EMAIL=this.w_SE_EMAIL;
             ,SEUNIORG=this.w_SEUNIORG;
             ,SEPERSRE=this.w_SEPERSRE;
             ,SECODRAG=this.w_SECODRAG;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SEDIAZIE_IDX,i_nConn)
      *
      * delete SEDIAZIE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SECODAZI',this.w_SECODAZI  ,'SECODDES',this.w_SECODDES  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SECODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SECODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SECODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_SECODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_SECODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SECODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SECODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SECODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEPERSRE
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEPERSRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_SEPERSRE)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_SEPERSRE))
          select DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SEPERSRE)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SEPERSRE) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oSEPERSRE_1_17'),i_cWhere,'GSAR_BDZ',"Risorse umane",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEPERSRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_SEPERSRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_SEPERSRE)
            select DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEPERSRE = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOM = NVL(_Link_.DPCOGNOM,space(50))
      this.w_NOME = NVL(_Link_.DPNOME,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_SEPERSRE = space(5)
      endif
      this.w_COGNOM = space(50)
      this.w_NOME = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEPERSRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.DPCODICE as DPCODICE117"+ ",link_1_17.DPCOGNOM as DPCOGNOM117"+ ",link_1_17.DPNOME as DPNOME117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on SEDIAZIE.SEPERSRE=link_1_17.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and SEDIAZIE.SEPERSRE=link_1_17.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SECODRAG
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SECODRAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SEDIAZIE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODDES like "+cp_ToStrODBC(trim(this.w_SECODRAG)+"%");
                   +" and SECODAZI="+cp_ToStrODBC(this.w_SECODAZI);

          i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SENOMSED,SEUNIORG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SECODAZI,SECODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SECODAZI',this.w_SECODAZI;
                     ,'SECODDES',trim(this.w_SECODRAG))
          select SECODAZI,SECODDES,SENOMSED,SEUNIORG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SECODAZI,SECODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SECODRAG)==trim(_Link_.SECODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SECODRAG) and !this.bDontReportError
            deferred_cp_zoom('SEDIAZIE','*','SECODAZI,SECODDES',cp_AbsName(oSource.parent,'oSECODRAG_1_20'),i_cWhere,'',"Unit� organizzative e sedi azienda",'GSAR2AUO.SEDIAZIE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SECODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SENOMSED,SEUNIORG";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SECODAZI,SECODDES,SENOMSED,SEUNIORG;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SENOMSED,SEUNIORG";
                     +" from "+i_cTable+" "+i_lTable+" where SECODDES="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SECODAZI="+cp_ToStrODBC(this.w_SECODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',oSource.xKey(1);
                       ,'SECODDES',oSource.xKey(2))
            select SECODAZI,SECODDES,SENOMSED,SEUNIORG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SECODRAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SENOMSED,SEUNIORG";
                   +" from "+i_cTable+" "+i_lTable+" where SECODDES="+cp_ToStrODBC(this.w_SECODRAG);
                   +" and SECODAZI="+cp_ToStrODBC(this.w_SECODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',this.w_SECODAZI;
                       ,'SECODDES',this.w_SECODRAG)
            select SECODAZI,SECODDES,SENOMSED,SEUNIORG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SECODRAG = NVL(_Link_.SECODDES,space(5))
      this.w_SEDRAG = NVL(_Link_.SENOMSED,space(40))
      this.w_UNIORGRA = NVL(_Link_.SEUNIORG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SECODRAG = space(5)
      endif
      this.w_SEDRAG = space(40)
      this.w_UNIORGRA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SECODAZI,1)+'\'+cp_ToStr(_Link_.SECODDES,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SECODRAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.SEDIAZIE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.SECODDES as SECODDES120"+ ",link_1_20.SENOMSED as SENOMSED120"+ ",link_1_20.SEUNIORG as SEUNIORG120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on SEDIAZIE.SECODRAG=link_1_20.SECODDES"+" and SEDIAZIE.SECODAZI=link_1_20.SECODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and SEDIAZIE.SECODRAG=link_1_20.SECODDES(+)"'+'+" and SEDIAZIE.SECODAZI=link_1_20.SECODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSECODDES_1_2.value==this.w_SECODDES)
      this.oPgFrm.Page1.oPag.oSECODDES_1_2.value=this.w_SECODDES
    endif
    if not(this.oPgFrm.Page1.oPag.oSENOMSED_1_4.value==this.w_SENOMSED)
      this.oPgFrm.Page1.oPag.oSENOMSED_1_4.value=this.w_SENOMSED
    endif
    if not(this.oPgFrm.Page1.oPag.oSETELEFO_1_6.value==this.w_SETELEFO)
      this.oPgFrm.Page1.oPag.oSETELEFO_1_6.value=this.w_SETELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oSE_EMAIL_1_8.value==this.w_SE_EMAIL)
      this.oPgFrm.Page1.oPag.oSE_EMAIL_1_8.value=this.w_SE_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oSEPERSRE_1_17.value==this.w_SEPERSRE)
      this.oPgFrm.Page1.oPag.oSEPERSRE_1_17.value=this.w_SEPERSRE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOGNOM_1_18.value==this.w_COGNOM)
      this.oPgFrm.Page1.oPag.oCOGNOM_1_18.value=this.w_COGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oNOME_1_19.value==this.w_NOME)
      this.oPgFrm.Page1.oPag.oNOME_1_19.value=this.w_NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oSECODRAG_1_20.value==this.w_SECODRAG)
      this.oPgFrm.Page1.oPag.oSECODRAG_1_20.value=this.w_SECODRAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSEDRAG_1_21.value==this.w_SEDRAG)
      this.oPgFrm.Page1.oPag.oSEDRAG_1_21.value=this.w_SEDRAG
    endif
    cp_SetControlsValueExtFlds(this,'SEDIAZIE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_auoPag1 as StdContainer
  Width  = 515
  height = 284
  stdWidth  = 515
  stdheight = 284
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSECODDES_1_2 as StdField with uid="MMBKZWWHVJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SECODDES", cQueryName = "SECODAZI,SECODDES",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice unit� organizzativa",;
    HelpContextID = 67765113,;
   bGlobalFont=.t.,;
    Height=21, Width=66, Left=147, Top=13, InputMask=replicate('X',5)

  add object oSENOMSED_1_4 as StdField with uid="CWTDAASYNY",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SENOMSED", cQueryName = "SENOMSED",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 60470122,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=147, Top=36, InputMask=replicate('X',40)

  add object oSETELEFO_1_6 as StdField with uid="RQRRKRTKIN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SETELEFO", cQueryName = "SETELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Telefono",;
    HelpContextID = 92345205,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=147, Top=74, InputMask=replicate('X',18)

  add object oSE_EMAIL_1_8 as StdField with uid="WVEEDMHLUT",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SE_EMAIL", cQueryName = "SE_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo e-mail",;
    HelpContextID = 26329970,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=147, Top=97, InputMask=replicate('X',50)


  add object oObj_1_16 as cp_runprogram with uid="OCLMAFIKRO",left=589, top=63, width=230,height=21,;
    caption='GSAR_BUO',;
   bGlobalFont=.t.,;
    prg="GSAR_BUO",;
    cEvent = "Insert start,Update start",;
    nPag=1;
    , HelpContextID = 62714037

  add object oSEPERSRE_1_17 as StdField with uid="UGBZSLTRPM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SEPERSRE", cQueryName = "SEPERSRE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 203369621,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=135, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_SEPERSRE"

  func oSEPERSRE_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oSEPERSRE_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSEPERSRE_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oSEPERSRE_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Risorse umane",'',this.parent.oContained
  endproc
  proc oSEPERSRE_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_SEPERSRE
     i_obj.ecpSave()
  endproc

  add object oCOGNOM_1_18 as StdField with uid="VKWXKUOZDN",rtseq=9,rtrep=.f.,;
    cFormVar = "w_COGNOM", cQueryName = "COGNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome della persona responsabile",;
    HelpContextID = 38187994,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=147, Top=158, InputMask=replicate('X',50)

  add object oNOME_1_19 as StdField with uid="ZZYEAJBXHM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NOME", cQueryName = "NOME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome della perona responsabile",;
    HelpContextID = 197176534,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=147, Top=182, InputMask=replicate('X',50)

  add object oSECODRAG_1_20 as StdField with uid="IJGXEDGMUO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SECODRAG", cQueryName = "SECODRAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice di raggruppamento",;
    HelpContextID = 234224787,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=25, Top=247, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SEDIAZIE", oKey_1_1="SECODAZI", oKey_1_2="this.w_SECODAZI", oKey_2_1="SECODDES", oKey_2_2="this.w_SECODRAG"

  func oSECODRAG_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oSECODRAG_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSECODRAG_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SEDIAZIE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODAZI="+cp_ToStrODBC(this.Parent.oContained.w_SECODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODAZI="+cp_ToStr(this.Parent.oContained.w_SECODAZI)
    endif
    do cp_zoom with 'SEDIAZIE','*','SECODAZI,SECODDES',cp_AbsName(this.parent,'oSECODRAG_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� organizzative e sedi azienda",'GSAR2AUO.SEDIAZIE_VZM',this.parent.oContained
  endproc

  add object oSEDRAG_1_21 as StdField with uid="QNGQFXNTBH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SEDRAG", cQueryName = "SEDRAG",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione raggruppamento",;
    HelpContextID = 153283802,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=90, Top=247, InputMask=replicate('X',40)

  add object oStr_1_3 as StdString with uid="TOWCVSBZIY",Visible=.t., Left=13, Top=17,;
    Alignment=1, Width=129, Height=18,;
    Caption="Unit� organizzativa:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="RXLGILIKYN",Visible=.t., Left=60, Top=40,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="MZCRCWWHDA",Visible=.t., Left=56, Top=78,;
    Alignment=1, Width=86, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="EZCPPVUPKB",Visible=.t., Left=29, Top=101,;
    Alignment=1, Width=113, Height=18,;
    Caption="Indirizzo e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="CMVRXYAHCG",Visible=.t., Left=4, Top=139,;
    Alignment=1, Width=138, Height=18,;
    Caption="Persona responsabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="NHOJGVPOQY",Visible=.t., Left=23, Top=223,;
    Alignment=0, Width=215, Height=18,;
    Caption="Unit� organizzativa di raggruppamento"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KSIWQQOGWR",Visible=.t., Left=240, Top=223,;
    Alignment=0, Width=104, Height=18,;
    Caption="(Sede azienda)"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_UNIORGRA <> "S")
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="ZSGGIVFNNG",Visible=.t., Left=76, Top=162,;
    Alignment=1, Width=66, Height=18,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="TBPBQIDEPZ",Visible=.t., Left=105, Top=186,;
    Alignment=1, Width=37, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oBox_1_13 as StdBox with uid="XLRQQHYGNX",left=20, top=243, width=368,height=31
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_auo','SEDIAZIE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SECODAZI=SEDIAZIE.SECODAZI";
  +" and "+i_cAliasName2+".SECODDES=SEDIAZIE.SECODDES";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
