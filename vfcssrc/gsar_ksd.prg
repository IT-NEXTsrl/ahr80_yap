* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ksd                                                        *
*              Scelta report principale                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-29                                                      *
* Last revis.: 2014-09-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_ksd",oParentObject))

* --- Class definition
define class tgsar_ksd as StdForm
  Top    = 58
  Left   = 66

  * --- Standard Properties
  Width  = 509
  Height = 328
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-26"
  HelpContextID=99186537
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsar_ksd"
  cComment = "Scelta report principale"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPDOC = space(5)
  w_CODLIN = space(3)
  w_GRPDEF = space(5)
  w_PRGSTAPRI = space(30)
  w_PRGSTAPRO = space(30)
  w_PRGSTASEC = space(30)
  w_ReportPrincipale = space(1)
  w_ReportSelezionato = 0
  w_DesRepo = space(100)
  w_ReportProdPrincipale = space(1)
  w_ARKSD_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ksdPag1","gsar_ksd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ARKSD_ZOOM = this.oPgFrm.Pages(1).oPag.ARKSD_ZOOM
    DoDefault()
    proc Destroy()
      this.w_ARKSD_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPDOC=space(5)
      .w_CODLIN=space(3)
      .w_GRPDEF=space(5)
      .w_PRGSTAPRI=space(30)
      .w_PRGSTAPRO=space(30)
      .w_PRGSTASEC=space(30)
      .w_ReportPrincipale=space(1)
      .w_ReportSelezionato=0
      .w_DesRepo=space(100)
      .w_ReportProdPrincipale=space(1)
      .w_TIPDOC=oParentObject.w_TIPDOC
      .w_CODLIN=oParentObject.w_CODLIN
      .w_GRPDEF=oParentObject.w_GRPDEF
      .w_PRGSTAPRI=oParentObject.w_PRGSTAPRI
      .w_PRGSTAPRO=oParentObject.w_PRGSTAPRO
      .w_PRGSTASEC=oParentObject.w_PRGSTASEC
      .w_ReportPrincipale=oParentObject.w_ReportPrincipale
      .w_ReportSelezionato=oParentObject.w_ReportSelezionato
      .w_ReportProdPrincipale=oParentObject.w_ReportProdPrincipale
          .DoRTCalc(1,7,.f.)
        .w_ReportSelezionato = .w_ARKSD_ZOOM.GetVar('OUROWNUM')
      .oPgFrm.Page1.oPag.ARKSD_ZOOM.Calculate()
        .w_DesRepo = .w_ARKSD_ZOOM.GetVar('OUDESCRI')
    endwith
    this.DoRTCalc(10,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIPDOC=.w_TIPDOC
      .oParentObject.w_CODLIN=.w_CODLIN
      .oParentObject.w_GRPDEF=.w_GRPDEF
      .oParentObject.w_PRGSTAPRI=.w_PRGSTAPRI
      .oParentObject.w_PRGSTAPRO=.w_PRGSTAPRO
      .oParentObject.w_PRGSTASEC=.w_PRGSTASEC
      .oParentObject.w_ReportPrincipale=.w_ReportPrincipale
      .oParentObject.w_ReportSelezionato=.w_ReportSelezionato
      .oParentObject.w_ReportProdPrincipale=.w_ReportProdPrincipale
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
            .w_ReportSelezionato = .w_ARKSD_ZOOM.GetVar('OUROWNUM')
        .oPgFrm.Page1.oPag.ARKSD_ZOOM.Calculate()
            .w_DesRepo = .w_ARKSD_ZOOM.GetVar('OUDESCRI')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ARKSD_ZOOM.Calculate()
    endwith
  return

  proc Calculate_VUFDGNKODU()
    with this
          * --- Doppio click chiude la maschera
          .EcpSave(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ARKSD_ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_ARKSD_ZOOM selected")
          .Calculate_VUFDGNKODU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_1.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_1.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLIN_1_2.value==this.w_CODLIN)
      this.oPgFrm.Page1.oPag.oCODLIN_1_2.value=this.w_CODLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRPDEF_1_3.value==this.w_GRPDEF)
      this.oPgFrm.Page1.oPag.oGRPDEF_1_3.value=this.w_GRPDEF
    endif
    if not(this.oPgFrm.Page1.oPag.oDesRepo_1_10.value==this.w_DesRepo)
      this.oPgFrm.Page1.oPag.oDesRepo_1_10.value=this.w_DesRepo
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_ksdPag1 as StdContainer
  Width  = 505
  height = 328
  stdWidth  = 505
  stdheight = 328
  resizeXpos=355
  resizeYpos=113
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPDOC_1_1 as StdField with uid="XFNLNQHTIK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 38786870,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=118, Top=278, InputMask=replicate('X',5)

  add object oCODLIN_1_2 as StdField with uid="UGKBLWJZZV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODLIN", cQueryName = "CODLIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 217521190,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=257, Top=278, InputMask=replicate('X',3)

  add object oGRPDEF_1_3 as StdField with uid="CXTRWAYBJH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GRPDEF", cQueryName = "GRPDEF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 78634854,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=379, Top=278, InputMask=replicate('X',5)


  add object ARKSD_ZOOM as cp_zoombox with uid="DCGPYIZGCC",left=2, top=1, width=503,height=227,;
    caption='ARKSD_ZOOM',;
   bGlobalFont=.t.,;
    cTable="REPO_DOC",bRetriveAllRows=.f.,cZoomFile="GSAR_KSD",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 38870715

  add object oDesRepo_1_10 as StdField with uid="GGCBJJXOGR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DesRepo", cQueryName = "DesRepo",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 12591670,;
   bGlobalFont=.t.,;
    Height=21, Width=378, Left=118, Top=241, InputMask=replicate('X',100)


  add object oBtn_1_11 as StdButton with uid="UUNSDLSMLD",left=447, top=275, width=48,height=46,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 99157786;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_12 as StdString with uid="JSOOMKZOFY",Visible=.t., Left=39, Top=242,;
    Alignment=1, Width=76, Height=18,;
    Caption="Selezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="BDQPWWZNDZ",Visible=.t., Left=6, Top=280,;
    Alignment=1, Width=109, Height=18,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="KEAAETYJTK",Visible=.t., Left=190, Top=280,;
    Alignment=1, Width=68, Height=18,;
    Caption="Lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="LEEMPZSWMZ",Visible=.t., Left=307, Top=280,;
    Alignment=1, Width=69, Height=18,;
    Caption="Gr. output"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ksd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
