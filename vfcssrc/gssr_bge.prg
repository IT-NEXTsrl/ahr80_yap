* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gssr_bge                                                        *
*              Elaborazioni non comuni AHE                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-07-29                                                      *
* Last revis.: 2004-07-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgssr_bge",oParentObject)
return(i_retval)

define class tgssr_bge as StdBatch
  * --- Local variables
  pTABLE = space(8)
  pOPER = space(4)
  * --- WorkFile variables
  MOVILOTT_idx=0
  MOVSLOTT_idx=0
  CCM_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSSR_BSD,GSSR_BSP
    *     esegue aggiornamenti tabelle non anfibie
    do case
      case this.pTABLE="MOVILOTT"
        do case
          case this.pOPER="RIGA" 
            * --- Insert into MOVILOTT
            i_nConn=i_TableProp[this.MOVILOTT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOVILOTT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVILOTT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MLSERIAL"+",MLROWORD"+",MLNUMRIF"+",CPROWNUM"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC("ZUCCHETTI"),'MOVILOTT','MLSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(-100),'MOVILOTT','MLROWORD');
              +","+cp_NullLink(cp_ToStrODBC(-100),'MOVILOTT','MLNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC(-100),'MOVILOTT','CPROWNUM');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MLSERIAL',"ZUCCHETTI",'MLROWORD',-100,'MLNUMRIF',-100,'CPROWNUM',-100)
              insert into (i_cTable) (MLSERIAL,MLROWORD,MLNUMRIF,CPROWNUM &i_ccchkf. );
                 values (;
                   "ZUCCHETTI";
                   ,-100;
                   ,-100;
                   ,-100;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          case this.pOPER="INSE" 
            * --- Movimenti Lotti ubicazioni
            * --- Insert into MOVSLOTT
            i_nConn=i_TableProp[this.MOVSLOTT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOVSLOTT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_cTempTable=cp_SetAzi(i_TableProp[this.MOVILOTT_idx,2])
              i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where MLSERIAL='ZUCCHETTI'",this.MOVSLOTT_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          case this.pOPER="DELE" 
            * --- Delete from MOVILOTT
            i_nConn=i_TableProp[this.MOVILOTT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOVILOTT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.MOVSLOTT_idx,2])
              i_cWhere=i_cTable+".MLSERIAL = "+i_cQueryTable+".MLSERIAL";
                    +" and "+i_cTable+".MLNUMRIF = "+i_cQueryTable+".MLNUMRIF";
            
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                    +" where exists( select 1 from "+i_cQueryTable+" where ";
                    +""+i_cTable+".MLSERIAL = "+i_cQueryTable+".MLSERIAL";
                    +" and "+i_cTable+".MLNUMRIF = "+i_cQueryTable+".MLNUMRIF";
                    +")")
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error='Errore eliminazione movimenti lotti/ubicazioni (documenti)'
              return
            endif
          case this.pOPER="INSS" 
            * --- Insert into MOVSLOTT
            i_nConn=i_TableProp[this.MOVSLOTT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOVSLOTT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR2STM",this.MOVSLOTT_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
        endcase
      case this.pTABLE="CCM_MAST"
        if this.pOPER="WRIT" 
          * --- Write into CCM_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CCM_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCM_MAST_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_SetAzi(i_TableProp[this.TMP_idx,2])
            i_cTempTable=cp_GetTempTableName(i_nConn)
            i_aIndex[1]='CCNUMDIS'
            cp_CreateTempTable(i_nConn,i_cTempTable,"DINUMDIS AS CCNUMDIS "," from "+i_cQueryTable+" where DINUMDIS IS NOT Null",.f.,@i_aIndex)
            i_cQueryTable=i_cTempTable
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CCM_MAST_idx,i_nConn)
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="CCM_MAST.CCNUMDIS = _t2.CCNUMDIS";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CCNUMDIS ="+cp_NullLink(cp_ToStrODBC(space(10)),'CCM_MAST','CCNUMDIS');
                +i_ccchkf;
                +" from "+i_cTable+" CCM_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="CCM_MAST.CCNUMDIS = _t2.CCNUMDIS";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CCM_MAST, "+i_cQueryTable+" _t2 set ";
            +"CCM_MAST.CCNUMDIS ="+cp_NullLink(cp_ToStrODBC(space(10)),'CCM_MAST','CCNUMDIS');
                +Iif(Empty(i_ccchkf),"",",CCM_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="PostgreSQL"
              i_cWhere="CCM_MAST.CCNUMDIS = _t2.CCNUMDIS";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CCM_MAST set ";
            +"CCNUMDIS ="+cp_NullLink(cp_ToStrODBC(space(10)),'CCM_MAST','CCNUMDIS');
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".CCNUMDIS = "+i_cQueryTable+".CCNUMDIS";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CCNUMDIS ="+cp_NullLink(cp_ToStrODBC(space(10)),'CCM_MAST','CCNUMDIS');
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MOVILOTT'
    this.cWorkTables[2]='MOVSLOTT'
    this.cWorkTables[3]='CCM_MAST'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
