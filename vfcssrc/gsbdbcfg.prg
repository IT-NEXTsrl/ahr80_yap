* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbdbcfg                                                        *
*              Connessione guidata al database                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-02                                                      *
* Last revis.: 2016-06-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbdbcfg",oParentObject,m.pOPER)
return(i_retval)

define class tgsbdbcfg as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_PROC_OK = .f.
  w_PADRE = .NULL.
  w_CTRL = .NULL.
  w_CONNSTRING = space(254)
  w_CONNRESULT = 0
  w_CONTA = 0
  w_OLDODBC = space(50)
  w_HWnd = 0
  w_DSN_NAME = space(50)
  w_BCKORIG = space(254)
  w_BCKDEST = space(254)
  w_RETVAL = space(254)
  w_MASTERDB = .f.
  w_SQLCOMMAND = space(254)
  w_ISUNICODE = .f.
  w_MDFNAME = space(254)
  w_CNFERROR = .f.
  w_FILECNF = space(254)
  w_ISAHE = .f.
  w_ISAHR = .f.
  w_ERRMESSPG = space(200)
  w_WBS = .NULL.
  w_PG_RESTORE = space(200)
  w_POSBCKDIR = space(200)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_ISAHE = ISAHE()
    this.w_ISAHR = ISAHR()
    this.w_PROC_OK = .T.
    this.w_PADRE = This.oparentobject
    do case
      case this.pOPER=="NEXT"
        do case
          case this.oParentObject.w_CUR_STEP=2
            if EMPTY(this.oParentObject.w_CFGTYPE)
              ah_Errormsg("Per poter proseguire occorre selezionare il tipo di configurazione che si desidera effettuare")
              this.w_PROC_OK = .F.
            else
              if LEFT(this.oParentObject.w_DBTYPE,5)<>"SQL20" AND this.oParentObject.w_CFGTYPE="S"
                ah_Errormsg("La configurazione semplificata � selezionabile solo con il tipo di database SQLServer")
                this.w_PROC_OK = .F.
              endif
            endif
            if this.w_PROC_OK
              if this.oParentObject.w_CFGTYPE=="O"
                this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP + 1
                if this.w_PROC_OK
                  this.w_CTRL = this.w_PADRE.GetCtrl("w_ODBCNAME")
                  this.w_CTRL.Init()     
                  this.oParentObject.w_ODBCNAME = IIF(VARTYPE(this.w_CTRL.ComboValues(1))="L", "", this.w_CTRL.ComboValues(1))
                  this.w_PADRE.SetControlsValue()     
                endif
                this.w_CTRL = this.w_PADRE.GetCtrl("w_DRVONAME")
                this.w_CTRL.Init()     
                this.oParentObject.w_DRVONAME = IIF(VARTYPE(this.w_CTRL.ComboValues(1))="L", "", this.w_CTRL.ComboValues(1))
                this.w_PADRE.SetControlsValue()     
                this.w_PADRE.NotifyEvent("AggInfoODBC")     
              else
                this.w_CTRL = this.w_PADRE.GetCtrl("w_DRV_NAME")
                this.w_CTRL.Init()     
                this.oParentObject.w_DRV_NAME = IIF(VARTYPE(this.w_CTRL.ComboValues(1))="L", "", this.w_CTRL.ComboValues(1))
                this.w_PADRE.SetControlsValue()     
              endif
              this.w_PADRE.NotiFyEvent("SetDbName")     
            endif
          case this.oParentObject.w_CUR_STEP=3
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if !this.oParentObject.w_VERIFCON AND this.w_PROC_OK and (Upper(LEFT(this.oParentObject.w_DBTYPE,5))="SQL20" or this.oParentObject.w_DB__MODE="E")
              this.w_PROC_OK = ah_yesno("La connessione non � mai stata verificata. Procedere comunque?")
            endif
            if this.w_PROC_OK AND this.oParentObject.w_CFGTYPE<>"O"
              this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP + 1
            endif
          case this.oParentObject.w_CUR_STEP=4
            if !this.oParentObject.w_VERIFCON and (Upper(LEFT(this.oParentObject.w_DBTYPE,5))="SQL20" or this.oParentObject.w_DB__MODE="E")
              this.w_PROC_OK = ah_yesno("La connessione non � mai stata verificata. Procedere comunque?")
            endif
        endcase
        if this.w_PROC_OK
          this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP + 1
        endif
      case this.pOPER=="PREV"
        if this.oParentObject.w_CUR_STEP=5 AND this.oParentObject.w_CFGTYPE<>"O"
          this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP - 1
        endif
        if this.oParentObject.w_CUR_STEP=4 AND this.oParentObject.w_CFGTYPE=="O"
          this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP - 1
        endif
        this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP - 1
      case this.pOPER=="CHECK"
        this.w_MASTERDB = .T.
        if this.oParentObject.w_CFGTYPE="O"
          this.w_CONNRESULT = SQLCONNECT(this.oParentObject.w_ODBCNAME)
        else
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_PROC_OK
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.oParentObject.w_DBTYPE="PostgreSQL"
              * --- Verifico i parametri di connessione
              this.w_CONNSTRING = this.w_CONNSTRING + ";" + ah_MakeODBCPar()
            endif
            this.w_CONNRESULT = SQLSTRINGCONNECT(this.w_CONNSTRING)
          endif
        endif
        if this.w_CONNRESULT>0
          if this.oParentObject.w_DBTYPE="PostgreSQL"
            this.w_CONNSTRING = SQLGETPROP(this.w_CONNRESULT,"ConnectString")
            this.w_RETVAL = ah_CheckODBCPar(this.w_CONNSTRING)
          endif
          this.oParentObject.w_VERIFCON = Empty(this.w_RETVAL)
          if this.oParentObject.w_VERIFCON
            Ah_ErrorMsg("Connessione verificata con successo",64,"Test connessione")
          else
            ah_errormsg("Parametri di connessione ODBC non corretti:%0%1",,,this.w_RETVAL)
          endif
          =SQLDISCONNECT(this.w_CONNRESULT)
          this.w_CONNRESULT = 0
        else
          if this.w_PROC_OK
            Ah_ErrorMsg("Connessione non riuscita.%0%1",,,LEFT(ALLTRIM(MESSAGE()),200))
          endif
          this.oParentObject.w_VERIFCON = .F.
          this.w_CONNRESULT = 0
        endif
      case this.pOPER=="NEWODBC"
        if !EMPTY(NVL(this.oParentObject.w_DRVONAME, " "))
          this.w_HWnd = MainHwnd()
          this.w_DSN_NAME = "AH_DSN"
          this.w_CONNSTRING = ""
          this.w_CONNRESULT = 0
          this.w_CONNSTRING = "DSN="+ALLTRIM(this.w_DSN_NAME)+CHR(0)+"DESCRIPTION="+ALLTRIM(_SCREEN.Caption)+CHR(0)
          do case
            case LEFT(this.oParentObject.w_DBTYPE,5)="SQL20"
              this.w_CONNSTRING = this.w_CONNSTRING + "SERVER=(local)"+IIF(EMPTY(this.oParentObject.w_IST_NAME),"","\"+ALLTRIM(this.oParentObject.w_IST_NAME))+CHR(0)+CHR(0)
            case this.oParentObject.w_DBTYPE="Oracle"
              this.w_CONNSTRING = this.w_CONNSTRING + CHR(0)+CHR(0)
            case this.oParentObject.w_DBTYPE="IBMDB2"
              this.w_CONNSTRING = this.w_CONNSTRING + "PATCH2=15"+CHR(0)+"PATCH1=3880"
          endcase
          ACOPY(this.w_PADRE.w_ODBCList, L_OldODBCList)
          L_StrDrv = ALLTRIM(this.oParentObject.w_DRVONAME)
          L_StrConn = this.w_CONNSTRING
          this.w_CONNRESULT = SQLConfigDataSource( this.w_HWnd , 1, @L_StrDrv , @L_StrConn)
          if this.w_CONNRESULT=0
            ah_ErrorMsg("Creazione ODBC annullata o fallita")
          else
            this.w_PADRE.NotifyEvent("AggODBC")     
          endif
          this.w_CONTA = 1
          do while this.w_CONTA<=ALEN(this.w_PADRE.w_ODBCList,1)
            if ASCAN(L_OldODBCList, this.w_PADRE.w_ODBCList(this.w_CONTA,1), 1, -1, 1, 7)=0
              this.oParentObject.w_ODBCNAME = ALLTRIM(this.w_PADRE.w_ODBCList(this.w_CONTA,1))
              this.w_CONTA = ALEN( this.w_PADRE.w_ODBCList,1)
            endif
            this.w_CONTA = this.w_CONTA + 1
          enddo
          this.w_PADRE.SetControlsValue()     
        else
          ah_ErrorMsg("Driver ODBC non impostato")
        endif
      case this.pOPER=="AGGODBC"
        if this.w_PROC_OK
          if this.oParentObject.w_CFGTYPE=="O"
            this.w_OLDODBC = this.oParentObject.w_ODBCNAME
            this.w_CTRL = this.w_PADRE.GetCtrl("w_DRVONAME")
            this.w_CTRL.Init()     
            this.oParentObject.w_DRVONAME = this.w_CTRL.ComboValues(1)
            this.w_PADRE.SetControlsValue()     
            this.w_CTRL = this.w_PADRE.GetCtrl("w_ODBCNAME")
            this.w_CTRL.Init()     
            if VARTYPE(this.w_CTRL.ComboValues)="L"
              this.oParentObject.w_ODBCNAME = ""
              this.w_PADRE.SetControlsValue()     
            else
              this.oParentObject.w_ODBCNAME = IIF(EMPTY(NVL(this.w_OLDODBC, " ")) OR VARTYPE(this.w_OLDODBC)<>"C", this.w_CTRL.ComboValues(1), this.w_OLDODBC)
              this.w_PADRE.SetControlsValue()     
              if ASCAN(this.w_CTRL.ComboValues, this.oParentObject.w_ODBCNAME, 1,-1,1,15)=0
                this.oParentObject.w_ODBCNAME = IIF(VARTYPE(this.w_CTRL.ComboValues(1))="L", "", this.w_CTRL.ComboValues(1))
                this.w_PADRE.SetControlsValue()     
              endif
            endif
            this.w_PADRE.NotifyEvent("AggInfoODBC")     
            this.w_PADRE.SetControlsValue()     
            this.w_CTRL = .NULL.
          endif
        endif
      case this.pOPER=="INFOODBC"
        this.oParentObject.w_ODBCINFO = ""
        this.w_CONTA = ASCAN(this.w_PADRE.w_ODBCList, this.oParentObject.w_ODBCNAME,1, -1, 1, 15)
        if this.w_CONTA>0
          this.oParentObject.w_ODBCINFO = ah_msgformat("Nome ODBC: %1%0", this.w_PADRE.w_ODBCList(this.w_CONTA,1))
          this.oParentObject.w_ODBCINFO = this.oParentObject.w_ODBCINFO + ah_msgformat("Descrizione: %1%0", this.w_PADRE.w_ODBCList(this.w_CONTA,7))
          this.oParentObject.w_ODBCINFO = this.oParentObject.w_ODBCINFO + ah_msgformat("Server: %1%0", this.w_PADRE.w_ODBCList(this.w_CONTA,5))
          this.oParentObject.w_ODBCINFO = this.oParentObject.w_ODBCINFO + ah_msgformat("Utente: %1%0", this.w_PADRE.w_ODBCList(this.w_CONTA,4))
          this.oParentObject.w_ODBCINFO = this.oParentObject.w_ODBCINFO + ah_msgformat("Usa connessione trusted: %1%0", this.w_PADRE.w_ODBCList(this.w_CONTA,6))
          this.oParentObject.w_ODBCINFO = this.oParentObject.w_ODBCINFO + ah_msgformat("Database: %1%0", this.w_PADRE.w_ODBCList(this.w_CONTA,2))
        endif
        this.w_PADRE.SetControlsValue()     
      case this.pOPER=="SETDBNAME" OR this.pOPER=="SETDBNAMEA"
        if this.oParentObject.w_DB__MODE="R"
          * --- Tento la lettura del file DEMO.INI per recuperare il nome del database
          *     da ripristinare
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if EMPTY(this.w_RETVAL)
            if this.pOPER=="SETDBNAME"
              ah_ErrorMsg("Archivi dimostrativi non trovati. Impossibile selezionare l'opzione per il ripristino automatico.")
            endif
            if this.pOPER=="SETDBNAMEA"
              ah_ErrorMsg("Archivi dimostrativi non trovati. Impossibile utilizzare la modalit� di configurazione semplificata")
            endif
            this.oParentObject.w_CFGTYPE = IIF(this.oParentObject.w_CFGTYPE="S", "C", this.oParentObject.w_CFGTYPE)
            this.oParentObject.w_DB__MODE = "E"
            this.w_PADRE.SetControlsValue()     
          else
            this.oParentObject.w_DB__NAME = this.w_RETVAL
          endif
        else
          this.oParentObject.w_DB__NAME = IIF(this.oParentObject.w_DB__MODE<>"R" AND !EMPTY(this.oParentObject.w_DB__NAME), this.oParentObject.w_DB__NAME, "")
        endif
      case this.pOPER=="SAVE"
        this.oParentObject.w_MSG = ""
        this.w_CNFERROR = .F.
        if this.oParentObject.w_DB__MODE<>"E"
          addmsgNL("Connessione al database...", this.w_PADRE)
          if this.oParentObject.w_CFGTYPE="O"
            this.w_CONNRESULT = SQLCONNECT(this.oParentObject.w_ODBCNAME)
          else
            this.w_MASTERDB = .T.
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_PROC_OK
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.oParentObject.w_DBTYPE<>"PostgreSQL" 
                this.w_CONNRESULT = SQLSTRINGCONNECT(this.w_CONNSTRING)
              else
                this.w_CONNRESULT = 1
              endif
            endif
            this.w_MASTERDB = .F.
          endif
          if this.w_CONNRESULT>0
            addmsgNL("%1connessione riuscita.", this.w_PADRE, CHR(9))
            do case
              case this.oParentObject.w_DB__MODE="R" and Upper(LEFT(this.oParentObject.w_DBTYPE,3))="SQL"
                addmsgNL("%1Inizio ripristino archivi demo...", this.w_PADRE, CHR(9))
                addmsgNL("%1Copia in locale archivi demo", this.w_PADRE, CHR(9)+CHR(9))
                this.w_CONTA = ASCAN(this.w_PADRE.w_DbEngines, this.oParentObject.w_DBTYPE, 1,-1,6,15)
                if this.w_CONTA>0
                  this.w_DSN_NAME = Addbs(Sys(5)+Sys(2003))+"..\exe\"+ADDBS(ALLTRIM(this.w_PADRE.w_DbEngines(this.w_Conta,2) ) )
                else
                  this.w_DSN_NAME = ""
                endif
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                this.w_BCKORIG = ADDBS(ALLTRIM(JUSTPATH(this.w_DSN_NAME)))+this.w_RETVAL+".bak"
                this.w_BCKDEST = "C:\adhoc_tmp\"+Justfname(this.w_BCKORIG)
                addmsgNL("%1Verifica presenza file", this.w_PADRE, CHR(9)+CHR(9))
                if FILE(this.w_BCKORIG)
                  if FILE(this.w_BCKDEST)
                    addmsgNL("%1File backup locale gi� rilevato, cancellazione...", this.w_PADRE, CHR(9)+CHR(9))
                    DELETE FILE (this.w_BCKDEST)
                  else
                    if !DIRECTORY("c:\adhoc_tmp\")
                      addmsgNL("%1Tentativo creazione directory locale", this.w_PADRE, CHR(9)+CHR(9))
                      MkDir "C:\adhoc_tmp\"
                    endif
                  endif
                  addmsgNL("%1Copia file archivi demo in locale", this.w_PADRE, CHR(9)+CHR(9))
                  COPY FILE (this.w_BCKORIG) TO (this.w_BCKDEST)
                  addmsgNL("%1Creazione del database %2", this.w_PADRE, CHR(9)+CHR(9), ALLTRIM(this.oParentObject.w_DB__NAME))
                  if SQLExec(this.w_CONNRESULT,"create database ["+ALLTRIM(this.oParentObject.w_DB__NAME)+"]")<0
                    addmsgNL("%1Creazione del database %2 fallita. Chiusura connessione", this.w_PADRE, CHR(9)+CHR(9), ALLTRIM(this.oParentObject.w_DB__NAME))
                    this.w_CNFERROR = .T.
                  else
                    SQLDISCONNECT(this.w_CONNRESULT)
                    this.Page_3()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    this.w_CONNRESULT = SQLSTRINGCONNECT(this.w_CONNSTRING)
                    if this.w_CONNRESULT<=0
                      addmsgNL("%1Connessione al database %2 fallita. Eliminazione database e chiusura connessione", this.w_PADRE, CHR(9)+CHR(9), ALLTRIM(this.oParentObject.w_DB__NAME))
                      this.w_MASTERDB = .T.
                      this.Page_3()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                      this.w_CONNRESULT = SQLSTRINGCONNECT(this.w_CONNSTRING)
                      SQLExec(this.w_CONNRESULT,"Drop database ["+ALLTRIM(this.oParentObject.w_DB__NAME)+"]")
                      SQLDISCONNECT(this.w_CONNRESULT)
                      this.w_CNFERROR = .T.
                    else
                      * --- Database locale creato, determino il nome logico dei file
                      if SQLExec(this.w_CONNRESULT,"RESTORE FILELISTONLY FROM DISK='"+ALLTRIM(this.w_BCKDEST)+"'","_flbackup_")<0
                        addmsgNL("%1Impossibile recuperare informazioni archivi dimostrativi.%0Errore restituito:%2", this.w_PADRE, CHR(9)+CHR(9), ALLTRIM(MESSAGE()))
                        addmsgNL("%1Eliminazione database:%2", this.w_PADRE, CHR(9)+CHR(9), ALLTRIM(this.oParentObject.w_DB__NAME))
                        SQLDISCONNECT(this.w_CONNRESULT)
                        this.w_CONNRESULT = 0
                        this.w_MASTERDB = .T.
                        this.Page_3()
                        if i_retcode='stop' or !empty(i_Error)
                          return
                        endif
                        this.w_CONNRESULT = SQLSTRINGCONNECT(this.w_CONNSTRING)
                        SQLExec(this.w_CONNRESULT,"Drop database ["+ALLTRIM(this.oParentObject.w_DB__NAME)+"]")
                        this.w_CNFERROR = .T.
                      else
                        * --- Determino posizione dei file fisici
                        SQLExec(this.w_CONNRESULT, "select name,filename from sysfiles","_flname_")
                        SQLDISCONNECT(this.w_CONNRESULT)
                        Select "_flname_"
                        GO TOP
                        this.w_SQLCOMMAND = "Restore Database ["+Rtrim(this.oParentObject.w_DB__NAME)+"] FROM DISK=N"+"'"+ALLTRIM(this.w_BCKDEST)+"'"+" WITH RECOVERY,REPLACE "
                        SCAN
                        * --- Cerco il file di dati
                        Select "_flbackup_"
                        GO TOP
                        this.w_ISUNICODE = Left(Strconv(Alltrim(_flname_.filename ),6),1)<>"?"
                        Locate For Type=Iif(Lower(Justext(Iif( this.w_ISUNICODE ,Strconv(Alltrim(_flname_.filename ),6),Alltrim(_flname_.filename ))))="mdf","D","L")
                        if FOUND()
                          this.w_MDFNAME = Alltrim(_flbackup_.LogicalName)
                        endif
                        Select "_flname_"
                        this.w_SQLCOMMAND = this.w_SQLCOMMAND + ",MOVE N'"+Alltrim(this.w_MDFNAME )+"'TO N'"+ Iif( this.w_ISUNICODE ,Strconv(Alltrim(_flname_.filename ),6),Alltrim(_flname_.filename ))+"'"
                        ENDSCAN
                        USE IN SELECT("_flbackup_")
                        USE IN SELECT("_flname_")
                        this.w_MASTERDB = .T.
                        this.Page_3()
                        if i_retcode='stop' or !empty(i_Error)
                          return
                        endif
                        this.w_CONNRESULT = SQLSTRINGCONNECT(this.w_CONNSTRING)
                        addmsgNL("%1Ripristino archivi dimostrativi in corso.%0Frase SQL in uso:%2", this.w_PADRE, CHR(9)+CHR(9), ALLTRIM(this.w_SQLCOMMAND))
                        if SQLExec(this.w_CONNRESULT, this.w_SQLCOMMAND)<0
                          addmsgNL("%1Impossibile ripristinare archivi dimostrativi.%0Errore restituito:%2", this.w_PADRE, CHR(9)+CHR(9), ALLTRIM(MESSAGE()))
                          addmsgNL("%1Eliminazione database:%2", this.w_PADRE, CHR(9)+CHR(9), ALLTRIM(this.oParentObject.w_DB__NAME))
                          SQLExec(this.w_CONNRESULT,"Drop database ["+ALLTRIM(this.oParentObject.w_DB__NAME)+"]")
                          this.w_CNFERROR = .T.
                        else
                          addmsgNL("%1Fine ripristino archivi demo", this.w_PADRE, CHR(9))
                          this.Page_5()
                          if i_retcode='stop' or !empty(i_Error)
                            return
                          endif
                          if FILE(this.w_BCKDEST)
                            DELETE FILE (this.w_BCKDEST)
                          endif
                          if DIRECTORY("c:\adhoc_tmp\")
                            olderr = ON("ERROR")
                            L_err = .F.
                            on error L_err=.T.
                            Rmdir "c:\adhoc_tmp\"
                            on error &OldErr
                          endif
                        endif
                      endif
                    endif
                  endif
                else
                  addmsgNL("%1File backup archivi dimostrativi non trovato (%2). Chiusura connessione", this.w_PADRE, CHR(9)+CHR(9), ALLTRIM(this.w_BCKORIG))
                  this.w_CNFERROR = .T.
                endif
              case this.oParentObject.w_DB__MODE="R" and this.oParentObject.w_DBTYPE="PostgreSQL" 
                this.w_PG_RESTORE = alltrim(addbs(this.oParentObject.w_POSQLDIR))+"PG_RESTORE.EXE"
                this.oParentObject.w_PASSWORD = alltrim(this.oParentObject.w_PASSWORD)
                do case
                  case this.w_ISAHE
                    this.w_POSBCKDIR = FULLPATH("posbck\aheposdemo.backup")
                  case this.w_ISAHR
                    this.w_POSBCKDIR = FULLPATH("posbck\ahrposdemo.backup")
                endcase
                ah_Msg("Creazione del database in corso...",.T.)
                pgr_create2 = '"'+ADDBS(this.oParentObject.w_POSQLDIR)+'createdb.exe"'+""+" -U " +alltrim(this.oParentObject.w_USER__ID) +" --encoding=WIN1252 --lc-collate=C --lc-ctype=C -T template0 "+alltrim(this.oParentObject.w_DB__NAME) + "  > " + '"'+addbs(tempadhoc())+'pgrcreate.msg"' + " 2> " + '"'+addbs(tempadhoc())+'pgrcreate.txt"'
                pgr_create = "@SET PGPASSWORD="+this.oParentObject.w_PASSWORD+CHR(13)+CHR(10) + pgr_create2
                create_bat = addbs(tempadhoc()) + "pgr_create.BAT"
                STRTOFILE(pgr_create, create_bat, 0)
                if FILE(create_bat)
                  ! &create_bat
                  if FILE(ADDBS(TEMPADHOC())+"pgrcreate.TXT")
                    this.w_ERRMESSPG = ""
                    this.w_ERRMESSPG = FILETOSTR(ADDBS(TEMPADHOC())+"pgrcreate.TXT")
                    delete file (ADDBS(TEMPADHOC())+"pgrcreate.TXT")
                    if not empty(alltrim(this.w_ERRMESSPG))
                      ah_errormsg("%1",48,,alltrim(this.w_ERRMESSPG))
                      delete file (create_bat)
                      this.w_CNFERROR = .T.
                    endif
                  endif
                  delete file (create_bat)
                  delete file (ADDBS(TEMPADHOC())+"pgrcreate.TXT")
                  delete file (ADDBS(TEMPADHOC())+"pgrcreate.msg")
                else
                  ah_ErrorMsg("Impossibile creare file BAT di ripristino database.","!","")
                  this.w_CNFERROR = .T.
                endif
                if !this.w_CNFERROR
                  ah_Msg("Ripristino del database in corso...",.T.)
                  pgr_restore2 = '"'+this.w_PG_RESTORE+'"'+" --host=" +alltrim(this.oParentObject.w_SRV_NAME)+ " --port=" +alltrim(this.oParentObject.w_SRV_PORT)+; 
 " --username=" +alltrim(this.oParentObject.w_USER__ID)+ " --dbname=" +alltrim(this.oParentObject.w_DB__NAME)+ + ' "' +this.w_POSBCKDIR+'"' + "  > " + '"'+addbs(tempadhoc())+'pgrestore.msg"' + " 2> " + '"' +addbs(tempadhoc())+'pgrestore.txt"'
                  pgr_restore = "@SET PGPASSWORD="+this.oParentObject.w_PASSWORD+CHR(13)+CHR(10) + pgr_restore2
                  restore_bat = addbs(tempadhoc()) + "pgr_restore.BAT"
                  STRTOFILE(pgr_restore, restore_bat, 0)
                  if FILE(restore_bat)
                    ! &restore_bat
                    if FILE(ADDBS(TEMPADHOC())+"pgrestore.TXT")
                      this.w_ERRMESSPG = ""
                      this.w_ERRMESSPG = FILETOSTR(ADDBS(TEMPADHOC())+"pgrestore.TXT")
                      delete file (ADDBS(TEMPADHOC())+"pgrestore.TXT")
                      if not empty(alltrim(this.w_ERRMESSPG))
                        ah_errormsg("%1",48,,alltrim(this.w_ERRMESSPG))
                        delete file (restore_bat)
                        this.w_CNFERROR = .T.
                      endif
                    endif
                    delete file (restore_bat)
                    delete file (ADDBS(TEMPADHOC())+"pgrestore.txt")
                    delete file (ADDBS(TEMPADHOC())+"pgrestore.msg")
                  else
                    ah_ErrorMsg("Impossibile creare file BAT di ripristino database.","!","")
                    this.w_CNFERROR = .T.
                  endif
                endif
                addmsgNL("%1Fine ripristino archivi demo", this.w_PADRE, CHR(9))
                this.Page_5()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
            endcase
            if this.oParentObject.w_DB__MODE="N"
              addmsgNL("%1Inizio creazione nuovo database...", this.w_PADRE, CHR(9))
              addmsgNL("%1Creazione del database %2", this.w_PADRE, CHR(9)+CHR(9), ALLTRIM(this.oParentObject.w_DB__NAME))
              if Upper(LEFT(this.oParentObject.w_DBTYPE,3))="SQL"
                if SQLExec(this.w_CONNRESULT,"create database ["+ALLTRIM(this.oParentObject.w_DB__NAME)+"]")<0
                  addmsgNL("%1Creazione del database %2 fallita. Chiusura connessione", this.w_PADRE, CHR(9)+CHR(9), ALLTRIM(this.oParentObject.w_DB__NAME))
                  this.w_CNFERROR = .T.
                else
                  SQLDISCONNECT(this.w_CONNRESULT)
                  this.w_CONNRESULT = 0
                  this.Page_5()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              else
                ah_Msg("Creazione del database in corso...",.T.)
                pgr_create2 = '"'+ADDBS(this.oParentObject.w_POSQLDIR)+'createdb.exe"'+""+" -U " +alltrim(this.oParentObject.w_USER__ID) +" --encoding=WIN1252 --lc-collate=C --lc-ctype=C -T template0 "+alltrim(this.oParentObject.w_DB__NAME) + "  > " + '"'+addbs(tempadhoc())+'pgrcreate.msg"' + " 2> " + '"'+addbs(tempadhoc())+'pgrcreate.txt"'
                pgr_create = "@SET PGPASSWORD="+this.oParentObject.w_PASSWORD+CHR(13)+CHR(10) + pgr_create2
                _ClipText=pgr_create
                create_bat = addbs(tempadhoc()) + "pgr_create.BAT"
                STRTOFILE(pgr_create, create_bat, 0)
                if FILE(create_bat)
                  ! &create_bat
                  if FILE(ADDBS(TEMPADHOC())+"pgcreate.TXT")
                    this.w_ERRMESSPG = FILETOSTR(ADDBS(TEMPADHOC())+"pgcreate.TXT")
                    delete file (ADDBS(TEMPADHOC())+"pgcreate.TXT")
                    delete file (ADDBS(TEMPADHOC())+"pgrcreate.msg")
                    if not empty(alltrim(this.w_ERRMESSPG))
                      ah_errormsg("%1",48,,alltrim(this.w_ERRMESSPG))
                      delete file (create_bat)
                      i_retcode = 'stop'
                      i_retval = .F.
                      return
                    endif
                  endif
                  delete file (create_bat)
                else
                  ah_ErrorMsg("Impossibile creare file BAT di creazione database.","!","")
                  i_retcode = 'stop'
                  i_retval = .F.
                  return
                endif
                this.w_CONNRESULT = 0
                this.Page_5()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
            if this.w_CONNRESULT>0 and Upper(LEFT(this.oParentObject.w_DBTYPE,3))="SQL"
              SQLDISCONNECT(this.w_CONNRESULT)
            endif
          else
            Ah_ErrorMsg("Connessione non riuscita.%0%1",,,LEFT(ALLTRIM(MESSAGE()),200))
            this.w_CNFERROR = .T.
            this.oParentObject.w_VERIFCON = .F.
          endif
        else
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if !this.w_CNFERROR AND this.oParentObject.w_CFGTYPE="O" AND LEFT(this.oParentObject.w_DBTYPE,5)="SQL20"
            addmsgNL("Inizio aggiornamento dati ODBC selezionato...", this.w_PADRE)
            #Define HKLM 0x80000002 && HKEY_LOCAL_MACHINE
            #Define HKLM 0x80000002 && HKEY_LOCAL_MACHINE
            #Define HKCU 0x80000001 && HKEY_CURRENT_USER
            Local oWMILocator, oWMI, oStdRegProv, cComputer, nLastError, cLastError
            cComputer = "."
            oStdRegProv = .Null.
            nLastError = 0
            cLastError = ""
            oWMILocator = Createobject("WbemScripting.SWbemLocator")
            oWMI = oWMILocator.ConnectServer(cComputer, "root\default")
            oStdRegProv = oWMI.Get("StdRegProv")
            * --- We need to use COMARRAY to change array behaviour to
            *     zero-based arrays passed by reference.
            *     Without this, the REG_BINARY and REG_MULTI_SZ types won't work.
            Comarray(oStdRegProv, 10)
            nHive = IIF(this.oParentObject.w_DSNTYPE="HKCU", HKCU , HKLM )
            cKey = "SOFTWARE\ODBC\ODBC.INI\"+ALLTRIM(this.oParentObject.w_ODBCNAME)
            if !EMPTY(NVL(this.oParentObject.w_DB__NAME, " "))
              addmsgNL("%1Aggiornamento database da utilizzare (%2)", this.w_PADRE, CHR(9), ALLTRIM(this.oParentObject.w_DB__NAME))
              cName = "Database"
              cValue = ALLTRIM(this.oParentObject.w_DB__NAME)
              nResult = oStdRegProv.SetStringValue(nHive, cKey, cName, cValue)
            endif
            if this.oParentObject.w_CONNTYPE="U" AND this.oParentObject.w_DBTYPE<>"IBMDB2"
              addmsgNL("%1Aggiornamento connessione accesso tramite utente %2", this.w_PADRE, CHR(9), ALLTRIM(this.oParentObject.w_USER__ID))
              do case
                case LEFT(this.oParentObject.w_DBTYPE,5)="SQL20"
                  addmsgNL("%1Elimino eventuale accesso tramite autenticazione di Windows integrata", this.w_PADRE, CHR(9)+CHR(9))
                  cName = "Trusted_Connection"
                  nResult = oStdRegProv.DeleteValue(nHive, cKey, cName)
                  cName = "LastUser"
                case this.oParentObject.w_DBTYPE="Oracle"
                  cName = "UserID"
              endcase
              cValue = ALLTRIM(this.oParentObject.w_USER__ID)
              nResult = oStdRegProv.SetStringValue(nHive, cKey, cName, cValue)
            else
              addmsgNL("%1Aggiornamento connessione tramite autenticazione di Windows integrata", this.w_PADRE, CHR(9))
              cName = "Trusted_Connection"
              cValue = "Yes"
              nResult = oStdRegProv.SetStringValue(nHive, cKey, cName, cValue)
            endif
            oStdRegProv = .NULL.
            oWMI = .NULL.
            oWMILocator = .NULL.
          endif
          addmsgNL("Fine aggiornamento dati ODBC selezionato", this.w_PADRE)
        endif
        if !this.w_CNFERROR AND FILE("..\exe\cp3start.cnf")
          addmsgNL("Lettura file di configurazione creato...", this.w_PADRE)
          p_FileCnf = "..\exe\cp3start.cnf"
          Do GetConfigFile In cp3start
          if VARTYPE(CP_ODBCCONN)<>"C" OR EMPTY(NVL(CP_ODBCCONN, " "))
            addmsgNL("%1Lettura file di configurazione fallita", this.w_PADRE, CHR(9))
            this.w_CNFERROR = .T.
          else
            addmsgNL("%1Lettura file di configurazione riuscita.%0Ingresso procedura", this.w_PADRE, CHR(9))
            * --- Attendo alcuni secondi prima della chiusura della maschera
            INKEY(5,"HM")
          endif
        endif
      case this.pOPER=="EXIT"
        if !ah_YesNo("Si desidera uscire dalla configurazione guidata?")
          i_retcode = 'stop'
          return
        endif
    endcase
    if this.pOPER=="EXIT" OR (this.pOPER=="SAVE" AND !this.w_CNFERROR)
      if this.oParentObject.w_DB__MODE="N"
        i_CodUte = 1
      endif
      RELEASE i_serverconn
      RELEASE i_tableprop
      RELEASE i_ntables
      USE IN SELECT("cpusrgrp")
      USE IN SELECT("cpprgsec")
      USE IN SELECT("cpgroups")
      RELEASE g_bDisableCfgGest
      g_bShowCpToolBar=.T.
      ocptoolbar.Visible=.T.
      this.w_PADRE.bUpdated = .F.
      this.w_PADRE.ecpQuit()     
    else
      this.w_PADRE.oPGFRM.ActivePage=this.oParentObject.w_CUR_STEP
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if EMPTY(this.oParentObject.w_DRV_NAME)
      ah_ErrorMsg("Impostare il driver da utilizzare per la connessione")
      this.w_PROC_OK = .F.
    endif
    if EMPTY(this.oParentObject.w_SRV_NAME)
      ah_ErrorMsg("Impostare il server da utilizzare per la connessione")
      this.w_PROC_OK = .F.
    endif
    if this.oParentObject.w_DBTYPE="Oracle" AND EMPTY(this.oParentObject.w_SERVICE)
      ah_ErrorMsg("Impostare il servizio da utilizzare per la connessione")
      this.w_PROC_OK = .F.
    endif
    if this.oParentObject.w_DBTYPE="IBMDB2" AND EMPTY(this.oParentObject.w_PROTOCOL)
      ah_ErrorMsg("Impostare il protocollo da utilizzare per la connessione")
      this.w_PROC_OK = .F.
    endif
    if inlist(this.oParentObject.w_DBTYPE,"PostgreSQL","IBMDB2") AND EMPTY(this.oParentObject.w_SRV_PORT)
      ah_ErrorMsg("Impostare la porta da utilizzare per la connessione")
      this.w_PROC_OK = .F.
    endif
    if EMPTY(this.oParentObject.w_USER__ID)
      ah_ErrorMsg("Impostare l'utente da utilizzare per la connessione")
      this.w_PROC_OK = .F.
    endif
    if inlist(this.oParentObject.w_DBTYPE,"PostgreSQL","IBMDB2") AND EMPTY(this.oParentObject.w_PASSWORD)
      this.w_PROC_OK = ah_YesNo("Non � stata impostata la password per l'utente con cui accedere al database. Continuare comunque?")
    endif
    if this.oParentObject.w_DBTYPE<>"Oracle" AND EMPTY(this.oParentObject.w_DB__NAME)
      ah_ErrorMsg("Impostare il database da utilizzare per la connessione")
      this.w_PROC_OK = .F.
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CONNSTRING = "Driver={"+ALLTRIM(this.oParentObject.w_DRV_NAME)+"};"
    do case
      case this.oParentObject.w_DBTYPE="IBMDB2"
        this.w_CONNSTRING = this.w_CONNSTRING + "Hostname="
      otherwise
        this.w_CONNSTRING = this.w_CONNSTRING + "SERVER="
    endcase
    this.w_CONNSTRING = this.w_CONNSTRING + ALLTRIM(this.oParentObject.w_SRV_NAME) + IIF(EMPTY(this.oParentObject.w_IST_NAME), "", "\" + ALLTRIM(this.oParentObject.w_IST_NAME) ) + ";"
    do case
      case this.oParentObject.w_DBTYPE="Oracle"
        this.w_CONNSTRING = this.w_CONNSTRING + "DBQ=" + ALLTRIM(this.oParentObject.w_SERVICE) + ";"
      otherwise
        if this.oParentObject.w_DB__MODE<>"E" and this.w_MASTERDB
          this.w_CONNSTRING = this.w_CONNSTRING + "DATABASE=master;"
        else
          this.w_CONNSTRING = this.w_CONNSTRING + "DATABASE=" + ALLTRIM(this.oParentObject.w_DB__NAME) + ";"
        endif
    endcase
    if this.oParentObject.w_DBTYPE="IBMDB2"
      this.w_CONNSTRING = this.w_CONNSTRING + "Port=" + ALLTRIM(this.oParentObject.w_SRV_PORT) + "; Protocol=" + ALLTRIM(this.oParentObject.w_PROTOCOL) + "; patch1=3880; patch2=15;"
    endif
    if this.oParentObject.w_DBTYPE="PostgreSQL"
      this.w_CONNSTRING = this.w_CONNSTRING + "Port=" + ALLTRIM(this.oParentObject.w_SRV_PORT) + ";"
    endif
    if this.oParentObject.w_CONNTYPE="U"
      this.w_CONNSTRING = this.w_CONNSTRING + "UID=" + ALLTRIM(this.oParentObject.w_USER__ID) + "; PWD=" + ALLTRIM(this.oParentObject.w_PASSWORD) + ";"
      if LEFT(this.oParentObject.w_DBTYPE,5)="SQL20"
        this.w_CONNSTRING = this.w_CONNSTRING + "TRUSTED_CONNECTION=NO;"
      endif
    else
      this.w_CONNSTRING = this.w_CONNSTRING + "TRUSTED_CONNECTION=YES;"
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_RETVAL = ""
    this.w_CONTA = ASCAN(this.w_PADRE.w_DbEngines, this.oParentObject.w_DBTYPE, 1,-1,6,15)
    if this.w_CONTA>0
      if left(this.oParentObject.w_DBTYPE,10)="PostgreSQL"
        do case
          case this.w_ISAHE
            this.w_DSN_NAME = FULLPATH("posbck\aheposdemo.backup")
          case this.w_ISAHR
            this.w_DSN_NAME = FULLPATH("posbck\ahrposdemo.backup")
        endcase
        if FILE(this.w_DSN_NAME)
          do case
            case this.w_ISAHE
              this.w_RETVAL = "aheposdemo"
            case this.w_ISAHR
              this.w_RETVAL = "ahrposdemo"
          endcase
        else
          this.w_RETVAL = ""
        endif
      else
        this.w_DSN_NAME = Addbs(Sys(5)+Sys(2003))+"..\exe\"+ADDBS(ALLTRIM(this.w_PADRE.w_DbEngines(this.w_Conta,2) ) )
        this.w_DSN_NAME = this.w_DSN_NAME + "DEMO.INI"
        if FILE(this.w_DSN_NAME)
          L_FR=FOPEN(this.w_DSN_NAME)
          if L_FR>0
            do while !FEOF(L_FR)
              L_LETTURA= FGETS(L_FR, 8192)
              &L_LETTURA
            enddo
            FCLOSE(L_FR)
            this.w_RETVAL = ALLTRIM(IIF(VARTYPE(CP_BCKFILE)="C", CP_BCKFILE, " "))
          else
            this.w_RETVAL = ""
          endif
        else
          this.w_RETVAL = ""
        endif
      endif
    else
      this.w_RETVAL = ""
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if !this.w_CNFERROR
      addmsgNL("Inizio scrittura file configurazione...", this.w_PADRE)
      this.w_FILECNF = "..\exe\CP3START.CNF"
      this.w_CNFERROR = .F.
      if FILE(this.w_FILECNF)
        addmsgNL("%1File gi� presente aggiunta dati", this.w_PADRE, CHR(9))
        L_FW=FOPEN(this.w_FILECNF, 12)
        FSEEK(L_FW,0,2)
      else
        addmsgNL("%1Creazione nuovo file di configurazione", this.w_PADRE, CHR(9))
        L_FW=FCREATE(this.w_FILECNF)
      endif
      if L_FW>0
        this.w_CONTA = ASCAN(this.w_PADRE.w_DbEngines, this.oParentObject.w_DBTYPE, 1,-1,6,15)
        if this.w_CONTA>0
          FPUTS(L_FW, "CP_DBTYPE='"+ALLTRIM(this.w_PADRE.w_DbEngines(this.w_Conta,3))+"'")
          if this.oParentObject.w_CFGTYPE="O"
            FPUTS(L_FW, "CP_ODBCCONN='"+ALLTRIM(this.oParentObject.w_ODBCNAME)+"'")
          else
            this.w_MASTERDB = .F.
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            FPUTS(L_FW, "CP_ODBCCONN='"+ALLTRIM(this.w_CONNSTRING)+"'")
          endif
          FPUTS(L_FW, "i_LoadXDC ="+IIF(this.oParentObject.w_LOAD_XDC,".T.",".F."))
          FPUTS(L_FW, "g_CheckUNC ="+IIF(this.oParentObject.w_CHECKUNC,".T.",".F."))
          FPUTS(L_FW, "i_bDisablePostin ="+IIF(this.oParentObject.w_POSTINON,".T.",".F."))
          FPUTS(L_FW, "i_bDisableBackgroundimage ="+IIF(this.oParentObject.w_MASKBCKG,".F.",".T."))
          if !EMPTY(this.oParentObject.w_APPTITLE)
            FPUTS(L_FW, "g_APPTITLEAHE ='"+ALLTRIM(this.oParentObject.w_APPTITLE)+"'")
          endif
          if this.oParentObject.w_MSKINGES
            FPUTS(L_FW, "public g_OpenAut")
            FPUTS(L_FW, "g_OpenAut =.T.")
          endif
          if !this.oParentObject.w_ENMODCFG
            FPUTS(L_FW, "public g_bDisableCfgGest")
            FPUTS(L_FW, "g_bDisableCfgGest =.T.")
          endif
          if this.oParentObject.w_DBTYPE="Oracle"
            FPUTS(L_FW, "public g_ORACLEVERS")
            FPUTS(L_FW, "g_ORACLEVERS ="+ALLTRIM(this.oParentObject.w_ORACVERS))
          endif
          if !empty(this.oParentObject.w_POSQLDIR)
            FPUTS(L_FW, "public g_PATH_PGDUMP")
            FPUTS(L_FW, "g_PATH_PGDUMP ='"+ALLTRIM(this.oParentObject.w_POSQLDIR)+"'")
          endif
          FPUTS(L_FW, "public g_NOINDEXES")
          FPUTS(L_FW, "g_NOINDEXES = "+IIF(this.oParentObject.w_DISIDXCR,".T.",".F."))
          FPUTS(L_FW, "public g_NOINDEXES_INV")
          FPUTS(L_FW, "g_NOINDEXES_INV = "+IIF(this.oParentObject.w_DISIDXIN,".T.",".F."))
          if !EMPTY(NVL(this.oParentObject.w_TERMPRIN, " "))
            FPUTS(L_FW, "public g_PRTJOLLY")
            FPUTS(L_FW,'g_PRTJOLLY = "'+ALLTRIM(this.oParentObject.w_TERMPRIN)+'"')
          endif
          FPUTS(L_FW, "i_nTbBtnSpEfc ="+ALLTRIM(STR(this.oParentObject.w_FLATTOOL)))
          FPUTS(L_FW, "i_nBtnSpEfc ="+ALLTRIM(STR(this.oParentObject.w_FLATGEST)))
          FPUTS(L_FW, "i_nPrnBtnSpEfc ="+ALLTRIM(STR(this.oParentObject.w_FLATPRIN)))
          FPUTS(L_FW, "public g_CRYPTSilentConnect")
          FPUTS(L_FW, "g_CRYPTSilentConnect ="+IIF(this.oParentObject.w_CIFRA_AS,".T.",".F."))
          FCLOSE(L_FW)
        else
          addmsgNL("%1Impossibile adeterminare il tipo di database.%0Creazione file di configurazione fallita", this.w_PADRE, CHR(9))
          FCLOSE(L_FW)
          if FILE(this.w_FILECNF)
            DELETE FILE (this.w_FILECNF)
          endif
        endif
      else
        addmsgNL("%1Impossibile aprire in scrittura file di configurazione.%0Creazione file di configurazione fallita", this.w_PADRE, CHR(9))
        this.w_CNFERROR = .T.
      endif
      addmsgNL("Fine scrittura file configurazione", this.w_PADRE)
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    this.w_WBS=createobject("WScript.Shell")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
