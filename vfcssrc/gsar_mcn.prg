* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mcn                                                        *
*              Contatti                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mcn")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mcn")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mcn")
  return

* --- Class definition
define class tgsar_mcn as StdPCForm
  Width  = 728
  Height = 466
  Top    = 10
  Left   = 10
  cComment = "Contatti"
  cPrg = "gsar_mcn"
  HelpContextID=171346071
  add object cnt as tcgsar_mcn
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mcn as PCContext
  w_NCCODICE = space(15)
  w_CPROWORD = 0
  w_NCCODCON = space(5)
  w_NCPERSON = space(125)
  w_NCQUALIF = space(25)
  w_NCTELEFO = space(20)
  w_NCNUMCEL = space(20)
  w_NCCODRUO = space(5)
  w_NCTELEFO = space(20)
  w_NCNUMCEL = space(20)
  w_NCTELEF2 = space(20)
  w_NC_EMAIL = space(254)
  w_NCTELFAX = space(20)
  w_NC_EMPEC = space(254)
  w_NC_SKYPE = space(50)
  w_NC__NOTE = space(10)
  w_NCDESRUO = space(30)
  proc Save(i_oFrom)
    this.w_NCCODICE = i_oFrom.w_NCCODICE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_NCCODCON = i_oFrom.w_NCCODCON
    this.w_NCPERSON = i_oFrom.w_NCPERSON
    this.w_NCQUALIF = i_oFrom.w_NCQUALIF
    this.w_NCTELEFO = i_oFrom.w_NCTELEFO
    this.w_NCNUMCEL = i_oFrom.w_NCNUMCEL
    this.w_NCCODRUO = i_oFrom.w_NCCODRUO
    this.w_NCTELEFO = i_oFrom.w_NCTELEFO
    this.w_NCNUMCEL = i_oFrom.w_NCNUMCEL
    this.w_NCTELEF2 = i_oFrom.w_NCTELEF2
    this.w_NC_EMAIL = i_oFrom.w_NC_EMAIL
    this.w_NCTELFAX = i_oFrom.w_NCTELFAX
    this.w_NC_EMPEC = i_oFrom.w_NC_EMPEC
    this.w_NC_SKYPE = i_oFrom.w_NC_SKYPE
    this.w_NC__NOTE = i_oFrom.w_NC__NOTE
    this.w_NCDESRUO = i_oFrom.w_NCDESRUO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_NCCODICE = this.w_NCCODICE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_NCCODCON = this.w_NCCODCON
    i_oTo.w_NCPERSON = this.w_NCPERSON
    i_oTo.w_NCQUALIF = this.w_NCQUALIF
    i_oTo.w_NCTELEFO = this.w_NCTELEFO
    i_oTo.w_NCNUMCEL = this.w_NCNUMCEL
    i_oTo.w_NCCODRUO = this.w_NCCODRUO
    i_oTo.w_NCTELEFO = this.w_NCTELEFO
    i_oTo.w_NCNUMCEL = this.w_NCNUMCEL
    i_oTo.w_NCTELEF2 = this.w_NCTELEF2
    i_oTo.w_NC_EMAIL = this.w_NC_EMAIL
    i_oTo.w_NCTELFAX = this.w_NCTELFAX
    i_oTo.w_NC_EMPEC = this.w_NC_EMPEC
    i_oTo.w_NC_SKYPE = this.w_NC_SKYPE
    i_oTo.w_NC__NOTE = this.w_NC__NOTE
    i_oTo.w_NCDESRUO = this.w_NCDESRUO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mcn as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 728
  Height = 466
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-13"
  HelpContextID=171346071
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  NOM_CONT_IDX = 0
  RUO_CONT_IDX = 0
  cFile = "NOM_CONT"
  cKeySelect = "NCCODICE"
  cKeyWhere  = "NCCODICE=this.w_NCCODICE"
  cKeyDetail  = "NCCODICE=this.w_NCCODICE and NCCODCON=this.w_NCCODCON"
  cKeyWhereODBC = '"NCCODICE="+cp_ToStrODBC(this.w_NCCODICE)';

  cKeyDetailWhereODBC = '"NCCODICE="+cp_ToStrODBC(this.w_NCCODICE)';
      +'+" and NCCODCON="+cp_ToStrODBC(this.w_NCCODCON)';

  cKeyWhereODBCqualified = '"NOM_CONT.NCCODICE="+cp_ToStrODBC(this.w_NCCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'NOM_CONT.CPROWORD '
  cPrg = "gsar_mcn"
  cComment = "Contatti"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_NCCODICE = space(15)
  w_CPROWORD = 0
  w_NCCODCON = space(5)
  w_NCPERSON = space(125)
  w_NCQUALIF = space(25)
  w_NCTELEFO = space(20)
  w_NCNUMCEL = space(20)
  w_NCCODRUO = space(5)
  w_NCTELEFO = space(20)
  w_NCNUMCEL = space(20)
  w_NCTELEF2 = space(20)
  w_NC_EMAIL = space(254)
  w_NCTELFAX = space(20)
  w_NC_EMPEC = space(254)
  w_NC_SKYPE = space(50)
  w_NC__NOTE = space(0)
  w_NCDESRUO = space(30)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar_mcn
  * -- Variabile utilizzata gestire la numerazione automatica dei
  * -- contatti in AeTop, in modo che la F_BLANK non cambi il suo valore.
  w_PROGR=1
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mcnPag1","gsar_mcn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='RUO_CONT'
    this.cWorkTables[2]='NOM_CONT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.NOM_CONT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.NOM_CONT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mcn'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_7_joined
    link_2_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from NOM_CONT where NCCODICE=KeySet.NCCODICE
    *                            and NCCODCON=KeySet.NCCODCON
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2],this.bLoadRecFilter,this.NOM_CONT_IDX,"gsar_mcn")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('NOM_CONT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "NOM_CONT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' NOM_CONT '
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'NCCODICE',this.w_NCCODICE  )
      select * from (i_cTable) NOM_CONT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_NCCODICE = NVL(NCCODICE,space(15))
        cp_LoadRecExtFlds(this,'NOM_CONT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_NCDESRUO = space(30)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_NCCODCON = NVL(NCCODCON,space(5))
          .w_NCPERSON = NVL(NCPERSON,space(125))
          .w_NCQUALIF = NVL(NCQUALIF,space(25))
          .w_NCTELEFO = NVL(NCTELEFO,space(20))
          .w_NCNUMCEL = NVL(NCNUMCEL,space(20))
          .w_NCCODRUO = NVL(NCCODRUO,space(5))
          if link_2_7_joined
            this.w_NCCODRUO = NVL(RCCODICE207,NVL(this.w_NCCODRUO,space(5)))
            this.w_NCDESRUO = NVL(RCDESCRI207,space(30))
          else
          .link_2_7('Load')
          endif
          .w_NCTELEFO = NVL(NCTELEFO,space(20))
          .w_NCNUMCEL = NVL(NCNUMCEL,space(20))
          .w_NCTELEF2 = NVL(NCTELEF2,space(20))
          .w_NC_EMAIL = NVL(NC_EMAIL,space(254))
          .w_NCTELFAX = NVL(NCTELFAX,space(20))
          .w_NC_EMPEC = NVL(NC_EMPEC,space(254))
          .w_NC_SKYPE = NVL(NC_SKYPE,space(50))
          .w_NC__NOTE = NVL(NC__NOTE,space(0))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace NCCODCON with .w_NCCODCON
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_17.enabled = .oPgFrm.Page1.oPag.oBtn_2_17.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_18.enabled = .oPgFrm.Page1.oPag.oBtn_2_18.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_19.enabled = .oPgFrm.Page1.oPag.oBtn_2_19.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_20.enabled = .oPgFrm.Page1.oPag.oBtn_2_20.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_21.enabled = .oPgFrm.Page1.oPag.oBtn_2_21.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_22.enabled = .oPgFrm.Page1.oPag.oBtn_2_22.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_NCCODICE=space(15)
      .w_CPROWORD=10
      .w_NCCODCON=space(5)
      .w_NCPERSON=space(125)
      .w_NCQUALIF=space(25)
      .w_NCTELEFO=space(20)
      .w_NCNUMCEL=space(20)
      .w_NCCODRUO=space(5)
      .w_NCTELEFO=space(20)
      .w_NCNUMCEL=space(20)
      .w_NCTELEF2=space(20)
      .w_NC_EMAIL=space(254)
      .w_NCTELFAX=space(20)
      .w_NC_EMPEC=space(254)
      .w_NC_SKYPE=space(50)
      .w_NC__NOTE=space(0)
      .w_NCDESRUO=space(30)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,2,.f.)
        .w_NCCODCON = IIF(IsAlt(), PADL(alltrim(str(.w_PROGR, 5, 0)), 5, '0'), SPACE(5))
        .DoRTCalc(4,8,.f.)
        if not(empty(.w_NCCODRUO))
         .link_2_7('Full')
        endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'NOM_CONT')
    this.DoRTCalc(9,17,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_17.enabled = this.oPgFrm.Page1.oPag.oBtn_2_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_18.enabled = this.oPgFrm.Page1.oPag.oBtn_2_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_19.enabled = this.oPgFrm.Page1.oPag.oBtn_2_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_20.enabled = this.oPgFrm.Page1.oPag.oBtn_2_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_21.enabled = this.oPgFrm.Page1.oPag.oBtn_2_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_22.enabled = this.oPgFrm.Page1.oPag.oBtn_2_22.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oNCCODRUO_2_7.enabled = i_bVal
      .Page1.oPag.oNCTELEFO_2_8.enabled = i_bVal
      .Page1.oPag.oNCNUMCEL_2_9.enabled = i_bVal
      .Page1.oPag.oNCTELEF2_2_10.enabled = i_bVal
      .Page1.oPag.oNC_EMAIL_2_11.enabled = i_bVal
      .Page1.oPag.oNCTELFAX_2_12.enabled = i_bVal
      .Page1.oPag.oNC_EMPEC_2_13.enabled = i_bVal
      .Page1.oPag.oNC_SKYPE_2_14.enabled = i_bVal
      .Page1.oPag.oNC__NOTE_2_15.enabled = i_bVal
      .Page1.oPag.oBtn_2_17.enabled = .Page1.oPag.oBtn_2_17.mCond()
      .Page1.oPag.oBtn_2_18.enabled = .Page1.oPag.oBtn_2_18.mCond()
      .Page1.oPag.oBtn_2_19.enabled = .Page1.oPag.oBtn_2_19.mCond()
      .Page1.oPag.oBtn_2_20.enabled = .Page1.oPag.oBtn_2_20.mCond()
      .Page1.oPag.oBtn_2_21.enabled = .Page1.oPag.oBtn_2_21.mCond()
      .Page1.oPag.oBtn_2_22.enabled = .Page1.oPag.oBtn_2_22.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'NOM_CONT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NCCODICE,"NCCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_NCCODCON C(5);
      ,t_NCPERSON C(125);
      ,t_NCQUALIF C(25);
      ,t_NCTELEFO C(20);
      ,t_NCNUMCEL C(20);
      ,t_NCCODRUO C(5);
      ,t_NCTELEF2 C(20);
      ,t_NC_EMAIL C(254);
      ,t_NCTELFAX C(20);
      ,t_NC_EMPEC C(254);
      ,t_NC_SKYPE C(50);
      ,t_NC__NOTE M(10);
      ,t_NCDESRUO C(30);
      ,NCCODCON C(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mcnbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNCCODCON_2_2.controlsource=this.cTrsName+'.t_NCCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNCPERSON_2_3.controlsource=this.cTrsName+'.t_NCPERSON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNCQUALIF_2_4.controlsource=this.cTrsName+'.t_NCQUALIF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNCTELEFO_2_5.controlsource=this.cTrsName+'.t_NCTELEFO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNCNUMCEL_2_6.controlsource=this.cTrsName+'.t_NCNUMCEL'
    this.oPgFRm.Page1.oPag.oNCCODRUO_2_7.controlsource=this.cTrsName+'.t_NCCODRUO'
    this.oPgFRm.Page1.oPag.oNCTELEFO_2_8.controlsource=this.cTrsName+'.t_NCTELEFO'
    this.oPgFRm.Page1.oPag.oNCNUMCEL_2_9.controlsource=this.cTrsName+'.t_NCNUMCEL'
    this.oPgFRm.Page1.oPag.oNCTELEF2_2_10.controlsource=this.cTrsName+'.t_NCTELEF2'
    this.oPgFRm.Page1.oPag.oNC_EMAIL_2_11.controlsource=this.cTrsName+'.t_NC_EMAIL'
    this.oPgFRm.Page1.oPag.oNCTELFAX_2_12.controlsource=this.cTrsName+'.t_NCTELFAX'
    this.oPgFRm.Page1.oPag.oNC_EMPEC_2_13.controlsource=this.cTrsName+'.t_NC_EMPEC'
    this.oPgFRm.Page1.oPag.oNC_SKYPE_2_14.controlsource=this.cTrsName+'.t_NC_SKYPE'
    this.oPgFRm.Page1.oPag.oNC__NOTE_2_15.controlsource=this.cTrsName+'.t_NC__NOTE'
    this.oPgFRm.Page1.oPag.oNCDESRUO_2_16.controlsource=this.cTrsName+'.t_NCDESRUO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(60)
    this.AddVLine(128)
    this.AddVLine(278)
    this.AddVLine(414)
    this.AddVLine(558)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
      *
      * insert into NOM_CONT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'NOM_CONT')
        i_extval=cp_InsertValODBCExtFlds(this,'NOM_CONT')
        i_cFldBody=" "+;
                  "(NCCODICE,CPROWORD,NCCODCON,NCPERSON,NCQUALIF"+;
                  ",NCTELEFO,NCNUMCEL,NCCODRUO,NCTELEF2,NC_EMAIL"+;
                  ",NCTELFAX,NC_EMPEC,NC_SKYPE,NC__NOTE,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_NCCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_NCCODCON)+","+cp_ToStrODBC(this.w_NCPERSON)+","+cp_ToStrODBC(this.w_NCQUALIF)+;
             ","+cp_ToStrODBC(this.w_NCTELEFO)+","+cp_ToStrODBC(this.w_NCNUMCEL)+","+cp_ToStrODBCNull(this.w_NCCODRUO)+","+cp_ToStrODBC(this.w_NCTELEF2)+","+cp_ToStrODBC(this.w_NC_EMAIL)+;
             ","+cp_ToStrODBC(this.w_NCTELFAX)+","+cp_ToStrODBC(this.w_NC_EMPEC)+","+cp_ToStrODBC(this.w_NC_SKYPE)+","+cp_ToStrODBC(this.w_NC__NOTE)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'NOM_CONT')
        i_extval=cp_InsertValVFPExtFlds(this,'NOM_CONT')
        cp_CheckDeletedKey(i_cTable,0,'NCCODICE',this.w_NCCODICE,'NCCODCON',this.w_NCCODCON)
        INSERT INTO (i_cTable) (;
                   NCCODICE;
                  ,CPROWORD;
                  ,NCCODCON;
                  ,NCPERSON;
                  ,NCQUALIF;
                  ,NCTELEFO;
                  ,NCNUMCEL;
                  ,NCCODRUO;
                  ,NCTELEF2;
                  ,NC_EMAIL;
                  ,NCTELFAX;
                  ,NC_EMPEC;
                  ,NC_SKYPE;
                  ,NC__NOTE;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_NCCODICE;
                  ,this.w_CPROWORD;
                  ,this.w_NCCODCON;
                  ,this.w_NCPERSON;
                  ,this.w_NCQUALIF;
                  ,this.w_NCTELEFO;
                  ,this.w_NCNUMCEL;
                  ,this.w_NCCODRUO;
                  ,this.w_NCTELEF2;
                  ,this.w_NC_EMAIL;
                  ,this.w_NCTELFAX;
                  ,this.w_NC_EMPEC;
                  ,this.w_NC_SKYPE;
                  ,this.w_NC__NOTE;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_NCCODCON) and Empty(t_NCPERSON))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'NOM_CONT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and NCCODCON="+cp_ToStrODBC(&i_TN.->NCCODCON)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'NOM_CONT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and NCCODCON=&i_TN.->NCCODCON;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_NCCODCON) and Empty(t_NCPERSON))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and NCCODCON="+cp_ToStrODBC(&i_TN.->NCCODCON)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and NCCODCON=&i_TN.->NCCODCON;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace NCCODCON with this.w_NCCODCON
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update NOM_CONT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'NOM_CONT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",NCPERSON="+cp_ToStrODBC(this.w_NCPERSON)+;
                     ",NCQUALIF="+cp_ToStrODBC(this.w_NCQUALIF)+;
                     ",NCTELEFO="+cp_ToStrODBC(this.w_NCTELEFO)+;
                     ",NCNUMCEL="+cp_ToStrODBC(this.w_NCNUMCEL)+;
                     ",NCCODRUO="+cp_ToStrODBCNull(this.w_NCCODRUO)+;
                     ",NCTELEF2="+cp_ToStrODBC(this.w_NCTELEF2)+;
                     ",NC_EMAIL="+cp_ToStrODBC(this.w_NC_EMAIL)+;
                     ",NCTELFAX="+cp_ToStrODBC(this.w_NCTELFAX)+;
                     ",NC_EMPEC="+cp_ToStrODBC(this.w_NC_EMPEC)+;
                     ",NC_SKYPE="+cp_ToStrODBC(this.w_NC_SKYPE)+;
                     ",NC__NOTE="+cp_ToStrODBC(this.w_NC__NOTE)+;
                     ",NCCODCON="+cp_ToStrODBC(this.w_NCCODCON)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and NCCODCON="+cp_ToStrODBC(NCCODCON)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'NOM_CONT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,NCPERSON=this.w_NCPERSON;
                     ,NCQUALIF=this.w_NCQUALIF;
                     ,NCTELEFO=this.w_NCTELEFO;
                     ,NCNUMCEL=this.w_NCNUMCEL;
                     ,NCCODRUO=this.w_NCCODRUO;
                     ,NCTELEF2=this.w_NCTELEF2;
                     ,NC_EMAIL=this.w_NC_EMAIL;
                     ,NCTELFAX=this.w_NCTELFAX;
                     ,NC_EMPEC=this.w_NC_EMPEC;
                     ,NC_SKYPE=this.w_NC_SKYPE;
                     ,NC__NOTE=this.w_NC__NOTE;
                     ,NCCODCON=this.w_NCCODCON;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and NCCODCON=&i_TN.->NCCODCON;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_NCCODCON) and Empty(t_NCPERSON))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete NOM_CONT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and NCCODCON="+cp_ToStrODBC(&i_TN.->NCCODCON)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and NCCODCON=&i_TN.->NCCODCON;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_NCCODCON) and Empty(t_NCPERSON))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_TVGZOYBACV()
    with this
          * --- Incrementa w_PROGR
          .w_PROGR = .w_PROGR+1
    endwith
  endproc
  proc Calculate_RUEZIWRWOW()
    with this
          * --- Inizializza w_NCCODCON
          .w_NCCODCON = IIF(IsAlt(), PADL(alltrim(str(.w_PROGR, 5, 0)), 5, '0'), .w_NCCODCON)
    endwith
  endproc
  proc Calculate_CHMYWYHTDO()
    with this
          * --- Inizializza w_PROGR
          GSAR_BTT(this;
              ,'LETTURA';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oNCCODCON_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oNCCODCON_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_17.enabled =this.oPgFrm.Page1.oPag.oBtn_2_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_18.enabled =this.oPgFrm.Page1.oPag.oBtn_2_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_19.enabled =this.oPgFrm.Page1.oPag.oBtn_2_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_20.enabled =this.oPgFrm.Page1.oPag.oBtn_2_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_21.enabled =this.oPgFrm.Page1.oPag.oBtn_2_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_22.enabled =this.oPgFrm.Page1.oPag.oBtn_2_22.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oNCCODRUO_2_7.visible=!this.oPgFrm.Page1.oPag.oNCCODRUO_2_7.mHide()
    this.oPgFrm.Page1.oPag.oNCDESRUO_2_16.visible=!this.oPgFrm.Page1.oPag.oNCDESRUO_2_16.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_18.visible=!this.oPgFrm.Page1.oPag.oBtn_2_18.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_19.visible=!this.oPgFrm.Page1.oPag.oBtn_2_19.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_20.visible=!this.oPgFrm.Page1.oPag.oBtn_2_20.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_21.visible=!this.oPgFrm.Page1.oPag.oBtn_2_21.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Init Row")
          .Calculate_TVGZOYBACV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init Row")
          .Calculate_RUEZIWRWOW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_CHMYWYHTDO()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NCCODRUO
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RUO_CONT_IDX,3]
    i_lTable = "RUO_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2], .t., this.RUO_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NCCODRUO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ARC',True,'RUO_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RCCODICE like "+cp_ToStrODBC(trim(this.w_NCCODRUO)+"%");

          i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RCCODICE',trim(this.w_NCCODRUO))
          select RCCODICE,RCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NCCODRUO)==trim(_Link_.RCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NCCODRUO) and !this.bDontReportError
            deferred_cp_zoom('RUO_CONT','*','RCCODICE',cp_AbsName(oSource.parent,'oNCCODRUO_2_7'),i_cWhere,'GSAR_ARC',"Ruoli contatti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',oSource.xKey(1))
            select RCCODICE,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NCCODRUO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(this.w_NCCODRUO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',this.w_NCCODRUO)
            select RCCODICE,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NCCODRUO = NVL(_Link_.RCCODICE,space(5))
      this.w_NCDESRUO = NVL(_Link_.RCDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_NCCODRUO = space(5)
      endif
      this.w_NCDESRUO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2])+'\'+cp_ToStr(_Link_.RCCODICE,1)
      cp_ShowWarn(i_cKey,this.RUO_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NCCODRUO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.RUO_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.RCCODICE as RCCODICE207"+ ",link_2_7.RCDESCRI as RCDESCRI207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on NOM_CONT.NCCODRUO=link_2_7.RCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and NOM_CONT.NCCODRUO=link_2_7.RCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oNCCODRUO_2_7.value==this.w_NCCODRUO)
      this.oPgFrm.Page1.oPag.oNCCODRUO_2_7.value=this.w_NCCODRUO
      replace t_NCCODRUO with this.oPgFrm.Page1.oPag.oNCCODRUO_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNCTELEFO_2_8.value==this.w_NCTELEFO)
      this.oPgFrm.Page1.oPag.oNCTELEFO_2_8.value=this.w_NCTELEFO
      replace t_NCTELEFO with this.oPgFrm.Page1.oPag.oNCTELEFO_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNCNUMCEL_2_9.value==this.w_NCNUMCEL)
      this.oPgFrm.Page1.oPag.oNCNUMCEL_2_9.value=this.w_NCNUMCEL
      replace t_NCNUMCEL with this.oPgFrm.Page1.oPag.oNCNUMCEL_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNCTELEF2_2_10.value==this.w_NCTELEF2)
      this.oPgFrm.Page1.oPag.oNCTELEF2_2_10.value=this.w_NCTELEF2
      replace t_NCTELEF2 with this.oPgFrm.Page1.oPag.oNCTELEF2_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNC_EMAIL_2_11.value==this.w_NC_EMAIL)
      this.oPgFrm.Page1.oPag.oNC_EMAIL_2_11.value=this.w_NC_EMAIL
      replace t_NC_EMAIL with this.oPgFrm.Page1.oPag.oNC_EMAIL_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNCTELFAX_2_12.value==this.w_NCTELFAX)
      this.oPgFrm.Page1.oPag.oNCTELFAX_2_12.value=this.w_NCTELFAX
      replace t_NCTELFAX with this.oPgFrm.Page1.oPag.oNCTELFAX_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNC_EMPEC_2_13.value==this.w_NC_EMPEC)
      this.oPgFrm.Page1.oPag.oNC_EMPEC_2_13.value=this.w_NC_EMPEC
      replace t_NC_EMPEC with this.oPgFrm.Page1.oPag.oNC_EMPEC_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNC_SKYPE_2_14.value==this.w_NC_SKYPE)
      this.oPgFrm.Page1.oPag.oNC_SKYPE_2_14.value=this.w_NC_SKYPE
      replace t_NC_SKYPE with this.oPgFrm.Page1.oPag.oNC_SKYPE_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNC__NOTE_2_15.value==this.w_NC__NOTE)
      this.oPgFrm.Page1.oPag.oNC__NOTE_2_15.value=this.w_NC__NOTE
      replace t_NC__NOTE with this.oPgFrm.Page1.oPag.oNC__NOTE_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNCDESRUO_2_16.value==this.w_NCDESRUO)
      this.oPgFrm.Page1.oPag.oNCDESRUO_2_16.value=this.w_NCDESRUO
      replace t_NCDESRUO with this.oPgFrm.Page1.oPag.oNCDESRUO_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCCODCON_2_2.value==this.w_NCCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCCODCON_2_2.value=this.w_NCCODCON
      replace t_NCCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCCODCON_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCPERSON_2_3.value==this.w_NCPERSON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCPERSON_2_3.value=this.w_NCPERSON
      replace t_NCPERSON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCPERSON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCQUALIF_2_4.value==this.w_NCQUALIF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCQUALIF_2_4.value=this.w_NCQUALIF
      replace t_NCQUALIF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCQUALIF_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCTELEFO_2_5.value==this.w_NCTELEFO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCTELEFO_2_5.value=this.w_NCTELEFO
      replace t_NCTELEFO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCTELEFO_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCNUMCEL_2_6.value==this.w_NCNUMCEL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCNUMCEL_2_6.value=this.w_NCNUMCEL
      replace t_NCNUMCEL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCNUMCEL_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'NOM_CONT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_NCCODCON) and (NOT IsAlt()) and (not(Empty(.w_NCCODCON) and Empty(.w_NCPERSON)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCCODCON_2_2
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   empty(.w_NCPERSON) and (not(Empty(.w_NCCODCON) and Empty(.w_NCPERSON)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNCPERSON_2_3
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_NCCODCON) and Empty(.w_NCPERSON))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_NCCODCON) and Empty(t_NCPERSON)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_NCCODCON=space(5)
      .w_NCPERSON=space(125)
      .w_NCQUALIF=space(25)
      .w_NCTELEFO=space(20)
      .w_NCNUMCEL=space(20)
      .w_NCCODRUO=space(5)
      .w_NCTELEFO=space(20)
      .w_NCNUMCEL=space(20)
      .w_NCTELEF2=space(20)
      .w_NC_EMAIL=space(254)
      .w_NCTELFAX=space(20)
      .w_NC_EMPEC=space(254)
      .w_NC_SKYPE=space(50)
      .w_NC__NOTE=space(0)
      .w_NCDESRUO=space(30)
      .DoRTCalc(1,2,.f.)
        .w_NCCODCON = IIF(IsAlt(), PADL(alltrim(str(.w_PROGR, 5, 0)), 5, '0'), SPACE(5))
      .DoRTCalc(4,8,.f.)
      if not(empty(.w_NCCODRUO))
        .link_2_7('Full')
      endif
    endwith
    this.DoRTCalc(9,17,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_NCCODCON = t_NCCODCON
    this.w_NCPERSON = t_NCPERSON
    this.w_NCQUALIF = t_NCQUALIF
    this.w_NCTELEFO = t_NCTELEFO
    this.w_NCNUMCEL = t_NCNUMCEL
    this.w_NCCODRUO = t_NCCODRUO
    this.w_NCTELEFO = t_NCTELEFO
    this.w_NCNUMCEL = t_NCNUMCEL
    this.w_NCTELEF2 = t_NCTELEF2
    this.w_NC_EMAIL = t_NC_EMAIL
    this.w_NCTELFAX = t_NCTELFAX
    this.w_NC_EMPEC = t_NC_EMPEC
    this.w_NC_SKYPE = t_NC_SKYPE
    this.w_NC__NOTE = t_NC__NOTE
    this.w_NCDESRUO = t_NCDESRUO
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_NCCODCON with this.w_NCCODCON
    replace t_NCPERSON with this.w_NCPERSON
    replace t_NCQUALIF with this.w_NCQUALIF
    replace t_NCTELEFO with this.w_NCTELEFO
    replace t_NCNUMCEL with this.w_NCNUMCEL
    replace t_NCCODRUO with this.w_NCCODRUO
    replace t_NCTELEFO with this.w_NCTELEFO
    replace t_NCNUMCEL with this.w_NCNUMCEL
    replace t_NCTELEF2 with this.w_NCTELEF2
    replace t_NC_EMAIL with this.w_NC_EMAIL
    replace t_NCTELFAX with this.w_NCTELFAX
    replace t_NC_EMPEC with this.w_NC_EMPEC
    replace t_NC_SKYPE with this.w_NC_SKYPE
    replace t_NC__NOTE with this.w_NC__NOTE
    replace t_NCDESRUO with this.w_NCDESRUO
    if i_srv='A'
      replace NCCODCON with this.w_NCCODCON
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mcnPag1 as StdContainer
  Width  = 724
  height = 466
  stdWidth  = 724
  stdheight = 466
  resizeXpos=462
  resizeYpos=76
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=3, width=712,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1=" Riga ",Field2="NCCODCON",Label2=" Codice ",Field3="NCPERSON",Label3=" Riferimento persona ",Field4="NCQUALIF",Label4=" Qualifica ",Field5="NCTELEFO",Label5=" Telefono ",Field6="NCNUMCEL",Label6=" Cellulare ",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49433990

  add object oStr_1_3 as StdString with uid="TTYDNBNHBB",Visible=.t., Left=8, Top=218,;
    Alignment=1, Width=103, Height=18,;
    Caption="Ruolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S'  or Isalt())
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="SVARONBDDV",Visible=.t., Left=446, Top=242,;
    Alignment=1, Width=103, Height=18,;
    Caption="Altro telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="FBYPEYLKBT",Visible=.t., Left=446, Top=269,;
    Alignment=1, Width=103, Height=18,;
    Caption="Telefax:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="FAGNCLDJXN",Visible=.t., Left=8, Top=268,;
    Alignment=1, Width=103, Height=18,;
    Caption="E.Mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="MGMXUDREME",Visible=.t., Left=446, Top=216,;
    Alignment=1, Width=103, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="GLSJMAOIYN",Visible=.t., Left=8, Top=243,;
    Alignment=1, Width=103, Height=18,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="THIJBXCXRV",Visible=.t., Left=448, Top=293,;
    Alignment=1, Width=101, Height=18,;
    Caption="Skype:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="KGEDQHSPRS",Visible=.t., Left=8, Top=293,;
    Alignment=1, Width=103, Height=18,;
    Caption="PEC:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=22,;
    width=705+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=23,width=704+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oNCCODRUO_2_7.Refresh()
      this.Parent.oNCTELEFO_2_8.Refresh()
      this.Parent.oNCNUMCEL_2_9.Refresh()
      this.Parent.oNCTELEF2_2_10.Refresh()
      this.Parent.oNC_EMAIL_2_11.Refresh()
      this.Parent.oNCTELFAX_2_12.Refresh()
      this.Parent.oNC_EMPEC_2_13.Refresh()
      this.Parent.oNC_SKYPE_2_14.Refresh()
      this.Parent.oNC__NOTE_2_15.Refresh()
      this.Parent.oNCDESRUO_2_16.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oNCCODRUO_2_7 as StdTrsField with uid="TIHDXFXEKY",rtseq=8,rtrep=.t.,;
    cFormVar="w_NCCODRUO",value=space(5),;
    ToolTipText = "Codice ruolo ricoperto",;
    HelpContextID = 255196891,;
    cTotal="", bFixedPos=.t., cQueryName = "NCCODRUO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=114, Top=218, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="RUO_CONT", cZoomOnZoom="GSAR_ARC", oKey_1_1="RCCODICE", oKey_1_2="this.w_NCCODRUO"

  func oNCCODRUO_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_OFFE<>'S' or Isalt())
    endwith
    endif
  endfunc

  func oNCCODRUO_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oNCCODRUO_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oNCCODRUO_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'RUO_CONT','*','RCCODICE',cp_AbsName(this.parent,'oNCCODRUO_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ARC',"Ruoli contatti",'',this.parent.oContained
  endproc
  proc oNCCODRUO_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ARC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RCCODICE=this.parent.oContained.w_NCCODRUO
    i_obj.ecpSave()
  endproc

  add object oNCTELEFO_2_8 as StdTrsField with uid="IBHCUWKVCZ",rtseq=9,rtrep=.t.,;
    cFormVar="w_NCTELEFO",value=space(20),;
    ToolTipText = "Numero di telefono primario",;
    HelpContextID = 197062363,;
    cTotal="", bFixedPos=.t., cQueryName = "NCTELEFO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=553, Top=215, InputMask=replicate('X',20)

  add object oNCNUMCEL_2_9 as StdTrsField with uid="NRVBPUQPHF",rtseq=10,rtrep=.t.,;
    cFormVar="w_NCNUMCEL",value=space(20),;
    ToolTipText = "Numero cellulare",;
    HelpContextID = 228544222,;
    cTotal="", bFixedPos=.t., cQueryName = "NCNUMCEL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=114, Top=242, InputMask=replicate('X',20)

  add object oNCTELEF2_2_10 as StdTrsField with uid="BNRJTTXCPO",rtseq=11,rtrep=.t.,;
    cFormVar="w_NCTELEF2",value=space(20),;
    ToolTipText = "Numero di telefono secondario",;
    HelpContextID = 197062392,;
    cTotal="", bFixedPos=.t., cQueryName = "NCTELEF2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=553, Top=242, InputMask=replicate('X',20)

  add object oNC_EMAIL_2_11 as StdTrsField with uid="JSDLCREAZZ",rtseq=12,rtrep=.t.,;
    cFormVar="w_NC_EMAIL",value=space(254),;
    ToolTipText = "Indirizzo di posta elettronica",;
    HelpContextID = 5357858,;
    cTotal="", bFixedPos=.t., cQueryName = "NC_EMAIL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=114, Top=266, InputMask=replicate('X',254)

  add object oNCTELFAX_2_12 as StdTrsField with uid="TSJSDAXFVR",rtseq=13,rtrep=.t.,;
    cFormVar="w_NCTELFAX",value=space(20),;
    ToolTipText = "Numero di telefax",;
    HelpContextID = 180285138,;
    cTotal="", bFixedPos=.t., cQueryName = "NCTELFAX",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=553, Top=266, InputMask=replicate('X',20)

  add object oNC_EMPEC_2_13 as StdTrsField with uid="SDNYJTSHLK",rtseq=14,rtrep=.t.,;
    cFormVar="w_NC_EMPEC",value=space(254),;
    ToolTipText = "Indirizzo di posta elettronica certificata (PEC)",;
    HelpContextID = 11419367,;
    cTotal="", bFixedPos=.t., cQueryName = "NC_EMPEC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=114, Top=290, InputMask=replicate('X',254)

  add object oNC_SKYPE_2_14 as StdTrsField with uid="KQYURIJCFP",rtseq=15,rtrep=.t.,;
    cFormVar="w_NC_SKYPE",value=space(50),;
    ToolTipText = "Codice utente Skype",;
    HelpContextID = 130039525,;
    cTotal="", bFixedPos=.t., cQueryName = "NC_SKYPE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=553, Top=290, InputMask=replicate('X',50)

  add object oNC__NOTE_2_15 as StdTrsMemo with uid="LIXDSKGOUG",rtseq=16,rtrep=.t.,;
    cFormVar="w_NC__NOTE",value=space(0),;
    ToolTipText = "Note aggiuntive",;
    HelpContextID = 25444069,;
    cTotal="", bFixedPos=.t., cQueryName = "NC__NOTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=145, Width=688, Left=11, Top=315

  add object oNCDESRUO_2_16 as StdTrsField with uid="GKKPPURAGM",rtseq=17,rtrep=.t.,;
    cFormVar="w_NCDESRUO",value=space(30),enabled=.f.,;
    ToolTipText = "Descrizione ruolo",;
    HelpContextID = 240119515,;
    cTotal="", bFixedPos=.t., cQueryName = "NCDESRUO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=180, Top=218, InputMask=replicate('X',30)

  func oNCDESRUO_2_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_OFFE<>'S' or Isalt())
    endwith
    endif
  endfunc

  add object oBtn_2_17 as StdButton with uid="XEUWBAPBCE",width=22,height=22,;
   left=414, top=267,;
    CpPicture="BMP\BTSEND.bmp", caption="", nPag=2;
    , ToolTipText = "Manda una e-mail all'indirizzo specificato";
    , HelpContextID = 23464518;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_17.Click()
      with this.Parent.oContained
        MAILTO(.w_NC_EMAIL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_17.mCond()
    with this.Parent.oContained
      return (!empty(.w_NC_EMAIL))
    endwith
  endfunc

  add object oBtn_2_18 as StdButton with uid="CBQFGDALDQ",width=22,height=22,;
   left=694, top=215,;
    CpPicture="BMP\PHONE.bmp", caption="", nPag=2;
    , ToolTipText = "Permette di effettuare chiamate al numero indicato";
    , HelpContextID = 16681766;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_18.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_NCTELEFO, "MN", .w_NCCODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_18.mCond()
    with this.Parent.oContained
      return (!empty(.w_NCTELEFO) And (g_SkypeService='C' Or !Empty(g_PhoneService)))
    endwith
  endfunc

  func oBtn_2_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NCTELEFO) Or (g_SkypeService<>'C' And Empty(g_PhoneService)))
    endwith
   endif
  endfunc

  add object oBtn_2_19 as StdButton with uid="NWYDAFDXUD",width=22,height=22,;
   left=262, top=243,;
    CpPicture="BMP\PHONE.bmp", caption="", nPag=2;
    , ToolTipText = "Permette di effettuare chiamate al numero indicato";
    , HelpContextID = 16681766;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_19.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_NCNUMCEL, "MM", .w_NCCODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_19.mCond()
    with this.Parent.oContained
      return (!empty(.w_NCNUMCEL) And (g_SkypeService='C' Or !Empty(g_PhoneService)))
    endwith
  endfunc

  func oBtn_2_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NCNUMCEL) Or (g_SkypeService<>'C' And Empty(g_PhoneService)))
    endwith
   endif
  endfunc

  add object oBtn_2_20 as StdButton with uid="LRLBVOBPLQ",width=22,height=22,;
   left=694, top=242,;
    CpPicture="BMP\PHONE.bmp", caption="", nPag=2;
    , ToolTipText = "Permette di effettuare chiamate al numero indicato";
    , HelpContextID = 16681766;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_20.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_NCTELEF2, "MN", .w_NCCODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_20.mCond()
    with this.Parent.oContained
      return (!empty(.w_NCTELEF2) And (g_SkypeService='C' Or !Empty(g_PhoneService)))
    endwith
  endfunc

  func oBtn_2_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NCTELEF2) Or (g_SkypeService<>'C' And Empty(g_PhoneService)))
    endwith
   endif
  endfunc

  add object oBtn_2_21 as StdButton with uid="MSFMETDRTQ",width=22,height=22,;
   left=694, top=290,;
    CpPicture="BMP\SKYPE.bmp", caption="", nPag=2;
    , ToolTipText = "Permette di effettuare chiamate via Skype al numero indicato";
    , HelpContextID = 16681766;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_21.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_NC_SKYPE, "CC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_21.mCond()
    with this.Parent.oContained
      return (!empty(.w_NC_SKYPE) AND g_SkypeService<>'N')
    endwith
  endfunc

  func oBtn_2_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NC_SKYPE) OR g_SkypeService='N')
    endwith
   endif
  endfunc

  add object oBtn_2_22 as StdButton with uid="UFWUDLAHFY",width=22,height=22,;
   left=414, top=290,;
    CpPicture="BMP\fxmail_pec.bmp", caption="", nPag=2;
    , ToolTipText = "Manda una e-mail PEC all'indirizzo specificato";
    , HelpContextID = 171639542;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_22.Click()
      with this.Parent.oContained
        MAILTO(.w_NC_EMPEC,,,.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_22.mCond()
    with this.Parent.oContained
      return (!empty(.w_NC_EMPEC))
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mcnBodyRow as CPBodyRowCnt
  Width=695
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="PBMEZKLUKW",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 17105558,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=-2, Top=0

  add object oNCCODCON_2_2 as StdTrsField with uid="NSBIQYIXDF",rtseq=3,rtrep=.t.,;
    cFormVar="w_NCCODCON",value=space(5),isprimarykey=.t.,;
    ToolTipText = "Codice contatto",;
    HelpContextID = 238419676,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=64, Left=53, Top=0, cSayPict=[IIF(IsAlt(),'99999','')], cGetPict=[IIF(IsAlt(),'99999','')], InputMask=replicate('X',5)

  func oNCCODCON_2_2.mCond()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oNCPERSON_2_3 as StdTrsField with uid="TJLRBCLGAH",rtseq=4,rtrep=.t.,;
    cFormVar="w_NCPERSON",value=space(125),;
    ToolTipText = "Nominativo della persona di riferimento",;
    HelpContextID = 224341724,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=146, Left=122, Top=0, InputMask=replicate('X',125)

  add object oNCQUALIF_2_4 as StdTrsField with uid="IFPYAAWHFJ",rtseq=5,rtrep=.t.,;
    cFormVar="w_NCQUALIF",value=space(25),;
    ToolTipText = "Qualifica delle persona",;
    HelpContextID = 178315548,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=271, Top=0, InputMask=replicate('X',25)

  add object oNCTELEFO_2_5 as StdTrsField with uid="SRITRVOXFZ",rtseq=6,rtrep=.t.,;
    cFormVar="w_NCTELEFO",value=space(20),;
    ToolTipText = "Numero di telefono primario",;
    HelpContextID = 197062363,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=408, Top=0, InputMask=replicate('X',20)

  add object oNCNUMCEL_2_6 as StdTrsField with uid="ZLDWQVWSXJ",rtseq=7,rtrep=.t.,;
    cFormVar="w_NCNUMCEL",value=space(20),;
    ToolTipText = "Numero cellulare",;
    HelpContextID = 228544222,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=551, Top=0, InputMask=replicate('X',20)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mcn','NOM_CONT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".NCCODICE=NOM_CONT.NCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
