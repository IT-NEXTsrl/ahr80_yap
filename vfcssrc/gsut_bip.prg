* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bip                                                        *
*              Inserimento postit                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-06-09                                                      *
* Last revis.: 2014-11-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pDESTINATARIO,pMESSAGGIO,pALLEGATI,pBOTTONE,pRETCOD
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bip",oParentObject,m.pDESTINATARIO,m.pMESSAGGIO,m.pALLEGATI,m.pBOTTONE,m.pRETCOD)
return(i_retval)

define class tgsut_bip as StdBatch
  * --- Local variables
  pDESTINATARIO = 0
  pMESSAGGIO = space(0)
  pALLEGATI = space(0)
  pBOTTONE = space(0)
  pRETCOD = .f.
  w_DESTINATARIO = 0
  w_MESSAGGIO = space(0)
  w_ALLEGATI = space(0)
  w_BOTTONE = space(0)
  w_CODE = space(10)
  w_USERCODE = 0
  w_CREATEDBY = 0
  w_STATUS = space(1)
  w_ACAPO = space(2)
  w_VIRGOLETTE = space(1)
  w_APERTAQUADRA = space(1)
  w_CHIUSAQUADRA = space(1)
  w_ASTERISCO = space(1)
  w_POSTIT = space(0)
  w_NUMEROBOTTONI = 0
  w_ALTEZZA = 0
  w_LARGHEZZA = 0
  w_NUMEROALLEGATI = 0
  w_CONTATORE = 0
  w_FILE = space(254)
  w_NOMEDELFILE = space(254)
  w_INIZIOFILE = 0
  w_FINEFILE = 0
  w_COLORE = 0
  w_WI = 0
  w_HE = 0
  w_PX = 0
  w_PY = 0
  w_DATESTART = ctod("  /  /  ")
  w_CREATED = ctot("")
  w_FRASEDIINSERT = space(10)
  * --- WorkFile variables
  AZIENDA_idx=0
  CONF_INT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametri:
    *     pDestinatario: utente destinatario
    *     pMessaggio: messaggio
    *     pAllegati: elenco degli allegati separati da ';'
    *     pBottone: bottone che apre gestione
    this.w_DESTINATARIO = this.pDESTINATARIO
    * --- Se il messaggio contiene degli invii a capo ( chr(13) oppure chr(13)+chr(10) ) , devono essere sostituiti con il carattere "�" ASC(255)
    this.w_MESSAGGIO = STRTRAN(STRTRAN( this.pMESSAGGIO ,CHR(13)+CHR(10),CHR(255)),CHR(13),CHR(255))
    this.w_ALLEGATI = this.pALLEGATI
    if NOT EMPTY( this.pBOTTONE )
      this.w_BOTTONE = this.pBOTTONE
    else
      this.w_BOTTONE = ""
    endif
    * --- Codice del postit
    this.w_CODE = UPPER( SYS( 2015 ) )
    * --- Destinatario
    this.w_USERCODE = this.w_DESTINATARIO
    * --- Mittente
    this.w_CREATEDBY = i_CODUTE
    * --- Status
    this.w_STATUS = "P"
    * --- Postit
    this.w_ACAPO = CHR(13) + CHR(10)
    this.w_VIRGOLETTE = CHR(34)
    this.w_APERTAQUADRA = CHR(91)
    this.w_CHIUSAQUADRA = CHR(93)
    this.w_ASTERISCO = CHR(42)
    * --- Messaggio
    this.w_POSTIT = this.w_VIRGOLETTE+this.w_MESSAGGIO+this.w_VIRGOLETTE
    * --- Numero bottoni
    this.w_NUMEROBOTTONI = OCCURS( "Desktopbutton _" , this.w_BOTTONE ) + 1
    * --- Altezza e larghezza del postin
    this.w_ALTEZZA = 482
    this.w_LARGHEZZA = 450
    if EMPTY( this.w_ALLEGATI )
      * --- Altezza
      this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+" 0"
      * --- Allegati
      this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+" 0"
    else
      * --- Altezza
      this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+" 112"
      * --- Allegati
      this.w_NUMEROALLEGATI = OCCURS( ";" , this.w_ALLEGATI ) + 1
      * --- Numero allegati
      this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+" "+ALLTRIM(STR( this.w_NUMEROALLEGATI ))
      this.w_CONTATORE = 1
      do while this.w_CONTATORE <= this.w_NUMEROALLEGATI
        * --- Nome del file completo di path
        * --- Nome del file senza path
        * --- Posizione iniziale del file corrente
        this.w_INIZIOFILE = IIF( this.w_CONTATORE = 1 , 1 , AT( ";" , this.w_ALLEGATI , this.w_CONTATORE - 1 ) + 1 )
        * --- Posizione finale del file corrente
        this.w_FINEFILE = IIF( this.w_CONTATORE < this.w_NUMEROALLEGATI , AT( ";" , this.w_ALLEGATI , this.w_CONTATORE ) - 1 , LEN(ALLTRIM( this.w_ALLEGATI )) )
        * --- File completo di path
        this.w_FILE = SUBSTR(this.w_ALLEGATI , this.w_INIZIOFILE , this.w_FINEFILE - this.w_INIZIOFILE + 1 )
        * --- Nome del file
        this.w_NOMEDELFILE = ALLTRIM( JUSTFNAME( this.w_FILE ) )
        this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+this.w_VIRGOLETTE+ALLTRIM(this.w_NOMEDELFILE)+this.w_VIRGOLETTE
        this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+this.w_VIRGOLETTE+ALLTRIM(this.w_FILE)+this.w_VIRGOLETTE
        this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+this.w_VIRGOLETTE+this.w_VIRGOLETTE
        this.w_CONTATORE = this.w_CONTATORE + 1
      enddo
    endif
    * --- Bottone
    this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+" "+ALLTRIM(STR(this.w_NUMEROBOTTONI))
    if EMPTY( this.w_BOTTONE )
      this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+this.w_APERTAQUADRA+this.w_ACAPO+this.w_CHIUSAQUADRA
    else
      this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+this.w_APERTAQUADRA+this.w_ACAPO+this.w_BOTTONE+this.w_ACAPO+this.w_CHIUSAQUADRA
    endif
    * --- Colore
    this.w_COLORE = 0
    this.w_COLORE = g_PostInColor
    if this.w_COLORE = 0
      * --- Altrimenti assegna un colore di default
      this.w_COLORE = 65535
    endif
    this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+" "+ALLTRIM(STR( this.w_COLORE ))
    this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+this.w_VIRGOLETTE+this.w_VIRGOLETTE
    this.w_POSTIT = this.w_POSTIT+this.w_ACAPO+this.w_ASTERISCO+this.w_ACAPO
    * --- Larghezza, altezza, posizione orizzontale, posizione verticale
    this.w_WI = this.w_LARGHEZZA
    this.w_HE = this.w_ALTEZZA
    this.w_PX = 0
    this.w_PY = 0
    * --- Datestart
    this.w_DATESTART = DATE()
    this.w_CREATED = DATETIME( YEAR( i_DATSYS ) , MONTH( i_DATSYS ) , DAY( i_DATSYS ) )
    this.w_FRASEDIINSERT = "insert into postit (code,usercode,created,createdby,status,postit,wi,he,px,py,datestart,cpccchk) values ( "
    this.w_FRASEDIINSERT = this.w_FRASEDIINSERT + CP_TOSTRODBC( this.w_CODE ) + ", "
    this.w_FRASEDIINSERT = this.w_FRASEDIINSERT + CP_TOSTRODBC( this.w_USERCODE ) + ", "
    this.w_FRASEDIINSERT = this.w_FRASEDIINSERT + CP_TOSTRODBC( this.w_CREATED ) + ", "
    this.w_FRASEDIINSERT = this.w_FRASEDIINSERT + CP_TOSTRODBC( this.w_CREATEDBY ) + ", "
    this.w_FRASEDIINSERT = this.w_FRASEDIINSERT + CP_TOSTRODBC( this.w_STATUS ) + ", "
    this.w_FRASEDIINSERT = this.w_FRASEDIINSERT + CP_TOSTRODBC( this.w_POSTIT ) + ", "
    this.w_FRASEDIINSERT = this.w_FRASEDIINSERT + CP_TOSTRODBC( this.w_WI ) + ", "
    this.w_FRASEDIINSERT = this.w_FRASEDIINSERT + CP_TOSTRODBC( this.w_HE ) + ", "
    this.w_FRASEDIINSERT = this.w_FRASEDIINSERT + CP_TOSTRODBC( this.w_PX ) + ", "
    this.w_FRASEDIINSERT = this.w_FRASEDIINSERT + CP_TOSTRODBC( this.w_PY ) + ", "
    this.w_FRASEDIINSERT = this.w_FRASEDIINSERT + CP_TOSTRODBC( this.w_DATESTART ) + ",'aaaaaaaaaa' )"
    * --- NELLE WORKING TABLES LA TABELLA AZIENDA � DICHIARATA PER POTER ESEGUIRE LE ISTRUZIONI SEGUENTI
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    SQLEXEC( i_nConn, this.w_FRASEDIINSERT )
    if this.pRETCOD
      i_retcode = 'stop'
      i_retval = this.w_CODE
      return
    endif
  endproc


  proc Init(oParentObject,pDESTINATARIO,pMESSAGGIO,pALLEGATI,pBOTTONE,pRETCOD)
    this.pDESTINATARIO=pDESTINATARIO
    this.pMESSAGGIO=pMESSAGGIO
    this.pALLEGATI=pALLEGATI
    this.pBOTTONE=pBOTTONE
    this.pRETCOD=pRETCOD
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONF_INT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pDESTINATARIO,pMESSAGGIO,pALLEGATI,pBOTTONE,pRETCOD"
endproc
