* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bia                                                        *
*              CHECK NUMERO RIGA DOCUMENTI (COAC)                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-07                                                      *
* Last revis.: 2008-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bia",oParentObject)
return(i_retval)

define class tgsve_bia as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli alla modifica del numero riga
    *     (contributi accessori)
    this.bUpdateParentObject=.t.
    if (this.oParentObject.w_ARTPADRE Or this.oParentObject.w_ARPUNPAD) And g_COAC="S"
      this.w_PADRE = this.oParentObject
      * --- Controllo numero riga KIT <---> Componenti
      this.w_PADRE.MarkPos()     
      if this.oParentObject.w_ARTPADRE
        * --- Se viene modificato il numero riga di un KIT occorre controllare che 
        *     non esista alcun suo componente con numero riga minore o uguale
        if this.oParentObject.w_MVTIPRIG="A"
          this.w_PADRE.Exec_Select("CheckRow", "Min(t_CPROWORD) as RIGA" , "t_MVRIFKIT="+ cp_ToStr(this.oParentObject.w_CPROWNUM) ,"","","")     
        else
          this.w_PADRE.Exec_Select("CheckRow", "Min(t_CPROWORD) as RIGA" , "t_MVRIFCAC="+ cp_ToStr(this.oParentObject.w_CPROWNUM) ,"","","")     
        endif
        if CheckRow.RIGA>0 And CheckRow.RIGA <= this.oParentObject.w_CPROWORD
          ah_ErrorMsg("Esiste almeno un componente/contributo accessorio con numero riga minore o uguale di %1",48,"",Alltrim(Str( this.oParentObject.w_CPROWORD )))
          this.w_PADRE.Set("t_CPROWORD", this.oParentObject.o_CPROWORD)     
        endif
        Use in CheckRow
      endif
      if this.oParentObject.w_ARPUNPAD
        * --- Se viene modificato il numero riga di un componente occorre controllare che 
        *     il KIT di riferimento mantenga sempre un numero riga pi� basso
        if this.oParentObject.w_MVRIFKIT > 0
          this.w_PADRE.Exec_Select("CheckRow", "t_CPROWORD as RIGA" , "CPROWNUM="+ cp_ToStr(this.oParentObject.w_MVRIFKIT) ,"","","")     
        else
          this.w_PADRE.Exec_Select("CheckRow", "t_CPROWORD as RIGA" , "CPROWNUM="+ cp_ToStr(this.oParentObject.w_MVRIFCAC) ,"","","")     
        endif
        if CheckRow.RIGA >= this.oParentObject.w_CPROWORD
          ah_ErrorMsg("Il padre dei componenti/contributi accessori ha un numero riga maggiore o uguale di %1",48,"",Alltrim(Str( this.oParentObject.w_CPROWORD )))
          this.w_PADRE.Set("t_CPROWORD", this.oParentObject.o_CPROWORD)     
        endif
        Use in CheckRow
      endif
      this.w_PADRE.RePos()     
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
