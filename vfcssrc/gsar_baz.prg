* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_baz                                                        *
*              Creazione azienda                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_58]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-13                                                      *
* Last revis.: 2014-01-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_baz",oParentObject)
return(i_retval)

define class tgsar_baz as StdBatch
  * --- Local variables
  w_CODAZI = space(5)
  w_C1 = 0
  w_C2 = 0
  w_C3 = 0
  w_C4 = 0
  w_C5 = 0
  w_C6 = 0
  w_C7 = 0
  w_C8 = ctod("  /  /  ")
  w_C9 = ctod("  /  /  ")
  w_C10 = space(4)
  w_C11 = space(30)
  w_C12 = space(30)
  w_C13 = space(30)
  w_C14 = space(30)
  w_C15 = space(30)
  w_C16 = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato sotto transazione da GSUT_BCA (Creazione Azienda)
    * --- Archivi
    *      Vendita funzioni avanzate
    this.w_CODAZI = this.oparentObject.oParentObject.w_CODAZI
    if UPPER(g_application)="AD HOC ENTERPRISE"
      * --- Entit� - solo vendite funzioni avanzate
      this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(nLogFile, i_CODAZI,this.w_codazi,"ENT_MAST","ENTIPENT","V"),.F.)
      * --- Non posso filtare con trasfarc su campi della testata, per cui utilizzo
      *     una query per idneitifcare le righe e le passo una alla volta...
      * --- Select from query\GSAR_BAZ
      do vq_exec with 'query\GSAR_BAZ',this,'_Curs_query_GSAR_BAZ','',.f.,.t.
      if used('_Curs_query_GSAR_BAZ')
        select _Curs_query_GSAR_BAZ
        locate for 1=1
        do while not(eof())
        this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(nLogFile, i_CODAZI,this.w_codazi,"ENT_DETT","ENCODICE", _Curs_query_GSAR_BAZ.ENCODICE), .F.)
        if Not this.oParentObject.VABENE
          exit
        endif
          select _Curs_query_GSAR_BAZ
          continue
        enddo
        use
      endif
      * --- Strutture EDI
      this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(nLogFile,i_CODAZI,this.w_codazi,"VASTRUTT"),.F.)
      if g_VEFA="S" 
        this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(nLogFile, i_CODAZI,this.w_codazi,"VAPREDEF"),.F.)
        this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(nLogFile, i_CODAZI,this.w_codazi,"VAVARNOM"),.F.)
      endif
    else
      * --- Entit� - solo vendite funzioni avanzate
      this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"ENT_MAST","ENTIPENT","V"),.F.)
      * --- Non posso filtare con trasfarc su campi della testata, per cui utilizzo
      *     una query per idneitifcare le righe e le passo una alla volta...
      * --- Select from query\GSAR_BAZ
      do vq_exec with 'query\GSAR_BAZ',this,'_Curs_query_GSAR_BAZ','',.f.,.t.
      if used('_Curs_query_GSAR_BAZ')
        select _Curs_query_GSAR_BAZ
        locate for 1=1
        do while not(eof())
        this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"ENT_DETT","ENCODICE", _Curs_query_GSAR_BAZ.ENCODICE), .F.)
        if Not this.oParentObject.VABENE
          exit
        endif
          select _Curs_query_GSAR_BAZ
          continue
        enddo
        use
      endif
      * --- Strutture EDI
      this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"VASTRUTT"),.F.)
      if g_VEFA="S" 
        this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"VAPREDEF"),.F.)
        this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"VAVARNOM"),.F.)
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_query_GSAR_BAZ')
      use in _Curs_query_GSAR_BAZ
    endif
    if used('_Curs_query_GSAR_BAZ')
      use in _Curs_query_GSAR_BAZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
