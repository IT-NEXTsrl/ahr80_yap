* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_blk                                                        *
*              Riicerca chiave in situazione di filtro                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-01                                                      *
* Last revis.: 2014-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_blk",oParentObject)
return(i_retval)

define class tgsma_blk as StdBatch
  * --- Local variables
  w_IDX = 0
  w_ROWNUM = 0
  * --- WorkFile variables
  LIS_TINI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se siamo in situazione di filtro per il dettaglio listini, 
    *     si deve recuperare la chiave CPROWNUM del dettaglio, 
    *     senza applicare i filtri, 
    *     altrimenti si avrebbe un problema di chiave duplicata nella creazione di nuove righe, 
    *     dato da incremento di un CPROWNUM non aggiornato
    this.w_ROWNUM = 0
    * --- Determino l'indice della tabella
    this.w_IDX = cp_GetTableDefIdx("lis_tini")
    if type("i_bsecurityrecord")<>"U" and i_bsecurityrecord=.t. and Type("i_TablePropSec[this.w_IDX,1]")="C"
      * --- Sicurezza del dato attivo e definita una limitazione per la tabella lis_tini
      * --- Select from LIS_TINI
      i_nConn=i_TableProp[this.LIS_TINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MAX(CPROWNUM) as ROWNUM  from "+i_cTable+" LIS_TINI ";
            +" where LICODART = "+cp_ToStrODBC(this.oParentObject.w_CODART)+"";
            +" group by LICODART";
             ,"_Curs_LIS_TINI")
      else
        select MAX(CPROWNUM) as ROWNUM from (i_cTable);
         where LICODART = this.oParentObject.w_CODART;
         group by LICODART;
          into cursor _Curs_LIS_TINI
      endif
      if used('_Curs_LIS_TINI')
        select _Curs_LIS_TINI
        locate for 1=1
        do while not(eof())
        this.w_ROWNUM = _Curs_LIS_TINI.ROWNUM
          select _Curs_LIS_TINI
          continue
        enddo
        use
      endif
      this.oparentobject.i_nRowNum=this.w_ROWNUM
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LIS_TINI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_LIS_TINI')
      use in _Curs_LIS_TINI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
