* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_boo                                                        *
*              Chiude zoom distinte bonifici                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-07                                                      *
* Last revis.: 2007-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Pparam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_boo",oParentObject,m.Pparam)
return(i_retval)

define class tgste_boo as StdBatch
  * --- Local variables
  Pparam = space(4)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiudo la Maschera - utilizzato in GSTE_KDO - Avviso Bonifico a fornitore
    *     Lanciato anche nella maschera: GSTE_KDS che esegue filtro distinte in:
    *     1) GSTE_SBE  stampa brogliaccio effetti
    *     2) GSSO_API  Piano Insoluti 
    *     3) GSCG_KCD Contabilizzazioni  Distinte Parziali
    *     4) GSCG_MSP Storno SBF
    do case
      case this.Pparam="INIT"
        do case
          case UPPER(this.oparentobject.oparentobject.CLASS) $ "TGSSO_API-TGSCG_MSP"
            * --- Se lanciato da piano contenzioso non devo visulizzare le distinte provvisorie
            this.oParentObject.w_RAGRU.cCpQueryName = "QUERY\GSTEZDIS"
          case UPPER(this.oparentobject.oparentobject.CLASS)="TGSCG_BC7"
            this.oParentObject.w_RAGRU.cCpQueryName = "QUERY\GSCG_KCD"
          otherwise
            this.oparentobject.w_NUMERO=this.oparentobject.oparentobject.w_DINUMERO
        endcase
        this.oparentobject.notifyevent("Esegui")
      case this.Pparam="SELE"
        this.oparentObject.ecpsave()
        i_retcode = 'stop'
        return
    endcase
  endproc


  proc Init(oParentObject,Pparam)
    this.Pparam=Pparam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Pparam"
endproc
