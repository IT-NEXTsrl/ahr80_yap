* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kof                                                        *
*              Suite Office                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_38]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-10                                                      *
* Last revis.: 2012-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kof",oParentObject))

* --- Class definition
define class tgsar_kof as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 439
  Height = 169
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-07-04"
  HelpContextID=166295401
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsar_kof"
  cComment = "Suite Office"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_OFFICE = space(1)
  o_OFFICE = space(1)
  w_PRINTMERGE = space(1)
  w_OFFICBIT = space(1)
  w_OFFTIPCO = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kofPag1","gsar_kof",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oOFFICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_OFFICE=space(1)
      .w_PRINTMERGE=space(1)
      .w_OFFICBIT=space(1)
      .w_OFFTIPCO=space(1)
        .w_CODAZI = i_CODAZI
        .w_OFFICE = IIF( TYPE('g_OFFICE')='C',g_OFFICE, 'N')
        .w_PRINTMERGE = IIF( TYPE('g_PRINTMERGE')='C',g_PRINTMERGE, '2')
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .w_OFFICBIT = IIF( VARTYPE(g_OFFICE_BIT)='C' and g_OFFICE_BIT$'3-6',g_OFFICE_BIT, '3') 
        .w_OFFTIPCO = IIF( EMPTY(.w_OFFTIPCO), IIF( VARTYPE(g_OFFICE_TYPE_CONV)='C' and g_OFFICE_TYPE_CONV$'N-S-C',g_OFFICE_TYPE_CONV, 'N'), .w_OFFTIPCO)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
  return

  proc Calculate_ACVWFPNYKN()
    with this
          * --- Legge file di configurazione legato alla macchina - GSUT_BLD(S)
          gsut_bld(this;
              ,"S";
              ,IIF(type('g_CODPEN')<>'U',g_CODPEN,' ');
              ,IIF(type('g_CODCAS')<>'U',g_CODCAS,' ');
              ,IIF(type('g_CODNEG')<>'U',g_CODNEG,'');
              ,.w_OFFICE;
              ,.w_PRINTMERGE;
              ,iif(type('g_PRINTERARCHI')<>'U',g_PRINTERARCHI,' ');
              ,iif(type('g_SCANNER')<>'U',g_SCANNER,' ');
              ,' ';
              ,.w_OFFICBIT;
              ,.w_OFFTIPCO;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPRINTMERGE_1_4.enabled_(this.oPgFrm.Page1.oPag.oPRINTMERGE_1_4.mCond())
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPRINTMERGE_1_4.visible=!this.oPgFrm.Page1.oPag.oPRINTMERGE_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_6.visible=!this.oPgFrm.Page1.oPag.oBtn_1_6.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
        if lower(cEvent)==lower("Salva")
          .Calculate_ACVWFPNYKN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oOFFICE_1_2.RadioValue()==this.w_OFFICE)
      this.oPgFrm.Page1.oPag.oOFFICE_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRINTMERGE_1_4.RadioValue()==this.w_PRINTMERGE)
      this.oPgFrm.Page1.oPag.oPRINTMERGE_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOFFICBIT_1_10.RadioValue()==this.w_OFFICBIT)
      this.oPgFrm.Page1.oPag.oOFFICBIT_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOFFTIPCO_1_12.RadioValue()==this.w_OFFTIPCO)
      this.oPgFrm.Page1.oPag.oOFFTIPCO_1_12.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsar_kof
      * - AL Salvataggio
      .NotifyEvent('AggAzienda')
      .NotifyEvent('Salva')
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_OFFICE = this.w_OFFICE
    return

enddefine

* --- Define pages as container
define class tgsar_kofPag1 as StdContainer
  Width  = 435
  height = 169
  stdWidth  = 435
  stdheight = 169
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oOFFICE_1_2 as StdCombo with uid="AGNCCFAPSO",rtseq=2,rtrep=.f.,left=92,top=17,width=337,height=21;
    , ToolTipText = "Permette di selezionare la suite Office utilizzata localmente";
    , HelpContextID = 261370854;
    , cFormVar="w_OFFICE",RowSource=""+"Microsoft Office,"+"OpenOffice/StarOffice,"+"Non utilizzata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOFFICE_1_2.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'O',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oOFFICE_1_2.GetRadio()
    this.Parent.oContained.w_OFFICE = this.RadioValue()
    return .t.
  endfunc

  func oOFFICE_1_2.SetRadio()
    this.Parent.oContained.w_OFFICE=trim(this.Parent.oContained.w_OFFICE)
    this.value = ;
      iif(this.Parent.oContained.w_OFFICE=='M',1,;
      iif(this.Parent.oContained.w_OFFICE=='O',2,;
      iif(this.Parent.oContained.w_OFFICE=='N',3,;
      0)))
  endfunc

  add object oPRINTMERGE_1_4 as StdRadio with uid="RXBBDWMQJK",rtseq=3,rtrep=.f.,left=92, top=107, width=144,height=23;
    , ToolTipText = "Permette di inviare direttamente alla stampante i file di OpenOffice generati con il Mailmerge";
    , cFormVar="w_PRINTMERGE", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPRINTMERGE_1_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="File"
      this.Buttons(1).HelpContextID = 145340856
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("File","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Stampante"
      this.Buttons(2).HelpContextID = 145340856
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Stampante","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Permette di inviare direttamente alla stampante i file di OpenOffice generati con il Mailmerge")
      StdRadio::init()
    endproc

  func oPRINTMERGE_1_4.RadioValue()
    return(iif(this.value =1,'2',;
    iif(this.value =2,'1',;
    space(1))))
  endfunc
  func oPRINTMERGE_1_4.GetRadio()
    this.Parent.oContained.w_PRINTMERGE = this.RadioValue()
    return .t.
  endfunc

  func oPRINTMERGE_1_4.SetRadio()
    this.Parent.oContained.w_PRINTMERGE=trim(this.Parent.oContained.w_PRINTMERGE)
    this.value = ;
      iif(this.Parent.oContained.w_PRINTMERGE=='2',1,;
      iif(this.Parent.oContained.w_PRINTMERGE=='1',2,;
      0))
  endfunc

  func oPRINTMERGE_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OFFICE='O')
    endwith
   endif
  endfunc

  func oPRINTMERGE_1_4.mHide()
    with this.Parent.oContained
      return (.w_OFFICE<>'O')
    endwith
  endfunc


  add object oBtn_1_6 as StdButton with uid="BJPIFLNLPS",left=326, top=116, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte sulla macchina client";
    , HelpContextID = 166266650;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_APPLICATION = "ADHOC REVOLUTION")
      endwith
    endif
  endfunc

  func oBtn_1_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION")
     endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="AELESHRMUJ",left=379, top=116, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 158977978;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_APPLICATION = "ADHOC REVOLUTION")
      endwith
    endif
  endfunc

  func oBtn_1_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION")
     endwith
    endif
  endfunc


  add object oObj_1_8 as cp_runprogram with uid="FZAYKELAJD",left=496, top=81, width=153,height=24,;
    caption='GSUT_BLD(L)',;
   bGlobalFont=.t.,;
    prg="GSUT_BLD('L',' ',' ',' ',' ',' ')",;
    cEvent = "Inizio",;
    nPag=1;
    , ToolTipText = "Legge file di configurazione legato alla macchina";
    , HelpContextID = 27062486


  add object oObj_1_9 as cp_runprogram with uid="HMCYDPLLLC",left=496, top=136, width=153,height=24,;
    caption='GSUT_BOF',;
   bGlobalFont=.t.,;
    prg="GSUT_BOF",;
    cEvent = "AggAzienda",;
    nPag=1;
    , ToolTipText = "Memorizza in azienda il campo azoffice";
    , HelpContextID = 241184940


  add object oOFFICBIT_1_10 as StdCombo with uid="LINKLBYHWL",rtseq=4,rtrep=.f.,left=92,top=43,width=126,height=22;
    , HelpContextID = 57396166;
    , cFormVar="w_OFFICBIT",RowSource=""+"32 bit,"+"64 bit", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOFFICBIT_1_10.RadioValue()
    return(iif(this.value =1,'3',;
    iif(this.value =2,'6',;
    space(1))))
  endfunc
  func oOFFICBIT_1_10.GetRadio()
    this.Parent.oContained.w_OFFICBIT = this.RadioValue()
    return .t.
  endfunc

  func oOFFICBIT_1_10.SetRadio()
    this.Parent.oContained.w_OFFICBIT=trim(this.Parent.oContained.w_OFFICBIT)
    this.value = ;
      iif(this.Parent.oContained.w_OFFICBIT=='3',1,;
      iif(this.Parent.oContained.w_OFFICBIT=='6',2,;
      0))
  endfunc


  add object oOFFTIPCO_1_12 as StdCombo with uid="LLASEKSEGW",rtseq=5,rtrep=.f.,left=92,top=72,width=337,height=21;
    , ToolTipText = "Permette di impostare il tipo di conversione dei caratteri da utilizzare in caso di esportazione dati con suite office/openoffice a 64 bit";
    , HelpContextID = 184497205;
    , cFormVar="w_OFFTIPCO",RowSource=""+"Nessuna,"+"Caratteri accentati con non accentati,"+"Caratteri accentati con non accentati e apostrofo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOFFTIPCO_1_12.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oOFFTIPCO_1_12.GetRadio()
    this.Parent.oContained.w_OFFTIPCO = this.RadioValue()
    return .t.
  endfunc

  func oOFFTIPCO_1_12.SetRadio()
    this.Parent.oContained.w_OFFTIPCO=trim(this.Parent.oContained.w_OFFTIPCO)
    this.value = ;
      iif(this.Parent.oContained.w_OFFTIPCO=='N',1,;
      iif(this.Parent.oContained.w_OFFTIPCO=='S',2,;
      iif(this.Parent.oContained.w_OFFTIPCO=='C',3,;
      0)))
  endfunc

  add object oStr_1_3 as StdString with uid="CDVBUNFHTW",Visible=.t., Left=26, Top=17,;
    Alignment=1, Width=65, Height=18,;
    Caption="Suite Office:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="FRUQICREFR",Visible=.t., Left=5, Top=107,;
    Alignment=1, Width=86, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (.w_OFFICE<>'O')
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="MLDBWGKTWY",Visible=.t., Left=0, Top=45,;
    Alignment=1, Width=83, Height=18,;
    Caption="Versione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="LXIKPXIOQO",Visible=.t., Left=0, Top=74,;
    Alignment=1, Width=83, Height=18,;
    Caption="Conversione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kof','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
