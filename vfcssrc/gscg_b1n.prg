* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_b1n                                                        *
*              Seleziona/des. acconti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-11-23                                                      *
* Last revis.: 2005-12-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_b1n",oParentObject,m.pOper)
return(i_retval)

define class tgscg_b1n as StdBatch
  * --- Local variables
  pOper = space(5)
  w_SERDOC = space(10)
  w_FLVEAC = space(1)
  w_CLADOC = space(2)
  w_ZOOM = space(10)
  w_PROG = .NULL.
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona/Deseleziona Acconti (Lanciato da GSCG_KCN)
    this.w_ZOOM = this.oParentObject.w_CalcZoom
    do case
      case this.pOper="DOCU" AND NOT EMPTY(this.oParentObject.w_SERIALE)
        * --- Documenti
        this.w_SERDOC = SPACE(10)
        this.w_FLVEAC = "V"
        this.w_CLADOC = "  "
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVSERIAL,MVFLVEAC,MVCLADOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVSERIAL,MVFLVEAC,MVCLADOC;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_SERIALE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERDOC = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
          this.w_FLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
          this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.w_SERDOC) OR i_Rows=0
          ah_errormsg("Documento inesistente")
        else
          this.w_PROG = GSVE_MDV(this.w_FLVEAC+this.w_CLADOC)
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_PROG.bSec1)
            i_retcode = 'stop'
            return
          endif
          this.w_PROG.w_MVSERIAL = this.w_SERDOC
          this.w_PROG.QueryKeySet("MVSERIAL='"+this.w_SERDOC+ "'","")     
          * --- carico il record
          this.w_PROG.LoadRecWarn()     
        endif
      case this.pOper="SELE"
        * --- Seleziona/Deseleziona Tutto
        NC = this.w_ZOOM.cCursor
        if this.oParentObject.w_SELEZI="S"
          UPDATE &NC SET XCHK = 1
          this.oParentObject.w_FLSELE = 1
        else
          UPDATE &NC SET XCHK = 0
          this.oParentObject.w_FLSELE = 0
        endif
      case this.pOper="VEAC"
        this.oParentObject.w_TIPCLF = IIF(this.w_FLVEAC="A", "F", "C")
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
