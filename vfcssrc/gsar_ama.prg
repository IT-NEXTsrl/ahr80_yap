* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ama                                                        *
*              Magazzini                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_50                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-10                                                      *
* Last revis.: 2014-10-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_ama"))

* --- Class definition
define class tgsar_ama as StdForm
  Top    = 0
  Left   = 8

  * --- Standard Properties
  Width  = 542
  Height = 416+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-28"
  HelpContextID=210335593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=38

  * --- Constant Properties
  MAGAZZIN_IDX = 0
  CONTI_IDX = 0
  cFile = "MAGAZZIN"
  cKeySelect = "MGCODMAG"
  cKeyWhere  = "MGCODMAG=this.w_MGCODMAG"
  cKeyWhereODBC = '"MGCODMAG="+cp_ToStrODBC(this.w_MGCODMAG)';

  cKeyWhereODBCqualified = '"MAGAZZIN.MGCODMAG="+cp_ToStrODBC(this.w_MGCODMAG)';

  cPrg = "gsar_ama"
  cComment = "Magazzini"
  icon = "anag.ico"
  cAutoZoom = 'GSMA0AMA'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MGCODMAG = space(5)
  o_MGCODMAG = space(5)
  w_MGDESMAG = space(30)
  w_MGINDMAG = space(30)
  w_MGMAGCAP = space(8)
  w_MGCAPMAG = space(5)
  w_MGCITMAG = space(30)
  w_MGPROMAG = space(2)
  w_MGPERSON = space(40)
  w_MG__NOTE = space(40)
  w_MGTELEFO = space(18)
  w_MGMAGWEB = space(1)
  w_MG_EMAIL = space(254)
  w_MG_EMPEC = space(254)
  w_MGGESCAR = space(1)
  o_MGGESCAR = space(1)
  w_MGRIFFOR = space(15)
  w_MGFISMAG = space(1)
  w_MGDISMAG = space(1)
  w_MGTIPMAG = space(1)
  w_MGFLUBIC = space(1)
  w_MGMAGRAG = space(5)
  w_DESRIF = space(30)
  w_MGDTINVA = ctod('  /  /  ')
  w_MGDTOBSO = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_RAGRIF = space(5)
  w_FISMAG = space(1)
  w_OLDFLUB = space(1)
  w_ODISMAG = space(10)
  w_MGSTAINT = space(1)
  o_MGSTAINT = space(1)
  w_MGPRPAGM = 0
  w_MGPREFIS = space(20)
  w_ANTIPCON = space(1)
  w_ANDESCRI = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MAGAZZIN','gsar_ama')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_amaPag1","gsar_ama",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Magazzino")
      .Pages(1).HelpContextID = 176265692
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMGCODMAG_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MAGAZZIN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MAGAZZIN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MAGAZZIN_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MGCODMAG = NVL(MGCODMAG,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_21_joined
    link_1_21_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MAGAZZIN where MGCODMAG=KeySet.MGCODMAG
    *
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MAGAZZIN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MAGAZZIN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MAGAZZIN '
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MGCODMAG',this.w_MGCODMAG  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESRIF = space(30)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_RAGRIF = space(5)
        .w_FISMAG = space(1)
        .w_ANTIPCON = 'F'
        .w_ANDESCRI = space(40)
        .w_MGCODMAG = NVL(MGCODMAG,space(5))
        .w_MGDESMAG = NVL(MGDESMAG,space(30))
        .w_MGINDMAG = NVL(MGINDMAG,space(30))
        .w_MGMAGCAP = NVL(MGMAGCAP,space(8))
        .w_MGCAPMAG = NVL(MGCAPMAG,space(5))
        .w_MGCITMAG = NVL(MGCITMAG,space(30))
        .w_MGPROMAG = NVL(MGPROMAG,space(2))
        .w_MGPERSON = NVL(MGPERSON,space(40))
        .w_MG__NOTE = NVL(MG__NOTE,space(40))
        .w_MGTELEFO = NVL(MGTELEFO,space(18))
        .w_MGMAGWEB = NVL(MGMAGWEB,space(1))
        .w_MG_EMAIL = NVL(MG_EMAIL,space(254))
        .w_MG_EMPEC = NVL(MG_EMPEC,space(254))
        .w_MGGESCAR = NVL(MGGESCAR,space(1))
        .w_MGRIFFOR = NVL(MGRIFFOR,space(15))
          .link_1_15('Load')
        .w_MGFISMAG = NVL(MGFISMAG,space(1))
        .w_MGDISMAG = NVL(MGDISMAG,space(1))
        .w_MGTIPMAG = NVL(MGTIPMAG,space(1))
        .w_MGFLUBIC = NVL(MGFLUBIC,space(1))
        .w_MGMAGRAG = NVL(MGMAGRAG,space(5))
          if link_1_21_joined
            this.w_MGMAGRAG = NVL(MGCODMAG121,NVL(this.w_MGMAGRAG,space(5)))
            this.w_DESRIF = NVL(MGDESMAG121,space(30))
            this.w_RAGRIF = NVL(MGMAGRAG121,space(5))
            this.w_FISMAG = NVL(MGFISMAG121,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(MGDTOBSO121),ctod("  /  /  "))
          else
          .link_1_21('Load')
          endif
        .w_MGDTINVA = NVL(cp_ToDate(MGDTINVA),ctod("  /  /  "))
        .w_MGDTOBSO = NVL(cp_ToDate(MGDTOBSO),ctod("  /  /  "))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_OLDFLUB = .w_MGFLUBIC
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .w_ODISMAG = .w_MGDISMAG
        .w_MGSTAINT = NVL(MGSTAINT,space(1))
        .w_MGPRPAGM = NVL(MGPRPAGM,0)
        .w_MGPREFIS = NVL(MGPREFIS,space(20))
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        cp_LoadRecExtFlds(this,'MAGAZZIN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MGCODMAG = space(5)
      .w_MGDESMAG = space(30)
      .w_MGINDMAG = space(30)
      .w_MGMAGCAP = space(8)
      .w_MGCAPMAG = space(5)
      .w_MGCITMAG = space(30)
      .w_MGPROMAG = space(2)
      .w_MGPERSON = space(40)
      .w_MG__NOTE = space(40)
      .w_MGTELEFO = space(18)
      .w_MGMAGWEB = space(1)
      .w_MG_EMAIL = space(254)
      .w_MG_EMPEC = space(254)
      .w_MGGESCAR = space(1)
      .w_MGRIFFOR = space(15)
      .w_MGFISMAG = space(1)
      .w_MGDISMAG = space(1)
      .w_MGTIPMAG = space(1)
      .w_MGFLUBIC = space(1)
      .w_MGMAGRAG = space(5)
      .w_DESRIF = space(30)
      .w_MGDTINVA = ctod("  /  /  ")
      .w_MGDTOBSO = ctod("  /  /  ")
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_RAGRIF = space(5)
      .w_FISMAG = space(1)
      .w_OLDFLUB = space(1)
      .w_ODISMAG = space(10)
      .w_MGSTAINT = space(1)
      .w_MGPRPAGM = 0
      .w_MGPREFIS = space(20)
      .w_ANTIPCON = space(1)
      .w_ANDESCRI = space(40)
      if .cFunction<>"Filter"
          .DoRTCalc(1,10,.f.)
        .w_MGMAGWEB = 'N'
          .DoRTCalc(12,13,.f.)
        .w_MGGESCAR = 'N'
        .w_MGRIFFOR = IIF(.w_MGGESCAR='S', .w_MGRIFFOR, SPACE(15))
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_MGRIFFOR))
          .link_1_15('Full')
          endif
        .w_MGFISMAG = 'S'
        .w_MGDISMAG = 'S'
        .DoRTCalc(18,20,.f.)
          if not(empty(.w_MGMAGRAG))
          .link_1_21('Full')
          endif
          .DoRTCalc(21,27,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(29,31,.f.)
        .w_OLDFLUB = .w_MGFLUBIC
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .w_ODISMAG = .w_MGDISMAG
          .DoRTCalc(34,35,.f.)
        .w_MGPREFIS = IIF(.w_MGSTAINT<>'S',SPACE(20),.w_MGPREFIS)
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .w_ANTIPCON = 'F'
      endif
    endwith
    cp_BlankRecExtFlds(this,'MAGAZZIN')
    this.DoRTCalc(38,38,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMGCODMAG_1_1.enabled = i_bVal
      .Page1.oPag.oMGDESMAG_1_2.enabled = i_bVal
      .Page1.oPag.oMGINDMAG_1_3.enabled = i_bVal
      .Page1.oPag.oMGMAGCAP_1_4.enabled = i_bVal
      .Page1.oPag.oMGCITMAG_1_6.enabled = i_bVal
      .Page1.oPag.oMGPROMAG_1_7.enabled = i_bVal
      .Page1.oPag.oMGPERSON_1_8.enabled = i_bVal
      .Page1.oPag.oMG__NOTE_1_9.enabled = i_bVal
      .Page1.oPag.oMGTELEFO_1_10.enabled = i_bVal
      .Page1.oPag.oMGMAGWEB_1_11.enabled = i_bVal
      .Page1.oPag.oMG_EMAIL_1_12.enabled = i_bVal
      .Page1.oPag.oMG_EMPEC_1_13.enabled = i_bVal
      .Page1.oPag.oMGGESCAR_1_14.enabled = i_bVal
      .Page1.oPag.oMGRIFFOR_1_15.enabled = i_bVal
      .Page1.oPag.oMGFISMAG_1_17.enabled = i_bVal
      .Page1.oPag.oMGDISMAG_1_18.enabled = i_bVal
      .Page1.oPag.oMGTIPMAG_1_19.enabled = i_bVal
      .Page1.oPag.oMGFLUBIC_1_20.enabled = i_bVal
      .Page1.oPag.oMGMAGRAG_1_21.enabled = i_bVal
      .Page1.oPag.oMGDTINVA_1_23.enabled = i_bVal
      .Page1.oPag.oMGDTOBSO_1_25.enabled = i_bVal
      .Page1.oPag.oMGSTAINT_1_47.enabled = i_bVal
      .Page1.oPag.oMGPRPAGM_1_48.enabled = i_bVal
      .Page1.oPag.oMGPREFIS_1_49.enabled = i_bVal
      .Page1.oPag.oObj_1_45.enabled = i_bVal
      .Page1.oPag.oObj_1_54.enabled = i_bVal
      .Page1.oPag.oObj_1_56.enabled = i_bVal
      .Page1.oPag.oObj_1_57.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMGCODMAG_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMGCODMAG_1_1.enabled = .t.
        .Page1.oPag.oMGDESMAG_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MAGAZZIN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGCODMAG,"MGCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGDESMAG,"MGDESMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGINDMAG,"MGINDMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGMAGCAP,"MGMAGCAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGCAPMAG,"MGCAPMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGCITMAG,"MGCITMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGPROMAG,"MGPROMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGPERSON,"MGPERSON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MG__NOTE,"MG__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGTELEFO,"MGTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGMAGWEB,"MGMAGWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MG_EMAIL,"MG_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MG_EMPEC,"MG_EMPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGGESCAR,"MGGESCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGRIFFOR,"MGRIFFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGFISMAG,"MGFISMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGDISMAG,"MGDISMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGTIPMAG,"MGTIPMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGFLUBIC,"MGFLUBIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGMAGRAG,"MGMAGRAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGDTINVA,"MGDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGDTOBSO,"MGDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGSTAINT,"MGSTAINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGPRPAGM,"MGPRPAGM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGPREFIS,"MGPREFIS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    i_lTable = "MAGAZZIN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MAGAZZIN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SAM with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MAGAZZIN_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MAGAZZIN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MAGAZZIN')
        i_extval=cp_InsertValODBCExtFlds(this,'MAGAZZIN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MGCODMAG,MGDESMAG,MGINDMAG,MGMAGCAP,MGCAPMAG"+;
                  ",MGCITMAG,MGPROMAG,MGPERSON,MG__NOTE,MGTELEFO"+;
                  ",MGMAGWEB,MG_EMAIL,MG_EMPEC,MGGESCAR,MGRIFFOR"+;
                  ",MGFISMAG,MGDISMAG,MGTIPMAG,MGFLUBIC,MGMAGRAG"+;
                  ",MGDTINVA,MGDTOBSO,UTCC,UTCV,UTDC"+;
                  ",UTDV,MGSTAINT,MGPRPAGM,MGPREFIS "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MGCODMAG)+;
                  ","+cp_ToStrODBC(this.w_MGDESMAG)+;
                  ","+cp_ToStrODBC(this.w_MGINDMAG)+;
                  ","+cp_ToStrODBC(this.w_MGMAGCAP)+;
                  ","+cp_ToStrODBC(this.w_MGCAPMAG)+;
                  ","+cp_ToStrODBC(this.w_MGCITMAG)+;
                  ","+cp_ToStrODBC(this.w_MGPROMAG)+;
                  ","+cp_ToStrODBC(this.w_MGPERSON)+;
                  ","+cp_ToStrODBC(this.w_MG__NOTE)+;
                  ","+cp_ToStrODBC(this.w_MGTELEFO)+;
                  ","+cp_ToStrODBC(this.w_MGMAGWEB)+;
                  ","+cp_ToStrODBC(this.w_MG_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_MG_EMPEC)+;
                  ","+cp_ToStrODBC(this.w_MGGESCAR)+;
                  ","+cp_ToStrODBCNull(this.w_MGRIFFOR)+;
                  ","+cp_ToStrODBC(this.w_MGFISMAG)+;
                  ","+cp_ToStrODBC(this.w_MGDISMAG)+;
                  ","+cp_ToStrODBC(this.w_MGTIPMAG)+;
                  ","+cp_ToStrODBC(this.w_MGFLUBIC)+;
                  ","+cp_ToStrODBCNull(this.w_MGMAGRAG)+;
                  ","+cp_ToStrODBC(this.w_MGDTINVA)+;
                  ","+cp_ToStrODBC(this.w_MGDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_MGSTAINT)+;
                  ","+cp_ToStrODBC(this.w_MGPRPAGM)+;
                  ","+cp_ToStrODBC(this.w_MGPREFIS)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MAGAZZIN')
        i_extval=cp_InsertValVFPExtFlds(this,'MAGAZZIN')
        cp_CheckDeletedKey(i_cTable,0,'MGCODMAG',this.w_MGCODMAG)
        INSERT INTO (i_cTable);
              (MGCODMAG,MGDESMAG,MGINDMAG,MGMAGCAP,MGCAPMAG,MGCITMAG,MGPROMAG,MGPERSON,MG__NOTE,MGTELEFO,MGMAGWEB,MG_EMAIL,MG_EMPEC,MGGESCAR,MGRIFFOR,MGFISMAG,MGDISMAG,MGTIPMAG,MGFLUBIC,MGMAGRAG,MGDTINVA,MGDTOBSO,UTCC,UTCV,UTDC,UTDV,MGSTAINT,MGPRPAGM,MGPREFIS  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MGCODMAG;
                  ,this.w_MGDESMAG;
                  ,this.w_MGINDMAG;
                  ,this.w_MGMAGCAP;
                  ,this.w_MGCAPMAG;
                  ,this.w_MGCITMAG;
                  ,this.w_MGPROMAG;
                  ,this.w_MGPERSON;
                  ,this.w_MG__NOTE;
                  ,this.w_MGTELEFO;
                  ,this.w_MGMAGWEB;
                  ,this.w_MG_EMAIL;
                  ,this.w_MG_EMPEC;
                  ,this.w_MGGESCAR;
                  ,this.w_MGRIFFOR;
                  ,this.w_MGFISMAG;
                  ,this.w_MGDISMAG;
                  ,this.w_MGTIPMAG;
                  ,this.w_MGFLUBIC;
                  ,this.w_MGMAGRAG;
                  ,this.w_MGDTINVA;
                  ,this.w_MGDTOBSO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_MGSTAINT;
                  ,this.w_MGPRPAGM;
                  ,this.w_MGPREFIS;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MAGAZZIN_IDX,i_nConn)
      *
      * update MAGAZZIN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MAGAZZIN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MGDESMAG="+cp_ToStrODBC(this.w_MGDESMAG)+;
             ",MGINDMAG="+cp_ToStrODBC(this.w_MGINDMAG)+;
             ",MGMAGCAP="+cp_ToStrODBC(this.w_MGMAGCAP)+;
             ",MGCAPMAG="+cp_ToStrODBC(this.w_MGCAPMAG)+;
             ",MGCITMAG="+cp_ToStrODBC(this.w_MGCITMAG)+;
             ",MGPROMAG="+cp_ToStrODBC(this.w_MGPROMAG)+;
             ",MGPERSON="+cp_ToStrODBC(this.w_MGPERSON)+;
             ",MG__NOTE="+cp_ToStrODBC(this.w_MG__NOTE)+;
             ",MGTELEFO="+cp_ToStrODBC(this.w_MGTELEFO)+;
             ",MGMAGWEB="+cp_ToStrODBC(this.w_MGMAGWEB)+;
             ",MG_EMAIL="+cp_ToStrODBC(this.w_MG_EMAIL)+;
             ",MG_EMPEC="+cp_ToStrODBC(this.w_MG_EMPEC)+;
             ",MGGESCAR="+cp_ToStrODBC(this.w_MGGESCAR)+;
             ",MGRIFFOR="+cp_ToStrODBCNull(this.w_MGRIFFOR)+;
             ",MGFISMAG="+cp_ToStrODBC(this.w_MGFISMAG)+;
             ",MGDISMAG="+cp_ToStrODBC(this.w_MGDISMAG)+;
             ",MGTIPMAG="+cp_ToStrODBC(this.w_MGTIPMAG)+;
             ",MGFLUBIC="+cp_ToStrODBC(this.w_MGFLUBIC)+;
             ",MGMAGRAG="+cp_ToStrODBCNull(this.w_MGMAGRAG)+;
             ",MGDTINVA="+cp_ToStrODBC(this.w_MGDTINVA)+;
             ",MGDTOBSO="+cp_ToStrODBC(this.w_MGDTOBSO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",MGSTAINT="+cp_ToStrODBC(this.w_MGSTAINT)+;
             ",MGPRPAGM="+cp_ToStrODBC(this.w_MGPRPAGM)+;
             ",MGPREFIS="+cp_ToStrODBC(this.w_MGPREFIS)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MAGAZZIN')
        i_cWhere = cp_PKFox(i_cTable  ,'MGCODMAG',this.w_MGCODMAG  )
        UPDATE (i_cTable) SET;
              MGDESMAG=this.w_MGDESMAG;
             ,MGINDMAG=this.w_MGINDMAG;
             ,MGMAGCAP=this.w_MGMAGCAP;
             ,MGCAPMAG=this.w_MGCAPMAG;
             ,MGCITMAG=this.w_MGCITMAG;
             ,MGPROMAG=this.w_MGPROMAG;
             ,MGPERSON=this.w_MGPERSON;
             ,MG__NOTE=this.w_MG__NOTE;
             ,MGTELEFO=this.w_MGTELEFO;
             ,MGMAGWEB=this.w_MGMAGWEB;
             ,MG_EMAIL=this.w_MG_EMAIL;
             ,MG_EMPEC=this.w_MG_EMPEC;
             ,MGGESCAR=this.w_MGGESCAR;
             ,MGRIFFOR=this.w_MGRIFFOR;
             ,MGFISMAG=this.w_MGFISMAG;
             ,MGDISMAG=this.w_MGDISMAG;
             ,MGTIPMAG=this.w_MGTIPMAG;
             ,MGFLUBIC=this.w_MGFLUBIC;
             ,MGMAGRAG=this.w_MGMAGRAG;
             ,MGDTINVA=this.w_MGDTINVA;
             ,MGDTOBSO=this.w_MGDTOBSO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,MGSTAINT=this.w_MGSTAINT;
             ,MGPRPAGM=this.w_MGPRPAGM;
             ,MGPREFIS=this.w_MGPREFIS;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MAGAZZIN_IDX,i_nConn)
      *
      * delete MAGAZZIN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MGCODMAG',this.w_MGCODMAG  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,14,.t.)
        if .o_MGGESCAR<>.w_MGGESCAR
            .w_MGRIFFOR = IIF(.w_MGGESCAR='S', .w_MGRIFFOR, SPACE(15))
          .link_1_15('Full')
        endif
        .DoRTCalc(16,31,.t.)
        if .o_MGCODMAG<>.w_MGCODMAG
            .w_OLDFLUB = .w_MGFLUBIC
        endif
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        if .o_MGCODMAG<>.w_MGCODMAG
            .w_ODISMAG = .w_MGDISMAG
        endif
        .DoRTCalc(34,35,.t.)
        if .o_MGSTAINT<>.w_MGSTAINT
            .w_MGPREFIS = IIF(.w_MGSTAINT<>'S',SPACE(20),.w_MGPREFIS)
        endif
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(37,38,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMGMAGWEB_1_11.enabled = this.oPgFrm.Page1.oPag.oMGMAGWEB_1_11.mCond()
    this.oPgFrm.Page1.oPag.oMGRIFFOR_1_15.enabled = this.oPgFrm.Page1.oPag.oMGRIFFOR_1_15.mCond()
    this.oPgFrm.Page1.oPag.oMGFLUBIC_1_20.enabled = this.oPgFrm.Page1.oPag.oMGFLUBIC_1_20.mCond()
    this.oPgFrm.Page1.oPag.oMGMAGRAG_1_21.enabled = this.oPgFrm.Page1.oPag.oMGMAGRAG_1_21.mCond()
    this.oPgFrm.Page1.oPag.oMGPRPAGM_1_48.enabled = this.oPgFrm.Page1.oPag.oMGPRPAGM_1_48.mCond()
    this.oPgFrm.Page1.oPag.oMGPREFIS_1_49.enabled = this.oPgFrm.Page1.oPag.oMGPREFIS_1_49.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMGMAGWEB_1_11.visible=!this.oPgFrm.Page1.oPag.oMGMAGWEB_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_45.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MGRIFFOR
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MGRIFFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MGRIFFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_ANTIPCON;
                     ,'ANCODICE',trim(this.w_MGRIFFOR))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MGRIFFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MGRIFFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oMGRIFFOR_1_15'),i_cWhere,'GSAR_BZC',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MGRIFFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MGRIFFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ANTIPCON;
                       ,'ANCODICE',this.w_MGRIFFOR)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MGRIFFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MGRIFFOR = space(15)
      endif
      this.w_ANDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MGRIFFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MGMAGRAG
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MGMAGRAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MGMAGRAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGMAGRAG,MGFISMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MGMAGRAG))
          select MGCODMAG,MGDESMAG,MGMAGRAG,MGFISMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MGMAGRAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_MGMAGRAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGMAGRAG,MGFISMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_MGMAGRAG)+"%");

            select MGCODMAG,MGDESMAG,MGMAGRAG,MGFISMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MGMAGRAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMGMAGRAG_1_21'),i_cWhere,'',"Magazzini di raggruppamento",'GSMA1AMA.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGMAGRAG,MGFISMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGMAGRAG,MGFISMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MGMAGRAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGMAGRAG,MGFISMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MGMAGRAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MGMAGRAG)
            select MGCODMAG,MGDESMAG,MGMAGRAG,MGFISMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MGMAGRAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESRIF = NVL(_Link_.MGDESMAG,space(30))
      this.w_RAGRIF = NVL(_Link_.MGMAGRAG,space(5))
      this.w_FISMAG = NVL(_Link_.MGFISMAG,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MGMAGRAG = space(5)
      endif
      this.w_DESRIF = space(30)
      this.w_RAGRIF = space(5)
      this.w_FISMAG = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_MGMAGRAG) OR (EMPTY(.w_RAGRIF) AND .w_FISMAG='S')) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino di raggruppamento inesistente, non fiscale oppure obsoleto")
        endif
        this.w_MGMAGRAG = space(5)
        this.w_DESRIF = space(30)
        this.w_RAGRIF = space(5)
        this.w_FISMAG = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MGMAGRAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.MGCODMAG as MGCODMAG121"+ ",link_1_21.MGDESMAG as MGDESMAG121"+ ",link_1_21.MGMAGRAG as MGMAGRAG121"+ ",link_1_21.MGFISMAG as MGFISMAG121"+ ",link_1_21.MGDTOBSO as MGDTOBSO121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on MAGAZZIN.MGMAGRAG=link_1_21.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and MAGAZZIN.MGMAGRAG=link_1_21.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMGCODMAG_1_1.value==this.w_MGCODMAG)
      this.oPgFrm.Page1.oPag.oMGCODMAG_1_1.value=this.w_MGCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDESMAG_1_2.value==this.w_MGDESMAG)
      this.oPgFrm.Page1.oPag.oMGDESMAG_1_2.value=this.w_MGDESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMGINDMAG_1_3.value==this.w_MGINDMAG)
      this.oPgFrm.Page1.oPag.oMGINDMAG_1_3.value=this.w_MGINDMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMGMAGCAP_1_4.value==this.w_MGMAGCAP)
      this.oPgFrm.Page1.oPag.oMGMAGCAP_1_4.value=this.w_MGMAGCAP
    endif
    if not(this.oPgFrm.Page1.oPag.oMGCITMAG_1_6.value==this.w_MGCITMAG)
      this.oPgFrm.Page1.oPag.oMGCITMAG_1_6.value=this.w_MGCITMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMGPROMAG_1_7.value==this.w_MGPROMAG)
      this.oPgFrm.Page1.oPag.oMGPROMAG_1_7.value=this.w_MGPROMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMGPERSON_1_8.value==this.w_MGPERSON)
      this.oPgFrm.Page1.oPag.oMGPERSON_1_8.value=this.w_MGPERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oMG__NOTE_1_9.value==this.w_MG__NOTE)
      this.oPgFrm.Page1.oPag.oMG__NOTE_1_9.value=this.w_MG__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oMGTELEFO_1_10.value==this.w_MGTELEFO)
      this.oPgFrm.Page1.oPag.oMGTELEFO_1_10.value=this.w_MGTELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oMGMAGWEB_1_11.RadioValue()==this.w_MGMAGWEB)
      this.oPgFrm.Page1.oPag.oMGMAGWEB_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMG_EMAIL_1_12.value==this.w_MG_EMAIL)
      this.oPgFrm.Page1.oPag.oMG_EMAIL_1_12.value=this.w_MG_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oMG_EMPEC_1_13.value==this.w_MG_EMPEC)
      this.oPgFrm.Page1.oPag.oMG_EMPEC_1_13.value=this.w_MG_EMPEC
    endif
    if not(this.oPgFrm.Page1.oPag.oMGGESCAR_1_14.RadioValue()==this.w_MGGESCAR)
      this.oPgFrm.Page1.oPag.oMGGESCAR_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGRIFFOR_1_15.value==this.w_MGRIFFOR)
      this.oPgFrm.Page1.oPag.oMGRIFFOR_1_15.value=this.w_MGRIFFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oMGFISMAG_1_17.RadioValue()==this.w_MGFISMAG)
      this.oPgFrm.Page1.oPag.oMGFISMAG_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDISMAG_1_18.RadioValue()==this.w_MGDISMAG)
      this.oPgFrm.Page1.oPag.oMGDISMAG_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGTIPMAG_1_19.RadioValue()==this.w_MGTIPMAG)
      this.oPgFrm.Page1.oPag.oMGTIPMAG_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGFLUBIC_1_20.RadioValue()==this.w_MGFLUBIC)
      this.oPgFrm.Page1.oPag.oMGFLUBIC_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGMAGRAG_1_21.value==this.w_MGMAGRAG)
      this.oPgFrm.Page1.oPag.oMGMAGRAG_1_21.value=this.w_MGMAGRAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIF_1_22.value==this.w_DESRIF)
      this.oPgFrm.Page1.oPag.oDESRIF_1_22.value=this.w_DESRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDTINVA_1_23.value==this.w_MGDTINVA)
      this.oPgFrm.Page1.oPag.oMGDTINVA_1_23.value=this.w_MGDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDTOBSO_1_25.value==this.w_MGDTOBSO)
      this.oPgFrm.Page1.oPag.oMGDTOBSO_1_25.value=this.w_MGDTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oMGSTAINT_1_47.RadioValue()==this.w_MGSTAINT)
      this.oPgFrm.Page1.oPag.oMGSTAINT_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGPRPAGM_1_48.value==this.w_MGPRPAGM)
      this.oPgFrm.Page1.oPag.oMGPRPAGM_1_48.value=this.w_MGPRPAGM
    endif
    if not(this.oPgFrm.Page1.oPag.oMGPREFIS_1_49.value==this.w_MGPREFIS)
      this.oPgFrm.Page1.oPag.oMGPREFIS_1_49.value=this.w_MGPREFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_59.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_59.value=this.w_ANDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'MAGAZZIN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MGCODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMGCODMAG_1_1.SetFocus()
            i_bnoObbl = !empty(.w_MGCODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(.w_MGFLUBIC='S', .w_MGTIPMAG<>'W', .T.))  and (g_PERUBI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMGFLUBIC_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Un magazzino WIP non pu� essere gestito a ubicazioni")
          case   not((EMPTY(.w_MGMAGRAG) OR (EMPTY(.w_RAGRIF) AND .w_FISMAG='S')) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_MGFISMAG='S')  and not(empty(.w_MGMAGRAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMGMAGRAG_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino di raggruppamento inesistente, non fiscale oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MGCODMAG = this.w_MGCODMAG
    this.o_MGGESCAR = this.w_MGGESCAR
    this.o_MGSTAINT = this.w_MGSTAINT
    return

enddefine

* --- Define pages as container
define class tgsar_amaPag1 as StdContainer
  Width  = 538
  height = 416
  stdWidth  = 538
  stdheight = 416
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMGCODMAG_1_1 as StdField with uid="OCHXKDTIBS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MGCODMAG", cQueryName = "MGCODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice alfanumerico del magazzino",;
    HelpContextID = 183892723,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=78, Top=8, InputMask=replicate('X',5)

  add object oMGDESMAG_1_2 as StdField with uid="EVFPSJUDJK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MGDESMAG", cQueryName = "MGDESMAG",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del magazzino",;
    HelpContextID = 168815347,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=142, Top=8, InputMask=replicate('X',30)

  add object oMGINDMAG_1_3 as StdField with uid="PVOIPCERJU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MGINDMAG", cQueryName = "MGINDMAG",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo completo del magazzino",;
    HelpContextID = 183933683,;
   bGlobalFont=.t.,;
    Height=21, Width=255, Left=78, Top=35, InputMask=replicate('X',30)

  add object oMGMAGCAP_1_4 as StdField with uid="VJBBWUSWGF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MGMAGCAP", cQueryName = "MGMAGCAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice avviamento postale",;
    HelpContextID = 80960234,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=78, Top=61, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8), bHasZoom = .t. 

  proc oMGMAGCAP_1_4.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_MGMAGCAP",".w_MGCITMAG",".w_MGPROMAG")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oMGCITMAG_1_6 as StdField with uid="DJCEUQDZIF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MGCITMAG", cQueryName = "MGCITMAG",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� dove � situato il magazzino",;
    HelpContextID = 167508723,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=215, Top=61, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oMGCITMAG_1_6.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_MGMAGCAP",".w_MGCITMAG",".w_MGPROMAG")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oMGPROMAG_1_7 as StdField with uid="IKEUZHUMXC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MGPROMAG", cQueryName = "MGPROMAG",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di appartenenza",;
    HelpContextID = 172108531,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=495, Top=61, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  proc oMGPROMAG_1_7.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_MGPROMAG")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oMGPERSON_1_8 as StdField with uid="LPEEYMLKXG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MGPERSON", cQueryName = "MGPERSON",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento responsabile del magazzino",;
    HelpContextID = 69151468,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=78, Top=88, InputMask=replicate('X',40)

  add object oMG__NOTE_1_9 as StdField with uid="KSYWHRDVAO",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MG__NOTE", cQueryName = "MG__NOTE",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Note di consegna",;
    HelpContextID = 129746187,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=78, Top=115, InputMask=replicate('X',40)

  add object oMGTELEFO_1_10 as StdField with uid="ISCKZGQEXY",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MGTELEFO", cQueryName = "MGTELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Telefono del magazzino",;
    HelpContextID = 226563349,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=78, Top=143, InputMask=replicate('X',18)


  add object oMGMAGWEB_1_11 as StdCombo with uid="MVOOZVPABN",rtseq=11,rtrep=.f.,left=78,top=167,width=222,height=21;
    , ToolTipText = "Se attivo: magazzino gestito dall'applicazione Web Application";
    , HelpContextID = 254584072;
    , cFormVar="w_MGMAGWEB",RowSource=""+"Pubblica su web (magazzino + saldi),"+"Pubblica su web (solo magazzino),"+"Non pubblicare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMGMAGWEB_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'T',;
    iif(this.value =3,'N',;
    'N'))))
  endfunc
  func oMGMAGWEB_1_11.GetRadio()
    this.Parent.oContained.w_MGMAGWEB = this.RadioValue()
    return .t.
  endfunc

  func oMGMAGWEB_1_11.SetRadio()
    this.Parent.oContained.w_MGMAGWEB=trim(this.Parent.oContained.w_MGMAGWEB)
    this.value = ;
      iif(this.Parent.oContained.w_MGMAGWEB=='S',1,;
      iif(this.Parent.oContained.w_MGMAGWEB=='T',2,;
      iif(this.Parent.oContained.w_MGMAGWEB=='N',3,;
      0)))
  endfunc

  func oMGMAGWEB_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ECRM='S' OR g_IZCP $ 'SA' OR g_CPIN='S')
    endwith
   endif
  endfunc

  func oMGMAGWEB_1_11.mHide()
    with this.Parent.oContained
      return (g_ECRM<>'S' AND g_IZCP='N' AND g_CPIN='N')
    endwith
  endfunc

  add object oMG_EMAIL_1_12 as StdField with uid="XCYNLAOKZX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MG_EMAIL", cQueryName = "MG_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo e-mail del magazzino",;
    HelpContextID = 160548114,;
   bGlobalFont=.t.,;
    Height=21, Width=457, Left=78, Top=194, InputMask=replicate('X',254)

  add object oMG_EMPEC_1_13 as StdField with uid="ZXVFXTZBCQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MG_EMPEC", cQueryName = "MG_EMPEC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo e-mail PEC del magazzino",;
    HelpContextID = 143770889,;
   bGlobalFont=.t.,;
    Height=21, Width=457, Left=78, Top=218, InputMask=replicate('X',254)

  add object oMGGESCAR_1_14 as StdCheck with uid="TISJQOVXKW",rtseq=14,rtrep=.f.,left=78, top=240, caption="Gestione caricatore",;
    ToolTipText = "se attivo permette di definire il soggetto caricatore da inserire nella scheda di trasporto",;
    HelpContextID = 68139752,;
    cFormVar="w_MGGESCAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGGESCAR_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMGGESCAR_1_14.GetRadio()
    this.Parent.oContained.w_MGGESCAR = this.RadioValue()
    return .t.
  endfunc

  func oMGGESCAR_1_14.SetRadio()
    this.Parent.oContained.w_MGGESCAR=trim(this.Parent.oContained.w_MGGESCAR)
    this.value = ;
      iif(this.Parent.oContained.w_MGGESCAR=='S',1,;
      0)
  endfunc

  add object oMGRIFFOR_1_15 as StdField with uid="SVLBZJMVAF",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MGRIFFOR", cQueryName = "MGRIFFOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Caricatore predefinito",;
    HelpContextID = 31132392,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=78, Top=264, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ANTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_MGRIFFOR"

  func oMGRIFFOR_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MGGESCAR='S')
    endwith
   endif
  endfunc

  func oMGRIFFOR_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oMGRIFFOR_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMGRIFFOR_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ANTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_ANTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oMGRIFFOR_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Fornitori",'',this.parent.oContained
  endproc
  proc oMGRIFFOR_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_ANTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_MGRIFFOR
     i_obj.ecpSave()
  endproc

  add object oMGFISMAG_1_17 as StdCheck with uid="FWWEXOLDAV",rtseq=16,rtrep=.f.,left=381, top=87, caption="Magazzino fiscale",;
    ToolTipText = "Se attivo: il magazzino � inserito nella stampa inventario",;
    HelpContextID = 168545011,;
    cFormVar="w_MGFISMAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGFISMAG_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMGFISMAG_1_17.GetRadio()
    this.Parent.oContained.w_MGFISMAG = this.RadioValue()
    return .t.
  endfunc

  func oMGFISMAG_1_17.SetRadio()
    this.Parent.oContained.w_MGFISMAG=trim(this.Parent.oContained.w_MGFISMAG)
    this.value = ;
      iif(this.Parent.oContained.w_MGFISMAG=='S',1,;
      0)
  endfunc

  add object oMGDISMAG_1_18 as StdCheck with uid="DLKVINVYJV",rtseq=17,rtrep=.f.,left=381, top=107, caption="Nettificabile",;
    ToolTipText = "Se attivo: magazzino di tipo nettificabile",;
    HelpContextID = 168553203,;
    cFormVar="w_MGDISMAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGDISMAG_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMGDISMAG_1_18.GetRadio()
    this.Parent.oContained.w_MGDISMAG = this.RadioValue()
    return .t.
  endfunc

  func oMGDISMAG_1_18.SetRadio()
    this.Parent.oContained.w_MGDISMAG=trim(this.Parent.oContained.w_MGDISMAG)
    this.value = ;
      iif(this.Parent.oContained.w_MGDISMAG=='S',1,;
      0)
  endfunc

  add object oMGTIPMAG_1_19 as StdCheck with uid="SOTXAKWTMW",rtseq=18,rtrep=.f.,left=381, top=127, caption="WIP",;
    ToolTipText = "Se attivo: magazzino WIP",;
    HelpContextID = 171633395,;
    cFormVar="w_MGTIPMAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGTIPMAG_1_19.RadioValue()
    return(iif(this.value =1,'W',;
    ' '))
  endfunc
  func oMGTIPMAG_1_19.GetRadio()
    this.Parent.oContained.w_MGTIPMAG = this.RadioValue()
    return .t.
  endfunc

  func oMGTIPMAG_1_19.SetRadio()
    this.Parent.oContained.w_MGTIPMAG=trim(this.Parent.oContained.w_MGTIPMAG)
    this.value = ;
      iif(this.Parent.oContained.w_MGTIPMAG=='W',1,;
      0)
  endfunc

  add object oMGFLUBIC_1_20 as StdCheck with uid="VQPRKWMVED",rtseq=19,rtrep=.f.,left=351, top=167, caption="Gestione ubicazioni",;
    ToolTipText = "Se attivo: il magazzino viene gestito per ubicazioni",;
    HelpContextID = 186070281,;
    cFormVar="w_MGFLUBIC", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Un magazzino WIP non pu� essere gestito a ubicazioni";
   , bGlobalFont=.t.


  func oMGFLUBIC_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMGFLUBIC_1_20.GetRadio()
    this.Parent.oContained.w_MGFLUBIC = this.RadioValue()
    return .t.
  endfunc

  func oMGFLUBIC_1_20.SetRadio()
    this.Parent.oContained.w_MGFLUBIC=trim(this.Parent.oContained.w_MGFLUBIC)
    this.value = ;
      iif(this.Parent.oContained.w_MGFLUBIC=='S',1,;
      0)
  endfunc

  func oMGFLUBIC_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI='S')
    endwith
   endif
  endfunc

  func oMGFLUBIC_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(.w_MGFLUBIC='S', .w_MGTIPMAG<>'W', .T.))
    endwith
    return bRes
  endfunc

  add object oMGMAGRAG_1_21 as StdField with uid="CRCUZEMQLL",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MGMAGRAG", cQueryName = "MGMAGRAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino di raggruppamento inesistente, non fiscale oppure obsoleto",;
    ToolTipText = "Codice dell'eventuale magazzino di raggruppamento fiscale",;
    HelpContextID = 97737459,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=214, Top=290, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MGMAGRAG"

  func oMGMAGRAG_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MGFISMAG='S')
    endwith
   endif
  endfunc

  func oMGMAGRAG_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oMGMAGRAG_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMGMAGRAG_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMGMAGRAG_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini di raggruppamento",'GSMA1AMA.MAGAZZIN_VZM',this.parent.oContained
  endproc

  add object oDESRIF_1_22 as StdField with uid="WIYWUOALRQ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESRIF", cQueryName = "DESRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 27393482,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=280, Top=290, InputMask=replicate('X',30)

  add object oMGDTINVA_1_23 as StdField with uid="MNAYUCSMMR",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MGDTINVA", cQueryName = "MGDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 106894599,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=214, Top=318, tabstop=.f.

  add object oMGDTOBSO_1_25 as StdField with uid="BZUPOZUKXU",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MGDTOBSO", cQueryName = "MGDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 88140523,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=447, Top=318, tabstop=.f.


  add object oObj_1_45 as cp_runprogram with uid="TGBJXGGEHG",left=1, top=438, width=186,height=19,;
    caption='GSAR_BKM(A)',;
   bGlobalFont=.t.,;
    prg="GSAR_BKM('A')",;
    cEvent = "Update end",;
    nPag=1;
    , HelpContextID = 197116979

  add object oMGSTAINT_1_47 as StdCheck with uid="TIQBTSJCVM",rtseq=34,rtrep=.f.,left=6, top=366, caption="Stampa intestazione",;
    ToolTipText = "Attivo: stampa intestazione e numerazione pag. nel giornale di magazzino",;
    HelpContextID = 253754086,;
    cFormVar="w_MGSTAINT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGSTAINT_1_47.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMGSTAINT_1_47.GetRadio()
    this.Parent.oContained.w_MGSTAINT = this.RadioValue()
    return .t.
  endfunc

  func oMGSTAINT_1_47.SetRadio()
    this.Parent.oContained.w_MGSTAINT=trim(this.Parent.oContained.w_MGSTAINT)
    this.value = ;
      iif(this.Parent.oContained.w_MGSTAINT=='S',1,;
      0)
  endfunc

  add object oMGPRPAGM_1_48 as StdField with uid="CJOTQZJBYF",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MGPRPAGM", cQueryName = "MGPRPAGM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo pagine nel giornale di magazzino",;
    HelpContextID = 164484371,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=473, Top=366

  func oMGPRPAGM_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MGSTAINT='S' AND .w_MGFISMAG='S')
    endwith
   endif
  endfunc

  add object oMGPREFIS_1_49 as StdField with uid="LHVYGDPNNY",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MGPREFIS", cQueryName = "MGPREFIS",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso numerazione pagine nel giornale di magazzino",;
    HelpContextID = 236836121,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=382, Top=393, InputMask=replicate('X',20)

  func oMGPREFIS_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MGSTAINT='S' AND .w_MGFISMAG='S')
    endwith
   endif
  endfunc


  add object oObj_1_54 as cp_runprogram with uid="YOMUGUKMXB",left=195, top=438, width=204,height=19,;
    caption='GSAR_BKM(B)',;
   bGlobalFont=.t.,;
    prg="GSAR_BKM('B')",;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 197117235


  add object oObj_1_56 as cp_runprogram with uid="PZLTVHFNZK",left=1, top=466, width=186,height=19,;
    caption='GSAR_BKM(C)',;
   bGlobalFont=.t.,;
    prg="GSAR_BKM('C')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 197117491


  add object oObj_1_57 as cp_runprogram with uid="ECTDZFZIPJ",left=195, top=466, width=243,height=19,;
    caption='GSAR_BKM(D)',;
   bGlobalFont=.t.,;
    prg="GSAR_BKM('D')",;
    cEvent = "w_MGFLUBIC Changed",;
    nPag=1;
    , HelpContextID = 197117747

  add object oANDESCRI_1_59 as StdField with uid="BPPIVDRETW",rtseq=38,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 68150449,;
   bGlobalFont=.t.,;
    Height=21, Width=312, Left=214, Top=264, InputMask=replicate('X',40)

  add object oStr_1_16 as StdString with uid="SVHWOXPPPH",Visible=.t., Left=2, Top=35,;
    Alignment=1, Width=75, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="QCCNQNJQHQ",Visible=.t., Left=2, Top=62,;
    Alignment=1, Width=75, Height=15,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="EMOZCCDMRO",Visible=.t., Left=152, Top=61,;
    Alignment=1, Width=61, Height=15,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="SPPQSVQHJA",Visible=.t., Left=2, Top=88,;
    Alignment=1, Width=75, Height=15,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="ZYWOSCNWYW",Visible=.t., Left=2, Top=115,;
    Alignment=1, Width=75, Height=15,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="KYVHGYRWAN",Visible=.t., Left=2, Top=143,;
    Alignment=1, Width=75, Height=15,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="HFLTCXEDCG",Visible=.t., Left=35, Top=194,;
    Alignment=1, Width=42, Height=18,;
    Caption="E-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="MMJIKIEJNE",Visible=.t., Left=319, Top=318,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="BDQWWMUMYX",Visible=.t., Left=457, Top=61,;
    Alignment=1, Width=38, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="WVXUWCGBXD",Visible=.t., Left=103, Top=318,;
    Alignment=1, Width=103, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="QDSHMVXUCV",Visible=.t., Left=2, Top=8,;
    Alignment=1, Width=75, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="RHBWQGRSDH",Visible=.t., Left=24, Top=290,;
    Alignment=1, Width=182, Height=18,;
    Caption="Magazzino di raggruppamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="EPSXHBJNMS",Visible=.t., Left=371, Top=366,;
    Alignment=1, Width=100, Height=18,;
    Caption="N. pag. G.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="MJANKNAJFT",Visible=.t., Left=230, Top=393,;
    Alignment=1, Width=148, Height=18,;
    Caption="Prefisso numerazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="AFUTEDCYZC",Visible=.t., Left=10, Top=341,;
    Alignment=0, Width=269, Height=18,;
    Caption="Giornale di magazzino"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="GRXNFTGBRG",Visible=.t., Left=47, Top=171,;
    Alignment=1, Width=30, Height=18,;
    Caption="Web:"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (g_ECRM<>'S' AND g_IZCP='N' AND g_CPIN='N')
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="EVHSCBXBGS",Visible=.t., Left=14, Top=220,;
    Alignment=1, Width=63, Height=18,;
    Caption="PEC:"  ;
  , bGlobalFont=.t.

  add object oBox_1_52 as StdBox with uid="GIPTQUOKZZ",left=6, top=358, width=527,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ama','MAGAZZIN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MGCODMAG=MAGAZZIN.MGCODMAG";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
