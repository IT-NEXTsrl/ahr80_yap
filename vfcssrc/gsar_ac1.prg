* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ac1                                                        *
*              Categorie contabili articoli                                    *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_2]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2011-11-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_ac1"))

* --- Class definition
define class tgsar_ac1 as StdForm
  Top    = 28
  Left   = 38

  * --- Standard Properties
  Width  = 386
  Height = 64+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-11-03"
  HelpContextID=158763159
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  CACOARTI_IDX = 0
  AZIENDA_IDX = 0
  cFile = "CACOARTI"
  cKeySelect = "C1CODICE"
  cKeyWhere  = "C1CODICE=this.w_C1CODICE"
  cKeyWhereODBC = '"C1CODICE="+cp_ToStrODBC(this.w_C1CODICE)';

  cKeyWhereODBCqualified = '"CACOARTI.C1CODICE="+cp_ToStrODBC(this.w_C1CODICE)';

  cPrg = "gsar_ac1"
  cComment = "Categorie contabili articoli"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0AC1'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_READAZI = space(10)
  w_C1CODICE = space(5)
  w_C1DESCRI = space(35)
  w_FLGEAC = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_C1CODICE = this.W_C1CODICE
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CACOARTI','gsar_ac1')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ac1Pag1","gsar_ac1",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Categoria contabile articoli")
      .Pages(1).HelpContextID = 55541034
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oC1CODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CACOARTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CACOARTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CACOARTI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_C1CODICE = NVL(C1CODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CACOARTI where C1CODICE=KeySet.C1CODICE
    *
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CACOARTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CACOARTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CACOARTI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'C1CODICE',this.w_C1CODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_READAZI = i_CODAZI
        .w_FLGEAC = space(1)
          .link_1_1('Load')
        .w_C1CODICE = NVL(C1CODICE,space(5))
        .op_C1CODICE = .w_C1CODICE
        .w_C1DESCRI = NVL(C1DESCRI,space(35))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CACOARTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READAZI = space(10)
      .w_C1CODICE = space(5)
      .w_C1DESCRI = space(35)
      .w_FLGEAC = space(1)
      if .cFunction<>"Filter"
        .w_READAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_READAZI))
          .link_1_1('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CACOARTI')
    this.DoRTCalc(2,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    with this	
      if .w_FLGEAC='S'
        cp_AskTableProg(this,i_nConn,"PROCA","i_codazi,w_C1CODICE")
      endif
      .op_codazi = .w_codazi
      .op_C1CODICE = .w_C1CODICE
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oC1CODICE_1_2.enabled = i_bVal
      .Page1.oPag.oC1DESCRI_1_3.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oC1CODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oC1CODICE_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CACOARTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_C1CODICE,"C1CODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_C1DESCRI,"C1DESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    i_lTable = "CACOARTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CACOARTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SCC with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CACOARTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CACOARTI_IDX,i_nConn)
      with this
        if .w_FLGEAC='S'
          cp_NextTableProg(this,i_nConn,"PROCA","i_codazi,w_C1CODICE")
        endif
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CACOARTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CACOARTI')
        i_extval=cp_InsertValODBCExtFlds(this,'CACOARTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(C1CODICE,C1DESCRI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_C1CODICE)+;
                  ","+cp_ToStrODBC(this.w_C1DESCRI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CACOARTI')
        i_extval=cp_InsertValVFPExtFlds(this,'CACOARTI')
        cp_CheckDeletedKey(i_cTable,0,'C1CODICE',this.w_C1CODICE)
        INSERT INTO (i_cTable);
              (C1CODICE,C1DESCRI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_C1CODICE;
                  ,this.w_C1DESCRI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CACOARTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CACOARTI_IDX,i_nConn)
      *
      * update CACOARTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CACOARTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " C1DESCRI="+cp_ToStrODBC(this.w_C1DESCRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CACOARTI')
        i_cWhere = cp_PKFox(i_cTable  ,'C1CODICE',this.w_C1CODICE  )
        UPDATE (i_cTable) SET;
              C1DESCRI=this.w_C1DESCRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CACOARTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CACOARTI_IDX,i_nConn)
      *
      * delete CACOARTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'C1CODICE',this.w_C1CODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
          if .w_FLGEAC='S'
             cp_AskTableProg(this,i_nConn,"PROCA","i_codazi,w_C1CODICE")
          endif
          .op_C1CODICE = .w_C1CODICE
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(2,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_FDHPLZUXPC()
    with this
          * --- Autnumber condizionato
          .w_C1CODICE = IIF(.w_FLGEAC='S',.w_C1CODICE,SPACE(5))
          .op_C1CODICE = .w_C1CODICE
    endwith
  endproc
  proc Calculate_WTBSFWEPUN()
    with this
          * --- Delete start
          GSVE_BCT(this;
              ,.w_C1CODICE;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("New record")
          .Calculate_FDHPLZUXPC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete start")
          .Calculate_WTBSFWEPUN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLGEAC";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_READAZI)
            select AZCODAZI,AZFLGEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.AZCODAZI,space(10))
      this.w_FLGEAC = NVL(_Link_.AZFLGEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(10)
      endif
      this.w_FLGEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oC1CODICE_1_2.value==this.w_C1CODICE)
      this.oPgFrm.Page1.oPag.oC1CODICE_1_2.value=this.w_C1CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oC1DESCRI_1_3.value==this.w_C1DESCRI)
      this.oPgFrm.Page1.oPag.oC1DESCRI_1_3.value=this.w_C1DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'CACOARTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_C1CODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oC1CODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_C1CODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_ac1Pag1 as StdContainer
  Width  = 382
  height = 64
  stdWidth  = 382
  stdheight = 64
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oC1CODICE_1_2 as StdField with uid="LCERHATQKS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_C1CODICE", cQueryName = "C1CODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria contabile articoli",;
    HelpContextID = 150344085,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=90, Top=11, InputMask=replicate('X',5)

  add object oC1DESCRI_1_3 as StdField with uid="KSUXHATCYS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_C1DESCRI", cQueryName = "C1DESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della categoria contabile",;
    HelpContextID = 235930001,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=90, Top=39, InputMask=replicate('X',35)

  add object oStr_1_4 as StdString with uid="BKCRGRMTHO",Visible=.t., Left=45, Top=11,;
    Alignment=1, Width=42, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="TDRDFFCIRN",Visible=.t., Left=5, Top=39,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ac1','CACOARTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".C1CODICE=CACOARTI.C1CODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
