* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bpa                                                        *
*              Controlli cancellazione pagamenti                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_45]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-09-20                                                      *
* Last revis.: 2006-03-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bpa",oParentObject)
return(i_retval)

define class tgsar_bpa as StdBatch
  * --- Local variables
  w_CODPAG = space(5)
  w_MESS = space(100)
  w_AZIENDA = space(5)
  * --- WorkFile variables
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_AZIENDA = i_CODAZI
    * --- Controlli per la cancellazione
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COSAPAGA"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COSAPAGA = "+cp_ToStrODBC(this.oParentObject.w_PACODICE);
            +" and COCODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COSAPAGA;
        from (i_cTable) where;
            COSAPAGA = this.oParentObject.w_PACODICE;
            and COCODAZI = this.w_AZIENDA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODPAG = NVL(cp_ToDate(_read_.COSAPAGA),cp_NullValue(_read_.COSAPAGA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if I_ROWS<>0
      this.w_MESS = ah_MsgFormat("Codice pagamento presente in PARAMETRI DIFF. E ABBUONI, impossibile cancellare" )
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COPAGRIC"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COPAGRIC = "+cp_ToStrODBC(this.oParentObject.w_PACODICE);
            +" and COCODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COPAGRIC;
        from (i_cTable) where;
            COPAGRIC = this.oParentObject.w_PACODICE;
            and COCODAZI = this.w_AZIENDA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODPAG = NVL(cp_ToDate(_read_.COPAGRIC),cp_NullValue(_read_.COPAGRIC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if I_ROWS<>0
      this.w_MESS = ah_MsgFormat("Codice pagamento presente in PARAMETRI RICEVUTE FISCALI, impossibile cancellare" )
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTROPA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
