* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_acc                                                        *
*              Causali contabili                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [116] [VRS_160]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-17                                                      *
* Last revis.: 2015-02-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_acc"))

* --- Class definition
define class tgscg_acc as StdForm
  Top    = 1
  Left   = 13

  * --- Standard Properties
  Width  = 796
  Height = 506+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-02-12"
  HelpContextID=109716841
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=66

  * --- Constant Properties
  CAU_CONT_IDX = 0
  CAUIVA1_IDX = 0
  CAUPRI1_IDX = 0
  CONTI_IDX = 0
  CCC_MAST_IDX = 0
  CAU_CESP_IDX = 0
  PAG_AMEN_IDX = 0
  cFile = "CAU_CONT"
  cKeySelect = "CCCODICE"
  cKeyWhere  = "CCCODICE=this.w_CCCODICE"
  cKeyWhereODBC = '"CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cKeyWhereODBCqualified = '"CAU_CONT.CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cPrg = "gscg_acc"
  cComment = "Causali contabili"
  icon = "anag.ico"
  cAutoZoom = 'GSCG0ACC'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPCON = space(1)
  w_CCCODICE = space(5)
  w_CCDESCRI = space(35)
  w_CCFLRIFE = space(1)
  o_CCFLRIFE = space(1)
  w_CCFLPART = space(1)
  o_CCFLPART = space(1)
  w_CCFLCOMP = space(1)
  w_CCFLRITE = space(1)
  w_CCGESRIT = space(1)
  w_CCFLINSO = space(1)
  w_CCFLSALI = space(1)
  o_CCFLSALI = space(1)
  w_CCFLSALF = space(1)
  o_CCFLSALF = space(1)
  w_CCFLASSE = space(1)
  w_CCFLANAL = space(1)
  w_CCPAGINS = space(5)
  w_CCTIPREG = space(1)
  o_CCTIPREG = space(1)
  w_CCNUMREG = 0
  w_CCFLBESE = space(1)
  w_CCCONIVA = space(15)
  w_DESSOT = space(40)
  w_CCTIPDOC = space(2)
  o_CCTIPDOC = space(2)
  w_CCCFDAVE = space(1)
  w_CCCALDOC = space(1)
  w_CCTESDOC = space(1)
  w_CCFLIVDF = space(1)
  w_CCFLPDIF = space(1)
  w_CCSCIPAG = space(1)
  w_CCFLAUTR = space(1)
  w_CCREGMAR = space(1)
  w_CCFLPDOC = space(1)
  o_CCFLPDOC = space(1)
  w_CCFLSTDA = space(1)
  w_CCFLCOSE = space(1)
  w_CCSERDOC = space(10)
  w_CCNUMDOC = space(1)
  o_CCNUMDOC = space(1)
  w_CCFLPPRO = space(1)
  o_CCFLPPRO = space(1)
  w_CCSERPRO = space(10)
  w_CCFLMOVC = space(1)
  o_CCFLMOVC = space(1)
  w_CCCAUMOV = space(5)
  w_CCCAUSBF = space(5)
  w_CCMOVCES = space(1)
  o_CCMOVCES = space(1)
  w_CCCAUCES = space(5)
  w_CCDTINVA = ctod('  /  /  ')
  w_CCDTOBSO = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_TIPSOT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CODI = space(5)
  w_DESC = space(35)
  w_CAUDESRI = space(35)
  w_TIPCAU = space(1)
  w_CIFLPART = space(1)
  w_UNIREG = space(1)
  w_DESCCESP = space(40)
  w_DTOBSOCA = ctod('  /  /  ')
  w_DESCSBF = space(35)
  w_TIPSBF = space(1)
  w_CODI1 = space(5)
  w_DESC1 = space(35)
  w_CCDESSUP = space(254)
  w_CCDESRIG = space(254)
  w_DESPAG = space(30)
  w_CCGESCAS = space(1)
  w_OLDTIPDOC = space(2)

  * --- Children pointers
  GSCG_MCI = .NULL.
  GSCG_MCP = .NULL.
  GSCG_MTC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAU_CONT','gscg_acc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_accPag1","gscg_acc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).HelpContextID = 234740533
      .Pages(2).addobject("oPag","tgscg_accPag2","gscg_acc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettagli")
      .Pages(2).HelpContextID = 150895457
      .Pages(3).addobject("oPag","tgscg_accPag3","gscg_acc",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Traduzioni")
      .Pages(3).HelpContextID = 80686459
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCCCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CAUIVA1'
    this.cWorkTables[2]='CAUPRI1'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='CCC_MAST'
    this.cWorkTables[5]='CAU_CESP'
    this.cWorkTables[6]='PAG_AMEN'
    this.cWorkTables[7]='CAU_CONT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAU_CONT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAU_CONT_IDX,3]
  return

  function CreateChildren()
    this.GSCG_MCI = CREATEOBJECT('stdDynamicChild',this,'GSCG_MCI',this.oPgFrm.Page2.oPag.oLinkPC_2_3)
    this.GSCG_MCP = CREATEOBJECT('stdDynamicChild',this,'GSCG_MCP',this.oPgFrm.Page2.oPag.oLinkPC_2_4)
    this.GSCG_MTC = CREATEOBJECT('stdDynamicChild',this,'GSCG_MTC',this.oPgFrm.Page3.oPag.oLinkPC_3_4)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_MCI)
      this.GSCG_MCI.DestroyChildrenChain()
      this.GSCG_MCI=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_3')
    if !ISNULL(this.GSCG_MCP)
      this.GSCG_MCP.DestroyChildrenChain()
      this.GSCG_MCP=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_4')
    if !ISNULL(this.GSCG_MTC)
      this.GSCG_MTC.DestroyChildrenChain()
      this.GSCG_MTC=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_4')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_MCI.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MCP.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MTC.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_MCI.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MCP.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MTC.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_MCI.NewDocument()
    this.GSCG_MCP.NewDocument()
    this.GSCG_MTC.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_MCI.SetKey(;
            .w_CCCODICE,"AICODCAU";
            )
      this.GSCG_MCP.SetKey(;
            .w_CCCODICE,"APCODCAU";
            )
      this.GSCG_MTC.SetKey(;
            .w_CCCODICE,"LGCODCAU";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_MCI.ChangeRow(this.cRowID+'      1',1;
             ,.w_CCCODICE,"AICODCAU";
             )
      .GSCG_MCP.ChangeRow(this.cRowID+'      1',1;
             ,.w_CCCODICE,"APCODCAU";
             )
      .GSCG_MTC.ChangeRow(this.cRowID+'      1',1;
             ,.w_CCCODICE,"LGCODCAU";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_MCI)
        i_f=.GSCG_MCI.BuildFilter()
        if !(i_f==.GSCG_MCI.cQueryFilter)
          i_fnidx=.GSCG_MCI.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MCI.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MCI.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MCI.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MCI.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_MCP)
        i_f=.GSCG_MCP.BuildFilter()
        if !(i_f==.GSCG_MCP.cQueryFilter)
          i_fnidx=.GSCG_MCP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MCP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MCP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MCP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MCP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_MTC)
        i_f=.GSCG_MTC.BuildFilter()
        if !(i_f==.GSCG_MTC.cQueryFilter)
          i_fnidx=.GSCG_MTC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MTC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MTC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MTC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MTC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CCCODICE = NVL(CCCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_37_joined
    link_1_37_joined=.f.
    local link_1_38_joined
    link_1_38_joined=.f.
    local link_1_40_joined
    link_1_40_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAU_CONT where CCCODICE=KeySet.CCCODICE
    *
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAU_CONT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAU_CONT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAU_CONT '
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_37_joined=this.AddJoinedLink_1_37(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_40_joined=this.AddJoinedLink_1_40(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESSOT = space(40)
        .w_TIPSOT = space(1)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_CAUDESRI = space(35)
        .w_TIPCAU = space(1)
        .w_CIFLPART = space(1)
        .w_UNIREG = 'S'
        .w_DESCCESP = space(40)
        .w_DTOBSOCA = ctod("  /  /  ")
        .w_DESCSBF = space(35)
        .w_TIPSBF = space(1)
        .w_DESPAG = space(30)
        .w_TIPCON = 'G'
        .w_CCCODICE = NVL(CCCODICE,space(5))
        .w_CCDESCRI = NVL(CCDESCRI,space(35))
        .w_CCFLRIFE = NVL(CCFLRIFE,space(1))
        .w_CCFLPART = NVL(CCFLPART,space(1))
        .w_CCFLCOMP = NVL(CCFLCOMP,space(1))
        .w_CCFLRITE = NVL(CCFLRITE,space(1))
        .w_CCGESRIT = NVL(CCGESRIT,space(1))
        .w_CCFLINSO = NVL(CCFLINSO,space(1))
        .w_CCFLSALI = NVL(CCFLSALI,space(1))
        .w_CCFLSALF = NVL(CCFLSALF,space(1))
        .w_CCFLASSE = NVL(CCFLASSE,space(1))
        .w_CCFLANAL = NVL(CCFLANAL,space(1))
        .w_CCPAGINS = NVL(CCPAGINS,space(5))
          if link_1_14_joined
            this.w_CCPAGINS = NVL(PACODICE114,NVL(this.w_CCPAGINS,space(5)))
            this.w_DESPAG = NVL(PADESCRI114,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(PADTOBSO114),ctod("  /  /  "))
          else
          .link_1_14('Load')
          endif
        .w_CCTIPREG = NVL(CCTIPREG,space(1))
        .w_CCNUMREG = NVL(CCNUMREG,0)
        .w_CCFLBESE = NVL(CCFLBESE,space(1))
        .w_CCCONIVA = NVL(CCCONIVA,space(15))
          .link_1_18('Load')
        .w_CCTIPDOC = NVL(CCTIPDOC,space(2))
        .w_CCCFDAVE = NVL(CCCFDAVE,space(1))
        .w_CCCALDOC = NVL(CCCALDOC,space(1))
        .w_CCTESDOC = NVL(CCTESDOC,space(1))
        .w_CCFLIVDF = NVL(CCFLIVDF,space(1))
        .w_CCFLPDIF = NVL(CCFLPDIF,space(1))
        .w_CCSCIPAG = NVL(CCSCIPAG,space(1))
        .w_CCFLAUTR = NVL(CCFLAUTR,space(1))
        .w_CCREGMAR = NVL(CCREGMAR,space(1))
        .w_CCFLPDOC = NVL(CCFLPDOC,space(1))
        .w_CCFLSTDA = NVL(CCFLSTDA,space(1))
        .w_CCFLCOSE = NVL(CCFLCOSE,space(1))
        .w_CCSERDOC = NVL(CCSERDOC,space(10))
        .w_CCNUMDOC = NVL(CCNUMDOC,space(1))
        .w_CCFLPPRO = NVL(CCFLPPRO,space(1))
        .w_CCSERPRO = NVL(CCSERPRO,space(10))
        .w_CCFLMOVC = NVL(CCFLMOVC,space(1))
        .w_CCCAUMOV = NVL(CCCAUMOV,space(5))
          if link_1_37_joined
            this.w_CCCAUMOV = NVL(CACODICE137,NVL(this.w_CCCAUMOV,space(5)))
            this.w_CAUDESRI = NVL(CADESCRI137,space(35))
            this.w_TIPCAU = NVL(CATIPCON137,space(1))
          else
          .link_1_37('Load')
          endif
        .w_CCCAUSBF = NVL(CCCAUSBF,space(5))
          if link_1_38_joined
            this.w_CCCAUSBF = NVL(CACODICE138,NVL(this.w_CCCAUSBF,space(5)))
            this.w_DESCSBF = NVL(CADESCRI138,space(35))
            this.w_TIPSBF = NVL(CATIPCON138,space(1))
          else
          .link_1_38('Load')
          endif
        .w_CCMOVCES = NVL(CCMOVCES,space(1))
        .w_CCCAUCES = NVL(CCCAUCES,space(5))
          if link_1_40_joined
            this.w_CCCAUCES = NVL(CCCODICE140,NVL(this.w_CCCAUCES,space(5)))
            this.w_DESCCESP = NVL(CCDESCRI140,space(40))
            this.w_DTOBSOCA = NVL(cp_ToDate(CCDTOBSO140),ctod("  /  /  "))
          else
          .link_1_40('Load')
          endif
        .w_CCDTINVA = NVL(cp_ToDate(CCDTINVA),ctod("  /  /  "))
        .w_CCDTOBSO = NVL(cp_ToDate(CCDTOBSO),ctod("  /  /  "))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .w_CODI = .w_CCCODICE
        .w_DESC = .w_CCDESCRI
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .w_CODI1 = .w_CCCODICE
        .w_DESC1 = .w_CCDESCRI
        .w_CCDESSUP = NVL(CCDESSUP,space(254))
        .w_CCDESRIG = NVL(CCDESRIG,space(254))
        .w_CCGESCAS = NVL(CCGESCAS,space(1))
        .w_OLDTIPDOC = .w_CCTIPDOC
        cp_LoadRecExtFlds(this,'CAU_CONT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON = space(1)
      .w_CCCODICE = space(5)
      .w_CCDESCRI = space(35)
      .w_CCFLRIFE = space(1)
      .w_CCFLPART = space(1)
      .w_CCFLCOMP = space(1)
      .w_CCFLRITE = space(1)
      .w_CCGESRIT = space(1)
      .w_CCFLINSO = space(1)
      .w_CCFLSALI = space(1)
      .w_CCFLSALF = space(1)
      .w_CCFLASSE = space(1)
      .w_CCFLANAL = space(1)
      .w_CCPAGINS = space(5)
      .w_CCTIPREG = space(1)
      .w_CCNUMREG = 0
      .w_CCFLBESE = space(1)
      .w_CCCONIVA = space(15)
      .w_DESSOT = space(40)
      .w_CCTIPDOC = space(2)
      .w_CCCFDAVE = space(1)
      .w_CCCALDOC = space(1)
      .w_CCTESDOC = space(1)
      .w_CCFLIVDF = space(1)
      .w_CCFLPDIF = space(1)
      .w_CCSCIPAG = space(1)
      .w_CCFLAUTR = space(1)
      .w_CCREGMAR = space(1)
      .w_CCFLPDOC = space(1)
      .w_CCFLSTDA = space(1)
      .w_CCFLCOSE = space(1)
      .w_CCSERDOC = space(10)
      .w_CCNUMDOC = space(1)
      .w_CCFLPPRO = space(1)
      .w_CCSERPRO = space(10)
      .w_CCFLMOVC = space(1)
      .w_CCCAUMOV = space(5)
      .w_CCCAUSBF = space(5)
      .w_CCMOVCES = space(1)
      .w_CCCAUCES = space(5)
      .w_CCDTINVA = ctod("  /  /  ")
      .w_CCDTOBSO = ctod("  /  /  ")
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_TIPSOT = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_CODI = space(5)
      .w_DESC = space(35)
      .w_CAUDESRI = space(35)
      .w_TIPCAU = space(1)
      .w_CIFLPART = space(1)
      .w_UNIREG = space(1)
      .w_DESCCESP = space(40)
      .w_DTOBSOCA = ctod("  /  /  ")
      .w_DESCSBF = space(35)
      .w_TIPSBF = space(1)
      .w_CODI1 = space(5)
      .w_DESC1 = space(35)
      .w_CCDESSUP = space(254)
      .w_CCDESRIG = space(254)
      .w_DESPAG = space(30)
      .w_CCGESCAS = space(1)
      .w_OLDTIPDOC = space(2)
      if .cFunction<>"Filter"
        .w_TIPCON = 'G'
          .DoRTCalc(2,3,.f.)
        .w_CCFLRIFE = 'N'
        .w_CCFLPART = 'N'
        .w_CCFLCOMP = 'N'
          .DoRTCalc(7,7,.f.)
        .w_CCGESRIT = 'N'
          .DoRTCalc(9,9,.f.)
        .w_CCFLSALI = 'N'
        .w_CCFLSALF = 'N'
        .w_CCFLASSE = IIF(.w_CCFLPART='N' AND .w_CCTIPREG='N' AND .w_CCFLSALI='N' AND .w_CCFLSALF='N' AND .w_CCFLPDOC$'LN' And .w_CCFLPPRO$'LN',.w_CCFLASSE,' ')
        .w_CCFLANAL = g_PERCCR
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_CCPAGINS))
          .link_1_14('Full')
          endif
        .w_CCTIPREG = 'N'
          .DoRTCalc(16,16,.f.)
        .w_CCFLBESE = 'B'
        .w_CCCONIVA = IIF (.w_CCTIPREG $ 'AVC',.w_CCCONIVA,SPACE(15))
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_CCCONIVA))
          .link_1_18('Full')
          endif
          .DoRTCalc(19,19,.f.)
        .w_CCTIPDOC = 'NO'
          .DoRTCalc(21,21,.f.)
        .w_CCCALDOC = 'N'
        .w_CCTESDOC = IIF(.w_CCFLPDOC='N' AND .w_CCFLPPRO='N',' ',.w_CCTESDOC)
          .DoRTCalc(24,25,.f.)
        .w_CCSCIPAG = 'N'
          .DoRTCalc(27,27,.f.)
        .w_CCREGMAR = iif(.w_CCTIPREG='N',' ',.w_CCREGMAR)
        .w_CCFLPDOC = IIF(.w_CCTIPREG$'CE','L','N')
        .w_CCFLSTDA = iif(.w_CCTIPREG$'C-E-N',' ',.w_CCFLSTDA)
        .w_CCFLCOSE = IIF(.w_CCFLRIFE="C" and .w_CCTIPREG $ "CE", .w_CCFLCOSE, " ")
          .DoRTCalc(32,32,.f.)
        .w_CCNUMDOC = iif(.w_CCFLPDOC='N' AND .w_CCFLPPRO='N',' ',.w_CCNUMDOC)
        .w_CCFLPPRO = 'N'
          .DoRTCalc(35,36,.f.)
        .w_CCCAUMOV = SPACE(5)
        .DoRTCalc(37,37,.f.)
          if not(empty(.w_CCCAUMOV))
          .link_1_37('Full')
          endif
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_CCCAUSBF))
          .link_1_38('Full')
          endif
          .DoRTCalc(39,39,.f.)
        .w_CCCAUCES = IIF(.w_CCMOVCES='S',.w_CCCAUCES,SPACE(5))
        .DoRTCalc(40,40,.f.)
          if not(empty(.w_CCCAUCES))
          .link_1_40('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
          .DoRTCalc(41,47,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(49,49,.f.)
        .w_CODI = .w_CCCODICE
        .w_DESC = .w_CCDESCRI
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
          .DoRTCalc(52,54,.f.)
        .w_UNIREG = 'S'
          .DoRTCalc(56,59,.f.)
        .w_CODI1 = .w_CCCODICE
        .w_DESC1 = .w_CCDESCRI
          .DoRTCalc(62,64,.f.)
        .w_CCGESCAS = iif(.w_CCFLPART<>'N','N','S')
        .w_OLDTIPDOC = .w_CCTIPDOC
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAU_CONT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCCCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oCCDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oCCFLRIFE_1_4.enabled = i_bVal
      .Page1.oPag.oCCFLPART_1_5.enabled = i_bVal
      .Page1.oPag.oCCFLCOMP_1_6.enabled = i_bVal
      .Page1.oPag.oCCFLRITE_1_7.enabled = i_bVal
      .Page1.oPag.oCCGESRIT_1_8.enabled = i_bVal
      .Page1.oPag.oCCFLINSO_1_9.enabled = i_bVal
      .Page1.oPag.oCCFLSALI_1_10.enabled = i_bVal
      .Page1.oPag.oCCFLSALF_1_11.enabled = i_bVal
      .Page1.oPag.oCCFLASSE_1_12.enabled = i_bVal
      .Page1.oPag.oCCFLANAL_1_13.enabled = i_bVal
      .Page1.oPag.oCCPAGINS_1_14.enabled = i_bVal
      .Page1.oPag.oCCTIPREG_1_15.enabled = i_bVal
      .Page1.oPag.oCCNUMREG_1_16.enabled = i_bVal
      .Page1.oPag.oCCFLBESE_1_17.enabled = i_bVal
      .Page1.oPag.oCCCONIVA_1_18.enabled = i_bVal
      .Page1.oPag.oCCTIPDOC_1_20.enabled = i_bVal
      .Page1.oPag.oCCCFDAVE_1_21.enabled = i_bVal
      .Page1.oPag.oCCCALDOC_1_22.enabled = i_bVal
      .Page1.oPag.oCCTESDOC_1_23.enabled = i_bVal
      .Page1.oPag.oCCFLIVDF_1_24.enabled = i_bVal
      .Page1.oPag.oCCFLPDIF_1_25.enabled = i_bVal
      .Page1.oPag.oCCSCIPAG_1_26.enabled = i_bVal
      .Page1.oPag.oCCFLAUTR_1_27.enabled = i_bVal
      .Page1.oPag.oCCREGMAR_1_28.enabled = i_bVal
      .Page1.oPag.oCCFLPDOC_1_29.enabled = i_bVal
      .Page1.oPag.oCCFLSTDA_1_30.enabled = i_bVal
      .Page1.oPag.oCCFLCOSE_1_31.enabled = i_bVal
      .Page1.oPag.oCCSERDOC_1_32.enabled = i_bVal
      .Page1.oPag.oCCNUMDOC_1_33.enabled = i_bVal
      .Page1.oPag.oCCFLPPRO_1_34.enabled = i_bVal
      .Page1.oPag.oCCSERPRO_1_35.enabled = i_bVal
      .Page1.oPag.oCCFLMOVC_1_36.enabled = i_bVal
      .Page1.oPag.oCCCAUMOV_1_37.enabled = i_bVal
      .Page1.oPag.oCCCAUSBF_1_38.enabled = i_bVal
      .Page1.oPag.oCCMOVCES_1_39.enabled = i_bVal
      .Page1.oPag.oCCCAUCES_1_40.enabled = i_bVal
      .Page1.oPag.oCCDTINVA_1_42.enabled = i_bVal
      .Page1.oPag.oCCDTOBSO_1_44.enabled = i_bVal
      .Page2.oPag.oCCDESSUP_2_10.enabled = i_bVal
      .Page2.oPag.oCCDESRIG_2_11.enabled = i_bVal
      .Page1.oPag.oCCGESCAS_1_92.enabled = i_bVal
      .Page1.oPag.oObj_1_54.enabled = i_bVal
      .Page1.oPag.oObj_1_55.enabled = i_bVal
      .Page1.oPag.oObj_1_78.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCCCODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCCCODICE_1_2.enabled = .t.
      endif
    endwith
    this.GSCG_MCI.SetStatus(i_cOp)
    this.GSCG_MCP.SetStatus(i_cOp)
    this.GSCG_MTC.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CAU_CONT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_MCI.SetChildrenStatus(i_cOp)
  *  this.GSCG_MCP.SetChildrenStatus(i_cOp)
  *  this.GSCG_MTC.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODICE,"CCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDESCRI,"CCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLRIFE,"CCFLRIFE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLPART,"CCFLPART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLCOMP,"CCFLCOMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLRITE,"CCFLRITE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCGESRIT,"CCGESRIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLINSO,"CCFLINSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLSALI,"CCFLSALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLSALF,"CCFLSALF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLASSE,"CCFLASSE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLANAL,"CCFLANAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCPAGINS,"CCPAGINS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCTIPREG,"CCTIPREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCNUMREG,"CCNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLBESE,"CCFLBESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCONIVA,"CCCONIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCTIPDOC,"CCTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCFDAVE,"CCCFDAVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCALDOC,"CCCALDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCTESDOC,"CCTESDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLIVDF,"CCFLIVDF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLPDIF,"CCFLPDIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCSCIPAG,"CCSCIPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLAUTR,"CCFLAUTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCREGMAR,"CCREGMAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLPDOC,"CCFLPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLSTDA,"CCFLSTDA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLCOSE,"CCFLCOSE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCSERDOC,"CCSERDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCNUMDOC,"CCNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLPPRO,"CCFLPPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCSERPRO,"CCSERPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLMOVC,"CCFLMOVC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCAUMOV,"CCCAUMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCAUSBF,"CCCAUSBF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCMOVCES,"CCMOVCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCAUCES,"CCCAUCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDTINVA,"CCDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDTOBSO,"CCDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDESSUP,"CCDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDESRIG,"CCDESRIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCGESCAS,"CCGESCAS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    i_lTable = "CAU_CONT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAU_CONT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCG_SCA with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAU_CONT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAU_CONT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAU_CONT')
        i_extval=cp_InsertValODBCExtFlds(this,'CAU_CONT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CCCODICE,CCDESCRI,CCFLRIFE,CCFLPART,CCFLCOMP"+;
                  ",CCFLRITE,CCGESRIT,CCFLINSO,CCFLSALI,CCFLSALF"+;
                  ",CCFLASSE,CCFLANAL,CCPAGINS,CCTIPREG,CCNUMREG"+;
                  ",CCFLBESE,CCCONIVA,CCTIPDOC,CCCFDAVE,CCCALDOC"+;
                  ",CCTESDOC,CCFLIVDF,CCFLPDIF,CCSCIPAG,CCFLAUTR"+;
                  ",CCREGMAR,CCFLPDOC,CCFLSTDA,CCFLCOSE,CCSERDOC"+;
                  ",CCNUMDOC,CCFLPPRO,CCSERPRO,CCFLMOVC,CCCAUMOV"+;
                  ",CCCAUSBF,CCMOVCES,CCCAUCES,CCDTINVA,CCDTOBSO"+;
                  ",UTCC,UTCV,UTDC,UTDV,CCDESSUP"+;
                  ",CCDESRIG,CCGESCAS "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CCCODICE)+;
                  ","+cp_ToStrODBC(this.w_CCDESCRI)+;
                  ","+cp_ToStrODBC(this.w_CCFLRIFE)+;
                  ","+cp_ToStrODBC(this.w_CCFLPART)+;
                  ","+cp_ToStrODBC(this.w_CCFLCOMP)+;
                  ","+cp_ToStrODBC(this.w_CCFLRITE)+;
                  ","+cp_ToStrODBC(this.w_CCGESRIT)+;
                  ","+cp_ToStrODBC(this.w_CCFLINSO)+;
                  ","+cp_ToStrODBC(this.w_CCFLSALI)+;
                  ","+cp_ToStrODBC(this.w_CCFLSALF)+;
                  ","+cp_ToStrODBC(this.w_CCFLASSE)+;
                  ","+cp_ToStrODBC(this.w_CCFLANAL)+;
                  ","+cp_ToStrODBCNull(this.w_CCPAGINS)+;
                  ","+cp_ToStrODBC(this.w_CCTIPREG)+;
                  ","+cp_ToStrODBC(this.w_CCNUMREG)+;
                  ","+cp_ToStrODBC(this.w_CCFLBESE)+;
                  ","+cp_ToStrODBCNull(this.w_CCCONIVA)+;
                  ","+cp_ToStrODBC(this.w_CCTIPDOC)+;
                  ","+cp_ToStrODBC(this.w_CCCFDAVE)+;
                  ","+cp_ToStrODBC(this.w_CCCALDOC)+;
                  ","+cp_ToStrODBC(this.w_CCTESDOC)+;
                  ","+cp_ToStrODBC(this.w_CCFLIVDF)+;
                  ","+cp_ToStrODBC(this.w_CCFLPDIF)+;
                  ","+cp_ToStrODBC(this.w_CCSCIPAG)+;
                  ","+cp_ToStrODBC(this.w_CCFLAUTR)+;
                  ","+cp_ToStrODBC(this.w_CCREGMAR)+;
                  ","+cp_ToStrODBC(this.w_CCFLPDOC)+;
                  ","+cp_ToStrODBC(this.w_CCFLSTDA)+;
                  ","+cp_ToStrODBC(this.w_CCFLCOSE)+;
                  ","+cp_ToStrODBC(this.w_CCSERDOC)+;
                  ","+cp_ToStrODBC(this.w_CCNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_CCFLPPRO)+;
                  ","+cp_ToStrODBC(this.w_CCSERPRO)+;
                  ","+cp_ToStrODBC(this.w_CCFLMOVC)+;
                  ","+cp_ToStrODBCNull(this.w_CCCAUMOV)+;
                  ","+cp_ToStrODBCNull(this.w_CCCAUSBF)+;
                  ","+cp_ToStrODBC(this.w_CCMOVCES)+;
                  ","+cp_ToStrODBCNull(this.w_CCCAUCES)+;
                  ","+cp_ToStrODBC(this.w_CCDTINVA)+;
                  ","+cp_ToStrODBC(this.w_CCDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_CCDESSUP)+;
                  ","+cp_ToStrODBC(this.w_CCDESRIG)+;
                  ","+cp_ToStrODBC(this.w_CCGESCAS)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAU_CONT')
        i_extval=cp_InsertValVFPExtFlds(this,'CAU_CONT')
        cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.w_CCCODICE)
        INSERT INTO (i_cTable);
              (CCCODICE,CCDESCRI,CCFLRIFE,CCFLPART,CCFLCOMP,CCFLRITE,CCGESRIT,CCFLINSO,CCFLSALI,CCFLSALF,CCFLASSE,CCFLANAL,CCPAGINS,CCTIPREG,CCNUMREG,CCFLBESE,CCCONIVA,CCTIPDOC,CCCFDAVE,CCCALDOC,CCTESDOC,CCFLIVDF,CCFLPDIF,CCSCIPAG,CCFLAUTR,CCREGMAR,CCFLPDOC,CCFLSTDA,CCFLCOSE,CCSERDOC,CCNUMDOC,CCFLPPRO,CCSERPRO,CCFLMOVC,CCCAUMOV,CCCAUSBF,CCMOVCES,CCCAUCES,CCDTINVA,CCDTOBSO,UTCC,UTCV,UTDC,UTDV,CCDESSUP,CCDESRIG,CCGESCAS  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CCCODICE;
                  ,this.w_CCDESCRI;
                  ,this.w_CCFLRIFE;
                  ,this.w_CCFLPART;
                  ,this.w_CCFLCOMP;
                  ,this.w_CCFLRITE;
                  ,this.w_CCGESRIT;
                  ,this.w_CCFLINSO;
                  ,this.w_CCFLSALI;
                  ,this.w_CCFLSALF;
                  ,this.w_CCFLASSE;
                  ,this.w_CCFLANAL;
                  ,this.w_CCPAGINS;
                  ,this.w_CCTIPREG;
                  ,this.w_CCNUMREG;
                  ,this.w_CCFLBESE;
                  ,this.w_CCCONIVA;
                  ,this.w_CCTIPDOC;
                  ,this.w_CCCFDAVE;
                  ,this.w_CCCALDOC;
                  ,this.w_CCTESDOC;
                  ,this.w_CCFLIVDF;
                  ,this.w_CCFLPDIF;
                  ,this.w_CCSCIPAG;
                  ,this.w_CCFLAUTR;
                  ,this.w_CCREGMAR;
                  ,this.w_CCFLPDOC;
                  ,this.w_CCFLSTDA;
                  ,this.w_CCFLCOSE;
                  ,this.w_CCSERDOC;
                  ,this.w_CCNUMDOC;
                  ,this.w_CCFLPPRO;
                  ,this.w_CCSERPRO;
                  ,this.w_CCFLMOVC;
                  ,this.w_CCCAUMOV;
                  ,this.w_CCCAUSBF;
                  ,this.w_CCMOVCES;
                  ,this.w_CCCAUCES;
                  ,this.w_CCDTINVA;
                  ,this.w_CCDTOBSO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_CCDESSUP;
                  ,this.w_CCDESRIG;
                  ,this.w_CCGESCAS;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAU_CONT_IDX,i_nConn)
      *
      * update CAU_CONT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAU_CONT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CCDESCRI="+cp_ToStrODBC(this.w_CCDESCRI)+;
             ",CCFLRIFE="+cp_ToStrODBC(this.w_CCFLRIFE)+;
             ",CCFLPART="+cp_ToStrODBC(this.w_CCFLPART)+;
             ",CCFLCOMP="+cp_ToStrODBC(this.w_CCFLCOMP)+;
             ",CCFLRITE="+cp_ToStrODBC(this.w_CCFLRITE)+;
             ",CCGESRIT="+cp_ToStrODBC(this.w_CCGESRIT)+;
             ",CCFLINSO="+cp_ToStrODBC(this.w_CCFLINSO)+;
             ",CCFLSALI="+cp_ToStrODBC(this.w_CCFLSALI)+;
             ",CCFLSALF="+cp_ToStrODBC(this.w_CCFLSALF)+;
             ",CCFLASSE="+cp_ToStrODBC(this.w_CCFLASSE)+;
             ",CCFLANAL="+cp_ToStrODBC(this.w_CCFLANAL)+;
             ",CCPAGINS="+cp_ToStrODBCNull(this.w_CCPAGINS)+;
             ",CCTIPREG="+cp_ToStrODBC(this.w_CCTIPREG)+;
             ",CCNUMREG="+cp_ToStrODBC(this.w_CCNUMREG)+;
             ",CCFLBESE="+cp_ToStrODBC(this.w_CCFLBESE)+;
             ",CCCONIVA="+cp_ToStrODBCNull(this.w_CCCONIVA)+;
             ",CCTIPDOC="+cp_ToStrODBC(this.w_CCTIPDOC)+;
             ",CCCFDAVE="+cp_ToStrODBC(this.w_CCCFDAVE)+;
             ",CCCALDOC="+cp_ToStrODBC(this.w_CCCALDOC)+;
             ",CCTESDOC="+cp_ToStrODBC(this.w_CCTESDOC)+;
             ",CCFLIVDF="+cp_ToStrODBC(this.w_CCFLIVDF)+;
             ",CCFLPDIF="+cp_ToStrODBC(this.w_CCFLPDIF)+;
             ",CCSCIPAG="+cp_ToStrODBC(this.w_CCSCIPAG)+;
             ",CCFLAUTR="+cp_ToStrODBC(this.w_CCFLAUTR)+;
             ",CCREGMAR="+cp_ToStrODBC(this.w_CCREGMAR)+;
             ",CCFLPDOC="+cp_ToStrODBC(this.w_CCFLPDOC)+;
             ",CCFLSTDA="+cp_ToStrODBC(this.w_CCFLSTDA)+;
             ",CCFLCOSE="+cp_ToStrODBC(this.w_CCFLCOSE)+;
             ",CCSERDOC="+cp_ToStrODBC(this.w_CCSERDOC)+;
             ",CCNUMDOC="+cp_ToStrODBC(this.w_CCNUMDOC)+;
             ",CCFLPPRO="+cp_ToStrODBC(this.w_CCFLPPRO)+;
             ",CCSERPRO="+cp_ToStrODBC(this.w_CCSERPRO)+;
             ",CCFLMOVC="+cp_ToStrODBC(this.w_CCFLMOVC)+;
             ",CCCAUMOV="+cp_ToStrODBCNull(this.w_CCCAUMOV)+;
             ",CCCAUSBF="+cp_ToStrODBCNull(this.w_CCCAUSBF)+;
             ",CCMOVCES="+cp_ToStrODBC(this.w_CCMOVCES)+;
             ",CCCAUCES="+cp_ToStrODBCNull(this.w_CCCAUCES)+;
             ",CCDTINVA="+cp_ToStrODBC(this.w_CCDTINVA)+;
             ",CCDTOBSO="+cp_ToStrODBC(this.w_CCDTOBSO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CCDESSUP="+cp_ToStrODBC(this.w_CCDESSUP)+;
             ",CCDESRIG="+cp_ToStrODBC(this.w_CCDESRIG)+;
             ",CCGESCAS="+cp_ToStrODBC(this.w_CCGESCAS)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAU_CONT')
        i_cWhere = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
        UPDATE (i_cTable) SET;
              CCDESCRI=this.w_CCDESCRI;
             ,CCFLRIFE=this.w_CCFLRIFE;
             ,CCFLPART=this.w_CCFLPART;
             ,CCFLCOMP=this.w_CCFLCOMP;
             ,CCFLRITE=this.w_CCFLRITE;
             ,CCGESRIT=this.w_CCGESRIT;
             ,CCFLINSO=this.w_CCFLINSO;
             ,CCFLSALI=this.w_CCFLSALI;
             ,CCFLSALF=this.w_CCFLSALF;
             ,CCFLASSE=this.w_CCFLASSE;
             ,CCFLANAL=this.w_CCFLANAL;
             ,CCPAGINS=this.w_CCPAGINS;
             ,CCTIPREG=this.w_CCTIPREG;
             ,CCNUMREG=this.w_CCNUMREG;
             ,CCFLBESE=this.w_CCFLBESE;
             ,CCCONIVA=this.w_CCCONIVA;
             ,CCTIPDOC=this.w_CCTIPDOC;
             ,CCCFDAVE=this.w_CCCFDAVE;
             ,CCCALDOC=this.w_CCCALDOC;
             ,CCTESDOC=this.w_CCTESDOC;
             ,CCFLIVDF=this.w_CCFLIVDF;
             ,CCFLPDIF=this.w_CCFLPDIF;
             ,CCSCIPAG=this.w_CCSCIPAG;
             ,CCFLAUTR=this.w_CCFLAUTR;
             ,CCREGMAR=this.w_CCREGMAR;
             ,CCFLPDOC=this.w_CCFLPDOC;
             ,CCFLSTDA=this.w_CCFLSTDA;
             ,CCFLCOSE=this.w_CCFLCOSE;
             ,CCSERDOC=this.w_CCSERDOC;
             ,CCNUMDOC=this.w_CCNUMDOC;
             ,CCFLPPRO=this.w_CCFLPPRO;
             ,CCSERPRO=this.w_CCSERPRO;
             ,CCFLMOVC=this.w_CCFLMOVC;
             ,CCCAUMOV=this.w_CCCAUMOV;
             ,CCCAUSBF=this.w_CCCAUSBF;
             ,CCMOVCES=this.w_CCMOVCES;
             ,CCCAUCES=this.w_CCCAUCES;
             ,CCDTINVA=this.w_CCDTINVA;
             ,CCDTOBSO=this.w_CCDTOBSO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CCDESSUP=this.w_CCDESSUP;
             ,CCDESRIG=this.w_CCDESRIG;
             ,CCGESCAS=this.w_CCGESCAS;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCG_MCI : Saving
      this.GSCG_MCI.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CCCODICE,"AICODCAU";
             )
      this.GSCG_MCI.mReplace()
      * --- GSCG_MCP : Saving
      this.GSCG_MCP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CCCODICE,"APCODCAU";
             )
      this.GSCG_MCP.mReplace()
      * --- GSCG_MTC : Saving
      this.GSCG_MTC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CCCODICE,"LGCODCAU";
             )
      this.GSCG_MTC.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCG_MCI : Deleting
    this.GSCG_MCI.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CCCODICE,"AICODCAU";
           )
    this.GSCG_MCI.mDelete()
    * --- GSCG_MCP : Deleting
    this.GSCG_MCP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CCCODICE,"APCODCAU";
           )
    this.GSCG_MCP.mDelete()
    * --- GSCG_MTC : Deleting
    this.GSCG_MTC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CCCODICE,"LGCODCAU";
           )
    this.GSCG_MTC.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAU_CONT_IDX,i_nConn)
      *
      * delete CAU_CONT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    if i_bUpd
      with this
            .w_TIPCON = 'G'
        .DoRTCalc(2,11,.t.)
        if .o_CCFLPART<>.w_CCFLPART.or. .o_CCTIPREG<>.w_CCTIPREG.or. .o_CCFLSALI<>.w_CCFLSALI.or. .o_CCFLSALF<>.w_CCFLSALF.or. .o_CCFLPPRO<>.w_CCFLPPRO.or. .o_CCFLPDOC<>.w_CCFLPDOC
            .w_CCFLASSE = IIF(.w_CCFLPART='N' AND .w_CCTIPREG='N' AND .w_CCFLSALI='N' AND .w_CCFLSALF='N' AND .w_CCFLPDOC$'LN' And .w_CCFLPPRO$'LN',.w_CCFLASSE,' ')
        endif
        .DoRTCalc(13,17,.t.)
        if .o_CCTIPREG<>.w_CCTIPREG
            .w_CCCONIVA = IIF (.w_CCTIPREG $ 'AVC',.w_CCCONIVA,SPACE(15))
          .link_1_18('Full')
        endif
        .DoRTCalc(19,22,.t.)
        if .o_CCTIPREG<>.w_CCTIPREG.or. .o_CCNUMDOC<>.w_CCNUMDOC.or. .o_CCFLPDOC<>.w_CCFLPDOC.or. .o_CCFLPPRO<>.w_CCFLPPRO
            .w_CCTESDOC = IIF(.w_CCFLPDOC='N' AND .w_CCFLPPRO='N',' ',.w_CCTESDOC)
        endif
        .DoRTCalc(24,27,.t.)
        if .o_CCTIPREG<>.w_CCTIPREG
            .w_CCREGMAR = iif(.w_CCTIPREG='N',' ',.w_CCREGMAR)
        endif
        if .o_CCTIPREG<>.w_CCTIPREG
            .w_CCFLPDOC = IIF(.w_CCTIPREG$'CE','L','N')
        endif
        if .o_CCTIPREG<>.w_CCTIPREG
            .w_CCFLSTDA = iif(.w_CCTIPREG$'C-E-N',' ',.w_CCFLSTDA)
        endif
        if .o_CCFLRIFE<>.w_CCFLRIFE.or. .o_CCTIPREG<>.w_CCTIPREG
            .w_CCFLCOSE = IIF(.w_CCFLRIFE="C" and .w_CCTIPREG $ "CE", .w_CCFLCOSE, " ")
        endif
        .DoRTCalc(32,32,.t.)
        if .o_CCFLPPRO<>.w_CCFLPPRO.or. .o_CCFLPDOC<>.w_CCFLPDOC
            .w_CCNUMDOC = iif(.w_CCFLPDOC='N' AND .w_CCFLPPRO='N',' ',.w_CCNUMDOC)
        endif
        .DoRTCalc(34,36,.t.)
        if .o_CCFLMOVC<>.w_CCFLMOVC.or. .o_CCTIPDOC<>.w_CCTIPDOC
            .w_CCCAUMOV = SPACE(5)
          .link_1_37('Full')
        endif
        .DoRTCalc(38,39,.t.)
        if .o_CCMOVCES<>.w_CCMOVCES
            .w_CCCAUCES = IIF(.w_CCMOVCES='S',.w_CCCAUCES,SPACE(5))
          .link_1_40('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .DoRTCalc(41,49,.t.)
            .w_CODI = .w_CCCODICE
            .w_DESC = .w_CCDESCRI
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .DoRTCalc(52,59,.t.)
            .w_CODI1 = .w_CCCODICE
            .w_DESC1 = .w_CCDESCRI
        .DoRTCalc(62,64,.t.)
        if .o_CCFLPART<>.w_CCFLPART
            .w_CCGESCAS = iif(.w_CCFLPART<>'N','N','S')
        endif
        if .o_CCTIPDOC<>.w_CCTIPDOC
            .w_OLDTIPDOC = .w_CCTIPDOC
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
    endwith
  return

  proc Calculate_FHXIIEVAVI()
    with this
          * --- Check cancellazione
          GSCG_BCC(this;
              ,'E';
             )
    endwith
  endproc
  proc Calculate_ZQGUGCPQKP()
    with this
          * --- Controllo campo regime del margine
          GSCG_BCC (this;
              ,'M';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCCFLASSE_1_12.enabled = this.oPgFrm.Page1.oPag.oCCFLASSE_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCCFLANAL_1_13.enabled = this.oPgFrm.Page1.oPag.oCCFLANAL_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCCNUMREG_1_16.enabled = this.oPgFrm.Page1.oPag.oCCNUMREG_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCCFLBESE_1_17.enabled = this.oPgFrm.Page1.oPag.oCCFLBESE_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCCCONIVA_1_18.enabled = this.oPgFrm.Page1.oPag.oCCCONIVA_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCCTIPDOC_1_20.enabled = this.oPgFrm.Page1.oPag.oCCTIPDOC_1_20.mCond()
    this.oPgFrm.Page1.oPag.oCCCFDAVE_1_21.enabled = this.oPgFrm.Page1.oPag.oCCCFDAVE_1_21.mCond()
    this.oPgFrm.Page1.oPag.oCCCALDOC_1_22.enabled = this.oPgFrm.Page1.oPag.oCCCALDOC_1_22.mCond()
    this.oPgFrm.Page1.oPag.oCCTESDOC_1_23.enabled = this.oPgFrm.Page1.oPag.oCCTESDOC_1_23.mCond()
    this.oPgFrm.Page1.oPag.oCCFLIVDF_1_24.enabled = this.oPgFrm.Page1.oPag.oCCFLIVDF_1_24.mCond()
    this.oPgFrm.Page1.oPag.oCCFLPDIF_1_25.enabled = this.oPgFrm.Page1.oPag.oCCFLPDIF_1_25.mCond()
    this.oPgFrm.Page1.oPag.oCCFLAUTR_1_27.enabled = this.oPgFrm.Page1.oPag.oCCFLAUTR_1_27.mCond()
    this.oPgFrm.Page1.oPag.oCCREGMAR_1_28.enabled = this.oPgFrm.Page1.oPag.oCCREGMAR_1_28.mCond()
    this.oPgFrm.Page1.oPag.oCCFLSTDA_1_30.enabled = this.oPgFrm.Page1.oPag.oCCFLSTDA_1_30.mCond()
    this.oPgFrm.Page1.oPag.oCCFLCOSE_1_31.enabled = this.oPgFrm.Page1.oPag.oCCFLCOSE_1_31.mCond()
    this.oPgFrm.Page1.oPag.oCCCAUCES_1_40.enabled = this.oPgFrm.Page1.oPag.oCCCAUCES_1_40.mCond()
    this.oPgFrm.Page1.oPag.oCCGESCAS_1_92.enabled = this.oPgFrm.Page1.oPag.oCCGESCAS_1_92.mCond()
    this.GSCG_MCI.enabled = this.oPgFrm.Page2.oPag.oLinkPC_2_3.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCCPAGINS_1_14.visible=!this.oPgFrm.Page1.oPag.oCCPAGINS_1_14.mHide()
    this.oPgFrm.Page1.oPag.oCCFLBESE_1_17.visible=!this.oPgFrm.Page1.oPag.oCCFLBESE_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCCFLMOVC_1_36.visible=!this.oPgFrm.Page1.oPag.oCCFLMOVC_1_36.mHide()
    this.oPgFrm.Page1.oPag.oCCCAUMOV_1_37.visible=!this.oPgFrm.Page1.oPag.oCCCAUMOV_1_37.mHide()
    this.oPgFrm.Page1.oPag.oCCCAUSBF_1_38.visible=!this.oPgFrm.Page1.oPag.oCCCAUSBF_1_38.mHide()
    this.oPgFrm.Page1.oPag.oCCMOVCES_1_39.visible=!this.oPgFrm.Page1.oPag.oCCMOVCES_1_39.mHide()
    this.oPgFrm.Page1.oPag.oCCCAUCES_1_40.visible=!this.oPgFrm.Page1.oPag.oCCCAUCES_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_71.visible=!this.oPgFrm.Page1.oPag.oStr_1_71.mHide()
    this.oPgFrm.Page1.oPag.oCAUDESRI_1_72.visible=!this.oPgFrm.Page1.oPag.oCAUDESRI_1_72.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_74.visible=!this.oPgFrm.Page1.oPag.oStr_1_74.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_76.visible=!this.oPgFrm.Page1.oPag.oStr_1_76.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oDESCCESP_1_82.visible=!this.oPgFrm.Page1.oPag.oDESCCESP_1_82.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_83.visible=!this.oPgFrm.Page1.oPag.oStr_1_83.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_85.visible=!this.oPgFrm.Page1.oPag.oStr_1_85.mHide()
    this.oPgFrm.Page1.oPag.oDESCSBF_1_86.visible=!this.oPgFrm.Page1.oPag.oDESCSBF_1_86.mHide()
    this.oPgFrm.Page1.oPag.oDESPAG_1_88.visible=!this.oPgFrm.Page1.oPag.oDESPAG_1_88.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_89.visible=!this.oPgFrm.Page1.oPag.oStr_1_89.mHide()
    this.oPgFrm.Page1.oPag.oCCGESCAS_1_92.visible=!this.oPgFrm.Page1.oPag.oCCGESCAS_1_92.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_78.Event(cEvent)
        if lower(cEvent)==lower("Delete start")
          .Calculate_FHXIIEVAVI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Insert start") or lower(cEvent)==lower("Update start")
          .Calculate_ZQGUGCPQKP()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CCPAGINS
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCPAGINS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_CCPAGINS)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_CCPAGINS))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCPAGINS)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCPAGINS) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oCCPAGINS_1_14'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCPAGINS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_CCPAGINS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_CCPAGINS)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCPAGINS = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CCPAGINS = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_CCPAGINS = space(5)
        this.w_DESPAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCPAGINS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.PACODICE as PACODICE114"+ ","+cp_TransLinkFldName('link_1_14.PADESCRI')+" as PADESCRI114"+ ",link_1_14.PADTOBSO as PADTOBSO114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on CAU_CONT.CCPAGINS=link_1_14.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and CAU_CONT.CCPAGINS=link_1_14.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CCCONIVA
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCONIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CCCONIVA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CCCONIVA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCONIVA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CCCONIVA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CCCONIVA)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CCCONIVA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCCCONIVA_1_18'),i_cWhere,'GSAR_BZC',"Conti IVA",'GSCG_ACC.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, di tipo non IVA, gestito a partite con IVA autotrasportatori oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCONIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CCCONIVA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CCCONIVA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCONIVA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESSOT = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSOT = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_CIFLPART = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CCCONIVA = space(15)
      endif
      this.w_DESSOT = space(40)
      this.w_TIPSOT = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CIFLPART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSOT='I' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and (EMPTY(.w_CCFLAUTR) OR EMPTY(.w_CIFLPART) OR (.w_CCFLAUTR='S' AND .w_CCFLIVDF='S'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, di tipo non IVA, gestito a partite con IVA autotrasportatori oppure obsoleto")
        endif
        this.w_CCCONIVA = space(15)
        this.w_DESSOT = space(40)
        this.w_TIPSOT = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CIFLPART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCONIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCCAUMOV
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_lTable = "CCC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2], .t., this.CCC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCAUMOV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBA_MCT',True,'CCC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CCCAUMOV)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CATIPCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CCCAUMOV))
          select CACODICE,CADESCRI,CATIPCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCAUMOV)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCAUMOV) and !this.bDontReportError
            deferred_cp_zoom('CCC_MAST','*','CACODICE',cp_AbsName(oSource.parent,'oCCCAUMOV_1_37'),i_cWhere,'GSBA_MCT',"Causali movimenti C\C",'GSCG_MCT.CCC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CATIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CATIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCAUMOV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CATIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CCCAUMOV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CCCAUMOV)
            select CACODICE,CADESCRI,CATIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCAUMOV = NVL(_Link_.CACODICE,space(5))
      this.w_CAUDESRI = NVL(_Link_.CADESCRI,space(35))
      this.w_TIPCAU = NVL(_Link_.CATIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CCCAUMOV = space(5)
      endif
      this.w_CAUDESRI = space(35)
      this.w_TIPCAU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CCTIPDOC= 'NO' AND .w_TIPCAU<>'S') OR (.w_CCTIPDOC<>'NO' AND .w_TIPCAU='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale movimento incongruente con il tipo documento")
        endif
        this.w_CCCAUMOV = space(5)
        this.w_CAUDESRI = space(35)
        this.w_TIPCAU = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CCC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCAUMOV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_37(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CCC_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_37.CACODICE as CACODICE137"+ ",link_1_37.CADESCRI as CADESCRI137"+ ",link_1_37.CATIPCON as CATIPCON137"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_37 on CAU_CONT.CCCAUMOV=link_1_37.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_37"
          i_cKey=i_cKey+'+" and CAU_CONT.CCCAUMOV=link_1_37.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CCCAUSBF
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_lTable = "CCC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2], .t., this.CCC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCAUSBF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBA_MCT',True,'CCC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CCCAUSBF)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CATIPCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CCCAUSBF))
          select CACODICE,CADESCRI,CATIPCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCAUSBF)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCAUSBF) and !this.bDontReportError
            deferred_cp_zoom('CCC_MAST','*','CACODICE',cp_AbsName(oSource.parent,'oCCCAUSBF_1_38'),i_cWhere,'GSBA_MCT',"Causali movimenti C\C",'GSCG_MCT.CCC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CATIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CATIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCAUSBF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CATIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CCCAUSBF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CCCAUSBF)
            select CACODICE,CADESCRI,CATIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCAUSBF = NVL(_Link_.CACODICE,space(5))
      this.w_DESCSBF = NVL(_Link_.CADESCRI,space(35))
      this.w_TIPSBF = NVL(_Link_.CATIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CCCAUSBF = space(5)
      endif
      this.w_DESCSBF = space(35)
      this.w_TIPSBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CCTIPDOC= 'NO' AND .w_TIPCAU<>'S') OR (.w_CCTIPDOC<>'NO' AND .w_TIPCAU='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale movimento incongruente con il tipo documento")
        endif
        this.w_CCCAUSBF = space(5)
        this.w_DESCSBF = space(35)
        this.w_TIPSBF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CCC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCAUSBF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CCC_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.CACODICE as CACODICE138"+ ",link_1_38.CADESCRI as CADESCRI138"+ ",link_1_38.CATIPCON as CATIPCON138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on CAU_CONT.CCCAUSBF=link_1_38.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and CAU_CONT.CCCAUSBF=link_1_38.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CCCAUCES
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_lTable = "CAU_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2], .t., this.CAU_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCAUCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACC',True,'CAU_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CCCAUCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CCCAUCES))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCAUCES)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CCCAUCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CCCAUCES)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CCCAUCES) and !this.bDontReportError
            deferred_cp_zoom('CAU_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCCCAUCES_1_40'),i_cWhere,'GSCE_ACC',"Causali cespiti",'GSCE_ZCA.CAU_CESP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCAUCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CCCAUCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CCCAUCES)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCAUCES = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCCESP = NVL(_Link_.CCDESCRI,space(40))
      this.w_DTOBSOCA = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CCCAUCES = space(5)
      endif
      this.w_DESCCESP = space(40)
      this.w_DTOBSOCA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSOCA>.w_OBTEST OR EMPTY(.w_DTOBSOCA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale cespite inesistente o obsoleta")
        endif
        this.w_CCCAUCES = space(5)
        this.w_DESCCESP = space(40)
        this.w_DTOBSOCA = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCAUCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_40(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CESP_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_40.CCCODICE as CCCODICE140"+ ",link_1_40.CCDESCRI as CCDESCRI140"+ ",link_1_40.CCDTOBSO as CCDTOBSO140"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_40 on CAU_CONT.CCCAUCES=link_1_40.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_40"
          i_cKey=i_cKey+'+" and CAU_CONT.CCCAUCES=link_1_40.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCCCODICE_1_2.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oCCCODICE_1_2.value=this.w_CCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_3.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_3.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLRIFE_1_4.RadioValue()==this.w_CCFLRIFE)
      this.oPgFrm.Page1.oPag.oCCFLRIFE_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLPART_1_5.RadioValue()==this.w_CCFLPART)
      this.oPgFrm.Page1.oPag.oCCFLPART_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLCOMP_1_6.RadioValue()==this.w_CCFLCOMP)
      this.oPgFrm.Page1.oPag.oCCFLCOMP_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLRITE_1_7.RadioValue()==this.w_CCFLRITE)
      this.oPgFrm.Page1.oPag.oCCFLRITE_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCGESRIT_1_8.RadioValue()==this.w_CCGESRIT)
      this.oPgFrm.Page1.oPag.oCCGESRIT_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLINSO_1_9.RadioValue()==this.w_CCFLINSO)
      this.oPgFrm.Page1.oPag.oCCFLINSO_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLSALI_1_10.RadioValue()==this.w_CCFLSALI)
      this.oPgFrm.Page1.oPag.oCCFLSALI_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLSALF_1_11.RadioValue()==this.w_CCFLSALF)
      this.oPgFrm.Page1.oPag.oCCFLSALF_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLASSE_1_12.RadioValue()==this.w_CCFLASSE)
      this.oPgFrm.Page1.oPag.oCCFLASSE_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLANAL_1_13.RadioValue()==this.w_CCFLANAL)
      this.oPgFrm.Page1.oPag.oCCFLANAL_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCPAGINS_1_14.value==this.w_CCPAGINS)
      this.oPgFrm.Page1.oPag.oCCPAGINS_1_14.value=this.w_CCPAGINS
    endif
    if not(this.oPgFrm.Page1.oPag.oCCTIPREG_1_15.RadioValue()==this.w_CCTIPREG)
      this.oPgFrm.Page1.oPag.oCCTIPREG_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCNUMREG_1_16.value==this.w_CCNUMREG)
      this.oPgFrm.Page1.oPag.oCCNUMREG_1_16.value=this.w_CCNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLBESE_1_17.RadioValue()==this.w_CCFLBESE)
      this.oPgFrm.Page1.oPag.oCCFLBESE_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCONIVA_1_18.value==this.w_CCCONIVA)
      this.oPgFrm.Page1.oPag.oCCCONIVA_1_18.value=this.w_CCCONIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSOT_1_19.value==this.w_DESSOT)
      this.oPgFrm.Page1.oPag.oDESSOT_1_19.value=this.w_DESSOT
    endif
    if not(this.oPgFrm.Page1.oPag.oCCTIPDOC_1_20.RadioValue()==this.w_CCTIPDOC)
      this.oPgFrm.Page1.oPag.oCCTIPDOC_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCFDAVE_1_21.RadioValue()==this.w_CCCFDAVE)
      this.oPgFrm.Page1.oPag.oCCCFDAVE_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCALDOC_1_22.RadioValue()==this.w_CCCALDOC)
      this.oPgFrm.Page1.oPag.oCCCALDOC_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCTESDOC_1_23.RadioValue()==this.w_CCTESDOC)
      this.oPgFrm.Page1.oPag.oCCTESDOC_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLIVDF_1_24.RadioValue()==this.w_CCFLIVDF)
      this.oPgFrm.Page1.oPag.oCCFLIVDF_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLPDIF_1_25.RadioValue()==this.w_CCFLPDIF)
      this.oPgFrm.Page1.oPag.oCCFLPDIF_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCSCIPAG_1_26.RadioValue()==this.w_CCSCIPAG)
      this.oPgFrm.Page1.oPag.oCCSCIPAG_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLAUTR_1_27.RadioValue()==this.w_CCFLAUTR)
      this.oPgFrm.Page1.oPag.oCCFLAUTR_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCREGMAR_1_28.RadioValue()==this.w_CCREGMAR)
      this.oPgFrm.Page1.oPag.oCCREGMAR_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLPDOC_1_29.RadioValue()==this.w_CCFLPDOC)
      this.oPgFrm.Page1.oPag.oCCFLPDOC_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLSTDA_1_30.RadioValue()==this.w_CCFLSTDA)
      this.oPgFrm.Page1.oPag.oCCFLSTDA_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLCOSE_1_31.RadioValue()==this.w_CCFLCOSE)
      this.oPgFrm.Page1.oPag.oCCFLCOSE_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCSERDOC_1_32.value==this.w_CCSERDOC)
      this.oPgFrm.Page1.oPag.oCCSERDOC_1_32.value=this.w_CCSERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCCNUMDOC_1_33.RadioValue()==this.w_CCNUMDOC)
      this.oPgFrm.Page1.oPag.oCCNUMDOC_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLPPRO_1_34.RadioValue()==this.w_CCFLPPRO)
      this.oPgFrm.Page1.oPag.oCCFLPPRO_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCSERPRO_1_35.value==this.w_CCSERPRO)
      this.oPgFrm.Page1.oPag.oCCSERPRO_1_35.value=this.w_CCSERPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLMOVC_1_36.RadioValue()==this.w_CCFLMOVC)
      this.oPgFrm.Page1.oPag.oCCFLMOVC_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCAUMOV_1_37.value==this.w_CCCAUMOV)
      this.oPgFrm.Page1.oPag.oCCCAUMOV_1_37.value=this.w_CCCAUMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCAUSBF_1_38.value==this.w_CCCAUSBF)
      this.oPgFrm.Page1.oPag.oCCCAUSBF_1_38.value=this.w_CCCAUSBF
    endif
    if not(this.oPgFrm.Page1.oPag.oCCMOVCES_1_39.RadioValue()==this.w_CCMOVCES)
      this.oPgFrm.Page1.oPag.oCCMOVCES_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCAUCES_1_40.value==this.w_CCCAUCES)
      this.oPgFrm.Page1.oPag.oCCCAUCES_1_40.value=this.w_CCCAUCES
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDTINVA_1_42.value==this.w_CCDTINVA)
      this.oPgFrm.Page1.oPag.oCCDTINVA_1_42.value=this.w_CCDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDTOBSO_1_44.value==this.w_CCDTOBSO)
      this.oPgFrm.Page1.oPag.oCCDTOBSO_1_44.value=this.w_CCDTOBSO
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_1.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESC_2_2.value==this.w_DESC)
      this.oPgFrm.Page2.oPag.oDESC_2_2.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUDESRI_1_72.value==this.w_CAUDESRI)
      this.oPgFrm.Page1.oPag.oCAUDESRI_1_72.value=this.w_CAUDESRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCESP_1_82.value==this.w_DESCCESP)
      this.oPgFrm.Page1.oPag.oDESCCESP_1_82.value=this.w_DESCCESP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCSBF_1_86.value==this.w_DESCSBF)
      this.oPgFrm.Page1.oPag.oDESCSBF_1_86.value=this.w_DESCSBF
    endif
    if not(this.oPgFrm.Page3.oPag.oCODI1_3_1.value==this.w_CODI1)
      this.oPgFrm.Page3.oPag.oCODI1_3_1.value=this.w_CODI1
    endif
    if not(this.oPgFrm.Page3.oPag.oDESC1_3_2.value==this.w_DESC1)
      this.oPgFrm.Page3.oPag.oDESC1_3_2.value=this.w_DESC1
    endif
    if not(this.oPgFrm.Page2.oPag.oCCDESSUP_2_10.value==this.w_CCDESSUP)
      this.oPgFrm.Page2.oPag.oCCDESSUP_2_10.value=this.w_CCDESSUP
    endif
    if not(this.oPgFrm.Page2.oPag.oCCDESRIG_2_11.value==this.w_CCDESRIG)
      this.oPgFrm.Page2.oPag.oCCDESRIG_2_11.value=this.w_CCDESRIG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_88.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_88.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCCGESCAS_1_92.RadioValue()==this.w_CCGESCAS)
      this.oPgFrm.Page1.oPag.oCCGESCAS_1_92.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'CAU_CONT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CCCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CCCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(NOT (.w_CCFLSALI='S' AND .w_CCFLSALF='S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCFLSALI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(NOT (.w_CCFLSALI='S' AND .w_CCFLSALF='S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCFLSALF_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(.w_CCFLINSO<>'S')  and not(empty(.w_CCPAGINS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCPAGINS_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   ((empty(.w_CCNUMREG)) or not(.w_CCTIPREG='N' OR CHKREGIV(.w_CCTIPREG,.w_CCNUMREG)=1))  and (.w_CCTIPREG<>'N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCNUMREG_1_16.SetFocus()
            i_bnoObbl = !empty(.w_CCNUMREG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il registro � associato a pi� attivit� o a nessuna attivit�")
          case   ((empty(.w_CCCONIVA)) or not(.w_TIPSOT='I' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and (EMPTY(.w_CCFLAUTR) OR EMPTY(.w_CIFLPART) OR (.w_CCFLAUTR='S' AND .w_CCFLIVDF='S'))))  and (.w_CCTIPREG $ 'AVC')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCONIVA_1_18.SetFocus()
            i_bnoObbl = !empty(.w_CCCONIVA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, di tipo non IVA, gestito a partite con IVA autotrasportatori oppure obsoleto")
          case   ((empty(.w_CCFLPDOC)) or not(.w_CCFLPDOC='D' OR NOT .w_CCTIPREG = 'V' OR .w_CCFLPDIF='S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCFLPDOC_1_29.SetFocus()
            i_bnoObbl = !empty(.w_CCFLPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo numerazione incongruente con tipo registro IVA (se reg.vendite = solo per anno)")
          case   ((empty(.w_CCFLPPRO)) or not(.w_CCFLPPRO='D' OR .w_CCTIPREG<>'A' OR .w_CCFLPDIF='S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCFLPPRO_1_34.SetFocus()
            i_bnoObbl = !empty(.w_CCFLPPRO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_CCTIPDOC= 'NO' AND .w_TIPCAU<>'S') OR (.w_CCTIPDOC<>'NO' AND .w_TIPCAU='S'))  and not(.w_CCFLMOVC<>'S' OR g_BANC<>'S')  and not(empty(.w_CCCAUMOV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCAUMOV_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale movimento incongruente con il tipo documento")
          case   not((.w_CCTIPDOC= 'NO' AND .w_TIPCAU<>'S') OR (.w_CCTIPDOC<>'NO' AND .w_TIPCAU='S'))  and not(.w_CCFLMOVC<>'S' OR g_BANC<>'S')  and not(empty(.w_CCCAUSBF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCAUSBF_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale movimento incongruente con il tipo documento")
          case   ((empty(.w_CCCAUCES)) or not(.w_DTOBSOCA>.w_OBTEST OR EMPTY(.w_DTOBSOCA)))  and not(.w_CCMOVCES<>'S')  and (.w_CCMOVCES='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCAUCES_1_40.SetFocus()
            i_bnoObbl = !empty(.w_CCCAUCES)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale cespite inesistente o obsoleta")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCG_MCI.CheckForm()
      if i_bres
        i_bres=  .GSCG_MCI.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_MCP.CheckForm()
      if i_bres
        i_bres=  .GSCG_MCP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_MTC.CheckForm()
      if i_bres
        i_bres=  .GSCG_MTC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gscg_acc
      this.NotifyEvent('Controllo')
      if THIS.w_UNIREG='N'
         i_bRes=.f.
         i_bnoChk = .f.	
         i_cErrorMsg = ah_MsgFormat("Numero registro non associato a nessuna attivit�")
      endif
      if THIS.w_UNIREG='M'
         i_bRes=.f.
         i_bnoChk = .f.	
         i_cErrorMsg = ah_MsgFormat("Esistono piu' attivita' associate allo stesso registro")
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CCFLRIFE = this.w_CCFLRIFE
    this.o_CCFLPART = this.w_CCFLPART
    this.o_CCFLSALI = this.w_CCFLSALI
    this.o_CCFLSALF = this.w_CCFLSALF
    this.o_CCTIPREG = this.w_CCTIPREG
    this.o_CCTIPDOC = this.w_CCTIPDOC
    this.o_CCFLPDOC = this.w_CCFLPDOC
    this.o_CCNUMDOC = this.w_CCNUMDOC
    this.o_CCFLPPRO = this.w_CCFLPPRO
    this.o_CCFLMOVC = this.w_CCFLMOVC
    this.o_CCMOVCES = this.w_CCMOVCES
    * --- GSCG_MCI : Depends On
    this.GSCG_MCI.SaveDependsOn()
    * --- GSCG_MCP : Depends On
    this.GSCG_MCP.SaveDependsOn()
    * --- GSCG_MTC : Depends On
    this.GSCG_MTC.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_accPag1 as StdContainer
  Width  = 792
  height = 506
  stdWidth  = 792
  stdheight = 506
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCCCODICE_1_2 as StdField with uid="OJLYJUFSGX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CCCODICE", cQueryName = "CCCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della causale contabile",;
    HelpContextID = 118051435,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=102, Top=11, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5)

  add object oCCDESCRI_1_3 as StdField with uid="CUVXLAPCYQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della causale contabile",;
    HelpContextID = 32465519,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=163, Top=11, InputMask=replicate('X',35)


  add object oCCFLRIFE_1_4 as StdCombo with uid="TBOOVSPJVW",rtseq=4,rtrep=.f.,left=102,top=41,width=96,height=21;
    , ToolTipText = "La causale pu� essere riferita a cliente o fornitore o nessuno";
    , HelpContextID = 132547179;
    , cFormVar="w_CCFLRIFE",RowSource=""+"Clienti,"+"Fornitori,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCCFLRIFE_1_4.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oCCFLRIFE_1_4.GetRadio()
    this.Parent.oContained.w_CCFLRIFE = this.RadioValue()
    return .t.
  endfunc

  func oCCFLRIFE_1_4.SetRadio()
    this.Parent.oContained.w_CCFLRIFE=trim(this.Parent.oContained.w_CCFLRIFE)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLRIFE=='C',1,;
      iif(this.Parent.oContained.w_CCFLRIFE=='F',2,;
      iif(this.Parent.oContained.w_CCFLRIFE=='N',3,;
      0)))
  endfunc


  add object oCCFLPART_1_5 as StdCombo with uid="HQADZZNMAI",rtseq=5,rtrep=.f.,left=102,top=71,width=96,height=21;
    , HelpContextID = 3767686;
    , cFormVar="w_CCFLPART",RowSource=""+"Acconto,"+"Crea,"+"Salda,"+"Non gestite", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCCFLPART_1_5.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    iif(this.value =3,'S',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oCCFLPART_1_5.GetRadio()
    this.Parent.oContained.w_CCFLPART = this.RadioValue()
    return .t.
  endfunc

  func oCCFLPART_1_5.SetRadio()
    this.Parent.oContained.w_CCFLPART=trim(this.Parent.oContained.w_CCFLPART)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLPART=='A',1,;
      iif(this.Parent.oContained.w_CCFLPART=='C',2,;
      iif(this.Parent.oContained.w_CCFLPART=='S',3,;
      iif(this.Parent.oContained.w_CCFLPART=='N',4,;
      0))))
  endfunc

  add object oCCFLCOMP_1_6 as StdCheck with uid="YNQGBKNBNK",rtseq=6,rtrep=.f.,left=393, top=37, caption="Es. di competenza",;
    ToolTipText = "Se attivo: accetta movimento di altro esercizio",;
    HelpContextID = 50953610,;
    cFormVar="w_CCFLCOMP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLCOMP_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCFLCOMP_1_6.GetRadio()
    this.Parent.oContained.w_CCFLCOMP = this.RadioValue()
    return .t.
  endfunc

  func oCCFLCOMP_1_6.SetRadio()
    this.Parent.oContained.w_CCFLCOMP=trim(this.Parent.oContained.w_CCFLCOMP)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLCOMP=='S',1,;
      0)
  endfunc

  add object oCCFLRITE_1_7 as StdCheck with uid="VYYRIXBSUH",rtseq=7,rtrep=.f.,left=393, top=58, caption="Rileva ritenute",;
    ToolTipText = "Se attivo: causale di rilevazione ritenute",;
    HelpContextID = 132547179,;
    cFormVar="w_CCFLRITE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLRITE_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLRITE_1_7.GetRadio()
    this.Parent.oContained.w_CCFLRITE = this.RadioValue()
    return .t.
  endfunc

  func oCCFLRITE_1_7.SetRadio()
    this.Parent.oContained.w_CCFLRITE=trim(this.Parent.oContained.w_CCFLRITE)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLRITE=='S',1,;
      0)
  endfunc

  add object oCCGESRIT_1_8 as StdCheck with uid="FEQRDXIAVB",rtseq=8,rtrep=.f.,left=393, top=79, caption="Gestisce le ritenute",;
    ToolTipText = "Attivare per le causali che verranno utilizzate per registrare fatture\documenti soggetti a ritenuta",;
    HelpContextID = 252734854,;
    cFormVar="w_CCGESRIT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCGESRIT_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCGESRIT_1_8.GetRadio()
    this.Parent.oContained.w_CCGESRIT = this.RadioValue()
    return .t.
  endfunc

  func oCCGESRIT_1_8.SetRadio()
    this.Parent.oContained.w_CCGESRIT=trim(this.Parent.oContained.w_CCGESRIT)
    this.value = ;
      iif(this.Parent.oContained.w_CCGESRIT=='S',1,;
      0)
  endfunc

  add object oCCFLINSO_1_9 as StdCheck with uid="ZORYCTIRPE",rtseq=9,rtrep=.f.,left=393, top=100, caption="Insoluti",;
    ToolTipText = "Se attivo: causale utilizzata per la registrazione manuale degli insoluti",;
    HelpContextID = 206996085,;
    cFormVar="w_CCFLINSO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLINSO_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLINSO_1_9.GetRadio()
    this.Parent.oContained.w_CCFLINSO = this.RadioValue()
    return .t.
  endfunc

  func oCCFLINSO_1_9.SetRadio()
    this.Parent.oContained.w_CCFLINSO=trim(this.Parent.oContained.w_CCFLINSO)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLINSO=='S',1,;
      0)
  endfunc

  add object oCCFLSALI_1_10 as StdCheck with uid="SERSOZUUHS",rtseq=10,rtrep=.f.,left=546, top=37, caption="Apertura conti",;
    ToolTipText = "Se attivo: aggiorna saldo iniziale durante le operazioni di apertura conti",;
    HelpContextID = 621969,;
    cFormVar="w_CCFLSALI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLSALI_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCFLSALI_1_10.GetRadio()
    this.Parent.oContained.w_CCFLSALI = this.RadioValue()
    return .t.
  endfunc

  func oCCFLSALI_1_10.SetRadio()
    this.Parent.oContained.w_CCFLSALI=trim(this.Parent.oContained.w_CCFLSALI)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLSALI=='S',1,;
      0)
  endfunc

  func oCCFLSALI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT (.w_CCFLSALI='S' AND .w_CCFLSALF='S'))
    endwith
    return bRes
  endfunc

  add object oCCFLSALF_1_11 as StdCheck with uid="CLWFYEJRBM",rtseq=11,rtrep=.f.,left=546, top=58, caption="Chiusura conti",;
    ToolTipText = "Se attivo: aggiorna saldo finale durante le operazioni di chiusura conti",;
    HelpContextID = 621972,;
    cFormVar="w_CCFLSALF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLSALF_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCFLSALF_1_11.GetRadio()
    this.Parent.oContained.w_CCFLSALF = this.RadioValue()
    return .t.
  endfunc

  func oCCFLSALF_1_11.SetRadio()
    this.Parent.oContained.w_CCFLSALF=trim(this.Parent.oContained.w_CCFLSALF)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLSALF=='S',1,;
      0)
  endfunc

  func oCCFLSALF_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT (.w_CCFLSALI='S' AND .w_CCFLSALF='S'))
    endwith
    return bRes
  endfunc

  add object oCCFLASSE_1_12 as StdCheck with uid="RAYOTDBUMD",rtseq=12,rtrep=.f.,left=546, top=79, caption="Assestamento",;
    ToolTipText = "Se attivo: causale per generazione scritture di assestamento (no IVA - no partite - no apertura/chiusura-no numerazioni)",;
    HelpContextID = 14058091,;
    cFormVar="w_CCFLASSE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLASSE_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLASSE_1_12.GetRadio()
    this.Parent.oContained.w_CCFLASSE = this.RadioValue()
    return .t.
  endfunc

  func oCCFLASSE_1_12.SetRadio()
    this.Parent.oContained.w_CCFLASSE=trim(this.Parent.oContained.w_CCFLASSE)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLASSE=='S',1,;
      0)
  endfunc

  func oCCFLASSE_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCFLPART='N' AND .w_CCTIPREG='N' AND .w_CCFLSALI='N' AND .w_CCFLSALF='N' AND .w_CCFLPDOC$'LN' And .w_CCFLPPRO$'LN')
    endwith
   endif
  endfunc

  add object oCCFLANAL_1_13 as StdCheck with uid="WGNVZKNDAH",rtseq=13,rtrep=.f.,left=546, top=100, caption="Movim.analitica",;
    ToolTipText = "Se attivo: abilita la gestione della contabilit� analitica in primanota",;
    HelpContextID = 69827982,;
    cFormVar="w_CCFLANAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLANAL_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLANAL_1_13.GetRadio()
    this.Parent.oContained.w_CCFLANAL = this.RadioValue()
    return .t.
  endfunc

  func oCCFLANAL_1_13.SetRadio()
    this.Parent.oContained.w_CCFLANAL=trim(this.Parent.oContained.w_CCFLANAL)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLANAL=='S',1,;
      0)
  endfunc

  func oCCFLANAL_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  add object oCCPAGINS_1_14 as StdField with uid="PPAAAEOJNH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CCPAGINS", cQueryName = "CCPAGINS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Pagamento per registrazione da insoluto",;
    HelpContextID = 148102535,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=393, Top=126, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_CCPAGINS"

  func oCCPAGINS_1_14.mHide()
    with this.Parent.oContained
      return (.w_CCFLINSO<>'S')
    endwith
  endfunc

  func oCCPAGINS_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCPAGINS_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCPAGINS_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oCCPAGINS_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oCCPAGINS_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_CCPAGINS
     i_obj.ecpSave()
  endproc


  add object oCCTIPREG_1_15 as StdCombo with uid="KKSQXEHRFL",rtseq=15,rtrep=.f.,left=109,top=165,width=134,height=21;
    , ToolTipText = "Tipo registro IVA associato alla causale contabile";
    , HelpContextID = 12870253;
    , cFormVar="w_CCTIPREG",RowSource=""+"Vendite,"+"Acquisti,"+"Corr.scorporo,"+"Corr.ventilazione,"+"No IVA", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCCTIPREG_1_15.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,'E',;
    iif(this.value =5,'N',;
    space(1)))))))
  endfunc
  func oCCTIPREG_1_15.GetRadio()
    this.Parent.oContained.w_CCTIPREG = this.RadioValue()
    return .t.
  endfunc

  func oCCTIPREG_1_15.SetRadio()
    this.Parent.oContained.w_CCTIPREG=trim(this.Parent.oContained.w_CCTIPREG)
    this.value = ;
      iif(this.Parent.oContained.w_CCTIPREG=='V',1,;
      iif(this.Parent.oContained.w_CCTIPREG=='A',2,;
      iif(this.Parent.oContained.w_CCTIPREG=='C',3,;
      iif(this.Parent.oContained.w_CCTIPREG=='E',4,;
      iif(this.Parent.oContained.w_CCTIPREG=='N',5,;
      0)))))
  endfunc

  add object oCCNUMREG_1_16 as StdField with uid="MZWMFONQCH",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CCNUMREG", cQueryName = "CCNUMREG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il registro � associato a pi� attivit� o a nessuna attivit�",;
    ToolTipText = "Numero registro IVA associato alla causale contabile",;
    HelpContextID = 10486381,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=303, Top=165, cSayPict='"99"', cGetPict='"99"'

  func oCCNUMREG_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPREG<>'N')
    endwith
   endif
  endfunc

  func oCCNUMREG_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CCTIPREG='N' OR CHKREGIV(.w_CCTIPREG,.w_CCNUMREG)=1)
    endwith
    return bRes
  endfunc


  add object oCCFLBESE_1_17 as StdCombo with uid="WABDNDEOUO",rtseq=17,rtrep=.f.,left=447,top=165,width=85,height=21;
    , ToolTipText = "Tipo corrispettivi (beni o servizi)";
    , HelpContextID = 48661099;
    , cFormVar="w_CCFLBESE",RowSource=""+"Beni,"+"Servizi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCCFLBESE_1_17.RadioValue()
    return(iif(this.value =1,'B',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oCCFLBESE_1_17.GetRadio()
    this.Parent.oContained.w_CCFLBESE = this.RadioValue()
    return .t.
  endfunc

  func oCCFLBESE_1_17.SetRadio()
    this.Parent.oContained.w_CCFLBESE=trim(this.Parent.oContained.w_CCFLBESE)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLBESE=='B',1,;
      iif(this.Parent.oContained.w_CCFLBESE=='S',2,;
      0))
  endfunc

  func oCCFLBESE_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPREG='C')
    endwith
   endif
  endfunc

  func oCCFLBESE_1_17.mHide()
    with this.Parent.oContained
      return (.w_CCTIPREG<>'C')
    endwith
  endfunc

  add object oCCCONIVA_1_18 as StdField with uid="UIQCSAQIDY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CCCONIVA", cQueryName = "CCCONIVA",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, di tipo non IVA, gestito a partite con IVA autotrasportatori oppure obsoleto",;
    ToolTipText = "Codice conto IVA detraibile usato per la contabilizzazione documenti",;
    HelpContextID = 128537191,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=109, Top=197, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CCCONIVA"

  func oCCCONIVA_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPREG $ 'AVC')
    endwith
   endif
  endfunc

  func oCCCONIVA_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCONIVA_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCONIVA_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCCCONIVA_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti IVA",'GSCG_ACC.CONTI_VZM',this.parent.oContained
  endproc
  proc oCCCONIVA_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CCCONIVA
     i_obj.ecpSave()
  endproc

  add object oDESSOT_1_19 as StdField with uid="SEOUBMZYIA",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESSOT", cQueryName = "DESSOT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 222407626,;
   bGlobalFont=.t.,;
    Height=21, Width=275, Left=244, Top=197, InputMask=replicate('X',40)


  add object oCCTIPDOC_1_20 as StdCombo with uid="JOIHKFMXML",rtseq=20,rtrep=.f.,left=109,top=226,width=136,height=21;
    , ToolTipText = "Tipo documento associato alla causale contabile";
    , HelpContextID = 222010775;
    , cFormVar="w_CCTIPDOC",RowSource=""+"Fattura,"+"Nota di credito,"+"Fatt.corrispettivi,"+"Corrispettivi,"+"Fattura UE,"+"Nota credito UE,"+"No documento,"+"Fattura RC,"+"Nota credito RC", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCCTIPDOC_1_20.RadioValue()
    return(iif(this.value =1,'FA',;
    iif(this.value =2,'NC',;
    iif(this.value =3,'FC',;
    iif(this.value =4,'CO',;
    iif(this.value =5,'FE',;
    iif(this.value =6,'NE',;
    iif(this.value =7,'NO',;
    iif(this.value =8,'AU',;
    iif(this.value =9,'NU',;
    space(2)))))))))))
  endfunc
  func oCCTIPDOC_1_20.GetRadio()
    this.Parent.oContained.w_CCTIPDOC = this.RadioValue()
    return .t.
  endfunc

  func oCCTIPDOC_1_20.SetRadio()
    this.Parent.oContained.w_CCTIPDOC=trim(this.Parent.oContained.w_CCTIPDOC)
    this.value = ;
      iif(this.Parent.oContained.w_CCTIPDOC=='FA',1,;
      iif(this.Parent.oContained.w_CCTIPDOC=='NC',2,;
      iif(this.Parent.oContained.w_CCTIPDOC=='FC',3,;
      iif(this.Parent.oContained.w_CCTIPDOC=='CO',4,;
      iif(this.Parent.oContained.w_CCTIPDOC=='FE',5,;
      iif(this.Parent.oContained.w_CCTIPDOC=='NE',6,;
      iif(this.Parent.oContained.w_CCTIPDOC=='NO',7,;
      iif(this.Parent.oContained.w_CCTIPDOC=='AU',8,;
      iif(this.Parent.oContained.w_CCTIPDOC=='NU',9,;
      0)))))))))
  endfunc

  func oCCTIPDOC_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPREG<>'N')
    endwith
   endif
  endfunc


  add object oCCCFDAVE_1_21 as StdCombo with uid="DQMPAQVMJA",rtseq=21,rtrep=.f.,left=109,top=256,width=86,height=21;
    , ToolTipText = "Sezione documento cliente/fornitore (dare o avere)";
    , HelpContextID = 251679339;
    , cFormVar="w_CCCFDAVE",RowSource=""+"Dare,"+"Avere", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCCCFDAVE_1_21.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oCCCFDAVE_1_21.GetRadio()
    this.Parent.oContained.w_CCCFDAVE = this.RadioValue()
    return .t.
  endfunc

  func oCCCFDAVE_1_21.SetRadio()
    this.Parent.oContained.w_CCCFDAVE=trim(this.Parent.oContained.w_CCCFDAVE)
    this.value = ;
      iif(this.Parent.oContained.w_CCCFDAVE=='D',1,;
      iif(this.Parent.oContained.w_CCCFDAVE=='A',2,;
      0))
  endfunc

  func oCCCFDAVE_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPREG<>'N')
    endwith
   endif
  endfunc


  add object oCCCALDOC_1_22 as StdCombo with uid="ZIXGGZYTHO",rtseq=22,rtrep=.f.,left=405,top=256,width=122,height=21;
    , ToolTipText = "Importo da proporre sulla riga del cliente/fornitore";
    , HelpContextID = 226798999;
    , cFormVar="w_CCCALDOC",RowSource=""+"Imponibile+IVA,"+"Imponibile,"+"IVA,"+"Importo zero,"+"No cli/for", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCCCALDOC_1_22.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'M',;
    iif(this.value =3,'I',;
    iif(this.value =4,'Z',;
    iif(this.value =5,'N',;
    space(1)))))))
  endfunc
  func oCCCALDOC_1_22.GetRadio()
    this.Parent.oContained.w_CCCALDOC = this.RadioValue()
    return .t.
  endfunc

  func oCCCALDOC_1_22.SetRadio()
    this.Parent.oContained.w_CCCALDOC=trim(this.Parent.oContained.w_CCCALDOC)
    this.value = ;
      iif(this.Parent.oContained.w_CCCALDOC=='E',1,;
      iif(this.Parent.oContained.w_CCCALDOC=='M',2,;
      iif(this.Parent.oContained.w_CCCALDOC=='I',3,;
      iif(this.Parent.oContained.w_CCCALDOC=='Z',4,;
      iif(this.Parent.oContained.w_CCCALDOC=='N',5,;
      0)))))
  endfunc

  func oCCCALDOC_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPREG<>'N' AND NOT .w_CCTIPDOC $ 'CO')
    endwith
   endif
  endfunc

  add object oCCTESDOC_1_23 as StdCheck with uid="YUYMKHFTPI",rtseq=23,rtrep=.f.,left=539, top=165, caption="Controllo univocit�",;
    ToolTipText = "Se attivo: controlla l'univocit� del documento registrato in P.N.",;
    HelpContextID = 219127191,;
    cFormVar="w_CCTESDOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCTESDOC_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCTESDOC_1_23.GetRadio()
    this.Parent.oContained.w_CCTESDOC = this.RadioValue()
    return .t.
  endfunc

  func oCCTESDOC_1_23.SetRadio()
    this.Parent.oContained.w_CCTESDOC=trim(this.Parent.oContained.w_CCTESDOC)
    this.value = ;
      iif(this.Parent.oContained.w_CCTESDOC=='S',1,;
      0)
  endfunc

  func oCCTESDOC_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPREG='N' AND (.w_CCFLPDOC<>'N' OR .w_CCFLPPRO<>'N'))
    endwith
   endif
  endfunc

  add object oCCFLIVDF_1_24 as StdCheck with uid="YKBVDIESEE",rtseq=24,rtrep=.f.,left=539, top=184, caption="Esigib.differita",;
    ToolTipText = "Se attivo: il documento � con IVA ad esigibilit� differita",;
    HelpContextID = 72778348,;
    cFormVar="w_CCFLIVDF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLIVDF_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLIVDF_1_24.GetRadio()
    this.Parent.oContained.w_CCFLIVDF = this.RadioValue()
    return .t.
  endfunc

  func oCCFLIVDF_1_24.SetRadio()
    this.Parent.oContained.w_CCFLIVDF=trim(this.Parent.oContained.w_CCFLIVDF)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLIVDF=='S',1,;
      0)
  endfunc

  func oCCFLIVDF_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPREG $ 'VA' )
    endwith
   endif
  endfunc

  add object oCCFLPDIF_1_25 as StdCheck with uid="FCIUIVHSUD",rtseq=25,rtrep=.f.,left=539, top=203, caption="Pag.esigib.differita",;
    ToolTipText = "Se attivo: l'incasso o il pagamento rileva l'IVA ad esigibilit� differita",;
    HelpContextID = 46563948,;
    cFormVar="w_CCFLPDIF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLPDIF_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLPDIF_1_25.GetRadio()
    this.Parent.oContained.w_CCFLPDIF = this.RadioValue()
    return .t.
  endfunc

  func oCCFLPDIF_1_25.SetRadio()
    this.Parent.oContained.w_CCFLPDIF=trim(this.Parent.oContained.w_CCFLPDIF)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLPDIF=='S',1,;
      0)
  endfunc

  func oCCFLPDIF_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPREG $ 'VA' AND (EMPTY(.w_CCFLAUTR) OR .w_CCFLAUTR<>'S'))
    endwith
   endif
  endfunc

  add object oCCSCIPAG_1_26 as StdCheck with uid="WYZEDHVMFJ",rtseq=26,rtrep=.f.,left=539, top=222, caption="Scissione pagamenti",;
    ToolTipText = "Causale utilizzata per c.d. scissione pagamenti",;
    HelpContextID = 28421523,;
    cFormVar="w_CCSCIPAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCSCIPAG_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCSCIPAG_1_26.GetRadio()
    this.Parent.oContained.w_CCSCIPAG = this.RadioValue()
    return .t.
  endfunc

  func oCCSCIPAG_1_26.SetRadio()
    this.Parent.oContained.w_CCSCIPAG=trim(this.Parent.oContained.w_CCSCIPAG)
    this.value = ;
      iif(this.Parent.oContained.w_CCSCIPAG=='S',1,;
      0)
  endfunc

  add object oCCFLAUTR_1_27 as StdCheck with uid="ELNNHFMIAZ",rtseq=27,rtrep=.f.,left=539, top=241, caption="IVA autotrasportatori",;
    ToolTipText = "Se attivo: il documento gestisce l'IVA autotrasportatori",;
    HelpContextID = 47612536,;
    cFormVar="w_CCFLAUTR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLAUTR_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLAUTR_1_27.GetRadio()
    this.Parent.oContained.w_CCFLAUTR = this.RadioValue()
    return .t.
  endfunc

  func oCCFLAUTR_1_27.SetRadio()
    this.Parent.oContained.w_CCFLAUTR=trim(this.Parent.oContained.w_CCFLAUTR)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLAUTR=='S',1,;
      0)
  endfunc

  func oCCFLAUTR_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPREG='V' AND (EMPTY(.w_CCFLPDIF) OR .w_CCFLPDIF<>'S'))
    endwith
   endif
  endfunc

  add object oCCREGMAR_1_28 as StdCheck with uid="OTTTZTKIQS",rtseq=28,rtrep=.f.,left=539, top=260, caption="Regime del margine",;
    ToolTipText = "Se attivo: viene gestito il regime del margine",;
    HelpContextID = 80723336,;
    cFormVar="w_CCREGMAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCREGMAR_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCCREGMAR_1_28.GetRadio()
    this.Parent.oContained.w_CCREGMAR = this.RadioValue()
    return .t.
  endfunc

  func oCCREGMAR_1_28.SetRadio()
    this.Parent.oContained.w_CCREGMAR=trim(this.Parent.oContained.w_CCREGMAR)
    this.value = ;
      iif(this.Parent.oContained.w_CCREGMAR=='S',1,;
      0)
  endfunc

  func oCCREGMAR_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPREG<>'N')
    endwith
   endif
  endfunc


  add object oCCFLPDOC_1_29 as StdCombo with uid="VACRGIQFGJ",rtseq=29,rtrep=.f.,left=102,top=326,width=98,height=21;
    , ToolTipText = "Tipo di numerazione documento";
    , HelpContextID = 221871511;
    , cFormVar="w_CCFLPDOC",RowSource=""+"Per anno,"+"Per esercizio,"+"Libera,"+"Non gestita", bObbl = .t. , nPag = 1;
    , sErrorMsg = "Tipo numerazione incongruente con tipo registro IVA (se reg.vendite = solo per anno)";
  , bGlobalFont=.t.


  func oCCFLPDOC_1_29.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'E',;
    iif(this.value =3,'L',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oCCFLPDOC_1_29.GetRadio()
    this.Parent.oContained.w_CCFLPDOC = this.RadioValue()
    return .t.
  endfunc

  func oCCFLPDOC_1_29.SetRadio()
    this.Parent.oContained.w_CCFLPDOC=trim(this.Parent.oContained.w_CCFLPDOC)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLPDOC=='D',1,;
      iif(this.Parent.oContained.w_CCFLPDOC=='E',2,;
      iif(this.Parent.oContained.w_CCFLPDOC=='L',3,;
      iif(this.Parent.oContained.w_CCFLPDOC=='N',4,;
      0))))
  endfunc

  func oCCFLPDOC_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CCFLPDOC='D' OR NOT .w_CCTIPREG = 'V' OR .w_CCFLPDIF='S')
    endwith
    return bRes
  endfunc

  add object oCCFLSTDA_1_30 as StdCheck with uid="KLFJWPKVDU",rtseq=30,rtrep=.f.,left=539, top=279, caption="Note in Reg.IVA",;
    ToolTipText = "Se attivo: stampa le note di primanota sui registri IVA (se acquisti o vendite) ",;
    HelpContextID = 49709671,;
    cFormVar="w_CCFLSTDA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLSTDA_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLSTDA_1_30.GetRadio()
    this.Parent.oContained.w_CCFLSTDA = this.RadioValue()
    return .t.
  endfunc

  func oCCFLSTDA_1_30.SetRadio()
    this.Parent.oContained.w_CCFLSTDA=trim(this.Parent.oContained.w_CCFLSTDA)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLSTDA=='S',1,;
      0)
  endfunc

  func oCCFLSTDA_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPREG<>'N')
    endwith
   endif
  endfunc

  add object oCCFLCOSE_1_31 as StdCheck with uid="NFOKWCTIKB",rtseq=31,rtrep=.f.,left=539, top=298, caption="Contabilizz. separata",;
    ToolTipText = "Permette la contabilizzazione separata dei corrispettivi che superano l'importo limite per l'anno",;
    HelpContextID = 217481835,;
    cFormVar="w_CCFLCOSE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLCOSE_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLCOSE_1_31.GetRadio()
    this.Parent.oContained.w_CCFLCOSE = this.RadioValue()
    return .t.
  endfunc

  func oCCFLCOSE_1_31.SetRadio()
    this.Parent.oContained.w_CCFLCOSE=trim(this.Parent.oContained.w_CCFLCOSE)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLCOSE=='S',1,;
      0)
  endfunc

  func oCCFLCOSE_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCFLRIFE="C" and .w_CCTIPREG $ "CE")
    endwith
   endif
  endfunc

  add object oCCSERDOC_1_32 as StdField with uid="OTUAKEFDTB",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CCSERDOC", cQueryName = "CCSERDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale serie del documento proposta in automatico",;
    HelpContextID = 220179863,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=259, Top=326, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oCCNUMDOC_1_33 as StdCheck with uid="OBZLISHAOW",rtseq=33,rtrep=.f.,left=102, top=350, caption="Docum.obbligatorio",;
    ToolTipText = "Se attivo: abilita l'obbligo del documento in primanota",;
    HelpContextID = 224394647,;
    cFormVar="w_CCNUMDOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCNUMDOC_1_33.RadioValue()
    return(iif(this.value =1,'O',;
    ' '))
  endfunc
  func oCCNUMDOC_1_33.GetRadio()
    this.Parent.oContained.w_CCNUMDOC = this.RadioValue()
    return .t.
  endfunc

  func oCCNUMDOC_1_33.SetRadio()
    this.Parent.oContained.w_CCNUMDOC=trim(this.Parent.oContained.w_CCNUMDOC)
    this.value = ;
      iif(this.Parent.oContained.w_CCNUMDOC=='O',1,;
      0)
  endfunc


  add object oCCFLPPRO_1_34 as StdCombo with uid="SETPYLBCTP",rtseq=34,rtrep=.f.,left=422,top=326,width=98,height=21;
    , ToolTipText = "Tipo di numerazione protocollo";
    , HelpContextID = 247890549;
    , cFormVar="w_CCFLPPRO",RowSource=""+"Per anno,"+"Per esercizio,"+"Libera,"+"Non gestita", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCCFLPPRO_1_34.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'E',;
    iif(this.value =3,'L',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oCCFLPPRO_1_34.GetRadio()
    this.Parent.oContained.w_CCFLPPRO = this.RadioValue()
    return .t.
  endfunc

  func oCCFLPPRO_1_34.SetRadio()
    this.Parent.oContained.w_CCFLPPRO=trim(this.Parent.oContained.w_CCFLPPRO)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLPPRO=='D',1,;
      iif(this.Parent.oContained.w_CCFLPPRO=='E',2,;
      iif(this.Parent.oContained.w_CCFLPPRO=='L',3,;
      iif(this.Parent.oContained.w_CCFLPPRO=='N',4,;
      0))))
  endfunc

  func oCCFLPPRO_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CCFLPPRO='D' OR .w_CCTIPREG<>'A' OR .w_CCFLPDIF='S')
    endwith
    return bRes
  endfunc

  add object oCCSERPRO_1_35 as StdField with uid="WHWZXNUZZH",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CCSERPRO", cQueryName = "CCSERPRO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale serie del protocollo proposta in automatico",;
    HelpContextID = 249582197,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=579, Top=326, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oCCFLMOVC_1_36 as StdCheck with uid="DXFIBGXPJS",rtseq=36,rtrep=.f.,left=6, top=396, caption="Movimenti di C\C",;
    ToolTipText = "Se attivo: causale con movimenti C\C",;
    HelpContextID = 227967593,;
    cFormVar="w_CCFLMOVC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLMOVC_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLMOVC_1_36.GetRadio()
    this.Parent.oContained.w_CCFLMOVC = this.RadioValue()
    return .t.
  endfunc

  func oCCFLMOVC_1_36.SetRadio()
    this.Parent.oContained.w_CCFLMOVC=trim(this.Parent.oContained.w_CCFLMOVC)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLMOVC=='S',1,;
      0)
  endfunc

  func oCCFLMOVC_1_36.mHide()
    with this.Parent.oContained
      return (g_BANC<>'S')
    endwith
  endfunc

  add object oCCCAUMOV_1_37 as StdField with uid="ULXLPKJUKJ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_CCCAUMOV", cQueryName = "CCCAUMOV",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale movimento incongruente con il tipo documento",;
    ToolTipText = "Causale movimenti di conto corrente utilizzata per le contabilizzazioni",;
    HelpContextID = 66366852,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=357, Top=396, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCC_MAST", cZoomOnZoom="GSBA_MCT", oKey_1_1="CACODICE", oKey_1_2="this.w_CCCAUMOV"

  func oCCCAUMOV_1_37.mHide()
    with this.Parent.oContained
      return (.w_CCFLMOVC<>'S' OR g_BANC<>'S')
    endwith
  endfunc

  func oCCCAUMOV_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCAUMOV_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCAUMOV_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCC_MAST','*','CACODICE',cp_AbsName(this.parent,'oCCCAUMOV_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBA_MCT',"Causali movimenti C\C",'GSCG_MCT.CCC_MAST_VZM',this.parent.oContained
  endproc
  proc oCCCAUMOV_1_37.mZoomOnZoom
    local i_obj
    i_obj=GSBA_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CCCAUMOV
     i_obj.ecpSave()
  endproc

  add object oCCCAUSBF_1_38 as StdField with uid="MHEYVMUXNA",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CCCAUSBF", cQueryName = "CCCAUSBF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale movimento incongruente con il tipo documento",;
    ToolTipText = "Causale movimenti C\C per contabilizzazione conti SBF",;
    HelpContextID = 34296428,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=357, Top=422, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCC_MAST", cZoomOnZoom="GSBA_MCT", oKey_1_1="CACODICE", oKey_1_2="this.w_CCCAUSBF"

  func oCCCAUSBF_1_38.mHide()
    with this.Parent.oContained
      return (.w_CCFLMOVC<>'S' OR g_BANC<>'S')
    endwith
  endfunc

  func oCCCAUSBF_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCAUSBF_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCAUSBF_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCC_MAST','*','CACODICE',cp_AbsName(this.parent,'oCCCAUSBF_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBA_MCT',"Causali movimenti C\C",'GSCG_MCT.CCC_MAST_VZM',this.parent.oContained
  endproc
  proc oCCCAUSBF_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSBA_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CCCAUSBF
     i_obj.ecpSave()
  endproc

  add object oCCMOVCES_1_39 as StdCheck with uid="AGYYKOTMLA",rtseq=39,rtrep=.f.,left=6, top=453, caption="Movimenti cespiti",;
    ToolTipText = "Se attivo: causale con movimenti cespiti associati",;
    HelpContextID = 36303481,;
    cFormVar="w_CCMOVCES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCMOVCES_1_39.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCMOVCES_1_39.GetRadio()
    this.Parent.oContained.w_CCMOVCES = this.RadioValue()
    return .t.
  endfunc

  func oCCMOVCES_1_39.SetRadio()
    this.Parent.oContained.w_CCMOVCES=trim(this.Parent.oContained.w_CCMOVCES)
    this.value = ;
      iif(this.Parent.oContained.w_CCMOVCES=='S',1,;
      0)
  endfunc

  func oCCMOVCES_1_39.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  add object oCCCAUCES_1_40 as StdField with uid="DWSLTZICJL",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CCCAUCES", cQueryName = "CCCAUCES",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale cespite inesistente o obsoleta",;
    ToolTipText = "Causale cespite associata",;
    HelpContextID = 34296441,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=357, Top=454, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CESP", cZoomOnZoom="GSCE_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CCCAUCES"

  func oCCCAUCES_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCMOVCES='S')
    endwith
   endif
  endfunc

  func oCCCAUCES_1_40.mHide()
    with this.Parent.oContained
      return (.w_CCMOVCES<>'S')
    endwith
  endfunc

  func oCCCAUCES_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCAUCES_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCAUCES_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCCCAUCES_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACC',"Causali cespiti",'GSCE_ZCA.CAU_CESP_VZM',this.parent.oContained
  endproc
  proc oCCCAUCES_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CCCAUCES
     i_obj.ecpSave()
  endproc

  add object oCCDTINVA_1_42 as StdField with uid="HJVZPUZTPE",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CCDTINVA", cQueryName = "CCDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 207512167,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=417, Top=480, tabstop=.f.

  add object oCCDTOBSO_1_44 as StdField with uid="QPUISKQECL",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CCDTOBSO", cQueryName = "CCDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 12477045,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=628, Top=480, tabstop=.f.


  add object oObj_1_54 as cp_runprogram with uid="BNNYHRXRZC",left=-3, top=558, width=258,height=19,;
    caption='GSCG_BCC(T)',;
   bGlobalFont=.t.,;
    prg='GSCG_BCC("T")',;
    cEvent = "w_CCTIPREG Changed",;
    nPag=1;
    , HelpContextID = 28592425


  add object oObj_1_55 as cp_runprogram with uid="TRWTGFRYLE",left=-3, top=601, width=258,height=19,;
    caption='GSGC_BCC(D)',;
   bGlobalFont=.t.,;
    prg="GSCG_BCC('D')",;
    cEvent = "w_CCTIPDOC Changed",;
    nPag=1;
    , HelpContextID = 28342569

  add object oCAUDESRI_1_72 as StdField with uid="OAADUWXYBA",rtseq=52,rtrep=.f.,;
    cFormVar = "w_CAUDESRI", cQueryName = "CAUDESRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 17789039,;
   bGlobalFont=.t.,;
    Height=21, Width=283, Left=424, Top=396, InputMask=replicate('X',35)

  func oCAUDESRI_1_72.mHide()
    with this.Parent.oContained
      return (.w_CCFLMOVC<>'S' OR g_BANC<>'S')
    endwith
  endfunc


  add object oObj_1_78 as cp_runprogram with uid="UIAIAAGKMB",left=-3, top=580, width=258,height=19,;
    caption='GSCG_BCC(C)',;
   bGlobalFont=.t.,;
    prg="GSCG_BCC('C')",;
    cEvent = "Controllo",;
    nPag=1;
    , HelpContextID = 28588073

  add object oDESCCESP_1_82 as StdField with uid="OWQKFJBKEF",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESCCESP", cQueryName = "DESCCESP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 49173638,;
   bGlobalFont=.t.,;
    Height=21, Width=283, Left=424, Top=454, InputMask=replicate('X',40)

  func oDESCCESP_1_82.mHide()
    with this.Parent.oContained
      return (.w_CCMOVCES<>'S')
    endwith
  endfunc

  add object oDESCSBF_1_86 as StdField with uid="UIBZAVRWWY",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DESCSBF", cQueryName = "DESCSBF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 15619126,;
   bGlobalFont=.t.,;
    Height=21, Width=283, Left=424, Top=422, InputMask=replicate('X',35)

  func oDESCSBF_1_86.mHide()
    with this.Parent.oContained
      return (.w_CCFLMOVC<>'S' OR g_BANC<>'S')
    endwith
  endfunc

  add object oDESPAG_1_88 as StdField with uid="RYGTRERTBU",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 186952650,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=460, Top=126, InputMask=replicate('X',30)

  func oDESPAG_1_88.mHide()
    with this.Parent.oContained
      return (.w_CCFLINSO<>'S')
    endwith
  endfunc

  add object oCCGESCAS_1_92 as StdCheck with uid="WYVSDCFGXJ",rtseq=65,rtrep=.f.,left=102, top=100, caption="Gestione cassa",;
    ToolTipText = "Se attivo, la causale contabile partecipa alla  gestione cassa",;
    HelpContextID = 235957639,;
    cFormVar="w_CCGESCAS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCGESCAS_1_92.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCGESCAS_1_92.GetRadio()
    this.Parent.oContained.w_CCGESCAS = this.RadioValue()
    return .t.
  endfunc

  func oCCGESCAS_1_92.SetRadio()
    this.Parent.oContained.w_CCGESCAS=trim(this.Parent.oContained.w_CCGESCAS)
    this.value = ;
      iif(this.Parent.oContained.w_CCGESCAS=='S',1,;
      0)
  endfunc

  func oCCGESCAS_1_92.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCFLPART='N')
    endwith
   endif
  endfunc

  func oCCGESCAS_1_92.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="KJQUKASKXU",Visible=.t., Left=30, Top=11,;
    Alignment=1, Width=70, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_43 as StdString with uid="YOLGPFXXSI",Visible=.t., Left=42, Top=75,;
    Alignment=1, Width=58, Height=15,;
    Caption="Partite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="EUUQPGIGQY",Visible=.t., Left=2, Top=166,;
    Alignment=1, Width=105, Height=15,;
    Caption="Tipo registro IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="IRJDOPYCKE",Visible=.t., Left=247, Top=165,;
    Alignment=1, Width=53, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="OXOHUFDKFS",Visible=.t., Left=2, Top=226,;
    Alignment=1, Width=105, Height=15,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="NNIIVKOSFQ",Visible=.t., Left=2, Top=256,;
    Alignment=1, Width=105, Height=15,;
    Caption="Sezione cli./for.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="ZXCAAIDLDT",Visible=.t., Left=504, Top=480,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="GRDSOUAFEK",Visible=.t., Left=311, Top=480,;
    Alignment=1, Width=103, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="JDCPRWNNVW",Visible=.t., Left=23, Top=41,;
    Alignment=1, Width=77, Height=15,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="LPKJQBCJSO",Visible=.t., Left=206, Top=326,;
    Alignment=1, Width=51, Height=15,;
    Caption="Serie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="SCGBJEETLC",Visible=.t., Left=525, Top=326,;
    Alignment=1, Width=51, Height=15,;
    Caption="Serie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="SLHMEJXLOR",Visible=.t., Left=2, Top=197,;
    Alignment=1, Width=105, Height=15,;
    Caption="Contropartita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="MVHKSHNFAC",Visible=.t., Left=4, Top=142,;
    Alignment=0, Width=342, Height=15,;
    Caption="Dati IVA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_66 as StdString with uid="DJNVOHCNVH",Visible=.t., Left=240, Top=256,;
    Alignment=1, Width=162, Height=15,;
    Caption="Calcolo importo cli./for.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="JWMECHRERZ",Visible=.t., Left=4, Top=301,;
    Alignment=0, Width=525, Height=15,;
    Caption="Progressivi numerazioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_68 as StdString with uid="UKECWRTEPG",Visible=.t., Left=-5, Top=326,;
    Alignment=1, Width=104, Height=15,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="MKFVPYQJSH",Visible=.t., Left=345, Top=326,;
    Alignment=1, Width=74, Height=15,;
    Caption="Protocollo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="WOBDNTRDEL",Visible=.t., Left=338, Top=165,;
    Alignment=1, Width=105, Height=15,;
    Caption="Tipo corrispettivi:"  ;
  , bGlobalFont=.t.

  func oStr_1_71.mHide()
    with this.Parent.oContained
      return (.w_CCTIPREG<>'C')
    endwith
  endfunc

  add object oStr_1_74 as StdString with uid="BSPMYXCDLP",Visible=.t., Left=4, Top=369,;
    Alignment=0, Width=663, Height=18,;
    Caption="Dati conto corrente"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_74.mHide()
    with this.Parent.oContained
      return (g_BANC<>'S')
    endwith
  endfunc

  add object oStr_1_76 as StdString with uid="DEACRZMANW",Visible=.t., Left=239, Top=398,;
    Alignment=1, Width=118, Height=18,;
    Caption="Causale mov.:"  ;
  , bGlobalFont=.t.

  func oStr_1_76.mHide()
    with this.Parent.oContained
      return (.w_CCFLMOVC<>'S' OR g_BANC<>'S')
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="DYPIJFTQCI",Visible=.t., Left=6, Top=430,;
    Alignment=0, Width=220, Height=18,;
    Caption="Dati cespiti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="LTSOLGFIWX",Visible=.t., Left=221, Top=458,;
    Alignment=1, Width=136, Height=18,;
    Caption="Causale cesp.:"  ;
  , bGlobalFont=.t.

  func oStr_1_83.mHide()
    with this.Parent.oContained
      return (.w_CCMOVCES<>'S')
    endwith
  endfunc

  add object oStr_1_85 as StdString with uid="LKVZDBLJDP",Visible=.t., Left=226, Top=422,;
    Alignment=1, Width=131, Height=18,;
    Caption="Causale mov. SBF:"  ;
  , bGlobalFont=.t.

  func oStr_1_85.mHide()
    with this.Parent.oContained
      return (.w_CCFLMOVC<>'S' OR g_BANC<>'S')
    endwith
  endfunc

  add object oStr_1_89 as StdString with uid="MPKKUQZBVX",Visible=.t., Left=251, Top=126,;
    Alignment=1, Width=140, Height=18,;
    Caption="Pag. insoluti:"  ;
  , bGlobalFont=.t.

  func oStr_1_89.mHide()
    with this.Parent.oContained
      return (.w_CCFLINSO<>'S')
    endwith
  endfunc

  add object oBox_1_53 as StdBox with uid="UGFQPAGVIX",left=1, top=158, width=675,height=1

  add object oBox_1_70 as StdBox with uid="ZMNPFDLZEY",left=1, top=319, width=675,height=1

  add object oBox_1_73 as StdBox with uid="UCAXIOSXBQ",left=1, top=389, width=709,height=1

  add object oBox_1_81 as StdBox with uid="RVWZBGMEPG",left=1, top=450, width=709,height=1
enddefine
define class tgscg_accPag2 as StdContainer
  Width  = 792
  height = 506
  stdWidth  = 792
  stdheight = 506
  resizeXpos=230
  resizeYpos=309
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_2_1 as StdField with uid="FLPABHRHLR",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della causale contabile",;
    HelpContextID = 104632794,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=107, Top=11, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5)

  add object oDESC_2_2 as StdField with uid="NEGGDVMIEO",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della causale contabile",;
    HelpContextID = 104967114,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=174, Top=11, InputMask=replicate('X',35)


  add object oLinkPC_2_3 as stdDynamicChildContainer with uid="TKAWCNXSSK",left=1, top=59, width=788, height=143, bOnScreen=.t.;


  func oLinkPC_2_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CCTIPREG<>'N')
      endwith
    endif
  endfunc


  add object oLinkPC_2_4 as stdDynamicChildContainer with uid="HCQVXPSTFK",left=1, top=232, width=669, height=180, bOnScreen=.t.;


  add object oCCDESSUP_2_10 as StdField with uid="PRMZANXWOV",rtseq=62,rtrep=.f.,;
    cFormVar = "w_CCDESSUP", cQueryName = "CCDESSUP",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione parametrica di testata",;
    HelpContextID = 32465526,;
   bGlobalFont=.t.,;
    Height=21, Width=486, Left=180, Top=417, InputMask=replicate('X',254)

  add object oCCDESRIG_2_11 as StdField with uid="ERVNPKARIA",rtseq=63,rtrep=.f.,;
    cFormVar = "w_CCDESRIG", cQueryName = "CCDESRIG",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione parametrica di riga",;
    HelpContextID = 252747155,;
   bGlobalFont=.t.,;
    Height=21, Width=486, Left=180, Top=443, InputMask=replicate('X',254)

  add object oStr_2_5 as StdString with uid="CJOQRUWEUM",Visible=.t., Left=6, Top=11,;
    Alignment=1, Width=97, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_6 as StdString with uid="YSTNBJGIKU",Visible=.t., Left=2, Top=40,;
    Alignment=0, Width=625, Height=15,;
    Caption="Righe IVA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="VJRTRQWCBN",Visible=.t., Left=2, Top=205,;
    Alignment=0, Width=625, Height=15,;
    Caption="Righe contabili"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_12 as StdString with uid="KQGADFKVVV",Visible=.t., Left=5, Top=418,;
    Alignment=1, Width=173, Height=18,;
    Caption="Formula descrizione di testata:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="GIPHMDFPBI",Visible=.t., Left=0, Top=444,;
    Alignment=1, Width=178, Height=18,;
    Caption="Formula descrizione di riga:"  ;
  , bGlobalFont=.t.

  add object oBox_2_8 as StdBox with uid="OYAPIFZJGS",left=-2, top=55, width=792,height=1

  add object oBox_2_9 as StdBox with uid="SHDGIREWCG",left=-2, top=223, width=668,height=1
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_mci",lower(this.oContained.GSCG_MCI.class))=0
        this.oContained.GSCG_MCI.createrealchild()
      endif
      if type('this.oContained')='O' and at("gscg_mcp",lower(this.oContained.GSCG_MCP.class))=0
        this.oContained.GSCG_MCP.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgscg_accPag3 as StdContainer
  Width  = 792
  height = 506
  stdWidth  = 792
  stdheight = 506
  resizeXpos=293
  resizeYpos=198
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI1_3_1 as StdField with uid="CDHXTJDCLC",rtseq=60,rtrep=.f.,;
    cFormVar = "w_CODI1", cQueryName = "CODI1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della causale contabile",;
    HelpContextID = 53252570,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=70, Top=9, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5)

  add object oDESC1_3_2 as StdField with uid="VCXPADXHXE",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESC1", cQueryName = "DESC1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della causale contabile",;
    HelpContextID = 53586890,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=137, Top=9, InputMask=replicate('X',35)


  add object oLinkPC_3_4 as stdDynamicChildContainer with uid="ETMPPORCIY",left=15, top=67, width=596, height=214, bOnScreen=.t.;


  add object oStr_3_3 as StdString with uid="YDEQOXSJSB",Visible=.t., Left=6, Top=9,;
    Alignment=1, Width=60, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_5 as StdString with uid="ICXCFWFHLX",Visible=.t., Left=15, Top=46,;
    Alignment=0, Width=607, Height=18,;
    Caption="Traduzioni causali contabili"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_mtc",lower(this.oContained.GSCG_MTC.class))=0
        this.oContained.GSCG_MTC.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_acc','CAU_CONT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCCODICE=CAU_CONT.CCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
