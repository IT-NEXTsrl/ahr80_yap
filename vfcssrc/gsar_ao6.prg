* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ao6                                                        *
*              Parametri ritenute                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_94]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2013-04-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_ao6"))

* --- Class definition
define class tgsar_ao6 as StdForm
  Top    = 32
  Left   = 17

  * --- Standard Properties
  Width  = 605
  Height = 440+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-04-16"
  HelpContextID=176781161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=39

  * --- Constant Properties
  CONTROPA_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  VOCIIVA_IDX = 0
  PAG_AMEN_IDX = 0
  AZIENDA_IDX = 0
  KEY_ARTI_IDX = 0
  cFile = "CONTROPA"
  cKeySelect = "COCODAZI"
  cKeyWhere  = "COCODAZI=this.w_COCODAZI"
  cKeyWhereODBC = '"COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cKeyWhereODBCqualified = '"CONTROPA.COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cPrg = "gsar_ao6"
  cComment = "Parametri ritenute"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COCODAZI = space(5)
  w_RAGAZI = space(40)
  w_COTIPCON = space(1)
  w_PERIVB = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_TIPART = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_COCAURIT = space(5)
  w_DESCRITE = space(35)
  w_COCONRIT = space(15)
  w_DESRITE = space(40)
  w_COCORIPR = space(15)
  w_COENASAR = space(15)
  w_COGIRRIT = space(5)
  w_COCOIRVE = space(15)
  w_COCOPRVE = space(15)
  w_COCAURIL = space(5)
  w_COCONTRS = space(15)
  w_COCAUSLD = space(5)
  w_COCAUGIR = space(5)
  w_COEVICRE = space(15)
  w_DESENAS = space(40)
  w_RITE = space(1)
  w_DESCIRPE = space(40)
  w_DECRPREV = space(40)
  w_DESCGIRO = space(35)
  w_DESRIP = space(40)
  w_PART = space(1)
  w_FLPART = space(1)
  w_DESTRS = space(40)
  w_DESEVI = space(40)
  w_DESSAL = space(35)
  w_DESGIR = space(35)
  w_DESRIT = space(35)
  w_RITE1 = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsar_ao6
  * --- Disabilita il Caricamento e la Cancellazione sulla Toolbar
  proc SetCPToolBar()
          doDefault()
          oCpToolBar.b3.enabled=.f.
          oCpToolBar.b5.enabled=.f.
  endproc
  * ---- Disattiva i metodi Load e Delete (posso solo variare)
  proc ecpLoad()
      * ----
  endproc
  proc ecpDelete()
      * ----
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CONTROPA','gsar_ao6')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ao6Pag1","gsar_ao6",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Ritenute")
      .Pages(1).HelpContextID = 238864517
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsar_ao6
    * --- nMaxFieldsJoin di default vale 200
    this.parent.nMaxFieldsJoin = iif(lower(i_ServerConn[1,6])="oracle", 150, this.parent.nMaxFieldsJoin)
    * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='PAG_AMEN'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='CONTROPA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTROPA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTROPA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_COCODAZI = NVL(COCODAZI,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_24_joined
    link_1_24_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    local link_1_26_joined
    link_1_26_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- gsar_ao6
    this.w_cocodazi=i_codazi
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONTROPA where COCODAZI=KeySet.COCODAZI
    *
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTROPA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTROPA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTROPA '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_24_joined=this.AddJoinedLink_1_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RAGAZI = space(40)
        .w_PERIVB = 0
        .w_TIPART = space(1)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESCRITE = space(35)
        .w_DESRITE = space(40)
        .w_DESENAS = space(40)
        .w_RITE = space(1)
        .w_DESCIRPE = space(40)
        .w_DECRPREV = space(40)
        .w_DESCGIRO = space(35)
        .w_DESRIP = space(40)
        .w_PART = space(1)
        .w_FLPART = space(1)
        .w_DESTRS = space(40)
        .w_DESEVI = space(40)
        .w_DESSAL = space(35)
        .w_DESGIR = space(35)
        .w_DESRIT = space(35)
        .w_RITE1 = space(1)
        .w_COCODAZI = NVL(COCODAZI,space(5))
          if link_1_1_joined
            this.w_COCODAZI = NVL(AZCODAZI101,NVL(this.w_COCODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_COTIPCON = NVL(COTIPCON,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .w_COCAURIT = NVL(COCAURIT,space(5))
          if link_1_13_joined
            this.w_COCAURIT = NVL(CCCODICE113,NVL(this.w_COCAURIT,space(5)))
            this.w_DESCRITE = NVL(CCDESCRI113,space(35))
            this.w_RITE = NVL(CCFLRITE113,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO113),ctod("  /  /  "))
          else
          .link_1_13('Load')
          endif
        .w_COCONRIT = NVL(COCONRIT,space(15))
          if link_1_15_joined
            this.w_COCONRIT = NVL(ANCODICE115,NVL(this.w_COCONRIT,space(15)))
            this.w_DESRITE = NVL(ANDESCRI115,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO115),ctod("  /  /  "))
          else
          .link_1_15('Load')
          endif
        .w_COCORIPR = NVL(COCORIPR,space(15))
          if link_1_17_joined
            this.w_COCORIPR = NVL(ANCODICE117,NVL(this.w_COCORIPR,space(15)))
            this.w_DESRIP = NVL(ANDESCRI117,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO117),ctod("  /  /  "))
            this.w_PART = NVL(ANPARTSN117,space(1))
          else
          .link_1_17('Load')
          endif
        .w_COENASAR = NVL(COENASAR,space(15))
          if link_1_18_joined
            this.w_COENASAR = NVL(ANCODICE118,NVL(this.w_COENASAR,space(15)))
            this.w_DESENAS = NVL(ANDESCRI118,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO118),ctod("  /  /  "))
          else
          .link_1_18('Load')
          endif
        .w_COGIRRIT = NVL(COGIRRIT,space(5))
          if link_1_19_joined
            this.w_COGIRRIT = NVL(CCCODICE119,NVL(this.w_COGIRRIT,space(5)))
            this.w_DESCGIRO = NVL(CCDESCRI119,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO119),ctod("  /  /  "))
            this.w_FLPART = NVL(CCFLPART119,space(1))
          else
          .link_1_19('Load')
          endif
        .w_COCOIRVE = NVL(COCOIRVE,space(15))
          if link_1_20_joined
            this.w_COCOIRVE = NVL(ANCODICE120,NVL(this.w_COCOIRVE,space(15)))
            this.w_DESCIRPE = NVL(ANDESCRI120,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO120),ctod("  /  /  "))
            this.w_PART = NVL(ANPARTSN120,space(1))
          else
          .link_1_20('Load')
          endif
        .w_COCOPRVE = NVL(COCOPRVE,space(15))
          if link_1_21_joined
            this.w_COCOPRVE = NVL(ANCODICE121,NVL(this.w_COCOPRVE,space(15)))
            this.w_DECRPREV = NVL(ANDESCRI121,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO121),ctod("  /  /  "))
            this.w_PART = NVL(ANPARTSN121,space(1))
          else
          .link_1_21('Load')
          endif
        .w_COCAURIL = NVL(COCAURIL,space(5))
          if link_1_22_joined
            this.w_COCAURIL = NVL(CCCODICE122,NVL(this.w_COCAURIL,space(5)))
            this.w_DESRIT = NVL(CCDESCRI122,space(35))
            this.w_RITE1 = NVL(CCFLRITE122,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO122),ctod("  /  /  "))
          else
          .link_1_22('Load')
          endif
        .w_COCONTRS = NVL(COCONTRS,space(15))
          if link_1_23_joined
            this.w_COCONTRS = NVL(ANCODICE123,NVL(this.w_COCONTRS,space(15)))
            this.w_DESTRS = NVL(ANDESCRI123,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO123),ctod("  /  /  "))
          else
          .link_1_23('Load')
          endif
        .w_COCAUSLD = NVL(COCAUSLD,space(5))
          if link_1_24_joined
            this.w_COCAUSLD = NVL(CCCODICE124,NVL(this.w_COCAUSLD,space(5)))
            this.w_DESSAL = NVL(CCDESCRI124,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO124),ctod("  /  /  "))
          else
          .link_1_24('Load')
          endif
        .w_COCAUGIR = NVL(COCAUGIR,space(5))
          if link_1_25_joined
            this.w_COCAUGIR = NVL(CCCODICE125,NVL(this.w_COCAUGIR,space(5)))
            this.w_DESGIR = NVL(CCDESCRI125,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO125),ctod("  /  /  "))
          else
          .link_1_25('Load')
          endif
        .w_COEVICRE = NVL(COEVICRE,space(15))
          if link_1_26_joined
            this.w_COEVICRE = NVL(ANCODICE126,NVL(this.w_COEVICRE,space(15)))
            this.w_DESEVI = NVL(ANDESCRI126,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO126),ctod("  /  /  "))
          else
          .link_1_26('Load')
          endif
        cp_LoadRecExtFlds(this,'CONTROPA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_ao6
    if this.w_cocodazi<>i_codazi and not empty(this.w_cocodazi)
     this.blankrec()
     this.w_cocodazi=""
     ah_ErrorMsg("Manutenzione consentita alla sola azienda corrente (%1)",,'',i_CODAZI)
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COCODAZI = space(5)
      .w_RAGAZI = space(40)
      .w_COTIPCON = space(1)
      .w_PERIVB = 0
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_TIPART = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_COCAURIT = space(5)
      .w_DESCRITE = space(35)
      .w_COCONRIT = space(15)
      .w_DESRITE = space(40)
      .w_COCORIPR = space(15)
      .w_COENASAR = space(15)
      .w_COGIRRIT = space(5)
      .w_COCOIRVE = space(15)
      .w_COCOPRVE = space(15)
      .w_COCAURIL = space(5)
      .w_COCONTRS = space(15)
      .w_COCAUSLD = space(5)
      .w_COCAUGIR = space(5)
      .w_COEVICRE = space(15)
      .w_DESENAS = space(40)
      .w_RITE = space(1)
      .w_DESCIRPE = space(40)
      .w_DECRPREV = space(40)
      .w_DESCGIRO = space(35)
      .w_DESRIP = space(40)
      .w_PART = space(1)
      .w_FLPART = space(1)
      .w_DESTRS = space(40)
      .w_DESEVI = space(40)
      .w_DESSAL = space(35)
      .w_DESGIR = space(35)
      .w_DESRIT = space(35)
      .w_RITE1 = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_COCODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_COTIPCON = 'G'
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
          .DoRTCalc(4,9,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(11,12,.f.)
          if not(empty(.w_COCAURIT))
          .link_1_13('Full')
          endif
        .DoRTCalc(13,14,.f.)
          if not(empty(.w_COCONRIT))
          .link_1_15('Full')
          endif
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_COCORIPR))
          .link_1_17('Full')
          endif
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_COENASAR))
          .link_1_18('Full')
          endif
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_COGIRRIT))
          .link_1_19('Full')
          endif
        .DoRTCalc(19,19,.f.)
          if not(empty(.w_COCOIRVE))
          .link_1_20('Full')
          endif
        .DoRTCalc(20,20,.f.)
          if not(empty(.w_COCOPRVE))
          .link_1_21('Full')
          endif
        .DoRTCalc(21,21,.f.)
          if not(empty(.w_COCAURIL))
          .link_1_22('Full')
          endif
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_COCONTRS))
          .link_1_23('Full')
          endif
        .DoRTCalc(23,23,.f.)
          if not(empty(.w_COCAUSLD))
          .link_1_24('Full')
          endif
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_COCAUGIR))
          .link_1_25('Full')
          endif
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_COEVICRE))
          .link_1_26('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTROPA')
    this.DoRTCalc(26,39,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_ao6
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCOCAURIT_1_13.enabled = i_bVal
      .Page1.oPag.oCOCONRIT_1_15.enabled = i_bVal
      .Page1.oPag.oCOCORIPR_1_17.enabled = i_bVal
      .Page1.oPag.oCOENASAR_1_18.enabled = i_bVal
      .Page1.oPag.oCOGIRRIT_1_19.enabled = i_bVal
      .Page1.oPag.oCOCOIRVE_1_20.enabled = i_bVal
      .Page1.oPag.oCOCOPRVE_1_21.enabled = i_bVal
      .Page1.oPag.oCOCAURIL_1_22.enabled = i_bVal
      .Page1.oPag.oCOCONTRS_1_23.enabled = i_bVal
      .Page1.oPag.oCOCAUSLD_1_24.enabled = i_bVal
      .Page1.oPag.oCOCAUGIR_1_25.enabled = i_bVal
      .Page1.oPag.oCOEVICRE_1_26.enabled = i_bVal
      .Page1.oPag.oBtn_1_28.enabled = i_bVal
      .Page1.oPag.oBtn_1_30.enabled = .Page1.oPag.oBtn_1_30.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'CONTROPA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODAZI,"COCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPCON,"COTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAURIT,"COCAURIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCONRIT,"COCONRIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCORIPR,"COCORIPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COENASAR,"COENASAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COGIRRIT,"COGIRRIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOIRVE,"COCOIRVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOPRVE,"COCOPRVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAURIL,"COCAURIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCONTRS,"COCONTRS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAUSLD,"COCAUSLD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAUGIR,"COCAUGIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COEVICRE,"COEVICRE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    i_lTable = "CONTROPA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CONTROPA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONTROPA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONTROPA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTROPA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(COCODAZI,COTIPCON,UTCC,UTCV,UTDC"+;
                  ",UTDV,COCAURIT,COCONRIT,COCORIPR,COENASAR"+;
                  ",COGIRRIT,COCOIRVE,COCOPRVE,COCAURIL,COCONTRS"+;
                  ",COCAUSLD,COCAUGIR,COEVICRE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_COCODAZI)+;
                  ","+cp_ToStrODBC(this.w_COTIPCON)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBCNull(this.w_COCAURIT)+;
                  ","+cp_ToStrODBCNull(this.w_COCONRIT)+;
                  ","+cp_ToStrODBCNull(this.w_COCORIPR)+;
                  ","+cp_ToStrODBCNull(this.w_COENASAR)+;
                  ","+cp_ToStrODBCNull(this.w_COGIRRIT)+;
                  ","+cp_ToStrODBCNull(this.w_COCOIRVE)+;
                  ","+cp_ToStrODBCNull(this.w_COCOPRVE)+;
                  ","+cp_ToStrODBCNull(this.w_COCAURIL)+;
                  ","+cp_ToStrODBCNull(this.w_COCONTRS)+;
                  ","+cp_ToStrODBCNull(this.w_COCAUSLD)+;
                  ","+cp_ToStrODBCNull(this.w_COCAUGIR)+;
                  ","+cp_ToStrODBCNull(this.w_COEVICRE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTROPA')
        cp_CheckDeletedKey(i_cTable,0,'COCODAZI',this.w_COCODAZI)
        INSERT INTO (i_cTable);
              (COCODAZI,COTIPCON,UTCC,UTCV,UTDC,UTDV,COCAURIT,COCONRIT,COCORIPR,COENASAR,COGIRRIT,COCOIRVE,COCOPRVE,COCAURIL,COCONTRS,COCAUSLD,COCAUGIR,COEVICRE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_COCODAZI;
                  ,this.w_COTIPCON;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_COCAURIT;
                  ,this.w_COCONRIT;
                  ,this.w_COCORIPR;
                  ,this.w_COENASAR;
                  ,this.w_COGIRRIT;
                  ,this.w_COCOIRVE;
                  ,this.w_COCOPRVE;
                  ,this.w_COCAURIL;
                  ,this.w_COCONTRS;
                  ,this.w_COCAUSLD;
                  ,this.w_COCAUGIR;
                  ,this.w_COEVICRE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONTROPA_IDX,i_nConn)
      *
      * update CONTROPA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONTROPA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " COTIPCON="+cp_ToStrODBC(this.w_COTIPCON)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",COCAURIT="+cp_ToStrODBCNull(this.w_COCAURIT)+;
             ",COCONRIT="+cp_ToStrODBCNull(this.w_COCONRIT)+;
             ",COCORIPR="+cp_ToStrODBCNull(this.w_COCORIPR)+;
             ",COENASAR="+cp_ToStrODBCNull(this.w_COENASAR)+;
             ",COGIRRIT="+cp_ToStrODBCNull(this.w_COGIRRIT)+;
             ",COCOIRVE="+cp_ToStrODBCNull(this.w_COCOIRVE)+;
             ",COCOPRVE="+cp_ToStrODBCNull(this.w_COCOPRVE)+;
             ",COCAURIL="+cp_ToStrODBCNull(this.w_COCAURIL)+;
             ",COCONTRS="+cp_ToStrODBCNull(this.w_COCONTRS)+;
             ",COCAUSLD="+cp_ToStrODBCNull(this.w_COCAUSLD)+;
             ",COCAUGIR="+cp_ToStrODBCNull(this.w_COCAUGIR)+;
             ",COEVICRE="+cp_ToStrODBCNull(this.w_COEVICRE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONTROPA')
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        UPDATE (i_cTable) SET;
              COTIPCON=this.w_COTIPCON;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,COCAURIT=this.w_COCAURIT;
             ,COCONRIT=this.w_COCONRIT;
             ,COCORIPR=this.w_COCORIPR;
             ,COENASAR=this.w_COENASAR;
             ,COGIRRIT=this.w_COGIRRIT;
             ,COCOIRVE=this.w_COCOIRVE;
             ,COCOPRVE=this.w_COCOPRVE;
             ,COCAURIL=this.w_COCAURIL;
             ,COCONTRS=this.w_COCONTRS;
             ,COCAUSLD=this.w_COCAUSLD;
             ,COCAUGIR=this.w_COCAUGIR;
             ,COEVICRE=this.w_COEVICRE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONTROPA_IDX,i_nConn)
      *
      * delete CONTROPA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
            .w_COTIPCON = 'G'
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,39,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCOGIRRIT_1_19.enabled = this.oPgFrm.Page1.oPag.oCOGIRRIT_1_19.mCond()
    this.oPgFrm.Page1.oPag.oCOCOIRVE_1_20.enabled = this.oPgFrm.Page1.oPag.oCOCOIRVE_1_20.mCond()
    this.oPgFrm.Page1.oPag.oCOCOPRVE_1_21.enabled = this.oPgFrm.Page1.oPag.oCOCOPRVE_1_21.mCond()
    this.oPgFrm.Page1.oPag.oCOCAURIL_1_22.enabled = this.oPgFrm.Page1.oPag.oCOCAURIL_1_22.mCond()
    this.oPgFrm.Page1.oPag.oCOCONTRS_1_23.enabled = this.oPgFrm.Page1.oPag.oCOCONTRS_1_23.mCond()
    this.oPgFrm.Page1.oPag.oCOCAUSLD_1_24.enabled = this.oPgFrm.Page1.oPag.oCOCAUSLD_1_24.mCond()
    this.oPgFrm.Page1.oPag.oCOCAUGIR_1_25.enabled = this.oPgFrm.Page1.oPag.oCOCAUGIR_1_25.mCond()
    this.oPgFrm.Page1.oPag.oCOEVICRE_1_26.enabled = this.oPgFrm.Page1.oPag.oCOEVICRE_1_26.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_COCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_COCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on CONTROPA.COCODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and CONTROPA.COCODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAURIT
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAURIT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAURIT)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRITE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAURIT))
          select CCCODICE,CCDESCRI,CCFLRITE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAURIT)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCAURIT) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAURIT_1_13'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCORIT.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRITE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLRITE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAURIT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRITE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAURIT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAURIT)
            select CCCODICE,CCDESCRI,CCFLRITE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAURIT = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCRITE = NVL(_Link_.CCDESCRI,space(35))
      this.w_RITE = NVL(_Link_.CCFLRITE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAURIT = space(5)
      endif
      this.w_DESCRITE = space(35)
      this.w_RITE = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_RITE='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La causale deve avere il flag rileva ritenute attivato oppure non obsoleta")
        endif
        this.w_COCAURIT = space(5)
        this.w_DESCRITE = space(35)
        this.w_RITE = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAURIT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.CCCODICE as CCCODICE113"+ ",link_1_13.CCDESCRI as CCDESCRI113"+ ",link_1_13.CCFLRITE as CCFLRITE113"+ ",link_1_13.CCDTOBSO as CCDTOBSO113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on CONTROPA.COCAURIT=link_1_13.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and CONTROPA.COCAURIT=link_1_13.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCONRIT
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCONRIT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCONRIT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCONRIT))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCONRIT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCONRIT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCONRIT)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCONRIT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCONRIT_1_15'),i_cWhere,'GSAR_BZC',"Conti erario C/Ritenute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCONRIT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCONRIT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCONRIT)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCONRIT = NVL(_Link_.ANCODICE,space(15))
      this.w_DESRITE = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCONRIT = space(15)
      endif
      this.w_DESRITE = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COCONRIT = space(15)
        this.w_DESRITE = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCONRIT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.ANCODICE as ANCODICE115"+ ",link_1_15.ANDESCRI as ANDESCRI115"+ ",link_1_15.ANDTOBSO as ANDTOBSO115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on CONTROPA.COCONRIT=link_1_15.ANCODICE"+" and CONTROPA.COTIPCON=link_1_15.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and CONTROPA.COCONRIT=link_1_15.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_15.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCORIPR
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCORIPR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCORIPR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCORIPR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCORIPR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCORIPR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCORIPR_1_17'),i_cWhere,'GSAR_BZC',"Elenco conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCORIPR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCORIPR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCORIPR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCORIPR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESRIP = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PART = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCORIPR = space(15)
      endif
      this.w_DESRIP = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COCORIPR = space(15)
        this.w_DESRIP = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCORIPR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.ANCODICE as ANCODICE117"+ ",link_1_17.ANDESCRI as ANDESCRI117"+ ",link_1_17.ANDTOBSO as ANDTOBSO117"+ ",link_1_17.ANPARTSN as ANPARTSN117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on CONTROPA.COCORIPR=link_1_17.ANCODICE"+" and CONTROPA.COTIPCON=link_1_17.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and CONTROPA.COCORIPR=link_1_17.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_17.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COENASAR
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COENASAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COENASAR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COENASAR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COENASAR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COENASAR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COENASAR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COENASAR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOENASAR_1_18'),i_cWhere,'GSAR_BZC',"Conti ENASARCO",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COENASAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COENASAR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COENASAR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COENASAR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESENAS = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COENASAR = space(15)
      endif
      this.w_DESENAS = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COENASAR = space(15)
        this.w_DESENAS = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COENASAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.ANCODICE as ANCODICE118"+ ",link_1_18.ANDESCRI as ANDESCRI118"+ ",link_1_18.ANDTOBSO as ANDTOBSO118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on CONTROPA.COENASAR=link_1_18.ANCODICE"+" and CONTROPA.COTIPCON=link_1_18.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and CONTROPA.COENASAR=link_1_18.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_18.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COGIRRIT
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COGIRRIT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COGIRRIT)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCFLPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COGIRRIT))
          select CCCODICE,CCDESCRI,CCDTOBSO,CCFLPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COGIRRIT)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COGIRRIT) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOGIRRIT_1_19'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCFLPART";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COGIRRIT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COGIRRIT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COGIRRIT)
            select CCCODICE,CCDESCRI,CCDTOBSO,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COGIRRIT = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCGIRO = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_FLPART = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COGIRRIT = space(5)
      endif
      this.w_DESCGIRO = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLPART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST ) AND .w_FLPART='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La causale selezionata � inesistente o obsoleta o non salda le partite")
        endif
        this.w_COGIRRIT = space(5)
        this.w_DESCGIRO = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLPART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COGIRRIT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.CCCODICE as CCCODICE119"+ ",link_1_19.CCDESCRI as CCDESCRI119"+ ",link_1_19.CCDTOBSO as CCDTOBSO119"+ ",link_1_19.CCFLPART as CCFLPART119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on CONTROPA.COGIRRIT=link_1_19.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and CONTROPA.COGIRRIT=link_1_19.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOIRVE
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOIRVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCOIRVE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCOIRVE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCOIRVE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCOIRVE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCOIRVE_1_20'),i_cWhere,'GSAR_BZC',"Conti erario C/Ritenute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOIRVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCOIRVE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCOIRVE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOIRVE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCIRPE = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PART = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCOIRVE = space(15)
      endif
      this.w_DESCIRPE = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_PART<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_COCOIRVE = space(15)
        this.w_DESCIRPE = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOIRVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.ANCODICE as ANCODICE120"+ ",link_1_20.ANDESCRI as ANDESCRI120"+ ",link_1_20.ANDTOBSO as ANDTOBSO120"+ ",link_1_20.ANPARTSN as ANPARTSN120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on CONTROPA.COCOIRVE=link_1_20.ANCODICE"+" and CONTROPA.COTIPCON=link_1_20.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and CONTROPA.COCOIRVE=link_1_20.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_20.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOPRVE
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOPRVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCOPRVE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCOPRVE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCOPRVE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCOPRVE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCOPRVE_1_21'),i_cWhere,'GSAR_BZC',"Conti erario C/Ritenute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOPRVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCOPRVE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCOPRVE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOPRVE = NVL(_Link_.ANCODICE,space(15))
      this.w_DECRPREV = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PART = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCOPRVE = space(15)
      endif
      this.w_DECRPREV = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_PART<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_COCOPRVE = space(15)
        this.w_DECRPREV = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOPRVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.ANCODICE as ANCODICE121"+ ",link_1_21.ANDESCRI as ANDESCRI121"+ ",link_1_21.ANDTOBSO as ANDTOBSO121"+ ",link_1_21.ANPARTSN as ANPARTSN121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on CONTROPA.COCOPRVE=link_1_21.ANCODICE"+" and CONTROPA.COTIPCON=link_1_21.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and CONTROPA.COCOPRVE=link_1_21.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_21.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAURIL
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAURIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAURIL)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRITE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAURIL))
          select CCCODICE,CCDESCRI,CCFLRITE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAURIL)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCAURIL) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAURIL_1_22'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCORIT.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRITE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLRITE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAURIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRITE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAURIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAURIL)
            select CCCODICE,CCDESCRI,CCFLRITE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAURIL = NVL(_Link_.CCCODICE,space(5))
      this.w_DESRIT = NVL(_Link_.CCDESCRI,space(35))
      this.w_RITE1 = NVL(_Link_.CCFLRITE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAURIL = space(5)
      endif
      this.w_DESRIT = space(35)
      this.w_RITE1 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_RITE1='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La causale deve avere il flag rileva ritenute attivato oppure non obsoleta")
        endif
        this.w_COCAURIL = space(5)
        this.w_DESRIT = space(35)
        this.w_RITE1 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAURIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.CCCODICE as CCCODICE122"+ ",link_1_22.CCDESCRI as CCDESCRI122"+ ",link_1_22.CCFLRITE as CCFLRITE122"+ ",link_1_22.CCDTOBSO as CCDTOBSO122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on CONTROPA.COCAURIL=link_1_22.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and CONTROPA.COCAURIL=link_1_22.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCONTRS
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCONTRS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCONTRS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCONTRS))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCONTRS)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCONTRS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCONTRS)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCONTRS) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCONTRS_1_23'),i_cWhere,'GSAR_API',"Conti contropartita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto!")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCONTRS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCONTRS);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCONTRS)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCONTRS = NVL(_Link_.ANCODICE,space(15))
      this.w_DESTRS = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCONTRS = space(15)
      endif
      this.w_DESTRS = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto!")
        endif
        this.w_COCONTRS = space(15)
        this.w_DESTRS = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCONTRS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.ANCODICE as ANCODICE123"+ ",link_1_23.ANDESCRI as ANDESCRI123"+ ",link_1_23.ANDTOBSO as ANDTOBSO123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on CONTROPA.COCONTRS=link_1_23.ANCODICE"+" and CONTROPA.COTIPCON=link_1_23.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and CONTROPA.COCONTRS=link_1_23.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_23.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAUSLD
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAUSLD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAUSLD)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAUSLD))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAUSLD)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCAUSLD)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCAUSLD)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCAUSLD) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAUSLD_1_24'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCOSAL.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAUSLD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAUSLD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAUSLD)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAUSLD = NVL(_Link_.CCCODICE,space(5))
      this.w_DESSAL = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAUSLD = space(5)
      endif
      this.w_DESSAL = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La causale selezionata � inesistente o obsoleta")
        endif
        this.w_COCAUSLD = space(5)
        this.w_DESSAL = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAUSLD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_24.CCCODICE as CCCODICE124"+ ",link_1_24.CCDESCRI as CCDESCRI124"+ ",link_1_24.CCDTOBSO as CCDTOBSO124"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_24 on CONTROPA.COCAUSLD=link_1_24.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_24"
          i_cKey=i_cKey+'+" and CONTROPA.COCAUSLD=link_1_24.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAUGIR
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAUGIR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAUGIR)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAUGIR))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAUGIR)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCAUGIR) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAUGIR_1_25'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAUGIR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAUGIR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAUGIR)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAUGIR = NVL(_Link_.CCCODICE,space(5))
      this.w_DESGIR = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAUGIR = space(5)
      endif
      this.w_DESGIR = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La causale selezionata � inesistente o obsoleta!")
        endif
        this.w_COCAUGIR = space(5)
        this.w_DESGIR = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAUGIR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.CCCODICE as CCCODICE125"+ ",link_1_25.CCDESCRI as CCDESCRI125"+ ",link_1_25.CCDTOBSO as CCDTOBSO125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on CONTROPA.COCAUGIR=link_1_25.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and CONTROPA.COCAUGIR=link_1_25.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COEVICRE
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COEVICRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COEVICRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COEVICRE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COEVICRE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COEVICRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COEVICRE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COEVICRE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOEVICRE_1_26'),i_cWhere,'GSAR_API',"Conti contropartita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto!")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COEVICRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COEVICRE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COEVICRE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COEVICRE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESEVI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COEVICRE = space(15)
      endif
      this.w_DESEVI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto!")
        endif
        this.w_COEVICRE = space(15)
        this.w_DESEVI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COEVICRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.ANCODICE as ANCODICE126"+ ",link_1_26.ANDESCRI as ANDESCRI126"+ ",link_1_26.ANDTOBSO as ANDTOBSO126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on CONTROPA.COEVICRE=link_1_26.ANCODICE"+" and CONTROPA.COTIPCON=link_1_26.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and CONTROPA.COEVICRE=link_1_26.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_26.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOCAURIT_1_13.value==this.w_COCAURIT)
      this.oPgFrm.Page1.oPag.oCOCAURIT_1_13.value=this.w_COCAURIT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRITE_1_14.value==this.w_DESCRITE)
      this.oPgFrm.Page1.oPag.oDESCRITE_1_14.value=this.w_DESCRITE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCONRIT_1_15.value==this.w_COCONRIT)
      this.oPgFrm.Page1.oPag.oCOCONRIT_1_15.value=this.w_COCONRIT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRITE_1_16.value==this.w_DESRITE)
      this.oPgFrm.Page1.oPag.oDESRITE_1_16.value=this.w_DESRITE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCORIPR_1_17.value==this.w_COCORIPR)
      this.oPgFrm.Page1.oPag.oCOCORIPR_1_17.value=this.w_COCORIPR
    endif
    if not(this.oPgFrm.Page1.oPag.oCOENASAR_1_18.value==this.w_COENASAR)
      this.oPgFrm.Page1.oPag.oCOENASAR_1_18.value=this.w_COENASAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCOGIRRIT_1_19.value==this.w_COGIRRIT)
      this.oPgFrm.Page1.oPag.oCOGIRRIT_1_19.value=this.w_COGIRRIT
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOIRVE_1_20.value==this.w_COCOIRVE)
      this.oPgFrm.Page1.oPag.oCOCOIRVE_1_20.value=this.w_COCOIRVE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOPRVE_1_21.value==this.w_COCOPRVE)
      this.oPgFrm.Page1.oPag.oCOCOPRVE_1_21.value=this.w_COCOPRVE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCAURIL_1_22.value==this.w_COCAURIL)
      this.oPgFrm.Page1.oPag.oCOCAURIL_1_22.value=this.w_COCAURIL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCONTRS_1_23.value==this.w_COCONTRS)
      this.oPgFrm.Page1.oPag.oCOCONTRS_1_23.value=this.w_COCONTRS
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCAUSLD_1_24.value==this.w_COCAUSLD)
      this.oPgFrm.Page1.oPag.oCOCAUSLD_1_24.value=this.w_COCAUSLD
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCAUGIR_1_25.value==this.w_COCAUGIR)
      this.oPgFrm.Page1.oPag.oCOCAUGIR_1_25.value=this.w_COCAUGIR
    endif
    if not(this.oPgFrm.Page1.oPag.oCOEVICRE_1_26.value==this.w_COEVICRE)
      this.oPgFrm.Page1.oPag.oCOEVICRE_1_26.value=this.w_COEVICRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESENAS_1_27.value==this.w_DESENAS)
      this.oPgFrm.Page1.oPag.oDESENAS_1_27.value=this.w_DESENAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCIRPE_1_34.value==this.w_DESCIRPE)
      this.oPgFrm.Page1.oPag.oDESCIRPE_1_34.value=this.w_DESCIRPE
    endif
    if not(this.oPgFrm.Page1.oPag.oDECRPREV_1_35.value==this.w_DECRPREV)
      this.oPgFrm.Page1.oPag.oDECRPREV_1_35.value=this.w_DECRPREV
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCGIRO_1_36.value==this.w_DESCGIRO)
      this.oPgFrm.Page1.oPag.oDESCGIRO_1_36.value=this.w_DESCGIRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIP_1_38.value==this.w_DESRIP)
      this.oPgFrm.Page1.oPag.oDESRIP_1_38.value=this.w_DESRIP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTRS_1_48.value==this.w_DESTRS)
      this.oPgFrm.Page1.oPag.oDESTRS_1_48.value=this.w_DESTRS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESEVI_1_49.value==this.w_DESEVI)
      this.oPgFrm.Page1.oPag.oDESEVI_1_49.value=this.w_DESEVI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSAL_1_51.value==this.w_DESSAL)
      this.oPgFrm.Page1.oPag.oDESSAL_1_51.value=this.w_DESSAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGIR_1_52.value==this.w_DESGIR)
      this.oPgFrm.Page1.oPag.oDESGIR_1_52.value=this.w_DESGIR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIT_1_56.value==this.w_DESRIT)
      this.oPgFrm.Page1.oPag.oDESRIT_1_56.value=this.w_DESRIT
    endif
    cp_SetControlsValueExtFlds(this,'CONTROPA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_RITE='S')  and not(empty(.w_COCAURIT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCAURIT_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La causale deve avere il flag rileva ritenute attivato oppure non obsoleta")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COCONRIT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCONRIT_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_COCORIPR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCORIPR_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COENASAR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOENASAR_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST ) AND .w_FLPART='S')  and (('RITE' $ UPPER(i_cModules)))  and not(empty(.w_COGIRRIT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOGIRRIT_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La causale selezionata � inesistente o obsoleta o non salda le partite")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_PART<>'S')  and (('RITE' $ UPPER(i_cModules)))  and not(empty(.w_COCOIRVE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCOIRVE_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_PART<>'S')  and (('RITE' $ UPPER(i_cModules)))  and not(empty(.w_COCOPRVE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCOPRVE_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_RITE1='S')  and ('RITE' $ UPPER(i_cModules) or isalt())  and not(empty(.w_COCAURIL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCAURIL_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La causale deve avere il flag rileva ritenute attivato oppure non obsoleta")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and ('RITE' $ UPPER(i_cModules) or isalt())  and not(empty(.w_COCONTRS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCONTRS_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto!")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and ('RITE' $ UPPER(i_cModules) or isalt())  and not(empty(.w_COCAUSLD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCAUSLD_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La causale selezionata � inesistente o obsoleta")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST ))  and ('RITE' $ UPPER(i_cModules) or isalt())  and not(empty(.w_COCAUGIR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCAUGIR_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La causale selezionata � inesistente o obsoleta!")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and ('RITE' $ UPPER(i_cModules) or isalt())  and not(empty(.w_COEVICRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOEVICRE_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto!")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_ao6Pag1 as StdContainer
  Width  = 601
  height = 440
  stdWidth  = 601
  stdheight = 440
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_9 as cp_runprogram with uid="AEMFBQRMHA",left=-5, top=456, width=226,height=20,;
    caption='GSAR_BAM(modifica)',;
   bGlobalFont=.t.,;
    prg='GSAR_BAM("modifica")',;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 179863462

  add object oCOCAURIT_1_13 as StdField with uid="KFUOIIUVSQ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COCAURIT", cQueryName = "COCAURIT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "La causale deve avere il flag rileva ritenute attivato oppure non obsoleta",;
    ToolTipText = "Causale rilevazione ritenuta",;
    HelpContextID = 49542022,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=170, Top=35, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAURIT"

  func oCOCAURIT_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAURIT_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAURIT_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAURIT_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCORIT.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAURIT_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAURIT
     i_obj.ecpSave()
  endproc

  add object oDESCRITE_1_14 as StdField with uid="HHGDEACQXC",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCRITE", cQueryName = "DESCRITE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 64946811,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=309, Top=35, InputMask=replicate('X',35)

  add object oCOCONRIT_1_15 as StdField with uid="UYDFDMRAII",rtseq=14,rtrep=.f.,;
    cFormVar = "w_COCONRIT", cQueryName = "COCONRIT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita di contabilizzazione ritenute IRPEF",;
    HelpContextID = 55964550,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=170, Top=63, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCONRIT"

  func oCOCONRIT_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCONRIT_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCONRIT_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCONRIT_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti erario C/Ritenute",'',this.parent.oContained
  endproc
  proc oCOCONRIT_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCONRIT
     i_obj.ecpSave()
  endproc

  add object oDESRITE_1_16 as StdField with uid="AIBBAQHYBG",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESRITE", cQueryName = "DESRITE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 241041974,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=309, Top=63, InputMask=replicate('X',40)

  add object oCOCORIPR_1_17 as StdField with uid="ASVTQSPEXG",rtseq=16,rtrep=.f.,;
    cFormVar = "w_COCORIPR", cQueryName = "COCORIPR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita di contabilizzazione contributi previdenziali",;
    HelpContextID = 65670264,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=170, Top=91, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCORIPR"

  func oCOCORIPR_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCORIPR_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCORIPR_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCORIPR_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco conti",'',this.parent.oContained
  endproc
  proc oCOCORIPR_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCORIPR
     i_obj.ecpSave()
  endproc

  add object oCOENASAR_1_18 as StdField with uid="FTPUMTICVQ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_COENASAR", cQueryName = "COENASAR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita di contabilizzazione ENASARCO",;
    HelpContextID = 215559288,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=170, Top=119, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COENASAR"

  func oCOENASAR_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOENASAR_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOENASAR_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOENASAR_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti ENASARCO",'',this.parent.oContained
  endproc
  proc oCOENASAR_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COENASAR
     i_obj.ecpSave()
  endproc

  add object oCOGIRRIT_1_19 as StdField with uid="TNSMKNSZTL",rtseq=18,rtrep=.f.,;
    cFormVar = "w_COGIRRIT", cQueryName = "COGIRRIT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "La causale selezionata � inesistente o obsoleta o non salda le partite",;
    ToolTipText = "Causale giroconto ritenute",;
    HelpContextID = 52147078,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=170, Top=147, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COGIRRIT"

  func oCOGIRRIT_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (('RITE' $ UPPER(i_cModules)))
    endwith
   endif
  endfunc

  func oCOGIRRIT_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOGIRRIT_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOGIRRIT_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOGIRRIT_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oCOGIRRIT_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COGIRRIT
     i_obj.ecpSave()
  endproc

  add object oCOCOIRVE_1_20 as StdField with uid="EOOWWKGNBT",rtseq=19,rtrep=.f.,;
    cFormVar = "w_COCOIRVE", cQueryName = "COCOIRVE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Contropartita per rilevazione debito v/erario",;
    HelpContextID = 61207445,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=170, Top=175, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCOIRVE"

  func oCOCOIRVE_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (('RITE' $ UPPER(i_cModules)))
    endwith
   endif
  endfunc

  func oCOCOIRVE_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCOIRVE_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCOIRVE_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCOIRVE_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti erario C/Ritenute",'',this.parent.oContained
  endproc
  proc oCOCOIRVE_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCOIRVE
     i_obj.ecpSave()
  endproc

  add object oCOCOPRVE_1_21 as StdField with uid="VIGDXLLKHW",rtseq=20,rtrep=.f.,;
    cFormVar = "w_COCOPRVE", cQueryName = "COCOPRVE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Contropartita per rilevazione debito v/istituto previdenziale",;
    HelpContextID = 53867413,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=170, Top=203, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCOPRVE"

  func oCOCOPRVE_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (('RITE' $ UPPER(i_cModules)))
    endwith
   endif
  endfunc

  func oCOCOPRVE_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCOPRVE_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCOPRVE_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCOPRVE_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti erario C/Ritenute",'',this.parent.oContained
  endproc
  proc oCOCOPRVE_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCOPRVE
     i_obj.ecpSave()
  endproc

  add object oCOCAURIL_1_22 as StdField with uid="AFTYIWHNMH",rtseq=21,rtrep=.f.,;
    cFormVar = "w_COCAURIL", cQueryName = "COCAURIL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "La causale deve avere il flag rileva ritenute attivato oppure non obsoleta",;
    ToolTipText = "Causale di rilevazione  ritenute attive",;
    HelpContextID = 49542030,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=170, Top=260, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAURIL"

  func oCOCAURIL_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ('RITE' $ UPPER(i_cModules) or isalt())
    endwith
   endif
  endfunc

  func oCOCAURIL_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAURIL_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAURIL_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAURIL_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCORIT.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAURIL_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAURIL
     i_obj.ecpSave()
  endproc

  add object oCOCONTRS_1_23 as StdField with uid="EIRZEQYNBW",rtseq=22,rtrep=.f.,;
    cFormVar = "w_COCONTRS", cQueryName = "COCONTRS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto!",;
    ToolTipText = "Conto di transito ritenute (da utilizzarsi solo con gestione storno immediato attivo)",;
    HelpContextID = 246025337,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=170, Top=287, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCONTRS"

  func oCOCONTRS_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ('RITE' $ UPPER(i_cModules) or isalt())
    endwith
   endif
  endfunc

  func oCOCONTRS_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCONTRS_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCONTRS_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCONTRS_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti contropartita",'',this.parent.oContained
  endproc
  proc oCOCONTRS_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCONTRS
     i_obj.ecpSave()
  endproc

  add object oCOCAUSLD_1_24 as StdField with uid="JCCUHIRFPL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_COCAUSLD", cQueryName = "COCAUSLD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "La causale selezionata � inesistente o obsoleta",;
    ToolTipText = "Causale di saldaconto utilizzata in caso di gestione storno differito",;
    HelpContextID = 32764822,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=170, Top=315, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAUSLD"

  func oCOCAUSLD_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ('RITE' $ UPPER(i_cModules) or isalt())
    endwith
   endif
  endfunc

  func oCOCAUSLD_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAUSLD_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAUSLD_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAUSLD_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCOSAL.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAUSLD_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAUSLD
     i_obj.ecpSave()
  endproc

  add object oCOCAUGIR_1_25 as StdField with uid="ZKZZFZETIT",rtseq=24,rtrep=.f.,;
    cFormVar = "w_COCAUGIR", cQueryName = "COCAUGIR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "La causale selezionata � inesistente o obsoleta!",;
    ToolTipText = "Causale di giroconto utilizzata in caso di gestione storno immediato",;
    HelpContextID = 234091400,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=170, Top=343, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAUGIR"

  func oCOCAUGIR_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ('RITE' $ UPPER(i_cModules) or isalt())
    endwith
   endif
  endfunc

  func oCOCAUGIR_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAUGIR_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAUGIR_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAUGIR_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oCOCAUGIR_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAUGIR
     i_obj.ecpSave()
  endproc

  add object oCOEVICRE_1_26 as StdField with uid="HAEIXGRLFI",rtseq=25,rtrep=.f.,;
    cFormVar = "w_COEVICRE", cQueryName = "COEVICRE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto!",;
    ToolTipText = "Conto di evidenza credito v/erario per ritenute subite",;
    HelpContextID = 224472171,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=169, Top=371, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COEVICRE"

  func oCOEVICRE_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ('RITE' $ UPPER(i_cModules) or isalt())
    endwith
   endif
  endfunc

  func oCOEVICRE_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOEVICRE_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOEVICRE_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOEVICRE_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti contropartita",'',this.parent.oContained
  endproc
  proc oCOEVICRE_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COEVICRE
     i_obj.ecpSave()
  endproc

  add object oDESENAS_1_27 as StdField with uid="IMOMMTXFUG",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESENAS", cQueryName = "DESENAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 195101238,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=309, Top=119, InputMask=replicate('X',40)


  add object oBtn_1_28 as StdButton with uid="JJOWZFIANO",left=497, top=395, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 176752410;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_30 as StdButton with uid="PVGQMYAUAW",left=550, top=395, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per abbandonare";
    , HelpContextID = 169463738;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCIRPE_1_34 as StdField with uid="VTZZPRPDTI",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESCIRPE", cQueryName = "DESCIRPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 206504571,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=309, Top=175, InputMask=replicate('X',40)

  add object oDECRPREV_1_35 as StdField with uid="PQNOYFIRYB",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DECRPREV", cQueryName = "DECRPREV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 214762124,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=309, Top=203, InputMask=replicate('X',40)

  add object oDESCGIRO_1_36 as StdField with uid="NZXTKXKWJJ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESCGIRO", cQueryName = "DESCGIRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 53412485,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=309, Top=147, InputMask=replicate('X',35)

  add object oDESRIP_1_38 as StdField with uid="FQPQZDOXPS",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESRIP", cQueryName = "DESRIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 173933110,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=309, Top=91, InputMask=replicate('X',40)

  add object oDESTRS_1_48 as StdField with uid="WDQLJBMSZN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESTRS", cQueryName = "DESTRS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 233833014,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=309, Top=287, InputMask=replicate('X',40)

  add object oDESEVI_1_49 as StdField with uid="SOANCYYSJZ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESEVI", cQueryName = "DESEVI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 69272118,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=309, Top=371, InputMask=replicate('X',40)

  add object oDESSAL_1_51 as StdField with uid="YCACPMZMUC",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESSAL", cQueryName = "DESSAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 98501174,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=309, Top=315, InputMask=replicate('X',35)

  add object oDESGIR_1_52 as StdField with uid="FLHYJEPFIP",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESGIR", cQueryName = "DESGIR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 206766646,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=309, Top=343, InputMask=replicate('X',35)

  add object oDESRIT_1_56 as StdField with uid="HELBFTVLPY",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESRIT", cQueryName = "DESRIT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 241041974,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=309, Top=260, InputMask=replicate('X',35)

  add object oStr_1_29 as StdString with uid="FMSRSZIPML",Visible=.t., Left=61, Top=63,;
    Alignment=1, Width=108, Height=18,;
    Caption="Ritenute IRPEF:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="CYXHRDCDGN",Visible=.t., Left=61, Top=119,;
    Alignment=1, Width=108, Height=15,;
    Caption="ENASARCO:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="HTYIPCPILF",Visible=.t., Left=61, Top=35,;
    Alignment=1, Width=108, Height=15,;
    Caption="Causale ritenuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="XAOYSWRHIU",Visible=.t., Left=41, Top=91,;
    Alignment=1, Width=128, Height=18,;
    Caption="Contributi previdenziali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="CNGLTUKGDJ",Visible=.t., Left=3, Top=175,;
    Alignment=1, Width=166, Height=15,;
    Caption="Ritenute IRPEF da versare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="BCKCKGVQXP",Visible=.t., Left=8, Top=203,;
    Alignment=1, Width=161, Height=18,;
    Caption="Ritenute prev. da versare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="GGHSCYNJLM",Visible=.t., Left=58, Top=147,;
    Alignment=1, Width=111, Height=15,;
    Caption="Causale giroconto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="GDJJBUFXJB",Visible=.t., Left=4, Top=7,;
    Alignment=0, Width=148, Height=18,;
    Caption="Ritenute operate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="VZZLEMRURE",Visible=.t., Left=32, Top=288,;
    Alignment=1, Width=137, Height=18,;
    Caption="Conto transito rit.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="GOGEVJFIXM",Visible=.t., Left=45, Top=317,;
    Alignment=1, Width=124, Height=18,;
    Caption="Causale saldaconto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="CBJAMFGQCL",Visible=.t., Left=52, Top=346,;
    Alignment=1, Width=117, Height=18,;
    Caption="Causale giroconto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="HBCEFLECFU",Visible=.t., Left=56, Top=373,;
    Alignment=1, Width=113, Height=18,;
    Caption="Credito v/erario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="TDHMBUSXPN",Visible=.t., Left=5, Top=234,;
    Alignment=0, Width=128, Height=18,;
    Caption="Ritenute subite"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_57 as StdString with uid="IKJFTOAXIP",Visible=.t., Left=61, Top=260,;
    Alignment=1, Width=108, Height=15,;
    Caption="Causale Ritenuta:"  ;
  , bGlobalFont=.t.

  add object oBox_1_44 as StdBox with uid="OKRXACBVGW",left=3, top=24, width=597,height=2

  add object oBox_1_54 as StdBox with uid="BYVGNCNYUK",left=3, top=251, width=597,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ao6','CONTROPA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COCODAZI=CONTROPA.COCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
