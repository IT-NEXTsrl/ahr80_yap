* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_spa                                                        *
*              Prospetto degli acquisti                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_393]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2007-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsac_spa",oParentObject))

* --- Class definition
define class tgsac_spa as StdForm
  Top    = 7
  Left   = 35

  * --- Standard Properties
  Width  = 638
  Height = 481
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-13"
  HelpContextID=141191017
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=50

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  ART_ICOL_IDX = 0
  LISTINI_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  CATECOMM_IDX = 0
  ZONE_IDX = 0
  PAG_AMEN_IDX = 0
  VALUTE_IDX = 0
  INVENTAR_IDX = 0
  GRUMERC_IDX = 0
  MARCHI_IDX = 0
  FAM_ARTI_IDX = 0
  CATEGOMO_IDX = 0
  cPrg = "gsac_spa"
  cComment = "Prospetto degli acquisti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_Dadata = ctod('  /  /  ')
  w_Adata = ctod('  /  /  ')
  w_DATREG = ctod('  /  /  ')
  w_DATREG1 = ctod('  /  /  ')
  w_stato = space(20)
  w_AZIENDA = space(5)
  w_CAOVAL1 = 0
  w_DECTOT = 0
  w_TIPOLN = space(1)
  w_ONUME = 0
  w_CODART = space(20)
  w_CODART1 = space(20)
  w_GRUMER = space(5)
  w_tipocon = space(1)
  w_cliente = space(15)
  w_catcomm = space(3)
  w_desccat = space(35)
  w_zona = space(3)
  w_desczona = space(35)
  w_pagam = space(5)
  w_CODMAR = space(5)
  w_CODFAM = space(5)
  w_CODCAT = space(5)
  w_descpaga = space(30)
  w_desccli = space(40)
  w_stval = space(10)
  o_stval = space(10)
  w_valu2 = space(3)
  o_valu2 = space(3)
  w_CAMBIO2 = 0
  w_doctra = space(30)
  w_Fatture = space(10)
  w_notecred = space(10)
  w_descvalu2 = space(35)
  w_CAOVAL2 = 0
  w_TIPO = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_Client = space(1)
  w_codtra = space(2)
  w_codfa = space(2)
  w_codnot = space(2)
  w_DESART = space(40)
  w_GDESC = space(35)
  w_CALCPICT = space(1)
  w_OBTEST1 = ctod('  /  /  ')
  w_SIMVAL = space(5)
  w_DESMAR = space(35)
  w_DESFAM = space(35)
  w_DESCAT = space(35)
  w_DESART1 = space(40)
  w_STALOG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsac_spaPag1","gsac_spa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDadata_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='LISTINI'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='AGENTI'
    this.cWorkTables[6]='CATECOMM'
    this.cWorkTables[7]='ZONE'
    this.cWorkTables[8]='PAG_AMEN'
    this.cWorkTables[9]='VALUTE'
    this.cWorkTables[10]='INVENTAR'
    this.cWorkTables[11]='GRUMERC'
    this.cWorkTables[12]='MARCHI'
    this.cWorkTables[13]='FAM_ARTI'
    this.cWorkTables[14]='CATEGOMO'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAC_BPV with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_Dadata=ctod("  /  /  ")
      .w_Adata=ctod("  /  /  ")
      .w_DATREG=ctod("  /  /  ")
      .w_DATREG1=ctod("  /  /  ")
      .w_stato=space(20)
      .w_AZIENDA=space(5)
      .w_CAOVAL1=0
      .w_DECTOT=0
      .w_TIPOLN=space(1)
      .w_ONUME=0
      .w_CODART=space(20)
      .w_CODART1=space(20)
      .w_GRUMER=space(5)
      .w_tipocon=space(1)
      .w_cliente=space(15)
      .w_catcomm=space(3)
      .w_desccat=space(35)
      .w_zona=space(3)
      .w_desczona=space(35)
      .w_pagam=space(5)
      .w_CODMAR=space(5)
      .w_CODFAM=space(5)
      .w_CODCAT=space(5)
      .w_descpaga=space(30)
      .w_desccli=space(40)
      .w_stval=space(10)
      .w_valu2=space(3)
      .w_CAMBIO2=0
      .w_doctra=space(30)
      .w_Fatture=space(10)
      .w_notecred=space(10)
      .w_descvalu2=space(35)
      .w_CAOVAL2=0
      .w_TIPO=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_Client=space(1)
      .w_codtra=space(2)
      .w_codfa=space(2)
      .w_codnot=space(2)
      .w_DESART=space(40)
      .w_GDESC=space(35)
      .w_CALCPICT=space(1)
      .w_OBTEST1=ctod("  /  /  ")
      .w_SIMVAL=space(5)
      .w_DESMAR=space(35)
      .w_DESFAM=space(35)
      .w_DESCAT=space(35)
      .w_DESART1=space(40)
      .w_STALOG=space(1)
          .DoRTCalc(1,1,.f.)
        .w_Adata = i_datsys
          .DoRTCalc(3,3,.f.)
        .w_DATREG1 = i_DATSYS
        .w_stato = 'N'
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(7,11,.f.)
        if not(empty(.w_CODART))
          .link_1_14('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CODART1))
          .link_1_15('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_GRUMER))
          .link_1_16('Full')
        endif
        .w_tipocon = 'C'
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_cliente))
          .link_1_19('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_catcomm))
          .link_1_21('Full')
        endif
        .DoRTCalc(17,18,.f.)
        if not(empty(.w_zona))
          .link_1_24('Full')
        endif
        .DoRTCalc(19,20,.f.)
        if not(empty(.w_pagam))
          .link_1_27('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_CODMAR))
          .link_1_28('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_CODFAM))
          .link_1_29('Full')
        endif
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_CODCAT))
          .link_1_30('Full')
        endif
          .DoRTCalc(24,25,.f.)
        .w_stval = 'C'
        .w_valu2 = iif(.w_STVAL='C',g_PERVAL,'')
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_valu2))
          .link_1_39('Full')
        endif
        .w_CAMBIO2 = GETCAM(.w_valu2, .w_Adata, 7)
        .w_doctra = 'S'
        .w_Fatture = 'S'
        .w_notecred = 'S'
          .DoRTCalc(32,34,.f.)
        .w_OBTEST = i_INIDAT
      .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
          .DoRTCalc(36,36,.f.)
        .w_Client = 'F'
        .w_codtra = IIF(.w_doctra='S','DT',' ')
        .w_codfa = IIF(.w_Fatture='S','FA',' ')
        .w_codnot = IIF(.w_notecred='S','NC',' ')
          .DoRTCalc(41,42,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_OBTEST1 = i_DATSYS
        .w_SIMVAL = IIF(EMPTY(.w_VALU2),' ',.w_SIMVAL)
          .DoRTCalc(46,49,.f.)
        .w_STALOG = ' '
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_55.enabled = this.oPgFrm.Page1.oPag.oBtn_1_55.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,26,.t.)
        if .o_stval<>.w_stval
            .w_valu2 = iif(.w_STVAL='C',g_PERVAL,'')
          .link_1_39('Full')
        endif
        if .o_valu2<>.w_valu2
            .w_CAMBIO2 = GETCAM(.w_valu2, .w_Adata, 7)
        endif
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .DoRTCalc(29,37,.t.)
            .w_codtra = IIF(.w_doctra='S','DT',' ')
            .w_codfa = IIF(.w_Fatture='S','FA',' ')
            .w_codnot = IIF(.w_notecred='S','NC',' ')
        .DoRTCalc(41,42,.t.)
        if .o_valu2<>.w_valu2
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(44,44,.t.)
        if .o_valu2<>.w_valu2
            .w_SIMVAL = IIF(EMPTY(.w_VALU2),' ',.w_SIMVAL)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(46,50,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.ovalu2_1_39.enabled = this.oPgFrm.Page1.oPag.ovalu2_1_39.mCond()
    this.oPgFrm.Page1.oPag.oCAMBIO2_1_41.enabled = this.oPgFrm.Page1.oPag.oCAMBIO2_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODART
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_14'),i_cWhere,'',"Articoli/servizi",'ARTZOOM1.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_CODART1)) OR  (upper(.w_CODART)<=upper(.w_CODART1))) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART1
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART1)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART1))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART1)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODART1) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART1_1_15'),i_cWhere,'',"Articoli/servizi",'ARTZOOM1.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART1)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART1 = NVL(_Link_.ARCODART,space(20))
      this.w_DESART1 = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODART1 = space(20)
      endif
      this.w_DESART1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((upper(.w_CODART1)>= upper(.w_CODART)) or (empty(.w_CODART1))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_CODART1 = space(20)
        this.w_DESART1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMER
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMER_1_16'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_GDESC = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMER = space(5)
      endif
      this.w_GDESC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=cliente
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_cliente) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_afr',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_cliente)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_Client);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_Client;
                     ,'ANCODICE',trim(this.w_cliente))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_cliente)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_cliente)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_Client);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_cliente)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_Client);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_cliente) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'ocliente_1_19'),i_cWhere,'gsar_afr',"Fornitori",'GSAR_KMS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_Client<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice fornitore � inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_Client);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_cliente)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_cliente);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_Client);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_Client;
                       ,'ANCODICE',this.w_cliente)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_cliente = NVL(_Link_.ANCODICE,space(15))
      this.w_desccli = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_cliente = space(15)
      endif
      this.w_desccli = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice fornitore � inesistente oppure obsoleto")
        endif
        this.w_cliente = space(15)
        this.w_desccli = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_cliente Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=catcomm
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_catcomm) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_catcomm)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_catcomm))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_catcomm)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_catcomm) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'ocatcomm_1_21'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_catcomm)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_catcomm);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_catcomm)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_catcomm = NVL(_Link_.CTCODICE,space(3))
      this.w_desccat = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_catcomm = space(3)
      endif
      this.w_desccat = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_catcomm Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=zona
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_zona) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_zona)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_zona))
          select ZOCODZON,ZODTOBSO,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_zona)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_zona) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'ozona_1_24'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODTOBSO,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_zona)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_zona);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_zona)
            select ZOCODZON,ZODTOBSO,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_zona = NVL(_Link_.ZOCODZON,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
      this.w_desczona = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_zona = space(3)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_desczona = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endif
        this.w_zona = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_desczona = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_zona Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=pagam
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_pagam) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_pagam)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_pagam))
          select PACODICE,PADTOBSO,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_pagam)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_pagam) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'opagam_1_27'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADTOBSO,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_pagam)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_pagam);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_pagam)
            select PACODICE,PADTOBSO,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_pagam = NVL(_Link_.PACODICE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
      this.w_descpaga = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_pagam = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_descpaga = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_pagam = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_descpaga = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_pagam Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAR
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_CODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oCODMAR_1_28'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_CODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_CODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAR = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_1_29'),i_cWhere,'GSAR_AFA',"Famiglie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCAT_1_30'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=valu2
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_valu2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_valu2)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_valu2))
          select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_valu2)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_valu2) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'ovalu2_1_39'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_valu2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_valu2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_valu2)
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_valu2 = NVL(_Link_.VACODVAL,space(3))
      this.w_descvalu2 = NVL(_Link_.VADESVAL,space(35))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_CAOVAL2 = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_valu2 = space(3)
      endif
      this.w_descvalu2 = space(35)
      this.w_DECTOT = 0
      this.w_CAOVAL2 = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_valu2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDadata_1_1.value==this.w_Dadata)
      this.oPgFrm.Page1.oPag.oDadata_1_1.value=this.w_Dadata
    endif
    if not(this.oPgFrm.Page1.oPag.oAdata_1_3.value==this.w_Adata)
      this.oPgFrm.Page1.oPag.oAdata_1_3.value=this.w_Adata
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG_1_4.value==this.w_DATREG)
      this.oPgFrm.Page1.oPag.oDATREG_1_4.value=this.w_DATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG1_1_5.value==this.w_DATREG1)
      this.oPgFrm.Page1.oPag.oDATREG1_1_5.value=this.w_DATREG1
    endif
    if not(this.oPgFrm.Page1.oPag.ostato_1_7.RadioValue()==this.w_stato)
      this.oPgFrm.Page1.oPag.ostato_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_14.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_14.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART1_1_15.value==this.w_CODART1)
      this.oPgFrm.Page1.oPag.oCODART1_1_15.value=this.w_CODART1
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUMER_1_16.value==this.w_GRUMER)
      this.oPgFrm.Page1.oPag.oGRUMER_1_16.value=this.w_GRUMER
    endif
    if not(this.oPgFrm.Page1.oPag.ocliente_1_19.value==this.w_cliente)
      this.oPgFrm.Page1.oPag.ocliente_1_19.value=this.w_cliente
    endif
    if not(this.oPgFrm.Page1.oPag.ocatcomm_1_21.value==this.w_catcomm)
      this.oPgFrm.Page1.oPag.ocatcomm_1_21.value=this.w_catcomm
    endif
    if not(this.oPgFrm.Page1.oPag.odesccat_1_22.value==this.w_desccat)
      this.oPgFrm.Page1.oPag.odesccat_1_22.value=this.w_desccat
    endif
    if not(this.oPgFrm.Page1.oPag.ozona_1_24.value==this.w_zona)
      this.oPgFrm.Page1.oPag.ozona_1_24.value=this.w_zona
    endif
    if not(this.oPgFrm.Page1.oPag.odesczona_1_25.value==this.w_desczona)
      this.oPgFrm.Page1.oPag.odesczona_1_25.value=this.w_desczona
    endif
    if not(this.oPgFrm.Page1.oPag.opagam_1_27.value==this.w_pagam)
      this.oPgFrm.Page1.oPag.opagam_1_27.value=this.w_pagam
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAR_1_28.value==this.w_CODMAR)
      this.oPgFrm.Page1.oPag.oCODMAR_1_28.value=this.w_CODMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAM_1_29.value==this.w_CODFAM)
      this.oPgFrm.Page1.oPag.oCODFAM_1_29.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAT_1_30.value==this.w_CODCAT)
      this.oPgFrm.Page1.oPag.oCODCAT_1_30.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.odescpaga_1_31.value==this.w_descpaga)
      this.oPgFrm.Page1.oPag.odescpaga_1_31.value=this.w_descpaga
    endif
    if not(this.oPgFrm.Page1.oPag.odesccli_1_32.value==this.w_desccli)
      this.oPgFrm.Page1.oPag.odesccli_1_32.value=this.w_desccli
    endif
    if not(this.oPgFrm.Page1.oPag.ostval_1_37.RadioValue()==this.w_stval)
      this.oPgFrm.Page1.oPag.ostval_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ovalu2_1_39.value==this.w_valu2)
      this.oPgFrm.Page1.oPag.ovalu2_1_39.value=this.w_valu2
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMBIO2_1_41.value==this.w_CAMBIO2)
      this.oPgFrm.Page1.oPag.oCAMBIO2_1_41.value=this.w_CAMBIO2
    endif
    if not(this.oPgFrm.Page1.oPag.odoctra_1_43.RadioValue()==this.w_doctra)
      this.oPgFrm.Page1.oPag.odoctra_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFatture_1_44.RadioValue()==this.w_Fatture)
      this.oPgFrm.Page1.oPag.oFatture_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.onotecred_1_45.RadioValue()==this.w_notecred)
      this.oPgFrm.Page1.oPag.onotecred_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_59.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_59.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oGDESC_1_60.value==this.w_GDESC)
      this.oPgFrm.Page1.oPag.oGDESC_1_60.value=this.w_GDESC
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_65.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_65.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAR_1_66.value==this.w_DESMAR)
      this.oPgFrm.Page1.oPag.oDESMAR_1_66.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_68.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_68.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_70.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_70.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART1_1_72.value==this.w_DESART1)
      this.oPgFrm.Page1.oPag.oDESART1_1_72.value=this.w_DESART1
    endif
    if not(this.oPgFrm.Page1.oPag.oSTALOG_1_76.RadioValue()==this.w_STALOG)
      this.oPgFrm.Page1.oPag.oSTALOG_1_76.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DADATA<=.w_ADATA OR EMPTY(.w_ADATA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDadata_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_Adata)) or not(.w_DADATA<=.w_ADATA OR EMPTY(.w_ADATA)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAdata_1_3.SetFocus()
            i_bnoObbl = !empty(.w_Adata)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_DATREG<=.w_DATREG1 OR EMPTY(.w_DATREG1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATREG_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_DATREG<=.w_DATREG1 OR EMPTY(.w_DATREG1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATREG1_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(((empty(.w_CODART1)) OR  (upper(.w_CODART)<=upper(.w_CODART1))) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((upper(.w_CODART1)>= upper(.w_CODART)) or (empty(.w_CODART1))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODART1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART1_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(empty(.w_cliente))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ocliente_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice fornitore � inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_zona))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ozona_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_pagam))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.opagam_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   (empty(.w_valu2))  and (.w_stval='A')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ovalu2_1_39.SetFocus()
            i_bnoObbl = !empty(.w_valu2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMBIO2))  and (.w_CAOVAL2=0 AND .w_VALU2<>' ')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMBIO2_1_41.SetFocus()
            i_bnoObbl = !empty(.w_CAMBIO2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cambio di riferimento")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_stval = this.w_stval
    this.o_valu2 = this.w_valu2
    return

enddefine

* --- Define pages as container
define class tgsac_spaPag1 as StdContainer
  Width  = 634
  height = 481
  stdWidth  = 634
  stdheight = 481
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDadata_1_1 as StdField with uid="KSMMEHAABE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_Dadata", cQueryName = "Dadata",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data di inizio dei documenti di acquisto da considerare",;
    HelpContextID = 264421834,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=117, Top=8

  func oDadata_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DADATA<=.w_ADATA OR EMPTY(.w_ADATA))
    endwith
    return bRes
  endfunc

  add object oAdata_1_3 as StdField with uid="TMLQZZAHGL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_Adata", cQueryName = "Adata",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data di termine dei documenti di acquisto da considerare",;
    HelpContextID = 31452922,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=117, Top=33

  func oAdata_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DADATA<=.w_ADATA OR EMPTY(.w_ADATA))
    endwith
    return bRes
  endfunc

  add object oDATREG_1_4 as StdField with uid="JYJRINEYLF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATREG", cQueryName = "DATREG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di inizio dei documenti di acquisto da considerare",;
    HelpContextID = 214098378,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=307, Top=8

  func oDATREG_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATREG<=.w_DATREG1 OR EMPTY(.w_DATREG1))
    endwith
    return bRes
  endfunc

  add object oDATREG1_1_5 as StdField with uid="EHKVWGRREU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATREG1", cQueryName = "DATREG1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di termine dei documenti di acquisto da considerare",;
    HelpContextID = 214098378,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=306, Top=33

  func oDATREG1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATREG<=.w_DATREG1 OR EMPTY(.w_DATREG1))
    endwith
    return bRes
  endfunc


  add object ostato_1_7 as StdCombo with uid="XUBDARSQDQ",value=3,rtseq=5,rtrep=.f.,left=500,top=8,width=110,height=21;
    , ToolTipText = "Permette di selezionare tutti i documenti, solo quelli provvisori o confermati";
    , HelpContextID = 16767962;
    , cFormVar="w_stato",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func ostato_1_7.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(20)))))
  endfunc
  func ostato_1_7.GetRadio()
    this.Parent.oContained.w_stato = this.RadioValue()
    return .t.
  endfunc

  func ostato_1_7.SetRadio()
    this.Parent.oContained.w_stato=trim(this.Parent.oContained.w_stato)
    this.value = ;
      iif(this.Parent.oContained.w_stato=='N',1,;
      iif(this.Parent.oContained.w_stato=='S',2,;
      iif(this.Parent.oContained.w_stato=='',3,;
      0)))
  endfunc

  add object oCODART_1_14 as StdField with uid="GDOWHDUCOT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice primo articolo selezionato",;
    HelpContextID = 251974618,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=85, Top=60, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli/servizi",'ARTZOOM1.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oCODART1_1_15 as StdField with uid="MCOKVXTUTG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODART1", cQueryName = "CODART1",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Codice secondo articolo selezionato",;
    HelpContextID = 251974618,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=85, Top=84, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART1"

  func oCODART1_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART1_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART1_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART1_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli/servizi",'ARTZOOM1.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oGRUMER_1_16 as StdField with uid="GMEHYOKZIG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_GRUMER", cQueryName = "GRUMER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico selezionato",;
    HelpContextID = 29868186,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=85, Top=108, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMER"

  func oGRUMER_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMER_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMER_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMER_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oGRUMER_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUMER
     i_obj.ecpSave()
  endproc

  add object ocliente_1_19 as StdField with uid="DNZWFJCDJL",rtseq=15,rtrep=.f.,;
    cFormVar = "w_cliente", cQueryName = "cliente",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice fornitore � inesistente oppure obsoleto",;
    ToolTipText = "Codice fornitore selezionato",;
    HelpContextID = 48339750,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=85, Top=132, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="gsar_afr", oKey_1_1="ANTIPCON", oKey_1_2="this.w_Client", oKey_2_1="ANCODICE", oKey_2_2="this.w_cliente"

  func ocliente_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc ocliente_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ocliente_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_Client)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_Client)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'ocliente_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_afr',"Fornitori",'GSAR_KMS.CONTI_VZM',this.parent.oContained
  endproc
  proc ocliente_1_19.mZoomOnZoom
    local i_obj
    i_obj=gsar_afr()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_Client
     i_obj.w_ANCODICE=this.parent.oContained.w_cliente
     i_obj.ecpSave()
  endproc

  add object ocatcomm_1_21 as StdField with uid="GDBNRJKCKA",rtseq=16,rtrep=.f.,;
    cFormVar = "w_catcomm", cQueryName = "catcomm",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale selezionata",;
    HelpContextID = 68141018,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=85, Top=156, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_catcomm"

  func ocatcomm_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc ocatcomm_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ocatcomm_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'ocatcomm_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc ocatcomm_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_catcomm
     i_obj.ecpSave()
  endproc

  add object odesccat_1_22 as StdField with uid="JBPIMKEVPW",rtseq=17,rtrep=.f.,;
    cFormVar = "w_desccat", cQueryName = "desccat",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 254817334,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=249, Top=156, InputMask=replicate('X',35)

  add object ozona_1_24 as StdField with uid="ZTIAWYAUWV",rtseq=18,rtrep=.f.,;
    cFormVar = "w_zona", cQueryName = "zona",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleto",;
    ToolTipText = "Zona selezionata",;
    HelpContextID = 134353002,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=85, Top=180, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_zona"

  func ozona_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc ozona_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ozona_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'ozona_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc ozona_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_zona
     i_obj.ecpSave()
  endproc

  add object odesczona_1_25 as StdField with uid="OHOKBHSDYQ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_desczona", cQueryName = "desczona",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 23055209,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=249, Top=180, InputMask=replicate('X',35)

  add object opagam_1_27 as StdField with uid="TGIYBOLWES",rtseq=20,rtrep=.f.,;
    cFormVar = "w_pagam", cQueryName = "pagam",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice pagamento selezionato",;
    HelpContextID = 20090634,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=85, Top=204, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_pagam"

  func opagam_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc opagam_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc opagam_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'opagam_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc opagam_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_pagam
     i_obj.ecpSave()
  endproc

  add object oCODMAR_1_28 as StdField with uid="QTGZNWTPFF",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CODMAR", cQueryName = "CODMAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice marchio selezionato",;
    HelpContextID = 34132954,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=85, Top=228, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_CODMAR"

  func oCODMAR_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAR_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAR_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oCODMAR_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oCODMAR_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_CODMAR
     i_obj.ecpSave()
  endproc

  add object oCODFAM_1_29 as StdField with uid="CWDFKEHJOA",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia selezionato",;
    HelpContextID = 118477786,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=85, Top=252, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglie",'',this.parent.oContained
  endproc
  proc oCODFAM_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CODFAM
     i_obj.ecpSave()
  endproc

  add object oCODCAT_1_30 as StdField with uid="MYVYIBQQAH",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria omogenea selezionata",;
    HelpContextID = 1233882,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=85, Top=276, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAT_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCAT_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCODCAT_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CODCAT
     i_obj.ecpSave()
  endproc

  add object odescpaga_1_31 as StdField with uid="CIURVSUYUU",rtseq=24,rtrep=.f.,;
    cFormVar = "w_descpaga", cQueryName = "descpaga",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 13463,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=249, Top=204, InputMask=replicate('X',30)

  add object odesccli_1_32 as StdField with uid="TWSXVINGFB",rtseq=25,rtrep=.f.,;
    cFormVar = "w_desccli", cQueryName = "desccli",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 97504202,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=249, Top=132, InputMask=replicate('X',40)


  add object ostval_1_37 as StdCombo with uid="VIZQGRQXYY",rtseq=26,rtrep=.f.,left=104,top=326,width=113,height=21;
    , ToolTipText = "Valuta di valorizzazione dei totali sulla stampa";
    , HelpContextID = 21072858;
    , cFormVar="w_stval",RowSource=""+"Valuta di conto,"+"Altra valuta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func ostval_1_37.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    space(10))))
  endfunc
  func ostval_1_37.GetRadio()
    this.Parent.oContained.w_stval = this.RadioValue()
    return .t.
  endfunc

  func ostval_1_37.SetRadio()
    this.Parent.oContained.w_stval=trim(this.Parent.oContained.w_stval)
    this.value = ;
      iif(this.Parent.oContained.w_stval=='C',1,;
      iif(this.Parent.oContained.w_stval=='A',2,;
      0))
  endfunc

  add object ovalu2_1_39 as StdField with uid="DWHGNCVMZI",rtseq=27,rtrep=.f.,;
    cFormVar = "w_valu2", cQueryName = "valu2",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 80625322,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=266, Top=326, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_valu2"

  func ovalu2_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_stval='A')
    endwith
   endif
  endfunc

  func ovalu2_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc ovalu2_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ovalu2_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'ovalu2_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc ovalu2_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_valu2
     i_obj.ecpSave()
  endproc

  add object oCAMBIO2_1_41 as StdField with uid="JFHQPTZNMO",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CAMBIO2", cQueryName = "CAMBIO2",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cambio di riferimento",;
    ToolTipText = "Cambio della valuta selezionata",;
    HelpContextID = 76763610,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=508, Top=326

  func oCAMBIO2_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL2=0 AND .w_VALU2<>' ')
    endwith
   endif
  endfunc

  add object odoctra_1_43 as StdCheck with uid="CWJIJWLNNE",rtseq=29,rtrep=.f.,left=12, top=374, caption="Documenti trasporto",;
    ToolTipText = "Se attivo seleziona nel report i documenti di trasporto",;
    HelpContextID = 265273802,;
    cFormVar="w_doctra", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func odoctra_1_43.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func odoctra_1_43.GetRadio()
    this.Parent.oContained.w_doctra = this.RadioValue()
    return .t.
  endfunc

  func odoctra_1_43.SetRadio()
    this.Parent.oContained.w_doctra=trim(this.Parent.oContained.w_doctra)
    this.value = ;
      iif(this.Parent.oContained.w_doctra=='S',1,;
      0)
  endfunc

  add object oFatture_1_44 as StdCheck with uid="IJEBOCKAGE",rtseq=30,rtrep=.f.,left=235, top=374, caption="Fatture",;
    ToolTipText = "Se attivo seleziona nel report le fatture",;
    HelpContextID = 23150166,;
    cFormVar="w_Fatture", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFatture_1_44.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFatture_1_44.GetRadio()
    this.Parent.oContained.w_Fatture = this.RadioValue()
    return .t.
  endfunc

  func oFatture_1_44.SetRadio()
    this.Parent.oContained.w_Fatture=trim(this.Parent.oContained.w_Fatture)
    this.value = ;
      iif(this.Parent.oContained.w_Fatture=='S',1,;
      0)
  endfunc

  add object onotecred_1_45 as StdCheck with uid="ZZJNWRIAOZ",rtseq=31,rtrep=.f.,left=423, top=374, caption="Note di credito",;
    ToolTipText = "Se attivo seleziona nel report le note di credito",;
    HelpContextID = 3297082,;
    cFormVar="w_notecred", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func onotecred_1_45.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func onotecred_1_45.GetRadio()
    this.Parent.oContained.w_notecred = this.RadioValue()
    return .t.
  endfunc

  func onotecred_1_45.SetRadio()
    this.Parent.oContained.w_notecred=trim(this.Parent.oContained.w_notecred)
    this.value = ;
      iif(this.Parent.oContained.w_notecred=='S',1,;
      0)
  endfunc


  add object oObj_1_52 as cp_outputCombo with uid="ZLERXIVPWT",left=149, top=402, width=462,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 36806630


  add object oBtn_1_53 as StdButton with uid="XAOFRYAIID",left=514, top=432, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 267836890;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_53.Click()
      with this.Parent.oContained
        do GSAC_BPV with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_53.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ODES))
      endwith
    endif
  endfunc


  add object oBtn_1_55 as StdButton with uid="KYQESPJULE",left=564, top=432, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 133873594;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_55.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESART_1_59 as StdField with uid="XTEYJOVCSQ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 251915722,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=249, Top=60, InputMask=replicate('X',40)

  add object oGDESC_1_60 as StdField with uid="OXDFLLORMT",rtseq=42,rtrep=.f.,;
    cFormVar = "w_GDESC", cQueryName = "GDESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 65195674,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=249, Top=108, InputMask=replicate('X',35)

  add object oSIMVAL_1_65 as StdField with uid="XAXMFHLJBQ",rtseq=45,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 134170842,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=315, Top=326, InputMask=replicate('X',5)

  add object oDESMAR_1_66 as StdField with uid="RPJKCKDSAT",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 34074058,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=249, Top=228, InputMask=replicate('X',35)

  add object oDESFAM_1_68 as StdField with uid="JLRZMEZPGL",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 118418890,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=249, Top=252, InputMask=replicate('X',35)

  add object oDESCAT_1_70 as StdField with uid="FQORJMRCKA",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1174986,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=249, Top=276, InputMask=replicate('X',35)

  add object oDESART1_1_72 as StdField with uid="OZGFFDWPTJ",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESART1", cQueryName = "DESART1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 251915722,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=249, Top=84, InputMask=replicate('X',40)

  add object oSTALOG_1_76 as StdCheck with uid="OUGSGYQVAE",rtseq=50,rtrep=.f.,left=141, top=432, caption="Log N.C. slegate",;
    ToolTipText = "Se attivo stampa il log delle note di credito slegate dal ciclo documentale",;
    HelpContextID = 204078554,;
    cFormVar="w_STALOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTALOG_1_76.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSTALOG_1_76.GetRadio()
    this.Parent.oContained.w_STALOG = this.RadioValue()
    return .t.
  endfunc

  func oSTALOG_1_76.SetRadio()
    this.Parent.oContained.w_STALOG=trim(this.Parent.oContained.w_STALOG)
    this.value = ;
      iif(this.Parent.oContained.w_STALOG=='S',1,;
      0)
  endfunc

  add object oStr_1_2 as StdString with uid="IOVTSCSLRF",Visible=.t., Left=27, Top=8,;
    Alignment=1, Width=87, Height=18,;
    Caption="Da data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="ICFOIHRQGF",Visible=.t., Left=42, Top=34,;
    Alignment=1, Width=72, Height=18,;
    Caption="A data doc:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="DYGLJWSYXZ",Visible=.t., Left=10, Top=402,;
    Alignment=1, Width=137, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="TMMKBBPYIZ",Visible=.t., Left=450, Top=8,;
    Alignment=1, Width=44, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="KHFYRTFHAU",Visible=.t., Left=14, Top=156,;
    Alignment=1, Width=67, Height=15,;
    Caption="Cat. comm.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="RUOPKHGJCK",Visible=.t., Left=38, Top=180,;
    Alignment=1, Width=43, Height=15,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="ZWRRXYXROA",Visible=.t., Left=7, Top=204,;
    Alignment=1, Width=74, Height=15,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="GLXRFLHNHN",Visible=.t., Left=20, Top=132,;
    Alignment=1, Width=61, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="SXOGKGKNEX",Visible=.t., Left=13, Top=300,;
    Alignment=0, Width=598, Height=15,;
    Caption="Valuta totali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="QKUPPYRXXA",Visible=.t., Left=8, Top=326,;
    Alignment=1, Width=92, Height=15,;
    Caption="Stampa in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="KHAUGDCEMY",Visible=.t., Left=216, Top=326,;
    Alignment=1, Width=49, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="PZHPGKGVAV",Visible=.t., Left=401, Top=326,;
    Alignment=1, Width=104, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="LGMZUMOWCU",Visible=.t., Left=13, Top=349,;
    Alignment=0, Width=595, Height=15,;
    Caption="Categorie documenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_61 as StdString with uid="SNFXLXSTPF",Visible=.t., Left=19, Top=58,;
    Alignment=1, Width=62, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="XYUGNJUKOO",Visible=.t., Left=21, Top=108,;
    Alignment=0, Width=60, Height=18,;
    Caption="Gru. Merc."  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="SGANFAHXYX",Visible=.t., Left=35, Top=228,;
    Alignment=1, Width=46, Height=18,;
    Caption="Marchio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="YSPSLACQKA",Visible=.t., Left=30, Top=252,;
    Alignment=1, Width=51, Height=18,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="DNDNIDHZNO",Visible=.t., Left=11, Top=276,;
    Alignment=1, Width=70, Height=18,;
    Caption="Cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="ETMDTRPPPZ",Visible=.t., Left=28, Top=84,;
    Alignment=1, Width=53, Height=18,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="BADNRNXKYG",Visible=.t., Left=204, Top=8,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="BTGFHRFVTX",Visible=.t., Left=228, Top=33,;
    Alignment=1, Width=75, Height=18,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oBox_1_34 as StdBox with uid="ILBOQDWCSU",left=13, top=318, width=598,height=1

  add object oBox_1_46 as StdBox with uid="YVRWSVEZJN",left=13, top=368, width=598,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac_spa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
