* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_sav                                                        *
*              Stampa costo del venduto                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_77]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-28                                                      *
* Last revis.: 2014-06-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_sav",oParentObject))

* --- Class definition
define class tgsma_sav as StdForm
  Top    = 22
  Left   = 35

  * --- Standard Properties
  Width  = 580
  Height = 321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-06-09"
  HelpContextID=124418921
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  CONTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  INVENTAR_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsma_sav"
  cComment = "Stampa costo del venduto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ESEINI = space(4)
  w_INVINI = space(6)
  w_DESC4 = ctod('  /  /  ')
  w_VALORIZ1 = space(1)
  w_VALORIZ3 = space(1)
  w_ESEFIN = space(4)
  w_INVFIN = space(6)
  w_VALORIZ2 = space(1)
  w_DESC5 = ctod('  /  /  ')
  w_CATOMO = space(5)
  w_DESC1 = space(35)
  w_GRUMER = space(5)
  w_DESC2 = space(35)
  w_CODFAM = space(5)
  w_DESC3 = space(35)
  w_DATESI = ctod('  /  /  ')
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_CODVAL2 = space(3)
  w_DECUNI = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_savPag1","gsma_sav",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVALORIZ1_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='FAM_ARTI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='CATEGOMO'
    this.cWorkTables[6]='MARCHI'
    this.cWorkTables[7]='INVENTAR'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='VALUTE'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_ESEINI=space(4)
      .w_INVINI=space(6)
      .w_DESC4=ctod("  /  /  ")
      .w_VALORIZ1=space(1)
      .w_VALORIZ3=space(1)
      .w_ESEFIN=space(4)
      .w_INVFIN=space(6)
      .w_VALORIZ2=space(1)
      .w_DESC5=ctod("  /  /  ")
      .w_CATOMO=space(5)
      .w_DESC1=space(35)
      .w_GRUMER=space(5)
      .w_DESC2=space(35)
      .w_CODFAM=space(5)
      .w_DESC3=space(35)
      .w_DATESI=ctod("  /  /  ")
      .w_CODVAL=space(3)
      .w_CAOVAL=0
      .w_CODVAL2=space(3)
      .w_DECUNI=0
        .w_AZIENDA = i_codazi
          .DoRTCalc(2,3,.f.)
        .w_ESEINI = g_CODESE
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ESEINI))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_INVINI))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_VALORIZ1 = 'A'
        .w_VALORIZ3 = 'B'
        .w_ESEFIN = g_codese
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_ESEFIN))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_INVFIN))
          .link_1_10('Full')
        endif
        .w_VALORIZ2 = 'B'
          .DoRTCalc(12,12,.f.)
        .w_CATOMO = ''
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CATOMO))
          .link_1_13('Full')
        endif
          .DoRTCalc(14,14,.f.)
        .w_GRUMER = ''
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_GRUMER))
          .link_1_15('Full')
        endif
          .DoRTCalc(16,16,.f.)
        .w_CODFAM = ''
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODFAM))
          .link_1_17('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(18,20,.f.)
        if not(empty(.w_CODVAL))
          .link_1_39('Full')
        endif
        .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS)
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_CODVAL2))
          .link_1_41('Full')
        endif
    endwith
    this.DoRTCalc(23,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_1_4('Full')
          .link_1_5('Full')
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(6,19,.t.)
          .link_1_39('Full')
        .DoRTCalc(21,21,.t.)
          .link_1_41('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ESEINI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESEINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESEINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESEINI);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_ESEINI)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESEINI = NVL(_Link_.ESCODESE,space(4))
      this.w_CODVAL = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESEINI = space(4)
      endif
      this.w_CODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESEINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INVINI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INVINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INVINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_INVINI);
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESEINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_ESEINI;
                       ,'INNUMINV',this.w_INVINI)
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INVINI = NVL(_Link_.INNUMINV,space(6))
      this.w_DESC4 = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_INVINI = space(6)
      endif
      this.w_DESC4 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INVINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESEFIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESEFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESEFIN)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_ESEFIN))
          select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESEFIN)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESEFIN) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESEFIN_1_9'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESEFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESEFIN);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_ESEFIN)
            select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESEFIN = NVL(_Link_.ESCODESE,space(4))
      this.w_DATESI = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_CODVAL2 = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESEFIN = space(4)
      endif
      this.w_DATESI = ctod("  /  /  ")
      this.w_CODVAL2 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESEFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INVFIN
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INVFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsma_ain',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_INVFIN)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESEFIN);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_ESEFIN;
                     ,'INNUMINV',trim(this.w_INVFIN))
          select INCODESE,INNUMINV,INDATINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INVFIN)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INVFIN) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oINVFIN_1_10'),i_cWhere,'gsma_ain',"Inventario",'GSMAINST.INVENTAR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ESEFIN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_ESEFIN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INVFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_INVFIN);
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESEFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_ESEFIN;
                       ,'INNUMINV',this.w_INVFIN)
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INVFIN = NVL(_Link_.INNUMINV,space(6))
      this.w_DESC5 = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_INVFIN = space(6)
      endif
      this.w_DESC5 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INVFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATOMO
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATOMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATOMO)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATOMO))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATOMO)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATOMO) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATOMO_1_13'),i_cWhere,'GSAR_AOM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATOMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATOMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATOMO)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATOMO = NVL(_Link_.OMCODICE,space(5))
      this.W_DESC1 = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATOMO = space(5)
      endif
      this.W_DESC1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATOMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMER
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMER_1_15'),i_cWhere,'GSAR_AGM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMER = NVL(_Link_.GMCODICE,space(5))
      this.W_DESC2 = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMER = space(5)
      endif
      this.W_DESC2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_1_17'),i_cWhere,'GSAR_AFA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(5))
      this.W_DESC3 = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(5)
      endif
      this.W_DESC3 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL2
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL2)
            select VACODVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL2 = NVL(_Link_.VACODVAL,space(3))
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL2 = space(3)
      endif
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oESEINI_1_4.value==this.w_ESEINI)
      this.oPgFrm.Page1.oPag.oESEINI_1_4.value=this.w_ESEINI
    endif
    if not(this.oPgFrm.Page1.oPag.oINVINI_1_5.value==this.w_INVINI)
      this.oPgFrm.Page1.oPag.oINVINI_1_5.value=this.w_INVINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC4_1_6.value==this.w_DESC4)
      this.oPgFrm.Page1.oPag.oDESC4_1_6.value=this.w_DESC4
    endif
    if not(this.oPgFrm.Page1.oPag.oVALORIZ1_1_7.RadioValue()==this.w_VALORIZ1)
      this.oPgFrm.Page1.oPag.oVALORIZ1_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALORIZ3_1_8.RadioValue()==this.w_VALORIZ3)
      this.oPgFrm.Page1.oPag.oVALORIZ3_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESEFIN_1_9.value==this.w_ESEFIN)
      this.oPgFrm.Page1.oPag.oESEFIN_1_9.value=this.w_ESEFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oINVFIN_1_10.value==this.w_INVFIN)
      this.oPgFrm.Page1.oPag.oINVFIN_1_10.value=this.w_INVFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oVALORIZ2_1_11.RadioValue()==this.w_VALORIZ2)
      this.oPgFrm.Page1.oPag.oVALORIZ2_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC5_1_12.value==this.w_DESC5)
      this.oPgFrm.Page1.oPag.oDESC5_1_12.value=this.w_DESC5
    endif
    if not(this.oPgFrm.Page1.oPag.oCATOMO_1_13.value==this.w_CATOMO)
      this.oPgFrm.Page1.oPag.oCATOMO_1_13.value=this.w_CATOMO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC1_1_14.value==this.w_DESC1)
      this.oPgFrm.Page1.oPag.oDESC1_1_14.value=this.w_DESC1
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUMER_1_15.value==this.w_GRUMER)
      this.oPgFrm.Page1.oPag.oGRUMER_1_15.value=this.w_GRUMER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC2_1_16.value==this.w_DESC2)
      this.oPgFrm.Page1.oPag.oDESC2_1_16.value=this.w_DESC2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAM_1_17.value==this.w_CODFAM)
      this.oPgFrm.Page1.oPag.oCODFAM_1_17.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC3_1_21.value==this.w_DESC3)
      this.oPgFrm.Page1.oPag.oDESC3_1_21.value=this.w_DESC3
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsma_savPag1 as StdContainer
  Width  = 576
  height = 321
  stdWidth  = 576
  stdheight = 321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oESEINI_1_4 as StdField with uid="ZVJBEWDYNF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ESEINI", cQueryName = "ESEINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 113454150,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=63, Top=116, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESEINI"

  func oESEINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_INVINI)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oINVINI_1_5 as StdField with uid="LVMHCJVWOO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_INVINI", cQueryName = "INVINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero inventario",;
    HelpContextID = 113522566,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=170, Top=116, InputMask=replicate('X',6), cLinkFile="INVENTAR", cZoomOnZoom="gsma_ain", oKey_1_1="INCODESE", oKey_1_2="this.w_ESEINI", oKey_2_1="INNUMINV", oKey_2_2="this.w_INVINI"

  func oINVINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESC4_1_6 as StdField with uid="OGZGXLJXVH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESC4", cQueryName = "DESC4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 65143242,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=334, Top=116


  add object oVALORIZ1_1_7 as StdCombo with uid="EPRMSIXSMD",rtseq=7,rtrep=.f.,left=417,top=116,width=152,height=21;
    , ToolTipText = "Valorizzazione inizio esercizio";
    , HelpContextID = 150369401;
    , cFormVar="w_VALORIZ1",RowSource=""+"Costo medio pond. anno,"+"Costo medio pond. periodo,"+"Costo ultimo,"+"Costo standard ultimo,"+"LIFO continuo,"+"FIFO continuo,"+"LIFO scatti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVALORIZ1_1_7.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    space(1)))))))))
  endfunc
  func oVALORIZ1_1_7.GetRadio()
    this.Parent.oContained.w_VALORIZ1 = this.RadioValue()
    return .t.
  endfunc

  func oVALORIZ1_1_7.SetRadio()
    this.Parent.oContained.w_VALORIZ1=trim(this.Parent.oContained.w_VALORIZ1)
    this.value = ;
      iif(this.Parent.oContained.w_VALORIZ1=='A',1,;
      iif(this.Parent.oContained.w_VALORIZ1=='B',2,;
      iif(this.Parent.oContained.w_VALORIZ1=='C',3,;
      iif(this.Parent.oContained.w_VALORIZ1=='D',4,;
      iif(this.Parent.oContained.w_VALORIZ1=='E',5,;
      iif(this.Parent.oContained.w_VALORIZ1=='F',6,;
      iif(this.Parent.oContained.w_VALORIZ1=='G',7,;
      0)))))))
  endfunc


  add object oVALORIZ3_1_8 as StdCombo with uid="RVEIERDGMJ",rtseq=8,rtrep=.f.,left=417,top=69,width=152,height=21;
    , ToolTipText = "Valorizzazione acquisti del periodo";
    , HelpContextID = 150369399;
    , cFormVar="w_VALORIZ3",RowSource=""+"Costo medio pond. anno,"+"Costo medio pond. periodo,"+"Costo ultimo,"+"Costo standard ultimo,"+"LIFO continuo,"+"FIFO continuo,"+"LIFO scatti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVALORIZ3_1_8.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    space(1)))))))))
  endfunc
  func oVALORIZ3_1_8.GetRadio()
    this.Parent.oContained.w_VALORIZ3 = this.RadioValue()
    return .t.
  endfunc

  func oVALORIZ3_1_8.SetRadio()
    this.Parent.oContained.w_VALORIZ3=trim(this.Parent.oContained.w_VALORIZ3)
    this.value = ;
      iif(this.Parent.oContained.w_VALORIZ3=='A',1,;
      iif(this.Parent.oContained.w_VALORIZ3=='B',2,;
      iif(this.Parent.oContained.w_VALORIZ3=='C',3,;
      iif(this.Parent.oContained.w_VALORIZ3=='D',4,;
      iif(this.Parent.oContained.w_VALORIZ3=='E',5,;
      iif(this.Parent.oContained.w_VALORIZ3=='F',6,;
      iif(this.Parent.oContained.w_VALORIZ3=='G',7,;
      0)))))))
  endfunc

  add object oESEFIN_1_9 as StdField with uid="WWLVQIOYFM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ESEFIN", cQueryName = "ESEFIN",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 191900742,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=63, Top=36, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESEFIN"

  func oESEFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
      if .not. empty(.w_INVFIN)
        bRes2=.link_1_10('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oESEFIN_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESEFIN_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESEFIN_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oINVFIN_1_10 as StdField with uid="JEEFMSZDZY",rtseq=10,rtrep=.f.,;
    cFormVar = "w_INVFIN", cQueryName = "INVFIN",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    HelpContextID = 191969158,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=170, Top=36, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="gsma_ain", oKey_1_1="INCODESE", oKey_1_2="this.w_ESEFIN", oKey_2_1="INNUMINV", oKey_2_2="this.w_INVFIN"

  func oINVFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oINVFIN_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINVFIN_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_ESEFIN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_ESEFIN)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oINVFIN_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'gsma_ain',"Inventario",'GSMAINST.INVENTAR_VZM',this.parent.oContained
  endproc
  proc oINVFIN_1_10.mZoomOnZoom
    local i_obj
    i_obj=gsma_ain()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_ESEFIN
     i_obj.w_INNUMINV=this.parent.oContained.w_INVFIN
     i_obj.ecpSave()
  endproc


  add object oVALORIZ2_1_11 as StdCombo with uid="FRPDHMOXUH",rtseq=11,rtrep=.f.,left=417,top=36,width=152,height=21;
    , ToolTipText = "Valorizzazione fine esercizio";
    , HelpContextID = 150369400;
    , cFormVar="w_VALORIZ2",RowSource=""+"Costo medio pond. anno,"+"Costo medio pond. periodo,"+"Costo ultimo,"+"Costo standard ultimo,"+"LIFO continuo,"+"FIFO continuo,"+"LIFO scatti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVALORIZ2_1_11.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    space(1)))))))))
  endfunc
  func oVALORIZ2_1_11.GetRadio()
    this.Parent.oContained.w_VALORIZ2 = this.RadioValue()
    return .t.
  endfunc

  func oVALORIZ2_1_11.SetRadio()
    this.Parent.oContained.w_VALORIZ2=trim(this.Parent.oContained.w_VALORIZ2)
    this.value = ;
      iif(this.Parent.oContained.w_VALORIZ2=='A',1,;
      iif(this.Parent.oContained.w_VALORIZ2=='B',2,;
      iif(this.Parent.oContained.w_VALORIZ2=='C',3,;
      iif(this.Parent.oContained.w_VALORIZ2=='D',4,;
      iif(this.Parent.oContained.w_VALORIZ2=='E',5,;
      iif(this.Parent.oContained.w_VALORIZ2=='F',6,;
      iif(this.Parent.oContained.w_VALORIZ2=='G',7,;
      0)))))))
  endfunc

  add object oDESC5_1_12 as StdField with uid="MJNZDRJPXY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESC5", cQueryName = "DESC5",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 64094666,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=334, Top=36

  add object oCATOMO_1_13 as StdField with uid="FOMDZYGCEV",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CATOMO", cQueryName = "CATOMO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 213518886,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=118, Top=211, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATOMO"

  func oCATOMO_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATOMO_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATOMO_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATOMO_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"",'',this.parent.oContained
  endproc
  proc oCATOMO_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CATOMO
     i_obj.ecpSave()
  endproc

  add object oDESC1_1_14 as StdField with uid="EAGEYTMABH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESC1", cQueryName = "DESC1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 68288970,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=183, Top=211, InputMask=replicate('X',35)

  add object oGRUMER_1_15 as StdField with uid="CCJMMLBZKB",rtseq=15,rtrep=.f.,;
    cFormVar = "w_GRUMER", cQueryName = "GRUMER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 255339366,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=118, Top=183, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMER"

  func oGRUMER_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMER_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMER_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMER_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"",'',this.parent.oContained
  endproc
  proc oGRUMER_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUMER
     i_obj.ecpSave()
  endproc

  add object oDESC2_1_16 as StdField with uid="ZOOFPWTERO",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESC2", cQueryName = "DESC2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 67240394,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=183, Top=183, InputMask=replicate('X',35)

  add object oCODFAM_1_17 as StdField with uid="VIHVKHVILZ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 166729766,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=118, Top=155, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"",'',this.parent.oContained
  endproc
  proc oCODFAM_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CODFAM
     i_obj.ecpSave()
  endproc


  add object oObj_1_18 as cp_outputCombo with uid="ZLERXIVPWT",left=118, top=243, width=450,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 53578726


  add object oBtn_1_19 as StdButton with uid="MYEZLWMSRW",left=470, top=272, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 17370662;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty (.w_INVFIN))
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="TYWOIZUCSI",left=521, top=272, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 117101498;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESC3_1_21 as StdField with uid="TKHTJWLWIM",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESC3", cQueryName = "DESC3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 66191818,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=183, Top=155, InputMask=replicate('X',35)


  add object oObj_1_34 as cp_runprogram with uid="MKSRZCNRIH",left=385, top=348, width=188,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSMA_BAI',;
    cEvent = "w_INVFIN Changed",;
    nPag=1;
    , HelpContextID = 53578726

  add object oStr_1_22 as StdString with uid="UPPTVTLPXO",Visible=.t., Left=6, Top=155,;
    Alignment=1, Width=110, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="UVJQIQHRSU",Visible=.t., Left=7, Top=87,;
    Alignment=0, Width=316, Height=15,;
    Caption="Inventario iniziale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="JVBOMBTNPH",Visible=.t., Left=417, Top=8,;
    Alignment=0, Width=150, Height=15,;
    Caption="Valorizzazioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="ORXCVCPJTY",Visible=.t., Left=7, Top=8,;
    Alignment=0, Width=316, Height=15,;
    Caption="Inventario finale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="GKTKYPFGZV",Visible=.t., Left=228, Top=69,;
    Alignment=1, Width=186, Height=15,;
    Caption="Valorizzazione acquisti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="WHYPTOMERK",Visible=.t., Left=4, Top=116,;
    Alignment=1, Width=57, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="VHJIZPNKYT",Visible=.t., Left=114, Top=116,;
    Alignment=1, Width=56, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="HQXFNVHNMK",Visible=.t., Left=231, Top=116,;
    Alignment=1, Width=102, Height=15,;
    Caption="Movimenti fino al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="UPCFEDUOCH",Visible=.t., Left=4, Top=36,;
    Alignment=1, Width=57, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="IMIBTPOVCE",Visible=.t., Left=115, Top=36,;
    Alignment=1, Width=55, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="XTNISZJVAE",Visible=.t., Left=231, Top=36,;
    Alignment=1, Width=102, Height=15,;
    Caption="Movimenti fino al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="EVZXSLBLKS",Visible=.t., Left=6, Top=211,;
    Alignment=1, Width=110, Height=15,;
    Caption="Categ.omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="AHZPRLPDMY",Visible=.t., Left=6, Top=183,;
    Alignment=1, Width=110, Height=15,;
    Caption="Gr.merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="HRTFFGQQPR",Visible=.t., Left=21, Top=243,;
    Alignment=1, Width=93, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_30 as StdBox with uid="NTXLHQQCAJ",left=4, top=25, width=565,height=1

  add object oBox_1_35 as StdBox with uid="FCWLWLPLIY",left=4, top=104, width=565,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_sav','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
