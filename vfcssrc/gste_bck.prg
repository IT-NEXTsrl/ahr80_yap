* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bck                                                        *
*              Controlli causali distinte                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_31]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-07                                                      *
* Last revis.: 2015-03-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bck",oParentObject,m.pOper)
return(i_retval)

define class tgste_bck as StdBatch
  * --- Local variables
  pOper = 0
  w_GSTE_MBC = .NULL.
  w_CONTROL = .NULL.
  w_MESS = space(100)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Causali Distinte (da GSTE_ACD)
    do case
      case this.pOper=1
        this.oParentObject.w_CATIPCON = this.oParentObject.w_CATIPSCD
      case this.pOper=2
        if this.oParentobject.currentEvent<>"Load"
          if this.oParentObject.w_CATIPDIS<>"CE"
            this.oParentObject.w_CADIS_RB = IIF(this.oParentObject.w_CATIPDIS="RB", this.oParentObject.w_CATIPDIS, "  ")
            this.oParentObject.w_CADIS_RD = IIF(this.oParentObject.w_CATIPDIS="RD", this.oParentObject.w_CATIPDIS, "  ")
            this.oParentObject.w_CADIS_MA = IIF(this.oParentObject.w_CATIPDIS="MA", this.oParentObject.w_CATIPDIS, "  ")
            this.oParentObject.w_CADIS_BO = IIF(this.oParentObject.w_CATIPDIS $ "BO-BE-SE-SC", "BO", "  ")
            this.oParentObject.w_CADIS_RI = IIF(this.oParentObject.w_CATIPDIS$ "RI-SD", "RI", "  ")
            this.oParentObject.w_CADIS_CA = IIF(this.oParentObject.w_CATIPDIS="CA", this.oParentObject.w_CATIPDIS, "  ")
          else
            this.oParentObject.w_CADIS_MA = "  "
          endif
        endif
        * --- Aggiorno la Combo al cambio del tipo distinta
        this.w_CONTROL = This.oParentObject.GetCtrl("w_CACAUBON")
        this.w_CONTROL.Popola()     
        * --- Rilancio la query per sapere se ho un valore per la causale
        this.oParentObject.w_TEST = this.w_CONTROL.LISTCOUNT>0
      case this.pOper=3
        this.w_GSTE_MBC = THIS.OPARENTOBJECT.GSTE_MBC
        this.w_GSTE_MBC.w_CONSBF = this.oParentObject.w_CACONSBF
      case this.pOper=4
        if This.oParentObject.cFunction="Edit" and this.oParentObject.w_CAFLCONT $ "S-I" 
          * --- Select from gste_bck
          do vq_exec with 'gste_bck',this,'_Curs_gste_bck','',.f.,.t.
          if used('_Curs_gste_bck')
            select _Curs_gste_bck
            locate for 1=1
            do while not(eof())
            this.w_MESS = ah_Msgformat("Attenzione impossibile cambiare tipo di contabilizzazione, esiste ancora la distinta aperta%0numero %1 del %2 anno %3", Alltrim(str(_Curs_GSTE_BCK.DINUMERO)), DTOC(_Curs_GSTE_BCK.DIDATDIS), Alltrim(_Curs_GSTE_BCK.DI__ANNO) ) 
              select _Curs_gste_bck
              continue
            enddo
            use
          endif
          if Not Empty(this.w_MESS)
            this.oParentObject.w_CAFLCONT = this.oParentObject.w_OLDCONT
            ah_ErrorMsg("%1",,"", this.w_MESS)
          endif
        endif
    endcase
    if this.pOper =1 or this.pOper =2
      if this.oParentObject.w_CATIPSCD="C"
        this.oParentObject.w_CAFLBANC = IIF(this.oParentObject.w_CATIPDIS $ "RB-RI-CA-SD", "A", IIF(this.oParentObject.w_CATIPDIS $ "BO-BE-SE-SC", "S", "N"))
      else
        this.oParentObject.w_CAFLBANC = IIF(this.oParentObject.w_CATIPDIS $ "RB-RI-CA-SD", "S", IIF(this.oParentObject.w_CATIPDIS $ "BO-BE-MA-SC-SE", "A", "N"))
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_gste_bck')
      use in _Curs_gste_bck
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
