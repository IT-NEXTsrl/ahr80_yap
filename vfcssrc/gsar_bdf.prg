* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bdf                                                        *
*              ABBINA DOCUMENTI A DISTINTA FATTURE ELETTRONICHE                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-25                                                      *
* Last revis.: 2012-06-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bdf",oParentObject,m.pEXEC)
return(i_retval)

define class tgsar_bdf as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_PADRE = .NULL.
  w_OK = .f.
  w_NUMDIS = space(10)
  w_MVTIPDOC = space(5)
  w_MVCODSED = space(5)
  w_MVCODSPE = space(3)
  w_MVCODBA2 = space(15)
  w_MVTIPCON = space(15)
  w_MVCODCON = space(15)
  w_LCODSTR = space(10)
  w_MVCODDES = space(10)
  w_MVNUMCOR = space(25)
  w_MVCODBAN = space(10)
  w_XCONORN = space(10)
  w_CODAZI = space(5)
  w_PATH = space(254)
  w_MVSERIAL = space(10)
  w_NAZION = space(3)
  w_CODGRU = space(10)
  w_oERRORLOG = .NULL.
  w_INDENT = 0
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod("  /  /  ")
  w_RIFDOC = space(30)
  w_BANCA = space(35)
  w_OKDIR = .f.
  w_CODESE = space(5)
  w_PROGRE = space(14)
  w_CODESE = space(5)
  w_FILE = space(10)
  w_CODCUC = space(1)
  w_HANDLEFILE = 0
  w_NOMFIL = space(200)
  w_CODELE = space(10)
  w_MESS = space(100)
  w_FLFILE = space(1)
  w_FARIFDOC = space(10)
  w_OK = .f.
  w_OKDIS = .f.
  w_STATUS = space(1)
  w_NUMREC = 0
  w_INDICE = 0
  w_CODTAB = space(30)
  w_INDPROG = 0
  * --- WorkFile variables
  TIP_DOCU_idx=0
  DOC_MAST_idx=0
  CONTROPA_idx=0
  VASTRUTT_idx=0
  DISMFAEL_idx=0
  CONTI_idx=0
  VAELEMEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- A abbina documenti
    *     G genera file CBI
    *     S  seleziona
    *     D deseleziona
    *     I   inverti selezione
    *     L libera documenti nel caso di stato rifiutato o cancellazione della distinta o F6 su distinta provvisoria
    if this.pEXEC $ "G-L"
      this.w_PADRE = This.oParentobject
    else
      this.w_PADRE = This.oParentobject.oParentobject
       
 NC =this.oParentObject.w_Zoomdoc.cCursor
    endif
    do case
      case this.pEXEC="A"
        * --- Try
        local bErr_052E6BD8
        bErr_052E6BD8=bTrsErr
        this.Try_052E6BD8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Errore durante l'aggiornamento; Operazione abbandonata",,"")
        endif
        bTrsErr=bTrsErr or bErr_052E6BD8
        * --- End
      case this.pEXEC="G"
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        if this.w_Padre.Numrow() = 0
          ah_errormsg("Attenzione, nessun documento presente nel dettaglio!")
          i_retcode = 'stop'
          return
        endif
         
 CountSegm=0 
 CountSect=0 
 CountIndent=0 
 TestRiga=.t. 
 Predi=" "
        this.w_CODAZI = i_CODAZI
        * --- Leggo struttura da parametri fattura elettronica
        this.w_BANCA = STRTRAN(this.oParentObject.w_DESBAN, "'", " ")
        this.w_BANCA = STRTRAN(this.w_BANCA, '"', " ")
        this.w_BANCA = STRTRAN(this.w_BANCA,"?" , " ")
        this.w_BANCA = STRTRAN(this.w_BANCA,"!" , " ")
        this.w_BANCA = STRTRAN(this.w_BANCA,"/" , " ")
        this.w_BANCA = STRTRAN(this.w_BANCA,"\" , " ")
        this.w_BANCA = STRTRAN(this.w_BANCA,"*" , " ")
        this.w_BANCA = STRTRAN(this.w_BANCA,"," , " ")
        this.w_BANCA = STRTRAN(this.w_BANCA,":" , " ")
        this.w_LCODSTR = this.oParentObject.w_STRCBI
        this.w_PATH = ADDBS(ALLTRIM(this.oParentObject.w_PATCBI))+ALLTRIM(IIF(this.oParentObject.w_TIPBAN $ "CD", iif(this.oParentObject.w_TIPBAN="C", ALLTRIM(this.oParentObject.w_FABANRIF) +ALLTRIM(IIF(EMPTY(this.oParentObject.w_FABANRIF), " ", "\")),ALLTRIM(this.w_BANCA) +ALLTRIM(IIF(EMPTY(this.w_BANCA), " ", "\"))), " "))
        this.oParentObject.w_PATFIL = this.w_PATH
        if Empty(this.w_PATH)
          ah_errormsg("Attenzione, path di generazione file CBI non presente nei parametri!")
          i_retcode = 'stop'
          return
        else
          this.w_PATH = ADDBS(ALLTRIM(this.w_PATH))
          if chknfile(this.w_PATH,"S")
            if Not Directory(Alltrim(this.w_PATH))
              this.w_OKDIR = Makedir(Alltrim(this.w_PATH))
              if Not this.w_OKDIR
                ah_errormsg("Errore generazione directory!")
                i_retcode = 'stop'
                return
              endif
            endif
          endif
        endif
        if Empty(this.w_LCODSTR)
          ah_errormsg("Attenzione, struttura file CBI non presente nei parametri!")
          i_retcode = 'stop'
          return
        endif
        if Empty(this.oParentObject.w_FILCBI)
          ah_errormsg("Attenzione, nome file non specificato!")
          i_retcode = 'stop'
          return
        endif
        * --- Read from VASTRUTT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VASTRUTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "STCODENT,STTIPFIL,STINDENT"+;
            " from "+i_cTable+" VASTRUTT where ";
                +"STCODICE = "+cp_ToStrODBC(this.w_LCODSTR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            STCODENT,STTIPFIL,STINDENT;
            from (i_cTable) where;
                STCODICE = this.w_LCODSTR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_CODENT = NVL(cp_ToDate(_read_.STCODENT),cp_NullValue(_read_.STCODENT))
          w_TIPFIL = NVL(cp_ToDate(_read_.STTIPFIL),cp_NullValue(_read_.STTIPFIL))
          this.w_INDENT = NVL(cp_ToDate(_read_.STINDENT),cp_NullValue(_read_.STINDENT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_HANDLEFILE = -1
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        * --- Verifica documenti per i quali non � possibile creare file stream
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          if this.w_PADRE.FullRow() 
            this.w_MVSERIAL = this.w_PADRE.w_FARIFDOC
            this.w_MVNUMDOC = Nvl(this.w_PADRE.w_NUMDOC,0)
            this.w_MVTIPDOC = this.w_PADRE.w_TIPDOC
            this.w_MVDATDOC = cp_todate(this.w_PADRE.w_DATDOC)
            this.w_RIFDOC = Alltrim(str(this.w_MVNUMDOC))+IIF(Not Empty(this.w_mvalfdoc),"/"+Alltrim(this.w_MVALFDOC),Alltrim(this.w_MVALFDOC))
            this.w_FILE = " "
            this.w_MVTIPCON = this.w_PADRE.w_TIPCON
            this.w_MVCODCON = this.w_PADRE.w_CODCON
            if EMPTY(this.w_LCODSTR)
              * --- Read from TIP_DOCU
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "TDCODSTR"+;
                  " from "+i_cTable+" TIP_DOCU where ";
                      +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  TDCODSTR;
                  from (i_cTable) where;
                      TDTIPDOC = this.w_MVTIPDOC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LCODSTR = NVL(cp_ToDate(_read_.TDCODSTR),cp_NullValue(_read_.TDCODSTR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- Select from VAELEMEN
            i_nConn=i_TableProp[this.VAELEMEN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select ELCODSTR,EL__TIPO,ELCODCLA,ELCODTAB  from "+i_cTable+" VAELEMEN ";
                  +" where ELCODSTR="+cp_ToStrODBC(this.w_LCODSTR)+" AND EL__TIPO='L'";
                   ,"_Curs_VAELEMEN")
            else
              select ELCODSTR,EL__TIPO,ELCODCLA,ELCODTAB from (i_cTable);
               where ELCODSTR=this.w_LCODSTR AND EL__TIPO="L";
                into cursor _Curs_VAELEMEN
            endif
            if used('_Curs_VAELEMEN')
              select _Curs_VAELEMEN
              locate for 1=1
              do while not(eof())
              * --- Eseguo ricerca indice
              if Empty(Nvl(_Curs_VAELEMEN.ELCODCLA,Space(5)))
                this.w_oERRORLOG.AddMsgLogPartNoTrans(space(4),"Documento numero %1 del %2 con causale %3 non esportato, classe documento  mancante nell'elemento di tipo allegato",Alltrim(this.w_RIFDOC),DTOC(this.w_MVDATDOC),Alltrim(this.w_MVTIPDOC))     
              else
                * --- Read from CONTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ANCODCUC"+;
                    " from "+i_cTable+" CONTI where ";
                        +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                        +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ANCODCUC;
                    from (i_cTable) where;
                        ANTIPCON = this.w_MVTIPCON;
                        and ANCODICE = this.w_MVCODCON;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CODCUC = NVL(cp_ToDate(_read_.ANCODCUC),cp_NullValue(_read_.ANCODCUC))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.w_CODCUC="03"
                  this.w_FILE = Searchindex(Nvl(_Curs_VAELEMEN.ELCODCLA,Space(5)),Nvl(_Curs_VAELEMEN.ELCODTAB," "),this.w_MVSERIAL,"3")
                  if Empty(this.w_FILE)
                    * --- Creazione FIle EDI
                    this.w_oERRORLOG.AddMsgLogPartNoTrans(space(4),"Documento numero %1 del %2 con causale %3 non esportato, indice mancante",Alltrim(this.w_RIFDOC),DTOC(this.w_MVDATDOC),Alltrim(this.w_MVTIPDOC))     
                  endif
                endif
              endif
              exit
                select _Curs_VAELEMEN
                continue
              enddo
              use
            endif
          endif
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.RePos()     
        if this.w_oErrorLog.IsFullLog()
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
          i_retcode = 'stop'
          return
        endif
        * --- Try
        local bErr_052E8828
        bErr_052E8828=bTrsErr
        this.Try_052E8828()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_errormsg("Errore nella generazione del file!")
        endif
        bTrsErr=bTrsErr or bErr_052E8828
        * --- End
        if this.w_HANDLEFILE<0
          i_retcode = 'stop'
          return
        endif
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        Predi=this.w_PROGRE
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          if this.w_PADRE.FullRow() 
            this.w_MVSERIAL = this.w_PADRE.w_FARIFDOC
            this.w_MVNUMDOC = Nvl(this.w_PADRE.w_NUMDOC,0)
            this.w_MVTIPDOC = this.w_PADRE.w_TIPDOC
            this.w_MVDATDOC = cp_todate(this.w_PADRE.w_DATDOC)
            this.w_MVALFDOC = Nvl(this.w_PADRE.w_ALFDOC,"")
            this.w_MVCODCON = this.w_PADRE.w_CODCON
            this.w_MVCODSPE = this.w_PADRE.w_CODSPE
            this.w_MVCODBA2 = this.w_PADRE.w_CODBA2
            this.w_MVTIPCON = this.w_PADRE.w_TIPCON
            this.w_MVCODDES = this.w_PADRE.w_CODDES
            this.w_MVNUMCOR = this.w_PADRE.w_NUMCOR
            this.w_MVCODBAN = this.w_PADRE.w_CODBAN
            this.w_XCONORN = this.w_PADRE.w_CODORN
            this.w_RIFDOC = Alltrim(str(this.w_MVNUMDOC))+IIF(Not Empty(this.w_mvalfdoc),"/"+Alltrim(this.w_MVALFDOC),Alltrim(this.w_MVALFDOC))
            if EMPTY(this.w_LCODSTR)
              * --- Read from TIP_DOCU
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "TDCODSTR"+;
                  " from "+i_cTable+" TIP_DOCU where ";
                      +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  TDCODSTR;
                  from (i_cTable) where;
                      TDTIPDOC = this.w_MVTIPDOC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LCODSTR = NVL(cp_ToDate(_read_.TDCODSTR),cp_NullValue(_read_.TDCODSTR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if Not empty(this.w_LCODSTR)
              * --- Creazione FIle EDI
               
 DECLARE ARRKEY(1,3) 
 ARRKEY[1,3]="DOC_MAST" 
 ARRKEY[1,1]=this.w_MVSERIAL 
 ARRKEY[1,2]="MVSERIAL"
              gsar_bee(this,this.w_LCODSTR,this.w_MVCODCON,this.w_MVTIPCON,this.w_CODGRU, @ARRKEY,this.w_HANDLEFILE,"U",this.w_oERRORLOG)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if !bTrsErr
                * --- Aggiorno flag generazione file edi
                * --- Write into DOC_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVTIPDIS ="+cp_NullLink(cp_ToStrODBC("C"),'DOC_MAST','MVTIPDIS');
                      +i_ccchkf ;
                  +" where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                         )
                else
                  update (i_cTable) set;
                      MVTIPDIS = "C";
                      &i_ccchkf. ;
                   where;
                      MVSERIAL = this.w_MVSERIAL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            else
              this.w_oERRORLOG.AddMsgLogPartNoTrans(space(4),"Documento numero %1 del %2 con causale %3 non esportato, struttura mancante",Alltrim(this.w_RIFDOC),DTOC(this.w_MVDATDOC),Alltrim(this.w_MVTIPDOC))     
            endif
          endif
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.RePos()     
        if Not Empty(this.oParentObject.w_FANOMFIL)
          Ah_msg("Generato file %1",.t.,.f.,1,Alltrim(this.w_nomfil))
        endif
        * --- Eseguo scruttura elementi di chiusura file
        * --- Select from gsva0bvs
        do vq_exec with 'gsva0bvs',this,'_Curs_gsva0bvs','',.f.,.t.
        if used('_Curs_gsva0bvs')
          select _Curs_gsva0bvs
          locate for 1=1
          do while not(eof())
          this.w_CODELE = _Curs_gsva0bvs.ELCODICE
           
 Dimension Arprog(1,3) 
 Arprog[1,1]="" 
 Arprog[1,2]=0 
 Arprog[1,3]=0
           
 cursore=SYS(2015) 
 w_a=CREAELEEDI(this.w_LCODSTR,w_CODENT,this.w_CODELE,"","","",this.w_HANDLEFILE,0,this.w_oerrorlog,"C","",this.w_INDENT,0,@Arprog)
            select _Curs_gsva0bvs
            continue
          enddo
          use
        endif
        * --- Chiusura ciclo documenti
        *     chiusura file 
        this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
        this.w_FLFILE = "S"
        this.w_MESS = IIF(this.w_FLFILE="S","File generato con successo","File generati con successo")
        if not (FCLOSE(this.w_HANDLEFILE))
          ah_ErrorMsg("Errore in chiusura del file %1.%0Operazione annullata","!","",this.w_NOMFIL)
        endif
        if this.w_oErrorLog.IsFullLog()
          ah_ErrorMsg("Generazione completata","i","")
        else
          ah_ErrorMsg("Generazione completata con successo","i","")
        endif
        this.oParentObject.w_FASTATUS = "2"
        this.oParentObject.w_FAFLPROV = "S"
        this.w_PADRE.bHeaderUpdated = .t.
        this.w_PADRE.Ecpsave()     
      case this.pEXEC="L"
        this.w_OK = .T.
        if this.w_PADRE.cFunction="Edit" 
          this.w_PADRE.FirstRow()     
          this.w_PADRE.SetRow(1)     
          this.w_PADRE.MarkPos()     
          * --- Test Se riga Eliminata
          this.w_PADRE.FirstRowDel()     
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            this.w_FARIFDOC = this.w_PADRE. Get( "w_FARIFDOC" )
            * --- Se tutto ok passo alla prossima riga altrimenti esco...
            * --- Write into DOC_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVTIPDIS ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVTIPDIS');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_FARIFDOC);
                     )
            else
              update (i_cTable) set;
                  MVTIPDIS = "N";
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_FARIFDOC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_PADRE.NextRowDel()     
          enddo
          this.w_PADRE.RePos()     
          this.w_PADRE.FirstRow()     
          this.w_PADRE.SetRow(1)     
        endif
        if Val(this.oParentObject.w_FASTATUS) < Val(this.oParentObject.w_OLDSTAT)
          Ah_errormsg("Attenzione impossibile portare distinta ad uno stato precedente!")
          this.oParentObject.w_FASTATUS = this.oParentObject.w_OLDSTAT
          this.w_OK = .F.
        endif
        if (this.oParentObject.w_FASTATUS<>this.oParentObject.w_OLDSTAT AND this.oParentObject.w_FASTATUS$ "5-4") or this.w_PADRE.cFunction="Query" 
          * --- libero documenti collegati nel caso di stats rifiutato o di cancellazione della disitnta
          this.w_OKDIS = .T.
          if this.w_PADRE.cFunction="Query" 
            this.w_OK = .T.
          else
            if this.oParentObject.w_FASTATUS="5" and !Ah_Yesno("I documenti abbinati saranno nuovamente abbinabili ad una nuova distinta!%0 Confermi ugualmente?")
              this.w_OK = .F.
              this.w_OKDIS = .F.
              this.oParentObject.w_FASTATUS = this.oParentObject.w_OLDSTAT
              * --- Se passo in stato rifiutato il documento torna abbinabile e quindi resetto stato della distinta
            endif
            if this.oParentObject.w_FASTATUS="4" 
              this.w_OKDIS = .F.
              if !Ah_Yesno("Confermando la distinta con stato accettato non sar� pi� possibile modificare!%0 Confermi ugualmente ?") 
                this.w_OK = .F.
                this.oParentObject.w_FASTATUS = this.oParentObject.w_OLDSTAT
              endif
            endif
          endif
          if this.w_OKDIS
            * --- Write into DOC_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MVSERIAL"
              do vq_exec with 'gsar_bdf',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVTIPDIS ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVTIPDIS');
                  +i_ccchkf;
                  +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
              +"DOC_MAST.MVTIPDIS ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVTIPDIS');
                  +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
                  +"MVTIPDIS";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVTIPDIS')+"";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
              +"MVTIPDIS ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVTIPDIS');
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVTIPDIS ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVTIPDIS');
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        if this.w_OK AND (this.oParentObject.w_FASTATUS<>this.oParentObject.w_OLDSTAT or this.w_PADRE.cFunction="Query" )
          * --- Aggiorno status nel documento
          this.w_STATUS = IIF(this.w_PADRE.cFunction="Query" ,"1",this.oParentObject.w_FASTATUS)
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MVSERIAL"
            do vq_exec with 'gsar_bdf',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVSTFILCB ="+cp_NullLink(cp_ToStrODBC(this.w_STATUS),'DOC_MAST','MVSTFILCB');
                +i_ccchkf;
                +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
            +"DOC_MAST.MVSTFILCB ="+cp_NullLink(cp_ToStrODBC(this.w_STATUS),'DOC_MAST','MVSTFILCB');
                +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
                +"MVSTFILCB";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +cp_NullLink(cp_ToStrODBC(this.w_STATUS),'DOC_MAST','MVSTFILCB')+"";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
            +"MVSTFILCB ="+cp_NullLink(cp_ToStrODBC(this.w_STATUS),'DOC_MAST','MVSTFILCB');
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVSTFILCB ="+cp_NullLink(cp_ToStrODBC(this.w_STATUS),'DOC_MAST','MVSTFILCB');
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        if Not this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=Ah_msgformat("Transazione abbandonata")
        endif
      otherwise
        ND=this.oParentObject.w_ZOOMDOC.cCursor
        UPDATE (ND) SET XCHK=ICASE(this.pEXEC="S",1,this.pEXEC="D", 0, IIF(XCHK=1,0,1))
    endcase
  endproc
  proc Try_052E6BD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Cicla sui record Selezionati
    this.w_OK = .F.
    this.w_PADRE.MarkPos()     
    SELECT (NC)
    GO TOP
    SCAN FOR XCHK<>0 
    this.w_PADRE.AddRow()     
    SELECT (NC)
    this.w_MVSERIAL = MVSERIAL
    this.w_PADRE.w_FARIFDOC = MVSERIAL
    this.w_PADRE.w_TIPDOC = Nvl(MVTIPDOC,Space(5))
    this.w_PADRE.w_CODCON = Nvl(MVCODCON,Space(15))
    this.w_PADRE.w_ALFDOC = MVALFDOC
    this.w_PADRE.w_DATDOC = CP_TODATE(MVDATDOC)
    this.w_PADRE.w_NUMDOC = MVNUMDOC
    this.w_PADRE.w_CODSED = Nvl(MVCODSED,Space(5))
    this.w_PADRE.w_TIPCON = Nvl(MVTIPCON,Space(1))
    this.w_PADRE.w_CODSPE = Nvl(MVCODSPE,Space(5))
    this.w_PADRE.w_CODBA2 = Nvl(MVCODBA2,Space(10))
    this.w_PADRE.w_CODDES = Nvl(MVCODDES,Space(5))
    this.w_PADRE.w_NUMCOR = Nvl(MVNUMCOR,Space(25))
    this.w_PADRE.w_CODBAN = Nvl(MVCODBAN,Space(15))
    this.w_PADRE.w_CODORN = Nvl(MVCODORN,Space(15))
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    this.w_PADRE.SaveRow()     
    this.w_OK = .T.
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVTIPDIS ="+cp_NullLink(cp_ToStrODBC("P"),'DOC_MAST','MVTIPDIS');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVTIPDIS = "P";
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    SELECT (NC)
    ENDSCAN
    this.w_PADRE.RePos()     
    if this.w_OK
      * --- Lancia lo Zoom
      this.w_NUMDIS = this.w_PADRE.w_FANUMDIS
      this.w_PADRE.NotifyEvent("Save")     
      this.w_PADRE.GotFocus()     
      this.w_PADRE.EcpSave()     
      this.w_PADRE.EcpQuery()     
      this.w_PADRE.EcpFilter()     
      this.w_PADRE.w_FANUMDIS = this.w_NUMDIS
      this.w_PADRE.EcpSave()     
      this.w_PADRE.EcpEdit()     
      * --- Chiudo la Maschera
      This.oParentObject.ecpQuit()
    else
      ah_ErrorMsg("Non ci sono documenti da abbinare",,"")
    endif
    return
  proc Try_052E8828()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_PROGRE = SPACE(14)
    this.w_CODESE = g_CODESE
    if "<PROGRE>" $ this.oParentObject.w_FILCBI 
      i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
      cp_NextTableProg(this, i_Conn, "PREDI", "i_CODAZI,w_CODESE,w_PROGRE")
    endif
     
 DIMENSION ARPARAM[6,2] 
 ARPARAM[1,1]="DATE" 
 ARPARAM[1,2]=ALLTRIM(STR(YEAR(i_DATSYS))+Right("00"+alltrim(STR(MONTH(i_DATSYS))),2)+Right("00"+alltrim(STR(DAY(i_DATSYS))),2)) 
 ARPARAM[2,1]="TIME" 
 ARPARAM[2,2]=ALLTRIM(STRTRAN(TIME(),":","")) 
 ARPARAM[3,1]="DATDIS" 
 Datdis=this.oParentObject.w_FADATDIS 
 CAMPO="ALLTRIM(STR(YEAR(DATDIS))+Right('00'+alltrim(STR(MONTH(DATDIS))),2)+Right('00'+alltrim(STR(DAY(DATDIS))),2))" 
 ARPARAM[3,2]=IIF(TYPE("&CAMPO")="C",&CAMPO,"X") 
 ARPARAM[4,1]="NUMDIS" 
 ARPARAM[4,2]=Alltrim(str(this.oParentObject.w_FANUMERO)) 
 ARPARAM[5,1]="PROGRE" 
 ARPARAM[5,2]=this.w_PROGRE 
 ARPARAM[6,1]="BANCA" 
 ARPARAM[6,2]=Alltrim(this.oParentObject.w_FABANRIF)
    this.w_NOMFIL = CALDESPA(this.oParentObject.w_FILCBI,@ARPARAM)
    if Not File(Alltrim(this.w_PATH)+Alltrim(this.w_NOMFIL)) Or ah_YesNo("Il file esiste gi�, si desidera sovrascriverlo?")
      this.w_HANDLEFILE = FCREATE(Alltrim(this.w_PATH)+Alltrim(this.w_NOMFIL))
    endif
    if this.w_HANDLEFILE<0
      * --- Raise
      i_Error="Errore nella creazione file CBI"
      return
    else
      this.oParentObject.w_FANOMFIL = Alltrim(this.oParentObject.w_PATFIL)+this.w_NOMFIL
      this.oParentObject.w_FADATVAL = i_datsys
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione FIle EDI
     
 DECLARE ARRKEY(1,3) 
 ARRKEY[1,3]="DOC_MAST" 
 ARRKEY[1,1]="@@@@@@@@@@" 
 ARRKEY[1,2]="MVSERIAL"
    * --- Eseguo creazione cursori con nome tabelle dell'entit�
    this.w_INDPROG = 1
    * --- Select from gsva1bge
    do vq_exec with 'gsva1bge',this,'_Curs_gsva1bge','',.f.,.t.
    if used('_Curs_gsva1bge')
      select _Curs_gsva1bge
      locate for 1=1
      do while not(eof())
      this.w_CODTAB = Alltrim(Nvl(_Curs_gsva1BGE.ELCODTAB,Space(30)))
       
 Dimension Arprog(this.w_INDPROG,3) 
 Arprog[this.w_INDPROG,1]=this.w_CODTAB 
 Arprog[this.w_INDPROG,2]=0 
 Arprog[this.w_INDPROG,3]=0
      this.w_INDPROG = this.w_INDPROG + 1
       
 a=creacuredi(this.w_LCODSTR,Alltrim(_Curs_gsva1bge.ELCODTAB),@Arrkey)
        select _Curs_gsva1bge
        continue
      enddo
      use
    endif
    * --- Select from gsva0bvs
    do vq_exec with 'gsva0bvs',this,'_Curs_gsva0bvs','',.f.,.t.
    if used('_Curs_gsva0bvs')
      select _Curs_gsva0bvs
      locate for 1=1
      do while not(eof())
      this.w_CODELE = _Curs_gsva0bvs.ELCODICE
       
 cursore=SYS(2015) 
 w_a=CREAELEEDI(this.w_LCODSTR,w_CODENT,this.w_CODELE,"","","",this.w_HANDLEFILE,0,this.w_oerrorlog,"A","",this.w_INDENT,0,@Arprog)
        select _Curs_gsva0bvs
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='CONTROPA'
    this.cWorkTables[4]='VASTRUTT'
    this.cWorkTables[5]='DISMFAEL'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='VAELEMEN'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_VAELEMEN')
      use in _Curs_VAELEMEN
    endif
    if used('_Curs_gsva0bvs')
      use in _Curs_gsva0bvs
    endif
    if used('_Curs_gsva1bge')
      use in _Curs_gsva1bge
    endif
    if used('_Curs_gsva0bvs')
      use in _Curs_gsva0bvs
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
