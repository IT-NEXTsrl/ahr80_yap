* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bes                                                        *
*              Legge dati cli/for                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_229]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-09                                                      *
* Last revis.: 2010-04-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bes",oParentObject,m.pOper)
return(i_retval)

define class tgsma_bes as StdBatch
  * --- Local variables
  pOper = space(1)
  w_FLAVA1 = space(1)
  w_MESS = space(10)
  w_RECO = 0
  w_AGGRIG = .f.
  w_GSMA_MVM = .NULL.
  w_WARN = space(1)
  * --- WorkFile variables
  DES_DIVE_idx=0
  LISTINI_idx=0
  PAG_AMEN_idx=0
  SIT_FIDI_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  CONTI_idx=0
  AGENTI_idx=0
  CON_TRAM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cambio data registrazione sui Movimenti di magazzino GSMA_MVM
    * --- Se esiste il Cliente, Ricalcola i Prezzi
    this.w_GSMA_MVM = this.oParentObject
    do case
      case this.pOper<>"S"
        * --- Se esiste il Cliente, Ricalcola i Prezzi
        * --- Forza i Calcoli prima di rielaborare le Righe Documento
        this.w_GSMA_MVM.mCalc(.T.)     
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="S"
        * --- Aggiorna le Righe Mov.magazzino al Variare degli Sconti Finali (da GSMA_MVM)
        this.w_GSMA_MVM.mCalc(.T.)     
        this.w_GSMA_MVM.MarkPos()     
        this.w_GSMA_MVM.FirstRow()     
        this.oParentObject.w_TOTALE = 0
        do while Not this.w_GSMA_MVM.Eof_Trs()
          this.w_GSMA_MVM.SetRow()     
          * --- Scorro le righe piene, cancellate (se non in salvataggio), o aggiunte e modificate
          *     se non cancellate
          if this.w_GSMA_MVM.FullRow()
            this.oParentObject.w_VALUNI2 = cp_ROUND (this.oParentObject.w_VALUNI * (1+this.oParentObject.w_MMSCOCL1/100)*(1+this.oParentObject.w_MMSCOCL2/100)*(1+this.oParentObject.w_MMSCOPAG/100),5)
            this.oParentObject.w_MMVALMAG = cp_ROUND(this.oParentObject.w_MMQTAMOV*this.oParentObject.w_VALUNI2, this.oParentObject.w_DECTOT)
            this.oParentObject.w_MMIMPNAZ = CALCNAZ(this.oParentObject.w_MMVALMAG,this.oParentObject.w_MMCAOVAL,this.oParentObject.w_CAONAZ,IIF(EMPTY(this.oParentObject.w_MMDATDOC), this.oParentObject.w_MMDATREG, this.oParentObject.w_MMDATDOC),this.oParentObject.w_MMVALNAZ,this.oParentObject.w_MMCODVAL,IIF(this.w_FLAVA1="A",(this.oParentObject.w_PERIVA*this.oParentObject.w_INDIVA),0))
            this.oParentObject.w_VISNAZ = cp_ROUND(this.oParentObject.w_MMIMPNAZ, this.oParentObject.w_DECTOP)
            this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE + this.oParentObject.w_MMVALMAG
            this.oParentObject.w_RICTOT = .T.
            this.w_GSMA_MVM.SaveRow()     
          endif
          this.w_GSMA_MVM.NextRow()     
        enddo
        this.w_GSMA_MVM.RePos(.t.)     
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riaggiorna i Prezzi e il Listino sulle Righe
    this.w_GSMA_MVM.MarkPos()     
    this.w_RECO = this.w_GSMA_MVM.NumRow()
    if this.w_RECO<>0
      if g_FLCESC = "S" And this.pOper="C"
        * --- Codifica Cliente/Fornitore Esclusiva attiva in Azienda
        this.w_WARN = "Attiva gestione codici clienti/fornitori esclusivi%0Sulle righe potrebbero essere presenti codici incongruenti con questo intestatario"
        ah_ErrorMsg(this.w_WARN,"i","")
      endif
      this.w_AGGRIG = ah_YesNo("Aggiorno anche i prezzi sulle righe?")
      do while Not this.w_GSMA_MVM.Eof_Trs()
        this.w_GSMA_MVM.SetRow()     
        * --- Scorro le righe piene, cancellate (se non in salvataggio), o aggiunte e modificate
        *     se non cancellate
        if this.w_GSMA_MVM.FullRow()
          this.oParentObject.w_MMIMPNAZ = CALCNAZ(this.oParentObject.w_MMVALMAG,this.oParentObject.w_MMCAOVAL,this.oParentObject.w_CAONAZ,IIF(EMPTY(this.oParentObject.w_MMDATDOC), this.oParentObject.w_MMDATREG, this.oParentObject.w_MMDATDOC),this.oParentObject.w_MMVALNAZ,this.oParentObject.w_MMCODVAL,IIF(this.w_FLAVA1="A",(this.oParentObject.w_PERIVA*this.oParentObject.w_INDIVA),0))
          this.oParentObject.w_VISNAZ = cp_ROUND(this.oParentObject.w_MMIMPNAZ, this.oParentObject.w_DECTOP)
          if this.w_AGGRIG
            * --- Se riaggiorno i Prezzi o Cambiata la Valuta Ricalcola il Listino/Contratto
            this.oParentObject.w_MMCODLIS = this.oParentObject.w_MMTCOLIS
          endif
          if this.w_GSMA_MVM.cFunction = "Edit"
            * --- In Modifica documento LIPREZZO � uguale a 0. Lanciando la mCalc() in uscita di questo batch
            *     verrebbe azzerato il prezzo di riga. 
            *     Per evitare questo riassegno a LIPREZZO il Prezzo di riga.
            this.oParentObject.w_LIPREZZO = this.oParentObject.w_MMPREZZO
          endif
          this.oParentObject.o_MMCODLIS = this.oParentObject.w_MMTCOLIS
          this.oParentObject.o_MMCODICE = this.oParentObject.w_MMCODICE
          this.oParentObject.o_MMUNIMIS = this.oParentObject.w_MMUNIMIS
          if this.w_AGGRIG
            this.oParentObject.w_LIPREZZO = 0
            this.oParentObject.o_LIPREZZO = 0
            this.w_GSMA_MVM.NotifyEvent("Ricalcola")     
          endif
          this.w_GSMA_MVM.SaveRow()     
        endif
        this.w_GSMA_MVM.NextRow()     
      enddo
      * --- Riposizionamento sul Transitorio.
      *     La SaveDependsOn() viene effettuata solo se rispondo No 
      *     alla domanda di aggiornamento prezzi per evitare che vengano ricalcolate le o_ gi� assegnate
      this.w_GSMA_MVM.RePos(this.w_AGGRIG)     
      * --- lancia il calcolo Provvigioni solo per vendite
      *     Riesegue una scansione del transitorio e anche il riposizionemanto
      if this.w_AGGRIG
        * --- Poich� la Repos non ha aggiornato le o_  allineo o_mmcodice e o_mmunimis al valore della prima riga per 
        *     evitare che vengano erroneamente ricalcolate le variabili che dipendono da mmcodice e e mmunimis
        this.oParentObject.o_MMCODICE = this.oParentObject.w_MMCODICE
        this.oParentObject.o_MMUNIMIS = this.oParentObject.w_MMUNIMIS
      endif
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='DES_DIVE'
    this.cWorkTables[2]='LISTINI'
    this.cWorkTables[3]='PAG_AMEN'
    this.cWorkTables[4]='SIT_FIDI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='VOCIIVA'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='AGENTI'
    this.cWorkTables[9]='CON_TRAM'
    return(this.OpenAllTables(9))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
