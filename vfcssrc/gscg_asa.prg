* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_asa                                                        *
*              Intestatario effettivo blacklist                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-10-11                                                      *
* Last revis.: 2010-10-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_asa")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_asa")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_asa")
  return

* --- Class definition
define class tgscg_asa as StdPCForm
  Width  = 414
  Height = 178
  Top    = 10
  Left   = 10
  cComment = "Intestatario effettivo blacklist"
  cPrg = "gscg_asa"
  HelpContextID=109716841
  add object cnt as tcgscg_asa
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_asa as PCContext
  w_SASERIAL = space(10)
  w_STAMPATO = space(1)
  w_SATIPCLF = space(1)
  w_SACODICE = space(15)
  w_ANDESCRI = space(40)
  w_ANFLBLLS = space(1)
  w_OBTEST = space(8)
  w_ANDTOBSO = space(8)
  proc Save(oFrom)
    this.w_SASERIAL = oFrom.w_SASERIAL
    this.w_STAMPATO = oFrom.w_STAMPATO
    this.w_SATIPCLF = oFrom.w_SATIPCLF
    this.w_SACODICE = oFrom.w_SACODICE
    this.w_ANDESCRI = oFrom.w_ANDESCRI
    this.w_ANFLBLLS = oFrom.w_ANFLBLLS
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_ANDTOBSO = oFrom.w_ANDTOBSO
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_SASERIAL = this.w_SASERIAL
    oTo.w_STAMPATO = this.w_STAMPATO
    oTo.w_SATIPCLF = this.w_SATIPCLF
    oTo.w_SACODICE = this.w_SACODICE
    oTo.w_ANDESCRI = this.w_ANDESCRI
    oTo.w_ANFLBLLS = this.w_ANFLBLLS
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_ANDTOBSO = this.w_ANDTOBSO
    PCContext::Load(oTo)
enddefine

define class tcgscg_asa as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 414
  Height = 178
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-10-28"
  HelpContextID=109716841
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  SOGGALTE_IDX = 0
  CONTI_IDX = 0
  cFile = "SOGGALTE"
  cKeySelect = "SASERIAL"
  cKeyWhere  = "SASERIAL=this.w_SASERIAL"
  cKeyWhereODBC = '"SASERIAL="+cp_ToStrODBC(this.w_SASERIAL)';

  cKeyWhereODBCqualified = '"SOGGALTE.SASERIAL="+cp_ToStrODBC(this.w_SASERIAL)';

  cPrg = "gscg_asa"
  cComment = "Intestatario effettivo blacklist"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SASERIAL = space(10)
  w_STAMPATO = space(1)
  w_SATIPCLF = space(1)
  w_SACODICE = space(15)
  w_ANDESCRI = space(40)
  w_ANFLBLLS = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_ANDTOBSO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_asaPag1","gscg_asa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 51269386
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSATIPCLF_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='SOGGALTE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SOGGALTE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SOGGALTE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_asa'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_6_joined
    link_1_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SOGGALTE where SASERIAL=KeySet.SASERIAL
    *
    i_nConn = i_TableProp[this.SOGGALTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SOGGALTE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SOGGALTE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SOGGALTE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SOGGALTE '
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SASERIAL',this.w_SASERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_STAMPATO = This.oParentObject .w_STAMPATO
        .w_ANDESCRI = space(40)
        .w_ANFLBLLS = space(1)
        .w_ANDTOBSO = ctod("  /  /  ")
        .w_SASERIAL = NVL(SASERIAL,space(10))
        .w_SATIPCLF = NVL(SATIPCLF,space(1))
        .w_SACODICE = NVL(SACODICE,space(15))
          if link_1_6_joined
            this.w_SACODICE = NVL(ANCODICE106,NVL(this.w_SACODICE,space(15)))
            this.w_ANDESCRI = NVL(ANDESCRI106,space(40))
            this.w_ANFLBLLS = NVL(ANFLBLLS106,space(1))
            this.w_ANDTOBSO = NVL(cp_ToDate(ANDTOBSO106),ctod("  /  /  "))
          else
          .link_1_6('Load')
          endif
        .w_OBTEST = This.oParentObject .w_OBTEST
        cp_LoadRecExtFlds(this,'SOGGALTE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_SASERIAL = space(10)
      .w_STAMPATO = space(1)
      .w_SATIPCLF = space(1)
      .w_SACODICE = space(15)
      .w_ANDESCRI = space(40)
      .w_ANFLBLLS = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_ANDTOBSO = ctod("  /  /  ")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_STAMPATO = This.oParentObject .w_STAMPATO
        .w_SATIPCLF = This.oParentObject .w_PNTIPCLF
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_SACODICE))
          .link_1_6('Full')
          endif
          .DoRTCalc(5,6,.f.)
        .w_OBTEST = This.oParentObject .w_OBTEST
      endif
    endwith
    cp_BlankRecExtFlds(this,'SOGGALTE')
    this.DoRTCalc(8,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oSATIPCLF_1_4.enabled = i_bVal
      .Page1.oPag.oSACODICE_1_6.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'SOGGALTE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SOGGALTE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SASERIAL,"SASERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SATIPCLF,"SATIPCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SACODICE,"SACODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SOGGALTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SOGGALTE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SOGGALTE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SOGGALTE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SOGGALTE')
        i_extval=cp_InsertValODBCExtFlds(this,'SOGGALTE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SASERIAL,SATIPCLF,SACODICE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_SASERIAL)+;
                  ","+cp_ToStrODBC(this.w_SATIPCLF)+;
                  ","+cp_ToStrODBCNull(this.w_SACODICE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SOGGALTE')
        i_extval=cp_InsertValVFPExtFlds(this,'SOGGALTE')
        cp_CheckDeletedKey(i_cTable,0,'SASERIAL',this.w_SASERIAL)
        INSERT INTO (i_cTable);
              (SASERIAL,SATIPCLF,SACODICE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SASERIAL;
                  ,this.w_SATIPCLF;
                  ,this.w_SACODICE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SOGGALTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SOGGALTE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SOGGALTE_IDX,i_nConn)
      *
      * update SOGGALTE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SOGGALTE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SATIPCLF="+cp_ToStrODBC(this.w_SATIPCLF)+;
             ",SACODICE="+cp_ToStrODBCNull(this.w_SACODICE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SOGGALTE')
        i_cWhere = cp_PKFox(i_cTable  ,'SASERIAL',this.w_SASERIAL  )
        UPDATE (i_cTable) SET;
              SATIPCLF=this.w_SATIPCLF;
             ,SACODICE=this.w_SACODICE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SOGGALTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SOGGALTE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SOGGALTE_IDX,i_nConn)
      *
      * delete SOGGALTE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SASERIAL',this.w_SASERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SOGGALTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SOGGALTE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
            .w_OBTEST = This.oParentObject .w_OBTEST
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSATIPCLF_1_4.enabled = this.oPgFrm.Page1.oPag.oSATIPCLF_1_4.mCond()
    this.oPgFrm.Page1.oPag.oSACODICE_1_6.enabled = this.oPgFrm.Page1.oPag.oSACODICE_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SACODICE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SACODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SATIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANFLBLLS,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SATIPCLF;
                     ,'ANCODICE',trim(this.w_SACODICE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANFLBLLS,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SACODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_SACODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SATIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANFLBLLS,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_SACODICE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_SATIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANFLBLLS,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SACODICE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSACODICE_1_6'),i_cWhere,'GSAR_BZC',"ANAGRAFICA CLIENTI/FORNITORI",'GSCG_ASA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SATIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANFLBLLS,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANFLBLLS,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice conto selezionato non ha attivato il flag fiscalit� privilegiata oppure � obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANFLBLLS,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SATIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANFLBLLS,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANFLBLLS,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SACODICE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SATIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SATIPCLF;
                       ,'ANCODICE',this.w_SACODICE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANFLBLLS,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SACODICE = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANFLBLLS = NVL(_Link_.ANFLBLLS,space(1))
      this.w_ANDTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SACODICE = space(15)
      endif
      this.w_ANDESCRI = space(40)
      this.w_ANFLBLLS = space(1)
      this.w_ANDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ANFLBLLS='S' And (.w_ANDTOBSO>.w_OBTEST OR EMPTY(.w_ANDTOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice conto selezionato non ha attivato il flag fiscalit� privilegiata oppure � obsoleto")
        endif
        this.w_SACODICE = space(15)
        this.w_ANDESCRI = space(40)
        this.w_ANFLBLLS = space(1)
        this.w_ANDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.ANCODICE as ANCODICE106"+ ",link_1_6.ANDESCRI as ANDESCRI106"+ ",link_1_6.ANFLBLLS as ANFLBLLS106"+ ",link_1_6.ANDTOBSO as ANDTOBSO106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on SOGGALTE.SACODICE=link_1_6.ANCODICE"+" and SOGGALTE.SATIPCLF=link_1_6.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and SOGGALTE.SACODICE=link_1_6.ANCODICE(+)"'+'+" and SOGGALTE.SATIPCLF=link_1_6.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSATIPCLF_1_4.RadioValue()==this.w_SATIPCLF)
      this.oPgFrm.Page1.oPag.oSATIPCLF_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSACODICE_1_6.value==this.w_SACODICE)
      this.oPgFrm.Page1.oPag.oSACODICE_1_6.value=this.w_SACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_7.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_7.value=this.w_ANDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'SOGGALTE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_ANFLBLLS='S' And (.w_ANDTOBSO>.w_OBTEST OR EMPTY(.w_ANDTOBSO)))  and (.w_STAMPATO<>'S')  and not(empty(.w_SACODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSACODICE_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice conto selezionato non ha attivato il flag fiscalit� privilegiata oppure � obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_asaPag1 as StdContainer
  Width  = 410
  height = 178
  stdWidth  = 410
  stdheight = 178
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSATIPCLF_1_4 as StdCombo with uid="CVAEEHYYAZ",rtseq=3,rtrep=.f.,left=109,top=11,width=125,height=22;
    , HelpContextID = 238788244;
    , cFormVar="w_SATIPCLF",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSATIPCLF_1_4.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oSATIPCLF_1_4.GetRadio()
    this.Parent.oContained.w_SATIPCLF = this.RadioValue()
    return .t.
  endfunc

  func oSATIPCLF_1_4.SetRadio()
    this.Parent.oContained.w_SATIPCLF=trim(this.Parent.oContained.w_SATIPCLF)
    this.value = ;
      iif(this.Parent.oContained.w_SATIPCLF=='C',1,;
      iif(this.Parent.oContained.w_SATIPCLF=='F',2,;
      0))
  endfunc

  func oSATIPCLF_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAMPATO<>'S')
    endwith
   endif
  endfunc

  func oSATIPCLF_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_SACODICE)
        bRes2=.link_1_6('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oSACODICE_1_6 as StdField with uid="QRNDHKXTJP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SACODICE", cQueryName = "SACODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice conto selezionato non ha attivato il flag fiscalit� privilegiata oppure � obsoleto",;
    HelpContextID = 118051179,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=109, Top=50, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SATIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_SACODICE"

  func oSACODICE_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAMPATO<>'S')
    endwith
   endif
  endfunc

  func oSACODICE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSACODICE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSACODICE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SATIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SATIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSACODICE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ANAGRAFICA CLIENTI/FORNITORI",'GSCG_ASA.CONTI_VZM',this.parent.oContained
  endproc
  proc oSACODICE_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SATIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_SACODICE
     i_obj.ecpSave()
  endproc

  add object oANDESCRI_1_7 as StdField with uid="QGFGEXJOSO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 235967153,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=109, Top=81, InputMask=replicate('X',40)

  add object oStr_1_3 as StdString with uid="GWANYBPBZG",Visible=.t., Left=6, Top=13,;
    Alignment=1, Width=99, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="XEIHXEJRUX",Visible=.t., Left=7, Top=52,;
    Alignment=1, Width=98, Height=18,;
    Caption="Codice conto:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_asa','SOGGALTE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SASERIAL=SOGGALTE.SASERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
