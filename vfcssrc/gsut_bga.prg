* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bga                                                        *
*              Gestione aggiornamenti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_36]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-05-28                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bga",oParentObject,m.pOper)
return(i_retval)

define class tgsut_bga as StdBatch
  * --- Local variables
  pOper = space(5)
  w_ZOOM = .NULL.
  sp_FILELOG = space(10)
  w_MESS = space(10)
  hf_FILE_LOG = 0
  w_OK = 0
  TmpC = space(100)
  w_NOK = 0
  w_TEMP = space(100)
  w_PUNPAD = .NULL.
  w_FL = space(10)
  w_COCODREL = space(15)
  w_COORDINE = space(5)
  w_CODESCON = space(50)
  w_CONOMPRO = space(50)
  w_PESEOK = .f.
  w_PMSG = space(0)
  w_PROG = .NULL.
  * --- WorkFile variables
  CONVERSI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione gestione Conversioni (da GSUT_KGA)
    * --- Inizializza ,,
    this.w_PUNPAD = this.oParentObject
    this.w_ZOOM = this.oParentObject.w_CalcZoom
    this.w_FL = "C"+dtos(date())+"."+strtran(time(),":","")+".LOG"
    this.sp_FILELOG = "..\PCON\LOG\"+this.w_FL
    do case
      case this.pOper="CALC"
        * --- Lancia lo Zoom
        This.OparentObject.NotifyEvent("Legge")
      case this.pOper="SELE"
        * --- Seleziona/Deseleziona Tutto
        NC = this.w_ZOOM.cCursor
        UPDATE &NC SET XCHK = IIF(this.oParentObject.w_SELEZI="S", 1, 0)
      case this.pOper="AGGIO"
        * --- Elimina i Codici Selezionati
        NC = this.w_ZOOM.cCursor
        SELECT &NC
        GO TOP
        * --- deve esistere almeno un Record Selezionato
        COUNT FOR XCHK=1 TO w_CONTA
        if w_CONTA=0
          ah_ErrorMsg("Non sono state selezionate conversioni da eseguire","!","")
          i_retcode = 'stop'
          return
        endif
        this.w_OK = 0
        this.w_NOK = 0
        if ah_YesNo("Confermi richiesta?")
          * --- Creazione file di log
          this.hf_FILE_LOG = fcreate(this.sp_FILELOG)
          * --- Apre il file di log e scrive intestazioni ...
          if this.hf_FILE_LOG>=0
            * --- Scrive Log
            this.TmpC = space(5)+this.w_FL+" - "+Ah_MsgFormat("Log conversioni ad hoc REVOLUTION")+" "+g_VERSION
            this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG,this.TmpC)
            this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG," ")
            this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG," ")
            this.TmpC = Ah_MsgFormat("[ Inizio operazioni: %1%3Ore: %2%3]",dtoc(date()), time(),space(3) )
            this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG,this.TmpC)
            this.TmpC = REPL("=",80)
            this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG,this.TmpC)
            this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG," ")
          endif
          * --- Cicla sui record Selezionati
          SELECT &NC
          GO TOP
          SCAN FOR XCHK=1
          * --- Parametri aggiornamento tabella
          this.w_COCODREL = nvl( &NC..COCODREL , space(15) )
          this.w_COORDINE = nvl( &NC..COORDINE , space(5) )
          this.w_CODESCON = nvl( &NC..CODESCON , space(50) )
          this.w_CONOMPRO = nvl( &NC..CONOMPRO , space(50) )
          * --- Stato procedura conversione
          this.w_PESEOK = .F.
          this.w_PMSG = " "
          * --- Aggiorna file di log
          this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG," ")
          this.TmpC = REPL("-",80)
          this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG,this.TmpC)
          this.TmpC = "[ "+this.w_COCODREL+"|"+this.w_COORDINE+" - "+Ah_MsgFormat("Procedura:")+" "+alltrim(this.w_CONOMPRO) + " ]"
          this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG,this.TmpC)
          this.TmpC = "[ "+alltrim(this.w_CODESCON)+ " ]"
          this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG,this.TmpC)
          this.TmpC = REPL("-",80)
          this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG,this.TmpC)
          * --- Lancia procedura di conversione
          ah_msg("%1Attendere...",.t.,,,alltrim(this.w_CODESCON)+chr(46)+chr(32))
          NP = alltrim(nvl( &NC..CONOMPRO , " " ))
          if FILE("..\PCON\VFCSSRC\"+ALLTRIM(NP)+".FXP")
            DO &NP with this, this.hf_FILE_LOG
          else
            ah_ErrorMsg("Non trovo il file: %1",,"", ALLTRIM(NP) )
          endif
          * --- Controllo risultato
          if this.w_PESEOK
            * --- Write into CONVERSI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CONVERSI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CONVERSI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"COESEGUI ="+cp_NullLink(cp_ToStrODBC("S"),'CONVERSI','COESEGUI');
              +",CODATESE ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'CONVERSI','CODATESE');
              +",COUTEESE ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'CONVERSI','COUTEESE');
              +",COERRORE ="+cp_NullLink(cp_ToStrODBC(this.w_PMSG),'CONVERSI','COERRORE');
                  +i_ccchkf ;
              +" where ";
                  +"COCODREL = "+cp_ToStrODBC(this.w_COCODREL);
                  +" and COORDINE = "+cp_ToStrODBC(this.w_COORDINE);
                     )
            else
              update (i_cTable) set;
                  COESEGUI = "S";
                  ,CODATESE = i_DATSYS;
                  ,COUTEESE = i_CODUTE;
                  ,COERRORE = this.w_PMSG;
                  &i_ccchkf. ;
               where;
                  COCODREL = this.w_COCODREL;
                  and COORDINE = this.w_COORDINE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_OK = this.w_OK+1
          else
            * --- Write into CONVERSI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CONVERSI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CONVERSI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"COESEGUI ="+cp_NullLink(cp_ToStrODBC("N"),'CONVERSI','COESEGUI');
              +",CODATESE ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'CONVERSI','CODATESE');
              +",COUTEESE ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'CONVERSI','COUTEESE');
              +",COERRORE ="+cp_NullLink(cp_ToStrODBC(this.w_PMSG),'CONVERSI','COERRORE');
                  +i_ccchkf ;
              +" where ";
                  +"COCODREL = "+cp_ToStrODBC(this.w_COCODREL);
                  +" and COORDINE = "+cp_ToStrODBC(this.w_COORDINE);
                     )
            else
              update (i_cTable) set;
                  COESEGUI = "N";
                  ,CODATESE = i_DATSYS;
                  ,COUTEESE = i_CODUTE;
                  ,COERRORE = this.w_PMSG;
                  &i_ccchkf. ;
               where;
                  COCODREL = this.w_COCODREL;
                  and COORDINE = this.w_COORDINE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_NOK = this.w_NOK+1
          endif
          if this.hf_FILE_LOG>=0
            this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG," ")
          endif
          ENDSCAN
          if this.hf_FILE_LOG>=0
            this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG," ")
            this.TmpC = REPL("=",80)
            this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG,this.TmpC)
            this.TmpC = Ah_MsgFormat("[ Fine operazioni: %1%3Ore: %2%3]",dtoc(date()),time(),space(3) )
            this.w_TEMP = SUPPSRVP("WRITELOG",this.hf_FILE_LOG,this.TmpC)
            =fCLOSE(this.hf_FILE_LOG)
          endif
          if this.w_OK+this.w_NOK>0
            this.w_MESS = "Numero operazioni di conversione eseguite: %1%0Conversioni con esito positivo: %2%0Conversioni con esito negativo: %3%0%0Creato log-file: %4%0Vuoi visualizzarlo?%0"
            if ah_YesNo(this.w_MESS,"",alltrim(str(this.w_OK+this.w_NOK,4,0)),alltrim(str(this.w_OK,4,0)),alltrim(str(this.w_NOK,4,0)),this.w_FL)
              private LRunCMD
              LRunCMD = "/N4 NOTEPAD.EXE "+ this.sp_FILELOG
              run &LRunCMD
            else
              ah_ErrorMsg("Elaborazione terminata","!","")
            endif
          else
            ah_ErrorMsg("Elaborazione terminata","!","")
          endif
          * --- Aggiorna lista
          This.OparentObject.NotifyEvent("Legge")
        endif
      case this.pOper="ORIG"
        this.w_PROG = GSUT_MCO()
        * --- Controllo se ha passato il test di accesso 
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- inizializzo la chiave Manutenzione Tabella Conversioni
        this.w_PROG.w_COCODREL = this.oParentObject.w_CODREL
        * --- creo il curosre delle solo chiavi
        this.w_PROG.QueryKeySet("COCODREL='"+this.oParentObject.w_CODREL+ "'","")     
        * --- mi metto in interrogazione
        this.w_PROG.LoadRecWarn()     
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONVERSI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
