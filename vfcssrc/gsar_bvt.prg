* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bvt                                                        *
*              Controllo cancellazione vettore                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-26                                                      *
* Last revis.: 2012-03-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bvt",oParentObject)
return(i_retval)

define class tgsar_bvt as StdBatch
  * --- Local variables
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da GSAR_AVT: inibisce la cancellazione se il vettore � presente nei documenti.
    * --- Select from DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_MAST ";
          +" where MVCODVET="+cp_ToStrODBC(this.oParentObject.w_VTCODVET)+" OR MVCODVE2="+cp_ToStrODBC(this.oParentObject.w_VTCODVET)+" OR MVCODVE3="+cp_ToStrODBC(this.oParentObject.w_VTCODVET)+"";
           ,"_Curs_DOC_MAST")
    else
      select * from (i_cTable);
       where MVCODVET=this.oParentObject.w_VTCODVET OR MVCODVE2=this.oParentObject.w_VTCODVET OR MVCODVE3=this.oParentObject.w_VTCODVET;
        into cursor _Curs_DOC_MAST
    endif
    if used('_Curs_DOC_MAST')
      select _Curs_DOC_MAST
      locate for 1=1
      do while not(eof())
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_MsgFormat("Il vettore che si intende cancellare � presente nei documenti%0Impossibile cancellare")
      i_retcode = 'stop'
      return
        select _Curs_DOC_MAST
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
