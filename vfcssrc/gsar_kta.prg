* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kta                                                        *
*              Import/export tariffe                                           *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_88]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-08-06                                                      *
* Last revis.: 2014-04-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kta",oParentObject))

* --- Class definition
define class tgsar_kta as StdForm
  Top    = 11
  Left   = 14

  * --- Standard Properties
  Width  = 624
  Height = 454+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-04-11"
  HelpContextID=82409321
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  LISTINI_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsar_kta"
  cComment = "Import/export tariffe"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_RADSELIE1 = space(10)
  w_SELUNI = space(2)
  w_DBF1 = space(254)
  w_SELIVA = space(2)
  w_DBF4 = space(254)
  w_SELTIP = space(2)
  w_DBF6 = space(254)
  w_SELCAR = space(10)
  w_DBF5 = space(254)
  w_SELART = space(10)
  w_DBF2 = space(254)
  w_TIPART = space(2)
  w_CODPRE = space(20)
  w_CLASSTARIF = space(1)
  w_SOLOPAR = space(1)
  w_SELKEY = space(10)
  w_DBF3 = space(254)
  w_SELNOT = space(10)
  w_DBF8 = space(254)
  w_SELTAR = space(10)
  w_DBF7 = space(254)
  w_DBF77 = space(254)
  w_CODLIS = space(5)
  w_DESLIS = space(40)
  w_MSG = space(0)
  w_FLVERBOS = .F.
  w_DBF44 = space(200)
  w_OBTEST = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ktaPag1","gsar_kta",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Archivi")
      .Pages(2).addobject("oPag","tgsar_ktaPag2","gsar_kta",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Messaggi elaborazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRADSELIE1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='ART_ICOL'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RADSELIE1=space(10)
      .w_SELUNI=space(2)
      .w_DBF1=space(254)
      .w_SELIVA=space(2)
      .w_DBF4=space(254)
      .w_SELTIP=space(2)
      .w_DBF6=space(254)
      .w_SELCAR=space(10)
      .w_DBF5=space(254)
      .w_SELART=space(10)
      .w_DBF2=space(254)
      .w_TIPART=space(2)
      .w_CODPRE=space(20)
      .w_CLASSTARIF=space(1)
      .w_SOLOPAR=space(1)
      .w_SELKEY=space(10)
      .w_DBF3=space(254)
      .w_SELNOT=space(10)
      .w_DBF8=space(254)
      .w_SELTAR=space(10)
      .w_DBF7=space(254)
      .w_DBF77=space(254)
      .w_CODLIS=space(5)
      .w_DESLIS=space(40)
      .w_MSG=space(0)
      .w_FLVERBOS=.f.
      .w_DBF44=space(200)
      .w_OBTEST=ctod("  /  /  ")
        .w_RADSELIE1 = ' '
        .w_SELUNI = 'UN'
        .w_DBF1 = "Files_x_Import\UNIMIS.DBF"
        .w_SELIVA = 'VI'
        .w_DBF4 = "Files_x_Import\VOCIIVA.DBF"
        .w_SELTIP = 'TR'
        .w_DBF6 = "Files_x_Import\CLA_RIGD.DBF"
        .w_SELCAR = 'CA'
        .w_DBF5 = "Files_x_Import\CACOARTI.DBF"
        .w_SELART = 'AR'
        .w_DBF2 = "Files_x_Import\ART_ICOL.DBF"
        .w_TIPART = 'TT'
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODPRE))
          .link_1_23('Full')
        endif
        .w_CLASSTARIF = 'K'
        .w_SOLOPAR = ' '
        .w_SELKEY = 'KA'
        .w_DBF3 = "Files_x_Import\KEY_ARTI.DBF"
        .w_SELNOT = 'NA'
        .w_DBF8 = "Files_x_Import\NOT_ARTI.DBF"
        .w_SELTAR = 'TA'
        .w_DBF7 = "Files_x_Import\TAR_IFFE.DBF"
        .w_DBF77 = "Files_x_Import\TAR_DETT.DBF"
        .w_CODLIS = ''
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_CODLIS))
          .link_1_37('Full')
        endif
          .DoRTCalc(24,25,.f.)
        .w_FLVERBOS = .F.
        .w_DBF44 = "..\VEFA\EXE\TRS_DETT.DBF"
        .w_OBTEST = i_datsys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oTIPART_1_21.visible=!this.oPgFrm.Page1.oPag.oTIPART_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oCODPRE_1_23.visible=!this.oPgFrm.Page1.oPag.oCODPRE_1_23.mHide()
    this.oPgFrm.Page1.oPag.oCLASSTARIF_1_24.visible=!this.oPgFrm.Page1.oPag.oCLASSTARIF_1_24.mHide()
    this.oPgFrm.Page1.oPag.oSOLOPAR_1_25.visible=!this.oPgFrm.Page1.oPag.oSOLOPAR_1_25.mHide()
    this.oPgFrm.Page1.oPag.oCODLIS_1_37.visible=!this.oPgFrm.Page1.oPag.oCODLIS_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS_1_39.visible=!this.oPgFrm.Page1.oPag.oDESLIS_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODPRE
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODPRE))
          select ARCODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRE)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODPRE) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODPRE_1_23'),i_cWhere,'GSMA_AAS',"Prestazioni",'GSMA2AAS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODPRE)
            select ARCODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRE = NVL(_Link_.ARCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRE = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLIS
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_CODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_CODLIS))
          select LSCODLIS,LSDESLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_CODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_CODLIS)+"%");

            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oCODLIS_1_37'),i_cWhere,'GSAR_ALI',"Listini",'listob1.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_CODLIS)
            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRADSELIE1_1_1.RadioValue()==this.w_RADSELIE1)
      this.oPgFrm.Page1.oPag.oRADSELIE1_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELUNI_1_5.RadioValue()==this.w_SELUNI)
      this.oPgFrm.Page1.oPag.oSELUNI_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF1_1_6.value==this.w_DBF1)
      this.oPgFrm.Page1.oPag.oDBF1_1_6.value=this.w_DBF1
    endif
    if not(this.oPgFrm.Page1.oPag.oSELIVA_1_8.RadioValue()==this.w_SELIVA)
      this.oPgFrm.Page1.oPag.oSELIVA_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF4_1_9.value==this.w_DBF4)
      this.oPgFrm.Page1.oPag.oDBF4_1_9.value=this.w_DBF4
    endif
    if not(this.oPgFrm.Page1.oPag.oSELTIP_1_11.RadioValue()==this.w_SELTIP)
      this.oPgFrm.Page1.oPag.oSELTIP_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF6_1_12.value==this.w_DBF6)
      this.oPgFrm.Page1.oPag.oDBF6_1_12.value=this.w_DBF6
    endif
    if not(this.oPgFrm.Page1.oPag.oSELCAR_1_14.RadioValue()==this.w_SELCAR)
      this.oPgFrm.Page1.oPag.oSELCAR_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF5_1_15.value==this.w_DBF5)
      this.oPgFrm.Page1.oPag.oDBF5_1_15.value=this.w_DBF5
    endif
    if not(this.oPgFrm.Page1.oPag.oSELART_1_17.RadioValue()==this.w_SELART)
      this.oPgFrm.Page1.oPag.oSELART_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF2_1_18.value==this.w_DBF2)
      this.oPgFrm.Page1.oPag.oDBF2_1_18.value=this.w_DBF2
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPART_1_21.RadioValue()==this.w_TIPART)
      this.oPgFrm.Page1.oPag.oTIPART_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRE_1_23.value==this.w_CODPRE)
      this.oPgFrm.Page1.oPag.oCODPRE_1_23.value=this.w_CODPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASSTARIF_1_24.RadioValue()==this.w_CLASSTARIF)
      this.oPgFrm.Page1.oPag.oCLASSTARIF_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSOLOPAR_1_25.RadioValue()==this.w_SOLOPAR)
      this.oPgFrm.Page1.oPag.oSOLOPAR_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELKEY_1_26.RadioValue()==this.w_SELKEY)
      this.oPgFrm.Page1.oPag.oSELKEY_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF3_1_27.value==this.w_DBF3)
      this.oPgFrm.Page1.oPag.oDBF3_1_27.value=this.w_DBF3
    endif
    if not(this.oPgFrm.Page1.oPag.oSELNOT_1_29.RadioValue()==this.w_SELNOT)
      this.oPgFrm.Page1.oPag.oSELNOT_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF8_1_30.value==this.w_DBF8)
      this.oPgFrm.Page1.oPag.oDBF8_1_30.value=this.w_DBF8
    endif
    if not(this.oPgFrm.Page1.oPag.oSELTAR_1_32.RadioValue()==this.w_SELTAR)
      this.oPgFrm.Page1.oPag.oSELTAR_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF7_1_33.value==this.w_DBF7)
      this.oPgFrm.Page1.oPag.oDBF7_1_33.value=this.w_DBF7
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF77_1_35.value==this.w_DBF77)
      this.oPgFrm.Page1.oPag.oDBF77_1_35.value=this.w_DBF77
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLIS_1_37.value==this.w_CODLIS)
      this.oPgFrm.Page1.oPag.oCODLIS_1_37.value=this.w_CODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_39.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_39.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oMSG_2_1.value==this.w_MSG)
      this.oPgFrm.Page2.oPag.oMSG_2_1.value=this.w_MSG
    endif
    if not(this.oPgFrm.Page2.oPag.oFLVERBOS_2_2.RadioValue()==this.w_FLVERBOS)
      this.oPgFrm.Page2.oPag.oFLVERBOS_2_2.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DBF1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF1_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DBF1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF4_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DBF4)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF6))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF6_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DBF6)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF5))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF5_1_15.SetFocus()
            i_bnoObbl = !empty(.w_DBF5)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF2_1_18.SetFocus()
            i_bnoObbl = !empty(.w_DBF2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF3_1_27.SetFocus()
            i_bnoObbl = !empty(.w_DBF3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF8))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF8_1_30.SetFocus()
            i_bnoObbl = !empty(.w_DBF8)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF7))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF7_1_33.SetFocus()
            i_bnoObbl = !empty(.w_DBF7)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF77))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF77_1_35.SetFocus()
            i_bnoObbl = !empty(.w_DBF77)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF44))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF44_1_45.SetFocus()
            i_bnoObbl = !empty(.w_DBF44)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_ktaPag1 as StdContainer
  Width  = 620
  height = 456
  stdWidth  = 620
  stdheight = 456
  resizeXpos=371
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRADSELIE1_1_1 as StdRadio with uid="OKXXQIMNCR",rtseq=1,rtrep=.f.,left=147, top=19, width=415,height=17;
    , ToolTipText = "Seleziona import/export";
    , cFormVar="w_RADSELIE1", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oRADSELIE1_1_1.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Importazione da file DBF"
      this.Buttons(1).HelpContextID = 197005931
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Importazione da file DBF","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Esportazione su file DBF"
      this.Buttons(2).HelpContextID = 197005931
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Esportazione su file DBF","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",17)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona import/export")
      StdRadio::init()
    endproc

  func oRADSELIE1_1_1.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    space(10))))
  endfunc
  func oRADSELIE1_1_1.GetRadio()
    this.Parent.oContained.w_RADSELIE1 = this.RadioValue()
    return .t.
  endfunc

  func oRADSELIE1_1_1.SetRadio()
    this.Parent.oContained.w_RADSELIE1=trim(this.Parent.oContained.w_RADSELIE1)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELIE1=='I',1,;
      iif(this.Parent.oContained.w_RADSELIE1=='E',2,;
      0))
  endfunc

  add object oSELUNI_1_5 as StdCheck with uid="AOLULFBESB",rtseq=2,rtrep=.f.,left=18, top=78, caption="Unit� di misura",;
    ToolTipText = "Se attivato importa unit� di misura",;
    HelpContextID = 112159962,;
    cFormVar="w_SELUNI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELUNI_1_5.RadioValue()
    return(iif(this.value =1,'UN',;
    ' '))
  endfunc
  func oSELUNI_1_5.GetRadio()
    this.Parent.oContained.w_SELUNI = this.RadioValue()
    return .t.
  endfunc

  func oSELUNI_1_5.SetRadio()
    this.Parent.oContained.w_SELUNI=trim(this.Parent.oContained.w_SELUNI)
    this.value = ;
      iif(this.Parent.oContained.w_SELUNI=='UN',1,;
      0)
  endfunc

  add object oDBF1_1_6 as StdField with uid="ZPJIIFTPAA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DBF1", cQueryName = "DBF1",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 78893258,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=79, InputMask=replicate('X',254)


  add object oBtn_1_7 as StdButton with uid="MKUMAQUBOB",left=584, top=82, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 82208298;
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      with this.Parent.oContained
        .w_DBF1=left(getfile("dbf")+space(254),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELIVA_1_8 as StdCheck with uid="FHVCXPOMNU",rtseq=4,rtrep=.f.,left=18, top=105, caption="Voci IVA",;
    ToolTipText = "Se attivato importa le voci iva",;
    HelpContextID = 238775514,;
    cFormVar="w_SELIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELIVA_1_8.RadioValue()
    return(iif(this.value =1,'VI',;
    ' '))
  endfunc
  func oSELIVA_1_8.GetRadio()
    this.Parent.oContained.w_SELIVA = this.RadioValue()
    return .t.
  endfunc

  func oSELIVA_1_8.SetRadio()
    this.Parent.oContained.w_SELIVA=trim(this.Parent.oContained.w_SELIVA)
    this.value = ;
      iif(this.Parent.oContained.w_SELIVA=='VI',1,;
      0)
  endfunc

  add object oDBF4_1_9 as StdField with uid="MGDWVYBSAH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DBF4", cQueryName = "DBF4",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 78696650,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=106, InputMask=replicate('X',254)


  add object oBtn_1_10 as StdButton with uid="NAFLVXSQWA",left=584, top=109, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 82208298;
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      with this.Parent.oContained
        .w_DBF4=left(getfile("dbf")+space(254),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELTIP_1_11 as StdCheck with uid="RJIBLBUMGY",rtseq=6,rtrep=.f.,left=18, top=133, caption="Tipologie righe documenti ",;
    ToolTipText = "Se attivato importa le tipologie righe documenti ",;
    HelpContextID = 27866,;
    cFormVar="w_SELTIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELTIP_1_11.RadioValue()
    return(iif(this.value =1,'TR',;
    ' '))
  endfunc
  func oSELTIP_1_11.GetRadio()
    this.Parent.oContained.w_SELTIP = this.RadioValue()
    return .t.
  endfunc

  func oSELTIP_1_11.SetRadio()
    this.Parent.oContained.w_SELTIP=trim(this.Parent.oContained.w_SELTIP)
    this.value = ;
      iif(this.Parent.oContained.w_SELTIP=='TR',1,;
      0)
  endfunc

  add object oDBF6_1_12 as StdField with uid="NDRBSOJHWQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DBF6", cQueryName = "DBF6",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 78565578,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=134, InputMask=replicate('X',254)


  add object oBtn_1_13 as StdButton with uid="IZDHNNHZYV",left=584, top=134, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 82208298;
  , bGlobalFont=.t.

    proc oBtn_1_13.Click()
      with this.Parent.oContained
        .w_DBF6=left(getfile("dbf")+space(254),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELCAR_1_14 as StdCheck with uid="BJUUDCEUQA",rtseq=8,rtrep=.f.,left=18, top=160, caption="Categorie contabili",;
    ToolTipText = "Se attivato importa gli categorie contabili ",;
    HelpContextID = 244411610,;
    cFormVar="w_SELCAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELCAR_1_14.RadioValue()
    return(iif(this.value =1,'CA',;
    ' '))
  endfunc
  func oSELCAR_1_14.GetRadio()
    this.Parent.oContained.w_SELCAR = this.RadioValue()
    return .t.
  endfunc

  func oSELCAR_1_14.SetRadio()
    this.Parent.oContained.w_SELCAR=trim(this.Parent.oContained.w_SELCAR)
    this.value = ;
      iif(this.Parent.oContained.w_SELCAR=='CA',1,;
      0)
  endfunc

  add object oDBF5_1_15 as StdField with uid="YWLNADVOMD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DBF5", cQueryName = "DBF5",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 78631114,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=161, InputMask=replicate('X',254)


  add object oBtn_1_16 as StdButton with uid="FAWOVLEUNZ",left=584, top=164, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 82208298;
  , bGlobalFont=.t.

    proc oBtn_1_16.Click()
      with this.Parent.oContained
        .w_DBF5=left(getfile("dbf")+space(254),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELART_1_17 as StdCheck with uid="XRFZASRLRC",rtseq=10,rtrep=.f.,left=18, top=188, caption="Voci del tariffario",;
    ToolTipText = "Se attivato importa le voci del tariffario",;
    HelpContextID = 193162458,;
    cFormVar="w_SELART", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELART_1_17.RadioValue()
    return(iif(this.value =1,'AR',;
    ' '))
  endfunc
  func oSELART_1_17.GetRadio()
    this.Parent.oContained.w_SELART = this.RadioValue()
    return .t.
  endfunc

  func oSELART_1_17.SetRadio()
    this.Parent.oContained.w_SELART=trim(this.Parent.oContained.w_SELART)
    this.value = ;
      iif(this.Parent.oContained.w_SELART=='AR',1,;
      0)
  endfunc

  add object oDBF2_1_18 as StdField with uid="RLYZERYPVU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DBF2", cQueryName = "DBF2",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 78827722,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=189, InputMask=replicate('X',254)


  add object oBtn_1_19 as StdButton with uid="OAPUGTGTUT",left=584, top=192, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 82208298;
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      with this.Parent.oContained
        .w_DBF2=left(getfile("dbf")+space(254),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oTIPART_1_21 as StdCombo with uid="VOFFPHCECI",rtseq=12,rtrep=.f.,left=216,top=216,width=152,height=21;
    , HelpContextID = 193145034;
    , cFormVar="w_TIPART",RowSource=""+"A quantit� e valore,"+"A valore,"+"Descrittivo,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPART_1_21.RadioValue()
    return(iif(this.value =1,'FM',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'DE',;
    iif(this.value =4,'TT',;
    space(2))))))
  endfunc
  func oTIPART_1_21.GetRadio()
    this.Parent.oContained.w_TIPART = this.RadioValue()
    return .t.
  endfunc

  func oTIPART_1_21.SetRadio()
    this.Parent.oContained.w_TIPART=trim(this.Parent.oContained.w_TIPART)
    this.value = ;
      iif(this.Parent.oContained.w_TIPART=='FM',1,;
      iif(this.Parent.oContained.w_TIPART=='FO',2,;
      iif(this.Parent.oContained.w_TIPART=='DE',3,;
      iif(this.Parent.oContained.w_TIPART=='TT',4,;
      0))))
  endfunc

  func oTIPART_1_21.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE1='I' OR .w_SELART=' ')
    endwith
  endfunc

  add object oCODPRE_1_23 as StdField with uid="VUURNFEHLG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODPRE", cQueryName = "CODPRE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice prestazione",;
    HelpContextID = 175432666,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=422, Top=216, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_CODPRE"

  func oCODPRE_1_23.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE1='I' OR .w_SELART=' ')
    endwith
  endfunc

  func oCODPRE_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRE_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRE_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODPRE_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Prestazioni",'GSMA2AAS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODPRE_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODPRE
     i_obj.ecpSave()
  endproc


  add object oCLASSTARIF_1_24 as StdCombo with uid="TCXAXUPRQG",rtseq=14,rtrep=.f.,left=216,top=242,width=152,height=22;
    , ToolTipText = "Classificazione";
    , HelpContextID = 190958584;
    , cFormVar="w_CLASSTARIF",RowSource=""+"Onorario civile,"+"Onorario penale,"+"Onorario stragiudiziale,"+"Diritto stragiudiziale,"+"Diritto civile,"+"Prestazione generica,"+"Prestazione a tempo,"+"Spesa,"+"Anticipazione,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLASSTARIF_1_24.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    iif(this.value =3,'T',;
    iif(this.value =4,'R',;
    iif(this.value =5,'C',;
    iif(this.value =6,'G',;
    iif(this.value =7,'P',;
    iif(this.value =8,'S',;
    iif(this.value =9,'A',;
    iif(this.value =10,'K',;
    space(1))))))))))))
  endfunc
  func oCLASSTARIF_1_24.GetRadio()
    this.Parent.oContained.w_CLASSTARIF = this.RadioValue()
    return .t.
  endfunc

  func oCLASSTARIF_1_24.SetRadio()
    this.Parent.oContained.w_CLASSTARIF=trim(this.Parent.oContained.w_CLASSTARIF)
    this.value = ;
      iif(this.Parent.oContained.w_CLASSTARIF=='I',1,;
      iif(this.Parent.oContained.w_CLASSTARIF=='E',2,;
      iif(this.Parent.oContained.w_CLASSTARIF=='T',3,;
      iif(this.Parent.oContained.w_CLASSTARIF=='R',4,;
      iif(this.Parent.oContained.w_CLASSTARIF=='C',5,;
      iif(this.Parent.oContained.w_CLASSTARIF=='G',6,;
      iif(this.Parent.oContained.w_CLASSTARIF=='P',7,;
      iif(this.Parent.oContained.w_CLASSTARIF=='S',8,;
      iif(this.Parent.oContained.w_CLASSTARIF=='A',9,;
      iif(this.Parent.oContained.w_CLASSTARIF=='K',10,;
      0))))))))))
  endfunc

  func oCLASSTARIF_1_24.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE1='I' OR .w_SELART=' ')
    endwith
  endfunc


  add object oSOLOPAR_1_25 as StdCombo with uid="IGGZYHQJOR",value=3,rtseq=15,rtrep=.f.,left=422,top=242,width=153,height=22;
    , ToolTipText = "Se attivo, vengono riportate solo le tariffe con il parametro di riferimento per la liquidazione dei compensi da parte del giudice";
    , HelpContextID = 244671194;
    , cFormVar="w_SOLOPAR",RowSource=""+"Solo parametri 2012,"+"Solo parametri 2014,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSOLOPAR_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'4',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oSOLOPAR_1_25.GetRadio()
    this.Parent.oContained.w_SOLOPAR = this.RadioValue()
    return .t.
  endfunc

  func oSOLOPAR_1_25.SetRadio()
    this.Parent.oContained.w_SOLOPAR=trim(this.Parent.oContained.w_SOLOPAR)
    this.value = ;
      iif(this.Parent.oContained.w_SOLOPAR=='S',1,;
      iif(this.Parent.oContained.w_SOLOPAR=='4',2,;
      iif(this.Parent.oContained.w_SOLOPAR=='',3,;
      0)))
  endfunc

  func oSOLOPAR_1_25.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE1='I' OR .w_SELART=' ')
    endwith
  endfunc

  add object oSELKEY_1_26 as StdCheck with uid="PXMYNZXMED",rtseq=16,rtrep=.f.,left=18, top=270, caption="Codici di ricerca",;
    ToolTipText = "Se attivato importa i codici di ricerca",;
    HelpContextID = 122252506,;
    cFormVar="w_SELKEY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELKEY_1_26.RadioValue()
    return(iif(this.value =1,'KA',;
    ' '))
  endfunc
  func oSELKEY_1_26.GetRadio()
    this.Parent.oContained.w_SELKEY = this.RadioValue()
    return .t.
  endfunc

  func oSELKEY_1_26.SetRadio()
    this.Parent.oContained.w_SELKEY=trim(this.Parent.oContained.w_SELKEY)
    this.value = ;
      iif(this.Parent.oContained.w_SELKEY=='KA',1,;
      0)
  endfunc

  add object oDBF3_1_27 as StdField with uid="WYAZJRQMFB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DBF3", cQueryName = "DBF3",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 78762186,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=270, InputMask=replicate('X',254)


  add object oBtn_1_28 as StdButton with uid="FUNDBAHXGT",left=584, top=272, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 82208298;
  , bGlobalFont=.t.

    proc oBtn_1_28.Click()
      with this.Parent.oContained
        .w_DBF3=left(getfile("dbf")+space(254),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELNOT_1_29 as StdCheck with uid="NAQAYGDNDI",rtseq=18,rtrep=.f.,left=18, top=296, caption="Note prestazioni",;
    ToolTipText = "Se attivato importa note prestazioni",;
    HelpContextID = 195456218,;
    cFormVar="w_SELNOT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELNOT_1_29.RadioValue()
    return(iif(this.value =1,'NA',;
    ' '))
  endfunc
  func oSELNOT_1_29.GetRadio()
    this.Parent.oContained.w_SELNOT = this.RadioValue()
    return .t.
  endfunc

  func oSELNOT_1_29.SetRadio()
    this.Parent.oContained.w_SELNOT=trim(this.Parent.oContained.w_SELNOT)
    this.value = ;
      iif(this.Parent.oContained.w_SELNOT=='NA',1,;
      0)
  endfunc

  add object oDBF8_1_30 as StdField with uid="COMWHFPKRC",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DBF8", cQueryName = "DBF8",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 78434506,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=297, InputMask=replicate('X',254)


  add object oBtn_1_31 as StdButton with uid="EYMXADSUQA",left=584, top=300, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 82208298;
  , bGlobalFont=.t.

    proc oBtn_1_31.Click()
      with this.Parent.oContained
        .w_DBF8=left(getfile("dbf")+space(254),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELTAR_1_32 as StdCheck with uid="ODRGBFSSHR",rtseq=20,rtrep=.f.,left=18, top=320, caption="Tariffe",;
    ToolTipText = "Se attivato importa le tariffe",;
    HelpContextID = 243297498,;
    cFormVar="w_SELTAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELTAR_1_32.RadioValue()
    return(iif(this.value =1,'TA',;
    ' '))
  endfunc
  func oSELTAR_1_32.GetRadio()
    this.Parent.oContained.w_SELTAR = this.RadioValue()
    return .t.
  endfunc

  func oSELTAR_1_32.SetRadio()
    this.Parent.oContained.w_SELTAR=trim(this.Parent.oContained.w_SELTAR)
    this.value = ;
      iif(this.Parent.oContained.w_SELTAR=='TA',1,;
      0)
  endfunc

  add object oDBF7_1_33 as StdField with uid="SLHGDWQMAO",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DBF7", cQueryName = "DBF7",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 78500042,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=323, InputMask=replicate('X',254)


  add object oBtn_1_34 as StdButton with uid="IIWNPBRVGH",left=584, top=326, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 82208298;
  , bGlobalFont=.t.

    proc oBtn_1_34.Click()
      with this.Parent.oContained
        .w_DBF7=left(getfile("dbf")+space(254),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDBF77_1_35 as StdField with uid="LGTPUQKUMO",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DBF77", cQueryName = "DBF77",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 20828362,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=349, InputMask=replicate('X',254)


  add object oBtn_1_36 as StdButton with uid="SJCYGOUIXJ",left=584, top=352, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 82208298;
  , bGlobalFont=.t.

    proc oBtn_1_36.Click()
      with this.Parent.oContained
        .w_DBF77=left(getfile("dbf")+space(254),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODLIS_1_37 as StdField with uid="ILCJFXJKQX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CODLIS", cQueryName = "CODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino",;
    HelpContextID = 218686426,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=216, Top=376, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_CODLIS"

  func oCODLIS_1_37.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE1='I' OR .w_SELTAR=' ')
    endwith
  endfunc

  func oCODLIS_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODLIS_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODLIS_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oCODLIS_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'listob1.LISTINI_VZM',this.parent.oContained
  endproc
  proc oCODLIS_1_37.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_CODLIS
     i_obj.ecpSave()
  endproc

  add object oDESLIS_1_39 as StdField with uid="EJXZEVCCRD",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 218627530,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=272, Top=376, InputMask=replicate('X',40)

  func oDESLIS_1_39.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE1='I' OR .w_SELTAR=' ')
    endwith
  endfunc


  add object oBtn_1_40 as StdButton with uid="CZRHGAVNYF",left=499, top=410, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 82081194;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      with this.Parent.oContained
        gsar_bex(this.Parent.oContained,"ELABO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_RADSELIE1))
      endwith
    endif
  endfunc


  add object oBtn_1_41 as StdButton with uid="NYQDDFCUPJ",left=551, top=410, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 19157833;
    , caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_42 as StdButton with uid="LUHNMBSWMQ",left=8, top=411, width=48,height=45,;
    CpPicture="BMP\CHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutti gli archivi da importare/esportare";
    , HelpContextID = 249384154;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        gsar_bex(this.Parent.oContained,"SELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_43 as StdButton with uid="ABJFIATDMP",left=59, top=411, width=48,height=45,;
    CpPicture="BMP\UNCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutti gli archivi da importare/esportare";
    , HelpContextID = 249384154;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      with this.Parent.oContained
        gsar_bex(this.Parent.oContained,"DESEL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_44 as StdButton with uid="PDWIPENRCZ",left=110, top=411, width=48,height=45,;
    CpPicture="BMP\INVCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione degli archivi da importare/esportare";
    , HelpContextID = 249384154;
    , caption='\<Inv. sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      with this.Parent.oContained
        gsar_bex(this.Parent.oContained,"INVSE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="HKXMTIVULE",Visible=.t., Left=19, Top=39,;
    Alignment=0, Width=110, Height=15,;
    Caption="Selezione archivi"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="OJVLVUPTEC",Visible=.t., Left=216, Top=62,;
    Alignment=0, Width=34, Height=15,;
    Caption="PATH"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="GDGKWILYVC",Visible=.t., Left=120, Top=217,;
    Alignment=1, Width=90, Height=18,;
    Caption="Tipo prestazioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE1='I' OR .w_SELART=' ')
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="YDZCCTRVJG",Visible=.t., Left=367, Top=217,;
    Alignment=1, Width=51, Height=18,;
    Caption="Prestaz.:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE1='I' OR .w_SELART=' ')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="CPUAGFLGRY",Visible=.t., Left=169, Top=376,;
    Alignment=1, Width=40, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE1='I' OR .w_SELTAR=' ')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="JUAYSURWTP",Visible=.t., Left=110, Top=243,;
    Alignment=1, Width=100, Height=18,;
    Caption="Classificazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE1='I' OR .w_SELART=' ')
    endwith
  endfunc

  add object oBox_1_3 as StdBox with uid="RAIJTTRZDL",left=11, top=57, width=599,height=349
enddefine
define class tgsar_ktaPag2 as StdContainer
  Width  = 620
  height = 456
  stdWidth  = 620
  stdheight = 456
  resizeXpos=296
  resizeYpos=191
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMSG_2_1 as StdMemo with uid="QVQLZYFOGE",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 82095930,;
   bGlobalFont=.t.,;
    Height=337, Width=612, Left=2, Top=5, Readonly=.T.

  add object oFLVERBOS_2_2 as StdCheck with uid="GJREOHVBHZ",rtseq=26,rtrep=.f.,left=3, top=345, caption="Produci log dettagliato",;
    HelpContextID = 226412119,;
    cFormVar="w_FLVERBOS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLVERBOS_2_2.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVERBOS_2_2.GetRadio()
    this.Parent.oContained.w_FLVERBOS = this.RadioValue()
    return .t.
  endfunc

  func oFLVERBOS_2_2.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVERBOS==.T.,1,;
      0)
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kta','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
