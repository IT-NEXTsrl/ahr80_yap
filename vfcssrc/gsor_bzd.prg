* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_bzd                                                        *
*              Elabora zoom disponibilit�                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_268]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-24                                                      *
* Last revis.: 2015-06-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsor_bzd",oParentObject)
return(i_retval)

define class tgsor_bzd as StdBatch
  * --- Local variables
  w_DatIni = ctod("  /  /  ")
  w_PrimoGiorno = 0
  w_Messaggio = space(10)
  w_TmpVar = 0
  w_TipoPeri = space(1)
  w_ZOOM = .NULL.
  w_ZOOM1 = .NULL.
  w_ProgDisp = 0
  w_FLDETMOV = space(1)
  w_Scaduto = space(1)
  w_QTAPER = 0
  w_QTRPER = 0
  w_GRUMAG = space(5)
  w_COUNT = 0
  w_i = 0
  w_c = 0
  w_COLIDX = space(3)
  w_PPCOLORE = 0
  w_PPCOLTES = 0
  w_PPCOLSFO = 0
  * --- WorkFile variables
  ESERCIZI_idx=0
  GIORMAGA_idx=0
  MVM_MAST_idx=0
  DOC_MAST_idx=0
  SALDIART_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Disponibilita' Nel Tempo
    * --- Lanciato da GSOR_SZD
    * --- Variabili della maschera
    * --- Variabili locali
    this.w_DatIni = cp_CharToDate("  -  -  ")
    this.w_PrimoGiorno = val(sys(11,this.w_DATINI))
    this.w_Messaggio = ""
    this.w_TmpVar = 0
    this.w_TipoPeri = ""
    this.w_ZOOM = this.oParentObject.w_ZoomDis
    this.w_zoom.visible = .F.
    this.w_ZOOM1 = this.oParentObject.w_ZoomDis1
    this.w_zoom1.visible = .T.
    this.w_ProgDisp = this.oParentObject.w_slqtaper
    this.w_FLDETMOV = "N"
    this.w_Scaduto = "S"
    this.oParentObject.w_DISPONIB = 0
    this.w_QTAPER = 0
    this.w_QTRPER = 0
    this.w_GRUMAG = this.oparentobject.w_GRUMAG
    * --- Il riservato deve essere visualizzato solo dopo aver premuto ricerca
    this.oParentObject.w_RISERV = this.oParentObject.w_SLQTRPER
    * --- Questa condizione � falsa solo la prima volta che si esegue la Disponibilit� nel Tempo partendo dalla voce del men� Ordini
    if (NOT EMPTY(this.oParentObject.w_CODMAG) or NOT EMPTY(this.w_GRUMAG))AND NOT EMPTY(this.oParentObject.w_CODART)
      * --- Creo array dei colori
      CREATE CURSOR GESCOL(COLIDX C(4), COLSFO N(15), COLTES N(15))
      =WRCURSOR("GESCOL")
      vq_exec("query\GSMACSDG",this,"_Colori_")
      =WRCURSOR("_Colori_")
      if reccount("_Colori_")>1
        Delete From _Colori_ where PPOPEODL=0
      endif
      SELECT * FROM _Colori_ INTO ARRAY ArrColor
      this.w_COUNT = _TALLY
      this.w_COUNT = ALEN(ArrColor , 2) - 1
      For this.w_i = 1 to this.w_COUNT step 2
      this.w_c = this.w_i+1
      this.w_PPCOLORE = iif(ArrColor(1, this.w_c)=0,rgb(255,255,255),ArrColor(1, this.w_c))
      this.w_PPCOLTES = int(this.w_PPCOLORE/(rgb(255,255,255)+1))
      this.w_PPCOLSFO = mod(this.w_PPCOLORE,(rgb(255,255,255)+1))
      select GESCOL
      APPEND BLANK
      REPLACE COLIDX WITH ArrColor(1, this.w_i), COLSFO with this.w_PPCOLSFO , COLTES with this.w_PPCOLTES
      Next
      release ArrColor
      if used("_Colori_")
        use in _Colori_
      endif
      * --- Gestione tipo periodicita'
      do case
        case this.oParentObject.w_SELPER=1
          this.w_TipoPeri = "G"
        case this.oParentObject.w_SELPER=7
          this.w_TipoPeri = "S"
        case this.oParentObject.w_SelPer = 30
          this.w_TipoPeri = "M"
        case this.oParentObject.w_SelPer = 99
          this.w_TipoPeri = "G"
          this.w_FLDETMOV = "S"
          * --- Questo � il caso del Dettaglio Movimenti
      endcase
      * --- Creazione cursore
      ah_Msg("Calcolo disponibilit� articoli...",.T.)
      if this.w_FLDETMOV="N"
        vq_exec("query\GSMA4SDI",this,"__tmp__")
      else
        vq_exec("query\GSMA_SDG",this,"__tmp__")
      endif
      if g_PROD="S" AND Used("__TMP__")
        * --- Lettura Movimenti da ODL
        if this.w_FLDETMOV="N"
          vq_exec("..\COLA\EXE\QUERY\GSCO_SDI",this,"MOVIODL")
        else
          vq_exec("..\COLA\EXE\QUERY\GSCO2SDI",this,"MOVIODL")
        endif
        if used("MOVIODL")
          * --- Mette in Union con __tmp__
          if this.oParentObject.w_SelPer = 99
            select __Tmp__.*, GESCOL.colsfo, GESCOL.coltes from __Tmp__ inner join GESCOL on __Tmp__.MVICOLSF=GESCOL.COLIDX ; 
 union ALL ; 
 select MOVIODL.*, GESCOL.colsfo, GESCOL.coltes from MOVIODL inner join GESCOL on MOVIODL.MVICOLSF=GESCOL.COLIDX ; 
 into cursor __Tmp__
          else
            select * from __Tmp__ ; 
 union ALL ; 
 select * from MOVIODL ; 
 into cursor __Tmp__
          endif
          USE IN SELECT("MOVIODL")
        endif
      else
        select __TMP__.*, 16777215 as COLSFO, 0 as COLTES FROM __TMP__ INTO CURSOR __TMP__
      endif
      if reccount("__tmp__")=0
        if this.w_FLDETMOV="N"
          select ( this.w_ZOOM.cCursor )
          DELETE FROM ( this.w_ZOOM.cCursor )
          this.w_zoom1.visible = .F.
          this.w_zoom.visible = .T.
          go top
          this.w_zoom.refresh()
        else
          select ( this.w_ZOOM1.cCursor )
          DELETE FROM ( this.w_ZOOM1.cCursor )
          this.w_zoom.visible = .F.
          this.w_zoom1.visible = .T.
          go top
          this.w_zoom1.refresh()
        endif
        * --- Read from SALDIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SLQTAPER,SLQTRPER"+;
            " from "+i_cTable+" SALDIART where ";
                +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SLQTAPER,SLQTRPER;
            from (i_cTable) where;
                SLCODICE = this.oParentObject.w_CODART;
                and SLCODMAG = this.oParentObject.w_CODMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_QTAPER = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
          this.w_QTRPER = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows=0
          ah_ErrorMsg("L'articolo non � mai stato movimentato",,"")
        else
          this.oParentObject.w_DISPONIB = this.w_QTAPER - this.w_QTRPER
          this.oParentObject.w_DATFIN = ELABPDT(i_datsys, this.w_TipoPeri, this.w_DatIni, this.oParentObject.w_DatFin, "I") - 1
        endif
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      * --- Seleziona la data da prendere come riferimento fra quella di registrazione e quella di prevista evasione
      Select *, cp_todate(MVDATEVA) as XXDATREG ;
      from __tmp__ ;
      into cursor __tmp__
      * --- Aggiunge i periodi di riferimento
      * --- Non � possibile fare un'unica select con la seguente perch� il fox entra in loop.
      Select *, ;
      ELABPDT(xxdatreg, this.w_TipoPeri, this.w_DatIni, this.oParentObject.w_DatFin, "I") as PeriIniz, ;
      ELABPDT(xxdatreg, this.w_TipoPeri, this.w_DatIni, this.oParentObject.w_DatFin, "F") as PeriFine, ;
      iif( this.oParentObject.w_SelPer=30, (year(XXDATREG)*100)+month(XXDATREG) , ;
      iif( this.oParentObject.w_SelPer=7, (year(XXDATREG)*100)+week(XXDATREG) , ;
      int( ( val(sys(11,XXDATREG)) - this.w_PrimoGiorno ) / iif(this.oParentObject.w_SelPer=99,1,this.oParentObject.w_SelPer) ) )) as periodo ;
      from __tmp__ ;
      into cursor __tmp__
      * --- Raggruppa i dati secondo il criterio selezionato
      if this.w_FLDETMOV="N"
        Select ;
        sum(Ordinato) as Ordinato,;
        sum(Impegnato) as Impegnato,;
        sum(Riservato) as Riservato,;
        cprownum, xxdatreg, " " AS MVTIPCON, SPACE(15) AS MVCODCON, SPACE(40) AS ANDESCRI, PeriIniz, PeriFine, Periodo ;
        from __tmp__ ;
        group by periiniz ;
        order by periiniz ;
        into cursor __tmp__
      else
        Select ;
        Ordinato,;
        Impegnato,;
        Riservato,;
        cprownum, MVTIPCON, MVCODCON, ANDESCRI, xxdatreg, MVTIPDOC, NumDoc, AlfDoc, Stato, CauMag, PeriIniz, PeriFine, Mvserial, Numrif, Periodo, Olcododl , colsfo, coltes ;
        from __tmp__ ;
        order by periiniz,XXDATREG,NUMDOC,ALFDOC,MVSERIAL ;
        into cursor __tmp__
      endif
      * --- Aggiorna il cursore dello zoom
      * --- La disponibilit� � inizializzata con il valore : Esistenza - Riservato (contenuti nei saldi)
      this.w_ProgDisp = this.oParentObject.w_slqtaper - this.oParentObject.w_SLQTRPER
      if this.w_FLDETMOV="N"
        DELETE FROM ( this.w_ZOOM.cCursor )
      else
        DELETE FROM ( this.w_ZOOM1.cCursor )
      endif
      select __tmp__
      go top
      do while .not. eof()
        * --- Assegna alla Disponibilit� presente nella testata della maschera il valore dato da:
        * --- Esistenza + Totale righe Ordinato Scadute - Totale Righe Impegnato Scadute.
        * --- Assegna alla data della maschera il giorno prima a quello della riga non ancora scaduta.
        if this.w_Scaduto="S" and PeriFine>=i_datsys
          this.oParentObject.w_DISPONIB = this.w_ProgDisp
          this.w_Scaduto = "N"
        endif
        this.w_ProgDisp = this.w_ProgDisp + Ordinato - Impegnato
        * --- La Disponibilit� delle righe scadute � sempre zero
        * --- non ha senso indicare la disponibilit� nel passato senza ricalcolare l'esistenza
        * --- inoltre avrebbe una valenza solamente statistica
        if this.w_FLDETMOV="N"
          if this.oParentObject.w_SELMOV = "T" or PERIFINE >= I_DATSYS
            * --- Se � stato scelto visualizza tutti i movimenti o se il movimento non � scaduto
            if (EMPTY(this.oParentObject.w_MDDAT) AND (EMPTY(this.oParentObject.w_MADAT) OR this.oParentObject.w_MADAT>=PERIFINE)) OR (this.oParentObject.w_MDDAT<=PERIINIZ AND (EMPTY(this.oParentObject.w_MADAT) OR this.oParentObject.w_MADAT>=PERIFINE))
              * --- Se il movimento appartiene ad un periodo compreso nei filtri da Data a Data
              insert into ( this.w_Zoom.cCursor ) (PeriIniz,PeriFine,Ordinato,Impegnato,Riservato,Disponibilita,Scaduto,Serial,Numrif,slcodice,slcodmag) values ;
              ( cp_todate(__tmp__.PeriIniz) , ;
               cp_todate( __tmp__.PeriFine) , ;
              __TMP__.Ordinato , ;
              __TMP__.Impegnato , ;
              __TMP__.Riservato , ;
              iif(__tmp__.perifine<i_datsys,0,this.w_ProgDisp), ;
              this.w_Scaduto ,"0",0,this.oParentObject.w_codart,this.oParentObject.w_codmag) 
            endif
          endif
        else
          if this.oParentObject.w_SELMOV = "T" or PERIFINE >= I_DATSYS
            * --- Se � stato scelto visualizza tutti i movimenti o se il movimento non � scaduto
            if (EMPTY(this.oParentObject.w_MDDAT) AND (EMPTY(this.oParentObject.w_MADAT) OR this.oParentObject.w_MADAT>=PERIFINE)) OR (this.oParentObject.w_MDDAT<=PERIINIZ AND (EMPTY(this.oParentObject.w_MADAT) OR this.oParentObject.w_MADAT>=PERIFINE))
              * --- Se il movimento appartiene ad un periodo compreso nei filtri da Data a Data
              insert into ( this.w_Zoom1.cCursor ) ;
              (mvtipdoc, Mvnumdoc, Mvalfdoc,Stato, olcododl, Mvtcamag, MVTIPCON, MVCODCON, ANDESCRI, PeriIniz,Ordinato,Impegnato,Riservato,Disponibilita,Scaduto,Serial,Numrif,Serial1, colsfo, coltes) values ;
              ( NVL( __TMP__.mvtipdoc , " " ) , ;
              __TMP__.Numdoc , ;
              __TMP__.Alfdoc , ;
              NVL( __TMP__.stato , " " ) , ;
              __TMP__.OLCODODL , ;
              __TMP__.Caumag , ;
              NVL( __TMP__.mvtipcon , " " ) , ;
              NVL( __TMP__.mvcodcon , SPACE( 15 ) ) , ;
              NVL( __TMP__.andescri , SPACE( 40 ) ) , ;
               cp_todate(__tmp__.PeriIniz) , ;
              __TMP__.Ordinato , ;
              __TMP__.Impegnato , ;
              __TMP__.Riservato , ;
              iif(__tmp__.perifine<i_datsys,0,this.w_ProgDisp) , ;
              this.w_Scaduto , ;
              __TMP__.Mvserial , ;
              __TMP__.Numrif , ;
              __TMP__.OLCODODL , ;
              __TMP__.COLSFO, ;
              __TMP__.COLTES)
            endif
          endif
        endif
        select __tmp__
        skip
      enddo
      * --- Nel caso in cui tutti i movimenti siano scaduti assegna il valore giusto (l'ultimo) alla Disponibilit�
      if this.w_Scaduto="S"
        this.oParentObject.w_DISPONIB = this.w_ProgDisp
      endif
      * --- La data in testata viene valorizzata con la data della fine del periodo che precede quello contente la data di sistema
      this.oParentObject.w_DATFIN = ELABPDT(i_datsys, this.w_TipoPeri, this.w_DatIni, this.oParentObject.w_DatFin, "I") - 1
      * --- Si riposiziona all'inizio del cursore
      if this.w_FLDETMOV="N"
        select ( this.w_ZOOM.cCursor )
      else
        select ( this.w_ZOOM1.cCursor )
      endif
      go top
      * --- Refresh della Griglia
      if this.w_FLDETMOV="N"
        this.w_zoom1.visible = .F.
        this.w_zoom.visible = .T.
        this.w_zoom.refresh()
      else
        this.w_zoom.visible = .F.
        this.w_zoom1.visible = .T.
        this.w_zoom1.refresh()
      endif
      * --- Chiusura cursori
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_zoom.visible = .F.
      this.w_zoom1.visible = .T.
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursori
    if used("__tmp__")
      select __tmp__
      use
    endif
    if used("MOVIODL")
      select MOVIODL
      use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='GIORMAGA'
    this.cWorkTables[3]='MVM_MAST'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='SALDIART'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
