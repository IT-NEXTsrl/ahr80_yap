* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mac                                                        *
*              Movimenti cespiti associati                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_124]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-13                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mac")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mac")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mac")
  return

* --- Class definition
define class tgscg_mac as StdPCForm
  Width  = 335
  Height = 270
  Top    = 10
  Left   = 12
  cComment = "Movimenti cespiti associati"
  cPrg = "gscg_mac"
  HelpContextID=130688361
  add object cnt as tcgscg_mac
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mac as PCContext
  w_ACSERIAL = space(10)
  w_CPROWORD = 0
  w_ACMOVCES = space(10)
  w_CESPRES = space(1)
  w_PNUMREG = 0
  w_PCODESE = space(10)
  w_PDATREG = space(8)
  w_ACTIPASS = space(1)
  w_MCSERIAL = space(10)
  w_PNCODCON = space(15)
  w_PNTIPCON = space(1)
  w_CAUCES = space(15)
  w_PNDATREG = space(8)
  w_PNCODESE = space(4)
  w_PNNUMDOC = 0
  w_PNALFDOC = space(10)
  w_PNDATDOC = space(8)
  w_MOVCES = space(1)
  w_HASEVENT = space(1)
  w_PNTIPREG = space(1)
  w_SER__PNT = space(10)
  proc Save(i_oFrom)
    this.w_ACSERIAL = i_oFrom.w_ACSERIAL
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_ACMOVCES = i_oFrom.w_ACMOVCES
    this.w_CESPRES = i_oFrom.w_CESPRES
    this.w_PNUMREG = i_oFrom.w_PNUMREG
    this.w_PCODESE = i_oFrom.w_PCODESE
    this.w_PDATREG = i_oFrom.w_PDATREG
    this.w_ACTIPASS = i_oFrom.w_ACTIPASS
    this.w_MCSERIAL = i_oFrom.w_MCSERIAL
    this.w_PNCODCON = i_oFrom.w_PNCODCON
    this.w_PNTIPCON = i_oFrom.w_PNTIPCON
    this.w_CAUCES = i_oFrom.w_CAUCES
    this.w_PNDATREG = i_oFrom.w_PNDATREG
    this.w_PNCODESE = i_oFrom.w_PNCODESE
    this.w_PNNUMDOC = i_oFrom.w_PNNUMDOC
    this.w_PNALFDOC = i_oFrom.w_PNALFDOC
    this.w_PNDATDOC = i_oFrom.w_PNDATDOC
    this.w_MOVCES = i_oFrom.w_MOVCES
    this.w_HASEVENT = i_oFrom.w_HASEVENT
    this.w_PNTIPREG = i_oFrom.w_PNTIPREG
    this.w_SER__PNT = i_oFrom.w_SER__PNT
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_ACSERIAL = this.w_ACSERIAL
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_ACMOVCES = this.w_ACMOVCES
    i_oTo.w_CESPRES = this.w_CESPRES
    i_oTo.w_PNUMREG = this.w_PNUMREG
    i_oTo.w_PCODESE = this.w_PCODESE
    i_oTo.w_PDATREG = this.w_PDATREG
    i_oTo.w_ACTIPASS = this.w_ACTIPASS
    i_oTo.w_MCSERIAL = this.w_MCSERIAL
    i_oTo.w_PNCODCON = this.w_PNCODCON
    i_oTo.w_PNTIPCON = this.w_PNTIPCON
    i_oTo.w_CAUCES = this.w_CAUCES
    i_oTo.w_PNDATREG = this.w_PNDATREG
    i_oTo.w_PNCODESE = this.w_PNCODESE
    i_oTo.w_PNNUMDOC = this.w_PNNUMDOC
    i_oTo.w_PNALFDOC = this.w_PNALFDOC
    i_oTo.w_PNDATDOC = this.w_PNDATDOC
    i_oTo.w_MOVCES = this.w_MOVCES
    i_oTo.w_HASEVENT = this.w_HASEVENT
    i_oTo.w_PNTIPREG = this.w_PNTIPREG
    i_oTo.w_SER__PNT = this.w_SER__PNT
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mac as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 335
  Height = 270
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=130688361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PNT_CESP_IDX = 0
  cFile = "PNT_CESP"
  cKeySelect = "ACSERIAL"
  cKeyWhere  = "ACSERIAL=this.w_ACSERIAL"
  cKeyDetail  = "ACSERIAL=this.w_ACSERIAL and ACMOVCES=this.w_ACMOVCES and ACTIPASS=this.w_ACTIPASS"
  cKeyWhereODBC = '"ACSERIAL="+cp_ToStrODBC(this.w_ACSERIAL)';

  cKeyDetailWhereODBC = '"ACSERIAL="+cp_ToStrODBC(this.w_ACSERIAL)';
      +'+" and ACMOVCES="+cp_ToStrODBC(this.w_ACMOVCES)';
      +'+" and ACTIPASS="+cp_ToStrODBC(this.w_ACTIPASS)';

  cKeyWhereODBCqualified = '"PNT_CESP.ACSERIAL="+cp_ToStrODBC(this.w_ACSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PNT_CESP.CPROWORD '
  cPrg = "gscg_mac"
  cComment = "Movimenti cespiti associati"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ACSERIAL = space(10)
  w_CPROWORD = 0
  w_ACMOVCES = space(10)
  o_ACMOVCES = space(10)
  w_CESPRES = .F.
  w_PNUMREG = 0
  w_PCODESE = space(10)
  w_PDATREG = ctod('  /  /  ')
  w_ACTIPASS = space(1)
  w_MCSERIAL = space(10)
  w_PNCODCON = space(15)
  w_PNTIPCON = space(1)
  w_CAUCES = space(15)
  w_PNDATREG = ctod('  /  /  ')
  w_PNCODESE = space(4)
  w_PNNUMDOC = 0
  w_PNALFDOC = space(10)
  w_PNDATDOC = ctod('  /  /  ')
  w_MOVCES = space(1)
  w_HASEVENT = .F.
  w_PNTIPREG = space(1)
  w_SER__PNT = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_macPag1","gscg_mac",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PNT_CESP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PNT_CESP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PNT_CESP_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mac'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PNT_CESP where ACSERIAL=KeySet.ACSERIAL
    *                            and ACMOVCES=KeySet.ACMOVCES
    *                            and ACTIPASS=KeySet.ACTIPASS
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PNT_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_CESP_IDX,2],this.bLoadRecFilter,this.PNT_CESP_IDX,"gscg_mac")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PNT_CESP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PNT_CESP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PNT_CESP '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ACSERIAL',this.w_ACSERIAL  )
      select * from (i_cTable) PNT_CESP where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ACSERIAL = NVL(ACSERIAL,space(10))
        .w_PNCODCON = THIS.oParentObject .w_PNCODCLF
        .w_PNTIPCON = THIS.oParentObject .w_PNTIPCLF
        .w_CAUCES = THIS.oParentObject .w_CAUCES
        .w_PNDATREG = THIS.oParentObject .w_PNDATREG
        .w_PNCODESE = THIS.oParentObject .w_PNCODESE
        .w_PNNUMDOC = THIS.oParentObject .w_PNNUMDOC
        .w_PNALFDOC = THIS.oParentObject .w_PNALFDOC
        .w_PNDATDOC = THIS.oParentObject .w_PNDATDOC
        .w_MOVCES = THIS.oParentObject .w_MOVCES
        .w_PNTIPREG = THIS.oParentObject .w_PNTIPREG
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PNT_CESP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CESPRES = .f.
          .w_PNUMREG = 0
          .w_PCODESE = space(10)
          .w_PDATREG = ctod("  /  /  ")
        .w_HASEVENT = .T.
        .w_SER__PNT = THIS.oParentObject .w_PNSERIAL
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_ACMOVCES = NVL(ACMOVCES,space(10))
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_3.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_4.Calculate()
          .w_ACTIPASS = NVL(ACTIPASS,space(1))
        .w_MCSERIAL = .w_ACMOVCES
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace ACMOVCES with .w_ACMOVCES
          replace ACTIPASS with .w_ACTIPASS
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_PNCODCON = THIS.oParentObject .w_PNCODCLF
        .w_PNTIPCON = THIS.oParentObject .w_PNTIPCLF
        .w_CAUCES = THIS.oParentObject .w_CAUCES
        .w_PNDATREG = THIS.oParentObject .w_PNDATREG
        .w_PNCODESE = THIS.oParentObject .w_PNCODESE
        .w_PNNUMDOC = THIS.oParentObject .w_PNNUMDOC
        .w_PNALFDOC = THIS.oParentObject .w_PNALFDOC
        .w_PNDATDOC = THIS.oParentObject .w_PNDATDOC
        .w_MOVCES = THIS.oParentObject .w_MOVCES
        .w_PNTIPREG = THIS.oParentObject .w_PNTIPREG
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_11.enabled = .oPgFrm.Page1.oPag.oBtn_2_11.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_ACSERIAL=space(10)
      .w_CPROWORD=10
      .w_ACMOVCES=space(10)
      .w_CESPRES=.f.
      .w_PNUMREG=0
      .w_PCODESE=space(10)
      .w_PDATREG=ctod("  /  /  ")
      .w_ACTIPASS=space(1)
      .w_MCSERIAL=space(10)
      .w_PNCODCON=space(15)
      .w_PNTIPCON=space(1)
      .w_CAUCES=space(15)
      .w_PNDATREG=ctod("  /  /  ")
      .w_PNCODESE=space(4)
      .w_PNNUMDOC=0
      .w_PNALFDOC=space(10)
      .w_PNDATDOC=ctod("  /  /  ")
      .w_MOVCES=space(1)
      .w_HASEVENT=.f.
      .w_PNTIPREG=space(1)
      .w_SER__PNT=space(10)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_3.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_4.Calculate()
        .DoRTCalc(1,8,.f.)
        .w_MCSERIAL = .w_ACMOVCES
        .w_PNCODCON = THIS.oParentObject .w_PNCODCLF
        .w_PNTIPCON = THIS.oParentObject .w_PNTIPCLF
        .w_CAUCES = THIS.oParentObject .w_CAUCES
        .w_PNDATREG = THIS.oParentObject .w_PNDATREG
        .w_PNCODESE = THIS.oParentObject .w_PNCODESE
        .w_PNNUMDOC = THIS.oParentObject .w_PNNUMDOC
        .w_PNALFDOC = THIS.oParentObject .w_PNALFDOC
        .w_PNDATDOC = THIS.oParentObject .w_PNDATDOC
        .w_MOVCES = THIS.oParentObject .w_MOVCES
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Calculate()
        .w_HASEVENT = .T.
        .w_PNTIPREG = THIS.oParentObject .w_PNTIPREG
        .w_SER__PNT = THIS.oParentObject .w_PNSERIAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PNT_CESP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_11.enabled = this.oPgFrm.Page1.oPag.oBtn_2_11.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_2_11.enabled = .Page1.oPag.oBtn_2_11.mCond()
      .Page1.oPag.oBtn_2_13.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_3.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_4.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PNT_CESP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PNT_CESP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACSERIAL,"ACSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_PNUMREG N(6);
      ,t_PCODESE C(10);
      ,t_PDATREG D(8);
      ,t_ACTIPASS C(1);
      ,ACMOVCES C(10);
      ,ACTIPASS C(1);
      ,t_ACMOVCES C(10);
      ,t_CESPRES L(1);
      ,t_MCSERIAL C(10);
      ,t_HASEVENT L(1);
      ,t_SER__PNT C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_macbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPNUMREG_2_6.controlsource=this.cTrsName+'.t_PNUMREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCODESE_2_7.controlsource=this.cTrsName+'.t_PCODESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPDATREG_2_8.controlsource=this.cTrsName+'.t_PDATREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oACTIPASS_2_9.controlsource=this.cTrsName+'.t_ACTIPASS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(47)
    this.AddVLine(102)
    this.AddVLine(181)
    this.AddVLine(256)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PNT_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_CESP_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PNT_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_CESP_IDX,2])
      *
      * insert into PNT_CESP
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PNT_CESP')
        i_extval=cp_InsertValODBCExtFlds(this,'PNT_CESP')
        i_cFldBody=" "+;
                  "(ACSERIAL,CPROWORD,ACMOVCES,ACTIPASS,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_ACSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_ACMOVCES)+","+cp_ToStrODBC(this.w_ACTIPASS)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PNT_CESP')
        i_extval=cp_InsertValVFPExtFlds(this,'PNT_CESP')
        cp_CheckDeletedKey(i_cTable,0,'ACSERIAL',this.w_ACSERIAL,'ACMOVCES',this.w_ACMOVCES,'ACTIPASS',this.w_ACTIPASS)
        INSERT INTO (i_cTable) (;
                   ACSERIAL;
                  ,CPROWORD;
                  ,ACMOVCES;
                  ,ACTIPASS;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_ACSERIAL;
                  ,this.w_CPROWORD;
                  ,this.w_ACMOVCES;
                  ,this.w_ACTIPASS;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PNT_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_CESP_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_ACMOVCES))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PNT_CESP')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and ACMOVCES="+cp_ToStrODBC(&i_TN.->ACMOVCES)+;
                 " and ACTIPASS="+cp_ToStrODBC(&i_TN.->ACTIPASS)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PNT_CESP')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and ACMOVCES=&i_TN.->ACMOVCES;
                      and ACTIPASS=&i_TN.->ACTIPASS;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_ACMOVCES))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and ACMOVCES="+cp_ToStrODBC(&i_TN.->ACMOVCES)+;
                            " and ACTIPASS="+cp_ToStrODBC(&i_TN.->ACTIPASS)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and ACMOVCES=&i_TN.->ACMOVCES;
                            and ACTIPASS=&i_TN.->ACTIPASS;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace ACMOVCES with this.w_ACMOVCES
              replace ACTIPASS with this.w_ACTIPASS
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PNT_CESP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PNT_CESP')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",ACMOVCES="+cp_ToStrODBC(this.w_ACMOVCES)+;
                     ",ACTIPASS="+cp_ToStrODBC(this.w_ACTIPASS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and ACMOVCES="+cp_ToStrODBC(ACMOVCES)+;
                             " and ACTIPASS="+cp_ToStrODBC(ACTIPASS)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PNT_CESP')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,ACMOVCES=this.w_ACMOVCES;
                     ,ACTIPASS=this.w_ACTIPASS;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and ACMOVCES=&i_TN.->ACMOVCES;
                                      and ACTIPASS=&i_TN.->ACTIPASS;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PNT_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_CESP_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_ACMOVCES))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PNT_CESP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and ACMOVCES="+cp_ToStrODBC(&i_TN.->ACMOVCES)+;
                            " and ACTIPASS="+cp_ToStrODBC(&i_TN.->ACTIPASS)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and ACMOVCES=&i_TN.->ACMOVCES;
                              and ACTIPASS=&i_TN.->ACTIPASS;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_ACMOVCES))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PNT_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_CESP_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_3.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_4.Calculate()
        .DoRTCalc(1,8,.t.)
        if .o_ACMOVCES<>.w_ACMOVCES
          .w_MCSERIAL = .w_ACMOVCES
        endif
          .w_PNCODCON = THIS.oParentObject .w_PNCODCLF
          .w_PNTIPCON = THIS.oParentObject .w_PNTIPCLF
          .w_CAUCES = THIS.oParentObject .w_CAUCES
          .w_PNDATREG = THIS.oParentObject .w_PNDATREG
          .w_PNCODESE = THIS.oParentObject .w_PNCODESE
          .w_PNNUMDOC = THIS.oParentObject .w_PNNUMDOC
          .w_PNALFDOC = THIS.oParentObject .w_PNALFDOC
          .w_PNDATDOC = THIS.oParentObject .w_PNDATDOC
          .w_MOVCES = THIS.oParentObject .w_MOVCES
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Calculate()
        .DoRTCalc(19,19,.t.)
          .w_PNTIPREG = THIS.oParentObject .w_PNTIPREG
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_ACMOVCES with this.w_ACMOVCES
      replace t_CESPRES with this.w_CESPRES
      replace t_MCSERIAL with this.w_MCSERIAL
      replace t_HASEVENT with this.w_HASEVENT
      replace t_SER__PNT with this.w_SER__PNT
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_3.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_4.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_3.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_4.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPNUMREG_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPNUMREG_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCODESE_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCODESE_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPDATREG_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPDATREG_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_11.enabled =this.oPgFrm.Page1.oPag.oBtn_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_13.enabled =this.oPgFrm.Page1.oPag.oBtn_2_13.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_3.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_4.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNUMREG_2_6.value==this.w_PNUMREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNUMREG_2_6.value=this.w_PNUMREG
      replace t_PNUMREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNUMREG_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCODESE_2_7.value==this.w_PCODESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCODESE_2_7.value=this.w_PCODESE
      replace t_PCODESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCODESE_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDATREG_2_8.value==this.w_PDATREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDATREG_2_8.value=this.w_PDATREG
      replace t_PDATREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDATREG_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACTIPASS_2_9.value==this.w_ACTIPASS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACTIPASS_2_9.value=this.w_ACTIPASS
      replace t_ACTIPASS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACTIPASS_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'PNT_CESP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_mac
      If g_CESP='S' and not empty(.w_ACMOVCES)
        .NotifyEvent('Valorizzacespiti2')
      Endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_ACMOVCES))
        * --- Area Manuale = Check Row
        * --- gscg_mac
        * Controllo che il movimento cespite inserito non sia gia presente
        * nel dettaglio
        Local L_Posizione,L_Area,L_Codces,L_Trovato,L_oldcodces
        
        L_Area = Select()
        
        Select (this.cTrsName)
        L_Posizione=Recno(this.cTrsName)
        L_Codces= t_ACMOVCES
        L_oldcodces = ACMOVCES
        
        if L_Posizione<>0 And Not Empty(L_Codces)
        	
        	Go Top
        	
        	LOCATE FOR t_ACMOVCES = L_Codces And Not Deleted()
        	
        	L_Trovato=Found()
        	
        	if L_Trovato And Recno()<>L_Posizione
        		i_bRes = .f.
        		i_bnoChk = .f.
        		i_cErrorMsg = ah_MsgFormat("Movimento cespite gi� utilizzato nel dettaglio")
        	ELse
        		if L_Trovato
        			Continue
        			if Found() And Recno()<>L_Posizione
        				i_bRes = .f.
        				i_bnoChk = .f.
        				i_cErrorMsg = ah_MsgFormat("Movimento Cespite gi� utilizzato nel dettaglio")
        			endif
        		Endif
        	Endif
        
        	* mi riposiziono nella riga di partenza
        	Select (this.cTrsName)
        	Go L_Posizione
        endif
        			
        * mi rimetto nella vecchia area
        Select (L_Area)
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ACMOVCES = this.w_ACMOVCES
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_ACMOVCES)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_ACMOVCES=space(10)
      .w_CESPRES=.f.
      .w_PNUMREG=0
      .w_PCODESE=space(10)
      .w_PDATREG=ctod("  /  /  ")
      .w_ACTIPASS=space(1)
      .w_MCSERIAL=space(10)
      .w_HASEVENT=.f.
      .w_SER__PNT=space(10)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_3.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_4.Calculate()
      .DoRTCalc(1,8,.f.)
        .w_MCSERIAL = .w_ACMOVCES
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_12.Calculate()
      .DoRTCalc(10,18,.f.)
        .w_HASEVENT = .T.
      .DoRTCalc(20,20,.f.)
        .w_SER__PNT = THIS.oParentObject .w_PNSERIAL
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_ACMOVCES = t_ACMOVCES
    this.w_CESPRES = t_CESPRES
    this.w_PNUMREG = t_PNUMREG
    this.w_PCODESE = t_PCODESE
    this.w_PDATREG = t_PDATREG
    this.w_ACTIPASS = t_ACTIPASS
    this.w_MCSERIAL = t_MCSERIAL
    this.w_HASEVENT = t_HASEVENT
    this.w_SER__PNT = t_SER__PNT
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_ACMOVCES with this.w_ACMOVCES
    replace t_CESPRES with this.w_CESPRES
    replace t_PNUMREG with this.w_PNUMREG
    replace t_PCODESE with this.w_PCODESE
    replace t_PDATREG with this.w_PDATREG
    replace t_ACTIPASS with this.w_ACTIPASS
    replace t_MCSERIAL with this.w_MCSERIAL
    replace t_HASEVENT with this.w_HASEVENT
    replace t_SER__PNT with this.w_SER__PNT
    if i_srv='A'
      replace ACMOVCES with this.w_ACMOVCES
      replace ACTIPASS with this.w_ACTIPASS
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_macPag1 as StdContainer
  Width  = 331
  height = 270
  stdWidth  = 331
  stdheight = 270
  resizeXpos=141
  resizeYpos=161
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=1, width=316,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Riga",Field2="PNUMREG",Label2="N.reg.",Field3="PCODESE",Label3="Esercizio",Field4="PDATREG",Label4="Del",Field5="ACTIPASS",Label5="Tipo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185402490

  add object oStr_1_16 as StdString with uid="BBLNIGLCRH",Visible=.t., Left=7, Top=377,;
    Alignment=0, Width=605, Height=19,;
    Caption="Se si cambia la grandezza della maschera cambiare le impostazioni anche nella manual block"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oBox_1_10 as StdBox with uid="UDURASWJNJ",left=5, top=1, width=321,height=20

  add object oBox_1_11 as StdBox with uid="XFOILCWOYG",left=102, top=1, width=2,height=214

  add object oBox_1_12 as StdBox with uid="IKUNQQXPTW",left=48, top=1, width=2,height=214

  add object oBox_1_13 as StdBox with uid="XEPWHVTWTE",left=256, top=1, width=2,height=214

  add object oBox_1_18 as StdBox with uid="QRVJQODYUK",left=181, top=1, width=2,height=214

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-13,top=20,;
    width=323+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-12,top=21,width=322+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_11 as StdButton with uid="KJUJIWCNMN",width=48,height=45,;
   left=223, top=218,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere al movimento cespite";
    , HelpContextID = 171866977;
    , tabstop=.f.,caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_11.Click()
      with this.Parent.oContained
        GSCE_BCZ(this.Parent.oContained,"Apri")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_11.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MCSERIAL))
    endwith
  endfunc

  add object oBtn_2_13 as StdButton with uid="TZIFDMLXPY",width=48,height=45,;
   left=274, top=218,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per caricare un nuovo movimento cespite";
    , HelpContextID = 6077226;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      with this.Parent.oContained
        GSCE_BCZ(this.Parent.oContained,"Newp")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_13.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CAUCES) AND EMPTY(.w_ACMOVCES))
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_macBodyRow as CPBodyRowCnt
  Width=313
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="MOQZPENTNQ",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 217730922,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=36, Left=9, Top=0

  add object oObj_2_3 as cp_runprogram with uid="YKPOGVHGDN",width=275,height=23,;
   left=-2, top=298,;
    caption='GSCE_BCZ(VAL1)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCZ('Val1')",;
    cEvent = "Valorizzacespiti",;
    nPag=2;
    , HelpContextID = 214944576

  add object oObj_2_4 as cp_runprogram with uid="DIHMYZHWCW",width=275,height=23,;
   left=-2, top=275,;
    caption='GSCE_BCZ(VALP)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCZ('Valp')",;
    cEvent = "Valorizzacespiti2, Load",;
    nPag=2;
    , HelpContextID = 247450432

  add object oPNUMREG_2_6 as StdTrsField with uid="PODKSAWXUN",rtseq=5,rtrep=.t.,;
    cFormVar="w_PNUMREG",value=0,;
    HelpContextID = 44596726,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=49, Left=52, Top=0, bHasZoom = .t. 

  func oPNUMREG_2_6.mCond()
    with this.Parent.oContained
      return (.w_CESPRES=.F.)
    endwith
  endfunc

  proc oPNUMREG_2_6.mZoom
      with this.Parent.oContained
        GSCE_BCZ(this.Parent.oContained,"Pnot")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPCODESE_2_7 as StdTrsField with uid="OTYREENLMU",rtseq=6,rtrep=.t.,;
    cFormVar="w_PCODESE",value=space(10),;
    HelpContextID = 265229046,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=74, Left=106, Top=0, InputMask=replicate('X',10), bHasZoom = .t. 

  func oPCODESE_2_7.mCond()
    with this.Parent.oContained
      return (.w_CESPRES=.F.)
    endwith
  endfunc

  proc oPCODESE_2_7.mZoom
      with this.Parent.oContained
        GSCE_BCZ(this.Parent.oContained,"Pnot")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPDATREG_2_8 as StdTrsField with uid="VZTIHOFTTN",rtseq=7,rtrep=.t.,;
    cFormVar="w_PDATREG",value=ctod("  /  /  "),;
    HelpContextID = 44970998,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=70, Left=185, Top=0, bHasZoom = .t. 

  func oPDATREG_2_8.mCond()
    with this.Parent.oContained
      return (.w_CESPRES=.F.)
    endwith
  endfunc

  proc oPDATREG_2_8.mZoom
      with this.Parent.oContained
        GSCE_BCZ(this.Parent.oContained,"Pnot")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oACTIPASS_2_9 as StdTrsField with uid="UENVVMFKKE",rtseq=8,rtrep=.t.,;
    cFormVar="w_ACTIPASS",value=space(1),enabled=.f.,isprimarykey=.t.,;
    ToolTipText = "Tipo associazione: contabilizzazione o abbinamento manuale",;
    HelpContextID = 243556953,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=260, Top=0, InputMask=replicate('X',1)

  add object oObj_2_12 as cp_runprogram with uid="NJTJYZXPHH",width=275,height=23,;
   left=-2, top=321,;
    caption='GSCE_BCZ(CANC)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCZ('Canc')",;
    cEvent = "HasEvent",;
    nPag=2;
    , HelpContextID = 233945152
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mac','PNT_CESP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ACSERIAL=PNT_CESP.ACSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_mac
define class tgscg_mac as StdPCForm
  Width  = 335
  Height = 270
  Top    = 10
  Left   = 12
  cComment = "Movimenti cespiti associati"
  HelpContextID=130688361

    Func ah_HasCPEvents(cEvent)
  	     If (Upper(This.cFunction)$"LOAD|EDIT" And Upper(cEvent)='ECPF6')
         this.cnt.NotifyEvent('HasEvent')
  			 return(this.cnt.w_HASEVENT)
         Else
  			 return(.t.)
        endif
  	
    EndFunc


  add object cnt as tcgscg_mac
enddefine
* --- Fine Area Manuale
