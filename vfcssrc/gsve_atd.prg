* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_atd                                                        *
*              Causali documenti di vendita                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_464]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-06                                                      *
* Last revis.: 2018-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_atd"))

* --- Class definition
define class tgsve_atd as StdForm
  Top    = 7
  Left   = 7

  * --- Standard Properties
  Width  = 725
  Height = 555+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-17"
  HelpContextID=92942953
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=218

  * --- Constant Properties
  TIP_DOCU_IDX = 0
  CAU_CONT_IDX = 0
  CAM_AGAZ_IDX = 0
  MAGAZZIN_IDX = 0
  DOC_COLL_IDX = 0
  CLA_RIGD_IDX = 0
  REPO_DOC_IDX = 0
  OUT_PUTS_IDX = 0
  LISTINI_IDX = 0
  CAU_CESP_IDX = 0
  METCALSP_IDX = 0
  MODMRIFE_IDX = 0
  VASTRUTT_IDX = 0
  VALUTE_IDX = 0
  DATI_AGG_IDX = 0
  cFile = "TIP_DOCU"
  cKeySelect = "TDTIPDOC"
  cKeyWhere  = "TDTIPDOC=this.w_TDTIPDOC"
  cKeyWhereODBC = '"TDTIPDOC="+cp_ToStrODBC(this.w_TDTIPDOC)';

  cKeyWhereODBCqualified = '"TIP_DOCU.TDTIPDOC="+cp_ToStrODBC(this.w_TDTIPDOC)';

  cPrg = "gsve_atd"
  cComment = "Causali documenti di vendita"
  icon = "anag.ico"
  cAutoZoom = 'GSVE0ATD'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TDTIPDOC = space(5)
  o_TDTIPDOC = space(5)
  w_TDDESDOC = space(35)
  w_TDCATDOC = space(2)
  o_TDCATDOC = space(2)
  w_TDCATDOC = space(2)
  w_TDCATDOC = space(2)
  w_TFFLGEFA = space(1)
  o_TFFLGEFA = space(1)
  w_TFFLGEFA = space(1)
  w_TDFLRICE = space(1)
  w_TDFLINTE = space(1)
  o_TDFLINTE = space(1)
  w_TDFLINTE = space(1)
  w_TDFLVEAC = space(1)
  o_TDFLVEAC = space(1)
  w_TDEMERIC = space(1)
  o_TDEMERIC = space(1)
  w_TDRICNOM = space(1)
  w_TDFLATIP = space(1)
  w_TDFLGCHI = space(1)
  w_TDPRODOC = space(2)
  w_TDCAUMAG = space(5)
  o_TDCAUMAG = space(5)
  w_DESMAG = space(35)
  w_FLAVAL = space(1)
  w_CAUCOL = space(5)
  w_FLCLFR = space(1)
  w_TDCODMAG = space(5)
  w_TDFLMGPR = space(1)
  w_FLRIFE = space(1)
  w_TDCODMAT = space(5)
  w_TDFLMTPR = space(1)
  w_TDCAUCON = space(5)
  o_TDCAUCON = space(5)
  w_DESCAU = space(35)
  w_TIPDOC = space(2)
  w_SERDOC = space(10)
  w_SERPRO = space(10)
  w_TDALFDOC = space(10)
  w_TDFLPDOC = space(1)
  w_TDFLPDOC = space(1)
  w_TDNUMSCO = 0
  w_TDSERPRO = space(10)
  w_TDFLPPRO = space(1)
  w_TDCODLIS = space(5)
  w_TDQTADEF = 0
  w_TDPROVVI = space(1)
  w_DESLIS = space(40)
  w_DESAPP = space(30)
  w_TDASPETT = space(30)
  w_TFFLRAGG = space(1)
  w_TDFLPREF = space(1)
  w_TDNOPRSC = space(1)
  w_TDFLDTPR = space(1)
  w_TDFLACCO = space(1)
  o_TDFLACCO = space(1)
  w_TDFLPACK = space(1)
  w_TDBOLDOG = space(1)
  w_TDFLCCAU = space(1)
  w_TDFLRIFI = space(1)
  w_TDFLRIAT = space(1)
  w_TDFLBACA = space(1)
  w_TDSEQPRE = space(1)
  w_TDSEQSCO = space(1)
  w_TDMODDES = space(1)
  w_TDSEQMA1 = space(1)
  w_TDSEQMA2 = space(1)
  w_TDFLORAT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_IVALIS = space(1)
  w_FLELGM = space(1)
  w_CAUOBSO = ctod('  /  /  ')
  w_TDCAUPFI = space(5)
  w_TDCAUCOD = space(5)
  w_TDFLEXPL = space(1)
  o_TDFLEXPL = space(1)
  w_TDPROSTA = 0
  w_FLANAL = space(1)
  w_CCFLANAL = space(1)
  w_TDFLDATT = space(1)
  w_TDEXPAUT = space(1)
  w_TIPREG = space(1)
  w_RESCHK = 0
  w_MOVIM = .F.
  w_FLRISE = space(1)
  w_FLCASC = space(1)
  w_OFLQRIO = space(1)
  w_CODI = space(5)
  w_DESC = space(35)
  w_TDFLNSTA = space(1)
  w_TDFLSTLM = space(1)
  w_CODI = space(5)
  w_TDDESRIF = space(18)
  w_TDMODRIF = space(5)
  w_DESC = space(35)
  w_TDFLNSRI = space(1)
  w_TDTPNDOC = space(3)
  w_TDFLVSRI = space(1)
  w_TDTPVDOC = space(3)
  w_TDFLRIDE = space(1)
  w_TDTPRDES = space(3)
  w_TDESCCL1 = space(3)
  w_TDESCCL2 = space(3)
  w_TDESCCL3 = space(3)
  w_TDESCCL4 = space(3)
  w_TDESCCL5 = space(3)
  w_TDSTACL1 = space(3)
  w_TDSTACL2 = space(3)
  w_TDSTACL3 = space(3)
  w_TDSTACL4 = space(3)
  w_TDSTACL5 = space(3)
  w_DESMOD = space(35)
  w_TDFLIMPA = space(1)
  w_TDFLIMAC = space(1)
  w_CODI = space(5)
  w_DESC = space(35)
  w_TDFLANAL = space(1)
  o_TDFLANAL = space(1)
  w_TDFLELAN = space(1)
  w_TDVOCECR = space(1)
  w_TD_SEGNO = space(1)
  w_TDFLPRAT = space(1)
  w_TDASSCES = space(1)
  o_TDASSCES = space(1)
  w_TDCAUCES = space(5)
  w_TDCONASS = space(1)
  w_TDCONASS = space(1)
  w_TDCODSTR = space(10)
  w_TDFLARCO = space(1)
  o_TDFLARCO = space(1)
  w_TDLOTDIF = space(1)
  w_TDTIPIMB = space(1)
  o_TDTIPIMB = space(1)
  w_TDCAUPFI = space(5)
  w_TDCAUCOD = space(5)
  w_TDFLEXPL = space(1)
  w_TDVALCOM = space(1)
  w_TDCOSEPL = space(1)
  w_TDEXPAUT = space(1)
  w_TDMAXLEV = 0
  w_DESCAUCE = space(40)
  w_DESPFI = space(35)
  w_DESCOD = space(35)
  w_CATPFI = space(2)
  w_CATCOD = space(2)
  w_FLICOD = space(1)
  w_FLIPFI = space(1)
  w_DTOBSOCA = ctod('  /  /  ')
  w_FLTCOM = space(1)
  w_FLDCOM = space(1)
  w_CODI = space(5)
  w_DESC = space(35)
  w_TDFLPROV = space(1)
  w_TDFLSILI = space(1)
  o_TDFLSILI = space(1)
  w_TDPRZVAC = space(1)
  w_TDPRZDES = space(1)
  w_TDFLVALO = space(1)
  w_TDCHKTOT = space(1)
  w_TDFLBLEV = space(1)
  w_TDFLSPIN = space(1)
  w_TDFLQRIO = space(1)
  w_TDFLPREV = space(1)
  w_TDFLAPCA = space(1)
  w_TDRIPCON = space(1)
  w_TDNOSTCO = space(1)
  w_TDFLGETR = space(1)
  w_TDFLSPIM = space(1)
  w_TDFLSPIM = space(1)
  w_MSDESIMB = space(40)
  w_TDFLSPTR = space(1)
  w_TDFLSPTR = space(1)
  w_MSDESTRA = space(40)
  w_TDRIPINC = space(1)
  w_TDRIPIMB = space(1)
  w_TDRIPTRA = space(1)
  w_TDMCALSI = space(5)
  w_TDMCALST = space(5)
  w_TDSINCFL = 0
  w_TDFLSCOR = space(1)
  w_TDFLRISC = space(1)
  o_TDFLRISC = space(1)
  w_TDFLCRIS = space(1)
  w_PRGSTA = space(8)
  w_PRGALT = space(8)
  w_DESSTRU = space(30)
  w_TDMINIMP = 0
  o_TDMINIMP = 0
  w_TDMINVAL = space(3)
  w_TDTOTDOC = space(1)
  o_TDTOTDOC = space(1)
  w_TDIMPMIN = 0
  o_TDIMPMIN = 0
  w_TDIVAMIN = space(1)
  w_TDMINVEN = space(1)
  o_TDMINVEN = space(1)
  w_TDRIOTOT = space(1)
  w_SIMVAL = space(5)
  w_MSDESIMB = space(40)
  w_MSDESTRA = space(40)
  w_TDMCALSI = space(5)
  w_TDMCALST = space(5)
  w_TDCHKUCA = space(1)
  w_DASERIAL = space(10)
  w_DACAM_01 = space(30)
  w_DACAM_02 = space(30)
  w_DACAM_04 = space(30)
  w_DACAM_03 = space(30)
  w_DACAM_05 = space(30)
  w_DACAM_06 = space(30)
  w_OBJ_CTRL = .F.
  w_TDFLIA01 = space(1)
  o_TDFLIA01 = space(1)
  w_TDFLIA02 = space(1)
  o_TDFLIA02 = space(1)
  w_TDFLIA03 = space(1)
  o_TDFLIA03 = space(1)
  w_TDFLIA04 = space(1)
  o_TDFLIA04 = space(1)
  w_TDFLIA05 = space(1)
  o_TDFLIA05 = space(1)
  w_TDFLIA06 = space(1)
  o_TDFLIA06 = space(1)
  w_TDFLRA01 = space(1)
  w_TDFLRA02 = space(1)
  w_TDFLRA03 = space(1)
  w_TDFLRA04 = space(1)
  w_TDFLRA05 = space(1)
  w_TDFLRA06 = space(1)
  w_DATOOLTI = space(100)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_TDFLCASH = space(1)
  w_TDFLNORC = space(1)
  w_TDFLCOMM = space(1)
  o_TDFLCOMM = space(1)
  w_MSG_AGGLETT = space(10)
  w_TDFLGEIN = space(1)
  w_TDTIPDFE = space(4)
  w_TDDESEST = space(35)
  w_TDCODCLA = space(5)

  * --- Children pointers
  GSVE_MTD = .NULL.
  GSVE_MDC = .NULL.
  w_CAMAGG01 = .NULL.
  w_CAMAGG02 = .NULL.
  w_CAMAGG03 = .NULL.
  w_CAMAGG04 = .NULL.
  w_CAMAGG05 = .NULL.
  w_CAMAGG06 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=6, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TIP_DOCU','gsve_atd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_atdPag1","gsve_atd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).HelpContextID = 217966645
      .Pages(2).addobject("oPag","tgsve_atdPag2","gsve_atd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Automatismi")
      .Pages(2).HelpContextID = 46369695
      .Pages(3).addobject("oPag","tgsve_atdPag3","gsve_atd",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Gestioni collegate")
      .Pages(3).HelpContextID = 55236240
      .Pages(4).addobject("oPag","tgsve_atdPag4","gsve_atd",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Origini")
      .Pages(4).HelpContextID = 9186074
      .Pages(5).addobject("oPag","tgsve_atdPag5","gsve_atd",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Report")
      .Pages(5).HelpContextID = 101464086
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTDTIPDOC_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsve_atd
    WITH THIS.PARENT
     If IsAlt()
       .cComment = CP_TRANSLATE('Tipi documenti')
     Else
       .cComment = CP_TRANSLATE('Causali documenti di vendita')
     Endif
    Endwith
    
    if g_ACQU<>'S'
      * --- Accetta ancge Ciclo Acquisti
      this.parent.cAutoZoom = 'GSVE9ATD'
    endif
    
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_CAMAGG01 = this.oPgFrm.Pages(4).oPag.CAMAGG01
      this.w_CAMAGG02 = this.oPgFrm.Pages(4).oPag.CAMAGG02
      this.w_CAMAGG03 = this.oPgFrm.Pages(4).oPag.CAMAGG03
      this.w_CAMAGG04 = this.oPgFrm.Pages(4).oPag.CAMAGG04
      this.w_CAMAGG05 = this.oPgFrm.Pages(4).oPag.CAMAGG05
      this.w_CAMAGG06 = this.oPgFrm.Pages(4).oPag.CAMAGG06
      DoDefault()
    proc Destroy()
      this.w_CAMAGG01 = .NULL.
      this.w_CAMAGG02 = .NULL.
      this.w_CAMAGG03 = .NULL.
      this.w_CAMAGG04 = .NULL.
      this.w_CAMAGG05 = .NULL.
      this.w_CAMAGG06 = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[15]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='DOC_COLL'
    this.cWorkTables[5]='CLA_RIGD'
    this.cWorkTables[6]='REPO_DOC'
    this.cWorkTables[7]='OUT_PUTS'
    this.cWorkTables[8]='LISTINI'
    this.cWorkTables[9]='CAU_CESP'
    this.cWorkTables[10]='METCALSP'
    this.cWorkTables[11]='MODMRIFE'
    this.cWorkTables[12]='VASTRUTT'
    this.cWorkTables[13]='VALUTE'
    this.cWorkTables[14]='DATI_AGG'
    this.cWorkTables[15]='TIP_DOCU'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(15))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TIP_DOCU_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TIP_DOCU_IDX,3]
  return

  function CreateChildren()
    this.GSVE_MTD = CREATEOBJECT('stdDynamicChild',this,'GSVE_MTD',this.oPgFrm.Page5.oPag.oLinkPC_5_5)
    this.GSVE_MTD.createrealchild()
    this.GSVE_MDC = CREATEOBJECT('stdDynamicChild',this,'GSVE_MDC',this.oPgFrm.Page4.oPag.oLinkPC_4_7)
    this.GSVE_MDC.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSVE_MTD)
      this.GSVE_MTD.DestroyChildrenChain()
      this.GSVE_MTD=.NULL.
    endif
    this.oPgFrm.Page5.oPag.RemoveObject('oLinkPC_5_5')
    if !ISNULL(this.GSVE_MDC)
      this.GSVE_MDC.DestroyChildrenChain()
      this.GSVE_MDC=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_7')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSVE_MTD.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSVE_MDC.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSVE_MTD.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSVE_MDC.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSVE_MTD.NewDocument()
    this.GSVE_MDC.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSVE_MTD.SetKey(;
            .w_TDTIPDOC,"LGCODICE";
            )
      this.GSVE_MDC.SetKey(;
            .w_TDTIPDOC,"DCCODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSVE_MTD.ChangeRow(this.cRowID+'      1',1;
             ,.w_TDTIPDOC,"LGCODICE";
             )
      .GSVE_MDC.ChangeRow(this.cRowID+'      1',1;
             ,.w_TDTIPDOC,"DCCODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSVE_MTD)
        i_f=.GSVE_MTD.BuildFilter()
        if !(i_f==.GSVE_MTD.cQueryFilter)
          i_fnidx=.GSVE_MTD.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSVE_MTD.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSVE_MTD.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSVE_MTD.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSVE_MTD.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSVE_MDC)
        i_f=.GSVE_MDC.BuildFilter()
        if !(i_f==.GSVE_MDC.cQueryFilter)
          i_fnidx=.GSVE_MDC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSVE_MDC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSVE_MDC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSVE_MDC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSVE_MDC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TDTIPDOC = NVL(TDTIPDOC,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    local link_1_27_joined
    link_1_27_joined=.f.
    local link_1_38_joined
    link_1_38_joined=.f.
    local link_4_4_joined
    link_4_4_joined=.f.
    local link_3_7_joined
    link_3_7_joined=.f.
    local link_3_10_joined
    link_3_10_joined=.f.
    local link_3_14_joined
    link_3_14_joined=.f.
    local link_3_15_joined
    link_3_15_joined=.f.
    local link_2_30_joined
    link_2_30_joined=.f.
    local link_2_31_joined
    link_2_31_joined=.f.
    local link_1_105_joined
    link_1_105_joined=.f.
    local link_2_42_joined
    link_2_42_joined=.f.
    local link_2_43_joined
    link_2_43_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TIP_DOCU where TDTIPDOC=KeySet.TDTIPDOC
    *
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TIP_DOCU')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TIP_DOCU.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TIP_DOCU '
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_27_joined=this.AddJoinedLink_1_27(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_4_joined=this.AddJoinedLink_4_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_7_joined=this.AddJoinedLink_3_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_10_joined=this.AddJoinedLink_3_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_14_joined=this.AddJoinedLink_3_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_15_joined=this.AddJoinedLink_3_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_30_joined=this.AddJoinedLink_2_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_31_joined=this.AddJoinedLink_2_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_105_joined=this.AddJoinedLink_1_105(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_42_joined=this.AddJoinedLink_2_42(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_43_joined=this.AddJoinedLink_2_43(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TDTIPDOC',this.w_TDTIPDOC  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESMAG = space(35)
        .w_FLAVAL = space(1)
        .w_CAUCOL = space(5)
        .w_FLCLFR = space(1)
        .w_FLRIFE = space(1)
        .w_DESCAU = space(35)
        .w_TIPDOC = space(2)
        .w_SERDOC = space(10)
        .w_SERPRO = space(10)
        .w_DESLIS = space(40)
        .w_DESAPP = space(30)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_IVALIS = space(1)
        .w_FLELGM = space(1)
        .w_CAUOBSO = ctod("  /  /  ")
        .w_FLANAL = space(1)
        .w_CCFLANAL = space(1)
        .w_TIPREG = space(1)
        .w_RESCHK = 0
        .w_MOVIM = .f.
        .w_FLRISE = space(1)
        .w_FLCASC = space(1)
        .w_OFLQRIO = space(1)
        .w_DESMOD = space(35)
        .w_DESCAUCE = space(40)
        .w_DESPFI = space(35)
        .w_DESCOD = space(35)
        .w_CATPFI = space(2)
        .w_CATCOD = space(2)
        .w_FLICOD = space(1)
        .w_FLIPFI = space(1)
        .w_DTOBSOCA = ctod("  /  /  ")
        .w_FLTCOM = space(1)
        .w_FLDCOM = space(1)
        .w_MSDESIMB = space(40)
        .w_MSDESTRA = space(40)
        .w_DESSTRU = space(30)
        .w_SIMVAL = space(5)
        .w_MSDESIMB = space(40)
        .w_MSDESTRA = space(40)
        .w_DASERIAL = '0000000001'
        .w_DACAM_01 = space(30)
        .w_DACAM_02 = space(30)
        .w_DACAM_04 = space(30)
        .w_DACAM_03 = space(30)
        .w_DACAM_05 = space(30)
        .w_DACAM_06 = space(30)
        .w_OBJ_CTRL = .f.
        .w_DATOOLTI = "Se attivo: il campo aggiuntivo %1 sar� importato"
        .w_MSG_AGGLETT = space(10)
        .w_TDTIPDOC = NVL(TDTIPDOC,space(5))
        .w_TDDESDOC = NVL(TDDESDOC,space(35))
        .w_TDCATDOC = NVL(TDCATDOC,space(2))
        .w_TDCATDOC = NVL(TDCATDOC,space(2))
        .w_TDCATDOC = NVL(TDCATDOC,space(2))
        .w_TFFLGEFA = NVL(TFFLGEFA,space(1))
        .w_TFFLGEFA = NVL(TFFLGEFA,space(1))
        .w_TDFLRICE = NVL(TDFLRICE,space(1))
        .w_TDFLINTE = NVL(TDFLINTE,space(1))
        .w_TDFLINTE = NVL(TDFLINTE,space(1))
        .w_TDFLVEAC = NVL(TDFLVEAC,space(1))
        .w_TDEMERIC = NVL(TDEMERIC,space(1))
        .w_TDRICNOM = NVL(TDRICNOM,space(1))
        .w_TDFLATIP = NVL(TDFLATIP,space(1))
        .w_TDFLGCHI = NVL(TDFLGCHI,space(1))
        .w_TDPRODOC = NVL(TDPRODOC,space(2))
        .w_TDCAUMAG = NVL(TDCAUMAG,space(5))
          if link_1_17_joined
            this.w_TDCAUMAG = NVL(CMCODICE117,NVL(this.w_TDCAUMAG,space(5)))
            this.w_DESMAG = NVL(CMDESCRI117,space(35))
            this.w_CAUCOL = NVL(CMCAUCOL117,space(5))
            this.w_FLCLFR = NVL(CMFLCLFR117,space(1))
            this.w_FLAVAL = NVL(CMFLAVAL117,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CMDTOBSO117),ctod("  /  /  "))
            this.w_FLELGM = NVL(CMFLELGM117,space(1))
            this.w_FLCASC = NVL(CMFLCASC117,space(1))
            this.w_FLRISE = NVL(CMFLRISE117,space(1))
          else
          .link_1_17('Load')
          endif
        .w_TDCODMAG = NVL(TDCODMAG,space(5))
          if link_1_22_joined
            this.w_TDCODMAG = NVL(MGCODMAG122,NVL(this.w_TDCODMAG,space(5)))
            this.w_DESAPP = NVL(MGDESMAG122,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(MGDTOBSO122),ctod("  /  /  "))
          else
          .link_1_22('Load')
          endif
        .w_TDFLMGPR = NVL(TDFLMGPR,space(1))
        .w_TDCODMAT = NVL(TDCODMAT,space(5))
          if link_1_25_joined
            this.w_TDCODMAT = NVL(MGCODMAG125,NVL(this.w_TDCODMAT,space(5)))
            this.w_DESAPP = NVL(MGDESMAG125,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(MGDTOBSO125),ctod("  /  /  "))
          else
          .link_1_25('Load')
          endif
        .w_TDFLMTPR = NVL(TDFLMTPR,space(1))
        .w_TDCAUCON = NVL(TDCAUCON,space(5))
          if link_1_27_joined
            this.w_TDCAUCON = NVL(CCCODICE127,NVL(this.w_TDCAUCON,space(5)))
            this.w_DESCAU = NVL(CCDESCRI127,space(35))
            this.w_TIPDOC = NVL(CCTIPDOC127,space(2))
            this.w_SERDOC = NVL(CCSERDOC127,space(10))
            this.w_SERPRO = NVL(CCSERPRO127,space(10))
            this.w_FLRIFE = NVL(CCFLRIFE127,space(1))
            this.w_CAUOBSO = NVL(cp_ToDate(CCDTOBSO127),ctod("  /  /  "))
            this.w_FLANAL = NVL(CCFLANAL127,space(1))
            this.w_CCFLANAL = NVL(CCFLANAL127,space(1))
            this.w_TIPREG = NVL(CCTIPREG127,space(1))
          else
          .link_1_27('Load')
          endif
        .w_TDALFDOC = NVL(TDALFDOC,space(10))
        .w_TDFLPDOC = NVL(TDFLPDOC,space(1))
        .w_TDFLPDOC = NVL(TDFLPDOC,space(1))
        .w_TDNUMSCO = NVL(TDNUMSCO,0)
        .w_TDSERPRO = NVL(TDSERPRO,space(10))
        .w_TDFLPPRO = NVL(TDFLPPRO,space(1))
        .w_TDCODLIS = NVL(TDCODLIS,space(5))
          if link_1_38_joined
            this.w_TDCODLIS = NVL(LSCODLIS138,NVL(this.w_TDCODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS138,space(40))
            this.w_IVALIS = NVL(LSIVALIS138,space(1))
          else
          .link_1_38('Load')
          endif
        .w_TDQTADEF = NVL(TDQTADEF,0)
        .w_TDPROVVI = NVL(TDPROVVI,space(1))
        .w_TDASPETT = NVL(TDASPETT,space(30))
        .w_TFFLRAGG = NVL(TFFLRAGG,space(1))
        .w_TDFLPREF = NVL(TDFLPREF,space(1))
        .w_TDNOPRSC = NVL(TDNOPRSC,space(1))
        .w_TDFLDTPR = NVL(TDFLDTPR,space(1))
        .w_TDFLACCO = NVL(TDFLACCO,space(1))
        .w_TDFLPACK = NVL(TDFLPACK,space(1))
        .w_TDBOLDOG = NVL(TDBOLDOG,space(1))
        .w_TDFLCCAU = NVL(TDFLCCAU,space(1))
        .w_TDFLRIFI = NVL(TDFLRIFI,space(1))
        .w_TDFLRIAT = NVL(TDFLRIAT,space(1))
        .w_TDFLBACA = NVL(TDFLBACA,space(1))
        .w_TDSEQPRE = NVL(TDSEQPRE,space(1))
        .w_TDSEQSCO = NVL(TDSEQSCO,space(1))
        .w_TDMODDES = NVL(TDMODDES,space(1))
        .w_TDSEQMA1 = NVL(TDSEQMA1,space(1))
        .w_TDSEQMA2 = NVL(TDSEQMA2,space(1))
        .w_TDFLORAT = NVL(TDFLORAT,space(1))
        .w_TDCAUPFI = NVL(TDCAUPFI,space(5))
          * evitabile
          *.link_1_79('Load')
        .w_TDCAUCOD = NVL(TDCAUCOD,space(5))
          * evitabile
          *.link_1_80('Load')
        .w_TDFLEXPL = NVL(TDFLEXPL,space(1))
        .w_TDPROSTA = NVL(TDPROSTA,0)
        .w_TDFLDATT = NVL(TDFLDATT,space(1))
        .w_TDEXPAUT = NVL(TDEXPAUT,space(1))
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .w_CODI = .w_TDTIPDOC
        .w_DESC = .w_TDDESDOC
        .w_TDFLNSTA = NVL(TDFLNSTA,space(1))
        .w_TDFLSTLM = NVL(TDFLSTLM,space(1))
        .w_CODI = .w_TDTIPDOC
        .w_TDDESRIF = NVL(TDDESRIF,space(18))
        .w_TDMODRIF = NVL(TDMODRIF,space(5))
          if link_4_4_joined
            this.w_TDMODRIF = NVL(MDCODICE404,NVL(this.w_TDMODRIF,space(5)))
            this.w_DESMOD = NVL(MDDESCRI404,space(35))
          else
          .link_4_4('Load')
          endif
        .w_DESC = .w_TDDESDOC
        .w_TDFLNSRI = NVL(TDFLNSRI,space(1))
        .w_TDTPNDOC = NVL(TDTPNDOC,space(3))
          * evitabile
          *.link_4_9('Load')
        .w_TDFLVSRI = NVL(TDFLVSRI,space(1))
        .w_TDTPVDOC = NVL(TDTPVDOC,space(3))
          * evitabile
          *.link_4_11('Load')
        .w_TDFLRIDE = NVL(TDFLRIDE,space(1))
        .w_TDTPRDES = NVL(TDTPRDES,space(3))
          * evitabile
          *.link_4_13('Load')
        .w_TDESCCL1 = NVL(TDESCCL1,space(3))
          * evitabile
          *.link_4_17('Load')
        .w_TDESCCL2 = NVL(TDESCCL2,space(3))
          * evitabile
          *.link_4_18('Load')
        .w_TDESCCL3 = NVL(TDESCCL3,space(3))
          * evitabile
          *.link_4_19('Load')
        .w_TDESCCL4 = NVL(TDESCCL4,space(3))
          * evitabile
          *.link_4_20('Load')
        .w_TDESCCL5 = NVL(TDESCCL5,space(3))
          * evitabile
          *.link_4_21('Load')
        .w_TDSTACL1 = NVL(TDSTACL1,space(3))
          * evitabile
          *.link_4_22('Load')
        .w_TDSTACL2 = NVL(TDSTACL2,space(3))
          * evitabile
          *.link_4_23('Load')
        .w_TDSTACL3 = NVL(TDSTACL3,space(3))
          * evitabile
          *.link_4_24('Load')
        .w_TDSTACL4 = NVL(TDSTACL4,space(3))
          * evitabile
          *.link_4_25('Load')
        .w_TDSTACL5 = NVL(TDSTACL5,space(3))
          * evitabile
          *.link_4_26('Load')
        .w_TDFLIMPA = NVL(TDFLIMPA,space(1))
        .w_TDFLIMAC = NVL(TDFLIMAC,space(1))
        .w_CODI = .w_TDTIPDOC
        .w_DESC = .w_TDDESDOC
        .w_TDFLANAL = NVL(TDFLANAL,space(1))
        .w_TDFLELAN = NVL(TDFLELAN,space(1))
        .w_TDVOCECR = NVL(TDVOCECR,space(1))
        .w_TD_SEGNO = NVL(TD_SEGNO,space(1))
        .w_TDFLPRAT = NVL(TDFLPRAT,space(1))
        .w_TDASSCES = NVL(TDASSCES,space(1))
        .w_TDCAUCES = NVL(TDCAUCES,space(5))
          if link_3_7_joined
            this.w_TDCAUCES = NVL(CCCODICE307,NVL(this.w_TDCAUCES,space(5)))
            this.w_DESCAUCE = NVL(CCDESCRI307,space(40))
            this.w_DTOBSOCA = NVL(cp_ToDate(CCDTOBSO307),ctod("  /  /  "))
          else
          .link_3_7('Load')
          endif
        .w_TDCONASS = NVL(TDCONASS,space(1))
        .w_TDCONASS = NVL(TDCONASS,space(1))
        .w_TDCODSTR = NVL(TDCODSTR,space(10))
          if link_3_10_joined
            this.w_TDCODSTR = NVL(STCODICE310,NVL(this.w_TDCODSTR,space(10)))
            this.w_DESSTRU = NVL(STDESCRI310,space(30))
          else
          .link_3_10('Load')
          endif
        .w_TDFLARCO = NVL(TDFLARCO,space(1))
        .w_TDLOTDIF = NVL(TDLOTDIF,space(1))
        .w_TDTIPIMB = NVL(TDTIPIMB,space(1))
        .w_TDCAUPFI = NVL(TDCAUPFI,space(5))
          if link_3_14_joined
            this.w_TDCAUPFI = NVL(TDTIPDOC314,NVL(this.w_TDCAUPFI,space(5)))
            this.w_DESPFI = NVL(TDDESDOC314,space(35))
            this.w_CATPFI = NVL(TDCATDOC314,space(2))
            this.w_FLIPFI = NVL(TDFLINTE314,space(1))
            this.w_FLTCOM = NVL(TDFLCOMM314,space(1))
          else
          .link_3_14('Load')
          endif
        .w_TDCAUCOD = NVL(TDCAUCOD,space(5))
          if link_3_15_joined
            this.w_TDCAUCOD = NVL(TDTIPDOC315,NVL(this.w_TDCAUCOD,space(5)))
            this.w_DESCOD = NVL(TDDESDOC315,space(35))
            this.w_CATCOD = NVL(TDCATDOC315,space(2))
            this.w_FLICOD = NVL(TDFLINTE315,space(1))
            this.w_FLDCOM = NVL(TDFLCOMM315,space(1))
          else
          .link_3_15('Load')
          endif
        .w_TDFLEXPL = NVL(TDFLEXPL,space(1))
        .w_TDVALCOM = NVL(TDVALCOM,space(1))
        .w_TDCOSEPL = NVL(TDCOSEPL,space(1))
        .w_TDEXPAUT = NVL(TDEXPAUT,space(1))
        .w_TDMAXLEV = NVL(TDMAXLEV,0)
        .oPgFrm.Page3.oPag.oObj_3_43.Calculate()
        .w_CODI = .w_TDTIPDOC
        .w_DESC = .w_TDDESDOC
        .w_TDFLPROV = NVL(TDFLPROV,space(1))
        .w_TDFLSILI = NVL(TDFLSILI,space(1))
        .w_TDPRZVAC = NVL(TDPRZVAC,space(1))
        .w_TDPRZDES = NVL(TDPRZDES,space(1))
        .w_TDFLVALO = NVL(TDFLVALO,space(1))
        .w_TDCHKTOT = NVL(TDCHKTOT,space(1))
        .w_TDFLBLEV = NVL(TDFLBLEV,space(1))
        .w_TDFLSPIN = NVL(TDFLSPIN,space(1))
        .w_TDFLQRIO = NVL(TDFLQRIO,space(1))
        .w_TDFLPREV = NVL(TDFLPREV,space(1))
        .w_TDFLAPCA = NVL(TDFLAPCA,space(1))
        .w_TDRIPCON = NVL(TDRIPCON,space(1))
        .w_TDNOSTCO = NVL(TDNOSTCO,space(1))
        .w_TDFLGETR = NVL(TDFLGETR,space(1))
        .w_TDFLSPIM = NVL(TDFLSPIM,space(1))
        .w_TDFLSPIM = NVL(TDFLSPIM,space(1))
        .w_TDFLSPTR = NVL(TDFLSPTR,space(1))
        .w_TDFLSPTR = NVL(TDFLSPTR,space(1))
        .w_TDRIPINC = NVL(TDRIPINC,space(1))
        .w_TDRIPIMB = NVL(TDRIPIMB,space(1))
        .w_TDRIPTRA = NVL(TDRIPTRA,space(1))
        .w_TDMCALSI = NVL(TDMCALSI,space(5))
          if link_2_30_joined
            this.w_TDMCALSI = NVL(MSCODICE230,NVL(this.w_TDMCALSI,space(5)))
            this.w_MSDESIMB = NVL(MSDESCRI230,space(40))
          else
          .link_2_30('Load')
          endif
        .w_TDMCALST = NVL(TDMCALST,space(5))
          if link_2_31_joined
            this.w_TDMCALST = NVL(MSCODICE231,NVL(this.w_TDMCALST,space(5)))
            this.w_MSDESTRA = NVL(MSDESCRI231,space(40))
          else
          .link_2_31('Load')
          endif
        .w_TDSINCFL = NVL(TDSINCFL,0)
        .w_TDFLSCOR = NVL(TDFLSCOR,space(1))
        .w_TDFLRISC = NVL(TDFLRISC,space(1))
        .w_TDFLCRIS = NVL(TDFLCRIS,space(1))
        .w_PRGSTA = IIF(.w_TDFLVEAC='A' AND g_ACQU<>'S', 'GSAC_MDV', 'GSVE_MDV')
        .w_PRGALT = IIF(.w_TDFLVEAC='A' AND g_ACQU<>'S', 'GSACAMDV', 'GSVEAMDV')
        .w_TDMINIMP = NVL(TDMINIMP,0)
        .w_TDMINVAL = NVL(TDMINVAL,space(3))
          if link_1_105_joined
            this.w_TDMINVAL = NVL(VACODVAL205,NVL(this.w_TDMINVAL,space(3)))
            this.w_SIMVAL = NVL(VASIMVAL205,space(5))
          else
          .link_1_105('Load')
          endif
        .w_TDTOTDOC = NVL(TDTOTDOC,space(1))
        .w_TDIMPMIN = NVL(TDIMPMIN,0)
        .w_TDIVAMIN = NVL(TDIVAMIN,space(1))
        .w_TDMINVEN = NVL(TDMINVEN,space(1))
        .w_TDRIOTOT = NVL(TDRIOTOT,space(1))
        .w_TDMCALSI = NVL(TDMCALSI,space(5))
          if link_2_42_joined
            this.w_TDMCALSI = NVL(MSCODICE242,NVL(this.w_TDMCALSI,space(5)))
            this.w_MSDESIMB = NVL(MSDESCRI242,space(40))
          else
          .link_2_42('Load')
          endif
        .w_TDMCALST = NVL(TDMCALST,space(5))
          if link_2_43_joined
            this.w_TDMCALST = NVL(MSCODICE243,NVL(this.w_TDMCALST,space(5)))
            this.w_MSDESTRA = NVL(MSDESCRI243,space(40))
          else
          .link_2_43('Load')
          endif
        .w_TDCHKUCA = NVL(TDCHKUCA,space(1))
          .link_4_41('Load')
        .w_TDFLIA01 = NVL(TDFLIA01,space(1))
        .w_TDFLIA02 = NVL(TDFLIA02,space(1))
        .w_TDFLIA03 = NVL(TDFLIA03,space(1))
        .w_TDFLIA04 = NVL(TDFLIA04,space(1))
        .w_TDFLIA05 = NVL(TDFLIA05,space(1))
        .w_TDFLIA06 = NVL(TDFLIA06,space(1))
        .w_TDFLRA01 = NVL(TDFLRA01,space(1))
        .oPgFrm.Page4.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
        .w_TDFLRA02 = NVL(TDFLRA02,space(1))
        .w_TDFLRA03 = NVL(TDFLRA03,space(1))
        .w_TDFLRA04 = NVL(TDFLRA04,space(1))
        .w_TDFLRA05 = NVL(TDFLRA05,space(1))
        .w_TDFLRA06 = NVL(TDFLRA06,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_TDFLCASH = NVL(TDFLCASH,space(1))
        .w_TDFLNORC = NVL(TDFLNORC,space(1))
        .w_TDFLCOMM = NVL(TDFLCOMM,space(1))
        .w_TDFLGEIN = NVL(TDFLGEIN,space(1))
        .w_TDTIPDFE = NVL(TDTIPDFE,space(4))
        .w_TDDESEST = NVL(TDDESEST,space(35))
        .w_TDCODCLA = NVL(TDCODCLA,space(5))
        cp_LoadRecExtFlds(this,'TIP_DOCU')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page2.oPag.oBtn_2_46.enabled = this.oPgFrm.Page2.oPag.oBtn_2_46.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsve_atd
    if (this.w_TDFLVEAC <> 'V' AND g_ACQU='S') OR this.w_TDCATDOC='OR'
      this.BlankRec()
    endif
    * - Memorizzo il valore iniziale di TDFLQRIO
    This.w_OFLQRIO = This.w_TDFLQRIO
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TDTIPDOC = space(5)
      .w_TDDESDOC = space(35)
      .w_TDCATDOC = space(2)
      .w_TDCATDOC = space(2)
      .w_TDCATDOC = space(2)
      .w_TFFLGEFA = space(1)
      .w_TFFLGEFA = space(1)
      .w_TDFLRICE = space(1)
      .w_TDFLINTE = space(1)
      .w_TDFLINTE = space(1)
      .w_TDFLVEAC = space(1)
      .w_TDEMERIC = space(1)
      .w_TDRICNOM = space(1)
      .w_TDFLATIP = space(1)
      .w_TDFLGCHI = space(1)
      .w_TDPRODOC = space(2)
      .w_TDCAUMAG = space(5)
      .w_DESMAG = space(35)
      .w_FLAVAL = space(1)
      .w_CAUCOL = space(5)
      .w_FLCLFR = space(1)
      .w_TDCODMAG = space(5)
      .w_TDFLMGPR = space(1)
      .w_FLRIFE = space(1)
      .w_TDCODMAT = space(5)
      .w_TDFLMTPR = space(1)
      .w_TDCAUCON = space(5)
      .w_DESCAU = space(35)
      .w_TIPDOC = space(2)
      .w_SERDOC = space(10)
      .w_SERPRO = space(10)
      .w_TDALFDOC = space(10)
      .w_TDFLPDOC = space(1)
      .w_TDFLPDOC = space(1)
      .w_TDNUMSCO = 0
      .w_TDSERPRO = space(10)
      .w_TDFLPPRO = space(1)
      .w_TDCODLIS = space(5)
      .w_TDQTADEF = 0
      .w_TDPROVVI = space(1)
      .w_DESLIS = space(40)
      .w_DESAPP = space(30)
      .w_TDASPETT = space(30)
      .w_TFFLRAGG = space(1)
      .w_TDFLPREF = space(1)
      .w_TDNOPRSC = space(1)
      .w_TDFLDTPR = space(1)
      .w_TDFLACCO = space(1)
      .w_TDFLPACK = space(1)
      .w_TDBOLDOG = space(1)
      .w_TDFLCCAU = space(1)
      .w_TDFLRIFI = space(1)
      .w_TDFLRIAT = space(1)
      .w_TDFLBACA = space(1)
      .w_TDSEQPRE = space(1)
      .w_TDSEQSCO = space(1)
      .w_TDMODDES = space(1)
      .w_TDSEQMA1 = space(1)
      .w_TDSEQMA2 = space(1)
      .w_TDFLORAT = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_IVALIS = space(1)
      .w_FLELGM = space(1)
      .w_CAUOBSO = ctod("  /  /  ")
      .w_TDCAUPFI = space(5)
      .w_TDCAUCOD = space(5)
      .w_TDFLEXPL = space(1)
      .w_TDPROSTA = 0
      .w_FLANAL = space(1)
      .w_CCFLANAL = space(1)
      .w_TDFLDATT = space(1)
      .w_TDEXPAUT = space(1)
      .w_TIPREG = space(1)
      .w_RESCHK = 0
      .w_MOVIM = .f.
      .w_FLRISE = space(1)
      .w_FLCASC = space(1)
      .w_OFLQRIO = space(1)
      .w_CODI = space(5)
      .w_DESC = space(35)
      .w_TDFLNSTA = space(1)
      .w_TDFLSTLM = space(1)
      .w_CODI = space(5)
      .w_TDDESRIF = space(18)
      .w_TDMODRIF = space(5)
      .w_DESC = space(35)
      .w_TDFLNSRI = space(1)
      .w_TDTPNDOC = space(3)
      .w_TDFLVSRI = space(1)
      .w_TDTPVDOC = space(3)
      .w_TDFLRIDE = space(1)
      .w_TDTPRDES = space(3)
      .w_TDESCCL1 = space(3)
      .w_TDESCCL2 = space(3)
      .w_TDESCCL3 = space(3)
      .w_TDESCCL4 = space(3)
      .w_TDESCCL5 = space(3)
      .w_TDSTACL1 = space(3)
      .w_TDSTACL2 = space(3)
      .w_TDSTACL3 = space(3)
      .w_TDSTACL4 = space(3)
      .w_TDSTACL5 = space(3)
      .w_DESMOD = space(35)
      .w_TDFLIMPA = space(1)
      .w_TDFLIMAC = space(1)
      .w_CODI = space(5)
      .w_DESC = space(35)
      .w_TDFLANAL = space(1)
      .w_TDFLELAN = space(1)
      .w_TDVOCECR = space(1)
      .w_TD_SEGNO = space(1)
      .w_TDFLPRAT = space(1)
      .w_TDASSCES = space(1)
      .w_TDCAUCES = space(5)
      .w_TDCONASS = space(1)
      .w_TDCONASS = space(1)
      .w_TDCODSTR = space(10)
      .w_TDFLARCO = space(1)
      .w_TDLOTDIF = space(1)
      .w_TDTIPIMB = space(1)
      .w_TDCAUPFI = space(5)
      .w_TDCAUCOD = space(5)
      .w_TDFLEXPL = space(1)
      .w_TDVALCOM = space(1)
      .w_TDCOSEPL = space(1)
      .w_TDEXPAUT = space(1)
      .w_TDMAXLEV = 0
      .w_DESCAUCE = space(40)
      .w_DESPFI = space(35)
      .w_DESCOD = space(35)
      .w_CATPFI = space(2)
      .w_CATCOD = space(2)
      .w_FLICOD = space(1)
      .w_FLIPFI = space(1)
      .w_DTOBSOCA = ctod("  /  /  ")
      .w_FLTCOM = space(1)
      .w_FLDCOM = space(1)
      .w_CODI = space(5)
      .w_DESC = space(35)
      .w_TDFLPROV = space(1)
      .w_TDFLSILI = space(1)
      .w_TDPRZVAC = space(1)
      .w_TDPRZDES = space(1)
      .w_TDFLVALO = space(1)
      .w_TDCHKTOT = space(1)
      .w_TDFLBLEV = space(1)
      .w_TDFLSPIN = space(1)
      .w_TDFLQRIO = space(1)
      .w_TDFLPREV = space(1)
      .w_TDFLAPCA = space(1)
      .w_TDRIPCON = space(1)
      .w_TDNOSTCO = space(1)
      .w_TDFLGETR = space(1)
      .w_TDFLSPIM = space(1)
      .w_TDFLSPIM = space(1)
      .w_MSDESIMB = space(40)
      .w_TDFLSPTR = space(1)
      .w_TDFLSPTR = space(1)
      .w_MSDESTRA = space(40)
      .w_TDRIPINC = space(1)
      .w_TDRIPIMB = space(1)
      .w_TDRIPTRA = space(1)
      .w_TDMCALSI = space(5)
      .w_TDMCALST = space(5)
      .w_TDSINCFL = 0
      .w_TDFLSCOR = space(1)
      .w_TDFLRISC = space(1)
      .w_TDFLCRIS = space(1)
      .w_PRGSTA = space(8)
      .w_PRGALT = space(8)
      .w_DESSTRU = space(30)
      .w_TDMINIMP = 0
      .w_TDMINVAL = space(3)
      .w_TDTOTDOC = space(1)
      .w_TDIMPMIN = 0
      .w_TDIVAMIN = space(1)
      .w_TDMINVEN = space(1)
      .w_TDRIOTOT = space(1)
      .w_SIMVAL = space(5)
      .w_MSDESIMB = space(40)
      .w_MSDESTRA = space(40)
      .w_TDMCALSI = space(5)
      .w_TDMCALST = space(5)
      .w_TDCHKUCA = space(1)
      .w_DASERIAL = space(10)
      .w_DACAM_01 = space(30)
      .w_DACAM_02 = space(30)
      .w_DACAM_04 = space(30)
      .w_DACAM_03 = space(30)
      .w_DACAM_05 = space(30)
      .w_DACAM_06 = space(30)
      .w_OBJ_CTRL = .f.
      .w_TDFLIA01 = space(1)
      .w_TDFLIA02 = space(1)
      .w_TDFLIA03 = space(1)
      .w_TDFLIA04 = space(1)
      .w_TDFLIA05 = space(1)
      .w_TDFLIA06 = space(1)
      .w_TDFLRA01 = space(1)
      .w_TDFLRA02 = space(1)
      .w_TDFLRA03 = space(1)
      .w_TDFLRA04 = space(1)
      .w_TDFLRA05 = space(1)
      .w_TDFLRA06 = space(1)
      .w_DATOOLTI = space(100)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_TDFLCASH = space(1)
      .w_TDFLNORC = space(1)
      .w_TDFLCOMM = space(1)
      .w_MSG_AGGLETT = space(10)
      .w_TDFLGEIN = space(1)
      .w_TDTIPDFE = space(4)
      .w_TDDESEST = space(35)
      .w_TDCODCLA = space(5)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_TDCATDOC = 'DI'
        .w_TDCATDOC = 'DI'
        .w_TDCATDOC = 'DI'
        .w_TFFLGEFA = 'A'
        .w_TFFLGEFA = 'A'
        .w_TDFLRICE = ' '
        .w_TDFLINTE = 'N'
        .w_TDFLINTE = 'N'
        .w_TDFLVEAC = IIF(.w_TDFLINTE='F', 'A', 'V')
        .w_TDEMERIC = .w_TDFLVEAC
        .w_TDRICNOM = 'N'
        .w_TDFLATIP = 'N'
          .DoRTCalc(15,15,.f.)
        .w_TDPRODOC = IIF(.w_TDFLVEAC='V' AND .w_TDCATDOC $ 'DT-DI' AND .w_TDFLPDOC='S', 'NN', CALCPD(.w_TDCATDOC,.w_TDFLVEAC))
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_TDCAUMAG))
          .link_1_17('Full')
          endif
          .DoRTCalc(18,21,.f.)
        .w_TDCODMAG = SPACE(5)
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_TDCODMAG))
          .link_1_22('Full')
          endif
        .w_TDFLMGPR = 'D'
          .DoRTCalc(24,24,.f.)
        .w_TDCODMAT = SPACE(5)
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_TDCODMAT))
          .link_1_25('Full')
          endif
        .w_TDFLMTPR = 'D'
        .w_TDCAUCON = IIF(NOT IsAlt(),IIF(.w_TDCATDOC $ 'FA-NC-RF', .w_TDCAUCON, SPACE(5)),IIF(.w_TDCATDOC='FA','400',IIF(.w_TDCATDOC='NC','450',SPACE(5))))
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_TDCAUCON))
          .link_1_27('Full')
          endif
          .DoRTCalc(28,31,.f.)
        .w_TDALFDOC = IIF(NOT EMPTY(.w_SERDOC) AND (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC'), .w_SERDOC,.w_TDALFDOC)
        .w_TDFLPDOC = IIF(.w_TDFLVEAC='A' AND g_ACQU<>'S', 'S', ' ')
        .w_TDFLPDOC = IIF(.w_TDEMERIC='A' OR .w_TDFLVEAC='A' AND g_ACQU<>'S', 'S', ' ')
        .w_TDNUMSCO = g_NUMSCO
        .w_TDSERPRO = IIF(NOT EMPTY(.w_SERPRO) AND (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC' OR (.w_TDCATDOC='DI' AND NOT EMPTY(.w_TDCAUCON) AND .w_TDFLPPRO='P')), .w_SERPRO, .w_TDSERPRO)
        .w_TDFLPPRO = IIF(empty(.w_TDCAUCON), 'N', 'P')
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_TDCODLIS))
          .link_1_38('Full')
          endif
          .DoRTCalc(39,39,.f.)
        .w_TDPROVVI = 'N'
          .DoRTCalc(41,42,.f.)
        .w_TDASPETT = IIF(.w_TDFLACCO='S',.w_TDASPETT,SPACE(30))
        .w_TFFLRAGG = '1'
          .DoRTCalc(45,49,.f.)
        .w_TDBOLDOG = ' '
        .w_TDFLCCAU = IIF(.w_TFFLGEFA = 'B', ' ', .w_TDFLCCAU)
        .w_TDFLRIFI = iif(.w_TDCATDOC='DI',.w_TDFLRIFI,'N')
        .w_TDFLRIAT = IIF(.w_TDCATDOC $ 'FA-NC-RF-NC', .w_TDFLRIAT, ' ')
        .w_TDFLBACA = ' '
          .DoRTCalc(55,56,.f.)
        .w_TDMODDES = 'S'
          .DoRTCalc(58,59,.f.)
        .w_TDFLORAT = IIF(.w_TDFLACCO='S','S',' ')
        .w_OBTEST = i_datsys
        .DoRTCalc(62,66,.f.)
          if not(empty(.w_TDCAUPFI))
          .link_1_79('Full')
          endif
        .DoRTCalc(67,67,.f.)
          if not(empty(.w_TDCAUCOD))
          .link_1_80('Full')
          endif
          .DoRTCalc(68,71,.f.)
        .w_TDFLDATT = IIF(.w_TDFLACCO='S','S',' ')
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
          .DoRTCalc(73,75,.f.)
        .w_MOVIM = .f.
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
          .DoRTCalc(77,79,.f.)
        .w_CODI = .w_TDTIPDOC
        .w_DESC = .w_TDDESDOC
          .DoRTCalc(82,82,.f.)
        .w_TDFLSTLM = 'N'
        .w_CODI = .w_TDTIPDOC
        .DoRTCalc(85,86,.f.)
          if not(empty(.w_TDMODRIF))
          .link_4_4('Full')
          endif
        .w_DESC = .w_TDDESDOC
        .DoRTCalc(88,89,.f.)
          if not(empty(.w_TDTPNDOC))
          .link_4_9('Full')
          endif
        .DoRTCalc(90,91,.f.)
          if not(empty(.w_TDTPVDOC))
          .link_4_11('Full')
          endif
        .DoRTCalc(92,93,.f.)
          if not(empty(.w_TDTPRDES))
          .link_4_13('Full')
          endif
        .DoRTCalc(94,94,.f.)
          if not(empty(.w_TDESCCL1))
          .link_4_17('Full')
          endif
        .DoRTCalc(95,95,.f.)
          if not(empty(.w_TDESCCL2))
          .link_4_18('Full')
          endif
        .DoRTCalc(96,96,.f.)
          if not(empty(.w_TDESCCL3))
          .link_4_19('Full')
          endif
        .DoRTCalc(97,97,.f.)
          if not(empty(.w_TDESCCL4))
          .link_4_20('Full')
          endif
        .DoRTCalc(98,98,.f.)
          if not(empty(.w_TDESCCL5))
          .link_4_21('Full')
          endif
        .DoRTCalc(99,99,.f.)
          if not(empty(.w_TDSTACL1))
          .link_4_22('Full')
          endif
        .DoRTCalc(100,100,.f.)
          if not(empty(.w_TDSTACL2))
          .link_4_23('Full')
          endif
        .DoRTCalc(101,101,.f.)
          if not(empty(.w_TDSTACL3))
          .link_4_24('Full')
          endif
        .DoRTCalc(102,102,.f.)
          if not(empty(.w_TDSTACL4))
          .link_4_25('Full')
          endif
        .DoRTCalc(103,103,.f.)
          if not(empty(.w_TDSTACL5))
          .link_4_26('Full')
          endif
          .DoRTCalc(104,106,.f.)
        .w_CODI = .w_TDTIPDOC
        .w_DESC = .w_TDDESDOC
        .w_TDFLANAL = ' '
        .w_TDFLELAN = ' '
        .w_TDVOCECR = 'C'
        .w_TD_SEGNO = 'D'
        .w_TDFLPRAT = IIF(IsAlt(), 'S', 'N')
        .w_TDASSCES = 'N'
        .w_TDCAUCES = IIF(.w_TDASSCES<>'M', SPACE(5),.w_TDCAUCES)
        .DoRTCalc(115,115,.f.)
          if not(empty(.w_TDCAUCES))
          .link_3_7('Full')
          endif
        .w_TDCONASS = IIF(.w_TDCATDOC<>'DI','N','F')
        .w_TDCONASS = IIF(.w_TDCATDOC<>'DT','N','F')
        .DoRTCalc(118,118,.f.)
          if not(empty(.w_TDCODSTR))
          .link_3_10('Full')
          endif
        .w_TDFLARCO = ' '
        .w_TDLOTDIF = IIF(.w_TDFLARCO<>'S', 'I', .w_TDLOTDIF)
        .w_TDTIPIMB = IIF(.w_TDFLARCO<>'S' Or Empty(.w_FLCASC), 'N', .w_TDTIPIMB)
        .w_TDCAUPFI = .w_TDCAUPFI
        .DoRTCalc(122,122,.f.)
          if not(empty(.w_TDCAUPFI))
          .link_3_14('Full')
          endif
        .w_TDCAUCOD = .w_TDCAUCOD
        .DoRTCalc(123,123,.f.)
          if not(empty(.w_TDCAUCOD))
          .link_3_15('Full')
          endif
        .w_TDFLEXPL = IIF(g_VEFA='S' And .w_TDFLARCO='S' And g_DISB<>'S' Or .w_TDTIPIMB<>'N','S',.w_TDFLEXPL)
        .w_TDVALCOM = IIF(g_VEFA='S' And g_DISB <>'S' And .w_TDFLARCO='S' Or .w_TDTIPIMB<>'N', 'N', .w_TDVALCOM)
        .w_TDCOSEPL = 'N'
        .w_TDEXPAUT = IIF(g_VEFA='S' And g_DISB <>'S' And .w_TDFLARCO='S' Or .w_TDTIPIMB<>'N', 'S', IIF(g_DISB='S',.w_TDEXPAUT,' '))
        .w_TDMAXLEV = 99
        .oPgFrm.Page3.oPag.oObj_3_43.Calculate()
          .DoRTCalc(129,138,.f.)
        .w_CODI = .w_TDTIPDOC
        .w_DESC = .w_TDDESDOC
        .w_TDFLPROV = 'N'
        .w_TDFLSILI = IIF(.w_TDCATDOC='DT', 'S', ' ')
        .w_TDPRZVAC = ' '
        .w_TDPRZDES = ' '
          .DoRTCalc(145,145,.f.)
        .w_TDCHKTOT = ' '
        .w_TDFLBLEV = IIF(IsAlt(),'S','N')
          .DoRTCalc(148,148,.f.)
        .w_TDFLQRIO = ' '
        .w_TDFLPREV = ' '
        .w_TDFLAPCA = 'N'
        .w_TDRIPCON = IIF( .w_TDFLINTE <> "N" AND .w_TDCATDOC <> "RF" , .w_TDRIPCON, " " )
        .w_TDNOSTCO = 'N'
        .w_TDFLGETR = 'N'
        .w_TDFLSPIM = 'N'
        .w_TDFLSPIM = 'N'
          .DoRTCalc(157,157,.f.)
        .w_TDFLSPTR = 'N'
        .w_TDFLSPTR = 'N'
          .DoRTCalc(160,160,.f.)
        .w_TDRIPINC = " "
        .w_TDRIPIMB = " "
        .w_TDRIPTRA = " "
        .DoRTCalc(164,164,.f.)
          if not(empty(.w_TDMCALSI))
          .link_2_30('Full')
          endif
        .DoRTCalc(165,165,.f.)
          if not(empty(.w_TDMCALST))
          .link_2_31('Full')
          endif
          .DoRTCalc(166,166,.f.)
        .w_TDFLSCOR = IIF(.w_TDCATDOC='RF','S','I')
          .DoRTCalc(168,168,.f.)
        .w_TDFLCRIS = iif(Not .w_TDFLRISC$'SD',' ',.w_TDFLCRIS)
        .w_PRGSTA = IIF(.w_TDFLVEAC='A' AND g_ACQU<>'S', 'GSAC_MDV', 'GSVE_MDV')
        .w_PRGALT = IIF(.w_TDFLVEAC='A' AND g_ACQU<>'S', 'GSACAMDV', 'GSVEAMDV')
          .DoRTCalc(172,172,.f.)
        .w_TDMINIMP = IIF(.w_TFFLGEFA<>'B',0,.w_TDMINIMP)
        .w_TDMINVAL = IIF(.w_TDMINIMP=0 Or .w_TFFLGEFA<>'B','', g_PERVAL)
        .DoRTCalc(174,174,.f.)
          if not(empty(.w_TDMINVAL))
          .link_1_105('Full')
          endif
        .w_TDTOTDOC = iif(.w_TDCATDOC $ 'FA-NC-RF','E', iif(empty(.w_TDTOTDOC), 'E',.w_TDTOTDOC))
        .w_TDIMPMIN = IIF(.w_TDTOTDOC<>'E', .w_TDIMPMIN,0)
        .w_TDIVAMIN = IIF(.w_TDIMPMIN=0, 'L', .w_TDIVAMIN)
        .w_TDMINVEN = iif(.w_TDCATDOC $ 'FA-NC','E', iif(empty(.w_TDMINVEN), 'E',.w_TDMINVEN)) 
        .w_TDRIOTOT = iif(.w_TDCATDOC $ 'FA-NC-RF' OR .w_TDMINVEN='E','R', .w_TDRIOTOT)
        .DoRTCalc(180,183,.f.)
          if not(empty(.w_TDMCALSI))
          .link_2_42('Full')
          endif
        .DoRTCalc(184,184,.f.)
          if not(empty(.w_TDMCALST))
          .link_2_43('Full')
          endif
        .w_TDCHKUCA = IIF(.w_TDFLVEAC='A' OR .w_TFFLGEFA='B' OR .w_TDCATDOC='NC', 'N', IIF( EMPTY(.w_TDCHKUCA) , 'N', .w_TDCHKUCA ) )
        .w_DASERIAL = '0000000001'
        .DoRTCalc(186,186,.f.)
          if not(empty(.w_DASERIAL))
          .link_4_41('Full')
          endif
          .DoRTCalc(187,193,.f.)
        .w_TDFLIA01 = 'N'
        .w_TDFLIA02 = 'N'
        .w_TDFLIA03 = 'N'
        .w_TDFLIA04 = 'N'
        .w_TDFLIA05 = 'N'
        .w_TDFLIA06 = 'N'
        .w_TDFLRA01 = IIF(.w_TDFLIA01<>'S', 'N', EVL(NVL(.w_TDFLRA01, 'N'), 'N') )
        .oPgFrm.Page4.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
        .w_TDFLRA02 = IIF(.w_TDFLIA02<>'S', 'N', EVL(NVL(.w_TDFLRA02, 'N'), 'N') )
        .w_TDFLRA03 = IIF(.w_TDFLIA03<>'S', 'N', EVL(NVL(.w_TDFLRA03, 'N'), 'N') )
        .w_TDFLRA04 = IIF(.w_TDFLIA04<>'S', 'N', EVL(NVL(.w_TDFLRA04, 'N'), 'N') )
        .w_TDFLRA05 = IIF(.w_TDFLIA05<>'S', 'N', EVL(NVL(.w_TDFLRA05, 'N'), 'N') )
        .w_TDFLRA06 = IIF(.w_TDFLIA06<>'S', 'N', EVL(NVL(.w_TDFLRA06, 'N'), 'N') )
        .w_DATOOLTI = "Se attivo: il campo aggiuntivo %1 sar� importato"
          .DoRTCalc(207,210,.f.)
        .w_TDFLCASH = ' '
        .w_TDFLNORC = 'N'
        .w_TDFLCOMM = ' '
          .DoRTCalc(214,214,.f.)
        .w_TDFLGEIN = iif(.w_TDCATDOC='DT',.w_TDFLGEIN,'N')
        .w_TDTIPDFE = Space(4)
        .w_TDDESEST = Space(35)
        .w_TDCODCLA = Space(5)
      endif
    endwith
    cp_BlankRecExtFlds(this,'TIP_DOCU')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page2.oPag.oBtn_2_46.enabled = this.oPgFrm.Page2.oPag.oBtn_2_46.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsve_atd
    * --- Valorizzo, a seguito di un caricamento, il flag rifiutato a 'N'
    if this.cFunction='Load'
     this.w_TDFLRIFI = 'N'
    endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTDTIPDOC_1_1.enabled = i_bVal
      .Page1.oPag.oTDDESDOC_1_2.enabled = i_bVal
      .Page1.oPag.oTDCATDOC_1_3.enabled = i_bVal
      .Page1.oPag.oTDCATDOC_1_4.enabled = i_bVal
      .Page1.oPag.oTDCATDOC_1_5.enabled = i_bVal
      .Page1.oPag.oTFFLGEFA_1_6.enabled = i_bVal
      .Page1.oPag.oTFFLGEFA_1_7.enabled = i_bVal
      .Page1.oPag.oTDFLRICE_1_8.enabled = i_bVal
      .Page1.oPag.oTDFLINTE_1_9.enabled = i_bVal
      .Page1.oPag.oTDFLINTE_1_10.enabled = i_bVal
      .Page1.oPag.oTDEMERIC_1_12.enabled = i_bVal
      .Page1.oPag.oTDRICNOM_1_13.enabled = i_bVal
      .Page1.oPag.oTDFLATIP_1_14.enabled = i_bVal
      .Page1.oPag.oTDFLGCHI_1_15.enabled = i_bVal
      .Page1.oPag.oTDCAUMAG_1_17.enabled = i_bVal
      .Page1.oPag.oTDCODMAG_1_22.enabled = i_bVal
      .Page1.oPag.oTDFLMGPR_1_23.enabled = i_bVal
      .Page1.oPag.oTDCODMAT_1_25.enabled = i_bVal
      .Page1.oPag.oTDFLMTPR_1_26.enabled = i_bVal
      .Page1.oPag.oTDCAUCON_1_27.enabled = i_bVal
      .Page1.oPag.oTDALFDOC_1_32.enabled = i_bVal
      .Page1.oPag.oTDFLPDOC_1_33.enabled = i_bVal
      .Page1.oPag.oTDFLPDOC_1_34.enabled = i_bVal
      .Page1.oPag.oTDNUMSCO_1_35.enabled = i_bVal
      .Page1.oPag.oTDSERPRO_1_36.enabled = i_bVal
      .Page1.oPag.oTDFLPPRO_1_37.enabled = i_bVal
      .Page1.oPag.oTDCODLIS_1_38.enabled = i_bVal
      .Page1.oPag.oTDQTADEF_1_39.enabled = i_bVal
      .Page1.oPag.oTDPROVVI_1_40.enabled = i_bVal
      .Page1.oPag.oTDASPETT_1_43.enabled = i_bVal
      .Page1.oPag.oTFFLRAGG_1_44.enabled = i_bVal
      .Page1.oPag.oTDFLPREF_1_45.enabled = i_bVal
      .Page1.oPag.oTDNOPRSC_1_46.enabled = i_bVal
      .Page1.oPag.oTDFLDTPR_1_47.enabled = i_bVal
      .Page1.oPag.oTDFLACCO_1_48.enabled = i_bVal
      .Page1.oPag.oTDFLPACK_1_49.enabled = i_bVal
      .Page1.oPag.oTDBOLDOG_1_50.enabled = i_bVal
      .Page1.oPag.oTDFLCCAU_1_51.enabled = i_bVal
      .Page1.oPag.oTDFLRIFI_1_52.enabled = i_bVal
      .Page1.oPag.oTDFLRIAT_1_53.enabled = i_bVal
      .Page1.oPag.oTDFLBACA_1_54.enabled = i_bVal
      .Page1.oPag.oTDSEQPRE_1_55.enabled = i_bVal
      .Page1.oPag.oTDSEQSCO_1_56.enabled = i_bVal
      .Page1.oPag.oTDMODDES_1_57.enabled = i_bVal
      .Page1.oPag.oTDSEQMA1_1_58.enabled = i_bVal
      .Page1.oPag.oTDSEQMA2_1_59.enabled = i_bVal
      .Page5.oPag.oTDFLNSTA_5_4.enabled = i_bVal
      .Page5.oPag.oTDFLSTLM_5_8.enabled = i_bVal
      .Page4.oPag.oTDDESRIF_4_3.enabled = i_bVal
      .Page4.oPag.oTDMODRIF_4_4.enabled = i_bVal
      .Page4.oPag.oTDFLNSRI_4_8.enabled = i_bVal
      .Page4.oPag.oTDTPNDOC_4_9.enabled = i_bVal
      .Page4.oPag.oTDFLVSRI_4_10.enabled = i_bVal
      .Page4.oPag.oTDTPVDOC_4_11.enabled = i_bVal
      .Page4.oPag.oTDFLRIDE_4_12.enabled = i_bVal
      .Page4.oPag.oTDTPRDES_4_13.enabled = i_bVal
      .Page4.oPag.oTDESCCL1_4_17.enabled = i_bVal
      .Page4.oPag.oTDESCCL2_4_18.enabled = i_bVal
      .Page4.oPag.oTDESCCL3_4_19.enabled = i_bVal
      .Page4.oPag.oTDESCCL4_4_20.enabled = i_bVal
      .Page4.oPag.oTDESCCL5_4_21.enabled = i_bVal
      .Page4.oPag.oTDSTACL1_4_22.enabled = i_bVal
      .Page4.oPag.oTDSTACL2_4_23.enabled = i_bVal
      .Page4.oPag.oTDSTACL3_4_24.enabled = i_bVal
      .Page4.oPag.oTDSTACL4_4_25.enabled = i_bVal
      .Page4.oPag.oTDSTACL5_4_26.enabled = i_bVal
      .Page4.oPag.oTDFLIMPA_4_37.enabled = i_bVal
      .Page4.oPag.oTDFLIMAC_4_38.enabled = i_bVal
      .Page3.oPag.oTDFLANAL_3_1.enabled = i_bVal
      .Page3.oPag.oTDFLELAN_3_2.enabled = i_bVal
      .Page3.oPag.oTDVOCECR_3_3.enabled = i_bVal
      .Page3.oPag.oTD_SEGNO_3_4.enabled = i_bVal
      .Page3.oPag.oTDFLPRAT_3_5.enabled = i_bVal
      .Page3.oPag.oTDASSCES_3_6.enabled = i_bVal
      .Page3.oPag.oTDCAUCES_3_7.enabled = i_bVal
      .Page3.oPag.oTDCONASS_3_8.enabled = i_bVal
      .Page3.oPag.oTDCONASS_3_9.enabled = i_bVal
      .Page3.oPag.oTDCODSTR_3_10.enabled = i_bVal
      .Page3.oPag.oTDFLARCO_3_11.enabled = i_bVal
      .Page3.oPag.oTDLOTDIF_3_12.enabled = i_bVal
      .Page3.oPag.oTDTIPIMB_3_13.enabled = i_bVal
      .Page3.oPag.oTDCAUPFI_3_14.enabled = i_bVal
      .Page3.oPag.oTDCAUCOD_3_15.enabled = i_bVal
      .Page3.oPag.oTDFLEXPL_3_16.enabled = i_bVal
      .Page3.oPag.oTDVALCOM_3_17.enabled = i_bVal
      .Page3.oPag.oTDCOSEPL_3_18.enabled = i_bVal
      .Page3.oPag.oTDEXPAUT_3_19.enabled = i_bVal
      .Page3.oPag.oTDMAXLEV_3_20.enabled = i_bVal
      .Page2.oPag.oTDFLPROV_2_4.enabled = i_bVal
      .Page2.oPag.oTDFLSILI_2_5.enabled = i_bVal
      .Page2.oPag.oTDPRZVAC_2_6.enabled = i_bVal
      .Page2.oPag.oTDPRZDES_2_7.enabled = i_bVal
      .Page2.oPag.oTDFLVALO_2_8.enabled = i_bVal
      .Page2.oPag.oTDCHKTOT_2_9.enabled = i_bVal
      .Page2.oPag.oTDFLBLEV_2_10.enabled = i_bVal
      .Page2.oPag.oTDFLSPIN_2_11.enabled = i_bVal
      .Page2.oPag.oTDFLQRIO_2_12.enabled = i_bVal
      .Page2.oPag.oTDFLPREV_2_13.enabled = i_bVal
      .Page2.oPag.oTDFLAPCA_2_14.enabled = i_bVal
      .Page2.oPag.oTDRIPCON_2_15.enabled = i_bVal
      .Page2.oPag.oTDNOSTCO_2_16.enabled = i_bVal
      .Page2.oPag.oTDFLGETR_2_17.enabled = i_bVal
      .Page2.oPag.oTDFLSPIM_2_19.enabled = i_bVal
      .Page2.oPag.oTDFLSPIM_2_20.enabled = i_bVal
      .Page2.oPag.oTDFLSPTR_2_23.enabled = i_bVal
      .Page2.oPag.oTDFLSPTR_2_24.enabled = i_bVal
      .Page2.oPag.oTDRIPINC_2_27.enabled = i_bVal
      .Page2.oPag.oTDRIPIMB_2_28.enabled = i_bVal
      .Page2.oPag.oTDRIPTRA_2_29.enabled = i_bVal
      .Page2.oPag.oTDMCALSI_2_30.enabled = i_bVal
      .Page2.oPag.oTDMCALST_2_31.enabled = i_bVal
      .Page2.oPag.oTDSINCFL_2_32.enabled = i_bVal
      .Page2.oPag.oTDFLSCOR_2_33.enabled = i_bVal
      .Page2.oPag.oTDFLRISC_2_34.enabled = i_bVal
      .Page2.oPag.oTDFLCRIS_2_35.enabled = i_bVal
      .Page1.oPag.oTDMINIMP_1_104.enabled = i_bVal
      .Page1.oPag.oTDMINVAL_1_105.enabled = i_bVal
      .Page1.oPag.oTDTOTDOC_1_106.enabled = i_bVal
      .Page1.oPag.oTDIMPMIN_1_107.enabled = i_bVal
      .Page1.oPag.oTDIVAMIN_1_108.enabled = i_bVal
      .Page1.oPag.oTDMINVEN_1_109.enabled = i_bVal
      .Page1.oPag.oTDRIOTOT_1_110.enabled = i_bVal
      .Page2.oPag.oTDMCALSI_2_42.enabled = i_bVal
      .Page2.oPag.oTDMCALST_2_43.enabled = i_bVal
      .Page2.oPag.oTDCHKUCA_2_44.enabled = i_bVal
      .Page4.oPag.oTDFLIA01_4_49.enabled = i_bVal
      .Page4.oPag.oTDFLIA02_4_50.enabled = i_bVal
      .Page4.oPag.oTDFLIA03_4_51.enabled = i_bVal
      .Page4.oPag.oTDFLIA04_4_52.enabled = i_bVal
      .Page4.oPag.oTDFLIA05_4_53.enabled = i_bVal
      .Page4.oPag.oTDFLIA06_4_54.enabled = i_bVal
      .Page4.oPag.oTDFLRA01_4_56.enabled = i_bVal
      .Page4.oPag.oTDFLRA02_4_67.enabled = i_bVal
      .Page4.oPag.oTDFLRA03_4_68.enabled = i_bVal
      .Page4.oPag.oTDFLRA04_4_69.enabled = i_bVal
      .Page4.oPag.oTDFLRA05_4_70.enabled = i_bVal
      .Page4.oPag.oTDFLRA06_4_71.enabled = i_bVal
      .Page3.oPag.oTDFLCASH_3_58.enabled = i_bVal
      .Page3.oPag.oTDFLNORC_3_59.enabled = i_bVal
      .Page3.oPag.oTDFLCOMM_3_60.enabled = i_bVal
      .Page1.oPag.oTDFLGEIN_1_127.enabled = i_bVal
      .Page3.oPag.oTDTIPDFE_3_64.enabled = i_bVal
      .Page3.oPag.oTDDESEST_3_65.enabled = i_bVal
      .Page3.oPag.oTDCODCLA_3_66.enabled = i_bVal
      .Page2.oPag.oBtn_2_46.enabled = .Page2.oPag.oBtn_2_46.mCond()
      .Page1.oPag.oObj_1_87.enabled = i_bVal
      .Page1.oPag.oObj_1_88.enabled = i_bVal
      .Page1.oPag.oObj_1_92.enabled = i_bVal
      .Page1.oPag.oObj_1_93.enabled = i_bVal
      .Page1.oPag.oObj_1_98.enabled = i_bVal
      .Page3.oPag.oObj_3_43.enabled = i_bVal
      .Page4.oPag.CAMAGG01.enabled = i_bVal
      .Page4.oPag.CAMAGG02.enabled = i_bVal
      .Page4.oPag.CAMAGG03.enabled = i_bVal
      .Page4.oPag.CAMAGG04.enabled = i_bVal
      .Page4.oPag.CAMAGG05.enabled = i_bVal
      .Page4.oPag.CAMAGG06.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTDTIPDOC_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTDTIPDOC_1_1.enabled = .t.
      endif
    endwith
    this.GSVE_MTD.SetStatus(i_cOp)
    this.GSVE_MDC.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'TIP_DOCU',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSVE_MTD.SetChildrenStatus(i_cOp)
  *  this.GSVE_MDC.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTIPDOC,"TDTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDDESDOC,"TDDESDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCATDOC,"TDCATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCATDOC,"TDCATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCATDOC,"TDCATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TFFLGEFA,"TFFLGEFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TFFLGEFA,"TFFLGEFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRICE,"TDFLRICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLINTE,"TDFLINTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLINTE,"TDFLINTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLVEAC,"TDFLVEAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDEMERIC,"TDEMERIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDRICNOM,"TDRICNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLATIP,"TDFLATIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLGCHI,"TDFLGCHI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDPRODOC,"TDPRODOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUMAG,"TDCAUMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCODMAG,"TDCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLMGPR,"TDFLMGPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCODMAT,"TDCODMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLMTPR,"TDFLMTPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUCON,"TDCAUCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDALFDOC,"TDALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPDOC,"TDFLPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPDOC,"TDFLPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDNUMSCO,"TDNUMSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSERPRO,"TDSERPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPPRO,"TDFLPPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCODLIS,"TDCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDQTADEF,"TDQTADEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDPROVVI,"TDPROVVI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDASPETT,"TDASPETT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TFFLRAGG,"TFFLRAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPREF,"TDFLPREF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDNOPRSC,"TDNOPRSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLDTPR,"TDFLDTPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLACCO,"TDFLACCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPACK,"TDFLPACK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDBOLDOG,"TDBOLDOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLCCAU,"TDFLCCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRIFI,"TDFLRIFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRIAT,"TDFLRIAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLBACA,"TDFLBACA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSEQPRE,"TDSEQPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSEQSCO,"TDSEQSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMODDES,"TDMODDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSEQMA1,"TDSEQMA1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSEQMA2,"TDSEQMA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLORAT,"TDFLORAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUPFI,"TDCAUPFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUCOD,"TDCAUCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLEXPL,"TDFLEXPL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDPROSTA,"TDPROSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLDATT,"TDFLDATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDEXPAUT,"TDEXPAUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLNSTA,"TDFLNSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSTLM,"TDFLSTLM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDDESRIF,"TDDESRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMODRIF,"TDMODRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLNSRI,"TDFLNSRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTPNDOC,"TDTPNDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLVSRI,"TDFLVSRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTPVDOC,"TDTPVDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRIDE,"TDFLRIDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTPRDES,"TDTPRDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDESCCL1,"TDESCCL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDESCCL2,"TDESCCL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDESCCL3,"TDESCCL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDESCCL4,"TDESCCL4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDESCCL5,"TDESCCL5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSTACL1,"TDSTACL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSTACL2,"TDSTACL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSTACL3,"TDSTACL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSTACL4,"TDSTACL4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSTACL5,"TDSTACL5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIMPA,"TDFLIMPA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIMAC,"TDFLIMAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLANAL,"TDFLANAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLELAN,"TDFLELAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDVOCECR,"TDVOCECR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TD_SEGNO,"TD_SEGNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPRAT,"TDFLPRAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDASSCES,"TDASSCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUCES,"TDCAUCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCONASS,"TDCONASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCONASS,"TDCONASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCODSTR,"TDCODSTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLARCO,"TDFLARCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDLOTDIF,"TDLOTDIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTIPIMB,"TDTIPIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUPFI,"TDCAUPFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUCOD,"TDCAUCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLEXPL,"TDFLEXPL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDVALCOM,"TDVALCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCOSEPL,"TDCOSEPL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDEXPAUT,"TDEXPAUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMAXLEV,"TDMAXLEV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPROV,"TDFLPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSILI,"TDFLSILI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDPRZVAC,"TDPRZVAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDPRZDES,"TDPRZDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLVALO,"TDFLVALO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCHKTOT,"TDCHKTOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLBLEV,"TDFLBLEV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSPIN,"TDFLSPIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLQRIO,"TDFLQRIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPREV,"TDFLPREV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLAPCA,"TDFLAPCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDRIPCON,"TDRIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDNOSTCO,"TDNOSTCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLGETR,"TDFLGETR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSPIM,"TDFLSPIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSPIM,"TDFLSPIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSPTR,"TDFLSPTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSPTR,"TDFLSPTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDRIPINC,"TDRIPINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDRIPIMB,"TDRIPIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDRIPTRA,"TDRIPTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMCALSI,"TDMCALSI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMCALST,"TDMCALST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSINCFL,"TDSINCFL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSCOR,"TDFLSCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRISC,"TDFLRISC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLCRIS,"TDFLCRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMINIMP,"TDMINIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMINVAL,"TDMINVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTOTDOC,"TDTOTDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDIMPMIN,"TDIMPMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDIVAMIN,"TDIVAMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMINVEN,"TDMINVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDRIOTOT,"TDRIOTOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMCALSI,"TDMCALSI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMCALST,"TDMCALST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCHKUCA,"TDCHKUCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIA01,"TDFLIA01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIA02,"TDFLIA02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIA03,"TDFLIA03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIA04,"TDFLIA04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIA05,"TDFLIA05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIA06,"TDFLIA06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRA01,"TDFLRA01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRA02,"TDFLRA02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRA03,"TDFLRA03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRA04,"TDFLRA04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRA05,"TDFLRA05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRA06,"TDFLRA06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLCASH,"TDFLCASH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLNORC,"TDFLNORC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLCOMM,"TDFLCOMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLGEIN,"TDFLGEIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTIPDFE,"TDTIPDFE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDDESEST,"TDDESEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCODCLA,"TDCODCLA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsve_atd
    * --- aggiunge alla Chiave ulteriore filtro su Ciclo e Categoria
    IF NOT EMPTY(i_cWhere)
       IF AT('TDFLVEAC', UPPER(i_cWhere))=0 AND g_ACQU='S'
          i_cWhere=i_cWhere+" and TDFLVEAC='V'"
       ENDIF
       IF AT('TDCATDOC', UPPER(i_cWhere))=0
          i_cWhere=i_cWhere+" and TDCATDOC<>'OR'"
       ENDIF
    ENDIF
    
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    i_lTable = "TIP_DOCU"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TIP_DOCU_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSVE_SCD with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TIP_DOCU_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TIP_DOCU
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TIP_DOCU')
        i_extval=cp_InsertValODBCExtFlds(this,'TIP_DOCU')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TDTIPDOC,TDDESDOC,TDCATDOC,TFFLGEFA,TDFLRICE"+;
                  ",TDFLINTE,TDFLVEAC,TDEMERIC,TDRICNOM,TDFLATIP"+;
                  ",TDFLGCHI,TDPRODOC,TDCAUMAG,TDCODMAG,TDFLMGPR"+;
                  ",TDCODMAT,TDFLMTPR,TDCAUCON,TDALFDOC,TDFLPDOC"+;
                  ",TDNUMSCO,TDSERPRO,TDFLPPRO,TDCODLIS,TDQTADEF"+;
                  ",TDPROVVI,TDASPETT,TFFLRAGG,TDFLPREF,TDNOPRSC"+;
                  ",TDFLDTPR,TDFLACCO,TDFLPACK,TDBOLDOG,TDFLCCAU"+;
                  ",TDFLRIFI,TDFLRIAT,TDFLBACA,TDSEQPRE,TDSEQSCO"+;
                  ",TDMODDES,TDSEQMA1,TDSEQMA2,TDFLORAT,TDCAUPFI"+;
                  ",TDCAUCOD,TDFLEXPL,TDPROSTA,TDFLDATT,TDEXPAUT"+;
                  ",TDFLNSTA,TDFLSTLM,TDDESRIF,TDMODRIF,TDFLNSRI"+;
                  ",TDTPNDOC,TDFLVSRI,TDTPVDOC,TDFLRIDE,TDTPRDES"+;
                  ",TDESCCL1,TDESCCL2,TDESCCL3,TDESCCL4,TDESCCL5"+;
                  ",TDSTACL1,TDSTACL2,TDSTACL3,TDSTACL4,TDSTACL5"+;
                  ",TDFLIMPA,TDFLIMAC,TDFLANAL,TDFLELAN,TDVOCECR"+;
                  ",TD_SEGNO,TDFLPRAT,TDASSCES,TDCAUCES,TDCONASS"+;
                  ",TDCODSTR,TDFLARCO,TDLOTDIF,TDTIPIMB,TDVALCOM"+;
                  ",TDCOSEPL,TDMAXLEV,TDFLPROV,TDFLSILI,TDPRZVAC"+;
                  ",TDPRZDES,TDFLVALO,TDCHKTOT,TDFLBLEV,TDFLSPIN"+;
                  ",TDFLQRIO,TDFLPREV,TDFLAPCA,TDRIPCON,TDNOSTCO"+;
                  ",TDFLGETR,TDFLSPIM,TDFLSPTR,TDRIPINC,TDRIPIMB"+;
                  ",TDRIPTRA,TDMCALSI,TDMCALST,TDSINCFL,TDFLSCOR"+;
                  ",TDFLRISC,TDFLCRIS,TDMINIMP,TDMINVAL,TDTOTDOC"+;
                  ",TDIMPMIN,TDIVAMIN,TDMINVEN,TDRIOTOT,TDCHKUCA"+;
                  ",TDFLIA01,TDFLIA02,TDFLIA03,TDFLIA04,TDFLIA05"+;
                  ",TDFLIA06,TDFLRA01,TDFLRA02,TDFLRA03,TDFLRA04"+;
                  ",TDFLRA05,TDFLRA06,UTCC,UTCV,UTDC"+;
                  ",UTDV,TDFLCASH,TDFLNORC,TDFLCOMM,TDFLGEIN"+;
                  ",TDTIPDFE,TDDESEST,TDCODCLA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TDTIPDOC)+;
                  ","+cp_ToStrODBC(this.w_TDDESDOC)+;
                  ","+cp_ToStrODBC(this.w_TDCATDOC)+;
                  ","+cp_ToStrODBC(this.w_TFFLGEFA)+;
                  ","+cp_ToStrODBC(this.w_TDFLRICE)+;
                  ","+cp_ToStrODBC(this.w_TDFLINTE)+;
                  ","+cp_ToStrODBC(this.w_TDFLVEAC)+;
                  ","+cp_ToStrODBC(this.w_TDEMERIC)+;
                  ","+cp_ToStrODBC(this.w_TDRICNOM)+;
                  ","+cp_ToStrODBC(this.w_TDFLATIP)+;
                  ","+cp_ToStrODBC(this.w_TDFLGCHI)+;
                  ","+cp_ToStrODBC(this.w_TDPRODOC)+;
                  ","+cp_ToStrODBCNull(this.w_TDCAUMAG)+;
                  ","+cp_ToStrODBCNull(this.w_TDCODMAG)+;
                  ","+cp_ToStrODBC(this.w_TDFLMGPR)+;
                  ","+cp_ToStrODBCNull(this.w_TDCODMAT)+;
                  ","+cp_ToStrODBC(this.w_TDFLMTPR)+;
                  ","+cp_ToStrODBCNull(this.w_TDCAUCON)+;
                  ","+cp_ToStrODBC(this.w_TDALFDOC)+;
                  ","+cp_ToStrODBC(this.w_TDFLPDOC)+;
                  ","+cp_ToStrODBC(this.w_TDNUMSCO)+;
                  ","+cp_ToStrODBC(this.w_TDSERPRO)+;
                  ","+cp_ToStrODBC(this.w_TDFLPPRO)+;
                  ","+cp_ToStrODBCNull(this.w_TDCODLIS)+;
                  ","+cp_ToStrODBC(this.w_TDQTADEF)+;
                  ","+cp_ToStrODBC(this.w_TDPROVVI)+;
                  ","+cp_ToStrODBC(this.w_TDASPETT)+;
                  ","+cp_ToStrODBC(this.w_TFFLRAGG)+;
                  ","+cp_ToStrODBC(this.w_TDFLPREF)+;
                  ","+cp_ToStrODBC(this.w_TDNOPRSC)+;
                  ","+cp_ToStrODBC(this.w_TDFLDTPR)+;
                  ","+cp_ToStrODBC(this.w_TDFLACCO)+;
                  ","+cp_ToStrODBC(this.w_TDFLPACK)+;
                  ","+cp_ToStrODBC(this.w_TDBOLDOG)+;
                  ","+cp_ToStrODBC(this.w_TDFLCCAU)+;
                  ","+cp_ToStrODBC(this.w_TDFLRIFI)+;
                  ","+cp_ToStrODBC(this.w_TDFLRIAT)+;
                  ","+cp_ToStrODBC(this.w_TDFLBACA)+;
                  ","+cp_ToStrODBC(this.w_TDSEQPRE)+;
                  ","+cp_ToStrODBC(this.w_TDSEQSCO)+;
                  ","+cp_ToStrODBC(this.w_TDMODDES)+;
                  ","+cp_ToStrODBC(this.w_TDSEQMA1)+;
                  ","+cp_ToStrODBC(this.w_TDSEQMA2)+;
                  ","+cp_ToStrODBC(this.w_TDFLORAT)+;
                  ","+cp_ToStrODBCNull(this.w_TDCAUPFI)+;
                  ","+cp_ToStrODBCNull(this.w_TDCAUCOD)+;
                  ","+cp_ToStrODBC(this.w_TDFLEXPL)+;
                  ","+cp_ToStrODBC(this.w_TDPROSTA)+;
                  ","+cp_ToStrODBC(this.w_TDFLDATT)+;
                  ","+cp_ToStrODBC(this.w_TDEXPAUT)+;
                  ","+cp_ToStrODBC(this.w_TDFLNSTA)+;
                  ","+cp_ToStrODBC(this.w_TDFLSTLM)+;
                  ","+cp_ToStrODBC(this.w_TDDESRIF)+;
                  ","+cp_ToStrODBCNull(this.w_TDMODRIF)+;
                  ","+cp_ToStrODBC(this.w_TDFLNSRI)+;
                  ","+cp_ToStrODBCNull(this.w_TDTPNDOC)+;
                  ","+cp_ToStrODBC(this.w_TDFLVSRI)+;
                  ","+cp_ToStrODBCNull(this.w_TDTPVDOC)+;
                  ","+cp_ToStrODBC(this.w_TDFLRIDE)+;
                  ","+cp_ToStrODBCNull(this.w_TDTPRDES)+;
                  ","+cp_ToStrODBCNull(this.w_TDESCCL1)+;
                  ","+cp_ToStrODBCNull(this.w_TDESCCL2)+;
                  ","+cp_ToStrODBCNull(this.w_TDESCCL3)+;
                  ","+cp_ToStrODBCNull(this.w_TDESCCL4)+;
                  ","+cp_ToStrODBCNull(this.w_TDESCCL5)+;
                  ","+cp_ToStrODBCNull(this.w_TDSTACL1)+;
                  ","+cp_ToStrODBCNull(this.w_TDSTACL2)+;
                  ","+cp_ToStrODBCNull(this.w_TDSTACL3)+;
                  ","+cp_ToStrODBCNull(this.w_TDSTACL4)+;
                  ","+cp_ToStrODBCNull(this.w_TDSTACL5)+;
                  ","+cp_ToStrODBC(this.w_TDFLIMPA)+;
                  ","+cp_ToStrODBC(this.w_TDFLIMAC)+;
                  ","+cp_ToStrODBC(this.w_TDFLANAL)+;
                  ","+cp_ToStrODBC(this.w_TDFLELAN)+;
                  ","+cp_ToStrODBC(this.w_TDVOCECR)+;
                  ","+cp_ToStrODBC(this.w_TD_SEGNO)+;
                  ","+cp_ToStrODBC(this.w_TDFLPRAT)+;
                  ","+cp_ToStrODBC(this.w_TDASSCES)+;
                  ","+cp_ToStrODBCNull(this.w_TDCAUCES)+;
                  ","+cp_ToStrODBC(this.w_TDCONASS)+;
                  ","+cp_ToStrODBCNull(this.w_TDCODSTR)+;
                  ","+cp_ToStrODBC(this.w_TDFLARCO)+;
                  ","+cp_ToStrODBC(this.w_TDLOTDIF)+;
                  ","+cp_ToStrODBC(this.w_TDTIPIMB)+;
                  ","+cp_ToStrODBC(this.w_TDVALCOM)+;
                  ","+cp_ToStrODBC(this.w_TDCOSEPL)+;
                  ","+cp_ToStrODBC(this.w_TDMAXLEV)+;
                  ","+cp_ToStrODBC(this.w_TDFLPROV)+;
                  ","+cp_ToStrODBC(this.w_TDFLSILI)+;
                  ","+cp_ToStrODBC(this.w_TDPRZVAC)+;
                  ","+cp_ToStrODBC(this.w_TDPRZDES)+;
                  ","+cp_ToStrODBC(this.w_TDFLVALO)+;
                  ","+cp_ToStrODBC(this.w_TDCHKTOT)+;
                  ","+cp_ToStrODBC(this.w_TDFLBLEV)+;
                  ","+cp_ToStrODBC(this.w_TDFLSPIN)+;
                  ","+cp_ToStrODBC(this.w_TDFLQRIO)+;
                  ","+cp_ToStrODBC(this.w_TDFLPREV)+;
                  ","+cp_ToStrODBC(this.w_TDFLAPCA)+;
                  ","+cp_ToStrODBC(this.w_TDRIPCON)+;
                  ","+cp_ToStrODBC(this.w_TDNOSTCO)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBC(this.w_TDFLGETR)+;
                  ","+cp_ToStrODBC(this.w_TDFLSPIM)+;
                  ","+cp_ToStrODBC(this.w_TDFLSPTR)+;
                  ","+cp_ToStrODBC(this.w_TDRIPINC)+;
                  ","+cp_ToStrODBC(this.w_TDRIPIMB)+;
                  ","+cp_ToStrODBC(this.w_TDRIPTRA)+;
                  ","+cp_ToStrODBCNull(this.w_TDMCALSI)+;
                  ","+cp_ToStrODBCNull(this.w_TDMCALST)+;
                  ","+cp_ToStrODBC(this.w_TDSINCFL)+;
                  ","+cp_ToStrODBC(this.w_TDFLSCOR)+;
                  ","+cp_ToStrODBC(this.w_TDFLRISC)+;
                  ","+cp_ToStrODBC(this.w_TDFLCRIS)+;
                  ","+cp_ToStrODBC(this.w_TDMINIMP)+;
                  ","+cp_ToStrODBCNull(this.w_TDMINVAL)+;
                  ","+cp_ToStrODBC(this.w_TDTOTDOC)+;
                  ","+cp_ToStrODBC(this.w_TDIMPMIN)+;
                  ","+cp_ToStrODBC(this.w_TDIVAMIN)+;
                  ","+cp_ToStrODBC(this.w_TDMINVEN)+;
                  ","+cp_ToStrODBC(this.w_TDRIOTOT)+;
                  ","+cp_ToStrODBC(this.w_TDCHKUCA)+;
                  ","+cp_ToStrODBC(this.w_TDFLIA01)+;
                  ","+cp_ToStrODBC(this.w_TDFLIA02)+;
                  ","+cp_ToStrODBC(this.w_TDFLIA03)+;
                  ","+cp_ToStrODBC(this.w_TDFLIA04)+;
                  ","+cp_ToStrODBC(this.w_TDFLIA05)+;
                  ","+cp_ToStrODBC(this.w_TDFLIA06)+;
                  ","+cp_ToStrODBC(this.w_TDFLRA01)+;
                  ","+cp_ToStrODBC(this.w_TDFLRA02)+;
                  ","+cp_ToStrODBC(this.w_TDFLRA03)+;
                  ","+cp_ToStrODBC(this.w_TDFLRA04)+;
                  ","+cp_ToStrODBC(this.w_TDFLRA05)+;
                  ","+cp_ToStrODBC(this.w_TDFLRA06)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_TDFLCASH)+;
                  ","+cp_ToStrODBC(this.w_TDFLNORC)+;
                  ","+cp_ToStrODBC(this.w_TDFLCOMM)+;
                  ","+cp_ToStrODBC(this.w_TDFLGEIN)+;
                  ","+cp_ToStrODBC(this.w_TDTIPDFE)+;
                  ","+cp_ToStrODBC(this.w_TDDESEST)+;
                  ","+cp_ToStrODBC(this.w_TDCODCLA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TIP_DOCU')
        i_extval=cp_InsertValVFPExtFlds(this,'TIP_DOCU')
        cp_CheckDeletedKey(i_cTable,0,'TDTIPDOC',this.w_TDTIPDOC)
        INSERT INTO (i_cTable);
              (TDTIPDOC,TDDESDOC,TDCATDOC,TFFLGEFA,TDFLRICE,TDFLINTE,TDFLVEAC,TDEMERIC,TDRICNOM,TDFLATIP,TDFLGCHI,TDPRODOC,TDCAUMAG,TDCODMAG,TDFLMGPR,TDCODMAT,TDFLMTPR,TDCAUCON,TDALFDOC,TDFLPDOC,TDNUMSCO,TDSERPRO,TDFLPPRO,TDCODLIS,TDQTADEF,TDPROVVI,TDASPETT,TFFLRAGG,TDFLPREF,TDNOPRSC,TDFLDTPR,TDFLACCO,TDFLPACK,TDBOLDOG,TDFLCCAU,TDFLRIFI,TDFLRIAT,TDFLBACA,TDSEQPRE,TDSEQSCO,TDMODDES,TDSEQMA1,TDSEQMA2,TDFLORAT,TDCAUPFI,TDCAUCOD,TDFLEXPL,TDPROSTA,TDFLDATT,TDEXPAUT,TDFLNSTA,TDFLSTLM,TDDESRIF,TDMODRIF,TDFLNSRI,TDTPNDOC,TDFLVSRI,TDTPVDOC,TDFLRIDE,TDTPRDES,TDESCCL1,TDESCCL2,TDESCCL3,TDESCCL4,TDESCCL5,TDSTACL1,TDSTACL2,TDSTACL3,TDSTACL4,TDSTACL5,TDFLIMPA,TDFLIMAC,TDFLANAL,TDFLELAN,TDVOCECR,TD_SEGNO,TDFLPRAT,TDASSCES,TDCAUCES,TDCONASS,TDCODSTR,TDFLARCO,TDLOTDIF,TDTIPIMB,TDVALCOM,TDCOSEPL,TDMAXLEV,TDFLPROV,TDFLSILI,TDPRZVAC,TDPRZDES,TDFLVALO,TDCHKTOT,TDFLBLEV,TDFLSPIN,TDFLQRIO,TDFLPREV,TDFLAPCA,TDRIPCON,TDNOSTCO,TDFLGETR,TDFLSPIM,TDFLSPTR,TDRIPINC,TDRIPIMB,TDRIPTRA,TDMCALSI,TDMCALST,TDSINCFL,TDFLSCOR,TDFLRISC,TDFLCRIS,TDMINIMP,TDMINVAL,TDTOTDOC,TDIMPMIN,TDIVAMIN,TDMINVEN,TDRIOTOT,TDCHKUCA,TDFLIA01,TDFLIA02,TDFLIA03,TDFLIA04,TDFLIA05,TDFLIA06,TDFLRA01,TDFLRA02,TDFLRA03,TDFLRA04,TDFLRA05,TDFLRA06,UTCC,UTCV,UTDC,UTDV,TDFLCASH,TDFLNORC,TDFLCOMM,TDFLGEIN,TDTIPDFE,TDDESEST,TDCODCLA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TDTIPDOC;
                  ,this.w_TDDESDOC;
                  ,this.w_TDCATDOC;
                  ,this.w_TFFLGEFA;
                  ,this.w_TDFLRICE;
                  ,this.w_TDFLINTE;
                  ,this.w_TDFLVEAC;
                  ,this.w_TDEMERIC;
                  ,this.w_TDRICNOM;
                  ,this.w_TDFLATIP;
                  ,this.w_TDFLGCHI;
                  ,this.w_TDPRODOC;
                  ,this.w_TDCAUMAG;
                  ,this.w_TDCODMAG;
                  ,this.w_TDFLMGPR;
                  ,this.w_TDCODMAT;
                  ,this.w_TDFLMTPR;
                  ,this.w_TDCAUCON;
                  ,this.w_TDALFDOC;
                  ,this.w_TDFLPDOC;
                  ,this.w_TDNUMSCO;
                  ,this.w_TDSERPRO;
                  ,this.w_TDFLPPRO;
                  ,this.w_TDCODLIS;
                  ,this.w_TDQTADEF;
                  ,this.w_TDPROVVI;
                  ,this.w_TDASPETT;
                  ,this.w_TFFLRAGG;
                  ,this.w_TDFLPREF;
                  ,this.w_TDNOPRSC;
                  ,this.w_TDFLDTPR;
                  ,this.w_TDFLACCO;
                  ,this.w_TDFLPACK;
                  ,this.w_TDBOLDOG;
                  ,this.w_TDFLCCAU;
                  ,this.w_TDFLRIFI;
                  ,this.w_TDFLRIAT;
                  ,this.w_TDFLBACA;
                  ,this.w_TDSEQPRE;
                  ,this.w_TDSEQSCO;
                  ,this.w_TDMODDES;
                  ,this.w_TDSEQMA1;
                  ,this.w_TDSEQMA2;
                  ,this.w_TDFLORAT;
                  ,this.w_TDCAUPFI;
                  ,this.w_TDCAUCOD;
                  ,this.w_TDFLEXPL;
                  ,this.w_TDPROSTA;
                  ,this.w_TDFLDATT;
                  ,this.w_TDEXPAUT;
                  ,this.w_TDFLNSTA;
                  ,this.w_TDFLSTLM;
                  ,this.w_TDDESRIF;
                  ,this.w_TDMODRIF;
                  ,this.w_TDFLNSRI;
                  ,this.w_TDTPNDOC;
                  ,this.w_TDFLVSRI;
                  ,this.w_TDTPVDOC;
                  ,this.w_TDFLRIDE;
                  ,this.w_TDTPRDES;
                  ,this.w_TDESCCL1;
                  ,this.w_TDESCCL2;
                  ,this.w_TDESCCL3;
                  ,this.w_TDESCCL4;
                  ,this.w_TDESCCL5;
                  ,this.w_TDSTACL1;
                  ,this.w_TDSTACL2;
                  ,this.w_TDSTACL3;
                  ,this.w_TDSTACL4;
                  ,this.w_TDSTACL5;
                  ,this.w_TDFLIMPA;
                  ,this.w_TDFLIMAC;
                  ,this.w_TDFLANAL;
                  ,this.w_TDFLELAN;
                  ,this.w_TDVOCECR;
                  ,this.w_TD_SEGNO;
                  ,this.w_TDFLPRAT;
                  ,this.w_TDASSCES;
                  ,this.w_TDCAUCES;
                  ,this.w_TDCONASS;
                  ,this.w_TDCODSTR;
                  ,this.w_TDFLARCO;
                  ,this.w_TDLOTDIF;
                  ,this.w_TDTIPIMB;
                  ,this.w_TDVALCOM;
                  ,this.w_TDCOSEPL;
                  ,this.w_TDMAXLEV;
                  ,this.w_TDFLPROV;
                  ,this.w_TDFLSILI;
                  ,this.w_TDPRZVAC;
                  ,this.w_TDPRZDES;
                  ,this.w_TDFLVALO;
                  ,this.w_TDCHKTOT;
                  ,this.w_TDFLBLEV;
                  ,this.w_TDFLSPIN;
                  ,this.w_TDFLQRIO;
                  ,this.w_TDFLPREV;
                  ,this.w_TDFLAPCA;
                  ,this.w_TDRIPCON;
                  ,this.w_TDNOSTCO;
                  ,this.w_TDFLGETR;
                  ,this.w_TDFLSPIM;
                  ,this.w_TDFLSPTR;
                  ,this.w_TDRIPINC;
                  ,this.w_TDRIPIMB;
                  ,this.w_TDRIPTRA;
                  ,this.w_TDMCALSI;
                  ,this.w_TDMCALST;
                  ,this.w_TDSINCFL;
                  ,this.w_TDFLSCOR;
                  ,this.w_TDFLRISC;
                  ,this.w_TDFLCRIS;
                  ,this.w_TDMINIMP;
                  ,this.w_TDMINVAL;
                  ,this.w_TDTOTDOC;
                  ,this.w_TDIMPMIN;
                  ,this.w_TDIVAMIN;
                  ,this.w_TDMINVEN;
                  ,this.w_TDRIOTOT;
                  ,this.w_TDCHKUCA;
                  ,this.w_TDFLIA01;
                  ,this.w_TDFLIA02;
                  ,this.w_TDFLIA03;
                  ,this.w_TDFLIA04;
                  ,this.w_TDFLIA05;
                  ,this.w_TDFLIA06;
                  ,this.w_TDFLRA01;
                  ,this.w_TDFLRA02;
                  ,this.w_TDFLRA03;
                  ,this.w_TDFLRA04;
                  ,this.w_TDFLRA05;
                  ,this.w_TDFLRA06;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_TDFLCASH;
                  ,this.w_TDFLNORC;
                  ,this.w_TDFLCOMM;
                  ,this.w_TDFLGEIN;
                  ,this.w_TDTIPDFE;
                  ,this.w_TDDESEST;
                  ,this.w_TDCODCLA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TIP_DOCU_IDX,i_nConn)
      *
      * update TIP_DOCU
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TIP_DOCU')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TDDESDOC="+cp_ToStrODBC(this.w_TDDESDOC)+;
             ",TDCATDOC="+cp_ToStrODBC(this.w_TDCATDOC)+;
             ",TFFLGEFA="+cp_ToStrODBC(this.w_TFFLGEFA)+;
             ",TDFLRICE="+cp_ToStrODBC(this.w_TDFLRICE)+;
             ",TDFLINTE="+cp_ToStrODBC(this.w_TDFLINTE)+;
             ",TDFLVEAC="+cp_ToStrODBC(this.w_TDFLVEAC)+;
             ",TDEMERIC="+cp_ToStrODBC(this.w_TDEMERIC)+;
             ",TDRICNOM="+cp_ToStrODBC(this.w_TDRICNOM)+;
             ",TDFLATIP="+cp_ToStrODBC(this.w_TDFLATIP)+;
             ",TDFLGCHI="+cp_ToStrODBC(this.w_TDFLGCHI)+;
             ",TDPRODOC="+cp_ToStrODBC(this.w_TDPRODOC)+;
             ",TDCAUMAG="+cp_ToStrODBCNull(this.w_TDCAUMAG)+;
             ",TDCODMAG="+cp_ToStrODBCNull(this.w_TDCODMAG)+;
             ",TDFLMGPR="+cp_ToStrODBC(this.w_TDFLMGPR)+;
             ",TDCODMAT="+cp_ToStrODBCNull(this.w_TDCODMAT)+;
             ",TDFLMTPR="+cp_ToStrODBC(this.w_TDFLMTPR)+;
             ",TDCAUCON="+cp_ToStrODBCNull(this.w_TDCAUCON)+;
             ",TDALFDOC="+cp_ToStrODBC(this.w_TDALFDOC)+;
             ",TDFLPDOC="+cp_ToStrODBC(this.w_TDFLPDOC)+;
             ",TDNUMSCO="+cp_ToStrODBC(this.w_TDNUMSCO)+;
             ",TDSERPRO="+cp_ToStrODBC(this.w_TDSERPRO)+;
             ",TDFLPPRO="+cp_ToStrODBC(this.w_TDFLPPRO)+;
             ",TDCODLIS="+cp_ToStrODBCNull(this.w_TDCODLIS)+;
             ",TDQTADEF="+cp_ToStrODBC(this.w_TDQTADEF)+;
             ",TDPROVVI="+cp_ToStrODBC(this.w_TDPROVVI)+;
             ",TDASPETT="+cp_ToStrODBC(this.w_TDASPETT)+;
             ",TFFLRAGG="+cp_ToStrODBC(this.w_TFFLRAGG)+;
             ",TDFLPREF="+cp_ToStrODBC(this.w_TDFLPREF)+;
             ",TDNOPRSC="+cp_ToStrODBC(this.w_TDNOPRSC)+;
             ",TDFLDTPR="+cp_ToStrODBC(this.w_TDFLDTPR)+;
             ",TDFLACCO="+cp_ToStrODBC(this.w_TDFLACCO)+;
             ",TDFLPACK="+cp_ToStrODBC(this.w_TDFLPACK)+;
             ",TDBOLDOG="+cp_ToStrODBC(this.w_TDBOLDOG)+;
             ",TDFLCCAU="+cp_ToStrODBC(this.w_TDFLCCAU)+;
             ",TDFLRIFI="+cp_ToStrODBC(this.w_TDFLRIFI)+;
             ",TDFLRIAT="+cp_ToStrODBC(this.w_TDFLRIAT)+;
             ",TDFLBACA="+cp_ToStrODBC(this.w_TDFLBACA)+;
             ",TDSEQPRE="+cp_ToStrODBC(this.w_TDSEQPRE)+;
             ",TDSEQSCO="+cp_ToStrODBC(this.w_TDSEQSCO)+;
             ",TDMODDES="+cp_ToStrODBC(this.w_TDMODDES)+;
             ",TDSEQMA1="+cp_ToStrODBC(this.w_TDSEQMA1)+;
             ",TDSEQMA2="+cp_ToStrODBC(this.w_TDSEQMA2)+;
             ",TDFLORAT="+cp_ToStrODBC(this.w_TDFLORAT)+;
             ",TDCAUPFI="+cp_ToStrODBCNull(this.w_TDCAUPFI)+;
             ",TDCAUCOD="+cp_ToStrODBCNull(this.w_TDCAUCOD)+;
             ",TDFLEXPL="+cp_ToStrODBC(this.w_TDFLEXPL)+;
             ",TDPROSTA="+cp_ToStrODBC(this.w_TDPROSTA)+;
             ",TDFLDATT="+cp_ToStrODBC(this.w_TDFLDATT)+;
             ",TDEXPAUT="+cp_ToStrODBC(this.w_TDEXPAUT)+;
             ",TDFLNSTA="+cp_ToStrODBC(this.w_TDFLNSTA)+;
             ",TDFLSTLM="+cp_ToStrODBC(this.w_TDFLSTLM)+;
             ",TDDESRIF="+cp_ToStrODBC(this.w_TDDESRIF)+;
             ",TDMODRIF="+cp_ToStrODBCNull(this.w_TDMODRIF)+;
             ",TDFLNSRI="+cp_ToStrODBC(this.w_TDFLNSRI)+;
             ",TDTPNDOC="+cp_ToStrODBCNull(this.w_TDTPNDOC)+;
             ",TDFLVSRI="+cp_ToStrODBC(this.w_TDFLVSRI)+;
             ",TDTPVDOC="+cp_ToStrODBCNull(this.w_TDTPVDOC)+;
             ",TDFLRIDE="+cp_ToStrODBC(this.w_TDFLRIDE)+;
             ",TDTPRDES="+cp_ToStrODBCNull(this.w_TDTPRDES)+;
             ",TDESCCL1="+cp_ToStrODBCNull(this.w_TDESCCL1)+;
             ",TDESCCL2="+cp_ToStrODBCNull(this.w_TDESCCL2)+;
             ",TDESCCL3="+cp_ToStrODBCNull(this.w_TDESCCL3)+;
             ",TDESCCL4="+cp_ToStrODBCNull(this.w_TDESCCL4)+;
             ",TDESCCL5="+cp_ToStrODBCNull(this.w_TDESCCL5)+;
             ",TDSTACL1="+cp_ToStrODBCNull(this.w_TDSTACL1)+;
             ",TDSTACL2="+cp_ToStrODBCNull(this.w_TDSTACL2)+;
             ",TDSTACL3="+cp_ToStrODBCNull(this.w_TDSTACL3)+;
             ",TDSTACL4="+cp_ToStrODBCNull(this.w_TDSTACL4)+;
             ",TDSTACL5="+cp_ToStrODBCNull(this.w_TDSTACL5)+;
             ",TDFLIMPA="+cp_ToStrODBC(this.w_TDFLIMPA)+;
             ",TDFLIMAC="+cp_ToStrODBC(this.w_TDFLIMAC)+;
             ",TDFLANAL="+cp_ToStrODBC(this.w_TDFLANAL)+;
             ",TDFLELAN="+cp_ToStrODBC(this.w_TDFLELAN)+;
             ",TDVOCECR="+cp_ToStrODBC(this.w_TDVOCECR)+;
             ",TD_SEGNO="+cp_ToStrODBC(this.w_TD_SEGNO)+;
             ",TDFLPRAT="+cp_ToStrODBC(this.w_TDFLPRAT)+;
             ",TDASSCES="+cp_ToStrODBC(this.w_TDASSCES)+;
             ",TDCAUCES="+cp_ToStrODBCNull(this.w_TDCAUCES)+;
             ",TDCONASS="+cp_ToStrODBC(this.w_TDCONASS)+;
             ",TDCODSTR="+cp_ToStrODBCNull(this.w_TDCODSTR)+;
             ",TDFLARCO="+cp_ToStrODBC(this.w_TDFLARCO)+;
             ",TDLOTDIF="+cp_ToStrODBC(this.w_TDLOTDIF)+;
             ",TDTIPIMB="+cp_ToStrODBC(this.w_TDTIPIMB)+;
             ",TDVALCOM="+cp_ToStrODBC(this.w_TDVALCOM)+;
             ",TDCOSEPL="+cp_ToStrODBC(this.w_TDCOSEPL)+;
             ",TDMAXLEV="+cp_ToStrODBC(this.w_TDMAXLEV)+;
             ",TDFLPROV="+cp_ToStrODBC(this.w_TDFLPROV)+;
             ",TDFLSILI="+cp_ToStrODBC(this.w_TDFLSILI)+;
             ",TDPRZVAC="+cp_ToStrODBC(this.w_TDPRZVAC)+;
             ",TDPRZDES="+cp_ToStrODBC(this.w_TDPRZDES)+;
             ",TDFLVALO="+cp_ToStrODBC(this.w_TDFLVALO)+;
             ",TDCHKTOT="+cp_ToStrODBC(this.w_TDCHKTOT)+;
             ",TDFLBLEV="+cp_ToStrODBC(this.w_TDFLBLEV)+;
             ",TDFLSPIN="+cp_ToStrODBC(this.w_TDFLSPIN)+;
             ",TDFLQRIO="+cp_ToStrODBC(this.w_TDFLQRIO)+;
             ",TDFLPREV="+cp_ToStrODBC(this.w_TDFLPREV)+;
             ",TDFLAPCA="+cp_ToStrODBC(this.w_TDFLAPCA)+;
             ",TDRIPCON="+cp_ToStrODBC(this.w_TDRIPCON)+;
             ",TDNOSTCO="+cp_ToStrODBC(this.w_TDNOSTCO)+;
             ",TDFLGETR="+cp_ToStrODBC(this.w_TDFLGETR)+;
             ""
             i_nnn=i_nnn+;
             ",TDFLSPIM="+cp_ToStrODBC(this.w_TDFLSPIM)+;
             ",TDFLSPTR="+cp_ToStrODBC(this.w_TDFLSPTR)+;
             ",TDRIPINC="+cp_ToStrODBC(this.w_TDRIPINC)+;
             ",TDRIPIMB="+cp_ToStrODBC(this.w_TDRIPIMB)+;
             ",TDRIPTRA="+cp_ToStrODBC(this.w_TDRIPTRA)+;
             ",TDMCALSI="+cp_ToStrODBCNull(this.w_TDMCALSI)+;
             ",TDMCALST="+cp_ToStrODBCNull(this.w_TDMCALST)+;
             ",TDSINCFL="+cp_ToStrODBC(this.w_TDSINCFL)+;
             ",TDFLSCOR="+cp_ToStrODBC(this.w_TDFLSCOR)+;
             ",TDFLRISC="+cp_ToStrODBC(this.w_TDFLRISC)+;
             ",TDFLCRIS="+cp_ToStrODBC(this.w_TDFLCRIS)+;
             ",TDMINIMP="+cp_ToStrODBC(this.w_TDMINIMP)+;
             ",TDMINVAL="+cp_ToStrODBCNull(this.w_TDMINVAL)+;
             ",TDTOTDOC="+cp_ToStrODBC(this.w_TDTOTDOC)+;
             ",TDIMPMIN="+cp_ToStrODBC(this.w_TDIMPMIN)+;
             ",TDIVAMIN="+cp_ToStrODBC(this.w_TDIVAMIN)+;
             ",TDMINVEN="+cp_ToStrODBC(this.w_TDMINVEN)+;
             ",TDRIOTOT="+cp_ToStrODBC(this.w_TDRIOTOT)+;
             ",TDCHKUCA="+cp_ToStrODBC(this.w_TDCHKUCA)+;
             ",TDFLIA01="+cp_ToStrODBC(this.w_TDFLIA01)+;
             ",TDFLIA02="+cp_ToStrODBC(this.w_TDFLIA02)+;
             ",TDFLIA03="+cp_ToStrODBC(this.w_TDFLIA03)+;
             ",TDFLIA04="+cp_ToStrODBC(this.w_TDFLIA04)+;
             ",TDFLIA05="+cp_ToStrODBC(this.w_TDFLIA05)+;
             ",TDFLIA06="+cp_ToStrODBC(this.w_TDFLIA06)+;
             ",TDFLRA01="+cp_ToStrODBC(this.w_TDFLRA01)+;
             ",TDFLRA02="+cp_ToStrODBC(this.w_TDFLRA02)+;
             ",TDFLRA03="+cp_ToStrODBC(this.w_TDFLRA03)+;
             ",TDFLRA04="+cp_ToStrODBC(this.w_TDFLRA04)+;
             ",TDFLRA05="+cp_ToStrODBC(this.w_TDFLRA05)+;
             ",TDFLRA06="+cp_ToStrODBC(this.w_TDFLRA06)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",TDFLCASH="+cp_ToStrODBC(this.w_TDFLCASH)+;
             ",TDFLNORC="+cp_ToStrODBC(this.w_TDFLNORC)+;
             ",TDFLCOMM="+cp_ToStrODBC(this.w_TDFLCOMM)+;
             ",TDFLGEIN="+cp_ToStrODBC(this.w_TDFLGEIN)+;
             ",TDTIPDFE="+cp_ToStrODBC(this.w_TDTIPDFE)+;
             ",TDDESEST="+cp_ToStrODBC(this.w_TDDESEST)+;
             ",TDCODCLA="+cp_ToStrODBC(this.w_TDCODCLA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TIP_DOCU')
        i_cWhere = cp_PKFox(i_cTable  ,'TDTIPDOC',this.w_TDTIPDOC  )
        UPDATE (i_cTable) SET;
              TDDESDOC=this.w_TDDESDOC;
             ,TDCATDOC=this.w_TDCATDOC;
             ,TFFLGEFA=this.w_TFFLGEFA;
             ,TDFLRICE=this.w_TDFLRICE;
             ,TDFLINTE=this.w_TDFLINTE;
             ,TDFLVEAC=this.w_TDFLVEAC;
             ,TDEMERIC=this.w_TDEMERIC;
             ,TDRICNOM=this.w_TDRICNOM;
             ,TDFLATIP=this.w_TDFLATIP;
             ,TDFLGCHI=this.w_TDFLGCHI;
             ,TDPRODOC=this.w_TDPRODOC;
             ,TDCAUMAG=this.w_TDCAUMAG;
             ,TDCODMAG=this.w_TDCODMAG;
             ,TDFLMGPR=this.w_TDFLMGPR;
             ,TDCODMAT=this.w_TDCODMAT;
             ,TDFLMTPR=this.w_TDFLMTPR;
             ,TDCAUCON=this.w_TDCAUCON;
             ,TDALFDOC=this.w_TDALFDOC;
             ,TDFLPDOC=this.w_TDFLPDOC;
             ,TDNUMSCO=this.w_TDNUMSCO;
             ,TDSERPRO=this.w_TDSERPRO;
             ,TDFLPPRO=this.w_TDFLPPRO;
             ,TDCODLIS=this.w_TDCODLIS;
             ,TDQTADEF=this.w_TDQTADEF;
             ,TDPROVVI=this.w_TDPROVVI;
             ,TDASPETT=this.w_TDASPETT;
             ,TFFLRAGG=this.w_TFFLRAGG;
             ,TDFLPREF=this.w_TDFLPREF;
             ,TDNOPRSC=this.w_TDNOPRSC;
             ,TDFLDTPR=this.w_TDFLDTPR;
             ,TDFLACCO=this.w_TDFLACCO;
             ,TDFLPACK=this.w_TDFLPACK;
             ,TDBOLDOG=this.w_TDBOLDOG;
             ,TDFLCCAU=this.w_TDFLCCAU;
             ,TDFLRIFI=this.w_TDFLRIFI;
             ,TDFLRIAT=this.w_TDFLRIAT;
             ,TDFLBACA=this.w_TDFLBACA;
             ,TDSEQPRE=this.w_TDSEQPRE;
             ,TDSEQSCO=this.w_TDSEQSCO;
             ,TDMODDES=this.w_TDMODDES;
             ,TDSEQMA1=this.w_TDSEQMA1;
             ,TDSEQMA2=this.w_TDSEQMA2;
             ,TDFLORAT=this.w_TDFLORAT;
             ,TDCAUPFI=this.w_TDCAUPFI;
             ,TDCAUCOD=this.w_TDCAUCOD;
             ,TDFLEXPL=this.w_TDFLEXPL;
             ,TDPROSTA=this.w_TDPROSTA;
             ,TDFLDATT=this.w_TDFLDATT;
             ,TDEXPAUT=this.w_TDEXPAUT;
             ,TDFLNSTA=this.w_TDFLNSTA;
             ,TDFLSTLM=this.w_TDFLSTLM;
             ,TDDESRIF=this.w_TDDESRIF;
             ,TDMODRIF=this.w_TDMODRIF;
             ,TDFLNSRI=this.w_TDFLNSRI;
             ,TDTPNDOC=this.w_TDTPNDOC;
             ,TDFLVSRI=this.w_TDFLVSRI;
             ,TDTPVDOC=this.w_TDTPVDOC;
             ,TDFLRIDE=this.w_TDFLRIDE;
             ,TDTPRDES=this.w_TDTPRDES;
             ,TDESCCL1=this.w_TDESCCL1;
             ,TDESCCL2=this.w_TDESCCL2;
             ,TDESCCL3=this.w_TDESCCL3;
             ,TDESCCL4=this.w_TDESCCL4;
             ,TDESCCL5=this.w_TDESCCL5;
             ,TDSTACL1=this.w_TDSTACL1;
             ,TDSTACL2=this.w_TDSTACL2;
             ,TDSTACL3=this.w_TDSTACL3;
             ,TDSTACL4=this.w_TDSTACL4;
             ,TDSTACL5=this.w_TDSTACL5;
             ,TDFLIMPA=this.w_TDFLIMPA;
             ,TDFLIMAC=this.w_TDFLIMAC;
             ,TDFLANAL=this.w_TDFLANAL;
             ,TDFLELAN=this.w_TDFLELAN;
             ,TDVOCECR=this.w_TDVOCECR;
             ,TD_SEGNO=this.w_TD_SEGNO;
             ,TDFLPRAT=this.w_TDFLPRAT;
             ,TDASSCES=this.w_TDASSCES;
             ,TDCAUCES=this.w_TDCAUCES;
             ,TDCONASS=this.w_TDCONASS;
             ,TDCODSTR=this.w_TDCODSTR;
             ,TDFLARCO=this.w_TDFLARCO;
             ,TDLOTDIF=this.w_TDLOTDIF;
             ,TDTIPIMB=this.w_TDTIPIMB;
             ,TDVALCOM=this.w_TDVALCOM;
             ,TDCOSEPL=this.w_TDCOSEPL;
             ,TDMAXLEV=this.w_TDMAXLEV;
             ,TDFLPROV=this.w_TDFLPROV;
             ,TDFLSILI=this.w_TDFLSILI;
             ,TDPRZVAC=this.w_TDPRZVAC;
             ,TDPRZDES=this.w_TDPRZDES;
             ,TDFLVALO=this.w_TDFLVALO;
             ,TDCHKTOT=this.w_TDCHKTOT;
             ,TDFLBLEV=this.w_TDFLBLEV;
             ,TDFLSPIN=this.w_TDFLSPIN;
             ,TDFLQRIO=this.w_TDFLQRIO;
             ,TDFLPREV=this.w_TDFLPREV;
             ,TDFLAPCA=this.w_TDFLAPCA;
             ,TDRIPCON=this.w_TDRIPCON;
             ,TDNOSTCO=this.w_TDNOSTCO;
             ,TDFLGETR=this.w_TDFLGETR;
             ,TDFLSPIM=this.w_TDFLSPIM;
             ,TDFLSPTR=this.w_TDFLSPTR;
             ,TDRIPINC=this.w_TDRIPINC;
             ,TDRIPIMB=this.w_TDRIPIMB;
             ,TDRIPTRA=this.w_TDRIPTRA;
             ,TDMCALSI=this.w_TDMCALSI;
             ,TDMCALST=this.w_TDMCALST;
             ,TDSINCFL=this.w_TDSINCFL;
             ,TDFLSCOR=this.w_TDFLSCOR;
             ,TDFLRISC=this.w_TDFLRISC;
             ,TDFLCRIS=this.w_TDFLCRIS;
             ,TDMINIMP=this.w_TDMINIMP;
             ,TDMINVAL=this.w_TDMINVAL;
             ,TDTOTDOC=this.w_TDTOTDOC;
             ,TDIMPMIN=this.w_TDIMPMIN;
             ,TDIVAMIN=this.w_TDIVAMIN;
             ,TDMINVEN=this.w_TDMINVEN;
             ,TDRIOTOT=this.w_TDRIOTOT;
             ,TDCHKUCA=this.w_TDCHKUCA;
             ,TDFLIA01=this.w_TDFLIA01;
             ,TDFLIA02=this.w_TDFLIA02;
             ,TDFLIA03=this.w_TDFLIA03;
             ,TDFLIA04=this.w_TDFLIA04;
             ,TDFLIA05=this.w_TDFLIA05;
             ,TDFLIA06=this.w_TDFLIA06;
             ,TDFLRA01=this.w_TDFLRA01;
             ,TDFLRA02=this.w_TDFLRA02;
             ,TDFLRA03=this.w_TDFLRA03;
             ,TDFLRA04=this.w_TDFLRA04;
             ,TDFLRA05=this.w_TDFLRA05;
             ,TDFLRA06=this.w_TDFLRA06;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,TDFLCASH=this.w_TDFLCASH;
             ,TDFLNORC=this.w_TDFLNORC;
             ,TDFLCOMM=this.w_TDFLCOMM;
             ,TDFLGEIN=this.w_TDFLGEIN;
             ,TDTIPDFE=this.w_TDTIPDFE;
             ,TDDESEST=this.w_TDDESEST;
             ,TDCODCLA=this.w_TDCODCLA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSVE_MTD : Saving
      this.GSVE_MTD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_TDTIPDOC,"LGCODICE";
             )
      this.GSVE_MTD.mReplace()
      * --- GSVE_MDC : Saving
      this.GSVE_MDC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_TDTIPDOC,"DCCODICE";
             )
      this.GSVE_MDC.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsve_atd
    * --- Controllo Origini: se la causale di magazzino diminuisce l'esistenza
    * --- o aumenta il riservato non posso attivare il check Raggruppa.
    * --- Attivazione check Raggruppa nelle Origini se attivo flag Calcolo Qt� Riordino
    this.NotifyEvent('ChkOrigini')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSVE_MTD : Deleting
    this.GSVE_MTD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_TDTIPDOC,"LGCODICE";
           )
    this.GSVE_MTD.mDelete()
    * --- GSVE_MDC : Deleting
    this.GSVE_MDC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_TDTIPDOC,"DCCODICE";
           )
    this.GSVE_MDC.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TIP_DOCU_IDX,i_nConn)
      *
      * delete TIP_DOCU
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TDTIPDOC',this.w_TDTIPDOC  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDFLRICE = ' '
        endif
        .DoRTCalc(9,10,.t.)
            .w_TDFLVEAC = IIF(.w_TDFLINTE='F', 'A', 'V')
        if .o_TDFLVEAC<>.w_TDFLVEAC
            .w_TDEMERIC = .w_TDFLVEAC
        endif
        .DoRTCalc(13,15,.t.)
            .w_TDPRODOC = IIF(.w_TDFLVEAC='V' AND .w_TDCATDOC $ 'DT-DI' AND .w_TDFLPDOC='S', 'NN', CALCPD(.w_TDCATDOC,.w_TDFLVEAC))
        .DoRTCalc(17,21,.t.)
        if .o_TDCAUMAG<>.w_TDCAUMAG
            .w_TDCODMAG = SPACE(5)
          .link_1_22('Full')
        endif
        .DoRTCalc(23,24,.t.)
        if .o_TDCAUMAG<>.w_TDCAUMAG
            .w_TDCODMAT = SPACE(5)
          .link_1_25('Full')
        endif
        .DoRTCalc(26,26,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDCAUCON = IIF(NOT IsAlt(),IIF(.w_TDCATDOC $ 'FA-NC-RF', .w_TDCAUCON, SPACE(5)),IIF(.w_TDCATDOC='FA','400',IIF(.w_TDCATDOC='NC','450',SPACE(5))))
          .link_1_27('Full')
        endif
        .DoRTCalc(28,31,.t.)
        if .o_TDCAUCON<>.w_TDCAUCON
            .w_TDALFDOC = IIF(NOT EMPTY(.w_SERDOC) AND (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC'), .w_SERDOC,.w_TDALFDOC)
        endif
        if .o_TDFLVEAC<>.w_TDFLVEAC.or. .o_TDCATDOC<>.w_TDCATDOC
            .w_TDFLPDOC = IIF(.w_TDFLVEAC='A' AND g_ACQU<>'S', 'S', ' ')
        endif
        if .o_TDFLVEAC<>.w_TDFLVEAC.or. .o_TDCATDOC<>.w_TDCATDOC.or. .o_TDEMERIC<>.w_TDEMERIC
            .w_TDFLPDOC = IIF(.w_TDEMERIC='A' OR .w_TDFLVEAC='A' AND g_ACQU<>'S', 'S', ' ')
        endif
        .DoRTCalc(35,35,.t.)
        if .o_TDCAUCON<>.w_TDCAUCON
            .w_TDSERPRO = IIF(NOT EMPTY(.w_SERPRO) AND (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC' OR (.w_TDCATDOC='DI' AND NOT EMPTY(.w_TDCAUCON) AND .w_TDFLPPRO='P')), .w_SERPRO, .w_TDSERPRO)
        endif
        if .o_TDCATDOC<>.w_TDCATDOC.or. .o_TDCAUCON<>.w_TDCAUCON
            .w_TDFLPPRO = IIF(empty(.w_TDCAUCON), 'N', 'P')
        endif
        .DoRTCalc(38,42,.t.)
        if .o_TDFLACCO<>.w_TDFLACCO
            .w_TDASPETT = IIF(.w_TDFLACCO='S',.w_TDASPETT,SPACE(30))
        endif
        if .o_TFFLGEFA<>.w_TFFLGEFA
            .w_TFFLRAGG = '1'
        endif
        .DoRTCalc(45,49,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC.or. .o_TDFLVEAC<>.w_TDFLVEAC
            .w_TDBOLDOG = ' '
        endif
        if .o_TFFLGEFA<>.w_TFFLGEFA
            .w_TDFLCCAU = IIF(.w_TFFLGEFA = 'B', ' ', .w_TDFLCCAU)
        endif
        if .o_TDCATDOC<>.w_TDCATDOC.or. .o_TDCATDOC<>.w_TDCATDOC
            .w_TDFLRIFI = iif(.w_TDCATDOC='DI',.w_TDFLRIFI,'N')
        endif
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDFLRIAT = IIF(.w_TDCATDOC $ 'FA-NC-RF-NC', .w_TDFLRIAT, ' ')
        endif
        .DoRTCalc(54,56,.t.)
        if .o_TDTIPDOC<>.w_TDTIPDOC
            .w_TDMODDES = 'S'
        endif
        .DoRTCalc(58,59,.t.)
        if .o_TDFLACCO<>.w_TDFLACCO
            .w_TDFLORAT = IIF(.w_TDFLACCO='S','S',' ')
        endif
        .DoRTCalc(61,65,.t.)
          .link_1_79('Full')
          .link_1_80('Full')
        .DoRTCalc(68,71,.t.)
        if .o_TDFLACCO<>.w_TDFLACCO
            .w_TDFLDATT = IIF(.w_TDFLACCO='S','S',' ')
        endif
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        if .o_TDCATDOC<>.w_TDCATDOC
          .Calculate_NYOCLEVCLU()
        endif
        .DoRTCalc(73,79,.t.)
            .w_CODI = .w_TDTIPDOC
            .w_DESC = .w_TDDESDOC
        .DoRTCalc(82,83,.t.)
            .w_CODI = .w_TDTIPDOC
        .DoRTCalc(85,86,.t.)
            .w_DESC = .w_TDDESDOC
        .DoRTCalc(88,106,.t.)
            .w_CODI = .w_TDTIPDOC
            .w_DESC = .w_TDDESDOC
        .DoRTCalc(109,109,.t.)
        if .o_TDFLANAL<>.w_TDFLANAL
            .w_TDFLELAN = ' '
        endif
        .DoRTCalc(111,114,.t.)
        if .o_TDASSCES<>.w_TDASSCES
            .w_TDCAUCES = IIF(.w_TDASSCES<>'M', SPACE(5),.w_TDCAUCES)
          .link_3_7('Full')
        endif
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDCONASS = IIF(.w_TDCATDOC<>'DI','N','F')
        endif
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDCONASS = IIF(.w_TDCATDOC<>'DT','N','F')
        endif
        .DoRTCalc(118,119,.t.)
        if .o_TDFLARCO<>.w_TDFLARCO
            .w_TDLOTDIF = IIF(.w_TDFLARCO<>'S', 'I', .w_TDLOTDIF)
        endif
        if .o_TDFLARCO<>.w_TDFLARCO.or. .o_TDCAUMAG<>.w_TDCAUMAG
            .w_TDTIPIMB = IIF(.w_TDFLARCO<>'S' Or Empty(.w_FLCASC), 'N', .w_TDTIPIMB)
        endif
        .DoRTCalc(122,123,.t.)
        if .o_TDFLARCO<>.w_TDFLARCO.or. .o_TDTIPDOC<>.w_TDTIPDOC.or. .o_TDTIPIMB<>.w_TDTIPIMB
            .w_TDFLEXPL = IIF(g_VEFA='S' And .w_TDFLARCO='S' And g_DISB<>'S' Or .w_TDTIPIMB<>'N','S',.w_TDFLEXPL)
        endif
        if .o_TDFLARCO<>.w_TDFLARCO.or. .o_TDTIPDOC<>.w_TDTIPDOC.or. .o_TDTIPIMB<>.w_TDTIPIMB
            .w_TDVALCOM = IIF(g_VEFA='S' And g_DISB <>'S' And .w_TDFLARCO='S' Or .w_TDTIPIMB<>'N', 'N', .w_TDVALCOM)
        endif
        .DoRTCalc(126,126,.t.)
        if .o_TDFLARCO<>.w_TDFLARCO.or. .o_TDTIPDOC<>.w_TDTIPDOC.or. .o_TDTIPIMB<>.w_TDTIPIMB
            .w_TDEXPAUT = IIF(g_VEFA='S' And g_DISB <>'S' And .w_TDFLARCO='S' Or .w_TDTIPIMB<>'N', 'S', IIF(g_DISB='S',.w_TDEXPAUT,' '))
        endif
        if .o_TDFLEXPL<>.w_TDFLEXPL
            .w_TDMAXLEV = 99
        endif
        .oPgFrm.Page3.oPag.oObj_3_43.Calculate()
        .DoRTCalc(129,138,.t.)
            .w_CODI = .w_TDTIPDOC
            .w_DESC = .w_TDDESDOC
        .DoRTCalc(141,141,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDFLSILI = IIF(.w_TDCATDOC='DT', 'S', ' ')
        endif
        .DoRTCalc(143,150,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC.or. .o_TDFLINTE<>.w_TDFLINTE
            .w_TDFLAPCA = 'N'
        endif
        if .o_TDFLINTE<>.w_TDFLINTE
            .w_TDRIPCON = IIF( .w_TDFLINTE <> "N" AND .w_TDCATDOC <> "RF" , .w_TDRIPCON, " " )
        endif
        .DoRTCalc(153,166,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDFLSCOR = IIF(.w_TDCATDOC='RF','S','I')
        endif
        .DoRTCalc(168,168,.t.)
        if .o_TDFLRISC<>.w_TDFLRISC
            .w_TDFLCRIS = iif(Not .w_TDFLRISC$'SD',' ',.w_TDFLCRIS)
        endif
            .w_PRGSTA = IIF(.w_TDFLVEAC='A' AND g_ACQU<>'S', 'GSAC_MDV', 'GSVE_MDV')
            .w_PRGALT = IIF(.w_TDFLVEAC='A' AND g_ACQU<>'S', 'GSACAMDV', 'GSVEAMDV')
        .DoRTCalc(172,172,.t.)
        if .o_TFFLGEFA<>.w_TFFLGEFA
            .w_TDMINIMP = IIF(.w_TFFLGEFA<>'B',0,.w_TDMINIMP)
        endif
        if .o_TDMINIMP<>.w_TDMINIMP.or. .o_TFFLGEFA<>.w_TFFLGEFA
            .w_TDMINVAL = IIF(.w_TDMINIMP=0 Or .w_TFFLGEFA<>'B','', g_PERVAL)
          .link_1_105('Full')
        endif
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDTOTDOC = iif(.w_TDCATDOC $ 'FA-NC-RF','E', iif(empty(.w_TDTOTDOC), 'E',.w_TDTOTDOC))
        endif
        if .o_TDTOTDOC<>.w_TDTOTDOC
            .w_TDIMPMIN = IIF(.w_TDTOTDOC<>'E', .w_TDIMPMIN,0)
        endif
        if .o_TDIMPMIN<>.w_TDIMPMIN
            .w_TDIVAMIN = IIF(.w_TDIMPMIN=0, 'L', .w_TDIVAMIN)
        endif
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDMINVEN = iif(.w_TDCATDOC $ 'FA-NC','E', iif(empty(.w_TDMINVEN), 'E',.w_TDMINVEN)) 
        endif
        if .o_TDCATDOC<>.w_TDCATDOC.or. .o_TDMINVEN<>.w_TDMINVEN
            .w_TDRIOTOT = iif(.w_TDCATDOC $ 'FA-NC-RF' OR .w_TDMINVEN='E','R', .w_TDRIOTOT)
        endif
        if .o_TDCATDOC<>.w_TDCATDOC
          .Calculate_MVQMBMGSBU()
        endif
        .DoRTCalc(180,184,.t.)
        if .o_TDFLVEAC<>.w_TDFLVEAC.or. .o_TFFLGEFA<>.w_TFFLGEFA.or. .o_TDCATDOC<>.w_TDCATDOC
            .w_TDCHKUCA = IIF(.w_TDFLVEAC='A' OR .w_TFFLGEFA='B' OR .w_TDCATDOC='NC', 'N', IIF( EMPTY(.w_TDCHKUCA) , 'N', .w_TDCHKUCA ) )
        endif
          .link_4_41('Full')
        .DoRTCalc(187,199,.t.)
        if .o_TDFLIA01<>.w_TDFLIA01
            .w_TDFLRA01 = IIF(.w_TDFLIA01<>'S', 'N', EVL(NVL(.w_TDFLRA01, 'N'), 'N') )
        endif
        .oPgFrm.Page4.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
        if .o_TDFLIA02<>.w_TDFLIA02
            .w_TDFLRA02 = IIF(.w_TDFLIA02<>'S', 'N', EVL(NVL(.w_TDFLRA02, 'N'), 'N') )
        endif
        if .o_TDFLIA03<>.w_TDFLIA03
            .w_TDFLRA03 = IIF(.w_TDFLIA03<>'S', 'N', EVL(NVL(.w_TDFLRA03, 'N'), 'N') )
        endif
        if .o_TDFLIA04<>.w_TDFLIA04
            .w_TDFLRA04 = IIF(.w_TDFLIA04<>'S', 'N', EVL(NVL(.w_TDFLRA04, 'N'), 'N') )
        endif
        if .o_TDFLIA05<>.w_TDFLIA05
            .w_TDFLRA05 = IIF(.w_TDFLIA05<>'S', 'N', EVL(NVL(.w_TDFLRA05, 'N'), 'N') )
        endif
        if .o_TDFLIA06<>.w_TDFLIA06
            .w_TDFLRA06 = IIF(.w_TDFLIA06<>'S', 'N', EVL(NVL(.w_TDFLRA06, 'N'), 'N') )
        endif
        .DoRTCalc(206,210,.t.)
        if .o_TDFLCOMM<>.w_TDFLCOMM
            .w_TDFLCASH = ' '
        endif
        .DoRTCalc(212,212,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDFLCOMM = ' '
        endif
        if .o_TDFLSILI<>.w_TDFLSILI
          .Calculate_FQEPSKTYNM()
        endif
        .DoRTCalc(214,214,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDFLGEIN = iif(.w_TDCATDOC='DT',.w_TDFLGEIN,'N')
        endif
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDTIPDFE = Space(4)
        endif
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDDESEST = Space(35)
        endif
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDCODCLA = Space(5)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_43.Calculate()
        .oPgFrm.Page4.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
    endwith
  return

  proc Calculate_AIYACBJCCS()
    with this
          * --- Sbianca al cambio causale flag rischio
          .w_TDFLCRIS = ' '
          .w_TDFLRISC = ' '
    endwith
  endproc
  proc Calculate_NYOCLEVCLU()
    with this
          * --- Forza scorporo piede fattura
          .w_TDFLSCOR = IIF( .w_TDCATDOC = 'RF', 'S', .w_TDFLSCOR )
    endwith
  endproc
  proc Calculate_MVQMBMGSBU()
    with this
          * --- Disattiva TDFLATIP
          .w_TDFLATIP = IIF(.w_TDCATDOC <> 'FA' , 'N', .w_TDFLATIP)
    endwith
  endproc
  proc Calculate_FQJJFNNWOT()
    with this
          * --- Inizializza etichette campi aggiuntivi
      if !EMPTY(NVL(.w_DACAM_01, ' '))
          .w_OBJ_CTRL = this.getCtrl("w"+"_TDFLIA01")
      endif
      if !EMPTY(NVL(.w_DACAM_01, ' '))
          .w_OBJ_CTRL.TooltipText = ah_MsgFormat(.w_DATOOLTI, ALLTRIM(.w_DACAM_01) )
      endif
      if !EMPTY(NVL(.w_DACAM_02, ' '))
          .w_OBJ_CTRL = this.getCtrl("w"+"_TDFLIA02")
      endif
      if !EMPTY(NVL(.w_DACAM_02, ' '))
          .w_OBJ_CTRL.TooltipText = ah_MsgFormat(.w_DATOOLTI, ALLTRIM(.w_DACAM_02) )
      endif
      if !EMPTY(NVL(.w_DACAM_03, ' '))
          .w_OBJ_CTRL = this.getCtrl("w"+"_TDFLIA03")
      endif
      if !EMPTY(NVL(.w_DACAM_03, ' '))
          .w_OBJ_CTRL.TooltipText = ah_MsgFormat(.w_DATOOLTI, ALLTRIM(.w_DACAM_03) )
      endif
      if !EMPTY(NVL(.w_DACAM_04, ' '))
          .w_OBJ_CTRL = this.getCtrl("w"+"_TDFLIA04")
      endif
      if !EMPTY(NVL(.w_DACAM_04, ' '))
          .w_OBJ_CTRL.TooltipText = ah_MsgFormat(.w_DATOOLTI, ALLTRIM(.w_DACAM_04) )
      endif
      if !EMPTY(NVL(.w_DACAM_05, ' '))
          .w_OBJ_CTRL = this.getCtrl("w"+"_TDFLIA05")
      endif
      if !EMPTY(NVL(.w_DACAM_05, ' '))
          .w_OBJ_CTRL.TooltipText = ah_MsgFormat(.w_DATOOLTI, ALLTRIM(.w_DACAM_05) )
      endif
      if !EMPTY(NVL(.w_DACAM_06, ' '))
          .w_OBJ_CTRL = this.getCtrl("w"+"_TDFLIA06")
      endif
      if !EMPTY(NVL(.w_DACAM_06, ' '))
          .w_OBJ_CTRL.TooltipText = ah_MsgFormat(.w_DATOOLTI, ALLTRIM(.w_DACAM_06) )
      endif
    endwith
  endproc
  proc Calculate_FQEPSKTYNM()
    with this
          * --- Attivazione/disattivazione flag "Aggiornamento lettere di intento"
     if Upper(This.cFunction)='EDIT'
          .w_MSG_AGGLETT = Ah_ErrorMsg("Attenzione, attivando/disattivando il flag � necessario ricostruire%0l'importo utilizzato sulle singole dichiarazioni")
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTDCATDOC_1_3.enabled = this.oPgFrm.Page1.oPag.oTDCATDOC_1_3.mCond()
    this.oPgFrm.Page1.oPag.oTDCATDOC_1_4.enabled = this.oPgFrm.Page1.oPag.oTDCATDOC_1_4.mCond()
    this.oPgFrm.Page1.oPag.oTDCATDOC_1_5.enabled = this.oPgFrm.Page1.oPag.oTDCATDOC_1_5.mCond()
    this.oPgFrm.Page1.oPag.oTFFLGEFA_1_6.enabled = this.oPgFrm.Page1.oPag.oTFFLGEFA_1_6.mCond()
    this.oPgFrm.Page1.oPag.oTFFLGEFA_1_7.enabled = this.oPgFrm.Page1.oPag.oTFFLGEFA_1_7.mCond()
    this.oPgFrm.Page1.oPag.oTDFLRICE_1_8.enabled = this.oPgFrm.Page1.oPag.oTDFLRICE_1_8.mCond()
    this.oPgFrm.Page1.oPag.oTDFLINTE_1_9.enabled = this.oPgFrm.Page1.oPag.oTDFLINTE_1_9.mCond()
    this.oPgFrm.Page1.oPag.oTDFLINTE_1_10.enabled = this.oPgFrm.Page1.oPag.oTDFLINTE_1_10.mCond()
    this.oPgFrm.Page1.oPag.oTDCODMAG_1_22.enabled = this.oPgFrm.Page1.oPag.oTDCODMAG_1_22.mCond()
    this.oPgFrm.Page1.oPag.oTDCODMAT_1_25.enabled = this.oPgFrm.Page1.oPag.oTDCODMAT_1_25.mCond()
    this.oPgFrm.Page1.oPag.oTDFLMTPR_1_26.enabled = this.oPgFrm.Page1.oPag.oTDFLMTPR_1_26.mCond()
    this.oPgFrm.Page1.oPag.oTDCAUCON_1_27.enabled = this.oPgFrm.Page1.oPag.oTDCAUCON_1_27.mCond()
    this.oPgFrm.Page1.oPag.oTDFLPDOC_1_33.enabled = this.oPgFrm.Page1.oPag.oTDFLPDOC_1_33.mCond()
    this.oPgFrm.Page1.oPag.oTDFLPDOC_1_34.enabled = this.oPgFrm.Page1.oPag.oTDFLPDOC_1_34.mCond()
    this.oPgFrm.Page1.oPag.oTDSERPRO_1_36.enabled = this.oPgFrm.Page1.oPag.oTDSERPRO_1_36.mCond()
    this.oPgFrm.Page1.oPag.oTDFLPPRO_1_37.enabled = this.oPgFrm.Page1.oPag.oTDFLPPRO_1_37.mCond()
    this.oPgFrm.Page1.oPag.oTDASPETT_1_43.enabled = this.oPgFrm.Page1.oPag.oTDASPETT_1_43.mCond()
    this.oPgFrm.Page1.oPag.oTFFLRAGG_1_44.enabled = this.oPgFrm.Page1.oPag.oTFFLRAGG_1_44.mCond()
    this.oPgFrm.Page1.oPag.oTDNOPRSC_1_46.enabled = this.oPgFrm.Page1.oPag.oTDNOPRSC_1_46.mCond()
    this.oPgFrm.Page1.oPag.oTDFLDTPR_1_47.enabled = this.oPgFrm.Page1.oPag.oTDFLDTPR_1_47.mCond()
    this.oPgFrm.Page1.oPag.oTDFLACCO_1_48.enabled = this.oPgFrm.Page1.oPag.oTDFLACCO_1_48.mCond()
    this.oPgFrm.Page1.oPag.oTDFLPACK_1_49.enabled = this.oPgFrm.Page1.oPag.oTDFLPACK_1_49.mCond()
    this.oPgFrm.Page1.oPag.oTDFLCCAU_1_51.enabled = this.oPgFrm.Page1.oPag.oTDFLCCAU_1_51.mCond()
    this.oPgFrm.Page1.oPag.oTDFLRIFI_1_52.enabled = this.oPgFrm.Page1.oPag.oTDFLRIFI_1_52.mCond()
    this.oPgFrm.Page1.oPag.oTDFLRIAT_1_53.enabled = this.oPgFrm.Page1.oPag.oTDFLRIAT_1_53.mCond()
    this.oPgFrm.Page1.oPag.oTDSEQMA2_1_59.enabled = this.oPgFrm.Page1.oPag.oTDSEQMA2_1_59.mCond()
    this.oPgFrm.Page5.oPag.oTDFLNSTA_5_4.enabled = this.oPgFrm.Page5.oPag.oTDFLNSTA_5_4.mCond()
    this.oPgFrm.Page4.oPag.oTDTPNDOC_4_9.enabled = this.oPgFrm.Page4.oPag.oTDTPNDOC_4_9.mCond()
    this.oPgFrm.Page4.oPag.oTDTPVDOC_4_11.enabled = this.oPgFrm.Page4.oPag.oTDTPVDOC_4_11.mCond()
    this.oPgFrm.Page4.oPag.oTDTPRDES_4_13.enabled = this.oPgFrm.Page4.oPag.oTDTPRDES_4_13.mCond()
    this.oPgFrm.Page4.oPag.oTDFLIMPA_4_37.enabled = this.oPgFrm.Page4.oPag.oTDFLIMPA_4_37.mCond()
    this.oPgFrm.Page4.oPag.oTDFLIMAC_4_38.enabled = this.oPgFrm.Page4.oPag.oTDFLIMAC_4_38.mCond()
    this.oPgFrm.Page3.oPag.oTDFLANAL_3_1.enabled = this.oPgFrm.Page3.oPag.oTDFLANAL_3_1.mCond()
    this.oPgFrm.Page3.oPag.oTDFLELAN_3_2.enabled = this.oPgFrm.Page3.oPag.oTDFLELAN_3_2.mCond()
    this.oPgFrm.Page3.oPag.oTDVOCECR_3_3.enabled = this.oPgFrm.Page3.oPag.oTDVOCECR_3_3.mCond()
    this.oPgFrm.Page3.oPag.oTD_SEGNO_3_4.enabled = this.oPgFrm.Page3.oPag.oTD_SEGNO_3_4.mCond()
    this.oPgFrm.Page3.oPag.oTDFLPRAT_3_5.enabled = this.oPgFrm.Page3.oPag.oTDFLPRAT_3_5.mCond()
    this.oPgFrm.Page3.oPag.oTDCAUCES_3_7.enabled = this.oPgFrm.Page3.oPag.oTDCAUCES_3_7.mCond()
    this.oPgFrm.Page3.oPag.oTDFLARCO_3_11.enabled = this.oPgFrm.Page3.oPag.oTDFLARCO_3_11.mCond()
    this.oPgFrm.Page3.oPag.oTDLOTDIF_3_12.enabled = this.oPgFrm.Page3.oPag.oTDLOTDIF_3_12.mCond()
    this.oPgFrm.Page3.oPag.oTDTIPIMB_3_13.enabled = this.oPgFrm.Page3.oPag.oTDTIPIMB_3_13.mCond()
    this.oPgFrm.Page3.oPag.oTDCAUPFI_3_14.enabled = this.oPgFrm.Page3.oPag.oTDCAUPFI_3_14.mCond()
    this.oPgFrm.Page3.oPag.oTDCAUCOD_3_15.enabled = this.oPgFrm.Page3.oPag.oTDCAUCOD_3_15.mCond()
    this.oPgFrm.Page3.oPag.oTDFLEXPL_3_16.enabled = this.oPgFrm.Page3.oPag.oTDFLEXPL_3_16.mCond()
    this.oPgFrm.Page3.oPag.oTDVALCOM_3_17.enabled = this.oPgFrm.Page3.oPag.oTDVALCOM_3_17.mCond()
    this.oPgFrm.Page3.oPag.oTDEXPAUT_3_19.enabled = this.oPgFrm.Page3.oPag.oTDEXPAUT_3_19.mCond()
    this.oPgFrm.Page3.oPag.oTDMAXLEV_3_20.enabled = this.oPgFrm.Page3.oPag.oTDMAXLEV_3_20.mCond()
    this.oPgFrm.Page2.oPag.oTDFLPROV_2_4.enabled = this.oPgFrm.Page2.oPag.oTDFLPROV_2_4.mCond()
    this.oPgFrm.Page2.oPag.oTDFLSILI_2_5.enabled = this.oPgFrm.Page2.oPag.oTDFLSILI_2_5.mCond()
    this.oPgFrm.Page2.oPag.oTDFLBLEV_2_10.enabled = this.oPgFrm.Page2.oPag.oTDFLBLEV_2_10.mCond()
    this.oPgFrm.Page2.oPag.oTDRIPCON_2_15.enabled = this.oPgFrm.Page2.oPag.oTDRIPCON_2_15.mCond()
    this.oPgFrm.Page2.oPag.oTDFLSPIM_2_19.enabled = this.oPgFrm.Page2.oPag.oTDFLSPIM_2_19.mCond()
    this.oPgFrm.Page2.oPag.oTDFLSPIM_2_20.enabled = this.oPgFrm.Page2.oPag.oTDFLSPIM_2_20.mCond()
    this.oPgFrm.Page2.oPag.oTDFLSPTR_2_23.enabled = this.oPgFrm.Page2.oPag.oTDFLSPTR_2_23.mCond()
    this.oPgFrm.Page2.oPag.oTDFLSPTR_2_24.enabled = this.oPgFrm.Page2.oPag.oTDFLSPTR_2_24.mCond()
    this.oPgFrm.Page2.oPag.oTDMCALSI_2_30.enabled = this.oPgFrm.Page2.oPag.oTDMCALSI_2_30.mCond()
    this.oPgFrm.Page2.oPag.oTDMCALST_2_31.enabled = this.oPgFrm.Page2.oPag.oTDMCALST_2_31.mCond()
    this.oPgFrm.Page1.oPag.oTDMINVAL_1_105.enabled = this.oPgFrm.Page1.oPag.oTDMINVAL_1_105.mCond()
    this.oPgFrm.Page1.oPag.oTDIMPMIN_1_107.enabled = this.oPgFrm.Page1.oPag.oTDIMPMIN_1_107.mCond()
    this.oPgFrm.Page1.oPag.oTDIVAMIN_1_108.enabled = this.oPgFrm.Page1.oPag.oTDIVAMIN_1_108.mCond()
    this.oPgFrm.Page1.oPag.oTDRIOTOT_1_110.enabled = this.oPgFrm.Page1.oPag.oTDRIOTOT_1_110.mCond()
    this.oPgFrm.Page2.oPag.oTDMCALSI_2_42.enabled = this.oPgFrm.Page2.oPag.oTDMCALSI_2_42.mCond()
    this.oPgFrm.Page2.oPag.oTDMCALST_2_43.enabled = this.oPgFrm.Page2.oPag.oTDMCALST_2_43.mCond()
    this.oPgFrm.Page2.oPag.oTDCHKUCA_2_44.enabled = this.oPgFrm.Page2.oPag.oTDCHKUCA_2_44.mCond()
    this.oPgFrm.Page4.oPag.oTDFLRA01_4_56.enabled = this.oPgFrm.Page4.oPag.oTDFLRA01_4_56.mCond()
    this.oPgFrm.Page4.oPag.oTDFLRA02_4_67.enabled = this.oPgFrm.Page4.oPag.oTDFLRA02_4_67.mCond()
    this.oPgFrm.Page4.oPag.oTDFLRA03_4_68.enabled = this.oPgFrm.Page4.oPag.oTDFLRA03_4_68.mCond()
    this.oPgFrm.Page4.oPag.oTDFLRA04_4_69.enabled = this.oPgFrm.Page4.oPag.oTDFLRA04_4_69.mCond()
    this.oPgFrm.Page4.oPag.oTDFLRA05_4_70.enabled = this.oPgFrm.Page4.oPag.oTDFLRA05_4_70.mCond()
    this.oPgFrm.Page4.oPag.oTDFLRA06_4_71.enabled = this.oPgFrm.Page4.oPag.oTDFLRA06_4_71.mCond()
    this.oPgFrm.Page3.oPag.oTDFLCASH_3_58.enabled = this.oPgFrm.Page3.oPag.oTDFLCASH_3_58.mCond()
    this.oPgFrm.Page3.oPag.oTDFLCOMM_3_60.enabled = this.oPgFrm.Page3.oPag.oTDFLCOMM_3_60.mCond()
    this.oPgFrm.Page3.oPag.oTDTIPDFE_3_64.enabled = this.oPgFrm.Page3.oPag.oTDTIPDFE_3_64.mCond()
    this.oPgFrm.Page3.oPag.oTDDESEST_3_65.enabled = this.oPgFrm.Page3.oPag.oTDDESEST_3_65.mCond()
    this.oPgFrm.Page3.oPag.oTDCODCLA_3_66.enabled = this.oPgFrm.Page3.oPag.oTDCODCLA_3_66.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_46.enabled = this.oPgFrm.Page2.oPag.oBtn_2_46.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTDCATDOC_1_3.visible=!this.oPgFrm.Page1.oPag.oTDCATDOC_1_3.mHide()
    this.oPgFrm.Page1.oPag.oTDCATDOC_1_4.visible=!this.oPgFrm.Page1.oPag.oTDCATDOC_1_4.mHide()
    this.oPgFrm.Page1.oPag.oTDCATDOC_1_5.visible=!this.oPgFrm.Page1.oPag.oTDCATDOC_1_5.mHide()
    this.oPgFrm.Page1.oPag.oTFFLGEFA_1_6.visible=!this.oPgFrm.Page1.oPag.oTFFLGEFA_1_6.mHide()
    this.oPgFrm.Page1.oPag.oTFFLGEFA_1_7.visible=!this.oPgFrm.Page1.oPag.oTFFLGEFA_1_7.mHide()
    this.oPgFrm.Page1.oPag.oTDFLRICE_1_8.visible=!this.oPgFrm.Page1.oPag.oTDFLRICE_1_8.mHide()
    this.oPgFrm.Page1.oPag.oTDFLINTE_1_9.visible=!this.oPgFrm.Page1.oPag.oTDFLINTE_1_9.mHide()
    this.oPgFrm.Page1.oPag.oTDFLINTE_1_10.visible=!this.oPgFrm.Page1.oPag.oTDFLINTE_1_10.mHide()
    this.oPgFrm.Page1.oPag.oTDEMERIC_1_12.visible=!this.oPgFrm.Page1.oPag.oTDEMERIC_1_12.mHide()
    this.oPgFrm.Page1.oPag.oTDRICNOM_1_13.visible=!this.oPgFrm.Page1.oPag.oTDRICNOM_1_13.mHide()
    this.oPgFrm.Page1.oPag.oTDFLATIP_1_14.visible=!this.oPgFrm.Page1.oPag.oTDFLATIP_1_14.mHide()
    this.oPgFrm.Page1.oPag.oTDFLGCHI_1_15.visible=!this.oPgFrm.Page1.oPag.oTDFLGCHI_1_15.mHide()
    this.oPgFrm.Page1.oPag.oTDCAUMAG_1_17.visible=!this.oPgFrm.Page1.oPag.oTDCAUMAG_1_17.mHide()
    this.oPgFrm.Page1.oPag.oDESMAG_1_18.visible=!this.oPgFrm.Page1.oPag.oDESMAG_1_18.mHide()
    this.oPgFrm.Page1.oPag.oTDCODMAG_1_22.visible=!this.oPgFrm.Page1.oPag.oTDCODMAG_1_22.mHide()
    this.oPgFrm.Page1.oPag.oTDFLMGPR_1_23.visible=!this.oPgFrm.Page1.oPag.oTDFLMGPR_1_23.mHide()
    this.oPgFrm.Page1.oPag.oTDCODMAT_1_25.visible=!this.oPgFrm.Page1.oPag.oTDCODMAT_1_25.mHide()
    this.oPgFrm.Page1.oPag.oTDFLMTPR_1_26.visible=!this.oPgFrm.Page1.oPag.oTDFLMTPR_1_26.mHide()
    this.oPgFrm.Page1.oPag.oTDCAUCON_1_27.visible=!this.oPgFrm.Page1.oPag.oTDCAUCON_1_27.mHide()
    this.oPgFrm.Page1.oPag.oDESCAU_1_28.visible=!this.oPgFrm.Page1.oPag.oDESCAU_1_28.mHide()
    this.oPgFrm.Page1.oPag.oTDFLPDOC_1_33.visible=!this.oPgFrm.Page1.oPag.oTDFLPDOC_1_33.mHide()
    this.oPgFrm.Page1.oPag.oTDFLPDOC_1_34.visible=!this.oPgFrm.Page1.oPag.oTDFLPDOC_1_34.mHide()
    this.oPgFrm.Page1.oPag.oTDNUMSCO_1_35.visible=!this.oPgFrm.Page1.oPag.oTDNUMSCO_1_35.mHide()
    this.oPgFrm.Page1.oPag.oTDSERPRO_1_36.visible=!this.oPgFrm.Page1.oPag.oTDSERPRO_1_36.mHide()
    this.oPgFrm.Page1.oPag.oTDFLPPRO_1_37.visible=!this.oPgFrm.Page1.oPag.oTDFLPPRO_1_37.mHide()
    this.oPgFrm.Page1.oPag.oTDQTADEF_1_39.visible=!this.oPgFrm.Page1.oPag.oTDQTADEF_1_39.mHide()
    this.oPgFrm.Page1.oPag.oTDASPETT_1_43.visible=!this.oPgFrm.Page1.oPag.oTDASPETT_1_43.mHide()
    this.oPgFrm.Page1.oPag.oTFFLRAGG_1_44.visible=!this.oPgFrm.Page1.oPag.oTFFLRAGG_1_44.mHide()
    this.oPgFrm.Page1.oPag.oTDFLPREF_1_45.visible=!this.oPgFrm.Page1.oPag.oTDFLPREF_1_45.mHide()
    this.oPgFrm.Page1.oPag.oTDNOPRSC_1_46.visible=!this.oPgFrm.Page1.oPag.oTDNOPRSC_1_46.mHide()
    this.oPgFrm.Page1.oPag.oTDFLDTPR_1_47.visible=!this.oPgFrm.Page1.oPag.oTDFLDTPR_1_47.mHide()
    this.oPgFrm.Page1.oPag.oTDFLACCO_1_48.visible=!this.oPgFrm.Page1.oPag.oTDFLACCO_1_48.mHide()
    this.oPgFrm.Page1.oPag.oTDFLPACK_1_49.visible=!this.oPgFrm.Page1.oPag.oTDFLPACK_1_49.mHide()
    this.oPgFrm.Page1.oPag.oTDBOLDOG_1_50.visible=!this.oPgFrm.Page1.oPag.oTDBOLDOG_1_50.mHide()
    this.oPgFrm.Page1.oPag.oTDFLCCAU_1_51.visible=!this.oPgFrm.Page1.oPag.oTDFLCCAU_1_51.mHide()
    this.oPgFrm.Page1.oPag.oTDFLRIFI_1_52.visible=!this.oPgFrm.Page1.oPag.oTDFLRIFI_1_52.mHide()
    this.oPgFrm.Page1.oPag.oTDFLRIAT_1_53.visible=!this.oPgFrm.Page1.oPag.oTDFLRIAT_1_53.mHide()
    this.oPgFrm.Page1.oPag.oTDFLBACA_1_54.visible=!this.oPgFrm.Page1.oPag.oTDFLBACA_1_54.mHide()
    this.oPgFrm.Page1.oPag.oTDSEQSCO_1_56.visible=!this.oPgFrm.Page1.oPag.oTDSEQSCO_1_56.mHide()
    this.oPgFrm.Page1.oPag.oTDSEQMA1_1_58.visible=!this.oPgFrm.Page1.oPag.oTDSEQMA1_1_58.mHide()
    this.oPgFrm.Page1.oPag.oTDSEQMA2_1_59.visible=!this.oPgFrm.Page1.oPag.oTDSEQMA2_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_66.visible=!this.oPgFrm.Page1.oPag.oStr_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_76.visible=!this.oPgFrm.Page1.oPag.oStr_1_76.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_89.visible=!this.oPgFrm.Page1.oPag.oStr_1_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_90.visible=!this.oPgFrm.Page1.oPag.oStr_1_90.mHide()
    this.oPgFrm.Page5.oPag.oTDFLNSTA_5_4.visible=!this.oPgFrm.Page5.oPag.oTDFLNSTA_5_4.mHide()
    this.oPgFrm.Page4.oPag.oTDTPNDOC_4_9.visible=!this.oPgFrm.Page4.oPag.oTDTPNDOC_4_9.mHide()
    this.oPgFrm.Page4.oPag.oTDTPVDOC_4_11.visible=!this.oPgFrm.Page4.oPag.oTDTPVDOC_4_11.mHide()
    this.oPgFrm.Page4.oPag.oTDTPRDES_4_13.visible=!this.oPgFrm.Page4.oPag.oTDTPRDES_4_13.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_27.visible=!this.oPgFrm.Page4.oPag.oStr_4_27.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_28.visible=!this.oPgFrm.Page4.oPag.oStr_4_28.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_32.visible=!this.oPgFrm.Page4.oPag.oStr_4_32.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_33.visible=!this.oPgFrm.Page4.oPag.oStr_4_33.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_34.visible=!this.oPgFrm.Page4.oPag.oStr_4_34.mHide()
    this.oPgFrm.Page4.oPag.oTDFLIMPA_4_37.visible=!this.oPgFrm.Page4.oPag.oTDFLIMPA_4_37.mHide()
    this.oPgFrm.Page4.oPag.oTDFLIMAC_4_38.visible=!this.oPgFrm.Page4.oPag.oTDFLIMAC_4_38.mHide()
    this.oPgFrm.Page3.oPag.oTDFLANAL_3_1.visible=!this.oPgFrm.Page3.oPag.oTDFLANAL_3_1.mHide()
    this.oPgFrm.Page3.oPag.oTDFLELAN_3_2.visible=!this.oPgFrm.Page3.oPag.oTDFLELAN_3_2.mHide()
    this.oPgFrm.Page3.oPag.oTDVOCECR_3_3.visible=!this.oPgFrm.Page3.oPag.oTDVOCECR_3_3.mHide()
    this.oPgFrm.Page3.oPag.oTD_SEGNO_3_4.visible=!this.oPgFrm.Page3.oPag.oTD_SEGNO_3_4.mHide()
    this.oPgFrm.Page3.oPag.oTDFLPRAT_3_5.visible=!this.oPgFrm.Page3.oPag.oTDFLPRAT_3_5.mHide()
    this.oPgFrm.Page3.oPag.oTDASSCES_3_6.visible=!this.oPgFrm.Page3.oPag.oTDASSCES_3_6.mHide()
    this.oPgFrm.Page3.oPag.oTDCAUCES_3_7.visible=!this.oPgFrm.Page3.oPag.oTDCAUCES_3_7.mHide()
    this.oPgFrm.Page3.oPag.oTDCONASS_3_8.visible=!this.oPgFrm.Page3.oPag.oTDCONASS_3_8.mHide()
    this.oPgFrm.Page3.oPag.oTDCONASS_3_9.visible=!this.oPgFrm.Page3.oPag.oTDCONASS_3_9.mHide()
    this.oPgFrm.Page3.oPag.oTDCODSTR_3_10.visible=!this.oPgFrm.Page3.oPag.oTDCODSTR_3_10.mHide()
    this.oPgFrm.Page3.oPag.oTDFLARCO_3_11.visible=!this.oPgFrm.Page3.oPag.oTDFLARCO_3_11.mHide()
    this.oPgFrm.Page3.oPag.oTDLOTDIF_3_12.visible=!this.oPgFrm.Page3.oPag.oTDLOTDIF_3_12.mHide()
    this.oPgFrm.Page3.oPag.oTDTIPIMB_3_13.visible=!this.oPgFrm.Page3.oPag.oTDTIPIMB_3_13.mHide()
    this.oPgFrm.Page3.oPag.oTDCAUPFI_3_14.visible=!this.oPgFrm.Page3.oPag.oTDCAUPFI_3_14.mHide()
    this.oPgFrm.Page3.oPag.oTDCAUCOD_3_15.visible=!this.oPgFrm.Page3.oPag.oTDCAUCOD_3_15.mHide()
    this.oPgFrm.Page3.oPag.oTDFLEXPL_3_16.visible=!this.oPgFrm.Page3.oPag.oTDFLEXPL_3_16.mHide()
    this.oPgFrm.Page3.oPag.oTDVALCOM_3_17.visible=!this.oPgFrm.Page3.oPag.oTDVALCOM_3_17.mHide()
    this.oPgFrm.Page3.oPag.oTDCOSEPL_3_18.visible=!this.oPgFrm.Page3.oPag.oTDCOSEPL_3_18.mHide()
    this.oPgFrm.Page3.oPag.oTDEXPAUT_3_19.visible=!this.oPgFrm.Page3.oPag.oTDEXPAUT_3_19.mHide()
    this.oPgFrm.Page3.oPag.oTDMAXLEV_3_20.visible=!this.oPgFrm.Page3.oPag.oTDMAXLEV_3_20.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_21.visible=!this.oPgFrm.Page3.oPag.oStr_3_21.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_22.visible=!this.oPgFrm.Page3.oPag.oStr_3_22.mHide()
    this.oPgFrm.Page3.oPag.oDESCAUCE_3_23.visible=!this.oPgFrm.Page3.oPag.oDESCAUCE_3_23.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_25.visible=!this.oPgFrm.Page3.oPag.oStr_3_25.mHide()
    this.oPgFrm.Page3.oPag.oDESPFI_3_26.visible=!this.oPgFrm.Page3.oPag.oDESPFI_3_26.mHide()
    this.oPgFrm.Page3.oPag.oDESCOD_3_27.visible=!this.oPgFrm.Page3.oPag.oDESCOD_3_27.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_28.visible=!this.oPgFrm.Page3.oPag.oStr_3_28.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_31.visible=!this.oPgFrm.Page3.oPag.oStr_3_31.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_32.visible=!this.oPgFrm.Page3.oPag.oStr_3_32.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_33.visible=!this.oPgFrm.Page3.oPag.oStr_3_33.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_34.visible=!this.oPgFrm.Page3.oPag.oStr_3_34.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_35.visible=!this.oPgFrm.Page3.oPag.oStr_3_35.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_36.visible=!this.oPgFrm.Page3.oPag.oStr_3_36.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_37.visible=!this.oPgFrm.Page3.oPag.oStr_3_37.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_38.visible=!this.oPgFrm.Page3.oPag.oStr_3_38.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_39.visible=!this.oPgFrm.Page3.oPag.oStr_3_39.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_40.visible=!this.oPgFrm.Page3.oPag.oStr_3_40.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_41.visible=!this.oPgFrm.Page3.oPag.oStr_3_41.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_42.visible=!this.oPgFrm.Page3.oPag.oStr_3_42.mHide()
    this.oPgFrm.Page2.oPag.oTDFLPROV_2_4.visible=!this.oPgFrm.Page2.oPag.oTDFLPROV_2_4.mHide()
    this.oPgFrm.Page2.oPag.oTDFLSILI_2_5.visible=!this.oPgFrm.Page2.oPag.oTDFLSILI_2_5.mHide()
    this.oPgFrm.Page2.oPag.oTDPRZVAC_2_6.visible=!this.oPgFrm.Page2.oPag.oTDPRZVAC_2_6.mHide()
    this.oPgFrm.Page2.oPag.oTDPRZDES_2_7.visible=!this.oPgFrm.Page2.oPag.oTDPRZDES_2_7.mHide()
    this.oPgFrm.Page2.oPag.oTDFLVALO_2_8.visible=!this.oPgFrm.Page2.oPag.oTDFLVALO_2_8.mHide()
    this.oPgFrm.Page2.oPag.oTDFLSPIN_2_11.visible=!this.oPgFrm.Page2.oPag.oTDFLSPIN_2_11.mHide()
    this.oPgFrm.Page2.oPag.oTDFLQRIO_2_12.visible=!this.oPgFrm.Page2.oPag.oTDFLQRIO_2_12.mHide()
    this.oPgFrm.Page2.oPag.oTDFLPREV_2_13.visible=!this.oPgFrm.Page2.oPag.oTDFLPREV_2_13.mHide()
    this.oPgFrm.Page2.oPag.oTDFLAPCA_2_14.visible=!this.oPgFrm.Page2.oPag.oTDFLAPCA_2_14.mHide()
    this.oPgFrm.Page2.oPag.oTDRIPCON_2_15.visible=!this.oPgFrm.Page2.oPag.oTDRIPCON_2_15.mHide()
    this.oPgFrm.Page2.oPag.oTDNOSTCO_2_16.visible=!this.oPgFrm.Page2.oPag.oTDNOSTCO_2_16.mHide()
    this.oPgFrm.Page2.oPag.oTDFLGETR_2_17.visible=!this.oPgFrm.Page2.oPag.oTDFLGETR_2_17.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_18.visible=!this.oPgFrm.Page2.oPag.oStr_2_18.mHide()
    this.oPgFrm.Page2.oPag.oTDFLSPIM_2_19.visible=!this.oPgFrm.Page2.oPag.oTDFLSPIM_2_19.mHide()
    this.oPgFrm.Page2.oPag.oTDFLSPIM_2_20.visible=!this.oPgFrm.Page2.oPag.oTDFLSPIM_2_20.mHide()
    this.oPgFrm.Page2.oPag.oMSDESIMB_2_21.visible=!this.oPgFrm.Page2.oPag.oMSDESIMB_2_21.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_22.visible=!this.oPgFrm.Page2.oPag.oStr_2_22.mHide()
    this.oPgFrm.Page2.oPag.oTDFLSPTR_2_23.visible=!this.oPgFrm.Page2.oPag.oTDFLSPTR_2_23.mHide()
    this.oPgFrm.Page2.oPag.oTDFLSPTR_2_24.visible=!this.oPgFrm.Page2.oPag.oTDFLSPTR_2_24.mHide()
    this.oPgFrm.Page2.oPag.oMSDESTRA_2_25.visible=!this.oPgFrm.Page2.oPag.oMSDESTRA_2_25.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_26.visible=!this.oPgFrm.Page2.oPag.oStr_2_26.mHide()
    this.oPgFrm.Page2.oPag.oTDRIPINC_2_27.visible=!this.oPgFrm.Page2.oPag.oTDRIPINC_2_27.mHide()
    this.oPgFrm.Page2.oPag.oTDRIPIMB_2_28.visible=!this.oPgFrm.Page2.oPag.oTDRIPIMB_2_28.mHide()
    this.oPgFrm.Page2.oPag.oTDRIPTRA_2_29.visible=!this.oPgFrm.Page2.oPag.oTDRIPTRA_2_29.mHide()
    this.oPgFrm.Page2.oPag.oTDMCALSI_2_30.visible=!this.oPgFrm.Page2.oPag.oTDMCALSI_2_30.mHide()
    this.oPgFrm.Page2.oPag.oTDMCALST_2_31.visible=!this.oPgFrm.Page2.oPag.oTDMCALST_2_31.mHide()
    this.oPgFrm.Page2.oPag.oTDFLSCOR_2_33.visible=!this.oPgFrm.Page2.oPag.oTDFLSCOR_2_33.mHide()
    this.oPgFrm.Page2.oPag.oTDFLRISC_2_34.visible=!this.oPgFrm.Page2.oPag.oTDFLRISC_2_34.mHide()
    this.oPgFrm.Page2.oPag.oTDFLCRIS_2_35.visible=!this.oPgFrm.Page2.oPag.oTDFLCRIS_2_35.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_37.visible=!this.oPgFrm.Page2.oPag.oStr_2_37.mHide()
    this.oPgFrm.Page3.oPag.oDESSTRU_3_54.visible=!this.oPgFrm.Page3.oPag.oDESSTRU_3_54.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_55.visible=!this.oPgFrm.Page3.oPag.oStr_3_55.mHide()
    this.oPgFrm.Page1.oPag.oTDMINIMP_1_104.visible=!this.oPgFrm.Page1.oPag.oTDMINIMP_1_104.mHide()
    this.oPgFrm.Page1.oPag.oTDMINVAL_1_105.visible=!this.oPgFrm.Page1.oPag.oTDMINVAL_1_105.mHide()
    this.oPgFrm.Page1.oPag.oTDTOTDOC_1_106.visible=!this.oPgFrm.Page1.oPag.oTDTOTDOC_1_106.mHide()
    this.oPgFrm.Page1.oPag.oTDIMPMIN_1_107.visible=!this.oPgFrm.Page1.oPag.oTDIMPMIN_1_107.mHide()
    this.oPgFrm.Page1.oPag.oTDIVAMIN_1_108.visible=!this.oPgFrm.Page1.oPag.oTDIVAMIN_1_108.mHide()
    this.oPgFrm.Page1.oPag.oTDMINVEN_1_109.visible=!this.oPgFrm.Page1.oPag.oTDMINVEN_1_109.mHide()
    this.oPgFrm.Page1.oPag.oTDRIOTOT_1_110.visible=!this.oPgFrm.Page1.oPag.oTDRIOTOT_1_110.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_111.visible=!this.oPgFrm.Page1.oPag.oStr_1_111.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_112.visible=!this.oPgFrm.Page1.oPag.oStr_1_112.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAL_1_113.visible=!this.oPgFrm.Page1.oPag.oSIMVAL_1_113.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_56.visible=!this.oPgFrm.Page3.oPag.oStr_3_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_114.visible=!this.oPgFrm.Page1.oPag.oStr_1_114.mHide()
    this.oPgFrm.Page2.oPag.oMSDESIMB_2_38.visible=!this.oPgFrm.Page2.oPag.oMSDESIMB_2_38.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_39.visible=!this.oPgFrm.Page2.oPag.oStr_2_39.mHide()
    this.oPgFrm.Page2.oPag.oMSDESTRA_2_40.visible=!this.oPgFrm.Page2.oPag.oMSDESTRA_2_40.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_41.visible=!this.oPgFrm.Page2.oPag.oStr_2_41.mHide()
    this.oPgFrm.Page2.oPag.oTDMCALSI_2_42.visible=!this.oPgFrm.Page2.oPag.oTDMCALSI_2_42.mHide()
    this.oPgFrm.Page2.oPag.oTDMCALST_2_43.visible=!this.oPgFrm.Page2.oPag.oTDMCALST_2_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_115.visible=!this.oPgFrm.Page1.oPag.oStr_1_115.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_117.visible=!this.oPgFrm.Page1.oPag.oStr_1_117.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_118.visible=!this.oPgFrm.Page1.oPag.oStr_1_118.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_120.visible=!this.oPgFrm.Page1.oPag.oStr_1_120.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_121.visible=!this.oPgFrm.Page1.oPag.oStr_1_121.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_57.visible=!this.oPgFrm.Page3.oPag.oStr_3_57.mHide()
    this.oPgFrm.Page3.oPag.oTDFLCASH_3_58.visible=!this.oPgFrm.Page3.oPag.oTDFLCASH_3_58.mHide()
    this.oPgFrm.Page3.oPag.oTDFLCOMM_3_60.visible=!this.oPgFrm.Page3.oPag.oTDFLCOMM_3_60.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_46.visible=!this.oPgFrm.Page2.oPag.oBtn_2_46.mHide()
    this.oPgFrm.Page1.oPag.oTDFLGEIN_1_127.visible=!this.oPgFrm.Page1.oPag.oTDFLGEIN_1_127.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_62.visible=!this.oPgFrm.Page3.oPag.oStr_3_62.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_87.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_88.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_92.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_93.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_98.Event(cEvent)
        if lower(cEvent)==lower("w_TDCATDOC Changed")
          .Calculate_AIYACBJCCS()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.oObj_3_43.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_FQJJFNNWOT()
          bRefresh=.t.
        endif
      .oPgFrm.Page4.oPag.CAMAGG01.Event(cEvent)
      .oPgFrm.Page4.oPag.CAMAGG02.Event(cEvent)
      .oPgFrm.Page4.oPag.CAMAGG03.Event(cEvent)
      .oPgFrm.Page4.oPag.CAMAGG04.Event(cEvent)
      .oPgFrm.Page4.oPag.CAMAGG05.Event(cEvent)
      .oPgFrm.Page4.oPag.CAMAGG06.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TDCAUMAG
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_TDCAUMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCASC,CMFLRISE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_TDCAUMAG))
          select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCASC,CMFLRISE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCAUMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_TDCAUMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCASC,CMFLRISE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_TDCAUMAG)+"%");

            select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCASC,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDCAUMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oTDCAUMAG_1_17'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCASC,CMFLRISE";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCASC,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCASC,CMFLRISE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_TDCAUMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_TDCAUMAG)
            select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCASC,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_DESMAG = NVL(_Link_.CMDESCRI,space(35))
      this.w_CAUCOL = NVL(_Link_.CMCAUCOL,space(5))
      this.w_FLCLFR = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FLAVAL = NVL(_Link_.CMFLAVAL,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
      this.w_FLELGM = NVL(_Link_.CMFLELGM,space(1))
      this.w_FLCASC = NVL(_Link_.CMFLCASC,space(1))
      this.w_FLRISE = NVL(_Link_.CMFLRISE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUMAG = space(5)
      endif
      this.w_DESMAG = space(35)
      this.w_CAUCOL = space(5)
      this.w_FLCLFR = space(1)
      this.w_FLAVAL = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLELGM = space(1)
      this.w_FLCASC = space(1)
      this.w_FLRISE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCAUMA(.w_TDCAUMAG,.w_CAUCOL,.w_TFFLGEFA,.w_FLAVAL,.w_DATOBSO,.w_OBTEST,'V', .w_TDFLVEAC, .w_TDCATDOC)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_TDCAUMAG = space(5)
        this.w_DESMAG = space(35)
        this.w_CAUCOL = space(5)
        this.w_FLCLFR = space(1)
        this.w_FLAVAL = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLELGM = space(1)
        this.w_FLCASC = space(1)
        this.w_FLRISE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.CMCODICE as CMCODICE117"+ ",link_1_17.CMDESCRI as CMDESCRI117"+ ",link_1_17.CMCAUCOL as CMCAUCOL117"+ ",link_1_17.CMFLCLFR as CMFLCLFR117"+ ",link_1_17.CMFLAVAL as CMFLAVAL117"+ ",link_1_17.CMDTOBSO as CMDTOBSO117"+ ",link_1_17.CMFLELGM as CMFLELGM117"+ ",link_1_17.CMFLCASC as CMFLCASC117"+ ",link_1_17.CMFLRISE as CMFLRISE117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on TIP_DOCU.TDCAUMAG=link_1_17.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCAUMAG=link_1_17.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCODMAG
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_TDCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_TDCODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_TDCODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_TDCODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oTDCODMAG_1_22'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_TDCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_TDCODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESAPP = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_TDCODMAG = space(5)
      endif
      this.w_DESAPP = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_TDCODMAG = space(5)
        this.w_DESAPP = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.MGCODMAG as MGCODMAG122"+ ",link_1_22.MGDESMAG as MGDESMAG122"+ ",link_1_22.MGDTOBSO as MGDTOBSO122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on TIP_DOCU.TDCODMAG=link_1_22.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCODMAG=link_1_22.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCODMAT
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCODMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_TDCODMAT)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_TDCODMAT))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCODMAT)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_TDCODMAT)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_TDCODMAT)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDCODMAT) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oTDCODMAT_1_25'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCODMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_TDCODMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_TDCODMAT)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCODMAT = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESAPP = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_TDCODMAT = space(5)
      endif
      this.w_DESAPP = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_TDCODMAT = space(5)
        this.w_DESAPP = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCODMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.MGCODMAG as MGCODMAG125"+ ",link_1_25.MGDESMAG as MGDESMAG125"+ ",link_1_25.MGDTOBSO as MGDTOBSO125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on TIP_DOCU.TDCODMAT=link_1_25.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCODMAT=link_1_25.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCAUCON
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_TDCAUCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPDOC,CCSERDOC,CCSERPRO,CCFLRIFE,CCDTOBSO,CCFLANAL,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_TDCAUCON))
          select CCCODICE,CCDESCRI,CCTIPDOC,CCSERDOC,CCSERPRO,CCFLRIFE,CCDTOBSO,CCFLANAL,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCAUCON)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDCAUCON) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oTDCAUCON_1_27'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAC_ATD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPDOC,CCSERDOC,CCSERPRO,CCFLRIFE,CCDTOBSO,CCFLANAL,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPDOC,CCSERDOC,CCSERPRO,CCFLRIFE,CCDTOBSO,CCFLANAL,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPDOC,CCSERDOC,CCSERPRO,CCFLRIFE,CCDTOBSO,CCFLANAL,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_TDCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_TDCAUCON)
            select CCCODICE,CCDESCRI,CCTIPDOC,CCSERDOC,CCSERPRO,CCFLRIFE,CCDTOBSO,CCFLANAL,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPDOC = NVL(_Link_.CCTIPDOC,space(2))
      this.w_SERDOC = NVL(_Link_.CCSERDOC,space(10))
      this.w_SERPRO = NVL(_Link_.CCSERPRO,space(10))
      this.w_FLRIFE = NVL(_Link_.CCFLRIFE,space(1))
      this.w_CAUOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_FLANAL = NVL(_Link_.CCFLANAL,space(1))
      this.w_CCFLANAL = NVL(_Link_.CCFLANAL,space(1))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUCON = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_TIPDOC = space(2)
      this.w_SERDOC = space(10)
      this.w_SERPRO = space(10)
      this.w_FLRIFE = space(1)
      this.w_CAUOBSO = ctod("  /  /  ")
      this.w_FLANAL = space(1)
      this.w_CCFLANAL = space(1)
      this.w_TIPREG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCAUDO(.w_TDCATDOC, ' ', .w_TDFLVEAC, .w_TIPDOC,.w_FLRIFE,.w_TIPREG) and (.w_CAUOBSO>.w_OBTEST or EMPTY(.w_CAUOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente o incongruente o obsoleta! (Verificare eventuale presenza del check analitica su entrambe le causali)")
        endif
        this.w_TDCAUCON = space(5)
        this.w_DESCAU = space(35)
        this.w_TIPDOC = space(2)
        this.w_SERDOC = space(10)
        this.w_SERPRO = space(10)
        this.w_FLRIFE = space(1)
        this.w_CAUOBSO = ctod("  /  /  ")
        this.w_FLANAL = space(1)
        this.w_CCFLANAL = space(1)
        this.w_TIPREG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_27(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_27.CCCODICE as CCCODICE127"+ ",link_1_27.CCDESCRI as CCDESCRI127"+ ",link_1_27.CCTIPDOC as CCTIPDOC127"+ ",link_1_27.CCSERDOC as CCSERDOC127"+ ",link_1_27.CCSERPRO as CCSERPRO127"+ ",link_1_27.CCFLRIFE as CCFLRIFE127"+ ",link_1_27.CCDTOBSO as CCDTOBSO127"+ ",link_1_27.CCFLANAL as CCFLANAL127"+ ",link_1_27.CCFLANAL as CCFLANAL127"+ ",link_1_27.CCTIPREG as CCTIPREG127"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_27 on TIP_DOCU.TDCAUCON=link_1_27.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_27"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCAUCON=link_1_27.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCODLIS
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_TDCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_TDCODLIS))
          select LSCODLIS,LSDESLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_TDCODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_TDCODLIS)+"%");

            select LSCODLIS,LSDESLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oTDCODLIS_1_38'),i_cWhere,'GSAR_ALI',"Elenco listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_TDCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_TDCODLIS)
            select LSCODLIS,LSDESLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TDCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_IVALIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TDCATDOC<>'RF' OR .w_IVALIS='L' OR EMPTY(.w_TDCODLIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino incongruente con categoria documento! (Netto)")
        endif
        this.w_TDCODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_IVALIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.LSCODLIS as LSCODLIS138"+ ",link_1_38.LSDESLIS as LSDESLIS138"+ ",link_1_38.LSIVALIS as LSIVALIS138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on TIP_DOCU.TDCODLIS=link_1_38.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCODLIS=link_1_38.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCAUPFI
  func Link_1_79(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUPFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUPFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TDCAUPFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TDCAUPFI)
            select TDTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUPFI = NVL(_Link_.TDTIPDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUPFI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUPFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDCAUCOD
  func Link_1_80(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TDCAUCOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TDCAUCOD)
            select TDTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUCOD = NVL(_Link_.TDTIPDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUCOD = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDMODRIF
  func Link_4_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODMRIFE_IDX,3]
    i_lTable = "MODMRIFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2], .t., this.MODMRIFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDMODRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MRR',True,'MODMRIFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_TDMODRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_TDMODRIF))
          select MDCODICE,MDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDMODRIF)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStrODBC(trim(this.w_TDMODRIF)+"%");

            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStr(trim(this.w_TDMODRIF)+"%");

            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDMODRIF) and !this.bDontReportError
            deferred_cp_zoom('MODMRIFE','*','MDCODICE',cp_AbsName(oSource.parent,'oTDMODRIF_4_4'),i_cWhere,'GSAR_MRR',"Modelli riferimenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDMODRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_TDMODRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_TDMODRIF)
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDMODRIF = NVL(_Link_.MDCODICE,space(5))
      this.w_DESMOD = NVL(_Link_.MDDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TDMODRIF = space(5)
      endif
      this.w_DESMOD = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODMRIFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDMODRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODMRIFE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_4.MDCODICE as MDCODICE404"+ ",link_4_4.MDDESCRI as MDDESCRI404"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_4 on TIP_DOCU.TDMODRIF=link_4_4.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_4"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDMODRIF=link_4_4.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDTPNDOC
  func Link_4_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDTPNDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDTPNDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDTPNDOC))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDTPNDOC)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDTPNDOC) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDTPNDOC_4_9'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDTPNDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDTPNDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDTPNDOC)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDTPNDOC = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDTPNDOC = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDTPNDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDTPVDOC
  func Link_4_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDTPVDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDTPVDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDTPVDOC))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDTPVDOC)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDTPVDOC) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDTPVDOC_4_11'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDTPVDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDTPVDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDTPVDOC)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDTPVDOC = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDTPVDOC = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDTPVDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDTPRDES
  func Link_4_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDTPRDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDTPRDES)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDTPRDES))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDTPRDES)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDTPRDES) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDTPRDES_4_13'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDTPRDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDTPRDES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDTPRDES)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDTPRDES = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDTPRDES = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDTPRDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDESCCL1
  func Link_4_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDESCCL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDESCCL1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDESCCL1))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDESCCL1)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDESCCL1) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDESCCL1_4_17'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDESCCL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDESCCL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDESCCL1)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDESCCL1 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDESCCL1 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDESCCL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDESCCL2
  func Link_4_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDESCCL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDESCCL2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDESCCL2))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDESCCL2)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDESCCL2) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDESCCL2_4_18'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDESCCL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDESCCL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDESCCL2)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDESCCL2 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDESCCL2 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDESCCL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDESCCL3
  func Link_4_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDESCCL3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDESCCL3)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDESCCL3))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDESCCL3)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDESCCL3) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDESCCL3_4_19'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDESCCL3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDESCCL3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDESCCL3)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDESCCL3 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDESCCL3 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDESCCL3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDESCCL4
  func Link_4_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDESCCL4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDESCCL4)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDESCCL4))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDESCCL4)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDESCCL4) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDESCCL4_4_20'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDESCCL4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDESCCL4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDESCCL4)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDESCCL4 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDESCCL4 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDESCCL4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDESCCL5
  func Link_4_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDESCCL5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDESCCL5)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDESCCL5))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDESCCL5)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDESCCL5) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDESCCL5_4_21'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDESCCL5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDESCCL5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDESCCL5)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDESCCL5 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDESCCL5 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDESCCL5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDSTACL1
  func Link_4_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDSTACL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDSTACL1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDSTACL1))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDSTACL1)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDSTACL1) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDSTACL1_4_22'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDSTACL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDSTACL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDSTACL1)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDSTACL1 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDSTACL1 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDSTACL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDSTACL2
  func Link_4_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDSTACL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDSTACL2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDSTACL2))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDSTACL2)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDSTACL2) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDSTACL2_4_23'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDSTACL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDSTACL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDSTACL2)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDSTACL2 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDSTACL2 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDSTACL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDSTACL3
  func Link_4_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDSTACL3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDSTACL3)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDSTACL3))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDSTACL3)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDSTACL3) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDSTACL3_4_24'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDSTACL3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDSTACL3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDSTACL3)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDSTACL3 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDSTACL3 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDSTACL3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDSTACL4
  func Link_4_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDSTACL4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDSTACL4)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDSTACL4))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDSTACL4)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDSTACL4) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDSTACL4_4_25'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDSTACL4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDSTACL4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDSTACL4)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDSTACL4 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDSTACL4 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDSTACL4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDSTACL5
  func Link_4_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDSTACL5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDSTACL5)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDSTACL5))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDSTACL5)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDSTACL5) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDSTACL5_4_26'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDSTACL5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDSTACL5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDSTACL5)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDSTACL5 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDSTACL5 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDSTACL5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDCAUCES
  func Link_3_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_lTable = "CAU_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2], .t., this.CAU_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACC',True,'CAU_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_TDCAUCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_TDCAUCES))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCAUCES)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_TDCAUCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_TDCAUCES)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDCAUCES) and !this.bDontReportError
            deferred_cp_zoom('CAU_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oTDCAUCES_3_7'),i_cWhere,'GSCE_ACC',"Causali cespiti",'GSCE_ZCA.CAU_CESP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_TDCAUCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_TDCAUCES)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUCES = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAUCE = NVL(_Link_.CCDESCRI,space(40))
      this.w_DTOBSOCA = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUCES = space(5)
      endif
      this.w_DESCAUCE = space(40)
      this.w_DTOBSOCA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSOCA>.w_OBTEST OR EMPTY(.w_DTOBSOCA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale cespite inesistente o obsoleta")
        endif
        this.w_TDCAUCES = space(5)
        this.w_DESCAUCE = space(40)
        this.w_DTOBSOCA = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CESP_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_7.CCCODICE as CCCODICE307"+ ",link_3_7.CCDESCRI as CCDESCRI307"+ ",link_3_7.CCDTOBSO as CCDTOBSO307"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_7 on TIP_DOCU.TDCAUCES=link_3_7.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_7"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCAUCES=link_3_7.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCODSTR
  func Link_3_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AST',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_TDCODSTR)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_TDCODSTR))
          select STCODICE,STDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCODSTR)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDCODSTR) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oTDCODSTR_3_10'),i_cWhere,'GSVA_AST',"Elenco strutture EDI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_TDCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_TDCODSTR)
            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCODSTR = NVL(_Link_.STCODICE,space(10))
      this.w_DESSTRU = NVL(_Link_.STDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TDCODSTR = space(10)
      endif
      this.w_DESSTRU = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VASTRUTT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_10.STCODICE as STCODICE310"+ ",link_3_10.STDESCRI as STDESCRI310"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_10 on TIP_DOCU.TDCODSTR=link_3_10.STCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_10"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCODSTR=link_3_10.STCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCAUPFI
  func Link_3_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUPFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_BZC',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TDCAUPFI)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TDCAUPFI))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCAUPFI)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDCAUPFI) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTDCAUPFI_3_14'),i_cWhere,'GSVE_BZC',"Causali documenti",'GSVE_KAC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUPFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TDCAUPFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TDCAUPFI)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUPFI = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESPFI = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATPFI = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLIPFI = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLTCOM = NVL(_Link_.TDFLCOMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUPFI = space(5)
      endif
      this.w_DESPFI = space(35)
      this.w_CATPFI = space(2)
      this.w_FLIPFI = space(1)
      this.w_FLTCOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TDCAUPFI) OR (.w_CATPFI='DI' AND (.w_FLIPFI='N' OR .w_TDFLINTE=.w_FLIPFI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente o obsoleta")
        endif
        this.w_TDCAUPFI = space(5)
        this.w_DESPFI = space(35)
        this.w_CATPFI = space(2)
        this.w_FLIPFI = space(1)
        this.w_FLTCOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUPFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_14.TDTIPDOC as TDTIPDOC314"+ ",link_3_14.TDDESDOC as TDDESDOC314"+ ",link_3_14.TDCATDOC as TDCATDOC314"+ ",link_3_14.TDFLINTE as TDFLINTE314"+ ",link_3_14.TDFLCOMM as TDFLCOMM314"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_14 on TIP_DOCU.TDCAUPFI=link_3_14.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_14"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCAUPFI=link_3_14.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCAUCOD
  func Link_3_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_BZC',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TDCAUCOD)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TDCAUCOD))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCAUCOD)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDCAUCOD) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTDCAUCOD_3_15'),i_cWhere,'GSVE_BZC',"Causali documenti",'GSVE_KAC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TDCAUCOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TDCAUCOD)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUCOD = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCOD = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATCOD = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLICOD = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLDCOM = NVL(_Link_.TDFLCOMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUCOD = space(5)
      endif
      this.w_DESCOD = space(35)
      this.w_CATCOD = space(2)
      this.w_FLICOD = space(1)
      this.w_FLDCOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TDCAUCOD) OR (.w_CATCOD='DI' AND (.w_FLICOD='N' OR .w_TDFLINTE=.w_FLICOD))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente o obsoleta")
        endif
        this.w_TDCAUCOD = space(5)
        this.w_DESCOD = space(35)
        this.w_CATCOD = space(2)
        this.w_FLICOD = space(1)
        this.w_FLDCOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_15.TDTIPDOC as TDTIPDOC315"+ ",link_3_15.TDDESDOC as TDDESDOC315"+ ",link_3_15.TDCATDOC as TDCATDOC315"+ ",link_3_15.TDFLINTE as TDFLINTE315"+ ",link_3_15.TDFLCOMM as TDFLCOMM315"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_15 on TIP_DOCU.TDCAUCOD=link_3_15.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_15"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCAUCOD=link_3_15.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDMCALSI
  func Link_2_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDMCALSI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_TDMCALSI)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_TDMCALSI))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDMCALSI)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDMCALSI) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oTDMCALSI_2_30'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDMCALSI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_TDMCALSI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_TDMCALSI)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDMCALSI = NVL(_Link_.MSCODICE,space(5))
      this.w_MSDESIMB = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TDMCALSI = space(5)
      endif
      this.w_MSDESIMB = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDMCALSI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_30.MSCODICE as MSCODICE230"+ ",link_2_30.MSDESCRI as MSDESCRI230"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_30 on TIP_DOCU.TDMCALSI=link_2_30.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_30"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDMCALSI=link_2_30.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDMCALST
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDMCALST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_TDMCALST)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_TDMCALST))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDMCALST)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDMCALST) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oTDMCALST_2_31'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDMCALST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_TDMCALST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_TDMCALST)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDMCALST = NVL(_Link_.MSCODICE,space(5))
      this.w_MSDESTRA = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TDMCALST = space(5)
      endif
      this.w_MSDESTRA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDMCALST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_31.MSCODICE as MSCODICE231"+ ",link_2_31.MSDESCRI as MSDESCRI231"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_31 on TIP_DOCU.TDMCALST=link_2_31.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_31"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDMCALST=link_2_31.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDMINVAL
  func Link_1_105(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDMINVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_TDMINVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_TDMINVAL))
          select VACODVAL,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDMINVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDMINVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oTDMINVAL_1_105'),i_cWhere,'GSAR_AVL',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDMINVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_TDMINVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_TDMINVAL)
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDMINVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TDMINVAL = space(3)
      endif
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDMINVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_105(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_105.VACODVAL as VACODVAL205"+ ",link_1_105.VASIMVAL as VASIMVAL205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_105 on TIP_DOCU.TDMINVAL=link_1_105.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_105"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDMINVAL=link_1_105.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDMCALSI
  func Link_2_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDMCALSI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_TDMCALSI)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_TDMCALSI))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDMCALSI)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDMCALSI) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oTDMCALSI_2_42'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDMCALSI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_TDMCALSI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_TDMCALSI)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDMCALSI = NVL(_Link_.MSCODICE,space(5))
      this.w_MSDESIMB = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TDMCALSI = space(5)
      endif
      this.w_MSDESIMB = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDMCALSI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_42(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_42.MSCODICE as MSCODICE242"+ ",link_2_42.MSDESCRI as MSDESCRI242"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_42 on TIP_DOCU.TDMCALSI=link_2_42.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_42"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDMCALSI=link_2_42.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDMCALST
  func Link_2_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDMCALST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_TDMCALST)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_TDMCALST))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDMCALST)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDMCALST) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oTDMCALST_2_43'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDMCALST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_TDMCALST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_TDMCALST)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDMCALST = NVL(_Link_.MSCODICE,space(5))
      this.w_MSDESTRA = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TDMCALST = space(5)
      endif
      this.w_MSDESTRA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDMCALST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_43(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_43.MSCODICE as MSCODICE243"+ ",link_2_43.MSDESCRI as MSDESCRI243"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_43 on TIP_DOCU.TDMCALST=link_2_43.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_43"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDMCALST=link_2_43.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DASERIAL
  func Link_4_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DATI_AGG_IDX,3]
    i_lTable = "DATI_AGG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2], .t., this.DATI_AGG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DASERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DASERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DASERIAL,DACAM_01,DACAM_02,DACAM_03,DACAM_04,DACAM_05,DACAM_06";
                   +" from "+i_cTable+" "+i_lTable+" where DASERIAL="+cp_ToStrODBC(this.w_DASERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DASERIAL',this.w_DASERIAL)
            select DASERIAL,DACAM_01,DACAM_02,DACAM_03,DACAM_04,DACAM_05,DACAM_06;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DASERIAL = NVL(_Link_.DASERIAL,space(10))
      this.w_DACAM_01 = NVL(_Link_.DACAM_01,space(30))
      this.w_DACAM_02 = NVL(_Link_.DACAM_02,space(30))
      this.w_DACAM_03 = NVL(_Link_.DACAM_03,space(30))
      this.w_DACAM_04 = NVL(_Link_.DACAM_04,space(30))
      this.w_DACAM_05 = NVL(_Link_.DACAM_05,space(30))
      this.w_DACAM_06 = NVL(_Link_.DACAM_06,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_DASERIAL = space(10)
      endif
      this.w_DACAM_01 = space(30)
      this.w_DACAM_02 = space(30)
      this.w_DACAM_03 = space(30)
      this.w_DACAM_04 = space(30)
      this.w_DACAM_05 = space(30)
      this.w_DACAM_06 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])+'\'+cp_ToStr(_Link_.DASERIAL,1)
      cp_ShowWarn(i_cKey,this.DATI_AGG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DASERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTDTIPDOC_1_1.value==this.w_TDTIPDOC)
      this.oPgFrm.Page1.oPag.oTDTIPDOC_1_1.value=this.w_TDTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTDDESDOC_1_2.value==this.w_TDDESDOC)
      this.oPgFrm.Page1.oPag.oTDDESDOC_1_2.value=this.w_TDDESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCATDOC_1_3.RadioValue()==this.w_TDCATDOC)
      this.oPgFrm.Page1.oPag.oTDCATDOC_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCATDOC_1_4.RadioValue()==this.w_TDCATDOC)
      this.oPgFrm.Page1.oPag.oTDCATDOC_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCATDOC_1_5.RadioValue()==this.w_TDCATDOC)
      this.oPgFrm.Page1.oPag.oTDCATDOC_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTFFLGEFA_1_6.RadioValue()==this.w_TFFLGEFA)
      this.oPgFrm.Page1.oPag.oTFFLGEFA_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTFFLGEFA_1_7.RadioValue()==this.w_TFFLGEFA)
      this.oPgFrm.Page1.oPag.oTFFLGEFA_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLRICE_1_8.RadioValue()==this.w_TDFLRICE)
      this.oPgFrm.Page1.oPag.oTDFLRICE_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLINTE_1_9.RadioValue()==this.w_TDFLINTE)
      this.oPgFrm.Page1.oPag.oTDFLINTE_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLINTE_1_10.RadioValue()==this.w_TDFLINTE)
      this.oPgFrm.Page1.oPag.oTDFLINTE_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDEMERIC_1_12.RadioValue()==this.w_TDEMERIC)
      this.oPgFrm.Page1.oPag.oTDEMERIC_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDRICNOM_1_13.RadioValue()==this.w_TDRICNOM)
      this.oPgFrm.Page1.oPag.oTDRICNOM_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLATIP_1_14.RadioValue()==this.w_TDFLATIP)
      this.oPgFrm.Page1.oPag.oTDFLATIP_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLGCHI_1_15.RadioValue()==this.w_TDFLGCHI)
      this.oPgFrm.Page1.oPag.oTDFLGCHI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCAUMAG_1_17.value==this.w_TDCAUMAG)
      this.oPgFrm.Page1.oPag.oTDCAUMAG_1_17.value=this.w_TDCAUMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_18.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_18.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCODMAG_1_22.value==this.w_TDCODMAG)
      this.oPgFrm.Page1.oPag.oTDCODMAG_1_22.value=this.w_TDCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLMGPR_1_23.RadioValue()==this.w_TDFLMGPR)
      this.oPgFrm.Page1.oPag.oTDFLMGPR_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCODMAT_1_25.value==this.w_TDCODMAT)
      this.oPgFrm.Page1.oPag.oTDCODMAT_1_25.value=this.w_TDCODMAT
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLMTPR_1_26.RadioValue()==this.w_TDFLMTPR)
      this.oPgFrm.Page1.oPag.oTDFLMTPR_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCAUCON_1_27.value==this.w_TDCAUCON)
      this.oPgFrm.Page1.oPag.oTDCAUCON_1_27.value=this.w_TDCAUCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_28.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_28.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oTDALFDOC_1_32.value==this.w_TDALFDOC)
      this.oPgFrm.Page1.oPag.oTDALFDOC_1_32.value=this.w_TDALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLPDOC_1_33.RadioValue()==this.w_TDFLPDOC)
      this.oPgFrm.Page1.oPag.oTDFLPDOC_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLPDOC_1_34.RadioValue()==this.w_TDFLPDOC)
      this.oPgFrm.Page1.oPag.oTDFLPDOC_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDNUMSCO_1_35.value==this.w_TDNUMSCO)
      this.oPgFrm.Page1.oPag.oTDNUMSCO_1_35.value=this.w_TDNUMSCO
    endif
    if not(this.oPgFrm.Page1.oPag.oTDSERPRO_1_36.value==this.w_TDSERPRO)
      this.oPgFrm.Page1.oPag.oTDSERPRO_1_36.value=this.w_TDSERPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLPPRO_1_37.RadioValue()==this.w_TDFLPPRO)
      this.oPgFrm.Page1.oPag.oTDFLPPRO_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCODLIS_1_38.value==this.w_TDCODLIS)
      this.oPgFrm.Page1.oPag.oTDCODLIS_1_38.value=this.w_TDCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oTDQTADEF_1_39.value==this.w_TDQTADEF)
      this.oPgFrm.Page1.oPag.oTDQTADEF_1_39.value=this.w_TDQTADEF
    endif
    if not(this.oPgFrm.Page1.oPag.oTDPROVVI_1_40.RadioValue()==this.w_TDPROVVI)
      this.oPgFrm.Page1.oPag.oTDPROVVI_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_41.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_41.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oTDASPETT_1_43.value==this.w_TDASPETT)
      this.oPgFrm.Page1.oPag.oTDASPETT_1_43.value=this.w_TDASPETT
    endif
    if not(this.oPgFrm.Page1.oPag.oTFFLRAGG_1_44.value==this.w_TFFLRAGG)
      this.oPgFrm.Page1.oPag.oTFFLRAGG_1_44.value=this.w_TFFLRAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLPREF_1_45.RadioValue()==this.w_TDFLPREF)
      this.oPgFrm.Page1.oPag.oTDFLPREF_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDNOPRSC_1_46.RadioValue()==this.w_TDNOPRSC)
      this.oPgFrm.Page1.oPag.oTDNOPRSC_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLDTPR_1_47.RadioValue()==this.w_TDFLDTPR)
      this.oPgFrm.Page1.oPag.oTDFLDTPR_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLACCO_1_48.RadioValue()==this.w_TDFLACCO)
      this.oPgFrm.Page1.oPag.oTDFLACCO_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLPACK_1_49.RadioValue()==this.w_TDFLPACK)
      this.oPgFrm.Page1.oPag.oTDFLPACK_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDBOLDOG_1_50.RadioValue()==this.w_TDBOLDOG)
      this.oPgFrm.Page1.oPag.oTDBOLDOG_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLCCAU_1_51.RadioValue()==this.w_TDFLCCAU)
      this.oPgFrm.Page1.oPag.oTDFLCCAU_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLRIFI_1_52.RadioValue()==this.w_TDFLRIFI)
      this.oPgFrm.Page1.oPag.oTDFLRIFI_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLRIAT_1_53.RadioValue()==this.w_TDFLRIAT)
      this.oPgFrm.Page1.oPag.oTDFLRIAT_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLBACA_1_54.RadioValue()==this.w_TDFLBACA)
      this.oPgFrm.Page1.oPag.oTDFLBACA_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDSEQPRE_1_55.RadioValue()==this.w_TDSEQPRE)
      this.oPgFrm.Page1.oPag.oTDSEQPRE_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDSEQSCO_1_56.RadioValue()==this.w_TDSEQSCO)
      this.oPgFrm.Page1.oPag.oTDSEQSCO_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDMODDES_1_57.RadioValue()==this.w_TDMODDES)
      this.oPgFrm.Page1.oPag.oTDMODDES_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDSEQMA1_1_58.RadioValue()==this.w_TDSEQMA1)
      this.oPgFrm.Page1.oPag.oTDSEQMA1_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDSEQMA2_1_59.RadioValue()==this.w_TDSEQMA2)
      this.oPgFrm.Page1.oPag.oTDSEQMA2_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oCODI_5_2.value==this.w_CODI)
      this.oPgFrm.Page5.oPag.oCODI_5_2.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page5.oPag.oDESC_5_3.value==this.w_DESC)
      this.oPgFrm.Page5.oPag.oDESC_5_3.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page5.oPag.oTDFLNSTA_5_4.RadioValue()==this.w_TDFLNSTA)
      this.oPgFrm.Page5.oPag.oTDFLNSTA_5_4.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oTDFLSTLM_5_8.RadioValue()==this.w_TDFLSTLM)
      this.oPgFrm.Page5.oPag.oTDFLSTLM_5_8.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCODI_4_2.value==this.w_CODI)
      this.oPgFrm.Page4.oPag.oCODI_4_2.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page4.oPag.oTDDESRIF_4_3.value==this.w_TDDESRIF)
      this.oPgFrm.Page4.oPag.oTDDESRIF_4_3.value=this.w_TDDESRIF
    endif
    if not(this.oPgFrm.Page4.oPag.oTDMODRIF_4_4.value==this.w_TDMODRIF)
      this.oPgFrm.Page4.oPag.oTDMODRIF_4_4.value=this.w_TDMODRIF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESC_4_5.value==this.w_DESC)
      this.oPgFrm.Page4.oPag.oDESC_4_5.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLNSRI_4_8.RadioValue()==this.w_TDFLNSRI)
      this.oPgFrm.Page4.oPag.oTDFLNSRI_4_8.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDTPNDOC_4_9.value==this.w_TDTPNDOC)
      this.oPgFrm.Page4.oPag.oTDTPNDOC_4_9.value=this.w_TDTPNDOC
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLVSRI_4_10.RadioValue()==this.w_TDFLVSRI)
      this.oPgFrm.Page4.oPag.oTDFLVSRI_4_10.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDTPVDOC_4_11.value==this.w_TDTPVDOC)
      this.oPgFrm.Page4.oPag.oTDTPVDOC_4_11.value=this.w_TDTPVDOC
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRIDE_4_12.RadioValue()==this.w_TDFLRIDE)
      this.oPgFrm.Page4.oPag.oTDFLRIDE_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDTPRDES_4_13.value==this.w_TDTPRDES)
      this.oPgFrm.Page4.oPag.oTDTPRDES_4_13.value=this.w_TDTPRDES
    endif
    if not(this.oPgFrm.Page4.oPag.oTDESCCL1_4_17.value==this.w_TDESCCL1)
      this.oPgFrm.Page4.oPag.oTDESCCL1_4_17.value=this.w_TDESCCL1
    endif
    if not(this.oPgFrm.Page4.oPag.oTDESCCL2_4_18.value==this.w_TDESCCL2)
      this.oPgFrm.Page4.oPag.oTDESCCL2_4_18.value=this.w_TDESCCL2
    endif
    if not(this.oPgFrm.Page4.oPag.oTDESCCL3_4_19.value==this.w_TDESCCL3)
      this.oPgFrm.Page4.oPag.oTDESCCL3_4_19.value=this.w_TDESCCL3
    endif
    if not(this.oPgFrm.Page4.oPag.oTDESCCL4_4_20.value==this.w_TDESCCL4)
      this.oPgFrm.Page4.oPag.oTDESCCL4_4_20.value=this.w_TDESCCL4
    endif
    if not(this.oPgFrm.Page4.oPag.oTDESCCL5_4_21.value==this.w_TDESCCL5)
      this.oPgFrm.Page4.oPag.oTDESCCL5_4_21.value=this.w_TDESCCL5
    endif
    if not(this.oPgFrm.Page4.oPag.oTDSTACL1_4_22.value==this.w_TDSTACL1)
      this.oPgFrm.Page4.oPag.oTDSTACL1_4_22.value=this.w_TDSTACL1
    endif
    if not(this.oPgFrm.Page4.oPag.oTDSTACL2_4_23.value==this.w_TDSTACL2)
      this.oPgFrm.Page4.oPag.oTDSTACL2_4_23.value=this.w_TDSTACL2
    endif
    if not(this.oPgFrm.Page4.oPag.oTDSTACL3_4_24.value==this.w_TDSTACL3)
      this.oPgFrm.Page4.oPag.oTDSTACL3_4_24.value=this.w_TDSTACL3
    endif
    if not(this.oPgFrm.Page4.oPag.oTDSTACL4_4_25.value==this.w_TDSTACL4)
      this.oPgFrm.Page4.oPag.oTDSTACL4_4_25.value=this.w_TDSTACL4
    endif
    if not(this.oPgFrm.Page4.oPag.oTDSTACL5_4_26.value==this.w_TDSTACL5)
      this.oPgFrm.Page4.oPag.oTDSTACL5_4_26.value=this.w_TDSTACL5
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMOD_4_36.value==this.w_DESMOD)
      this.oPgFrm.Page4.oPag.oDESMOD_4_36.value=this.w_DESMOD
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIMPA_4_37.RadioValue()==this.w_TDFLIMPA)
      this.oPgFrm.Page4.oPag.oTDFLIMPA_4_37.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIMAC_4_38.RadioValue()==this.w_TDFLIMAC)
      this.oPgFrm.Page4.oPag.oTDFLIMAC_4_38.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_2.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_2.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESC_2_3.value==this.w_DESC)
      this.oPgFrm.Page2.oPag.oDESC_2_3.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLANAL_3_1.RadioValue()==this.w_TDFLANAL)
      this.oPgFrm.Page3.oPag.oTDFLANAL_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLELAN_3_2.RadioValue()==this.w_TDFLELAN)
      this.oPgFrm.Page3.oPag.oTDFLELAN_3_2.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDVOCECR_3_3.RadioValue()==this.w_TDVOCECR)
      this.oPgFrm.Page3.oPag.oTDVOCECR_3_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTD_SEGNO_3_4.RadioValue()==this.w_TD_SEGNO)
      this.oPgFrm.Page3.oPag.oTD_SEGNO_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLPRAT_3_5.RadioValue()==this.w_TDFLPRAT)
      this.oPgFrm.Page3.oPag.oTDFLPRAT_3_5.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDASSCES_3_6.RadioValue()==this.w_TDASSCES)
      this.oPgFrm.Page3.oPag.oTDASSCES_3_6.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCAUCES_3_7.value==this.w_TDCAUCES)
      this.oPgFrm.Page3.oPag.oTDCAUCES_3_7.value=this.w_TDCAUCES
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCONASS_3_8.RadioValue()==this.w_TDCONASS)
      this.oPgFrm.Page3.oPag.oTDCONASS_3_8.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCONASS_3_9.RadioValue()==this.w_TDCONASS)
      this.oPgFrm.Page3.oPag.oTDCONASS_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCODSTR_3_10.value==this.w_TDCODSTR)
      this.oPgFrm.Page3.oPag.oTDCODSTR_3_10.value=this.w_TDCODSTR
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLARCO_3_11.RadioValue()==this.w_TDFLARCO)
      this.oPgFrm.Page3.oPag.oTDFLARCO_3_11.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDLOTDIF_3_12.RadioValue()==this.w_TDLOTDIF)
      this.oPgFrm.Page3.oPag.oTDLOTDIF_3_12.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDTIPIMB_3_13.RadioValue()==this.w_TDTIPIMB)
      this.oPgFrm.Page3.oPag.oTDTIPIMB_3_13.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCAUPFI_3_14.value==this.w_TDCAUPFI)
      this.oPgFrm.Page3.oPag.oTDCAUPFI_3_14.value=this.w_TDCAUPFI
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCAUCOD_3_15.value==this.w_TDCAUCOD)
      this.oPgFrm.Page3.oPag.oTDCAUCOD_3_15.value=this.w_TDCAUCOD
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLEXPL_3_16.RadioValue()==this.w_TDFLEXPL)
      this.oPgFrm.Page3.oPag.oTDFLEXPL_3_16.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDVALCOM_3_17.RadioValue()==this.w_TDVALCOM)
      this.oPgFrm.Page3.oPag.oTDVALCOM_3_17.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCOSEPL_3_18.RadioValue()==this.w_TDCOSEPL)
      this.oPgFrm.Page3.oPag.oTDCOSEPL_3_18.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDEXPAUT_3_19.RadioValue()==this.w_TDEXPAUT)
      this.oPgFrm.Page3.oPag.oTDEXPAUT_3_19.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDMAXLEV_3_20.value==this.w_TDMAXLEV)
      this.oPgFrm.Page3.oPag.oTDMAXLEV_3_20.value=this.w_TDMAXLEV
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCAUCE_3_23.value==this.w_DESCAUCE)
      this.oPgFrm.Page3.oPag.oDESCAUCE_3_23.value=this.w_DESCAUCE
    endif
    if not(this.oPgFrm.Page3.oPag.oDESPFI_3_26.value==this.w_DESPFI)
      this.oPgFrm.Page3.oPag.oDESPFI_3_26.value=this.w_DESPFI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCOD_3_27.value==this.w_DESCOD)
      this.oPgFrm.Page3.oPag.oDESCOD_3_27.value=this.w_DESCOD
    endif
    if not(this.oPgFrm.Page3.oPag.oCODI_3_52.value==this.w_CODI)
      this.oPgFrm.Page3.oPag.oCODI_3_52.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESC_3_53.value==this.w_DESC)
      this.oPgFrm.Page3.oPag.oDESC_3_53.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLPROV_2_4.RadioValue()==this.w_TDFLPROV)
      this.oPgFrm.Page2.oPag.oTDFLPROV_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLSILI_2_5.RadioValue()==this.w_TDFLSILI)
      this.oPgFrm.Page2.oPag.oTDFLSILI_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDPRZVAC_2_6.RadioValue()==this.w_TDPRZVAC)
      this.oPgFrm.Page2.oPag.oTDPRZVAC_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDPRZDES_2_7.RadioValue()==this.w_TDPRZDES)
      this.oPgFrm.Page2.oPag.oTDPRZDES_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLVALO_2_8.RadioValue()==this.w_TDFLVALO)
      this.oPgFrm.Page2.oPag.oTDFLVALO_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDCHKTOT_2_9.RadioValue()==this.w_TDCHKTOT)
      this.oPgFrm.Page2.oPag.oTDCHKTOT_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLBLEV_2_10.RadioValue()==this.w_TDFLBLEV)
      this.oPgFrm.Page2.oPag.oTDFLBLEV_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLSPIN_2_11.RadioValue()==this.w_TDFLSPIN)
      this.oPgFrm.Page2.oPag.oTDFLSPIN_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLQRIO_2_12.RadioValue()==this.w_TDFLQRIO)
      this.oPgFrm.Page2.oPag.oTDFLQRIO_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLPREV_2_13.RadioValue()==this.w_TDFLPREV)
      this.oPgFrm.Page2.oPag.oTDFLPREV_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLAPCA_2_14.RadioValue()==this.w_TDFLAPCA)
      this.oPgFrm.Page2.oPag.oTDFLAPCA_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDRIPCON_2_15.RadioValue()==this.w_TDRIPCON)
      this.oPgFrm.Page2.oPag.oTDRIPCON_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDNOSTCO_2_16.RadioValue()==this.w_TDNOSTCO)
      this.oPgFrm.Page2.oPag.oTDNOSTCO_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLGETR_2_17.RadioValue()==this.w_TDFLGETR)
      this.oPgFrm.Page2.oPag.oTDFLGETR_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLSPIM_2_19.RadioValue()==this.w_TDFLSPIM)
      this.oPgFrm.Page2.oPag.oTDFLSPIM_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLSPIM_2_20.RadioValue()==this.w_TDFLSPIM)
      this.oPgFrm.Page2.oPag.oTDFLSPIM_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMSDESIMB_2_21.value==this.w_MSDESIMB)
      this.oPgFrm.Page2.oPag.oMSDESIMB_2_21.value=this.w_MSDESIMB
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLSPTR_2_23.RadioValue()==this.w_TDFLSPTR)
      this.oPgFrm.Page2.oPag.oTDFLSPTR_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLSPTR_2_24.RadioValue()==this.w_TDFLSPTR)
      this.oPgFrm.Page2.oPag.oTDFLSPTR_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMSDESTRA_2_25.value==this.w_MSDESTRA)
      this.oPgFrm.Page2.oPag.oMSDESTRA_2_25.value=this.w_MSDESTRA
    endif
    if not(this.oPgFrm.Page2.oPag.oTDRIPINC_2_27.RadioValue()==this.w_TDRIPINC)
      this.oPgFrm.Page2.oPag.oTDRIPINC_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDRIPIMB_2_28.RadioValue()==this.w_TDRIPIMB)
      this.oPgFrm.Page2.oPag.oTDRIPIMB_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDRIPTRA_2_29.RadioValue()==this.w_TDRIPTRA)
      this.oPgFrm.Page2.oPag.oTDRIPTRA_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDMCALSI_2_30.value==this.w_TDMCALSI)
      this.oPgFrm.Page2.oPag.oTDMCALSI_2_30.value=this.w_TDMCALSI
    endif
    if not(this.oPgFrm.Page2.oPag.oTDMCALST_2_31.value==this.w_TDMCALST)
      this.oPgFrm.Page2.oPag.oTDMCALST_2_31.value=this.w_TDMCALST
    endif
    if not(this.oPgFrm.Page2.oPag.oTDSINCFL_2_32.RadioValue()==this.w_TDSINCFL)
      this.oPgFrm.Page2.oPag.oTDSINCFL_2_32.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLSCOR_2_33.RadioValue()==this.w_TDFLSCOR)
      this.oPgFrm.Page2.oPag.oTDFLSCOR_2_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLRISC_2_34.RadioValue()==this.w_TDFLRISC)
      this.oPgFrm.Page2.oPag.oTDFLRISC_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLCRIS_2_35.RadioValue()==this.w_TDFLCRIS)
      this.oPgFrm.Page2.oPag.oTDFLCRIS_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDESSTRU_3_54.value==this.w_DESSTRU)
      this.oPgFrm.Page3.oPag.oDESSTRU_3_54.value=this.w_DESSTRU
    endif
    if not(this.oPgFrm.Page1.oPag.oTDMINIMP_1_104.value==this.w_TDMINIMP)
      this.oPgFrm.Page1.oPag.oTDMINIMP_1_104.value=this.w_TDMINIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oTDMINVAL_1_105.value==this.w_TDMINVAL)
      this.oPgFrm.Page1.oPag.oTDMINVAL_1_105.value=this.w_TDMINVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTDTOTDOC_1_106.RadioValue()==this.w_TDTOTDOC)
      this.oPgFrm.Page1.oPag.oTDTOTDOC_1_106.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDIMPMIN_1_107.value==this.w_TDIMPMIN)
      this.oPgFrm.Page1.oPag.oTDIMPMIN_1_107.value=this.w_TDIMPMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTDIVAMIN_1_108.RadioValue()==this.w_TDIVAMIN)
      this.oPgFrm.Page1.oPag.oTDIVAMIN_1_108.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDMINVEN_1_109.RadioValue()==this.w_TDMINVEN)
      this.oPgFrm.Page1.oPag.oTDMINVEN_1_109.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDRIOTOT_1_110.RadioValue()==this.w_TDRIOTOT)
      this.oPgFrm.Page1.oPag.oTDRIOTOT_1_110.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_113.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_113.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oMSDESIMB_2_38.value==this.w_MSDESIMB)
      this.oPgFrm.Page2.oPag.oMSDESIMB_2_38.value=this.w_MSDESIMB
    endif
    if not(this.oPgFrm.Page2.oPag.oMSDESTRA_2_40.value==this.w_MSDESTRA)
      this.oPgFrm.Page2.oPag.oMSDESTRA_2_40.value=this.w_MSDESTRA
    endif
    if not(this.oPgFrm.Page2.oPag.oTDMCALSI_2_42.value==this.w_TDMCALSI)
      this.oPgFrm.Page2.oPag.oTDMCALSI_2_42.value=this.w_TDMCALSI
    endif
    if not(this.oPgFrm.Page2.oPag.oTDMCALST_2_43.value==this.w_TDMCALST)
      this.oPgFrm.Page2.oPag.oTDMCALST_2_43.value=this.w_TDMCALST
    endif
    if not(this.oPgFrm.Page2.oPag.oTDCHKUCA_2_44.RadioValue()==this.w_TDCHKUCA)
      this.oPgFrm.Page2.oPag.oTDCHKUCA_2_44.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIA01_4_49.RadioValue()==this.w_TDFLIA01)
      this.oPgFrm.Page4.oPag.oTDFLIA01_4_49.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIA02_4_50.RadioValue()==this.w_TDFLIA02)
      this.oPgFrm.Page4.oPag.oTDFLIA02_4_50.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIA03_4_51.RadioValue()==this.w_TDFLIA03)
      this.oPgFrm.Page4.oPag.oTDFLIA03_4_51.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIA04_4_52.RadioValue()==this.w_TDFLIA04)
      this.oPgFrm.Page4.oPag.oTDFLIA04_4_52.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIA05_4_53.RadioValue()==this.w_TDFLIA05)
      this.oPgFrm.Page4.oPag.oTDFLIA05_4_53.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIA06_4_54.RadioValue()==this.w_TDFLIA06)
      this.oPgFrm.Page4.oPag.oTDFLIA06_4_54.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRA01_4_56.RadioValue()==this.w_TDFLRA01)
      this.oPgFrm.Page4.oPag.oTDFLRA01_4_56.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRA02_4_67.RadioValue()==this.w_TDFLRA02)
      this.oPgFrm.Page4.oPag.oTDFLRA02_4_67.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRA03_4_68.RadioValue()==this.w_TDFLRA03)
      this.oPgFrm.Page4.oPag.oTDFLRA03_4_68.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRA04_4_69.RadioValue()==this.w_TDFLRA04)
      this.oPgFrm.Page4.oPag.oTDFLRA04_4_69.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRA05_4_70.RadioValue()==this.w_TDFLRA05)
      this.oPgFrm.Page4.oPag.oTDFLRA05_4_70.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRA06_4_71.RadioValue()==this.w_TDFLRA06)
      this.oPgFrm.Page4.oPag.oTDFLRA06_4_71.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLCASH_3_58.RadioValue()==this.w_TDFLCASH)
      this.oPgFrm.Page3.oPag.oTDFLCASH_3_58.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLNORC_3_59.RadioValue()==this.w_TDFLNORC)
      this.oPgFrm.Page3.oPag.oTDFLNORC_3_59.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLCOMM_3_60.RadioValue()==this.w_TDFLCOMM)
      this.oPgFrm.Page3.oPag.oTDFLCOMM_3_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLGEIN_1_127.RadioValue()==this.w_TDFLGEIN)
      this.oPgFrm.Page1.oPag.oTDFLGEIN_1_127.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDTIPDFE_3_64.value==this.w_TDTIPDFE)
      this.oPgFrm.Page3.oPag.oTDTIPDFE_3_64.value=this.w_TDTIPDFE
    endif
    if not(this.oPgFrm.Page3.oPag.oTDDESEST_3_65.value==this.w_TDDESEST)
      this.oPgFrm.Page3.oPag.oTDDESEST_3_65.value=this.w_TDDESEST
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCODCLA_3_66.value==this.w_TDCODCLA)
      this.oPgFrm.Page3.oPag.oTDCODCLA_3_66.value=this.w_TDCODCLA
    endif
    cp_SetControlsValueExtFlds(this,'TIP_DOCU')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TDTIPDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDTIPDOC_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TDTIPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TDCATDOC))  and not(.w_TDFLINTE='F' OR IsAlt())  and (.w_TDFLINTE<>'F' And Not .w_MOVIM)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCATDOC_1_3.SetFocus()
            i_bnoObbl = !empty(.w_TDCATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TDCATDOC))  and not(.w_TDFLINTE<>'F' OR IsAlt())  and (.w_TDFLINTE='F' And Not .w_MOVIM)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCATDOC_1_4.SetFocus()
            i_bnoObbl = !empty(.w_TDCATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TDCATDOC))  and not(NOT IsAlt())  and (Not .w_MOVIM)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCATDOC_1_5.SetFocus()
            i_bnoObbl = !empty(.w_TDCATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_TDCAUMAG)) or not(CHKCAUMA(.w_TDCAUMAG,.w_CAUCOL,.w_TFFLGEFA,.w_FLAVAL,.w_DATOBSO,.w_OBTEST,'V', .w_TDFLVEAC, .w_TDCATDOC)))  and not(IsAlt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCAUMAG_1_17.SetFocus()
            i_bnoObbl = !empty(.w_TDCAUMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(IsAlt())  and (NOT EMPTY(.w_TDCAUMAG))  and not(empty(.w_TDCODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCODMAG_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(EMPTY(.w_CAUCOL))  and (NOT EMPTY(.w_CAUCOL))  and not(empty(.w_TDCODMAT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCODMAT_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   ((empty(.w_TDCAUCON)) or not(CHKCAUDO(.w_TDCATDOC, ' ', .w_TDFLVEAC, .w_TIPDOC,.w_FLRIFE,.w_TIPREG) and (.w_CAUOBSO>.w_OBTEST or EMPTY(.w_CAUOBSO))))  and not(.w_TDFLVEAC<>'V')  and ((g_COGE='S' OR IsAlt()) AND .w_TDCATDOC $ 'FA-RF-FC-NC'  AND .w_TDFLVEAC='V')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCAUCON_1_27.SetFocus()
            i_bnoObbl = !empty(.w_TDCAUCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente o incongruente o obsoleta! (Verificare eventuale presenza del check analitica su entrambe le causali)")
          case   not(.w_TDNUMSCO<g_NUMSCO+1)  and not(IsAlt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDNUMSCO_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire al massimo il num. sconti utilizzati definito nei dati azienda")
          case   not(.w_TDFLPPRO='P' AND NOT empty(.w_TDCAUCON)  OR .w_TDFLPPRO<>'P' )  and not(isalt())  and (g_COGE<>'S' OR NOT .w_TDCATDOC $ 'FA-NC' OR .w_TDFLPDOC="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDFLPPRO_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo numerazione protocollo incongruente")
          case   not(.w_TDCATDOC<>'RF' OR .w_IVALIS='L' OR EMPTY(.w_TDCODLIS))  and not(empty(.w_TDCODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCODLIS_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino incongruente con categoria documento! (Netto)")
          case   (empty(.w_TDVOCECR))  and not(NOT ((g_PERCCR='S' AND .w_TDFLANAL='S') OR  (g_COMM='S' AND .w_TDFLCOMM $ 'M-S')))  and ((g_PERCCR='S' AND .w_TDFLANAL='S')  OR (g_COMM='S' AND .w_TDFLCOMM $ 'M-S'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTDVOCECR_3_3.SetFocus()
            i_bnoObbl = !empty(.w_TDVOCECR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TD_SEGNO))  and not(g_PERCCR<>'S' OR .w_TDFLANAL<>'S')  and (g_PERCCR='S' AND .w_TDFLANAL='S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTD_SEGNO_3_4.SetFocus()
            i_bnoObbl = !empty(.w_TD_SEGNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_TDCAUCES)) or not(.w_DTOBSOCA>.w_OBTEST OR EMPTY(.w_DTOBSOCA)))  and not(g_CESP<>'S')  and (.w_TDASSCES='M')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTDCAUCES_3_7.SetFocus()
            i_bnoObbl = !empty(.w_TDCAUCES)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale cespite inesistente o obsoleta")
          case   not(EMPTY(.w_TDCAUPFI) OR (.w_CATPFI='DI' AND (.w_FLIPFI='N' OR .w_TDFLINTE=.w_FLIPFI)))  and not(g_EACD<>'S' OR .w_TDFLARCO<>'S')  and (g_EACD='S' AND .w_TDFLARCO='S')  and not(empty(.w_TDCAUPFI))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTDCAUPFI_3_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente o obsoleta")
          case   not(EMPTY(.w_TDCAUCOD) OR (.w_CATCOD='DI' AND (.w_FLICOD='N' OR .w_TDFLINTE=.w_FLICOD)))  and not(g_EACD<>'S' OR .w_TDFLARCO<>'S')  and (g_EACD='S' AND .w_TDFLARCO='S')  and not(empty(.w_TDCAUCOD))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTDCAUCOD_3_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente o obsoleta")
          case   not(.w_TDMAXLEV>0)  and not(g_EACD<>'S' OR .w_TDFLARCO<>'S')  and (NOT EMPTY(.w_TDCAUCOD) AND g_DISB='S' AND .w_TDFLARCO='S' And .w_TDFLEXPL<>'D' And .w_TDTIPIMB='N')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTDMAXLEV_3_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, numero massimo livelli di esplosione non specificato")
          case   not((.w_TDCATDOC $  'FA-NC' And .w_TDFLVALO='S') Or Empty( .w_TDFLVALO ))  and not(IsAlt())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oTDFLVALO_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attivabile solo per fatture o note di credito")
          case   not(.w_TDMINIMP>=0)  and not(.w_TFFLGEFA<>'B')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDMINIMP_1_104.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore positivo")
          case   (empty(.w_TDMINVAL))  and not(.w_TFFLGEFA<>'B')  and (.w_TDMINIMP<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDMINVAL_1_105.SetFocus()
            i_bnoObbl = !empty(.w_TDMINVAL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSVE_MTD.CheckForm()
      if i_bres
        i_bres=  .GSVE_MTD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=5
        endif
      endif
      *i_bRes = i_bRes .and. .GSVE_MDC.CheckForm()
      if i_bres
        i_bres=  .GSVE_MDC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsve_atd
      * --- Controlli
      if i_bRes
         .w_RESCHK=0
           .NotifyEvent('CheckFinali')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      endif
      * --- Controllo che le causali per imballi a perdere e rendere gestiscano l'intestatario
      if i_bRes And .w_TDFLARCO='S' And .w_TDTIPIMB<>'N' And (.w_FLIPFI='N' Or .w_FLICOD='N' Or .w_FLTCOM='S' Or .w_FLDCOM='S')
         i_bRes = .f.
         i_bnoChk = .f.
      	 i_cErrorMsg = Ah_MsgFormat("Gestione imballi attiva: le causali per imballi a rendere e a perdere devono gestire l'intestatario e non avere attiva la gestione progetti")
      Endif
      * --- Controllo non bloccante sull'abnalitica della causale contabile
      if i_bRes and !(this.w_CCFLANAL<>'S' OR g_PERCCR<>'S' OR (this.w_TDFLELAN<>'S' AND this.w_TDFLANAL<>'S'))
         i_bRes = AH_YESNO("L'analitica � gestita sia sulla causale documento che sulla causale contabile.%0Si desidera confermare?")
      Endif
      
      * - Controllo importo minimo fatturabile
      if i_bRes And .w_TDMINIMP<>0 And Empty(.w_TDMINVAL)
         i_bRes=.f.
         i_bnoChk = .f.
      	 i_cErrorMsg =Ah_MsgFormat( "Indicare valuta per importo minimo fatturabile")
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TDTIPDOC = this.w_TDTIPDOC
    this.o_TDCATDOC = this.w_TDCATDOC
    this.o_TFFLGEFA = this.w_TFFLGEFA
    this.o_TDFLINTE = this.w_TDFLINTE
    this.o_TDFLVEAC = this.w_TDFLVEAC
    this.o_TDEMERIC = this.w_TDEMERIC
    this.o_TDCAUMAG = this.w_TDCAUMAG
    this.o_TDCAUCON = this.w_TDCAUCON
    this.o_TDFLACCO = this.w_TDFLACCO
    this.o_TDFLEXPL = this.w_TDFLEXPL
    this.o_TDFLANAL = this.w_TDFLANAL
    this.o_TDASSCES = this.w_TDASSCES
    this.o_TDFLARCO = this.w_TDFLARCO
    this.o_TDTIPIMB = this.w_TDTIPIMB
    this.o_TDFLSILI = this.w_TDFLSILI
    this.o_TDFLRISC = this.w_TDFLRISC
    this.o_TDMINIMP = this.w_TDMINIMP
    this.o_TDTOTDOC = this.w_TDTOTDOC
    this.o_TDIMPMIN = this.w_TDIMPMIN
    this.o_TDMINVEN = this.w_TDMINVEN
    this.o_TDFLIA01 = this.w_TDFLIA01
    this.o_TDFLIA02 = this.w_TDFLIA02
    this.o_TDFLIA03 = this.w_TDFLIA03
    this.o_TDFLIA04 = this.w_TDFLIA04
    this.o_TDFLIA05 = this.w_TDFLIA05
    this.o_TDFLIA06 = this.w_TDFLIA06
    this.o_TDFLCOMM = this.w_TDFLCOMM
    * --- GSVE_MTD : Depends On
    this.GSVE_MTD.SaveDependsOn()
    * --- GSVE_MDC : Depends On
    this.GSVE_MDC.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsve_atdPag1 as StdContainer
  Width  = 721
  height = 555
  stdWidth  = 721
  stdheight = 555
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTDTIPDOC_1_1 as StdField with uid="MUWPDNBNFZ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TDTIPDOC", cQueryName = "TDTIPDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo di documento",;
    HelpContextID = 205236359,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=142, Top=20, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5)

  add object oTDDESDOC_1_2 as StdField with uid="JZUJKSOEBJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TDDESDOC", cQueryName = "TDDESDOC",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 202418311,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=206, Top=20, InputMask=replicate('X',35)


  add object oTDCATDOC_1_3 as StdCombo with uid="NJVBJQVNIH",rtseq=3,rtrep=.f.,left=142,top=51,width=134,height=21;
    , ToolTipText = "Categoria di appartenenza del documento";
    , HelpContextID = 201635975;
    , cFormVar="w_TDCATDOC",RowSource=""+"Documento interno,"+"Doc. di trasporto,"+"Fattura,"+"Nota di credito,"+"Corrispettivi", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oTDCATDOC_1_3.RadioValue()
    return(iif(this.value =1,'DI',;
    iif(this.value =2,'DT',;
    iif(this.value =3,'FA',;
    iif(this.value =4,'NC',;
    iif(this.value =5,'RF',;
    space(2)))))))
  endfunc
  func oTDCATDOC_1_3.GetRadio()
    this.Parent.oContained.w_TDCATDOC = this.RadioValue()
    return .t.
  endfunc

  func oTDCATDOC_1_3.SetRadio()
    this.Parent.oContained.w_TDCATDOC=trim(this.Parent.oContained.w_TDCATDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TDCATDOC=='DI',1,;
      iif(this.Parent.oContained.w_TDCATDOC=='DT',2,;
      iif(this.Parent.oContained.w_TDCATDOC=='FA',3,;
      iif(this.Parent.oContained.w_TDCATDOC=='NC',4,;
      iif(this.Parent.oContained.w_TDCATDOC=='RF',5,;
      0)))))
  endfunc

  func oTDCATDOC_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLINTE<>'F' And Not .w_MOVIM)
    endwith
   endif
  endfunc

  func oTDCATDOC_1_3.mHide()
    with this.Parent.oContained
      return (.w_TDFLINTE='F' OR IsAlt())
    endwith
  endfunc


  add object oTDCATDOC_1_4 as StdCombo with uid="NTURNBTDGB",rtseq=4,rtrep=.f.,left=142,top=51,width=177,height=21;
    , ToolTipText = "Categoria di appartenenza del documento";
    , HelpContextID = 201635975;
    , cFormVar="w_TDCATDOC",RowSource=""+"Carico da fornitore,"+"Doc.di trasporto a fornitore", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oTDCATDOC_1_4.RadioValue()
    return(iif(this.value =1,'DI',;
    iif(this.value =2,'DT',;
    space(2))))
  endfunc
  func oTDCATDOC_1_4.GetRadio()
    this.Parent.oContained.w_TDCATDOC = this.RadioValue()
    return .t.
  endfunc

  func oTDCATDOC_1_4.SetRadio()
    this.Parent.oContained.w_TDCATDOC=trim(this.Parent.oContained.w_TDCATDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TDCATDOC=='DI',1,;
      iif(this.Parent.oContained.w_TDCATDOC=='DT',2,;
      0))
  endfunc

  func oTDCATDOC_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLINTE='F' And Not .w_MOVIM)
    endwith
   endif
  endfunc

  func oTDCATDOC_1_4.mHide()
    with this.Parent.oContained
      return (.w_TDFLINTE<>'F' OR IsAlt())
    endwith
  endfunc


  add object oTDCATDOC_1_5 as StdCombo with uid="YMQYBPKMSB",rtseq=5,rtrep=.f.,left=142,top=51,width=149,height=21;
    , ToolTipText = "Categoria di appartenenza del documento";
    , HelpContextID = 201635975;
    , cFormVar="w_TDCATDOC",RowSource=""+"Altro,"+"Fattura,"+"Nota di credito", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oTDCATDOC_1_5.RadioValue()
    return(iif(this.value =1,'DI',;
    iif(this.value =2,'FA',;
    iif(this.value =3,'NC',;
    space(2)))))
  endfunc
  func oTDCATDOC_1_5.GetRadio()
    this.Parent.oContained.w_TDCATDOC = this.RadioValue()
    return .t.
  endfunc

  func oTDCATDOC_1_5.SetRadio()
    this.Parent.oContained.w_TDCATDOC=trim(this.Parent.oContained.w_TDCATDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TDCATDOC=='DI',1,;
      iif(this.Parent.oContained.w_TDCATDOC=='FA',2,;
      iif(this.Parent.oContained.w_TDCATDOC=='NC',3,;
      0)))
  endfunc

  func oTDCATDOC_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not .w_MOVIM)
    endwith
   endif
  endfunc

  func oTDCATDOC_1_5.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oTFFLGEFA_1_6 as StdCheck with uid="TZSKYWNONP",rtseq=6,rtrep=.f.,left=334, top=51, caption="Genera Fattura differita",;
    ToolTipText = "Se attivo: il documento genera fatture differite",;
    HelpContextID = 70678903,;
    cFormVar="w_TFFLGEFA", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Test fattura differita incongruente con categoria documento";
   , bGlobalFont=.t.


  func oTFFLGEFA_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'A'))
  endfunc
  func oTFFLGEFA_1_6.GetRadio()
    this.Parent.oContained.w_TFFLGEFA = this.RadioValue()
    return .t.
  endfunc

  func oTFFLGEFA_1_6.SetRadio()
    this.Parent.oContained.w_TFFLGEFA=trim(this.Parent.oContained.w_TFFLGEFA)
    this.value = ;
      iif(this.Parent.oContained.w_TFFLGEFA=='S',1,;
      0)
  endfunc

  func oTFFLGEFA_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_TDCATDOC$'FA-NC-RF') AND .w_TDFLVEAC='V' AND NOT IsAlt())
    endwith
   endif
  endfunc

  func oTFFLGEFA_1_6.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC$'FA-NC-RF' OR .w_TDFLVEAC<>'V' OR IsAlt())
    endwith
  endfunc

  add object oTFFLGEFA_1_7 as StdCheck with uid="JIHUBCKKJH",rtseq=7,rtrep=.f.,left=334, top=51, caption="Fattura differita",;
    ToolTipText = "Se attivo: il documento � una fattura differita",;
    HelpContextID = 70678903,;
    cFormVar="w_TFFLGEFA", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Test fattura differita incongruente con categoria documento";
   , bGlobalFont=.t.


  func oTFFLGEFA_1_7.RadioValue()
    return(iif(this.value =1,'B',;
    'A'))
  endfunc
  func oTFFLGEFA_1_7.GetRadio()
    this.Parent.oContained.w_TFFLGEFA = this.RadioValue()
    return .t.
  endfunc

  func oTFFLGEFA_1_7.SetRadio()
    this.Parent.oContained.w_TFFLGEFA=trim(this.Parent.oContained.w_TFFLGEFA)
    this.value = ;
      iif(this.Parent.oContained.w_TFFLGEFA=='B',1,;
      0)
  endfunc

  func oTFFLGEFA_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC='FA' AND .w_TDFLVEAC='V' AND NOT IsAlt())
    endwith
   endif
  endfunc

  func oTFFLGEFA_1_7.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC<>'FA' OR .w_TDFLVEAC<>'V' OR IsAlt())
    endwith
  endfunc

  add object oTDFLRICE_1_8 as StdCheck with uid="UCFQNTDEZY",rtseq=8,rtrep=.f.,left=334, top=70, caption="C/corrispettivi",;
    ToolTipText = "Se atttivo: fattura compresa nei corrispettivi",;
    HelpContextID = 149321595,;
    cFormVar="w_TDFLRICE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLRICE_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLRICE_1_8.GetRadio()
    this.Parent.oContained.w_TDFLRICE = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRICE_1_8.SetRadio()
    this.Parent.oContained.w_TDFLRICE=trim(this.Parent.oContained.w_TDFLRICE)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRICE=='S',1,;
      0)
  endfunc

  func oTDFLRICE_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE<>'S' AND .w_TDCATDOC='FA' AND .w_TDFLVEAC='V')
    endwith
   endif
  endfunc

  func oTDFLRICE_1_8.mHide()
    with this.Parent.oContained
      return (g_COGE='S' OR .w_TDCATDOC<>'FA' OR .w_TDFLVEAC<>'V' or IsAlt())
    endwith
  endfunc

  add object oTDFLINTE_1_9 as StdCheck with uid="KCSRSKQEYO",rtseq=9,rtrep=.f.,left=568, top=20, caption="Cliente",;
    ToolTipText = "Se attivo: documento intestato a cliente, altrimenti no intestazione",;
    HelpContextID = 223770491,;
    cFormVar="w_TDFLINTE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLINTE_1_9.RadioValue()
    return(iif(this.value =1,'C',;
    'N'))
  endfunc
  func oTDFLINTE_1_9.GetRadio()
    this.Parent.oContained.w_TDFLINTE = this.RadioValue()
    return .t.
  endfunc

  func oTDFLINTE_1_9.SetRadio()
    this.Parent.oContained.w_TDFLINTE=trim(this.Parent.oContained.w_TDFLINTE)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLINTE=='C',1,;
      0)
  endfunc

  func oTDFLINTE_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC='DI' AND g_ACQU='S')
    endwith
   endif
  endfunc

  func oTDFLINTE_1_9.mHide()
    with this.Parent.oContained
      return (g_ACQU<>'S')
    endwith
  endfunc


  add object oTDFLINTE_1_10 as StdCombo with uid="LONDVBFKSD",rtseq=10,rtrep=.f.,left=568,top=20,width=149,height=21;
    , ToolTipText = "Tipo intestatario; cliente/fornitore/nessuno";
    , HelpContextID = 223770491;
    , cFormVar="w_TDFLINTE",RowSource=""+"Cliente,"+"Fornitore,"+"Nessuno", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Tipo intestatario incongruente";
  , bGlobalFont=.t.


  func oTDFLINTE_1_10.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oTDFLINTE_1_10.GetRadio()
    this.Parent.oContained.w_TDFLINTE = this.RadioValue()
    return .t.
  endfunc

  func oTDFLINTE_1_10.SetRadio()
    this.Parent.oContained.w_TDFLINTE=trim(this.Parent.oContained.w_TDFLINTE)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLINTE=='C',1,;
      iif(this.Parent.oContained.w_TDFLINTE=='F',2,;
      iif(this.Parent.oContained.w_TDFLINTE=='N',3,;
      0)))
  endfunc

  func oTDFLINTE_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ACQU<>'S' and not .w_MOVIM)
    endwith
   endif
  endfunc

  func oTDFLINTE_1_10.mHide()
    with this.Parent.oContained
      return (g_ACQU='S')
    endwith
  endfunc


  add object oTDEMERIC_1_12 as StdCombo with uid="HCCAONOGSA",rtseq=12,rtrep=.f.,left=142,top=75,width=149,height=21;
    , ToolTipText = "Documento emesso/ricevuto: utilizzato per effettuare il controllo sulla numerazione del documento";
    , HelpContextID = 250124423;
    , cFormVar="w_TDEMERIC",RowSource=""+"Documento emesso,"+"Documento ricevuto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDEMERIC_1_12.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oTDEMERIC_1_12.GetRadio()
    this.Parent.oContained.w_TDEMERIC = this.RadioValue()
    return .t.
  endfunc

  func oTDEMERIC_1_12.SetRadio()
    this.Parent.oContained.w_TDEMERIC=trim(this.Parent.oContained.w_TDEMERIC)
    this.value = ;
      iif(this.Parent.oContained.w_TDEMERIC=='V',1,;
      iif(this.Parent.oContained.w_TDEMERIC=='A',2,;
      0))
  endfunc

  func oTDEMERIC_1_12.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC $ 'FA-NC-OR-RF' or isalt())
    endwith
  endfunc

  add object oTDRICNOM_1_13 as StdCheck with uid="MNPCPRBBHV",rtseq=13,rtrep=.f.,left=538, top=51, caption="Utilizza nominativo",;
    ToolTipText = "Utilizza nominativo al posto del cliente",;
    HelpContextID = 51103869,;
    cFormVar="w_TDRICNOM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDRICNOM_1_13.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'A',;
    'N')))
  endfunc
  func oTDRICNOM_1_13.GetRadio()
    this.Parent.oContained.w_TDRICNOM = this.RadioValue()
    return .t.
  endfunc

  func oTDRICNOM_1_13.SetRadio()
    this.Parent.oContained.w_TDRICNOM=trim(this.Parent.oContained.w_TDRICNOM)
    this.value = ;
      iif(this.Parent.oContained.w_TDRICNOM=='A',1,;
      iif(this.Parent.oContained.w_TDRICNOM=='A',2,;
      0))
  endfunc

  func oTDRICNOM_1_13.mHide()
    with this.Parent.oContained
      return (Not ( .w_TDFLINTE='C' And (g_OFFE='S' Or g_AGEN='S')) OR .w_TDCATDOC='RF')
    endwith
  endfunc

  add object oTDFLATIP_1_14 as StdCheck with uid="LZDMIBMBGW",rtseq=14,rtrep=.f.,left=538, top=72, caption="Attivit� non professionale",;
    ToolTipText = "Se attivo, il documento sar� escluso da elaborazioni statistiche",;
    HelpContextID = 220825722,;
    cFormVar="w_TDFLATIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLATIP_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDFLATIP_1_14.GetRadio()
    this.Parent.oContained.w_TDFLATIP = this.RadioValue()
    return .t.
  endfunc

  func oTDFLATIP_1_14.SetRadio()
    this.Parent.oContained.w_TDFLATIP=trim(this.Parent.oContained.w_TDFLATIP)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLATIP=='S',1,;
      0)
  endfunc

  func oTDFLATIP_1_14.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC <> 'FA' OR NOT ISALT())
    endwith
  endfunc

  add object oTDFLGCHI_1_15 as StdCheck with uid="JKVXNTTWFO",rtseq=15,rtrep=.f.,left=538, top=93, caption="Chiusura pratiche",;
    ToolTipText = "Se attivo verr� richiesta la chiusura delle pratiche al salvataggio",;
    HelpContextID = 231311489,;
    cFormVar="w_TDFLGCHI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLGCHI_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDFLGCHI_1_15.GetRadio()
    this.Parent.oContained.w_TDFLGCHI = this.RadioValue()
    return .t.
  endfunc

  func oTDFLGCHI_1_15.SetRadio()
    this.Parent.oContained.w_TDFLGCHI=trim(this.Parent.oContained.w_TDFLGCHI)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLGCHI=='S',1,;
      0)
  endfunc

  func oTDFLGCHI_1_15.mHide()
    with this.Parent.oContained
      return (!Isalt() or .w_TDCATDOC<>'FA')
    endwith
  endfunc

  add object oTDCAUMAG_1_17 as StdField with uid="WYGMEZMCKN",rtseq=17,rtrep=.f.,;
    cFormVar = "w_TDCAUMAG", cQueryName = "TDCAUMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di movimentazione magazzino associata al documento",;
    HelpContextID = 218843005,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=142, Top=104, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_TDCAUMAG"

  func oTDCAUMAG_1_17.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oTDCAUMAG_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCAUMAG_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCAUMAG_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oTDCAUMAG_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oTDCAUMAG_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_TDCAUMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_18 as StdField with uid="WTSJHCLHFP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 98060086,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=206, Top=104, InputMask=replicate('X',35)

  func oDESMAG_1_18.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDCODMAG_1_22 as StdField with uid="UJBNFGSKOS",rtseq=22,rtrep=.f.,;
    cFormVar = "w_TDCODMAG", cQueryName = "TDCODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 201934717,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=142, Top=132, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_TDCODMAG"

  func oTDCODMAG_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_TDCAUMAG))
    endwith
   endif
  endfunc

  func oTDCODMAG_1_22.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oTDCODMAG_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCODMAG_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCODMAG_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oTDCODMAG_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oTDCODMAG_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_TDCODMAG
     i_obj.ecpSave()
  endproc


  add object oTDFLMGPR_1_23 as StdCombo with uid="VSDSTPXZHX",rtseq=23,rtrep=.f.,left=370,top=133,width=125,height=21;
    , ToolTipText = "Metodo di attribuzione del magazzino sui documenti sia caricati manualmente che generati o importati o di esplosione";
    , HelpContextID = 110524296;
    , cFormVar="w_TDFLMGPR",RowSource=""+"Default,"+"Origine,"+"Forzato,"+"Intestatario,"+"Preferenziale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDFLMGPR_1_23.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'O',;
    iif(this.value =3,'F',;
    iif(this.value =4,'I',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oTDFLMGPR_1_23.GetRadio()
    this.Parent.oContained.w_TDFLMGPR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLMGPR_1_23.SetRadio()
    this.Parent.oContained.w_TDFLMGPR=trim(this.Parent.oContained.w_TDFLMGPR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLMGPR=='D',1,;
      iif(this.Parent.oContained.w_TDFLMGPR=='O',2,;
      iif(this.Parent.oContained.w_TDFLMGPR=='F',3,;
      iif(this.Parent.oContained.w_TDFLMGPR=='I',4,;
      iif(this.Parent.oContained.w_TDFLMGPR=='P',5,;
      0)))))
  endfunc

  func oTDFLMGPR_1_23.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDCODMAT_1_25 as StdField with uid="NCYXYQMRBM",rtseq=25,rtrep=.f.,;
    cFormVar = "w_TDCODMAT", cQueryName = "TDCODMAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice del magazzino collegato",;
    HelpContextID = 201934730,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=142, Top=160, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_TDCODMAT"

  func oTDCODMAT_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CAUCOL))
    endwith
   endif
  endfunc

  func oTDCODMAT_1_25.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL))
    endwith
  endfunc

  func oTDCODMAT_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCODMAT_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCODMAT_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oTDCODMAT_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oTDCODMAT_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_TDCODMAT
     i_obj.ecpSave()
  endproc


  add object oTDFLMTPR_1_26 as StdCombo with uid="RYBIACZKTN",rtseq=26,rtrep=.f.,left=370,top=162,width=125,height=21;
    , ToolTipText = "Metodo di attribuzione del magazzino sui documenti sia caricati manualmente che generati o importati o di esplosione";
    , HelpContextID = 60192648;
    , cFormVar="w_TDFLMTPR",RowSource=""+"Default,"+"Origine,"+"Forzato,"+"Intestatario,"+"Preferenziale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDFLMTPR_1_26.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'O',;
    iif(this.value =3,'F',;
    iif(this.value =4,'I',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oTDFLMTPR_1_26.GetRadio()
    this.Parent.oContained.w_TDFLMTPR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLMTPR_1_26.SetRadio()
    this.Parent.oContained.w_TDFLMTPR=trim(this.Parent.oContained.w_TDFLMTPR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLMTPR=='D',1,;
      iif(this.Parent.oContained.w_TDFLMTPR=='O',2,;
      iif(this.Parent.oContained.w_TDFLMTPR=='F',3,;
      iif(this.Parent.oContained.w_TDFLMTPR=='I',4,;
      iif(this.Parent.oContained.w_TDFLMTPR=='P',5,;
      0)))))
  endfunc

  func oTDFLMTPR_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CAUCOL))
    endwith
   endif
  endfunc

  func oTDFLMTPR_1_26.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL))
    endwith
  endfunc

  add object oTDCAUCON_1_27 as StdField with uid="PRJGJIHRCU",rtseq=27,rtrep=.f.,;
    cFormVar = "w_TDCAUCON", cQueryName = "TDCAUCON",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente o incongruente o obsoleta! (Verificare eventuale presenza del check analitica su entrambe le causali)",;
    ToolTipText = "Causale contabile associata al documento",;
    HelpContextID = 217364604,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=142, Top=188, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_TDCAUCON"

  func oTDCAUCON_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_COGE='S' OR IsAlt()) AND .w_TDCATDOC $ 'FA-RF-FC-NC'  AND .w_TDFLVEAC='V')
    endwith
   endif
  endfunc

  func oTDCAUCON_1_27.mHide()
    with this.Parent.oContained
      return (.w_TDFLVEAC<>'V')
    endwith
  endfunc

  func oTDCAUCON_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCAUCON_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCAUCON_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oTDCAUCON_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAC_ATD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oTDCAUCON_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_TDCAUCON
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_28 as StdField with uid="WXWZJWXWUK",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 63850294,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=206, Top=188, InputMask=replicate('X',35)

  func oDESCAU_1_28.mHide()
    with this.Parent.oContained
      return (.w_TDFLVEAC<>'V')
    endwith
  endfunc

  add object oTDALFDOC_1_32 as StdField with uid="HWIWWJSNZF",rtseq=32,rtrep=.f.,;
    cFormVar = "w_TDALFDOC", cQueryName = "TDALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale parte alfanumerica del documento proposta di default",;
    HelpContextID = 215603335,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=142, Top=216, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oTDFLPDOC_1_33 as StdCheck with uid="KVFYIOCHUA",rtseq=33,rtrep=.f.,left=231, top=216, caption="Numeraz. vendite",;
    ToolTipText = "Numerazione documento allineata a quelli di vendita",;
    HelpContextID = 205097095,;
    cFormVar="w_TDFLPDOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLPDOC_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLPDOC_1_33.GetRadio()
    this.Parent.oContained.w_TDFLPDOC = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPDOC_1_33.SetRadio()
    this.Parent.oContained.w_TDFLPDOC=trim(this.Parent.oContained.w_TDFLPDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPDOC=='S',1,;
      0)
  endfunc

  func oTDFLPDOC_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLVEAC='A' AND g_ACQU<>'S')
    endwith
   endif
  endfunc

  func oTDFLPDOC_1_33.mHide()
    with this.Parent.oContained
      return (NOT (.w_TDFLVEAC='A' AND g_ACQU<>'S'))
    endwith
  endfunc

  add object oTDFLPDOC_1_34 as StdCheck with uid="QRJUNITAWG",rtseq=34,rtrep=.f.,left=231, top=216, caption="Numeraz. libera",;
    ToolTipText = "Se attivo: la numerazione documento non sar� associata ad alcun progressivo",;
    HelpContextID = 205097095,;
    cFormVar="w_TDFLPDOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLPDOC_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLPDOC_1_34.GetRadio()
    this.Parent.oContained.w_TDFLPDOC = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPDOC_1_34.SetRadio()
    this.Parent.oContained.w_TDFLPDOC=trim(this.Parent.oContained.w_TDFLPDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPDOC=='S',1,;
      0)
  endfunc

  func oTDFLPDOC_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLVEAC='V' AND .w_TDCATDOC $ 'DT-DI')
    endwith
   endif
  endfunc

  func oTDFLPDOC_1_34.mHide()
    with this.Parent.oContained
      return (NOT (.w_TDFLVEAC='V' AND .w_TDCATDOC $ 'DT-DI'))
    endwith
  endfunc

  add object oTDNUMSCO_1_35 as StdField with uid="KBHLQLNZQN",rtseq=35,rtrep=.f.,;
    cFormVar = "w_TDNUMSCO", cQueryName = "TDNUMSCO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire al massimo il num. sconti utilizzati definito nei dati azienda",;
    ToolTipText = "Numero massimo di sconti/maggiorazioni utilizzate (max. 4)",;
    HelpContextID = 44038021,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=472, Top=216, cSayPict='"9"', cGetPict='"9"'

  func oTDNUMSCO_1_35.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oTDNUMSCO_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TDNUMSCO<g_NUMSCO+1)
    endwith
    return bRes
  endfunc

  add object oTDSERPRO_1_36 as StdField with uid="PAEGHASSJL",rtseq=36,rtrep=.f.,;
    cFormVar = "w_TDSERPRO", cQueryName = "TDSERPRO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale serie del protocollo proposta in automatico",;
    HelpContextID = 266356613,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=142, Top=241, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oTDSERPRO_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLPPRO<>"N")
    endwith
   endif
  endfunc

  func oTDSERPRO_1_36.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc


  add object oTDFLPPRO_1_37 as StdCombo with uid="WLBMCHTTXM",rtseq=37,rtrep=.f.,left=232,top=241,width=112,height=21;
    , ToolTipText = "Tipo di numerazione protocollo";
    , HelpContextID = 264664965;
    , cFormVar="w_TDFLPPRO",RowSource=""+"Per anno,"+"Per esercizio,"+"Libera,"+"Non gestita,"+"Da cau. contabile", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Tipo numerazione protocollo incongruente";
  , bGlobalFont=.t.


  func oTDFLPPRO_1_37.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'E',;
    iif(this.value =3,'L',;
    iif(this.value =4,'N',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oTDFLPPRO_1_37.GetRadio()
    this.Parent.oContained.w_TDFLPPRO = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPPRO_1_37.SetRadio()
    this.Parent.oContained.w_TDFLPPRO=trim(this.Parent.oContained.w_TDFLPPRO)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPPRO=='D',1,;
      iif(this.Parent.oContained.w_TDFLPPRO=='E',2,;
      iif(this.Parent.oContained.w_TDFLPPRO=='L',3,;
      iif(this.Parent.oContained.w_TDFLPPRO=='N',4,;
      iif(this.Parent.oContained.w_TDFLPPRO=='P',5,;
      0)))))
  endfunc

  func oTDFLPPRO_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE<>'S' OR NOT .w_TDCATDOC $ 'FA-NC' OR .w_TDFLPDOC="S")
    endwith
   endif
  endfunc

  func oTDFLPPRO_1_37.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  func oTDFLPPRO_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TDFLPPRO='P' AND NOT empty(.w_TDCAUCON)  OR .w_TDFLPPRO<>'P' )
    endwith
    return bRes
  endfunc

  add object oTDCODLIS_1_38 as StdField with uid="EUEHPSDHWW",rtseq=38,rtrep=.f.,;
    cFormVar = "w_TDCODLIS", cQueryName = "TDCODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino incongruente con categoria documento! (Netto)",;
    ToolTipText = "Listino proposto da utilizzare in mancanza del listino associato all'intestatario",;
    HelpContextID = 83277943,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=142, Top=270, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_TDCODLIS"

  func oTDCODLIS_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCODLIS_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCODLIS_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oTDCODLIS_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Elenco listini",'',this.parent.oContained
  endproc
  proc oTDCODLIS_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_TDCODLIS
     i_obj.ecpSave()
  endproc

  add object oTDQTADEF_1_39 as StdField with uid="PXWTHWGZOP",rtseq=39,rtrep=.f.,;
    cFormVar = "w_TDQTADEF", cQueryName = "TDQTADEF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Eventuale quantit� di riga proposta di default durante l'inserimento delle righe",;
    HelpContextID = 48179068,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=142, Top=298, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  func oTDQTADEF_1_39.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oTDPROVVI_1_40 as StdCombo with uid="RGNOPVHCJN",rtseq=40,rtrep=.f.,left=357,top=298,width=138,height=21;
    , ToolTipText = "Default stato documenti ";
    , HelpContextID = 96278399;
    , cFormVar="w_TDPROVVI",RowSource=""+"Confermato,"+"Provvisorio", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDPROVVI_1_40.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTDPROVVI_1_40.GetRadio()
    this.Parent.oContained.w_TDPROVVI = this.RadioValue()
    return .t.
  endfunc

  func oTDPROVVI_1_40.SetRadio()
    this.Parent.oContained.w_TDPROVVI=trim(this.Parent.oContained.w_TDPROVVI)
    this.value = ;
      iif(this.Parent.oContained.w_TDPROVVI=='N',1,;
      iif(this.Parent.oContained.w_TDPROVVI=='S',2,;
      0))
  endfunc

  add object oDESLIS_1_41 as StdField with uid="YMEJVXRTQD",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 39274294,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=206, Top=270, InputMask=replicate('X',40)

  add object oTDASPETT_1_43 as StdField with uid="LPMHXBIFNQ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_TDASPETT", cQueryName = "TDASPETT",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione aspetto esteriore beni di default",;
    HelpContextID = 80553866,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=142, Top=326, InputMask=replicate('X',30)

  func oTDASPETT_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLACCO='S' AND .w_TDCATDOC<>'RF')
    endwith
   endif
  endfunc

  func oTDASPETT_1_43.mHide()
    with this.Parent.oContained
      return (.w_TDFLACCO<>'S' OR .w_TDCATDOC='RF')
    endwith
  endfunc

  add object oTFFLRAGG_1_44 as StdField with uid="GWQIYOTSHR",rtseq=44,rtrep=.f.,;
    cFormVar = "w_TFFLRAGG", cQueryName = "TFFLRAGG",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "1..9 =Codice di raggruppamento in fattura",;
    HelpContextID = 15104381,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=472, Top=326, cSayPict="'9'", cGetPict="'9'", InputMask=replicate('X',1)

  func oTFFLRAGG_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TFFLGEFA='S' AND .w_TDCATDOC<>'RF')
    endwith
   endif
  endfunc

  func oTFFLRAGG_1_44.mHide()
    with this.Parent.oContained
      return (NOT (.w_TFFLGEFA='S' AND .w_TDCATDOC<>'RF'))
    endwith
  endfunc

  add object oTDFLPREF_1_45 as StdCheck with uid="VQVYBSLCUI",rtseq=45,rtrep=.f.,left=538, top=96, caption="Preferenziale",;
    ToolTipText = "Se attivo: codice documento preferenziale della categoria di appartenenza",;
    HelpContextID = 29783932,;
    cFormVar="w_TDFLPREF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLPREF_1_45.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLPREF_1_45.GetRadio()
    this.Parent.oContained.w_TDFLPREF = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPREF_1_45.SetRadio()
    this.Parent.oContained.w_TDFLPREF=trim(this.Parent.oContained.w_TDFLPREF)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPREF=='S',1,;
      0)
  endfunc

  func oTDFLPREF_1_45.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDNOPRSC_1_46 as StdCheck with uid="CNVJXVQNOG",rtseq=46,rtrep=.f.,left=538, top=115, caption="No prezzo/sconto",;
    ToolTipText = "Se attivo: il documento non prevede l'inserimento dei prezzi e sconti",;
    HelpContextID = 30013305,;
    cFormVar="w_TDNOPRSC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDNOPRSC_1_46.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDNOPRSC_1_46.GetRadio()
    this.Parent.oContained.w_TDNOPRSC = this.RadioValue()
    return .t.
  endfunc

  func oTDNOPRSC_1_46.SetRadio()
    this.Parent.oContained.w_TDNOPRSC=trim(this.Parent.oContained.w_TDNOPRSC)
    this.value = ;
      iif(this.Parent.oContained.w_TDNOPRSC=='S',1,;
      0)
  endfunc

  func oTDNOPRSC_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC<>'RF')
    endwith
   endif
  endfunc

  func oTDNOPRSC_1_46.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF' OR IsAlt())
    endwith
  endfunc

  add object oTDFLDTPR_1_47 as StdCheck with uid="RSKHMFITHZ",rtseq=47,rtrep=.f.,left=538, top=134, caption="Dati consegna",;
    ToolTipText = "Se attivo: gestisce i dati di consegna sui documenti",;
    HelpContextID = 50755464,;
    cFormVar="w_TDFLDTPR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLDTPR_1_47.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLDTPR_1_47.GetRadio()
    this.Parent.oContained.w_TDFLDTPR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLDTPR_1_47.SetRadio()
    this.Parent.oContained.w_TDFLDTPR=trim(this.Parent.oContained.w_TDFLDTPR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLDTPR=='S',1,;
      0)
  endfunc

  func oTDFLDTPR_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC<>'RF')
    endwith
   endif
  endfunc

  func oTDFLDTPR_1_47.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF' or IsAlt())
    endwith
  endfunc

  add object oTDFLACCO_1_48 as StdCheck with uid="JPXPIJEVEU",rtseq=48,rtrep=.f.,left=538, top=153, caption="Dati accompagnatori",;
    ToolTipText = "Se attivo: documento con dati accompagnatori",;
    HelpContextID = 30832517,;
    cFormVar="w_TDFLACCO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLACCO_1_48.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLACCO_1_48.GetRadio()
    this.Parent.oContained.w_TDFLACCO = this.RadioValue()
    return .t.
  endfunc

  func oTDFLACCO_1_48.SetRadio()
    this.Parent.oContained.w_TDFLACCO=trim(this.Parent.oContained.w_TDFLACCO)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLACCO=='S',1,;
      0)
  endfunc

  func oTDFLACCO_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TFFLGEFA<>'B' AND .w_TDCATDOC<>'RF')
    endwith
   endif
  endfunc

  func oTDFLACCO_1_48.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF' or IsAlt())
    endwith
  endfunc

  add object oTDFLPACK_1_49 as StdCheck with uid="SCJGJWIVOY",rtseq=49,rtrep=.f.,left=538, top=172, caption="Packing List",;
    ToolTipText = "Se attivo: documento elaborato attraverso la Packing List",;
    HelpContextID = 13006721,;
    cFormVar="w_TDFLPACK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLPACK_1_49.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLPACK_1_49.GetRadio()
    this.Parent.oContained.w_TDFLPACK = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPACK_1_49.SetRadio()
    this.Parent.oContained.w_TDFLPACK=trim(this.Parent.oContained.w_TDFLPACK)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPACK=='S',1,;
      0)
  endfunc

  func oTDFLPACK_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TFFLGEFA<>'B' AND .w_TDCATDOC<>'RF')
    endwith
   endif
  endfunc

  func oTDFLPACK_1_49.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF' or IsAlt())
    endwith
  endfunc

  add object oTDBOLDOG_1_50 as StdCheck with uid="RMOTHJZZJT",rtseq=50,rtrep=.f.,left=538, top=191, caption="Bolla doganale",;
    ToolTipText = "Se attivo, la prima nota generata dalla contabilizzazione non creer� righe omaggio contabili",;
    HelpContextID = 209111171,;
    cFormVar="w_TDBOLDOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDBOLDOG_1_50.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDBOLDOG_1_50.GetRadio()
    this.Parent.oContained.w_TDBOLDOG = this.RadioValue()
    return .t.
  endfunc

  func oTDBOLDOG_1_50.SetRadio()
    this.Parent.oContained.w_TDBOLDOG=trim(this.Parent.oContained.w_TDBOLDOG)
    this.value = ;
      iif(this.Parent.oContained.w_TDBOLDOG=='S',1,;
      0)
  endfunc

  func oTDBOLDOG_1_50.mHide()
    with this.Parent.oContained
      return (.w_TDFLVEAC='V' Or Not .w_TDCATDOC $ 'FA-NC')
    endwith
  endfunc

  add object oTDFLCCAU_1_51 as StdCheck with uid="ZXSLUBLRQP",rtseq=51,rtrep=.f.,left=538, top=210, caption="Cambio causale mag.",;
    ToolTipText = "Attivo: abilita la modifica della causale di magazzino nei dati di riga documenti",;
    HelpContextID = 32929675,;
    cFormVar="w_TDFLCCAU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLCCAU_1_51.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLCCAU_1_51.GetRadio()
    this.Parent.oContained.w_TDFLCCAU = this.RadioValue()
    return .t.
  endfunc

  func oTDFLCCAU_1_51.SetRadio()
    this.Parent.oContained.w_TDFLCCAU=trim(this.Parent.oContained.w_TDFLCCAU)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLCCAU=='S',1,;
      0)
  endfunc

  func oTDFLCCAU_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TFFLGEFA <> 'B')
    endwith
   endif
  endfunc

  func oTDFLCCAU_1_51.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDFLRIFI_1_52 as StdCheck with uid="XAYFAXDYIC",rtseq=52,rtrep=.f.,left=538, top=229, caption="Documento rifiutato",;
    ToolTipText = "Flag documento rifiutato",;
    HelpContextID = 149321599,;
    cFormVar="w_TDFLRIFI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLRIFI_1_52.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDFLRIFI_1_52.GetRadio()
    this.Parent.oContained.w_TDFLRIFI = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRIFI_1_52.SetRadio()
    this.Parent.oContained.w_TDFLRIFI=trim(this.Parent.oContained.w_TDFLRIFI)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRIFI=='S',1,;
      0)
  endfunc

  func oTDFLRIFI_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC='DI')
    endwith
   endif
  endfunc

  func oTDFLRIFI_1_52.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDFLRIAT_1_53 as StdCheck with uid="PLGDPHTYZX",rtseq=53,rtrep=.f.,left=538, top=248, caption="Ritenuta",;
    ToolTipText = "Se attivo, abilita la gestione delle ritenute",;
    HelpContextID = 149321610,;
    cFormVar="w_TDFLRIAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLRIAT_1_53.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLRIAT_1_53.GetRadio()
    this.Parent.oContained.w_TDFLRIAT = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRIAT_1_53.SetRadio()
    this.Parent.oContained.w_TDFLRIAT=trim(this.Parent.oContained.w_TDFLRIAT)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRIAT=='S',1,;
      0)
  endfunc

  func oTDFLRIAT_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_TDCATDOC $ 'FA-RF-FC-NC'  OR (.w_TDCATDOC='DI' AND IsAlt())) AND .w_TDFLVEAC='V')
    endwith
   endif
  endfunc

  func oTDFLRIAT_1_53.mHide()
    with this.Parent.oContained
      return (g_RITE='S' OR IsAlt())
    endwith
  endfunc

  add object oTDFLBACA_1_54 as StdCheck with uid="UEWCGRWHNO",rtseq=54,rtrep=.f.,left=538, top=267, caption="Basi di calcolo",;
    ToolTipText = "Se attivo, abilita la gestione delle basi di calcolo sui documenti",;
    HelpContextID = 266762103,;
    cFormVar="w_TDFLBACA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLBACA_1_54.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLBACA_1_54.GetRadio()
    this.Parent.oContained.w_TDFLBACA = this.RadioValue()
    return .t.
  endfunc

  func oTDFLBACA_1_54.SetRadio()
    this.Parent.oContained.w_TDFLBACA=trim(this.Parent.oContained.w_TDFLBACA)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLBACA=='S',1,;
      0)
  endfunc

  func oTDFLBACA_1_54.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oTDSEQPRE_1_55 as StdCheck with uid="ZIPEZWZBNP",rtseq=55,rtrep=.f.,left=20, top=468, caption="Prezzo unitario",;
    ToolTipText = "Se attivo: fuori sequenza durante l'inserimento riga documento",;
    HelpContextID = 265308027,;
    cFormVar="w_TDSEQPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDSEQPRE_1_55.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDSEQPRE_1_55.GetRadio()
    this.Parent.oContained.w_TDSEQPRE = this.RadioValue()
    return .t.
  endfunc

  func oTDSEQPRE_1_55.SetRadio()
    this.Parent.oContained.w_TDSEQPRE=trim(this.Parent.oContained.w_TDSEQPRE)
    this.value = ;
      iif(this.Parent.oContained.w_TDSEQPRE=='S',1,;
      0)
  endfunc

  add object oTDSEQSCO_1_56 as StdCheck with uid="POKNWEJPPZ",rtseq=56,rtrep=.f.,left=20, top=491, caption="Sconti",;
    ToolTipText = "Se attivo: fuori sequenza durante l'inserimento riga documento",;
    HelpContextID = 47204229,;
    cFormVar="w_TDSEQSCO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDSEQSCO_1_56.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDSEQSCO_1_56.GetRadio()
    this.Parent.oContained.w_TDSEQSCO = this.RadioValue()
    return .t.
  endfunc

  func oTDSEQSCO_1_56.SetRadio()
    this.Parent.oContained.w_TDSEQSCO=trim(this.Parent.oContained.w_TDSEQSCO)
    this.value = ;
      iif(this.Parent.oContained.w_TDSEQSCO=='S',1,;
      0)
  endfunc

  func oTDSEQSCO_1_56.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDMODDES_1_57 as StdCheck with uid="TJGGPXKTYD",rtseq=57,rtrep=.f.,left=538, top=286, caption="Modifica descrizione articolo",;
    ToolTipText = "Se attivo, sulla riga del documento risulter� possibile modificare la descrizione/ descrizione supplementare dell'articolo",;
    HelpContextID = 50980745,;
    cFormVar="w_TDMODDES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDMODDES_1_57.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDMODDES_1_57.GetRadio()
    this.Parent.oContained.w_TDMODDES = this.RadioValue()
    return .t.
  endfunc

  func oTDMODDES_1_57.SetRadio()
    this.Parent.oContained.w_TDMODDES=trim(this.Parent.oContained.w_TDMODDES)
    this.value = ;
      iif(this.Parent.oContained.w_TDMODDES=='S',1,;
      0)
  endfunc

  add object oTDSEQMA1_1_58 as StdCheck with uid="IKFBVEEYIO",rtseq=58,rtrep=.f.,left=140, top=468, caption="Magazzino principale",;
    ToolTipText = "Se attivo: fuori sequenza durante l'inserimento riga documento",;
    HelpContextID = 214976359,;
    cFormVar="w_TDSEQMA1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDSEQMA1_1_58.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDSEQMA1_1_58.GetRadio()
    this.Parent.oContained.w_TDSEQMA1 = this.RadioValue()
    return .t.
  endfunc

  func oTDSEQMA1_1_58.SetRadio()
    this.Parent.oContained.w_TDSEQMA1=trim(this.Parent.oContained.w_TDSEQMA1)
    this.value = ;
      iif(this.Parent.oContained.w_TDSEQMA1=='S',1,;
      0)
  endfunc

  func oTDSEQMA1_1_58.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDSEQMA2_1_59 as StdCheck with uid="FIEDUBBJNU",rtseq=59,rtrep=.f.,left=140, top=491, caption="Magazzino collegato",;
    ToolTipText = "Se attivo: fuori sequenza durante l'inserimento riga documento",;
    HelpContextID = 214976360,;
    cFormVar="w_TDSEQMA2", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDSEQMA2_1_59.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDSEQMA2_1_59.GetRadio()
    this.Parent.oContained.w_TDSEQMA2 = this.RadioValue()
    return .t.
  endfunc

  func oTDSEQMA2_1_59.SetRadio()
    this.Parent.oContained.w_TDSEQMA2=trim(this.Parent.oContained.w_TDSEQMA2)
    this.value = ;
      iif(this.Parent.oContained.w_TDSEQMA2=='S',1,;
      0)
  endfunc

  func oTDSEQMA2_1_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CAUCOL))
    endwith
   endif
  endfunc

  func oTDSEQMA2_1_59.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL))
    endwith
  endfunc


  add object oObj_1_87 as cp_runprogram with uid="OSFIGFIRRJ",left=0, top=552, width=234,height=23,;
    caption='GSVE_BTD(I)',;
   bGlobalFont=.t.,;
    prg="GSVE_BTD('I')",;
    cEvent = "w_TDFLINTE Changed",;
    nPag=1;
    , HelpContextID = 45310250


  add object oObj_1_88 as cp_runprogram with uid="JQNCVKRAMZ",left=238, top=552, width=266,height=20,;
    caption='GSVE_BTD(T)',;
   bGlobalFont=.t.,;
    prg="GSVE_BTD('T')",;
    cEvent = "w_TDCATDOC Changed",;
    nPag=1;
    , HelpContextID = 45313066


  add object oObj_1_92 as cp_runprogram with uid="PMPPDSGNKI",left=0, top=578, width=179,height=19,;
    caption='GSAR_BTD(CA)',;
   bGlobalFont=.t.,;
    prg="GSAR_BTD('CA')",;
    cEvent = "CheckFinali",;
    nPag=1;
    , ToolTipText = "Check all'F10 (da gsac_atd/gsve_atd)";
    , HelpContextID = 48859946


  add object oObj_1_93 as cp_runprogram with uid="WXOYMSQPBQ",left=238, top=578, width=205,height=20,;
    caption='GSAR_BTD(ED)',;
   bGlobalFont=.t.,;
    prg="GSAR_BTD('ED')",;
    cEvent = "Edit Started",;
    nPag=1;
    , ToolTipText = "Verifica se la casuale � gia stata movimentata (impedisce modifica tipo)";
    , HelpContextID = 48872746


  add object oObj_1_98 as cp_runprogram with uid="WYBNWTKUEH",left=511, top=552, width=142,height=26,;
    caption='GSVE_BCR',;
   bGlobalFont=.t.,;
    prg="GSVE_BCR",;
    cEvent = "ChkOrigini",;
    nPag=1;
    , HelpContextID = 45123000

  add object oTDMINIMP_1_104 as StdField with uid="GYTATYYSLL",rtseq=173,rtrep=.f.,;
    cFormVar = "w_TDMINIMP", cQueryName = "TDMINIMP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore positivo",;
    ToolTipText = "Importo minimo fatturabile. Se il totale delle rate risulter� inferiore a questo importo, il loro valore verr� considerato come sconto finanziario e il totale documento impostato a 0",;
    HelpContextID = 123476090,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=142, Top=354, cSayPict="v_PV(38+VVL)", cGetPict="v_PV(38+VVL)"

  func oTDMINIMP_1_104.mHide()
    with this.Parent.oContained
      return (.w_TFFLGEFA<>'B')
    endwith
  endfunc

  func oTDMINIMP_1_104.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TDMINIMP>=0)
    endwith
    return bRes
  endfunc

  add object oTDMINVAL_1_105 as StdField with uid="FORMAWLMOQ",rtseq=174,rtrep=.f.,;
    cFormVar = "w_TDMINVAL", cQueryName = "TDMINVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta per l'importo minimo fatturabile",;
    HelpContextID = 94627714,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=315, Top=354, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_TDMINVAL"

  func oTDMINVAL_1_105.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDMINIMP<>0)
    endwith
   endif
  endfunc

  func oTDMINVAL_1_105.mHide()
    with this.Parent.oContained
      return (.w_TFFLGEFA<>'B')
    endwith
  endfunc

  func oTDMINVAL_1_105.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_105('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDMINVAL_1_105.ecpDrop(oSource)
    this.Parent.oContained.link_1_105('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDMINVAL_1_105.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oTDMINVAL_1_105'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"",'',this.parent.oContained
  endproc
  proc oTDMINVAL_1_105.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_TDMINVAL
     i_obj.ecpSave()
  endproc


  add object oTDTOTDOC_1_106 as StdCombo with uid="TJVRRBXGXO",rtseq=175,rtrep=.f.,left=143,top=381,width=84,height=21;
    , ToolTipText = "Controllo sul totale minimo vendibile nel documento";
    , HelpContextID = 200648839;
    , cFormVar="w_TDTOTDOC",RowSource=""+"Escluso,"+"Bloccante,"+"Avviso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDTOTDOC_1_106.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'B',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDTOTDOC_1_106.GetRadio()
    this.Parent.oContained.w_TDTOTDOC = this.RadioValue()
    return .t.
  endfunc

  func oTDTOTDOC_1_106.SetRadio()
    this.Parent.oContained.w_TDTOTDOC=trim(this.Parent.oContained.w_TDTOTDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TDTOTDOC=='E',1,;
      iif(this.Parent.oContained.w_TDTOTDOC=='B',2,;
      iif(this.Parent.oContained.w_TDTOTDOC=='A',3,;
      0)))
  endfunc

  func oTDTOTDOC_1_106.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC $ 'FA-NC-RF' )
    endwith
  endfunc

  add object oTDIMPMIN_1_107 as StdField with uid="DMMKULYJAM",rtseq=176,rtrep=.f.,;
    cFormVar = "w_TDIMPMIN", cQueryName = "TDIMPMIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore minimo totale documento",;
    HelpContextID = 54024316,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=235, Top=381, cSayPict="v_PU(38+VVP)", cGetPict="v_GU(38+VVP)"

  func oTDIMPMIN_1_107.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDTOTDOC<>'E')
    endwith
   endif
  endfunc

  func oTDIMPMIN_1_107.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC $ 'FA-NC-RF' )
    endwith
  endfunc


  add object oTDIVAMIN_1_108 as StdCombo with uid="VBRAABZQAX",rtseq=177,rtrep=.f.,left=385,top=381,width=90,height=21;
    , ToolTipText = "Importo minimo espresso al lordo o netto";
    , HelpContextID = 69163132;
    , cFormVar="w_TDIVAMIN",RowSource=""+"Lordo IVA,"+"Netto IVA", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDIVAMIN_1_108.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oTDIVAMIN_1_108.GetRadio()
    this.Parent.oContained.w_TDIVAMIN = this.RadioValue()
    return .t.
  endfunc

  func oTDIVAMIN_1_108.SetRadio()
    this.Parent.oContained.w_TDIVAMIN=trim(this.Parent.oContained.w_TDIVAMIN)
    this.value = ;
      iif(this.Parent.oContained.w_TDIVAMIN=='L',1,;
      iif(this.Parent.oContained.w_TDIVAMIN=='N',2,;
      0))
  endfunc

  func oTDIVAMIN_1_108.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDIMPMIN<>0)
    endwith
   endif
  endfunc

  func oTDIVAMIN_1_108.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC $ 'FA-NC-RF')
    endwith
  endfunc


  add object oTDMINVEN_1_109 as StdCombo with uid="SSPUUHXSRO",rtseq=178,rtrep=.f.,left=143,top=409,width=84,height=21;
    , ToolTipText = "Controllo sulla quantit� minima vendibile/ordinabile impostabile a livello di riga o di totale documento";
    , HelpContextID = 94627716;
    , cFormVar="w_TDMINVEN",RowSource=""+"Escluso,"+"Bloccante,"+"Avviso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDMINVEN_1_109.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'B',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDMINVEN_1_109.GetRadio()
    this.Parent.oContained.w_TDMINVEN = this.RadioValue()
    return .t.
  endfunc

  func oTDMINVEN_1_109.SetRadio()
    this.Parent.oContained.w_TDMINVEN=trim(this.Parent.oContained.w_TDMINVEN)
    this.value = ;
      iif(this.Parent.oContained.w_TDMINVEN=='E',1,;
      iif(this.Parent.oContained.w_TDMINVEN=='B',2,;
      iif(this.Parent.oContained.w_TDMINVEN=='A',3,;
      0)))
  endfunc

  func oTDMINVEN_1_109.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC $ 'FA-NC-RF' or Isalt())
    endwith
  endfunc


  add object oTDRIOTOT_1_110 as StdCombo with uid="HCZOSVTFCP",rtseq=179,rtrep=.f.,left=235,top=409,width=138,height=21;
    , ToolTipText = "Controllo a livello di riga o di totale documento";
    , HelpContextID = 62142346;
    , cFormVar="w_TDRIOTOT",RowSource=""+"Riga,"+"Totale documento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDRIOTOT_1_110.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oTDRIOTOT_1_110.GetRadio()
    this.Parent.oContained.w_TDRIOTOT = this.RadioValue()
    return .t.
  endfunc

  func oTDRIOTOT_1_110.SetRadio()
    this.Parent.oContained.w_TDRIOTOT=trim(this.Parent.oContained.w_TDRIOTOT)
    this.value = ;
      iif(this.Parent.oContained.w_TDRIOTOT=='R',1,;
      iif(this.Parent.oContained.w_TDRIOTOT=='T',2,;
      0))
  endfunc

  func oTDRIOTOT_1_110.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDMINVEN<>'E')
    endwith
   endif
  endfunc

  func oTDRIOTOT_1_110.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC $ 'FA-NC-RF' or Isalt())
    endwith
  endfunc

  add object oSIMVAL_1_113 as StdField with uid="FCWVDJDHRU",rtseq=180,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione valuta",;
    HelpContextID = 182512678,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=365, Top=354, InputMask=replicate('X',5)

  func oSIMVAL_1_113.mHide()
    with this.Parent.oContained
      return (.w_TFFLGEFA<>'B')
    endwith
  endfunc

  add object oTDFLGEIN_1_127 as StdCheck with uid="SRIFJGRQKA",rtseq=215,rtrep=.f.,left=538, top=305, caption="Genera INTRA",;
    ToolTipText = "Se attivo genera gli elenchi intra per i documenti di trasporto",;
    HelpContextID = 197757052,;
    cFormVar="w_TDFLGEIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLGEIN_1_127.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDFLGEIN_1_127.GetRadio()
    this.Parent.oContained.w_TDFLGEIN = this.RadioValue()
    return .t.
  endfunc

  func oTDFLGEIN_1_127.SetRadio()
    this.Parent.oContained.w_TDFLGEIN=trim(this.Parent.oContained.w_TDFLGEIN)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLGEIN=='S',1,;
      0)
  endfunc

  func oTDFLGEIN_1_127.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC<>'DT')
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="TUXWQXSLSC",Visible=.t., Left=10, Top=20,;
    Alignment=1, Width=130, Height=15,;
    Caption="Tipo documento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_62 as StdString with uid="URDHUVEFSK",Visible=.t., Left=10, Top=51,;
    Alignment=1, Width=130, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="KGFERLFAFF",Visible=.t., Left=10, Top=104,;
    Alignment=1, Width=130, Height=15,;
    Caption="Causale magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="DCBJINRORY",Visible=.t., Left=10, Top=188,;
    Alignment=1, Width=130, Height=15,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (.w_TDFLVEAC<>'V')
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="SBXJJFXBZB",Visible=.t., Left=10, Top=132,;
    Alignment=1, Width=130, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_66 as StdString with uid="KBYKWINDGB",Visible=.t., Left=10, Top=160,;
    Alignment=1, Width=130, Height=15,;
    Caption="Magazzino collegato:"  ;
  , bGlobalFont=.t.

  func oStr_1_66.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL))
    endwith
  endfunc

  add object oStr_1_67 as StdString with uid="PANAHKMQIK",Visible=.t., Left=361, Top=326,;
    Alignment=1, Width=109, Height=15,;
    Caption="Raggruppamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (NOT (.w_TFFLGEFA='S' AND .w_TDCATDOC<>'RF'))
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="OPHRHETLXJ",Visible=.t., Left=10, Top=326,;
    Alignment=1, Width=130, Height=15,;
    Caption="Aspetto esteriore:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (.w_TDFLACCO<>'S' OR .w_TDCATDOC='RF')
    endwith
  endfunc

  add object oStr_1_71 as StdString with uid="VFEIKLGIRJ",Visible=.t., Left=10, Top=216,;
    Alignment=1, Width=130, Height=15,;
    Caption="Serie documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="LWAEZJBJIY",Visible=.t., Left=17, Top=442,;
    Alignment=0, Width=333, Height=18,;
    Caption="Campi fuori sequenza"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="WMDIWTJOFP",Visible=.t., Left=10, Top=270,;
    Alignment=1, Width=130, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="IFHSDAFYKO",Visible=.t., Left=10, Top=299,;
    Alignment=1, Width=130, Height=15,;
    Caption="Qt� proposta:"  ;
  , bGlobalFont=.t.

  func oStr_1_76.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_89 as StdString with uid="KATNUYNIVZ",Visible=.t., Left=223, Top=133,;
    Alignment=1, Width=143, Height=18,;
    Caption="Valorizzazione magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_89.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_90 as StdString with uid="ELTLPSSSWG",Visible=.t., Left=221, Top=162,;
    Alignment=1, Width=145, Height=18,;
    Caption="Valorizzazione magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_90.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL))
    endwith
  endfunc

  add object oStr_1_111 as StdString with uid="CJWPJWLJAE",Visible=.t., Left=10, Top=356,;
    Alignment=1, Width=130, Height=18,;
    Caption="Imp. min. fatturabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_111.mHide()
    with this.Parent.oContained
      return (.w_TFFLGEFA<>'B')
    endwith
  endfunc

  add object oStr_1_112 as StdString with uid="IRLOSCGHRT",Visible=.t., Left=286, Top=356,;
    Alignment=1, Width=26, Height=18,;
    Caption="in:"  ;
  , bGlobalFont=.t.

  func oStr_1_112.mHide()
    with this.Parent.oContained
      return (.w_TFFLGEFA<>'B')
    endwith
  endfunc

  add object oStr_1_114 as StdString with uid="MXNSZYQSVY",Visible=.t., Left=340, Top=216,;
    Alignment=1, Width=131, Height=15,;
    Caption="N� sconti/maggior.:"  ;
  , bGlobalFont=.t.

  func oStr_1_114.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_115 as StdString with uid="VGKFZQEEDG",Visible=.t., Left=494, Top=20,;
    Alignment=1, Width=71, Height=15,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  func oStr_1_115.mHide()
    with this.Parent.oContained
      return (g_ACQU='S')
    endwith
  endfunc

  add object oStr_1_117 as StdString with uid="LLDJZYJYKY",Visible=.t., Left=10, Top=74,;
    Alignment=1, Width=130, Height=15,;
    Caption="Doc. emesso/ricevuto:"  ;
  , bGlobalFont=.t.

  func oStr_1_117.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC $ 'FA-NC-OR-RF' or isalt())
    endwith
  endfunc

  add object oStr_1_118 as StdString with uid="QFPIGWOGYK",Visible=.t., Left=21, Top=241,;
    Alignment=1, Width=119, Height=18,;
    Caption="Serie protocollo:"  ;
  , bGlobalFont=.t.

  func oStr_1_118.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_1_120 as StdString with uid="MWLPVJZEXD",Visible=.t., Left=14, Top=382,;
    Alignment=1, Width=126, Height=18,;
    Caption="Controllo totale doc.:"  ;
  , bGlobalFont=.t.

  func oStr_1_120.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC $ 'FA-NC-RF' )
    endwith
  endfunc

  add object oStr_1_121 as StdString with uid="IPJAXSJNZL",Visible=.t., Left=1, Top=410,;
    Alignment=1, Width=139, Height=18,;
    Caption="Controllo min. vendibile:"  ;
  , bGlobalFont=.t.

  func oStr_1_121.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC$'FA-NC-RF' or Isalt())
    endwith
  endfunc

  add object oStr_1_122 as StdString with uid="OQYIBKOOCQ",Visible=.t., Left=221, Top=299,;
    Alignment=1, Width=134, Height=18,;
    Caption="Stato dei documenti:"  ;
  , bGlobalFont=.t.

  add object oBox_1_73 as StdBox with uid="OXZGYUDHMG",left=14, top=461, width=290,height=59
enddefine
define class tgsve_atdPag2 as StdContainer
  Width  = 721
  height = 555
  stdWidth  = 721
  stdheight = 555
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_2_2 as StdField with uid="XYZLNMEZNL",rtseq=107,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 87858906,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=60, Left=136, Top=25, InputMask=replicate('X',5)

  add object oDESC_2_3 as StdField with uid="VJNQIGQTCN",rtseq=108,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 88193226,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=200, Top=25, InputMask=replicate('X',35)

  add object oTDFLPROV_2_4 as StdCheck with uid="OASYARMXTP",rtseq=141,rtrep=.f.,left=41, top=68, caption="Genera provvigioni",;
    ToolTipText = "Se attivo: il documento gestisce le provvigioni agente",;
    HelpContextID = 29783948,;
    cFormVar="w_TDFLPROV", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLPROV_2_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDFLPROV_2_4.GetRadio()
    this.Parent.oContained.w_TDFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPROV_2_4.SetRadio()
    this.Parent.oContained.w_TDFLPROV=trim(this.Parent.oContained.w_TDFLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPROV=='S',1,;
      0)
  endfunc

  func oTDFLPROV_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERAGE='S' AND .w_TDCATDOC<>'RF' AND .w_TDFLVEAC='V')
    endwith
   endif
  endfunc

  func oTDFLPROV_2_4.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF' OR .w_TDFLVEAC<>'V' or IsAlt())
    endwith
  endfunc

  add object oTDFLSILI_2_5 as StdCheck with uid="ZHEENJLQCG",rtseq=142,rtrep=.f.,left=41, top=93, caption="Agg.lettere di intento",;
    ToolTipText = "Se attivo: aggiorna gli importi relativi ad eventuali lettere di intento",;
    HelpContextID = 118065281,;
    cFormVar="w_TDFLSILI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLSILI_2_5.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLSILI_2_5.GetRadio()
    this.Parent.oContained.w_TDFLSILI = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSILI_2_5.SetRadio()
    this.Parent.oContained.w_TDFLSILI=trim(this.Parent.oContained.w_TDFLSILI)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSILI=='S',1,;
      0)
  endfunc

  func oTDFLSILI_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC $ 'DT-FA-NC')
    endwith
   endif
  endfunc

  func oTDFLSILI_2_5.mHide()
    with this.Parent.oContained
      return (NOT .w_TDCATDOC $ 'DT-FA-NC')
    endwith
  endfunc

  add object oTDPRZVAC_2_6 as StdCheck with uid="RBFCRVZYZK",rtseq=143,rtrep=.f.,left=41, top=118, caption="Prezzo default U.P.V.",;
    ToolTipText = "Prezzo e sconti, in assenza di altre condizioni valide, verranno calcolati dall'ultimo movimento di vendita",;
    HelpContextID = 107812729,;
    cFormVar="w_TDPRZVAC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDPRZVAC_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDPRZVAC_2_6.GetRadio()
    this.Parent.oContained.w_TDPRZVAC = this.RadioValue()
    return .t.
  endfunc

  func oTDPRZVAC_2_6.SetRadio()
    this.Parent.oContained.w_TDPRZVAC=trim(this.Parent.oContained.w_TDPRZVAC)
    this.value = ;
      iif(this.Parent.oContained.w_TDPRZVAC=='S',1,;
      0)
  endfunc

  func oTDPRZVAC_2_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDPRZDES_2_7 as StdCheck with uid="NBENINRKUH",rtseq=144,rtrep=.f.,left=41, top=143, caption="Ricalcolo prezzi",;
    ToolTipText = "Se attivo, in importazione e generazione fatture differite vengono ricalcolati prezzo e sconti in base ai dati del documento di destinazione",;
    HelpContextID = 74258313,;
    cFormVar="w_TDPRZDES", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDPRZDES_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDPRZDES_2_7.GetRadio()
    this.Parent.oContained.w_TDPRZDES = this.RadioValue()
    return .t.
  endfunc

  func oTDPRZDES_2_7.SetRadio()
    this.Parent.oContained.w_TDPRZDES=trim(this.Parent.oContained.w_TDPRZDES)
    this.value = ;
      iif(this.Parent.oContained.w_TDPRZDES=='S',1,;
      0)
  endfunc

  func oTDPRZDES_2_7.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDFLVALO_2_8 as StdCheck with uid="ZOSEZWRWYM",rtseq=145,rtrep=.f.,left=41, top=168, caption="Modifica valore",;
    ToolTipText = "Fattura spese, consente la modifica in valore di dt rivalorizzati o fatture immediate",;
    HelpContextID = 249137275,;
    cFormVar="w_TDFLVALO", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Attivabile solo per fatture o note di credito";
   , bGlobalFont=.t.


  func oTDFLVALO_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLVALO_2_8.GetRadio()
    this.Parent.oContained.w_TDFLVALO = this.RadioValue()
    return .t.
  endfunc

  func oTDFLVALO_2_8.SetRadio()
    this.Parent.oContained.w_TDFLVALO=trim(this.Parent.oContained.w_TDFLVALO)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLVALO=='S',1,;
      0)
  endfunc

  func oTDFLVALO_2_8.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oTDFLVALO_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_TDCATDOC $  'FA-NC' And .w_TDFLVALO='S') Or Empty( .w_TDFLVALO ))
    endwith
    return bRes
  endfunc

  add object oTDCHKTOT_2_9 as StdCheck with uid="MFQBTQBIOE",rtseq=146,rtrep=.f.,left=41, top=193, caption="Righe normali <> 0",;
    ToolTipText = "Attivo: impedisce il salvataggio del documento se presenti righe a valore 0",;
    HelpContextID = 57821066,;
    cFormVar="w_TDCHKTOT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDCHKTOT_2_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDCHKTOT_2_9.GetRadio()
    this.Parent.oContained.w_TDCHKTOT = this.RadioValue()
    return .t.
  endfunc

  func oTDCHKTOT_2_9.SetRadio()
    this.Parent.oContained.w_TDCHKTOT=trim(this.Parent.oContained.w_TDCHKTOT)
    this.value = ;
      iif(this.Parent.oContained.w_TDCHKTOT=='S',1,;
      0)
  endfunc

  add object oTDFLBLEV_2_10 as StdCheck with uid="PJHYBBYPMU",rtseq=147,rtrep=.f.,left=41, top=218, caption="Blocca doc. evasi",;
    ToolTipText = "Se attivo: impedisce la modifica dei documenti evasi (anche parzialmente)",;
    HelpContextID = 182876044,;
    cFormVar="w_TDFLBLEV", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLBLEV_2_10.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLBLEV_2_10.GetRadio()
    this.Parent.oContained.w_TDFLBLEV = this.RadioValue()
    return .t.
  endfunc

  func oTDFLBLEV_2_10.SetRadio()
    this.Parent.oContained.w_TDFLBLEV=trim(this.Parent.oContained.w_TDFLBLEV)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLBLEV=='S',1,;
      0)
  endfunc

  func oTDFLBLEV_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not IsAlt())
    endwith
   endif
  endfunc

  add object oTDFLSPIN_2_11 as StdCheck with uid="ZVJLEGOGKY",rtseq=148,rtrep=.f.,left=41, top=243, caption="Spese incasso",;
    ToolTipText = "Attivo: sul documento le spese di incasso saranno calcolate e modificabili",;
    HelpContextID = 624764,;
    cFormVar="w_TDFLSPIN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLSPIN_2_11.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLSPIN_2_11.GetRadio()
    this.Parent.oContained.w_TDFLSPIN = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSPIN_2_11.SetRadio()
    this.Parent.oContained.w_TDFLSPIN=trim(this.Parent.oContained.w_TDFLSPIN)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSPIN=='S',1,;
      0)
  endfunc

  func oTDFLSPIN_2_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDFLQRIO_2_12 as StdCheck with uid="WYIFNHYOYM",rtseq=149,rtrep=.f.,left=41, top=268, caption="Calcolo qt� riordino",;
    ToolTipText = "Se attivo: la quantit� viene ricalcolata in base alla quantit� minima e al lotto di riordino. Check no evasione e raggruppa nelle origini obbligatorio.",;
    HelpContextID = 237602939,;
    cFormVar="w_TDFLQRIO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLQRIO_2_12.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLQRIO_2_12.GetRadio()
    this.Parent.oContained.w_TDFLQRIO = this.RadioValue()
    return .t.
  endfunc

  func oTDFLQRIO_2_12.SetRadio()
    this.Parent.oContained.w_TDFLQRIO=trim(this.Parent.oContained.w_TDFLQRIO)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLQRIO=='S',1,;
      0)
  endfunc

  func oTDFLQRIO_2_12.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDFLPREV_2_13 as StdCheck with uid="FWYYUDXQDM",rtseq=150,rtrep=.f.,left=41, top=293, caption="Calcolo data evasione",;
    ToolTipText = "Se attivo: la data prevista evasione viene calcolata in base ai giorni approvvigionamento",;
    HelpContextID = 29783948,;
    cFormVar="w_TDFLPREV", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLPREV_2_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLPREV_2_13.GetRadio()
    this.Parent.oContained.w_TDFLPREV = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPREV_2_13.SetRadio()
    this.Parent.oContained.w_TDFLPREV=trim(this.Parent.oContained.w_TDFLPREV)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPREV=='S',1,;
      0)
  endfunc

  func oTDFLPREV_2_13.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDFLAPCA_2_14 as StdCheck with uid="EYKGDCBJDX",rtseq=151,rtrep=.f.,left=41, top=318, caption="Applica contributi accessori",;
    ToolTipText = "Se attivo sul documento sar� applicato il contributo accessorio. Il contributo con applicazione su peso viene applicato solo se la categoria documento � fattura o nota di credito",;
    HelpContextID = 248936311,;
    cFormVar="w_TDFLAPCA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLAPCA_2_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDFLAPCA_2_14.GetRadio()
    this.Parent.oContained.w_TDFLAPCA = this.RadioValue()
    return .t.
  endfunc

  func oTDFLAPCA_2_14.SetRadio()
    this.Parent.oContained.w_TDFLAPCA=trim(this.Parent.oContained.w_TDFLAPCA)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLAPCA=='S',1,;
      0)
  endfunc

  func oTDFLAPCA_2_14.mHide()
    with this.Parent.oContained
      return (g_COAC='N' OR ( .w_TDFLINTE='N' AND .w_TDCATDOC<>'RF' ))
    endwith
  endfunc

  add object oTDRIPCON_2_15 as StdCheck with uid="TOFXIKTXEB",rtseq=152,rtrep=.f.,left=41, top=341, caption="Ripartisce contributi accessori",;
    ToolTipText = "Permette la ripartizione del valore dei contributi accessori sul valore fiscale dell'articolo che li ha generati",;
    HelpContextID = 222021756,;
    cFormVar="w_TDRIPCON", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDRIPCON_2_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDRIPCON_2_15.GetRadio()
    this.Parent.oContained.w_TDRIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTDRIPCON_2_15.SetRadio()
    this.Parent.oContained.w_TDRIPCON=trim(this.Parent.oContained.w_TDRIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TDRIPCON=='S',1,;
      0)
  endfunc

  func oTDRIPCON_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC $ "FA-NC-RF" AND g_COAC = "S")
    endwith
   endif
  endfunc

  func oTDRIPCON_2_15.mHide()
    with this.Parent.oContained
      return (NOT ( .w_TDCATDOC $ "FA-NC-RF" AND g_COAC = "S" ))
    endwith
  endfunc

  add object oTDNOSTCO_2_16 as StdCheck with uid="AAHNRPZIJX",rtseq=153,rtrep=.f.,left=41, top=369, caption="Non stampa i contributi accessori",;
    ToolTipText = "Se attivo sulla stampa del documento non saranno riportate le righe di contributo accessorio",;
    HelpContextID = 66713477,;
    cFormVar="w_TDNOSTCO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDNOSTCO_2_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDNOSTCO_2_16.GetRadio()
    this.Parent.oContained.w_TDNOSTCO = this.RadioValue()
    return .t.
  endfunc

  func oTDNOSTCO_2_16.SetRadio()
    this.Parent.oContained.w_TDNOSTCO=trim(this.Parent.oContained.w_TDNOSTCO)
    this.value = ;
      iif(this.Parent.oContained.w_TDNOSTCO=='S',1,;
      0)
  endfunc

  func oTDNOSTCO_2_16.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  add object oTDFLGETR_2_17 as StdCheck with uid="KYLTSFMISU",rtseq=154,rtrep=.f.,left=41, top=391, caption="Gestione scheda di trasporto",;
    ToolTipText = "Se attivo la causale gestisce la stampa della scheda di trasporto",;
    HelpContextID = 70678408,;
    cFormVar="w_TDFLGETR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLGETR_2_17.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLGETR_2_17.GetRadio()
    this.Parent.oContained.w_TDFLGETR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLGETR_2_17.SetRadio()
    this.Parent.oContained.w_TDFLGETR=trim(this.Parent.oContained.w_TDFLGETR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLGETR=='S',1,;
      0)
  endfunc

  func oTDFLGETR_2_17.mHide()
    with this.Parent.oContained
      return (.w_TDFLINTE<>'F' OR !(.w_TDCATDOC='FA' OR .w_TDCATDOC='DT'))
    endwith
  endfunc

  add object oTDFLSPIM_2_19 as StdCheck with uid="RRWUDIISZJ",rtseq=155,rtrep=.f.,left=409, top=68, caption="Cal. spese imballo",;
    ToolTipText = "Se attivo: calcola le spese di imballo utilizzando il metodo specificato",;
    HelpContextID = 624765,;
    cFormVar="w_TDFLSPIM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLSPIM_2_19.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLSPIM_2_19.GetRadio()
    this.Parent.oContained.w_TDFLSPIM = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSPIM_2_19.SetRadio()
    this.Parent.oContained.w_TDFLSPIM=trim(this.Parent.oContained.w_TDFLSPIM)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSPIM=='S',1,;
      0)
  endfunc

  func oTDFLSPIM_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
   endif
  endfunc

  func oTDFLSPIM_2_19.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDFLSPIM_2_20 as StdCheck with uid="OWCDCGHNRV",rtseq=156,rtrep=.f.,left=409, top=68, caption="Applica le spese generali",;
    ToolTipText = "Se attivo: applica le spese generali utilizzando il metodo specificato",;
    HelpContextID = 624765,;
    cFormVar="w_TDFLSPIM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLSPIM_2_20.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLSPIM_2_20.GetRadio()
    this.Parent.oContained.w_TDFLSPIM = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSPIM_2_20.SetRadio()
    this.Parent.oContained.w_TDFLSPIM=trim(this.Parent.oContained.w_TDFLSPIM)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSPIM=='S',1,;
      0)
  endfunc

  func oTDFLSPIM_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oTDFLSPIM_2_20.mHide()
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
  endfunc

  add object oMSDESIMB_2_21 as StdField with uid="QGWPUCKSXR",rtseq=157,rtrep=.f.,;
    cFormVar = "w_MSDESIMB", cQueryName = "MSDESIMB",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 118528504,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=464, Top=193, InputMask=replicate('X',40)

  func oMSDESIMB_2_21.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDFLSPTR_2_23 as StdCheck with uid="DCVIWUGWUP",rtseq=158,rtrep=.f.,left=409, top=93, caption="Cal. spese trasporto",;
    ToolTipText = "Se attivo: calcola le spese di trasporto utilizzando il metodo specificato",;
    HelpContextID = 267810696,;
    cFormVar="w_TDFLSPTR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLSPTR_2_23.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLSPTR_2_23.GetRadio()
    this.Parent.oContained.w_TDFLSPTR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSPTR_2_23.SetRadio()
    this.Parent.oContained.w_TDFLSPTR=trim(this.Parent.oContained.w_TDFLSPTR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSPTR=='S',1,;
      0)
  endfunc

  func oTDFLSPTR_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
   endif
  endfunc

  func oTDFLSPTR_2_23.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDFLSPTR_2_24 as StdCheck with uid="LGMJZMYJPA",rtseq=159,rtrep=.f.,left=409, top=93, caption="Applica la cassa di previdenza",;
    ToolTipText = "Se attivo: applica il CPA utilizzando il metodo specificato",;
    HelpContextID = 267810696,;
    cFormVar="w_TDFLSPTR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLSPTR_2_24.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLSPTR_2_24.GetRadio()
    this.Parent.oContained.w_TDFLSPTR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSPTR_2_24.SetRadio()
    this.Parent.oContained.w_TDFLSPTR=trim(this.Parent.oContained.w_TDFLSPTR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSPTR=='S',1,;
      0)
  endfunc

  func oTDFLSPTR_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oTDFLSPTR_2_24.mHide()
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
  endfunc

  add object oMSDESTRA_2_25 as StdField with uid="ICMJCNKZKT",rtseq=160,rtrep=.f.,;
    cFormVar = "w_MSDESTRA", cQueryName = "MSDESTRA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 66020871,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=464, Top=218, InputMask=replicate('X',40)

  func oMSDESTRA_2_25.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTDRIPINC_2_27 as StdCheck with uid="KYIXBFFWVM",rtseq=161,rtrep=.f.,left=409, top=118, caption="Ripartisce spese incasso",;
    ToolTipText = "Ripartisce spese incasso",;
    HelpContextID = 121358471,;
    cFormVar="w_TDRIPINC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDRIPINC_2_27.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDRIPINC_2_27.GetRadio()
    this.Parent.oContained.w_TDRIPINC = this.RadioValue()
    return .t.
  endfunc

  func oTDRIPINC_2_27.SetRadio()
    this.Parent.oContained.w_TDRIPINC=trim(this.Parent.oContained.w_TDRIPINC)
    this.value = ;
      iif(this.Parent.oContained.w_TDRIPINC=='S',1,;
      0)
  endfunc

  func oTDRIPINC_2_27.mHide()
    with this.Parent.oContained
      return (g_DETCON = 'S' OR IsAlt())
    endwith
  endfunc

  add object oTDRIPIMB_2_28 as StdCheck with uid="IHJVYMMOCQ",rtseq=162,rtrep=.f.,left=409, top=143, caption="Ripartisce spese imballo",;
    ToolTipText = "Ripartisce spese imballo",;
    HelpContextID = 121358472,;
    cFormVar="w_TDRIPIMB", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDRIPIMB_2_28.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDRIPIMB_2_28.GetRadio()
    this.Parent.oContained.w_TDRIPIMB = this.RadioValue()
    return .t.
  endfunc

  func oTDRIPIMB_2_28.SetRadio()
    this.Parent.oContained.w_TDRIPIMB=trim(this.Parent.oContained.w_TDRIPIMB)
    this.value = ;
      iif(this.Parent.oContained.w_TDRIPIMB=='S',1,;
      0)
  endfunc

  func oTDRIPIMB_2_28.mHide()
    with this.Parent.oContained
      return (g_DETCON = 'S' OR IsAlt())
    endwith
  endfunc

  add object oTDRIPTRA_2_29 as StdCheck with uid="DUSMVYDYWK",rtseq=163,rtrep=.f.,left=409, top=168, caption="Ripartisce spese trasporto",;
    ToolTipText = "Ripartisce spese trasporto",;
    HelpContextID = 63190903,;
    cFormVar="w_TDRIPTRA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDRIPTRA_2_29.RadioValue()
    return(iif(this.value =1,"S",;
    space(1)))
  endfunc
  func oTDRIPTRA_2_29.GetRadio()
    this.Parent.oContained.w_TDRIPTRA = this.RadioValue()
    return .t.
  endfunc

  func oTDRIPTRA_2_29.SetRadio()
    this.Parent.oContained.w_TDRIPTRA=trim(this.Parent.oContained.w_TDRIPTRA)
    this.value = ;
      iif(this.Parent.oContained.w_TDRIPTRA=="S",1,;
      0)
  endfunc

  func oTDRIPTRA_2_29.mHide()
    with this.Parent.oContained
      return (g_DETCON = 'S' OR IsAlt())
    endwith
  endfunc

  add object oTDMCALSI_2_30 as StdField with uid="UAEYKJSJKD",rtseq=164,rtrep=.f.,;
    cFormVar = "w_TDMCALSI", cQueryName = "TDMCALSI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo spese di imballo",;
    HelpContextID = 181266303,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=409, Top=193, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_TDMCALSI"

  func oTDMCALSI_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
   endif
  endfunc

  func oTDMCALSI_2_30.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oTDMCALSI_2_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDMCALSI_2_30.ecpDrop(oSource)
    this.Parent.oContained.link_2_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDMCALSI_2_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oTDMCALSI_2_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oTDMCALSI_2_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_TDMCALSI
     i_obj.ecpSave()
  endproc

  add object oTDMCALST_2_31 as StdField with uid="ZENQCFITHG",rtseq=165,rtrep=.f.,;
    cFormVar = "w_TDMCALST", cQueryName = "TDMCALST",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo spese di trasporto",;
    HelpContextID = 181266314,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=409, Top=218, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_TDMCALST"

  func oTDMCALST_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
   endif
  endfunc

  func oTDMCALST_2_31.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oTDMCALST_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDMCALST_2_31.ecpDrop(oSource)
    this.Parent.oContained.link_2_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDMCALST_2_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oTDMCALST_2_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oTDMCALST_2_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_TDMCALST
     i_obj.ecpSave()
  endproc


  add object oTDSINCFL_2_32 as StdCombo with uid="ROIYQNNVWC",value=1,rtseq=166,rtrep=.f.,left=409,top=247,width=149,height=21;
    , HelpContextID = 44320642;
    , cFormVar="w_TDSINCFL",RowSource=""+"Non gestito,"+"Positivo,"+"Negativo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTDSINCFL_2_32.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,+1,;
    iif(this.value =3,-1,;
    0))))
  endfunc
  func oTDSINCFL_2_32.GetRadio()
    this.Parent.oContained.w_TDSINCFL = this.RadioValue()
    return .t.
  endfunc

  func oTDSINCFL_2_32.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TDSINCFL==0,1,;
      iif(this.Parent.oContained.w_TDSINCFL==+1,2,;
      iif(this.Parent.oContained.w_TDSINCFL==-1,3,;
      0)))
  endfunc

  proc oTDSINCFL_2_32.mDefault
    with this.Parent.oContained
      if empty(.w_TDSINCFL)
        .w_TDSINCFL = 0
      endif
    endwith
  endproc


  add object oTDFLSCOR_2_33 as StdCombo with uid="IJUPTDTQXP",rtseq=167,rtrep=.f.,left=409,top=273,width=149,height=21;
    , ToolTipText = "Scorporo a piede fattura";
    , HelpContextID = 218728568;
    , cFormVar="w_TDFLSCOR",RowSource=""+"Da intestatario,"+"Attivo,"+"Disattivo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTDFLSCOR_2_33.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oTDFLSCOR_2_33.GetRadio()
    this.Parent.oContained.w_TDFLSCOR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSCOR_2_33.SetRadio()
    this.Parent.oContained.w_TDFLSCOR=trim(this.Parent.oContained.w_TDFLSCOR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSCOR=='I',1,;
      iif(this.Parent.oContained.w_TDFLSCOR=='S',2,;
      iif(this.Parent.oContained.w_TDFLSCOR=='N',3,;
      0)))
  endfunc

  func oTDFLSCOR_2_33.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC = 'RF' or IsAlt())
    endwith
  endfunc


  add object oTDFLRISC_2_34 as StdCombo with uid="FRVSWBXGZU",value=3,rtseq=168,rtrep=.f.,left=409,top=298,width=149,height=21;
    , ToolTipText = "Incrementa: aumenta l'esposizione del cliente e diminuisce il fido disponibile. Decrementa: diminuisce l'esposizione e aumenta il fido disponibile";
    , HelpContextID = 149321593;
    , cFormVar="w_TDFLRISC",RowSource=""+"Incrementa,"+"Decrementa,"+"Non partecipa", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTDFLRISC_2_34.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oTDFLRISC_2_34.GetRadio()
    this.Parent.oContained.w_TDFLRISC = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRISC_2_34.SetRadio()
    this.Parent.oContained.w_TDFLRISC=trim(this.Parent.oContained.w_TDFLRISC)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRISC=='S',1,;
      iif(this.Parent.oContained.w_TDFLRISC=='D',2,;
      iif(this.Parent.oContained.w_TDFLRISC=='',3,;
      0)))
  endfunc

  func oTDFLRISC_2_34.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF' Or .w_TDCATDOC='DI'  OR .w_TDFLINTE<>'C' OR .w_TDFLVEAC<>'V')
    endwith
  endfunc

  add object oTDFLCRIS_2_35 as StdCheck with uid="QYJYLBHQSR",rtseq=169,rtrep=.f.,left=409, top=327, caption="Controlla il rischio",;
    ToolTipText = "Se attivo: sul documento viene eseguito il controllo del rischio",;
    HelpContextID = 252282999,;
    cFormVar="w_TDFLCRIS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLCRIS_2_35.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLCRIS_2_35.GetRadio()
    this.Parent.oContained.w_TDFLCRIS = this.RadioValue()
    return .t.
  endfunc

  func oTDFLCRIS_2_35.SetRadio()
    this.Parent.oContained.w_TDFLCRIS=trim(this.Parent.oContained.w_TDFLCRIS)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLCRIS=='S',1,;
      0)
  endfunc

  func oTDFLCRIS_2_35.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF' Or .w_TDCATDOC='DI' OR .w_TDFLINTE<>'C' OR Not .w_TDFLRISC$'SD' OR .w_TDFLVEAC<>'V')
    endwith
  endfunc

  add object oMSDESIMB_2_38 as StdField with uid="SOBFQPXZRS",rtseq=181,rtrep=.f.,;
    cFormVar = "w_MSDESIMB", cQueryName = "MSDESIMB",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 118528504,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=464, Top=193, InputMask=replicate('X',40)

  func oMSDESIMB_2_38.mHide()
    with this.Parent.oContained
      return (NOT IsAlt() OR .w_TDFLSPIM#'S')
    endwith
  endfunc

  add object oMSDESTRA_2_40 as StdField with uid="WQZNYVOXWA",rtseq=182,rtrep=.f.,;
    cFormVar = "w_MSDESTRA", cQueryName = "MSDESTRA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 66020871,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=464, Top=218, InputMask=replicate('X',40)

  func oMSDESTRA_2_40.mHide()
    with this.Parent.oContained
      return (NOT IsAlt() OR .w_TDFLSPTR#'S')
    endwith
  endfunc

  add object oTDMCALSI_2_42 as StdField with uid="YDAXYHZDBW",rtseq=183,rtrep=.f.,;
    cFormVar = "w_TDMCALSI", cQueryName = "TDMCALSI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Spese generali",;
    HelpContextID = 181266303,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=409, Top=193, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_TDMCALSI"

  func oTDMCALSI_2_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oTDMCALSI_2_42.mHide()
    with this.Parent.oContained
      return (NOT IsAlt() OR .w_TDFLSPIM#'S')
    endwith
  endfunc

  func oTDMCALSI_2_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDMCALSI_2_42.ecpDrop(oSource)
    this.Parent.oContained.link_2_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDMCALSI_2_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oTDMCALSI_2_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oTDMCALSI_2_42.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_TDMCALSI
     i_obj.ecpSave()
  endproc

  add object oTDMCALST_2_43 as StdField with uid="RCCAXFWEDO",rtseq=184,rtrep=.f.,;
    cFormVar = "w_TDMCALST", cQueryName = "TDMCALST",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Cassa di previdenza",;
    HelpContextID = 181266314,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=409, Top=218, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_TDMCALST"

  func oTDMCALST_2_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oTDMCALST_2_43.mHide()
    with this.Parent.oContained
      return (NOT IsAlt() OR .w_TDFLSPTR#'S')
    endwith
  endfunc

  func oTDMCALST_2_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDMCALST_2_43.ecpDrop(oSource)
    this.Parent.oContained.link_2_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDMCALST_2_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oTDMCALST_2_43'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oTDMCALST_2_43.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_TDMCALST
     i_obj.ecpSave()
  endproc


  add object oTDCHKUCA_2_44 as StdCombo with uid="BUZIILUHPK",rtseq=185,rtrep=.f.,left=409,top=348,width=138,height=21;
    , ToolTipText = "Tipologia di controllo prezzo";
    , HelpContextID = 74598263;
    , cFormVar="w_TDCHKUCA",RowSource=""+"Nessuno,"+"Avviso,"+"Bloccante", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTDCHKUCA_2_44.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'A',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oTDCHKUCA_2_44.GetRadio()
    this.Parent.oContained.w_TDCHKUCA = this.RadioValue()
    return .t.
  endfunc

  func oTDCHKUCA_2_44.SetRadio()
    this.Parent.oContained.w_TDCHKUCA=trim(this.Parent.oContained.w_TDCHKUCA)
    this.value = ;
      iif(this.Parent.oContained.w_TDCHKUCA=='N',1,;
      iif(this.Parent.oContained.w_TDCHKUCA=='A',2,;
      iif(this.Parent.oContained.w_TDCHKUCA=='B',3,;
      0)))
  endfunc

  func oTDCHKUCA_2_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_TDFLVEAC='A' OR .w_TFFLGEFA='B' OR .w_TDCATDOC='NC'))
    endwith
   endif
  endfunc


  add object oBtn_2_46 as StdButton with uid="PKKSUWHQWC",left=664, top=68, width=48,height=45,;
    CpPicture="bmp\INTENTI.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere al visualizza utilizzo dichiarazioni di intento per causale";
    , HelpContextID = 92942858;
    , caption='\<Dic. Int.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_46.Click()
      do GSCG_KDZ with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_46.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( .cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_2_46.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_TDCATDOC $ 'DT-FA-NC')
     endwith
    endif
  endfunc

  add object oStr_2_1 as StdString with uid="NDHHDYJBYF",Visible=.t., Left=4, Top=25,;
    Alignment=1, Width=129, Height=15,;
    Caption="Tipo documento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_18 as StdString with uid="WUWLSDGIMS",Visible=.t., Left=320, Top=298,;
    Alignment=1, Width=83, Height=17,;
    Caption="Rischio:"  ;
  , bGlobalFont=.t.

  func oStr_2_18.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF' Or .w_TDCATDOC='DI'  OR .w_TDFLINTE<>'C' OR .w_TDFLVEAC<>'V')
    endwith
  endfunc

  add object oStr_2_22 as StdString with uid="FNDHIHBJLH",Visible=.t., Left=278, Top=193,;
    Alignment=1, Width=125, Height=18,;
    Caption="M. c. spese imballo:"  ;
  , bGlobalFont=.t.

  func oStr_2_22.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_26 as StdString with uid="KUIVQIANTJ",Visible=.t., Left=270, Top=218,;
    Alignment=1, Width=133, Height=18,;
    Caption="M. c. spese trasporto:"  ;
  , bGlobalFont=.t.

  func oStr_2_26.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_36 as StdString with uid="HFNIEPGVYG",Visible=.t., Left=315, Top=247,;
    Alignment=1, Width=88, Height=18,;
    Caption="Cash flow:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="ZLXLLXXDDI",Visible=.t., Left=275, Top=273,;
    Alignment=1, Width=128, Height=18,;
    Caption="Scorp. piede fatt.:"  ;
  , bGlobalFont=.t.

  func oStr_2_37.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC = 'RF' or IsAlt())
    endwith
  endfunc

  add object oStr_2_39 as StdString with uid="ZVPCXMCKZE",Visible=.t., Left=268, Top=193,;
    Alignment=1, Width=135, Height=18,;
    Caption="Spese generali:"  ;
  , bGlobalFont=.t.

  func oStr_2_39.mHide()
    with this.Parent.oContained
      return (NOT IsAlt() OR .w_TDFLSPIM#'S')
    endwith
  endfunc

  add object oStr_2_41 as StdString with uid="WHFWOEYSGH",Visible=.t., Left=249, Top=218,;
    Alignment=1, Width=154, Height=18,;
    Caption="Cassa previdenza:"  ;
  , bGlobalFont=.t.

  func oStr_2_41.mHide()
    with this.Parent.oContained
      return (NOT IsAlt() OR .w_TDFLSPTR#'S')
    endwith
  endfunc

  add object oStr_2_45 as StdString with uid="ZYMHZPIDAZ",Visible=.t., Left=274, Top=349,;
    Alignment=1, Width=129, Height=18,;
    Caption="Controllo prezzo:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsve_atdPag3 as StdContainer
  Width  = 721
  height = 555
  stdWidth  = 721
  stdheight = 555
  resizeXpos=272
  resizeYpos=227
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTDFLANAL_3_1 as StdCheck with uid="ZDPRWZIYVV",rtseq=109,rtrep=.f.,left=148, top=62, caption="Dati analitica",;
    ToolTipText = "Se attivo: abilita l'inserimento dei dati di analitica sul documento",;
    HelpContextID = 215381890,;
    cFormVar="w_TDFLANAL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDFLANAL_3_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLANAL_3_1.GetRadio()
    this.Parent.oContained.w_TDFLANAL = this.RadioValue()
    return .t.
  endfunc

  func oTDFLANAL_3_1.SetRadio()
    this.Parent.oContained.w_TDFLANAL=trim(this.Parent.oContained.w_TDFLANAL)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLANAL=='S',1,;
      0)
  endfunc

  func oTDFLANAL_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oTDFLANAL_3_1.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oTDFLELAN_3_2 as StdCheck with uid="SOZYCQKRKP",rtseq=110,rtrep=.f.,left=148, top=90, caption="Movimento di analitica",;
    ToolTipText = "Se attivo movimenta l'analitica",;
    HelpContextID = 186021764,;
    cFormVar="w_TDFLELAN", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDFLELAN_3_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLELAN_3_2.GetRadio()
    this.Parent.oContained.w_TDFLELAN = this.RadioValue()
    return .t.
  endfunc

  func oTDFLELAN_3_2.SetRadio()
    this.Parent.oContained.w_TDFLELAN=trim(this.Parent.oContained.w_TDFLELAN)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLELAN=='S',1,;
      0)
  endfunc

  func oTDFLELAN_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' AND .w_TDFLANAL='S')
    endwith
   endif
  endfunc

  func oTDFLELAN_3_2.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S' OR .w_TDFLANAL<>'S')
    endwith
  endfunc


  add object oTDVOCECR_3_3 as StdCombo with uid="UZLRKJSNPK",rtseq=111,rtrep=.f.,left=408,top=62,width=124,height=21;
    , ToolTipText = "Voce costo/ricavo di analitica";
    , HelpContextID = 66746248;
    , cFormVar="w_TDVOCECR",RowSource=""+"di costo,"+"di ricavo", bObbl = .t. , nPag = 3;
  , bGlobalFont=.t.


  func oTDVOCECR_3_3.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTDVOCECR_3_3.GetRadio()
    this.Parent.oContained.w_TDVOCECR = this.RadioValue()
    return .t.
  endfunc

  func oTDVOCECR_3_3.SetRadio()
    this.Parent.oContained.w_TDVOCECR=trim(this.Parent.oContained.w_TDVOCECR)
    this.value = ;
      iif(this.Parent.oContained.w_TDVOCECR=='C',1,;
      iif(this.Parent.oContained.w_TDVOCECR=='R',2,;
      0))
  endfunc

  func oTDVOCECR_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCCR='S' AND .w_TDFLANAL='S')  OR (g_COMM='S' AND .w_TDFLCOMM $ 'M-S'))
    endwith
   endif
  endfunc

  func oTDVOCECR_3_3.mHide()
    with this.Parent.oContained
      return (NOT ((g_PERCCR='S' AND .w_TDFLANAL='S') OR  (g_COMM='S' AND .w_TDFLCOMM $ 'M-S')))
    endwith
  endfunc


  add object oTD_SEGNO_3_4 as StdCombo with uid="AZHJBWLFLD",rtseq=112,rtrep=.f.,left=408,top=90,width=124,height=21;
    , ToolTipText = "Segno dare/avere per analitica";
    , HelpContextID = 165738619;
    , cFormVar="w_TD_SEGNO",RowSource=""+"Dare,"+"Avere", bObbl = .t. , nPag = 3;
  , bGlobalFont=.t.


  func oTD_SEGNO_3_4.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oTD_SEGNO_3_4.GetRadio()
    this.Parent.oContained.w_TD_SEGNO = this.RadioValue()
    return .t.
  endfunc

  func oTD_SEGNO_3_4.SetRadio()
    this.Parent.oContained.w_TD_SEGNO=trim(this.Parent.oContained.w_TD_SEGNO)
    this.value = ;
      iif(this.Parent.oContained.w_TD_SEGNO=='D',1,;
      iif(this.Parent.oContained.w_TD_SEGNO=='A',2,;
      0))
  endfunc

  func oTD_SEGNO_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' AND .w_TDFLANAL='S')
    endwith
   endif
  endfunc

  func oTD_SEGNO_3_4.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S' OR .w_TDFLANAL<>'S')
    endwith
  endfunc

  add object oTDFLPRAT_3_5 as StdCheck with uid="EWICXJQZBN",rtseq=113,rtrep=.f.,left=546, top=61, caption="Dati pratica",;
    ToolTipText = "Se attivo: all'interno del documento verr� richiesto di associare una pratica per ogni riga di prestazione",;
    HelpContextID = 29783946,;
    cFormVar="w_TDFLPRAT", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDFLPRAT_3_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDFLPRAT_3_5.GetRadio()
    this.Parent.oContained.w_TDFLPRAT = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPRAT_3_5.SetRadio()
    this.Parent.oContained.w_TDFLPRAT=trim(this.Parent.oContained.w_TDFLPRAT)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPRAT=='S',1,;
      0)
  endfunc

  func oTDFLPRAT_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLCOMM # 'S')
    endwith
   endif
  endfunc

  func oTDFLPRAT_3_5.mHide()
    with this.Parent.oContained
      return (g_Prat='N')
    endwith
  endfunc


  add object oTDASSCES_3_6 as StdCombo with uid="AUPTBSJRNS",rtseq=114,rtrep=.f.,left=148,top=137,width=138,height=21;
    , ToolTipText = "Tipo di associazione cespiti legata alla riga del documento";
    , HelpContextID = 50145161;
    , cFormVar="w_TDASSCES",RowSource=""+"Movimenti cespiti,"+"Cespiti,"+"Non gestita", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDASSCES_3_6.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'C',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oTDASSCES_3_6.GetRadio()
    this.Parent.oContained.w_TDASSCES = this.RadioValue()
    return .t.
  endfunc

  func oTDASSCES_3_6.SetRadio()
    this.Parent.oContained.w_TDASSCES=trim(this.Parent.oContained.w_TDASSCES)
    this.value = ;
      iif(this.Parent.oContained.w_TDASSCES=='M',1,;
      iif(this.Parent.oContained.w_TDASSCES=='C',2,;
      iif(this.Parent.oContained.w_TDASSCES=='N',3,;
      0)))
  endfunc

  func oTDASSCES_3_6.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  add object oTDCAUCES_3_7 as StdField with uid="HZODBXGJLU",rtseq=115,rtrep=.f.,;
    cFormVar = "w_TDCAUCES", cQueryName = "TDCAUCES",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale cespite inesistente o obsoleta",;
    ToolTipText = "Causale cespiti associata",;
    HelpContextID = 51070857,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=148, Top=167, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CESP", cZoomOnZoom="GSCE_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_TDCAUCES"

  func oTDCAUCES_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDASSCES='M')
    endwith
   endif
  endfunc

  func oTDCAUCES_3_7.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  func oTDCAUCES_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCAUCES_3_7.ecpDrop(oSource)
    this.Parent.oContained.link_3_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCAUCES_3_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CESP','*','CCCODICE',cp_AbsName(this.parent,'oTDCAUCES_3_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACC',"Causali cespiti",'GSCE_ZCA.CAU_CESP_VZM',this.parent.oContained
  endproc
  proc oTDCAUCES_3_7.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_TDCAUCES
     i_obj.ecpSave()
  endproc


  add object oTDCONASS_3_8 as StdCombo with uid="YAXMQKVVPH",rtseq=116,rtrep=.f.,left=546,top=136,width=165,height=21;
    , ToolTipText = "Tipologia documento per generazione scritture di assestamento";
    , HelpContextID = 11093897;
    , cFormVar="w_TDCONASS",RowSource=""+"Fatture da emettere,"+"Storno fat. da emettere,"+"Nessuno", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDCONASS_3_8.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oTDCONASS_3_8.GetRadio()
    this.Parent.oContained.w_TDCONASS = this.RadioValue()
    return .t.
  endfunc

  func oTDCONASS_3_8.SetRadio()
    this.Parent.oContained.w_TDCONASS=trim(this.Parent.oContained.w_TDCONASS)
    this.value = ;
      iif(this.Parent.oContained.w_TDCONASS=='F',1,;
      iif(this.Parent.oContained.w_TDCONASS=='S',2,;
      iif(this.Parent.oContained.w_TDCONASS=='N',3,;
      0)))
  endfunc

  func oTDCONASS_3_8.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC<>'DI' OR NOT IsAlt())
    endwith
  endfunc


  add object oTDCONASS_3_9 as StdCombo with uid="CAKGPEUQGK",rtseq=117,rtrep=.f.,left=546,top=136,width=165,height=21;
    , ToolTipText = "Tipologia documento per generazione scritture di assestamento";
    , HelpContextID = 11093897;
    , cFormVar="w_TDCONASS",RowSource=""+"Fatture da emettere/ricevere,"+"Storno fat. da emettere/ricevere,"+"Nessuno", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDCONASS_3_9.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oTDCONASS_3_9.GetRadio()
    this.Parent.oContained.w_TDCONASS = this.RadioValue()
    return .t.
  endfunc

  func oTDCONASS_3_9.SetRadio()
    this.Parent.oContained.w_TDCONASS=trim(this.Parent.oContained.w_TDCONASS)
    this.value = ;
      iif(this.Parent.oContained.w_TDCONASS=='F',1,;
      iif(this.Parent.oContained.w_TDCONASS=='S',2,;
      iif(this.Parent.oContained.w_TDCONASS=='N',3,;
      0)))
  endfunc

  func oTDCONASS_3_9.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC<>'DT' OR IsAlt())
    endwith
  endfunc

  add object oTDCODSTR_3_10 as StdField with uid="MJUUZHWLFP",rtseq=118,rtrep=.f.,;
    cFormVar = "w_TDCODSTR", cQueryName = "TDCODSTR",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 34162568,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=148, Top=218, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", cZoomOnZoom="GSVA_AST", oKey_1_1="STCODICE", oKey_1_2="this.w_TDCODSTR"

  func oTDCODSTR_3_10.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' or .w_TDCATDOC='RF')
    endwith
  endfunc

  func oTDCODSTR_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCODSTR_3_10.ecpDrop(oSource)
    this.Parent.oContained.link_3_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCODSTR_3_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oTDCODSTR_3_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AST',"Elenco strutture EDI",'',this.parent.oContained
  endproc
  proc oTDCODSTR_3_10.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STCODICE=this.parent.oContained.w_TDCODSTR
     i_obj.ecpSave()
  endproc

  add object oTDFLARCO_3_11 as StdCheck with uid="KFQLOJZPGU",rtseq=119,rtrep=.f.,left=148, top=268, caption="Articoli composti",;
    ToolTipText = "Se attivo: la causale gestisce l'evasione degli articoli composti (distinte, articoli kit, kit imballi)",;
    HelpContextID = 14055301,;
    cFormVar="w_TDFLARCO", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDFLARCO_3_11.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLARCO_3_11.GetRadio()
    this.Parent.oContained.w_TDFLARCO = this.RadioValue()
    return .t.
  endfunc

  func oTDFLARCO_3_11.SetRadio()
    this.Parent.oContained.w_TDFLARCO=trim(this.Parent.oContained.w_TDFLARCO)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLARCO=='S',1,;
      0)
  endfunc

  func oTDFLARCO_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_EACD='S' AND .w_TDCATDOC<>'RF')
    endwith
   endif
  endfunc

  func oTDFLARCO_3_11.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDCATDOC='RF')
    endwith
  endfunc


  add object oTDLOTDIF_3_12 as StdCombo with uid="OXDCYSDBGT",rtseq=120,rtrep=.f.,left=408,top=245,width=124,height=21;
    , ToolTipText = "Attribuzione differita lotti/matricole";
    , HelpContextID = 200681604;
    , cFormVar="w_TDLOTDIF",RowSource=""+"Immediata,"+"Alla conferma,"+"Differita", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDLOTDIF_3_12.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oTDLOTDIF_3_12.GetRadio()
    this.Parent.oContained.w_TDLOTDIF = this.RadioValue()
    return .t.
  endfunc

  func oTDLOTDIF_3_12.SetRadio()
    this.Parent.oContained.w_TDLOTDIF=trim(this.Parent.oContained.w_TDLOTDIF)
    this.value = ;
      iif(this.Parent.oContained.w_TDLOTDIF=='I',1,;
      iif(this.Parent.oContained.w_TDLOTDIF=='C',2,;
      iif(this.Parent.oContained.w_TDLOTDIF=='D',3,;
      0)))
  endfunc

  func oTDLOTDIF_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MADV='S' AND .w_TDFLARCO<>'S')
    endwith
   endif
  endfunc

  func oTDLOTDIF_3_12.mHide()
    with this.Parent.oContained
      return (g_MADV<>'S' OR .w_TDFLARCO='S' OR (empty(NVL(.w_FLCASC,' ')) AND EMPTY(NVL(.w_FLRISE, ' '))))
    endwith
  endfunc


  add object oTDTIPIMB_3_13 as StdCombo with uid="CIQWNWJPHM",rtseq=121,rtrep=.f.,left=408,top=270,width=124,height=21;
    , ToolTipText = "Gestione degli imballi a rendere e/o a perdere sui documenti. Gestione cauzioni consente l'utilizzo di tutte le tipologie di imballo.";
    , HelpContextID = 121350280;
    , cFormVar="w_TDTIPIMB",RowSource=""+"Non gestito,"+"A perdere,"+"A perdere e rendere,"+"Gestione cauzioni", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDTIPIMB_3_13.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'R',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oTDTIPIMB_3_13.GetRadio()
    this.Parent.oContained.w_TDTIPIMB = this.RadioValue()
    return .t.
  endfunc

  func oTDTIPIMB_3_13.SetRadio()
    this.Parent.oContained.w_TDTIPIMB=trim(this.Parent.oContained.w_TDTIPIMB)
    this.value = ;
      iif(this.Parent.oContained.w_TDTIPIMB=='N',1,;
      iif(this.Parent.oContained.w_TDTIPIMB=='P',2,;
      iif(this.Parent.oContained.w_TDTIPIMB=='R',3,;
      iif(this.Parent.oContained.w_TDTIPIMB=='C',4,;
      0))))
  endfunc

  func oTDTIPIMB_3_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_VEFA='S')
    endwith
   endif
  endfunc

  func oTDTIPIMB_3_13.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' Or .w_TDFLARCO <>'S' Or Empty(.w_FLCASC))
    endwith
  endfunc

  add object oTDCAUPFI_3_14 as StdField with uid="BFYWTIHBTE",rtseq=122,rtrep=.f.,;
    cFormVar = "w_TDCAUPFI", cQueryName = "TDCAUPFI",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente o obsoleta",;
    ToolTipText = "Causale documento generato associato alla testata (prodotti finiti, articolo kit, imballi resi)",;
    HelpContextID = 739199,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=148, Top=295, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_BZC", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TDCAUPFI"

  func oTDCAUPFI_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_EACD='S' AND .w_TDFLARCO='S')
    endwith
   endif
  endfunc

  func oTDCAUPFI_3_14.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  func oTDCAUPFI_3_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCAUPFI_3_14.ecpDrop(oSource)
    this.Parent.oContained.link_3_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCAUPFI_3_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTDCAUPFI_3_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_BZC',"Causali documenti",'GSVE_KAC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTDCAUPFI_3_14.mZoomOnZoom
    local i_obj
    i_obj=GSVE_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TDCAUPFI
     i_obj.ecpSave()
  endproc

  add object oTDCAUCOD_3_15 as StdField with uid="QUHJQPSILE",rtseq=123,rtrep=.f.,;
    cFormVar = "w_TDCAUCOD", cQueryName = "TDCAUCOD",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente o obsoleta",;
    ToolTipText = "Causale documento generato associato alle singole righe (componenti, articoli nel kit, imballi consegnati)",;
    HelpContextID = 217364614,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=148, Top=323, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_BZC", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TDCAUCOD"

  func oTDCAUCOD_3_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_EACD='S' AND .w_TDFLARCO='S')
    endwith
   endif
  endfunc

  func oTDCAUCOD_3_15.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  func oTDCAUCOD_3_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCAUCOD_3_15.ecpDrop(oSource)
    this.Parent.oContained.link_3_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCAUCOD_3_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTDCAUCOD_3_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_BZC',"Causali documenti",'GSVE_KAC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTDCAUCOD_3_15.mZoomOnZoom
    local i_obj
    i_obj=GSVE_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TDCAUCOD
     i_obj.ecpSave()
  endproc


  add object oTDFLEXPL_3_16 as StdCombo with uid="YYMEGJTHVS",rtseq=124,rtrep=.f.,left=148,top=351,width=385,height=21;
    , ToolTipText = "Metodo di generazione dei documenti di evasione, se da documento di origine o da esplosione distinta base";
    , HelpContextID = 118912898;
    , cFormVar="w_TDFLEXPL",RowSource=""+"Documento di origine o espl.distinta (in alternativa),"+"Solo esplosione distinta,"+"Solo documento origine", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDFLEXPL_3_16.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oTDFLEXPL_3_16.GetRadio()
    this.Parent.oContained.w_TDFLEXPL = this.RadioValue()
    return .t.
  endfunc

  func oTDFLEXPL_3_16.SetRadio()
    this.Parent.oContained.w_TDFLEXPL=trim(this.Parent.oContained.w_TDFLEXPL)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLEXPL=='S',1,;
      iif(this.Parent.oContained.w_TDFLEXPL=='N',2,;
      iif(this.Parent.oContained.w_TDFLEXPL=='D',3,;
      0)))
  endfunc

  func oTDFLEXPL_3_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_TDCAUCOD) AND g_EACD='S' AND .w_TDFLARCO='S'  And .w_TDTIPIMB='N')
    endwith
   endif
  endfunc

  func oTDFLEXPL_3_16.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc


  add object oTDVALCOM_3_17 as StdCombo with uid="QGJTJTUXVQ",rtseq=125,rtrep=.f.,left=148,top=378,width=385,height=21;
    , ToolTipText = "Metodo di valorizzazione del prodotto finito in funzione dei componenti associati";
    , HelpContextID = 226723965;
    , cFormVar="w_TDVALCOM",RowSource=""+"Documento principale,"+"Documento di evasione prodotti finiti,"+"Documento principale e documento di evasione prodotti finiti,"+"Non gestito", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDVALCOM_3_17.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'P',;
    iif(this.value =3,'E',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oTDVALCOM_3_17.GetRadio()
    this.Parent.oContained.w_TDVALCOM = this.RadioValue()
    return .t.
  endfunc

  func oTDVALCOM_3_17.SetRadio()
    this.Parent.oContained.w_TDVALCOM=trim(this.Parent.oContained.w_TDVALCOM)
    this.value = ;
      iif(this.Parent.oContained.w_TDVALCOM=='D',1,;
      iif(this.Parent.oContained.w_TDVALCOM=='P',2,;
      iif(this.Parent.oContained.w_TDVALCOM=='E',3,;
      iif(this.Parent.oContained.w_TDVALCOM=='N',4,;
      0))))
  endfunc

  func oTDVALCOM_3_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_TDCAUCOD) AND g_EACD='S' AND .w_TDFLARCO='S' And .w_TDTIPIMB='N')
    endwith
   endif
  endfunc

  func oTDVALCOM_3_17.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oTDCOSEPL_3_18 as StdCheck with uid="ZWFPWEIXGO",rtseq=126,rtrep=.f.,left=546, top=376, caption="Valorizza da esplosione",;
    ToolTipText = "Se attivo valorizza prodotto finito in base ai componenti effettivamente presenti nella maschera di esplosione componenti",;
    HelpContextID = 83445634,;
    cFormVar="w_TDCOSEPL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDCOSEPL_3_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDCOSEPL_3_18.GetRadio()
    this.Parent.oContained.w_TDCOSEPL = this.RadioValue()
    return .t.
  endfunc

  func oTDCOSEPL_3_18.SetRadio()
    this.Parent.oContained.w_TDCOSEPL=trim(this.Parent.oContained.w_TDCOSEPL)
    this.value = ;
      iif(this.Parent.oContained.w_TDCOSEPL=='S',1,;
      0)
  endfunc

  func oTDCOSEPL_3_18.mHide()
    with this.Parent.oContained
      return ((.w_TDEXPAUT = 'S' and .w_TDFLEXPL='N' ) or .w_TDVALCOM='N' or g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oTDEXPAUT_3_19 as StdCheck with uid="WWCQCSNKDW",rtseq=127,rtrep=.f.,left=148, top=404, caption="Esplosione automatica",;
    ToolTipText = "Se attivo: genera automaticamente i documenti associati ai p.finito e/o componenti",;
    HelpContextID = 13789066,;
    cFormVar="w_TDEXPAUT", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDEXPAUT_3_19.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDEXPAUT_3_19.GetRadio()
    this.Parent.oContained.w_TDEXPAUT = this.RadioValue()
    return .t.
  endfunc

  func oTDEXPAUT_3_19.SetRadio()
    this.Parent.oContained.w_TDEXPAUT=trim(this.Parent.oContained.w_TDEXPAUT)
    this.value = ;
      iif(this.Parent.oContained.w_TDEXPAUT=='S',1,;
      0)
  endfunc

  func oTDEXPAUT_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_EACD='S' AND .w_TDFLARCO='S' AND .w_TDTIPIMB='N')
    endwith
   endif
  endfunc

  func oTDEXPAUT_3_19.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oTDMAXLEV_3_20 as StdField with uid="AZJRJYNUPG",rtseq=128,rtrep=.f.,;
    cFormVar = "w_TDMAXLEV", cQueryName = "TDMAXLEV",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, numero massimo livelli di esplosione non specificato",;
    ToolTipText = "Numero massimo livelli di esplosione distinta base",;
    HelpContextID = 205252492,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=506, Top=402, cSayPict="'99'", cGetPict="'99'", tabstop=.f.

  func oTDMAXLEV_3_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_TDCAUCOD) AND g_DISB='S' AND .w_TDFLARCO='S' And .w_TDFLEXPL<>'D' And .w_TDTIPIMB='N')
    endwith
   endif
  endfunc

  func oTDMAXLEV_3_20.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  func oTDMAXLEV_3_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TDMAXLEV>0)
    endwith
    return bRes
  endfunc

  add object oDESCAUCE_3_23 as StdField with uid="SDMRNHSDUX",rtseq=129,rtrep=.f.,;
    cFormVar = "w_DESCAUCE", cQueryName = "DESCAUCE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 63850363,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=213, Top=167, InputMask=replicate('X',40)

  func oDESCAUCE_3_23.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  add object oDESPFI_3_26 as StdField with uid="NPMCXTHGFV",rtseq=130,rtrep=.f.,;
    cFormVar = "w_DESPFI", cQueryName = "DESPFI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 137054006,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=212, Top=295, InputMask=replicate('X',35)

  func oDESPFI_3_26.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oDESCOD_3_27 as StdField with uid="UUSHKENXUZ",rtseq=131,rtrep=.f.,;
    cFormVar = "w_DESCOD", cQueryName = "DESCOD",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 61753142,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=212, Top=323, InputMask=replicate('X',35)

  func oDESCOD_3_27.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc


  add object oObj_3_43 as cp_runprogram with uid="TXVYSMWUJJ",left=8, top=573, width=205,height=20,;
    caption='GSAR_BTD(AS)',;
   bGlobalFont=.t.,;
    prg="GSAR_BTD('AS')",;
    cEvent = "w_TDASSCES Changed",;
    nPag=3;
    , ToolTipText = "Verifica l'associazione cespiti (impedisce di impostare movimenti cespiti senza cau. Cont.)";
    , HelpContextID = 48933162

  add object oCODI_3_52 as StdField with uid="INONSZRMVW",rtseq=139,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 87858906,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=60, Left=148, Top=9, InputMask=replicate('X',5)

  add object oDESC_3_53 as StdField with uid="PFFOHOMMKR",rtseq=140,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 88193226,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=212, Top=9, InputMask=replicate('X',35)

  add object oDESSTRU_3_54 as StdField with uid="DIFWYFRGLM",rtseq=172,rtrep=.f.,;
    cFormVar = "w_DESSTRU", cQueryName = "DESSTRU",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione struttura EDI",;
    HelpContextID = 34490166,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=246, Top=218, InputMask=replicate('X',30)

  func oDESSTRU_3_54.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' or .w_TDCATDOC='RF')
    endwith
  endfunc

  add object oTDFLCASH_3_58 as StdCheck with uid="HCIUXGGQMV",rtseq=211,rtrep=.f.,left=372, top=448, caption="Cash flow commessa",;
    ToolTipText = "Se attivo: il documento concorre alla elaborazione del cash flow di commessa",;
    HelpContextID = 267810686,;
    cFormVar="w_TDFLCASH", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDFLCASH_3_58.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLCASH_3_58.GetRadio()
    this.Parent.oContained.w_TDFLCASH = this.RadioValue()
    return .t.
  endfunc

  func oTDFLCASH_3_58.SetRadio()
    this.Parent.oContained.w_TDFLCASH=trim(this.Parent.oContained.w_TDFLCASH)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLCASH=='S',1,;
      0)
  endfunc

  func oTDFLCASH_3_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S' AND .w_TDFLCOMM $ 'M-S')
    endwith
   endif
  endfunc

  func oTDFLCASH_3_58.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oTDFLNORC_3_59 as StdCheck with uid="YCRMLZIYWL",rtseq=212,rtrep=.f.,left=546, top=448, caption="No ricalcolo commessa",;
    ToolTipText = "Se attivo in fase di import documenti per le righe con commessa vuota non verr� valorizzata con quella specificata nella riga precedente",;
    HelpContextID = 245790585,;
    cFormVar="w_TDFLNORC", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDFLNORC_3_59.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLNORC_3_59.GetRadio()
    this.Parent.oContained.w_TDFLNORC = this.RadioValue()
    return .t.
  endfunc

  func oTDFLNORC_3_59.SetRadio()
    this.Parent.oContained.w_TDFLNORC=trim(this.Parent.oContained.w_TDFLNORC)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLNORC=='S',1,;
      0)
  endfunc


  add object oTDFLCOMM_3_60 as StdCombo with uid="TMQJASEQLU",value=3,rtseq=213,rtrep=.f.,left=148,top=448,width=203,height=22;
    , ToolTipText = "Se attivo: il documento abilita l'input delle commesse relativo alla gestione progetti";
    , HelpContextID = 34179197;
    , cFormVar="w_TDFLCOMM",RowSource=""+"Si (sempre obbligatoria),"+"Si (obbligatoriet� da cau. mag.),"+"No (non gestita)", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDFLCOMM_3_60.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    iif(this.value =3,' ',;
    ' '))))
  endfunc
  func oTDFLCOMM_3_60.GetRadio()
    this.Parent.oContained.w_TDFLCOMM = this.RadioValue()
    return .t.
  endfunc

  func oTDFLCOMM_3_60.SetRadio()
    this.Parent.oContained.w_TDFLCOMM=trim(this.Parent.oContained.w_TDFLCOMM)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLCOMM=='S',1,;
      iif(this.Parent.oContained.w_TDFLCOMM=='M',2,;
      iif(this.Parent.oContained.w_TDFLCOMM=='',3,;
      0)))
  endfunc

  func oTDFLCOMM_3_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S' AND .w_TDFLPRAT<>'S')
    endwith
   endif
  endfunc

  func oTDFLCOMM_3_60.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' OR .w_TDFLPRAT='S')
    endwith
  endfunc

  add object oTDTIPDFE_3_64 as StdField with uid="JFTGSMLRCV",rtseq=216,rtrep=.f.,;
    cFormVar = "w_TDTIPDFE", cQueryName = "TDTIPDFE",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia documento per fatturazione elettronica",;
    HelpContextID = 63199099,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=148, Top=505, InputMask=replicate('X',4)

  func oTDTIPDFE_3_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC')
    endwith
   endif
  endfunc

  add object oTDDESEST_3_65 as StdField with uid="JLPPLMXXMZ",rtseq=217,rtrep=.f.,;
    cFormVar = "w_TDDESEST", cQueryName = "TDDESEST",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione esterna relativa alla tipologia documento per fatturazione elettronica",;
    HelpContextID = 82794378,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=408, Top=505, InputMask=replicate('X',35)

  func oTDDESEST_3_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC')
    endwith
   endif
  endfunc

  add object oTDCODCLA_3_66 as StdField with uid="JRFZLXFZCR",rtseq=218,rtrep=.f.,;
    cFormVar = "w_TDCODCLA", cQueryName = "TDCODCLA",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice di classificazione per fatturazione elettronica",;
    HelpContextID = 234272905,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=148, Top=531, InputMask=replicate('X',5)

  func oTDCODCLA_3_66.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC')
    endwith
   endif
  endfunc

  add object oStr_3_21 as StdString with uid="SAYUCPTWZQ",Visible=.t., Left=38, Top=112,;
    Alignment=0, Width=254, Height=18,;
    Caption="Gestione cespiti ammortizzabili"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_21.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  add object oStr_3_22 as StdString with uid="DNGYJZDSJC",Visible=.t., Left=9, Top=168,;
    Alignment=1, Width=137, Height=18,;
    Caption="Causale cespite:"  ;
  , bGlobalFont=.t.

  func oStr_3_22.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  add object oStr_3_25 as StdString with uid="LRVOLPSNRI",Visible=.t., Left=38, Top=39,;
    Alignment=0, Width=179, Height=18,;
    Caption="Contabilit� analitica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_25.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' OR g_COMM='S'))
    endwith
  endfunc

  add object oStr_3_28 as StdString with uid="LWFVFVXLHA",Visible=.t., Left=9, Top=351,;
    Alignment=1, Width=137, Height=18,;
    Caption="Tipo evasione:"  ;
  , bGlobalFont=.t.

  func oStr_3_28.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oStr_3_31 as StdString with uid="DAWCHWTZTM",Visible=.t., Left=315, Top=62,;
    Alignment=1, Width=91, Height=18,;
    Caption="Tipo voce:"  ;
  , bGlobalFont=.t.

  func oStr_3_31.mHide()
    with this.Parent.oContained
      return (NOT ((g_PERCCR='S' AND .w_TDFLANAL='S') OR  (g_COMM='S' AND .w_TDFLCOMM $ 'M-S')))
    endwith
  endfunc

  add object oStr_3_32 as StdString with uid="KJGEGFHYRZ",Visible=.t., Left=345, Top=90,;
    Alignment=1, Width=61, Height=18,;
    Caption="Segno:"  ;
  , bGlobalFont=.t.

  func oStr_3_32.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S' OR .w_TDFLANAL<>'S')
    endwith
  endfunc

  add object oStr_3_33 as StdString with uid="NNFXRSGGQW",Visible=.t., Left=9, Top=137,;
    Alignment=1, Width=137, Height=18,;
    Caption="Associazione cespite:"  ;
  , bGlobalFont=.t.

  func oStr_3_33.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  add object oStr_3_34 as StdString with uid="GXIHGBSOSN",Visible=.t., Left=9, Top=378,;
    Alignment=1, Width=137, Height=18,;
    Caption="Valorizza componenti:"  ;
  , bGlobalFont=.t.

  func oStr_3_34.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oStr_3_35 as StdString with uid="LMDQPTIWZX",Visible=.t., Left=376, Top=404,;
    Alignment=1, Width=129, Height=18,;
    Caption="Livello massimo:"  ;
  , bGlobalFont=.t.

  func oStr_3_35.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oStr_3_36 as StdString with uid="OKEKEGFNAB",Visible=.t., Left=231, Top=245,;
    Alignment=1, Width=175, Height=18,;
    Caption="Imputazione lotti/matricole:"  ;
  , bGlobalFont=.t.

  func oStr_3_36.mHide()
    with this.Parent.oContained
      return (g_MADV<>'S' OR .w_TDFLARCO='S' OR (empty(NVL(.w_FLCASC,' ')) AND EMPTY(NVL(.w_FLRISE, ' '))))
    endwith
  endfunc

  add object oStr_3_37 as StdString with uid="HXSVBAPVKE",Visible=.t., Left=538, Top=112,;
    Alignment=0, Width=159, Height=18,;
    Caption="Scritture di assestamento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_37.mHide()
    with this.Parent.oContained
      return ((.w_TDCATDOC<>'DT' OR IsAlt()) AND (.w_TDCATDOC<>'DI' OR NOT IsAlt()))
    endwith
  endfunc

  add object oStr_3_38 as StdString with uid="FMKVGKYAZB",Visible=.t., Left=456, Top=136,;
    Alignment=1, Width=85, Height=18,;
    Caption="Contabilizza:"  ;
  , bGlobalFont=.t.

  func oStr_3_38.mHide()
    with this.Parent.oContained
      return ((.w_TDCATDOC<>'DT' OR IsAlt()) AND (.w_TDCATDOC<>'DI' OR NOT IsAlt()))
    endwith
  endfunc

  add object oStr_3_39 as StdString with uid="WVPMVKPYEO",Visible=.t., Left=278, Top=270,;
    Alignment=1, Width=128, Height=18,;
    Caption="Tipologia imballi:"  ;
  , bGlobalFont=.t.

  func oStr_3_39.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' Or .w_TDFLARCO <>'S' Or Empty(.w_FLCASC))
    endwith
  endfunc

  add object oStr_3_40 as StdString with uid="NPDUQUYLYJ",Visible=.t., Left=9, Top=218,;
    Alignment=1, Width=137, Height=18,;
    Caption="Struttura EDI:"  ;
  , bGlobalFont=.t.

  func oStr_3_40.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' or .w_TDCATDOC='RF')
    endwith
  endfunc

  add object oStr_3_41 as StdString with uid="UVFFVFRBGT",Visible=.t., Left=9, Top=324,;
    Alignment=1, Width=137, Height=18,;
    Caption="Documento di riga:"  ;
  , bGlobalFont=.t.

  func oStr_3_41.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oStr_3_42 as StdString with uid="AXDNERUTJM",Visible=.t., Left=6, Top=296,;
    Alignment=1, Width=140, Height=18,;
    Caption="Documento di testata:"  ;
  , bGlobalFont=.t.

  func oStr_3_42.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oStr_3_51 as StdString with uid="OBGCGUNCMG",Visible=.t., Left=4, Top=9,;
    Alignment=1, Width=141, Height=15,;
    Caption="Tipo documento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_55 as StdString with uid="CAUMIQITUP",Visible=.t., Left=38, Top=193,;
    Alignment=0, Width=381, Height=19,;
    Caption="Magazzino produzione - Vendite funzioni avanzate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_55.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S')
    endwith
  endfunc

  add object oStr_3_56 as StdString with uid="AXXCBVAOTM",Visible=.t., Left=558, Top=39,;
    Alignment=0, Width=124, Height=18,;
    Caption="Gestione pratiche"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_56.mHide()
    with this.Parent.oContained
      return (g_Prat='N')
    endwith
  endfunc

  add object oStr_3_57 as StdString with uid="YPPXSYPMKJ",Visible=.t., Left=38, Top=426,;
    Alignment=0, Width=141, Height=19,;
    Caption="Gestione progetti"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_57.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oStr_3_62 as StdString with uid="ECONLEUPKX",Visible=.t., Left=40, Top=450,;
    Alignment=1, Width=106, Height=18,;
    Caption="Gestione progetti:"  ;
  , bGlobalFont=.t.

  func oStr_3_62.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' OR .w_TDFLPRAT='S')
    endwith
  endfunc

  add object oStr_3_63 as StdString with uid="XQRKIKRCPA",Visible=.t., Left=21, Top=481,;
    Alignment=0, Width=257, Height=18,;
    Caption="Informazioni per fatturazione elettronica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_67 as StdString with uid="ALGPCPZQRH",Visible=.t., Left=47, Top=507,;
    Alignment=1, Width=99, Height=18,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_68 as StdString with uid="ZUYFDBCYJI",Visible=.t., Left=285, Top=507,;
    Alignment=1, Width=121, Height=18,;
    Caption="Descrizione esterna:"  ;
  , bGlobalFont=.t.

  add object oStr_3_69 as StdString with uid="BRQAAFGCCM",Visible=.t., Left=33, Top=531,;
    Alignment=1, Width=113, Height=18,;
    Caption="Classificazione FE:"  ;
  , bGlobalFont=.t.

  add object oBox_3_24 as StdBox with uid="CJDTNRGGDX",left=25, top=57, width=672,height=1

  add object oBox_3_29 as StdBox with uid="DQATRRMWVH",left=25, top=130, width=672,height=1

  add object oBox_3_30 as StdBox with uid="YWAYZGVPXX",left=25, top=213, width=672,height=1

  add object oBox_3_61 as StdBox with uid="WOEXISLZJK",left=25, top=444, width=672,height=2

  add object oBox_3_70 as StdBox with uid="EOSUPFRRUG",left=8, top=499, width=671,height=1
enddefine
define class tgsve_atdPag4 as StdContainer
  Width  = 721
  height = 555
  stdWidth  = 721
  stdheight = 555
  resizeXpos=516
  resizeYpos=184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_4_2 as StdField with uid="TTJYFJGKZV",rtseq=84,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 87858906,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=60, Left=129, Top=6, InputMask=replicate('X',5)

  add object oTDDESRIF_4_3 as StdField with uid="XSEFJWMUSP",rtseq=85,rtrep=.f.,;
    cFormVar = "w_TDDESRIF", cQueryName = "TDDESRIF",;
    bObbl = .f. , nPag = 4, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Dicitura di riferimento nei documenti generati/importati",;
    HelpContextID = 235972740,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=129, Top=32, InputMask=replicate('X',18)

  add object oTDMODRIF_4_4 as StdField with uid="YBPOCHOBPT",rtseq=86,rtrep=.f.,;
    cFormVar = "w_TDMODRIF", cQueryName = "TDMODRIF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Modello dei riferimenti in lingua dei documenti di destinazione",;
    HelpContextID = 251009156,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=129, Top=58, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MODMRIFE", cZoomOnZoom="GSAR_MRR", oKey_1_1="MDCODICE", oKey_1_2="this.w_TDMODRIF"

  func oTDMODRIF_4_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDMODRIF_4_4.ecpDrop(oSource)
    this.Parent.oContained.link_4_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDMODRIF_4_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODMRIFE','*','MDCODICE',cp_AbsName(this.parent,'oTDMODRIF_4_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MRR',"Modelli riferimenti",'',this.parent.oContained
  endproc
  proc oTDMODRIF_4_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MRR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_TDMODRIF
     i_obj.ecpSave()
  endproc

  add object oDESC_4_5 as StdField with uid="TWMLMNTAWI",rtseq=87,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 88193226,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=193, Top=6, InputMask=replicate('X',35)


  add object oLinkPC_4_7 as stdDynamicChildContainer with uid="BYZKFFFDZE",left=-1, top=99, width=716, height=176, bOnScreen=.t.;


  add object oTDFLNSRI_4_8 as StdCheck with uid="XXRUOSIWXL",rtseq=88,rtrep=.f.,left=19, top=306, caption="Nostro riferimento",;
    ToolTipText = "Se attivo: durante l'import genera la riga riferimento del documento di origine",;
    HelpContextID = 44463999,;
    cFormVar="w_TDFLNSRI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLNSRI_4_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLNSRI_4_8.GetRadio()
    this.Parent.oContained.w_TDFLNSRI = this.RadioValue()
    return .t.
  endfunc

  func oTDFLNSRI_4_8.SetRadio()
    this.Parent.oContained.w_TDFLNSRI=trim(this.Parent.oContained.w_TDFLNSRI)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLNSRI=='S',1,;
      0)
  endfunc

  add object oTDTPNDOC_4_9 as StdField with uid="TDTOVJVHMM",rtseq=89,rtrep=.f.,;
    cFormVar = "w_TDTPNDOC", cQueryName = "TDTPNDOC",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia assegnata alla riga nostro riferimento",;
    HelpContextID = 206874759,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=265, Top=306, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDTPNDOC"

  func oTDTPNDOC_4_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLNSRI='S')
    endwith
   endif
  endfunc

  func oTDTPNDOC_4_9.mHide()
    with this.Parent.oContained
      return (.w_TDFLNSRI<>'S')
    endwith
  endfunc

  func oTDTPNDOC_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDTPNDOC_4_9.ecpDrop(oSource)
    this.Parent.oContained.link_4_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDTPNDOC_4_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDTPNDOC_4_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDTPNDOC_4_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDTPNDOC
     i_obj.ecpSave()
  endproc

  add object oTDFLVSRI_4_10 as StdCheck with uid="KXUNSESXMA",rtseq=90,rtrep=.f.,left=19, top=326, caption="Vostro riferimento",;
    ToolTipText = "Se attivo: durante l'import genera la riga vostro riferimento del documento di origine",;
    HelpContextID = 52852607,;
    cFormVar="w_TDFLVSRI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLVSRI_4_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLVSRI_4_10.GetRadio()
    this.Parent.oContained.w_TDFLVSRI = this.RadioValue()
    return .t.
  endfunc

  func oTDFLVSRI_4_10.SetRadio()
    this.Parent.oContained.w_TDFLVSRI=trim(this.Parent.oContained.w_TDFLVSRI)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLVSRI=='S',1,;
      0)
  endfunc

  add object oTDTPVDOC_4_11 as StdField with uid="XWEIBZYMMR",rtseq=91,rtrep=.f.,;
    cFormVar = "w_TDTPVDOC", cQueryName = "TDTPVDOC",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia assegnata alla riga vostro riferimento",;
    HelpContextID = 198486151,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=265, Top=326, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDTPVDOC"

  func oTDTPVDOC_4_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLVSRI='S')
    endwith
   endif
  endfunc

  func oTDTPVDOC_4_11.mHide()
    with this.Parent.oContained
      return (.w_TDFLVSRI<>'S')
    endwith
  endfunc

  func oTDTPVDOC_4_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDTPVDOC_4_11.ecpDrop(oSource)
    this.Parent.oContained.link_4_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDTPVDOC_4_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDTPVDOC_4_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDTPVDOC_4_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDTPVDOC
     i_obj.ecpSave()
  endproc

  add object oTDFLRIDE_4_12 as StdCheck with uid="GSNENOGBJF",rtseq=92,rtrep=.f.,left=19, top=346, caption="Rif. descrittivo",;
    ToolTipText = "Se attivo: durante l'import genera la riga rif. descrittivo",;
    HelpContextID = 149321595,;
    cFormVar="w_TDFLRIDE", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLRIDE_4_12.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLRIDE_4_12.GetRadio()
    this.Parent.oContained.w_TDFLRIDE = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRIDE_4_12.SetRadio()
    this.Parent.oContained.w_TDFLRIDE=trim(this.Parent.oContained.w_TDFLRIDE)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRIDE=='S',1,;
      0)
  endfunc

  add object oTDTPRDES_4_13 as StdField with uid="CCBHYIYPEC",rtseq=93,rtrep=.f.,;
    cFormVar = "w_TDTPRDES", cQueryName = "TDTPRDES",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia assegnata alla riga rif. descrittivo",;
    HelpContextID = 65755017,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=265, Top=346, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDTPRDES"

  func oTDTPRDES_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLRIDE='S')
    endwith
   endif
  endfunc

  func oTDTPRDES_4_13.mHide()
    with this.Parent.oContained
      return (.w_TDFLRIDE<>'S')
    endwith
  endfunc

  func oTDTPRDES_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDTPRDES_4_13.ecpDrop(oSource)
    this.Parent.oContained.link_4_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDTPRDES_4_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDTPRDES_4_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDTPRDES_4_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDTPRDES
     i_obj.ecpSave()
  endproc

  add object oTDESCCL1_4_17 as StdField with uid="YRWYUXPMDR",rtseq=94,rtrep=.f.,;
    cFormVar = "w_TDESCCL1", cQueryName = "TDESCCL1",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non importare sui documenti generati",;
    HelpContextID = 235051161,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=441, Top=306, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDESCCL1"

  func oTDESCCL1_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDESCCL1_4_17.ecpDrop(oSource)
    this.Parent.oContained.link_4_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDESCCL1_4_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDESCCL1_4_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDESCCL1_4_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDESCCL1
     i_obj.ecpSave()
  endproc

  add object oTDESCCL2_4_18 as StdField with uid="OIXQSORJBD",rtseq=95,rtrep=.f.,;
    cFormVar = "w_TDESCCL2", cQueryName = "TDESCCL2",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non importare sui documenti generati",;
    HelpContextID = 235051160,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=486, Top=306, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDESCCL2"

  func oTDESCCL2_4_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDESCCL2_4_18.ecpDrop(oSource)
    this.Parent.oContained.link_4_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDESCCL2_4_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDESCCL2_4_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDESCCL2_4_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDESCCL2
     i_obj.ecpSave()
  endproc

  add object oTDESCCL3_4_19 as StdField with uid="TDDRKSBZPX",rtseq=96,rtrep=.f.,;
    cFormVar = "w_TDESCCL3", cQueryName = "TDESCCL3",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non importare sui documenti generati",;
    HelpContextID = 235051159,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=531, Top=306, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDESCCL3"

  func oTDESCCL3_4_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDESCCL3_4_19.ecpDrop(oSource)
    this.Parent.oContained.link_4_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDESCCL3_4_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDESCCL3_4_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDESCCL3_4_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDESCCL3
     i_obj.ecpSave()
  endproc

  add object oTDESCCL4_4_20 as StdField with uid="PFTSPHWWAI",rtseq=97,rtrep=.f.,;
    cFormVar = "w_TDESCCL4", cQueryName = "TDESCCL4",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non importare sui documenti generati",;
    HelpContextID = 235051158,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=576, Top=306, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDESCCL4"

  func oTDESCCL4_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDESCCL4_4_20.ecpDrop(oSource)
    this.Parent.oContained.link_4_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDESCCL4_4_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDESCCL4_4_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDESCCL4_4_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDESCCL4
     i_obj.ecpSave()
  endproc

  add object oTDESCCL5_4_21 as StdField with uid="ZBXGRCEKPC",rtseq=98,rtrep=.f.,;
    cFormVar = "w_TDESCCL5", cQueryName = "TDESCCL5",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non importare sui documenti generati",;
    HelpContextID = 235051157,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=621, Top=306, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDESCCL5"

  func oTDESCCL5_4_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDESCCL5_4_21.ecpDrop(oSource)
    this.Parent.oContained.link_4_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDESCCL5_4_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDESCCL5_4_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDESCCL5_4_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDESCCL5
     i_obj.ecpSave()
  endproc

  add object oTDSTACL1_4_22 as StdField with uid="KDWKYAOGCD",rtseq=99,rtrep=.f.,;
    cFormVar = "w_TDSTACL1", cQueryName = "TDSTACL1",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non stampare sui documenti generati",;
    HelpContextID = 237025433,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=441, Top=332, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDSTACL1"

  func oTDSTACL1_4_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDSTACL1_4_22.ecpDrop(oSource)
    this.Parent.oContained.link_4_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDSTACL1_4_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDSTACL1_4_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDSTACL1_4_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDSTACL1
     i_obj.ecpSave()
  endproc

  add object oTDSTACL2_4_23 as StdField with uid="LCQZAHLAKB",rtseq=100,rtrep=.f.,;
    cFormVar = "w_TDSTACL2", cQueryName = "TDSTACL2",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non stampare sui documenti generati",;
    HelpContextID = 237025432,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=486, Top=332, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDSTACL2"

  func oTDSTACL2_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDSTACL2_4_23.ecpDrop(oSource)
    this.Parent.oContained.link_4_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDSTACL2_4_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDSTACL2_4_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDSTACL2_4_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDSTACL2
     i_obj.ecpSave()
  endproc

  add object oTDSTACL3_4_24 as StdField with uid="ULBMSODDKY",rtseq=101,rtrep=.f.,;
    cFormVar = "w_TDSTACL3", cQueryName = "TDSTACL3",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non stampare sui documenti generati",;
    HelpContextID = 237025431,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=531, Top=332, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDSTACL3"

  func oTDSTACL3_4_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDSTACL3_4_24.ecpDrop(oSource)
    this.Parent.oContained.link_4_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDSTACL3_4_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDSTACL3_4_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDSTACL3_4_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDSTACL3
     i_obj.ecpSave()
  endproc

  add object oTDSTACL4_4_25 as StdField with uid="PZWLFDLNRC",rtseq=102,rtrep=.f.,;
    cFormVar = "w_TDSTACL4", cQueryName = "TDSTACL4",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non stampare sui documenti generati",;
    HelpContextID = 237025430,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=576, Top=332, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDSTACL4"

  func oTDSTACL4_4_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDSTACL4_4_25.ecpDrop(oSource)
    this.Parent.oContained.link_4_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDSTACL4_4_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDSTACL4_4_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDSTACL4_4_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDSTACL4
     i_obj.ecpSave()
  endproc

  add object oTDSTACL5_4_26 as StdField with uid="PVMNNMGPUP",rtseq=103,rtrep=.f.,;
    cFormVar = "w_TDSTACL5", cQueryName = "TDSTACL5",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non stampare sui documenti generati",;
    HelpContextID = 237025429,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=621, Top=332, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDSTACL5"

  func oTDSTACL5_4_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDSTACL5_4_26.ecpDrop(oSource)
    this.Parent.oContained.link_4_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDSTACL5_4_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDSTACL5_4_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDSTACL5_4_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDSTACL5
     i_obj.ecpSave()
  endproc

  add object oDESMOD_4_36 as StdField with uid="HRKBUYDRNQ",rtseq=104,rtrep=.f.,;
    cFormVar = "w_DESMOD", cQueryName = "DESMOD",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 62408502,;
   bGlobalFont=.t.,;
    Height=21, Width=277, Left=205, Top=58, InputMask=replicate('X',35)

  add object oTDFLIMPA_4_37 as StdCheck with uid="DVVKKDMDAC",rtseq=105,rtrep=.f.,left=19, top=395, caption="Controllo dati pagamento",;
    ToolTipText = "Se attivo, effettua il controllo di congruenza dei dati di pagamento (in import documenti)",;
    HelpContextID = 206993271,;
    cFormVar="w_TDFLIMPA", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIMPA_4_37.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLIMPA_4_37.GetRadio()
    this.Parent.oContained.w_TDFLIMPA = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIMPA_4_37.SetRadio()
    this.Parent.oContained.w_TDFLIMPA=trim(this.Parent.oContained.w_TDFLIMPA)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIMPA=='S',1,;
      0)
  endfunc

  func oTDFLIMPA_4_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC<>'RF')
    endwith
   endif
  endfunc

  func oTDFLIMPA_4_37.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF')
    endwith
  endfunc

  add object oTDFLIMAC_4_38 as StdCheck with uid="QHZCBHDHNX",rtseq=106,rtrep=.f.,left=19, top=415, caption="Controllo dati accompagnatori",;
    ToolTipText = "Se attivo, effettua il controllo di congruenza dei dati accompagnatori (in import documenti)",;
    HelpContextID = 206993273,;
    cFormVar="w_TDFLIMAC", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIMAC_4_38.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLIMAC_4_38.GetRadio()
    this.Parent.oContained.w_TDFLIMAC = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIMAC_4_38.SetRadio()
    this.Parent.oContained.w_TDFLIMAC=trim(this.Parent.oContained.w_TDFLIMAC)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIMAC=='S',1,;
      0)
  endfunc

  func oTDFLIMAC_4_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC<>'RF')
    endwith
   endif
  endfunc

  func oTDFLIMAC_4_38.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF'  or IsAlt())
    endwith
  endfunc

  add object oTDFLIA01_4_49 as StdCheck with uid="ZGQSFXDDMD",rtseq=194,rtrep=.t.,left=540, top=392, caption=" ",;
    ToolTipText = "Se attivo: il campo aggiuntivo sar� importato",;
    HelpContextID = 5666663,;
    cFormVar="w_TDFLIA01", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIA01_4_49.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLIA01_4_49.GetRadio()
    this.Parent.oContained.w_TDFLIA01 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIA01_4_49.SetRadio()
    this.Parent.oContained.w_TDFLIA01=trim(this.Parent.oContained.w_TDFLIA01)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIA01=='S',1,;
      0)
  endfunc

  add object oTDFLIA02_4_50 as StdCheck with uid="RYRSHVGIYH",rtseq=195,rtrep=.t.,left=540, top=417, caption=" ",;
    ToolTipText = "Se attivo: il campo aggiuntivo sar� importato",;
    HelpContextID = 5666664,;
    cFormVar="w_TDFLIA02", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIA02_4_50.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLIA02_4_50.GetRadio()
    this.Parent.oContained.w_TDFLIA02 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIA02_4_50.SetRadio()
    this.Parent.oContained.w_TDFLIA02=trim(this.Parent.oContained.w_TDFLIA02)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIA02=='S',1,;
      0)
  endfunc

  add object oTDFLIA03_4_51 as StdCheck with uid="CFAQBEEMTJ",rtseq=196,rtrep=.t.,left=540, top=440, caption=" ",;
    ToolTipText = "Se attivo: il campo aggiuntivo sar� importato",;
    HelpContextID = 5666665,;
    cFormVar="w_TDFLIA03", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIA03_4_51.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLIA03_4_51.GetRadio()
    this.Parent.oContained.w_TDFLIA03 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIA03_4_51.SetRadio()
    this.Parent.oContained.w_TDFLIA03=trim(this.Parent.oContained.w_TDFLIA03)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIA03=='S',1,;
      0)
  endfunc

  add object oTDFLIA04_4_52 as StdCheck with uid="QPBXOWCELL",rtseq=197,rtrep=.t.,left=540, top=463, caption=" ",;
    ToolTipText = "Se attivo: il campo aggiuntivo sar� importato",;
    HelpContextID = 5666666,;
    cFormVar="w_TDFLIA04", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIA04_4_52.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLIA04_4_52.GetRadio()
    this.Parent.oContained.w_TDFLIA04 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIA04_4_52.SetRadio()
    this.Parent.oContained.w_TDFLIA04=trim(this.Parent.oContained.w_TDFLIA04)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIA04=='S',1,;
      0)
  endfunc

  add object oTDFLIA05_4_53 as StdCheck with uid="JUACTYYBAI",rtseq=198,rtrep=.t.,left=540, top=486, caption=" ",;
    ToolTipText = "Se attivo: il campo aggiuntivo sar� importato",;
    HelpContextID = 5666667,;
    cFormVar="w_TDFLIA05", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIA05_4_53.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLIA05_4_53.GetRadio()
    this.Parent.oContained.w_TDFLIA05 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIA05_4_53.SetRadio()
    this.Parent.oContained.w_TDFLIA05=trim(this.Parent.oContained.w_TDFLIA05)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIA05=='S',1,;
      0)
  endfunc

  add object oTDFLIA06_4_54 as StdCheck with uid="UFQFMTVLPF",rtseq=199,rtrep=.t.,left=540, top=509, caption=" ",;
    ToolTipText = "Se attivo: il campo aggiuntivo sar� importato",;
    HelpContextID = 5666668,;
    cFormVar="w_TDFLIA06", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIA06_4_54.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLIA06_4_54.GetRadio()
    this.Parent.oContained.w_TDFLIA06 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIA06_4_54.SetRadio()
    this.Parent.oContained.w_TDFLIA06=trim(this.Parent.oContained.w_TDFLIA06)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIA06=='S',1,;
      0)
  endfunc


  add object oTDFLRA01_4_56 as StdCombo with uid="LTJVJLANZZ",rtseq=200,rtrep=.t.,left=591,top=394,width=118,height=21;
    , ToolTipText = "Imposta la modalit� di rottura dei documenti (Nessuna rottura, su dati differenti, anche in presenza di valori vuoti nel campo)";
    , HelpContextID = 15103847;
    , cFormVar="w_TDFLRA01",RowSource=""+"No,"+"Si,"+"Tassativa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTDFLRA01_4_56.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDFLRA01_4_56.GetRadio()
    this.Parent.oContained.w_TDFLRA01 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRA01_4_56.SetRadio()
    this.Parent.oContained.w_TDFLRA01=trim(this.Parent.oContained.w_TDFLRA01)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRA01=='N',1,;
      iif(this.Parent.oContained.w_TDFLRA01=='S',2,;
      iif(this.Parent.oContained.w_TDFLRA01=='A',3,;
      0)))
  endfunc

  func oTDFLRA01_4_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLIA01='S')
    endwith
   endif
  endfunc


  add object CAMAGG01 as cp_calclbl with uid="JZCKXLHDHV",left=334, top=393, width=202,height=20,;
    caption='Campo aggiuntivo 1',;
   bGlobalFont=.t.,;
    caption="Campo 1",Alignment =1,;
    nPag=4;
    , HelpContextID = 225249684


  add object CAMAGG02 as cp_calclbl with uid="YBFIFIXNYF",left=334, top=418, width=202,height=20,;
    caption='Campo aggiuntivo 2',;
   bGlobalFont=.t.,;
    caption="Campo 2",Alignment =1,;
    nPag=4;
    , HelpContextID = 225249428


  add object CAMAGG03 as cp_calclbl with uid="EELMGPXNWQ",left=334, top=441, width=202,height=20,;
    caption='Campo aggiuntivo 3',;
   bGlobalFont=.t.,;
    caption="Campo 3",alignment =1,;
    nPag=4;
    , HelpContextID = 225249172


  add object CAMAGG04 as cp_calclbl with uid="WWUGRPNAOG",left=334, top=464, width=202,height=20,;
    caption='Campo aggiuntivo 4',;
   bGlobalFont=.t.,;
    caption="Campo 4",alignment =1,;
    nPag=4;
    , HelpContextID = 225248916


  add object CAMAGG05 as cp_calclbl with uid="BLEZPMMBEJ",left=334, top=487, width=202,height=20,;
    caption='Campo aggiuntivo 5',;
   bGlobalFont=.t.,;
    caption="Campo 5",alignment =1,;
    nPag=4;
    , HelpContextID = 225248660


  add object CAMAGG06 as cp_calclbl with uid="ORVYGKKIKW",left=334, top=510, width=202,height=20,;
    caption='Campo aggiuntivo 6',;
   bGlobalFont=.t.,;
    caption="Campo 6",alignment =1,;
    nPag=4;
    , HelpContextID = 225248404


  add object oTDFLRA02_4_67 as StdCombo with uid="FIIEIQYREQ",rtseq=201,rtrep=.t.,left=591,top=417,width=118,height=21;
    , ToolTipText = "Imposta la modalit� di rottura dei documenti (Nessuna rottura, su dati differenti, anche in presenza di valori vuoti nel campo)";
    , HelpContextID = 15103848;
    , cFormVar="w_TDFLRA02",RowSource=""+"No,"+"Si,"+"Tassativa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTDFLRA02_4_67.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDFLRA02_4_67.GetRadio()
    this.Parent.oContained.w_TDFLRA02 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRA02_4_67.SetRadio()
    this.Parent.oContained.w_TDFLRA02=trim(this.Parent.oContained.w_TDFLRA02)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRA02=='N',1,;
      iif(this.Parent.oContained.w_TDFLRA02=='S',2,;
      iif(this.Parent.oContained.w_TDFLRA02=='A',3,;
      0)))
  endfunc

  func oTDFLRA02_4_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLIA02='S')
    endwith
   endif
  endfunc


  add object oTDFLRA03_4_68 as StdCombo with uid="HSGFALZDKG",rtseq=202,rtrep=.t.,left=591,top=440,width=118,height=21;
    , ToolTipText = "Imposta la modalit� di rottura dei documenti (Nessuna rottura, su dati differenti, anche in presenza di valori vuoti nel campo)";
    , HelpContextID = 15103849;
    , cFormVar="w_TDFLRA03",RowSource=""+"No,"+"Si,"+"Tassativa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTDFLRA03_4_68.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDFLRA03_4_68.GetRadio()
    this.Parent.oContained.w_TDFLRA03 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRA03_4_68.SetRadio()
    this.Parent.oContained.w_TDFLRA03=trim(this.Parent.oContained.w_TDFLRA03)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRA03=='N',1,;
      iif(this.Parent.oContained.w_TDFLRA03=='S',2,;
      iif(this.Parent.oContained.w_TDFLRA03=='A',3,;
      0)))
  endfunc

  func oTDFLRA03_4_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLIA03='S')
    endwith
   endif
  endfunc


  add object oTDFLRA04_4_69 as StdCombo with uid="CHVURPHYST",rtseq=203,rtrep=.t.,left=591,top=463,width=118,height=21;
    , ToolTipText = "Imposta la modalit� di rottura dei documenti (Nessuna rottura, su dati differenti, anche in presenza di valori vuoti nel campo)";
    , HelpContextID = 15103850;
    , cFormVar="w_TDFLRA04",RowSource=""+"No,"+"Si,"+"Tassativa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTDFLRA04_4_69.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDFLRA04_4_69.GetRadio()
    this.Parent.oContained.w_TDFLRA04 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRA04_4_69.SetRadio()
    this.Parent.oContained.w_TDFLRA04=trim(this.Parent.oContained.w_TDFLRA04)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRA04=='N',1,;
      iif(this.Parent.oContained.w_TDFLRA04=='S',2,;
      iif(this.Parent.oContained.w_TDFLRA04=='A',3,;
      0)))
  endfunc

  func oTDFLRA04_4_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLIA04='S')
    endwith
   endif
  endfunc


  add object oTDFLRA05_4_70 as StdCombo with uid="YUYZHJIIIJ",rtseq=204,rtrep=.t.,left=591,top=486,width=118,height=21;
    , ToolTipText = "Imposta la modalit� di rottura dei documenti (Nessuna rottura, su dati differenti, anche in presenza di valori vuoti nel campo)";
    , HelpContextID = 15103851;
    , cFormVar="w_TDFLRA05",RowSource=""+"No,"+"Si,"+"Tassativa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTDFLRA05_4_70.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDFLRA05_4_70.GetRadio()
    this.Parent.oContained.w_TDFLRA05 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRA05_4_70.SetRadio()
    this.Parent.oContained.w_TDFLRA05=trim(this.Parent.oContained.w_TDFLRA05)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRA05=='N',1,;
      iif(this.Parent.oContained.w_TDFLRA05=='S',2,;
      iif(this.Parent.oContained.w_TDFLRA05=='A',3,;
      0)))
  endfunc

  func oTDFLRA05_4_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLIA05='S')
    endwith
   endif
  endfunc


  add object oTDFLRA06_4_71 as StdCombo with uid="HFTHUADXUI",rtseq=205,rtrep=.t.,left=591,top=509,width=118,height=21;
    , ToolTipText = "Imposta la modalit� di rottura dei documenti (Nessuna rottura, su dati differenti, anche in presenza di valori vuoti nel campo)";
    , HelpContextID = 15103852;
    , cFormVar="w_TDFLRA06",RowSource=""+"No,"+"Si,"+"Tassativa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTDFLRA06_4_71.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDFLRA06_4_71.GetRadio()
    this.Parent.oContained.w_TDFLRA06 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRA06_4_71.SetRadio()
    this.Parent.oContained.w_TDFLRA06=trim(this.Parent.oContained.w_TDFLRA06)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRA06=='N',1,;
      iif(this.Parent.oContained.w_TDFLRA06=='S',2,;
      iif(this.Parent.oContained.w_TDFLRA06=='A',3,;
      0)))
  endfunc

  func oTDFLRA06_4_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLIA06='S')
    endwith
   endif
  endfunc

  add object oStr_4_1 as StdString with uid="CSITQOXQUW",Visible=.t., Left=0, Top=6,;
    Alignment=1, Width=126, Height=15,;
    Caption="Tipo documento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_6 as StdString with uid="VRWDDEAQLK",Visible=.t., Left=16, Top=81,;
    Alignment=0, Width=556, Height=15,;
    Caption="Documenti di origine"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="CTCLNLIUKJ",Visible=.t., Left=352, Top=280,;
    Alignment=0, Width=305, Height=15,;
    Caption="Filtra tipologie righe documenti"  ;
  , bGlobalFont=.t.

  add object oStr_4_15 as StdString with uid="YDTHVQNBFV",Visible=.t., Left=325, Top=332,;
    Alignment=1, Width=113, Height=15,;
    Caption="Non stampare:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="YSZOOZQXFQ",Visible=.t., Left=325, Top=306,;
    Alignment=1, Width=113, Height=15,;
    Caption="Non importare:"  ;
  , bGlobalFont=.t.

  add object oStr_4_27 as StdString with uid="SCVRGDDOCH",Visible=.t., Left=174, Top=306,;
    Alignment=1, Width=89, Height=15,;
    Caption="Tipologia riga:"  ;
  , bGlobalFont=.t.

  func oStr_4_27.mHide()
    with this.Parent.oContained
      return (.w_TDFLNSRI<>'S')
    endwith
  endfunc

  add object oStr_4_28 as StdString with uid="EDQQCYDXLC",Visible=.t., Left=174, Top=326,;
    Alignment=1, Width=89, Height=15,;
    Caption="Tipologia riga:"  ;
  , bGlobalFont=.t.

  func oStr_4_28.mHide()
    with this.Parent.oContained
      return (.w_TDFLVSRI<>'S')
    endwith
  endfunc

  add object oStr_4_29 as StdString with uid="QFGZXOCPZK",Visible=.t., Left=18, Top=280,;
    Alignment=0, Width=277, Height=15,;
    Caption="Genera righe riferimenti"  ;
  , bGlobalFont=.t.

  add object oStr_4_32 as StdString with uid="PEEYOTWPAN",Visible=.t., Left=174, Top=346,;
    Alignment=1, Width=89, Height=15,;
    Caption="Tipologia riga:"  ;
  , bGlobalFont=.t.

  func oStr_4_32.mHide()
    with this.Parent.oContained
      return (.w_TDFLRIDE<>'S')
    endwith
  endfunc

  add object oStr_4_33 as StdString with uid="XHVNPIDBLO",Visible=.t., Left=0, Top=32,;
    Alignment=1, Width=126, Height=18,;
    Caption="Descrizione ns.rif.:"  ;
  , bGlobalFont=.t.

  func oStr_4_33.mHide()
    with this.Parent.oContained
      return (.w_TDFLVEAC='A' AND g_ACQU<>'S')
    endwith
  endfunc

  add object oStr_4_34 as StdString with uid="VCUTVAMPJF",Visible=.t., Left=0, Top=32,;
    Alignment=1, Width=126, Height=18,;
    Caption="Descrizione vs.rif.:"  ;
  , bGlobalFont=.t.

  func oStr_4_34.mHide()
    with this.Parent.oContained
      return (NOT (.w_TDFLVEAC='A' AND g_ACQU<>'S'))
    endwith
  endfunc

  add object oStr_4_35 as StdString with uid="FBAYGPXGRM",Visible=.t., Left=0, Top=58,;
    Alignment=1, Width=126, Height=18,;
    Caption="Modello riferimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_4_39 as StdString with uid="MQITMZPWRN",Visible=.t., Left=18, Top=368,;
    Alignment=0, Width=277, Height=18,;
    Caption="Controlli in fase di import"  ;
  , bGlobalFont=.t.

  add object oStr_4_57 as StdString with uid="JULKPNDXUZ",Visible=.t., Left=330, Top=368,;
    Alignment=0, Width=201, Height=18,;
    Caption="Campi aggiuntivi"  ;
  , bGlobalFont=.t.

  add object oStr_4_58 as StdString with uid="GNGJNJUWXN",Visible=.t., Left=535, Top=368,;
    Alignment=0, Width=56, Height=18,;
    Caption="Importa"  ;
  , bGlobalFont=.t.

  add object oStr_4_66 as StdString with uid="PPOFDIRCFV",Visible=.t., Left=591, Top=368,;
    Alignment=2, Width=118, Height=18,;
    Caption="Rottura"  ;
  , bGlobalFont=.t.

  add object oBox_4_30 as StdBox with uid="IBTMLGVNUO",left=319, top=281, width=1,height=86

  add object oBox_4_31 as StdBox with uid="OQARUZHWDE",left=15, top=298, width=655,height=1

  add object oBox_4_40 as StdBox with uid="FJOCQIAVPQ",left=14, top=389, width=695,height=1

  add object oBox_4_65 as StdBox with uid="BINUUEJLZT",left=319, top=372, width=1,height=159
enddefine
define class tgsve_atdPag5 as StdContainer
  Width  = 721
  height = 555
  stdWidth  = 721
  stdheight = 555
  resizeXpos=472
  resizeYpos=208
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_5_2 as StdField with uid="XJTTZDEFUM",rtseq=80,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 87858906,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=129, Top=26, InputMask=replicate('X',5)

  add object oDESC_5_3 as StdField with uid="LZZEYGGJDH",rtseq=81,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 88193226,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=193, Top=26, InputMask=replicate('X',35)

  add object oTDFLNSTA_5_4 as StdCheck with uid="ZZPZNUZEAZ",rtseq=82,rtrep=.f.,left=26, top=80, caption="No stampa immediata",;
    ToolTipText = "Se attivo: disabilita la stampa immediata del documento",;
    HelpContextID = 44463991,;
    cFormVar="w_TDFLNSTA", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oTDFLNSTA_5_4.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLNSTA_5_4.GetRadio()
    this.Parent.oContained.w_TDFLNSTA = this.RadioValue()
    return .t.
  endfunc

  func oTDFLNSTA_5_4.SetRadio()
    this.Parent.oContained.w_TDFLNSTA=trim(this.Parent.oContained.w_TDFLNSTA)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLNSTA=='S',1,;
      0)
  endfunc

  func oTDFLNSTA_5_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC<>'RF')
    endwith
   endif
  endfunc

  func oTDFLNSTA_5_4.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF')
    endwith
  endfunc


  add object oLinkPC_5_5 as stdDynamicChildContainer with uid="YYCILGGURB",left=13, top=105, width=673, height=285, bOnScreen=.t.;



  add object oTDFLSTLM_5_8 as StdCombo with uid="DNXLCKJKSW",rtseq=83,rtrep=.f.,left=525,top=368,width=155,height=21;
    , ToolTipText = "Se attivo stampa report secondari/produzione";
    , HelpContextID = 201951357;
    , cFormVar="w_TDFLSTLM",RowSource=""+"No,"+"S�,"+"S� opzionali con conferma", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oTDFLSTLM_5_8.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oTDFLSTLM_5_8.GetRadio()
    this.Parent.oContained.w_TDFLSTLM = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSTLM_5_8.SetRadio()
    this.Parent.oContained.w_TDFLSTLM=trim(this.Parent.oContained.w_TDFLSTLM)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSTLM=='N',1,;
      iif(this.Parent.oContained.w_TDFLSTLM=='S',2,;
      iif(this.Parent.oContained.w_TDFLSTLM=='B',3,;
      0)))
  endfunc

  add object oStr_5_1 as StdString with uid="LQDMPVFJME",Visible=.t., Left=10, Top=26,;
    Alignment=1, Width=116, Height=15,;
    Caption="Tipo documento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_6 as StdString with uid="UXOWSYRQYR",Visible=.t., Left=14, Top=58,;
    Alignment=0, Width=651, Height=18,;
    Caption="Report documento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_7 as StdString with uid="TJVILIJMHM",Visible=.t., Left=370, Top=368,;
    Alignment=1, Width=154, Height=18,;
    Caption="Report integrativi:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_atd','TIP_DOCU','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TDTIPDOC=TIP_DOCU.TDTIPDOC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
