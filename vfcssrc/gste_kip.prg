* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kip                                                        *
*              Esportazione estratto conto in xml                              *
*                                                                              *
*      Author: ZUCCHETTI S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_44]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-23                                                      *
* Last revis.: 2009-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kip",oParentObject))

* --- Class definition
define class tgste_kip as StdForm
  Top    = 67
  Left   = 167

  * --- Standard Properties
  Width  = 434
  Height = 53
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-04-14"
  HelpContextID=1428375
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=43

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  AGENTI_IDX = 0
  cPrg = "gste_kip"
  cComment = "Esportazione estratto conto in xml"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(4)
  w_GESTBUN = space(1)
  w_CODLIN = space(3)
  w_DATSTA = ctod('  /  /  ')
  w_UTENTE = 0
  w_TIPCLF = space(1)
  o_TIPCLF = space(1)
  w_VALCLI = space(1)
  w_CONSEL1 = space(15)
  w_TIPORD = space(1)
  w_CONSEL2 = space(15)
  w_TIPORI = space(1)
  w_CODBUN = space(3)
  w_APERTE = space(1)
  w_SUPERBU = space(15)
  w_OQRY = space(50)
  w_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_OREP = space(50)
  w_AGENTE = space(5)
  w_CLIFOR = .F.
  w_STACON = space(5)
  w_ESPORTAL = space(1)
  w_ESPOSIZ = space(1)
  w_FLSALD = space(1)
  o_FLSALD = space(1)
  w_CODINI = space(15)
  w_CODFIN = space(15)
  w_FLPART1 = space(10)
  w_DATPAR = ctod('  /  /  ')
  w_CODZON = space(10)
  w_CATCOM = space(10)
  w_FLANSCA = space(1)
  w_FLSOSP = space(1)
  w_FLGSOS = space(1)
  w_FLGNSO = space(1)
  w_CODAGE = space(5)
  w_AGEOBSO = ctod('  /  /  ')
  w_PARNEG = space(1)
  w_DESAGE = space(35)
  w_EFFETTI = space(1)
  w_TIPRAG = space(1)
  w_ONUME = 0
  w_FLORIG = space(1)
  w_SEDE = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kipPag1","gste_kip",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='AGENTI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSTE_BEC with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(4)
      .w_GESTBUN=space(1)
      .w_CODLIN=space(3)
      .w_DATSTA=ctod("  /  /  ")
      .w_UTENTE=0
      .w_TIPCLF=space(1)
      .w_VALCLI=space(1)
      .w_CONSEL1=space(15)
      .w_TIPORD=space(1)
      .w_CONSEL2=space(15)
      .w_TIPORI=space(1)
      .w_CODBUN=space(3)
      .w_APERTE=space(1)
      .w_SUPERBU=space(15)
      .w_OQRY=space(50)
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_OREP=space(50)
      .w_AGENTE=space(5)
      .w_CLIFOR=.f.
      .w_STACON=space(5)
      .w_ESPORTAL=space(1)
      .w_ESPOSIZ=space(1)
      .w_FLSALD=space(1)
      .w_CODINI=space(15)
      .w_CODFIN=space(15)
      .w_FLPART1=space(10)
      .w_DATPAR=ctod("  /  /  ")
      .w_CODZON=space(10)
      .w_CATCOM=space(10)
      .w_FLANSCA=space(1)
      .w_FLSOSP=space(1)
      .w_FLGSOS=space(1)
      .w_FLGNSO=space(1)
      .w_CODAGE=space(5)
      .w_AGEOBSO=ctod("  /  /  ")
      .w_PARNEG=space(1)
      .w_DESAGE=space(35)
      .w_EFFETTI=space(1)
      .w_TIPRAG=space(1)
      .w_ONUME=0
      .w_FLORIG=space(1)
      .w_SEDE=space(2)
        .w_AZIENDA = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CODLIN = space(3)
        .w_DATSTA = i_datsys
        .w_UTENTE = g_CODUTE
        .w_TIPCLF = 'C'
        .w_VALCLI = 'N'
        .w_CONSEL1 = space(15)
        .w_TIPORD = 'P'
        .w_CONSEL2 = space(15)
        .w_TIPORI = 'T'
        .w_CODBUN = space(3)
        .w_APERTE = 'N'
        .w_SUPERBU = space(15)
        .w_OQRY = 'QUERY\GSTE_QEC.VQR'
        .w_SCAINI = cp_CharToDate('  -  -    ')
        .w_SCAFIN = cp_CharToDate('  -  -    ')
        .w_OREP = space(50)
        .w_AGENTE = space(5)
        .w_CLIFOR = .f.
        .w_STACON = space(5)
        .w_ESPORTAL = 'S'
        .w_ESPOSIZ = 'N'
        .w_FLSALD = 'R'
        .w_CODINI = space(15)
        .w_CODFIN = space(15)
        .w_FLPART1 = ' '
        .w_DATPAR = cp_CharToDate('  -  -    ')
        .w_CODZON = SPACE(3)
        .w_CATCOM = SPACE(3)
        .w_FLANSCA = ' '
        .w_FLSOSP = 'N'
        .w_FLGSOS = IIF(.w_FLSOSP $ 'TN', ' ', 'S')
        .w_FLGNSO = IIF(.w_FLSOSP $ 'TS', ' ', 'S')
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_CODAGE))
          .link_1_37('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_41.Calculate(Ah_MsgFormat("Maschera per la generazione del file xml per i dati presenti%0nell'estratto conto clienti relativi alle sole scadenze aperte"))
          .DoRTCalc(36,38,.f.)
        .w_EFFETTI = 'A'
          .DoRTCalc(40,41,.f.)
        .w_FLORIG = ' '
        .w_SEDE = 'NO'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,23,.t.)
            .w_FLSALD = 'R'
        .DoRTCalc(25,31,.t.)
            .w_FLSOSP = 'N'
            .w_FLGSOS = IIF(.w_FLSOSP $ 'TN', ' ', 'S')
            .w_FLGNSO = IIF(.w_FLSOSP $ 'TS', ' ', 'S')
          .link_1_37('Full')
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate(Ah_MsgFormat("Maschera per la generazione del file xml per i dati presenti%0nell'estratto conto clienti relativi alle sole scadenze aperte"))
        .DoRTCalc(36,38,.t.)
            .w_EFFETTI = 'A'
        .DoRTCalc(40,41,.t.)
            .w_FLORIG = ' '
            .w_SEDE = 'NO'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate(Ah_MsgFormat("Maschera per la generazione del file xml per i dati presenti%0nell'estratto conto clienti relativi alle sole scadenze aperte"))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(4))
      this.w_GESTBUN = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(4)
      endif
      this.w_GESTBUN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_AGEOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_AGEOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCLF = this.w_TIPCLF
    this.o_FLSALD = this.w_FLSALD
    return

enddefine

* --- Define pages as container
define class tgste_kipPag1 as StdContainer
  Width  = 430
  height = 53
  stdWidth  = 430
  stdheight = 53
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_32 as StdButton with uid="FVOSRFCDOZ",left=323, top=2, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'elaborazione";
    , HelpContextID = 176061162;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        do GSTE_BEC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_33 as StdButton with uid="KUXAWRSZKX",left=374, top=2, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176061162;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_41 as cp_calclbl with uid="IITWOOTKUH",left=5, top=8, width=315,height=37,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 89009434
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kip','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
