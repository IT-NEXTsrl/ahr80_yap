* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kpr                                                        *
*              Gestione tabella progressivi                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-02                                                      *
* Last revis.: 2013-07-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kpr",oParentObject))

* --- Class definition
define class tgsar_kpr as StdForm
  Top    = 38
  Left   = 10

  * --- Standard Properties
  Width  = 645
  Height = 476
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-07-12"
  HelpContextID=149518185
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  TSDESPRO_IDX = 0
  cPrg = "gsar_kpr"
  cComment = "Gestione tabella progressivi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_NUMDOC2 = 0
  o_NUMDOC2 = 0
  w_NUMDOC1 = 0
  w_CODPROG = space(20)
  o_CODPROG = space(20)
  w_DESPRO = space(40)
  w_TCODE = space(50)
  w_TABLECODE = space(50)
  w_WARNCODE = space(50)
  w_NOMTAB = space(20)
  w_NOMCAM = space(20)
  w_NOTE = space(0)
  w_DEPON = space(50)
  w_ZoomProg = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kprPag1","gsar_kpr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNUMDOC1_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomProg = this.oPgFrm.Pages(1).oPag.ZoomProg
    DoDefault()
    proc Destroy()
      this.w_ZoomProg = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='TSDESPRO'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_NUMDOC2=0
      .w_NUMDOC1=0
      .w_CODPROG=space(20)
      .w_DESPRO=space(40)
      .w_TCODE=space(50)
      .w_TABLECODE=space(50)
      .w_WARNCODE=space(50)
      .w_NOMTAB=space(20)
      .w_NOMCAM=space(20)
      .w_NOTE=space(0)
      .w_DEPON=space(50)
        .w_CODAZI = i_CODAZI
        .w_NUMDOC2 = .w_ZoomProg.getVar("AUTONUM")
        .w_NUMDOC1 = .w_ZoomProg.getVar("AUTONUM")
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODPROG))
          .link_1_8('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomProg.Calculate()
          .DoRTCalc(5,5,.f.)
        .w_TCODE = 'prog\'+alltrim(.w_CODPROG)+'%'
        .w_TABLECODE = .w_ZoomProg.getVar("TABLECODE")
        .w_WARNCODE = .w_ZoomProg.getVar("WARNCODE")
    endwith
    this.DoRTCalc(9,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_NUMDOC2 = .w_ZoomProg.getVar("AUTONUM")
        if .o_NUMDOC2<>.w_NUMDOC2
            .w_NUMDOC1 = .w_ZoomProg.getVar("AUTONUM")
        endif
        .oPgFrm.Page1.oPag.ZoomProg.Calculate()
        .DoRTCalc(4,5,.t.)
        if .o_CODPROG<>.w_CODPROG
            .w_TCODE = 'prog\'+alltrim(.w_CODPROG)+'%'
        endif
            .w_TABLECODE = .w_ZoomProg.getVar("TABLECODE")
            .w_WARNCODE = .w_ZoomProg.getVar("WARNCODE")
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomProg.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomProg.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODPROG
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TSDESPRO_IDX,3]
    i_lTable = "TSDESPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TSDESPRO_IDX,2], .t., this.TSDESPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TSDESPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPROG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TSDESPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODPROG)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESPRO,DPCOMTAB,DP_CAMPO,DP__NOTE,DPDEPEON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_CODPROG))
          select DPCODICE,DPDESPRO,DPCOMTAB,DP_CAMPO,DP__NOTE,DPDEPEON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPROG)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESPRO like "+cp_ToStrODBC(trim(this.w_CODPROG)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESPRO,DPCOMTAB,DP_CAMPO,DP__NOTE,DPDEPEON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESPRO like "+cp_ToStr(trim(this.w_CODPROG)+"%");

            select DPCODICE,DPDESPRO,DPCOMTAB,DP_CAMPO,DP__NOTE,DPDEPEON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPROG) and !this.bDontReportError
            deferred_cp_zoom('TSDESPRO','*','DPCODICE',cp_AbsName(oSource.parent,'oCODPROG_1_8'),i_cWhere,'',"Tabella progressivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESPRO,DPCOMTAB,DP_CAMPO,DP__NOTE,DPDEPEON";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPDESPRO,DPCOMTAB,DP_CAMPO,DP__NOTE,DPDEPEON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPROG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESPRO,DPCOMTAB,DP_CAMPO,DP__NOTE,DPDEPEON";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPROG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODPROG)
            select DPCODICE,DPDESPRO,DPCOMTAB,DP_CAMPO,DP__NOTE,DPDEPEON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPROG = NVL(_Link_.DPCODICE,space(20))
      this.w_DESPRO = NVL(_Link_.DPDESPRO,space(40))
      this.w_NOMTAB = NVL(_Link_.DPCOMTAB,space(20))
      this.w_NOMCAM = NVL(_Link_.DP_CAMPO,space(20))
      this.w_NOTE = NVL(_Link_.DP__NOTE,space(0))
      this.w_DEPON = NVL(_Link_.DPDEPEON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODPROG = space(20)
      endif
      this.w_DESPRO = space(40)
      this.w_NOMTAB = space(20)
      this.w_NOMCAM = space(20)
      this.w_NOTE = space(0)
      this.w_DEPON = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TSDESPRO_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.TSDESPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPROG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNUMDOC2_1_4.value==this.w_NUMDOC2)
      this.oPgFrm.Page1.oPag.oNUMDOC2_1_4.value=this.w_NUMDOC2
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC1_1_6.value==this.w_NUMDOC1)
      this.oPgFrm.Page1.oPag.oNUMDOC1_1_6.value=this.w_NUMDOC1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPROG_1_8.value==this.w_CODPROG)
      this.oPgFrm.Page1.oPag.oCODPROG_1_8.value=this.w_CODPROG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRO_1_10.value==this.w_DESPRO)
      this.oPgFrm.Page1.oPag.oDESPRO_1_10.value=this.w_DESPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oTABLECODE_1_13.value==this.w_TABLECODE)
      this.oPgFrm.Page1.oPag.oTABLECODE_1_13.value=this.w_TABLECODE
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMTAB_1_16.value==this.w_NOMTAB)
      this.oPgFrm.Page1.oPag.oNOMTAB_1_16.value=this.w_NOMTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMCAM_1_17.value==this.w_NOMCAM)
      this.oPgFrm.Page1.oPag.oNOMCAM_1_17.value=this.w_NOMCAM
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_20.value==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_20.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDEPON_1_23.value==this.w_DEPON)
      this.oPgFrm.Page1.oPag.oDEPON_1_23.value=this.w_DEPON
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODPROG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPROG_1_8.SetFocus()
            i_bnoObbl = !empty(.w_CODPROG)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NUMDOC2 = this.w_NUMDOC2
    this.o_CODPROG = this.w_CODPROG
    return

enddefine

* --- Define pages as container
define class tgsar_kprPag1 as StdContainer
  Width  = 641
  height = 476
  stdWidth  = 641
  stdheight = 476
  resizeXpos=337
  resizeYpos=227
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_3 as StdButton with uid="PPWCVSFLXD",left=586, top=4, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 94363114;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        .NotifyEvent('Aggiorna')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMDOC2_1_4 as StdField with uid="RAQVYVSMAM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_NUMDOC2", cQueryName = "NUMDOC2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore progressivo selezionato",;
    HelpContextID = 11554090,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=198, Top=421, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  add object oNUMDOC1_1_6 as StdField with uid="ORYXTLZYHF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NUMDOC1", cQueryName = "NUMDOC1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Nuovo valore da assegnare al progressivo selezionato",;
    HelpContextID = 11554090,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=198, Top=444, cSayPict='"999999999999999"', cGetPict='"999999999999999"', FORECOLOR=RGB(255,0,0)


  add object oBtn_1_7 as StdButton with uid="TGIHDLMVKD",left=585, top=425, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142200762;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODPROG_1_8 as StdField with uid="ZROOMEHHNQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODPROG", cQueryName = "CODPROG",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Selezionare codice progressivo - <F9>=zoom",;
    HelpContextID = 193666086,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=93, Top=9, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="TSDESPRO", oKey_1_1="DPCODICE", oKey_1_2="this.w_CODPROG"

  func oCODPROG_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPROG_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPROG_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TSDESPRO','*','DPCODICE',cp_AbsName(this.parent,'oCODPROG_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tabella progressivi",'',this.parent.oContained
  endproc


  add object oBtn_1_9 as StdButton with uid="CDLCARTVMT",left=485, top=425, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare la numerazione progressiva";
    , HelpContextID = 149516266;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        do GSAR_BPR with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_TABLECODE))
      endwith
    endif
  endfunc

  add object oDESPRO_1_10 as StdField with uid="ILELLRWFGB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESPRO", cQueryName = "DESPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 74710474,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=263, Top=9, InputMask=replicate('X',40)


  add object ZoomProg as cp_zoombox with uid="UCDFOSNLIN",left=6, top=53, width=628,height=229,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSAR_KPR",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="CPWARN",cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bRetriveAllRows=.f.,bNoZoomGridShape=.f.,;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 28479462

  add object oTABLECODE_1_13 as StdField with uid="LYHGRYWWKV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TABLECODE", cQueryName = "TABLECODE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Codifica completa del progressivo",;
    HelpContextID = 21564470,;
   bGlobalFont=.t.,;
    Height=21, Width=531, Left=100, Top=287, InputMask=replicate('X',50)

  add object oNOMTAB_1_16 as StdField with uid="NFBCMCYVSX",rtseq=9,rtrep=.f.,;
    cFormVar = "w_NOMTAB", cQueryName = "NOMTAB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome tabella di riferimento",;
    HelpContextID = 41964330,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=100, Top=312, InputMask=replicate('X',20)

  add object oNOMCAM_1_17 as StdField with uid="ZFJWDUWHNN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NOMCAM", cQueryName = "NOMCAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome campo",;
    HelpContextID = 126964522,;
   bGlobalFont=.t.,;
    Height=21, Width=226, Left=405, Top=312, InputMask=replicate('X',20)

  add object oNOTE_1_20 as StdMemo with uid="TSBXGTHGSL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note aggiuntive (non modificabili)",;
    HelpContextID = 144630570,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=48, Width=531, Left=100, Top=363, TABSTOP=.F.

  add object oDEPON_1_23 as StdField with uid="WIZJAPPLFY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DEPON", cQueryName = "DEPON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Dipendenze progressivo",;
    HelpContextID = 62205386,;
   bGlobalFont=.t.,;
    Height=21, Width=531, Left=100, Top=337, InputMask=replicate('X',50)


  add object oBtn_1_26 as StdButton with uid="ZJKKZSBHAP",left=535, top=425, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare la numerazione progressiva";
    , HelpContextID = 149516266;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      do GSAR_SPR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="BQRGPBQDAB",Visible=.t., Left=11, Top=9,;
    Alignment=1, Width=78, Height=18,;
    Caption="Progressivo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="MGJFGTFSSS",Visible=.t., Left=107, Top=445,;
    Alignment=1, Width=85, Height=18,;
    Caption="Nuovo valore:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="IFGKXWIDVU",Visible=.t., Left=12, Top=287,;
    Alignment=1, Width=84, Height=18,;
    Caption="Progressivo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="VEPZOTSCIA",Visible=.t., Left=36, Top=312,;
    Alignment=1, Width=60, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="TMWQZMMXZV",Visible=.t., Left=337, Top=312,;
    Alignment=1, Width=65, Height=18,;
    Caption="Colonna:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="VBJNCNWFTM",Visible=.t., Left=46, Top=363,;
    Alignment=1, Width=50, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="DBYJNPDHWW",Visible=.t., Left=8, Top=337,;
    Alignment=1, Width=88, Height=18,;
    Caption="Dipendenze:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="HSHNLEMHCD",Visible=.t., Left=100, Top=422,;
    Alignment=1, Width=92, Height=18,;
    Caption="Valore attuale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_22 as StdBox with uid="CIIMNICWRY",left=91, top=417, width=234,height=52
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kpr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
