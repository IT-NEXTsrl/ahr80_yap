* ---------------------------------------------------------------------------- *
* #%&%#Build:0000
*                                                                              *
*   Procedure: NUMGIO_V                                                        *
*              Num. Altri Bollati Verticale                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 24/4/01                                                         *
* Last revis.: 24/4/01                                                         *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
private i_formh13,i_formh14
private i_brk,i_quit,i_row,i_pag,i_oldarea,i_oldrows
private i_rdvars,i_rdkvars,i_rdkey
private w_s,i_rec,i_wait,i_modal
private i_usr_brk          && .T. se l'utente ha interrotto la stampa
private i_frm_rpr          && .T. se � stata stampata la testata del report
private Sm                 && Variabile di appoggio per formattazione memo
private i_form_ph,i_form_phh,i_form_phg,i_form_pf,i_saveh13,i_exec

* --- Variabili per configurazione stampante
private w_t_stdevi,w_t_stmsin,w_t_stnrig
private w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
private w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
private w_t_stpica,w_t_stelit
private w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
private w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
private w_stdesc
store " " to w_t_stdevi
store " " to w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
store " " to w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
store " " to w_t_stpica,w_t_stelit, i_rdkey
store " " to w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
store " " to w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
store ""  to Sm
store 0 to i_formh13,i_formh14,i_form_phh,i_form_phg,i_saveh13
store space(20) to w_stdesc
i_form_ph = 12
i_form_pf = 13
i_wait = 1
i_modal = .t.
i_oldrows = 0
store .F. to i_usr_brk, i_frm_rpr, w_s
i_exec = ""

dimension i_rdvars[41,2],i_rdkvars[6,2],i_zoompars[3]
store 0 to i_rdvars[1,1],i_rdkvars[1,1]
private p_TESTA,p_ANNO,p_RAGAZI,p_PARTO,w_DATIAZ
private p_CODO,w_SETTA,w_SETTA
p_TESTA = space(99)
p_ANNO = space(4)
p_RAGAZI = space(99)
p_PARTO = space(16)
w_DATIAZ = space(99)
p_CODO = space(16)
w_SETTA = space(1)
w_SETTA = space(1)

i_oldarea = select()
select __tmp__
go top

w_t_stnrig = 65
w_t_stmsin = 0
  
  
* --- Inizializza Variabili per configurazione stampante da CP_CHPRN
w_t_stdevi = cFileStampa+'.prn'
w_t_stlung = ts_ForPag
w_t_stnrig = ts_RowOk 
w_t_strese = ts_Reset+ts_Inizia
w_t_st10 = ts_10Cpi
w_t_st12 = ts_12Cpi
w_t_st15 = ts_15Cpi
w_t_stcomp = ts_Comp
w_t_stnorm = ts_RtComp
w_t_stbold = ts_StBold
w_t_stwide = ts_StDoub
w_t_stital = ts_StItal
w_t_stunde = ts_StUnde
w_t_stbol_ = ts_FiBold
w_t_stwid_ = ts_FiDoub
w_t_stita_ = ts_FiItal
w_t_stund_ = ts_FiUnde
* --- non definiti
*w_t_stmsin
*w_t_stnlq
*w_t_stdraf
*w_t_stpica
*w_t_stelit

i_row = 0
i_pag = 1
*---------------------------------------
wait wind "Generazione file appoggio..." nowait
*----------------------------------------
activate screen
* --- Settaggio stampante
set printer to &w_t_stdevi
set device to printer
set margin to w_t_stmsin
if len(trim(w_t_strese))>0
  @ 0,0 say &w_t_strese
endif
if len(trim(w_t_stlung))>0
  @ 0,0 say &w_t_stlung
endif
* --- Inizio stampa
do NUMG4O_V with 1, 0
if i_frm_rpr .and. .not. i_usr_brk
  * stampa il piede del report
  do NUMG4O_V with 14, 0
endif
if i_row<>0 
  @ prow(),pcol() say chr(12)
endif
set device to screen
set printer off
set printer to
if .not. i_frm_rpr
  do cplu_erm with "Non ci sono dati da stampare"
endif
* --- Fine
if .not.(empty(wontop()))
  activate  window (wontop())
endif
i_warea = alltrim(str(i_oldarea))
select (i_oldarea)
return


procedure NUMG4O_V
* === Procedure NUMG4O_V
parameters i_form_id, i_height

private i_currec, i_prevrec, i_formh
private i_frm_brk    && flag che indica il verificarsi di un break interform
                     && anche se la specifica condizione non � soddisfatta
private i_break, i_cond1

do case
  case i_form_id=1
    select __tmp__
    i_warea = '__tmp__'
    * --- inizializza le condizioni dei break interform
    i_cond1 = (CAMPO1)
    i_frm_brk = .T.
    do while .not. eof() 
     wait wind "Elabora riga:"+str(recno()) +"/"+str(reccount()) nowait
      if .not. i_frm_rpr
        * --- stampa l'intestazione del report
        do NUMG4O_V with 11, 0
        i_frm_rpr = .T.
      endif
      if i_cond1<>(CAMPO1) .or. i_frm_brk
        i_cond1 = (CAMPO1)
        i_frm_brk = .T.
        do NUMG5O_V with 1.00, 0
      endif
      i_frm_brk = .F.
      * stampa del dettaglio
      do NUMG5O_V with 1.01, 3
      if i_usr_brk
        exit
      endif
      * --- passa al record successivo
      i_prevrec = recno()
      if .not. eof()
        skip
      endif
      i_currec = iif(eof(), -1, recno())
      if eof()  .or. i_cond1<>(CAMPO1)      
        go i_prevrec
        do NUMG5O_V with 1.02, 0
        do cplu_go with i_currec
      endif
    enddo
  case i_form_id=11
    do NUMG5O_V with 11.00, 1
  case i_form_id=99
    * --- controllo per il salto pagina
    if inkey()=27
      i_usr_brk = .T.
    else
      if i_row+i_height+i_formh13>w_t_stnrig
        * --- stampa il piede di pagina
        i_row = w_t_stnrig-i_formh13
        if i_form_pf=13
          do NUMG4O_V with 13, 0
        else
          do NUMG5O_V with -i_form_pf,i_formh13
        endif
        i_row = 0
        i_pag = i_pag+1
        @ prow(),pcol() say chr(12)+chr(13)
        w_s = 0
        * --- stampa l'intestazione di pagina
        if i_form_ph=12
          do NUMG4O_V with 12, 0
        else
          do NUMG5O_V with -i_form_ph,i_form_phh
        endif
      endif
    endif
  case i_form_id=98
    i_form_pf = 0.00
    i_saveh13 = i_formh13
    i_formh13 = 0
endcase
return

procedure NUMG5O_V
* === Procedure NUMG5O_V
parameters i_form_id, i_form_h

* --- controllo per il salto pagina
if i_form_id<11 .and. i_form_id>0
  do NUMG4O_V with 99, i_form_h
  if i_usr_brk
    return
  endif
endif
if i_form_id<0
  i_form_id = -i_form_id
endif
do case
  * --- 1� form
  case i_form_id=1.0
    do frm1_0
  case i_form_id=1.01
    do frm1_01
  case i_form_id=1.02
    do frm1_02
  * --- 11� form
  case i_form_id=11.0
    do frm11_0
endcase
i_row = i_row+i_form_h
return


* --- 1� form
procedure frm1_0
return

* --- frm1_01
procedure frm1_01
  p_TESTA = TESTA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(19-18),0,at_x(7),transform(p_TESTA,""),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(19-18),111,at_x(891),"Anno:",i_fn
  p_ANNO = ANNO
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(19-18),116,at_x(933),transform(p_ANNO,""),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(19-18),122,at_x(979),"Pag.",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(19-18),126,at_x(1013),transform(CAMPO1,"999999"),i_fn
  p_RAGAZI = g_RAGAZI
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(38-18),0,at_x(7),transform(p_RAGAZI,""),i_fn
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(38-18),104,at_x(835),"Partita IVA:",i_fn
  p_PARTO = L_PARTO
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(38-18),116,at_x(933),transform(p_PARTO,""),i_fn
  w_DATIAZ = trim(L_indi)+' - '+L_CAPO+' - '+TRIM(L_LOCO)+IIF(EMPTY(L_PROVO),'',' ( '+L_PROVO+' )')
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(57-18),0,at_x(7),transform(w_DATIAZ,""),i_fn
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(57-18),101,at_x(811),"Codice fiscale:",i_fn
  p_CODO = L_CODO
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(57-18),116,at_x(933),transform(p_CODO,""),i_fn
return

* --- frm1_02
procedure frm1_02
  i_row = w_t_stnrig-i_formh13
return

* --- 11� form
procedure frm11_0
  w_SETTA = " "
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),1,at_x(8),transform(w_SETTA,"X"),i_fn
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),12,at_x(96),"// Setta carattere di stampa e compresso",i_fn
   endif
  w_SETTA = " "
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),61,at_x(489),transform(w_SETTA,"X"),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
return

function at_x
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/6
  return i_pos

function at_y
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/13
  return i_pos

procedure F_Say
  parameter y,py,x,px,s,f
  * --- Questa funzione corregge un errore del driver "Generica solo testo"
  *     di Windows che aggiunge spazi oltre la 89 colonna
  *     Inoltre in Windows sostituisce il carattere 196 con un '-'
  if y=-1
    y = prow()
    x = pcol()
  endif
  @ y,x say s
  return

PROCEDURE CPLU_GO
parameter i_recpos

if i_recpos<=0 .or. i_recpos>reccount()
  if reccount()<>0
    goto bottom
    if .not. eof()
      skip
    endif
  endif  
else
  goto i_recpos
endif
return

* --- Area Manuale = Functions & Procedures 
* --- Fine Area Manuale 
