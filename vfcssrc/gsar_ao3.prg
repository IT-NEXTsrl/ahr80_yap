* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ao3                                                        *
*              Contropartite di assestamento                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_100]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-25                                                      *
* Last revis.: 2013-04-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_ao3")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_ao3")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_ao3")
  return

* --- Class definition
define class tgsar_ao3 as StdPCForm
  Width  = 584
  Height = 445+35
  Top    = 9
  Left   = 14
  cComment = "Contropartite di assestamento"
  cPrg = "gsar_ao3"
  HelpContextID=176781161
  add object cnt as tcgsar_ao3
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_ao3 as PCContext
  w_COCODAZI = space(5)
  w_RAGAZI = space(40)
  w_COTIPCON = space(1)
  w_PERIVB = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = space(8)
  w_UTDV = space(8)
  w_TIPART = space(1)
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_COFATEME = space(15)
  w_DESFEM = space(40)
  w_COFATRIC = space(15)
  w_DESFRI = space(40)
  w_CORATATT = space(15)
  w_DESRAA = space(40)
  w_CORATPAS = space(15)
  w_DESRAP = space(40)
  w_CORISATT = space(15)
  w_DESRIA = space(40)
  w_CORISPAS = space(15)
  w_DESRIP = space(40)
  w_COIVADEB = space(15)
  w_DESIVD = space(40)
  w_TIVDEB = space(1)
  w_COIVACRE = space(15)
  w_DESIVC = space(40)
  w_TIVCRE = space(1)
  w_COCREBRE = space(15)
  w_COCREMED = space(15)
  w_CODEBBRE = space(15)
  w_CODEBMED = space(15)
  w_DESCRB = space(40)
  w_DESCRM = space(40)
  w_DESDEB = space(40)
  w_DESDEM = space(40)
  w_COCONARR = space(15)
  w_DESARR = space(40)
  w_COCAUFDE = space(5)
  w_COCASFDE = space(5)
  w_COCAUFDR = space(5)
  w_COCASFDR = space(5)
  w_COINDIVA = space(1)
  w_COCAURIM = space(5)
  w_COCASRIM = space(5)
  w_COCAURER = space(5)
  w_COCASRER = space(5)
  w_COCAFRER = space(5)
  w_COCAUCES = space(5)
  w_COCASCES = space(5)
  w_DESRIM = space(35)
  w_FLASSE = space(1)
  w_DESRIMS = space(35)
  w_DESFDE = space(35)
  w_DESFDES = space(35)
  w_DESFDR = space(35)
  w_DESFDRS = space(35)
  w_DESRER = space(35)
  w_DESRERS = space(35)
  w_DESRERF = space(35)
  w_DESCES = space(35)
  w_DESCESS = space(35)
  w_FLASSE1 = space(1)
  w_FLASSE2 = space(1)
  w_FLASSE3 = space(1)
  w_FLASSE4 = space(1)
  w_FLASSE5 = space(1)
  w_FLASSE6 = space(1)
  w_FLASSE7 = space(1)
  w_FLASSE8 = space(1)
  w_FLASSE9 = space(1)
  w_FLASSE10 = space(1)
  proc Save(oFrom)
    this.w_COCODAZI = oFrom.w_COCODAZI
    this.w_RAGAZI = oFrom.w_RAGAZI
    this.w_COTIPCON = oFrom.w_COTIPCON
    this.w_PERIVB = oFrom.w_PERIVB
    this.w_UTCC = oFrom.w_UTCC
    this.w_UTCV = oFrom.w_UTCV
    this.w_UTDC = oFrom.w_UTDC
    this.w_UTDV = oFrom.w_UTDV
    this.w_TIPART = oFrom.w_TIPART
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_DATOBSO = oFrom.w_DATOBSO
    this.w_COFATEME = oFrom.w_COFATEME
    this.w_DESFEM = oFrom.w_DESFEM
    this.w_COFATRIC = oFrom.w_COFATRIC
    this.w_DESFRI = oFrom.w_DESFRI
    this.w_CORATATT = oFrom.w_CORATATT
    this.w_DESRAA = oFrom.w_DESRAA
    this.w_CORATPAS = oFrom.w_CORATPAS
    this.w_DESRAP = oFrom.w_DESRAP
    this.w_CORISATT = oFrom.w_CORISATT
    this.w_DESRIA = oFrom.w_DESRIA
    this.w_CORISPAS = oFrom.w_CORISPAS
    this.w_DESRIP = oFrom.w_DESRIP
    this.w_COIVADEB = oFrom.w_COIVADEB
    this.w_DESIVD = oFrom.w_DESIVD
    this.w_TIVDEB = oFrom.w_TIVDEB
    this.w_COIVACRE = oFrom.w_COIVACRE
    this.w_DESIVC = oFrom.w_DESIVC
    this.w_TIVCRE = oFrom.w_TIVCRE
    this.w_COCREBRE = oFrom.w_COCREBRE
    this.w_COCREMED = oFrom.w_COCREMED
    this.w_CODEBBRE = oFrom.w_CODEBBRE
    this.w_CODEBMED = oFrom.w_CODEBMED
    this.w_DESCRB = oFrom.w_DESCRB
    this.w_DESCRM = oFrom.w_DESCRM
    this.w_DESDEB = oFrom.w_DESDEB
    this.w_DESDEM = oFrom.w_DESDEM
    this.w_COCONARR = oFrom.w_COCONARR
    this.w_DESARR = oFrom.w_DESARR
    this.w_COCAUFDE = oFrom.w_COCAUFDE
    this.w_COCASFDE = oFrom.w_COCASFDE
    this.w_COCAUFDR = oFrom.w_COCAUFDR
    this.w_COCASFDR = oFrom.w_COCASFDR
    this.w_COINDIVA = oFrom.w_COINDIVA
    this.w_COCAURIM = oFrom.w_COCAURIM
    this.w_COCASRIM = oFrom.w_COCASRIM
    this.w_COCAURER = oFrom.w_COCAURER
    this.w_COCASRER = oFrom.w_COCASRER
    this.w_COCAFRER = oFrom.w_COCAFRER
    this.w_COCAUCES = oFrom.w_COCAUCES
    this.w_COCASCES = oFrom.w_COCASCES
    this.w_DESRIM = oFrom.w_DESRIM
    this.w_FLASSE = oFrom.w_FLASSE
    this.w_DESRIMS = oFrom.w_DESRIMS
    this.w_DESFDE = oFrom.w_DESFDE
    this.w_DESFDES = oFrom.w_DESFDES
    this.w_DESFDR = oFrom.w_DESFDR
    this.w_DESFDRS = oFrom.w_DESFDRS
    this.w_DESRER = oFrom.w_DESRER
    this.w_DESRERS = oFrom.w_DESRERS
    this.w_DESRERF = oFrom.w_DESRERF
    this.w_DESCES = oFrom.w_DESCES
    this.w_DESCESS = oFrom.w_DESCESS
    this.w_FLASSE1 = oFrom.w_FLASSE1
    this.w_FLASSE2 = oFrom.w_FLASSE2
    this.w_FLASSE3 = oFrom.w_FLASSE3
    this.w_FLASSE4 = oFrom.w_FLASSE4
    this.w_FLASSE5 = oFrom.w_FLASSE5
    this.w_FLASSE6 = oFrom.w_FLASSE6
    this.w_FLASSE7 = oFrom.w_FLASSE7
    this.w_FLASSE8 = oFrom.w_FLASSE8
    this.w_FLASSE9 = oFrom.w_FLASSE9
    this.w_FLASSE10 = oFrom.w_FLASSE10
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_COCODAZI = this.w_COCODAZI
    oTo.w_RAGAZI = this.w_RAGAZI
    oTo.w_COTIPCON = this.w_COTIPCON
    oTo.w_PERIVB = this.w_PERIVB
    oTo.w_UTCC = this.w_UTCC
    oTo.w_UTCV = this.w_UTCV
    oTo.w_UTDC = this.w_UTDC
    oTo.w_UTDV = this.w_UTDV
    oTo.w_TIPART = this.w_TIPART
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_DATOBSO = this.w_DATOBSO
    oTo.w_COFATEME = this.w_COFATEME
    oTo.w_DESFEM = this.w_DESFEM
    oTo.w_COFATRIC = this.w_COFATRIC
    oTo.w_DESFRI = this.w_DESFRI
    oTo.w_CORATATT = this.w_CORATATT
    oTo.w_DESRAA = this.w_DESRAA
    oTo.w_CORATPAS = this.w_CORATPAS
    oTo.w_DESRAP = this.w_DESRAP
    oTo.w_CORISATT = this.w_CORISATT
    oTo.w_DESRIA = this.w_DESRIA
    oTo.w_CORISPAS = this.w_CORISPAS
    oTo.w_DESRIP = this.w_DESRIP
    oTo.w_COIVADEB = this.w_COIVADEB
    oTo.w_DESIVD = this.w_DESIVD
    oTo.w_TIVDEB = this.w_TIVDEB
    oTo.w_COIVACRE = this.w_COIVACRE
    oTo.w_DESIVC = this.w_DESIVC
    oTo.w_TIVCRE = this.w_TIVCRE
    oTo.w_COCREBRE = this.w_COCREBRE
    oTo.w_COCREMED = this.w_COCREMED
    oTo.w_CODEBBRE = this.w_CODEBBRE
    oTo.w_CODEBMED = this.w_CODEBMED
    oTo.w_DESCRB = this.w_DESCRB
    oTo.w_DESCRM = this.w_DESCRM
    oTo.w_DESDEB = this.w_DESDEB
    oTo.w_DESDEM = this.w_DESDEM
    oTo.w_COCONARR = this.w_COCONARR
    oTo.w_DESARR = this.w_DESARR
    oTo.w_COCAUFDE = this.w_COCAUFDE
    oTo.w_COCASFDE = this.w_COCASFDE
    oTo.w_COCAUFDR = this.w_COCAUFDR
    oTo.w_COCASFDR = this.w_COCASFDR
    oTo.w_COINDIVA = this.w_COINDIVA
    oTo.w_COCAURIM = this.w_COCAURIM
    oTo.w_COCASRIM = this.w_COCASRIM
    oTo.w_COCAURER = this.w_COCAURER
    oTo.w_COCASRER = this.w_COCASRER
    oTo.w_COCAFRER = this.w_COCAFRER
    oTo.w_COCAUCES = this.w_COCAUCES
    oTo.w_COCASCES = this.w_COCASCES
    oTo.w_DESRIM = this.w_DESRIM
    oTo.w_FLASSE = this.w_FLASSE
    oTo.w_DESRIMS = this.w_DESRIMS
    oTo.w_DESFDE = this.w_DESFDE
    oTo.w_DESFDES = this.w_DESFDES
    oTo.w_DESFDR = this.w_DESFDR
    oTo.w_DESFDRS = this.w_DESFDRS
    oTo.w_DESRER = this.w_DESRER
    oTo.w_DESRERS = this.w_DESRERS
    oTo.w_DESRERF = this.w_DESRERF
    oTo.w_DESCES = this.w_DESCES
    oTo.w_DESCESS = this.w_DESCESS
    oTo.w_FLASSE1 = this.w_FLASSE1
    oTo.w_FLASSE2 = this.w_FLASSE2
    oTo.w_FLASSE3 = this.w_FLASSE3
    oTo.w_FLASSE4 = this.w_FLASSE4
    oTo.w_FLASSE5 = this.w_FLASSE5
    oTo.w_FLASSE6 = this.w_FLASSE6
    oTo.w_FLASSE7 = this.w_FLASSE7
    oTo.w_FLASSE8 = this.w_FLASSE8
    oTo.w_FLASSE9 = this.w_FLASSE9
    oTo.w_FLASSE10 = this.w_FLASSE10
    PCContext::Load(oTo)
enddefine

define class tcgsar_ao3 as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 584
  Height = 445+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-04-16"
  HelpContextID=176781161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=73

  * --- Constant Properties
  CONTROPA_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  VOCIIVA_IDX = 0
  PAG_AMEN_IDX = 0
  AZIENDA_IDX = 0
  KEY_ARTI_IDX = 0
  cFile = "CONTROPA"
  cKeySelect = "COCODAZI"
  cKeyWhere  = "COCODAZI=this.w_COCODAZI"
  cKeyWhereODBC = '"COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cKeyWhereODBCqualified = '"CONTROPA.COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cPrg = "gsar_ao3"
  cComment = "Contropartite di assestamento"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COCODAZI = space(5)
  w_RAGAZI = space(40)
  w_COTIPCON = space(1)
  w_PERIVB = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_TIPART = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_COFATEME = space(15)
  w_DESFEM = space(40)
  w_COFATRIC = space(15)
  w_DESFRI = space(40)
  w_CORATATT = space(15)
  w_DESRAA = space(40)
  w_CORATPAS = space(15)
  w_DESRAP = space(40)
  w_CORISATT = space(15)
  w_DESRIA = space(40)
  w_CORISPAS = space(15)
  w_DESRIP = space(40)
  w_COIVADEB = space(15)
  w_DESIVD = space(40)
  w_TIVDEB = space(1)
  w_COIVACRE = space(15)
  w_DESIVC = space(40)
  w_TIVCRE = space(1)
  w_COCREBRE = space(15)
  w_COCREMED = space(15)
  w_CODEBBRE = space(15)
  w_CODEBMED = space(15)
  w_DESCRB = space(40)
  w_DESCRM = space(40)
  w_DESDEB = space(40)
  w_DESDEM = space(40)
  w_COCONARR = space(15)
  w_DESARR = space(40)
  w_COCAUFDE = space(5)
  w_COCASFDE = space(5)
  w_COCAUFDR = space(5)
  w_COCASFDR = space(5)
  w_COINDIVA = space(1)
  w_COCAURIM = space(5)
  w_COCASRIM = space(5)
  w_COCAURER = space(5)
  w_COCASRER = space(5)
  w_COCAFRER = space(5)
  w_COCAUCES = space(5)
  w_COCASCES = space(5)
  w_DESRIM = space(35)
  w_FLASSE = space(1)
  w_DESRIMS = space(35)
  w_DESFDE = space(35)
  w_DESFDES = space(35)
  w_DESFDR = space(35)
  w_DESFDRS = space(35)
  w_DESRER = space(35)
  w_DESRERS = space(35)
  w_DESRERF = space(35)
  w_DESCES = space(35)
  w_DESCESS = space(35)
  w_FLASSE1 = space(1)
  w_FLASSE2 = space(1)
  w_FLASSE3 = space(1)
  w_FLASSE4 = space(1)
  w_FLASSE5 = space(1)
  w_FLASSE6 = space(1)
  w_FLASSE7 = space(1)
  w_FLASSE8 = space(1)
  w_FLASSE9 = space(1)
  w_FLASSE10 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ao3Pag1","gsar_ao3",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Analisi di bilancio")
      .Pages(1).HelpContextID = 41212420
      .Pages(2).addobject("oPag","tgsar_ao3Pag2","gsar_ao3",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Scritture di assestamento - causali contabili")
      .Pages(2).HelpContextID = 75244566
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOFATEME_1_12
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsar_ao3
    * --- nMaxFieldsJoin di default vale 200
    this.parent.nMaxFieldsJoin = iif(lower(i_ServerConn[1,6])="oracle", 150, this.parent.nMaxFieldsJoin)
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='PAG_AMEN'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='CONTROPA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTROPA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTROPA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_ao3'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_32_joined
    link_1_32_joined=.f.
    local link_1_35_joined
    link_1_35_joined=.f.
    local link_1_38_joined
    link_1_38_joined=.f.
    local link_1_39_joined
    link_1_39_joined=.f.
    local link_1_40_joined
    link_1_40_joined=.f.
    local link_1_41_joined
    link_1_41_joined=.f.
    local link_1_52_joined
    link_1_52_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_2_8_joined
    link_2_8_joined=.f.
    local link_2_9_joined
    link_2_9_joined=.f.
    local link_2_10_joined
    link_2_10_joined=.f.
    local link_2_11_joined
    link_2_11_joined=.f.
    local link_2_12_joined
    link_2_12_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONTROPA where COCODAZI=KeySet.COCODAZI
    *
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTROPA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTROPA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTROPA '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_32_joined=this.AddJoinedLink_1_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_35_joined=this.AddJoinedLink_1_35(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_39_joined=this.AddJoinedLink_1_39(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_40_joined=this.AddJoinedLink_1_40(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_41_joined=this.AddJoinedLink_1_41(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_52_joined=this.AddJoinedLink_1_52(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_9_joined=this.AddJoinedLink_2_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_10_joined=this.AddJoinedLink_2_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_11_joined=this.AddJoinedLink_2_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_12_joined=this.AddJoinedLink_2_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RAGAZI = space(40)
        .w_PERIVB = 0
        .w_TIPART = space(1)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESFEM = space(40)
        .w_DESFRI = space(40)
        .w_DESRAA = space(40)
        .w_DESRAP = space(40)
        .w_DESRIA = space(40)
        .w_DESRIP = space(40)
        .w_DESIVD = space(40)
        .w_TIVDEB = space(1)
        .w_DESIVC = space(40)
        .w_TIVCRE = space(1)
        .w_DESCRB = space(40)
        .w_DESCRM = space(40)
        .w_DESDEB = space(40)
        .w_DESDEM = space(40)
        .w_DESARR = space(40)
        .w_DESRIM = space(35)
        .w_FLASSE = space(1)
        .w_DESRIMS = space(35)
        .w_DESFDE = space(35)
        .w_DESFDES = space(35)
        .w_DESFDR = space(35)
        .w_DESFDRS = space(35)
        .w_DESRER = space(35)
        .w_DESRERS = space(35)
        .w_DESRERF = space(35)
        .w_DESCES = space(35)
        .w_DESCESS = space(35)
        .w_FLASSE1 = space(1)
        .w_FLASSE2 = space(1)
        .w_FLASSE3 = space(1)
        .w_FLASSE4 = space(1)
        .w_FLASSE5 = space(1)
        .w_FLASSE6 = space(1)
        .w_FLASSE7 = space(1)
        .w_FLASSE8 = space(1)
        .w_FLASSE9 = space(1)
        .w_FLASSE10 = space(1)
        .w_COCODAZI = NVL(COCODAZI,space(5))
          if link_1_1_joined
            this.w_COCODAZI = NVL(AZCODAZI101,NVL(this.w_COCODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_COTIPCON = NVL(COTIPCON,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_COFATEME = NVL(COFATEME,space(15))
          if link_1_12_joined
            this.w_COFATEME = NVL(ANCODICE112,NVL(this.w_COFATEME,space(15)))
            this.w_DESFEM = NVL(ANDESCRI112,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO112),ctod("  /  /  "))
          else
          .link_1_12('Load')
          endif
        .w_COFATRIC = NVL(COFATRIC,space(15))
          if link_1_15_joined
            this.w_COFATRIC = NVL(ANCODICE115,NVL(this.w_COFATRIC,space(15)))
            this.w_DESFRI = NVL(ANDESCRI115,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO115),ctod("  /  /  "))
          else
          .link_1_15('Load')
          endif
        .w_CORATATT = NVL(CORATATT,space(15))
          if link_1_17_joined
            this.w_CORATATT = NVL(ANCODICE117,NVL(this.w_CORATATT,space(15)))
            this.w_DESRAA = NVL(ANDESCRI117,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO117),ctod("  /  /  "))
          else
          .link_1_17('Load')
          endif
        .w_CORATPAS = NVL(CORATPAS,space(15))
          if link_1_19_joined
            this.w_CORATPAS = NVL(ANCODICE119,NVL(this.w_CORATPAS,space(15)))
            this.w_DESRAP = NVL(ANDESCRI119,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO119),ctod("  /  /  "))
          else
          .link_1_19('Load')
          endif
        .w_CORISATT = NVL(CORISATT,space(15))
          if link_1_21_joined
            this.w_CORISATT = NVL(ANCODICE121,NVL(this.w_CORISATT,space(15)))
            this.w_DESRIA = NVL(ANDESCRI121,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO121),ctod("  /  /  "))
          else
          .link_1_21('Load')
          endif
        .w_CORISPAS = NVL(CORISPAS,space(15))
          if link_1_23_joined
            this.w_CORISPAS = NVL(ANCODICE123,NVL(this.w_CORISPAS,space(15)))
            this.w_DESRIP = NVL(ANDESCRI123,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO123),ctod("  /  /  "))
          else
          .link_1_23('Load')
          endif
        .w_COIVADEB = NVL(COIVADEB,space(15))
          if link_1_32_joined
            this.w_COIVADEB = NVL(ANCODICE132,NVL(this.w_COIVADEB,space(15)))
            this.w_DESIVD = NVL(ANDESCRI132,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO132),ctod("  /  /  "))
            this.w_TIVDEB = NVL(ANTIPSOT132,space(1))
          else
          .link_1_32('Load')
          endif
        .w_COIVACRE = NVL(COIVACRE,space(15))
          if link_1_35_joined
            this.w_COIVACRE = NVL(ANCODICE135,NVL(this.w_COIVACRE,space(15)))
            this.w_DESIVC = NVL(ANDESCRI135,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO135),ctod("  /  /  "))
            this.w_TIVCRE = NVL(ANTIPSOT135,space(1))
          else
          .link_1_35('Load')
          endif
        .w_COCREBRE = NVL(COCREBRE,space(15))
          if link_1_38_joined
            this.w_COCREBRE = NVL(ANCODICE138,NVL(this.w_COCREBRE,space(15)))
            this.w_DESCRB = NVL(ANDESCRI138,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO138),ctod("  /  /  "))
          else
          .link_1_38('Load')
          endif
        .w_COCREMED = NVL(COCREMED,space(15))
          if link_1_39_joined
            this.w_COCREMED = NVL(ANCODICE139,NVL(this.w_COCREMED,space(15)))
            this.w_DESCRM = NVL(ANDESCRI139,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO139),ctod("  /  /  "))
          else
          .link_1_39('Load')
          endif
        .w_CODEBBRE = NVL(CODEBBRE,space(15))
          if link_1_40_joined
            this.w_CODEBBRE = NVL(ANCODICE140,NVL(this.w_CODEBBRE,space(15)))
            this.w_DESDEB = NVL(ANDESCRI140,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO140),ctod("  /  /  "))
          else
          .link_1_40('Load')
          endif
        .w_CODEBMED = NVL(CODEBMED,space(15))
          if link_1_41_joined
            this.w_CODEBMED = NVL(ANCODICE141,NVL(this.w_CODEBMED,space(15)))
            this.w_DESDEM = NVL(ANDESCRI141,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO141),ctod("  /  /  "))
          else
          .link_1_41('Load')
          endif
        .w_COCONARR = NVL(COCONARR,space(15))
          if link_1_52_joined
            this.w_COCONARR = NVL(ANCODICE152,NVL(this.w_COCONARR,space(15)))
            this.w_DESARR = NVL(ANDESCRI152,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO152),ctod("  /  /  "))
          else
          .link_1_52('Load')
          endif
        .w_COCAUFDE = NVL(COCAUFDE,space(5))
          if link_2_1_joined
            this.w_COCAUFDE = NVL(CCCODICE201,NVL(this.w_COCAUFDE,space(5)))
            this.w_DESFDE = NVL(CCDESCRI201,space(35))
            this.w_FLASSE = NVL(CCFLASSE201,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO201),ctod("  /  /  "))
          else
          .link_2_1('Load')
          endif
        .w_COCASFDE = NVL(COCASFDE,space(5))
          if link_2_2_joined
            this.w_COCASFDE = NVL(CCCODICE202,NVL(this.w_COCASFDE,space(5)))
            this.w_DESFDES = NVL(CCDESCRI202,space(35))
            this.w_FLASSE2 = NVL(CCFLASSE202,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO202),ctod("  /  /  "))
          else
          .link_2_2('Load')
          endif
        .w_COCAUFDR = NVL(COCAUFDR,space(5))
          if link_2_3_joined
            this.w_COCAUFDR = NVL(CCCODICE203,NVL(this.w_COCAUFDR,space(5)))
            this.w_DESFDR = NVL(CCDESCRI203,space(35))
            this.w_FLASSE3 = NVL(CCFLASSE203,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO203),ctod("  /  /  "))
          else
          .link_2_3('Load')
          endif
        .w_COCASFDR = NVL(COCASFDR,space(5))
          if link_2_4_joined
            this.w_COCASFDR = NVL(CCCODICE204,NVL(this.w_COCASFDR,space(5)))
            this.w_DESFDRS = NVL(CCDESCRI204,space(35))
            this.w_FLASSE4 = NVL(CCFLASSE204,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO204),ctod("  /  /  "))
          else
          .link_2_4('Load')
          endif
        .w_COINDIVA = NVL(COINDIVA,space(1))
        .w_COCAURIM = NVL(COCAURIM,space(5))
          if link_2_6_joined
            this.w_COCAURIM = NVL(CCCODICE206,NVL(this.w_COCAURIM,space(5)))
            this.w_DESRIM = NVL(CCDESCRI206,space(35))
            this.w_FLASSE5 = NVL(CCFLASSE206,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO206),ctod("  /  /  "))
          else
          .link_2_6('Load')
          endif
        .w_COCASRIM = NVL(COCASRIM,space(5))
          if link_2_7_joined
            this.w_COCASRIM = NVL(CCCODICE207,NVL(this.w_COCASRIM,space(5)))
            this.w_DESRIMS = NVL(CCDESCRI207,space(35))
            this.w_FLASSE6 = NVL(CCFLASSE207,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO207),ctod("  /  /  "))
          else
          .link_2_7('Load')
          endif
        .w_COCAURER = NVL(COCAURER,space(5))
          if link_2_8_joined
            this.w_COCAURER = NVL(CCCODICE208,NVL(this.w_COCAURER,space(5)))
            this.w_DESRER = NVL(CCDESCRI208,space(35))
            this.w_FLASSE7 = NVL(CCFLASSE208,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO208),ctod("  /  /  "))
          else
          .link_2_8('Load')
          endif
        .w_COCASRER = NVL(COCASRER,space(5))
          if link_2_9_joined
            this.w_COCASRER = NVL(CCCODICE209,NVL(this.w_COCASRER,space(5)))
            this.w_DESRERS = NVL(CCDESCRI209,space(35))
            this.w_FLASSE8 = NVL(CCFLASSE209,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO209),ctod("  /  /  "))
          else
          .link_2_9('Load')
          endif
        .w_COCAFRER = NVL(COCAFRER,space(5))
          if link_2_10_joined
            this.w_COCAFRER = NVL(CCCODICE210,NVL(this.w_COCAFRER,space(5)))
            this.w_DESRERF = NVL(CCDESCRI210,space(35))
            this.w_FLASSE1 = NVL(CCFLASSE210,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO210),ctod("  /  /  "))
          else
          .link_2_10('Load')
          endif
        .w_COCAUCES = NVL(COCAUCES,space(5))
          if link_2_11_joined
            this.w_COCAUCES = NVL(CCCODICE211,NVL(this.w_COCAUCES,space(5)))
            this.w_DESCES = NVL(CCDESCRI211,space(35))
            this.w_FLASSE9 = NVL(CCFLASSE211,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO211),ctod("  /  /  "))
          else
          .link_2_11('Load')
          endif
        .w_COCASCES = NVL(COCASCES,space(5))
          if link_2_12_joined
            this.w_COCASCES = NVL(CCCODICE212,NVL(this.w_COCASCES,space(5)))
            this.w_DESCESS = NVL(CCDESCRI212,space(35))
            this.w_FLASSE10 = NVL(CCFLASSE212,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO212),ctod("  /  /  "))
          else
          .link_2_12('Load')
          endif
        cp_LoadRecExtFlds(this,'CONTROPA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_14.enabled = this.oPgFrm.Page2.oPag.oBtn_2_14.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_COCODAZI = space(5)
      .w_RAGAZI = space(40)
      .w_COTIPCON = space(1)
      .w_PERIVB = 0
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_TIPART = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_COFATEME = space(15)
      .w_DESFEM = space(40)
      .w_COFATRIC = space(15)
      .w_DESFRI = space(40)
      .w_CORATATT = space(15)
      .w_DESRAA = space(40)
      .w_CORATPAS = space(15)
      .w_DESRAP = space(40)
      .w_CORISATT = space(15)
      .w_DESRIA = space(40)
      .w_CORISPAS = space(15)
      .w_DESRIP = space(40)
      .w_COIVADEB = space(15)
      .w_DESIVD = space(40)
      .w_TIVDEB = space(1)
      .w_COIVACRE = space(15)
      .w_DESIVC = space(40)
      .w_TIVCRE = space(1)
      .w_COCREBRE = space(15)
      .w_COCREMED = space(15)
      .w_CODEBBRE = space(15)
      .w_CODEBMED = space(15)
      .w_DESCRB = space(40)
      .w_DESCRM = space(40)
      .w_DESDEB = space(40)
      .w_DESDEM = space(40)
      .w_COCONARR = space(15)
      .w_DESARR = space(40)
      .w_COCAUFDE = space(5)
      .w_COCASFDE = space(5)
      .w_COCAUFDR = space(5)
      .w_COCASFDR = space(5)
      .w_COINDIVA = space(1)
      .w_COCAURIM = space(5)
      .w_COCASRIM = space(5)
      .w_COCAURER = space(5)
      .w_COCASRER = space(5)
      .w_COCAFRER = space(5)
      .w_COCAUCES = space(5)
      .w_COCASCES = space(5)
      .w_DESRIM = space(35)
      .w_FLASSE = space(1)
      .w_DESRIMS = space(35)
      .w_DESFDE = space(35)
      .w_DESFDES = space(35)
      .w_DESFDR = space(35)
      .w_DESFDRS = space(35)
      .w_DESRER = space(35)
      .w_DESRERS = space(35)
      .w_DESRERF = space(35)
      .w_DESCES = space(35)
      .w_DESCESS = space(35)
      .w_FLASSE1 = space(1)
      .w_FLASSE2 = space(1)
      .w_FLASSE3 = space(1)
      .w_FLASSE4 = space(1)
      .w_FLASSE5 = space(1)
      .w_FLASSE6 = space(1)
      .w_FLASSE7 = space(1)
      .w_FLASSE8 = space(1)
      .w_FLASSE9 = space(1)
      .w_FLASSE10 = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_COCODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_COTIPCON = 'G'
          .DoRTCalc(4,9,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(11,12,.f.)
          if not(empty(.w_COFATEME))
          .link_1_12('Full')
          endif
        .DoRTCalc(13,14,.f.)
          if not(empty(.w_COFATRIC))
          .link_1_15('Full')
          endif
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_CORATATT))
          .link_1_17('Full')
          endif
        .DoRTCalc(17,18,.f.)
          if not(empty(.w_CORATPAS))
          .link_1_19('Full')
          endif
        .DoRTCalc(19,20,.f.)
          if not(empty(.w_CORISATT))
          .link_1_21('Full')
          endif
        .DoRTCalc(21,22,.f.)
          if not(empty(.w_CORISPAS))
          .link_1_23('Full')
          endif
        .DoRTCalc(23,24,.f.)
          if not(empty(.w_COIVADEB))
          .link_1_32('Full')
          endif
        .DoRTCalc(25,27,.f.)
          if not(empty(.w_COIVACRE))
          .link_1_35('Full')
          endif
        .DoRTCalc(28,30,.f.)
          if not(empty(.w_COCREBRE))
          .link_1_38('Full')
          endif
        .DoRTCalc(31,31,.f.)
          if not(empty(.w_COCREMED))
          .link_1_39('Full')
          endif
        .DoRTCalc(32,32,.f.)
          if not(empty(.w_CODEBBRE))
          .link_1_40('Full')
          endif
        .DoRTCalc(33,33,.f.)
          if not(empty(.w_CODEBMED))
          .link_1_41('Full')
          endif
        .DoRTCalc(34,38,.f.)
          if not(empty(.w_COCONARR))
          .link_1_52('Full')
          endif
        .DoRTCalc(39,40,.f.)
          if not(empty(.w_COCAUFDE))
          .link_2_1('Full')
          endif
        .DoRTCalc(41,41,.f.)
          if not(empty(.w_COCASFDE))
          .link_2_2('Full')
          endif
        .DoRTCalc(42,42,.f.)
          if not(empty(.w_COCAUFDR))
          .link_2_3('Full')
          endif
        .DoRTCalc(43,43,.f.)
          if not(empty(.w_COCASFDR))
          .link_2_4('Full')
          endif
        .DoRTCalc(44,45,.f.)
          if not(empty(.w_COCAURIM))
          .link_2_6('Full')
          endif
        .DoRTCalc(46,46,.f.)
          if not(empty(.w_COCASRIM))
          .link_2_7('Full')
          endif
        .DoRTCalc(47,47,.f.)
          if not(empty(.w_COCAURER))
          .link_2_8('Full')
          endif
        .DoRTCalc(48,48,.f.)
          if not(empty(.w_COCASRER))
          .link_2_9('Full')
          endif
        .DoRTCalc(49,49,.f.)
          if not(empty(.w_COCAFRER))
          .link_2_10('Full')
          endif
        .DoRTCalc(50,50,.f.)
          if not(empty(.w_COCAUCES))
          .link_2_11('Full')
          endif
        .DoRTCalc(51,51,.f.)
          if not(empty(.w_COCASCES))
          .link_2_12('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTROPA')
    this.DoRTCalc(52,73,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_14.enabled = this.oPgFrm.Page2.oPag.oBtn_2_14.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCOFATEME_1_12.enabled = i_bVal
      .Page1.oPag.oCOFATRIC_1_15.enabled = i_bVal
      .Page1.oPag.oCORATATT_1_17.enabled = i_bVal
      .Page1.oPag.oCORATPAS_1_19.enabled = i_bVal
      .Page1.oPag.oCORISATT_1_21.enabled = i_bVal
      .Page1.oPag.oCORISPAS_1_23.enabled = i_bVal
      .Page1.oPag.oCOIVADEB_1_32.enabled = i_bVal
      .Page1.oPag.oCOIVACRE_1_35.enabled = i_bVal
      .Page1.oPag.oCOCREBRE_1_38.enabled = i_bVal
      .Page1.oPag.oCOCREMED_1_39.enabled = i_bVal
      .Page1.oPag.oCODEBBRE_1_40.enabled = i_bVal
      .Page1.oPag.oCODEBMED_1_41.enabled = i_bVal
      .Page1.oPag.oCOCONARR_1_52.enabled = i_bVal
      .Page2.oPag.oCOCAUFDE_2_1.enabled = i_bVal
      .Page2.oPag.oCOCASFDE_2_2.enabled = i_bVal
      .Page2.oPag.oCOCAUFDR_2_3.enabled = i_bVal
      .Page2.oPag.oCOCASFDR_2_4.enabled = i_bVal
      .Page2.oPag.oCOINDIVA_2_5.enabled = i_bVal
      .Page2.oPag.oCOCAURIM_2_6.enabled = i_bVal
      .Page2.oPag.oCOCASRIM_2_7.enabled = i_bVal
      .Page2.oPag.oCOCAURER_2_8.enabled = i_bVal
      .Page2.oPag.oCOCASRER_2_9.enabled = i_bVal
      .Page2.oPag.oCOCAFRER_2_10.enabled = i_bVal
      .Page2.oPag.oCOCAUCES_2_11.enabled = i_bVal
      .Page2.oPag.oCOCASCES_2_12.enabled = i_bVal
      .Page1.oPag.oBtn_1_43.enabled = i_bVal
      .Page1.oPag.oBtn_1_45.enabled = .Page1.oPag.oBtn_1_45.mCond()
      .Page2.oPag.oBtn_2_13.enabled = i_bVal
      .Page2.oPag.oBtn_2_14.enabled = .Page2.oPag.oBtn_2_14.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'CONTROPA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODAZI,"COCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPCON,"COTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFATEME,"COFATEME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFATRIC,"COFATRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CORATATT,"CORATATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CORATPAS,"CORATPAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CORISATT,"CORISATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CORISPAS,"CORISPAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COIVADEB,"COIVADEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COIVACRE,"COIVACRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCREBRE,"COCREBRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCREMED,"COCREMED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODEBBRE,"CODEBBRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODEBMED,"CODEBMED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCONARR,"COCONARR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAUFDE,"COCAUFDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCASFDE,"COCASFDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAUFDR,"COCAUFDR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCASFDR,"COCASFDR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COINDIVA,"COINDIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAURIM,"COCAURIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCASRIM,"COCASRIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAURER,"COCAURER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCASRER,"COCASRER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAFRER,"COCAFRER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAUCES,"COCAUCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCASCES,"COCASCES",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONTROPA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONTROPA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTROPA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(COCODAZI,COTIPCON,UTCC,UTCV,UTDC"+;
                  ",UTDV,COFATEME,COFATRIC,CORATATT,CORATPAS"+;
                  ",CORISATT,CORISPAS,COIVADEB,COIVACRE,COCREBRE"+;
                  ",COCREMED,CODEBBRE,CODEBMED,COCONARR,COCAUFDE"+;
                  ",COCASFDE,COCAUFDR,COCASFDR,COINDIVA,COCAURIM"+;
                  ",COCASRIM,COCAURER,COCASRER,COCAFRER,COCAUCES"+;
                  ",COCASCES "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_COCODAZI)+;
                  ","+cp_ToStrODBC(this.w_COTIPCON)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBCNull(this.w_COFATEME)+;
                  ","+cp_ToStrODBCNull(this.w_COFATRIC)+;
                  ","+cp_ToStrODBCNull(this.w_CORATATT)+;
                  ","+cp_ToStrODBCNull(this.w_CORATPAS)+;
                  ","+cp_ToStrODBCNull(this.w_CORISATT)+;
                  ","+cp_ToStrODBCNull(this.w_CORISPAS)+;
                  ","+cp_ToStrODBCNull(this.w_COIVADEB)+;
                  ","+cp_ToStrODBCNull(this.w_COIVACRE)+;
                  ","+cp_ToStrODBCNull(this.w_COCREBRE)+;
                  ","+cp_ToStrODBCNull(this.w_COCREMED)+;
                  ","+cp_ToStrODBCNull(this.w_CODEBBRE)+;
                  ","+cp_ToStrODBCNull(this.w_CODEBMED)+;
                  ","+cp_ToStrODBCNull(this.w_COCONARR)+;
                  ","+cp_ToStrODBCNull(this.w_COCAUFDE)+;
                  ","+cp_ToStrODBCNull(this.w_COCASFDE)+;
                  ","+cp_ToStrODBCNull(this.w_COCAUFDR)+;
                  ","+cp_ToStrODBCNull(this.w_COCASFDR)+;
                  ","+cp_ToStrODBC(this.w_COINDIVA)+;
                  ","+cp_ToStrODBCNull(this.w_COCAURIM)+;
                  ","+cp_ToStrODBCNull(this.w_COCASRIM)+;
                  ","+cp_ToStrODBCNull(this.w_COCAURER)+;
                  ","+cp_ToStrODBCNull(this.w_COCASRER)+;
                  ","+cp_ToStrODBCNull(this.w_COCAFRER)+;
                  ","+cp_ToStrODBCNull(this.w_COCAUCES)+;
                  ","+cp_ToStrODBCNull(this.w_COCASCES)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTROPA')
        cp_CheckDeletedKey(i_cTable,0,'COCODAZI',this.w_COCODAZI)
        INSERT INTO (i_cTable);
              (COCODAZI,COTIPCON,UTCC,UTCV,UTDC,UTDV,COFATEME,COFATRIC,CORATATT,CORATPAS,CORISATT,CORISPAS,COIVADEB,COIVACRE,COCREBRE,COCREMED,CODEBBRE,CODEBMED,COCONARR,COCAUFDE,COCASFDE,COCAUFDR,COCASFDR,COINDIVA,COCAURIM,COCASRIM,COCAURER,COCASRER,COCAFRER,COCAUCES,COCASCES  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_COCODAZI;
                  ,this.w_COTIPCON;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_COFATEME;
                  ,this.w_COFATRIC;
                  ,this.w_CORATATT;
                  ,this.w_CORATPAS;
                  ,this.w_CORISATT;
                  ,this.w_CORISPAS;
                  ,this.w_COIVADEB;
                  ,this.w_COIVACRE;
                  ,this.w_COCREBRE;
                  ,this.w_COCREMED;
                  ,this.w_CODEBBRE;
                  ,this.w_CODEBMED;
                  ,this.w_COCONARR;
                  ,this.w_COCAUFDE;
                  ,this.w_COCASFDE;
                  ,this.w_COCAUFDR;
                  ,this.w_COCASFDR;
                  ,this.w_COINDIVA;
                  ,this.w_COCAURIM;
                  ,this.w_COCASRIM;
                  ,this.w_COCAURER;
                  ,this.w_COCASRER;
                  ,this.w_COCAFRER;
                  ,this.w_COCAUCES;
                  ,this.w_COCASCES;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONTROPA_IDX,i_nConn)
      *
      * update CONTROPA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONTROPA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " COTIPCON="+cp_ToStrODBC(this.w_COTIPCON)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",COFATEME="+cp_ToStrODBCNull(this.w_COFATEME)+;
             ",COFATRIC="+cp_ToStrODBCNull(this.w_COFATRIC)+;
             ",CORATATT="+cp_ToStrODBCNull(this.w_CORATATT)+;
             ",CORATPAS="+cp_ToStrODBCNull(this.w_CORATPAS)+;
             ",CORISATT="+cp_ToStrODBCNull(this.w_CORISATT)+;
             ",CORISPAS="+cp_ToStrODBCNull(this.w_CORISPAS)+;
             ",COIVADEB="+cp_ToStrODBCNull(this.w_COIVADEB)+;
             ",COIVACRE="+cp_ToStrODBCNull(this.w_COIVACRE)+;
             ",COCREBRE="+cp_ToStrODBCNull(this.w_COCREBRE)+;
             ",COCREMED="+cp_ToStrODBCNull(this.w_COCREMED)+;
             ",CODEBBRE="+cp_ToStrODBCNull(this.w_CODEBBRE)+;
             ",CODEBMED="+cp_ToStrODBCNull(this.w_CODEBMED)+;
             ",COCONARR="+cp_ToStrODBCNull(this.w_COCONARR)+;
             ",COCAUFDE="+cp_ToStrODBCNull(this.w_COCAUFDE)+;
             ",COCASFDE="+cp_ToStrODBCNull(this.w_COCASFDE)+;
             ",COCAUFDR="+cp_ToStrODBCNull(this.w_COCAUFDR)+;
             ",COCASFDR="+cp_ToStrODBCNull(this.w_COCASFDR)+;
             ",COINDIVA="+cp_ToStrODBC(this.w_COINDIVA)+;
             ",COCAURIM="+cp_ToStrODBCNull(this.w_COCAURIM)+;
             ",COCASRIM="+cp_ToStrODBCNull(this.w_COCASRIM)+;
             ",COCAURER="+cp_ToStrODBCNull(this.w_COCAURER)+;
             ",COCASRER="+cp_ToStrODBCNull(this.w_COCASRER)+;
             ",COCAFRER="+cp_ToStrODBCNull(this.w_COCAFRER)+;
             ",COCAUCES="+cp_ToStrODBCNull(this.w_COCAUCES)+;
             ",COCASCES="+cp_ToStrODBCNull(this.w_COCASCES)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONTROPA')
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        UPDATE (i_cTable) SET;
              COTIPCON=this.w_COTIPCON;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,COFATEME=this.w_COFATEME;
             ,COFATRIC=this.w_COFATRIC;
             ,CORATATT=this.w_CORATATT;
             ,CORATPAS=this.w_CORATPAS;
             ,CORISATT=this.w_CORISATT;
             ,CORISPAS=this.w_CORISPAS;
             ,COIVADEB=this.w_COIVADEB;
             ,COIVACRE=this.w_COIVACRE;
             ,COCREBRE=this.w_COCREBRE;
             ,COCREMED=this.w_COCREMED;
             ,CODEBBRE=this.w_CODEBBRE;
             ,CODEBMED=this.w_CODEBMED;
             ,COCONARR=this.w_COCONARR;
             ,COCAUFDE=this.w_COCAUFDE;
             ,COCASFDE=this.w_COCASFDE;
             ,COCAUFDR=this.w_COCAUFDR;
             ,COCASFDR=this.w_COCASFDR;
             ,COINDIVA=this.w_COINDIVA;
             ,COCAURIM=this.w_COCAURIM;
             ,COCASRIM=this.w_COCASRIM;
             ,COCAURER=this.w_COCAURER;
             ,COCASRER=this.w_COCASRER;
             ,COCAFRER=this.w_COCAFRER;
             ,COCAUCES=this.w_COCAUCES;
             ,COCASCES=this.w_COCASCES;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsar_ao3
    * Valorizzo l'esito delle modifiche sul database
    * per lanciare o meno SETT_AMB alla Done di GSAR_K03
    This.oParentObject.Call_Sett_Amb=Not bTrsErr
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONTROPA_IDX,i_nConn)
      *
      * delete CONTROPA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
            .w_COTIPCON = 'G'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,73,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oCOCAURIM_2_6.enabled = this.oPgFrm.Page2.oPag.oCOCAURIM_2_6.mCond()
    this.oPgFrm.Page2.oPag.oCOCASRIM_2_7.enabled = this.oPgFrm.Page2.oPag.oCOCASRIM_2_7.mCond()
    this.oPgFrm.Page2.oPag.oCOCAUCES_2_11.enabled = this.oPgFrm.Page2.oPag.oCOCAUCES_2_11.mCond()
    this.oPgFrm.Page2.oPag.oCOCASCES_2_12.enabled = this.oPgFrm.Page2.oPag.oCOCASCES_2_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_COCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_COCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on CONTROPA.COCODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and CONTROPA.COCODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COFATEME
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COFATEME) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COFATEME)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COFATEME))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COFATEME)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COFATEME)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COFATEME)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COFATEME) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOFATEME_1_12'),i_cWhere,'GSAR_BZC',"Conti fatture da emettere",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COFATEME)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COFATEME);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COFATEME)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COFATEME = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFEM = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COFATEME = space(15)
      endif
      this.w_DESFEM = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COFATEME = space(15)
        this.w_DESFEM = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COFATEME Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.ANCODICE as ANCODICE112"+ ",link_1_12.ANDESCRI as ANDESCRI112"+ ",link_1_12.ANDTOBSO as ANDTOBSO112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on CONTROPA.COFATEME=link_1_12.ANCODICE"+" and CONTROPA.COTIPCON=link_1_12.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and CONTROPA.COFATEME=link_1_12.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_12.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COFATRIC
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COFATRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COFATRIC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COFATRIC))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COFATRIC)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COFATRIC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COFATRIC)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COFATRIC) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOFATRIC_1_15'),i_cWhere,'GSAR_BZC',"Conti fatture ricevute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COFATRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COFATRIC);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COFATRIC)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COFATRIC = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COFATRIC = space(15)
      endif
      this.w_DESFRI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COFATRIC = space(15)
        this.w_DESFRI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COFATRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.ANCODICE as ANCODICE115"+ ",link_1_15.ANDESCRI as ANDESCRI115"+ ",link_1_15.ANDTOBSO as ANDTOBSO115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on CONTROPA.COFATRIC=link_1_15.ANCODICE"+" and CONTROPA.COTIPCON=link_1_15.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and CONTROPA.COFATRIC=link_1_15.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_15.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CORATATT
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CORATATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CORATATT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_CORATATT))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CORATATT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CORATATT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CORATATT)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CORATATT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCORATATT_1_17'),i_cWhere,'GSAR_BZC',"Conti ratei attivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CORATATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CORATATT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_CORATATT)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CORATATT = NVL(_Link_.ANCODICE,space(15))
      this.w_DESRAA = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CORATATT = space(15)
      endif
      this.w_DESRAA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_CORATATT = space(15)
        this.w_DESRAA = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CORATATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.ANCODICE as ANCODICE117"+ ",link_1_17.ANDESCRI as ANDESCRI117"+ ",link_1_17.ANDTOBSO as ANDTOBSO117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on CONTROPA.CORATATT=link_1_17.ANCODICE"+" and CONTROPA.COTIPCON=link_1_17.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and CONTROPA.CORATATT=link_1_17.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_17.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CORATPAS
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CORATPAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CORATPAS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_CORATPAS))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CORATPAS)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CORATPAS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CORATPAS)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CORATPAS) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCORATPAS_1_19'),i_cWhere,'GSAR_BZC',"Conti ratei passivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CORATPAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CORATPAS);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_CORATPAS)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CORATPAS = NVL(_Link_.ANCODICE,space(15))
      this.w_DESRAP = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CORATPAS = space(15)
      endif
      this.w_DESRAP = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_CORATPAS = space(15)
        this.w_DESRAP = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CORATPAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.ANCODICE as ANCODICE119"+ ",link_1_19.ANDESCRI as ANDESCRI119"+ ",link_1_19.ANDTOBSO as ANDTOBSO119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on CONTROPA.CORATPAS=link_1_19.ANCODICE"+" and CONTROPA.COTIPCON=link_1_19.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and CONTROPA.CORATPAS=link_1_19.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_19.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CORISATT
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CORISATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CORISATT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_CORISATT))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CORISATT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CORISATT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CORISATT)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CORISATT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCORISATT_1_21'),i_cWhere,'GSAR_BZC',"Conti risconti attivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CORISATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CORISATT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_CORISATT)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CORISATT = NVL(_Link_.ANCODICE,space(15))
      this.w_DESRIA = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CORISATT = space(15)
      endif
      this.w_DESRIA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_CORISATT = space(15)
        this.w_DESRIA = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CORISATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.ANCODICE as ANCODICE121"+ ",link_1_21.ANDESCRI as ANDESCRI121"+ ",link_1_21.ANDTOBSO as ANDTOBSO121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on CONTROPA.CORISATT=link_1_21.ANCODICE"+" and CONTROPA.COTIPCON=link_1_21.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and CONTROPA.CORISATT=link_1_21.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_21.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CORISPAS
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CORISPAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CORISPAS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_CORISPAS))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CORISPAS)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CORISPAS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CORISPAS)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CORISPAS) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCORISPAS_1_23'),i_cWhere,'GSAR_BZC',"Conti risconti passivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CORISPAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CORISPAS);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_CORISPAS)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CORISPAS = NVL(_Link_.ANCODICE,space(15))
      this.w_DESRIP = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CORISPAS = space(15)
      endif
      this.w_DESRIP = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_CORISPAS = space(15)
        this.w_DESRIP = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CORISPAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.ANCODICE as ANCODICE123"+ ",link_1_23.ANDESCRI as ANDESCRI123"+ ",link_1_23.ANDTOBSO as ANDTOBSO123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on CONTROPA.CORISPAS=link_1_23.ANCODICE"+" and CONTROPA.COTIPCON=link_1_23.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and CONTROPA.CORISPAS=link_1_23.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_23.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COIVADEB
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COIVADEB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COIVADEB)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COIVADEB))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COIVADEB)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COIVADEB)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COIVADEB)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COIVADEB) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOIVADEB_1_32'),i_cWhere,'GSAR_BZC',"Conti IVA",'CONTIZOOMIVA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente o no IVA oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COIVADEB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COIVADEB);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COIVADEB)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COIVADEB = NVL(_Link_.ANCODICE,space(15))
      this.w_DESIVD = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_TIVDEB = NVL(_Link_.ANTIPSOT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COIVADEB = space(15)
      endif
      this.w_DESIVD = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIVDEB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_TIVDEB='I' OR EMPTY(.w_COIVADEB))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente o no IVA oppure obsoleto")
        endif
        this.w_COIVADEB = space(15)
        this.w_DESIVD = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIVDEB = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COIVADEB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_32.ANCODICE as ANCODICE132"+ ",link_1_32.ANDESCRI as ANDESCRI132"+ ",link_1_32.ANDTOBSO as ANDTOBSO132"+ ",link_1_32.ANTIPSOT as ANTIPSOT132"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_32 on CONTROPA.COIVADEB=link_1_32.ANCODICE"+" and CONTROPA.COTIPCON=link_1_32.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_32"
          i_cKey=i_cKey+'+" and CONTROPA.COIVADEB=link_1_32.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_32.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COIVACRE
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COIVACRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COIVACRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COIVACRE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COIVACRE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COIVACRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COIVACRE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COIVACRE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOIVACRE_1_35'),i_cWhere,'GSAR_BZC',"Conti IVA",'CONTIZOOMIVA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente o no IVA oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COIVACRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COIVACRE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COIVACRE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COIVACRE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESIVC = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_TIVCRE = NVL(_Link_.ANTIPSOT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COIVACRE = space(15)
      endif
      this.w_DESIVC = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIVCRE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_TIVCRE='I' OR EMPTY(.w_COIVACRE))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente o no IVA oppure obsoleto")
        endif
        this.w_COIVACRE = space(15)
        this.w_DESIVC = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIVCRE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COIVACRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_35(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_35.ANCODICE as ANCODICE135"+ ",link_1_35.ANDESCRI as ANDESCRI135"+ ",link_1_35.ANDTOBSO as ANDTOBSO135"+ ",link_1_35.ANTIPSOT as ANTIPSOT135"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_35 on CONTROPA.COIVACRE=link_1_35.ANCODICE"+" and CONTROPA.COTIPCON=link_1_35.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_35"
          i_cKey=i_cKey+'+" and CONTROPA.COIVACRE=link_1_35.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_35.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCREBRE
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCREBRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCREBRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCREBRE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCREBRE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCREBRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCREBRE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCREBRE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCREBRE_1_38'),i_cWhere,'GSAR_BZC',"Conti crediti a breve",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCREBRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCREBRE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCREBRE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCREBRE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRB = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCREBRE = space(15)
      endif
      this.w_DESCRB = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COCREBRE = space(15)
        this.w_DESCRB = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCREBRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.ANCODICE as ANCODICE138"+ ",link_1_38.ANDESCRI as ANDESCRI138"+ ",link_1_38.ANDTOBSO as ANDTOBSO138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on CONTROPA.COCREBRE=link_1_38.ANCODICE"+" and CONTROPA.COTIPCON=link_1_38.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and CONTROPA.COCREBRE=link_1_38.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_38.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCREMED
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCREMED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCREMED)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCREMED))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCREMED)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCREMED)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCREMED)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCREMED) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCREMED_1_39'),i_cWhere,'GSAR_BZC',"Conti crediti a medio/lungo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCREMED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCREMED);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCREMED)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCREMED = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRM = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCREMED = space(15)
      endif
      this.w_DESCRM = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COCREMED = space(15)
        this.w_DESCRM = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCREMED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_39(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_39.ANCODICE as ANCODICE139"+ ",link_1_39.ANDESCRI as ANDESCRI139"+ ",link_1_39.ANDTOBSO as ANDTOBSO139"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_39 on CONTROPA.COCREMED=link_1_39.ANCODICE"+" and CONTROPA.COTIPCON=link_1_39.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_39"
          i_cKey=i_cKey+'+" and CONTROPA.COCREMED=link_1_39.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_39.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODEBBRE
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODEBBRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODEBBRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_CODEBBRE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODEBBRE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODEBBRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODEBBRE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODEBBRE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODEBBRE_1_40'),i_cWhere,'GSAR_BZC',"Conti debiti a breve",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODEBBRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODEBBRE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_CODEBBRE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODEBBRE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDEB = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODEBBRE = space(15)
      endif
      this.w_DESDEB = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_CODEBBRE = space(15)
        this.w_DESDEB = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODEBBRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_40(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_40.ANCODICE as ANCODICE140"+ ",link_1_40.ANDESCRI as ANDESCRI140"+ ",link_1_40.ANDTOBSO as ANDTOBSO140"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_40 on CONTROPA.CODEBBRE=link_1_40.ANCODICE"+" and CONTROPA.COTIPCON=link_1_40.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_40"
          i_cKey=i_cKey+'+" and CONTROPA.CODEBBRE=link_1_40.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_40.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODEBMED
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODEBMED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODEBMED)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_CODEBMED))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODEBMED)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODEBMED)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODEBMED)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODEBMED) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODEBMED_1_41'),i_cWhere,'GSAR_BZC',"Conti debiti a medio/lungo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODEBMED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODEBMED);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_CODEBMED)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODEBMED = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDEM = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODEBMED = space(15)
      endif
      this.w_DESDEM = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_CODEBMED = space(15)
        this.w_DESDEM = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODEBMED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_41(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_41.ANCODICE as ANCODICE141"+ ",link_1_41.ANDESCRI as ANDESCRI141"+ ",link_1_41.ANDTOBSO as ANDTOBSO141"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_41 on CONTROPA.CODEBMED=link_1_41.ANCODICE"+" and CONTROPA.COTIPCON=link_1_41.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_41"
          i_cKey=i_cKey+'+" and CONTROPA.CODEBMED=link_1_41.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_41.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCONARR
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCONARR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCONARR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCONARR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCONARR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCONARR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCONARR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCONARR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCONARR_1_52'),i_cWhere,'GSAR_BZC',"Conti debiti a medio/lungo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCONARR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCONARR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCONARR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCONARR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESARR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCONARR = space(15)
      endif
      this.w_DESARR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COCONARR = space(15)
        this.w_DESARR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCONARR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_52(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_52.ANCODICE as ANCODICE152"+ ",link_1_52.ANDESCRI as ANDESCRI152"+ ",link_1_52.ANDTOBSO as ANDTOBSO152"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_52 on CONTROPA.COCONARR=link_1_52.ANCODICE"+" and CONTROPA.COTIPCON=link_1_52.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_52"
          i_cKey=i_cKey+'+" and CONTROPA.COCONARR=link_1_52.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_52.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAUFDE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAUFDE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAUFDE)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAUFDE))
          select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAUFDE)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCAUFDE)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCAUFDE)+"%");

            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCAUFDE) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAUFDE_2_1'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAUFDE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAUFDE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAUFDE)
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAUFDE = NVL(_Link_.CCCODICE,space(5))
      this.w_DESFDE = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLASSE = NVL(_Link_.CCFLASSE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAUFDE = space(5)
      endif
      this.w_DESFDE = space(35)
      this.w_FLASSE = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE='S') OR EMPTY(.w_COCAUFDE)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
        endif
        this.w_COCAUFDE = space(5)
        this.w_DESFDE = space(35)
        this.w_FLASSE = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAUFDE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.CCCODICE as CCCODICE201"+ ",link_2_1.CCDESCRI as CCDESCRI201"+ ",link_2_1.CCFLASSE as CCFLASSE201"+ ",link_2_1.CCDTOBSO as CCDTOBSO201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on CONTROPA.COCAUFDE=link_2_1.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and CONTROPA.COCAUFDE=link_2_1.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCASFDE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCASFDE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCASFDE)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCASFDE))
          select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCASFDE)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCASFDE)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCASFDE)+"%");

            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCASFDE) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCASFDE_2_2'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCASFDE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCASFDE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCASFDE)
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCASFDE = NVL(_Link_.CCCODICE,space(5))
      this.w_DESFDES = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLASSE2 = NVL(_Link_.CCFLASSE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCASFDE = space(5)
      endif
      this.w_DESFDES = space(35)
      this.w_FLASSE2 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE2='S') OR EMPTY(.w_COCASFDE)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
        endif
        this.w_COCASFDE = space(5)
        this.w_DESFDES = space(35)
        this.w_FLASSE2 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCASFDE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CCCODICE as CCCODICE202"+ ",link_2_2.CCDESCRI as CCDESCRI202"+ ",link_2_2.CCFLASSE as CCFLASSE202"+ ",link_2_2.CCDTOBSO as CCDTOBSO202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on CONTROPA.COCASFDE=link_2_2.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and CONTROPA.COCASFDE=link_2_2.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAUFDR
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAUFDR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAUFDR)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAUFDR))
          select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAUFDR)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCAUFDR)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCAUFDR)+"%");

            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCAUFDR) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAUFDR_2_3'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAUFDR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAUFDR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAUFDR)
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAUFDR = NVL(_Link_.CCCODICE,space(5))
      this.w_DESFDR = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLASSE3 = NVL(_Link_.CCFLASSE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAUFDR = space(5)
      endif
      this.w_DESFDR = space(35)
      this.w_FLASSE3 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE3='S') OR EMPTY(.w_COCAUFDR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
        endif
        this.w_COCAUFDR = space(5)
        this.w_DESFDR = space(35)
        this.w_FLASSE3 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAUFDR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CCCODICE as CCCODICE203"+ ",link_2_3.CCDESCRI as CCDESCRI203"+ ",link_2_3.CCFLASSE as CCFLASSE203"+ ",link_2_3.CCDTOBSO as CCDTOBSO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on CONTROPA.COCAUFDR=link_2_3.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and CONTROPA.COCAUFDR=link_2_3.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCASFDR
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCASFDR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCASFDR)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCASFDR))
          select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCASFDR)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCASFDR)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCASFDR)+"%");

            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCASFDR) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCASFDR_2_4'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCASFDR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCASFDR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCASFDR)
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCASFDR = NVL(_Link_.CCCODICE,space(5))
      this.w_DESFDRS = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLASSE4 = NVL(_Link_.CCFLASSE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCASFDR = space(5)
      endif
      this.w_DESFDRS = space(35)
      this.w_FLASSE4 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE4='S') OR EMPTY(.w_COCASFDR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
        endif
        this.w_COCASFDR = space(5)
        this.w_DESFDRS = space(35)
        this.w_FLASSE4 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCASFDR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.CCCODICE as CCCODICE204"+ ",link_2_4.CCDESCRI as CCDESCRI204"+ ",link_2_4.CCFLASSE as CCFLASSE204"+ ",link_2_4.CCDTOBSO as CCDTOBSO204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on CONTROPA.COCASFDR=link_2_4.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and CONTROPA.COCASFDR=link_2_4.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAURIM
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAURIM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAURIM)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAURIM))
          select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAURIM)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCAURIM)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCAURIM)+"%");

            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCAURIM) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAURIM_2_6'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAURIM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAURIM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAURIM)
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAURIM = NVL(_Link_.CCCODICE,space(5))
      this.w_DESRIM = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLASSE5 = NVL(_Link_.CCFLASSE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAURIM = space(5)
      endif
      this.w_DESRIM = space(35)
      this.w_FLASSE5 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE5='S') OR EMPTY(.w_COCAURIM)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
        endif
        this.w_COCAURIM = space(5)
        this.w_DESRIM = space(35)
        this.w_FLASSE5 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAURIM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.CCCODICE as CCCODICE206"+ ",link_2_6.CCDESCRI as CCDESCRI206"+ ",link_2_6.CCFLASSE as CCFLASSE206"+ ",link_2_6.CCDTOBSO as CCDTOBSO206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on CONTROPA.COCAURIM=link_2_6.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and CONTROPA.COCAURIM=link_2_6.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCASRIM
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCASRIM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCASRIM)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCASRIM))
          select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCASRIM)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCASRIM)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCASRIM)+"%");

            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCASRIM) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCASRIM_2_7'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCASRIM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCASRIM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCASRIM)
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCASRIM = NVL(_Link_.CCCODICE,space(5))
      this.w_DESRIMS = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLASSE6 = NVL(_Link_.CCFLASSE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCASRIM = space(5)
      endif
      this.w_DESRIMS = space(35)
      this.w_FLASSE6 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE6='S') OR EMPTY(.w_COCASRIM)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
        endif
        this.w_COCASRIM = space(5)
        this.w_DESRIMS = space(35)
        this.w_FLASSE6 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCASRIM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.CCCODICE as CCCODICE207"+ ",link_2_7.CCDESCRI as CCDESCRI207"+ ",link_2_7.CCFLASSE as CCFLASSE207"+ ",link_2_7.CCDTOBSO as CCDTOBSO207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on CONTROPA.COCASRIM=link_2_7.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and CONTROPA.COCASRIM=link_2_7.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAURER
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAURER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAURER)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAURER))
          select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAURER)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCAURER)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCAURER)+"%");

            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCAURER) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAURER_2_8'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAURER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAURER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAURER)
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAURER = NVL(_Link_.CCCODICE,space(5))
      this.w_DESRER = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLASSE7 = NVL(_Link_.CCFLASSE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAURER = space(5)
      endif
      this.w_DESRER = space(35)
      this.w_FLASSE7 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE7='S') OR EMPTY(.w_COCAURER)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
        endif
        this.w_COCAURER = space(5)
        this.w_DESRER = space(35)
        this.w_FLASSE7 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAURER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.CCCODICE as CCCODICE208"+ ",link_2_8.CCDESCRI as CCDESCRI208"+ ",link_2_8.CCFLASSE as CCFLASSE208"+ ",link_2_8.CCDTOBSO as CCDTOBSO208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on CONTROPA.COCAURER=link_2_8.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and CONTROPA.COCAURER=link_2_8.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCASRER
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCASRER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCASRER)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCASRER))
          select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCASRER)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCASRER)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCASRER)+"%");

            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCASRER) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCASRER_2_9'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCASRER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCASRER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCASRER)
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCASRER = NVL(_Link_.CCCODICE,space(5))
      this.w_DESRERS = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLASSE8 = NVL(_Link_.CCFLASSE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCASRER = space(5)
      endif
      this.w_DESRERS = space(35)
      this.w_FLASSE8 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE8='S') OR EMPTY(.w_COCASRER)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
        endif
        this.w_COCASRER = space(5)
        this.w_DESRERS = space(35)
        this.w_FLASSE8 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCASRER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_9.CCCODICE as CCCODICE209"+ ",link_2_9.CCDESCRI as CCDESCRI209"+ ",link_2_9.CCFLASSE as CCFLASSE209"+ ",link_2_9.CCDTOBSO as CCDTOBSO209"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_9 on CONTROPA.COCASRER=link_2_9.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_9"
          i_cKey=i_cKey+'+" and CONTROPA.COCASRER=link_2_9.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAFRER
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAFRER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAFRER)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAFRER))
          select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAFRER)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCAFRER)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCAFRER)+"%");

            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCAFRER) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAFRER_2_10'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAR1KAA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAFRER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAFRER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAFRER)
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAFRER = NVL(_Link_.CCCODICE,space(5))
      this.w_DESRERF = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLASSE1 = NVL(_Link_.CCFLASSE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAFRER = space(5)
      endif
      this.w_DESRERF = space(35)
      this.w_FLASSE1 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE1<>'S') OR EMPTY(.w_COCAFRER)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente obsoleta o di tipo assestamento")
        endif
        this.w_COCAFRER = space(5)
        this.w_DESRERF = space(35)
        this.w_FLASSE1 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAFRER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_10.CCCODICE as CCCODICE210"+ ",link_2_10.CCDESCRI as CCDESCRI210"+ ",link_2_10.CCFLASSE as CCFLASSE210"+ ",link_2_10.CCDTOBSO as CCDTOBSO210"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_10 on CONTROPA.COCAFRER=link_2_10.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_10"
          i_cKey=i_cKey+'+" and CONTROPA.COCAFRER=link_2_10.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAUCES
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAUCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAUCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAUCES))
          select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAUCES)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCAUCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCAUCES)+"%");

            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCAUCES) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAUCES_2_11'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAUCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAUCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAUCES)
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAUCES = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCES = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLASSE9 = NVL(_Link_.CCFLASSE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAUCES = space(5)
      endif
      this.w_DESCES = space(35)
      this.w_FLASSE9 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE9='S') OR EMPTY(.w_COCAUCES)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
        endif
        this.w_COCAUCES = space(5)
        this.w_DESCES = space(35)
        this.w_FLASSE9 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAUCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_11.CCCODICE as CCCODICE211"+ ",link_2_11.CCDESCRI as CCDESCRI211"+ ",link_2_11.CCFLASSE as CCFLASSE211"+ ",link_2_11.CCDTOBSO as CCDTOBSO211"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_11 on CONTROPA.COCAUCES=link_2_11.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_11"
          i_cKey=i_cKey+'+" and CONTROPA.COCAUCES=link_2_11.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCASCES
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCASCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCASCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCASCES))
          select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCASCES)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCASCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCASCES)+"%");

            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCASCES) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCASCES_2_12'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCASCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCASCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCASCES)
            select CCCODICE,CCDESCRI,CCFLASSE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCASCES = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCESS = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLASSE10 = NVL(_Link_.CCFLASSE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCASCES = space(5)
      endif
      this.w_DESCESS = space(35)
      this.w_FLASSE10 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE10='S') OR EMPTY(.w_COCASCES)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
        endif
        this.w_COCASCES = space(5)
        this.w_DESCESS = space(35)
        this.w_FLASSE10 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCASCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_12.CCCODICE as CCCODICE212"+ ",link_2_12.CCDESCRI as CCDESCRI212"+ ",link_2_12.CCFLASSE as CCFLASSE212"+ ",link_2_12.CCDTOBSO as CCDTOBSO212"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_12 on CONTROPA.COCASCES=link_2_12.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_12"
          i_cKey=i_cKey+'+" and CONTROPA.COCASCES=link_2_12.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOFATEME_1_12.value==this.w_COFATEME)
      this.oPgFrm.Page1.oPag.oCOFATEME_1_12.value=this.w_COFATEME
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFEM_1_13.value==this.w_DESFEM)
      this.oPgFrm.Page1.oPag.oDESFEM_1_13.value=this.w_DESFEM
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFATRIC_1_15.value==this.w_COFATRIC)
      this.oPgFrm.Page1.oPag.oCOFATRIC_1_15.value=this.w_COFATRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFRI_1_16.value==this.w_DESFRI)
      this.oPgFrm.Page1.oPag.oDESFRI_1_16.value=this.w_DESFRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCORATATT_1_17.value==this.w_CORATATT)
      this.oPgFrm.Page1.oPag.oCORATATT_1_17.value=this.w_CORATATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRAA_1_18.value==this.w_DESRAA)
      this.oPgFrm.Page1.oPag.oDESRAA_1_18.value=this.w_DESRAA
    endif
    if not(this.oPgFrm.Page1.oPag.oCORATPAS_1_19.value==this.w_CORATPAS)
      this.oPgFrm.Page1.oPag.oCORATPAS_1_19.value=this.w_CORATPAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRAP_1_20.value==this.w_DESRAP)
      this.oPgFrm.Page1.oPag.oDESRAP_1_20.value=this.w_DESRAP
    endif
    if not(this.oPgFrm.Page1.oPag.oCORISATT_1_21.value==this.w_CORISATT)
      this.oPgFrm.Page1.oPag.oCORISATT_1_21.value=this.w_CORISATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIA_1_22.value==this.w_DESRIA)
      this.oPgFrm.Page1.oPag.oDESRIA_1_22.value=this.w_DESRIA
    endif
    if not(this.oPgFrm.Page1.oPag.oCORISPAS_1_23.value==this.w_CORISPAS)
      this.oPgFrm.Page1.oPag.oCORISPAS_1_23.value=this.w_CORISPAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIP_1_24.value==this.w_DESRIP)
      this.oPgFrm.Page1.oPag.oDESRIP_1_24.value=this.w_DESRIP
    endif
    if not(this.oPgFrm.Page1.oPag.oCOIVADEB_1_32.value==this.w_COIVADEB)
      this.oPgFrm.Page1.oPag.oCOIVADEB_1_32.value=this.w_COIVADEB
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVD_1_33.value==this.w_DESIVD)
      this.oPgFrm.Page1.oPag.oDESIVD_1_33.value=this.w_DESIVD
    endif
    if not(this.oPgFrm.Page1.oPag.oCOIVACRE_1_35.value==this.w_COIVACRE)
      this.oPgFrm.Page1.oPag.oCOIVACRE_1_35.value=this.w_COIVACRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVC_1_36.value==this.w_DESIVC)
      this.oPgFrm.Page1.oPag.oDESIVC_1_36.value=this.w_DESIVC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCREBRE_1_38.value==this.w_COCREBRE)
      this.oPgFrm.Page1.oPag.oCOCREBRE_1_38.value=this.w_COCREBRE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCREMED_1_39.value==this.w_COCREMED)
      this.oPgFrm.Page1.oPag.oCOCREMED_1_39.value=this.w_COCREMED
    endif
    if not(this.oPgFrm.Page1.oPag.oCODEBBRE_1_40.value==this.w_CODEBBRE)
      this.oPgFrm.Page1.oPag.oCODEBBRE_1_40.value=this.w_CODEBBRE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODEBMED_1_41.value==this.w_CODEBMED)
      this.oPgFrm.Page1.oPag.oCODEBMED_1_41.value=this.w_CODEBMED
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRB_1_42.value==this.w_DESCRB)
      this.oPgFrm.Page1.oPag.oDESCRB_1_42.value=this.w_DESCRB
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRM_1_44.value==this.w_DESCRM)
      this.oPgFrm.Page1.oPag.oDESCRM_1_44.value=this.w_DESCRM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDEB_1_46.value==this.w_DESDEB)
      this.oPgFrm.Page1.oPag.oDESDEB_1_46.value=this.w_DESDEB
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDEM_1_47.value==this.w_DESDEM)
      this.oPgFrm.Page1.oPag.oDESDEM_1_47.value=this.w_DESDEM
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCONARR_1_52.value==this.w_COCONARR)
      this.oPgFrm.Page1.oPag.oCOCONARR_1_52.value=this.w_COCONARR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESARR_1_53.value==this.w_DESARR)
      this.oPgFrm.Page1.oPag.oDESARR_1_53.value=this.w_DESARR
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCAUFDE_2_1.value==this.w_COCAUFDE)
      this.oPgFrm.Page2.oPag.oCOCAUFDE_2_1.value=this.w_COCAUFDE
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCASFDE_2_2.value==this.w_COCASFDE)
      this.oPgFrm.Page2.oPag.oCOCASFDE_2_2.value=this.w_COCASFDE
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCAUFDR_2_3.value==this.w_COCAUFDR)
      this.oPgFrm.Page2.oPag.oCOCAUFDR_2_3.value=this.w_COCAUFDR
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCASFDR_2_4.value==this.w_COCASFDR)
      this.oPgFrm.Page2.oPag.oCOCASFDR_2_4.value=this.w_COCASFDR
    endif
    if not(this.oPgFrm.Page2.oPag.oCOINDIVA_2_5.RadioValue()==this.w_COINDIVA)
      this.oPgFrm.Page2.oPag.oCOINDIVA_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCAURIM_2_6.value==this.w_COCAURIM)
      this.oPgFrm.Page2.oPag.oCOCAURIM_2_6.value=this.w_COCAURIM
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCASRIM_2_7.value==this.w_COCASRIM)
      this.oPgFrm.Page2.oPag.oCOCASRIM_2_7.value=this.w_COCASRIM
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCAURER_2_8.value==this.w_COCAURER)
      this.oPgFrm.Page2.oPag.oCOCAURER_2_8.value=this.w_COCAURER
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCASRER_2_9.value==this.w_COCASRER)
      this.oPgFrm.Page2.oPag.oCOCASRER_2_9.value=this.w_COCASRER
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCAFRER_2_10.value==this.w_COCAFRER)
      this.oPgFrm.Page2.oPag.oCOCAFRER_2_10.value=this.w_COCAFRER
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCAUCES_2_11.value==this.w_COCAUCES)
      this.oPgFrm.Page2.oPag.oCOCAUCES_2_11.value=this.w_COCAUCES
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCASCES_2_12.value==this.w_COCASCES)
      this.oPgFrm.Page2.oPag.oCOCASCES_2_12.value=this.w_COCASCES
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRIM_2_18.value==this.w_DESRIM)
      this.oPgFrm.Page2.oPag.oDESRIM_2_18.value=this.w_DESRIM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRIMS_2_21.value==this.w_DESRIMS)
      this.oPgFrm.Page2.oPag.oDESRIMS_2_21.value=this.w_DESRIMS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFDE_2_25.value==this.w_DESFDE)
      this.oPgFrm.Page2.oPag.oDESFDE_2_25.value=this.w_DESFDE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFDES_2_27.value==this.w_DESFDES)
      this.oPgFrm.Page2.oPag.oDESFDES_2_27.value=this.w_DESFDES
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFDR_2_31.value==this.w_DESFDR)
      this.oPgFrm.Page2.oPag.oDESFDR_2_31.value=this.w_DESFDR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFDRS_2_33.value==this.w_DESFDRS)
      this.oPgFrm.Page2.oPag.oDESFDRS_2_33.value=this.w_DESFDRS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRER_2_37.value==this.w_DESRER)
      this.oPgFrm.Page2.oPag.oDESRER_2_37.value=this.w_DESRER
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRERS_2_39.value==this.w_DESRERS)
      this.oPgFrm.Page2.oPag.oDESRERS_2_39.value=this.w_DESRERS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRERF_2_41.value==this.w_DESRERF)
      this.oPgFrm.Page2.oPag.oDESRERF_2_41.value=this.w_DESRERF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCES_2_45.value==this.w_DESCES)
      this.oPgFrm.Page2.oPag.oDESCES_2_45.value=this.w_DESCES
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCESS_2_47.value==this.w_DESCESS)
      this.oPgFrm.Page2.oPag.oDESCESS_2_47.value=this.w_DESCESS
    endif
    cp_SetControlsValueExtFlds(this,'CONTROPA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COFATEME))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOFATEME_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COFATRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOFATRIC_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CORATATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCORATATT_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CORATPAS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCORATPAS_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CORISATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCORISATT_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CORISPAS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCORISPAS_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_TIVDEB='I' OR EMPTY(.w_COIVADEB)))  and not(empty(.w_COIVADEB))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOIVADEB_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente o no IVA oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_TIVCRE='I' OR EMPTY(.w_COIVACRE)))  and not(empty(.w_COIVACRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOIVACRE_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente o no IVA oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COCREBRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCREBRE_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COCREMED))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCREMED_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODEBBRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODEBBRE_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODEBMED))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODEBMED_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COCONARR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCONARR_1_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE='S') OR EMPTY(.w_COCAUFDE))  and not(empty(.w_COCAUFDE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCAUFDE_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE2='S') OR EMPTY(.w_COCASFDE))  and not(empty(.w_COCASFDE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCASFDE_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE3='S') OR EMPTY(.w_COCAUFDR))  and not(empty(.w_COCAUFDR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCAUFDR_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE4='S') OR EMPTY(.w_COCASFDR))  and not(empty(.w_COCASFDR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCASFDR_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE5='S') OR EMPTY(.w_COCAURIM))  and (g_MAGA='S')  and not(empty(.w_COCAURIM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCAURIM_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE6='S') OR EMPTY(.w_COCASRIM))  and (g_MAGA='S')  and not(empty(.w_COCASRIM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCASRIM_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE7='S') OR EMPTY(.w_COCAURER))  and not(empty(.w_COCAURER))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCAURER_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE8='S') OR EMPTY(.w_COCASRER))  and not(empty(.w_COCASRER))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCASRER_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE1<>'S') OR EMPTY(.w_COCAFRER))  and not(empty(.w_COCAFRER))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCAFRER_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente obsoleta o di tipo assestamento")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE9='S') OR EMPTY(.w_COCAUCES))  and ('CESP' $ UPPER(i_cModules))  and not(empty(.w_COCAUCES))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCAUCES_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_FLASSE10='S') OR EMPTY(.w_COCASCES))  and ('CESP' $ UPPER(i_cModules))  and not(empty(.w_COCASCES))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCASCES_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente obsoleta o non di tipo assestamento")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_ao3Pag1 as StdContainer
  Width  = 580
  height = 446
  stdWidth  = 580
  stdheight = 446
  resizeXpos=352
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOFATEME_1_12 as StdField with uid="GKDKNASBCC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COFATEME", cQueryName = "COFATEME",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita fatture emesse usata nelle registrazioni di assestamento",;
    HelpContextID = 246677,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=30, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COFATEME"

  func oCOFATEME_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOFATEME_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOFATEME_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOFATEME_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti fatture da emettere",'',this.parent.oContained
  endproc
  proc oCOFATEME_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COFATEME
     i_obj.ecpSave()
  endproc

  add object oDESFEM_1_13 as StdField with uid="NFIWJGCSCA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESFEM", cQueryName = "DESFEM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 149814730,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=30, InputMask=replicate('X',40)

  add object oCOFATRIC_1_15 as StdField with uid="LXPNAQECMO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_COFATRIC", cQueryName = "COFATRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita fatture ricevute usata nelle registrazioni di assestamento",;
    HelpContextID = 50578327,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=59, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COFATRIC"

  func oCOFATRIC_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOFATRIC_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOFATRIC_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOFATRIC_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti fatture ricevute",'',this.parent.oContained
  endproc
  proc oCOFATRIC_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COFATRIC
     i_obj.ecpSave()
  endproc

  add object oDESFRI_1_16 as StdField with uid="YUZDGMUKXA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESFRI", cQueryName = "DESFRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 203292106,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=59, InputMask=replicate('X',40)

  add object oCORATATT_1_17 as StdField with uid="SWVREYTGML",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CORATATT", cQueryName = "CORATATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita ratei attivi usata nelle registrazioni di assestamento",;
    HelpContextID = 201129082,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=88, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CORATATT"

  func oCORATATT_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCORATATT_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCORATATT_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCORATATT_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti ratei attivi",'',this.parent.oContained
  endproc
  proc oCORATATT_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CORATATT
     i_obj.ecpSave()
  endproc

  add object oDESRAA_1_18 as StdField with uid="IABMEWYNWU",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESRAA", cQueryName = "DESRAA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 86113738,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=88, InputMask=replicate('X',40)

  add object oCORATPAS_1_19 as StdField with uid="YIGHIDBIGX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CORATPAS", cQueryName = "CORATPAS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita ratei passivi usata nelle registrazioni di assestamento",;
    HelpContextID = 184351865,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=117, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CORATPAS"

  func oCORATPAS_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCORATPAS_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCORATPAS_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCORATPAS_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti ratei passivi",'',this.parent.oContained
  endproc
  proc oCORATPAS_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CORATPAS
     i_obj.ecpSave()
  endproc

  add object oDESRAP_1_20 as StdField with uid="CSZKEHMMGE",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESRAP", cQueryName = "DESRAP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 102890954,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=117, InputMask=replicate('X',40)

  add object oCORISATT_1_21 as StdField with uid="FQJMXWDVYE",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CORISATT", cQueryName = "CORISATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita risconti attivi usata nelle registrazioni di assestamento",;
    HelpContextID = 200604794,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=146, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CORISATT"

  func oCORISATT_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCORISATT_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCORISATT_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCORISATT_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti risconti attivi",'',this.parent.oContained
  endproc
  proc oCORISATT_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CORISATT
     i_obj.ecpSave()
  endproc

  add object oDESRIA_1_22 as StdField with uid="ZZBYMBIEUX",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESRIA", cQueryName = "DESRIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 77725130,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=146, InputMask=replicate('X',40)

  add object oCORISPAS_1_23 as StdField with uid="HMJCNOKILX",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CORISPAS", cQueryName = "CORISPAS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita risconti passivi usata nelle registrazioni di assestamento",;
    HelpContextID = 183827577,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=175, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CORISPAS"

  func oCORISPAS_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCORISPAS_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCORISPAS_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCORISPAS_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti risconti passivi",'',this.parent.oContained
  endproc
  proc oCORISPAS_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CORISPAS
     i_obj.ecpSave()
  endproc

  add object oDESRIP_1_24 as StdField with uid="BFXEVXTKAV",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESRIP", cQueryName = "DESRIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 94502346,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=175, InputMask=replicate('X',40)

  add object oCOIVADEB_1_32 as StdField with uid="MAAHQVLQPZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_COIVADEB", cQueryName = "COIVADEB",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente o no IVA oppure obsoleto",;
    ToolTipText = "Contropartita IVA a debito usata nelle registrazioni di assestamento",;
    HelpContextID = 232877160,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=204, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COIVADEB"

  func oCOIVADEB_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOIVADEB_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOIVADEB_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOIVADEB_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti IVA",'CONTIZOOMIVA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCOIVADEB_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COIVADEB
     i_obj.ecpSave()
  endproc

  add object oDESIVD_1_33 as StdField with uid="TBFSITRRBZ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESIVD", cQueryName = "DESIVD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 14351818,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=204, InputMask=replicate('X',40)

  add object oCOIVACRE_1_35 as StdField with uid="DYJDVBWXIL",rtseq=27,rtrep=.f.,;
    cFormVar = "w_COIVACRE", cQueryName = "COIVACRE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente o no IVA oppure obsoleto",;
    ToolTipText = "Contropartita IVA a credito usata nelle registrazioni di assestamento",;
    HelpContextID = 216099947,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=233, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COIVACRE"

  func oCOIVACRE_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOIVACRE_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOIVACRE_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOIVACRE_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti IVA",'CONTIZOOMIVA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCOIVACRE_1_35.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COIVACRE
     i_obj.ecpSave()
  endproc

  add object oDESIVC_1_36 as StdField with uid="WMGLRAPCTO",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESIVC", cQueryName = "DESIVC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 31129034,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=233, InputMask=replicate('X',40)

  add object oCOCREBRE_1_38 as StdField with uid="RXQXVMRWNM",rtseq=30,rtrep=.f.,;
    cFormVar = "w_COCREBRE", cQueryName = "COCREBRE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita crediti a breve termine usata nelle registrazioni di assestamento",;
    HelpContextID = 203230315,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=262, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCREBRE"

  func oCOCREBRE_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCREBRE_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCREBRE_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCREBRE_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti crediti a breve",'',this.parent.oContained
  endproc
  proc oCOCREBRE_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCREBRE
     i_obj.ecpSave()
  endproc

  add object oCOCREMED_1_39 as StdField with uid="YJTLLMFMLP",rtseq=31,rtrep=.f.,;
    cFormVar = "w_COCREMED", cQueryName = "COCREMED",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita crediti a medio/lungo termine usata nelle registrazioni di assest.",;
    HelpContextID = 119344234,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=291, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCREMED"

  func oCOCREMED_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCREMED_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCREMED_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCREMED_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti crediti a medio/lungo",'',this.parent.oContained
  endproc
  proc oCOCREMED_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCREMED
     i_obj.ecpSave()
  endproc

  add object oCODEBBRE_1_40 as StdField with uid="UWKGDBBUNK",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CODEBBRE", cQueryName = "CODEBBRE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita debiti a breve termine usata nelle registrazioni di assestamento",;
    HelpContextID = 199236715,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=320, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODEBBRE"

  func oCODEBBRE_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODEBBRE_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODEBBRE_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODEBBRE_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti debiti a breve",'',this.parent.oContained
  endproc
  proc oCODEBBRE_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODEBBRE
     i_obj.ecpSave()
  endproc

  add object oCODEBMED_1_41 as StdField with uid="VZBXOLDBYI",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CODEBMED", cQueryName = "CODEBMED",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita debiti a medio/lungo termine usata nelle registrazioni di assest.",;
    HelpContextID = 115350634,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=349, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODEBMED"

  func oCODEBMED_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODEBMED_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODEBMED_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODEBMED_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti debiti a medio/lungo",'',this.parent.oContained
  endproc
  proc oCODEBMED_1_41.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODEBMED
     i_obj.ecpSave()
  endproc

  add object oDESCRB_1_42 as StdField with uid="ADWMIMWBQK",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESCRB", cQueryName = "DESCRB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 52493770,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=262, InputMask=replicate('X',40)


  add object oBtn_1_43 as StdButton with uid="MHZONZHJMS",left=465, top=401, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 176752410;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRM_1_44 as StdField with uid="SGNXXNUHFR",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESCRM", cQueryName = "DESCRM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 136379850,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=291, InputMask=replicate('X',40)


  add object oBtn_1_45 as StdButton with uid="JZDYGLSZTD",left=515, top=401, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per abbandonare";
    , HelpContextID = 169463738;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_45.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESDEB_1_46 as StdField with uid="TWWDBSVMYH",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESDEB", cQueryName = "DESDEB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 66059722,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=320, InputMask=replicate('X',40)

  add object oDESDEM_1_47 as StdField with uid="ZMCRDJUTUO",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESDEM", cQueryName = "DESDEM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 149945802,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=349, InputMask=replicate('X',40)

  add object oCOCONARR_1_52 as StdField with uid="FAACGNLHCM",rtseq=38,rtrep=.f.,;
    cFormVar = "w_COCONARR", cQueryName = "COCONARR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita arrotondamenti",;
    HelpContextID = 195693688,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=145, Top=377, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCONARR"

  func oCOCONARR_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCONARR_1_52.ecpDrop(oSource)
    this.Parent.oContained.link_1_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCONARR_1_52.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCONARR_1_52'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti debiti a medio/lungo",'',this.parent.oContained
  endproc
  proc oCOCONARR_1_52.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCONARR
     i_obj.ecpSave()
  endproc

  add object oDESARR_1_53 as StdField with uid="XRFCAIPKUV",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESARR", cQueryName = "DESARR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 52624842,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=377, InputMask=replicate('X',40)

  add object oStr_1_14 as StdString with uid="KJFPJLNKGT",Visible=.t., Left=0, Top=30,;
    Alignment=1, Width=143, Height=15,;
    Caption="Fatture da emettere:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="FHMDPHYVXT",Visible=.t., Left=0, Top=59,;
    Alignment=1, Width=143, Height=15,;
    Caption="Fatture da ricevere:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="WBEFTFHFAM",Visible=.t., Left=0, Top=88,;
    Alignment=1, Width=143, Height=15,;
    Caption="Ratei attivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="IEBUWZPTHD",Visible=.t., Left=0, Top=117,;
    Alignment=1, Width=143, Height=15,;
    Caption="Ratei passivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="SCZELRAKFX",Visible=.t., Left=0, Top=146,;
    Alignment=1, Width=143, Height=15,;
    Caption="Risconti attivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="QPHQLZPUFD",Visible=.t., Left=0, Top=175,;
    Alignment=1, Width=143, Height=15,;
    Caption="Risconti passivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="RCJTNHCVKK",Visible=.t., Left=0, Top=204,;
    Alignment=1, Width=143, Height=15,;
    Caption="IVA a debito:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="NFSSQOKHNO",Visible=.t., Left=0, Top=233,;
    Alignment=1, Width=143, Height=15,;
    Caption="IVA a credito:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="CENULLLUIM",Visible=.t., Left=0, Top=262,;
    Alignment=1, Width=143, Height=15,;
    Caption="Crediti a breve:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="EBQKHGFFEB",Visible=.t., Left=0, Top=291,;
    Alignment=1, Width=143, Height=15,;
    Caption="Crediti a medio/l.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="PDBHGAJBXT",Visible=.t., Left=0, Top=320,;
    Alignment=1, Width=143, Height=15,;
    Caption="Debiti a breve:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="YLJJSKRSNA",Visible=.t., Left=0, Top=349,;
    Alignment=1, Width=143, Height=15,;
    Caption="Debiti a medio/l.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="IHHAOBHNAH",Visible=.t., Left=0, Top=377,;
    Alignment=1, Width=143, Height=18,;
    Caption="Arrotondamenti:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsar_ao3Pag2 as StdContainer
  Width  = 580
  height = 446
  stdWidth  = 580
  stdheight = 446
  resizeXpos=331
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOCAUFDE_2_1 as StdField with uid="VSVIESKAJS",rtseq=40,rtrep=.f.,;
    cFormVar = "w_COCAUFDE", cQueryName = "COCAUFDE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente obsoleta o non di tipo assestamento",;
    ToolTipText = "Causale contabile fatture da emettere",;
    HelpContextID = 17566827,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=159, Top=34, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAUFDE"

  func oCOCAUFDE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAUFDE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAUFDE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAUFDE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAUFDE_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAUFDE
     i_obj.ecpSave()
  endproc

  add object oCOCASFDE_2_2 as StdField with uid="AINWIFYPZO",rtseq=41,rtrep=.f.,;
    cFormVar = "w_COCASFDE", cQueryName = "COCASFDE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente obsoleta o non di tipo assestamento",;
    ToolTipText = "Causale contabile di storno fatture da emettere",;
    HelpContextID = 15469675,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=159, Top=58, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCASFDE"

  func oCOCASFDE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCASFDE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCASFDE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCASFDE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCASFDE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCASFDE
     i_obj.ecpSave()
  endproc

  add object oCOCAUFDR_2_3 as StdField with uid="YWRFCIGNYC",rtseq=42,rtrep=.f.,;
    cFormVar = "w_COCAUFDR", cQueryName = "COCAUFDR",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente obsoleta o non di tipo assestamento",;
    ToolTipText = "Causale contabile fatture da ricevere",;
    HelpContextID = 17566840,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=159, Top=106, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAUFDR"

  func oCOCAUFDR_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAUFDR_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAUFDR_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAUFDR_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAUFDR_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAUFDR
     i_obj.ecpSave()
  endproc

  add object oCOCASFDR_2_4 as StdField with uid="VNRIDJNLGW",rtseq=43,rtrep=.f.,;
    cFormVar = "w_COCASFDR", cQueryName = "COCASFDR",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente obsoleta o non di tipo assestamento",;
    ToolTipText = "Causale contabile di storno fatture da ricevere",;
    HelpContextID = 15469688,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=159, Top=130, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCASFDR"

  func oCOCASFDR_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCASFDR_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCASFDR_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCASFDR_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCASFDR_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCASFDR
     i_obj.ecpSave()
  endproc

  add object oCOINDIVA_2_5 as StdCheck with uid="KAHBIPBSIC",rtseq=44,rtrep=.f.,left=343, top=151, caption="Rilevazione indetraibilitÓ IVA",;
    ToolTipText = "Se attivo abilita rilevazione indetraibilitÓ IVA",;
    HelpContextID = 50949223,;
    cFormVar="w_COINDIVA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCOINDIVA_2_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCOINDIVA_2_5.GetRadio()
    this.Parent.oContained.w_COINDIVA = this.RadioValue()
    return .t.
  endfunc

  func oCOINDIVA_2_5.SetRadio()
    this.Parent.oContained.w_COINDIVA=trim(this.Parent.oContained.w_COINDIVA)
    this.value = ;
      iif(this.Parent.oContained.w_COINDIVA=='S',1,;
      0)
  endfunc

  add object oCOCAURIM_2_6 as StdField with uid="JBKKCWZIOT",rtseq=45,rtrep=.f.,;
    cFormVar = "w_COCAURIM", cQueryName = "COCAURIM",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente obsoleta o non di tipo assestamento",;
    ToolTipText = "Causale contabile rimanenze",;
    HelpContextID = 49542029,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=159, Top=183, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAURIM"

  func oCOCAURIM_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MAGA='S')
    endwith
   endif
  endfunc

  func oCOCAURIM_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAURIM_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAURIM_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAURIM_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAURIM_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAURIM
     i_obj.ecpSave()
  endproc

  add object oCOCASRIM_2_7 as StdField with uid="ALSLQOKCSO",rtseq=46,rtrep=.f.,;
    cFormVar = "w_COCASRIM", cQueryName = "COCASRIM",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente obsoleta o non di tipo assestamento",;
    ToolTipText = "Causale contabile di storno rimanenze",;
    HelpContextID = 51639181,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=159, Top=207, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCASRIM"

  func oCOCASRIM_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MAGA='S')
    endwith
   endif
  endfunc

  func oCOCASRIM_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCASRIM_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCASRIM_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCASRIM_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCASRIM_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCASRIM
     i_obj.ecpSave()
  endproc

  add object oCOCAURER_2_8 as StdField with uid="TFYNPVQIDH",rtseq=47,rtrep=.f.,;
    cFormVar = "w_COCAURER", cQueryName = "COCAURER",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente obsoleta o non di tipo assestamento",;
    ToolTipText = "Causale contabile ratei e risconti",;
    HelpContextID = 218893432,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=159, Top=256, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAURER"

  func oCOCAURER_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAURER_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAURER_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAURER_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAURER_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAURER
     i_obj.ecpSave()
  endproc

  add object oCOCASRER_2_9 as StdField with uid="EHIDKNYYQB",rtseq=48,rtrep=.f.,;
    cFormVar = "w_COCASRER", cQueryName = "COCASRER",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente obsoleta o non di tipo assestamento",;
    ToolTipText = "Causale contabile di storno ratei e risconti",;
    HelpContextID = 216796280,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=159, Top=280, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCASRER"

  func oCOCASRER_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCASRER_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCASRER_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCASRER_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCASRER_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCASRER
     i_obj.ecpSave()
  endproc

  add object oCOCAFRER_2_10 as StdField with uid="XNLDXGVBJT",rtseq=49,rtrep=.f.,;
    cFormVar = "w_COCAFRER", cQueryName = "COCAFRER",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente obsoleta o di tipo assestamento",;
    ToolTipText = "Causale contabile di storno ratei e risconti di fine esercizio",;
    HelpContextID = 203164792,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=159, Top=304, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAFRER"

  func oCOCAFRER_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAFRER_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAFRER_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAFRER_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAR1KAA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAFRER_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAFRER
     i_obj.ecpSave()
  endproc

  add object oCOCAUCES_2_11 as StdField with uid="QHAUZVCWZC",rtseq=50,rtrep=.f.,;
    cFormVar = "w_COCAUCES", cQueryName = "COCAUCES",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente obsoleta o non di tipo assestamento",;
    ToolTipText = "Causale contabile cespiti",;
    HelpContextID = 235670649,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=159, Top=352, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAUCES"

  func oCOCAUCES_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ('CESP' $ UPPER(i_cModules))
    endwith
   endif
  endfunc

  func oCOCAUCES_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAUCES_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAUCES_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAUCES_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAUCES_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAUCES
     i_obj.ecpSave()
  endproc

  add object oCOCASCES_2_12 as StdField with uid="JTYKCZNVYI",rtseq=51,rtrep=.f.,;
    cFormVar = "w_COCASCES", cQueryName = "COCASCES",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente obsoleta o non di tipo assestamento",;
    ToolTipText = "Causale contabile di storno cespiti",;
    HelpContextID = 233573497,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=159, Top=376, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCASCES"

  func oCOCASCES_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ('CESP' $ UPPER(i_cModules))
    endwith
   endif
  endfunc

  func oCOCASCES_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCASCES_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCASCES_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCASCES_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAR_KAA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCASCES_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCASCES
     i_obj.ecpSave()
  endproc


  add object oBtn_2_13 as StdButton with uid="NPJRPVIRTV",left=480, top=400, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 176752410;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_14 as StdButton with uid="UKNFUTOXUH",left=530, top=400, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per abbandonare";
    , HelpContextID = 169463738;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESRIM_2_18 as StdField with uid="XPEOHNJBOK",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESRIM", cQueryName = "DESRIM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 144833994,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=226, Top=183, InputMask=replicate('X',35)

  add object oDESRIMS_2_21 as StdField with uid="ISLRABEBRX",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DESRIMS", cQueryName = "DESRIMS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 123601462,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=226, Top=207, InputMask=replicate('X',35)

  add object oDESFDE_2_25 as StdField with uid="SYDOHAOYKV",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DESFDE", cQueryName = "DESFDE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 16645578,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=226, Top=34, InputMask=replicate('X',35)

  add object oDESFDES_2_27 as StdField with uid="ATFDHYUPXE",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESFDES", cQueryName = "DESFDES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 251789878,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=226, Top=58, InputMask=replicate('X',35)

  add object oDESFDR_2_31 as StdField with uid="SPCHWQMCBF",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DESFDR", cQueryName = "DESFDR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 66977226,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=226, Top=106, InputMask=replicate('X',35)

  add object oDESFDRS_2_33 as StdField with uid="UEDDYRLYUF",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DESFDRS", cQueryName = "DESFDRS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 201458230,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=226, Top=130, InputMask=replicate('X',35)

  add object oDESRER_2_37 as StdField with uid="NKAJBVYBSQ",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DESRER", cQueryName = "DESRER",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 65142218,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=226, Top=256, InputMask=replicate('X',35)

  add object oDESRERS_2_39 as StdField with uid="ZFYVMATTMP",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESRERS", cQueryName = "DESRERS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 203293238,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=226, Top=280, InputMask=replicate('X',35)

  add object oDESRERF_2_41 as StdField with uid="ELUDBOFMIR",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESRERF", cQueryName = "DESRERF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 203293238,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=226, Top=304, InputMask=replicate('X',35)

  add object oDESCES_2_45 as StdField with uid="CNIZARJRJA",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESCES", cQueryName = "DESCES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 49348042,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=226, Top=352, InputMask=replicate('X',35)

  add object oDESCESS_2_47 as StdField with uid="AFAGPZXKOU",rtseq=63,rtrep=.f.,;
    cFormVar = "w_DESCESS", cQueryName = "DESCESS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 219087414,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=226, Top=376, InputMask=replicate('X',35)

  add object oStr_2_15 as StdString with uid="AMLBLHDNUD",Visible=.t., Left=18, Top=159,;
    Alignment=0, Width=199, Height=18,;
    Caption="Rimanenze magazzino"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="MXGXBAUKTI",Visible=.t., Left=78, Top=183,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="XALNDYLHEY",Visible=.t., Left=68, Top=207,;
    Alignment=1, Width=86, Height=18,;
    Caption="di storno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="FNHAQQUFWF",Visible=.t., Left=18, Top=8,;
    Alignment=0, Width=160, Height=18,;
    Caption="Fatture da emettere"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="GBNOMGRADQ",Visible=.t., Left=78, Top=34,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="PDVEGYEJST",Visible=.t., Left=68, Top=58,;
    Alignment=1, Width=86, Height=18,;
    Caption="di storno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="GBBAFZNUXR",Visible=.t., Left=18, Top=81,;
    Alignment=0, Width=177, Height=18,;
    Caption="Fatture da ricevere"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="TEEDYOXKTP",Visible=.t., Left=78, Top=106,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="LHVFERBUZZ",Visible=.t., Left=68, Top=130,;
    Alignment=1, Width=86, Height=18,;
    Caption="di storno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="HGPSAKLFAY",Visible=.t., Left=18, Top=231,;
    Alignment=0, Width=198, Height=18,;
    Caption="Ratei e risconti"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="LTDFNEJZAW",Visible=.t., Left=78, Top=256,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="ISOIWSFEEY",Visible=.t., Left=68, Top=280,;
    Alignment=1, Width=86, Height=18,;
    Caption="di storno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="UDNOEEEXVE",Visible=.t., Left=12, Top=304,;
    Alignment=1, Width=142, Height=18,;
    Caption="Stor. per fine eser.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="TQNRNHUWKV",Visible=.t., Left=18, Top=328,;
    Alignment=0, Width=72, Height=18,;
    Caption="Cespiti"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="BSRPAIPXFB",Visible=.t., Left=78, Top=352,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="DCJJAYILTI",Visible=.t., Left=68, Top=376,;
    Alignment=1, Width=86, Height=18,;
    Caption="di storno:"  ;
  , bGlobalFont=.t.

  add object oBox_2_16 as StdBox with uid="YXBLUHEFKS",left=5, top=175, width=573,height=4

  add object oBox_2_23 as StdBox with uid="XIDEKQRDNM",left=5, top=24, width=573,height=4

  add object oBox_2_29 as StdBox with uid="GMDYPJVVAM",left=5, top=97, width=573,height=4

  add object oBox_2_35 as StdBox with uid="QBZAPATMGJ",left=5, top=247, width=573,height=4

  add object oBox_2_43 as StdBox with uid="MUUTPOPCMW",left=5, top=344, width=573,height=4
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ao3','CONTROPA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COCODAZI=CONTROPA.COCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
