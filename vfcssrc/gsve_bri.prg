* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bri                                                        *
*              Calcola mvcodiva documenti                                      *
*                                                                              *
*      Author: Andrea Tamburini                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_42]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-27                                                      *
* Last revis.: 2012-05-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bri",oParentObject)
return(i_retval)

define class tgsve_bri as StdBatch
  * --- Local variables
  w_TOTRIG = 0
  w_UPDRIG = 0
  w_PADRE = .NULL.
  w_NCODIVA = space(3)
  w_OBJCTRL = .NULL.
  w_CHECK = .f.
  w_TIPOPE = space(10)
  w_MVCODDES = space(5)
  w_MVCODCON = space(15)
  w_MVTIPCON = space(1)
  w_RIPRISTINO_O_MVCODCON = space(15)
  w_RIPRISTINO_O_MVCODDES = space(5)
  * --- WorkFile variables
  VOCIIVA_idx=0
  DES_DIVE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riassegna codici IVA
    *     Da Dati Contabili Documenti aggiorna tutte le righe del dettaglio
    *     con il corretto codice IVA
    if upper (THIS.OPARENTOBJECT.CLASS)<>"TGSVE_BCO" and Not ah_YesNo("I codici IVA sulle righe del documento saranno ricalcolati. Vuoi confermare?")
      i_retcode = 'stop'
      return
    else
      this.w_PADRE = This.oParentObject.oParentObject
      this.w_TOTRIG = 0
      this.w_UPDRIG = 0
      this.w_PADRE.MarkPos()     
      this.w_PADRE.FirstRow()     
      do while Not this.w_PADRE.Eof_Trs()
        this.w_PADRE.SetRow()     
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TOTRIG = this.w_TOTRIG + 1
        if this.w_PADRE.FullRow() And Empty(this.w_PADRE.Get("w_MVCACONT"))
          * --- MVTipoPe variabile della maschera non dei documenti (la maschera deve essere ancora confermata)
          * --- Calcolo il codice IVA, recupero il control e lo valorizzo....
          if upper (THIS.OPARENTOBJECT.CLASS)="TGSVE_BCO"
            this.w_MVCODDES = this.oparentobject.oparentobject.w_MVCODDES
            this.w_MVCODCON = this.oparentobject.oparentobject.w_MVCODCON
            this.w_MVTIPCON = this.oparentobject.oparentobject.w_MVTIPCON
            * --- Read from DES_DIVE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DES_DIVE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DDTIPOPE"+;
                " from "+i_cTable+" DES_DIVE where ";
                    +"DDTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                    +" and DDCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                    +" and DDCODDES = "+cp_ToStrODBC(this.w_MVCODDES);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DDTIPOPE;
                from (i_cTable) where;
                    DDTIPCON = this.w_MVTIPCON;
                    and DDCODICE = this.w_MVCODCON;
                    and DDCODDES = this.w_MVCODDES;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TIPOPE = NVL(cp_ToDate(_read_.DDTIPOPE),cp_NullValue(_read_.DDTIPOPE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.w_TIPOPE = this.oParentObject.w_MVTIPOPE
          endif
          * --- Per non far partire l'mcalc sui prezzi delle righe diverse da quella su cui
          *     � posizionato il transitorio aggiorno la o_ liprezzo
          this.w_PADRE.o_LIPREZZO=this.w_PADRE.w_LIPREZZO
          this.w_NCODIVA = CALCODIV(this.w_PADRE.w_MVCODART, this.w_PADRE.w_MVTIPCON, this.w_PADRE.w_XCONORN, this.w_TIPOPE, iif( this.w_PADRE.w_MVFLVEAC="V" , this.w_PADRE.w_MVDATREG , this.w_PADRE.w_MVDATDOC ))
          if this.w_PADRE.w_MVCODIVA<>this.w_NCODIVA
            * --- Ricerco e modifico solo se il codice IVA cambia
            this.w_UPDRIG = this.w_UPDRIG + 1
            this.w_PADRE.w_MVCODIVA = this.w_NCODIVA
            this.w_OBJCTRL = this.w_PADRE.GetBodyCtrl( "w_MVCODIVA" )
            if IsNull( this.w_OBJCTRL )
              this.w_OBJCTRL = this.w_PADRE.GetCtrl( "w_MVCODIVA" )
            endif
            L_Method_Name="this.w_PADRE.Link"+Right( this.w_OBJCTRL.Name , LEN( this.w_OBJCTRL.Name ) - rat("_" , this.w_OBJCTRL.Name , 2)+1)+"('Full',this)"
            this.w_CHECK = &L_Method_Name
            if this.w_CHECK
              this.w_PADRE.mCalc(.t.)     
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PADRE.Set("w_MVCODIVA" , this.w_NCODIVA)     
              this.w_PADRE.SetUpdateRow()     
            endif
          endif
        endif
        this.w_PADRE.NextRow()     
      enddo
      this.w_PADRE.RePos(.t.)     
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_ErrorMsg("Aggiornate [%1] righe su [%2]%0Ricalcolo codici IVA terminato","i","", Alltrim(Str(this.w_UPDRIG)), Alltrim(Str(this.w_TOTRIG)) )
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Il batch GSVE_BRI va in esecuzione al cambio dell'intestatario o della sede
    *     Il metodo SaveDependsOn esegue gli assegnamenti
    *     o_MVCODCON = w_MVCODCON
    *     e
    *     o_MVCODDES = w_MVCODDES
    *     In questo caso i valori di o_MVCODDES e o_MVCODCON devono essere ripristinati
    *     per mantenere i calcoli sulle espressioni dipendenti dagli stessi.
    this.w_RIPRISTINO_O_MVCODCON = this.w_PADRE.o_MVCODCON
    this.w_RIPRISTINO_O_MVCODDES = this.w_PADRE.o_MVCODDES
    this.w_PADRE.SaveDependsOn()     
    * --- Il batch GSVE_BRI va in esecuzione al cambio dell'intestatario o della sede
    *     Il metodo SaveDependsOn esegue gli assegnamenti
    *     o_MVCODCON = w_MVCODCON
    *     e
    *     o_MVCODDES = w_MVCODDES
    *     In questo caso i valori di o_MVCODDES e o_MVCODCON devono essere ripristinati
    *     per mantenere i calcoli sulle espressioni dipendenti dagli stessi.
    this.w_PADRE.o_MVCODCON = this.w_RIPRISTINO_O_MVCODCON
    this.w_PADRE.o_MVCODDES = this.w_RIPRISTINO_O_MVCODDES
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='DES_DIVE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
