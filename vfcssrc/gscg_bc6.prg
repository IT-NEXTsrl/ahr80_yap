* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bc6                                                        *
*              Aggiorna ripartizioni                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_7]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2000-01-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bc6",oParentObject)
return(i_retval)

define class tgscg_bc6 as StdBatch
  * --- Local variables
  w_TIPCON = space(1)
  w_CODVOC = space(15)
  w_CODCON = space(15)
  w_CODCDC = space(15)
  w_PARAME = 0
  * --- WorkFile variables
  COLLCENT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Tabella Ripartizioni sul Conto in base ai valori impostati in Primanota (da GSCG_MCA)
    * --- Richiesta Conferma
    if NOT ah_YesNo("Attenzione:%0Questa funzione sovrascrive gli eventuali parametri di ripartizione.%0inseriti sul conto contabile di riferimento.%0si vuole proseguire?")
      i_retcode = 'stop'
      return
    endif
    this.w_TIPCON = this.oParentObject.oParentObject.w_PNTIPCON
    this.w_CODCON = this.oParentObject.oParentObject.w_PNCODCON
    * --- Verifica se ci sono record da Trasferire
    SELECT (this.oParentObject.cTrsName)
    COUNT FOR NOT EMPTY(NVL(t_MRCODICE, " ")) AND NOT EMPTY(NVL(t_MRCODVOC, " ")) TO w_CONTA
    if w_CONTA=0
      ah_ErrorMsg("Non esistono righe da aggiornare",,"")
    else
      * --- Azzera precedenti Records
      * --- Delete from COLLCENT
      i_nConn=i_TableProp[this.COLLCENT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MRTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
              +" and MRCODICE = "+cp_ToStrODBC(this.w_CODCON);
               )
      else
        delete from (i_cTable) where;
              MRTIPCON = this.w_TIPCON;
              and MRCODICE = this.w_CODCON;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Cicla sul Transitorio
      SELECT (this.oParentObject.cTrsName)
      GO TOP
      SCAN FOR NOT EMPTY(t_MRCODICE) AND NOT EMPTY(t_MRCODVOC)
      this.w_CODVOC = t_MRCODVOC
      this.w_CODCDC = t_MRCODICE
      this.w_PARAME = t_MRPARAME
      * --- Aggiorna Tabella Ripartizioni del Conto
      * --- Try
      local bErr_0381E7D8
      bErr_0381E7D8=bTrsErr
      this.Try_0381E7D8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into COLLCENT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.COLLCENT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.COLLCENT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MRPARAME ="+cp_NullLink(cp_ToStrODBC(this.w_PARAME),'COLLCENT','MRPARAME');
              +i_ccchkf ;
          +" where ";
              +"MRTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
              +" and MRCODICE = "+cp_ToStrODBC(this.w_CODCON);
              +" and MRCODVOC = "+cp_ToStrODBC(this.w_CODVOC);
              +" and MRCODCDC = "+cp_ToStrODBC(this.w_CODCDC);
                 )
        else
          update (i_cTable) set;
              MRPARAME = this.w_PARAME;
              &i_ccchkf. ;
           where;
              MRTIPCON = this.w_TIPCON;
              and MRCODICE = this.w_CODCON;
              and MRCODVOC = this.w_CODVOC;
              and MRCODCDC = this.w_CODCDC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_0381E7D8
      * --- End
      ENDSCAN
    endif
    * --- Questa Parte derivata dal Metodo LoadRec
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    With this.oParentObject
    .WorkFromTrs()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=1
    .oPgFrm.Page1.oPag.oBody.nRelRow=1
    EndWith
  endproc
  proc Try_0381E7D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into COLLCENT
    i_nConn=i_TableProp[this.COLLCENT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COLLCENT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MRTIPCON"+",MRCODICE"+",MRCODVOC"+",MRCODCDC"+",MRPARAME"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'COLLCENT','MRTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'COLLCENT','MRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVOC),'COLLCENT','MRCODVOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCDC),'COLLCENT','MRCODCDC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PARAME),'COLLCENT','MRPARAME');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MRTIPCON',this.w_TIPCON,'MRCODICE',this.w_CODCON,'MRCODVOC',this.w_CODVOC,'MRCODCDC',this.w_CODCDC,'MRPARAME',this.w_PARAME)
      insert into (i_cTable) (MRTIPCON,MRCODICE,MRCODVOC,MRCODCDC,MRPARAME &i_ccchkf. );
         values (;
           this.w_TIPCON;
           ,this.w_CODCON;
           ,this.w_CODVOC;
           ,this.w_CODCDC;
           ,this.w_PARAME;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='COLLCENT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
