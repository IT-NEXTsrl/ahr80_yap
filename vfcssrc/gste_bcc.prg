* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bcc                                                        *
*              Controllo caratteri incongruenti nel C/C                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_41]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-06                                                      *
* Last revis.: 2011-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bcc",oParentObject)
return(i_retval)

define class tgste_bcc as StdBatch
  * --- Local variables
  w_IBAN = space(35)
  w_BBAN = space(30)
  w_OK = .f.
  w_MESS = space(100)
  w_MESS1 = space(100)
  w_MESS2 = space(100)
  w_CONTA = 0
  w_LEN = 0
  w_CONCOR = space(12)
  w_CODCAU = space(5)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_DINUM = 0
  w_ANNO = space(4)
  w_DATDIS = ctod("  /  /  ")
  * --- WorkFile variables
  CCC_DETT_idx=0
  CAUPRI1_idx=0
  CONTI_idx=0
  DIS_TINT_idx=0
  COC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo congruit� Conto Corrente
    this.w_oMESS=createobject("AH_Message")
    this.w_OK = .T.
    if This.oParentobject.cFunction <>"Query"
      if ISALT() AND this.oParentObject.w_BAFLPREF = "S"
        * --- Select from COC_MAST
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select BACODBAN  from "+i_cTable+" COC_MAST ";
              +" where BACODBAN<>"+cp_ToStrODBC(this.oParentObject.w_BACODBAN)+" AND BAFLPREF='S'";
               ,"_Curs_COC_MAST")
        else
          select BACODBAN from (i_cTable);
           where BACODBAN<>this.oParentObject.w_BACODBAN AND BAFLPREF="S";
            into cursor _Curs_COC_MAST
        endif
        if used('_Curs_COC_MAST')
          select _Curs_COC_MAST
          locate for 1=1
          do while not(eof())
          this.w_MESS = ah_Msgformat("sul conto banca %1 � attivo il flag preferenziale, impossibile selezionarne un altro", Alltrim(BACODBAN) )
          this.w_OK = .F.
          this.oParentObject.w_BAFLPREF = "N"
            select _Curs_COC_MAST
            continue
          enddo
          use
        endif
      endif
      if this.w_OK
        if this.oParentObject.w_FLBANEST # "S"
          this.w_CONCOR = ALLTRIM(this.oParentObject.w_BACONCOR)
          this.w_BBAN = this.oParentObject.w_BA__BBAN
          this.w_IBAN = RIGHT(ALLTRIM(this.oParentObject.w_BA__IBAN), LEN(ALLTRIM(this.w_BBAN)))
          this.w_CONTA = 1
          this.w_LEN = LEN(this.w_CONCOR)
          do while this.w_CONTA<=this.w_LEN
            if (ASC(SUBSTR(this.w_CONCOR, this.w_CONTA, this.w_CONTA+1)) >=65 AND ASC(SUBSTR(this.w_CONCOR, this.w_CONTA, this.w_CONTA+1))<=90) OR (ASC(SUBSTR(this.w_CONCOR, this.w_CONTA, this.w_CONTA+1)) >=48 AND ASC(SUBSTR(this.w_CONCOR, this.w_CONTA, this.w_CONTA+1))<=57)
              this.w_CONTA = this.w_CONTA+1
            else
              this.w_oPART = this.w_oMESS.AddMsgPartNL("Il codice C/C non � normalizzato secondo le regole BBAN%0in quanto presenta caratteri speciali, separatori o segni di interpunzione%0")
              this.w_CONTA = this.w_LEN+1
            endif
          enddo
          if EMPTY(this.oParentObject.w_BATIPCON) AND ISLITA()
            if EMPTY(this.oParentObject.w_BA__BBAN) 
              this.w_oPART = this.w_oMESS.AddMsgPartNL("Dati mancanti che impediscono la creazione del BBAN:%0CIN di controllo e/o codice ABI e/o codice CAB e/o C/C%0")
            endif
            if (EMPTY(this.oParentObject.w_CODPA) OR EMPTY(this.oParentObject.w_CIFRA)) 
              this.w_oPART = this.w_oMESS.AddMsgPartNL("Dati mancanti nel codice IBAN%0Codice paese e/o check di controllo%0")
            endif
          endif
        else
          if EMPTY(this.oParentObject.w_BATIPCON) 
            if empty(this.oParentObject.w_BACODBIC)
              this.w_oPART = this.w_oMESS.AddMsgPartNL("Dati mancanti nel codice BIC%0")
            endif
            if empty(this.oParentObject.w_CODIBAN)
              this.w_oPART = this.w_oMESS.AddMsgPartNL("Dati mancanti nel codice IBAN/Cod. int.%0")
            endif
          endif
        endif
      endif
      if this.w_oMESS.AH_MsgParts.count<>0
        this.w_oMESS.AH_ERRORMSG()     
      endif
    else
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANTIPCON,ANCODICE"+;
          " from "+i_cTable+" CONTI where ";
              +"ANCODBA2 = "+cp_ToStrODBC(this.oParentObject.w_BACODBAN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANTIPCON,ANCODICE;
          from (i_cTable) where;
              ANCODBA2 = this.oParentObject.w_BACODBAN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPCON = NVL(cp_ToDate(_read_.ANTIPCON),cp_NullValue(_read_.ANTIPCON))
        this.w_CODCON = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0
        if g_BANC="S"
          if this.w_TIPCON="F"
            this.w_MESS = ah_Msgformat("Il codice C/C � presente nel fornitore %1, impossibile cancellare", Alltrim(this.w_CODCON) )
          else
            this.w_MESS = ah_Msgformat("Il codice C/C � presente nel cliente %1, impossibile cancellare", Alltrim(this.w_CODCON) )
          endif
        else
          if this.w_TIPCON="F"
            this.w_MESS = ah_Msgformat("Il conto banche � presente nel fornitore %1, impossibile cancellare", Alltrim(this.w_CODCON) )
          else
            this.w_MESS = ah_Msgformat("Il conto banche � presente nel cliente %1, impossibile cancellare", Alltrim(this.w_CODCON) )
          endif
        endif
        this.w_OK = .F.
      endif
      if this.w_OK
        * --- Eseguo i controlli sulle distinte pagamento  (campo Banca di presentazione distinta)
        * --- Read from DIS_TINT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIS_TINT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DINUMERO,DI__ANNO,DIDATDIS"+;
            " from "+i_cTable+" DIS_TINT where ";
                +"DIBANRIF = "+cp_ToStrODBC(this.oParentObject.w_BACODBAN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DINUMERO,DI__ANNO,DIDATDIS;
            from (i_cTable) where;
                DIBANRIF = this.oParentObject.w_BACODBAN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DINUM = NVL(cp_ToDate(_read_.DINUMERO),cp_NullValue(_read_.DINUMERO))
          this.w_ANNO = NVL(cp_ToDate(_read_.DI__ANNO),cp_NullValue(_read_.DI__ANNO))
          this.w_DATDIS = NVL(cp_ToDate(_read_.DIDATDIS),cp_NullValue(_read_.DIDATDIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          if g_BANC="S"
            this.w_MESS = ah_Msgformat("Il codice C/C � presente nella distinta num. %1 del %3 anno %2, impossibile cancellare", Alltrim(str(this.w_DINUM)),alltrim(this.w_ANNO),dtoc(this.w_DATDIS) )
          else
            this.w_MESS = ah_Msgformat("Il conto banche � presente nella distinta num. %1 del %3 anno %2, impossibile cancellare", Alltrim(str(this.w_DINUM)),alltrim(this.w_ANNO),dtoc(this.w_DATDIS) )
          endif
          this.w_OK = .F.
        endif
      endif
      if g_BANC="S" and this.w_OK
        this.w_OK = .T.
        * --- Eseguo Controllo Conto Corrente inserito in una Causale Conto Corrente
        * --- Read from CCC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CCC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CCC_DETT_idx,2],.t.,this.CCC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODICE"+;
            " from "+i_cTable+" CCC_DETT where ";
                +"CANUMCOR = "+cp_ToStrODBC(this.oParentObject.w_BACODBAN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODICE;
            from (i_cTable) where;
                CANUMCOR = this.oParentObject.w_BACODBAN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODCAU = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          this.w_MESS = ah_Msgformat("Il codice C/C � presente nella causale movimenti C/C %1, impossibile cancellare", Alltrim(this.w_CODCAU) )
          this.w_OK = .F.
        endif
        * --- Read from CAUPRI1
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAUPRI1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2],.t.,this.CAUPRI1_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "APCODCAU"+;
            " from "+i_cTable+" CAUPRI1 where ";
                +"APCONBAN = "+cp_ToStrODBC(this.oParentObject.w_BACODBAN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            APCODCAU;
            from (i_cTable) where;
                APCONBAN = this.oParentObject.w_BACODBAN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODCAU = NVL(cp_ToDate(_read_.APCODCAU),cp_NullValue(_read_.APCODCAU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          this.w_MESS = ah_Msgformat("Il codice C/C � presente nella causale contabile %1, impossibile cancellare", Alltrim(this.w_CODCAU) )
          this.w_OK = .F.
        endif
      endif
    endif
    if NOT this.w_OK
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CCC_DETT'
    this.cWorkTables[2]='CAUPRI1'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='DIS_TINT'
    this.cWorkTables[5]='COC_MAST'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_COC_MAST')
      use in _Curs_COC_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
