* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kms                                                        *
*              Metodi di calcolo spese                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_14]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-26                                                      *
* Last revis.: 2012-08-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kms",oParentObject))

* --- Class definition
define class tgsar_kms as StdForm
  Top    = 10
  Left   = 9

  * --- Standard Properties
  Width  = 454
  Height = 364
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-08-23"
  HelpContextID=199849833
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Constant Properties
  _IDX = 0
  METCALSP_IDX = 0
  cPrg = "gsar_kms"
  cComment = "Metodi di calcolo spese"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PADRE = space(4)
  w_MCSIVT1 = space(5)
  w_MCSTVT1 = space(5)
  w_MCSIVT2 = space(5)
  w_MCSTVT2 = space(5)
  w_MCSIVT3 = space(5)
  w_MCSTVT3 = space(5)
  w_MCALSI = space(5)
  w_DESMCIMB = space(40)
  w_MCALST = space(5)
  w_DESMCTRA = space(40)
  w_DESIMBV1 = space(40)
  w_DESTRAV1 = space(40)
  w_DESIMBV2 = space(40)
  w_DESTRAV2 = space(40)
  w_DESIMBV3 = space(40)
  w_DESTRAV3 = space(40)
  w_MVCODVET = space(5)
  w_MVCODVE2 = space(5)
  w_MVCODVE3 = space(5)
  w_FLSPTR = space(1)
  w_FLSPIM = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kmsPag1","gsar_kms",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMCSIVT1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='METCALSP'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PADRE=space(4)
      .w_MCSIVT1=space(5)
      .w_MCSTVT1=space(5)
      .w_MCSIVT2=space(5)
      .w_MCSTVT2=space(5)
      .w_MCSIVT3=space(5)
      .w_MCSTVT3=space(5)
      .w_MCALSI=space(5)
      .w_DESMCIMB=space(40)
      .w_MCALST=space(5)
      .w_DESMCTRA=space(40)
      .w_DESIMBV1=space(40)
      .w_DESTRAV1=space(40)
      .w_DESIMBV2=space(40)
      .w_DESTRAV2=space(40)
      .w_DESIMBV3=space(40)
      .w_DESTRAV3=space(40)
      .w_MVCODVET=space(5)
      .w_MVCODVE2=space(5)
      .w_MVCODVE3=space(5)
      .w_FLSPTR=space(1)
      .w_FLSPIM=space(1)
      .w_MCSIVT1=oParentObject.w_MCSIVT1
      .w_MCSTVT1=oParentObject.w_MCSTVT1
      .w_MCSIVT2=oParentObject.w_MCSIVT2
      .w_MCSTVT2=oParentObject.w_MCSTVT2
      .w_MCSIVT3=oParentObject.w_MCSIVT3
      .w_MCSTVT3=oParentObject.w_MCSTVT3
      .w_MCALSI=oParentObject.w_MCALSI
      .w_MCALST=oParentObject.w_MCALST
      .w_FLSPTR=oParentObject.w_FLSPTR
      .w_FLSPIM=oParentObject.w_FLSPIM
        .w_PADRE = IIF(Upper(this.oParentObject.Class)='TGSOF_AOF','OFFE', 'DOCU')
        .w_MCSIVT1 = iif(.w_FLSPIM='S', .w_MCSIVT1, SPACE(5))
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_MCSIVT1))
          .link_1_2('Full')
        endif
        .w_MCSTVT1 = iif(.w_FLSPTR='S', .w_MCSTVT1, SPACE(5))
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_MCSTVT1))
          .link_1_3('Full')
        endif
        .w_MCSIVT2 = iif(.w_FLSPIM='S', .w_MCSIVT2, SPACE(5))
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_MCSIVT2))
          .link_1_4('Full')
        endif
        .w_MCSTVT2 = iif(.w_FLSPTR='S', .w_MCSTVT2, SPACE(5))
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_MCSTVT2))
          .link_1_5('Full')
        endif
        .w_MCSIVT3 = iif(.w_FLSPIM='S', .w_MCSIVT3, SPACE(5))
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_MCSIVT3))
          .link_1_6('Full')
        endif
        .w_MCSTVT3 = iif(.w_FLSPTR='S', .w_MCSTVT3, SPACE(5))
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_MCSTVT3))
          .link_1_7('Full')
        endif
        .w_MCALSI = iif(.w_FLSPIM='S', .w_MCALSI, SPACE(5))
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_MCALSI))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_MCALST = iif(.w_FLSPTR='S', .w_MCALST, SPACE(5))
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_MCALST))
          .link_1_10('Full')
        endif
          .DoRTCalc(11,17,.f.)
        .w_MVCODVET = IIF(Upper(this.oParentObject.Class)='TGSOF_AOF', space(5), this.oParentObject .w_MVCODVET)
        .w_MVCODVE2 = IIF(Upper(this.oParentObject.Class)='TGSOF_AOF', space(5), this.oParentObject .w_MVCODVE2)
        .w_MVCODVE3 = IIF(Upper(this.oParentObject.Class)='TGSOF_AOF', space(5), this.oParentObject .w_MVCODVE3)
      .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
    endwith
    this.DoRTCalc(21,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MCSIVT1=.w_MCSIVT1
      .oParentObject.w_MCSTVT1=.w_MCSTVT1
      .oParentObject.w_MCSIVT2=.w_MCSIVT2
      .oParentObject.w_MCSTVT2=.w_MCSTVT2
      .oParentObject.w_MCSIVT3=.w_MCSIVT3
      .oParentObject.w_MCSTVT3=.w_MCSTVT3
      .oParentObject.w_MCALSI=.w_MCALSI
      .oParentObject.w_MCALST=.w_MCALST
      .oParentObject.w_FLSPTR=.w_FLSPTR
      .oParentObject.w_FLSPIM=.w_FLSPIM
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMCSIVT1_1_2.enabled = this.oPgFrm.Page1.oPag.oMCSIVT1_1_2.mCond()
    this.oPgFrm.Page1.oPag.oMCSTVT1_1_3.enabled = this.oPgFrm.Page1.oPag.oMCSTVT1_1_3.mCond()
    this.oPgFrm.Page1.oPag.oMCSIVT2_1_4.enabled = this.oPgFrm.Page1.oPag.oMCSIVT2_1_4.mCond()
    this.oPgFrm.Page1.oPag.oMCSTVT2_1_5.enabled = this.oPgFrm.Page1.oPag.oMCSTVT2_1_5.mCond()
    this.oPgFrm.Page1.oPag.oMCSIVT3_1_6.enabled = this.oPgFrm.Page1.oPag.oMCSIVT3_1_6.mCond()
    this.oPgFrm.Page1.oPag.oMCSTVT3_1_7.enabled = this.oPgFrm.Page1.oPag.oMCSTVT3_1_7.mCond()
    this.oPgFrm.Page1.oPag.oMCALSI_1_8.enabled = this.oPgFrm.Page1.oPag.oMCALSI_1_8.mCond()
    this.oPgFrm.Page1.oPag.oMCALST_1_10.enabled = this.oPgFrm.Page1.oPag.oMCALST_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMCSIVT1_1_2.visible=!this.oPgFrm.Page1.oPag.oMCSIVT1_1_2.mHide()
    this.oPgFrm.Page1.oPag.oMCSTVT1_1_3.visible=!this.oPgFrm.Page1.oPag.oMCSTVT1_1_3.mHide()
    this.oPgFrm.Page1.oPag.oMCSIVT2_1_4.visible=!this.oPgFrm.Page1.oPag.oMCSIVT2_1_4.mHide()
    this.oPgFrm.Page1.oPag.oMCSTVT2_1_5.visible=!this.oPgFrm.Page1.oPag.oMCSTVT2_1_5.mHide()
    this.oPgFrm.Page1.oPag.oMCSIVT3_1_6.visible=!this.oPgFrm.Page1.oPag.oMCSIVT3_1_6.mHide()
    this.oPgFrm.Page1.oPag.oMCSTVT3_1_7.visible=!this.oPgFrm.Page1.oPag.oMCSTVT3_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oDESIMBV1_1_16.visible=!this.oPgFrm.Page1.oPag.oDESIMBV1_1_16.mHide()
    this.oPgFrm.Page1.oPag.oDESTRAV1_1_17.visible=!this.oPgFrm.Page1.oPag.oDESTRAV1_1_17.mHide()
    this.oPgFrm.Page1.oPag.oDESIMBV2_1_20.visible=!this.oPgFrm.Page1.oPag.oDESIMBV2_1_20.mHide()
    this.oPgFrm.Page1.oPag.oDESTRAV2_1_21.visible=!this.oPgFrm.Page1.oPag.oDESTRAV2_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oDESIMBV3_1_24.visible=!this.oPgFrm.Page1.oPag.oDESIMBV3_1_24.mHide()
    this.oPgFrm.Page1.oPag.oDESTRAV3_1_25.visible=!this.oPgFrm.Page1.oPag.oDESTRAV3_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MCSIVT1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCSIVT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_MCSIVT1)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_MCSIVT1))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCSIVT1)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCSIVT1) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oMCSIVT1_1_2'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCSIVT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_MCSIVT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_MCSIVT1)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCSIVT1 = NVL(_Link_.MSCODICE,space(5))
      this.w_DESIMBV1 = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCSIVT1 = space(5)
      endif
      this.w_DESIMBV1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCSIVT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCSTVT1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCSTVT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_MCSTVT1)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_MCSTVT1))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCSTVT1)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCSTVT1) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oMCSTVT1_1_3'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCSTVT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_MCSTVT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_MCSTVT1)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCSTVT1 = NVL(_Link_.MSCODICE,space(5))
      this.w_DESTRAV1 = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCSTVT1 = space(5)
      endif
      this.w_DESTRAV1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCSTVT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCSIVT2
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCSIVT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_MCSIVT2)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_MCSIVT2))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCSIVT2)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCSIVT2) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oMCSIVT2_1_4'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCSIVT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_MCSIVT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_MCSIVT2)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCSIVT2 = NVL(_Link_.MSCODICE,space(5))
      this.w_DESIMBV2 = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCSIVT2 = space(5)
      endif
      this.w_DESIMBV2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCSIVT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCSTVT2
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCSTVT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_MCSTVT2)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_MCSTVT2))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCSTVT2)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCSTVT2) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oMCSTVT2_1_5'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCSTVT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_MCSTVT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_MCSTVT2)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCSTVT2 = NVL(_Link_.MSCODICE,space(5))
      this.w_DESTRAV2 = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCSTVT2 = space(5)
      endif
      this.w_DESTRAV2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCSTVT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCSIVT3
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCSIVT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_MCSIVT3)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_MCSIVT3))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCSIVT3)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCSIVT3) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oMCSIVT3_1_6'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCSIVT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_MCSIVT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_MCSIVT3)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCSIVT3 = NVL(_Link_.MSCODICE,space(5))
      this.w_DESIMBV3 = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCSIVT3 = space(5)
      endif
      this.w_DESIMBV3 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCSIVT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCSTVT3
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCSTVT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_MCSTVT3)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_MCSTVT3))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCSTVT3)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCSTVT3) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oMCSTVT3_1_7'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCSTVT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_MCSTVT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_MCSTVT3)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCSTVT3 = NVL(_Link_.MSCODICE,space(5))
      this.w_DESTRAV3 = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCSTVT3 = space(5)
      endif
      this.w_DESTRAV3 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCSTVT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCALSI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCALSI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_MCALSI)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_MCALSI))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCALSI)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCALSI) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oMCALSI_1_8'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCALSI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_MCALSI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_MCALSI)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCALSI = NVL(_Link_.MSCODICE,space(5))
      this.w_DESMCIMB = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCALSI = space(5)
      endif
      this.w_DESMCIMB = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCALSI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCALST
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCALST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_MCALST)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_MCALST))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCALST)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCALST) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oMCALST_1_10'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCALST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_MCALST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_MCALST)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCALST = NVL(_Link_.MSCODICE,space(5))
      this.w_DESMCTRA = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCALST = space(5)
      endif
      this.w_DESMCTRA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCALST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMCSIVT1_1_2.value==this.w_MCSIVT1)
      this.oPgFrm.Page1.oPag.oMCSIVT1_1_2.value=this.w_MCSIVT1
    endif
    if not(this.oPgFrm.Page1.oPag.oMCSTVT1_1_3.value==this.w_MCSTVT1)
      this.oPgFrm.Page1.oPag.oMCSTVT1_1_3.value=this.w_MCSTVT1
    endif
    if not(this.oPgFrm.Page1.oPag.oMCSIVT2_1_4.value==this.w_MCSIVT2)
      this.oPgFrm.Page1.oPag.oMCSIVT2_1_4.value=this.w_MCSIVT2
    endif
    if not(this.oPgFrm.Page1.oPag.oMCSTVT2_1_5.value==this.w_MCSTVT2)
      this.oPgFrm.Page1.oPag.oMCSTVT2_1_5.value=this.w_MCSTVT2
    endif
    if not(this.oPgFrm.Page1.oPag.oMCSIVT3_1_6.value==this.w_MCSIVT3)
      this.oPgFrm.Page1.oPag.oMCSIVT3_1_6.value=this.w_MCSIVT3
    endif
    if not(this.oPgFrm.Page1.oPag.oMCSTVT3_1_7.value==this.w_MCSTVT3)
      this.oPgFrm.Page1.oPag.oMCSTVT3_1_7.value=this.w_MCSTVT3
    endif
    if not(this.oPgFrm.Page1.oPag.oMCALSI_1_8.value==this.w_MCALSI)
      this.oPgFrm.Page1.oPag.oMCALSI_1_8.value=this.w_MCALSI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMCIMB_1_9.value==this.w_DESMCIMB)
      this.oPgFrm.Page1.oPag.oDESMCIMB_1_9.value=this.w_DESMCIMB
    endif
    if not(this.oPgFrm.Page1.oPag.oMCALST_1_10.value==this.w_MCALST)
      this.oPgFrm.Page1.oPag.oMCALST_1_10.value=this.w_MCALST
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMCTRA_1_11.value==this.w_DESMCTRA)
      this.oPgFrm.Page1.oPag.oDESMCTRA_1_11.value=this.w_DESMCTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMBV1_1_16.value==this.w_DESIMBV1)
      this.oPgFrm.Page1.oPag.oDESIMBV1_1_16.value=this.w_DESIMBV1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTRAV1_1_17.value==this.w_DESTRAV1)
      this.oPgFrm.Page1.oPag.oDESTRAV1_1_17.value=this.w_DESTRAV1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMBV2_1_20.value==this.w_DESIMBV2)
      this.oPgFrm.Page1.oPag.oDESIMBV2_1_20.value=this.w_DESIMBV2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTRAV2_1_21.value==this.w_DESTRAV2)
      this.oPgFrm.Page1.oPag.oDESTRAV2_1_21.value=this.w_DESTRAV2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMBV3_1_24.value==this.w_DESIMBV3)
      this.oPgFrm.Page1.oPag.oDESIMBV3_1_24.value=this.w_DESIMBV3
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTRAV3_1_25.value==this.w_DESTRAV3)
      this.oPgFrm.Page1.oPag.oDESTRAV3_1_25.value=this.w_DESTRAV3
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsar_kms
      *--- Valorizzo le Old cos� non viene dato il msg
      If i_bRes
         .oParentObject.w_OMCALSI = .w_MCALSI
         .oParentObject.w_OMCALST = .w_MCALST
         .oParentObject.w_OMCSIVT1 = .w_MCSIVT1
         .oParentObject.w_OMCSIVT2 = .w_MCSIVT2
         .oParentObject.w_OMCSIVT3 = .w_MCSIVT3
         .oParentObject.w_OMCSTVT1 = .w_MCSTVT1
         .oParentObject.w_OMCSTVT2 = .w_MCSTVT2
         .oParentObject.w_OMCSTVT3 = .w_MCSTVT3
      EndIf
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_kmsPag1 as StdContainer
  Width  = 450
  height = 364
  stdWidth  = 450
  stdheight = 364
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMCSIVT1_1_2 as StdField with uid="MWOEXDIDOS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MCSIVT1", cQueryName = "MCSIVT1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo per le spese di imballo",;
    HelpContextID = 231014598,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=86, Top=32, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_MCSIVT1"

  func oMCSIVT1_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.oParentObject.cFunction, 'Edit', 'Load') AND NOT EMPTY(.w_MVCODVET) AND .w_FLSPIM='S')
    endwith
   endif
  endfunc

  func oMCSIVT1_1_2.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  func oMCSIVT1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCSIVT1_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCSIVT1_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oMCSIVT1_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oMCSIVT1_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_MCSIVT1
     i_obj.ecpSave()
  endproc

  add object oMCSTVT1_1_3 as StdField with uid="USZHZKPDGW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MCSTVT1", cQueryName = "MCSTVT1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo per le spese di trasporto",;
    HelpContextID = 231735494,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=86, Top=56, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_MCSTVT1"

  func oMCSTVT1_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.oParentObject.cFunction, 'Edit', 'Load') AND NOT EMPTY(.w_MVCODVET) AND .w_FLSPTR='S')
    endwith
   endif
  endfunc

  func oMCSTVT1_1_3.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  func oMCSTVT1_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCSTVT1_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCSTVT1_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oMCSTVT1_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oMCSTVT1_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_MCSTVT1
     i_obj.ecpSave()
  endproc

  add object oMCSIVT2_1_4 as StdField with uid="HBCZGIBOPO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MCSIVT2", cQueryName = "MCSIVT2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo per le spese di imballo",;
    HelpContextID = 231014598,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=86, Top=104, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_MCSIVT2"

  func oMCSIVT2_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.oParentObject.cFunction, 'Edit', 'Load') AND NOT EMPTY(.w_MVCODVE2) AND .w_FLSPIM='S')
    endwith
   endif
  endfunc

  func oMCSIVT2_1_4.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  func oMCSIVT2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCSIVT2_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCSIVT2_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oMCSIVT2_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oMCSIVT2_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_MCSIVT2
     i_obj.ecpSave()
  endproc

  add object oMCSTVT2_1_5 as StdField with uid="JVLPJXEFQE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MCSTVT2", cQueryName = "MCSTVT2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo per le spese di trasporto",;
    HelpContextID = 231735494,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=86, Top=128, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_MCSTVT2"

  func oMCSTVT2_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.oParentObject.cFunction, 'Edit', 'Load') AND NOT EMPTY(.w_MVCODVE2) AND .w_FLSPTR='S')
    endwith
   endif
  endfunc

  func oMCSTVT2_1_5.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  func oMCSTVT2_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCSTVT2_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCSTVT2_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oMCSTVT2_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oMCSTVT2_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_MCSTVT2
     i_obj.ecpSave()
  endproc

  add object oMCSIVT3_1_6 as StdField with uid="QCYSRVBICX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MCSIVT3", cQueryName = "MCSIVT3",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo per le spese di imballo",;
    HelpContextID = 231014598,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=86, Top=177, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_MCSIVT3"

  func oMCSIVT3_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.oParentObject.cFunction, 'Edit', 'Load') AND NOT EMPTY(.w_MVCODVE3) AND .w_FLSPIM='S')
    endwith
   endif
  endfunc

  func oMCSIVT3_1_6.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  func oMCSIVT3_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCSIVT3_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCSIVT3_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oMCSIVT3_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oMCSIVT3_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_MCSIVT3
     i_obj.ecpSave()
  endproc

  add object oMCSTVT3_1_7 as StdField with uid="IRTHECIGVL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MCSTVT3", cQueryName = "MCSTVT3",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo per le spese di trasporto",;
    HelpContextID = 231735494,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=86, Top=201, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_MCSTVT3"

  func oMCSTVT3_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.oParentObject.cFunction, 'Edit', 'Load') AND NOT EMPTY(.w_MVCODVE3) AND .w_FLSPTR='S')
    endwith
   endif
  endfunc

  func oMCSTVT3_1_7.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  func oMCSTVT3_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCSTVT3_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCSTVT3_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oMCSTVT3_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oMCSTVT3_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_MCSTVT3
     i_obj.ecpSave()
  endproc

  add object oMCALSI_1_8 as StdField with uid="SEDMDNFJER",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MCALSI", cQueryName = "MCALSI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo per le spese di imballo",;
    HelpContextID = 224993082,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=86, Top=264, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_MCALSI"

  func oMCALSI_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.oParentObject.cFunction, 'Edit', 'Load') And Empty(.w_MCSIVT1) And Empty(.w_MCSIVT2) And Empty(.w_MCSIVT3) AND .w_FLSPIM='S')
    endwith
   endif
  endfunc

  func oMCALSI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCALSI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCALSI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oMCALSI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oMCALSI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_MCALSI
     i_obj.ecpSave()
  endproc

  add object oDESMCIMB_1_9 as StdField with uid="KJZLACQFUQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESMCIMB", cQueryName = "DESMCIMB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 241630600,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=161, Top=264, InputMask=replicate('X',40)

  add object oMCALST_1_10 as StdField with uid="VDWFXYBSUT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MCALST", cQueryName = "MCALST",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo per le spese di trasporto",;
    HelpContextID = 40443706,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=86, Top=288, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_MCALST"

  func oMCALST_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.oParentObject.cFunction, 'Edit', 'Load') And Empty(.w_MCSTVT1) And Empty(.w_MCSTVT2) And Empty(.w_MCSTVT3) AND .w_FLSPTR='S')
    endwith
   endif
  endfunc

  func oMCALST_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCALST_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCALST_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oMCALST_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oMCALST_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_MCALST
     i_obj.ecpSave()
  endproc

  add object oDESMCTRA_1_11 as StdField with uid="NPGQFFDPOV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESMCTRA", cQueryName = "DESMCTRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 211354231,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=161, Top=288, InputMask=replicate('X',40)


  add object oBtn_1_12 as StdButton with uid="KHHDHHWPVJ",left=331, top=314, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 199821082;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="IKEKYDBJPL",left=383, top=314, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 192532410;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESIMBV1_1_16 as StdField with uid="IKBPRGVZQW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESIMBV1", cQueryName = "DESIMBV1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 188023399,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=161, Top=32, InputMask=replicate('X',40)

  func oDESIMBV1_1_16.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oDESTRAV1_1_17 as StdField with uid="HAJXMUBIKH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESTRAV1", cQueryName = "DESTRAV1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 177209959,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=161, Top=56, InputMask=replicate('X',40)

  func oDESTRAV1_1_17.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oDESIMBV2_1_20 as StdField with uid="EQGFIZJZUW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESIMBV2", cQueryName = "DESIMBV2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 188023400,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=161, Top=104, InputMask=replicate('X',40)

  func oDESIMBV2_1_20.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oDESTRAV2_1_21 as StdField with uid="ARFGOBMLOF",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESTRAV2", cQueryName = "DESTRAV2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 177209960,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=161, Top=128, InputMask=replicate('X',40)

  func oDESTRAV2_1_21.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oDESIMBV3_1_24 as StdField with uid="AWIJMEZWLH",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESIMBV3", cQueryName = "DESIMBV3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 188023401,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=161, Top=177, InputMask=replicate('X',40)

  func oDESIMBV3_1_24.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oDESTRAV3_1_25 as StdField with uid="YAAACRVFJT",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESTRAV3", cQueryName = "DESTRAV3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 177209961,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=161, Top=201, InputMask=replicate('X',40)

  func oDESTRAV3_1_25.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc


  add object oObj_1_41 as cp_runprogram with uid="UHZHAMSWBY",left=24, top=376, width=116,height=25,;
    caption='GSAR_BMS',;
   bGlobalFont=.t.,;
    prg="GSAR_BMS",;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Riposizionamento e ridimensionamento della maschera e suoi oggetti";
    , HelpContextID = 61017927

  add object oStr_1_14 as StdString with uid="UAGNOZSERA",Visible=.t., Left=13, Top=267,;
    Alignment=1, Width=68, Height=18,;
    Caption="Imballo:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="PDXRHGOVHZ",Visible=.t., Left=13, Top=292,;
    Alignment=1, Width=68, Height=18,;
    Caption="Trasporto:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="FVNYIPPFYO",Visible=.t., Left=13, Top=35,;
    Alignment=1, Width=68, Height=18,;
    Caption="Imballo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="MOCWGSZDQB",Visible=.t., Left=13, Top=60,;
    Alignment=1, Width=68, Height=18,;
    Caption="Trasporto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="YRUPXNYZQC",Visible=.t., Left=13, Top=107,;
    Alignment=1, Width=68, Height=18,;
    Caption="Imballo:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="UJKPLUFLQQ",Visible=.t., Left=13, Top=132,;
    Alignment=1, Width=68, Height=18,;
    Caption="Trasporto:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="TLYGENBYQH",Visible=.t., Left=13, Top=180,;
    Alignment=1, Width=68, Height=18,;
    Caption="Imballo:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="JDPKAQVAAY",Visible=.t., Left=13, Top=205,;
    Alignment=1, Width=68, Height=18,;
    Caption="Trasporto:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="CFPKOALKKJ",Visible=.t., Left=58, Top=9,;
    Alignment=0, Width=72, Height=18,;
    Caption="Vettore"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="OZLDKRPDPB",Visible=.t., Left=58, Top=81,;
    Alignment=0, Width=95, Height=18,;
    Caption="2^Vettore"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="HWHGCXBXGT",Visible=.t., Left=58, Top=154,;
    Alignment=0, Width=95, Height=18,;
    Caption="3^Vettore"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_PADRE='OFFE')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="OBAAPZUMVL",Visible=.t., Left=58, Top=240,;
    Alignment=0, Width=72, Height=18,;
    Caption="Documento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_28 as StdBox with uid="GQRDUTNULV",left=38, top=26, width=316,height=1

  add object oBox_1_32 as StdBox with uid="DSQMYOHKGA",left=38, top=98, width=316,height=1

  add object oBox_1_33 as StdBox with uid="ATINZJVWPW",left=38, top=171, width=316,height=1

  add object oBox_1_34 as StdBox with uid="UEUQNLQXKU",left=38, top=257, width=316,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kms','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
