* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kv1                                                        *
*              ZOOM PRIMANOTA                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-13                                                      *
* Last revis.: 2009-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kv1",oParentObject))

* --- Class definition
define class tgscg_kv1 as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 681
  Height = 327
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-10-29"
  HelpContextID=48899433
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  CAU_CONT_IDX = 0
  PNT_MAST_IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gscg_kv1"
  cComment = "ZOOM PRIMANOTA"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SERIALE = space(10)
  w_CODCAU = space(5)
  w_TIPCON = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_NUMINI = 0
  w_ALFINI = space(10)
  w_NUMFIN = 0
  w_ALFFIN = space(10)
  w_CODCON = space(15)
  w_TIPOCONTO = space(1)
  w_NUMREG = 0
  w_UTENTE = 0
  w_DATREG = ctod('  /  /  ')
  w_NREGINI = 0
  w_CODUTINI = 0
  w_NREGFIN = 0
  w_CODUTFIN = 0
  w_DADOCINI = ctod('  /  /  ')
  w_DADOCFIN = ctod('  /  /  ')
  w_CODESE = space(4)
  w_Zoompnt = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kv1Pag1","gscg_kv1",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoompnt = this.oPgFrm.Pages(1).oPag.Zoompnt
    DoDefault()
    proc Destroy()
      this.w_Zoompnt = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='ESERCIZI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SERIALE=space(10)
      .w_CODCAU=space(5)
      .w_TIPCON=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_NUMINI=0
      .w_ALFINI=space(10)
      .w_NUMFIN=0
      .w_ALFFIN=space(10)
      .w_CODCON=space(15)
      .w_TIPOCONTO=space(1)
      .w_NUMREG=0
      .w_UTENTE=0
      .w_DATREG=ctod("  /  /  ")
      .w_NREGINI=0
      .w_CODUTINI=0
      .w_NREGFIN=0
      .w_CODUTFIN=0
      .w_DADOCINI=ctod("  /  /  ")
      .w_DADOCFIN=ctod("  /  /  ")
      .w_CODESE=space(4)
      .w_CODCAU=oParentObject.w_CODCAU
      .w_TIPCON=oParentObject.w_TIPCON
      .w_DATINI=oParentObject.w_DATINI
      .w_DATFIN=oParentObject.w_DATFIN
      .w_NUMINI=oParentObject.w_NUMINI
      .w_ALFINI=oParentObject.w_ALFINI
      .w_NUMFIN=oParentObject.w_NUMFIN
      .w_ALFFIN=oParentObject.w_ALFFIN
      .w_CODCON=oParentObject.w_CODCON
      .w_NREGINI=oParentObject.w_NREGINI
      .w_CODUTINI=oParentObject.w_CODUTINI
      .w_NREGFIN=oParentObject.w_NREGFIN
      .w_CODUTFIN=oParentObject.w_CODUTFIN
      .w_DADOCINI=oParentObject.w_DADOCINI
      .w_DADOCFIN=oParentObject.w_DADOCFIN
      .w_CODESE=oParentObject.w_CODESE
        .w_SERIALE = .w_Zoompnt.getVar('PNSERIAL')
      .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
          .DoRTCalc(2,10,.f.)
        .w_TIPOCONTO = IIF(.w_TIPCON='N',' ',.w_TIPCON)
      .oPgFrm.Page1.oPag.Zoompnt.Calculate()
        .w_NUMREG = .w_Zoompnt.getVar('PNNUMRER')
        .w_UTENTE = .w_Zoompnt.getVar('PNCODUTE')
        .w_DATREG = .w_Zoompnt.getVar('PNDATREG')
    endwith
    this.DoRTCalc(15,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CODCAU=.w_CODCAU
      .oParentObject.w_TIPCON=.w_TIPCON
      .oParentObject.w_DATINI=.w_DATINI
      .oParentObject.w_DATFIN=.w_DATFIN
      .oParentObject.w_NUMINI=.w_NUMINI
      .oParentObject.w_ALFINI=.w_ALFINI
      .oParentObject.w_NUMFIN=.w_NUMFIN
      .oParentObject.w_ALFFIN=.w_ALFFIN
      .oParentObject.w_CODCON=.w_CODCON
      .oParentObject.w_NREGINI=.w_NREGINI
      .oParentObject.w_CODUTINI=.w_CODUTINI
      .oParentObject.w_NREGFIN=.w_NREGFIN
      .oParentObject.w_CODUTFIN=.w_CODUTFIN
      .oParentObject.w_DADOCINI=.w_DADOCINI
      .oParentObject.w_DADOCFIN=.w_DADOCFIN
      .oParentObject.w_CODESE=.w_CODESE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_SERIALE = .w_Zoompnt.getVar('PNSERIAL')
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .DoRTCalc(2,10,.t.)
            .w_TIPOCONTO = IIF(.w_TIPCON='N',' ',.w_TIPCON)
        .oPgFrm.Page1.oPag.Zoompnt.Calculate()
            .w_NUMREG = .w_Zoompnt.getVar('PNNUMRER')
            .w_UTENTE = .w_Zoompnt.getVar('PNCODUTE')
            .w_DATREG = .w_Zoompnt.getVar('PNDATREG')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.Zoompnt.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
      .oPgFrm.Page1.oPag.Zoompnt.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kv1Pag1 as StdContainer
  Width  = 677
  height = 327
  stdWidth  = 677
  stdheight = 327
  resizeXpos=315
  resizeYpos=203
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_4 as cp_runprogram with uid="PLAQTHLMHF",left=8, top=335, width=214,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BV1('SELEZ')",;
    cEvent = "w_Zoompnt selected",;
    nPag=1;
    , HelpContextID = 129098214


  add object Zoompnt as cp_zoombox with uid="TRPLKGPBRJ",left=6, top=3, width=663,height=315,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PNT_MAST",cZoomFile="GSCG_KVD",bOptions=.f.,bAdvOptions=.t.,bQueryOnLoad=.f.,bReadOnly=.f.,bRetriveAllRows=.f.,cZoomOnZoom="",cMenuFile="",bQueryOnDblClick=.t.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 129098214
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kv1','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
