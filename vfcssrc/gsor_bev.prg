* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_bev                                                        *
*              Stampa evadibilitą ordini                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_71]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-25                                                      *
* Last revis.: 2014-04-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsor_bev",oParentObject)
return(i_retval)

define class tgsor_bev as StdBatch
  * --- Local variables
  w_QUERYP = space(50)
  w_TIPDOC = space(2)
  w_TIPCON = space(1)
  w_FLVEAC = space(1)
  w_CODART = space(20)
  w_MAGAZZ = space(5)
  w_QTAPP = 0
  * --- WorkFile variables
  TMP_TOEVA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Evadibilitą Ordini (da GSOR_SEV)
    * --- Variabili per la Stampa
    * --- Caller per lancio di OpenRows
    * --- Variabili di Appoggio
    this.w_TIPDOC = "OR"
    this.w_TIPCON = "C"
    this.oParentObject.w_CODCON = ALLTRIM(this.oParentObject.w_CODCON)
    this.oParentObject.w_CODAGE = ALLTRIM(this.oParentObject.w_CODAGE)
    this.oParentObject.w_CODMAG = ALLTRIM(this.oParentObject.w_CODMAG)
    this.oParentObject.w_CODICE1 = ALLTRIM(this.oParentObject.w_CODICE1)
    this.oParentObject.w_CODICE2 = ALLTRIM(this.oParentObject.w_CODICE2)
    this.oParentObject.w_CAUORD = ALLTRIM(this.oParentObject.w_CAUORD)
    this.w_FLVEAC = "V"
    GSOR_BOR(this,this.w_TIPDOC,this.w_TIPCON,this.oParentObject.w_CODCON,this.oParentObject.w_DATDOC1,this.oParentObject.w_DATDOC2,this.oParentObject.w_CODAGE,this.oParentObject.w_CODMAG,this.oParentObject.w_CODICE1,this.oParentObject.w_CODICE2,this.oParentObject.w_DATAEV1,this.oParentObject.w_DATAEV2,this.w_FLVEAC, this.oParentObject.w_CAUORD,this.oParentObject.w_BNUMORD,this.oParentObject.w_ENUMORD,this.oParentObject.w_DAALFA,this.oParentObject.w_AALFA)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    do case
      case this.oParentObject.w_CRITERIO="A"
        * --- Criterio Evasione Alternativo: Verifico puntualmente l'evadibilitą degli ordini.
        *     Attraverso la query (campo TIPO) so gią quali sono evadibili e quali no
        * --- Create temporary table TMP_TOEVA
        i_nIdx=cp_AddTableDef('TMP_TOEVA') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('query\gsor_be1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMP_TOEVA_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      case this.oParentObject.w_CRITERIO="C"
        * --- Criterio Evasione Contemporaneo: Per verificare l'evadibilitą devo tenere
        *     conto degli ordini con data minore di quello che sto analizzando.
        *     Ordino quindi la TMP_TOEVA per la MVDATEVA.
        *     Per ogni 'riga' evadibile storno da w_QTAPP la quantitą di riga.
        * --- Create temporary table TMP_TOEVA
        i_nIdx=cp_AddTableDef('TMP_TOEVA') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('query\gsor_be3',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMP_TOEVA_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Select from TMP_TOEVA
        i_nConn=i_TableProp[this.TMP_TOEVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_TOEVA_idx,2],.t.,this.TMP_TOEVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_TOEVA ";
              +" where TIPO=1";
              +" order by MVCODART, MVCODMAG, MVDATEVA";
               ,"_Curs_TMP_TOEVA")
        else
          select * from (i_cTable);
           where TIPO=1;
           order by MVCODART, MVCODMAG, MVDATEVA;
            into cursor _Curs_TMP_TOEVA
        endif
        if used('_Curs_TMP_TOEVA')
          select _Curs_TMP_TOEVA
          locate for 1=1
          do while not(eof())
          * --- Ciclo su TMP_TOEVA e scrivo nel campo TIPO 0 se evadibili, 2 altrimenti.
          if (Empty(this.w_CODART) AND Empty(this.w_MAGAZZ)) OR ((_Curs_TMP_TOEVA.MVCODART<>this.w_CODART) OR (_Curs_TMP_TOEVA.MVCODMAG<>this.w_MAGAZZ)) 
            this.w_CODART = _Curs_TMP_TOEVA.MVCODART
            this.w_MAGAZZ = _Curs_TMP_TOEVA.MVCODMAG
            this.w_QTAPP = _Curs_TMP_TOEVA.ESISTEN
          endif
          if _Curs_TMP_TOEVA.MVQTASAL<=this.w_QTAPP
            * --- Write into TMP_TOEVA
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMP_TOEVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_TOEVA_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_TOEVA_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TIPO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_TOEVA','TIPO');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(_Curs_TMP_TOEVA.MVSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(_Curs_TMP_TOEVA.CPROWNUM);
                  +" and MVCODART = "+cp_ToStrODBC(_Curs_TMP_TOEVA.MVCODART);
                  +" and MVCODMAG = "+cp_ToStrODBC(_Curs_TMP_TOEVA.MVCODMAG);
                     )
            else
              update (i_cTable) set;
                  TIPO = 0;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = _Curs_TMP_TOEVA.MVSERIAL;
                  and CPROWNUM = _Curs_TMP_TOEVA.CPROWNUM;
                  and MVCODART = _Curs_TMP_TOEVA.MVCODART;
                  and MVCODMAG = _Curs_TMP_TOEVA.MVCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_QTAPP = this.w_QTAPP-_Curs_TMP_TOEVA.MVQTASAL
          else
            * --- Write into TMP_TOEVA
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMP_TOEVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_TOEVA_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_TOEVA_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TIPO ="+cp_NullLink(cp_ToStrODBC(2),'TMP_TOEVA','TIPO');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(_Curs_TMP_TOEVA.MVSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(_Curs_TMP_TOEVA.CPROWNUM);
                  +" and MVCODART = "+cp_ToStrODBC(_Curs_TMP_TOEVA.MVCODART);
                  +" and MVCODMAG = "+cp_ToStrODBC(_Curs_TMP_TOEVA.MVCODMAG);
                     )
            else
              update (i_cTable) set;
                  TIPO = 2;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = _Curs_TMP_TOEVA.MVSERIAL;
                  and CPROWNUM = _Curs_TMP_TOEVA.CPROWNUM;
                  and MVCODART = _Curs_TMP_TOEVA.MVCODART;
                  and MVCODMAG = _Curs_TMP_TOEVA.MVCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
            select _Curs_TMP_TOEVA
            continue
          enddo
          use
        endif
    endcase
    * --- Drop temporary table TMP_OROWS
    i_nIdx=cp_GetTableDefIdx('TMP_OROWS')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_OROWS')
    endif
    L_CRITERIO=this.oParentObject.w_CRITERIO
    L_CODMAG=this.oParentObject.w_CODMAG
    L_CODICE1=this.oParentObject.w_CODICE1
    L_CODICE2=this.oParentObject.w_CODICE2
    L_DATDOC1=this.oParentObject.w_DATDOC1
    L_DATDOC2=this.oParentObject.w_DATDOC2
    L_DATAEV1=this.oParentObject.w_DATAEV1
    L_DATAEV2=this.oParentObject.w_DATAEV2
    L_CODCON=this.oParentObject.w_CODCON
    L_CODAGE=this.oParentObject.w_CODAGE
    L_DISPONI=this.oParentObject.w_DISPONI
    L_CAUORD = this.oParentObject.w_CAUORD
    L_TDDESDOC = this.oParentObject.w_TDDESDOC
    L_BNUMORD=this.oParentObject.w_BNUMORD
    L_ENUMORD=this.oParentObject.w_ENUMORD
    L_DAALFA=this.oParentObject.w_DAALFA
    L_AALFA=this.oParentObject.w_AALFA
    this.w_QUERYP = ALLTRIM(this.oParentObject.w_OQRY)
    * --- Popolo il temporaneo __tmp__ attreaverso la query specificata nell'Output Utente
    vq_exec(this.w_QUERYP, this, "__TMP__")
    * --- Lancio il report specificato nell'Output Utente..
    CP_CHPRN( ALLTRIM(this.oParentObject.w_OREP), " ", this )
    USE IN SELECT("__TMP__")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMP_TOEVA'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_TMP_TOEVA')
      use in _Curs_TMP_TOEVA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
