* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_gmn                                                        *
*              Gadget Menu                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-01-29                                                      *
* Last revis.: 2016-03-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_gmn",oParentObject))

* --- Class definition
define class tgsut_gmn as StdGadgetForm
  Top    = 12
  Left   = 10

  * --- Standard Properties
  Width  = 123
  Height = 118
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-21"
  HelpContextID=64404631
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_gmn"
  cComment = "Gadget Menu"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_VMNFILE = space(254)
  o_VMNFILE = space(254)
  w_RET = .F.
  w_FIND = space(254)
  w_TITLE = space(254)
  o_TITLE = space(254)
  w_FONTCLR = 0
  w_SEARCH = space(254)
  w_MENUOBJ = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_gmn
  *--- ApplyTheme
  Proc ApplyTheme()
     Local l_find
     DoDefault()
     Local l_oldErr, l_OnErrorMsg
     With This
       m.l_OnErrorMsg = ""
       m.l_oldErr = On("Error")
       On Error m.l_OnErrorMsg=ah_MsgFormat("%1%0%2",Iif(Empty(m.l_OnErrorMsg), "Aggiornamento gadget:",m.l_OnErrorMsg),Message())
       
       .w_FONTCLR =  Iif(.nFontColor<>-1, .nFontColor, Rgb(243,243,243))
       l_find = .GetCtrl('W_FIND',1,1,'SEARCHFIELD','cFormVar')
       l_find.BorderColor = RGBAlphaBlending(.nBackColor, GetBestForeColor(.nBackColor), 0.3)
       l_find.BackColor = l_find.BorderColor
       l_find.ForeColor = .w_FONTCLR
       .w_MENUOBJ.SetColor(.nBackColor, .w_FONTCLR)
       
       On Error &l_oldErr
       If !Empty(m.l_OnErrorMsg) && c'� stato un errore di programma
          .cAlertMsg = Iif(Empty(.cAlertMsg), m.l_OnErrorMsg, ah_MsgFormat("%1%0%2",.cAlertMsg,m.l_OnErrorMsg))
       Endif
       
       .mCalc(.t.)
       .SaveDependsOn()
     Endwith
  EndProc
  * --- Fine Area Manuale
  *--- Define Header
  add object oHeader as cp_headergadget with Width=this.Width, Left=0, Top=0, nNumPages=1 
  Height = This.Height + this.oHeader.Height

  * --- Define Page Frame
  add object oPgFrm as cp_oPgFrm with PageCount=1, Top=this.oHeader.Height, Width=this.Width, Height=this.Height-this.oHeader.Height
  proc oPgFrm.Init
    cp_oPgFrm::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_gmnPag1","gsut_gmn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFIND_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_MENUOBJ = this.oPgFrm.Pages(1).oPag.MENUOBJ
    DoDefault()
    proc Destroy()
      this.w_MENUOBJ = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_gmn
    *--- Inizializzazioni e posizionamento oggetti
    
    Local l_find
    l_find = This.GetCtrl('W_FIND',1,1,'SEARCHFIELD','cFormVar')
    l_find.FontItalic = .T.
    l_find.bNoBackColor = .T.
    
    This.w_MENUOBJ.Move(5, 5, This.Width-10)
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VMNFILE=space(254)
      .w_RET=.f.
      .w_FIND=space(254)
      .w_TITLE=space(254)
      .w_FONTCLR=0
      .w_SEARCH=space(254)
          .DoRTCalc(1,2,.f.)
        .w_FIND = MSG_SEARCH
      .oPgFrm.Page1.oPag.MENUOBJ.Calculate(.w_VMNFILE)
          .DoRTCalc(4,4,.f.)
        .w_FONTCLR = Rgb(243,243,243)
        .w_SEARCH = MSG_SEARCH
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.MENUOBJ.Calculate(.w_VMNFILE)
      	*--- Assegnamento caption ad oggetto header
          If .o_TITLE <> .w_TITLE
			   .oHeader.Calculate(.oPgFrm.Page1.oPag.oContained.w_TITLE)
          EndIf
        if .o_VMNFILE<>.w_VMNFILE
          .Calculate_QQYAJZWBHK()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.MENUOBJ.Calculate(.w_VMNFILE)
    endwith
  return

  proc Calculate_YCUUTVASEB()
    with this
          * --- Gadget Option
          .w_RET = This.GadgetOption()
    endwith
  endproc
  proc Calculate_QQYAJZWBHK()
    with this
          * --- Verifica esistenza file.VMN
          .cAlertMsg = Iif(!Empty(.w_VMNFILE) And !cp_FileExist(ForceExt(.w_VMNFILE,'VMN')), ah_MsgFormat('File "%1" non trovato:%0caricato il menu corrente',Alltrim(.w_VMNFILE)), '')
    endwith
  endproc
  proc Calculate_RVSOEPYMGX()
    with this
          * --- Resize del Menu
          .w_RET = .w_MENUOBJ.ViewMenu()
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("GadgetOption")
          .Calculate_YCUUTVASEB()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.MENUOBJ.Event(cEvent)
        if lower(cEvent)==lower("MouseUpResizeGadget") or lower(cEvent)==lower("GadgetMaximize End") or lower(cEvent)==lower("GadgetMinimize End")
          .Calculate_RVSOEPYMGX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFIND_1_4.value==this.w_FIND)
      this.oPgFrm.Page1.oPag.oFIND_1_4.value=this.w_FIND
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VMNFILE = this.w_VMNFILE
    this.o_TITLE = this.w_TITLE
    return

enddefine

* --- Define pages as container
define class tgsut_gmnPag1 as StdContainer
  Width  = 123
  height = 118
  stdWidth  = 123
  stdheight = 118
  resizeXpos=100
  resizeYpos=62
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFIND_1_4 as SearchField with uid="XPFSKUGGSH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FIND", cQueryName = "FIND",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Voce di menu da cercare (almeno 3 caratteri)",;
    HelpContextID = 69200470,;
   bGlobalFont=.t.,;
    Height=21, Width=108, Left=7, Top=91, InputMask=replicate('X',254), SelectOnEntry=.T.


  add object oBtn_1_5 as SearchButton with uid="VUZESSFGDF",left=95, top=94, width=20,height=20,;
    caption="", nPag=1;
    , HelpContextID = 64404726;
    , Top=-10, Left=-10, Width=2, Height=2;
  , bGlobalFont=.t.

    proc oBtn_1_5.Click()
      with this.Parent.oContained
        Search(This.Parent.oContained)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object MENUOBJ as cp_MenuGrid with uid="ETSKWLKTCD",left=6, top=5, width=105,height=83,;
    caption='Menu',;
   bGlobalFont=.t.,;
    nIndent=0,cVmnFile="",cCursor="",ForeColor=16777215,;
    cEvent = "GadgetArranged,oheader RefreshOnDemand",;
    nPag=1;
    , HelpContextID = 72550086
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_gmn','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_gmn
* --- Classe per bottone nascosto
* --- quando ottiene il focus fa scattare
* --- la ricerca e passa il focus alla treeview

Define class SearchButton as StdButton

 Proc GotFocus
    This.Click()
    this.Parent.oContained.w_MENUOBJ.SetFocus()
 Endproc

Enddefine

* --- Classe per campo di ricerca
* --- quando ottiene il focus seleziona il testo contenuto

Define class SearchField as StdField

 Proc GotFocus
   DoDefault()
   If This.FontItalic
     This.FontItalic = .F.
     This.Value = ''
   Else
     This.SelStart = 1
     This.SelLength = LEN(This.Value)
     This.SetFocus()
     NODEFAULT
   Endif
 Endproc
 Proc LostFocus
   DoDefault()
   This.FontItalic = Empty(This.Value)
   This.Parent.oContained.w_SEARCH = This.Value
   This.Value = Iif(Empty(This.Value), MSG_SEARCH, This.Value)
   This.Parent.oContained.w_FIND = This.Value
 Endproc
 
Enddefine

Proc Search(pParent)
   m.pParent.w_MENUOBJ.Search(m.pParent.w_SEARCH)
EndProc
* --- Fine Area Manuale
