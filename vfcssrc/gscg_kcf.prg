* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kcf                                                        *
*              Calcola formula totalizzatori                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-07                                                      *
* Last revis.: 2009-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kcf",oParentObject))

* --- Class definition
define class tgscg_kcf as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 616
  Height = 184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-01-02"
  HelpContextID=99231081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_kcf"
  cComment = "Calcola formula totalizzatori"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIFORMUL = space(254)
  w_DTOBSO = ctod('  /  /  ')
  w_ERRMSG = space(1)
  w_CODTOT = space(10)
  w_CODMIS = space(15)
  w_CODICE = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kcfPag1","gscg_kcf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIFORMUL_1_22
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        gscg_bib(this,"CF")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIFORMUL=space(254)
      .w_DTOBSO=ctod("  /  /  ")
      .w_ERRMSG=space(1)
      .w_CODTOT=space(10)
      .w_CODMIS=space(15)
      .w_CODICE=space(10)
      .w_TIFORMUL=oParentObject.w_TIFORMUL
          .DoRTCalc(1,1,.f.)
        .w_DTOBSO = I_DATSYS
          .DoRTCalc(3,5,.f.)
        .w_CODICE = This.Oparentobject .w_TICODICE
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIFORMUL=.w_TIFORMUL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
            .w_CODICE = This.Oparentobject .w_TICODICE
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIFORMUL_1_22.value==this.w_TIFORMUL)
      this.oPgFrm.Page1.oPag.oTIFORMUL_1_22.value=this.w_TIFORMUL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kcfPag1 as StdContainer
  Width  = 612
  height = 184
  stdWidth  = 612
  stdheight = 184
  resizeXpos=326
  resizeYpos=182
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="DQFFCOVBWZ",left=77, top=142, width=25,height=21,;
    caption="(", nPag=1;
    , HelpContextID = 99230346;
  , bGlobalFont=.t.

    proc oBtn_1_1.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"(")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_2 as StdButton with uid="MQEUSHYDQR",left=108, top=43, width=25,height=21,;
    caption="+", nPag=1;
    , HelpContextID = 99230298;
  , bGlobalFont=.t.

    proc oBtn_1_2.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"+")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_3 as StdButton with uid="FXHSTHYJSL",left=108, top=68, width=25,height=21,;
    caption="-", nPag=1;
    , HelpContextID = 99230266;
  , bGlobalFont=.t.

    proc oBtn_1_3.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"-")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_4 as StdButton with uid="JIZTKPIOMO",left=108, top=93, width=25,height=21,;
    caption="*", nPag=1;
    , HelpContextID = 99230314;
  , bGlobalFont=.t.

    proc oBtn_1_4.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"*")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="MPKSNHVSKO",left=108, top=118, width=25,height=21,;
    caption="/", nPag=1;
    , HelpContextID = 99230234;
  , bGlobalFont=.t.

    proc oBtn_1_5.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"/")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="RIHGKTUCTN",left=108, top=142, width=25,height=21,;
    caption=")", nPag=1;
    , HelpContextID = 99230330;
  , bGlobalFont=.t.

    proc oBtn_1_6.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,")")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="PTABTKISDQ",left=77, top=43, width=25,height=21,;
    caption="9", nPag=1;
    , HelpContextID = 99230074;
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"9")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="SGTRSPWIBX",left=77, top=93, width=25,height=21,;
    caption="3", nPag=1;
    , HelpContextID = 99230170;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"3")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="HZEWJNCLCI",left=46, top=142, width=25,height=21,;
    caption=".", nPag=1;
    , HelpContextID = 99230250;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,".")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="HSHQEARWCQ",left=77, top=68, width=25,height=21,;
    caption="6", nPag=1;
    , HelpContextID = 99230122;
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"6")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="DEKFXLOESQ",left=46, top=43, width=25,height=21,;
    caption="8", nPag=1;
    , HelpContextID = 99230090;
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"8")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="XARRPTMIQC",left=46, top=93, width=25,height=21,;
    caption="2", nPag=1;
    , HelpContextID = 99230186;
  , bGlobalFont=.t.

    proc oBtn_1_12.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"2")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="ISWLOHVNVV",left=46, top=118, width=25,height=21,;
    caption="0", nPag=1;
    , HelpContextID = 99230218;
  , bGlobalFont=.t.

    proc oBtn_1_13.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"0")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="QCLUEFTLZZ",left=46, top=68, width=25,height=21,;
    caption="5", nPag=1;
    , HelpContextID = 99230138;
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"5")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="RKQVHORBBC",left=15, top=43, width=25,height=21,;
    caption="7", nPag=1;
    , HelpContextID = 99230106;
  , bGlobalFont=.t.

    proc oBtn_1_15.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"7")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="DSCGETDPJJ",left=15, top=93, width=25,height=21,;
    caption="1", nPag=1;
    , HelpContextID = 99230202;
  , bGlobalFont=.t.

    proc oBtn_1_16.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"1")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="WRVYTPHEIJ",left=15, top=68, width=25,height=21,;
    caption="4", nPag=1;
    , HelpContextID = 99230154;
  , bGlobalFont=.t.

    proc oBtn_1_17.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"4")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="HAOEAUUSBK",left=15, top=142, width=25,height=21,;
    caption="C", nPag=1;
    , HelpContextID = 99229914;
  , bGlobalFont=.t.

    proc oBtn_1_18.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"CZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="OWRLXNSCTD",left=15, top=118, width=25,height=21,;
    caption="CE", nPag=1;
    , HelpContextID = 99212250;
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      with this.Parent.oContained
        gscg_bib(this.Parent.oContained,"CE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Load' OR .cFunction ='Edit')
      endwith
    endif
  endfunc

  add object oTIFORMUL_1_22 as StdField with uid="NYEBOIGAUB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TIFORMUL", cQueryName = "TIFORMUL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 210340226,;
   bGlobalFont=.t.,;
    Height=21, Width=439, Left=157, Top=43, InputMask=replicate('X',254)


  add object oBtn_1_30 as StdButton with uid="QBMXSMUFEE",left=157, top=68, width=48,height=45,;
    CpPicture="BMP\VISUALI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare totalizzatore";
    , HelpContextID = 96562544;
    , caption='\<Totalizzatori';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        do GSCG_KTT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_31 as StdButton with uid="MWSBEATOIB",left=157, top=115, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la verifica della formula";
    , HelpContextID = 166094263;
    , caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        GSCG_BIB(this.Parent.oContained,"VE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_TIFORMUL))
      endwith
    endif
  endfunc

  add object oStr_1_21 as StdString with uid="RXWKIIJTUH",Visible=.t., Left=15, Top=20,;
    Alignment=0, Width=134, Height=18,;
    Caption="Parametri di calcolo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="OFSKIOSVAG",Visible=.t., Left=158, Top=23,;
    Alignment=0, Width=62, Height=18,;
    Caption="Formula"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="VGDUHGBWYZ",Visible=.t., Left=236, Top=97,;
    Alignment=0, Width=102, Height=15,;
    Caption="Sintassi corretta"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (.w_ERRMSG = 'N' or .w_ERRMSG = 'T' or .w_ERRMSG = ' ' or .w_ERRMSG = 'M')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="DBLJKWPFUV",Visible=.t., Left=234, Top=97,;
    Alignment=0, Width=153, Height=15,;
    Caption="Sintassi errata"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_ERRMSG = 'S' or .w_ERRMSG = 'T' or .w_ERRMSG = ' ' or .w_ERRMSG = 'M')
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="NATSBYTZMT",Visible=.t., Left=236, Top=97,;
    Alignment=0, Width=165, Height=18,;
    Caption="Misura non trovata"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_ERRMSG = 'N' or .w_ERRMSG = 'S' or .w_ERRMSG = ' ' or .w_ERRMSG = 'T')
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="NDTNMAKOCB",Visible=.t., Left=233, Top=97,;
    Alignment=0, Width=181, Height=18,;
    Caption="Totalizzatore non trovato"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_ERRMSG = 'N' or .w_ERRMSG = 'S' or .w_ERRMSG = ' ' or .w_ERRMSG = 'M')
    endwith
  endfunc

  add object oBox_1_20 as StdBox with uid="VCRBDJOJPH",left=4, top=17, width=600,height=158
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kcf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
