* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bed                                                        *
*              Altri quadri                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_32]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-24                                                      *
* Last revis.: 2003-02-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bed",oParentObject)
return(i_retval)

define class tgscg_bed as StdBatch
  * --- Local variables
  w_INIANNO = ctod("  /  /  ")
  w_FINANNO = ctod("  /  /  ")
  w_VE37 = 0
  w_VE38 = 0
  * --- WorkFile variables
  TMPVEND1_idx=0
  TMPVEND2_idx=0
  TMPVEND3_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_VE37 = 0
    this.w_VE38 = 0
    * --- Imposto il filtro sulle date (tutto l'anno solare)
    this.w_INIANNO = cp_CharToDate("01-01"+"-"+ALLTRIM(this.oParentObject.oParentObject.w_VP__ANNO))
    this.w_FINANNO = cp_CharToDate("31-12"+"-"+ALLTRIM(this.oParentObject.oParentObject.w_VP__ANNO))
    * --- Recupero le partite di apertura / chiusura. Aperte da Reg. con 
    *     competenza ne periodo e chiuse da registrazioni con data registrazione
    *     nel periodo divise per fattura esigibile (SERIAL)
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gscg3qed',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Recupero totale registrazioni ad esigibilitÓ differita di vendita
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gscg2qed',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gscg5qed',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPVEND3
    i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gscg7qed',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    vq_exec("QUERY\GSCG4QED.VQR",this,"DATI")
    SELECT DATI
    LOCATE FOR RIGO="VE37"
    if FOUND()
      this.w_VE37 = NVL(VALORE,0)
    endif
    SELECT DATI
    LOCATE FOR RIGO="VE38"
    if FOUND()
      this.w_VE38 = NVL(VALORE,0)
    endif
    this.w_VE37 = INTROUND(this.w_VE37, 1)
    this.w_VE38 = INTROUND(this.w_VE38, 1)
    do GSCG_SED with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if USED("DATI")
      SELECT DATI
      USE
    endif
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
    * --- Drop temporary table TMPVEND2
    i_nIdx=cp_GetTableDefIdx('TMPVEND2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND2')
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='*TMPVEND1'
    this.cWorkTables[2]='*TMPVEND2'
    this.cWorkTables[3]='*TMPVEND3'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
