* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_sdv                                                        *
*              Prospetto del venduto                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_488]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2007-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_sdv",oParentObject))

* --- Class definition
define class tgsma_sdv as StdForm
  Top    = 7
  Left   = 35

  * --- Standard Properties
  Width  = 590
  Height = 392+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-13"
  HelpContextID=74087273
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=76

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  ART_ICOL_IDX = 0
  LISTINI_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  CATECOMM_IDX = 0
  ZONE_IDX = 0
  PAG_AMEN_IDX = 0
  VALUTE_IDX = 0
  INVENTAR_IDX = 0
  GRUMERC_IDX = 0
  MARCHI_IDX = 0
  FAM_ARTI_IDX = 0
  CATEGOMO_IDX = 0
  VETTORI_IDX = 0
  cPrg = "gsma_sdv"
  cComment = "Prospetto del venduto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATA = ctod('  /  /  ')
  o_DATA = ctod('  /  /  ')
  w_Dadata = ctod('  /  /  ')
  w_Adata = ctod('  /  /  ')
  w_DATSTA = ctod('  /  /  ')
  w_costmarg = space(1)
  o_costmarg = space(1)
  w_Aggiorn = space(27)
  o_Aggiorn = space(27)
  w_FLPUNT = space(1)
  o_FLPUNT = space(1)
  w_AZIENDA = space(5)
  w_LISTINO = space(8)
  w_DECTOT = 0
  w_CODESE = space(4)
  w_VALUTA = space(3)
  o_VALUTA = space(3)
  w_TIPOLN = space(1)
  w_Numero = space(6)
  o_Numero = space(6)
  w_DATINV = ctod('  /  /  ')
  w_ONUME = 0
  w_LISTINO = space(8)
  w_tipocon = space(1)
  w_VALUTA = space(3)
  w_Contrnu = space(20)
  w_CAMBIO = 0
  w_Contrli = space(20)
  w_CAOVAL1 = 0
  w_CAMBIO1 = 0
  w_cliente = space(15)
  w_catcomm = space(3)
  w_desccat = space(35)
  w_zona = space(3)
  w_desczona = space(35)
  w_agente = space(5)
  w_descage = space(35)
  w_pagam = space(5)
  w_CODART = space(20)
  w_CODART1 = space(20)
  w_GRUMER = space(5)
  w_CODMAR = space(5)
  w_descpaga = space(30)
  w_CODFAM = space(5)
  w_CODCAT = space(5)
  w_stval = space(10)
  o_stval = space(10)
  w_valu2 = space(3)
  o_valu2 = space(3)
  w_CAMBIO2 = 0
  w_descvalu2 = space(35)
  w_CAOVAL2 = 0
  w_doctra = space(30)
  w_Fatture = space(10)
  w_ricfisc = space(10)
  w_notecred = space(10)
  w_desccli = space(40)
  w_TIPO = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_Client = space(1)
  w_codtra = space(2)
  w_codfa = space(2)
  w_codric = space(2)
  w_codnot = space(2)
  w_dectot2 = 0
  w_VALESE = space(3)
  w_CAOESE = 0
  w_DESART = space(40)
  w_GDESC = space(35)
  w_CALCPICT = space(1)
  w_OBTEST1 = ctod('  /  /  ')
  w_SPEACC = space(1)
  w_SIMVAL = space(5)
  w_SIMVAL2 = space(5)
  w_SIMVAL = space(5)
  w_MADESCRI = space(35)
  w_DESFAM = space(35)
  w_DESOMO = space(35)
  w_DESART1 = space(40)
  w_STALOG = space(1)
  w_stato = space(20)
  w_CODVET = space(5)
  w_DESVET = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_sdvPag1","gsma_sdv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsma_sdvPag2","gsma_sdv",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri aggiuntivi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDadata_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[15]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='LISTINI'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='AGENTI'
    this.cWorkTables[6]='CATECOMM'
    this.cWorkTables[7]='ZONE'
    this.cWorkTables[8]='PAG_AMEN'
    this.cWorkTables[9]='VALUTE'
    this.cWorkTables[10]='INVENTAR'
    this.cWorkTables[11]='GRUMERC'
    this.cWorkTables[12]='MARCHI'
    this.cWorkTables[13]='FAM_ARTI'
    this.cWorkTables[14]='CATEGOMO'
    this.cWorkTables[15]='VETTORI'
    return(this.OpenAllTables(15))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSVE_BPV with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATA=ctod("  /  /  ")
      .w_Dadata=ctod("  /  /  ")
      .w_Adata=ctod("  /  /  ")
      .w_DATSTA=ctod("  /  /  ")
      .w_costmarg=space(1)
      .w_Aggiorn=space(27)
      .w_FLPUNT=space(1)
      .w_AZIENDA=space(5)
      .w_LISTINO=space(8)
      .w_DECTOT=0
      .w_CODESE=space(4)
      .w_VALUTA=space(3)
      .w_TIPOLN=space(1)
      .w_Numero=space(6)
      .w_DATINV=ctod("  /  /  ")
      .w_ONUME=0
      .w_LISTINO=space(8)
      .w_tipocon=space(1)
      .w_VALUTA=space(3)
      .w_Contrnu=space(20)
      .w_CAMBIO=0
      .w_Contrli=space(20)
      .w_CAOVAL1=0
      .w_CAMBIO1=0
      .w_cliente=space(15)
      .w_catcomm=space(3)
      .w_desccat=space(35)
      .w_zona=space(3)
      .w_desczona=space(35)
      .w_agente=space(5)
      .w_descage=space(35)
      .w_pagam=space(5)
      .w_CODART=space(20)
      .w_CODART1=space(20)
      .w_GRUMER=space(5)
      .w_CODMAR=space(5)
      .w_descpaga=space(30)
      .w_CODFAM=space(5)
      .w_CODCAT=space(5)
      .w_stval=space(10)
      .w_valu2=space(3)
      .w_CAMBIO2=0
      .w_descvalu2=space(35)
      .w_CAOVAL2=0
      .w_doctra=space(30)
      .w_Fatture=space(10)
      .w_ricfisc=space(10)
      .w_notecred=space(10)
      .w_desccli=space(40)
      .w_TIPO=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_Client=space(1)
      .w_codtra=space(2)
      .w_codfa=space(2)
      .w_codric=space(2)
      .w_codnot=space(2)
      .w_dectot2=0
      .w_VALESE=space(3)
      .w_CAOESE=0
      .w_DESART=space(40)
      .w_GDESC=space(35)
      .w_CALCPICT=space(1)
      .w_OBTEST1=ctod("  /  /  ")
      .w_SPEACC=space(1)
      .w_SIMVAL=space(5)
      .w_SIMVAL2=space(5)
      .w_SIMVAL=space(5)
      .w_MADESCRI=space(35)
      .w_DESFAM=space(35)
      .w_DESOMO=space(35)
      .w_DESART1=space(40)
      .w_STALOG=space(1)
      .w_stato=space(20)
      .w_CODVET=space(5)
      .w_DESVET=space(35)
        .w_DATA = IIF(.w_COSTMARG='S' AND .w_AGGIORN$'LICO-FICO' AND .w_FLPUNT='S' AND NOT EMPTY(.w_NUMERO), .w_DATINV +1, cp_CharToDate('    -  -  '))
        .w_Dadata = .w_DATA
        .w_Adata = i_datsys
        .w_DATSTA = i_DATSYS
        .w_costmarg = 'N'
        .w_Aggiorn = 'CMPA'
        .w_FLPUNT = 'N'
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_LISTINO))
          .link_1_17('Full')
        endif
          .DoRTCalc(10,10,.f.)
        .w_CODESE = g_CODESE
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODESE))
          .link_1_20('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_VALUTA))
          .link_1_21('Full')
        endif
        .DoRTCalc(13,14,.f.)
        if not(empty(.w_Numero))
          .link_1_23('Full')
        endif
        .DoRTCalc(15,17,.f.)
        if not(empty(.w_LISTINO))
          .link_1_26('Full')
        endif
        .w_tipocon = 'C'
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_VALUTA))
          .link_1_28('Full')
        endif
        .w_Contrnu = IIF(.w_costmarg='S',IIF(.w_Aggiorn<>'LIST' AND (NOT EMPTY(.w_Numero)),'N',' '),'X')
        .w_CAMBIO = IIF(.w_CAOVAL1<>0,.w_CAOVAL1,GETCAM(.w_VALUTA, .w_Adata, 7))
        .w_Contrli = IIF(.w_costmarg='S',IIF(.w_Aggiorn='LIST' AND (NOT EMPTY(.w_LISTINO)),'L',' '),'X')
          .DoRTCalc(23,23,.f.)
        .w_CAMBIO1 = IIF(.w_CAOVAL1<>0,.w_CAOVAL1,GETCAM(.w_VALUTA, .w_Adata, 7))
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_cliente))
          .link_2_1('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_catcomm))
          .link_2_3('Full')
        endif
        .DoRTCalc(27,28,.f.)
        if not(empty(.w_zona))
          .link_2_6('Full')
        endif
        .DoRTCalc(29,30,.f.)
        if not(empty(.w_agente))
          .link_2_9('Full')
        endif
        .DoRTCalc(31,32,.f.)
        if not(empty(.w_pagam))
          .link_2_12('Full')
        endif
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_CODART))
          .link_2_13('Full')
        endif
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_CODART1))
          .link_2_14('Full')
        endif
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_GRUMER))
          .link_2_15('Full')
        endif
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_CODMAR))
          .link_2_16('Full')
        endif
        .DoRTCalc(37,38,.f.)
        if not(empty(.w_CODFAM))
          .link_2_18('Full')
        endif
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_CODCAT))
          .link_2_19('Full')
        endif
        .w_stval = 'C'
        .w_valu2 = iif(.w_STVAL='C',g_PERVAL,'')
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_valu2))
          .link_2_25('Full')
        endif
        .w_CAMBIO2 = GETCAM(.w_VALU2, .w_Adata, 7)
          .DoRTCalc(43,44,.f.)
        .w_doctra = 'S'
        .w_Fatture = 'S'
        .w_ricfisc = 'S'
        .w_notecred = 'S'
          .DoRTCalc(49,49,.f.)
        .w_TIPO = 'C'
        .w_OBTEST = i_INIDAT
          .DoRTCalc(52,52,.f.)
        .w_Client = 'C'
        .w_codtra = IIF(.w_doctra='S','DT',' ')
        .w_codfa = IIF(.w_Fatture='S','FA',' ')
        .w_codric = IIF(.w_ricfisc='S','RF',' ')
        .w_codnot = IIF(.w_notecred='S','NC',' ')
        .DoRTCalc(58,59,.f.)
        if not(empty(.w_VALESE))
          .link_1_42('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
          .DoRTCalc(60,62,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT2)
        .w_OBTEST1 = i_DATSYS
        .w_SPEACC = 'S'
        .w_SIMVAL = IIF(EMPTY(.w_VALUTA),' ',.w_SIMVAL)
        .w_SIMVAL2 = IIF(EMPTY(.w_VALU2),' ',.w_SIMVAL2)
        .w_SIMVAL = IIF(EMPTY(.w_VALUTA),' ',.w_SIMVAL)
          .DoRTCalc(69,72,.f.)
        .w_STALOG = ' '
        .w_stato = 'N'
        .DoRTCalc(75,75,.f.)
        if not(empty(.w_CODVET))
          .link_2_62('Full')
        endif
    endwith
    this.DoRTCalc(76,76,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_DATA = IIF(.w_COSTMARG='S' AND .w_AGGIORN$'LICO-FICO' AND .w_FLPUNT='S' AND NOT EMPTY(.w_NUMERO), .w_DATINV +1, cp_CharToDate('    -  -  '))
        if .o_Aggiorn<>.w_Aggiorn.or. .o_costmarg<>.w_costmarg.or. .o_FLPUNT<>.w_FLPUNT.or. .o_DATA<>.w_DATA
            .w_Dadata = .w_DATA
        endif
        .DoRTCalc(3,6,.t.)
        if .o_AGGIORN<>.w_AGGIORN
            .w_FLPUNT = 'N'
        endif
        .DoRTCalc(8,10,.t.)
        if .o_Aggiorn<>.w_Aggiorn
          .link_1_20('Full')
        endif
          .link_1_21('Full')
        .DoRTCalc(13,18,.t.)
          .link_1_28('Full')
            .w_Contrnu = IIF(.w_costmarg='S',IIF(.w_Aggiorn<>'LIST' AND (NOT EMPTY(.w_Numero)),'N',' '),'X')
        if .o_VALUTA<>.w_VALUTA
            .w_CAMBIO = IIF(.w_CAOVAL1<>0,.w_CAOVAL1,GETCAM(.w_VALUTA, .w_Adata, 7))
        endif
            .w_Contrli = IIF(.w_costmarg='S',IIF(.w_Aggiorn='LIST' AND (NOT EMPTY(.w_LISTINO)),'L',' '),'X')
        .DoRTCalc(23,23,.t.)
        if .o_VALUTA<>.w_VALUTA
            .w_CAMBIO1 = IIF(.w_CAOVAL1<>0,.w_CAOVAL1,GETCAM(.w_VALUTA, .w_Adata, 7))
        endif
        .DoRTCalc(25,40,.t.)
        if .o_stval<>.w_stval
            .w_valu2 = iif(.w_STVAL='C',g_PERVAL,'')
          .link_2_25('Full')
        endif
        if .o_valu2<>.w_valu2
            .w_CAMBIO2 = GETCAM(.w_VALU2, .w_Adata, 7)
        endif
        .DoRTCalc(43,53,.t.)
            .w_codtra = IIF(.w_doctra='S','DT',' ')
            .w_codfa = IIF(.w_Fatture='S','FA',' ')
            .w_codric = IIF(.w_ricfisc='S','RF',' ')
            .w_codnot = IIF(.w_notecred='S','NC',' ')
        .DoRTCalc(58,58,.t.)
          .link_1_42('Full')
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .DoRTCalc(60,62,.t.)
        if .o_valu2<>.w_valu2
            .w_CALCPICT = DEFPIC(.w_DECTOT2)
        endif
        .DoRTCalc(64,65,.t.)
            .w_SIMVAL = IIF(EMPTY(.w_VALUTA),' ',.w_SIMVAL)
            .w_SIMVAL2 = IIF(EMPTY(.w_VALU2),' ',.w_SIMVAL2)
            .w_SIMVAL = IIF(EMPTY(.w_VALUTA),' ',.w_SIMVAL)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(69,76,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDadata_1_2.enabled = this.oPgFrm.Page1.oPag.oDadata_1_2.mCond()
    this.oPgFrm.Page1.oPag.oAggiorn_1_11.enabled = this.oPgFrm.Page1.oPag.oAggiorn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oLISTINO_1_17.enabled = this.oPgFrm.Page1.oPag.oLISTINO_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCODESE_1_20.enabled = this.oPgFrm.Page1.oPag.oCODESE_1_20.mCond()
    this.oPgFrm.Page1.oPag.oNumero_1_23.enabled = this.oPgFrm.Page1.oPag.oNumero_1_23.mCond()
    this.oPgFrm.Page1.oPag.oLISTINO_1_26.enabled = this.oPgFrm.Page1.oPag.oLISTINO_1_26.mCond()
    this.oPgFrm.Page1.oPag.oCAMBIO_1_30.enabled = this.oPgFrm.Page1.oPag.oCAMBIO_1_30.mCond()
    this.oPgFrm.Page1.oPag.oCAMBIO1_1_37.enabled = this.oPgFrm.Page1.oPag.oCAMBIO1_1_37.mCond()
    this.oPgFrm.Page2.oPag.ovalu2_2_25.enabled = this.oPgFrm.Page2.oPag.ovalu2_2_25.mCond()
    this.oPgFrm.Page2.oPag.oCAMBIO2_2_27.enabled = this.oPgFrm.Page2.oPag.oCAMBIO2_2_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oFLPUNT_1_12.visible=!this.oPgFrm.Page1.oPag.oFLPUNT_1_12.mHide()
    this.oPgFrm.Page1.oPag.oLISTINO_1_17.visible=!this.oPgFrm.Page1.oPag.oLISTINO_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCODESE_1_20.visible=!this.oPgFrm.Page1.oPag.oCODESE_1_20.mHide()
    this.oPgFrm.Page1.oPag.oVALUTA_1_21.visible=!this.oPgFrm.Page1.oPag.oVALUTA_1_21.mHide()
    this.oPgFrm.Page1.oPag.oNumero_1_23.visible=!this.oPgFrm.Page1.oPag.oNumero_1_23.mHide()
    this.oPgFrm.Page1.oPag.oCAMBIO_1_30.visible=!this.oPgFrm.Page1.oPag.oCAMBIO_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oCAMBIO1_1_37.visible=!this.oPgFrm.Page1.oPag.oCAMBIO1_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAL_1_49.visible=!this.oPgFrm.Page1.oPag.oSIMVAL_1_49.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_45.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LISTINO
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LISTINO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LISTINO)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LISTINO))
          select LSCODLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LISTINO)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LISTINO) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLISTINO_1_17'),i_cWhere,'GSAR_ALI',"Listini",'GSMAPDV.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LISTINO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LISTINO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LISTINO)
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LISTINO = NVL(_Link_.LSCODLIS,space(8))
      this.w_VALUTA = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPOLN = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LISTINO = space(8)
      endif
      this.w_VALUTA = space(3)
      this.w_TIPOLN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LISTINO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_20'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALESE = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_VALESE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_CAOVAL1 = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECTOT = 0
      this.w_CAOVAL1 = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=Numero
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_Numero) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsma_ain',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_Numero)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_CODESE);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_CODESE;
                     ,'INNUMINV',trim(this.w_Numero))
          select INCODESE,INNUMINV,INDATINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_Numero)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_Numero) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oNumero_1_23'),i_cWhere,'gsma_ain',"Inventari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Numero inventario errato o mancante")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_CODESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_Numero)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_Numero);
                   +" and INCODESE="+cp_ToStrODBC(this.w_CODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_CODESE;
                       ,'INNUMINV',this.w_Numero)
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_Numero = NVL(_Link_.INNUMINV,space(6))
      this.w_CODESE = NVL(_Link_.INCODESE,space(4))
      this.w_DATINV = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_Numero = space(6)
      endif
      this.w_CODESE = space(4)
      this.w_DATINV = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_Numero Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LISTINO
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LISTINO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LISTINO)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LISTINO))
          select LSCODLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LISTINO)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LISTINO) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLISTINO_1_26'),i_cWhere,'GSAR_ALI',"Listini",'GSMAPDV.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LISTINO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LISTINO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LISTINO)
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LISTINO = NVL(_Link_.LSCODLIS,space(8))
      this.w_VALUTA = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPOLN = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LISTINO = space(8)
      endif
      this.w_VALUTA = space(3)
      this.w_TIPOLN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LISTINO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_CAOVAL1 = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECTOT = 0
      this.w_CAOVAL1 = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=cliente
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_cliente) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_cliente)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_Client);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_Client;
                     ,'ANCODICE',trim(this.w_cliente))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_cliente)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_cliente)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_Client);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_cliente)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_Client);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_cliente) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'ocliente_2_1'),i_cWhere,'GSAR_BZC',"Clienti",'GSAR_KMS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_Client<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice cliente � inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_Client);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_cliente)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_cliente);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_Client);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_Client;
                       ,'ANCODICE',this.w_cliente)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_cliente = NVL(_Link_.ANCODICE,space(15))
      this.w_desccli = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_cliente = space(15)
      endif
      this.w_desccli = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice cliente � inesistente oppure obsoleto")
        endif
        this.w_cliente = space(15)
        this.w_desccli = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_cliente Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=catcomm
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_catcomm) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_catcomm)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_catcomm))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_catcomm)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_catcomm) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'ocatcomm_2_3'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_catcomm)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_catcomm);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_catcomm)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_catcomm = NVL(_Link_.CTCODICE,space(3))
      this.w_desccat = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_catcomm = space(3)
      endif
      this.w_desccat = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_catcomm Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=zona
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_zona) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_zona)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_zona))
          select ZOCODZON,ZODTOBSO,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_zona)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_zona) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'ozona_2_6'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODTOBSO,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_zona)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_zona);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_zona)
            select ZOCODZON,ZODTOBSO,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_zona = NVL(_Link_.ZOCODZON,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
      this.w_desczona = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_zona = space(3)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_desczona = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endif
        this.w_zona = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_desczona = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_zona Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=agente
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_agente) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_agente)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDTOBSO,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_agente))
          select AGCODAGE,AGDTOBSO,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_agente)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_agente) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oagente_2_9'),i_cWhere,'GSAR_AGE',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDTOBSO,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDTOBSO,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_agente)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDTOBSO,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_agente);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_agente)
            select AGCODAGE,AGDTOBSO,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_agente = NVL(_Link_.AGCODAGE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_descage = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_agente = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_descage = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente inesistente oppure obsoleto")
        endif
        this.w_agente = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_descage = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_agente Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=pagam
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_pagam) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_pagam)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_pagam))
          select PACODICE,PADTOBSO,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_pagam)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_pagam) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'opagam_2_12'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADTOBSO,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_pagam)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_pagam);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_pagam)
            select PACODICE,PADTOBSO,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_pagam = NVL(_Link_.PACODICE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
      this.w_descpaga = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_pagam = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_descpaga = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_pagam = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_descpaga = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_pagam Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_2_13'),i_cWhere,'',"Articoli/servizi",'ARTZOOM1.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_CODART1)) OR  (upper(.w_CODART)<=upper(.w_CODART1))) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART1
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART1)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART1))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART1)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODART1) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART1_2_14'),i_cWhere,'GSMA_BZA',"",'ARTZOOM1.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART1)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART1 = NVL(_Link_.ARCODART,space(20))
      this.w_DESART1 = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODART1 = space(20)
      endif
      this.w_DESART1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((upper(.w_CODART1)>= upper(.w_CODART)) or (empty(.w_CODART1))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_CODART1 = space(20)
        this.w_DESART1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMER
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMER_2_15'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_GDESC = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMER = space(5)
      endif
      this.w_GDESC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAR
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_CODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oCODMAR_2_16'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_CODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_CODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_MADESCRI = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAR = space(5)
      endif
      this.w_MADESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_2_18'),i_cWhere,'GSAR_AFA',"Famiglie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCAT_2_19'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESOMO = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(5)
      endif
      this.w_DESOMO = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=valu2
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_valu2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_valu2)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_valu2))
          select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_valu2)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_valu2) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'ovalu2_2_25'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_valu2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_valu2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_valu2)
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_valu2 = NVL(_Link_.VACODVAL,space(3))
      this.w_descvalu2 = NVL(_Link_.VADESVAL,space(35))
      this.w_dectot2 = NVL(_Link_.VADECTOT,0)
      this.w_CAOVAL2 = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL2 = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_valu2 = space(3)
      endif
      this.w_descvalu2 = space(35)
      this.w_dectot2 = 0
      this.w_CAOVAL2 = 0
      this.w_SIMVAL2 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_valu2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALESE
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   +" and VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALESE;
                       ,'VACODVAL',this.w_VALESE)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALESE = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOESE = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALESE = space(3)
      endif
      this.w_CAOESE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVET
  func Link_2_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_lTable = "VETTORI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2], .t., this.VETTORI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVT',True,'VETTORI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VTCODVET like "+cp_ToStrODBC(trim(this.w_CODVET)+"%");

          i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VTCODVET","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VTCODVET',trim(this.w_CODVET))
          select VTCODVET,VTDESVET;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VTCODVET into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVET)==trim(_Link_.VTCODVET) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStrODBC(trim(this.w_CODVET)+"%");

            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStr(trim(this.w_CODVET)+"%");

            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODVET) and !this.bDontReportError
            deferred_cp_zoom('VETTORI','*','VTCODVET',cp_AbsName(oSource.parent,'oCODVET_2_62'),i_cWhere,'GSAR_AVT',"Vettori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                     +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',oSource.xKey(1))
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                   +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(this.w_CODVET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',this.w_CODVET)
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVET = NVL(_Link_.VTCODVET,space(5))
      this.w_DESVET = NVL(_Link_.VTDESVET,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODVET = space(5)
      endif
      this.w_DESVET = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])+'\'+cp_ToStr(_Link_.VTCODVET,1)
      cp_ShowWarn(i_cKey,this.VETTORI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDadata_1_2.value==this.w_Dadata)
      this.oPgFrm.Page1.oPag.oDadata_1_2.value=this.w_Dadata
    endif
    if not(this.oPgFrm.Page1.oPag.oAdata_1_4.value==this.w_Adata)
      this.oPgFrm.Page1.oPag.oAdata_1_4.value=this.w_Adata
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_6.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_6.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.ocostmarg_1_7.RadioValue()==this.w_costmarg)
      this.oPgFrm.Page1.oPag.ocostmarg_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAggiorn_1_11.RadioValue()==this.w_Aggiorn)
      this.oPgFrm.Page1.oPag.oAggiorn_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPUNT_1_12.RadioValue()==this.w_FLPUNT)
      this.oPgFrm.Page1.oPag.oFLPUNT_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLISTINO_1_17.value==this.w_LISTINO)
      this.oPgFrm.Page1.oPag.oLISTINO_1_17.value=this.w_LISTINO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_20.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_20.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_21.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_21.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oNumero_1_23.value==this.w_Numero)
      this.oPgFrm.Page1.oPag.oNumero_1_23.value=this.w_Numero
    endif
    if not(this.oPgFrm.Page1.oPag.oLISTINO_1_26.value==this.w_LISTINO)
      this.oPgFrm.Page1.oPag.oLISTINO_1_26.value=this.w_LISTINO
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_28.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_28.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMBIO_1_30.value==this.w_CAMBIO)
      this.oPgFrm.Page1.oPag.oCAMBIO_1_30.value=this.w_CAMBIO
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMBIO1_1_37.value==this.w_CAMBIO1)
      this.oPgFrm.Page1.oPag.oCAMBIO1_1_37.value=this.w_CAMBIO1
    endif
    if not(this.oPgFrm.Page2.oPag.ocliente_2_1.value==this.w_cliente)
      this.oPgFrm.Page2.oPag.ocliente_2_1.value=this.w_cliente
    endif
    if not(this.oPgFrm.Page2.oPag.ocatcomm_2_3.value==this.w_catcomm)
      this.oPgFrm.Page2.oPag.ocatcomm_2_3.value=this.w_catcomm
    endif
    if not(this.oPgFrm.Page2.oPag.odesccat_2_4.value==this.w_desccat)
      this.oPgFrm.Page2.oPag.odesccat_2_4.value=this.w_desccat
    endif
    if not(this.oPgFrm.Page2.oPag.ozona_2_6.value==this.w_zona)
      this.oPgFrm.Page2.oPag.ozona_2_6.value=this.w_zona
    endif
    if not(this.oPgFrm.Page2.oPag.odesczona_2_7.value==this.w_desczona)
      this.oPgFrm.Page2.oPag.odesczona_2_7.value=this.w_desczona
    endif
    if not(this.oPgFrm.Page2.oPag.oagente_2_9.value==this.w_agente)
      this.oPgFrm.Page2.oPag.oagente_2_9.value=this.w_agente
    endif
    if not(this.oPgFrm.Page2.oPag.odescage_2_10.value==this.w_descage)
      this.oPgFrm.Page2.oPag.odescage_2_10.value=this.w_descage
    endif
    if not(this.oPgFrm.Page2.oPag.opagam_2_12.value==this.w_pagam)
      this.oPgFrm.Page2.oPag.opagam_2_12.value=this.w_pagam
    endif
    if not(this.oPgFrm.Page2.oPag.oCODART_2_13.value==this.w_CODART)
      this.oPgFrm.Page2.oPag.oCODART_2_13.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page2.oPag.oCODART1_2_14.value==this.w_CODART1)
      this.oPgFrm.Page2.oPag.oCODART1_2_14.value=this.w_CODART1
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUMER_2_15.value==this.w_GRUMER)
      this.oPgFrm.Page2.oPag.oGRUMER_2_15.value=this.w_GRUMER
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMAR_2_16.value==this.w_CODMAR)
      this.oPgFrm.Page2.oPag.oCODMAR_2_16.value=this.w_CODMAR
    endif
    if not(this.oPgFrm.Page2.oPag.odescpaga_2_17.value==this.w_descpaga)
      this.oPgFrm.Page2.oPag.odescpaga_2_17.value=this.w_descpaga
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFAM_2_18.value==this.w_CODFAM)
      this.oPgFrm.Page2.oPag.oCODFAM_2_18.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCAT_2_19.value==this.w_CODCAT)
      this.oPgFrm.Page2.oPag.oCODCAT_2_19.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page2.oPag.ostval_2_23.RadioValue()==this.w_stval)
      this.oPgFrm.Page2.oPag.ostval_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.ovalu2_2_25.value==this.w_valu2)
      this.oPgFrm.Page2.oPag.ovalu2_2_25.value=this.w_valu2
    endif
    if not(this.oPgFrm.Page2.oPag.oCAMBIO2_2_27.value==this.w_CAMBIO2)
      this.oPgFrm.Page2.oPag.oCAMBIO2_2_27.value=this.w_CAMBIO2
    endif
    if not(this.oPgFrm.Page2.oPag.odoctra_2_31.RadioValue()==this.w_doctra)
      this.oPgFrm.Page2.oPag.odoctra_2_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFatture_2_32.RadioValue()==this.w_Fatture)
      this.oPgFrm.Page2.oPag.oFatture_2_32.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oricfisc_2_33.RadioValue()==this.w_ricfisc)
      this.oPgFrm.Page2.oPag.oricfisc_2_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.onotecred_2_34.RadioValue()==this.w_notecred)
      this.oPgFrm.Page2.oPag.onotecred_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.odesccli_2_35.value==this.w_desccli)
      this.oPgFrm.Page2.oPag.odesccli_2_35.value=this.w_desccli
    endif
    if not(this.oPgFrm.Page2.oPag.oDESART_2_47.value==this.w_DESART)
      this.oPgFrm.Page2.oPag.oDESART_2_47.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page2.oPag.oGDESC_2_48.value==this.w_GDESC)
      this.oPgFrm.Page2.oPag.oGDESC_2_48.value=this.w_GDESC
    endif
    if not(this.oPgFrm.Page1.oPag.oSPEACC_1_47.RadioValue()==this.w_SPEACC)
      this.oPgFrm.Page1.oPag.oSPEACC_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_48.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_48.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oSIMVAL2_2_53.value==this.w_SIMVAL2)
      this.oPgFrm.Page2.oPag.oSIMVAL2_2_53.value=this.w_SIMVAL2
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_49.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_49.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oMADESCRI_2_54.value==this.w_MADESCRI)
      this.oPgFrm.Page2.oPag.oMADESCRI_2_54.value=this.w_MADESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAM_2_56.value==this.w_DESFAM)
      this.oPgFrm.Page2.oPag.oDESFAM_2_56.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESOMO_2_58.value==this.w_DESOMO)
      this.oPgFrm.Page2.oPag.oDESOMO_2_58.value=this.w_DESOMO
    endif
    if not(this.oPgFrm.Page2.oPag.oDESART1_2_60.value==this.w_DESART1)
      this.oPgFrm.Page2.oPag.oDESART1_2_60.value=this.w_DESART1
    endif
    if not(this.oPgFrm.Page1.oPag.oSTALOG_1_50.RadioValue()==this.w_STALOG)
      this.oPgFrm.Page1.oPag.oSTALOG_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ostato_1_52.RadioValue()==this.w_stato)
      this.oPgFrm.Page1.oPag.ostato_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODVET_2_62.value==this.w_CODVET)
      this.oPgFrm.Page2.oPag.oCODVET_2_62.value=this.w_CODVET
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVET_2_63.value==this.w_DESVET)
      this.oPgFrm.Page2.oPag.oDESVET_2_63.value=this.w_DESVET
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DADATA<=.w_ADATA OR EMPTY(.w_ADATA))  and (EMPTY(.w_DATA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDadata_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_Adata)) or not(.w_DADATA<=.w_ADATA OR EMPTY(.w_ADATA)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAdata_1_4.SetFocus()
            i_bnoObbl = !empty(.w_Adata)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   (empty(.w_DATSTA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTA_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DATSTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODESE))  and not(.w_Aggiorn='LIST' OR .w_Aggiorn='UCST')  and (.w_costmarg='S' AND .w_Aggiorn<>'LIST')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODESE_1_20.SetFocus()
            i_bnoObbl = !empty(.w_CODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CAMBIO<>0)  and not(.w_CAOVAL1<>0 or Empty(nvl(.w_VALUTA,' ')))  and (.w_CAOVAL1=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMBIO_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cambio di riferimento")
          case   not(.w_CAMBIO1<>0)  and not(.w_CAOVAL1<>0 OR .w_Aggiorn<>'LIST' OR Empty(nvl(.w_VALUTA,' ')) OR .w_Aggiorn='UCST')  and (.w_CAOVAL1=0 AND .w_costmarg='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMBIO1_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cambio di riferimento")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(empty(.w_cliente))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.ocliente_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice cliente � inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_zona))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.ozona_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_agente))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oagente_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice agente inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_pagam))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.opagam_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   not(((empty(.w_CODART1)) OR  (upper(.w_CODART)<=upper(.w_CODART1))) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODART_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((upper(.w_CODART1)>= upper(.w_CODART)) or (empty(.w_CODART1))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODART1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODART1_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   (empty(.w_valu2))  and (.w_stval='A')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.ovalu2_2_25.SetFocus()
            i_bnoObbl = !empty(.w_valu2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMBIO2))  and (.w_CAOVAL2=0 AND .w_VALU2<>' ')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCAMBIO2_2_27.SetFocus()
            i_bnoObbl = !empty(.w_CAMBIO2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cambio di riferimento")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATA = this.w_DATA
    this.o_costmarg = this.w_costmarg
    this.o_Aggiorn = this.w_Aggiorn
    this.o_FLPUNT = this.w_FLPUNT
    this.o_VALUTA = this.w_VALUTA
    this.o_Numero = this.w_Numero
    this.o_stval = this.w_stval
    this.o_valu2 = this.w_valu2
    return

enddefine

* --- Define pages as container
define class tgsma_sdvPag1 as StdContainer
  Width  = 586
  height = 398
  stdWidth  = 586
  stdheight = 398
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDadata_1_2 as StdField with uid="KSMMEHAABE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_Dadata", cQueryName = "Dadata",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data di inizio dei documenti di vendita da considerare",;
    HelpContextID = 71117366,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=64, Top=17

  func oDadata_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DATA))
    endwith
   endif
  endfunc

  func oDadata_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DADATA<=.w_ADATA OR EMPTY(.w_ADATA))
    endwith
    return bRes
  endfunc

  add object oAdata_1_4 as StdField with uid="TMLQZZAHGL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_Adata", cQueryName = "Adata",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data di termine dei documenti di vendita da considerare",;
    HelpContextID = 232784634,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=64, Top=47

  func oAdata_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DADATA<=.w_ADATA OR EMPTY(.w_ADATA))
    endwith
    return bRes
  endfunc

  add object oDATSTA_1_6 as StdField with uid="XKCTIADZZD",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di elaborazione, usata come riferimento per il listino",;
    HelpContextID = 36571702,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=504, Top=17

  add object ocostmarg_1_7 as StdCheck with uid="UZCGPCZDBF",rtseq=5,rtrep=.f.,left=13, top=77, caption="Costi e margine",;
    ToolTipText = "Se attivo riporta nella stampa i campi costo acquisto, margine lordo e %M.L.",;
    HelpContextID = 65088141,;
    cFormVar="w_costmarg", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func ocostmarg_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func ocostmarg_1_7.GetRadio()
    this.Parent.oContained.w_costmarg = this.RadioValue()
    return .t.
  endfunc

  func ocostmarg_1_7.SetRadio()
    this.Parent.oContained.w_costmarg=trim(this.Parent.oContained.w_costmarg)
    this.value = ;
      iif(this.Parent.oContained.w_costmarg=='S',1,;
      0)
  endfunc


  add object oAggiorn_1_11 as StdCombo with uid="SVYFDQOGHY",rtseq=6,rtrep=.f.,left=66,top=138,width=192,height=21;
    , ToolTipText = "Criterio di valorizzazione degli articoli di magazzino";
    , HelpContextID = 83189766;
    , cFormVar="w_Aggiorn",RowSource=""+"Costo medio ponderato annuo,"+"Costo medio ponderato periodico,"+"Costo ultimo,"+"Costo standard,"+"LIFO continuo,"+"FIFO continuo,"+"LIFO scatti,"+"Ultimo costo standard,"+"Listino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAggiorn_1_11.RadioValue()
    return(iif(this.value =1,'CMPA',;
    iif(this.value =2,'CMPP',;
    iif(this.value =3,'COUL',;
    iif(this.value =4,'COST',;
    iif(this.value =5,'LICO',;
    iif(this.value =6,'FICO',;
    iif(this.value =7,'LISC',;
    iif(this.value =8,'UCST',;
    iif(this.value =9,'LIST',;
    space(27)))))))))))
  endfunc
  func oAggiorn_1_11.GetRadio()
    this.Parent.oContained.w_Aggiorn = this.RadioValue()
    return .t.
  endfunc

  func oAggiorn_1_11.SetRadio()
    this.Parent.oContained.w_Aggiorn=trim(this.Parent.oContained.w_Aggiorn)
    this.value = ;
      iif(this.Parent.oContained.w_Aggiorn=='CMPA',1,;
      iif(this.Parent.oContained.w_Aggiorn=='CMPP',2,;
      iif(this.Parent.oContained.w_Aggiorn=='COUL',3,;
      iif(this.Parent.oContained.w_Aggiorn=='COST',4,;
      iif(this.Parent.oContained.w_Aggiorn=='LICO',5,;
      iif(this.Parent.oContained.w_Aggiorn=='FICO',6,;
      iif(this.Parent.oContained.w_Aggiorn=='LISC',7,;
      iif(this.Parent.oContained.w_Aggiorn=='UCST',8,;
      iif(this.Parent.oContained.w_Aggiorn=='LIST',9,;
      0)))))))))
  endfunc

  func oAggiorn_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_costmarg='S')
    endwith
   endif
  endfunc

  add object oFLPUNT_1_12 as StdCheck with uid="LBRMVGCFGH",rtseq=7,rtrep=.f.,left=419, top=141, caption="Elab. Fifo\Lifo puntuale",;
    ToolTipText = "Se attivo esegue elaborazione FIFO-LIFO puntuale",;
    HelpContextID = 80729430,;
    cFormVar="w_FLPUNT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLPUNT_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLPUNT_1_12.GetRadio()
    this.Parent.oContained.w_FLPUNT = this.RadioValue()
    return .t.
  endfunc

  func oFLPUNT_1_12.SetRadio()
    this.Parent.oContained.w_FLPUNT=trim(this.Parent.oContained.w_FLPUNT)
    this.value = ;
      iif(this.Parent.oContained.w_FLPUNT=='S',1,;
      0)
  endfunc

  func oFLPUNT_1_12.mHide()
    with this.Parent.oContained
      return (NOT(.w_AGGIORN $ 'FICO-LICO'))
    endwith
  endfunc

  add object oLISTINO_1_17 as StdField with uid="KINQNTIZDJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_LISTINO", cQueryName = "LISTINO",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino di riferimento",;
    HelpContextID = 243204790,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=66, Top=172, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LISTINO"

  func oLISTINO_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_costmarg='S')
    endwith
   endif
  endfunc

  func oLISTINO_1_17.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn<>'LIST' OR .w_Aggiorn='UCST')
    endwith
  endfunc

  func oLISTINO_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oLISTINO_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLISTINO_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLISTINO_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMAPDV.LISTINI_VZM',this.parent.oContained
  endproc
  proc oLISTINO_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_LISTINO
     i_obj.ecpSave()
  endproc

  add object oCODESE_1_20 as StdField with uid="TYVLWVSSGB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 101652518,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=66, Top=172, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_costmarg='S' AND .w_Aggiorn<>'LIST')
    endwith
   endif
  endfunc

  func oCODESE_1_20.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn='LIST' OR .w_Aggiorn='UCST')
    endwith
  endfunc

  func oCODESE_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
      if .not. empty(.w_Numero)
        bRes2=.link_1_23('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODESE_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oVALUTA_1_21 as StdField with uid="WYWPJCWGRG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta associata al listino",;
    HelpContextID = 36670294,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=199, Top=172, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_21.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn<>'LIST' OR .w_Aggiorn ='UCST')
    endwith
  endfunc

  func oVALUTA_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oNumero_1_23 as StdField with uid="TETFPVTIKO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_Numero", cQueryName = "Numero",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    sErrorMsg = "Numero inventario errato o mancante",;
    ToolTipText = "Numero dell'inventario da visualizzare",;
    HelpContextID = 35770070,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=199, Top=172, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="gsma_ain", oKey_1_1="INCODESE", oKey_1_2="this.w_CODESE", oKey_2_1="INNUMINV", oKey_2_2="this.w_Numero"

  func oNumero_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_costmarg='S' AND .w_Aggiorn<>'LIST' AND NOT EMPTY(.w_CODESE))
    endwith
   endif
  endfunc

  func oNumero_1_23.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn='LIST' OR .w_Aggiorn='UCST')
    endwith
  endfunc

  func oNumero_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oNumero_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNumero_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_CODESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_CODESE)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oNumero_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'gsma_ain',"Inventari",'',this.parent.oContained
  endproc
  proc oNumero_1_23.mZoomOnZoom
    local i_obj
    i_obj=gsma_ain()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_CODESE
     i_obj.w_INNUMINV=this.parent.oContained.w_Numero
     i_obj.ecpSave()
  endproc

  add object oLISTINO_1_26 as StdField with uid="XUOTALSCCK",rtseq=17,rtrep=.f.,;
    cFormVar = "w_LISTINO", cQueryName = "LISTINO",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Listino di valorizzazione dei servizi",;
    HelpContextID = 243204790,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=66, Top=254, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LISTINO"

  func oLISTINO_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_costmarg='S')
    endwith
   endif
  endfunc

  func oLISTINO_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oLISTINO_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLISTINO_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLISTINO_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMAPDV.LISTINI_VZM',this.parent.oContained
  endproc
  proc oLISTINO_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_LISTINO
     i_obj.ecpSave()
  endproc

  add object oVALUTA_1_28 as StdField with uid="SFXVRNVBHA",rtseq=19,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta associata al listino",;
    HelpContextID = 36670294,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=199, Top=254, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCAMBIO_1_30 as StdField with uid="EPKAJJECXB",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CAMBIO", cQueryName = "CAMBIO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cambio di riferimento",;
    ToolTipText = "Cambio della valuta selezionata",;
    HelpContextID = 258775590,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=372, Top=254

  func oCAMBIO_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL1=0)
    endwith
   endif
  endfunc

  func oCAMBIO_1_30.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL1<>0 or Empty(nvl(.w_VALUTA,' ')))
    endwith
  endfunc

  func oCAMBIO_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAMBIO<>0)
    endwith
    return bRes
  endfunc

  add object oCAMBIO1_1_37 as StdField with uid="SYXEXKZMQO",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CAMBIO1", cQueryName = "CAMBIO1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cambio di riferimento",;
    ToolTipText = "Cambio della valuta selezionata",;
    HelpContextID = 258775590,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=372, Top=172

  func oCAMBIO1_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL1=0 AND .w_costmarg='S')
    endwith
   endif
  endfunc

  func oCAMBIO1_1_37.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL1<>0 OR .w_Aggiorn<>'LIST' OR Empty(nvl(.w_VALUTA,' ')) OR .w_Aggiorn='UCST')
    endwith
  endfunc

  func oCAMBIO1_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAMBIO1<>0)
    endwith
    return bRes
  endfunc


  add object oBtn_1_44 as StdButton with uid="XAOFRYAIID",left=480, top=346, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 67702310;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      with this.Parent.oContained
        do GSVE_BPV with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_44.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ODES) AND (NOT EMPTY(.w_Contrnu) OR NOT EMPTY(.w_Contrli)) OR .w_Aggiorn='UCST')
      endwith
    endif
  endfunc


  add object oObj_1_45 as cp_outputCombo with uid="ZLERXIVPWT",left=110, top=304, width=470,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 103910374


  add object oBtn_1_46 as StdButton with uid="KYQESPJULE",left=530, top=346, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 66769850;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_46.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSPEACC_1_47 as StdCheck with uid="PDZVMVORTF",rtseq=65,rtrep=.f.,left=419, top=77, caption="Includi spese accessorie",;
    ToolTipText = "Se attivo include spese accessorie ripartite.",;
    HelpContextID = 51063334,;
    cFormVar="w_SPEACC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSPEACC_1_47.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSPEACC_1_47.GetRadio()
    this.Parent.oContained.w_SPEACC = this.RadioValue()
    return .t.
  endfunc

  func oSPEACC_1_47.SetRadio()
    this.Parent.oContained.w_SPEACC=trim(this.Parent.oContained.w_SPEACC)
    this.value = ;
      iif(this.Parent.oContained.w_SPEACC=='S',1,;
      0)
  endfunc

  add object oSIMVAL_1_48 as StdField with uid="ECTOAJKETF",rtseq=66,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 201368358,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=244, Top=254, InputMask=replicate('X',5)

  add object oSIMVAL_1_49 as StdField with uid="EEZHHDKHAS",rtseq=68,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 201368358,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=244, Top=172, InputMask=replicate('X',5)

  func oSIMVAL_1_49.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn<>'LIST' OR .w_Aggiorn ='UCST')
    endwith
  endfunc

  add object oSTALOG_1_50 as StdCheck with uid="OUGSGYQVAE",rtseq=73,rtrep=.f.,left=110, top=343, caption="Log N.C. slegate",;
    ToolTipText = "Se attivo stampa il log delle note di credito slegate dal ciclo documentale",;
    HelpContextID = 131460646,;
    cFormVar="w_STALOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTALOG_1_50.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSTALOG_1_50.GetRadio()
    this.Parent.oContained.w_STALOG = this.RadioValue()
    return .t.
  endfunc

  func oSTALOG_1_50.SetRadio()
    this.Parent.oContained.w_STALOG=trim(this.Parent.oContained.w_STALOG)
    this.value = ;
      iif(this.Parent.oContained.w_STALOG=='S',1,;
      0)
  endfunc


  add object ostato_1_52 as StdCombo with uid="RIGACPWOCZ",value=3,rtseq=74,rtrep=.f.,left=470,top=45,width=110,height=21;
    , ToolTipText = "Permette di selezionare tutti i documenti, solo quelli provvisori o confermati";
    , HelpContextID = 218099674;
    , cFormVar="w_stato",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func ostato_1_52.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(20)))))
  endfunc
  func ostato_1_52.GetRadio()
    this.Parent.oContained.w_stato = this.RadioValue()
    return .t.
  endfunc

  func ostato_1_52.SetRadio()
    this.Parent.oContained.w_stato=trim(this.Parent.oContained.w_stato)
    this.value = ;
      iif(this.Parent.oContained.w_stato=='N',1,;
      iif(this.Parent.oContained.w_stato=='S',2,;
      iif(this.Parent.oContained.w_stato=='',3,;
      0)))
  endfunc

  add object oStr_1_3 as StdString with uid="IOVTSCSLRF",Visible=.t., Left=0, Top=17,;
    Alignment=1, Width=62, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="ICFOIHRQGF",Visible=.t., Left=0, Top=47,;
    Alignment=1, Width=62, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="YHYTTKORAT",Visible=.t., Left=3, Top=173,;
    Alignment=1, Width=60, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn='LIST' OR .w_Aggiorn='UCST')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="MFIMKBSCEY",Visible=.t., Left=137, Top=173,;
    Alignment=1, Width=59, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn='LIST' OR .w_Aggiorn='UCST')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="HNGIIZNRAB",Visible=.t., Left=9, Top=110,;
    Alignment=0, Width=575, Height=15,;
    Caption="Valorizzazione costi articoli"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="DYGLJWSYXZ",Visible=.t., Left=9, Top=304,;
    Alignment=1, Width=99, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="ACIRBGYASN",Visible=.t., Left=9, Top=225,;
    Alignment=0, Width=569, Height=15,;
    Caption="Valorizzazione costi servizi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="KPFIKXAECL",Visible=.t., Left=137, Top=254,;
    Alignment=1, Width=59, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="KXXCYMYEKH",Visible=.t., Left=305, Top=254,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL1<>0 OR Empty(nvl(.w_VALUTA,' ')))
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="ZTTIZZOTZG",Visible=.t., Left=3, Top=138,;
    Alignment=1, Width=60, Height=15,;
    Caption="Valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="SCNSLAHCGY",Visible=.t., Left=0, Top=254,;
    Alignment=1, Width=64, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="YXOQTYNOIN",Visible=.t., Left=137, Top=173,;
    Alignment=1, Width=59, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn<>'LIST')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="KPNQOYPNED",Visible=.t., Left=303, Top=173,;
    Alignment=1, Width=67, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL1<>0 OR .w_Aggiorn<>'LIST' OR Empty(nvl(.w_VALUTA,' ')))
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="ZTUSXTFWWM",Visible=.t., Left=3, Top=173,;
    Alignment=1, Width=60, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_Aggiorn<>'LIST')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="AMTALZPSCH",Visible=.t., Left=370, Top=18,;
    Alignment=1, Width=132, Height=18,;
    Caption="Data di elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="GNNUZEUIYM",Visible=.t., Left=375, Top=45,;
    Alignment=1, Width=92, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oBox_1_39 as StdBox with uid="CONANCLOVL",left=7, top=128, width=572,height=1

  add object oBox_1_40 as StdBox with uid="SSXAOYKQYU",left=7, top=244, width=572,height=1
enddefine
define class tgsma_sdvPag2 as StdContainer
  Width  = 586
  height = 398
  stdWidth  = 586
  stdheight = 398
  resizeXpos=566
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object ocliente_2_1 as StdField with uid="DNZWFJCDJL",rtseq=25,rtrep=.f.,;
    cFormVar = "w_cliente", cQueryName = "cliente",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice cliente � inesistente oppure obsoleto",;
    ToolTipText = "Codice cliente selezionato",;
    HelpContextID = 152991962,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=121, Top=8, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_Client", oKey_2_1="ANCODICE", oKey_2_2="this.w_cliente"

  func ocliente_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc ocliente_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ocliente_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_Client)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_Client)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'ocliente_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'GSAR_KMS.CONTI_VZM',this.parent.oContained
  endproc
  proc ocliente_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_Client
     i_obj.w_ANCODICE=this.parent.oContained.w_cliente
     i_obj.ecpSave()
  endproc

  add object ocatcomm_2_3 as StdField with uid="GDBNRJKCKA",rtseq=26,rtrep=.f.,;
    cFormVar = "w_catcomm", cQueryName = "catcomm",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale selezionata",;
    HelpContextID = 267398182,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=121, Top=33, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_catcomm"

  func ocatcomm_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc ocatcomm_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ocatcomm_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'ocatcomm_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc ocatcomm_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_catcomm
     i_obj.ecpSave()
  endproc

  add object odesccat_2_4 as StdField with uid="JBPIMKEVPW",rtseq=27,rtrep=.f.,;
    cFormVar = "w_desccat", cQueryName = "desccat",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 53485622,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=33, InputMask=replicate('X',35)

  add object ozona_2_6 as StdField with uid="ZTIAWYAUWV",rtseq=28,rtrep=.f.,;
    cFormVar = "w_zona", cQueryName = "zona",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleto",;
    ToolTipText = "Zona selezionata",;
    HelpContextID = 67249258,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=121, Top=58, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_zona"

  func ozona_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc ozona_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ozona_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'ozona_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc ozona_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_zona
     i_obj.ecpSave()
  endproc

  add object odesczona_2_7 as StdField with uid="OHOKBHSDYQ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_desczona", cQueryName = "desczona",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 44048535,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=58, InputMask=replicate('X',35)

  add object oagente_2_9 as StdField with uid="DORWLHLUMI",rtseq=30,rtrep=.f.,;
    cFormVar = "w_agente", cQueryName = "agente",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente inesistente oppure obsoleto",;
    ToolTipText = "Codice agente selezionato",;
    HelpContextID = 139084294,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=121, Top=83, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_agente"

  func oagente_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oagente_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oagente_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oagente_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"",'',this.parent.oContained
  endproc
  proc oagente_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_agente
     i_obj.ecpSave()
  endproc

  add object odescage_2_10 as StdField with uid="RNXXVNOXPV",rtseq=31,rtrep=.f.,;
    cFormVar = "w_descage", cQueryName = "descage",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 116383690,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=83, InputMask=replicate('X',35)

  add object opagam_2_12 as StdField with uid="TGIYBOLWES",rtseq=32,rtrep=.f.,;
    cFormVar = "w_pagam", cQueryName = "pagam",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice pagamento selezionato",;
    HelpContextID = 221422346,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=121, Top=108, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_pagam"

  func opagam_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc opagam_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc opagam_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'opagam_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc opagam_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_pagam
     i_obj.ecpSave()
  endproc

  add object oCODART_2_13 as StdField with uid="GDOWHDUCOT",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice primo articolo selezionato",;
    HelpContextID = 83564582,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=121, Top=133, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli/servizi",'ARTZOOM1.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oCODART1_2_14 as StdField with uid="UERZZTFGMO",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CODART1", cQueryName = "CODART1",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Codice secondo articolo selezionato",;
    HelpContextID = 83564582,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=121, Top=158, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART1"

  func oCODART1_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART1_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART1_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART1_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"",'ARTZOOM1.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART1_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART1
     i_obj.ecpSave()
  endproc

  add object oGRUMER_2_15 as StdField with uid="GMEHYOKZIG",rtseq=35,rtrep=.f.,;
    cFormVar = "w_GRUMER", cQueryName = "GRUMER",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico selezionato",;
    HelpContextID = 37235558,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=183, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMER"

  func oGRUMER_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMER_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMER_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMER_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oGRUMER_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUMER
     i_obj.ecpSave()
  endproc

  add object oCODMAR_2_16 as StdField with uid="YSFMTDGTWH",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CODMAR", cQueryName = "CODMAR",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marchio selezionato",;
    HelpContextID = 32970790,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=208, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_CODMAR"

  func oCODMAR_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAR_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAR_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oCODMAR_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oCODMAR_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_CODMAR
     i_obj.ecpSave()
  endproc

  add object odescpaga_2_17 as StdField with uid="CIURVSUYUU",rtseq=37,rtrep=.f.,;
    cFormVar = "w_descpaga", cQueryName = "descpaga",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 201318249,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=108, InputMask=replicate('X',30)

  add object oCODFAM_2_18 as StdField with uid="TIMHSCSIDL",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Famiglia articolo selezionata",;
    HelpContextID = 217061414,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=233, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglie",'',this.parent.oContained
  endproc
  proc oCODFAM_2_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CODFAM
     i_obj.ecpSave()
  endproc

  add object oCODCAT_2_19 as StdField with uid="RIZVGNUOZH",rtseq=39,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria omogenea selezionata",;
    HelpContextID = 65869862,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=258, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAT_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCAT_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCODCAT_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CODCAT
     i_obj.ecpSave()
  endproc


  add object ostval_2_23 as StdCombo with uid="VIZQGRQXYY",rtseq=40,rtrep=.f.,left=121,top=330,width=113,height=21;
    , ToolTipText = "Valuta di valorizzazione dei totali sulla stampa";
    , HelpContextID = 222404570;
    , cFormVar="w_stval",RowSource=""+"Valuta di conto,"+"Altra valuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func ostval_2_23.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    space(10))))
  endfunc
  func ostval_2_23.GetRadio()
    this.Parent.oContained.w_stval = this.RadioValue()
    return .t.
  endfunc

  func ostval_2_23.SetRadio()
    this.Parent.oContained.w_stval=trim(this.Parent.oContained.w_stval)
    this.value = ;
      iif(this.Parent.oContained.w_stval=='C',1,;
      iif(this.Parent.oContained.w_stval=='A',2,;
      0))
  endfunc

  add object ovalu2_2_25 as StdField with uid="DWHGNCVMZI",rtseq=41,rtrep=.f.,;
    cFormVar = "w_valu2", cQueryName = "valu2",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 13521578,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=285, Top=330, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_valu2"

  func ovalu2_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_stval='A')
    endwith
   endif
  endfunc

  func ovalu2_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc ovalu2_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ovalu2_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'ovalu2_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc ovalu2_2_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_valu2
     i_obj.ecpSave()
  endproc

  add object oCAMBIO2_2_27 as StdField with uid="JFHQPTZNMO",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CAMBIO2", cQueryName = "CAMBIO2",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cambio di riferimento",;
    ToolTipText = "Cambio della valuta selezionata",;
    HelpContextID = 258775590,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=441, Top=330

  func oCAMBIO2_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL2=0 AND .w_VALU2<>' ')
    endwith
   endif
  endfunc

  add object odoctra_2_31 as StdCheck with uid="CWJIJWLNNE",rtseq=45,rtrep=.f.,left=32, top=381, caption="Documenti trasporto",;
    ToolTipText = "Se attivo seleziona nel report i documenti di trasporto",;
    HelpContextID = 70265398,;
    cFormVar="w_doctra", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func odoctra_2_31.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func odoctra_2_31.GetRadio()
    this.Parent.oContained.w_doctra = this.RadioValue()
    return .t.
  endfunc

  func odoctra_2_31.SetRadio()
    this.Parent.oContained.w_doctra=trim(this.Parent.oContained.w_doctra)
    this.value = ;
      iif(this.Parent.oContained.w_doctra=='S',1,;
      0)
  endfunc

  add object oFatture_2_32 as StdCheck with uid="IJEBOCKAGE",rtseq=46,rtrep=.f.,left=190, top=381, caption="Fatture",;
    ToolTipText = "Se attivo seleziona nel report le fatture",;
    HelpContextID = 178181546,;
    cFormVar="w_Fatture", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFatture_2_32.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFatture_2_32.GetRadio()
    this.Parent.oContained.w_Fatture = this.RadioValue()
    return .t.
  endfunc

  func oFatture_2_32.SetRadio()
    this.Parent.oContained.w_Fatture=trim(this.Parent.oContained.w_Fatture)
    this.value = ;
      iif(this.Parent.oContained.w_Fatture=='S',1,;
      0)
  endfunc

  add object oricfisc_2_33 as StdCheck with uid="CAFDRKYIKH",rtseq=47,rtrep=.f.,left=292, top=381, caption="Ricevute fiscali",;
    ToolTipText = "Se attivo seleziona nel report le ricevute fiscali",;
    HelpContextID = 93463830,;
    cFormVar="w_ricfisc", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oricfisc_2_33.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oricfisc_2_33.GetRadio()
    this.Parent.oContained.w_ricfisc = this.RadioValue()
    return .t.
  endfunc

  func oricfisc_2_33.SetRadio()
    this.Parent.oContained.w_ricfisc=trim(this.Parent.oContained.w_ricfisc)
    this.value = ;
      iif(this.Parent.oContained.w_ricfisc=='S',1,;
      0)
  endfunc

  add object onotecred_2_34 as StdCheck with uid="ZZJNWRIAOZ",rtseq=48,rtrep=.f.,left=442, top=381, caption="Note di credito",;
    ToolTipText = "Se attivo seleziona nel report le note di credito",;
    HelpContextID = 198034630,;
    cFormVar="w_notecred", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func onotecred_2_34.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func onotecred_2_34.GetRadio()
    this.Parent.oContained.w_notecred = this.RadioValue()
    return .t.
  endfunc

  func onotecred_2_34.SetRadio()
    this.Parent.oContained.w_notecred=trim(this.Parent.oContained.w_notecred)
    this.value = ;
      iif(this.Parent.oContained.w_notecred=='S',1,;
      0)
  endfunc

  add object odesccli_2_35 as StdField with uid="TWSXVINGFB",rtseq=49,rtrep=.f.,;
    cFormVar = "w_desccli", cQueryName = "desccli",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 30400458,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=8, InputMask=replicate('X',40)

  add object oDESART_2_47 as StdField with uid="XTEYJOVCSQ",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 83623478,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=133, InputMask=replicate('X',40)

  add object oGDESC_2_48 as StdField with uid="OXDFLLORMT",rtseq=62,rtrep=.f.,;
    cFormVar = "w_GDESC", cQueryName = "GDESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 266527386,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=183, InputMask=replicate('X',35)

  add object oSIMVAL2_2_53 as StdField with uid="YJEYKYZTGR",rtseq=67,rtrep=.f.,;
    cFormVar = "w_SIMVAL2", cQueryName = "SIMVAL2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 201368358,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=334, Top=330, InputMask=replicate('X',5)

  add object oMADESCRI_2_54 as StdField with uid="HDPEQWUXRH",rtseq=69,rtrep=.f.,;
    cFormVar = "w_MADESCRI", cQueryName = "MADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 68094735,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=208, InputMask=replicate('X',35)

  add object oDESFAM_2_56 as StdField with uid="DFPKWJWJKQ",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 217120310,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=233, InputMask=replicate('X',35)

  add object oDESOMO_2_58 as StdField with uid="UEZVFSZHFP",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DESOMO", cQueryName = "DESOMO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 263847478,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=258, InputMask=replicate('X',35)

  add object oDESART1_2_60 as StdField with uid="VKYLCMXUNT",rtseq=72,rtrep=.f.,;
    cFormVar = "w_DESART1", cQueryName = "DESART1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 83623478,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=158, InputMask=replicate('X',40)

  add object oCODVET_2_62 as StdField with uid="NAIWVLBYFP",rtseq=75,rtrep=.f.,;
    cFormVar = "w_CODVET", cQueryName = "CODVET",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Vettore",;
    HelpContextID = 71309350,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=283, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VETTORI", cZoomOnZoom="GSAR_AVT", oKey_1_1="VTCODVET", oKey_1_2="this.w_CODVET"

  func oCODVET_2_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_62('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVET_2_62.ecpDrop(oSource)
    this.Parent.oContained.link_2_62('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVET_2_62.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VETTORI','*','VTCODVET',cp_AbsName(this.parent,'oCODVET_2_62'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVT',"Vettori",'',this.parent.oContained
  endproc
  proc oCODVET_2_62.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VTCODVET=this.parent.oContained.w_CODVET
     i_obj.ecpSave()
  endproc

  add object oDESVET_2_63 as StdField with uid="LXCKKLGKIU",rtseq=76,rtrep=.f.,;
    cFormVar = "w_DESVET", cQueryName = "DESVET",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 71368246,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=285, Top=283, InputMask=replicate('X',35)

  add object oStr_2_2 as StdString with uid="KHFYRTFHAU",Visible=.t., Left=26, Top=33,;
    Alignment=1, Width=92, Height=15,;
    Caption="Cat. comm.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="RUOPKHGJCK",Visible=.t., Left=26, Top=58,;
    Alignment=1, Width=92, Height=15,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="FZGVIKQJWL",Visible=.t., Left=26, Top=83,;
    Alignment=1, Width=92, Height=15,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="ZWRRXYXROA",Visible=.t., Left=26, Top=108,;
    Alignment=1, Width=92, Height=15,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="SXOGKGKNEX",Visible=.t., Left=33, Top=305,;
    Alignment=0, Width=551, Height=15,;
    Caption="Valuta totali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_22 as StdString with uid="QKUPPYRXXA",Visible=.t., Left=25, Top=330,;
    Alignment=1, Width=92, Height=15,;
    Caption="Stampa in:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="KHAUGDCEMY",Visible=.t., Left=235, Top=330,;
    Alignment=1, Width=48, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="PZHPGKGVAV",Visible=.t., Left=334, Top=330,;
    Alignment=1, Width=104, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="LGMZUMOWCU",Visible=.t., Left=33, Top=357,;
    Alignment=0, Width=549, Height=15,;
    Caption="Categorie documenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_36 as StdString with uid="GLXRFLHNHN",Visible=.t., Left=26, Top=8,;
    Alignment=1, Width=92, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="SNFXLXSTPF",Visible=.t., Left=56, Top=133,;
    Alignment=1, Width=62, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="XYUGNJUKOO",Visible=.t., Left=63, Top=183,;
    Alignment=0, Width=60, Height=18,;
    Caption="Gru. Merc."  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="RIBDQGUFXR",Visible=.t., Left=72, Top=208,;
    Alignment=1, Width=46, Height=18,;
    Caption="Marchio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_57 as StdString with uid="KRTXIJIGFY",Visible=.t., Left=67, Top=233,;
    Alignment=1, Width=51, Height=18,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_59 as StdString with uid="HETPRRQGDS",Visible=.t., Left=8, Top=258,;
    Alignment=1, Width=110, Height=18,;
    Caption="Cat. omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_2_61 as StdString with uid="KATZJYBIII",Visible=.t., Left=65, Top=158,;
    Alignment=1, Width=53, Height=18,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_64 as StdString with uid="QJBVAGYSVX",Visible=.t., Left=28, Top=284,;
    Alignment=1, Width=90, Height=18,;
    Caption="Vettore:"  ;
  , bGlobalFont=.t.

  add object oBox_2_20 as StdBox with uid="ILBOQDWCSU",left=33, top=323, width=550,height=1

  add object oBox_2_45 as StdBox with uid="YVRWSVEZJN",left=33, top=373, width=551,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_sdv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
