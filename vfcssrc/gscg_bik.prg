* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bik                                                        *
*              Check prima nota reg IVA                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_17]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2016-04-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Ppar
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bik",oParentObject,m.w_Ppar)
return(i_retval)

define class tgscg_bik as StdBatch
  * --- Local variables
  w_Ppar = .f.
  w_OK = .f.
  w_NUMREG = 0
  w_DATREG = ctod("  /  /  ")
  w_CONTA = 0
  w_PERLIQ = 0
  w_ONUMREG = 0
  w_ODATREG = ctod("  /  /  ")
  w_ANNO = space(4)
  w_DATBLO = ctod("  /  /  ")
  w_TIPREG = space(1)
  w_COMIVA = ctod("  /  /  ")
  w_STALIG = ctod("  /  /  ")
  w_OTIPREG = space(1)
  w_OCOMIVA = ctod("  /  /  ")
  w_DATTEST = ctod("  /  /  ")
  w_MESS = space(4)
  w_PADRE = .NULL.
  * --- WorkFile variables
  PRI_MAST_idx=0
  ATTIDETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli modifiche castelletto registrazioni IVA (da GSCG_MIV)
    * --- controlla la data con quella dell'ultima stampa registri IVA
    * --- variabili locali
    this.w_OK = .T.
    this.w_MESS = "Transazione abbandonata"
    if this.w_Ppar
      * --- Controllo variazione su righe
      this.w_PADRE = this.oParentObject
      this.w_PADRE.MarkPos()     
      SELECT (this.oParentObject.cTrsName)
      GO TOP
      SCAN FOR i_SRV="U" AND NOT(NOT EMPTY(t_IVCODIVA) AND (t_IVIMPONI<>0 OR t_IVIMPIVA<>0 OR t_IVCFLOMA$"SZ"))
      this.w_OK = .F.
      EXIT
      ENDSCAN
      this.w_PADRE.RePos()     
      if Not this.w_OK
        * --- Abbandona 
        i_retval=.F.
      else
        i_retval=.T.
      endif
      i_retcode = 'stop'
      return
    endif
    this.w_PERLIQ = 13
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    this.w_STALIG = cp_CharToDate("  -  -  ")
    WITH this.oParentObject.oParentObject
    * --- Legge Dati dalla Primanota
    this.w_ANNO = ALLTRIM(STR(YEAR(.w_PNCOMIVA)))
    this.w_DATREG = .w_PNDATREG
    this.w_COMIVA = .w_PNCOMIVA
    if .cFunction="Load"
      this.w_ODATREG = this.w_DATREG
      this.w_OCOMIVA = this.w_COMIVA
    else
      this.w_ODATREG = .w_ODATREG
      this.w_OCOMIVA = .w_OCOMIVA
    endif
    ENDWITH
    * --- Testa la Data piu' elevata tra Data Reg. e Data Competenza IVA
    this.w_DATTEST = MAX(this.w_DATREG, this.w_ODATREG, this.w_COMIVA, this.w_OCOMIVA)
    this.w_ONUMREG = -1
    this.w_OTIPREG = "X"
    * --- Cicla sulle Righe IVA
    this.w_PADRE = this.oParentObject
    * --- Imposta la Deleted ON perche se lanciato dalla Primanota, l'ambiente e' settato DELETED OFF
    *     set deleted ripristinata al valore originario al tern�mine del Batch.
    *     Occorre per verificare eventuali righe IVA cancellate e stampate in
    *     definitiva.
    this.w_PADRE.MarkPos()     
     
 OLDSET = SET("DELETED") 
 SET DELETED OFF 
 Select ( this.w_PADRE.cTrsName) 
 Go Top
    SCAN FOR (t_IVCODIVA<>SPACE(5) AND t_CTIPREG $ "VACE" AND (t_IVIMPONI<>0 OR t_IVIMPIVA<>0 OR t_IVCFLOMA="S")) OR I_srv="U"
    this.w_TIPREG = t_CTIPREG
    this.w_NUMREG = t_IVNUMREG
    if ! this.w_PADRE.Fullrow()
      if empty(t_IVCODIVA)
        this.w_MESS = Ah_msgFormat("E' ancora presente nel database una riga con aliquota IVA eliminata; uscire dalla modifica e richiamare la registrazione utilizzando la funzione F6 per la cancellazione desiderata")
      else
        this.w_MESS = Ah_msgFormat("Controllare la riga IVA non valorizzata corrispondente all'aliquota %1",t_IVCODIVA)
      endif
      this.w_OK = .F.
    endif
    if this.w_TIPREG<>this.w_OTIPREG OR this.w_NUMREG<>this.w_ONUMREG
      this.w_CONTA = 0
      * --- Select from ATTIDETT
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ATDATSTA,ATDATBLO  from "+i_cTable+" ATTIDETT ";
            +" where ATNUMREG="+cp_ToStrODBC(this.w_NUMREG)+" AND ATTIPREG="+cp_ToStrODBC(this.w_TIPREG)+"";
             ,"_Curs_ATTIDETT")
      else
        select ATDATSTA,ATDATBLO from (i_cTable);
         where ATNUMREG=this.w_NUMREG AND ATTIPREG=this.w_TIPREG;
          into cursor _Curs_ATTIDETT
      endif
      if used('_Curs_ATTIDETT')
        select _Curs_ATTIDETT
        locate for 1=1
        do while not(eof())
        this.w_STALIG = CP_TODATE(_Curs_ATTIDETT.ATDATSTA)
        this.w_DATBLO = CP_TODATE(_Curs_ATTIDETT.ATDATBLO)
        this.w_CONTA = this.w_CONTA+1
          select _Curs_ATTIDETT
          continue
        enddo
        use
      endif
      if this.w_CONTA>1
        this.w_MESS = "Esistono pi�' attivit� associate allo stesso registro; verificare attivit�"
        this.w_OK = .F.
      endif
      if this.w_CONTA<1
        this.w_MESS = "Numero registro non associato a nessuna attivit�"
        this.w_OK = .F.
      endif
      if this.w_OK
        do case
          case this.w_DATTEST <= this.w_STALIG AND NOT EMPTY(this.w_STALIG) and (this.oParentObject.oParentObject.w_PNFLREGI<>"S" or g_APPLICATION <> "ad hoc ENTERPRISE")
            this.w_MESS = "Data registrazione inferiore o uguale a ultima stampa registri IVA"
            this.w_OK = .F.
            EXIT
          case this.w_DATTEST <= this.w_DATBLO AND NOT EMPTY(this.w_DATBLO)
            this.w_MESS = "Stampa registri IVA in corso con selezione comprendente la registrazione"
            this.w_OK = .F.
            EXIT
        endcase
      else
        EXIT
      endif
    endif
    this.w_ONUMREG = this.w_NUMREG
    this.w_OTIPREG = this.w_TIPREG
    ENDSCAN
    * --- set deleted ripristinata al valore originario al tern�mine del Batch
    SET DELETED &OLDSET
    this.w_PADRE.RePos(.t.)     
    if Not this.w_OK
      * --- Abbandona la Transazione
      this.w_MESS = Ah_MsgFormat(this.w_MESS)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  proc Init(oParentObject,w_Ppar)
    this.w_Ppar=w_Ppar
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PRI_MAST'
    this.cWorkTables[2]='ATTIDETT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Ppar"
endproc
