* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bam                                                        *
*              Controlli archivi monoazienda                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_27]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-07                                                      *
* Last revis.: 2018-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pQUANDO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bam",oParentObject,m.pQUANDO)
return(i_retval)

define class tgsar_bam as StdBatch
  * --- Local variables
  pQUANDO = space(10)
  w_AITIPE = space(1)
  w_AITIPV = space(1)
  w_CODAZI = space(5)
  w_INPECE = 0
  w_INPEAC = 0
  * --- WorkFile variables
  ESERCIZI_idx=0
  AZIENDA_idx=0
  cpazi_idx=0
  AZDATINT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato Dalle gestioni Monoazienda: GSAR_AZI, GSAR_ACO, GSAR_MES...
    do case
      case this.pQuando="modifica"
        this.w_CODAZI = i_CODAZI
        * --- Read from AZDATINT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZDATINT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZDATINT_idx,2],.t.,this.AZDATINT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ITAITIPE,ITAITIPV"+;
            " from "+i_cTable+" AZDATINT where ";
                +"ITCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ITAITIPE,ITAITIPV;
            from (i_cTable) where;
                ITCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_AITIPE = NVL(cp_ToDate(_read_.ITAITIPE),cp_NullValue(_read_.ITAITIPE))
          this.w_AITIPV = NVL(cp_ToDate(_read_.ITAITIPV),cp_NullValue(_read_.ITAITIPV))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZAITIPE ="+cp_NullLink(cp_ToStrODBC(this.w_AITIPE),'AZIENDA','AZAITIPE');
          +",AZAITIPV ="+cp_NullLink(cp_ToStrODBC(this.w_AITIPV),'AZIENDA','AZAITIPV');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZAITIPE = this.w_AITIPE;
              ,AZAITIPV = this.w_AITIPV;
              &i_ccchkf. ;
           where;
              AZCODAZI = this.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Aggiorna i dati di base a seguito di un aggiornamento delle variabili globali
        if g_APPLICATION = "ADHOC REVOLUTION"
          SETT_AMB(i_CODAZI, i_DATSYS, g_CODESE)
          ACTKEY(3)
        else
          SETT_AMB(i_CODAZI, i_DATSYS, g_CODESE, g_CODBUN)
          ACTKEY(0)
        endif
        AddModuleToPath()
        if UPPER(this.oparentobject.class)="TGSAR_KZI"
          * --- Aggiorno anche la descrizione della tabella di sistema...
          * --- Write into cpazi
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.cpazi_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpazi_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.cpazi_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"desazi ="+cp_NullLink(cp_ToStrODBC(Left( g_RAGAZI , 30 )),'cpazi','desazi');
                +i_ccchkf ;
            +" where ";
                +"codazi = "+cp_ToStrODBC(this.w_CODAZI);
                   )
          else
            update (i_cTable) set;
                desazi = Left( g_RAGAZI , 30 );
                &i_ccchkf. ;
             where;
                codazi = this.w_CODAZI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pQuando="LoaDel"
        ah_ErrorMsg("Operazione non consentita",,"")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=MSG_TRANSACTION_ERROR
      case this.pQuando="FlagMess"
        ah_ErrorMsg("Attenzione: la modifica potrebbe avere influenza su tutti i documenti gi� caricati",,"")
        if this.oParentObject.w_AZFLSCOM="S"
          ah_ErrorMsg("Attenzione: prima di procedere alla contabilizzazione dei documenti o alla generazione fatture differite verificare la congruenza dei calcoli con le nuove impostazioni",,"")
        endif
      case this.pQuando="DataMess"
        ah_ErrorMsg("Attenzione: prima di procedere alla contabilizzazione dei documenti o alla generazione fatture differite verificare la congruenza dei calcoli con le nuove impostazioni",,"")
      case this.pQuando="RigaMess"
        ah_ErrorMsg("Attenzione: il nuovo ordine di calcolo del valore fiscale,%0potrebbe avere influenza sui documenti gi� caricati nel caso in cui si tenti di modificarli",,"")
      case this.pQuando="Info"
        ah_ErrorMsg("Attenzione: se il calcolo delle provvigioni � di tipo differito, la generazione delle stesse%0pu� modificare le percentuali sui dati di riga dei documenti",,"")
    endcase
  endproc


  proc Init(oParentObject,pQUANDO)
    this.pQUANDO=pQUANDO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='cpazi'
    this.cWorkTables[4]='AZDATINT'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pQUANDO"
endproc
