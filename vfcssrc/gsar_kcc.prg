* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kcc                                                        *
*              Conferma clienti                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-23                                                      *
* Last revis.: 2018-07-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kcc",oParentObject))

* --- Class definition
define class tgsar_kcc as StdForm
  Top    = 11
  Left   = 38

  * --- Standard Properties
  Width  = 626
  Height = 429
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-26"
  HelpContextID=99186537
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=76

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  OFF_NOMI_IDX = 0
  MASTRI_IDX = 0
  CACOCLFO_IDX = 0
  LINGUE_IDX = 0
  VOCIIVA_IDX = 0
  PAG_AMEN_IDX = 0
  TRI_BUTI_IDX = 0
  NAZIONI_IDX = 0
  TIP_CLIE_IDX = 0
  NUMAUT_M_IDX = 0
  cPrg = "gsar_kcc"
  cComment = "Conferma clienti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_NewTipCon = space(1)
  w_ACTIVE = space(1)
  w_CCCODICE = space(15)
  w_CCDESCRI = space(40)
  w_ANCODICE = space(15)
  w_CCNAZION = space(3)
  w_CCPARIVA = space(12)
  w_CCMASCON = space(15)
  w_CCCODFIS = space(16)
  w_CCMASCON = space(15)
  w_CCDESMAS = space(40)
  w_CCCATCON = space(5)
  w_CCCODLIN = space(3)
  w_CCCODIVA = space(5)
  w_CCCODIVA = space(5)
  w_CCCODPAG = space(5)
  w_CCFLPRIV = space(1)
  w_CCFLESIG = space(10)
  o_CCFLESIG = space(10)
  w_CCFLRITE = space(1)
  o_CCFLRITE = space(1)
  w_CCCODIRP = space(5)
  o_CCCODIRP = space(5)
  w_CCFLESIG = space(1)
  w_CCIVASOS = space(10)
  w_TIPCLIE = space(5)
  w_CCDESCR2 = space(40)
  w_CCINDIRI = space(35)
  w_CCINDIR2 = space(35)
  w_CC___CAP = space(8)
  w_CCLOCALI = space(30)
  w_CCPROVIN = space(2)
  w_CCTELEFO = space(18)
  w_CCTELFAX = space(16)
  w_CCINDWEB = space(50)
  w_CC_EMAIL = space(50)
  w_CC_EMPEC = space(50)
  w_CCDTINVA = ctod('  /  /  ')
  w_CCDTOBSO = ctod('  /  /  ')
  w_CCCODZON = space(3)
  w_CCCODLIN = space(3)
  w_CCCODVAL = space(3)
  w_CCCODAGE = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CCDESIVA = space(35)
  w_CCDESLIN = space(30)
  w_CCDESCCN = space(35)
  w_CHKERR = .F.
  w_PERIVA = 0
  w_MCTIPMAS = space(1)
  w_CCCODSAL = space(5)
  w_CCDESPAG = space(30)
  w_FLIRPE = space(1)
  w_CAUPRE2 = space(1)
  w_DESIRPEF = space(60)
  w_CCCAURIT = space(1)
  w_NONUMLIS = space(5)
  w_NOCODBAN = space(10)
  w_NOCODBA2 = space(10)
  w_NOCATCOM = space(3)
  w_NOTIFICA = space(1)
  w_DESTIP = space(50)
  w_CCCODPAG = space(5)
  w_CCNUMCEL = space(18)
  w_CC_SKYPE = space(50)
  w_CCSOGGET = space(2)
  w_CCCOGNOM = space(50)
  w_CC__NOME = space(50)
  w_CCLOCNAS = space(30)
  w_CCPRONAS = space(2)
  w_CCDATNAS = ctod('  /  /  ')
  w_CCNUMCAR = space(18)
  w_CCCHKSTA = space(1)
  w_CCCHKMAI = space(1)
  w_CCCHKPEC = space(1)
  w_CCCHKFAX = space(1)
  w_PERFIS = space(1)
  w_CCNOINDI_2 = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kccPag1","gsar_kcc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANCODICE_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsar_kcc
    if (VARTYPE(g_SCHEDULER)='C' AND g_SCHEDULER='S')
       this.parent.left=_screen.width+1
    endif
    
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='MASTRI'
    this.cWorkTables[4]='CACOCLFO'
    this.cWorkTables[5]='LINGUE'
    this.cWorkTables[6]='VOCIIVA'
    this.cWorkTables[7]='PAG_AMEN'
    this.cWorkTables[8]='TRI_BUTI'
    this.cWorkTables[9]='NAZIONI'
    this.cWorkTables[10]='TIP_CLIE'
    this.cWorkTables[11]='NUMAUT_M'
    return(this.OpenAllTables(11))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NewTipCon=space(1)
      .w_ACTIVE=space(1)
      .w_CCCODICE=space(15)
      .w_CCDESCRI=space(40)
      .w_ANCODICE=space(15)
      .w_CCNAZION=space(3)
      .w_CCPARIVA=space(12)
      .w_CCMASCON=space(15)
      .w_CCCODFIS=space(16)
      .w_CCMASCON=space(15)
      .w_CCDESMAS=space(40)
      .w_CCCATCON=space(5)
      .w_CCCODLIN=space(3)
      .w_CCCODIVA=space(5)
      .w_CCCODIVA=space(5)
      .w_CCCODPAG=space(5)
      .w_CCFLPRIV=space(1)
      .w_CCFLESIG=space(10)
      .w_CCFLRITE=space(1)
      .w_CCCODIRP=space(5)
      .w_CCFLESIG=space(1)
      .w_CCIVASOS=space(10)
      .w_TIPCLIE=space(5)
      .w_CCDESCR2=space(40)
      .w_CCINDIRI=space(35)
      .w_CCINDIR2=space(35)
      .w_CC___CAP=space(8)
      .w_CCLOCALI=space(30)
      .w_CCPROVIN=space(2)
      .w_CCTELEFO=space(18)
      .w_CCTELFAX=space(16)
      .w_CCINDWEB=space(50)
      .w_CC_EMAIL=space(50)
      .w_CC_EMPEC=space(50)
      .w_CCDTINVA=ctod("  /  /  ")
      .w_CCDTOBSO=ctod("  /  /  ")
      .w_CCCODZON=space(3)
      .w_CCCODLIN=space(3)
      .w_CCCODVAL=space(3)
      .w_CCCODAGE=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CCDESIVA=space(35)
      .w_CCDESLIN=space(30)
      .w_CCDESCCN=space(35)
      .w_CHKERR=.f.
      .w_PERIVA=0
      .w_MCTIPMAS=space(1)
      .w_CCCODSAL=space(5)
      .w_CCDESPAG=space(30)
      .w_FLIRPE=space(1)
      .w_CAUPRE2=space(1)
      .w_DESIRPEF=space(60)
      .w_CCCAURIT=space(1)
      .w_NONUMLIS=space(5)
      .w_NOCODBAN=space(10)
      .w_NOCODBA2=space(10)
      .w_NOCATCOM=space(3)
      .w_NOTIFICA=space(1)
      .w_DESTIP=space(50)
      .w_CCCODPAG=space(5)
      .w_CCNUMCEL=space(18)
      .w_CC_SKYPE=space(50)
      .w_CCSOGGET=space(2)
      .w_CCCOGNOM=space(50)
      .w_CC__NOME=space(50)
      .w_CCLOCNAS=space(30)
      .w_CCPRONAS=space(2)
      .w_CCDATNAS=ctod("  /  /  ")
      .w_CCNUMCAR=space(18)
      .w_CCCHKSTA=space(1)
      .w_CCCHKMAI=space(1)
      .w_CCCHKPEC=space(1)
      .w_CCCHKFAX=space(1)
      .w_PERFIS=space(1)
      .w_CCNOINDI_2=space(35)
      .w_NewTipCon=oParentObject.w_NewTipCon
      .w_ACTIVE=oParentObject.w_ACTIVE
      .w_CCCODICE=oParentObject.w_CCCODICE
      .w_CCDESCRI=oParentObject.w_CCDESCRI
      .w_ANCODICE=oParentObject.w_ANCODICE
      .w_CCNAZION=oParentObject.w_CCNAZION
      .w_CCPARIVA=oParentObject.w_CCPARIVA
      .w_CCMASCON=oParentObject.w_CCMASCON
      .w_CCCODFIS=oParentObject.w_CCCODFIS
      .w_CCMASCON=oParentObject.w_CCMASCON
      .w_CCCATCON=oParentObject.w_CCCATCON
      .w_CCCODLIN=oParentObject.w_CCCODLIN
      .w_CCCODPAG=oParentObject.w_CCCODPAG
      .w_CCFLPRIV=oParentObject.w_CCFLPRIV
      .w_CCFLESIG=oParentObject.w_CCFLESIG
      .w_CCFLRITE=oParentObject.w_CCFLRITE
      .w_CCFLESIG=oParentObject.w_CCFLESIG
      .w_CCIVASOS=oParentObject.w_CCIVASOS
      .w_CCDESCR2=oParentObject.w_CCDESCR2
      .w_CCINDIRI=oParentObject.w_CCINDIRI
      .w_CCINDIR2=oParentObject.w_CCINDIR2
      .w_CC___CAP=oParentObject.w_CC___CAP
      .w_CCLOCALI=oParentObject.w_CCLOCALI
      .w_CCPROVIN=oParentObject.w_CCPROVIN
      .w_CCTELEFO=oParentObject.w_CCTELEFO
      .w_CCTELFAX=oParentObject.w_CCTELFAX
      .w_CCINDWEB=oParentObject.w_CCINDWEB
      .w_CC_EMAIL=oParentObject.w_CC_EMAIL
      .w_CC_EMPEC=oParentObject.w_CC_EMPEC
      .w_CCDTINVA=oParentObject.w_CCDTINVA
      .w_CCDTOBSO=oParentObject.w_CCDTOBSO
      .w_CCCODZON=oParentObject.w_CCCODZON
      .w_CCCODLIN=oParentObject.w_CCCODLIN
      .w_CCCODVAL=oParentObject.w_CCCODVAL
      .w_CCCODAGE=oParentObject.w_CCCODAGE
      .w_CCCODSAL=oParentObject.w_CCCODSAL
      .w_NONUMLIS=oParentObject.w_NONUMLIS
      .w_NOCODBAN=oParentObject.w_NOCODBAN
      .w_NOCODBA2=oParentObject.w_NOCODBA2
      .w_NOCATCOM=oParentObject.w_NOCATCOM
      .w_NOTIFICA=oParentObject.w_NOTIFICA
      .w_CCCODPAG=oParentObject.w_CCCODPAG
      .w_CCNUMCEL=oParentObject.w_CCNUMCEL
      .w_CC_SKYPE=oParentObject.w_CC_SKYPE
      .w_CCSOGGET=oParentObject.w_CCSOGGET
      .w_CCCOGNOM=oParentObject.w_CCCOGNOM
      .w_CC__NOME=oParentObject.w_CC__NOME
      .w_CCLOCNAS=oParentObject.w_CCLOCNAS
      .w_CCPRONAS=oParentObject.w_CCPRONAS
      .w_CCDATNAS=oParentObject.w_CCDATNAS
      .w_CCNUMCAR=oParentObject.w_CCNUMCAR
      .w_CCCHKSTA=oParentObject.w_CCCHKSTA
      .w_CCCHKMAI=oParentObject.w_CCCHKMAI
      .w_CCCHKPEC=oParentObject.w_CCCHKPEC
      .w_CCCHKFAX=oParentObject.w_CCCHKFAX
      .w_PERFIS=oParentObject.w_PERFIS
      .w_CCNOINDI_2=oParentObject.w_CCNOINDI_2
        .DoRTCalc(1,6,.f.)
        if not(empty(.w_CCNAZION))
          .link_1_9('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_CCMASCON))
          .link_1_11('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_CCMASCON))
          .link_1_13('Full')
        endif
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_CCCATCON))
          .link_1_16('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CCCODLIN))
          .link_1_18('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CCCODIVA))
          .link_1_20('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CCCODIVA))
          .link_1_21('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CCCODPAG))
          .link_1_22('Full')
        endif
          .DoRTCalc(17,19,.f.)
        .w_CCCODIRP = IIF(.w_CCFLRITE='S', READALTE('I'), space(5))
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CCCODIRP))
          .link_1_26('Full')
        endif
        .DoRTCalc(21,23,.f.)
        if not(empty(.w_TIPCLIE))
          .link_1_29('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
          .DoRTCalc(24,40,.f.)
        .w_OBTEST = i_DATSYS
          .DoRTCalc(42,45,.f.)
        .w_CHKERR = .F.
          .DoRTCalc(47,53,.f.)
        .w_CCCAURIT = .w_CAUPRE2
    endwith
    this.DoRTCalc(55,76,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_68.enabled = this.oPgFrm.Page1.oPag.oBtn_1_68.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_NewTipCon=.w_NewTipCon
      .oParentObject.w_ACTIVE=.w_ACTIVE
      .oParentObject.w_CCCODICE=.w_CCCODICE
      .oParentObject.w_CCDESCRI=.w_CCDESCRI
      .oParentObject.w_ANCODICE=.w_ANCODICE
      .oParentObject.w_CCNAZION=.w_CCNAZION
      .oParentObject.w_CCPARIVA=.w_CCPARIVA
      .oParentObject.w_CCMASCON=.w_CCMASCON
      .oParentObject.w_CCCODFIS=.w_CCCODFIS
      .oParentObject.w_CCMASCON=.w_CCMASCON
      .oParentObject.w_CCCATCON=.w_CCCATCON
      .oParentObject.w_CCCODLIN=.w_CCCODLIN
      .oParentObject.w_CCCODPAG=.w_CCCODPAG
      .oParentObject.w_CCFLPRIV=.w_CCFLPRIV
      .oParentObject.w_CCFLESIG=.w_CCFLESIG
      .oParentObject.w_CCFLRITE=.w_CCFLRITE
      .oParentObject.w_CCFLESIG=.w_CCFLESIG
      .oParentObject.w_CCIVASOS=.w_CCIVASOS
      .oParentObject.w_CCDESCR2=.w_CCDESCR2
      .oParentObject.w_CCINDIRI=.w_CCINDIRI
      .oParentObject.w_CCINDIR2=.w_CCINDIR2
      .oParentObject.w_CC___CAP=.w_CC___CAP
      .oParentObject.w_CCLOCALI=.w_CCLOCALI
      .oParentObject.w_CCPROVIN=.w_CCPROVIN
      .oParentObject.w_CCTELEFO=.w_CCTELEFO
      .oParentObject.w_CCTELFAX=.w_CCTELFAX
      .oParentObject.w_CCINDWEB=.w_CCINDWEB
      .oParentObject.w_CC_EMAIL=.w_CC_EMAIL
      .oParentObject.w_CC_EMPEC=.w_CC_EMPEC
      .oParentObject.w_CCDTINVA=.w_CCDTINVA
      .oParentObject.w_CCDTOBSO=.w_CCDTOBSO
      .oParentObject.w_CCCODZON=.w_CCCODZON
      .oParentObject.w_CCCODLIN=.w_CCCODLIN
      .oParentObject.w_CCCODVAL=.w_CCCODVAL
      .oParentObject.w_CCCODAGE=.w_CCCODAGE
      .oParentObject.w_CCCODSAL=.w_CCCODSAL
      .oParentObject.w_NONUMLIS=.w_NONUMLIS
      .oParentObject.w_NOCODBAN=.w_NOCODBAN
      .oParentObject.w_NOCODBA2=.w_NOCODBA2
      .oParentObject.w_NOCATCOM=.w_NOCATCOM
      .oParentObject.w_NOTIFICA=.w_NOTIFICA
      .oParentObject.w_CCCODPAG=.w_CCCODPAG
      .oParentObject.w_CCNUMCEL=.w_CCNUMCEL
      .oParentObject.w_CC_SKYPE=.w_CC_SKYPE
      .oParentObject.w_CCSOGGET=.w_CCSOGGET
      .oParentObject.w_CCCOGNOM=.w_CCCOGNOM
      .oParentObject.w_CC__NOME=.w_CC__NOME
      .oParentObject.w_CCLOCNAS=.w_CCLOCNAS
      .oParentObject.w_CCPRONAS=.w_CCPRONAS
      .oParentObject.w_CCDATNAS=.w_CCDATNAS
      .oParentObject.w_CCNUMCAR=.w_CCNUMCAR
      .oParentObject.w_CCCHKSTA=.w_CCCHKSTA
      .oParentObject.w_CCCHKMAI=.w_CCCHKMAI
      .oParentObject.w_CCCHKPEC=.w_CCCHKPEC
      .oParentObject.w_CCCHKFAX=.w_CCCHKFAX
      .oParentObject.w_PERFIS=.w_PERFIS
      .oParentObject.w_CCNOINDI_2=.w_CCNOINDI_2
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,19,.t.)
        if .o_CCFLRITE<>.w_CCFLRITE
            .w_CCCODIRP = IIF(.w_CCFLRITE='S', READALTE('I'), space(5))
          .link_1_26('Full')
        endif
        .DoRTCalc(21,21,.t.)
        if .o_CCFLESIG<>.w_CCFLESIG
            .w_CCIVASOS = 'N'
        endif
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .DoRTCalc(23,40,.t.)
            .w_OBTEST = i_DATSYS
        .DoRTCalc(42,53,.t.)
        if .o_CCCODIRP<>.w_CCCODIRP
            .w_CCCAURIT = .w_CAUPRE2
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(55,76,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
    endwith
  return

  proc Calculate_IFMQGKRXAP()
    with this
          * --- Apertura clienti
     if .w_ACTIVE='S'
          GSAR_BO3(this;
              ,'B';
             )
     endif
    endwith
  endproc
  proc Calculate_TKGKPESWZF()
    with this
          * --- Cambia caption form
          .Caption = IIF(.w_NewTipCon='F', AH_MSGFORMAT("Conferma fornitori"), .caption)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oANCODICE_1_7.enabled = this.oPgFrm.Page1.oPag.oANCODICE_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCCMASCON_1_11.enabled = this.oPgFrm.Page1.oPag.oCCMASCON_1_11.mCond()
    this.oPgFrm.Page1.oPag.oCCMASCON_1_13.enabled = this.oPgFrm.Page1.oPag.oCCMASCON_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCCCODIVA_1_20.enabled = this.oPgFrm.Page1.oPag.oCCCODIVA_1_20.mCond()
    this.oPgFrm.Page1.oPag.oCCCODIVA_1_21.enabled = this.oPgFrm.Page1.oPag.oCCCODIVA_1_21.mCond()
    this.oPgFrm.Page1.oPag.oCCCODIRP_1_26.enabled = this.oPgFrm.Page1.oPag.oCCCODIRP_1_26.mCond()
    this.oPgFrm.Page1.oPag.oCCIVASOS_1_28.enabled = this.oPgFrm.Page1.oPag.oCCIVASOS_1_28.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oCCMASCON_1_11.visible=!this.oPgFrm.Page1.oPag.oCCMASCON_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCCMASCON_1_13.visible=!this.oPgFrm.Page1.oPag.oCCMASCON_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oCCCODIVA_1_20.visible=!this.oPgFrm.Page1.oPag.oCCCODIVA_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCCCODIVA_1_21.visible=!this.oPgFrm.Page1.oPag.oCCCODIVA_1_21.mHide()
    this.oPgFrm.Page1.oPag.oCCFLPRIV_1_23.visible=!this.oPgFrm.Page1.oPag.oCCFLPRIV_1_23.mHide()
    this.oPgFrm.Page1.oPag.oCCFLESIG_1_24.visible=!this.oPgFrm.Page1.oPag.oCCFLESIG_1_24.mHide()
    this.oPgFrm.Page1.oPag.oCCFLRITE_1_25.visible=!this.oPgFrm.Page1.oPag.oCCFLRITE_1_25.mHide()
    this.oPgFrm.Page1.oPag.oCCCODIRP_1_26.visible=!this.oPgFrm.Page1.oPag.oCCCODIRP_1_26.mHide()
    this.oPgFrm.Page1.oPag.oCCFLESIG_1_27.visible=!this.oPgFrm.Page1.oPag.oCCFLESIG_1_27.mHide()
    this.oPgFrm.Page1.oPag.oTIPCLIE_1_29.visible=!this.oPgFrm.Page1.oPag.oTIPCLIE_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oDESIRPEF_1_65.visible=!this.oPgFrm.Page1.oPag.oDESIRPEF_1_65.mHide()
    this.oPgFrm.Page1.oPag.oDESTIP_1_76.visible=!this.oPgFrm.Page1.oPag.oDESTIP_1_76.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_77.visible=!this.oPgFrm.Page1.oPag.oStr_1_77.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_78.visible=!this.oPgFrm.Page1.oPag.oStr_1_78.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_96.visible=!this.oPgFrm.Page1.oPag.oStr_1_96.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
        if lower(cEvent)==lower("w_CCNAZION GotFocus")
          .Calculate_IFMQGKRXAP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_TKGKPESWZF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CCNAZION
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_CCNAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_CCNAZION))
          select NACODNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCNAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCNAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oCCNAZION_1_9'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_CCNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_CCNAZION)
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCNAZION = NVL(_Link_.NACODNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CCNAZION = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCMASCON
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCMASCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CCMASCON)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CCMASCON))
          select MCCODICE,MCDESCRI,MCTIPMAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCMASCON)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_CCMASCON)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_CCMASCON)+"%");

            select MCCODICE,MCDESCRI,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CCMASCON) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCCMASCON_1_11'),i_cWhere,'GSAR_AMC',"Mastri contabili ("+iif((type('this.w_NewTipCon')='C' and this.w_NewTipCon='C') OR (type('this.parent.ocontained.w_NewTipCon')='C' and this.parent.ocontained.w_NewTipCon='C'),'clienti','fornitori')+')'+"",''+iif((type("this.w_NewTipCon")="C" and this.w_NewTipCon="C") OR (type("this.parent.ocontained.w_NewTipCon")="C" and this.parent.ocontained.w_NewTipCon="C"),"GSAR_ACL","GSAR_AFR")+'.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCMASCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CCMASCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CCMASCON)
            select MCCODICE,MCDESCRI,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCMASCON = NVL(_Link_.MCCODICE,space(15))
      this.w_CCDESMAS = NVL(_Link_.MCDESCRI,space(40))
      this.w_MCTIPMAS = NVL(_Link_.MCTIPMAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CCMASCON = space(15)
      endif
      this.w_CCDESMAS = space(40)
      this.w_MCTIPMAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MCTIPMAS=.w_NewTipCon
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CCMASCON = space(15)
        this.w_CCDESMAS = space(40)
        this.w_MCTIPMAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCMASCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCMASCON
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCMASCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CCMASCON)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CCMASCON))
          select MCCODICE,MCDESCRI,MCTIPMAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCMASCON)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_CCMASCON)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_CCMASCON)+"%");

            select MCCODICE,MCDESCRI,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CCMASCON) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCCMASCON_1_13'),i_cWhere,'GSAR_AMC',"Mastri contabili (clienti)",'GSAR2AMC.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCMASCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CCMASCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CCMASCON)
            select MCCODICE,MCDESCRI,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCMASCON = NVL(_Link_.MCCODICE,space(15))
      this.w_CCDESMAS = NVL(_Link_.MCDESCRI,space(40))
      this.w_MCTIPMAS = NVL(_Link_.MCTIPMAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CCMASCON = space(15)
      endif
      this.w_CCDESMAS = space(40)
      this.w_MCTIPMAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MCTIPMAS=.w_NewTipCon
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CCMASCON = space(15)
        this.w_CCDESMAS = space(40)
        this.w_MCTIPMAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCMASCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCCATCON
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CCCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CCCATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCCCATCON_1_16'),i_cWhere,'GSAR_AC2',"Categorie contabili clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CCCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CCCATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_CCDESCCN = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CCCATCON = space(5)
      endif
      this.w_CCDESCCN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCCODLIN
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_CCCODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_CCCODLIN))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oCCCODLIN_1_18'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_CCCODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_CCCODLIN)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODLIN = NVL(_Link_.LUCODICE,space(3))
      this.w_CCDESLIN = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODLIN = space(3)
      endif
      this.w_CCDESLIN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCCODIVA
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_CCCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_CCCODIVA))
          select IVCODIVA,IVDESIVA,IVPERIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_CCCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_CCCODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CCCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCCCODIVA_1_20'),i_cWhere,'GSAR_AIV',"Codici IVA",'GSCG0ADI.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CCCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CCCODIVA)
            select IVCODIVA,IVDESIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_CCDESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_CCCODIVA = space(5)
      endif
      this.w_CCDESIVA = space(35)
      this.w_PERIVA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PERIVA=0
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA inesistente, imponibile oppure obsoleto")
        endif
        this.w_CCCODIVA = space(5)
        this.w_CCDESIVA = space(35)
        this.w_PERIVA = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCCODIVA
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_CCCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_CCCODIVA))
          select IVCODIVA,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_CCCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_CCCODIVA)+"%");

            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CCCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCCCODIVA_1_21'),i_cWhere,'GSAR_AIV',"Codici IVA",'GSVE0MDV.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CCCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CCCODIVA)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_CCDESIVA = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODIVA = space(5)
      endif
      this.w_CCDESIVA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCCODPAG
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_CCCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_CCCODPAG))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_CCCODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_CCCODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_CCCODPAG)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CCCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oCCCODPAG_1_22'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_CCCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_CCCODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_CCDESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODPAG = space(5)
      endif
      this.w_CCDESPAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_CCCODPAG = space(5)
        this.w_CCDESPAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCCODIRP
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODIRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_CCCODIRP)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_CCCODIRP))
          select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODIRP)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCODIRP) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oCCCODIRP_1_26'),i_cWhere,'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODIRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_CCCODIRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_CCCODIRP)
            select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODIRP = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESIRPEF = NVL(_Link_.TRDESTRI,space(60))
      this.w_FLIRPE = NVL(_Link_.TRFLACON,space(1))
      this.w_CAUPRE2 = NVL(_Link_.TRCAUPRE2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODIRP = space(5)
      endif
      this.w_DESIRPEF = space(60)
      this.w_FLIRPE = space(1)
      this.w_CAUPRE2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLIRPE='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
        endif
        this.w_CCCODIRP = space(5)
        this.w_DESIRPEF = space(60)
        this.w_FLIRPE = space(1)
        this.w_CAUPRE2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODIRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCLIE
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CLIE_IDX,3]
    i_lTable = "TIP_CLIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CLIE_IDX,2], .t., this.TIP_CLIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CLIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCLIE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ATC',True,'TIP_CLIE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_TIPCLIE)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_TIPCLIE))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPCLIE)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPCLIE) and !this.bDontReportError
            deferred_cp_zoom('TIP_CLIE','*','TCCODICE',cp_AbsName(oSource.parent,'oTIPCLIE_1_29'),i_cWhere,'GSPR_ATC',"Tipologie clientela",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCLIE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCLIE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCLIE)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCLIE = NVL(_Link_.TCCODICE,space(5))
      this.w_DESTIP = NVL(_Link_.TCDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCLIE = space(5)
      endif
      this.w_DESTIP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CLIE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CLIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCLIE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCCCODICE_1_4.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oCCCODICE_1_4.value=this.w_CCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_5.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_5.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODICE_1_7.value==this.w_ANCODICE)
      this.oPgFrm.Page1.oPag.oANCODICE_1_7.value=this.w_ANCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCNAZION_1_9.value==this.w_CCNAZION)
      this.oPgFrm.Page1.oPag.oCCNAZION_1_9.value=this.w_CCNAZION
    endif
    if not(this.oPgFrm.Page1.oPag.oCCPARIVA_1_10.value==this.w_CCPARIVA)
      this.oPgFrm.Page1.oPag.oCCPARIVA_1_10.value=this.w_CCPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCCMASCON_1_11.value==this.w_CCMASCON)
      this.oPgFrm.Page1.oPag.oCCMASCON_1_11.value=this.w_CCMASCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODFIS_1_12.value==this.w_CCCODFIS)
      this.oPgFrm.Page1.oPag.oCCCODFIS_1_12.value=this.w_CCCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCCMASCON_1_13.value==this.w_CCMASCON)
      this.oPgFrm.Page1.oPag.oCCMASCON_1_13.value=this.w_CCMASCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESMAS_1_14.value==this.w_CCDESMAS)
      this.oPgFrm.Page1.oPag.oCCDESMAS_1_14.value=this.w_CCDESMAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCATCON_1_16.value==this.w_CCCATCON)
      this.oPgFrm.Page1.oPag.oCCCATCON_1_16.value=this.w_CCCATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODLIN_1_18.value==this.w_CCCODLIN)
      this.oPgFrm.Page1.oPag.oCCCODLIN_1_18.value=this.w_CCCODLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODIVA_1_20.value==this.w_CCCODIVA)
      this.oPgFrm.Page1.oPag.oCCCODIVA_1_20.value=this.w_CCCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODIVA_1_21.value==this.w_CCCODIVA)
      this.oPgFrm.Page1.oPag.oCCCODIVA_1_21.value=this.w_CCCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODPAG_1_22.value==this.w_CCCODPAG)
      this.oPgFrm.Page1.oPag.oCCCODPAG_1_22.value=this.w_CCCODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLPRIV_1_23.RadioValue()==this.w_CCFLPRIV)
      this.oPgFrm.Page1.oPag.oCCFLPRIV_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLESIG_1_24.RadioValue()==this.w_CCFLESIG)
      this.oPgFrm.Page1.oPag.oCCFLESIG_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLRITE_1_25.RadioValue()==this.w_CCFLRITE)
      this.oPgFrm.Page1.oPag.oCCFLRITE_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODIRP_1_26.value==this.w_CCCODIRP)
      this.oPgFrm.Page1.oPag.oCCCODIRP_1_26.value=this.w_CCCODIRP
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLESIG_1_27.RadioValue()==this.w_CCFLESIG)
      this.oPgFrm.Page1.oPag.oCCFLESIG_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCIVASOS_1_28.RadioValue()==this.w_CCIVASOS)
      this.oPgFrm.Page1.oPag.oCCIVASOS_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCLIE_1_29.value==this.w_TIPCLIE)
      this.oPgFrm.Page1.oPag.oTIPCLIE_1_29.value=this.w_TIPCLIE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESIVA_1_51.value==this.w_CCDESIVA)
      this.oPgFrm.Page1.oPag.oCCDESIVA_1_51.value=this.w_CCDESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESLIN_1_52.value==this.w_CCDESLIN)
      this.oPgFrm.Page1.oPag.oCCDESLIN_1_52.value=this.w_CCDESLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCCN_1_53.value==this.w_CCDESCCN)
      this.oPgFrm.Page1.oPag.oCCDESCCN_1_53.value=this.w_CCDESCCN
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPAG_1_61.value==this.w_CCDESPAG)
      this.oPgFrm.Page1.oPag.oCCDESPAG_1_61.value=this.w_CCDESPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIRPEF_1_65.value==this.w_DESIRPEF)
      this.oPgFrm.Page1.oPag.oDESIRPEF_1_65.value=this.w_DESIRPEF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIP_1_76.value==this.w_DESTIP)
      this.oPgFrm.Page1.oPag.oDESTIP_1_76.value=this.w_DESTIP
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ANCODICE))  and (.w_ACTIVE<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCODICE_1_7.SetFocus()
            i_bnoObbl = !empty(.w_ANCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF(.w_ACTIVE<>'S',CHKCFP(.w_CCPARIVA, "PI",'C',0,.w_ANCODICE, .w_CCNAZION),.T.))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCPARIVA_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MCTIPMAS=.w_NewTipCon)  and not(upper(g_APPLICATION) <> "ADHOC REVOLUTION")  and (upper(g_APPLICATION) = "ADHOC REVOLUTION")  and not(empty(.w_CCMASCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCMASCON_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MCTIPMAS=.w_NewTipCon)  and not(upper(g_APPLICATION) <> "AD HOC ENTERPRISE")  and (upper(g_APPLICATION) = "AD HOC ENTERPRISE")  and not(empty(.w_CCMASCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCMASCON_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PERIVA=0)  and not(upper(g_APPLICATION) <> "AD HOC ENTERPRISE")  and (upper(g_APPLICATION) = "AD HOC ENTERPRISE")  and not(empty(.w_CCCODIVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCODIVA_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA inesistente, imponibile oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CCCODPAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCODPAG_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   not(.w_FLIRPE='R')  and not(NOT IsAlt())  and (.w_CCFLRITE='S')  and not(empty(.w_CCCODIRP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCODIRP_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_kcc
      if i_bRes=.t. and this.w_ACTIVE<>'S'
         CodiceGenerato=GSAR_BO4( THIS, this.w_CCCODICE, this.w_ANCODICE, this.w_CCMASCON, this.w_CCCATCON, this.w_CCCODLIN, this.w_CCCODIVA, this.w_CCCODPAG, this.w_CCFLRITE, this.w_CCCODIRP, this.w_CCNAZION, this.w_CCPARIVA,' ',' ',this.w_TIPCLIE, this.w_CCFLPRIV, this.w_CCFLESIG, this.w_CCCODFIS, this.w_CCIVASOS, this.w_NewTipCon)
         .w_CHKERR = EMPTY( CodiceGenerato )
         if .w_CHKERR=.T.
            i_bRes = .f.
            i_bnoChk = .t.
            this.w_NOTIFICA = "N"
         else
            this.w_NOTIFICA = "S"
         endif
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CCFLESIG = this.w_CCFLESIG
    this.o_CCFLRITE = this.w_CCFLRITE
    this.o_CCCODIRP = this.w_CCCODIRP
    return

enddefine

* --- Define pages as container
define class tgsar_kccPag1 as StdContainer
  Width  = 622
  height = 429
  stdWidth  = 622
  stdheight = 429
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCCCODICE_1_4 as StdField with uid="DIHSSMWTWK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CCCODICE", cQueryName = "CCCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 128581739,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=167, Top=12, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15)

  add object oCCDESCRI_1_5 as StdField with uid="MXVWQNBUEM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42995823,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=302, Top=12, InputMask=replicate('X',40)

  add object oANCODICE_1_7 as StdField with uid="WEVYDYFDPI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ANCODICE", cQueryName = "ANCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 128584523,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=167, Top=40, cSayPict="p_cli", cGetPict="p_cli", InputMask=replicate('X',15)

  func oANCODICE_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ACTIVE<>'S')
    endwith
   endif
  endfunc

  proc oANCODICE_1_7.mAfter
    with this.Parent.oContained
      .w_ANCODICE=CALCZERO(.w_ANCODICE)
    endwith
  endproc

  add object oCCNAZION_1_9 as StdField with uid="LAULZVRXQR",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CCNAZION", cQueryName = "CCNAZION",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 117657484,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=167, Top=68, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_CCNAZION"

  func oCCNAZION_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCNAZION_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCNAZION_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oCCNAZION_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oCCNAZION_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_CCNAZION
     i_obj.ecpSave()
  endproc

  add object oCCPARIVA_1_10 as StdField with uid="OLENWTTCWD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CCPARIVA", cQueryName = "CCPARIVA",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 142397543,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=167, Top=96, InputMask=replicate('X',12)

  func oCCPARIVA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(.w_ACTIVE<>'S',CHKCFP(.w_CCPARIVA, "PI",'C',0,.w_ANCODICE, .w_CCNAZION),.T.))
    endwith
    return bRes
  endfunc

  add object oCCMASCON_1_11 as StdField with uid="VQSYFGXIEK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CCMASCON", cQueryName = "CCMASCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 225664908,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=167, Top=124, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CCMASCON"

  func oCCMASCON_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oCCMASCON_1_11.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  func oCCMASCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCMASCON_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCMASCON_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCCMASCON_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili ("+iif((type('this.w_NewTipCon')='C' and this.w_NewTipCon='C') OR (type('this.parent.ocontained.w_NewTipCon')='C' and this.parent.ocontained.w_NewTipCon='C'),'clienti','fornitori')+')'+"",''+iif((type("this.w_NewTipCon")="C" and this.w_NewTipCon="C") OR (type("this.parent.ocontained.w_NewTipCon")="C" and this.parent.ocontained.w_NewTipCon="C"),"GSAR_ACL","GSAR_AFR")+'.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCCMASCON_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CCMASCON
     i_obj.ecpSave()
  endproc

  add object oCCCODFIS_1_12 as StdField with uid="TUMDVUQEGH",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CCCODFIS", cQueryName = "CCCODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 78250105,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=477, Top=96, cSayPict='REPL("!",16)', cGetPict='REPL("!",16)', InputMask=replicate('X',16)

  add object oCCMASCON_1_13 as StdField with uid="BKNNNGIHTM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CCMASCON", cQueryName = "CCMASCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento del cliente",;
    HelpContextID = 225664908,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=167, Top=124, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CCMASCON"

  func oCCMASCON_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
   endif
  endfunc

  func oCCMASCON_1_13.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oCCMASCON_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCMASCON_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCMASCON_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCCMASCON_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (clienti)",'GSAR2AMC.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCCMASCON_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CCMASCON
     i_obj.ecpSave()
  endproc

  add object oCCDESMAS_1_14 as StdField with uid="XKXHRMWDLH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CCDESMAS", cQueryName = "CCDESMAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 57667463,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=302, Top=124, InputMask=replicate('X',40)

  add object oCCCATCON_1_16 as StdField with uid="RIXUWBVQVC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CCCATCON", cQueryName = "CCCATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria contabile del cliente",;
    HelpContextID = 224657292,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=167, Top=150, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_CCCATCON"

  func oCCCATCON_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCATCON_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCATCON_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCCCATCON_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili clienti",'',this.parent.oContained
  endproc
  proc oCCCATCON_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_CCCATCON
     i_obj.ecpSave()
  endproc

  add object oCCCODLIN_1_18 as StdField with uid="LRMZAYGBZD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CCCODLIN", cQueryName = "CCCODLIN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Lingua in cui stampare i documenti",;
    HelpContextID = 178913396,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=167, Top=176, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_CCCODLIN"

  func oCCCODLIN_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCODLIN_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCODLIN_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oCCCODLIN_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oCCCODLIN_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_CCCODLIN
     i_obj.ecpSave()
  endproc

  add object oCCCODIVA_1_20 as StdField with uid="RSPAIYKGOP",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CCCODIVA", cQueryName = "CCCODIVA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA inesistente, imponibile oppure obsoleto",;
    ToolTipText = "Eventuale codice IVA non imponibile per cliente estero",;
    HelpContextID = 128581735,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=167, Top=202, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_CCCODIVA"

  func oCCCODIVA_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
   endif
  endfunc

  func oCCCODIVA_1_20.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oCCCODIVA_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCODIVA_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCODIVA_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCCCODIVA_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'GSCG0ADI.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oCCCODIVA_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_CCCODIVA
     i_obj.ecpSave()
  endproc

  add object oCCCODIVA_1_21 as StdField with uid="GIAYPSJZYZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CCCODIVA", cQueryName = "CCCODIVA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA inesistente oppure obsoleto",;
    ToolTipText = "Codice IVA non imponibile",;
    HelpContextID = 128581735,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=167, Top=202, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_CCCODIVA"

  func oCCCODIVA_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oCCCODIVA_1_21.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  func oCCCODIVA_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCODIVA_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCODIVA_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCCCODIVA_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'GSVE0MDV.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oCCCODIVA_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_CCCODIVA
     i_obj.ecpSave()
  endproc

  add object oCCCODPAG_1_22 as StdField with uid="GWRLPYSGQS",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CCCODPAG", cQueryName = "CCCODPAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice pagamento del cliente",;
    HelpContextID = 22413203,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=167, Top=228, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_CCCODPAG"

  func oCCCODPAG_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCODPAG_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCODPAG_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oCCCODPAG_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oCCCODPAG_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_CCCODPAG
     i_obj.ecpSave()
  endproc

  add object oCCFLPRIV_1_23 as StdCheck with uid="EISGGQAIID",rtseq=17,rtrep=.f.,left=167, top=256, caption="Cliente privato",;
    ToolTipText = "Se attivo identifica il cliente come privato (non Business)",;
    HelpContextID = 244895620,;
    cFormVar="w_CCFLPRIV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLPRIV_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCFLPRIV_1_23.GetRadio()
    this.Parent.oContained.w_CCFLPRIV = this.RadioValue()
    return .t.
  endfunc

  func oCCFLPRIV_1_23.SetRadio()
    this.Parent.oContained.w_CCFLPRIV=trim(this.Parent.oContained.w_CCFLPRIV)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLPRIV=='S',1,;
      0)
  endfunc

  func oCCFLPRIV_1_23.mHide()
    with this.Parent.oContained
      return (.w_NewTipCon='F')
    endwith
  endfunc

  add object oCCFLESIG_1_24 as StdCheck with uid="JCENMMWCXZ",rtseq=18,rtrep=.f.,left=302, top=256, caption="Soggetto PA",;
    ToolTipText = "Se attivo: stampa su fatture esigibilitÓ (differita o immediata)",;
    HelpContextID = 239652755,;
    cFormVar="w_CCFLESIG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLESIG_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCFLESIG_1_24.GetRadio()
    this.Parent.oContained.w_CCFLESIG = this.RadioValue()
    return .t.
  endfunc

  func oCCFLESIG_1_24.SetRadio()
    this.Parent.oContained.w_CCFLESIG=trim(this.Parent.oContained.w_CCFLESIG)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLESIG=='S',1,;
      0)
  endfunc

  func oCCFLESIG_1_24.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION" OR .w_NewTipCon='F')
    endwith
  endfunc

  add object oCCFLRITE_1_25 as StdCheck with uid="EEXYYJENRL",rtseq=19,rtrep=.f.,left=477, top=256, caption="IRPEF/IRES",;
    ToolTipText = "Se attivo, il cliente gestisce le ritenute attive. Pu˛ valere solo IRPEF/IRES.",;
    HelpContextID = 143077483,;
    cFormVar="w_CCFLRITE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLRITE_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCFLRITE_1_25.GetRadio()
    this.Parent.oContained.w_CCFLRITE = this.RadioValue()
    return .t.
  endfunc

  func oCCFLRITE_1_25.SetRadio()
    this.Parent.oContained.w_CCFLRITE=trim(this.Parent.oContained.w_CCFLRITE)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLRITE=='S',1,;
      0)
  endfunc

  func oCCFLRITE_1_25.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oCCCODIRP_1_26 as StdField with uid="LLYHQDAREC",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CCCODIRP", cQueryName = "CCCODIRP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un codice tributo I.R.PE.F.",;
    ToolTipText = "Codice Tributo I.R.PE.F.",;
    HelpContextID = 128581750,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=167, Top=278, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_CCCODIRP"

  func oCCCODIRP_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCFLRITE='S')
    endwith
   endif
  endfunc

  func oCCCODIRP_1_26.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  func oCCCODIRP_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCODIRP_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCODIRP_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oCCCODIRP_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oCCCODIRP_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_CCCODIRP
     i_obj.ecpSave()
  endproc

  add object oCCFLESIG_1_27 as StdRadio with uid="GWRKBGDBGF",rtseq=21,rtrep=.f.,left=167, top=279, width=156,height=32;
    , ToolTipText = "Tipo di esigibilitÓ in fase di fatturazione";
    , cFormVar="w_CCFLESIG", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCCFLESIG_1_27.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Immediata"
      this.Buttons(1).HelpContextID = 239652755
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Immediata+differita"
      this.Buttons(2).HelpContextID = 239652755
      this.Buttons(2).Top=15
      this.SetAll("Width",154)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Tipo di esigibilitÓ in fase di fatturazione")
      StdRadio::init()
    endproc

  func oCCFLESIG_1_27.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oCCFLESIG_1_27.GetRadio()
    this.Parent.oContained.w_CCFLESIG = this.RadioValue()
    return .t.
  endfunc

  func oCCFLESIG_1_27.SetRadio()
    this.Parent.oContained.w_CCFLESIG=trim(this.Parent.oContained.w_CCFLESIG)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLESIG=='N',1,;
      iif(this.Parent.oContained.w_CCFLESIG=='S',2,;
      0))
  endfunc

  func oCCFLESIG_1_27.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oCCIVASOS_1_28 as StdCheck with uid="XEWFUNKGSC",rtseq=22,rtrep=.f.,left=167, top=309, caption="Maturazione temporale IVA in sospensione",;
    ToolTipText = "Maturazione temporale IVA in sospensione",;
    HelpContextID = 243179399,;
    cFormVar="w_CCIVASOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCIVASOS_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCIVASOS_1_28.GetRadio()
    this.Parent.oContained.w_CCIVASOS = this.RadioValue()
    return .t.
  endfunc

  func oCCIVASOS_1_28.SetRadio()
    this.Parent.oContained.w_CCIVASOS=trim(this.Parent.oContained.w_CCIVASOS)
    this.value = ;
      iif(this.Parent.oContained.w_CCIVASOS=='S',1,;
      0)
  endfunc

  func oCCIVASOS_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCFLESIG='N')
    endwith
   endif
  endfunc

  add object oTIPCLIE_1_29 as StdField with uid="UBFKQUNNVV",rtseq=23,rtrep=.f.,;
    cFormVar = "w_TIPCLIE", cQueryName = "TIPCLIE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia clientela",;
    HelpContextID = 136238902,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=167, Top=332, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_CLIE", cZoomOnZoom="GSPR_ATC", oKey_1_1="TCCODICE", oKey_1_2="this.w_TIPCLIE"

  func oTIPCLIE_1_29.mHide()
    with this.Parent.oContained
      return (!Isalt() OR .w_NewTipCon='F')
    endwith
  endfunc

  func oTIPCLIE_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPCLIE_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPCLIE_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_CLIE','*','TCCODICE',cp_AbsName(this.parent,'oTIPCLIE_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPR_ATC',"Tipologie clientela",'',this.parent.oContained
  endproc
  proc oTIPCLIE_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSPR_ATC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_TIPCLIE
     i_obj.ecpSave()
  endproc


  add object oObj_1_30 as cp_runprogram with uid="PUQEESMDTK",left=3, top=524, width=229,height=19,;
    caption='GSAR_BO3(A)',;
   bGlobalFont=.t.,;
    prg="GSAR_BO3('A')",;
    cEvent = "w_ANCODICE Changed",;
    nPag=1;
    , HelpContextID = 228604903

  add object oCCDESIVA_1_51 as StdField with uid="NIIYXDFBPC",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CCDESIVA", cQueryName = "CCDESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 143659111,;
   bGlobalFont=.t.,;
    Height=21, Width=370, Left=239, Top=202, InputMask=replicate('X',35)

  add object oCCDESLIN_1_52 as StdField with uid="QSTTHMBOSW",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CCDESLIN", cQueryName = "CCDESLIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 193990772,;
   bGlobalFont=.t.,;
    Height=21, Width=370, Left=239, Top=176, InputMask=replicate('X',30)

  add object oCCDESCCN_1_53 as StdField with uid="BQLRCDCVJD",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CCDESCCN", cQueryName = "CCDESCCN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 42995828,;
   bGlobalFont=.t.,;
    Height=21, Width=370, Left=239, Top=150, InputMask=replicate('X',35)

  add object oCCDESPAG_1_61 as StdField with uid="KRMSQXGXAR",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CCDESPAG", cQueryName = "CCDESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 7335827,;
   bGlobalFont=.t.,;
    Height=21, Width=370, Left=239, Top=228, InputMask=replicate('X',30)

  add object oDESIRPEF_1_65 as StdField with uid="PTUIOUGTTJ",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DESIRPEF", cQueryName = "DESIRPEF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 260375164,;
   bGlobalFont=.t.,;
    Height=21, Width=370, Left=239, Top=278, InputMask=replicate('X',60)

  func oDESIRPEF_1_65.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc


  add object oBtn_1_67 as StdButton with uid="APIWEFNJEL",left=510, top=362, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 99186442;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_67.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_68 as StdButton with uid="ZJIUAZYUNY",left=561, top=362, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 99186442;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_68.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESTIP_1_76 as StdField with uid="UEDCIPQINE",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESTIP", cQueryName = "DESTIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 16776650,;
   bGlobalFont=.t.,;
    Height=21, Width=366, Left=243, Top=332, InputMask=replicate('X',50)

  func oDESTIP_1_76.mHide()
    with this.Parent.oContained
      return (!Isalt() OR .w_NewTipCon='F')
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="CLZHIJGKIM",Visible=.t., Left=10, Top=12,;
    Alignment=1, Width=153, Height=18,;
    Caption="Codice nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="GJDQJWOOJP",Visible=.t., Left=10, Top=40,;
    Alignment=1, Width=153, Height=18,;
    Caption="Codice cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_NewTipCon='F')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="DLBMKBUPOU",Visible=.t., Left=10, Top=124,;
    Alignment=1, Width=153, Height=18,;
    Caption="Mastro contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="BWFTEURSVP",Visible=.t., Left=10, Top=150,;
    Alignment=1, Width=153, Height=18,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="GMIWORNPWT",Visible=.t., Left=10, Top=176,;
    Alignment=1, Width=153, Height=18,;
    Caption="Cod. lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="GDLSITIGCM",Visible=.t., Left=10, Top=202,;
    Alignment=1, Width=153, Height=18,;
    Caption="Cod.IVA esenz./agev.:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="VTCQXQHMZM",Visible=.t., Left=1, Top=551,;
    Alignment=0, Width=456, Height=18,;
    Caption="Attenzione: i campi w_ccmascon e w_cccodiva sono doppi (AHR/AHE)"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="CMVYHZWEHJ",Visible=.t., Left=10, Top=202,;
    Alignment=1, Width=153, Height=18,;
    Caption="Cod.IVA non imp.:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="CFNMRQVETK",Visible=.t., Left=20, Top=228,;
    Alignment=1, Width=143, Height=18,;
    Caption="Cod. pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="EUWUMDOYMF",Visible=.t., Left=113, Top=280,;
    Alignment=1, Width=50, Height=18,;
    Caption="Tributo:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_1_74 as StdString with uid="PFGBLQRVCS",Visible=.t., Left=32, Top=98,;
    Alignment=1, Width=130, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="JFFCMSWAXW",Visible=.t., Left=58, Top=69,;
    Alignment=1, Width=104, Height=18,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="SURMUMAMKY",Visible=.t., Left=12, Top=335,;
    Alignment=1, Width=151, Height=18,;
    Caption="Tipologia clientela:"  ;
  , bGlobalFont=.t.

  func oStr_1_77.mHide()
    with this.Parent.oContained
      return (NOT IsAlt() OR .w_NewTipCon='F')
    endwith
  endfunc

  add object oStr_1_78 as StdString with uid="ELFXIJQAMY",Visible=.t., Left=20, Top=280,;
    Alignment=1, Width=143, Height=18,;
    Caption="Fatturazione ad esigibilitÓ:"  ;
  , bGlobalFont=.t.

  func oStr_1_78.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_95 as StdString with uid="ODYWIXGPAJ",Visible=.t., Left=334, Top=98,;
    Alignment=1, Width=135, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_96 as StdString with uid="JVELBROPBL",Visible=.t., Left=10, Top=40,;
    Alignment=1, Width=153, Height=18,;
    Caption="Codice fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_96.mHide()
    with this.Parent.oContained
      return (.w_NewTipCon='C')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kcc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_kcc
func  calczero
param pCODICE
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  private w_APPO
  w_APPO = pCODICE
  if g_CFNUME="S" AND LEN(ALLTRIM(p_CLI))<>0 AND NOT EMPTY(m.w_APPO)
    w_APPO = ALLTRIM(m.w_APPO)
    * ---  Controllo Zero Fill Modificato:
    * --- 1 - Viene testato solo se l'input e' INTERAMENTE composto da Cifre
    * --- 2 - Se presente una Picture sulla GET, calcola sulla Lunghezza di questa
    Private p_L, p_LL
    p_LL=0
    p_L=0
    * --- Cerca se presente un Qualsiasi Carattere diverso da un numero
    do while p_LL<LEN(m.w_APPO)
      p_LL = p_LL + 1
      if NOT SUBSTR(m.w_APPO, p_LL, 1) $ "1234567890"
        * --- Se esiste non esegue la Zero Fill
        p_L=-1
        EXIT
      endif
    enddo
    if p_L<>-1
      * --- Calcola la lunghezza dell'input sulla Picture di GET (se esiste)
      p_LL=LEN(ALLTRIM(p_CLI))
      w_APPO = RIGHT(REPL("0", p_LL) + ALLTRIM(m.w_APPO), p_LL)
    endif
  endif
  *i_retcode = 'stop'
  return m.w_APPO

endfunc
* --- Fine Area Manuale
