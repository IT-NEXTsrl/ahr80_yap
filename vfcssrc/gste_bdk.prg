* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bdk                                                        *
*              Elimina/varia la distinta                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_64]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-01                                                      *
* Last revis.: 2012-10-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bdk",oParentObject,m.pOper)
return(i_retval)

define class tgste_bdk as StdBatch
  * --- Local variables
  pOper = space(10)
  w_OK = .f.
  pOper = space(10)
  w_RIFCON = space(10)
  * --- WorkFile variables
  PNT_MAST_idx=0
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione, Variazione Distinta: Controlli Preliminari 
    *     (Lanciato da GSTE_ADI) Conditions and Check 
    * --- Parametro contenenente l'operazione che st� vagliando se possibile (Edit,Load,Delete)
    this.oParentObject.w_MESS = ah_Msgformat("Operazione abbandonata")
    this.w_OK = .T.
    if this.pOper="Load"
      do case
        case EMPTY(this.oParentObject.w_DICAURIF)
          this.oParentObject.w_MESS = ah_Msgformat("Inserire la causale distinta")
          this.w_OK = .F.
        case EMPTY(this.oParentObject.w_DIBANRIF)
          this.oParentObject.w_MESS = ah_Msgformat("Inserire la banca di presentazione")
          this.w_OK = .F.
        case EMPTY(this.oParentObject.w_DICODVAL)
          this.oParentObject.w_MESS = ah_Msgformat("Inserire la valuta della distinta")
          this.w_OK = .F.
        case EMPTY(this.oParentObject.w_DICAOVAL) AND g_COGE="S"
          this.oParentObject.w_MESS = ah_Msgformat("Inserire il cambio di contabilizzazione delle scadenze")
          this.w_OK = .F.
      endcase
    else
      * --- Se il movimento di primanota � statop generato da docfinance allora cancella il legame con il movimento di DocFinance
      if g_ISDF="S" And this.w_OK AND this.pOper="Query"
        GSDF_BDM (this, "DEL" , this.oParentObject.w_DINUMDIS)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Legge sulle partite collegate se hanno generato una contabilizzazione
      * --- Select from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PTRIFIND  from "+i_cTable+" PAR_TITE ";
            +" where PTSERIAL="+cp_ToStrODBC(this.oParentObject.w_DINUMDIS)+" AND PTROWORD=-2";
             ,"_Curs_PAR_TITE")
      else
        select PTRIFIND from (i_cTable);
         where PTSERIAL=this.oParentObject.w_DINUMDIS AND PTROWORD=-2;
          into cursor _Curs_PAR_TITE
      endif
      if used('_Curs_PAR_TITE')
        select _Curs_PAR_TITE
        locate for 1=1
        do while not(eof())
        if Not Empty(Nvl(_Curs_PAR_TITE.PTRIFIND,Space(10)))
          this.w_RIFCON = Nvl(_Curs_PAR_TITE.PTRIFIND,Space(10))
          exit
        endif
          select _Curs_PAR_TITE
          continue
        enddo
        use
      endif
      if this.w_OK AND this.oParentObject.w_DIFLDEFI="S" AND NOT EMPTY(this.w_RIFCON) AND g_COGE="S"
        if this.pOper="Query"
          this.oParentObject.w_MESS = ah_Msgformat("Distinta definitiva e contabilizzata; impossibile eliminare")
        else
          this.oParentObject.w_MESS = ah_Msgformat("Distinta definitiva e contabilizzata; impossibile variare")
        endif
        this.w_OK = .F.
      endif
      if this.w_OK AND this.oParentObject.w_DIFLDEFI="S" AND EMPTY(this.w_RIFCON) AND (NOT EMPTY(this.oParentObject.w_DICODCAU) OR g_COGE<>"S")
        * --- Distinta Definitiva
        if this.pOper="Edit"
          if g_COGE="S"
            this.oParentObject.w_MESS = "ATTENZIONE%0Distinta definitiva non ancora contabilizzata; proseguo?%0[Rispondendo affermativamente la distinta diverr� provvisoria]"
          else
            this.oParentObject.w_MESS = "ATTENZIONE%0Distinta definitiva; proseguo?%0[Rispondendo affermativamente la distinta diverr� provvisoria]"
          endif
        else
          if g_COGE="S"
            this.oParentObject.w_MESS = "ATTENZIONE%0Distinta definitiva non ancora contabilizzata; proseguo?"
          else
            this.oParentObject.w_MESS = "ATTENZIONE%0Distinta definitiva; proseguo?"
          endif
        endif
        if NOT ah_YesNo(this.oParentObject.w_MESS)
          this.oParentObject.w_MESS = ah_Msgformat("Operazione abbandonata")
          this.w_OK = .F.
        else
          * --- Toglie Flag Definitiva se l'utente decide cmq di modificarla...
          this.oParentObject.w_DIFLDEFI = "N"
        endif
      endif
    endif
    * --- Se .F. operazione non possibile...
    i_retcode = 'stop'
    i_retval = this.w_OK
    return
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='PAR_TITE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
