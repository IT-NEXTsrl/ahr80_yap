* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_kda                                                        *
*              Dati di riga ordini                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_248]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-27                                                      *
* Last revis.: 2018-02-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsor_kda",oParentObject))

* --- Class definition
define class tgsor_kda as StdForm
  Top    = 149
  Left   = 35

  * --- Standard Properties
  Width  = 601
  Height = 397+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-02-13"
  HelpContextID=186029719
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=218

  * --- Constant Properties
  _IDX = 0
  NOMENCLA_IDX = 0
  CACOARTI_IDX = 0
  SALDIART_IDX = 0
  VOCIIVA_IDX = 0
  MAGAZZIN_IDX = 0
  CAM_AGAZ_IDX = 0
  CLA_RIGD_IDX = 0
  CON_TRAM_IDX = 0
  UNIMIS_IDX = 0
  CONTI_IDX = 0
  NAZIONI_IDX = 0
  DOC_MAST_IDX = 0
  VOC_COST_IDX = 0
  ART_ICOL_IDX = 0
  CES_PITI_IDX = 0
  CAU_CESP_IDX = 0
  TIPICONF_IDX = 0
  COD_AREO_IDX = 0
  MODCLDAT_IDX = 0
  CATMCONT_IDX = 0
  cPrg = "gsor_kda"
  cComment = "Dati di riga ordini"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_FLEDIT = space(1)
  w_MVCLADOC = space(2)
  w_MVIMPEVA = 0
  w_MVPREZZO = 0
  w_MVFLSCOR = space(1)
  w_FLDTPR = space(1)
  w_CODNAZ = space(3)
  w_AZCODPOR = space(10)
  w_TIPCON = space(1)
  w_MVDATREG = ctod('  /  /  ')
  w_MVTIPRIG = space(1)
  w_MVDATDOC = ctod('  /  /  ')
  o_MVDATDOC = ctod('  /  /  ')
  w_MVCODVAL = space(3)
  w_MVCAOVAL = 0
  w_MVQTAUM1 = 0
  w_AZICOD = space(5)
  w_FLGEFA = space(1)
  w_OCONTRA = space(15)
  w_MVCODCLA = space(3)
  w_DESCLA = space(30)
  w_MVFLRAGG = space(1)
  w_MVIMPACC = 0
  w_MVIMPNAZ = 0
  w_MVCATCON = space(5)
  w_DESCON = space(35)
  w_MVCONTRO = space(15)
  w_DESCO2 = space(40)
  w_MVCODIVA = space(5)
  o_MVCODIVA = space(5)
  w_DESIVA = space(35)
  w_PERIND = 0
  w_PERIVA = 0
  w_MVCONIND = space(15)
  w_DESIND = space(40)
  w_MVCONTRA = space(15)
  w_DESCTR = space(50)
  w_FLAGEN = space(1)
  w_CT = space(1)
  w_CC = space(15)
  w_CM = space(3)
  w_CI = ctod('  /  /  ')
  w_CF = ctod('  /  /  ')
  w_CV = space(3)
  w_MVTIPCON = space(1)
  w_MVCODCON = space(15)
  w_CATCOM = space(3)
  w_IVACON = space(1)
  w_QUACON = space(1)
  w_OIMPACC = 0
  w_TIPSOT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_MVINICOM = ctod('  /  /  ')
  w_MVFINCOM = ctod('  /  /  ')
  w_MVFLVEAC = space(1)
  w_MVCONTRA = space(15)
  w_DESCTR = space(50)
  w_MVRIFESC = space(10)
  w_FLGCOM = space(1)
  w_UNMIS1 = space(3)
  w_MVUNIMIS = space(3)
  w_INDIVA = 0
  w_MVVALMAG = 0
  w_PERIVE = 0
  w_CAONAZ = 0
  w_MVVALNAZ = space(3)
  w_INDIVE = 0
  w_MVCODIVE = space(5)
  w_MVIMPNAZ = 0
  w_MVFLVEAC = space(1)
  w_TIPVOC = space(1)
  w_CODCOS = space(5)
  w_VOCTIP = space(1)
  w_MVVOCCEN = space(15)
  w_DESVOC = space(40)
  w_MVFLNOAN = space(1)
  w_MVFLPROV = space(1)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_XCONORN = space(15)
  w_MVCODCEN = space(15)
  w_VOCOBSO = ctod('  /  /  ')
  w_FLANAL = space(10)
  w_MVTIPRIG = space(1)
  w_MVSERRIF = space(10)
  w_MVCODART = space(20)
  w_FLCCAU = space(1)
  w_OCAUMAG = space(5)
  w_MVCAUMAG = space(5)
  o_MVCAUMAG = space(5)
  w_OCAUMAG = space(5)
  w_DESCAU = space(35)
  w_DTOBSO = ctod('  /  /  ')
  w_MVFLCASC = space(1)
  w_MVFLRISE = space(1)
  w_MVFLORDI = space(1)
  w_MVFLIMPE = space(1)
  w_MVCAUCOL = space(5)
  o_MVCAUCOL = space(5)
  w_MVF2CASC = space(1)
  w_MVF2RISE = space(1)
  w_MVF2ORDI = space(1)
  w_MVF2IMPE = space(1)
  w_FLAVA1 = space(1)
  w_AGGSAL = space(10)
  w_TESCOMP = .F.
  w_MVFLELGM = space(1)
  w_MVCODMAG = space(5)
  o_MVCODMAG = space(5)
  w_READMAT = space(5)
  w_MVCODMAT = space(5)
  o_MVCODMAT = space(5)
  w_MVKEYSAL = space(20)
  w_READMAG = space(5)
  w_QTAPER = 0
  w_QTRPER = 0
  w_DESMAG = space(30)
  w_MVDATEVA = ctod('  /  /  ')
  w_MVFLEVAS = space(1)
  w_QTDISP = 0
  w_BOLIVA = space(1)
  w_FLCOMM = space(1)
  w_MVEFFEVA = ctod('  /  /  ')
  w_MVQTAEVA = 0
  w_MVIMPEVA = 0
  w_MVCODATT = space(15)
  w_MVCODCOS = space(5)
  w_MVCODART = space(20)
  w_DESART = space(40)
  w_MVCODICE = space(20)
  w_OFLEVAS = space(1)
  w_MVQTAEV1 = 0
  w_UM1 = space(3)
  w_UMR = space(3)
  w_MVQTAIMP = 0
  w_MVQTAIM1 = 0
  w_UM1 = space(3)
  w_UMR = space(3)
  w_FLCASC = space(1)
  w_FLRISE = space(1)
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_F2CASC = space(1)
  w_F2RISE = space(1)
  w_F2ORDI = space(1)
  w_F2IMPE = space(1)
  w_MVSERIAL = space(10)
  w_CPROWNUM = 0
  w_MVROWRIF = 0
  w_SERRIF1 = space(10)
  w_SERIAL1 = space(10)
  w_ROWNUM1 = 0
  w_ROWRIF1 = 0
  w_DESMAT = space(30)
  w_Q2DISP = 0
  w_DESCA2 = space(35)
  w_Q2APER = 0
  w_Q2RPER = 0
  w_F2UBIC = space(1)
  w_FLUBIC = space(1)
  w_MVDATOAI = ctod('  /  /  ')
  w_MVDATOAF = ctod('  /  /  ')
  w_MVMC_PER = space(3)
  w_FLORDAPE = space(1)
  w_MVFLTRAS = space(1)
  w_MVNOMENC = space(8)
  o_MVNOMENC = space(8)
  w_DESNOM = space(35)
  w_ONOMENC = space(8)
  w_UMSUPP = space(3)
  w_MVUMSUPP = space(3)
  w_MVMOLSUP = 0
  w_UM1 = space(3)
  w_MVPESNET = 0
  w_TOTMASNE = 0
  w_MVPROORD = space(2)
  w_MVNAZPRO = space(3)
  w_OAIRPOR = space(10)
  w_MVAIRPOR = space(10)
  o_MVAIRPOR = space(10)
  w_MVTIPPRO = space(2)
  o_MVTIPPRO = space(2)
  w_MVTIPPRO = space(2)
  w_MVTIPPRO = space(2)
  w_MVPERPRO = 0
  o_MVPERPRO = 0
  w_MVIMPPRO = 0
  o_MVIMPPRO = 0
  w_MVTIPPR2 = space(2)
  o_MVTIPPR2 = space(2)
  w_MVTIPPR2 = space(2)
  w_MVTIPPR2 = space(2)
  w_MVPROCAP = 0
  o_MVPROCAP = 0
  w_MVIMPCAP = 0
  o_MVIMPCAP = 0
  w_ONAZPRO = space(3)
  w_OPROORD = space(2)
  w_MVNUMCOL = 0
  w_CODCONF = space(3)
  w_DESCONF = space(35)
  w_FLCOVA = space(1)
  w_DESCRI = space(40)
  w_MODDES = space(1)
  w_MVDESSUP = space(0)
  w_FLCESP = space(1)
  w_CATCES = space(15)
  w_DESCCESP = space(40)
  w_ASSCES = space(1)
  w_CESPRES = .F.
  w_MVCODCES = space(20)
  w_MVCESSER = space(10)
  o_MVCESSER = space(10)
  w_MVDESART = space(40)
  w_MCSERIAL = space(10)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod('  /  /  ')
  w_MVCODESE = space(4)
  w_STABEN = space(1)
  w_TIPART = space(2)
  w_CETIPO = space(2)
  w_CAUCES = space(5)
  w_MVPAEFOR = space(3)
  w_ISONAZ = space(3)
  w_ISONA2 = space(3)
  w_MVRIFEDI = space(14)
  w_MVCACONT = space(5)
  w_CGDESCRI = space(40)
  w_ANDTOBSO = ctod('  /  /  ')
  w_ARUTISER = space(1)
  w_MV_FLAGG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsor_kdaPag1","gsor_kda",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(2).addobject("oPag","tgsor_kdaPag2","gsor_kda",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Magazzino")
      .Pages(3).addobject("oPag","tgsor_kdaPag3","gsor_kda",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("INTRA/provv./conf.")
      .Pages(4).addobject("oPag","tgsor_kdaPag4","gsor_kda",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Note di riga")
      .Pages(5).addobject("oPag","tgsor_kdaPag5","gsor_kda",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Cespiti")
      .Pages(5).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMVCODCLA_1_19
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[20]
    this.cWorkTables[1]='NOMENCLA'
    this.cWorkTables[2]='CACOARTI'
    this.cWorkTables[3]='SALDIART'
    this.cWorkTables[4]='VOCIIVA'
    this.cWorkTables[5]='MAGAZZIN'
    this.cWorkTables[6]='CAM_AGAZ'
    this.cWorkTables[7]='CLA_RIGD'
    this.cWorkTables[8]='CON_TRAM'
    this.cWorkTables[9]='UNIMIS'
    this.cWorkTables[10]='CONTI'
    this.cWorkTables[11]='NAZIONI'
    this.cWorkTables[12]='DOC_MAST'
    this.cWorkTables[13]='VOC_COST'
    this.cWorkTables[14]='ART_ICOL'
    this.cWorkTables[15]='CES_PITI'
    this.cWorkTables[16]='CAU_CESP'
    this.cWorkTables[17]='TIPICONF'
    this.cWorkTables[18]='COD_AREO'
    this.cWorkTables[19]='MODCLDAT'
    this.cWorkTables[20]='CATMCONT'
    return(this.OpenAllTables(20))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLEDIT=space(1)
      .w_MVCLADOC=space(2)
      .w_MVIMPEVA=0
      .w_MVPREZZO=0
      .w_MVFLSCOR=space(1)
      .w_FLDTPR=space(1)
      .w_CODNAZ=space(3)
      .w_AZCODPOR=space(10)
      .w_TIPCON=space(1)
      .w_MVDATREG=ctod("  /  /  ")
      .w_MVTIPRIG=space(1)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_MVCODVAL=space(3)
      .w_MVCAOVAL=0
      .w_MVQTAUM1=0
      .w_AZICOD=space(5)
      .w_FLGEFA=space(1)
      .w_OCONTRA=space(15)
      .w_MVCODCLA=space(3)
      .w_DESCLA=space(30)
      .w_MVFLRAGG=space(1)
      .w_MVIMPACC=0
      .w_MVIMPNAZ=0
      .w_MVCATCON=space(5)
      .w_DESCON=space(35)
      .w_MVCONTRO=space(15)
      .w_DESCO2=space(40)
      .w_MVCODIVA=space(5)
      .w_DESIVA=space(35)
      .w_PERIND=0
      .w_PERIVA=0
      .w_MVCONIND=space(15)
      .w_DESIND=space(40)
      .w_MVCONTRA=space(15)
      .w_DESCTR=space(50)
      .w_FLAGEN=space(1)
      .w_CT=space(1)
      .w_CC=space(15)
      .w_CM=space(3)
      .w_CI=ctod("  /  /  ")
      .w_CF=ctod("  /  /  ")
      .w_CV=space(3)
      .w_MVTIPCON=space(1)
      .w_MVCODCON=space(15)
      .w_CATCOM=space(3)
      .w_IVACON=space(1)
      .w_QUACON=space(1)
      .w_OIMPACC=0
      .w_TIPSOT=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_MVINICOM=ctod("  /  /  ")
      .w_MVFINCOM=ctod("  /  /  ")
      .w_MVFLVEAC=space(1)
      .w_MVCONTRA=space(15)
      .w_DESCTR=space(50)
      .w_MVRIFESC=space(10)
      .w_FLGCOM=space(1)
      .w_UNMIS1=space(3)
      .w_MVUNIMIS=space(3)
      .w_INDIVA=0
      .w_MVVALMAG=0
      .w_PERIVE=0
      .w_CAONAZ=0
      .w_MVVALNAZ=space(3)
      .w_INDIVE=0
      .w_MVCODIVE=space(5)
      .w_MVIMPNAZ=0
      .w_MVFLVEAC=space(1)
      .w_TIPVOC=space(1)
      .w_CODCOS=space(5)
      .w_VOCTIP=space(1)
      .w_MVVOCCEN=space(15)
      .w_DESVOC=space(40)
      .w_MVFLNOAN=space(1)
      .w_MVFLPROV=space(1)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_XCONORN=space(15)
      .w_MVCODCEN=space(15)
      .w_VOCOBSO=ctod("  /  /  ")
      .w_FLANAL=space(10)
      .w_MVTIPRIG=space(1)
      .w_MVSERRIF=space(10)
      .w_MVCODART=space(20)
      .w_FLCCAU=space(1)
      .w_OCAUMAG=space(5)
      .w_MVCAUMAG=space(5)
      .w_OCAUMAG=space(5)
      .w_DESCAU=space(35)
      .w_DTOBSO=ctod("  /  /  ")
      .w_MVFLCASC=space(1)
      .w_MVFLRISE=space(1)
      .w_MVFLORDI=space(1)
      .w_MVFLIMPE=space(1)
      .w_MVCAUCOL=space(5)
      .w_MVF2CASC=space(1)
      .w_MVF2RISE=space(1)
      .w_MVF2ORDI=space(1)
      .w_MVF2IMPE=space(1)
      .w_FLAVA1=space(1)
      .w_AGGSAL=space(10)
      .w_TESCOMP=.f.
      .w_MVFLELGM=space(1)
      .w_MVCODMAG=space(5)
      .w_READMAT=space(5)
      .w_MVCODMAT=space(5)
      .w_MVKEYSAL=space(20)
      .w_READMAG=space(5)
      .w_QTAPER=0
      .w_QTRPER=0
      .w_DESMAG=space(30)
      .w_MVDATEVA=ctod("  /  /  ")
      .w_MVFLEVAS=space(1)
      .w_QTDISP=0
      .w_BOLIVA=space(1)
      .w_FLCOMM=space(1)
      .w_MVEFFEVA=ctod("  /  /  ")
      .w_MVQTAEVA=0
      .w_MVIMPEVA=0
      .w_MVCODATT=space(15)
      .w_MVCODCOS=space(5)
      .w_MVCODART=space(20)
      .w_DESART=space(40)
      .w_MVCODICE=space(20)
      .w_OFLEVAS=space(1)
      .w_MVQTAEV1=0
      .w_UM1=space(3)
      .w_UMR=space(3)
      .w_MVQTAIMP=0
      .w_MVQTAIM1=0
      .w_UM1=space(3)
      .w_UMR=space(3)
      .w_FLCASC=space(1)
      .w_FLRISE=space(1)
      .w_FLORDI=space(1)
      .w_FLIMPE=space(1)
      .w_F2CASC=space(1)
      .w_F2RISE=space(1)
      .w_F2ORDI=space(1)
      .w_F2IMPE=space(1)
      .w_MVSERIAL=space(10)
      .w_CPROWNUM=0
      .w_MVROWRIF=0
      .w_SERRIF1=space(10)
      .w_SERIAL1=space(10)
      .w_ROWNUM1=0
      .w_ROWRIF1=0
      .w_DESMAT=space(30)
      .w_Q2DISP=0
      .w_DESCA2=space(35)
      .w_Q2APER=0
      .w_Q2RPER=0
      .w_F2UBIC=space(1)
      .w_FLUBIC=space(1)
      .w_MVDATOAI=ctod("  /  /  ")
      .w_MVDATOAF=ctod("  /  /  ")
      .w_MVMC_PER=space(3)
      .w_FLORDAPE=space(1)
      .w_MVFLTRAS=space(1)
      .w_MVNOMENC=space(8)
      .w_DESNOM=space(35)
      .w_ONOMENC=space(8)
      .w_UMSUPP=space(3)
      .w_MVUMSUPP=space(3)
      .w_MVMOLSUP=0
      .w_UM1=space(3)
      .w_MVPESNET=0
      .w_TOTMASNE=0
      .w_MVPROORD=space(2)
      .w_MVNAZPRO=space(3)
      .w_OAIRPOR=space(10)
      .w_MVAIRPOR=space(10)
      .w_MVTIPPRO=space(2)
      .w_MVTIPPRO=space(2)
      .w_MVTIPPRO=space(2)
      .w_MVPERPRO=0
      .w_MVIMPPRO=0
      .w_MVTIPPR2=space(2)
      .w_MVTIPPR2=space(2)
      .w_MVTIPPR2=space(2)
      .w_MVPROCAP=0
      .w_MVIMPCAP=0
      .w_ONAZPRO=space(3)
      .w_OPROORD=space(2)
      .w_MVNUMCOL=0
      .w_CODCONF=space(3)
      .w_DESCONF=space(35)
      .w_FLCOVA=space(1)
      .w_DESCRI=space(40)
      .w_MODDES=space(1)
      .w_MVDESSUP=space(0)
      .w_FLCESP=space(1)
      .w_CATCES=space(15)
      .w_DESCCESP=space(40)
      .w_ASSCES=space(1)
      .w_CESPRES=.f.
      .w_MVCODCES=space(20)
      .w_MVCESSER=space(10)
      .w_MVDESART=space(40)
      .w_MCSERIAL=space(10)
      .w_MVNUMDOC=0
      .w_MVALFDOC=space(10)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_MVCODESE=space(4)
      .w_STABEN=space(1)
      .w_TIPART=space(2)
      .w_CETIPO=space(2)
      .w_CAUCES=space(5)
      .w_MVPAEFOR=space(3)
      .w_ISONAZ=space(3)
      .w_ISONA2=space(3)
      .w_MVRIFEDI=space(14)
      .w_MVCACONT=space(5)
      .w_CGDESCRI=space(40)
      .w_ANDTOBSO=ctod("  /  /  ")
      .w_ARUTISER=space(1)
      .w_MV_FLAGG=space(1)
      .w_FLEDIT=oParentObject.w_FLEDIT
      .w_MVCLADOC=oParentObject.w_MVCLADOC
      .w_MVIMPEVA=oParentObject.w_MVIMPEVA
      .w_MVPREZZO=oParentObject.w_MVPREZZO
      .w_MVFLSCOR=oParentObject.w_MVFLSCOR
      .w_FLDTPR=oParentObject.w_FLDTPR
      .w_CODNAZ=oParentObject.w_CODNAZ
      .w_AZCODPOR=oParentObject.w_AZCODPOR
      .w_MVDATREG=oParentObject.w_MVDATREG
      .w_MVTIPRIG=oParentObject.w_MVTIPRIG
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_MVCODVAL=oParentObject.w_MVCODVAL
      .w_MVCAOVAL=oParentObject.w_MVCAOVAL
      .w_MVQTAUM1=oParentObject.w_MVQTAUM1
      .w_AZICOD=oParentObject.w_AZICOD
      .w_FLGEFA=oParentObject.w_FLGEFA
      .w_MVCODCLA=oParentObject.w_MVCODCLA
      .w_MVFLRAGG=oParentObject.w_MVFLRAGG
      .w_MVIMPACC=oParentObject.w_MVIMPACC
      .w_MVIMPNAZ=oParentObject.w_MVIMPNAZ
      .w_MVCATCON=oParentObject.w_MVCATCON
      .w_MVCONTRO=oParentObject.w_MVCONTRO
      .w_MVCODIVA=oParentObject.w_MVCODIVA
      .w_PERIVA=oParentObject.w_PERIVA
      .w_MVCONIND=oParentObject.w_MVCONIND
      .w_MVCONTRA=oParentObject.w_MVCONTRA
      .w_FLAGEN=oParentObject.w_FLAGEN
      .w_MVTIPCON=oParentObject.w_MVTIPCON
      .w_MVCODCON=oParentObject.w_MVCODCON
      .w_CATCOM=oParentObject.w_CATCOM
      .w_MVINICOM=oParentObject.w_MVINICOM
      .w_MVFINCOM=oParentObject.w_MVFINCOM
      .w_MVFLVEAC=oParentObject.w_MVFLVEAC
      .w_MVCONTRA=oParentObject.w_MVCONTRA
      .w_MVRIFESC=oParentObject.w_MVRIFESC
      .w_FLGCOM=oParentObject.w_FLGCOM
      .w_UNMIS1=oParentObject.w_UNMIS1
      .w_MVUNIMIS=oParentObject.w_MVUNIMIS
      .w_INDIVA=oParentObject.w_INDIVA
      .w_MVVALMAG=oParentObject.w_MVVALMAG
      .w_PERIVE=oParentObject.w_PERIVE
      .w_CAONAZ=oParentObject.w_CAONAZ
      .w_MVVALNAZ=oParentObject.w_MVVALNAZ
      .w_INDIVE=oParentObject.w_INDIVE
      .w_MVCODIVE=oParentObject.w_MVCODIVE
      .w_MVIMPNAZ=oParentObject.w_MVIMPNAZ
      .w_MVFLVEAC=oParentObject.w_MVFLVEAC
      .w_CODCOS=oParentObject.w_CODCOS
      .w_VOCTIP=oParentObject.w_VOCTIP
      .w_MVVOCCEN=oParentObject.w_MVVOCCEN
      .w_MVFLNOAN=oParentObject.w_MVFLNOAN
      .w_MVFLPROV=oParentObject.w_MVFLPROV
      .w_UNMIS2=oParentObject.w_UNMIS2
      .w_UNMIS3=oParentObject.w_UNMIS3
      .w_XCONORN=oParentObject.w_XCONORN
      .w_MVCODCEN=oParentObject.w_MVCODCEN
      .w_VOCOBSO=oParentObject.w_VOCOBSO
      .w_FLANAL=oParentObject.w_FLANAL
      .w_MVTIPRIG=oParentObject.w_MVTIPRIG
      .w_MVSERRIF=oParentObject.w_MVSERRIF
      .w_MVCODART=oParentObject.w_MVCODART
      .w_FLCCAU=oParentObject.w_FLCCAU
      .w_MVCAUMAG=oParentObject.w_MVCAUMAG
      .w_MVFLCASC=oParentObject.w_MVFLCASC
      .w_MVFLRISE=oParentObject.w_MVFLRISE
      .w_MVFLORDI=oParentObject.w_MVFLORDI
      .w_MVFLIMPE=oParentObject.w_MVFLIMPE
      .w_MVCAUCOL=oParentObject.w_MVCAUCOL
      .w_MVF2CASC=oParentObject.w_MVF2CASC
      .w_MVF2RISE=oParentObject.w_MVF2RISE
      .w_MVF2ORDI=oParentObject.w_MVF2ORDI
      .w_MVF2IMPE=oParentObject.w_MVF2IMPE
      .w_FLAVA1=oParentObject.w_FLAVA1
      .w_TESCOMP=oParentObject.w_TESCOMP
      .w_MVFLELGM=oParentObject.w_MVFLELGM
      .w_MVCODMAG=oParentObject.w_MVCODMAG
      .w_MVCODMAT=oParentObject.w_MVCODMAT
      .w_MVKEYSAL=oParentObject.w_MVKEYSAL
      .w_QTAPER=oParentObject.w_QTAPER
      .w_QTRPER=oParentObject.w_QTRPER
      .w_MVDATEVA=oParentObject.w_MVDATEVA
      .w_MVFLEVAS=oParentObject.w_MVFLEVAS
      .w_BOLIVA=oParentObject.w_BOLIVA
      .w_MVEFFEVA=oParentObject.w_MVEFFEVA
      .w_MVQTAEVA=oParentObject.w_MVQTAEVA
      .w_MVIMPEVA=oParentObject.w_MVIMPEVA
      .w_MVCODATT=oParentObject.w_MVCODATT
      .w_MVCODCOS=oParentObject.w_MVCODCOS
      .w_MVCODART=oParentObject.w_MVCODART
      .w_MVCODICE=oParentObject.w_MVCODICE
      .w_MVQTAEV1=oParentObject.w_MVQTAEV1
      .w_MVQTAIMP=oParentObject.w_MVQTAIMP
      .w_MVQTAIM1=oParentObject.w_MVQTAIM1
      .w_FLCASC=oParentObject.w_FLCASC
      .w_FLRISE=oParentObject.w_FLRISE
      .w_FLORDI=oParentObject.w_FLORDI
      .w_FLIMPE=oParentObject.w_FLIMPE
      .w_F2CASC=oParentObject.w_F2CASC
      .w_F2RISE=oParentObject.w_F2RISE
      .w_F2ORDI=oParentObject.w_F2ORDI
      .w_F2IMPE=oParentObject.w_F2IMPE
      .w_MVSERIAL=oParentObject.w_MVSERIAL
      .w_CPROWNUM=oParentObject.w_CPROWNUM
      .w_MVROWRIF=oParentObject.w_MVROWRIF
      .w_Q2APER=oParentObject.w_Q2APER
      .w_Q2RPER=oParentObject.w_Q2RPER
      .w_F2UBIC=oParentObject.w_F2UBIC
      .w_FLUBIC=oParentObject.w_FLUBIC
      .w_MVDATOAI=oParentObject.w_MVDATOAI
      .w_MVDATOAF=oParentObject.w_MVDATOAF
      .w_MVMC_PER=oParentObject.w_MVMC_PER
      .w_FLORDAPE=oParentObject.w_FLORDAPE
      .w_MVFLTRAS=oParentObject.w_MVFLTRAS
      .w_MVNOMENC=oParentObject.w_MVNOMENC
      .w_MVUMSUPP=oParentObject.w_MVUMSUPP
      .w_MVMOLSUP=oParentObject.w_MVMOLSUP
      .w_MVPESNET=oParentObject.w_MVPESNET
      .w_MVPROORD=oParentObject.w_MVPROORD
      .w_MVNAZPRO=oParentObject.w_MVNAZPRO
      .w_OAIRPOR=oParentObject.w_OAIRPOR
      .w_MVAIRPOR=oParentObject.w_MVAIRPOR
      .w_MVTIPPRO=oParentObject.w_MVTIPPRO
      .w_MVTIPPRO=oParentObject.w_MVTIPPRO
      .w_MVTIPPRO=oParentObject.w_MVTIPPRO
      .w_MVPERPRO=oParentObject.w_MVPERPRO
      .w_MVIMPPRO=oParentObject.w_MVIMPPRO
      .w_MVTIPPR2=oParentObject.w_MVTIPPR2
      .w_MVTIPPR2=oParentObject.w_MVTIPPR2
      .w_MVTIPPR2=oParentObject.w_MVTIPPR2
      .w_MVPROCAP=oParentObject.w_MVPROCAP
      .w_MVIMPCAP=oParentObject.w_MVIMPCAP
      .w_ONAZPRO=oParentObject.w_ONAZPRO
      .w_OPROORD=oParentObject.w_OPROORD
      .w_MVNUMCOL=oParentObject.w_MVNUMCOL
      .w_CODCONF=oParentObject.w_CODCONF
      .w_MODDES=oParentObject.w_MODDES
      .w_MVDESSUP=oParentObject.w_MVDESSUP
      .w_ASSCES=oParentObject.w_ASSCES
      .w_MVCODCES=oParentObject.w_MVCODCES
      .w_MVCESSER=oParentObject.w_MVCESSER
      .w_MVDESART=oParentObject.w_MVDESART
      .w_MVNUMDOC=oParentObject.w_MVNUMDOC
      .w_MVALFDOC=oParentObject.w_MVALFDOC
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_MVCODESE=oParentObject.w_MVCODESE
      .w_CAUCES=oParentObject.w_CAUCES
      .w_MVPAEFOR=oParentObject.w_MVPAEFOR
      .w_MVRIFEDI=oParentObject.w_MVRIFEDI
      .w_MVCACONT=oParentObject.w_MVCACONT
      .w_ARUTISER=oParentObject.w_ARUTISER
      .w_MV_FLAGG=oParentObject.w_MV_FLAGG
          .DoRTCalc(1,8,.f.)
        .w_TIPCON = 'G'
          .DoRTCalc(10,17,.f.)
        .w_OCONTRA = .w_MVCONTRA
        .w_MVCODCLA = .w_MVCODCLA
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_MVCODCLA))
          .link_1_19('Full')
        endif
          .DoRTCalc(20,23,.f.)
        .w_MVCATCON = .w_MVCATCON
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_MVCATCON))
          .link_1_24('Full')
        endif
          .DoRTCalc(25,25,.f.)
        .w_MVCONTRO = .w_MVCONTRO
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_MVCONTRO))
          .link_1_26('Full')
        endif
          .DoRTCalc(27,27,.f.)
        .w_MVCODIVA = .w_MVCODIVA
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_MVCODIVA))
          .link_1_28('Full')
        endif
          .DoRTCalc(29,31,.f.)
        .w_MVCONIND = .w_MVCONIND
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_MVCONIND))
          .link_1_33('Full')
        endif
          .DoRTCalc(33,33,.f.)
        .w_MVCONTRA = .w_MVCONTRA
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_MVCONTRA))
          .link_1_35('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
          .DoRTCalc(35,47,.f.)
        .w_OIMPACC = .w_MVIMPACC
          .DoRTCalc(49,49,.f.)
        .w_OBTEST = .w_MVDATDOC
          .DoRTCalc(51,54,.f.)
        .w_MVCONTRA = .w_MVCONTRA
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_MVCONTRA))
          .link_1_67('Full')
        endif
          .DoRTCalc(56,60,.f.)
        .w_INDIVA = IIF(.w_INDIVE<>0 OR .w_PERIVE<>0,.w_INDIVE,.w_PERIND)
          .DoRTCalc(62,67,.f.)
        .w_MVIMPNAZ = CAIMPNAZ(.w_MVFLVEAC, .w_MVVALMAG, .w_MVCAOVAL, .w_CAONAZ, .w_MVDATDOC, .w_MVVALNAZ, .w_MVCODVAL, .w_MVCODIVE, .w_PERIVE, .w_INDIVE, .w_PERIVA, .w_INDIVA )
          .DoRTCalc(69,72,.f.)
        .w_MVVOCCEN = .w_MVVOCCEN
        .DoRTCalc(73,73,.f.)
        if not(empty(.w_MVVOCCEN))
          .link_1_88('Full')
        endif
        .DoRTCalc(74,85,.f.)
        if not(empty(.w_MVCODART))
          .link_2_4('Full')
        endif
          .DoRTCalc(86,86,.f.)
        .w_OCAUMAG = .w_MVCAUMAG
        .w_MVCAUMAG = .w_MVCAUMAG
        .DoRTCalc(88,88,.f.)
        if not(empty(.w_MVCAUMAG))
          .link_2_7('Full')
        endif
        .w_OCAUMAG = .w_MVCAUMAG
          .DoRTCalc(90,91,.f.)
        .w_MVFLCASC = IIF(.w_MVFLPROV='S',' ',.w_FLCASC)
        .w_MVFLRISE = IIF(.w_MVFLPROV='S',' ',.w_FLRISE)
        .w_MVFLORDI = IIF(.w_MVFLPROV='S',' ',.w_FLORDI)
        .w_MVFLIMPE = IIF(.w_MVFLPROV='S',' ',.w_FLIMPE)
        .DoRTCalc(96,96,.f.)
        if not(empty(.w_MVCAUCOL))
          .link_2_15('Full')
        endif
        .w_MVF2CASC = IIF(.w_MVFLPROV='S',' ',.w_F2CASC)
        .w_MVF2RISE = IIF(.w_MVFLPROV='S',' ',.w_F2RISE)
        .w_MVF2ORDI = IIF(.w_MVFLPROV='S',' ',.w_F2ORDI)
        .w_MVF2IMPE = IIF(.w_MVFLPROV='S',' ',.w_F2IMPE)
          .DoRTCalc(101,101,.f.)
        .w_AGGSAL = ALLTRIM(.w_MVFLCASC+.w_MVFLRISE+.w_MVFLORDI+.w_MVFLIMPE)
          .DoRTCalc(103,104,.f.)
        .w_MVCODMAG = .w_MVCODMAG
        .DoRTCalc(105,105,.f.)
        if not(empty(.w_MVCODMAG))
          .link_2_24('Full')
        endif
        .w_READMAT = .w_MVCODMAT
        .DoRTCalc(106,106,.f.)
        if not(empty(.w_READMAT))
          .link_2_25('Full')
        endif
        .DoRTCalc(107,107,.f.)
        if not(empty(.w_MVCODMAT))
          .link_2_26('Full')
        endif
        .w_MVKEYSAL = IIF(.w_MVTIPRIG='R' AND NOT EMPTY(.w_AGGSAL), .w_MVCODART, SPACE(20))
        .w_READMAG = .w_MVCODMAG
        .DoRTCalc(109,109,.f.)
        if not(empty(.w_READMAG))
          .link_2_28('Full')
        endif
          .DoRTCalc(110,114,.f.)
        .w_QTDISP = .w_QTAPER-.w_QTRPER
          .DoRTCalc(116,121,.f.)
        .w_MVCODCOS = IIF(EMPTY(.w_MVCODATT), SPACE(5), .w_CODCOS)
        .DoRTCalc(123,123,.f.)
        if not(empty(.w_MVCODART))
          .link_2_53('Full')
        endif
          .DoRTCalc(124,125,.f.)
        .w_OFLEVAS = .w_MVFLEVAS
          .DoRTCalc(127,127,.f.)
        .w_UM1 = .w_UNMIS1
        .w_UMR = .w_MVUNIMIS
          .DoRTCalc(130,131,.f.)
        .w_UM1 = .w_UNMIS1
        .w_UMR = .w_MVUNIMIS
          .DoRTCalc(134,144,.f.)
        .w_SERRIF1 = .w_MVSERRIF
        .w_SERIAL1 = .w_MVSERIAL
        .w_ROWNUM1 = .w_CPROWNUM
        .w_ROWRIF1 = .w_MVROWRIF
      .oPgFrm.Page2.oPag.oObj_2_83.Calculate()
          .DoRTCalc(149,149,.f.)
        .w_Q2DISP = .w_Q2APER-.w_Q2RPER
          .DoRTCalc(151,160,.f.)
        .w_MVNOMENC = .w_MVNOMENC
        .DoRTCalc(161,161,.f.)
        if not(empty(.w_MVNOMENC))
          .link_3_2('Full')
        endif
          .DoRTCalc(162,162,.f.)
        .w_ONOMENC = .w_MVNOMENC
          .DoRTCalc(164,164,.f.)
        .w_MVUMSUPP = IIF(.w_ONOMENC=.w_MVNOMENC, .w_MVUMSUPP, .w_UMSUPP)
        .DoRTCalc(165,165,.f.)
        if not(empty(.w_MVUMSUPP))
          .link_3_6('Full')
        endif
          .DoRTCalc(166,166,.f.)
        .w_UM1 = .w_UNMIS1
          .DoRTCalc(168,168,.f.)
        .w_TOTMASNE = .w_MVPESNET*.w_MVQTAUM1
          .DoRTCalc(170,170,.f.)
        .w_MVNAZPRO = .w_MVNAZPRO
        .DoRTCalc(171,171,.f.)
        if not(empty(.w_MVNAZPRO))
          .link_3_12('Full')
        endif
        .w_OAIRPOR = IIF(EMPTY(.w_MVAIRPOR), .w_AZCODPOR, .w_MVAIRPOR)
        .DoRTCalc(173,173,.f.)
        if not(empty(.w_MVAIRPOR))
          .link_3_14('Full')
        endif
          .DoRTCalc(174,183,.f.)
        .w_ONAZPRO = IIF(EMPTY(.w_MVNAZPRO), .w_CODNAZ, .w_MVNAZPRO)
        .w_OPROORD = IIF(EMPTY(.w_MVPROORD), g_PROAZI, .w_MVPROORD)
        .DoRTCalc(186,187,.f.)
        if not(empty(.w_CODCONF))
          .link_3_44('Full')
        endif
      .oPgFrm.Page5.oPag.oObj_5_5.Calculate()
        .DoRTCalc(188,198,.f.)
        if not(empty(.w_MVCODCES))
          .link_5_7('Full')
        endif
          .DoRTCalc(199,200,.f.)
        .w_MCSERIAL = .w_MVCESSER
          .DoRTCalc(202,209,.f.)
        .w_MVPAEFOR = .w_MVPAEFOR
        .DoRTCalc(210,210,.f.)
        if not(empty(.w_MVPAEFOR))
          .link_3_57('Full')
        endif
        .DoRTCalc(211,214,.f.)
        if not(empty(.w_MVCACONT))
          .link_1_99('Full')
        endif
    endwith
    this.DoRTCalc(215,218,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_70.enabled = this.oPgFrm.Page1.oPag.oBtn_1_70.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_77.enabled = this.oPgFrm.Page2.oPag.oBtn_2_77.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_78.enabled = this.oPgFrm.Page2.oPag.oBtn_2_78.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_8.enabled = this.oPgFrm.Page5.oPag.oBtn_5_8.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_9.enabled = this.oPgFrm.Page5.oPag.oBtn_5_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsor_kda
    * Se il modulo cespiti non � attivato o non sono attivati i flag per la gestione cespiti sui documenti la pagina 5 � disattivata
        this.oPgFrm.Pages[5].Enabled=Not(this.w_FLCESP<>'S' OR g_CESP<>'S' OR this.w_ASSCES='N')
    
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oMVCODCLA_1_19.enabled = i_bVal
      .Page1.oPag.oMVFLRAGG_1_21.enabled = i_bVal
      .Page1.oPag.oMVIMPNAZ_1_23.enabled = i_bVal
      .Page1.oPag.oMVCATCON_1_24.enabled = i_bVal
      .Page1.oPag.oMVCONTRO_1_26.enabled = i_bVal
      .Page1.oPag.oMVCODIVA_1_28.enabled = i_bVal
      .Page1.oPag.oMVCONIND_1_33.enabled = i_bVal
      .Page1.oPag.oMVCONTRA_1_35.enabled = i_bVal
      .Page1.oPag.oMVINICOM_1_63.enabled = i_bVal
      .Page1.oPag.oMVFINCOM_1_64.enabled = i_bVal
      .Page1.oPag.oMVCONTRA_1_67.enabled = i_bVal
      .Page1.oPag.oMVVOCCEN_1_88.enabled = i_bVal
      .Page1.oPag.oMVFLNOAN_1_90.enabled = i_bVal
      .Page2.oPag.oMVCAUMAG_2_7.enabled = i_bVal
      .Page2.oPag.oMVCODMAG_2_24.enabled = i_bVal
      .Page2.oPag.oMVCODMAT_2_26.enabled = i_bVal
      .Page2.oPag.oMVDATEVA_2_32.enabled = i_bVal
      .Page2.oPag.oMVFLEVAS_2_33.enabled = i_bVal
      .Page2.oPag.oMVDATOAI_2_94.enabled = i_bVal
      .Page2.oPag.oMVDATOAF_2_95.enabled = i_bVal
      .Page2.oPag.oMVMC_PER_2_99.enabled = i_bVal
      .Page3.oPag.oMVFLTRAS_3_1.enabled = i_bVal
      .Page3.oPag.oMVNOMENC_3_2.enabled = i_bVal
      .Page3.oPag.oMVMOLSUP_3_7.enabled = i_bVal
      .Page3.oPag.oMVPESNET_3_9.enabled = i_bVal
      .Page3.oPag.oMVPROORD_3_11.enabled = i_bVal
      .Page3.oPag.oMVNAZPRO_3_12.enabled = i_bVal
      .Page3.oPag.oMVAIRPOR_3_14.enabled = i_bVal
      .Page3.oPag.oMVTIPPRO_3_21.enabled = i_bVal
      .Page3.oPag.oMVTIPPRO_3_22.enabled = i_bVal
      .Page3.oPag.oMVTIPPRO_3_23.enabled = i_bVal
      .Page3.oPag.oMVPERPRO_3_27.enabled = i_bVal
      .Page3.oPag.oMVIMPPRO_3_28.enabled = i_bVal
      .Page3.oPag.oMVTIPPR2_3_29.enabled = i_bVal
      .Page3.oPag.oMVTIPPR2_3_30.enabled = i_bVal
      .Page3.oPag.oMVTIPPR2_3_31.enabled = i_bVal
      .Page3.oPag.oMVPROCAP_3_32.enabled = i_bVal
      .Page3.oPag.oMVIMPCAP_3_33.enabled = i_bVal
      .Page3.oPag.oMVNUMCOL_3_43.enabled = i_bVal
      .Page4.oPag.oMVDESSUP_4_2.enabled = i_bVal
      .Page5.oPag.oMVCODCES_5_7.enabled = i_bVal
      .Page3.oPag.oMVPAEFOR_3_57.enabled = i_bVal
      .Page1.oPag.oMVCACONT_1_99.enabled = i_bVal
      .Page1.oPag.oBtn_1_29.enabled = .Page1.oPag.oBtn_1_29.mCond()
      .Page1.oPag.oBtn_1_70.enabled = .Page1.oPag.oBtn_1_70.mCond()
      .Page2.oPag.oBtn_2_77.enabled = .Page2.oPag.oBtn_2_77.mCond()
      .Page2.oPag.oBtn_2_78.enabled = .Page2.oPag.oBtn_2_78.mCond()
      .Page5.oPag.oBtn_5_8.enabled = .Page5.oPag.oBtn_5_8.mCond()
      .Page5.oPag.oBtn_5_9.enabled = .Page5.oPag.oBtn_5_9.mCond()
      .Page1.oPag.oObj_1_54.enabled = i_bVal
      .Page2.oPag.oObj_2_83.enabled = i_bVal
      .Page5.oPag.oObj_5_5.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_FLEDIT=.w_FLEDIT
      .oParentObject.w_MVCLADOC=.w_MVCLADOC
      .oParentObject.w_MVIMPEVA=.w_MVIMPEVA
      .oParentObject.w_MVPREZZO=.w_MVPREZZO
      .oParentObject.w_MVFLSCOR=.w_MVFLSCOR
      .oParentObject.w_FLDTPR=.w_FLDTPR
      .oParentObject.w_CODNAZ=.w_CODNAZ
      .oParentObject.w_AZCODPOR=.w_AZCODPOR
      .oParentObject.w_MVDATREG=.w_MVDATREG
      .oParentObject.w_MVTIPRIG=.w_MVTIPRIG
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_MVCODVAL=.w_MVCODVAL
      .oParentObject.w_MVCAOVAL=.w_MVCAOVAL
      .oParentObject.w_MVQTAUM1=.w_MVQTAUM1
      .oParentObject.w_AZICOD=.w_AZICOD
      .oParentObject.w_FLGEFA=.w_FLGEFA
      .oParentObject.w_MVCODCLA=.w_MVCODCLA
      .oParentObject.w_MVFLRAGG=.w_MVFLRAGG
      .oParentObject.w_MVIMPACC=.w_MVIMPACC
      .oParentObject.w_MVIMPNAZ=.w_MVIMPNAZ
      .oParentObject.w_MVCATCON=.w_MVCATCON
      .oParentObject.w_MVCONTRO=.w_MVCONTRO
      .oParentObject.w_MVCODIVA=.w_MVCODIVA
      .oParentObject.w_PERIVA=.w_PERIVA
      .oParentObject.w_MVCONIND=.w_MVCONIND
      .oParentObject.w_MVCONTRA=.w_MVCONTRA
      .oParentObject.w_FLAGEN=.w_FLAGEN
      .oParentObject.w_MVTIPCON=.w_MVTIPCON
      .oParentObject.w_MVCODCON=.w_MVCODCON
      .oParentObject.w_CATCOM=.w_CATCOM
      .oParentObject.w_MVINICOM=.w_MVINICOM
      .oParentObject.w_MVFINCOM=.w_MVFINCOM
      .oParentObject.w_MVFLVEAC=.w_MVFLVEAC
      .oParentObject.w_MVCONTRA=.w_MVCONTRA
      .oParentObject.w_MVRIFESC=.w_MVRIFESC
      .oParentObject.w_FLGCOM=.w_FLGCOM
      .oParentObject.w_UNMIS1=.w_UNMIS1
      .oParentObject.w_MVUNIMIS=.w_MVUNIMIS
      .oParentObject.w_INDIVA=.w_INDIVA
      .oParentObject.w_MVVALMAG=.w_MVVALMAG
      .oParentObject.w_PERIVE=.w_PERIVE
      .oParentObject.w_CAONAZ=.w_CAONAZ
      .oParentObject.w_MVVALNAZ=.w_MVVALNAZ
      .oParentObject.w_INDIVE=.w_INDIVE
      .oParentObject.w_MVCODIVE=.w_MVCODIVE
      .oParentObject.w_MVIMPNAZ=.w_MVIMPNAZ
      .oParentObject.w_MVFLVEAC=.w_MVFLVEAC
      .oParentObject.w_CODCOS=.w_CODCOS
      .oParentObject.w_VOCTIP=.w_VOCTIP
      .oParentObject.w_MVVOCCEN=.w_MVVOCCEN
      .oParentObject.w_MVFLNOAN=.w_MVFLNOAN
      .oParentObject.w_MVFLPROV=.w_MVFLPROV
      .oParentObject.w_UNMIS2=.w_UNMIS2
      .oParentObject.w_UNMIS3=.w_UNMIS3
      .oParentObject.w_XCONORN=.w_XCONORN
      .oParentObject.w_MVCODCEN=.w_MVCODCEN
      .oParentObject.w_VOCOBSO=.w_VOCOBSO
      .oParentObject.w_FLANAL=.w_FLANAL
      .oParentObject.w_MVTIPRIG=.w_MVTIPRIG
      .oParentObject.w_MVSERRIF=.w_MVSERRIF
      .oParentObject.w_MVCODART=.w_MVCODART
      .oParentObject.w_FLCCAU=.w_FLCCAU
      .oParentObject.w_MVCAUMAG=.w_MVCAUMAG
      .oParentObject.w_MVFLCASC=.w_MVFLCASC
      .oParentObject.w_MVFLRISE=.w_MVFLRISE
      .oParentObject.w_MVFLORDI=.w_MVFLORDI
      .oParentObject.w_MVFLIMPE=.w_MVFLIMPE
      .oParentObject.w_MVCAUCOL=.w_MVCAUCOL
      .oParentObject.w_MVF2CASC=.w_MVF2CASC
      .oParentObject.w_MVF2RISE=.w_MVF2RISE
      .oParentObject.w_MVF2ORDI=.w_MVF2ORDI
      .oParentObject.w_MVF2IMPE=.w_MVF2IMPE
      .oParentObject.w_FLAVA1=.w_FLAVA1
      .oParentObject.w_TESCOMP=.w_TESCOMP
      .oParentObject.w_MVFLELGM=.w_MVFLELGM
      .oParentObject.w_MVCODMAG=.w_MVCODMAG
      .oParentObject.w_MVCODMAT=.w_MVCODMAT
      .oParentObject.w_MVKEYSAL=.w_MVKEYSAL
      .oParentObject.w_QTAPER=.w_QTAPER
      .oParentObject.w_QTRPER=.w_QTRPER
      .oParentObject.w_MVDATEVA=.w_MVDATEVA
      .oParentObject.w_MVFLEVAS=.w_MVFLEVAS
      .oParentObject.w_BOLIVA=.w_BOLIVA
      .oParentObject.w_MVEFFEVA=.w_MVEFFEVA
      .oParentObject.w_MVQTAEVA=.w_MVQTAEVA
      .oParentObject.w_MVIMPEVA=.w_MVIMPEVA
      .oParentObject.w_MVCODATT=.w_MVCODATT
      .oParentObject.w_MVCODCOS=.w_MVCODCOS
      .oParentObject.w_MVCODART=.w_MVCODART
      .oParentObject.w_MVCODICE=.w_MVCODICE
      .oParentObject.w_MVQTAEV1=.w_MVQTAEV1
      .oParentObject.w_MVQTAIMP=.w_MVQTAIMP
      .oParentObject.w_MVQTAIM1=.w_MVQTAIM1
      .oParentObject.w_FLCASC=.w_FLCASC
      .oParentObject.w_FLRISE=.w_FLRISE
      .oParentObject.w_FLORDI=.w_FLORDI
      .oParentObject.w_FLIMPE=.w_FLIMPE
      .oParentObject.w_F2CASC=.w_F2CASC
      .oParentObject.w_F2RISE=.w_F2RISE
      .oParentObject.w_F2ORDI=.w_F2ORDI
      .oParentObject.w_F2IMPE=.w_F2IMPE
      .oParentObject.w_MVSERIAL=.w_MVSERIAL
      .oParentObject.w_CPROWNUM=.w_CPROWNUM
      .oParentObject.w_MVROWRIF=.w_MVROWRIF
      .oParentObject.w_Q2APER=.w_Q2APER
      .oParentObject.w_Q2RPER=.w_Q2RPER
      .oParentObject.w_F2UBIC=.w_F2UBIC
      .oParentObject.w_FLUBIC=.w_FLUBIC
      .oParentObject.w_MVDATOAI=.w_MVDATOAI
      .oParentObject.w_MVDATOAF=.w_MVDATOAF
      .oParentObject.w_MVMC_PER=.w_MVMC_PER
      .oParentObject.w_FLORDAPE=.w_FLORDAPE
      .oParentObject.w_MVFLTRAS=.w_MVFLTRAS
      .oParentObject.w_MVNOMENC=.w_MVNOMENC
      .oParentObject.w_MVUMSUPP=.w_MVUMSUPP
      .oParentObject.w_MVMOLSUP=.w_MVMOLSUP
      .oParentObject.w_MVPESNET=.w_MVPESNET
      .oParentObject.w_MVPROORD=.w_MVPROORD
      .oParentObject.w_MVNAZPRO=.w_MVNAZPRO
      .oParentObject.w_OAIRPOR=.w_OAIRPOR
      .oParentObject.w_MVAIRPOR=.w_MVAIRPOR
      .oParentObject.w_MVTIPPRO=.w_MVTIPPRO
      .oParentObject.w_MVTIPPRO=.w_MVTIPPRO
      .oParentObject.w_MVTIPPRO=.w_MVTIPPRO
      .oParentObject.w_MVPERPRO=.w_MVPERPRO
      .oParentObject.w_MVIMPPRO=.w_MVIMPPRO
      .oParentObject.w_MVTIPPR2=.w_MVTIPPR2
      .oParentObject.w_MVTIPPR2=.w_MVTIPPR2
      .oParentObject.w_MVTIPPR2=.w_MVTIPPR2
      .oParentObject.w_MVPROCAP=.w_MVPROCAP
      .oParentObject.w_MVIMPCAP=.w_MVIMPCAP
      .oParentObject.w_ONAZPRO=.w_ONAZPRO
      .oParentObject.w_OPROORD=.w_OPROORD
      .oParentObject.w_MVNUMCOL=.w_MVNUMCOL
      .oParentObject.w_CODCONF=.w_CODCONF
      .oParentObject.w_MODDES=.w_MODDES
      .oParentObject.w_MVDESSUP=.w_MVDESSUP
      .oParentObject.w_ASSCES=.w_ASSCES
      .oParentObject.w_MVCODCES=.w_MVCODCES
      .oParentObject.w_MVCESSER=.w_MVCESSER
      .oParentObject.w_MVDESART=.w_MVDESART
      .oParentObject.w_MVNUMDOC=.w_MVNUMDOC
      .oParentObject.w_MVALFDOC=.w_MVALFDOC
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_MVCODESE=.w_MVCODESE
      .oParentObject.w_CAUCES=.w_CAUCES
      .oParentObject.w_MVPAEFOR=.w_MVPAEFOR
      .oParentObject.w_MVRIFEDI=.w_MVRIFEDI
      .oParentObject.w_MVCACONT=.w_MVCACONT
      .oParentObject.w_ARUTISER=.w_ARUTISER
      .oParentObject.w_MV_FLAGG=.w_MV_FLAGG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
            .w_TIPCON = 'G'
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .DoRTCalc(10,49,.t.)
        if .o_MVDATDOC<>.w_MVDATDOC
            .w_OBTEST = .w_MVDATDOC
        endif
        .DoRTCalc(51,60,.t.)
        if .o_MVCODIVA<>.w_MVCODIVA
            .w_INDIVA = IIF(.w_INDIVE<>0 OR .w_PERIVE<>0,.w_INDIVE,.w_PERIND)
        endif
        .DoRTCalc(62,67,.t.)
        if .o_MVCODIVA<>.w_MVCODIVA
            .w_MVIMPNAZ = CAIMPNAZ(.w_MVFLVEAC, .w_MVVALMAG, .w_MVCAOVAL, .w_CAONAZ, .w_MVDATDOC, .w_MVVALNAZ, .w_MVCODVAL, .w_MVCODIVE, .w_PERIVE, .w_INDIVE, .w_PERIVA, .w_INDIVA )
        endif
        .DoRTCalc(69,84,.t.)
          .link_2_4('Full')
        .DoRTCalc(86,91,.t.)
        if .o_MVCAUMAG<>.w_MVCAUMAG
            .w_MVFLCASC = IIF(.w_MVFLPROV='S',' ',.w_FLCASC)
        endif
        if .o_MVCAUMAG<>.w_MVCAUMAG
            .w_MVFLRISE = IIF(.w_MVFLPROV='S',' ',.w_FLRISE)
        endif
        if .o_MVCAUMAG<>.w_MVCAUMAG
            .w_MVFLORDI = IIF(.w_MVFLPROV='S',' ',.w_FLORDI)
        endif
        if .o_MVCAUMAG<>.w_MVCAUMAG
            .w_MVFLIMPE = IIF(.w_MVFLPROV='S',' ',.w_FLIMPE)
        endif
        if .o_MVCAUMAG<>.w_MVCAUMAG
          .link_2_15('Full')
        endif
        if .o_MVCAUCOL<>.w_MVCAUCOL
            .w_MVF2CASC = IIF(.w_MVFLPROV='S',' ',.w_F2CASC)
        endif
        if .o_MVCAUCOL<>.w_MVCAUCOL
            .w_MVF2RISE = IIF(.w_MVFLPROV='S',' ',.w_F2RISE)
        endif
        if .o_MVCAUCOL<>.w_MVCAUCOL
            .w_MVF2ORDI = IIF(.w_MVFLPROV='S',' ',.w_F2ORDI)
        endif
        if .o_MVCAUCOL<>.w_MVCAUCOL
            .w_MVF2IMPE = IIF(.w_MVFLPROV='S',' ',.w_F2IMPE)
        endif
        .DoRTCalc(101,101,.t.)
            .w_AGGSAL = ALLTRIM(.w_MVFLCASC+.w_MVFLRISE+.w_MVFLORDI+.w_MVFLIMPE)
        .DoRTCalc(103,105,.t.)
        if .o_MVCODMAT<>.w_MVCODMAT
            .w_READMAT = .w_MVCODMAT
          .link_2_25('Full')
        endif
        if .o_MVCAUMAG<>.w_MVCAUMAG
            .w_MVCODMAT = IIF(EMPTY(.w_MVCAUCOL), SPACE(5), .w_MVCODMAT)
          .link_2_26('Full')
        endif
            .w_MVKEYSAL = IIF(.w_MVTIPRIG='R' AND NOT EMPTY(.w_AGGSAL), .w_MVCODART, SPACE(20))
        if .o_MVCODMAG<>.w_MVCODMAG
            .w_READMAG = .w_MVCODMAG
          .link_2_28('Full')
        endif
        .DoRTCalc(110,121,.t.)
            .w_MVCODCOS = IIF(EMPTY(.w_MVCODATT), SPACE(5), .w_CODCOS)
          .link_2_53('Full')
        .DoRTCalc(124,127,.t.)
            .w_UM1 = .w_UNMIS1
            .w_UMR = .w_MVUNIMIS
        .DoRTCalc(130,131,.t.)
            .w_UM1 = .w_UNMIS1
            .w_UMR = .w_MVUNIMIS
        .DoRTCalc(134,144,.t.)
            .w_SERRIF1 = .w_MVSERRIF
            .w_SERIAL1 = .w_MVSERIAL
            .w_ROWNUM1 = .w_CPROWNUM
            .w_ROWRIF1 = .w_MVROWRIF
        .oPgFrm.Page2.oPag.oObj_2_83.Calculate()
        .DoRTCalc(149,164,.t.)
        if .o_MVNOMENC<>.w_MVNOMENC
            .w_MVUMSUPP = IIF(.w_ONOMENC=.w_MVNOMENC, .w_MVUMSUPP, .w_UMSUPP)
          .link_3_6('Full')
        endif
        .DoRTCalc(166,166,.t.)
            .w_UM1 = .w_UNMIS1
        .DoRTCalc(168,168,.t.)
            .w_TOTMASNE = .w_MVPESNET*.w_MVQTAUM1
        .DoRTCalc(170,171,.t.)
            .w_OAIRPOR = IIF(EMPTY(.w_MVAIRPOR), .w_AZCODPOR, .w_MVAIRPOR)
        if .o_MVAIRPOR<>.w_MVAIRPOR
            .w_MVAIRPOR = .w_OAIRPOR
          .link_3_14('Full')
        endif
        .DoRTCalc(174,176,.t.)
        if .o_MVTIPPRO<>.w_MVTIPPRO.or. .o_MVIMPPRO<>.w_MVIMPPRO
            .w_MVPERPRO = iif((.w_MVIMPPRO<>0 AND .w_MVTIPPRO='FO') OR .w_MVTIPPRO='ES',0,.w_MVPERPRO)
        endif
        if .o_MVTIPPRO<>.w_MVTIPPRO.or. .o_MVPERPRO<>.w_MVPERPRO
            .w_MVIMPPRO = iif((.w_MVPERPRO<>0 AND .w_MVTIPPRO='FO') OR .w_MVTIPPRO='ES',0,.w_MVIMPPRO)
        endif
        .DoRTCalc(179,181,.t.)
        if .o_MVTIPPR2<>.w_MVTIPPR2.or. .o_MVIMPCAP<>.w_MVIMPCAP
            .w_MVPROCAP = iif((.w_MVIMPCAP<>0 AND .w_MVTIPPR2='FO') OR .w_MVTIPPR2='ES',0,.w_MVPROCAP)
        endif
        if .o_MVTIPPR2<>.w_MVTIPPR2.or. .o_MVPROCAP<>.w_MVPROCAP
            .w_MVIMPCAP = iif((.w_MVPROCAP<>0 AND .w_MVTIPPR2='FO') OR .w_MVTIPPR2='ES',0,.w_MVIMPCAP)
        endif
            .w_ONAZPRO = IIF(EMPTY(.w_MVNAZPRO), .w_CODNAZ, .w_MVNAZPRO)
            .w_OPROORD = IIF(EMPTY(.w_MVPROORD), g_PROAZI, .w_MVPROORD)
        .DoRTCalc(186,186,.t.)
          .link_3_44('Full')
        .oPgFrm.Page5.oPag.oObj_5_5.Calculate()
        .DoRTCalc(188,200,.t.)
        if .o_MVCESSER<>.w_MVCESSER
            .w_MCSERIAL = .w_MVCESSER
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(202,218,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_83.Calculate()
        .oPgFrm.Page5.oPag.oObj_5_5.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMVFLRAGG_1_21.enabled = this.oPgFrm.Page1.oPag.oMVFLRAGG_1_21.mCond()
    this.oPgFrm.Page1.oPag.oMVIMPNAZ_1_23.enabled = this.oPgFrm.Page1.oPag.oMVIMPNAZ_1_23.mCond()
    this.oPgFrm.Page1.oPag.oMVCATCON_1_24.enabled = this.oPgFrm.Page1.oPag.oMVCATCON_1_24.mCond()
    this.oPgFrm.Page1.oPag.oMVCONTRO_1_26.enabled = this.oPgFrm.Page1.oPag.oMVCONTRO_1_26.mCond()
    this.oPgFrm.Page1.oPag.oMVCODIVA_1_28.enabled = this.oPgFrm.Page1.oPag.oMVCODIVA_1_28.mCond()
    this.oPgFrm.Page1.oPag.oMVCONIND_1_33.enabled = this.oPgFrm.Page1.oPag.oMVCONIND_1_33.mCond()
    this.oPgFrm.Page1.oPag.oMVCONTRA_1_35.enabled = this.oPgFrm.Page1.oPag.oMVCONTRA_1_35.mCond()
    this.oPgFrm.Page1.oPag.oMVCONTRA_1_67.enabled = this.oPgFrm.Page1.oPag.oMVCONTRA_1_67.mCond()
    this.oPgFrm.Page1.oPag.oMVVOCCEN_1_88.enabled = this.oPgFrm.Page1.oPag.oMVVOCCEN_1_88.mCond()
    this.oPgFrm.Page2.oPag.oMVCAUMAG_2_7.enabled = this.oPgFrm.Page2.oPag.oMVCAUMAG_2_7.mCond()
    this.oPgFrm.Page2.oPag.oMVCODMAG_2_24.enabled = this.oPgFrm.Page2.oPag.oMVCODMAG_2_24.mCond()
    this.oPgFrm.Page2.oPag.oMVCODMAT_2_26.enabled = this.oPgFrm.Page2.oPag.oMVCODMAT_2_26.mCond()
    this.oPgFrm.Page2.oPag.oMVDATEVA_2_32.enabled = this.oPgFrm.Page2.oPag.oMVDATEVA_2_32.mCond()
    this.oPgFrm.Page2.oPag.oMVFLEVAS_2_33.enabled = this.oPgFrm.Page2.oPag.oMVFLEVAS_2_33.mCond()
    this.oPgFrm.Page2.oPag.oMVDATOAI_2_94.enabled = this.oPgFrm.Page2.oPag.oMVDATOAI_2_94.mCond()
    this.oPgFrm.Page2.oPag.oMVDATOAF_2_95.enabled = this.oPgFrm.Page2.oPag.oMVDATOAF_2_95.mCond()
    this.oPgFrm.Page2.oPag.oMVMC_PER_2_99.enabled = this.oPgFrm.Page2.oPag.oMVMC_PER_2_99.mCond()
    this.oPgFrm.Page3.oPag.oMVFLTRAS_3_1.enabled = this.oPgFrm.Page3.oPag.oMVFLTRAS_3_1.mCond()
    this.oPgFrm.Page3.oPag.oMVNOMENC_3_2.enabled = this.oPgFrm.Page3.oPag.oMVNOMENC_3_2.mCond()
    this.oPgFrm.Page3.oPag.oMVMOLSUP_3_7.enabled = this.oPgFrm.Page3.oPag.oMVMOLSUP_3_7.mCond()
    this.oPgFrm.Page3.oPag.oMVPESNET_3_9.enabled = this.oPgFrm.Page3.oPag.oMVPESNET_3_9.mCond()
    this.oPgFrm.Page3.oPag.oMVPROORD_3_11.enabled = this.oPgFrm.Page3.oPag.oMVPROORD_3_11.mCond()
    this.oPgFrm.Page3.oPag.oMVNAZPRO_3_12.enabled = this.oPgFrm.Page3.oPag.oMVNAZPRO_3_12.mCond()
    this.oPgFrm.Page3.oPag.oMVAIRPOR_3_14.enabled = this.oPgFrm.Page3.oPag.oMVAIRPOR_3_14.mCond()
    this.oPgFrm.Page3.oPag.oMVTIPPRO_3_21.enabled = this.oPgFrm.Page3.oPag.oMVTIPPRO_3_21.mCond()
    this.oPgFrm.Page3.oPag.oMVTIPPRO_3_22.enabled = this.oPgFrm.Page3.oPag.oMVTIPPRO_3_22.mCond()
    this.oPgFrm.Page3.oPag.oMVPERPRO_3_27.enabled = this.oPgFrm.Page3.oPag.oMVPERPRO_3_27.mCond()
    this.oPgFrm.Page3.oPag.oMVIMPPRO_3_28.enabled = this.oPgFrm.Page3.oPag.oMVIMPPRO_3_28.mCond()
    this.oPgFrm.Page3.oPag.oMVTIPPR2_3_30.enabled = this.oPgFrm.Page3.oPag.oMVTIPPR2_3_30.mCond()
    this.oPgFrm.Page3.oPag.oMVTIPPR2_3_31.enabled = this.oPgFrm.Page3.oPag.oMVTIPPR2_3_31.mCond()
    this.oPgFrm.Page3.oPag.oMVPROCAP_3_32.enabled = this.oPgFrm.Page3.oPag.oMVPROCAP_3_32.mCond()
    this.oPgFrm.Page3.oPag.oMVIMPCAP_3_33.enabled = this.oPgFrm.Page3.oPag.oMVIMPCAP_3_33.mCond()
    this.oPgFrm.Page3.oPag.oMVNUMCOL_3_43.enabled = this.oPgFrm.Page3.oPag.oMVNUMCOL_3_43.mCond()
    this.oPgFrm.Page4.oPag.oMVDESSUP_4_2.enabled = this.oPgFrm.Page4.oPag.oMVDESSUP_4_2.mCond()
    this.oPgFrm.Page5.oPag.oMVCODCES_5_7.enabled = this.oPgFrm.Page5.oPag.oMVCODCES_5_7.mCond()
    this.oPgFrm.Page3.oPag.oMVPAEFOR_3_57.enabled = this.oPgFrm.Page3.oPag.oMVPAEFOR_3_57.mCond()
    this.oPgFrm.Page1.oPag.oMVCACONT_1_99.enabled = this.oPgFrm.Page1.oPag.oMVCACONT_1_99.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_70.enabled = this.oPgFrm.Page1.oPag.oBtn_1_70.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_8.enabled = this.oPgFrm.Page5.oPag.oBtn_5_8.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_9.enabled = this.oPgFrm.Page5.oPag.oBtn_5_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMVIMPNAZ_1_23.visible=!this.oPgFrm.Page1.oPag.oMVIMPNAZ_1_23.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    this.oPgFrm.Page1.oPag.oMVCONIND_1_33.visible=!this.oPgFrm.Page1.oPag.oMVCONIND_1_33.mHide()
    this.oPgFrm.Page1.oPag.oDESIND_1_34.visible=!this.oPgFrm.Page1.oPag.oDESIND_1_34.mHide()
    this.oPgFrm.Page1.oPag.oMVCONTRA_1_35.visible=!this.oPgFrm.Page1.oPag.oMVCONTRA_1_35.mHide()
    this.oPgFrm.Page1.oPag.oDESCTR_1_36.visible=!this.oPgFrm.Page1.oPag.oDESCTR_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oMVCONTRA_1_67.visible=!this.oPgFrm.Page1.oPag.oMVCONTRA_1_67.mHide()
    this.oPgFrm.Page1.oPag.oDESCTR_1_68.visible=!this.oPgFrm.Page1.oPag.oDESCTR_1_68.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_70.visible=!this.oPgFrm.Page1.oPag.oBtn_1_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_83.visible=!this.oPgFrm.Page1.oPag.oStr_1_83.mHide()
    this.oPgFrm.Page1.oPag.oMVVOCCEN_1_88.visible=!this.oPgFrm.Page1.oPag.oMVVOCCEN_1_88.mHide()
    this.oPgFrm.Page1.oPag.oDESVOC_1_89.visible=!this.oPgFrm.Page1.oPag.oDESVOC_1_89.mHide()
    this.oPgFrm.Page1.oPag.oMVFLNOAN_1_90.visible=!this.oPgFrm.Page1.oPag.oMVFLNOAN_1_90.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    this.oPgFrm.Page2.oPag.oMVCAUCOL_2_15.visible=!this.oPgFrm.Page2.oPag.oMVCAUCOL_2_15.mHide()
    this.oPgFrm.Page2.oPag.oMVCODMAG_2_24.visible=!this.oPgFrm.Page2.oPag.oMVCODMAG_2_24.mHide()
    this.oPgFrm.Page2.oPag.oMVCODMAT_2_26.visible=!this.oPgFrm.Page2.oPag.oMVCODMAT_2_26.mHide()
    this.oPgFrm.Page2.oPag.oDESMAG_2_31.visible=!this.oPgFrm.Page2.oPag.oDESMAG_2_31.mHide()
    this.oPgFrm.Page2.oPag.oMVDATEVA_2_32.visible=!this.oPgFrm.Page2.oPag.oMVDATEVA_2_32.mHide()
    this.oPgFrm.Page2.oPag.oMVFLEVAS_2_33.visible=!this.oPgFrm.Page2.oPag.oMVFLEVAS_2_33.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_34.visible=!this.oPgFrm.Page2.oPag.oStr_2_34.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_37.visible=!this.oPgFrm.Page2.oPag.oStr_2_37.mHide()
    this.oPgFrm.Page2.oPag.oQTDISP_2_38.visible=!this.oPgFrm.Page2.oPag.oQTDISP_2_38.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_42.visible=!this.oPgFrm.Page2.oPag.oStr_2_42.mHide()
    this.oPgFrm.Page2.oPag.oMVEFFEVA_2_43.visible=!this.oPgFrm.Page2.oPag.oMVEFFEVA_2_43.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_44.visible=!this.oPgFrm.Page2.oPag.oStr_2_44.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_45.visible=!this.oPgFrm.Page2.oPag.oStr_2_45.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_47.visible=!this.oPgFrm.Page2.oPag.oStr_2_47.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    this.oPgFrm.Page2.oPag.oMVQTAEVA_2_49.visible=!this.oPgFrm.Page2.oPag.oMVQTAEVA_2_49.mHide()
    this.oPgFrm.Page2.oPag.oMVIMPEVA_2_50.visible=!this.oPgFrm.Page2.oPag.oMVIMPEVA_2_50.mHide()
    this.oPgFrm.Page2.oPag.oMVCODART_2_53.visible=!this.oPgFrm.Page2.oPag.oMVCODART_2_53.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_54.visible=!this.oPgFrm.Page2.oPag.oStr_2_54.mHide()
    this.oPgFrm.Page2.oPag.oDESART_2_55.visible=!this.oPgFrm.Page2.oPag.oDESART_2_55.mHide()
    this.oPgFrm.Page2.oPag.oMVQTAEV1_2_58.visible=!this.oPgFrm.Page2.oPag.oMVQTAEV1_2_58.mHide()
    this.oPgFrm.Page2.oPag.oUM1_2_59.visible=!this.oPgFrm.Page2.oPag.oUM1_2_59.mHide()
    this.oPgFrm.Page2.oPag.oUMR_2_60.visible=!this.oPgFrm.Page2.oPag.oUMR_2_60.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_61.visible=!this.oPgFrm.Page2.oPag.oStr_2_61.mHide()
    this.oPgFrm.Page2.oPag.oMVQTAIMP_2_62.visible=!this.oPgFrm.Page2.oPag.oMVQTAIMP_2_62.mHide()
    this.oPgFrm.Page2.oPag.oMVQTAIM1_2_63.visible=!this.oPgFrm.Page2.oPag.oMVQTAIM1_2_63.mHide()
    this.oPgFrm.Page2.oPag.oUM1_2_64.visible=!this.oPgFrm.Page2.oPag.oUM1_2_64.mHide()
    this.oPgFrm.Page2.oPag.oUMR_2_65.visible=!this.oPgFrm.Page2.oPag.oUMR_2_65.mHide()
    this.oPgFrm.Page2.oPag.oDESMAT_2_84.visible=!this.oPgFrm.Page2.oPag.oDESMAT_2_84.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_85.visible=!this.oPgFrm.Page2.oPag.oStr_2_85.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_86.visible=!this.oPgFrm.Page2.oPag.oStr_2_86.mHide()
    this.oPgFrm.Page2.oPag.oQ2DISP_2_87.visible=!this.oPgFrm.Page2.oPag.oQ2DISP_2_87.mHide()
    this.oPgFrm.Page2.oPag.oDESCA2_2_88.visible=!this.oPgFrm.Page2.oPag.oDESCA2_2_88.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_89.visible=!this.oPgFrm.Page2.oPag.oStr_2_89.mHide()
    this.oPgFrm.Page2.oPag.oMVDATOAI_2_94.visible=!this.oPgFrm.Page2.oPag.oMVDATOAI_2_94.mHide()
    this.oPgFrm.Page2.oPag.oMVDATOAF_2_95.visible=!this.oPgFrm.Page2.oPag.oMVDATOAF_2_95.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_96.visible=!this.oPgFrm.Page2.oPag.oStr_2_96.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_97.visible=!this.oPgFrm.Page2.oPag.oStr_2_97.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_98.visible=!this.oPgFrm.Page2.oPag.oStr_2_98.mHide()
    this.oPgFrm.Page2.oPag.oMVMC_PER_2_99.visible=!this.oPgFrm.Page2.oPag.oMVMC_PER_2_99.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_100.visible=!this.oPgFrm.Page2.oPag.oStr_2_100.mHide()
    this.oPgFrm.Page3.oPag.oMVPROORD_3_11.visible=!this.oPgFrm.Page3.oPag.oMVPROORD_3_11.mHide()
    this.oPgFrm.Page3.oPag.oMVNAZPRO_3_12.visible=!this.oPgFrm.Page3.oPag.oMVNAZPRO_3_12.mHide()
    this.oPgFrm.Page3.oPag.oMVAIRPOR_3_14.visible=!this.oPgFrm.Page3.oPag.oMVAIRPOR_3_14.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_16.visible=!this.oPgFrm.Page3.oPag.oStr_3_16.mHide()
    this.oPgFrm.Page3.oPag.oMVTIPPRO_3_21.visible=!this.oPgFrm.Page3.oPag.oMVTIPPRO_3_21.mHide()
    this.oPgFrm.Page3.oPag.oMVTIPPRO_3_22.visible=!this.oPgFrm.Page3.oPag.oMVTIPPRO_3_22.mHide()
    this.oPgFrm.Page3.oPag.oMVTIPPRO_3_23.visible=!this.oPgFrm.Page3.oPag.oMVTIPPRO_3_23.mHide()
    this.oPgFrm.Page3.oPag.oMVTIPPR2_3_29.visible=!this.oPgFrm.Page3.oPag.oMVTIPPR2_3_29.mHide()
    this.oPgFrm.Page3.oPag.oMVTIPPR2_3_30.visible=!this.oPgFrm.Page3.oPag.oMVTIPPR2_3_30.mHide()
    this.oPgFrm.Page3.oPag.oMVTIPPR2_3_31.visible=!this.oPgFrm.Page3.oPag.oMVTIPPR2_3_31.mHide()
    this.oPgFrm.Page3.oPag.oMVPROCAP_3_32.visible=!this.oPgFrm.Page3.oPag.oMVPROCAP_3_32.mHide()
    this.oPgFrm.Page3.oPag.oMVIMPCAP_3_33.visible=!this.oPgFrm.Page3.oPag.oMVIMPCAP_3_33.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_37.visible=!this.oPgFrm.Page3.oPag.oStr_3_37.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_38.visible=!this.oPgFrm.Page3.oPag.oStr_3_38.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_39.visible=!this.oPgFrm.Page3.oPag.oStr_3_39.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_51.visible=!this.oPgFrm.Page3.oPag.oStr_3_51.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_52.visible=!this.oPgFrm.Page3.oPag.oStr_3_52.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_53.visible=!this.oPgFrm.Page3.oPag.oStr_3_53.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_54.visible=!this.oPgFrm.Page3.oPag.oStr_3_54.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_55.visible=!this.oPgFrm.Page3.oPag.oStr_3_55.mHide()
    this.oPgFrm.Page3.oPag.oDESCRI_3_56.visible=!this.oPgFrm.Page3.oPag.oDESCRI_3_56.mHide()
    this.oPgFrm.Page5.oPag.oDESCCESP_5_3.visible=!this.oPgFrm.Page5.oPag.oDESCCESP_5_3.mHide()
    this.oPgFrm.Page5.oPag.oMVCODCES_5_7.visible=!this.oPgFrm.Page5.oPag.oMVCODCES_5_7.mHide()
    this.oPgFrm.Page5.oPag.oBtn_5_8.visible=!this.oPgFrm.Page5.oPag.oBtn_5_8.mHide()
    this.oPgFrm.Page5.oPag.oBtn_5_9.visible=!this.oPgFrm.Page5.oPag.oBtn_5_9.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_10.visible=!this.oPgFrm.Page5.oPag.oStr_5_10.mHide()
    this.oPgFrm.Page3.oPag.oMVPAEFOR_3_57.visible=!this.oPgFrm.Page3.oPag.oMVPAEFOR_3_57.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_58.visible=!this.oPgFrm.Page3.oPag.oStr_3_58.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_59.visible=!this.oPgFrm.Page3.oPag.oStr_3_59.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_60.visible=!this.oPgFrm.Page3.oPag.oStr_3_60.mHide()
    this.oPgFrm.Page3.oPag.oISONAZ_3_61.visible=!this.oPgFrm.Page3.oPag.oISONAZ_3_61.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_62.visible=!this.oPgFrm.Page3.oPag.oStr_3_62.mHide()
    this.oPgFrm.Page3.oPag.oISONA2_3_63.visible=!this.oPgFrm.Page3.oPag.oISONA2_3_63.mHide()
    this.oPgFrm.Page2.oPag.oMVRIFEDI_2_102.visible=!this.oPgFrm.Page2.oPag.oMVRIFEDI_2_102.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_103.visible=!this.oPgFrm.Page2.oPag.oStr_2_103.mHide()
    this.oPgFrm.Page1.oPag.oMVCACONT_1_99.visible=!this.oPgFrm.Page1.oPag.oMVCACONT_1_99.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_100.visible=!this.oPgFrm.Page1.oPag.oStr_1_100.mHide()
    this.oPgFrm.Page1.oPag.oCGDESCRI_1_101.visible=!this.oPgFrm.Page1.oPag.oCGDESCRI_1_101.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_102.visible=!this.oPgFrm.Page1.oPag.oStr_1_102.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_65.visible=!this.oPgFrm.Page3.oPag.oStr_3_65.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_83.Event(cEvent)
      .oPgFrm.Page5.oPag.oObj_5_5.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsor_kda
    *se i cespiti nn sono attivati nn lancia il batch gsce_bcz
    If cEvent='Init' And g_CESP='S'
     this.Notifyevent('Inizio')
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVCODCLA
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_MVCODCLA)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA,TRDESCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_MVCODCLA))
          select TRCODCLA,TRDESCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODCLA)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODCLA) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oMVCODCLA_1_19'),i_cWhere,'GSAR_ATR',"Tipologie di documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA,TRDESCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA,TRDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA,TRDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_MVCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_MVCODCLA)
            select TRCODCLA,TRDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODCLA = NVL(_Link_.TRCODCLA,space(3))
      this.w_DESCLA = NVL(_Link_.TRDESCLA,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODCLA = space(3)
      endif
      this.w_DESCLA = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCATCON
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_lTable = "CACOARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2], .t., this.CACOARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC1',True,'CACOARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_MVCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_MVCATCON))
          select C1CODICE,C1DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCATCON)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOARTI','*','C1CODICE',cp_AbsName(oSource.parent,'oMVCATCON_1_24'),i_cWhere,'GSAR_AC1',"Categorie contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_MVCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_MVCATCON)
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCATCON = NVL(_Link_.C1CODICE,space(5))
      this.w_DESCON = NVL(_Link_.C1DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MVCATCON = space(5)
      endif
      this.w_DESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCONTRO
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCONTRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MVCONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_MVCONTRO))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCONTRO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_MVCONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_MVCONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCONTRO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oMVCONTRO_1_26'),i_cWhere,'GSAR_BZC',"Conti contropartita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto contropartita non congruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCONTRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MVCONTRO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_MVCONTRO)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCONTRO = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCO2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANDTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVCONTRO = space(15)
      endif
      this.w_DESCO2 = space(40)
      this.w_ANDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_ANDTOBSO) OR .w_OBTEST<.w_ANDTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto contropartita non congruente")
        endif
        this.w_MVCONTRO = space(15)
        this.w_DESCO2 = space(40)
        this.w_ANDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCONTRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODIVA
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_MVCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_MVCODIVA))
          select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_MVCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_MVCODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oMVCODIVA_1_28'),i_cWhere,'GSAR_AIV',"Codici IVA",'GSCG_IVA.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVCODIVA)
            select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
      this.w_PERIND = NVL(_Link_.IVPERIND,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
      this.w_BOLIVA = NVL(_Link_.IVBOLIVA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_PERIVA = 0
      this.w_PERIND = 0
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_BOLIVA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA inesistente oppure obsoleto")
        endif
        this.w_MVCODIVA = space(5)
        this.w_DESIVA = space(35)
        this.w_PERIVA = 0
        this.w_PERIND = 0
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_BOLIVA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCONIND
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCONIND) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MVCONIND)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_MVCONIND))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCONIND)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_MVCONIND)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_MVCONIND)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCONIND) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oMVCONIND_1_33'),i_cWhere,'GSAR_BZC',"Conti contropartita IVA",'GSAR2API.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto contropartita IVA indetraibile non congruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCONIND)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MVCONIND);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_MVCONIND)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCONIND = NVL(_Link_.ANCODICE,space(15))
      this.w_DESIND = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSOT = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVCONIND = space(15)
      endif
      this.w_DESIND = space(40)
      this.w_TIPSOT = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPSOT<>'I' OR EMPTY(.w_MVCONIND)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto contropartita IVA indetraibile non congruente")
        endif
        this.w_MVCONIND = space(15)
        this.w_DESIND = space(40)
        this.w_TIPSOT = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCONIND Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCONTRA
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BCZ',True,'CON_TRAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CONUMERO like "+cp_ToStrODBC(trim(this.w_MVCONTRA)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_MVTIPCON);

          i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COTIPCLF,CONUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COTIPCLF',this.w_MVTIPCON;
                     ,'CONUMERO',trim(this.w_MVCONTRA))
          select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COTIPCLF,CONUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCONTRA)==trim(_Link_.CONUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCONTRA) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(oSource.parent,'oMVCONTRA_1_35'),i_cWhere,'GSAR_BCZ',"Elenco contratti",'GSVE1MDV.CON_TRAM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and COTIPCLF="+cp_ToStrODBC(this.w_MVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',oSource.xKey(1);
                       ,'CONUMERO',oSource.xKey(2))
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_MVCONTRA);
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_MVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',this.w_MVTIPCON;
                       ,'CONUMERO',this.w_MVCONTRA)
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCONTRA = NVL(_Link_.CONUMERO,space(15))
      this.w_DESCTR = NVL(_Link_.CODESCON,space(50))
      this.w_CT = NVL(_Link_.COTIPCLF,space(1))
      this.w_CC = NVL(_Link_.COCODCLF,space(15))
      this.w_CM = NVL(_Link_.COCATCOM,space(3))
      this.w_CI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_CF = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_CV = NVL(_Link_.COCODVAL,space(3))
      this.w_QUACON = NVL(_Link_.COQUANTI,space(1))
      this.w_IVACON = NVL(_Link_.COIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCONTRA = space(15)
      endif
      this.w_DESCTR = space(50)
      this.w_CT = space(1)
      this.w_CC = space(15)
      this.w_CM = space(3)
      this.w_CI = ctod("  /  /  ")
      this.w_CF = ctod("  /  /  ")
      this.w_CV = space(3)
      this.w_QUACON = space(1)
      this.w_IVACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCONTR(.w_MVCONTRA,.w_MVTIPCON,.w_XCONORN,.w_CATCOM,.w_MVFLSCOR,.w_MVCODVAL,.w_MVDATREG,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MVCONTRA = space(15)
        this.w_DESCTR = space(50)
        this.w_CT = space(1)
        this.w_CC = space(15)
        this.w_CM = space(3)
        this.w_CI = ctod("  /  /  ")
        this.w_CF = ctod("  /  /  ")
        this.w_CV = space(3)
        this.w_QUACON = space(1)
        this.w_IVACON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.COTIPCLF,1)+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCONTRA
  func Link_1_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BCZ',True,'CON_TRAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CONUMERO like "+cp_ToStrODBC(trim(this.w_MVCONTRA)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_MVTIPCON);

          i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COTIPCLF,CONUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COTIPCLF',this.w_MVTIPCON;
                     ,'CONUMERO',trim(this.w_MVCONTRA))
          select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COTIPCLF,CONUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCONTRA)==trim(_Link_.CONUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCONTRA) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(oSource.parent,'oMVCONTRA_1_67'),i_cWhere,'GSAR_BCZ',"Elenco contratti",'GSVE1MDV.CON_TRAM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and COTIPCLF="+cp_ToStrODBC(this.w_MVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',oSource.xKey(1);
                       ,'CONUMERO',oSource.xKey(2))
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_MVCONTRA);
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_MVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',this.w_MVTIPCON;
                       ,'CONUMERO',this.w_MVCONTRA)
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCONTRA = NVL(_Link_.CONUMERO,space(15))
      this.w_DESCTR = NVL(_Link_.CODESCON,space(50))
      this.w_CT = NVL(_Link_.COTIPCLF,space(1))
      this.w_CC = NVL(_Link_.COCODCLF,space(15))
      this.w_CM = NVL(_Link_.COCATCOM,space(3))
      this.w_CI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_CF = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_CV = NVL(_Link_.COCODVAL,space(3))
      this.w_QUACON = NVL(_Link_.COQUANTI,space(1))
      this.w_IVACON = NVL(_Link_.COIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCONTRA = space(15)
      endif
      this.w_DESCTR = space(50)
      this.w_CT = space(1)
      this.w_CC = space(15)
      this.w_CM = space(3)
      this.w_CI = ctod("  /  /  ")
      this.w_CF = ctod("  /  /  ")
      this.w_CV = space(3)
      this.w_QUACON = space(1)
      this.w_IVACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCONTR(.w_MVCONTRA,.w_MVTIPCON,.w_XCONORN,.w_CATCOM,.w_MVFLSCOR,.w_MVCODVAL,.w_MVDATREG,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MVCONTRA = space(15)
        this.w_DESCTR = space(50)
        this.w_CT = space(1)
        this.w_CC = space(15)
        this.w_CM = space(3)
        this.w_CI = ctod("  /  /  ")
        this.w_CF = ctod("  /  /  ")
        this.w_CV = space(3)
        this.w_QUACON = space(1)
        this.w_IVACON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.COTIPCLF,1)+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVVOCCEN
  func Link_1_88(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVVOCCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_MVVOCCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_MVVOCCEN))
          select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVVOCCEN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVVOCCEN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oMVVOCCEN_1_88'),i_cWhere,'GSCA_AVC',"Voci di costo o ricavo",'GSVE_KDA.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVVOCCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_MVVOCCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_MVVOCCEN)
            select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVVOCCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(5))
      this.w_VOCOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVVOCCEN = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_TIPVOC = space(1)
      this.w_CODCOS = space(5)
      this.w_VOCOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_MVVOCCEN) OR (CHKDTOBS(.w_VOCOBSO,.w_OBTEST,"Voce di Costo/Ricavo obsoleta!",.F.) and .w_TIPVOC=.w_VOCTIP)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di costo/ricavo incongruente o obsoleto")
        endif
        this.w_MVVOCCEN = space(15)
        this.w_DESVOC = space(40)
        this.w_TIPVOC = space(1)
        this.w_CODCOS = space(5)
        this.w_VOCOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVVOCCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODART
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARCATCES,ARFLCESP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MVCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MVCODART)
            select ARCODART,ARTIPART,ARCATCES,ARFLCESP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODART = NVL(_Link_.ARCODART,space(20))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_CATCES = NVL(_Link_.ARCATCES,space(15))
      this.w_FLCESP = NVL(_Link_.ARFLCESP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODART = space(20)
      endif
      this.w_TIPART = space(2)
      this.w_CATCES = space(15)
      this.w_FLCESP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCAUMAG
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCAUMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_MVCAUMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLAVAL,CMDTOBSO,CMFLCOMM,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_MVCAUMAG))
          select CMCODICE,CMDESCRI,CMFLAVAL,CMDTOBSO,CMFLCOMM,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCAUMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_MVCAUMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLAVAL,CMDTOBSO,CMFLCOMM,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_MVCAUMAG)+"%");

            select CMCODICE,CMDESCRI,CMFLAVAL,CMDTOBSO,CMFLCOMM,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCAUMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oMVCAUMAG_2_7'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLAVAL,CMDTOBSO,CMFLCOMM,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLAVAL,CMDTOBSO,CMFLCOMM,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCAUMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLAVAL,CMDTOBSO,CMFLCOMM,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MVCAUMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MVCAUMAG)
            select CMCODICE,CMDESCRI,CMFLAVAL,CMDTOBSO,CMFLCOMM,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCAUMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLAVA1 = NVL(_Link_.CMFLAVAL,space(1))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
      this.w_FLCOMM = NVL(_Link_.CMFLCOMM,space(1))
      this.w_FLCASC = NVL(_Link_.CMFLCASC,space(1))
      this.w_FLORDI = NVL(_Link_.CMFLORDI,space(1))
      this.w_FLIMPE = NVL(_Link_.CMFLIMPE,space(1))
      this.w_FLRISE = NVL(_Link_.CMFLRISE,space(1))
      this.w_MVCAUCOL = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MVCAUMAG = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_FLAVA1 = space(1)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_FLCOMM = space(1)
      this.w_FLCASC = space(1)
      this.w_FLORDI = space(1)
      this.w_FLIMPE = space(1)
      this.w_FLRISE = space(1)
      this.w_MVCAUCOL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCAUOR(.w_MVCAUMAG,.w_MVCAUCOL,.w_DTOBSO,.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MVCAUMAG = space(5)
        this.w_DESCAU = space(35)
        this.w_FLAVA1 = space(1)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_FLCOMM = space(1)
        this.w_FLCASC = space(1)
        this.w_FLORDI = space(1)
        this.w_FLIMPE = space(1)
        this.w_FLRISE = space(1)
        this.w_MVCAUCOL = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCAUMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCAUCOL
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCAUCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCAUCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MVCAUCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MVCAUCOL)
            select CMCODICE,CMDESCRI,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCAUCOL = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCA2 = NVL(_Link_.CMDESCRI,space(35))
      this.w_F2CASC = NVL(_Link_.CMFLCASC,space(1))
      this.w_F2ORDI = NVL(_Link_.CMFLORDI,space(1))
      this.w_F2IMPE = NVL(_Link_.CMFLIMPE,space(1))
      this.w_F2RISE = NVL(_Link_.CMFLRISE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCAUCOL = space(5)
      endif
      this.w_DESCA2 = space(35)
      this.w_F2CASC = space(1)
      this.w_F2ORDI = space(1)
      this.w_F2IMPE = space(1)
      this.w_F2RISE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCAUCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODMAG
  func Link_2_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MVCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MVCODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_MVCODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_MVCODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMVCODMAG_2_24'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MVCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MVCODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLUBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente o obsoleto")
        endif
        this.w_MVCODMAG = space(5)
        this.w_DESMAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLUBIC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READMAT
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_lTable = "SALDIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2], .t., this.SALDIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER";
                   +" from "+i_cTable+" "+i_lTable+" where SLCODMAG="+cp_ToStrODBC(this.w_READMAT);
                   +" and SLCODICE="+cp_ToStrODBC(this.w_MVKEYSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SLCODICE',this.w_MVKEYSAL;
                       ,'SLCODMAG',this.w_READMAT)
            select SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READMAT = NVL(_Link_.SLCODMAG,space(5))
      this.w_Q2APER = NVL(_Link_.SLQTAPER,0)
      this.w_Q2RPER = NVL(_Link_.SLQTRPER,0)
    else
      if i_cCtrl<>'Load'
        this.w_READMAT = space(5)
      endif
      this.w_Q2APER = 0
      this.w_Q2RPER = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])+'\'+cp_ToStr(_Link_.SLCODICE,1)+'\'+cp_ToStr(_Link_.SLCODMAG,1)
      cp_ShowWarn(i_cKey,this.SALDIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODMAT
  func Link_2_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MVCODMAT)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MVCODMAT))
          select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODMAT)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_MVCODMAT)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_MVCODMAT)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCODMAT) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMVCODMAT_2_26'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MVCODMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MVCODMAT)
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODMAT = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAT = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
      this.w_F2UBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODMAT = space(5)
      endif
      this.w_DESMAT = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_F2UBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente o obsoleto")
        endif
        this.w_MVCODMAT = space(5)
        this.w_DESMAT = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_F2UBIC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READMAG
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_lTable = "SALDIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2], .t., this.SALDIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER";
                   +" from "+i_cTable+" "+i_lTable+" where SLCODMAG="+cp_ToStrODBC(this.w_READMAG);
                   +" and SLCODICE="+cp_ToStrODBC(this.w_MVKEYSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SLCODICE',this.w_MVKEYSAL;
                       ,'SLCODMAG',this.w_READMAG)
            select SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READMAG = NVL(_Link_.SLCODMAG,space(5))
      this.w_QTAPER = NVL(_Link_.SLQTAPER,0)
      this.w_QTRPER = NVL(_Link_.SLQTRPER,0)
    else
      if i_cCtrl<>'Load'
        this.w_READMAG = space(5)
      endif
      this.w_QTAPER = 0
      this.w_QTRPER = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])+'\'+cp_ToStr(_Link_.SLCODICE,1)+'\'+cp_ToStr(_Link_.SLCODMAG,1)
      cp_ShowWarn(i_cKey,this.SALDIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODART
  func Link_2_53(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MVCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MVCODART)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODART = space(20)
      endif
      this.w_DESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVNOMENC
  func Link_3_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
    i_lTable = "NOMENCLA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2], .t., this.NOMENCLA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVNOMENC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANM',True,'NOMENCLA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NMCODICE like "+cp_ToStrODBC(trim(this.w_MVNOMENC)+"%");

          i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI,NMUNISUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NMCODICE',trim(this.w_MVNOMENC))
          select NMCODICE,NMDESCRI,NMUNISUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVNOMENC)==trim(_Link_.NMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVNOMENC) and !this.bDontReportError
            deferred_cp_zoom('NOMENCLA','*','NMCODICE',cp_AbsName(oSource.parent,'oMVNOMENC_3_2'),i_cWhere,'GSAR_ANM',"Nomenclature",'GSMA_AZN.NOMENCLA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI,NMUNISUP";
                     +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',oSource.xKey(1))
            select NMCODICE,NMDESCRI,NMUNISUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVNOMENC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI,NMUNISUP";
                   +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(this.w_MVNOMENC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',this.w_MVNOMENC)
            select NMCODICE,NMDESCRI,NMUNISUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVNOMENC = NVL(_Link_.NMCODICE,space(8))
      this.w_DESNOM = NVL(_Link_.NMDESCRI,space(35))
      this.w_UMSUPP = NVL(_Link_.NMUNISUP,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MVNOMENC = space(8)
      endif
      this.w_DESNOM = space(35)
      this.w_UMSUPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARUTISER='N' OR LEN(ALLTRIM(.w_MVNOMENC))=6 OR LEN(ALLTRIM(.w_MVNOMENC))=5
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MVNOMENC = space(8)
        this.w_DESNOM = space(35)
        this.w_UMSUPP = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])+'\'+cp_ToStr(_Link_.NMCODICE,1)
      cp_ShowWarn(i_cKey,this.NOMENCLA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVNOMENC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVUMSUPP
  func Link_3_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVUMSUPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVUMSUPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_MVUMSUPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_MVUMSUPP)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVUMSUPP = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MVUMSUPP = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVUMSUPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVNAZPRO
  func Link_3_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVNAZPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_MVNAZPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_MVNAZPRO))
          select NACODNAZ,NACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVNAZPRO)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVNAZPRO) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oMVNAZPRO_3_12'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVNAZPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_MVNAZPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_MVNAZPRO)
            select NACODNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVNAZPRO = NVL(_Link_.NACODNAZ,space(3))
      this.w_ISONAZ = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MVNAZPRO = space(3)
      endif
      this.w_ISONAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVNAZPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVAIRPOR
  func Link_3_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_AREO_IDX,3]
    i_lTable = "COD_AREO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2], .t., this.COD_AREO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVAIRPOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GLES_APP',True,'COD_AREO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PPCODICE like "+cp_ToStrODBC(trim(this.w_MVAIRPOR)+"%");

          i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PPCODICE',trim(this.w_MVAIRPOR))
          select PPCODICE,PPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVAIRPOR)==trim(_Link_.PPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVAIRPOR) and !this.bDontReportError
            deferred_cp_zoom('COD_AREO','*','PPCODICE',cp_AbsName(oSource.parent,'oMVAIRPOR_3_14'),i_cWhere,'GLES_APP',"Porti/aeroporti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',oSource.xKey(1))
            select PPCODICE,PPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVAIRPOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_MVAIRPOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_MVAIRPOR)
            select PPCODICE,PPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVAIRPOR = NVL(_Link_.PPCODICE,space(10))
      this.w_DESCRI = NVL(_Link_.PPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MVAIRPOR = space(10)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_AREO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVAIRPOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCONF
  func Link_3_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPICONF_IDX,3]
    i_lTable = "TIPICONF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2], .t., this.TIPICONF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCONF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCONF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI"+cp_TransInsFldName("TCDESCRI")+",TCFLCOVA";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CODCONF);
                   +" and TCCODICE="+cp_ToStrODBC(this.w_CODCONF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CODCONF;
                       ,'TCCODICE',this.w_CODCONF)
            select TCCODICE,TCDESCRI,TCFLCOVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCONF = NVL(_Link_.TCCODICE,space(3))
      this.w_DESCONF = NVL(cp_TransLoadField('_Link_.TCDESCRI'),space(35))
      this.w_FLCOVA = NVL(_Link_.TCFLCOVA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCONF = space(3)
      endif
      this.w_DESCONF = space(35)
      this.w_FLCOVA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPICONF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCONF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODCES
  func Link_5_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_MVCODCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CESTABEN,CETIPCES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_MVCODCES))
          select CECODICE,CEDESCRI,CESTABEN,CETIPCES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODCES)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODCES) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oMVCODCES_5_7'),i_cWhere,'',"Cespiti",'GSVE_KCE.CES_PITI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CESTABEN,CETIPCES";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CESTABEN,CETIPCES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CESTABEN,CETIPCES";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_MVCODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_MVCODCES)
            select CECODICE,CEDESCRI,CESTABEN,CETIPCES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODCES = NVL(_Link_.CECODICE,space(20))
      this.w_DESCCESP = NVL(_Link_.CEDESCRI,space(40))
      this.w_STABEN = NVL(_Link_.CESTABEN,space(1))
      this.w_CETIPO = NVL(_Link_.CETIPCES,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODCES = space(20)
      endif
      this.w_DESCCESP = space(40)
      this.w_STABEN = space(1)
      this.w_CETIPO = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF(EMPTY(.w_CETIPO),SPACE(2),IIF(.w_CETIPO='CQ','FM','FO'))=.w_TIPART OR EMPTY(.w_CETIPO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cespite inesistente o di tipo non valido")
        endif
        this.w_MVCODCES = space(20)
        this.w_DESCCESP = space(40)
        this.w_STABEN = space(1)
        this.w_CETIPO = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVPAEFOR
  func Link_3_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVPAEFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_MVPAEFOR)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_MVPAEFOR))
          select NACODNAZ,NACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVPAEFOR)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVPAEFOR) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oMVPAEFOR_3_57'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVPAEFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_MVPAEFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_MVPAEFOR)
            select NACODNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVPAEFOR = NVL(_Link_.NACODNAZ,space(3))
      this.w_ISONA2 = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MVPAEFOR = space(3)
      endif
      this.w_ISONA2 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVPAEFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCACONT
  func Link_1_99(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATMCONT_IDX,3]
    i_lTable = "CATMCONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2], .t., this.CATMCONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCACONT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MCT',True,'CATMCONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CGCODICE like "+cp_ToStrODBC(trim(this.w_MVCACONT)+"%");

          i_ret=cp_SQL(i_nConn,"select CGCODICE,CGDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CGCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CGCODICE',trim(this.w_MVCACONT))
          select CGCODICE,CGDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CGCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCACONT)==trim(_Link_.CGCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCACONT) and !this.bDontReportError
            deferred_cp_zoom('CATMCONT','*','CGCODICE',cp_AbsName(oSource.parent,'oMVCACONT_1_99'),i_cWhere,'GSAR_MCT',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCODICE,CGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CGCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CGCODICE',oSource.xKey(1))
            select CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCACONT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCODICE,CGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CGCODICE="+cp_ToStrODBC(this.w_MVCACONT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CGCODICE',this.w_MVCACONT)
            select CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCACONT = NVL(_Link_.CGCODICE,space(5))
      this.w_CGDESCRI = NVL(_Link_.CGDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MVCACONT = space(5)
      endif
      this.w_CGDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])+'\'+cp_ToStr(_Link_.CGCODICE,1)
      cp_ShowWarn(i_cKey,this.CATMCONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCACONT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMVCODCLA_1_19.value==this.w_MVCODCLA)
      this.oPgFrm.Page1.oPag.oMVCODCLA_1_19.value=this.w_MVCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_1_20.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_1_20.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFLRAGG_1_21.value==this.w_MVFLRAGG)
      this.oPgFrm.Page1.oPag.oMVFLRAGG_1_21.value=this.w_MVFLRAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oMVIMPNAZ_1_23.value==this.w_MVIMPNAZ)
      this.oPgFrm.Page1.oPag.oMVIMPNAZ_1_23.value=this.w_MVIMPNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCATCON_1_24.value==this.w_MVCATCON)
      this.oPgFrm.Page1.oPag.oMVCATCON_1_24.value=this.w_MVCATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_25.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_25.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCONTRO_1_26.value==this.w_MVCONTRO)
      this.oPgFrm.Page1.oPag.oMVCONTRO_1_26.value=this.w_MVCONTRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCO2_1_27.value==this.w_DESCO2)
      this.oPgFrm.Page1.oPag.oDESCO2_1_27.value=this.w_DESCO2
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODIVA_1_28.value==this.w_MVCODIVA)
      this.oPgFrm.Page1.oPag.oMVCODIVA_1_28.value=this.w_MVCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_30.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_30.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCONIND_1_33.value==this.w_MVCONIND)
      this.oPgFrm.Page1.oPag.oMVCONIND_1_33.value=this.w_MVCONIND
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIND_1_34.value==this.w_DESIND)
      this.oPgFrm.Page1.oPag.oDESIND_1_34.value=this.w_DESIND
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCONTRA_1_35.value==this.w_MVCONTRA)
      this.oPgFrm.Page1.oPag.oMVCONTRA_1_35.value=this.w_MVCONTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCTR_1_36.value==this.w_DESCTR)
      this.oPgFrm.Page1.oPag.oDESCTR_1_36.value=this.w_DESCTR
    endif
    if not(this.oPgFrm.Page1.oPag.oMVINICOM_1_63.value==this.w_MVINICOM)
      this.oPgFrm.Page1.oPag.oMVINICOM_1_63.value=this.w_MVINICOM
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFINCOM_1_64.value==this.w_MVFINCOM)
      this.oPgFrm.Page1.oPag.oMVFINCOM_1_64.value=this.w_MVFINCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCONTRA_1_67.value==this.w_MVCONTRA)
      this.oPgFrm.Page1.oPag.oMVCONTRA_1_67.value=this.w_MVCONTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCTR_1_68.value==this.w_DESCTR)
      this.oPgFrm.Page1.oPag.oDESCTR_1_68.value=this.w_DESCTR
    endif
    if not(this.oPgFrm.Page1.oPag.oMVVOCCEN_1_88.value==this.w_MVVOCCEN)
      this.oPgFrm.Page1.oPag.oMVVOCCEN_1_88.value=this.w_MVVOCCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVOC_1_89.value==this.w_DESVOC)
      this.oPgFrm.Page1.oPag.oDESVOC_1_89.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFLNOAN_1_90.RadioValue()==this.w_MVFLNOAN)
      this.oPgFrm.Page1.oPag.oMVFLNOAN_1_90.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMVCAUMAG_2_7.value==this.w_MVCAUMAG)
      this.oPgFrm.Page2.oPag.oMVCAUMAG_2_7.value=this.w_MVCAUMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_2_9.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_2_9.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oMVCAUCOL_2_15.value==this.w_MVCAUCOL)
      this.oPgFrm.Page2.oPag.oMVCAUCOL_2_15.value=this.w_MVCAUCOL
    endif
    if not(this.oPgFrm.Page2.oPag.oMVFLELGM_2_23.RadioValue()==this.w_MVFLELGM)
      this.oPgFrm.Page2.oPag.oMVFLELGM_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMVCODMAG_2_24.value==this.w_MVCODMAG)
      this.oPgFrm.Page2.oPag.oMVCODMAG_2_24.value=this.w_MVCODMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oMVCODMAT_2_26.value==this.w_MVCODMAT)
      this.oPgFrm.Page2.oPag.oMVCODMAT_2_26.value=this.w_MVCODMAT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAG_2_31.value==this.w_DESMAG)
      this.oPgFrm.Page2.oPag.oDESMAG_2_31.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oMVDATEVA_2_32.value==this.w_MVDATEVA)
      this.oPgFrm.Page2.oPag.oMVDATEVA_2_32.value=this.w_MVDATEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oMVFLEVAS_2_33.RadioValue()==this.w_MVFLEVAS)
      this.oPgFrm.Page2.oPag.oMVFLEVAS_2_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oQTDISP_2_38.value==this.w_QTDISP)
      this.oPgFrm.Page2.oPag.oQTDISP_2_38.value=this.w_QTDISP
    endif
    if not(this.oPgFrm.Page2.oPag.oMVEFFEVA_2_43.value==this.w_MVEFFEVA)
      this.oPgFrm.Page2.oPag.oMVEFFEVA_2_43.value=this.w_MVEFFEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oMVQTAEVA_2_49.value==this.w_MVQTAEVA)
      this.oPgFrm.Page2.oPag.oMVQTAEVA_2_49.value=this.w_MVQTAEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oMVIMPEVA_2_50.value==this.w_MVIMPEVA)
      this.oPgFrm.Page2.oPag.oMVIMPEVA_2_50.value=this.w_MVIMPEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oMVCODART_2_53.value==this.w_MVCODART)
      this.oPgFrm.Page2.oPag.oMVCODART_2_53.value=this.w_MVCODART
    endif
    if not(this.oPgFrm.Page2.oPag.oDESART_2_55.value==this.w_DESART)
      this.oPgFrm.Page2.oPag.oDESART_2_55.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page2.oPag.oMVQTAEV1_2_58.value==this.w_MVQTAEV1)
      this.oPgFrm.Page2.oPag.oMVQTAEV1_2_58.value=this.w_MVQTAEV1
    endif
    if not(this.oPgFrm.Page2.oPag.oUM1_2_59.value==this.w_UM1)
      this.oPgFrm.Page2.oPag.oUM1_2_59.value=this.w_UM1
    endif
    if not(this.oPgFrm.Page2.oPag.oUMR_2_60.value==this.w_UMR)
      this.oPgFrm.Page2.oPag.oUMR_2_60.value=this.w_UMR
    endif
    if not(this.oPgFrm.Page2.oPag.oMVQTAIMP_2_62.value==this.w_MVQTAIMP)
      this.oPgFrm.Page2.oPag.oMVQTAIMP_2_62.value=this.w_MVQTAIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oMVQTAIM1_2_63.value==this.w_MVQTAIM1)
      this.oPgFrm.Page2.oPag.oMVQTAIM1_2_63.value=this.w_MVQTAIM1
    endif
    if not(this.oPgFrm.Page2.oPag.oUM1_2_64.value==this.w_UM1)
      this.oPgFrm.Page2.oPag.oUM1_2_64.value=this.w_UM1
    endif
    if not(this.oPgFrm.Page2.oPag.oUMR_2_65.value==this.w_UMR)
      this.oPgFrm.Page2.oPag.oUMR_2_65.value=this.w_UMR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAT_2_84.value==this.w_DESMAT)
      this.oPgFrm.Page2.oPag.oDESMAT_2_84.value=this.w_DESMAT
    endif
    if not(this.oPgFrm.Page2.oPag.oQ2DISP_2_87.value==this.w_Q2DISP)
      this.oPgFrm.Page2.oPag.oQ2DISP_2_87.value=this.w_Q2DISP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCA2_2_88.value==this.w_DESCA2)
      this.oPgFrm.Page2.oPag.oDESCA2_2_88.value=this.w_DESCA2
    endif
    if not(this.oPgFrm.Page2.oPag.oMVDATOAI_2_94.value==this.w_MVDATOAI)
      this.oPgFrm.Page2.oPag.oMVDATOAI_2_94.value=this.w_MVDATOAI
    endif
    if not(this.oPgFrm.Page2.oPag.oMVDATOAF_2_95.value==this.w_MVDATOAF)
      this.oPgFrm.Page2.oPag.oMVDATOAF_2_95.value=this.w_MVDATOAF
    endif
    if not(this.oPgFrm.Page2.oPag.oMVMC_PER_2_99.RadioValue()==this.w_MVMC_PER)
      this.oPgFrm.Page2.oPag.oMVMC_PER_2_99.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oMVFLTRAS_3_1.RadioValue()==this.w_MVFLTRAS)
      this.oPgFrm.Page3.oPag.oMVFLTRAS_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oMVNOMENC_3_2.value==this.w_MVNOMENC)
      this.oPgFrm.Page3.oPag.oMVNOMENC_3_2.value=this.w_MVNOMENC
    endif
    if not(this.oPgFrm.Page3.oPag.oDESNOM_3_3.value==this.w_DESNOM)
      this.oPgFrm.Page3.oPag.oDESNOM_3_3.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page3.oPag.oUMSUPP_3_5.value==this.w_UMSUPP)
      this.oPgFrm.Page3.oPag.oUMSUPP_3_5.value=this.w_UMSUPP
    endif
    if not(this.oPgFrm.Page3.oPag.oMVMOLSUP_3_7.value==this.w_MVMOLSUP)
      this.oPgFrm.Page3.oPag.oMVMOLSUP_3_7.value=this.w_MVMOLSUP
    endif
    if not(this.oPgFrm.Page3.oPag.oUM1_3_8.value==this.w_UM1)
      this.oPgFrm.Page3.oPag.oUM1_3_8.value=this.w_UM1
    endif
    if not(this.oPgFrm.Page3.oPag.oMVPESNET_3_9.value==this.w_MVPESNET)
      this.oPgFrm.Page3.oPag.oMVPESNET_3_9.value=this.w_MVPESNET
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTMASNE_3_10.value==this.w_TOTMASNE)
      this.oPgFrm.Page3.oPag.oTOTMASNE_3_10.value=this.w_TOTMASNE
    endif
    if not(this.oPgFrm.Page3.oPag.oMVPROORD_3_11.value==this.w_MVPROORD)
      this.oPgFrm.Page3.oPag.oMVPROORD_3_11.value=this.w_MVPROORD
    endif
    if not(this.oPgFrm.Page3.oPag.oMVNAZPRO_3_12.value==this.w_MVNAZPRO)
      this.oPgFrm.Page3.oPag.oMVNAZPRO_3_12.value=this.w_MVNAZPRO
    endif
    if not(this.oPgFrm.Page3.oPag.oMVAIRPOR_3_14.value==this.w_MVAIRPOR)
      this.oPgFrm.Page3.oPag.oMVAIRPOR_3_14.value=this.w_MVAIRPOR
    endif
    if not(this.oPgFrm.Page3.oPag.oMVTIPPRO_3_21.RadioValue()==this.w_MVTIPPRO)
      this.oPgFrm.Page3.oPag.oMVTIPPRO_3_21.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oMVTIPPRO_3_22.RadioValue()==this.w_MVTIPPRO)
      this.oPgFrm.Page3.oPag.oMVTIPPRO_3_22.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oMVTIPPRO_3_23.RadioValue()==this.w_MVTIPPRO)
      this.oPgFrm.Page3.oPag.oMVTIPPRO_3_23.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oMVPERPRO_3_27.value==this.w_MVPERPRO)
      this.oPgFrm.Page3.oPag.oMVPERPRO_3_27.value=this.w_MVPERPRO
    endif
    if not(this.oPgFrm.Page3.oPag.oMVIMPPRO_3_28.value==this.w_MVIMPPRO)
      this.oPgFrm.Page3.oPag.oMVIMPPRO_3_28.value=this.w_MVIMPPRO
    endif
    if not(this.oPgFrm.Page3.oPag.oMVTIPPR2_3_29.RadioValue()==this.w_MVTIPPR2)
      this.oPgFrm.Page3.oPag.oMVTIPPR2_3_29.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oMVTIPPR2_3_30.RadioValue()==this.w_MVTIPPR2)
      this.oPgFrm.Page3.oPag.oMVTIPPR2_3_30.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oMVTIPPR2_3_31.RadioValue()==this.w_MVTIPPR2)
      this.oPgFrm.Page3.oPag.oMVTIPPR2_3_31.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oMVPROCAP_3_32.value==this.w_MVPROCAP)
      this.oPgFrm.Page3.oPag.oMVPROCAP_3_32.value=this.w_MVPROCAP
    endif
    if not(this.oPgFrm.Page3.oPag.oMVIMPCAP_3_33.value==this.w_MVIMPCAP)
      this.oPgFrm.Page3.oPag.oMVIMPCAP_3_33.value=this.w_MVIMPCAP
    endif
    if not(this.oPgFrm.Page3.oPag.oMVNUMCOL_3_43.value==this.w_MVNUMCOL)
      this.oPgFrm.Page3.oPag.oMVNUMCOL_3_43.value=this.w_MVNUMCOL
    endif
    if not(this.oPgFrm.Page3.oPag.oCODCONF_3_44.value==this.w_CODCONF)
      this.oPgFrm.Page3.oPag.oCODCONF_3_44.value=this.w_CODCONF
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCONF_3_46.value==this.w_DESCONF)
      this.oPgFrm.Page3.oPag.oDESCONF_3_46.value=this.w_DESCONF
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI_3_56.value==this.w_DESCRI)
      this.oPgFrm.Page3.oPag.oDESCRI_3_56.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page4.oPag.oMVDESSUP_4_2.value==this.w_MVDESSUP)
      this.oPgFrm.Page4.oPag.oMVDESSUP_4_2.value=this.w_MVDESSUP
    endif
    if not(this.oPgFrm.Page5.oPag.oDESCCESP_5_3.value==this.w_DESCCESP)
      this.oPgFrm.Page5.oPag.oDESCCESP_5_3.value=this.w_DESCCESP
    endif
    if not(this.oPgFrm.Page5.oPag.oMVCODCES_5_7.value==this.w_MVCODCES)
      this.oPgFrm.Page5.oPag.oMVCODCES_5_7.value=this.w_MVCODCES
    endif
    if not(this.oPgFrm.Page3.oPag.oMVPAEFOR_3_57.value==this.w_MVPAEFOR)
      this.oPgFrm.Page3.oPag.oMVPAEFOR_3_57.value=this.w_MVPAEFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oISONAZ_3_61.value==this.w_ISONAZ)
      this.oPgFrm.Page3.oPag.oISONAZ_3_61.value=this.w_ISONAZ
    endif
    if not(this.oPgFrm.Page3.oPag.oISONA2_3_63.value==this.w_ISONA2)
      this.oPgFrm.Page3.oPag.oISONA2_3_63.value=this.w_ISONA2
    endif
    if not(this.oPgFrm.Page2.oPag.oMVRIFEDI_2_102.value==this.w_MVRIFEDI)
      this.oPgFrm.Page2.oPag.oMVRIFEDI_2_102.value=this.w_MVRIFEDI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCACONT_1_99.value==this.w_MVCACONT)
      this.oPgFrm.Page1.oPag.oMVCACONT_1_99.value=this.w_MVCACONT
    endif
    if not(this.oPgFrm.Page1.oPag.oCGDESCRI_1_101.value==this.w_CGDESCRI)
      this.oPgFrm.Page1.oPag.oCGDESCRI_1_101.value=this.w_CGDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_ANDTOBSO) OR .w_OBTEST<.w_ANDTOBSO))  and (.w_MVTIPRIG $ 'RFMA')  and not(empty(.w_MVCONTRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCONTRO_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto contropartita non congruente")
          case   ((empty(.w_MVCODIVA)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_MVTIPRIG $ 'RFMA')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCODIVA_1_28.SetFocus()
            i_bnoObbl = !empty(.w_MVCODIVA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA inesistente oppure obsoleto")
          case   not((.w_TIPSOT<>'I' OR EMPTY(.w_MVCONIND)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PERIND=0)  and (.w_MVTIPRIG $ 'RFMA' AND .w_PERIND<>0)  and not(empty(.w_MVCONIND))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCONIND_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto contropartita IVA indetraibile non congruente")
          case   not(CHKCONTR(.w_MVCONTRA,.w_MVTIPCON,.w_XCONORN,.w_CATCOM,.w_MVFLSCOR,.w_MVCODVAL,.w_MVDATREG,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON))  and not(.w_MVTIPCON<>'F')  and (g_GESCON='S')  and not(empty(.w_MVCONTRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCONTRA_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_MVINICOM) OR .w_MVINICOM<=.w_MVFINCOM)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVFINCOM_1_64.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCONTR(.w_MVCONTRA,.w_MVTIPCON,.w_XCONORN,.w_CATCOM,.w_MVFLSCOR,.w_MVCODVAL,.w_MVDATREG,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON))  and not(.w_MVTIPCON='F')  and (g_GESCON='S')  and not(empty(.w_MVCONTRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCONTRA_1_67.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_MVVOCCEN) OR (CHKDTOBS(.w_VOCOBSO,.w_OBTEST,"Voce di Costo/Ricavo obsoleta!",.F.) and .w_TIPVOC=.w_VOCTIP))  and not(NOT (((g_PERCCR='S' AND .w_FLANAL='S') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D'))  and (((g_PERCCR='S' AND .w_FLANAL='S' and .w_MVFLNOAN='N') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D')  and not(empty(.w_MVVOCCEN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVVOCCEN_1_88.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di costo/ricavo incongruente o obsoleto")
          case   ((empty(.w_MVCAUMAG)) or not(CHKCAUOR(.w_MVCAUMAG,.w_MVCAUCOL,.w_DTOBSO,.w_OBTEST)))  and (.w_FLCCAU = 'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMVCAUMAG_2_7.SetFocus()
            i_bnoObbl = !empty(.w_MVCAUMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MVCODMAG)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_MVTIPRIG <> 'R' OR EMPTY(.w_AGGSAL))  and (.w_MVTIPRIG = 'R' AND NOT EMPTY(.w_AGGSAL) and Not .w_TESCOMP)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMVCODMAG_2_24.SetFocus()
            i_bnoObbl = !empty(.w_MVCODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente o obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))  and (NOT EMPTY(.w_MVCAUCOL) AND .w_MVTIPRIG = 'R' AND NOT EMPTY(.w_AGGSAL1) and Not .w_TESCOMP)  and not(empty(.w_MVCODMAT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMVCODMAT_2_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente o obsoleto")
          case   not(Not Empty(.w_MVDATOAI))  and not(.w_FLORDAPE <> 'S')  and (.w_FLORDAPE = 'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMVDATOAI_2_94.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare una data inizio validit�")
          case   (empty(.w_MVMC_PER))  and not(.w_FLORDAPE <> 'S')  and (.w_FLORDAPE = 'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMVMC_PER_2_99.SetFocus()
            i_bnoObbl = !empty(.w_MVMC_PER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ARUTISER='N' OR LEN(ALLTRIM(.w_MVNOMENC))=6 OR LEN(ALLTRIM(.w_MVNOMENC))=5)  and (.w_MVTIPRIG $ 'FRM' AND g_INTR='S')  and not(empty(.w_MVNOMENC))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMVNOMENC_3_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MVNUMCOL<=30000)  and (not empty(.w_CODCONF) and .w_FLCOVA='S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMVNUMCOL_3_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Superato massimo numero confezioni per riga. Valore massimo 30000.")
          case   not(IIF(EMPTY(.w_CETIPO),SPACE(2),IIF(.w_CETIPO='CQ','FM','FO'))=.w_TIPART OR EMPTY(.w_CETIPO))  and not(.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')  and ((.oparentobject.cfunction ='Load' OR .oparentobject.cfunction ='Edit'  )  and .w_CESPRES=.T.)  and not(empty(.w_MVCODCES))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oMVCODCES_5_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cespite inesistente o di tipo non valido")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsor_kda
      *--- Lancio ricalcolo data prossima fatturazione se ordine aperto
      If i_bRes And .w_FLORDAPE='S'
        *--- Salvo vecchi valori
         Local Old_MVDATEVA, Old_MVMC_PER
         Old_MVDATEVA = .oParentObject.w_MVDATEVA
         Old_MVMC_PER = .oParentObject.w_MVMC_PER
         *--- Aggiorno la var caller
         .oParentObject.w_MVDATEVA = .w_MVDATEVA
         .oParentObject.w_MVMC_PER = .w_MVMC_PER
         .oParentObject.NotifyEvent("CalcolaDataFat")
         *--- Valorizzo la variable locale per forzare l'aggiornamento al salvataggio
         .w_MVDATEVA=.oParentObject.w_MVDATEVA
         *--- Aggiorno il video
         .SetControlsValue()
      EndIf
      *--- Controllo data prossima fatturazione
      If i_bRes And .w_FLORDAPE='S' And !(.w_MVDATEVA>=.w_MVDATOAI AND (.w_MVDATEVA<=.w_MVDATOAF OR Empty(.w_MVDATOAF)))
         i_bRes=.F.
         Ah_ErrorMsg("La data di prossima fatturazione deve essere compresa tra le date di validit�")
         *--- Ripristino i vecchi valori
         .oParentObject.w_MVDATEVA = Old_MVDATEVA
         .oParentObject.w_MVMC_PER = Old_MVMC_PER
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVDATDOC = this.w_MVDATDOC
    this.o_MVCODIVA = this.w_MVCODIVA
    this.o_MVCAUMAG = this.w_MVCAUMAG
    this.o_MVCAUCOL = this.w_MVCAUCOL
    this.o_MVCODMAG = this.w_MVCODMAG
    this.o_MVCODMAT = this.w_MVCODMAT
    this.o_MVNOMENC = this.w_MVNOMENC
    this.o_MVAIRPOR = this.w_MVAIRPOR
    this.o_MVTIPPRO = this.w_MVTIPPRO
    this.o_MVPERPRO = this.w_MVPERPRO
    this.o_MVIMPPRO = this.w_MVIMPPRO
    this.o_MVTIPPR2 = this.w_MVTIPPR2
    this.o_MVPROCAP = this.w_MVPROCAP
    this.o_MVIMPCAP = this.w_MVIMPCAP
    this.o_MVCESSER = this.w_MVCESSER
    return

enddefine

* --- Define pages as container
define class tgsor_kdaPag1 as StdContainer
  Width  = 597
  height = 397
  stdWidth  = 597
  stdheight = 397
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVCODCLA_1_19 as StdField with uid="UUASRNIVXB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MVCODCLA", cQueryName = "MVCODCLA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia riga documento per eventuali filtri",;
    HelpContextID = 223731193,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=116, Top=14, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_MVCODCLA"

  func oMVCODCLA_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODCLA_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODCLA_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oMVCODCLA_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie di documenti",'',this.parent.oContained
  endproc
  proc oMVCODCLA_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_MVCODCLA
     i_obj.ecpSave()
  endproc

  add object oDESCLA_1_20 as StdField with uid="CXCILNAYHO",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 249622474,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=159, Top=14, InputMask=replicate('X',30)

  add object oMVFLRAGG_1_21 as StdField with uid="CKBKFXDAWL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MVFLRAGG", cQueryName = "MVFLRAGG",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 25645581,;
   bGlobalFont=.t.,;
    Height=21, Width=16, Left=521, Top=14, cSayPict="'9'", cGetPict="'9'", InputMask=replicate('X',1)

  func oMVFLRAGG_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGEFA='S')
    endwith
   endif
  endfunc

  add object oMVIMPNAZ_1_23 as StdField with uid="HPRPTIJRAE",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MVIMPNAZ", cQueryName = "MVIMPNAZ",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo totale di riga riferito alla valuta di conto (usato per le valorizzazioni)",;
    HelpContextID = 26705376,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=116, Top=42, cSayPict="v_PU(18)", cGetPict="v_GU(18)"

  func oMVIMPNAZ_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLELGM='S')
    endwith
   endif
  endfunc

  func oMVIMPNAZ_1_23.mHide()
    with this.Parent.oContained
      return (.w_MVFLELGM<>'S')
    endwith
  endfunc

  add object oMVCATCON_1_24 as StdField with uid="UFUFHYEYFQ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MVCATCON", cQueryName = "MVCATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contabile per contabilizzazione associata alla riga documento",;
    HelpContextID = 207871468,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=116, Top=70, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOARTI", cZoomOnZoom="GSAR_AC1", oKey_1_1="C1CODICE", oKey_1_2="this.w_MVCATCON"

  func oMVCATCON_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA')
    endwith
   endif
  endfunc

  func oMVCATCON_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCATCON_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCATCON_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOARTI','*','C1CODICE',cp_AbsName(this.parent,'oMVCATCON_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC1',"Categorie contabili",'',this.parent.oContained
  endproc
  proc oMVCATCON_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_MVCATCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_25 as StdField with uid="IOZBOOAWTD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 28372938,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=179, Top=70, InputMask=replicate('X',35)

  add object oMVCONTRO_1_26 as StdField with uid="BIPOAFEUST",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MVCONTRO", cQueryName = "MVCONTRO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto contropartita non congruente",;
    ToolTipText = "Contropartita di contabilizzazione riga documento (vuoto: prende cat.contabile)",;
    HelpContextID = 196468203,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=116, Top=126, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_MVCONTRO"

  func oMVCONTRO_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA')
    endwith
   endif
  endfunc

  func oMVCONTRO_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCONTRO_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCONTRO_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oMVCONTRO_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'',this.parent.oContained
  endproc
  proc oMVCONTRO_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_MVCONTRO
     i_obj.ecpSave()
  endproc

  add object oDESCO2_1_27 as StdField with uid="EBKKPYVSLX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCO2", cQueryName = "DESCO2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 229699530,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=248, Top=126, InputMask=replicate('X',40)

  add object oMVCODIVA_1_28 as StdField with uid="HTLQYWDDQA",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MVCODIVA", cQueryName = "MVCODIVA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA inesistente oppure obsoleto",;
    ToolTipText = "Codice IVA articolo (utilizzato in caso di documento non esente)",;
    HelpContextID = 145367559,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=116, Top=98, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVCODIVA"

  func oMVCODIVA_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA')
    endwith
   endif
  endfunc

  func oMVCODIVA_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODIVA_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODIVA_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oMVCODIVA_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'GSCG_IVA.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oMVCODIVA_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_MVCODIVA
     i_obj.ecpSave()
  endproc


  add object oBtn_1_29 as StdButton with uid="MBAYGNIOMQ",left=489, top=70, width=48,height=45,;
    CpPicture="bmp\compone.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la combinazione categoria codice IVA applicata";
    , HelpContextID = 267378481;
    , Caption='\<Combin.IVA';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSVE_BFC(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MVTIPRIG <> 'D' and Empty( .w_MVCACONT ))
      endwith
    endif
  endfunc

  func oBtn_1_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_MVTIPRIG = 'D' or not Empty( .w_MVCACONT ))
     endwith
    endif
  endfunc

  add object oDESIVA_1_30 as StdField with uid="GBMBBGCKYW",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 238743498,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=179, Top=98, InputMask=replicate('X',35)

  add object oMVCONIND_1_33 as StdField with uid="BHNKDGJAIL",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MVCONIND", cQueryName = "MVCONIND",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto contropartita IVA indetraibile non congruente",;
    ToolTipText = "Contropartita IVA indetraibile di contabilizzazione riga",;
    HelpContextID = 112582134,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=116, Top=154, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_MVCONIND"

  func oMVCONIND_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA' AND .w_PERIND<>0)
    endwith
   endif
  endfunc

  func oMVCONIND_1_33.mHide()
    with this.Parent.oContained
      return (.w_PERIND=0)
    endwith
  endfunc

  func oMVCONIND_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCONIND_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCONIND_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oMVCONIND_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita IVA",'GSAR2API.CONTI_VZM',this.parent.oContained
  endproc
  proc oMVCONIND_1_33.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_MVCONIND
     i_obj.ecpSave()
  endproc

  add object oDESIND_1_34 as StdField with uid="QSNLZGDKUT",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESIND", cQueryName = "DESIND",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 196800458,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=248, Top=154, InputMask=replicate('X',40)

  func oDESIND_1_34.mHide()
    with this.Parent.oContained
      return (.w_PERIND=0)
    endwith
  endfunc

  add object oMVCONTRA_1_35 as StdField with uid="YYXXAPUYPL",rtseq=34,rtrep=.f.,;
    cFormVar = "w_MVCONTRA", cQueryName = "MVCONTRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice del contratto applicato",;
    HelpContextID = 196468217,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=116, Top=182, InputMask=replicate('X',15), bHasZoom = .t. , visible=iif(g_GESCON='S', .T., .F.), cLinkFile="CON_TRAM", cZoomOnZoom="GSAR_BCZ", oKey_1_1="COTIPCLF", oKey_1_2="this.w_MVTIPCON", oKey_2_1="CONUMERO", oKey_2_2="this.w_MVCONTRA"

  func oMVCONTRA_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GESCON='S')
    endwith
   endif
  endfunc

  func oMVCONTRA_1_35.mHide()
    with this.Parent.oContained
      return (.w_MVTIPCON<>'F')
    endwith
  endfunc

  func oMVCONTRA_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCONTRA_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCONTRA_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_TRAM_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStrODBC(this.Parent.oContained.w_MVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStr(this.Parent.oContained.w_MVTIPCON)
    endif
    do cp_zoom with 'CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(this.parent,'oMVCONTRA_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BCZ',"Elenco contratti",'GSVE1MDV.CON_TRAM_VZM',this.parent.oContained
  endproc
  proc oMVCONTRA_1_35.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BCZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.COTIPCLF=w_MVTIPCON
     i_obj.w_CONUMERO=this.parent.oContained.w_MVCONTRA
     i_obj.ecpSave()
  endproc

  add object oDESCTR_1_36 as StdField with uid="GCIYGWWSRU",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESCTR", cQueryName = "DESCTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 224456650,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=248, Top=182, InputMask=replicate('X',50), visible=iif(g_GESCON='S', .T., .F.)

  func oDESCTR_1_36.mHide()
    with this.Parent.oContained
      return (.w_MVTIPCON<>'F')
    endwith
  endfunc


  add object oObj_1_54 as cp_runprogram with uid="GXZKIBROUM",left=471, top=413, width=134,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSVE_BFC',;
    cEvent = "Update end",;
    nPag=1;
    , HelpContextID = 172843546

  add object oMVINICOM_1_63 as StdField with uid="TMGYWULPGF",rtseq=52,rtrep=.f.,;
    cFormVar = "w_MVINICOM", cQueryName = "MVINICOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio competenza contabile",;
    HelpContextID = 218529261,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=116, Top=210

  add object oMVFINCOM_1_64 as StdField with uid="LHIVBKPYHJ",rtseq=53,rtrep=.f.,;
    cFormVar = "w_MVFINCOM", cQueryName = "MVFINCOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine competenza contabile",;
    HelpContextID = 213626349,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=248, Top=210

  func oMVFINCOM_1_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_MVINICOM) OR .w_MVINICOM<=.w_MVFINCOM)
    endwith
    return bRes
  endfunc

  add object oMVCONTRA_1_67 as StdField with uid="FMQFNPCRSH",rtseq=55,rtrep=.f.,;
    cFormVar = "w_MVCONTRA", cQueryName = "MVCONTRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice del contratto applicato",;
    HelpContextID = 196468217,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=116, Top=182, InputMask=replicate('X',15), bHasZoom = .t. , visible=iif(g_GESCON='S', .T., .F.), cLinkFile="CON_TRAM", cZoomOnZoom="GSAR_BCZ", oKey_1_1="COTIPCLF", oKey_1_2="this.w_MVTIPCON", oKey_2_1="CONUMERO", oKey_2_2="this.w_MVCONTRA"

  func oMVCONTRA_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GESCON='S')
    endwith
   endif
  endfunc

  func oMVCONTRA_1_67.mHide()
    with this.Parent.oContained
      return (.w_MVTIPCON='F')
    endwith
  endfunc

  func oMVCONTRA_1_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_67('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCONTRA_1_67.ecpDrop(oSource)
    this.Parent.oContained.link_1_67('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCONTRA_1_67.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_TRAM_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStrODBC(this.Parent.oContained.w_MVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStr(this.Parent.oContained.w_MVTIPCON)
    endif
    do cp_zoom with 'CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(this.parent,'oMVCONTRA_1_67'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BCZ',"Elenco contratti",'GSVE1MDV.CON_TRAM_VZM',this.parent.oContained
  endproc
  proc oMVCONTRA_1_67.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BCZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.COTIPCLF=w_MVTIPCON
     i_obj.w_CONUMERO=this.parent.oContained.w_MVCONTRA
     i_obj.ecpSave()
  endproc

  add object oDESCTR_1_68 as StdField with uid="NRGQXAWYIE",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESCTR", cQueryName = "DESCTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 224456650,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=248, Top=182, InputMask=replicate('X',50), visible=iif(g_GESCON='S', .T., .F.)

  func oDESCTR_1_68.mHide()
    with this.Parent.oContained
      return (.w_MVTIPCON='F')
    endwith
  endfunc


  add object oBtn_1_70 as StdButton with uid="XYEYKENOKZ",left=489, top=210, width=48,height=45,;
    CpPicture="bmp\doc1.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento generato dalla evasione componenti";
    , HelpContextID = 8276692;
    , Caption='\<Componenti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_70.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_MVRIFESC, -20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_70.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MVRIFESC) AND g_EACD='S')
      endwith
    endif
  endfunc

  func oBtn_1_70.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_MVRIFESC) OR g_EACD<>'S')
     endwith
    endif
  endfunc

  add object oMVVOCCEN_1_88 as StdField with uid="BXYHJRRPSK",rtseq=73,rtrep=.f.,;
    cFormVar = "w_MVVOCCEN", cQueryName = "MVVOCCEN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di costo/ricavo incongruente o obsoleto",;
    ToolTipText = "Codice voce di costo/ricavo",;
    HelpContextID = 43733524,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=116, Top=277, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_MVVOCCEN"

  func oMVVOCCEN_1_88.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (((g_PERCCR='S' AND .w_FLANAL='S' and .w_MVFLNOAN='N') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D')
    endwith
   endif
  endfunc

  func oMVVOCCEN_1_88.mHide()
    with this.Parent.oContained
      return (NOT (((g_PERCCR='S' AND .w_FLANAL='S') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D'))
    endwith
  endfunc

  func oMVVOCCEN_1_88.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_88('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVVOCCEN_1_88.ecpDrop(oSource)
    this.Parent.oContained.link_1_88('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVVOCCEN_1_88.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oMVVOCCEN_1_88'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo o ricavo",'GSVE_KDA.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oMVVOCCEN_1_88.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_MVVOCCEN
     i_obj.ecpSave()
  endproc

  add object oDESVOC_1_89 as StdField with uid="RRXGULMIJI",rtseq=74,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 211677130,;
   bGlobalFont=.t.,;
    Height=21, Width=330, Left=207, Top=277, InputMask=replicate('X',40)

  func oDESVOC_1_89.mHide()
    with this.Parent.oContained
      return (NOT (((g_PERCCR='S' AND .w_FLANAL='S') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D'))
    endwith
  endfunc

  add object oMVFLNOAN_1_90 as StdCheck with uid="MITWDAIHPR",rtseq=75,rtrep=.f.,left=116, top=307, caption="Escludi analitica",;
    ToolTipText = "Se attivo esclude analitica di riga",;
    HelpContextID = 12103148,;
    cFormVar="w_MVFLNOAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMVFLNOAN_1_90.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMVFLNOAN_1_90.GetRadio()
    this.Parent.oContained.w_MVFLNOAN = this.RadioValue()
    return .t.
  endfunc

  func oMVFLNOAN_1_90.SetRadio()
    this.Parent.oContained.w_MVFLNOAN=trim(this.Parent.oContained.w_MVFLNOAN)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLNOAN=='S',1,;
      0)
  endfunc

  func oMVFLNOAN_1_90.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' AND .w_FLANAL='S'))
    endwith
  endfunc

  add object oMVCACONT_1_99 as StdField with uid="KVLLBRQSWS",rtseq=214,rtrep=.f.,;
    cFormVar = "w_MVCACONT", cQueryName = "MVCACONT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contributo accessorio",;
    HelpContextID = 24370662,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=116, Top=367, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATMCONT", cZoomOnZoom="GSAR_MCT", oKey_1_1="CGCODICE", oKey_1_2="this.w_MVCACONT"

  func oMVCACONT_1_99.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.F.)
    endwith
   endif
  endfunc

  func oMVCACONT_1_99.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  func oMVCACONT_1_99.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_99('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCACONT_1_99.ecpDrop(oSource)
    this.Parent.oContained.link_1_99('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCACONT_1_99.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATMCONT','*','CGCODICE',cp_AbsName(this.parent,'oMVCACONT_1_99'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MCT',"",'',this.parent.oContained
  endproc
  proc oMVCACONT_1_99.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CGCODICE=this.parent.oContained.w_MVCACONT
     i_obj.ecpSave()
  endproc

  add object oCGDESCRI_1_101 as StdField with uid="GOQBGRCCTH",rtseq=215,rtrep=.f.,;
    cFormVar = "w_CGDESCRI", cQueryName = "CGDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 208657809,;
   bGlobalFont=.t.,;
    Height=21, Width=358, Left=179, Top=366, InputMask=replicate('X',40)

  func oCGDESCRI_1_101.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="EYQOJNELIO",Visible=.t., Left=3, Top=99,;
    Alignment=1, Width=111, Height=15,;
    Caption="Codice IVA articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="YJEDDCTQIT",Visible=.t., Left=3, Top=71,;
    Alignment=1, Width=111, Height=15,;
    Caption="Cat.contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="LOURSBDLXV",Visible=.t., Left=388, Top=15,;
    Alignment=1, Width=132, Height=15,;
    Caption="Liv. raggruppamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="SQRMVZAVIK",Visible=.t., Left=4, Top=127,;
    Alignment=1, Width=111, Height=15,;
    Caption="Contropartita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="VQGPHJQJDY",Visible=.t., Left=3, Top=183,;
    Alignment=1, Width=111, Height=15,;
    Caption="Contratto:"    , visible=iif(g_GESCON='S', .T., .F.);
  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="ZUOUBBIWQG",Visible=.t., Left=3, Top=15,;
    Alignment=1, Width=111, Height=15,;
    Caption="Tipologia riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="DDWUTYQQLK",Visible=.t., Left=-1, Top=155,;
    Alignment=1, Width=115, Height=15,;
    Caption="C.IVA indetraibile:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_PERIND=0)
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="FJKJSMOITD",Visible=.t., Left=3, Top=43,;
    Alignment=1, Width=111, Height=15,;
    Caption="Tot.valore di riga:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_MVFLELGM<>'S')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="FCPOTZEILS",Visible=.t., Left=199, Top=211,;
    Alignment=1, Width=45, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="ERSMOMAYSO",Visible=.t., Left=3, Top=211,;
    Alignment=1, Width=111, Height=15,;
    Caption="Competenza da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="LNORNUBEIT",Visible=.t., Left=9, Top=244,;
    Alignment=0, Width=101, Height=18,;
    Caption="Analitica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_83.mHide()
    with this.Parent.oContained
      return (NOT (((g_PERCCR='S' AND .w_FLANAL='S') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D'))
    endwith
  endfunc

  add object oStr_1_91 as StdString with uid="JHVMHAMUZO",Visible=.t., Left=1, Top=279,;
    Alignment=1, Width=113, Height=18,;
    Caption="Voce di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (NOT (((g_PERCCR='S' AND .w_FLANAL='S') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D') OR .w_VOCTIP<>'C')
    endwith
  endfunc

  add object oStr_1_92 as StdString with uid="SIYNLROMSZ",Visible=.t., Left=1, Top=279,;
    Alignment=1, Width=113, Height=18,;
    Caption="Voce di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return (NOT (((g_PERCCR='S' AND .w_FLANAL='S') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D') OR .w_VOCTIP<>'R')
    endwith
  endfunc

  add object oStr_1_100 as StdString with uid="MRSIBYWRTC",Visible=.t., Left=-1, Top=368,;
    Alignment=1, Width=115, Height=18,;
    Caption="Categoria contributo:"  ;
  , bGlobalFont=.t.

  func oStr_1_100.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  add object oStr_1_102 as StdString with uid="TNNHJNBACR",Visible=.t., Left=9, Top=345,;
    Alignment=0, Width=180, Height=18,;
    Caption="Contributi accessori"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_102.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  add object oBox_1_87 as StdBox with uid="VCWKEHGRUU",left=9, top=265, width=562,height=2

  add object oBox_1_103 as StdBox with uid="RVOSDXQNTR",left=9, top=362, width=562,height=2
enddefine
define class tgsor_kdaPag2 as StdContainer
  Width  = 597
  height = 397
  stdWidth  = 597
  stdheight = 397
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVCAUMAG_2_7 as StdField with uid="SHKYLZADJE",rtseq=88,rtrep=.f.,;
    cFormVar = "w_MVCAUMAG", cQueryName = "MVCAUMAG",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di movimentazione magazzino",;
    HelpContextID = 39050739,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=138, Top=62, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_MVCAUMAG"

  func oMVCAUMAG_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCCAU = 'S')
    endwith
   endif
  endfunc

  func oMVCAUMAG_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCAUMAG_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCAUMAG_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oMVCAUMAG_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oMVCAUMAG_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_MVCAUMAG
     i_obj.ecpSave()
  endproc

  add object oDESCAU_2_9 as StdField with uid="DAJGKZLOSL",rtseq=90,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 194047946,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=200, Top=62, InputMask=replicate('X',35)

  add object oMVCAUCOL_2_15 as StdField with uid="FJDAUAPNPY",rtseq=96,rtrep=.f.,;
    cFormVar = "w_MVCAUCOL", cQueryName = "MVCAUCOL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 206822894,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=139, Top=120, InputMask=replicate('X',5), cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_MVCAUCOL"

  func oMVCAUCOL_2_15.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  func oMVCAUCOL_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMVFLELGM_2_23 as StdCheck with uid="FSKEYGXDVN",rtseq=104,rtrep=.f.,left=459, top=62, caption="Movimento fiscale", enabled=.f.,;
    ToolTipText = "Se attivo: riga valida per la contabilit� fiscale di magazzino",;
    HelpContextID = 196563475,;
    cFormVar="w_MVFLELGM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMVFLELGM_2_23.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMVFLELGM_2_23.GetRadio()
    this.Parent.oContained.w_MVFLELGM = this.RadioValue()
    return .t.
  endfunc

  func oMVFLELGM_2_23.SetRadio()
    this.Parent.oContained.w_MVFLELGM=trim(this.Parent.oContained.w_MVFLELGM)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLELGM=='S',1,;
      0)
  endfunc

  add object oMVCODMAG_2_24 as StdField with uid="ZPBEMDNDBA",rtseq=105,rtrep=.f.,;
    cFormVar = "w_MVCODMAG", cQueryName = "MVCODMAG",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente o obsoleto",;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 55959027,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=138, Top=91, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MVCODMAG"

  func oMVCODMAG_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG = 'R' AND NOT EMPTY(.w_AGGSAL) and Not .w_TESCOMP)
    endwith
   endif
  endfunc

  func oMVCODMAG_2_24.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG <> 'R' OR EMPTY(.w_AGGSAL))
    endwith
  endfunc

  func oMVCODMAG_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODMAG_2_24.ecpDrop(oSource)
    this.Parent.oContained.link_2_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODMAG_2_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMVCODMAG_2_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oMVCODMAG_2_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MVCODMAG
     i_obj.ecpSave()
  endproc

  add object oMVCODMAT_2_26 as StdField with uid="MCLBWSULNT",rtseq=107,rtrep=.f.,;
    cFormVar = "w_MVCODMAT", cQueryName = "MVCODMAT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente o obsoleto",;
    ToolTipText = "Codice eventuale magazzino collegato",;
    HelpContextID = 55959014,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=139, Top=150, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MVCODMAT"

  func oMVCODMAT_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVCAUCOL) AND .w_MVTIPRIG = 'R' AND NOT EMPTY(.w_AGGSAL1) and Not .w_TESCOMP)
    endwith
   endif
  endfunc

  func oMVCODMAT_2_26.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  func oMVCODMAT_2_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODMAT_2_26.ecpDrop(oSource)
    this.Parent.oContained.link_2_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODMAT_2_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMVCODMAT_2_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oMVCODMAT_2_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MVCODMAT
     i_obj.ecpSave()
  endproc

  add object oDESMAG_2_31 as StdField with uid="SJRRDUZUNA",rtseq=112,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 159838154,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=200, Top=91, InputMask=replicate('X',30)

  func oDESMAG_2_31.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG <> 'R' OR EMPTY(.w_AGGSAL))
    endwith
  endfunc

  add object oMVDATEVA_2_32 as StdField with uid="OHNKJCVLEE",rtseq=113,rtrep=.f.,;
    cFormVar = "w_MVDATEVA", cQueryName = "MVDATEVA",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data prevista evasione riga",;
    HelpContextID = 94122503,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=139, Top=202

  func oMVDATEVA_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDTPR='S' and Not .w_TESCOMP)
    endwith
   endif
  endfunc

  func oMVDATEVA_2_32.mHide()
    with this.Parent.oContained
      return (.w_FLDTPR<>'S' AND .w_MVQTAEVA=0 AND .w_MVIMPEVA=0)
    endwith
  endfunc

  func oMVDATEVA_2_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_MVDATEVA>=.w_MVDATDOC OR EMPTY(.w_MVDATEVA))
         bRes=(cp_WarningMsg(thisform.msgFmt("La data di prevista evasione di riga � minore della data documento")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oMVFLEVAS_2_33 as StdCheck with uid="FPTQCFPXYM",rtseq=114,rtrep=.f.,left=141, top=230, caption="Evasa",;
    ToolTipText = "Se attivo: riga evasa",;
    HelpContextID = 172535271,;
    cFormVar="w_MVFLEVAS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMVFLEVAS_2_33.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMVFLEVAS_2_33.GetRadio()
    this.Parent.oContained.w_MVFLEVAS = this.RadioValue()
    return .t.
  endfunc

  func oMVFLEVAS_2_33.SetRadio()
    this.Parent.oContained.w_MVFLEVAS=trim(this.Parent.oContained.w_MVFLEVAS)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLEVAS=='S',1,;
      0)
  endfunc

  func oMVFLEVAS_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDTPR='S'  AND .w_MVCLADOC<>'RF' and Not .w_TESCOMP)
    endwith
   endif
  endfunc

  func oMVFLEVAS_2_33.mHide()
    with this.Parent.oContained
      return (.w_MVCLADOC='RF')
    endwith
  endfunc

  add object oQTDISP_2_38 as StdField with uid="BDCLFPZILR",rtseq=115,rtrep=.f.,;
    cFormVar = "w_QTDISP", cQueryName = "QTDISP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Disponibilit� articolo relativa al magazzino principale",;
    HelpContextID = 258723834,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=499, Top=91, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oQTDISP_2_38.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG <> 'R' OR EMPTY(.w_AGGSAL))
    endwith
  endfunc

  add object oMVEFFEVA_2_43 as StdField with uid="BTVUFDRQBT",rtseq=118,rtrep=.f.,;
    cFormVar = "w_MVEFFEVA", cQueryName = "MVEFFEVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data effettiva evasione riga",;
    HelpContextID = 79774215,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=280, Top=202

  func oMVEFFEVA_2_43.mHide()
    with this.Parent.oContained
      return (.w_FLDTPR<>'S' AND .w_MVQTAEVA=0 AND .w_MVIMPEVA=0)
    endwith
  endfunc

  add object oMVQTAEVA_2_49 as StdField with uid="YSQAVWJZIZ",rtseq=119,rtrep=.f.,;
    cFormVar = "w_MVQTAEVA", cQueryName = "MVQTAEVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� complessivamente evasa sulla riga documento",;
    HelpContextID = 75497991,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=506, Top=202, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oMVQTAEVA_2_49.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVQTAEVA=0) OR .w_MVTIPRIG='F')
    endwith
  endfunc

  add object oMVIMPEVA_2_50 as StdField with uid="HNJRARUEGJ",rtseq=120,rtrep=.f.,;
    cFormVar = "w_MVIMPEVA", cQueryName = "MVIMPEVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo complessivamente evaso sulla riga documento",;
    HelpContextID = 90735111,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=464, Top=202, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVIMPEVA_2_50.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVIMPEVA=0) OR .w_MVTIPRIG<>'F')
    endwith
  endfunc

  add object oMVCODART_2_53 as StdField with uid="BIMWERAPQD",rtseq=123,rtrep=.f.,;
    cFormVar = "w_MVCODART", cQueryName = "MVCODART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 257285606,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=138, Top=33, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_MVCODART"

  func oMVCODART_2_53.mHide()
    with this.Parent.oContained
      return (.w_MVCODART=.w_MVCODICE)
    endwith
  endfunc

  func oMVCODART_2_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESART_2_55 as StdField with uid="QISOWWOTKQ",rtseq=124,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 193130442,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=291, Top=33, InputMask=replicate('X',40)

  func oDESART_2_55.mHide()
    with this.Parent.oContained
      return (.w_MVCODART=.w_MVCODICE)
    endwith
  endfunc

  add object oMVQTAEV1_2_58 as StdField with uid="BNMIOCVSRT",rtseq=127,rtrep=.f.,;
    cFormVar = "w_MVQTAEV1", cQueryName = "MVQTAEV1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� complessivamente evasa sulla riga documento",;
    HelpContextID = 75497975,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=506, Top=230, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oMVQTAEV1_2_58.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVQTAEVA=0) OR .w_MVTIPRIG='F' OR .w_MVUNIMIS=.w_UNMIS1 OR (.w_MVUNIMIS<>.w_UNMIS1 And .w_MVUNIMIS<>.w_UNMIS2 AND .w_MVUNIMIS<>.w_UNMIS3))
    endwith
  endfunc

  add object oUM1_2_59 as StdField with uid="UTFGSOSNKI",rtseq=128,rtrep=.f.,;
    cFormVar = "w_UM1", cQueryName = "UM1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 186251590,;
   bGlobalFont=.t.,;
    Height=20, Width=38, Left=466, Top=230, InputMask=replicate('X',3)

  func oUM1_2_59.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVQTAEVA=0) OR .w_MVTIPRIG='F' OR .w_MVUNIMIS=.w_UNMIS1 OR (.w_MVUNIMIS<>.w_UNMIS1 And .w_MVUNIMIS<>.w_UNMIS2 AND .w_MVUNIMIS<>.w_UNMIS3))
    endwith
  endfunc

  add object oUMR_2_60 as StdField with uid="VZGXPGNWNC",rtseq=129,rtrep=.f.,;
    cFormVar = "w_UMR", cQueryName = "UMR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 186386758,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=466, Top=202, InputMask=replicate('X',3)

  func oUMR_2_60.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVQTAEVA=0) OR .w_MVTIPRIG='F')
    endwith
  endfunc

  add object oMVQTAIMP_2_62 as StdField with uid="DLEFKMJSMT",rtseq=130,rtrep=.f.,;
    cFormVar = "w_MVQTAIMP", cQueryName = "MVQTAIMP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� complessivamente importata dal documento di origine",;
    HelpContextID = 125828586,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=180, Top=262, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oMVQTAIMP_2_62.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'DF' OR EMPTY(.w_MVSERRIF))
    endwith
  endfunc

  add object oMVQTAIM1_2_63 as StdField with uid="KXLBRZHDTO",rtseq=131,rtrep=.f.,;
    cFormVar = "w_MVQTAIM1", cQueryName = "MVQTAIM1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� complessivamente importata dal documento di origine espressa nella 1^ U.M.",;
    HelpContextID = 125828617,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=311, Top=262, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oMVQTAIM1_2_63.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'DF' OR EMPTY(.w_MVSERRIF) OR .w_MVUNIMIS=.w_UNMIS1 OR (.w_MVUNIMIS<>.w_UNMIS1 And .w_MVUNIMIS<>.w_UNMIS2 AND .w_MVUNIMIS<>.w_UNMIS3))
    endwith
  endfunc

  add object oUM1_2_64 as StdField with uid="BPJMGLJKPU",rtseq=132,rtrep=.f.,;
    cFormVar = "w_UM1", cQueryName = "UM1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 186251590,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=272, Top=262, InputMask=replicate('X',3)

  func oUM1_2_64.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'DF' OR EMPTY(.w_MVSERRIF) OR .w_MVUNIMIS=.w_UNMIS1 OR (.w_MVUNIMIS<>.w_UNMIS1 And .w_MVUNIMIS<>.w_UNMIS2 AND .w_MVUNIMIS<>.w_UNMIS3))
    endwith
  endfunc

  add object oUMR_2_65 as StdField with uid="PAKZONKFRT",rtseq=133,rtrep=.f.,;
    cFormVar = "w_UMR", cQueryName = "UMR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 186386758,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=141, Top=262, InputMask=replicate('X',3)

  func oUMR_2_65.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'DF' OR EMPTY(.w_MVSERRIF))
    endwith
  endfunc


  add object oBtn_2_77 as StdButton with uid="QWNRWBBQRE",left=544, top=262, width=48,height=45,;
    CpPicture="bmp\DocDest.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai documenti di destinazione che hanno evaso la riga";
    , HelpContextID = 21721065;
    , Tabstop=.f.,Caption='\<Doc. Dest.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_77.Click()
      with this.Parent.oContained
        GSVE_BD3(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_78 as StdButton with uid="RGMQJTWBTF",left=495, top=262, width=48,height=45,;
    CpPicture="bmp\Origine.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere al documento di origine che ha evaso la riga";
    , HelpContextID = 197001427;
    , Tabstop=.f.,Caption='\<Doc.Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_78.Click()
      with this.Parent.oContained
        GSVE_BD3(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_2_83 as cp_runprogram with uid="OEGXODOFHV",left=-7, top=418, width=152,height=33,;
    caption='GSVE_BEV',;
   bGlobalFont=.t.,;
    prg="GSVE_BEV",;
    cEvent = "w_MVFLEVAS Changed",;
    nPag=2;
    , HelpContextID = 55660220

  add object oDESMAT_2_84 as StdField with uid="CYALAXXGKO",rtseq=149,rtrep=.f.,;
    cFormVar = "w_DESMAT", cQueryName = "DESMAT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 210169802,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=200, Top=150, InputMask=replicate('X',30)

  func oDESMAT_2_84.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  add object oQ2DISP_2_87 as StdField with uid="FLMIBBJHUN",rtseq=150,rtrep=.f.,;
    cFormVar = "w_Q2DISP", cQueryName = "Q2DISP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Disponibilit� articolo relativa al magazzino collegato",;
    HelpContextID = 258732538,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=499, Top=150, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oQ2DISP_2_87.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  add object oDESCA2_2_88 as StdField with uid="FRDHXIINZF",rtseq=151,rtrep=.f.,;
    cFormVar = "w_DESCA2", cQueryName = "DESCA2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 244379594,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=200, Top=120, InputMask=replicate('X',35)

  func oDESCA2_2_88.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  add object oMVDATOAI_2_94 as StdField with uid="BTVZLOPRTM",rtseq=156,rtrep=.f.,;
    cFormVar = "w_MVDATOAI", cQueryName = "MVDATOAI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Specificare una data inizio validit�",;
    ToolTipText = "Data dalla quale parte la fatturazione e l'intervallo di fatturazione dell'ordine aperto",;
    HelpContextID = 6540785,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=141, Top=291

  func oMVDATOAI_2_94.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLORDAPE = 'S')
    endwith
   endif
  endfunc

  func oMVDATOAI_2_94.mHide()
    with this.Parent.oContained
      return (.w_FLORDAPE <> 'S')
    endwith
  endfunc

  func oMVDATOAI_2_94.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Not Empty(.w_MVDATOAI))
    endwith
    return bRes
  endfunc

  add object oMVDATOAF_2_95 as StdField with uid="ACQJOMOGKQ",rtseq=157,rtrep=.f.,;
    cFormVar = "w_MVDATOAF", cQueryName = "MVDATOAF",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dopo la quale l'ordine aperto non � pi� attivo",;
    HelpContextID = 6540788,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=141, Top=317

  func oMVDATOAF_2_95.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLORDAPE = 'S')
    endwith
   endif
  endfunc

  func oMVDATOAF_2_95.mHide()
    with this.Parent.oContained
      return (.w_FLORDAPE <> 'S')
    endwith
  endfunc


  add object oMVMC_PER_2_99 as StdTableCombo with uid="KUGPFNZUTN",rtseq=158,rtrep=.f.,left=312,top=317,width=278,height=21;
    , HelpContextID = 21938712;
    , cFormVar="w_MVMC_PER",tablefilter="", bObbl = .t. , nPag = 2;
    , cTable='MODCLDAT',cKey='MDCODICE',cValue='MDDESCRI',cOrderBy='MDDESCRI',xDefault=space(3);
  , bGlobalFont=.t.


  func oMVMC_PER_2_99.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLORDAPE = 'S')
    endwith
   endif
  endfunc

  func oMVMC_PER_2_99.mHide()
    with this.Parent.oContained
      return (.w_FLORDAPE <> 'S')
    endwith
  endfunc

  add object oMVRIFEDI_2_102 as StdField with uid="UWVPTTLLCY",rtseq=213,rtrep=.f.,;
    cFormVar = "w_MVRIFEDI", cQueryName = "MVRIFEDI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(14), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento evasione EDI",;
    HelpContextID = 80024079,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=141, Top=343, InputMask=replicate('X',14)

  func oMVRIFEDI_2_102.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oStr_2_34 as StdString with uid="WNLIJMVSVU",Visible=.t., Left=24, Top=91,;
    Alignment=1, Width=113, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_2_34.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG <> 'R' OR EMPTY(.w_AGGSAL))
    endwith
  endfunc

  add object oStr_2_35 as StdString with uid="GPVYVAGRVB",Visible=.t., Left=24, Top=62,;
    Alignment=1, Width=113, Height=15,;
    Caption="Causale magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="XOQMWXGFNQ",Visible=.t., Left=30, Top=9,;
    Alignment=0, Width=384, Height=15,;
    Caption="Dati magazzino"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_37 as StdString with uid="GZFIJOOQYI",Visible=.t., Left=452, Top=92,;
    Alignment=1, Width=46, Height=15,;
    Caption="Disp.:"  ;
  , bGlobalFont=.t.

  func oStr_2_37.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG <> 'R' OR EMPTY(.w_AGGSAL))
    endwith
  endfunc

  add object oStr_2_42 as StdString with uid="NKJUWJZDJT",Visible=.t., Left=30, Top=176,;
    Alignment=0, Width=67, Height=15,;
    Caption="Evasione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_42.mHide()
    with this.Parent.oContained
      return (.w_FLDTPR<>'S')
    endwith
  endfunc

  add object oStr_2_44 as StdString with uid="BUVSAVLQID",Visible=.t., Left=25, Top=202,;
    Alignment=1, Width=113, Height=15,;
    Caption="Prevista:"  ;
  , bGlobalFont=.t.

  func oStr_2_44.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVQTAEVA=0 AND .w_MVIMPEVA=0) OR .w_FLORDAPE = 'S')
    endwith
  endfunc

  add object oStr_2_45 as StdString with uid="OWDSMXEBFF",Visible=.t., Left=221, Top=202,;
    Alignment=1, Width=57, Height=15,;
    Caption="Effettiva:"  ;
  , bGlobalFont=.t.

  func oStr_2_45.mHide()
    with this.Parent.oContained
      return (.w_FLDTPR<>'S' AND .w_MVQTAEVA=0 AND .w_MVIMPEVA=0)
    endwith
  endfunc

  add object oStr_2_47 as StdString with uid="GKNGRJYXBY",Visible=.t., Left=364, Top=202,;
    Alignment=1, Width=101, Height=15,;
    Caption="Quantit� evasa:"  ;
  , bGlobalFont=.t.

  func oStr_2_47.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVQTAEVA=0) OR .w_MVTIPRIG='F')
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="PCCCILIIUJ",Visible=.t., Left=361, Top=202,;
    Alignment=1, Width=100, Height=15,;
    Caption="Importo evaso:"  ;
  , bGlobalFont=.t.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVIMPEVA=0) OR .w_MVTIPRIG<>'F')
    endwith
  endfunc

  add object oStr_2_54 as StdString with uid="LFJDBJJRGT",Visible=.t., Left=24, Top=33,;
    Alignment=1, Width=113, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_54.mHide()
    with this.Parent.oContained
      return (.w_MVCODART=.w_MVCODICE)
    endwith
  endfunc

  add object oStr_2_61 as StdString with uid="MFPUOCTZHK",Visible=.t., Left=26, Top=262,;
    Alignment=1, Width=114, Height=18,;
    Caption="Quantit� importata:"  ;
  , bGlobalFont=.t.

  func oStr_2_61.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'DF' OR EMPTY(.w_MVSERRIF))
    endwith
  endfunc

  add object oStr_2_85 as StdString with uid="GCGOUWGWOL",Visible=.t., Left=26, Top=150,;
    Alignment=1, Width=112, Height=15,;
    Caption="Mag.collegato:"  ;
  , bGlobalFont=.t.

  func oStr_2_85.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  add object oStr_2_86 as StdString with uid="VZZBCBZEYH",Visible=.t., Left=452, Top=151,;
    Alignment=1, Width=45, Height=15,;
    Caption="Disp.:"  ;
  , bGlobalFont=.t.

  func oStr_2_86.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  add object oStr_2_89 as StdString with uid="SWOEEGOSRH",Visible=.t., Left=25, Top=120,;
    Alignment=1, Width=113, Height=18,;
    Caption="Causale collegata:"  ;
  , bGlobalFont=.t.

  func oStr_2_89.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  add object oStr_2_96 as StdString with uid="QQGAJGWJJT",Visible=.t., Left=5, Top=291,;
    Alignment=1, Width=135, Height=18,;
    Caption="Data inizio validit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_96.mHide()
    with this.Parent.oContained
      return (.w_FLORDAPE <> 'S')
    endwith
  endfunc

  add object oStr_2_97 as StdString with uid="PNEFXKTUNH",Visible=.t., Left=11, Top=317,;
    Alignment=1, Width=129, Height=18,;
    Caption="Data fine validit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_97.mHide()
    with this.Parent.oContained
      return (.w_FLORDAPE <> 'S')
    endwith
  endfunc

  add object oStr_2_98 as StdString with uid="DCKMLROSFF",Visible=.t., Left=25, Top=202,;
    Alignment=1, Width=113, Height=15,;
    Caption="Data prossima fatt.:"  ;
  , bGlobalFont=.t.

  func oStr_2_98.mHide()
    with this.Parent.oContained
      return (.w_FLORDAPE <> 'S')
    endwith
  endfunc

  add object oStr_2_100 as StdString with uid="XPGKAYJGHF",Visible=.t., Left=225, Top=317,;
    Alignment=1, Width=85, Height=17,;
    Caption="Periodicit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_100.mHide()
    with this.Parent.oContained
      return (.w_FLORDAPE <> 'S')
    endwith
  endfunc

  add object oStr_2_103 as StdString with uid="FFLARVCVTO",Visible=.t., Left=34, Top=344,;
    Alignment=1, Width=106, Height=18,;
    Caption="Riferimento EDI:"  ;
  , bGlobalFont=.t.

  func oStr_2_103.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oBox_2_41 as StdBox with uid="CZHMVAZUYJ",left=30, top=27, width=562,height=2

  add object oBox_2_46 as StdBox with uid="TITEAEVSXW",left=30, top=196, width=562,height=2
enddefine
define class tgsor_kdaPag3 as StdContainer
  Width  = 597
  height = 397
  stdWidth  = 597
  stdheight = 397
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oMVFLTRAS_3_1 as StdCombo with uid="ZIHZVFCINR",value=3,rtseq=160,rtrep=.f.,left=180,top=33,width=160,height=21;
    , ToolTipText = "Compilazione campi dati Intrastat: nessuno - solo statistici - statistici e fiscali";
    , HelpContextID = 223915495;
    , cFormVar="w_MVFLTRAS",RowSource=""+"Nessuna valorizzazione,"+"Solo dati statistici,"+"Dati statistici e fiscali,"+"Solo dati fiscali", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oMVFLTRAS_3_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'Z',;
    iif(this.value =3,' ',;
    iif(this.value =4,'I',;
    space(1))))))
  endfunc
  func oMVFLTRAS_3_1.GetRadio()
    this.Parent.oContained.w_MVFLTRAS = this.RadioValue()
    return .t.
  endfunc

  func oMVFLTRAS_3_1.SetRadio()
    this.Parent.oContained.w_MVFLTRAS=trim(this.Parent.oContained.w_MVFLTRAS)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLTRAS=='S',1,;
      iif(this.Parent.oContained.w_MVFLTRAS=='Z',2,;
      iif(this.Parent.oContained.w_MVFLTRAS=='',3,;
      iif(this.Parent.oContained.w_MVFLTRAS=='I',4,;
      0))))
  endfunc

  func oMVFLTRAS_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFM' AND g_INTR='S'  AND .w_ARUTISER='N')
    endwith
   endif
  endfunc

  add object oMVNOMENC_3_2 as StdField with uid="FEEBQFWPXT",rtseq=161,rtrep=.f.,;
    cFormVar = "w_MVNOMENC", cQueryName = "MVNOMENC",;
    bObbl = .f. , nPag = 3, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice nomenclatura",;
    HelpContextID = 180694519,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=180, Top=63, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="NOMENCLA", cZoomOnZoom="GSAR_ANM", oKey_1_1="NMCODICE", oKey_1_2="this.w_MVNOMENC"

  func oMVNOMENC_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND g_INTR='S')
    endwith
   endif
  endfunc

  func oMVNOMENC_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVNOMENC_3_2.ecpDrop(oSource)
    this.Parent.oContained.link_3_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVNOMENC_3_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NOMENCLA','*','NMCODICE',cp_AbsName(this.parent,'oMVNOMENC_3_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANM',"Nomenclature",'GSMA_AZN.NOMENCLA_VZM',this.parent.oContained
  endproc
  proc oMVNOMENC_3_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NMCODICE=this.parent.oContained.w_MVNOMENC
     i_obj.ecpSave()
  endproc

  add object oDESNOM_3_3 as StdField with uid="BQCSNZNNFA",rtseq=162,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 44429258,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=272, Top=63, InputMask=replicate('X',35)

  add object oUMSUPP_3_5 as StdField with uid="LHPCKVZZMJ",rtseq=164,rtrep=.f.,;
    cFormVar = "w_UMSUPP", cQueryName = "UMSUPP",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 261023418,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=180, Top=88, InputMask=replicate('X',3)

  add object oMVMOLSUP_3_7 as StdField with uid="RVGUTPVWBA",rtseq=166,rtrep=.f.,;
    cFormVar = "w_MVMOLSUP", cQueryName = "MVMOLSUP",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Moltiplicatore U.M. supplementare rispetto alla 1^U.M.",;
    HelpContextID = 53133846,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=247, Top=88, cSayPict='"9999.999"', cGetPict='"9999.999"'

  func oMVMOLSUP_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND g_INTR='S' AND .w_ARUTISER='N')
    endwith
   endif
  endfunc

  add object oUM1_3_8 as StdField with uid="JVUSBLTDXW",rtseq=167,rtrep=.f.,;
    cFormVar = "w_UM1", cQueryName = "UM1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 186251590,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=328, Top=88, InputMask=replicate('X',3)

  add object oMVPESNET_3_9 as StdField with uid="DKGBWCLDGU",rtseq=168,rtrep=.f.,;
    cFormVar = "w_MVPESNET", cQueryName = "MVPESNET",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Massa netta unitaria in kg.",;
    HelpContextID = 244380186,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=180, Top=113, cSayPict='"99999.999"', cGetPict='"99999.999"'

  func oMVPESNET_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND .w_ARUTISER='N')
    endwith
   endif
  endfunc

  add object oTOTMASNE_3_10 as StdField with uid="YNOZIDSHKO",rtseq=169,rtrep=.f.,;
    cFormVar = "w_TOTMASNE", cQueryName = "TOTMASNE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 226940037,;
   bGlobalFont=.t.,;
    Height=21, Width=96, Left=320, Top=113, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  add object oMVPROORD_3_11 as StdField with uid="WDSHYJXPVE",rtseq=170,rtrep=.f.,;
    cFormVar = "w_MVPROORD", cQueryName = "MVPROORD",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di origine/destinazione della merce",;
    HelpContextID = 10620406,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=180, Top=138, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oMVPROORD_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND g_INTR='S' AND .w_ARUTISER='N')
    endwith
   endif
  endfunc

  func oMVPROORD_3_11.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  add object oMVNAZPRO_3_12 as StdField with uid="NCQOOBIRHE",rtseq=171,rtrep=.f.,;
    cFormVar = "w_MVNAZPRO", cQueryName = "MVNAZPRO",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Nazione di provenienza della merce (se non specificato, prende naz. fornitore)",;
    HelpContextID = 251866603,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=359, Top=138, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_MVNAZPRO"

  func oMVNAZPRO_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND g_INTR='S' AND .w_ARUTISER='N')
    endwith
   endif
  endfunc

  func oMVNAZPRO_3_12.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  func oMVNAZPRO_3_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVNAZPRO_3_12.ecpDrop(oSource)
    this.Parent.oContained.link_3_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVNAZPRO_3_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oMVNAZPRO_3_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oMVNAZPRO_3_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_MVNAZPRO
     i_obj.ecpSave()
  endproc

  add object oMVAIRPOR_3_14 as StdField with uid="GRJKZXSLKE",rtseq=173,rtrep=.f.,;
    cFormVar = "w_MVAIRPOR", cQueryName = "MVAIRPOR",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Porto/aeroporto di destinazione",;
    HelpContextID = 259784168,;
   bGlobalFont=.t.,;
    Height=21, Width=92, Left=180, Top=194, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COD_AREO", cZoomOnZoom="GLES_APP", oKey_1_1="PPCODICE", oKey_1_2="this.w_MVAIRPOR"

  func oMVAIRPOR_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND g_INTR='S')
    endwith
   endif
  endfunc

  func oMVAIRPOR_3_14.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S') OR g_ISONAZ<>'ESP')
    endwith
  endfunc

  func oMVAIRPOR_3_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVAIRPOR_3_14.ecpDrop(oSource)
    this.Parent.oContained.link_3_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVAIRPOR_3_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_AREO','*','PPCODICE',cp_AbsName(this.parent,'oMVAIRPOR_3_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GLES_APP',"Porti/aeroporti",'',this.parent.oContained
  endproc
  proc oMVAIRPOR_3_14.mZoomOnZoom
    local i_obj
    i_obj=GLES_APP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PPCODICE=this.parent.oContained.w_MVAIRPOR
     i_obj.ecpSave()
  endproc


  add object oMVTIPPRO_3_21 as StdCombo with uid="ARCUOTIPJA",rtseq=174,rtrep=.f.,left=167,top=253,width=176,height=21;
    , HelpContextID = 261803499;
    , cFormVar="w_MVTIPPRO",RowSource=""+"Da calcolare,"+"Forzata,"+"Calcolata da contratto,"+"Esclusa", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oMVTIPPRO_3_21.RadioValue()
    return(iif(this.value =1,'DC',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'CC',;
    iif(this.value =4,'ES',;
    space(2))))))
  endfunc
  func oMVTIPPRO_3_21.GetRadio()
    this.Parent.oContained.w_MVTIPPRO = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPRO_3_21.SetRadio()
    this.Parent.oContained.w_MVTIPPRO=trim(this.Parent.oContained.w_MVTIPPRO)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPRO=='DC',1,;
      iif(this.Parent.oContained.w_MVTIPPRO=='FO',2,;
      iif(this.Parent.oContained.w_MVTIPPRO=='CC',3,;
      iif(this.Parent.oContained.w_MVTIPPRO=='ES',4,;
      0))))
  endfunc

  func oMVTIPPRO_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(g_CALPRO$'GI-GD'))
    endwith
   endif
  endfunc

  func oMVTIPPRO_3_21.mHide()
    with this.Parent.oContained
      return (g_CALPRO$'GD-GI')
    endwith
  endfunc


  add object oMVTIPPRO_3_22 as StdCombo with uid="ILGCUIDBLC",rtseq=175,rtrep=.f.,left=167,top=253,width=176,height=21;
    , HelpContextID = 261803499;
    , cFormVar="w_MVTIPPRO",RowSource=""+"Calcolata da tab.provvigioni,"+"Forzata,"+"Esclusa,"+"Calcolata da contratto,"+"Esclusa da tab.provvigioni", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oMVTIPPRO_3_22.RadioValue()
    return(iif(this.value =1,'CT',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'ES',;
    iif(this.value =4,'CC',;
    iif(this.value =5,'ET',;
    space(2)))))))
  endfunc
  func oMVTIPPRO_3_22.GetRadio()
    this.Parent.oContained.w_MVTIPPRO = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPRO_3_22.SetRadio()
    this.Parent.oContained.w_MVTIPPRO=trim(this.Parent.oContained.w_MVTIPPRO)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPRO=='CT',1,;
      iif(this.Parent.oContained.w_MVTIPPRO=='FO',2,;
      iif(this.Parent.oContained.w_MVTIPPRO=='ES',3,;
      iif(this.Parent.oContained.w_MVTIPPRO=='CC',4,;
      iif(this.Parent.oContained.w_MVTIPPRO=='ET',5,;
      0)))))
  endfunc

  func oMVTIPPRO_3_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(g_CALPRO$'DI-GD'))
    endwith
   endif
  endfunc

  func oMVTIPPRO_3_22.mHide()
    with this.Parent.oContained
      return (g_CALPRO$'DI-GD')
    endwith
  endfunc


  add object oMVTIPPRO_3_23 as StdCombo with uid="KRTFQIVOMZ",rtseq=176,rtrep=.f.,left=167,top=253,width=176,height=21;
    , HelpContextID = 261803499;
    , cFormVar="w_MVTIPPRO",RowSource=""+"Suggerita da tab.provvigioni,"+"Forzata,"+"Calcolata da contratto,"+"Esclusa,"+"Esclusa da tab.provvigioni", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oMVTIPPRO_3_23.RadioValue()
    return(iif(this.value =1,'ST',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'CC',;
    iif(this.value =4,'ES',;
    iif(this.value =5,'ET',;
    space(2)))))))
  endfunc
  func oMVTIPPRO_3_23.GetRadio()
    this.Parent.oContained.w_MVTIPPRO = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPRO_3_23.SetRadio()
    this.Parent.oContained.w_MVTIPPRO=trim(this.Parent.oContained.w_MVTIPPRO)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPRO=='ST',1,;
      iif(this.Parent.oContained.w_MVTIPPRO=='FO',2,;
      iif(this.Parent.oContained.w_MVTIPPRO=='CC',3,;
      iif(this.Parent.oContained.w_MVTIPPRO=='ES',4,;
      iif(this.Parent.oContained.w_MVTIPPRO=='ET',5,;
      0)))))
  endfunc

  func oMVTIPPRO_3_23.mHide()
    with this.Parent.oContained
      return (g_CALPRO$'GI-DI')
    endwith
  endfunc

  add object oMVPERPRO_3_27 as StdField with uid="QZCSCCTRMN",rtseq=177,rtrep=.f.,;
    cFormVar = "w_MVPERPRO", cQueryName = "MVPERPRO",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale provvigioni agente applicato alla riga documento",;
    HelpContextID = 259984875,;
   bGlobalFont=.t.,;
    Height=20, Width=57, Left=167, Top=278, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMVPERPRO_3_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA' AND .w_MVIMPPRO=0  AND g_PERAGE='S' AND .w_MVTIPPRO='FO')
    endwith
   endif
  endfunc

  add object oMVIMPPRO_3_28 as StdField with uid="HRSMINDCEA",rtseq=178,rtrep=.f.,;
    cFormVar = "w_MVIMPPRO", cQueryName = "MVIMPPRO",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo provvigioni agente riferito alla riga documento",;
    HelpContextID = 261586411,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=365, Top=278, cSayPict="v_PU(38+VVL)", cGetPict="v_GU(38+VVL)"

  func oMVIMPPRO_3_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA' AND .w_MVPERPRO=0 AND g_PERAGE='S' AND .w_MVTIPPRO='FO')
    endwith
   endif
  endfunc


  add object oMVTIPPR2_3_29 as StdCombo with uid="MYXZZLAUPT",rtseq=179,rtrep=.f.,left=167,top=305,width=176,height=21;
    , HelpContextID = 261803528;
    , cFormVar="w_MVTIPPR2",RowSource=""+"Suggerita da tab.provvigioni,"+"Forzata,"+"Calcolata da contratto,"+"Esclusa,"+"Esclusa da tab.provvigioni", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oMVTIPPR2_3_29.RadioValue()
    return(iif(this.value =1,'ST',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'CC',;
    iif(this.value =4,'ES',;
    iif(this.value =5,'ET',;
    space(2)))))))
  endfunc
  func oMVTIPPR2_3_29.GetRadio()
    this.Parent.oContained.w_MVTIPPR2 = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPR2_3_29.SetRadio()
    this.Parent.oContained.w_MVTIPPR2=trim(this.Parent.oContained.w_MVTIPPR2)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPR2=='ST',1,;
      iif(this.Parent.oContained.w_MVTIPPR2=='FO',2,;
      iif(this.Parent.oContained.w_MVTIPPR2=='CC',3,;
      iif(this.Parent.oContained.w_MVTIPPR2=='ES',4,;
      iif(this.Parent.oContained.w_MVTIPPR2=='ET',5,;
      0)))))
  endfunc

  func oMVTIPPR2_3_29.mHide()
    with this.Parent.oContained
      return (g_CALPRO$'GI-DI'   OR .w_MVPROCAP=999.9)
    endwith
  endfunc


  add object oMVTIPPR2_3_30 as StdCombo with uid="OSHPWARMKM",rtseq=180,rtrep=.f.,left=167,top=305,width=176,height=21;
    , HelpContextID = 261803528;
    , cFormVar="w_MVTIPPR2",RowSource=""+"Calcolata da tab.provvigioni,"+"Forzata,"+"Esclusa,"+"Calcolata da contratto,"+"Esclusa da tab.provvigioni", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oMVTIPPR2_3_30.RadioValue()
    return(iif(this.value =1,'CT',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'ES',;
    iif(this.value =4,'CC',;
    iif(this.value =5,'ET',;
    space(2)))))))
  endfunc
  func oMVTIPPR2_3_30.GetRadio()
    this.Parent.oContained.w_MVTIPPR2 = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPR2_3_30.SetRadio()
    this.Parent.oContained.w_MVTIPPR2=trim(this.Parent.oContained.w_MVTIPPR2)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPR2=='CT',1,;
      iif(this.Parent.oContained.w_MVTIPPR2=='FO',2,;
      iif(this.Parent.oContained.w_MVTIPPR2=='ES',3,;
      iif(this.Parent.oContained.w_MVTIPPR2=='CC',4,;
      iif(this.Parent.oContained.w_MVTIPPR2=='ET',5,;
      0)))))
  endfunc

  func oMVTIPPR2_3_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(g_CALPRO$'DI-GD'))
    endwith
   endif
  endfunc

  func oMVTIPPR2_3_30.mHide()
    with this.Parent.oContained
      return (g_CALPRO$'DI-GD'  OR .w_MVPROCAP=999.9)
    endwith
  endfunc


  add object oMVTIPPR2_3_31 as StdCombo with uid="NWMYAAZQUK",rtseq=181,rtrep=.f.,left=167,top=305,width=176,height=21;
    , HelpContextID = 261803528;
    , cFormVar="w_MVTIPPR2",RowSource=""+"Da calcolare,"+"Forzata,"+"Calcolata da contratto,"+"Esclusa", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oMVTIPPR2_3_31.RadioValue()
    return(iif(this.value =1,'DC',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'CC',;
    iif(this.value =4,'ES',;
    space(2))))))
  endfunc
  func oMVTIPPR2_3_31.GetRadio()
    this.Parent.oContained.w_MVTIPPR2 = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPR2_3_31.SetRadio()
    this.Parent.oContained.w_MVTIPPR2=trim(this.Parent.oContained.w_MVTIPPR2)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPR2=='DC',1,;
      iif(this.Parent.oContained.w_MVTIPPR2=='FO',2,;
      iif(this.Parent.oContained.w_MVTIPPR2=='CC',3,;
      iif(this.Parent.oContained.w_MVTIPPR2=='ES',4,;
      0))))
  endfunc

  func oMVTIPPR2_3_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(g_CALPRO$'GI-GD'))
    endwith
   endif
  endfunc

  func oMVTIPPR2_3_31.mHide()
    with this.Parent.oContained
      return (g_CALPRO$'GD-GI' OR .w_MVPROCAP=999.9)
    endwith
  endfunc

  add object oMVPROCAP_3_32 as StdField with uid="YJGMPKTEOL",rtseq=182,rtrep=.f.,;
    cFormVar = "w_MVPROCAP", cQueryName = "MVPROCAP",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale provvigioni capoarea applicato alla riga documento",;
    HelpContextID = 211946986,;
   bGlobalFont=.t.,;
    Height=20, Width=57, Left=167, Top=329, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMVPROCAP_3_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA' AND .w_MVIMPCAP=0  AND g_PERAGE='S' AND .w_MVTIPPR2='FO')
    endwith
   endif
  endfunc

  func oMVPROCAP_3_32.mHide()
    with this.Parent.oContained
      return (.w_MVPROCAP=999.9)
    endwith
  endfunc

  add object oMVIMPCAP_3_33 as StdField with uid="VRAQDTUXBD",rtseq=183,rtrep=.f.,;
    cFormVar = "w_MVIMPCAP", cQueryName = "MVIMPCAP",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo provvigioni capoarea riferito alla riga documento",;
    HelpContextID = 211254762,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=365, Top=329, cSayPict="v_PU(38+VVL)", cGetPict="v_GU(38+VVL)"

  func oMVIMPCAP_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA' AND .w_MVPROCAP=0 AND g_PERAGE='S' AND .w_MVTIPPR2='FO')
    endwith
   endif
  endfunc

  func oMVIMPCAP_3_33.mHide()
    with this.Parent.oContained
      return (.w_MVPROCAP=999.9)
    endwith
  endfunc

  add object oMVNUMCOL_3_43 as StdField with uid="ALNMBMTVNQ",rtseq=186,rtrep=.f.,;
    cFormVar = "w_MVNUMCOL", cQueryName = "MVNUMCOL",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Superato massimo numero confezioni per riga. Valore massimo 30000.",;
    ToolTipText = "Numero confezioni per riga",;
    HelpContextID = 213855726,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=496, Top=374, cSayPict='"99999"', cGetPict='"99999"'

  func oMVNUMCOL_3_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CODCONF) and .w_FLCOVA='S')
    endwith
   endif
  endfunc

  func oMVNUMCOL_3_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVNUMCOL<=30000)
    endwith
    return bRes
  endfunc

  add object oCODCONF_3_44 as StdField with uid="NHZQBTOGGX",rtseq=187,rtrep=.f.,;
    cFormVar = "w_CODCONF", cQueryName = "CODCONF",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo confezione associata all'articolo",;
    HelpContextID = 240003622,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=81, Top=374, InputMask=replicate('X',3), cLinkFile="TIPICONF", oKey_1_1="TCCODICE", oKey_1_2="this.w_CODCONF", oKey_2_1="TCCODICE", oKey_2_2="this.w_CODCONF"

  func oCODCONF_3_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCONF)
        bRes2=.link_3_44('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDESCONF_3_46 as StdField with uid="JDTBRLINCS",rtseq=188,rtrep=.f.,;
    cFormVar = "w_DESCONF", cQueryName = "DESCONF",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 240062518,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=138, Top=374, InputMask=replicate('X',35)

  add object oDESCRI_3_56 as StdField with uid="NMHTSXTPVR",rtseq=190,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 109113290,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=273, Top=194, InputMask=replicate('X',40)

  func oDESCRI_3_56.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S') OR g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oMVPAEFOR_3_57 as StdField with uid="HGHCDDHPOO",rtseq=210,rtrep=.f.,;
    cFormVar = "w_MVPAEFOR", cQueryName = "MVPAEFOR",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Origine del fornitore",;
    HelpContextID = 173215208,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=359, Top=164, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_MVPAEFOR"

  func oMVPAEFOR_3_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND g_INTR='S' AND .w_MVFLVEAC = 'A' AND .w_ARUTISER='N')
    endwith
   endif
  endfunc

  func oMVPAEFOR_3_57.mHide()
    with this.Parent.oContained
      return (NOT(.w_MVFLVEAC='A' AND .w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  func oMVPAEFOR_3_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_57('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVPAEFOR_3_57.ecpDrop(oSource)
    this.Parent.oContained.link_3_57('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVPAEFOR_3_57.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oMVPAEFOR_3_57'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oMVPAEFOR_3_57.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_MVPAEFOR
     i_obj.ecpSave()
  endproc

  add object oISONAZ_3_61 as StdField with uid="IJPKXAXNUL",rtseq=211,rtrep=.f.,;
    cFormVar = "w_ISONAZ", cQueryName = "ISONAZ",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 109453690,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=484, Top=138, InputMask=replicate('X',3)

  func oISONAZ_3_61.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  add object oISONA2_3_63 as StdField with uid="EHSEVUMMLI",rtseq=212,rtrep=.f.,;
    cFormVar = "w_ISONA2", cQueryName = "ISONA2",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 243671418,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=484, Top=165, InputMask=replicate('X',3)

  func oISONA2_3_63.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  add object oStr_3_15 as StdString with uid="VVDALHSZFB",Visible=.t., Left=4, Top=5,;
    Alignment=0, Width=224, Height=15,;
    Caption="Dati INTRA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_16 as StdString with uid="JTTSZNQIPJ",Visible=.t., Left=30, Top=63,;
    Alignment=1, Width=147, Height=15,;
    Caption="Nomenclatura:"  ;
  , bGlobalFont=.t.

  func oStr_3_16.mHide()
    with this.Parent.oContained
      return (.w_ARUTISER<>'N')
    endwith
  endfunc

  add object oStr_3_17 as StdString with uid="TYMZAGJFQJ",Visible=.t., Left=30, Top=88,;
    Alignment=1, Width=147, Height=15,;
    Caption="U.M. supplementare:"  ;
  , bGlobalFont=.t.

  add object oStr_3_18 as StdString with uid="DLJSEMYSGP",Visible=.t., Left=233, Top=88,;
    Alignment=0, Width=15, Height=15,;
    Caption="="  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="UFOWTOFZFA",Visible=.t., Left=30, Top=113,;
    Alignment=1, Width=147, Height=15,;
    Caption="Massa netta unitaria:"  ;
  , bGlobalFont=.t.

  add object oStr_3_20 as StdString with uid="JTGJMPOZFW",Visible=.t., Left=265, Top=113,;
    Alignment=1, Width=53, Height=15,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_24 as StdString with uid="JEDGLSPRQI",Visible=.t., Left=426, Top=113,;
    Alignment=0, Width=42, Height=15,;
    Caption="Kg."  ;
  , bGlobalFont=.t.

  add object oStr_3_25 as StdString with uid="XWQBGWKLTD",Visible=.t., Left=33, Top=279,;
    Alignment=1, Width=130, Height=14,;
    Caption="% Provvigioni agente:"  ;
  , bGlobalFont=.t.

  add object oStr_3_26 as StdString with uid="HLDXXSOKTE",Visible=.t., Left=233, Top=279,;
    Alignment=1, Width=129, Height=14,;
    Caption="Importo provvigioni:"  ;
  , bGlobalFont=.t.

  add object oStr_3_34 as StdString with uid="NSDQMCDQYU",Visible=.t., Left=4, Top=221,;
    Alignment=0, Width=222, Height=15,;
    Caption="Dati provvigioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_37 as StdString with uid="ZJKPNDRKCD",Visible=.t., Left=216, Top=138,;
    Alignment=1, Width=141, Height=18,;
    Caption="Paese di provenienza:"  ;
  , bGlobalFont=.t.

  func oStr_3_37.mHide()
    with this.Parent.oContained
      return (NOT(.w_MVFLVEAC='A' AND .w_MVTIPRIG = 'R' AND g_INTR='S'))
    endwith
  endfunc

  add object oStr_3_38 as StdString with uid="RAAFEXVPZP",Visible=.t., Left=30, Top=138,;
    Alignment=1, Width=147, Height=15,;
    Caption="Provincia di destinazione:"  ;
  , bGlobalFont=.t.

  func oStr_3_38.mHide()
    with this.Parent.oContained
      return (NOT(.w_MVFLVEAC='A' AND .w_MVTIPRIG = 'R' AND g_INTR='S'))
    endwith
  endfunc

  add object oStr_3_39 as StdString with uid="ZJNYAMFYLL",Visible=.t., Left=30, Top=138,;
    Alignment=1, Width=147, Height=15,;
    Caption="Provincia di origine:"  ;
  , bGlobalFont=.t.

  func oStr_3_39.mHide()
    with this.Parent.oContained
      return (NOT(.w_MVFLVEAC='V' AND .w_MVTIPRIG = 'R' AND g_INTR='S'))
    endwith
  endfunc

  add object oStr_3_42 as StdString with uid="GPJSHZHSQE",Visible=.t., Left=29, Top=253,;
    Alignment=1, Width=134, Height=18,;
    Caption="Tipo provvigione agente:"  ;
  , bGlobalFont=.t.

  add object oStr_3_45 as StdString with uid="RTVIFESQWJ",Visible=.t., Left=14, Top=374,;
    Alignment=1, Width=65, Height=18,;
    Caption="Confezione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_47 as StdString with uid="YOCXNKXDSG",Visible=.t., Left=402, Top=376,;
    Alignment=1, Width=93, Height=18,;
    Caption="Qta. Confezioni:"  ;
  , bGlobalFont=.t.

  add object oStr_3_50 as StdString with uid="OCSYYNENJW",Visible=.t., Left=10, Top=347,;
    Alignment=0, Width=111, Height=15,;
    Caption="Dati confezioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_51 as StdString with uid="ALCPNXYBSQ",Visible=.t., Left=8, Top=329,;
    Alignment=1, Width=155, Height=15,;
    Caption="% Provvigioni capoarea:"  ;
  , bGlobalFont=.t.

  func oStr_3_51.mHide()
    with this.Parent.oContained
      return (.w_MVPROCAP=999.9)
    endwith
  endfunc

  add object oStr_3_52 as StdString with uid="BEMFCTLCDB",Visible=.t., Left=233, Top=329,;
    Alignment=1, Width=129, Height=15,;
    Caption="Importo provvigioni:"  ;
  , bGlobalFont=.t.

  func oStr_3_52.mHide()
    with this.Parent.oContained
      return (.w_MVPROCAP=999.9)
    endwith
  endfunc

  add object oStr_3_53 as StdString with uid="MHDFWSNQAR",Visible=.t., Left=4, Top=306,;
    Alignment=1, Width=159, Height=18,;
    Caption="Tipo provvigione capoarea:"  ;
  , bGlobalFont=.t.

  func oStr_3_53.mHide()
    with this.Parent.oContained
      return (.w_MVPROCAP=999.9)
    endwith
  endfunc

  add object oStr_3_54 as StdString with uid="MQQAYBARNU",Visible=.t., Left=18, Top=194,;
    Alignment=1, Width=159, Height=18,;
    Caption="Porto/aeroporto di origine:"  ;
  , bGlobalFont=.t.

  func oStr_3_54.mHide()
    with this.Parent.oContained
      return ((NOT(.w_MVFLVEAC='V' AND .w_MVTIPRIG = 'R' AND g_INTR='S')) OR g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oStr_3_55 as StdString with uid="ERQURYBOZC",Visible=.t., Left=3, Top=194,;
    Alignment=1, Width=174, Height=18,;
    Caption="Porto/aeroporto di destinazione:"  ;
  , bGlobalFont=.t.

  func oStr_3_55.mHide()
    with this.Parent.oContained
      return ((NOT(.w_MVFLVEAC='A' AND .w_MVTIPRIG = 'R' AND g_INTR='S')) OR g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oStr_3_58 as StdString with uid="VCODEWNVAG",Visible=.t., Left=258, Top=166,;
    Alignment=1, Width=100, Height=18,;
    Caption="Paese di origine:"  ;
  , bGlobalFont=.t.

  func oStr_3_58.mHide()
    with this.Parent.oContained
      return (NOT(.w_MVFLVEAC='A' AND .w_MVTIPRIG = 'R' AND g_INTR='S'))
    endwith
  endfunc

  add object oStr_3_59 as StdString with uid="WUWZAMGAGS",Visible=.t., Left=226, Top=138,;
    Alignment=1, Width=131, Height=18,;
    Caption="Paese di destinazione:"  ;
  , bGlobalFont=.t.

  func oStr_3_59.mHide()
    with this.Parent.oContained
      return (NOT(.w_MVFLVEAC='V' AND .w_MVTIPRIG = 'R' AND g_INTR='S'))
    endwith
  endfunc

  add object oStr_3_60 as StdString with uid="WKRNPEQEYZ",Visible=.t., Left=421, Top=141,;
    Alignment=1, Width=55, Height=18,;
    Caption="Cod. ISO:"  ;
  , bGlobalFont=.t.

  func oStr_3_60.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  add object oStr_3_62 as StdString with uid="SHHEJSAHDI",Visible=.t., Left=421, Top=168,;
    Alignment=1, Width=55, Height=18,;
    Caption="Cod. ISO:"  ;
  , bGlobalFont=.t.

  func oStr_3_62.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  add object oStr_3_65 as StdString with uid="ERIUYGJTBK",Visible=.t., Left=50, Top=63,;
    Alignment=1, Width=127, Height=15,;
    Caption="Codice servizio:"  ;
  , bGlobalFont=.t.

  func oStr_3_65.mHide()
    with this.Parent.oContained
      return (.w_ARUTISER='N')
    endwith
  endfunc

  add object oStr_3_66 as StdString with uid="SNPOLPVPXG",Visible=.t., Left=6, Top=36,;
    Alignment=1, Width=171, Height=17,;
    Caption="Manutenzione elenchi INTRA:"  ;
  , bGlobalFont=.t.

  add object oBox_3_35 as StdBox with uid="GRRMBNFBTL",left=2, top=24, width=567,height=2

  add object oBox_3_36 as StdBox with uid="FJXMDVVOGX",left=2, top=240, width=567,height=1

  add object oBox_3_48 as StdBox with uid="VIBTHTBDVB",left=2, top=366, width=568,height=1
enddefine
define class tgsor_kdaPag4 as StdContainer
  Width  = 597
  height = 397
  stdWidth  = 597
  stdheight = 397
  resizeXpos=385
  resizeYpos=205
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVDESSUP_4_2 as StdMemo with uid="XRJOWUCTUJ",rtseq=192,rtrep=.f.,;
    cFormVar = "w_MVDESSUP", cQueryName = "MVDESSUP",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali annotazioni aggiuntive di riga",;
    HelpContextID = 59781654,;
   bGlobalFont=.t.,;
    Height=290, Width=589, Left=4, Top=8

  func oMVDESSUP_4_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODDES='S')
    endwith
   endif
  endfunc
enddefine
define class tgsor_kdaPag5 as StdContainer
  Width  = 597
  height = 397
  stdWidth  = 597
  stdheight = 397
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESCCESP_5_3 as StdField with uid="WEQFMWZPXO",rtseq=195,rtrep=.f.,;
    cFormVar = "w_DESCCESP", cQueryName = "DESCCESP",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 191950714,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=249, Top=44, InputMask=replicate('X',40)

  func oDESCCESP_5_3.mHide()
    with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
    endwith
  endfunc


  add object oObj_5_5 as cp_runprogram with uid="OIADURCMKB",left=632, top=261, width=181,height=26,;
    caption='Object(VALO)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCZ('Valo')",;
    cEvent = "Valorizzacespiti,Inizio",;
    nPag=5;
    , HelpContextID = 98623052

  add object oMVCODCES_5_7 as StdField with uid="SVKCQZVOHE",rtseq=198,rtrep=.f.,;
    cFormVar = "w_MVCODCES", cQueryName = "MVCODCES",;
    bObbl = .f. , nPag = 5, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Cespite inesistente o di tipo non valido",;
    ToolTipText = "Codice cespite associato al servizio",;
    HelpContextID = 44704281,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=92, Top=44, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", oKey_1_1="CECODICE", oKey_1_2="this.w_MVCODCES"

  func oMVCODCES_5_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.oparentobject.cfunction ='Load' OR .oparentobject.cfunction ='Edit'  )  and .w_CESPRES=.T.)
    endwith
   endif
  endfunc

  func oMVCODCES_5_7.mHide()
    with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
    endwith
  endfunc

  func oMVCODCES_5_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODCES_5_7.ecpDrop(oSource)
    this.Parent.oContained.link_5_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODCES_5_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oMVCODCES_5_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Cespiti",'GSVE_KCE.CES_PITI_VZM',this.parent.oContained
  endproc


  add object oBtn_5_8 as StdButton with uid="PGVBPUDNUF",left=92, top=80, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per accedere al cespite";
    , HelpContextID = 123584353;
    , tabstop=.f.,caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_8.Click()
      with this.Parent.oContained
        GSCE_BCZ(this.Parent.oContained,"Aprc")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MVCODCES))
      endwith
    endif
  endfunc

  func oBtn_5_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
     endwith
    endif
  endfunc


  add object oBtn_5_9 as StdButton with uid="RCFXNPPRKB",left=142, top=80, width=48,height=45,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per caricare un nuovo cespite";
    , HelpContextID = 226230058;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_9.Click()
      with this.Parent.oContained
        GSCE_BCZ(this.Parent.oContained,"Load")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.oparentobject.cfunction ='Load' OR .oparentobject.cfunction ='Edit'  ))
      endwith
    endif
  endfunc

  func oBtn_5_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N' OR .oparentobject.cfunction ='Query')
     endwith
    endif
  endfunc

  add object oStr_5_10 as StdString with uid="AOGTALZTVI",Visible=.t., Left=19, Top=45,;
    Alignment=1, Width=71, Height=18,;
    Caption="Cespite:"  ;
  , bGlobalFont=.t.

  func oStr_5_10.mHide()
    with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
    endwith
  endfunc

  add object oStr_5_11 as StdString with uid="EDMBSQNAMS",Visible=.t., Left=11, Top=16,;
    Alignment=0, Width=240, Height=18,;
    Caption="Cespite"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_5_12 as StdBox with uid="UUCBNQSAON",left=1, top=35, width=548,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsor_kda','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
