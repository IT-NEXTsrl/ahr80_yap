* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_add                                                        *
*              Addetti alla rilevazione                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_8]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-05-14                                                      *
* Last revis.: 2006-11-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_add"))

* --- Class definition
define class tgsma_add as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 471
  Height = 126+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2006-11-28"
  HelpContextID=92961641
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  ADDERILE_IDX = 0
  cFile = "ADDERILE"
  cKeySelect = "ARCODICE"
  cKeyWhere  = "ARCODICE=this.w_ARCODICE"
  cKeyWhereODBC = '"ARCODICE="+cp_ToStrODBC(this.w_ARCODICE)';

  cKeyWhereODBCqualified = '"ADDERILE.ARCODICE="+cp_ToStrODBC(this.w_ARCODICE)';

  cPrg = "gsma_add"
  cComment = "Addetti alla rilevazione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ARCODICE = space(5)
  w_ARDESCRI = space(40)
  w_AR__NOME = space(40)
  w_AR_COGNO = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ADDERILE','gsma_add')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_addPag1","gsma_add",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Addetto")
      .Pages(1).HelpContextID = 102837510
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oARCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ADDERILE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ADDERILE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ADDERILE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ARCODICE = NVL(ARCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ADDERILE where ARCODICE=KeySet.ARCODICE
    *
    i_nConn = i_TableProp[this.ADDERILE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ADDERILE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ADDERILE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ADDERILE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ARCODICE',this.w_ARCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ARCODICE = NVL(ARCODICE,space(5))
        .w_ARDESCRI = NVL(ARDESCRI,space(40))
        .w_AR__NOME = NVL(AR__NOME,space(40))
        .w_AR_COGNO = NVL(AR_COGNO,space(40))
        cp_LoadRecExtFlds(this,'ADDERILE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ARCODICE = space(5)
      .w_ARDESCRI = space(40)
      .w_AR__NOME = space(40)
      .w_AR_COGNO = space(40)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'ADDERILE')
    this.DoRTCalc(1,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oARCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oARDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oAR__NOME_1_4.enabled = i_bVal
      .Page1.oPag.oAR_COGNO_1_6.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oARCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oARCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ADDERILE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ADDERILE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODICE,"ARCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDESCRI,"ARDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AR__NOME,"AR__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AR_COGNO,"AR_COGNO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ADDERILE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2])
    i_lTable = "ADDERILE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ADDERILE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ADDERILE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ADDERILE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ADDERILE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ADDERILE')
        i_extval=cp_InsertValODBCExtFlds(this,'ADDERILE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ARCODICE,ARDESCRI,AR__NOME,AR_COGNO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ARCODICE)+;
                  ","+cp_ToStrODBC(this.w_ARDESCRI)+;
                  ","+cp_ToStrODBC(this.w_AR__NOME)+;
                  ","+cp_ToStrODBC(this.w_AR_COGNO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ADDERILE')
        i_extval=cp_InsertValVFPExtFlds(this,'ADDERILE')
        cp_CheckDeletedKey(i_cTable,0,'ARCODICE',this.w_ARCODICE)
        INSERT INTO (i_cTable);
              (ARCODICE,ARDESCRI,AR__NOME,AR_COGNO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ARCODICE;
                  ,this.w_ARDESCRI;
                  ,this.w_AR__NOME;
                  ,this.w_AR_COGNO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ADDERILE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ADDERILE_IDX,i_nConn)
      *
      * update ADDERILE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ADDERILE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ARDESCRI="+cp_ToStrODBC(this.w_ARDESCRI)+;
             ",AR__NOME="+cp_ToStrODBC(this.w_AR__NOME)+;
             ",AR_COGNO="+cp_ToStrODBC(this.w_AR_COGNO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ADDERILE')
        i_cWhere = cp_PKFox(i_cTable  ,'ARCODICE',this.w_ARCODICE  )
        UPDATE (i_cTable) SET;
              ARDESCRI=this.w_ARDESCRI;
             ,AR__NOME=this.w_AR__NOME;
             ,AR_COGNO=this.w_AR_COGNO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ADDERILE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ADDERILE_IDX,i_nConn)
      *
      * delete ADDERILE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ARCODICE',this.w_ARCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ADDERILE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oARCODICE_1_1.value==this.w_ARCODICE)
      this.oPgFrm.Page1.oPag.oARCODICE_1_1.value=this.w_ARCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESCRI_1_3.value==this.w_ARDESCRI)
      this.oPgFrm.Page1.oPag.oARDESCRI_1_3.value=this.w_ARDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oAR__NOME_1_4.value==this.w_AR__NOME)
      this.oPgFrm.Page1.oPag.oAR__NOME_1_4.value=this.w_AR__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oAR_COGNO_1_6.value==this.w_AR_COGNO)
      this.oPgFrm.Page1.oPag.oAR_COGNO_1_6.value=this.w_AR_COGNO
    endif
    cp_SetControlsValueExtFlds(this,'ADDERILE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsma_addPag1 as StdContainer
  Width  = 467
  height = 126
  stdWidth  = 467
  stdheight = 126
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oARCODICE_1_1 as StdField with uid="HEJHRXUCSP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ARCODICE", cQueryName = "ARCODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice dell'addetto alla rilevazione",;
    HelpContextID = 134810443,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=55, Left=104, Top=24, InputMask=replicate('X',5)

  add object oARDESCRI_1_3 as StdField with uid="OOYBXZSELB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ARDESCRI", cQueryName = "ARDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 49224527,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=161, Top=24, InputMask=replicate('X',40)

  add object oAR__NOME_1_4 as StdField with uid="UFMDUWUULA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_AR__NOME", cQueryName = "AR__NOME",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 21312693,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=104, Top=52, InputMask=replicate('X',40)

  add object oAR_COGNO_1_6 as StdField with uid="ZVDQKVWNXL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_AR_COGNO", cQueryName = "AR_COGNO",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 156316843,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=104, Top=79, InputMask=replicate('X',40)

  add object oStr_1_2 as StdString with uid="GWRYJOZTYG",Visible=.t., Left=9, Top=29,;
    Alignment=1, Width=90, Height=18,;
    Caption="Codice utente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="FDMGFLBPAT",Visible=.t., Left=53, Top=56,;
    Alignment=1, Width=46, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="XMIGRYUSRZ",Visible=.t., Left=35, Top=83,;
    Alignment=1, Width=64, Height=18,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_add','ADDERILE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ARCODICE=ADDERILE.ARCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
