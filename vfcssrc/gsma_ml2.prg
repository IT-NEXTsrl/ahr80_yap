* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_ml2                                                        *
*              Inserimento listini                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_106]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-17                                                      *
* Last revis.: 2012-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_ml2"))

* --- Class definition
define class tgsma_ml2 as StdTrsForm
  Top    = 8
  Left   = 9

  * --- Standard Properties
  Width  = 691
  Height = 298+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-01-02"
  HelpContextID=214596457
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  INS_LIST_IDX = 0
  LISTINI_IDX = 0
  VALUTE_IDX = 0
  ART_ICOL_IDX = 0
  INS_SCAG_IDX = 0
  UNIMIS_IDX = 0
  cFile = "INS_LIST"
  cKeySelect = "LICODLIS"
  cKeyWhere  = "LICODLIS=this.w_LICODLIS"
  cKeyDetail  = "LICODLIS=this.w_LICODLIS"
  cKeyWhereODBC = '"LICODLIS="+cp_ToStrODBC(this.w_LICODLIS)';

  cKeyDetailWhereODBC = '"LICODLIS="+cp_ToStrODBC(this.w_LICODLIS)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"INS_LIST.LICODLIS="+cp_ToStrODBC(this.w_LICODLIS)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'INS_LIST.CPROWNUM '
  cPrg = "gsma_ml2"
  cComment = "Inserimento listini"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 9
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LICODLIS = space(5)
  o_LICODLIS = space(5)
  w_DTINVA = ctod('  /  /  ')
  w_DTFINV = ctod('  /  /  ')
  w_DESLIS = space(40)
  w_VALLIS = space(3)
  w_DECUNI = 0
  w_CALCPICT = 0
  w_IVALIS = space(1)
  w_LIDATATT = ctod('  /  /  ')
  w_LIDATDIS = ctod('  /  /  ')
  w_TIPOPE = space(10)
  w_LICODART = space(20)
  o_LICODART = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESART1 = space(40)
  w_TIPART = space(2)
  w_UNMIS1 = space(3)
  w_DESART = space(40)
  w_FLSCO = space(1)
  w_OK = .F.
  w_UNMIS2 = space(3)
  w_CODART = space(10)
  w_FLSERG = space(1)
  w_LIUNIMIS = space(3)

  * --- Children pointers
  GSMA_MS2 = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsma_ml2
  * --- Esce Subito senza dare messaggi
  
  
  proc ecpQuit()
    DoDefault()
    if Not this.bUpdated
        this.Hide()
    this.Release()
    endif
    return
  Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'INS_LIST','gsma_ml2')
    stdPageFrame::Init()
    *set procedure to GSMA_MS2 additive
    with this
      .Pages(1).addobject("oPag","tgsma_ml2Pag1","gsma_ml2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Listino")
      .Pages(1).HelpContextID = 129949002
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLICODLIS_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSMA_MS2
    * --- Area Manuale = Init Page Frame
    * --- gsma_ml2
      * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='INS_SCAG'
    this.cWorkTables[5]='UNIMIS'
    this.cWorkTables[6]='INS_LIST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INS_LIST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INS_LIST_IDX,3]
  return

  function CreateChildren()
    this.GSMA_MS2 = CREATEOBJECT('stdDynamicChild',this,'GSMA_MS2',this.oPgFrm.Page1.oPag.oLinkPC_2_2)
    this.GSMA_MS2.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSMA_MS2)
      this.GSMA_MS2.DestroyChildrenChain()
      this.GSMA_MS2=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_2')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMA_MS2.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMA_MS2.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMA_MS2.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSMA_MS2.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_LICODLIS,"LICODLIS";
             ,.w_CPROWNUM,"LIROWNUM";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_LICODLIS = NVL(LICODLIS,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from INS_LIST where LICODLIS=KeySet.LICODLIS
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.INS_LIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INS_LIST_IDX,2],this.bLoadRecFilter,this.INS_LIST_IDX,"gsma_ml2")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INS_LIST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INS_LIST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INS_LIST '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LICODLIS',this.w_LICODLIS  )
      select * from (i_cTable) INS_LIST where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DTINVA = ctod("  /  /  ")
        .w_DTFINV = ctod("  /  /  ")
        .w_DESLIS = space(40)
        .w_VALLIS = space(3)
        .w_DECUNI = 0
        .w_IVALIS = space(1)
        .w_OBTEST = I_DATSYS
        .w_FLSCO = space(1)
        .w_OK = .T.
        .w_FLSERG = space(1)
        .w_LICODLIS = NVL(LICODLIS,space(5))
          if link_1_1_joined
            this.w_LICODLIS = NVL(LSCODLIS101,NVL(this.w_LICODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS101,space(40))
            this.w_IVALIS = NVL(LSIVALIS101,space(1))
            this.w_VALLIS = NVL(LSVALLIS101,space(3))
            this.w_DTINVA = NVL(cp_ToDate(LSDTINVA101),ctod("  /  /  "))
            this.w_DTFINV = NVL(cp_ToDate(LSDTOBSO101),ctod("  /  /  "))
            this.w_FLSCO = NVL(LSFLSCON101,space(1))
          else
          .link_1_1('Load')
          endif
          .link_1_5('Load')
        .w_CALCPICT = DEFPIC(.w_DECUNI)
        .w_LIDATATT = NVL(cp_ToDate(LIDATATT),ctod("  /  /  "))
        .w_LIDATDIS = NVL(cp_ToDate(LIDATDIS),ctod("  /  /  "))
        .w_TIPOPE = this.cFunction
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'INS_LIST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DATOBSO = ctod("  /  /  ")
          .w_TIPART = space(2)
          .w_UNMIS1 = space(3)
          .w_DESART = space(40)
          .w_UNMIS2 = space(3)
          .w_CODART = space(10)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_LICODART = NVL(LICODART,space(20))
          if link_2_1_joined
            this.w_LICODART = NVL(ARCODART201,NVL(this.w_LICODART,space(20)))
            this.w_DESART = NVL(ARDESART201,space(40))
            this.w_TIPART = NVL(ARTIPART201,space(2))
            this.w_DATOBSO = NVL(cp_ToDate(ARDTOBSO201),ctod("  /  /  "))
            this.w_UNMIS1 = NVL(ARUNMIS1201,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2201,space(3))
            this.w_CODART = NVL(ARCODART201,space(10))
            this.w_FLSERG = NVL(ARFLSERG201,space(1))
          else
          .link_2_1('Load')
          endif
        .w_DESART1 = .w_DESART
          .w_LIUNIMIS = NVL(LIUNIMIS,space(3))
          * evitabile
          *.link_2_12('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CALCPICT = DEFPIC(.w_DECUNI)
        .w_TIPOPE = this.cFunction
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_LICODLIS=space(5)
      .w_DTINVA=ctod("  /  /  ")
      .w_DTFINV=ctod("  /  /  ")
      .w_DESLIS=space(40)
      .w_VALLIS=space(3)
      .w_DECUNI=0
      .w_CALCPICT=0
      .w_IVALIS=space(1)
      .w_LIDATATT=ctod("  /  /  ")
      .w_LIDATDIS=ctod("  /  /  ")
      .w_TIPOPE=space(10)
      .w_LICODART=space(20)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESART1=space(40)
      .w_TIPART=space(2)
      .w_UNMIS1=space(3)
      .w_DESART=space(40)
      .w_FLSCO=space(1)
      .w_OK=.f.
      .w_UNMIS2=space(3)
      .w_CODART=space(10)
      .w_FLSERG=space(1)
      .w_LIUNIMIS=space(3)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_LICODLIS))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,5,.f.)
        if not(empty(.w_VALLIS))
         .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        .w_CALCPICT = DEFPIC(.w_DECUNI)
        .DoRTCalc(8,8,.f.)
        .w_LIDATATT = .w_DTINVA
        .w_LIDATDIS = .w_DTFINV
        .w_TIPOPE = this.cFunction
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_LICODART))
         .link_2_1('Full')
        endif
        .w_OBTEST = I_DATSYS
        .DoRTCalc(14,14,.f.)
        .w_DESART1 = .w_DESART
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .DoRTCalc(16,19,.f.)
        .w_OK = .T.
        .DoRTCalc(21,23,.f.)
        .w_LIUNIMIS = .w_UNMIS1
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_LIUNIMIS))
         .link_2_12('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'INS_LIST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsma_ml2
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oLICODLIS_1_1.enabled = i_bVal
      .Page1.oPag.oLIDATATT_1_9.enabled = i_bVal
      .Page1.oPag.oLIDATDIS_1_10.enabled = i_bVal
      .Page1.oPag.oObj_1_18.enabled = i_bVal
      .Page1.oPag.oObj_1_20.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oLICODLIS_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oLICODLIS_1_1.enabled = .t.
      endif
    endwith
    this.GSMA_MS2.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'INS_LIST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSMA_MS2.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INS_LIST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LICODLIS,"LICODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LIDATATT,"LIDATATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LIDATDIS,"LIDATDIS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INS_LIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INS_LIST_IDX,2])
    i_lTable = "INS_LIST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.INS_LIST_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_LICODART C(20);
      ,t_DESART1 C(40);
      ,t_DESART C(40);
      ,t_LIUNIMIS C(3);
      ,CPROWNUM N(10);
      ,t_DATOBSO D(8);
      ,t_TIPART C(2);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_CODART C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsma_ml2bodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLICODART_2_1.controlsource=this.cTrsName+'.t_LICODART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESART1_2_5.controlsource=this.cTrsName+'.t_DESART1'
    this.oPgFRm.Page1.oPag.oDESART_2_8.controlsource=this.cTrsName+'.t_DESART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLIUNIMIS_2_12.controlsource=this.cTrsName+'.t_LIUNIMIS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(161)
    this.AddVLine(375)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLICODART_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INS_LIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INS_LIST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INS_LIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INS_LIST_IDX,2])
      *
      * insert into INS_LIST
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INS_LIST')
        i_extval=cp_InsertValODBCExtFlds(this,'INS_LIST')
        i_cFldBody=" "+;
                  "(LICODLIS,LIDATATT,LIDATDIS,LICODART,LIUNIMIS,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_LICODLIS)+","+cp_ToStrODBC(this.w_LIDATATT)+","+cp_ToStrODBC(this.w_LIDATDIS)+","+cp_ToStrODBCNull(this.w_LICODART)+","+cp_ToStrODBCNull(this.w_LIUNIMIS)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INS_LIST')
        i_extval=cp_InsertValVFPExtFlds(this,'INS_LIST')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'LICODLIS',this.w_LICODLIS)
        INSERT INTO (i_cTable) (;
                   LICODLIS;
                  ,LIDATATT;
                  ,LIDATDIS;
                  ,LICODART;
                  ,LIUNIMIS;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_LICODLIS;
                  ,this.w_LIDATATT;
                  ,this.w_LIDATDIS;
                  ,this.w_LICODART;
                  ,this.w_LIUNIMIS;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.INS_LIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INS_LIST_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_LICODART)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'INS_LIST')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " LIDATATT="+cp_ToStrODBC(this.w_LIDATATT)+;
                 ",LIDATDIS="+cp_ToStrODBC(this.w_LIDATDIS)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'INS_LIST')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  LIDATATT=this.w_LIDATATT;
                 ,LIDATDIS=this.w_LIDATDIS;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_LICODART)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSMA_MS2.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_LICODLIS,"LICODLIS";
                     ,this.w_CPROWNUM,"LIROWNUM";
                     )
              this.GSMA_MS2.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update INS_LIST
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'INS_LIST')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " LIDATATT="+cp_ToStrODBC(this.w_LIDATATT)+;
                     ",LIDATDIS="+cp_ToStrODBC(this.w_LIDATDIS)+;
                     ",LICODART="+cp_ToStrODBCNull(this.w_LICODART)+;
                     ",LIUNIMIS="+cp_ToStrODBCNull(this.w_LIUNIMIS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'INS_LIST')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      LIDATATT=this.w_LIDATATT;
                     ,LIDATDIS=this.w_LIDATDIS;
                     ,LICODART=this.w_LICODART;
                     ,LIUNIMIS=this.w_LIUNIMIS;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (NOT EMPTY(t_LICODART))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSMA_MS2.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_LICODLIS,"LICODLIS";
               ,this.w_CPROWNUM,"LIROWNUM";
               )
          this.GSMA_MS2.mReplace()
          this.GSMA_MS2.bSaveContext=.f.
        endif
      endscan
     this.GSMA_MS2.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- gsma_ml2
    This.NotifyEvent('Inserisci')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INS_LIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INS_LIST_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_LICODART)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSMA_MS2 : Deleting
        this.GSMA_MS2.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_LICODLIS,"LICODLIS";
               ,this.w_CPROWNUM,"LIROWNUM";
               )
        this.GSMA_MS2.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete INS_LIST
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_LICODART)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INS_LIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INS_LIST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .link_1_5('Full')
        .DoRTCalc(6,6,.t.)
          .w_CALCPICT = DEFPIC(.w_DECUNI)
        .DoRTCalc(8,8,.t.)
        if .o_LICODLIS<>.w_LICODLIS
          .w_LIDATATT = .w_DTINVA
        endif
        if .o_LICODLIS<>.w_LICODLIS
          .w_LIDATDIS = .w_DTFINV
        endif
          .w_TIPOPE = this.cFunction
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .DoRTCalc(12,14,.t.)
          .w_DESART1 = .w_DESART
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .DoRTCalc(16,23,.t.)
        if .o_LICODART<>.w_LICODART
          .w_LIUNIMIS = .w_UNMIS1
          .link_2_12('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DATOBSO with this.w_DATOBSO
      replace t_TIPART with this.w_TIPART
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_CODART with this.w_CODART
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LICODLIS
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LICODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LICODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LICODLIS))
          select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LICODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_LICODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_LICODLIS)+"%");

            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_LICODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLICODLIS_1_1'),i_cWhere,'GSAR_ALI',"Listini",'listob1.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LICODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LICODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LICODLIS)
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LICODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_DTINVA = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_DTFINV = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_FLSCO = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LICODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_IVALIS = space(1)
      this.w_VALLIS = space(3)
      this.w_DTINVA = ctod("  /  /  ")
      this.w_DTFINV = ctod("  /  /  ")
      this.w_FLSCO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_dtfinv) OR .w_dtfinv>= i_datsys
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_LICODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_IVALIS = space(1)
        this.w_VALLIS = space(3)
        this.w_DTINVA = ctod("  /  /  ")
        this.w_DTFINV = ctod("  /  /  ")
        this.w_FLSCO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LICODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.LSCODLIS as LSCODLIS101"+ ",link_1_1.LSDESLIS as LSDESLIS101"+ ",link_1_1.LSIVALIS as LSIVALIS101"+ ",link_1_1.LSVALLIS as LSVALLIS101"+ ",link_1_1.LSDTINVA as LSDTINVA101"+ ",link_1_1.LSDTOBSO as LSDTOBSO101"+ ",link_1_1.LSFLSCON as LSFLSCON101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on INS_LIST.LICODLIS=link_1_1.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and INS_LIST.LICODLIS=link_1_1.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALLIS
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALLIS)
            select VACODVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALLIS = NVL(_Link_.VACODVAL,space(3))
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALLIS = space(3)
      endif
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LICODART
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LICODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_LICODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_LICODART))
          select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LICODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_LICODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_LICODART)+"%");

            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_LICODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oLICODART_2_1'),i_cWhere,'GSMA_BZA',"Articoli/servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LICODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_LICODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_LICODART)
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LICODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_CODART = NVL(_Link_.ARCODART,space(10))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LICODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_TIPART = space(2)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_CODART = space(10)
      this.w_FLSERG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo/servizio inesistente oppure obsoleto")
        endif
        this.w_LICODART = space(20)
        this.w_DESART = space(40)
        this.w_TIPART = space(2)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_UNMIS1 = space(3)
        this.w_UNMIS2 = space(3)
        this.w_CODART = space(10)
        this.w_FLSERG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LICODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.ARCODART as ARCODART201"+ ",link_2_1.ARDESART as ARDESART201"+ ",link_2_1.ARTIPART as ARTIPART201"+ ",link_2_1.ARDTOBSO as ARDTOBSO201"+ ",link_2_1.ARUNMIS1 as ARUNMIS1201"+ ",link_2_1.ARUNMIS2 as ARUNMIS2201"+ ",link_2_1.ARCODART as ARCODART201"+ ",link_2_1.ARFLSERG as ARFLSERG201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on INS_LIST.LICODART=link_2_1.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and INS_LIST.LICODART=link_2_1.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LIUNIMIS
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LIUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_LIUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_LIUNIMIS))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LIUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LIUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oLIUNIMIS_2_12'),i_cWhere,'',"",'GSMA1QUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LIUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_LIUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_LIUNIMIS)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LIUNIMIS = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_LIUNIMIS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUMLIS(.w_LICODART, IIF(.w_FLSERG='S', '***', .w_LIUNIMIS), .w_UNMIS1, .w_UNMIS2) AND NOT EMPTY(.w_LIUNIMIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_LIUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LIUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oLICODLIS_1_1.value==this.w_LICODLIS)
      this.oPgFrm.Page1.oPag.oLICODLIS_1_1.value=this.w_LICODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_4.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_4.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oVALLIS_1_5.value==this.w_VALLIS)
      this.oPgFrm.Page1.oPag.oVALLIS_1_5.value=this.w_VALLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oIVALIS_1_8.RadioValue()==this.w_IVALIS)
      this.oPgFrm.Page1.oPag.oIVALIS_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLIDATATT_1_9.value==this.w_LIDATATT)
      this.oPgFrm.Page1.oPag.oLIDATATT_1_9.value=this.w_LIDATATT
    endif
    if not(this.oPgFrm.Page1.oPag.oLIDATDIS_1_10.value==this.w_LIDATDIS)
      this.oPgFrm.Page1.oPag.oLIDATDIS_1_10.value=this.w_LIDATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_2_8.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_2_8.value=this.w_DESART
      replace t_DESART with this.oPgFrm.Page1.oPag.oDESART_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLICODART_2_1.value==this.w_LICODART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLICODART_2_1.value=this.w_LICODART
      replace t_LICODART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLICODART_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART1_2_5.value==this.w_DESART1)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART1_2_5.value=this.w_DESART1
      replace t_DESART1 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART1_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIUNIMIS_2_12.value==this.w_LIUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIUNIMIS_2_12.value=this.w_LIUNIMIS
      replace t_LIUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIUNIMIS_2_12.value
    endif
    cp_SetControlsValueExtFlds(this,'INS_LIST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_dtfinv) OR .w_dtfinv>= i_datsys)  and not(empty(.w_LICODLIS))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLICODLIS_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LIDATATT))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLIDATATT_1_9.SetFocus()
            i_bnoObbl = !empty(.w_LIDATATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LIDATDIS) or not(EMPTY(.w_LIDATDIS) OR .w_LIDATDIS>=.w_LIDATATT))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLIDATDIS_1_10.SetFocus()
            i_bnoObbl = !empty(.w_LIDATDIS)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsma_ml2
      this.NotifyEvent('Controlli')
      If Not this.w_OK
         i_bRes = .F.
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (NOT EMPTY(t_LICODART));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and not(empty(.w_LICODART)) and (NOT EMPTY(.w_LICODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLICODART_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Articolo/servizio inesistente oppure obsoleto")
        case   not(CHKUMLIS(.w_LICODART, IIF(.w_FLSERG='S', '***', .w_LIUNIMIS), .w_UNMIS1, .w_UNMIS2) AND NOT EMPTY(.w_LIUNIMIS)) and not(empty(.w_LIUNIMIS)) and (NOT EMPTY(.w_LICODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIUNIMIS_2_12
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
      endcase
      i_bRes = i_bRes .and. .GSMA_MS2.CheckForm()
      if NOT EMPTY(.w_LICODART)
        * --- Area Manuale = Check Row
        * --- gsma_ml2
        if NOT EMPTY(.w_LICODART) AND EMPTY(.w_LIUNIMIS)
           i_bRes = .f.
           i_bnoChk = .f.
        	 i_cErrorMsg = Ah_MsgFormat("Unit� di misura non definita")
        Endif
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LICODLIS = this.w_LICODLIS
    this.o_LICODART = this.w_LICODART
    * --- GSMA_MS2 : Depends On
    this.GSMA_MS2.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_LICODART))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_LICODART=space(20)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESART1=space(40)
      .w_TIPART=space(2)
      .w_UNMIS1=space(3)
      .w_DESART=space(40)
      .w_UNMIS2=space(3)
      .w_CODART=space(10)
      .w_LIUNIMIS=space(3)
      .DoRTCalc(1,12,.f.)
      if not(empty(.w_LICODART))
        .link_2_1('Full')
      endif
      .DoRTCalc(13,14,.f.)
        .w_DESART1 = .w_DESART
      .DoRTCalc(16,23,.f.)
        .w_LIUNIMIS = .w_UNMIS1
      .DoRTCalc(24,24,.f.)
      if not(empty(.w_LIUNIMIS))
        .link_2_12('Full')
      endif
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_LICODART = t_LICODART
    this.w_DATOBSO = t_DATOBSO
    this.w_DESART1 = t_DESART1
    this.w_TIPART = t_TIPART
    this.w_UNMIS1 = t_UNMIS1
    this.w_DESART = t_DESART
    this.w_UNMIS2 = t_UNMIS2
    this.w_CODART = t_CODART
    this.w_LIUNIMIS = t_LIUNIMIS
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_LICODART with this.w_LICODART
    replace t_DATOBSO with this.w_DATOBSO
    replace t_DESART1 with this.w_DESART1
    replace t_TIPART with this.w_TIPART
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_DESART with this.w_DESART
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_CODART with this.w_CODART
    replace t_LIUNIMIS with this.w_LIUNIMIS
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsma_ml2Pag1 as StdContainer
  Width  = 687
  height = 298
  stdWidth  = 687
  stdheight = 298
  resizeXpos=312
  resizeYpos=157
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLICODLIS_1_1 as StdField with uid="QDSQXJLMUA",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LICODLIS", cQueryName = "LICODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino",;
    HelpContextID = 63505161,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=63, Top=8, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LICODLIS"

  func oLICODLIS_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oLICODLIS_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLICODLIS_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oLICODLIS_1_1.readonly and this.parent.oLICODLIS_1_1.isprimarykey)
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLICODLIS_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'listob1.LISTINI_VZM',this.parent.oContained
   endif
  endproc
  proc oLICODLIS_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_LICODLIS
    i_obj.ecpSave()
  endproc

  add object oDESLIS_1_4 as StdField with uid="DGDRWKWRNY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del listino",;
    HelpContextID = 82379210,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=128, Top=8, InputMask=replicate('X',40)

  add object oVALLIS_1_5 as StdField with uid="NWRMSZUEAI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_VALLIS", cQueryName = "VALLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta listino",;
    HelpContextID = 82408618,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=504, Top=36, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALLIS"

  func oVALLIS_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oIVALIS_1_8 as StdCombo with uid="FXNBHZFTCS",rtseq=8,rtrep=.f.,left=601,top=34,width=67,height=21, enabled=.f.;
    , ToolTipText = "Se attivo: listino comprensivo di IVA";
    , HelpContextID = 82448506;
    , cFormVar="w_IVALIS",RowSource=""+"Lordo,"+"Netto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVALIS_1_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IVALIS,&i_cF..t_IVALIS),this.value)
    return(iif(xVal =1,'L',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oIVALIS_1_8.GetRadio()
    this.Parent.oContained.w_IVALIS = this.RadioValue()
    return .t.
  endfunc

  func oIVALIS_1_8.ToRadio()
    this.Parent.oContained.w_IVALIS=trim(this.Parent.oContained.w_IVALIS)
    return(;
      iif(this.Parent.oContained.w_IVALIS=='L',1,;
      iif(this.Parent.oContained.w_IVALIS=='N',2,;
      0)))
  endfunc

  func oIVALIS_1_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oLIDATATT_1_9 as StdField with uid="MDFCSKIXTS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_LIDATATT", cQueryName = "LIDATATT",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di entrata in vigore",;
    HelpContextID = 163255050,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=462, Top=8

  add object oLIDATDIS_1_10 as StdField with uid="WWZHSOMNNA",rtseq=10,rtrep=.f.,;
    cFormVar = "w_LIDATDIS", cQueryName = "LIDATDIS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine attivit� del listino",;
    HelpContextID = 213586697,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=601, Top=8

  func oLIDATDIS_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_LIDATDIS) OR .w_LIDATDIS>=.w_LIDATATT)
    endwith
    return bRes
  endfunc


  add object oObj_1_18 as cp_runprogram with uid="CCPCBFHIMB",left=-1, top=310, width=193,height=21,;
    caption='GSMA_BL3',;
   bGlobalFont=.t.,;
    prg="GSMA_BL3",;
    cEvent = "Inserisci",;
    nPag=1;
    , HelpContextID = 76829543


  add object oObj_1_20 as cp_runprogram with uid="FZOFYMCPEA",left=0, top=336, width=191,height=29,;
    caption='GSMA_BL4',;
   bGlobalFont=.t.,;
    prg="GSMA_BL4",;
    cEvent = "Controlli",;
    nPag=1;
    , HelpContextID = 76829542


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=58, width=418,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="LICODART",Label1="Articolo",Field2="DESART1",Label2="Descrizione",Field3="LIUNIMIS",Label3="U.M.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101494394

  add object oStr_1_11 as StdString with uid="USYEUFHIDD",Visible=.t., Left=8, Top=254,;
    Alignment=1, Width=71, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="JRECQEEXFW",Visible=.t., Left=544, Top=8,;
    Alignment=1, Width=55, Height=15,;
    Caption="Fino al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="TXZSBBFKNB",Visible=.t., Left=375, Top=8,;
    Alignment=1, Width=85, Height=15,;
    Caption="Valido dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="BXEUYPCXHX",Visible=.t., Left=-1, Top=8,;
    Alignment=1, Width=62, Height=15,;
    Caption="Listino:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="GDXLTQGXKH",Visible=.t., Left=372, Top=36,;
    Alignment=1, Width=130, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="MOGYYLFDKP",Visible=.t., Left=544, Top=36,;
    Alignment=1, Width=54, Height=15,;
    Caption="IVA:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsma_ms2",lower(this.oContained.GSMA_MS2.class))=0
        this.oContained.GSMA_MS2.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=77,;
    width=414+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=78,width=413+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='ART_ICOL|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESART_2_8.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='ART_ICOL'
        oDropInto=this.oBodyCol.oRow.oLICODART_2_1
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oLIUNIMIS_2_12
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_2 as stdDynamicChildContainer with uid="FUVRUWFUAI",bOnScreen=.t.,width=263,height=243,;
   left=428, top=58;


  add object oDESART_2_8 as StdTrsField with uid="HHOIOVXWXI",rtseq=18,rtrep=.t.,;
    cFormVar="w_DESART",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione dell'articolo",;
    HelpContextID = 56885706,;
    cTotal="", bFixedPos=.t., cQueryName = "DESART",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=82, Top=255, InputMask=replicate('X',40)

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsma_ml2BodyRow as CPBodyRowCnt
  Width=404
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oLICODART_2_1 as StdTrsField with uid="LMWMZGCEMH",rtseq=12,rtrep=.t.,;
    cFormVar="w_LICODART",value=space(20),;
    ToolTipText = "Codice articolo/servizio",;
    HelpContextID = 121044214,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Articolo/servizio inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=152, Left=-2, Top=0, cSayPict=[p_ART], cGetPict=[p_ART], InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_LICODART"

  func oLICODART_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oLICODART_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLICODART_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oLICODART_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli/servizi",'',this.parent.oContained
  endproc
  proc oLICODART_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_LICODART
    i_obj.ecpSave()
  endproc

  add object oDESART1_2_5 as StdTrsField with uid="JFHWSDZGAR",rtseq=15,rtrep=.t.,;
    cFormVar="w_DESART1",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione dell'articolo",;
    HelpContextID = 56885706,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=212, Left=151, Top=0, InputMask=replicate('X',40)

  add object oLIUNIMIS_2_12 as StdTrsField with uid="ZXRXHJNMIP",rtseq=24,rtrep=.t.,;
    cFormVar="w_LIUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura del listino",;
    HelpContextID = 85533449,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=365, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_LIUNIMIS"

  func oLIUNIMIS_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oLIUNIMIS_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLIUNIMIS_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oLIUNIMIS_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSMA1QUM.UNIMIS_VZM',this.parent.oContained
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oLICODART_2_1.When()
    return(.t.)
  proc oLICODART_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oLICODART_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=8
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_ml2','INS_LIST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LICODLIS=INS_LIST.LICODLIS";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
