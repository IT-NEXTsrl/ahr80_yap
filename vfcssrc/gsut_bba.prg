* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bba                                                        *
*              Archivia documento                                              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-16                                                      *
* Last revis.: 2017-10-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pArrayParam,pIdSerial
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bba",oParentObject,m.pArrayParam,m.pIdSerial)
return(i_retval)

define class tgsut_bba as StdBatch
  * --- Local variables
  pArrayParam = space(1)
  pORIGINE = space(2)
  pDOCUMENTO = space(250)
  pCLASSEDOCU = space(20)
  pUTENTE = 0
  pOBJATTRIBUTI = .NULL.
  pTABLENAME = space(20)
  pKEYRECORD = space(20)
  pDESCRIZIONE = space(200)
  pTESTO = space(0)
  pSILENTE = .f.
  pNoVisMsg = .f.
  pTIPOPER = space(1)
  pFLGFAE = space(1)
  pGestAtt = .NULL.
  pTipoAll = space(5)
  pClasAll = space(5)
  pPdProces = space(3)
  pQuemod = space(62)
  pOperazione = space(1)
  pIdSerial = space(15)
  pFIRMDIG = .f.
  pPathArc = space(254)
  pINFINITY = .f.
  pINFINITYEASY = .f.
  pIDTIPBST = space(1)
  pDataArch = ctod("  /  /  ")
  pOraArch = space(8)
  w_FLGFAE = space(1)
  w_TMPC = space(100)
  w_TMPT = space(1)
  w_TMPD = ctod("  /  /  ")
  w_ISALT = .f.
  w_VARTYPEOBJATTR = space(1)
  w_OBJATTRclass = space(10)
  w_PARENTCLASS = space(10)
  w_CODERRORE = 0
  w_RESULT = 0
  w_PATHBASE = space(100)
  w_CRITERIORAGGR = space(1)
  w_IDVALDAT = ctod("  /  /  ")
  w_IDVALNUM = 0
  w_CAMPORAGGR = space(100)
  w_PERCORSOARCH = space(100)
  w_DATARAGGR = ctod("  /  /  ")
  w_FOLDER = space(254)
  w_TIPOARCH = space(1)
  w_IDSERIAL = space(15)
  w_FILE = space(20)
  w_APPO = space(40)
  w_posizpunto = 0
  w_estensione = space(10)
  w_DESTINAZIONE = space(250)
  w_TmpPat = space(10)
  w_Ok = space(10)
  w_COUNTER = 0
  w_CLASSEDOCU = space(15)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_CDCODATT = space(15)
  w_CDATTINF = space(15)
  w_CDDESATT = space(40)
  w_CDCHKOBB = space(1)
  w_CDTIPATT = space(1)
  w_CDTABKEY = space(30)
  w_IDVALATT = space(100)
  w_CONTA = 0
  w_CONTASOS = 0
  w_CHIEDIATTRIBUTI = .f.
  w_CHKOBBL = .f.
  w_TIPOATTR = space(1)
  w_CDVLPRED = space(100)
  w_CDCAMCUR = space(100)
  w_CHKVALPRED = .f.
  w_VALPRED = space(100)
  w_VALATTR = space(100)
  w_GSUT_AID = .NULL.
  w_FORIGINE = space(100)
  w_CDMODALL = space(1)
  w_DATA_ARCH_DOC = ctod("  /  /  ")
  w_ORA_ARCH_DOC = space(8)
  w_INSPATIP = space(1)
  w_INSPACLA = space(1)
  w_CODTIPOL = space(5)
  w_CODCLASS = space(5)
  w_GESTEXT = space(5)
  w_CHKUNIVOC = .f.
  w_CLAALLE = space(15)
  w_CHIAVE = space(100)
  w_ARCHIVIO = space(15)
  w_MASK = .f.
  w_MID_APER = space(1)
  w_GESTIONE = .NULL.
  w_CLASSOS = space(1)
  w_STATODOC = space(18)
  w_IDFLHIDE = space(1)
  w_ALIASTAB = space(15)
  w_FLGDESC = space(1)
  w_DESCREC = space(40)
  w_CARORIG = 0
  w_EXMODALL = space(1)
  w_PATHPRA = space(254)
  w_NOCODAZ = .f.
  w_FILENAME = space(60)
  w_TIPOPERAZ = space(1)
  w_CLAPRA = space(1)
  w_ATTOBBL = space(1)
  w_TABRIFER = space(20)
  w_KEY_FIELDS = space(250)
  w_FIELDNAME = space(200)
  w_CursRead = space(10)
  w_IdxKey = 0
  w_CDERASEF = space(1)
  w_CDNOMFIL = space(254)
  w_CDTIPOBC = space(1)
  w_CDREPOBC = space(254)
  w_PUBWEB = space(1)
  w_COPATHDO = space(100)
  w_COPATHDS = space(100)
  w_DIRLAV = space(10)
  w_IDWEBFIL = space(254)
  w_DIMATT = 0
  w_OBJCTRL = .NULL.
  w_OSOURCE = .NULL.
  w_ASBAUTOM = space(1)
  w_ASNOREPS = space(1)
  w_pQuemod = space(60)
  w_IDTIPBST = space(1)
  w_CDPUBWEB = space(1)
  w_CDCLAINF = space(15)
  w_CODPRAT = space(15)
  w_ATTR_OK = .f.
  w_OLDAREA = 0
  w_FILENEW = space(20)
  w_PATHFILE = space(100)
  w_PATHSAVE = space(100)
  w_EXTGRF = space(6)
  w_PATH_KRF = space(254)
  w_PUBWEB = space(1)
  w_CNPUBWEB = space(1)
  w_FIL_BRQ = space(100)
  w_IDOGGETT = space(220)
  w_FILORIGI = space(100)
  w_QUERY = space(1)
  w_PROCOD = space(1)
  w_PDKEYPRO = space(100)
  w_PDCLADOC = space(15)
  w_ERRODIR = .f.
  w_FILENAMECRC = space(254)
  w_REPNAME = space(50)
  w_ExternalCode = space(50)
  w_PROTOCOLLO = space(40)
  w_MVSERIAL = space(10)
  w_MVNUMEST = 0
  w_MVALFEST = space(10)
  w_MVDATREG = ctod("  /  /  ")
  w_CURSEXT = .f.
  w_STAR = space(1)
  w_Nomefile = space(10)
  w_Pathfile = space(10)
  w_Nomefilebak = space(10)
  w_sNode = .NULL.
  w_XMLdscr = space(254)
  w_NATTRXML = 0
  w_INTESTAZIONE = space(254)
  w_OLDSETCENTURY = space(3)
  w_sCHILD = .NULL.
  w_sCHILDDETT = .NULL.
  w_sCHILDDETTPROP = .NULL.
  w_sCHILDvoid = .f.
  w_CDCREARA = space(1)
  w_MVTIPCON = space(1)
  w_MVCODCON = space(15)
  w_ANDESCRI = space(60)
  w_ANDESCR2 = space(60)
  w_ANINDIRI = space(35)
  w_ANINDIR2 = space(35)
  w_AN___CAP = space(9)
  w_ANLOCALI = space(30)
  w_ANPROVIN = space(2)
  w_ANNAZION = space(3)
  w_ANCODFIS = space(16)
  w_ANPARIVA = space(12)
  w_AN_EMAIL = space(254)
  w_ANPERFIS = space(1)
  w_ANTELEFO = space(18)
  w_ANNUMCEL = space(18)
  w_ANTELFAX = space(18)
  w_NFTEL = space(1)
  w_ANFLPRIV = space(1)
  w_ANCOGNOM = space(50)
  w_AN__NOME = space(50)
  w_AN_SESSO = space(1)
  w_ANDATNAS = ctod("  /  /  ")
  w_ANPRONAS = space(2)
  w_ANLOCNAS = space(30)
  w_ANFORGIU = space(3)
  w_ANCODCOM = space(50)
  w_QUERYMOD = space(60)
  w_COD_PRAT = space(15)
  w_DATA_ARCH = ctod("  /  /  ")
  w_TIPCBI = space(4)
  w_NON_INS_TIP_CLA = .f.
  w_SBIANCCLASS = space(1)
  * --- WorkFile variables
  PROMCLAS_idx=0
  PRODCLAS_idx=0
  PROMINDI_idx=0
  PRODINDI_idx=0
  EXT_ENS_idx=0
  CLA_ALLE_idx=0
  CAN_TIER_idx=0
  CONTROPA_idx=0
  TIP_ALLE_idx=0
  PAR_ALTE_idx=0
  DOC_MAST_idx=0
  PNT_MAST_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Componente di archiviazione
    * --- Per evitare che Painter usi this. utilizzando pArrayparam come carattere, utilizziamo pArrParam che sar� correttamente strutturato
    Private pArrParam
    Dimension pArrParam(30)
    * --- Lo statement ACOPY(pArrayParam , pArrParam) viene tradotto da Code Painter in ACOPY(this.pArrayParam , pArrParam)
    *     Essendo this.pArrayParam dichiarato come carattere, l'esecuzione d� errore. Si ricorre pertanto allo statement per evitare ogni tipo di anomalia
    w_Stmnt="ACOPY(pArrayParam , pArrParam)"
    &w_Stmnt
    * --- 1-Parametro indicante l'origine dell'archiviazione: Manuale (AR), da processo diocumentale (PR)
    this.pORIGINE = pArrParam(1)
    * --- 2-Parametro contenente il documento (file) da archiviare
    this.pDOCUMENTO = pArrParam(2)
    * --- 3-Parametro contenente la classe documentale da utilizzare per l'archiviazione
    this.pCLASSEDOCU = pArrParam(3)
    * --- 4-Codice dell'utente che esegue l'archiviazione (utente con cui si � entrati nella procedura)
    this.pUTENTE = pArrParam(4)
    * --- 5-Oggetto da cui estrarre i valori da assegnare agli attributi presenti nella classe documentale
    this.pOBJATTRIBUTI = pArrParam(5)
    * --- 6-Nome dell'archivio associato alla classe o alla gestione (es. ART_ICOL)
    this.pTABLENAME = pArrParam(6)
    * --- 7-Valore del campo chiave
    this.pKEYRECORD = pArrParam(7)
    * --- 8-Descrizione da riportare nel campo 'descrizione' dell'indice che verr� creato
    this.pDESCRIZIONE = pArrParam(8)
    * --- 9-Notazioni aggiuntive
    this.pTESTO = pArrParam(9)
    * --- 10-Indica se attivare o meno la modalit� silente
    this.pSILENTE = pArrParam(10)
    * --- 11-Permette di non visualizzare nessun messaggio: se lanciato da duplicazione pratiche siamo sotto transazione
    this.pNoVisMsg = pArrParam(11)
    * --- 12-Indica l'operazione da eseguire, valorizzato a 'D' solo nel caso in cui si debba generare un documento Word o Writer
    this.pTIPOPER = pArrParam(12)
    * --- 13-Se vale 'S' viene attivato il check: Indice utilizzato nel formato elettronico
    this.pFLGFAE = pArrParam(13)
    * --- 14-Riferimento alla gestione attiva (se presente)
    this.pGestAtt = pArrParam(14)
    * --- 15-Tipologia allegato
    this.pTipoAll = pArrParam(15)
    * --- 16-Classe allegato
    this.pClasAll = pArrParam(16)
    * --- 17-Tipo processo IRP  o no IRP
    this.pPdProces = pArrParam(17)
    * --- 18-Query associata al modello, solo in caso di creazione nuovo documento
    *     Non potendo aggiungere ulteriori parametri, si � scelto di concatenare in pQuemod anche la tipologia busta.
    *     I due elementi saranno separati dal carattere *
    this.pQuemod = pArrParam(18)
    * --- 19-Tipo operazione richiesta (A/C/S/D/N/U)
    *     A Invio mail con archiviazione
    *     C Cattura
    *     S Cattura da scanner
    *     D documento Word/Open Office
    *     U duplica
    *     N nuovo documento
    this.pOperazione = pArrParam(19)
    * --- 20-Seriale indice creato/modificato (Usato per firma digitale documento) inoltre viene riportato il percorso di archiviazione del file
    * --- 21-Parametro per creazione copia con firma digitale - Questo deve essere sempre l'ultimo parametro
    this.pFIRMDIG = pArrParam(21)
    * --- 22-Se presente contiene il percorso di archiviazione del documento 
    this.pPathArc = pArrParam(22)
    * --- 23-Se lanciato per pubblicazione Infinity
    this.pINFINITY = pArrParam(23)
    * --- 24-Per integrazione infinity easy quando lancio da print system
    this.pINFINITYEASY = pArrParam(24)
    * --- 25-Tipologia busta
    this.pIDTIPBST = pArrParam(25)
    * --- 26-Data archiviazione
    this.pDataArch = pArrParam(26)
    * --- 27-Ora archiviazione
    this.pOraArch = pArrParam(27)
    * --- Valorizza flag indice Formato elettronico se previsto
    this.w_FLGFAE = IIF(vartype(this.pFLGFAE)="C", this.pFLGFAE, "N")
    * --- Nuove variabili usate per gestire il path di archiviazione file nel caso in cui il modulo DOCM non � installato.
    *     In questo caso devo verificare se la classe in esame ha attivi i flag di aggiunta path per codice tipologia e/o
    *     classe allegato al path di archiviazione file.
    * --- Variabili per controllo univocit�
    this.w_MID_APER = "N"
    * --- Variabili per composizione nome file (solo per la modalit� copia)
    this.w_PATHPRA = " "
    this.w_DIMATT = 3
    this.w_ISALT = isalt()
    this.w_VARTYPEOBJATTR = vartype(this.pOBJATTRIBUTI)
    this.w_OBJATTRclass = iif(Type("This.pOBJATTRIBUTI.class") = "C", UPPER(this.pOBJATTRIBUTI.class), "XXXXXXXX")
    this.w_PARENTCLASS = iif(Type("This.oParentObject.class") = "C", UPPER(This.oParentObject.class), "XXXXXXXX")
    if VARTYPE(this.pIDTIPBST)="C"
      this.w_IDTIPBST = this.pIDTIPBST
    endif
    this.w_pQuemod = ""
    if VARTYPE(this.pQuemod)="C"
      this.w_pQuemod = this.pQuemod
    endif
    if VARTYPE(this.pDataArch)="D"
      this.w_DATA_ARCH_DOC = this.pDataArch
    else
      this.w_DATA_ARCH_DOC = i_DatSys
    endif
    if VARTYPE(this.pOraArch)="C"
      this.w_ORA_ARCH_DOC = this.pOraArch
    else
      this.w_ORA_ARCH_DOC = ""
    endif
    * --- Variabile per inserire o meno il codice azienda nel path di archiviazione, di default  viene inserito (.F.)
    this.w_NOCODAZ = .F.
    * --- Valorizzo la variabile in base al contenuto del parametro pGestAtt, per garantire la compatibilit� all'indietro
    this.w_GESTIONE = iif(vartype(this.pGestAtt) = "O", this.pGestAtt,this.oParentObject.oParentObject)
    * --- Variabile Risultato
    this.w_CODERRORE = 0
    * --- Variabile controllo univocit� allegato, richiesta dall'eventuale classe della tipologia allegato selezionata
    this.w_CHKUNIVOC = .T.
    * --- Variabile controllo oggetto chiamante, vale .t. solo nel caso in cui il chiamante � 'GSUT_KFG'
    this.w_MASK = .F.
    * --- =========================
    this.pDESCRIZIONE = IIF(vartype(this.pDESCRIZIONE)="C", this.pDESCRIZIONE, " ")
    this.pTESTO = IIF(vartype(this.pTESTO)="C", this.pTESTO, " ")
    this.pSILENTE = IIF(vartype(this.pSILENTE)="L", this.pSILENTE, .F.)
    if this.pOperazione="B" and len(this.pOperazione)>1
      this.w_ASBAUTOM = substr(this.pOperazione,2,1)
      * --- w_ASBAUTOM:
      *     'S': automatico - sys(2015)
      *     'N': manuale, automatico se non indicato
      *     'O': manuale obbligatorio
      if len(this.pOperazione)>2
        this.w_ASNOREPS = substr(this.pOperazione,3,1)
      endif
      this.pOperazione = "B"
    endif
    this.w_RESULT = 0
    do case
      case empty(this.pDOCUMENTO) or empty(this.pCLASSEDOCU) or this.pUTENTE=0
        * --- Parametri non corretti
      case not FILE(this.pDOCUMENTO)
        * --- Non esiste il file da archiviare !
      case SUBSTR( CHKPECLA( this.pCLASSEDOCU, this.pUTENTE), 2, 1 ) <> "S"
        * --- Utente senza i necessari diritti
    endcase
    * --- =======================================================================
    *       A R C H I V I A Z I O N E
    *     =======================================================================
    if this.w_RESULT=0
      this.w_IDFLHIDE = "N"
      * --- Legge da classe documentale i dati necessari
      * --- Read from PROMCLAS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PROMCLAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CDPATSTD,CDTIPRAG,CDCAMRAG,CDTIPARC,CDMODALL,CDTIPALL,CDCLAALL,CDCOMTIP,CDCOMCLA,CDCONSOS,CDALIASF,CDFLGDES,CDCLAPRA,CDRIFTAB,CDERASEF,CDNOMFIL,CDTIPOBC,CDREPOBC,CDPUBWEB,CDCLAINF"+;
          " from "+i_cTable+" PROMCLAS where ";
              +"CDCODCLA = "+cp_ToStrODBC(this.pCLASSEDOCU);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CDPATSTD,CDTIPRAG,CDCAMRAG,CDTIPARC,CDMODALL,CDTIPALL,CDCLAALL,CDCOMTIP,CDCOMCLA,CDCONSOS,CDALIASF,CDFLGDES,CDCLAPRA,CDRIFTAB,CDERASEF,CDNOMFIL,CDTIPOBC,CDREPOBC,CDPUBWEB,CDCLAINF;
          from (i_cTable) where;
              CDCODCLA = this.pCLASSEDOCU;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PATHBASE = NVL(cp_ToDate(_read_.CDPATSTD),cp_NullValue(_read_.CDPATSTD))
        this.w_CRITERIORAGGR = NVL(cp_ToDate(_read_.CDTIPRAG),cp_NullValue(_read_.CDTIPRAG))
        this.w_CAMPORAGGR = NVL(cp_ToDate(_read_.CDCAMRAG),cp_NullValue(_read_.CDCAMRAG))
        this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
        this.w_CDMODALL = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
        this.w_CODTIPOL = NVL(cp_ToDate(_read_.CDTIPALL),cp_NullValue(_read_.CDTIPALL))
        this.w_CODCLASS = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
        this.w_INSPATIP = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
        this.w_INSPACLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
        this.w_CLASSOS = NVL(cp_ToDate(_read_.CDCONSOS),cp_NullValue(_read_.CDCONSOS))
        this.w_ALIASTAB = NVL(cp_ToDate(_read_.CDALIASF),cp_NullValue(_read_.CDALIASF))
        this.w_FLGDESC = NVL(cp_ToDate(_read_.CDFLGDES),cp_NullValue(_read_.CDFLGDES))
        this.w_CLAPRA = NVL(cp_ToDate(_read_.CDCLAPRA),cp_NullValue(_read_.CDCLAPRA))
        this.w_TABRIFER = NVL(cp_ToDate(_read_.CDRIFTAB),cp_NullValue(_read_.CDRIFTAB))
        this.w_CDERASEF = NVL(cp_ToDate(_read_.CDERASEF),cp_NullValue(_read_.CDERASEF))
        this.w_CDNOMFIL = NVL(cp_ToDate(_read_.CDNOMFIL),cp_NullValue(_read_.CDNOMFIL))
        this.w_CDTIPOBC = NVL(cp_ToDate(_read_.CDTIPOBC),cp_NullValue(_read_.CDTIPOBC))
        this.w_CDREPOBC = NVL(cp_ToDate(_read_.CDREPOBC),cp_NullValue(_read_.CDREPOBC))
        this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
        this.w_CDCLAINF = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Verifico se la classe attuale � la classe di default per le pratiche (AET)
      if this.w_CLAPRA = "S"
        * --- Leggo le impostazioni inserite nella gestione delle tipologie e classi allegato, per quanto riguarda il completa path
        * --- Read from TIP_ALLE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_ALLE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_ALLE_idx,2],.t.,this.TIP_ALLE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TACOPTIP"+;
            " from "+i_cTable+" TIP_ALLE where ";
                +"TACODICE = "+cp_ToStrODBC(this.w_CODTIPOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TACOPTIP;
            from (i_cTable) where;
                TACODICE = this.w_CODTIPOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INSPATIP = NVL(cp_ToDate(_read_.TACOPTIP),cp_NullValue(_read_.TACOPTIP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from CLA_ALLE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CLA_ALLE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CLA_ALLE_idx,2],.t.,this.CLA_ALLE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TACOPCLA"+;
            " from "+i_cTable+" CLA_ALLE where ";
                +"TACODICE = "+cp_ToStrODBC(this.w_CODTIPOL);
                +" and TACODCLA = "+cp_ToStrODBC(this.w_CODCLASS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TACOPCLA;
            from (i_cTable) where;
                TACODICE = this.w_CODTIPOL;
                and TACODCLA = this.w_CODCLASS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INSPACLA = NVL(cp_ToDate(_read_.TACOPCLA),cp_NullValue(_read_.TACOPCLA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if this.w_ISALT and not(this.pOperazione$"D-N") and Not Empty(Nvl(this.pOperazione," "))
        * --- Leggo la modalit� di archiviazione inserita in 'Parametri generali'
        * --- Read from PAR_ALTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PATIPARC"+;
            " from "+i_cTable+" PAR_ALTE where ";
                +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PATIPARC;
            from (i_cTable) where;
                PACODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CDMODALL = NVL(cp_ToDate(_read_.PATIPARC),cp_NullValue(_read_.PATIPARC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if (this.w_OBJATTRclass="TGSUT_BIM" Or this.w_OBJATTRclass="TGSUT_BIS")
        this.w_CDMODALL = "L"
        this.w_IDFLHIDE = "S"
        if TYPE("This.oParentObject.w_CONFARC")<>"U"
          this.w_IDFLHIDE = IIF(This.oParentObject.w_CONFARC="S", "N", "S")
        endif
      endif
      if this.pOperazione$"D-N" or( type("this.pTipoAll")="C" AND NOT EMPTY(this.pTipoAll) )
        this.w_CODTIPOL = this.pTipoAll
        if this.pOperazione$"D-N" or (type("this.pClasAll")="C" AND NOT EMPTY(this.pClasAll))
          this.w_CODCLASS = this.pClasAll
        endif
        * --- Non inserisco eventuali path associati nella tipologia e classe allegati impostati nella classe documentale
        this.w_INSPATIP = "N"
        this.w_INSPACLA = "N"
      endif
      if this.w_OBJATTRclass="TGSUT_KSO" 
        if !this.w_ISALT
          * --- Mantengo modalit� letta dai parametri
          this.w_CDMODALL = IIF(this.pOBJATTRIBUTI.w_CDMODALL="S", "F", this.pOBJATTRIBUTI.w_CDMODALL)
        endif
        this.w_PATHBASE = this.pOBJATTRIBUTI.w_AEPATHFI
        this.w_NOCODAZ = .T.
      endif
      if this.w_OBJATTRclass="TGSUT_BSO"
        this.w_CDMODALL = IIF(this.pOBJATTRIBUTI.w_CDMODALL="S", "F", this.pOBJATTRIBUTI.w_CDMODALL)
        this.w_PATHBASE = this.pOBJATTRIBUTI.w_AEPATHFI
        this.w_INSPATIP = this.pOBJATTRIBUTI.w_INSPATIP
        this.w_INSPACLA = this.pOBJATTRIBUTI.w_INSPACLA
        this.w_CNPUBWEB = this.pOBJATTRIBUTI.w_PUBWEB
        if !Empty(this.pOBJATTRIBUTI.w_CNPATHFA)
          * --- Caso send to aggiungo al path indicato nella pratica per gli allegati il codice studio
          this.w_PATHBASE = this.pOBJATTRIBUTI.w_CNPATHFA
          this.w_NOCODAZ = .T.
        endif
      endif
      if this.w_OBJATTRclass="TGSUT_BLP"
        this.w_CDMODALL = "F"
      endif
      if this.pOperazione$"D-N" and type("this.pPathArc")="C" and not empty(this.pPathArc)
        * --- Sostituisco il path di archiviazione
        this.w_PATHBASE = AddBs(JustPath(this.pPathArc))
        * --- Leggo la modalit� di archiviazione inserita nei parametri generali AET
        if this.w_ISALT
          * --- Read from PAR_ALTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PATIPARC"+;
              " from "+i_cTable+" PAR_ALTE where ";
                  +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PATIPARC;
              from (i_cTable) where;
                  PACODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_EXMODALL = NVL(cp_ToDate(_read_.PATIPARC),cp_NullValue(_read_.PATIPARC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_NOCODAZ = .T.
      endif
      * --- Nel caso l'operazione di archiviazione sia fatta sulla gestione delle pratiche, verifico la presenza del percorso di archiviazione allegati
      if ! this.pOperazione$"D-N" and (vartype(this.ptablename)="C" and this.ptablename == "CAN_TIER") OR (type("this.oParentObject.oParentObject.w_CODPRAT") = "C")
        this.w_CODPRAT = iif(type("this.oParentObject.oParentObject.w_CODPRAT") = "C",this.oParentObject.oParentObject.w_CODPRAT,this.pkeyrecord)
      endif
      if Not empty(this.w_CODPRAT)
        * --- Leggo il percorso allegati presente sulla pratica in oggetto
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNPATHFA,CNPUBWEB"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_CODPRAT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNPATHFA,CNPUBWEB;
            from (i_cTable) where;
                CNCODCAN = this.w_CODPRAT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PATHPRA = NVL(cp_ToDate(_read_.CNPATHFA),cp_NullValue(_read_.CNPATHFA))
          this.w_CNPUBWEB = NVL(cp_ToDate(_read_.CNPUBWEB),cp_NullValue(_read_.CNPUBWEB))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if ! this.pOperazione$"D-N"
          if ! empty(this.w_PATHPRA)
            this.w_PATHBASE = addbs(this.w_PATHPRA)
            this.w_NOCODAZ = .T.
          endif
        endif
      endif
      if (this.w_PARENTCLASS="TGSUT_BFI" or this.pOperazione="C") and Not empty(this.pPatharc)
        this.w_PATHBASE = addbs(JUSTPATH(addbs(this.pPathArc)))
      endif
      if this.pOperazione = "A"
        this.w_NOCODAZ = .T.
        * --- Acquisisco il path di archiviazione inserito in GSUT_KAE da GSUT_KIM attraverso GSUT_BIM
        if type("this.oParentObject.oParentObject.w_AR__PATH") = "C" and ! empty(this.oParentObject.oParentObject.w_AR__PATH)
          this.w_PATHBASE = addbs(this.oParentObject.oParentObject.w_AR__PATH)
        endif
      endif
      if this.w_ISALT and this.pOperazione$"U-C-S"
        * --- Nel caso di archiviazione (cattura\Cattura da scanner)  e duplica non devo aggiungere al path di archiviazione il codice studio
        this.w_NOCODAZ = .T.
      endif
      * --- Trattamento Attributi
      if this.pOperazione="B" AND this.w_CDTIPOBC#"E-Q"
        this.w_DIMATT = 6
      endif
      * --- Una volta determinata la classe, occorre valorizzare gli attributi
      this.w_CHIEDIATTRIBUTI = (this.w_TIPOARCH = "M")
      this.w_CONTA = 1
      this.w_CONTASOS = 1
      if this.w_VARTYPEOBJATTR="O" and inlist(this.w_OBJATTRclass,"TGSUT_KFG" , "TGSUT_BIM", "TGSPR_BCO", "TGSUT_KSO") and not empty(this.pCLASSEDOCU)
        * --- L'array viene dichiarato per ovviare all'errore di variabile non trovata al successivo passo di lettura -
        *     L'errore si presenta nel caso in cui non � stata selezionata alcuna classe in GSUT_KFG
        dimension L_ARRAYATTRIBUTI(this.w_CONTA,this.w_DIMATT)
         
 oldCursor = Select()
        if this.w_OBJATTRclass="TGSUT_KFG" or this.w_OBJATTRclass="TGSUT_KSO"
          * --- Ricavo la chiave relativa alla tabella di riferimento
          if this.w_OBJATTRclass="TGSUT_KFG"
            this.pOBJATTRIBUTI.gsut_ma1.Exec_Select("TMP_ATTR", "t_CPROWORD,t_IDCODATT AS CDCODATT,t_IDDESATT AS CDDESATT,t_OPERAT3 AS OPERATORE,t_IDVALATT AS VALORE", "","", "", "", .f.)     
            NC="TMP_ATTR"
          else
            NC=this.pOBJATTRIBUTI.w_ZOOMSELE.cCursor
          endif
          this.w_KEY_FIELDS = ALLTRIM(cp_KeyToSQL(I_DCX.GetIdxDef(this.w_TABRIFER,1)))
          DIMENSION L_ArrValues( OCCURS(",", this.w_KEY_FIELDS )+1 ,2 )
          this.w_IdxKey = 1
           
 Select ((NC)) 
 Go Top
          SCAN
          * --- Modifico/Aggiorno la dimensione dell'array
          dimension L_ARRAYATTRIBUTI(this.w_CONTA,this.w_DIMATT)
          * --- L'attributo prende il valore specificato
          this.w_VALATTR = NVL(VALORE," ")
          this.w_CDCODATT = NVL(CDCODATT," ")
          * --- Leggo il valore di cprownum corrispondente al codice attributo letto direttamente dalla tabella,
          *     questo per ovviare a problemi nella fase successiva di inserimento attributi, dovuti ad una numerazione non consecutiva e lineare.
          * --- Read from PRODCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRODCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CPROWNUM,CDCHKOBB,CDCAMCUR,CDATTINF                                ,CDDESATT"+;
              " from "+i_cTable+" PRODCLAS where ";
                  +"CDCODCLA = "+cp_ToStrODBC(this.pCLASSEDOCU);
                  +" and CDCODATT = "+cp_ToStrODBC(this.w_CDCODATT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CPROWNUM,CDCHKOBB,CDCAMCUR,CDATTINF                                ,CDDESATT;
              from (i_cTable) where;
                  CDCODCLA = this.pCLASSEDOCU;
                  and CDCODATT = this.w_CDCODATT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
            this.w_ATTOBBL = NVL(cp_ToDate(_read_.CDCHKOBB),cp_NullValue(_read_.CDCHKOBB))
            this.w_FIELDNAME = NVL(cp_ToDate(_read_.CDCAMCUR),cp_NullValue(_read_.CDCAMCUR))
            this.w_CDATTINF = NVL(cp_ToDate(_read_.CDATTINF                                ),cp_NullValue(_read_.CDATTINF                                ))
            this.w_CDDESATT = NVL(cp_ToDate(_read_.CDDESATT),cp_NullValue(_read_.CDDESATT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Costruisco  il filtro da applicare alla tabella in base alla composizione della chiave pimaria
          this.w_FIELDNAME = alltrim(substr(this.w_FIELDNAME,1,200))
          if at(this.w_FIELDNAME,this.w_KEY_FIELDS) > 0
            L_ArrValues( this.w_IdxKey ,1 ) = this.w_FIELDNAME
            L_ArrValues( this.w_IdxKey ,2 ) = this.w_VALATTR
            this.w_IdxKey = this.w_IdxKey + 1
          endif
          if this.w_ATTOBBL="S" and empty(this.w_VALATTR)
            * --- Leggo direttamente sulla tabella associata alla classe il valore richiesto per il campo obbligatorio e non valorizzato nello zoom della maschera
            this.w_CursRead = ""
            this.w_CursRead = ReadTable(ALLTRIM(this.w_TABRIFER), this.w_FIELDNAME, @L_ArrValues, this.oParentObject, .F.)
            if !EMPTY(this.w_CursRead)
              val = alltrim(this.w_CursRead)+"."+this.w_FIELDNAME
              this.w_VALATTR = NVL(&val," ")
            endif
          endif
          L_ARRAYATTRIBUTI(this.w_CONTA,1) = iif(g_DMIP="S", nvl( this.w_CDATTINF, " "), this.pCLASSEDOCU)
          this.w_ATTR_OK = True
          if g_REVI="S" and not Empty(this.w_VALATTR) and this.w_CDMODALL="I"
            * --- Verifico se l'attributo � applicabile ed eseguo l'eventuale trascodifica (alla tipologia Stand Alone non si applica)
            this.w_VALATTR = InfinityAttribValue(L_ARRAYATTRIBUTI(this.w_CONTA,1), this.w_VALATTR)
            this.w_ATTR_OK = not Empty(this.w_VALATTR)
          endif
          if this.w_ATTR_OK
            L_ARRAYATTRIBUTI(this.w_CONTA,2) = this.w_CPROWNUM
            L_ARRAYATTRIBUTI(this.w_CONTA,3) = this.w_VALATTR
            this.w_TMPT = ALLTRIM(ICASE(VARTYPE(this.w_VALATTR)="C", this.w_VALATTR, VARTYPE(this.w_VALATTR)="T", DTOC(this.w_VALATTR), TRANSFORM(this.w_VALATTR)))
            this.w_FILE = ALLTRIM(this.w_FILE)+this.w_TMPT
            if this.w_DIMATT>3
              L_ARRAYATTRIBUTI(this.w_CONTA,4) = alltrim(iif(g_DMIP="S", nvl( this.w_CDATTINF, " "), this.w_CDCODATT))
              L_ARRAYATTRIBUTI(this.w_CONTA,5) = ALLTRIM(this.w_CDDESATT)
              L_ARRAYATTRIBUTI(this.w_CONTA,6) = ALLTRIM(ICASE(VARTYPE(this.w_VALATTR)="C", this.w_VALATTR, VARTYPE(this.w_VALATTR)="T", DTOC(this.w_VALATTR), TRANSFORM(this.w_VALATTR)))
            endif
            * --- Aggiorna Contatore
            this.w_CONTA = this.w_CONTA + 1
          else
            L_ARRAYATTRIBUTI(this.w_CONTA,1) = .F.
          endif
          ENDSCAN
          if Used("TMP_ATTR")
            Select TMP_ATTR 
 Use
          endif
        else
          * --- L'attributo prende il valore specificato
          this.w_VALATTR = this.pOBJATTRIBUTI.oParentObject.w_ARTABKEY
          this.w_CDCODATT = "CODICE"
          * --- Leggo il valore di cprownum corrispondente al codice attributo letto direttamente dalla tabella,
          *     questo per ovviare a problemi nella fase successiva di inserimento attributi, dovuti ad una numerazione non consecutiva e lineare.
          * --- Read from PRODCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRODCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CPROWNUM,CDATTINF"+;
              " from "+i_cTable+" PRODCLAS where ";
                  +"CDCODCLA = "+cp_ToStrODBC(this.pCLASSEDOCU);
                  +" and CDCODATT = "+cp_ToStrODBC(this.w_CDCODATT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CPROWNUM,CDATTINF;
              from (i_cTable) where;
                  CDCODCLA = this.pCLASSEDOCU;
                  and CDCODATT = this.w_CDCODATT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
            this.w_CDATTINF = NVL(cp_ToDate(_read_.CDATTINF),cp_NullValue(_read_.CDATTINF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          L_ARRAYATTRIBUTI(this.w_CONTA,1) = iif(g_DMIP="S", nvl( this.w_CDATTINF, " "), this.pCLASSEDOCU)
          this.w_ATTR_OK = True
          if g_REVI="S" and not Empty(this.w_VALATTR) and this.w_CDMODALL="I"
            * --- Verifico se l'attributo � applicabile ed eseguo l'eventuale trascodifica (alla tipologia Stand Alone non si applica)
            this.w_VALATTR = InfinityAttribValue(L_ARRAYATTRIBUTI(this.w_CONTA,1), this.w_VALATTR)
            this.w_ATTR_OK = not Empty(this.w_VALATTR)
          endif
          if this.w_ATTR_OK
            L_ARRAYATTRIBUTI(this.w_CONTA,2) = this.w_CPROWNUM
            L_ARRAYATTRIBUTI(this.w_CONTA,3) = this.w_VALATTR
            this.w_TMPT = ALLTRIM(ICASE(VARTYPE(this.w_VALATTR)="C", this.w_VALATTR, VARTYPE(this.w_VALATTR)="T", DTOC(this.w_VALATTR), TRANSFORM(this.w_VALATTR)))
            this.w_FILE = ALLTRIM(this.w_FILE)+this.w_TMPT
            if this.w_DIMATT>3
              L_ARRAYATTRIBUTI(this.w_CONTA,4) = alltrim(iif(g_DMIP="S", nvl( this.w_CDATTINF, " "), this.w_CDCODATT))
              L_ARRAYATTRIBUTI(this.w_CONTA,5) = ALLTRIM(this.w_CDDESATT)
              L_ARRAYATTRIBUTI(this.w_CONTA,6) = ALLTRIM(ICASE(VARTYPE(this.w_VALATTR)="C", this.w_VALATTR, VARTYPE(this.w_VALATTR)="T", DTOC(this.w_VALATTR), TRANSFORM(this.w_VALATTR)))
            endif
            * --- Aggiorna Contatore
            this.w_CONTA = this.w_CONTA + 1
            dimension L_ARRAYATTRIBUTI(this.w_CONTA,this.w_DIMATT)
          else
            L_ARRAYATTRIBUTI(this.w_CONTA,1) = .F.
          endif
          this.w_CDCODATT = "DESCRIZIONE"
          * --- Leggo il valore di cprownum corrispondente al codice attributo letto direttamente dalla tabella,
          *     questo per ovviare a problemi nella fase successiva di inserimento attributi, dovuti ad una numerazione non consecutiva e lineare.
          * --- Read from PRODCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRODCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CPROWNUM,CDATTINF"+;
              " from "+i_cTable+" PRODCLAS where ";
                  +"CDCODCLA = "+cp_ToStrODBC(this.pCLASSEDOCU);
                  +" and CDCODATT = "+cp_ToStrODBC(this.w_CDCODATT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CPROWNUM,CDATTINF;
              from (i_cTable) where;
                  CDCODCLA = this.pCLASSEDOCU;
                  and CDCODATT = this.w_CDCODATT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
            this.w_CDATTINF = NVL(cp_ToDate(_read_.CDATTINF),cp_NullValue(_read_.CDATTINF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_VALATTR = this.pOBJATTRIBUTI.oParentObject.w_ARDESKEY
          L_ARRAYATTRIBUTI(this.w_CONTA,1) = iif(g_DMIP="S", nvl( this.w_CDATTINF, " "), this.pCLASSEDOCU)
          L_ARRAYATTRIBUTI(this.w_CONTA,2) = this.w_CPROWNUM
          L_ARRAYATTRIBUTI(this.w_CONTA,3) = this.w_VALATTR
          if this.w_DIMATT>3
            L_ARRAYATTRIBUTI(this.w_CONTA,4) = alltrim(iif(g_DMIP="S", nvl( this.w_CDATTINF, " "), this.w_CDCODATT))
            L_ARRAYATTRIBUTI(this.w_CONTA,5) = ALLTRIM(this.w_CDDESATT)
            L_ARRAYATTRIBUTI(this.w_CONTA,6) = this.w_TMPT
          endif
        endif
        this.w_MASK = .T.
         
 Select (oldCursor)
      else
        * --- Select from PRODCLAS
        i_nConn=i_TableProp[this.PRODCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" PRODCLAS ";
              +" where CDCODCLA = "+cp_ToStrODBC(this.pCLASSEDOCU)+" and   CDFLGSOS not in ('HS', 'NM', 'SI', 'CS')";
               ,"_Curs_PRODCLAS")
        else
          select * from (i_cTable);
           where CDCODCLA = this.pCLASSEDOCU and   CDFLGSOS not in ("HS", "NM", "SI", "CS");
            into cursor _Curs_PRODCLAS
        endif
        if used('_Curs_PRODCLAS')
          select _Curs_PRODCLAS
          locate for 1=1
          do while not(eof())
          * --- Attributo obbligatorio ?!?
          this.w_CHKOBBL = ( NVL(_Curs_PRODCLAS.CDCHKOBB, "N") = "S" )
          this.w_TIPOATTR = nvl(_Curs_PRODCLAS.CDTIPATT, " ")
          this.w_CDVLPRED = nvl(_Curs_PRODCLAS.CDVLPRED, " ")
          if empty( this.w_CDVLPRED )
            this.w_CHKVALPRED = .F.
            this.w_VALPRED = IIF(this.w_TIPOATTR="N",0,IIF(this.w_TIPOATTR="D",{}," "))
          else
            this.w_CHKVALPRED = .T.
            this.w_VALPRED = eval(this.w_CDVLPRED)
          endif
          * --- Prova a determinare il valore dell'attributo
          w_ERRORE = .F.
          this.w_CDCAMCUR = nvl(_Curs_PRODCLAS.CDCAMCUR," ")
          if empty(this.w_CDCAMCUR)
            * --- L'attributo prende il valore predefinito
            this.w_VALATTR = this.w_VALPRED
          else
            * --- Legge il dato dalle propriet� o dal cursore padre
            if this.pOperazione="B" AND inlist(this.pTABLENAME,"DOC_MAST","PNT_MAST") and inlist(upper(alltrim(this.w_CDCAMCUR)),"MVSERIAL","PNSERIAL")
              this.w_MVSERIAL = "0000000000"
            endif
            this.w_VALATTR = ""
            w_ErrorHandler = on("ERROR")
            on error w_ERRORE = .T.
            do case
              case this.w_VARTYPEOBJATTR="C"
                * --- Salvo area di lavoro altrimenti la use del painter chiude il cursore
                this.w_OLDAREA = SELECT()
                if (this.w_PARENTCLASS="TGSAL_BXL" or this.w_PARENTCLASS="TGSAL1BXL")
                  this.w_VALATTR = this.pOBJATTRIBUTI
                else
                  select (this.pOBJATTRIBUTI)
                  * --- Se sono fuori dal cursore mi riposiziono sulla prima riga
                  if EOF()
                    Go Top
                  endif
                  this.w_VALATTR = eval(this.w_CDCAMCUR)
                endif
                * --- Ripristino la vecchia area di lavoro
                SELECT(this.w_OLDAREA)
              case this.w_VARTYPEOBJATTR="O"
                * --- Salvo area di lavoro altrimenti la use del painter chiude il cursore
                this.w_OLDAREA = SELECT()
                if at("+",this.w_CDCAMCUR)>0 or at("(",this.w_CDCAMCUR)>0
                  select (this.pOBJATTRIBUTI.cCursor)
                else
                  this.w_CDCAMCUR = "this.pOBJATTRIBUTI.w_" + alltrim(this.w_CDCAMCUR)
                endif
                this.w_VALATTR = eval(this.w_CDCAMCUR)
                * --- Ripristino la vecchia area di lavoro
                SELECT(this.w_OLDAREA)
              otherwise
                this.w_VALATTR = IIF(this.w_TIPOATTR="N",0,IIF(this.w_TIPOATTR="D",{}," "))
            endcase
            on error &w_ErrorHandler
          endif
          if this.pOperazione="B" AND inlist(this.pTABLENAME,"DOC_MAST","PNT_MAST") and this.w_MVSERIAL="0000000000"
            this.w_MVSERIAL = this.w_VALATTR
          endif
          * --- L'attributo � stato valorizzato adeguatamente ?!?!
          this.w_VALATTR = IIF(empty(this.w_VALATTR), this.w_VALPRED, this.w_VALATTR)
          if empty(this.w_VALATTR) and this.w_CHKOBBL
            this.w_CHIEDIATTRIBUTI = .T.
          endif
          * --- Per sicurezza, ritipizzo e pulisco valori null
          this.w_VALATTR = IIF(isnull(this.w_VALATTR), IIF(this.w_TIPOATTR="N",0,IIF(this.w_TIPOATTR="D",{}," ")), this.w_VALATTR)
          * --- Controlla Tipo Attributo:
          this.w_TMPT = VarType(this.w_VALATTR)
          this.w_TMPT = IIF(this.w_TMPT="M","C",IIF(this.w_TMPT="T","D",IIF(this.w_TMPT="Y","N",this.w_TMPT)))
          w_ERRORE = w_ERRORE OR ( NVL(_Curs_PRODCLAS.CDTIPATT," ") <> this.w_TMPT )
          * --- Dimensiona adeguatamente array attributi
          if w_ERRORE and this.pOperazione<>"I"
            if this.w_CHKOBBL
              this.w_CODERRORE = 9
              if this.pORIGINE<>"PR" and not this.pSILENTE
                ah_ERRORMSG("Impossibile archiviare il documento%3%3Classe documentale: %1%3Errore nella valorizzazione dell'attributo %2",16,"",alltrim(_Curs_PRODCLAS.CDCODCLA),alltrim(str(_Curs_PRODCLAS.CPROWORD,3,0)),CHR(13))
              endif
              exit
            else
              if this.pORIGINE<>"PR" and not this.pSILENTE
                ah_ERRORMSG("Classe documentale: %1%3Errore nella valorizzazione dell'attributo %2","!","",alltrim(_Curs_PRODCLAS.CDCODCLA),alltrim(str(_Curs_PRODCLAS.CPROWORD,3,0)),CHR(13))
              endif
            endif
          else
            dimension L_ARRAYATTRIBUTI(this.w_CONTA,this.w_DIMATT)
            L_ARRAYATTRIBUTI(this.w_CONTA,1) = iif(g_DMIP="S", nvl( _Curs_PRODCLAS.CDATTINF, " "), nvl( _Curs_PRODCLAS.CDCODCLA, " "))
            this.w_ATTR_OK = True
            if g_REVI="S" and not Empty(this.w_VALATTR) and this.w_CDMODALL="I"
              * --- Verifico se l'attributo � applicabile ed eseguo l'eventuale trascodifica (alla tipologia Stand Alone non si applica)
              this.w_VALATTR = InfinityAttribValue(L_ARRAYATTRIBUTI(this.w_CONTA,1), this.w_VALATTR)
              this.w_ATTR_OK = not Empty(this.w_VALATTR)
            endif
            if this.w_ATTR_OK
              L_ARRAYATTRIBUTI(this.w_CONTA,2) = nvl( _Curs_PRODCLAS.CPROWNUM,0)
              L_ARRAYATTRIBUTI(this.w_CONTA,3) = this.w_VALATTR
              this.w_TMPT = ALLTRIM(ICASE(VARTYPE(this.w_VALATTR)="C", this.w_VALATTR, VARTYPE(this.w_VALATTR)="T", DTOC(this.w_VALATTR), TRANSFORM(this.w_VALATTR)))
              this.w_FILE = ALLTRIM(this.w_FILE)+this.w_TMPT
              if this.w_DIMATT>3
                L_ARRAYATTRIBUTI(this.w_CONTA,4) = alltrim(iif(g_DMIP="S", _Curs_PRODCLAS.CDATTINF, _Curs_PRODCLAS.CDCODATT))
                L_ARRAYATTRIBUTI(this.w_CONTA,5) = ALLTRIM(_Curs_PRODCLAS.CDDESATT)
                L_ARRAYATTRIBUTI(this.w_CONTA,6) = ALLTRIM(ICASE(VARTYPE(this.w_VALATTR)="C", this.w_VALATTR, VARTYPE(this.w_VALATTR)="T", DTOC(this.w_VALATTR), TRANSFORM(this.w_VALATTR)))
              endif
              * --- Aggiorna Contatore
              this.w_CONTA = this.w_CONTA + 1
            else
              L_ARRAYATTRIBUTI(this.w_CONTA,1) = .F.
            endif
          endif
            select _Curs_PRODCLAS
            continue
          enddo
          use
        endif
      endif
      * --- Inizio ...
      if this.w_CODERRORE=0
        if this.pORIGINE<>"PR" AND (this.pOperazione=="I" OR g_DMIP="S" AND NOT this.pINFINITYEASY)
          INFINITYOPEN (this,this.pOperazione, this.w_CONTA, this.pCLASSEDOCU)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Read from CONTROPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTROPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "COPATHDO,COPATHDS"+;
              " from "+i_cTable+" CONTROPA where ";
                  +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              COPATHDO,COPATHDS;
              from (i_cTable) where;
                  COCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_COPATHDO = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
            this.w_COPATHDS = NVL(cp_ToDate(_read_.COPATHDS),cp_NullValue(_read_.COPATHDS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Aggiunge backslash finale
          this.w_COPATHDO = ADDBS(iif(this.w_CDMODALL="S", this.w_COPATHDS, this.w_COPATHDO))
          if this.pOperazione="B" 
            if g_DMIP="S" AND this.w_CDMODALL<>"S"
              this.w_COPATHDO = ADDBS( ADDBS(JUSTPATH(this.pDOCUMENTO)) + SYS(2015) )
            endif
          else
            if g_DMIP="S" AND this.w_CDMODALL<>"S" AND (this.pORIGINE = "PR" OR this.pINFINITY)
              do case
                case not empty(this.oParentObject.w_UploadDocumentsXML)
                  this.w_COPATHDO = ADDBS(JUSTPATH(this.oParentObject.w_UploadDocumentsXML))
                case this.pORIGINE = "PR"
                  w_ERRORE = .F.
                  if DIRECTORY(ADDBS(this.w_COPATHDO+alltrim(this.oParentObject.w_CODPROC)+"\"+alltrim(this.oParentObject.pKEYISTANZA)))
                    w_ERRORE = not DELETEFOLDER(ADDBS(this.w_COPATHDO+alltrim(this.oParentObject.w_CODPROC)+"\"+alltrim(this.oParentObject.pKEYISTANZA)))
                  endif
                  if not w_ERRORE
                    w_ErrorHandler = on("ERROR")
                    on error w_ERRORE = .T.
                    MKDIR ADDBS(this.w_COPATHDO+alltrim(this.oParentObject.w_CODPROC)+"\"+alltrim(this.oParentObject.pKEYISTANZA))
                    on error &w_ErrorHandler
                  endif
                  if w_ERRORE OR NOT DIRECTORY(ADDBS(this.w_COPATHDO+alltrim(this.oParentObject.w_CODPROC)+"\"+alltrim(this.oParentObject.pKEYISTANZA)))
                    AH_ERRORMSG("Impossibile utilizzare la cartella <%1> per la preparazione dei file per l'archiviazione su Infinity D.M.S.",48,"",ADDBS(this.w_COPATHDO+alltrim(this.oParentObject.w_CODPROC)+"\"+alltrim(this.oParentObject.pKEYISTANZA)))
                  endif
                  if NOT w_ERRORE
                    this.w_COPATHDO = ADDBS(this.w_COPATHDO+alltrim(this.oParentObject.w_CODPROC)+"\"+alltrim(this.oParentObject.pKEYISTANZA))
                  endif
                otherwise
                  this.w_COPATHDO = ADDBS(this.w_COPATHDO+SYS(2015))
              endcase
            endif
          endif
          * --- Valuta campo di raggruppamento
          do case
            case this.w_CDMODALL="F"
              if empty(this.w_CAMPORAGGR)
                this.w_DATARAGGR = i_DATSYS
              else
                * --- Legge il dato dalle propriet� o dal cursore padre
                do case
                  case this.w_VARTYPEOBJATTR="C" 
                    if (this.w_PARENTCLASS="TGSAL_BXL" or this.w_PARENTCLASS="TGSAL1BXL")
                      * --- Select from Query\gsut_bba
                      do vq_exec with 'Query\gsut_bba',this,'_Curs_Query_gsut_bba','',.f.,.t.
                      if used('_Curs_Query_gsut_bba')
                        select _Curs_Query_gsut_bba
                        locate for 1=1
                        do while not(eof())
                        this.w_DATARAGGR = CAMPO
                          select _Curs_Query_gsut_bba
                          continue
                        enddo
                        use
                      endif
                    else
                      select (this.pOBJATTRIBUTI)
                      this.w_TMPC = this.w_CAMPORAGGR
                      this.w_DATARAGGR = eval(this.w_TMPC)
                    endif
                  case this.w_VARTYPEOBJATTR="O" and (this.w_OBJATTRclass<>"TGSUT_KFG" and this.w_OBJATTRclass<>"TGSUT_KSO")
                    this.w_TMPC = "this.pObjAttributi.w_"+alltrim(this.w_CAMPORAGGR)
                    this.w_DATARAGGR = eval(this.w_TMPC)
                  otherwise
                    this.w_DATARAGGR = i_DATSYS
                endcase
              endif
              this.w_DATARAGGR = IIF(EMPTY(cp_todate(this.w_DATARAGGR)), i_INIDAT, this.w_DATARAGGR)
              if (g_CPIN="S" or g_REVI="S") AND this.pINFINITY And not(this.oParentObject.w_INARCHIVIO$"SX")
                this.w_PERCORSOARCH = ADDBS(this.w_COPATHDO)
              else
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            otherwise
              this.w_DATARAGGR = i_DATSYS
              if this.pOperazione="P" 
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                this.w_PERCORSOARCH = ""
                this.Page_2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
          endcase
          if NOT((g_CPIN="S" or g_REVI="S") AND TYPE("this.oParentObject.w_INARCHIVIO")="C" AND this.oParentObject.w_INARCHIVIO<>"S" AND this.oParentObject.w_INARCHIVIO<>"X")
            if this.w_CDMODALL="F" and (.not.directory(alltrim( this.w_PERCORSOARCH )))
              * --- Creazione directory immagini (LA GENERALE)
              this.w_FOLDER = left(this.w_PERCORSOARCH , len(this.w_PERCORSOARCH )-1)
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Determina il nome del file <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            if this.pORIGINE = "AR"
              * --- Prenota seriale per archivio indici documenti
              this.w_IDSERIAL = space(20)
              * --- w_CDMODALL="S" => INFINITY STAND ALONE
              if g_DMIP<>"S" AND this.w_CDMODALL<>"S"
                this.w_IDSERIAL = cp_GetProg("PROMINDI","SEIDO",this.w_IDSERIAL,i_CODAZI)
              endif
              if g_CPIN="S" And TYPE("this.oParentObject.w_INARCHIVIO")="C" AND this.oParentObject.w_INARCHIVIO="S" and !this.w_ISALT
                this.w_FILE = alltrim(this.w_IDSERIAL) + "_" +IIF(empty(this.pDOCUMENTO), " ", substr(this.pDOCUMENTO ,RAT("\", this.pDOCUMENTO)+1,250))
                * --- elimina estensione
                this.w_FILE = JUSTSTEM(this.w_FILE)
              else
                if ! this.pFIRMDIG
                  if type("w_GESTIONE.class") <>"O" or not UPPER(this.w_GESTIONE.class)="TGSIR_BGD"
                    if this.w_CDMODALL="F"
                      * --- caso copia file
                      if ((this.w_EXMODALL="L" and this.pOperazione$"D-N" ) OR (this.w_PARENTCLASS="TGSUT_BFI")) and type("this.pPathArc")="C" and not empty(JustStem(this.pPathArc))
                        this.w_FILE = JUSTSTEM(this.pPathArc)
                      else
                        * --- Nome file � composto da:
                        *          - Seriale indice;
                        *          - Nome archivio (tabella fisica) o stringa inserita nella classe documentale (solo in Alterego);
                        *          - Codice chiave record a cui viene associato il file o la sua descrizione (solo in Alterego).
                        if this.w_ISALT
                          * --- Eseguo la sostituzione dei caratteri per i quali non � possibile salvare il nome di un file. 
                          *     Nella parte relativa alla descrizione, terza sezione del nome file
                          *     I caratteri non ammessi sono: ^\/:*?"<>|&.'
                          * --- Il check � attivabile solo per le classi documentali definite sull'archivio CAN_TIER
                          do case
                            case this.w_FLGDESC $ "S-U"
                              * --- Per descrizione\descrizione + utente
                              * --- Read from CAN_TIER
                              i_nOldArea=select()
                              if used('_read_')
                                select _read_
                                use
                              endif
                              i_nConn=i_TableProp[this.CAN_TIER_idx,3]
                              i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
                              if i_nConn<>0
                                cp_sqlexec(i_nConn,"select "+;
                                  "CNDESCAN"+;
                                  " from "+i_cTable+" CAN_TIER where ";
                                      +"CNCODCAN = "+cp_ToStrODBC(this.pKEYRECORD);
                                       ,"_read_")
                                i_Rows=iif(used('_read_'),reccount(),0)
                              else
                                select;
                                  CNDESCAN;
                                  from (i_cTable) where;
                                      CNCODCAN = this.pKEYRECORD;
                                   into cursor _read_
                                i_Rows=_tally
                              endif
                              if used('_read_')
                                locate for 1=1
                                this.w_DESCREC = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
                                use
                              else
                                * --- Error: sql sentence error.
                                i_Error = MSG_READ_ERROR
                                return
                              endif
                              select (i_nOldArea)
                              if this.w_FLGDESC = "U"
                                this.w_DESCREC = alltrim(this.w_DESCREC) + "_" + Alltrim(str(i_codute))
                              endif
                              this.w_CARORIG = len(alltrim(this.w_DESCREC))
                              this.w_DESCREC = CHRTRAN(this.w_DESCREC,'^\/:*?"<>|&.'+"' ","----------------")
                              this.w_DESCREC = left(this.w_DESCREC,this.w_CARORIG)
                            case this.w_FLGDESC $ "C-T"
                              * --- Per codice\codice + utente
                              this.w_DESCREC = alltrim(this.pKEYRECORD)
                              if this.w_FLGDESC = "T"
                                this.w_DESCREC = alltrim(this.w_DESCREC) + "_" + Alltrim(str(i_codute))
                              endif
                            case this.w_FLGDESC = "K"
                              this.w_DESCREC = Alltrim(str(i_codute))
                            otherwise
                              * --- Nessuno
                              this.w_DESCREC = ""
                          endcase
                          this.w_FILE = alltrim(this.w_IDSERIAL) + "_" +alltrim(this.w_ALIASTAB) + iif(Empty(this.w_DESCREC),"", "_" + this.w_DESCREC)
                          if !empty(this.pDOCUMENTO) AND UPPER(JUSTEXT(this.pDocumento))="P7M" AND !EMPTY(JUSTEXT(JUSTSTEM(this.pDocumento)))
                            * --- Stiamo copiando un documento nella forma .estensione.P7M, dobbiamo mantenere nel nome la doppia estensione
                            this.w_FILE = this.w_FILE+"."+JUSTEXT(JUSTSTEM(this.pDocumento))
                          endif
                        else
                          this.w_FILE = alltrim(this.w_IDSERIAL) + "_" + alltrim(this.pTABLENAME) +"_" + alltrim(this.pKEYRECORD)
                        endif
                        if this.pOperazione$"D-N" and empty(JustStem(this.pPathArc))
                          this.w_FILENAME = this.w_FILE
                        endif
                      endif
                    else
                      * --- caso collegamento
                      if Not empty(this.pDOCUMENTO)
                        * --- File nuovo da creare\duplicare
                        * --- Path presente nel file
                        * --- Path di salvataggio\duplicazione del file
                        if this.pOperazione $ "U-P"
                          this.w_estensione = Justext(this.pDOCUMENTO)
                          this.w_FILE = substr(this.pDOCUMENTO ,RAT("\", this.pDOCUMENTO)+1,250)
                          this.w_PATHFILE = ADDBS(Alltrim(JUSTPATH(this.pDOCUMENTO)))
                          if this.pOperazione $ "U-P" and this.w_ISALT and !(this.w_PATHBASE== this.w_PATHFILE)
                            * --- Ho cambiato pratica in fase di duplicazione
                            this.w_PATHSAVE = ADDBS(this.w_PATHBASE)
                          else
                            this.w_PATHSAVE = this.w_PATHFILE
                          endif
                          this.w_FOLDER = Fullpath(this.w_PATHSAVE)
                          if .not.directory(alltrim(this.w_folder))
                            this.Page_4()
                            if i_retcode='stop' or !empty(i_Error)
                              return
                            endif
                          endif
                          this.w_FILENEW = ADDBS(Alltrim(this.w_PATHSAVE))+Alltrim(this.w_FILE)
                          this.w_FILENAME = Alltrim(this.w_FILE)
                           
 w_ERRORE = .F. 
 w_ErrorHandler = on("ERROR") 
 on error w_ERRORE = .T.
                          if cp_fileexist(this.w_FILENEW)
                            this.w_PATH_KRF = Alltrim(Left(this.pdocumento,AT("\",this.PDOCUMENTO,OCCUR("\",this.PDOCUMENTO))))
                            if this.w_PARENTCLASS="TGSUT_BFI"
                              this.w_TIPOPERAZ = "S"
                            else
                              do GSUT_KRF with this
                              if i_retcode='stop' or !empty(i_Error)
                                return
                              endif
                            endif
                            if this.w_TIPOPERAZ="S"
                               
 copy file (this.pDOCUMENTO) to (this.w_FILENEW)
                            else
                              if empty(this.w_FILENAME) or cp_fileexist(Alltrim(Left(this.pdocumento,AT("\",this.PDOCUMENTO,OCCUR("\",this.PDOCUMENTO))))+Alltrim(this.w_FILENAME)) 
                                Ah_ErrorMsg("Operazione annullata",48,"")
                                on error &w_ErrorHandler
                                i_retcode = 'stop'
                                return
                              else
                                this.w_FILE = this.w_FILENAME
                              endif
                               
 copy file (this.pDOCUMENTO) to (Alltrim(this.w_PATHSAVE)+this.w_FILENAME) 
                            endif
                          else
                             
 copy file (this.pDOCUMENTO) to (this.w_FILENEW) 
                          endif
                          on error &w_ErrorHandler
                          if this.pOperazione $ "U-P" and this.w_ISALT and !(this.w_PATHBASE== this.w_PATHFILE )
                            * --- Ho cambiato pratica in fase di duplicazione
                            this.pDOCUMENTO = Alltrim(this.w_PATHSAVE)+justfname(this.pDOCUMENTO)
                          endif
                        else
                          this.w_FILE = substr(this.pDOCUMENTO ,RAT("\", this.pDOCUMENTO)+1,250)
                        endif
                      else
                        this.w_FILE = " "
                      endif
                      * --- elimina estensione
                      if RAT(".", this.w_FILE)>0
                        this.w_FILE = JUSTSTEM(this.w_FILE)
                      endif
                    endif
                  else
                    if (this.pOperazione$"D-N" OR (this.w_PARENTCLASS="TGSUT_BFI")) and type("this.pPathArc")="C" and not empty(JustStem(this.pPathArc))
                      this.w_FILE = JUSTSTEM(this.pPathArc)
                    else
                      this.w_FILE = alltrim(this.w_IDSERIAL) + "_" + alltrim(this.pTABLENAME) +"_" + ALLTRIM(this.w_FILE)
                      if this.pOperazione$"D-N" 
                        this.w_FILENAME = this.w_FILE
                      endif
                    endif
                  endif
                else
                  this.w_FILE = JUSTFNAME(this.pDOCUMENTO)
                  * --- elimina estensione
                  if RAT(".", this.w_FILE)>0
                    this.w_FILE = JUSTSTEM(this.w_FILE)
                  endif
                endif
              endif
            else
              this.w_FILE = alltrim(this.pTABLENAME) +"_" + ALLTRIM(iif(UPPER(this.w_GESTIONE.class)="TGSIR_BGD", this.w_FILE, this.pKEYRECORD))
            endif
            this.w_FILE = this.CaratteriDOS(this.w_FILE)
            * --- Determina la posizione del punto prima dell'estensione e l'estensione stessa
            this.w_estensione = JUSTEXT(this.pDOCUMENTO)
            * --- controllo se sto archiviando un archivio zip con file in formato grafico e in caso positivo aggiungo al nome del file web l'indicazione dell'estensione
            this.w_EXTGRF = this.check_grafico(this.pDOCUMENTO)
            if (LOWER(this.w_ESTENSIONE)="zip" OR LOWER(this.w_ESTENSIONE)=".zip") AND not EMPTY(this.w_EXTGRF)
              * --- Controllo che il nome non contenga gi� l'indicazione dell'estensione dei file grafici
              if EMPTY(this.check_grafico(this.w_FILE))
                this.w_FILE = ALLTRIM(this.W_FILE)+this.w_EXTGRF
              endif
            endif
            * --- File di destinazione
            * --- ATTENZIONE! Eliminata ALLTRIM(w_File) per gestire files con spazio prima dell'estensione
            * --- Se archiviazione verso Infinity il nome completo del file non deve eccedere i 100 caratteri
            if this.w_CDMODALL="I" Or this.w_CDMODALL="S"
              this.w_DESTINAZIONE = Left(Upper(this.w_FILE), 100-Iif(Empty(this.w_estensione), 5, Len(this.w_estensione)+1)) + Iif(empty(this.w_estensione),"","."+this.w_estensione)
            else
              this.w_DESTINAZIONE = Upper(this.w_FILE) + iif(empty(this.w_estensione),"","."+this.w_estensione)
            endif
            if this.pFIRMDIG
              this.w_TmpPat = this.pDOCUMENTO
            else
              * --- Esegue copia del file solo se richiesto nella classe documentale (F=Default)
              do case
                case this.w_CDMODALL="F"
                  * --- Costruzione Path di default che punta alla cartella in cui sono definiti i file associati alle varie chiavi
                  this.w_TmpPat = alltrim( this.w_PERCORSOARCH )+this.w_DESTINAZIONE 
                  * --- Esegue la copia fisica del file <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                  w_ERRORE = .F.
                  w_ErrorHandler = on("ERROR")
                  on error w_ERRORE = .T.
                  if !EMPTY(this.pDOCUMENTO)
                    if this.pOperazione$"D-N" 
                      do case
                        case cp_fileexist(this.w_TmpPat) 
                          this.w_PATH_KRF = alltrim( this.w_PERCORSOARCH )
                          this.w_FILENAME = this.w_DESTINAZIONE 
                          do GSUT_KRF with this
                          if i_retcode='stop' or !empty(i_Error)
                            return
                          endif
                          if this.w_TIPOPERAZ="S"
                            copy file (this.pDOCUMENTO) to (this.w_TmpPat) 
                          else
                            this.w_TmpPat = FORCEEXT(Alltrim(ADDBS(JUSTPATH(this.w_TmpPat)))+ALLTRIM(JUSTSTEM(this.w_FILENAME)),this.w_Estensione)
                            if empty(this.w_FILENAME) or cp_fileexist(this.w_TmpPat) 
                              Ah_ErrorMsg("Operazione annullata",48,"")
                              i_retcode = 'stop'
                              return
                            else
                              copy file (this.pDOCUMENTO) to (this.w_TmpPat) 
                              this.w_FILE = JUSTSTEM(this.w_TmpPat)
                            endif
                          endif
                        case ! cp_fileexist(this.w_TmpPat) 
                          copy file (this.pDOCUMENTO) to (this.w_TmpPat) 
                      endcase
                    else
                      if ! cp_fileexist(this.w_TmpPat) or ! this.pFIRMDIG
                        copy file (this.pDOCUMENTO) to (this.w_TmpPat) 
                      endif
                    endif
                  endif
                  if type("this.pPdProces")="C" AND this.pPdProces="IRP" AND UPPER(RIGHT(ALLTRIM(this.pDOCUMENTO),5))=".HTML"
                    copy file (LEFT(ALLTRIM(this.pDOCUMENTO),LEN(ALLTRIM(this.pDOCUMENTO))-5)+"_Data.html") to (alltrim(this.w_PERCORSOARCH)+substr(this.pDOCUMENTO,rat("\", this.pDOCUMENTO)+1,rat(".", this.pDOCUMENTO)-rat("\", this.pDOCUMENTO)-1)+"_Data.html") 
                  endif
                  on error &w_ErrorHandler
                  if w_ERRORE
                    if !this.pNoVisMsg
                      * --- Nessun messaggio se sotto transazione
                      AH_ERRORMSG("Impossibile copiare il documento nella cartella di destinazione %1",48,"",alltrim(this.w_PERCORSOARCH))
                    endif
                    this.w_CODERRORE = 40
                    i_retcode = 'stop'
                    i_retval = this.w_CODERRORE
                    return
                  endif
                case this.w_CDMODALL="E"
                  local fileindice
                  FileEds("S", this.pCLASSEDOCU, @fileindice, this.pDOCUMENTO, left(alltrim(this.pDESCRIZIONE),254))
                  this.w_FORIGINE = fileindice
                  if EMPTY(this.w_FORIGINE)
                    this.w_CODERRORE = 40
                    i_retcode = 'stop'
                    i_retval = this.w_CODERRORE
                    return
                  endif
                  this.w_TmpPat = this.pDOCUMENTO
                otherwise
                  this.w_TmpPat = this.pDOCUMENTO
              endcase
            endif
          endif
          * --- Verifico se richiesta l'univocit�
          if ! this.w_CHKUNIVOC
            * --- Sbianco la classe per cui � richiesta l'univocit� dell'allegato
            this.w_CODCLASS = " "
          endif
          * --- Memorizza il nome del file di origine "pulito" (senza percorso)
          if this.w_CDMODALL<>"E" and this.w_CDMODALL<>"L"
            if this.pOperazione $ "U-P"
              this.w_FOrigine = IIF(empty(this.w_FILE), " ", substr(this.w_FILE ,RAT("\", this.w_FILE)+1,250))
            else
              this.w_FOrigine = IIF(empty(this.pDOCUMENTO), " ", substr(this.pDOCUMENTO ,RAT("\", this.pDOCUMENTO)+1,250))
            endif
          endif
          * --- Leggo flag pubblica su WEB
          if g_CPIN<>"S" and not Empty( this.w_CODTIPOL + this.w_CODCLASS )
            * --- Read from CLA_ALLE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CLA_ALLE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CLA_ALLE_idx,2],.t.,this.CLA_ALLE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TDPUBWEB"+;
                " from "+i_cTable+" CLA_ALLE where ";
                    +"TACODICE = "+cp_ToStrODBC(this.w_CODTIPOL);
                    +" and TACODCLA = "+cp_ToStrODBC(this.w_CODCLASS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TDPUBWEB;
                from (i_cTable) where;
                    TACODICE = this.w_CODTIPOL;
                    and TACODCLA = this.w_CODCLASS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_PUBWEB = NVL(cp_ToDate(_read_.TDPUBWEB),cp_NullValue(_read_.TDPUBWEB))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.w_PUBWEB = "N"
          endif
          if this.w_ISALT
            * --- Se AlterEgoTop il flag � sempre attivo
            if this.w_IDFLHIDE = "S"
              * --- Se provvisorio non attivo il check
              this.w_PUBWEB = "N"
            else
              if g_CPIN= "S" and g_przi="S"
                this.w_PUBWEB = iif(this.pTABLENAME="CAN_TIER" ,this.w_CNPUBWEB,this.w_CDPUBWEB)
              else
                this.w_PUBWEB = "N"
              endif
            endif
            if this.pOperazione$"D-N"
              * --- Modifico la modalit� di archiviazione, sostituendola con quella inserita in Azienda
              this.w_CDMODALL = this.w_EXMODALL
            endif
          endif
          * --- In caso di Nuovo documento  calcolo il nome del file di origine in base a quanto inserito in maschera
          this.w_FIL_BRQ = this.pDOCUMENTO
          if type("this.pPathArc")="C" and not empty(this.pPathArc)
            if empty(this.w_FILENAME)
              do case
                case this.w_PARENTCLASS="TGSUT_BFI"
                  * --- Il file � appena stato firmato e deve riportare l'esensione originale + .P7M
                  this.pDOCUMENTO = alltrim(iif(empty(justDrive(this.w_TmpPat)) and !Directory(addbs(alltrim(JustPath(this.w_TmpPat)))),addbs(sys(5)+sys(2003)),""))+addbs(alltrim(JustPath(this.w_TmpPat)))+alltrim(JUSTFNAME(this.w_TmpPat))
                case !(this.w_OBJATTRclass="TGSUT_KSO" )
                  * --- Se invia  mantengo nome file origine spostato
                  this.pDOCUMENTO = alltrim(iif(empty(justDrive(this.w_TmpPat)) and !Directory(addbs(alltrim(JustPath(this.w_TmpPat)))),addbs(sys(5)+sys(2003)),""))+addbs(alltrim(JustPath(this.w_TmpPat)))+alltrim(JUSTSTEM(this.pPATHARC)+this.w_estensione)
              endcase
              this.w_FORIGINE = alltrim(JUSTFNAME(this.pPATHARC))
            else
              this.pDOCUMENTO = alltrim(iif(empty(justDrive(this.w_TmpPat)) and !Directory(addbs(alltrim(JustPath(this.w_TmpPat)))),addbs(sys(5)+sys(2003)),""))+addbs(alltrim(JustPath(this.w_TmpPat)))+alltrim(JUSTSTEM(this.w_FILENAME))+iif(empty(this.w_estensione),"","."+this.w_estensione)
              this.w_FORIGINE = this.w_FILENAME
              this.w_DESTINAZIONE = Upper(alltrim(JUSTSTEM(this.w_FILENAME))) +"."+this.w_Estensione
            endif
            if this.w_ISALT
              this.w_FIL_BRQ = alltrim(iif(empty(justDrive(this.w_TmpPat)),addbs(sys(5)+sys(2003)),""))+addbs(alltrim(JustPath(this.w_TmpPat)))+ alltrim(JUSTSTEM(this.w_TmpPat)+"."+this.w_estensione)
            endif
          endif
          if Not Empty(this.w_PERCORSOARCH) and Empty(JustDrive(this.w_PERCORSOARCH)) and ! DIRECTORY(Alltrim(this.w_PERCORSOARCH))
            * --- Non � stato indicato il driver nel path lo aggiungo
            this.w_PERCORSOARCH = addbs(sys(5)+sys(2003))+Alltrim(this.w_PERCORSOARCH)
          endif
          * --- Gestisco il campo stato SOS del documento in base alle impostazioni inserite nella classe attuale
          this.w_STATODOC = iif(this.w_CLASSOS="S","DA_INVIARE","NOT_SOS")
          * --- Se l'archiviazione deriva da processo documentale, verifica se il file esiste gi�
          if this.pORIGINE = "PR"
            this.w_IDOGGETT = this.pDESCRIZIONE
            if NOT(g_DMIP="S") AND this.w_CDMODALL<>"S"
              * --- Ricorda ... se l'archiviazione � da processo la modalit� di archiviazione indicata sulla classe DEVE essere di tipo <copia file> e non <collegamento>
              * --- Read from PROMINDI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PROMINDI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "IDSERIAL,IDDATRAG,IDPATALL"+;
                  " from "+i_cTable+" PROMINDI where ";
                      +"IDNOMFIL = "+cp_ToStrODBC(this.w_DESTINAZIONE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  IDSERIAL,IDDATRAG,IDPATALL;
                  from (i_cTable) where;
                      IDNOMFIL = this.w_DESTINAZIONE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_IDSERIAL = NVL(cp_ToDate(_read_.IDSERIAL),cp_NullValue(_read_.IDSERIAL))
                this.w_TMPD = NVL(cp_ToDate(_read_.IDDATRAG),cp_NullValue(_read_.IDDATRAG))
                this.w_TMPC = NVL(cp_ToDate(_read_.IDPATALL),cp_NullValue(_read_.IDPATALL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if empty(this.w_IDSERIAL)
                * --- Prenota seriale per archivio indici documenti
                this.w_IDSERIAL = space(20)
                this.w_IDSERIAL = cp_GetProg("PROMINDI","SEIDO",this.w_IDSERIAL,i_CODAZI)
                * --- Inserisce testata
                * --- Insert into PROMINDI
                i_nConn=i_TableProp[this.PROMINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PROMINDI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"IDSERIAL"+",IDCLADOC"+",IDNOMFIL"+",IDTIPFIL"+",IDUTECRE"+",IDDATCRE"+",IDDATRAG"+",IDOGGETT"+",ID_TESTO"+",IDORIGIN"+",IDMODALL"+",IDDATINI"+",IDDATOBS"+",IDPATALL"+",IDTIPALL"+",IDCLAALL"+",UTCC"+",UTDC"+",IDPUBWEB"+",IDFLGFAE"+",IDCODCBI"+",IDSTASOS"+",IDFLHIDE"+",IDTIPBST"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_IDSERIAL),'PROMINDI','IDSERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.pCLASSEDOCU),'PROMINDI','IDCLADOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DESTINAZIONE),'PROMINDI','IDNOMFIL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ESTENSIONE),'PROMINDI','IDTIPFIL');
                  +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PROMINDI','IDUTECRE');
                  +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PROMINDI','IDDATCRE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DATARAGGR),'PROMINDI','IDDATRAG');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_IDOGGETT),'PROMINDI','IDOGGETT');
                  +","+cp_NullLink(cp_ToStrODBC(this.pTESTO),'PROMINDI','ID_TESTO');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_FORIGINE),'PROMINDI','IDORIGIN');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CDMODALL),'PROMINDI','IDMODALL');
                  +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PROMINDI','IDDATINI');
                  +","+cp_NullLink(cp_ToStrODBC(i_findat),'PROMINDI','IDDATOBS');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PERCORSOARCH),'PROMINDI','IDPATALL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODTIPOL),'PROMINDI','IDTIPALL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODCLASS),'PROMINDI','IDCLAALL');
                  +","+cp_NullLink(cp_ToStrODBC(i_CodUte),'PROMINDI','UTCC');
                  +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PROMINDI','UTDC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PUBWEB),'PROMINDI','IDPUBWEB');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_FLGFAE),'PROMINDI','IDFLGFAE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCBI),'PROMINDI','IDCODCBI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_STATODOC),'PROMINDI','IDSTASOS');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_IDFLHIDE),'PROMINDI','IDFLHIDE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_IDTIPBST),'PROMINDI','IDTIPBST');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL,'IDCLADOC',this.pCLASSEDOCU,'IDNOMFIL',this.w_DESTINAZIONE,'IDTIPFIL',this.w_ESTENSIONE,'IDUTECRE',i_CODUTE,'IDDATCRE',i_DATSYS,'IDDATRAG',this.w_DATARAGGR,'IDOGGETT',this.w_IDOGGETT,'ID_TESTO',this.pTESTO,'IDORIGIN',this.w_FORIGINE,'IDMODALL',this.w_CDMODALL,'IDDATINI',i_DATSYS)
                  insert into (i_cTable) (IDSERIAL,IDCLADOC,IDNOMFIL,IDTIPFIL,IDUTECRE,IDDATCRE,IDDATRAG,IDOGGETT,ID_TESTO,IDORIGIN,IDMODALL,IDDATINI,IDDATOBS,IDPATALL,IDTIPALL,IDCLAALL,UTCC,UTDC,IDPUBWEB,IDFLGFAE,IDCODCBI,IDSTASOS,IDFLHIDE,IDTIPBST &i_ccchkf. );
                     values (;
                       this.w_IDSERIAL;
                       ,this.pCLASSEDOCU;
                       ,this.w_DESTINAZIONE;
                       ,this.w_ESTENSIONE;
                       ,i_CODUTE;
                       ,i_DATSYS;
                       ,this.w_DATARAGGR;
                       ,this.w_IDOGGETT;
                       ,this.pTESTO;
                       ,this.w_FORIGINE;
                       ,this.w_CDMODALL;
                       ,i_DATSYS;
                       ,i_findat;
                       ,this.w_PERCORSOARCH;
                       ,this.w_CODTIPOL;
                       ,this.w_CODCLASS;
                       ,i_CodUte;
                       ,SetInfoDate( g_CALUTD );
                       ,this.w_PUBWEB;
                       ,this.w_FLGFAE;
                       ,this.w_TIPCBI;
                       ,this.w_STATODOC;
                       ,this.w_IDFLHIDE;
                       ,this.w_IDTIPBST;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              else
                * --- Se � cambiato il percorso di archiviazione, cancella la vecchia versione del file <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                this.w_TMPC = Alltrim(this.w_TMPC)
                if Upper(Alltrim(this.w_PERCORSOARCH))<>Upper(this.w_TMPC) And FILE(this.w_TMPC+this.w_DESTINAZIONE)
                  w_ErrorHandler = on("ERROR")
                  w_ERRORE = .F.
                  on error w_ERRORE = .T.
                  delete file (this.w_TMPC+this.w_DESTINAZIONE) 
                  on error &w_ErrorHandler
                  if w_ERRORE
                    if !this.pNoVisMsg
                      * --- Nessun messaggio se sotto transazione
                      this.w_TMPC = "Impossibile eliminare copia precedente del documento archiviato"
                      Ah_ErrorMSG(this.w_TMPC,48)
                    endif
                  endif
                endif
                * --- Aggiorna Testata Indice
                * --- Write into PROMINDI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PROMINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IDUTECRE ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PROMINDI','IDUTECRE');
                  +",IDDATCRE ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PROMINDI','IDDATCRE');
                  +",IDDATRAG ="+cp_NullLink(cp_ToStrODBC(this.w_DATARAGGR),'PROMINDI','IDDATRAG');
                  +",IDOGGETT ="+cp_NullLink(cp_ToStrODBC(this.w_IDOGGETT),'PROMINDI','IDOGGETT');
                  +",ID_TESTO ="+cp_NullLink(cp_ToStrODBC(this.pTESTO),'PROMINDI','ID_TESTO');
                  +",IDORIGIN ="+cp_NullLink(cp_ToStrODBC(this.w_FORIGINE),'PROMINDI','IDORIGIN');
                  +",IDMODALL ="+cp_NullLink(cp_ToStrODBC(this.w_CDMODALL),'PROMINDI','IDMODALL');
                  +",IDDATINI ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PROMINDI','IDDATINI');
                  +",IDDATOBS ="+cp_NullLink(cp_ToStrODBC(i_findat),'PROMINDI','IDDATOBS');
                  +",IDPATALL ="+cp_NullLink(cp_ToStrODBC(this.w_PERCORSOARCH),'PROMINDI','IDPATALL');
                  +",IDTIPALL ="+cp_NullLink(cp_ToStrODBC(this.w_CODTIPOL),'PROMINDI','IDTIPALL');
                  +",IDCLAALL ="+cp_NullLink(cp_ToStrODBC(this.w_CODCLASS),'PROMINDI','IDCLAALL');
                  +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CodUte),'PROMINDI','UTCV');
                  +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PROMINDI','UTDV');
                  +",IDPUBWEB ="+cp_NullLink(cp_ToStrODBC(this.w_PUBWEB),'PROMINDI','IDPUBWEB');
                  +",IDCODCBI ="+cp_NullLink(cp_ToStrODBC(this.w_TIPCBI),'PROMINDI','IDCODCBI');
                  +",IDSTASOS ="+cp_NullLink(cp_ToStrODBC(this.w_STATODOC),'PROMINDI','IDSTASOS');
                  +",IDFLHIDE ="+cp_NullLink(cp_ToStrODBC(this.w_IDFLHIDE),'PROMINDI','IDFLHIDE');
                      +i_ccchkf ;
                  +" where ";
                      +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                         )
                else
                  update (i_cTable) set;
                      IDUTECRE = i_CODUTE;
                      ,IDDATCRE = i_DATSYS;
                      ,IDDATRAG = this.w_DATARAGGR;
                      ,IDOGGETT = this.w_IDOGGETT;
                      ,ID_TESTO = this.pTESTO;
                      ,IDORIGIN = this.w_FORIGINE;
                      ,IDMODALL = this.w_CDMODALL;
                      ,IDDATINI = i_DATSYS;
                      ,IDDATOBS = i_findat;
                      ,IDPATALL = this.w_PERCORSOARCH;
                      ,IDTIPALL = this.w_CODTIPOL;
                      ,IDCLAALL = this.w_CODCLASS;
                      ,UTCV = i_CodUte;
                      ,UTDV = SetInfoDate( g_CALUTD );
                      ,IDPUBWEB = this.w_PUBWEB;
                      ,IDCODCBI = this.w_TIPCBI;
                      ,IDSTASOS = this.w_STATODOC;
                      ,IDFLHIDE = this.w_IDFLHIDE;
                      &i_ccchkf. ;
                   where;
                      IDSERIAL = this.w_IDSERIAL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Cancella attuale dettaglio attributi
                * --- Delete from PRODINDI
                i_nConn=i_TableProp[this.PRODINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                         )
                else
                  delete from (i_cTable) where;
                        IDSERIAL = this.w_IDSERIAL;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
              endif
            endif
          else
            * --- Inserisce testata
            * --- ATTENZIONE! Eliminata ALLTRIM(w_File) per gestire files con spazio prima dell'estensione
            do case
              case this.w_CDMODALL="L"
                this.w_FORIGINE = this.w_FILE + iif(empty(this.w_estensione),"","."+this.w_estensione)
                this.w_FILORIGI = ADDBS(JUSTPATH(this.pDOCUMENTO)) + this.w_FORIGINE
              case this.pOperazione $ "U-P"
                this.w_FILORIGI = this.w_FILE + iif(empty(this.w_estensione),"","."+this.w_estensione)
              otherwise
                this.w_FILORIGI = this.pDOCUMENTO
            endcase
            this.w_FORIGINE = IIF(Empty(Justext(this.w_FORIGINE)) and Not Empty(this.w_FORIGINE), Alltrim(this.w_FORIGINE)+iif(empty(this.w_estensione),"","."+this.w_estensione), this.w_FORIGINE)
            * --- Se archiviazione valorizzo descrizione con nome file di origine
            this.w_IDOGGETT = iif(this.w_ISALT and this.pOperazione$"C-S" and Empty(this.pDESCRIZIONE) ,JUSTFNAME(this.w_FORIGINE),this.pDESCRIZIONE)
            if (g_CPIN="S" or g_REVI="S") AND this.pINFINITY
              this.w_TMPC = SPACE(10)
              if NOT(this.oParentObject.w_INARCHIVIO$"SX")
                * --- Sono nel caso di archiviazione solo web
                * --- Se archiviazione verso Infinity il nome completo del file non deve eccedere i 100 caratteri
                if this.w_CDMODALL="I" Or this.w_CDMODALL="S"
                  this.w_estensione = JustExt(this.w_FORIGINE)
                  this.w_DESTINAZIONE = Left(ForceExt(this.w_FORIGINE,""), 100-Len(this.w_estensione)+1) + "." + this.w_estensione
                else
                  this.w_DESTINAZIONE = this.w_FORIGINE
                endif
                if g_DOCM="S" And Vartype(this.oParentObject)="O" And PemStatus(this.oParentObject, "w_NomRep", 5)
                  * --- Devo verificare se c'� un processo documentale associato al report per utilizzarne il codice come ExternalCode
                  this.w_QUERY = "1"
                  this.w_PROCOD = ""
                  this.w_PDCLADOC = ""
                  * --- Select from ..\DOCM\EXE\QUERY\chkprocd
                  do vq_exec with '..\DOCM\EXE\QUERY\chkprocd',this,'_Curs__d__d__DOCM_EXE_QUERY_chkprocd','',.f.,.t.
                  if used('_Curs__d__d__DOCM_EXE_QUERY_chkprocd')
                    select _Curs__d__d__DOCM_EXE_QUERY_chkprocd
                    locate for 1=1
                    do while not(eof())
                    this.w_PROCOD = NVL(PDCODPRO,"")
                    this.w_PDKEYPRO = NVL(PDKEYPRO,"")
                    this.w_PDCLADOC = NVL(PDCLADOC,"")
                    if Alltrim(this.w_PDCLADOC) == Alltrim(this.pCLASSEDOCU)
                      * --- Se c'� un processo documentale associato che ha la stessa classe documentale esco e uso questo
                      exit
                    else
                      this.w_PDCLADOC = ""
                    endif
                      select _Curs__d__d__DOCM_EXE_QUERY_chkprocd
                      continue
                    enddo
                    use
                  endif
                  if !Empty(this.w_PDCLADOC) And Vartype(this.pOBJATTRIBUTI)="C" And Used(this.pOBJATTRIBUTI)
                    * --- Trovato un processo con stessa classe documentale
                    l_Errore = .F.
                    l_ErrorHandler = on("ERROR")
                    l_oldArea = Select()
                    Select(this.pOBJATTRIBUTI)
                    On Error l_Errore = .T.
                    * --- L'ExternalCode deve essere CODICEPROCESSO_CHIAVEPROCESSO
                    this.w_ExternalCode = Alltrim(this.w_PROCOD) + "_"
                    l_Macro = Alltrim(this.w_PDKEYPRO)
                    this.w_ExternalCode = this.w_ExternalCode + &l_Macro
                    On Error &l_ErrorHandler
                    Select(l_oldArea)
                    * --- Se c'� stato un errore, svuoto l'externalcode, verr� utilizzato w_DESTINAZIONE al suo posto
                    *     altrimenti imposto la stessa estensione del file
                    if l_Errore
                      this.w_ExternalCode = ""
                    else
                      this.w_ExternalCode = ForceExt(Alltrim(this.w_ExternalCode), JustExt(this.w_DESTINAZIONE))
                    endif
                  endif
                endif
                if g_DMIP<>"S" AND this.w_CDMODALL<>"S"
                  * --- Prenota seriale per archivio indici documenti
                  this.w_IDSERIAL = space(20)
                  * --- Creo un nuovo indice
                  this.w_IDSERIAL = cp_GetProg("PROMINDI","SEIDO",this.w_IDSERIAL,i_CODAZI)
                  if vartype(this.pIdSerial) = "C"
                    * --- Passato all'indietro per inserire nell'indice il nome del documento firmato e il path usato per l'archiviazione del documento
                    m.pIdSerial = this.w_IDSERIAL
                  endif
                  this.w_DESTINAZIONE = this.w_IDSERIAL+"_"+this.w_FORIGINE
                endif
              endif
              if empty(this.w_estensione)
                this.w_estensione = justext(this.w_DESTINAZIONE)
              endif
            endif
            if g_DMIP<>"S" AND this.w_CDMODALL<>"S"
              * --- Inserisco un nuovo indice
              * --- Insert into PROMINDI
              i_nConn=i_TableProp[this.PROMINDI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PROMINDI_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"IDSERIAL"+",IDCLADOC"+",IDNOMFIL"+",IDTIPFIL"+",IDUTECRE"+",IDDATCRE"+",IDDATRAG"+",IDOGGETT"+",ID_TESTO"+",IDORIGIN"+",IDMODALL"+",IDORIFIL"+",IDDATINI"+",IDDATOBS"+",IDTIPALL"+",IDCLAALL"+",IDPATALL"+",UTCC"+",UTDC"+",IDPUBWEB"+",IDFLGFAE"+",IDCODCBI"+",IDSTASOS"+",IDFLHIDE"+",IDTIPBST"+",IDORACRE"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_IDSERIAL),'PROMINDI','IDSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.pCLASSEDOCU),'PROMINDI','IDCLADOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DESTINAZIONE),'PROMINDI','IDNOMFIL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ESTENSIONE),'PROMINDI','IDTIPFIL');
                +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PROMINDI','IDUTECRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DATA_ARCH_DOC),'PROMINDI','IDDATCRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DATARAGGR),'PROMINDI','IDDATRAG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IDOGGETT),'PROMINDI','IDOGGETT');
                +","+cp_NullLink(cp_ToStrODBC(this.pTESTO),'PROMINDI','ID_TESTO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_FORIGINE),'PROMINDI','IDORIGIN');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CDMODALL),'PROMINDI','IDMODALL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_FILORIGI),'PROMINDI','IDORIFIL');
                +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PROMINDI','IDDATINI');
                +","+cp_NullLink(cp_ToStrODBC(i_findat),'PROMINDI','IDDATOBS');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODTIPOL),'PROMINDI','IDTIPALL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODCLASS),'PROMINDI','IDCLAALL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PERCORSOARCH),'PROMINDI','IDPATALL');
                +","+cp_NullLink(cp_ToStrODBC(i_CodUte),'PROMINDI','UTCC');
                +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PROMINDI','UTDC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PUBWEB),'PROMINDI','IDPUBWEB');
                +","+cp_NullLink(cp_ToStrODBC(this.w_FLGFAE),'PROMINDI','IDFLGFAE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCBI),'PROMINDI','IDCODCBI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_STATODOC),'PROMINDI','IDSTASOS');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IDFLHIDE),'PROMINDI','IDFLHIDE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IDTIPBST),'PROMINDI','IDTIPBST');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ORA_ARCH_DOC),'PROMINDI','IDORACRE');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL,'IDCLADOC',this.pCLASSEDOCU,'IDNOMFIL',this.w_DESTINAZIONE,'IDTIPFIL',this.w_ESTENSIONE,'IDUTECRE',i_CODUTE,'IDDATCRE',this.w_DATA_ARCH_DOC,'IDDATRAG',this.w_DATARAGGR,'IDOGGETT',this.w_IDOGGETT,'ID_TESTO',this.pTESTO,'IDORIGIN',this.w_FORIGINE,'IDMODALL',this.w_CDMODALL,'IDORIFIL',this.w_FILORIGI)
                insert into (i_cTable) (IDSERIAL,IDCLADOC,IDNOMFIL,IDTIPFIL,IDUTECRE,IDDATCRE,IDDATRAG,IDOGGETT,ID_TESTO,IDORIGIN,IDMODALL,IDORIFIL,IDDATINI,IDDATOBS,IDTIPALL,IDCLAALL,IDPATALL,UTCC,UTDC,IDPUBWEB,IDFLGFAE,IDCODCBI,IDSTASOS,IDFLHIDE,IDTIPBST,IDORACRE &i_ccchkf. );
                   values (;
                     this.w_IDSERIAL;
                     ,this.pCLASSEDOCU;
                     ,this.w_DESTINAZIONE;
                     ,this.w_ESTENSIONE;
                     ,i_CODUTE;
                     ,this.w_DATA_ARCH_DOC;
                     ,this.w_DATARAGGR;
                     ,this.w_IDOGGETT;
                     ,this.pTESTO;
                     ,this.w_FORIGINE;
                     ,this.w_CDMODALL;
                     ,this.w_FILORIGI;
                     ,i_DATSYS;
                     ,i_findat;
                     ,this.w_CODTIPOL;
                     ,this.w_CODCLASS;
                     ,this.w_PERCORSOARCH;
                     ,i_CodUte;
                     ,SetInfoDate( g_CALUTD );
                     ,this.w_PUBWEB;
                     ,this.w_FLGFAE;
                     ,this.w_TIPCBI;
                     ,this.w_STATODOC;
                     ,this.w_IDFLHIDE;
                     ,this.w_IDTIPBST;
                     ,this.w_ORA_ARCH_DOC;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          endif
          if !(g_CPIN="S" AND this.pINFINITY) and vartype(this.pIdSerial) = "C" or g_DMIP="S"
            * --- Passato all'indietro per inserire nell'indice il nome del documento firmato e il path usato per l'archiviazione del documento
            m.pIdSerial = this.w_IDSERIAL+"*"+alltrim(this.w_PERCORSOARCH)
          endif
          if g_CPIN="S" AND this.pINFINITY or g_DMIP="S" or this.w_CDMODALL="S" or (this.w_PUBWEB="S" and this.w_ISALT)
            do case
              case this.w_PUBWEB="S" and this.w_ISALT
                do case
                  case this.pTABLENAME="ATTIVITA"
                    this.w_IDWEBFIL = STRTRAN(JUSTFNAME(this.w_FORIGINE),"ATTIVITA","Elenco_Attivit�")
                    this.w_IDOGGETT = "Elenco_Attivit�"
                  case Not EMPTY(this.w_IDOGGETT)
                    this.w_IDWEBFIL = FORCEEXT(Alltrim(this.w_IDOGGETT) + "_"+ ALLTRIM(this.w_IDSERIAL), this.w_estensione)
                  otherwise
                    this.w_IDWEBFIL = FORCEEXT(ALLTRIM(this.w_IDSERIAL), this.w_estensione)
                endcase
              case NOT(this.oParentObject.w_INARCHIVIO$"SX")
                * --- Sono nel caso di archiviazione solo web
                this.w_IDWEBFIL = JUSTFNAME(this.w_DESTINAZIONE)
              case this.oParentObject.w_INARCHIVIO="X" AND this.w_CDMODALL="F" AND g_DMIP="S"
                * --- Archiviazione standard+web
                this.w_IDWEBFIL = FORCEEXT(JUSTFNAME(this.w_FILE)+"_"+ALLTRIM(SYS(2015)),this.w_estensione)
              case this.oParentObject.w_INARCHIVIO="X" AND this.w_CDMODALL="F"
                this.w_IDWEBFIL = JUSTFNAME(this.w_FILE) + iif(empty(this.w_estensione),"","."+this.w_estensione)
              case this.oParentObject.w_INARCHIVIO="X" AND this.w_CDMODALL="E" AND g_DMIP="S"
                this.w_IDWEBFIL = FORCEEXT(JUSTSTEM(this.w_DESTINAZIONE)+SYS(2015)+".", JUSTEXT(this.w_DESTINAZIONE))
              case this.oParentObject.w_INARCHIVIO="X" AND this.w_CDMODALL="E"
                this.w_IDWEBFIL = ALLTRIM(this.w_IDSERIAL)+"_"+JUSTFNAME(this.w_DESTINAZIONE)
              case g_DMIP="S" OR this.w_CDMODALL="S"
                if this.pORIGINE = "PR" AND this.w_CDMODALL $ "IS"
                  this.w_IDWEBFIL = ""
                  if not EMPTY(this.w_CDNOMFIL) and this.w_VARTYPEOBJATTR="C" and used(this.pOBJATTRIBUTI)
                    this.w_OLDAREA = SELECT()
                    w_ERRORE = .F.
                    w_ErrorHandler = on("ERROR")
                    on error w_ERRORE = .T.
                    select (this.pOBJATTRIBUTI)
                    if EOF()
                      * --- Se sono fuori dal cursore mi riposiziono sulla prima riga
                      Go Top
                    endif
                    this.w_VALATTR = eval(this.w_CDNOMFIL)
                    on error &w_ErrorHandler
                    * --- Ripristino la vecchia area di lavoro
                    SELECT(this.w_OLDAREA)
                    if NOT w_ERRORE AND NOT EMPTY(this.w_VALATTR)
                      this.w_IDWEBFIL = FORCEEXT(this.w_VALATTR+".", JUSTEXT(this.w_FORIGINE))
                    endif
                  else
                    * --- NOME FILE DI DESTINAZIONE MANCANTE
                    this.w_CODERRORE = iif(this.w_CDMODALL="S", 85, 72)
                    i_retcode = 'stop'
                    i_retval = this.w_CODERRORE
                    return
                  endif
                  if EMPTY(this.w_IDWEBFIL)
                    * --- NOME FILE DI DESTINAZIONE NON VALUTABILE
                    this.w_CODERRORE = iif(this.w_CDMODALL="S", 86, 73)
                    i_retcode = 'stop'
                    i_retval = this.w_CODERRORE
                    return
                  endif
                else
                  this.w_IDWEBFIL = this.w_FORIGINE
                endif
              otherwise
                this.w_IDWEBFIL = ALLTRIM(this.w_IDSERIAL)+"_"+JUSTFNAME(this.w_FORIGINE)
            endcase
            if (this.w_CDMODALL="E" OR this.w_CDMODALL="I") AND this.oParentObject.w_INARCHIVIO<>"X" AND g_DMIP<>"S"
              this.w_IDWEBFIL = IIF(EMPTY(JUSTEXT(ALLTRIM(this.w_IDWEBFIL))),FORCEEXT(this.w_IDWEBFIL,this.w_ESTENSIONE),this.w_IDWEBFIL)
            endif
            if this.w_CDMODALL="S"
              this.w_IDWEBFIL = FORCEEXT(JUSTSTEM(this.w_IDWEBFIL) + SYS(2015)+".", JUSTEXT(this.w_IDWEBFIL))
            endif
            * --- controllo se sto archiviando un archivio zip con file in formato grafico e in caso positivo aggiungo al nome del file web l'indicazione dell'estensione
            this.w_EXTGRF = this.check_grafico(this.pDOCUMENTO)
            if (LOWER(this.w_ESTENSIONE)="zip" OR LOWER(this.w_ESTENSIONE)=".zip") AND not EMPTY(this.w_EXTGRF)
              * --- Controllo che il nome non contenga gi� l'indicazione dell'estensione dei file grafici
              if EMPTY(this.check_grafico(this.w_IDWEBFIL))
                this.w_IDWEBFIL = FORCEEXT(JUSTSTEM(this.w_IDWEBFIL)+this.w_EXTGRF,"zip")
              endif
            endif
            this.w_IDWEBFIL = this.CaratteriDOS(upper(this.w_IDWEBFIL))
            do case
              case this.oParentObject.w_INARCHIVIO="X" OR g_DMIP="S"
                * --- Archiviazione standard+web nel caso di archiviazione manuale
                * --- Legge percorso per documenti
                * --- Aggiunge backslash finale
                this.w_COPATHDO = ADDBS(this.w_COPATHDO)
                * --- Elimina eventuale \ finale, poi la rimette ...
                this.w_DIRLAV = this.w_COPATHDO
                if FILE(this.pDOCUMENTO)
                  * --- Se non c'� crea la cartella ...
                  if !DIRECTORY(this.w_DIRLAV)
                    this.w_ERRODIR = Not(this.AH_CreateFolder(this.w_DIRLAV))
                  endif
                  if cp_fileexist(this.w_COPATHDO+this.w_IDWEBFIL)
                    delete file (this.w_COPATHDO+this.w_IDWEBFIL)
                  endif
                  if this.pORIGINE="AR" and this.w_CDMODALL="S" and this.pOperazione<>"C"
                    rename (this.pDOCUMENTO) to (this.w_COPATHDO+this.w_IDWEBFIL) 
                  else
                    copy file (this.pDOCUMENTO) to (this.w_COPATHDO+this.w_IDWEBFIL) 
                  endif
                endif
              case this.pORIGINE="AR"
                * --- Archiviazione manuale
                *     Archiviazione da print system
                *     Archiviazione da processo documentale solo web
                w_ERRORE = .F.
                w_ErrorHandler = on("ERROR")
                on error w_ERRORE = .T.
                if cp_fileexist(alltrim(this.w_COPATHDO)+JUSTFNAME(this.w_FORIGINE))
                  if this.w_CDMODALL<>"S" and cp_fileexist(this.w_COPATHDO+this.w_IDWEBFIL)
                    delete file (this.w_COPATHDO+this.w_IDWEBFIL)
                  endif
                  rename alltrim(this.w_COPATHDO)+JUSTFNAME(this.w_FORIGINE) TO (this.w_COPATHDO+this.w_IDWEBFIL)
                else
                  w_ERRORE = .T.
                endif
                on error &w_ErrorHandler
              case this.pORIGINE="PR" AND this.w_CDMODALL="S"
                * --- Archiviazione da processo documentale su IDMS stand alone
                w_ERRORE = .F.
                w_ErrorHandler = on("ERROR")
                on error w_ERRORE = .T.
                if cp_fileexist(this.pDOCUMENTO)
                  copy file (this.pDOCUMENTO) TO (this.w_COPATHDO+this.w_IDWEBFIL)
                else
                  w_ERRORE = .T.
                endif
                on error &w_ErrorHandler
            endcase
            if this.pORIGINE="PR"
              * --- Nel caso di processo documentale non ho ancora il file dentro la cartella di invio mail
              if this.w_CDMODALL="E" OR this.w_CDMODALL="I"
                this.w_FILENAMECRC = alltrim(this.pDocumento)
              else
                this.w_FILENAMECRC = ADDBS(this.w_PERCORSOARCH)+alltrim(this.w_DESTINAZIONE)
              endif
            else
              this.w_FILENAMECRC = alltrim(this.w_COPATHDO)+this.w_IDWEBFIL
            endif
            this.w_FILENAMECRC = iif(cp_fileexist(this.w_FILENAMECRC), SYS(2007, FILETOSTR(this.w_FILENAMECRC), -1, 1), space(20))
            if this.pOperazione="B"
              * --- Acquisizione file da Infinity con associazione tramite barcode
              this.w_CDREPOBC = iif(file(this.w_CDREPOBC), this.w_CDREPOBC, "")
              if g_DMIP<>"S" AND this.w_CDMODALL<>"S" AND NOT EMPTY(this.w_IDSERIAL)
                this.w_ExternalCode = this.w_IDSERIAL
              else
                if this.w_ASBAUTOM $ "ON"
                  * --- External Code= O: Obbligatorio, N: Non automatico
                  do GSUT_KGB with this
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if empty(this.w_ExternalCode)
                    if this.w_ASBAUTOM="O"
                      ah_Errormsg("Barcode non definito, operazione annullata.")
                      this.w_CODERRORE = -1
                      i_retcode = 'stop'
                      return
                    endif
                  else
                    this.w_ExternalCode = alltrim(this.w_ExternalCode)
                  endif
                endif
                if empty(this.w_ExternalCode)
                  this.w_ExternalCode = SUBSTR(SYS(2015),2)
                  this.w_ASNOREPS = "N"
                endif
              endif
              this.w_PROTOCOLLO = ""
              this.w_CURSEXT = VARTYPE(this.oParentObject.w_CURS_BARCODE)="C" AND NOT EMPTY(this.oParentObject.w_CURS_BARCODE) AND USED(this.oParentObject.w_CURS_BARCODE)
              if this.w_CURSEXT
                SELECT (this.oParentObject.w_CURS_BARCODE)
              else
                create cursor __tmp__ (EXTCODE C(30), ATTRIBUTO C(15), DESCRIZIONE C(40), VALORE C(254))
              endif
              * --- stampo l'etichetta/frontespizio barcode
              if this.w_DIMATT>3 AND this.w_CDTIPOBC$"F-R"
                * --- Frontespizio
                this.w_STAR = ""
                if this.w_CDTIPOBC="F"
                  this.w_STAR = "*"
                endif
                if this.w_CONTA>1
                  FOR JJ=1 TO this.w_CONTA-1
                  append blank
                  replace EXTCODE with this.w_STAR+"%X" + this.w_ExternalCode + "%"+this.w_STAR, ATTRIBUTO with L_ARRAYATTRIBUTI(JJ,4)
                  replace DESCRIZIONE with L_ARRAYATTRIBUTI(JJ,5), VALORE with L_ARRAYATTRIBUTI(JJ,6)
                  NEXT
                else
                  append blank
                  replace EXTCODE with this.w_STAR+"%X" + this.w_ExternalCode + "%"+this.w_STAR
                endif
                this.w_REPNAME = evl(this.w_CDREPOBC,IIF(this.w_CDTIPOBC = "F", "query\gsut1bcv", "query\gsut3bcv"))
              else
                * --- Etichetta
                if not empty(this.w_MVSERIAL)
                  do case
                    case this.pTABLENAME="DOC_MAST" 
                      * --- Read from DOC_MAST
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "MVNUMEST,MVALFEST,MVDATREG"+;
                          " from "+i_cTable+" DOC_MAST where ";
                              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          MVNUMEST,MVALFEST,MVDATREG;
                          from (i_cTable) where;
                              MVSERIAL = this.w_MVSERIAL;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_MVNUMEST = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
                        this.w_MVALFEST = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
                        this.w_MVDATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      if this.w_MVNUMEST>0
                        this.w_PROTOCOLLO = "Protocollo: "+alltrim(str(this.w_MVNUMEST))
                        if not empty(this.w_MVALFEST)
                          this.w_PROTOCOLLO = this.w_PROTOCOLLO + "/"+alltrim(this.w_MVALFEST)
                        endif
                        if not empty(this.w_MVDATREG)
                          this.w_PROTOCOLLO = this.w_PROTOCOLLO + " del "+ dtoc(this.w_MVDATREG)
                        endif
                      endif
                    case this.pTABLENAME="PNT_MAST" 
                      * --- Read from PNT_MAST
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "PNNUMPRO,PNALFPRO,PNDATREG"+;
                          " from "+i_cTable+" PNT_MAST where ";
                              +"PNSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          PNNUMPRO,PNALFPRO,PNDATREG;
                          from (i_cTable) where;
                              PNSERIAL = this.w_MVSERIAL;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_MVNUMEST = NVL(cp_ToDate(_read_.PNNUMPRO),cp_NullValue(_read_.PNNUMPRO))
                        this.w_MVALFEST = NVL(cp_ToDate(_read_.PNALFPRO),cp_NullValue(_read_.PNALFPRO))
                        this.w_MVDATREG = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      if this.w_MVNUMEST>0
                        this.w_PROTOCOLLO = "Protocollo: "+alltrim(str(this.w_MVNUMEST))
                        if not empty(this.w_MVALFEST)
                          this.w_PROTOCOLLO = this.w_PROTOCOLLO + "/"+alltrim(this.w_MVALFEST)
                        endif
                        if not empty(this.w_MVDATREG)
                          this.w_PROTOCOLLO = this.w_PROTOCOLLO + " del "+ dtoc(this.w_MVDATREG)
                        endif
                      endif
                  endcase
                endif
                append blank
                if this.w_CDTIPOBC = "E"
                  replace EXTCODE with "*%X" + this.w_ExternalCode + "%*", ATTRIBUTO with this.w_MVSERIAL, VALORE with this.w_PROTOCOLLO
                  append blank
                  replace EXTCODE with "*%OWNER" + alltrim(InfinityApplicationID()) + "%*"
                  this.w_REPNAME = evl(this.w_CDREPOBC, "query\gsut_bcv" )
                else
                  replace EXTCODE with "%X" + this.w_ExternalCode + "%", ATTRIBUTO with this.w_MVSERIAL, VALORE with this.w_PROTOCOLLO
                  * --- La riga di OwnerCode non serve nel QRCode perch� va gestito sulla stessa riga dell'ExternalCode
                  this.w_REPNAME = evl(this.w_CDREPOBC, "query\gsut2bcv")
                endif
                this.w_MVSERIAL = ""
              endif
              if NOT this.w_CURSEXT and this.w_ASNOREPS<>"S"
                cp_chprn(this.w_REPNAME)
              endif
              if this.w_CDMODALL<>"S"
                * --- Creo PDF da allegare
                this.w_Pathfile = this.w_COPATHDO
                this.w_Nomefile = lower(this.w_COPATHDO+this.w_IDWEBFIL)
                this.w_Nomefilebak = this.w_Nomefile+".bak"
                w_ERRORE = .F.
                if cp_fileexist(this.w_Nomefile)
                  w_ErrorHandler = on("ERROR")
                  on error w_ERRORE = .T.
                  rename (this.w_Nomefile) to (this.w_Nomefilebak)
                  on error &w_ErrorHandler
                  if w_ERRORE and !this.pNoVisMsg
                    * --- Nessun messaggio se sotto transazione
                    Ah_ErrorMSG("Errore di accesso al file.%0%1",48,,message())
                  endif
                endif
                if not w_ERRORE
                  w_ErrorHandler = on("ERROR")
                  on error w_ERRORE = .T.
                  * --- creo il PDF di intestazione per creare l'associazione con Infinity
                  cp_chprn(this.w_REPNAME, " ", this, 10)
                  if cp_fileexist(this.w_Nomefile)
                    delete file (this.w_Nomefilebak)
                    * --- Nell'upload, quando scompatter� lo zip, potrebbe non riconoscere i caratteri maiuscoli
                    this.w_Nomefile = lower(this.w_Nomefile)
                    rename (this.w_Nomefile) to (this.w_Nomefile)
                  else
                    * --- ripristino il file originale
                    rename (this.w_Nomefilebak) to (this.w_Nomefile)
                    if !this.pNoVisMsg
                      * --- Nessun messaggio se sotto transazione
                      ah_errormsg("Impossibile creare PDF da etichetta barcode.%0%1",48,,message())
                    endif
                  endif
                  on error &w_ErrorHandler
                  if w_ERRORE and !this.pNoVisMsg
                    * --- Nessun messaggio se sotto transazione
                    Ah_ErrorMSG("Errore di accesso al file.%0%1",48,,message())
                  endif
                endif
              endif
            endif
            if NOT(g_DMIP="S") AND this.w_CDMODALL<>"S"
              * --- Creo CRC e setto flag cancellazione
              * --- Write into PROMINDI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PROMINDI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"IDWEBAPP ="+cp_NullLink(cp_ToStrODBC(IIF(this.oParentObject.w_INARCHIVIO$"SX", "A", "S")),'PROMINDI','IDWEBAPP');
                +",IDCRCALL ="+cp_NullLink(cp_ToStrODBC(this.w_FILENAMECRC),'PROMINDI','IDCRCALL');
                +",IDERASEF ="+cp_NullLink(cp_ToStrODBC(this.w_CDERASEF),'PROMINDI','IDERASEF');
                +",IDWEBFIL ="+cp_NullLink(cp_ToStrODBC(this.w_IDWEBFIL),'PROMINDI','IDWEBFIL');
                +",IDWEBRET ="+cp_NullLink(cp_ToStrODBC(" "),'PROMINDI','IDWEBRET');
                +",IDWEBERR ="+cp_NullLink(cp_ToStrODBC(" "),'PROMINDI','IDWEBERR');
                +",IDOGGETT ="+cp_NullLink(cp_ToStrODBC(this.w_IDOGGETT),'PROMINDI','IDOGGETT');
                    +i_ccchkf ;
                +" where ";
                    +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                       )
              else
                update (i_cTable) set;
                    IDWEBAPP = IIF(this.oParentObject.w_INARCHIVIO$"SX", "A", "S");
                    ,IDCRCALL = this.w_FILENAMECRC;
                    ,IDERASEF = this.w_CDERASEF;
                    ,IDWEBFIL = this.w_IDWEBFIL;
                    ,IDWEBRET = " ";
                    ,IDWEBERR = " ";
                    ,IDOGGETT = this.w_IDOGGETT;
                    &i_ccchkf. ;
                 where;
                    IDSERIAL = this.w_IDSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              * --- Nel caso di archiviazione su Infinity Easy mi preparo xml descrittore
              if this.w_CDMODALL="S"
                if empty(this.oParentObject.w_UploadDocumentsXML)
                  this.oParentObject.w_UploadDocumentsXML = lower(this.w_COPATHDO + iif(this.pOperazione="B", evl(this.w_ExternalCode,SYS(2015)),this.w_IDWEBFIL + ".xml"))
                endif
                if VARTYPE(attr_xml)="C"
                  RELEASE attr_xml
                endif
                this.w_NATTRXML = 1
                dimension attr_xml(1,2)
                attr_xml(1,1) = alltrim(this.w_ExternalCode)
                attr_xml(1,2) = alltrim(iif(g_REVI="S", this.w_CDCLAINF, this.pCLASSEDOCU))
              else
                if empty(this.oParentObject.w_UploadDocumentsXML)
                  * --- Se non esiste ancora xml descrittore lo creo
                  this.oParentObject.w_UploadDocumentsXML = this.w_COPATHDO+ evl(this.w_Externalcode,SYS(2015)) +"_UPLOADDOCUMENTS.zar"
                  this.oParentObject.w_OBJXML = createobject("Msxml2.DOMDocument.4.0")
                  * --- Encoding usato dalla classe Msxml2.DOMDocument. Es. utf-8, iso-8859-1, iso-8859-15
                  *     utf-8: tutti i caratteri
                  *     iso-8859-15 (manca il carattere TM)
                  *     iso-8859-1 (manca il carattere � )
                  if TYPE("g_Encoding_Msxml2_DOMDocument")<>"C"
                    public g_Encoding_Msxml2_DOMDocument 
 g_Encoding_Msxml2_DOMDocument="utf-8"
                  endif
                  this.w_INTESTAZIONE = 'version="1.0" encoding="'+g_Encoding_Msxml2_DOMDocument+'"'
                  this.oParentObject.w_OBJXML.appendChild(this.oParentObject.w_OBJXML.createProcessingInstruction("xml", this.w_INTESTAZIONE))     
                  this.w_sNode = this.oParentObject.w_OBJXML.createNode(1,"UploadDocuments","")
                  this.w_sNode.setAttribute("applicationId", InfinityApplicationID())     
                  this.w_sNode.setAttribute("ConsolidationDate","01-01-1900")     
                  this.w_sNode.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")     
                  this.oParentObject.w_OBJXML.appendChild(this.w_sNode)     
                endif
                this.w_sNode = this.oParentObject.w_OBJXML.createNode(1,"Add_UploadDocuments","")
                this.w_sNode.setAttribute("ExternalCode", alltrim(iif(this.pOperazione="B" Or !Empty(this.w_ExternalCode), this.w_ExternalCode, this.w_DESTINAZIONE)))     
                this.w_sNode.setAttribute("ReferenceName", lower(alltrim(this.w_IDWEBFIL)))     
                this.w_sNode.setAttribute("LogicalName", this.w_IDWEBFIL)     
                this.w_sNode.setAttribute("ClassDoc", alltrim(iif(g_REVI="S", this.w_CDCLAINF, this.pCLASSEDOCU)))     
                this.w_sNode.setAttribute("PathOnClient", this.w_COPATHDO+alltrim(this.w_IDWEBFIL))     
                this.w_sNode.setAttribute("Description", left(alltrim(this.w_IDOGGETT),100))     
                this.w_sNode.setAttribute("Notes", alltrim(this.pTESTO))     
                this.w_sNode.setAttribute("Crc", alltrim(this.w_FILENAMECRC))     
                this.oParentObject.w_OBJXML.childNodes.Item(1).appendChild(this.w_sNode)     
              endif
            endif
            if TYPE("This.oParentObject.w_BPDSERIAL")<>"U"
              this.oParentObject.w_BPDSERIAL = this.w_IDSERIAL
            endif
          else
            * --- Write into PROMINDI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PROMINDI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"IDWEBAPP ="+cp_NullLink(cp_ToStrODBC("N"),'PROMINDI','IDWEBAPP');
              +",IDCRCALL ="+cp_NullLink(cp_ToStrODBC(space(20)),'PROMINDI','IDCRCALL');
              +",IDERASEF ="+cp_NullLink(cp_ToStrODBC("N"),'PROMINDI','IDERASEF');
              +",IDWEBFIL ="+cp_NullLink(cp_ToStrODBC(space(254)),'PROMINDI','IDWEBFIL');
                  +i_ccchkf ;
              +" where ";
                  +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                     )
            else
              update (i_cTable) set;
                  IDWEBAPP = "N";
                  ,IDCRCALL = space(20);
                  ,IDERASEF = "N";
                  ,IDWEBFIL = space(254);
                  &i_ccchkf. ;
               where;
                  IDSERIAL = this.w_IDSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Inserisce attributi e relativo valore
          this.w_OLDSETCENTURY = SET("CENTURY")
          if this.w_OLDSETCENTURY <> "ON"
            SET CENTURY ON
          endif
          this.w_COUNTER = 1
          this.w_sCHILDvoid = true
          do while this.w_COUNTER <= ALEN(L_ARRAYATTRIBUTI,1)
            * --- Legge Valori Attributi
            this.w_CLASSEDOCU = this.pCLASSEDOCU
            this.w_CPROWNUM = L_ARRAYATTRIBUTI(this.w_COUNTER,2)
            this.w_IDVALATT = L_ARRAYATTRIBUTI(this.w_COUNTER,3)
            * --- Read from PRODCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PRODCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CPROWORD,CDCODATT,CDDESATT,CDCHKOBB,CDTIPATT,CDTABKEY,CDATTINF,CDCREARA"+;
                " from "+i_cTable+" PRODCLAS where ";
                    +"CDCODCLA = "+cp_ToStrODBC(this.w_CLASSEDOCU);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CPROWORD,CDCODATT,CDDESATT,CDCHKOBB,CDTIPATT,CDTABKEY,CDATTINF,CDCREARA;
                from (i_cTable) where;
                    CDCODCLA = this.w_CLASSEDOCU;
                    and CPROWNUM = this.w_CPROWNUM;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CPROWORD = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
              this.w_CDCODATT = NVL(cp_ToDate(_read_.CDCODATT),cp_NullValue(_read_.CDCODATT))
              this.w_CDDESATT = NVL(cp_ToDate(_read_.CDDESATT),cp_NullValue(_read_.CDDESATT))
              this.w_CDCHKOBB = NVL(cp_ToDate(_read_.CDCHKOBB),cp_NullValue(_read_.CDCHKOBB))
              this.w_CDTIPATT = NVL(cp_ToDate(_read_.CDTIPATT),cp_NullValue(_read_.CDTIPATT))
              this.w_CDTABKEY = NVL(cp_ToDate(_read_.CDTABKEY),cp_NullValue(_read_.CDTABKEY))
              this.w_CDATTINF = NVL(cp_ToDate(_read_.CDATTINF),cp_NullValue(_read_.CDATTINF))
              this.w_CDCREARA = NVL(cp_ToDate(_read_.CDCREARA),cp_NullValue(_read_.CDCREARA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_CDMODALL="S"
              this.w_CDCODATT = EVL(this.w_CDATTINF, this.w_CDCODATT)
            else
              this.w_CDCODATT = iif(g_DMIP="S", this.w_CDATTINF, this.w_CDCODATT)
            endif
            if this.w_MASK
              * --- Tipizza il dato 
              do case
                case this.w_CDTIPATT="N"
                  this.w_IDVALNUM = val(this.w_IDVALATT)
                  this.w_IDVALDAT = {}
                case this.w_CDTIPATT="D"
                  this.w_IDVALDAT = cp_CharToDate(this.w_IDVALATT)
                  this.w_IDVALNUM = 0
                otherwise
                  this.w_IDVALNUM = 0
                  this.w_IDVALDAT = {}
              endcase
              * --- Riassegno i valori a w_CPROWORD, per garantire una numerazione corretta (10,20,....)
              this.w_CPROWORD = this.w_COUNTER * 10
            else
              * --- Traduce in stringa
              do case
                case this.w_CDTIPATT="N"
                  this.w_IDVALNUM = this.w_IDVALATT
                  this.w_IDVALATT = alltrim(transform(this.w_IDVALATT))
                  this.w_IDVALDAT = {}
                case this.w_CDTIPATT="D"
                  this.w_IDVALDAT = this.w_IDVALATT
                  this.w_IDVALATT = iif(empty(this.w_IDVALATT),"", dtoc(this.w_IDVALATT))
                  this.w_IDVALNUM = 0
                otherwise
                  this.w_IDVALNUM = 0
                  this.w_IDVALDAT = {}
              endcase
            endif
            * --- Scrive ...
            if g_DMIP="S" or this.w_CDMODALL="S"
              if not empty(this.w_IDVALATT)
                if this.w_CDMODALL="S"
                  this.w_NATTRXML = this.w_NATTRXML+1
                  dimension attr_xml(this.w_NATTRXML,2)
                  attr_xml(this.w_NATTRXML,1) = alltrim(this.w_CDCODATT)
                  attr_xml(this.w_NATTRXML,2) = alltrim(this.w_IDVALATT)
                  if this.w_CDCREARA="S" AND this.w_CDTABKEY="CONTI" AND inlist(left(this.w_IDVALATT,1), "C", "F")
                    this.w_MVTIPCON = left(this.w_IDVALATT,1)
                    this.w_MVCODCON = Substr(this.w_IDVALATT,2)
                  endif
                else
                  if this.w_sCHILDvoid
                    this.w_sCHILDvoid = false
                    this.w_sCHILD = this.oParentObject.w_OBJXML.createNode(1,"BO_Attribute","")
                    this.w_sNode.appendChild(this.w_sCHILD)     
                  endif
                  this.w_sCHILDDETT = this.oParentObject.w_OBJXML.createNode(1,"Attribute","")
                  this.w_sCHILD.appendChild(this.w_sCHILDDETT)     
                  this.w_sCHILDDETTPROP = this.oParentObject.w_OBJXML.createNode(1,"AttributeName","")
                  this.w_sCHILDDETTPROP.text = alltrim(this.w_CDCODATT)
                  this.w_sCHILDDETT.appendChild(this.w_sCHILDDETTPROP)     
                  this.w_sCHILDDETTPROP = this.oParentObject.w_OBJXML.createNode(1,"AttributeValue","")
                  this.w_sCHILDDETTPROP.text = alltrim(this.w_IDVALATT)
                  this.w_sCHILDDETT.appendChild(this.w_sCHILDDETTPROP)     
                  this.w_sCHILDDETTPROP = this.oParentObject.w_OBJXML.createNode(1,"Crc","")
                  this.w_sCHILDDETTPROP.text = SYS(2007, alltrim(this.w_IDVALATT), -1, 1)
                  this.w_sCHILDDETT.appendChild(this.w_sCHILDDETTPROP)     
                endif
              endif
            else
              * --- Insert into PRODINDI
              i_nConn=i_TableProp[this.PRODINDI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"IDSERIAL"+",CPROWNUM"+",IDDESATT"+",IDTIPATT"+",IDCHKOBB"+",IDVALATT"+",IDTABKEY"+",CPROWORD"+",IDCODATT"+",IDVALDAT"+",IDVALNUM"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_IDSERIAL),'PRODINDI','IDSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PRODINDI','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CDDESATT),'PRODINDI','IDDESATT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CDTIPATT),'PRODINDI','IDTIPATT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CDCHKOBB),'PRODINDI','IDCHKOBB');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IDVALATT),'PRODINDI','IDVALATT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CDTABKEY),'PRODINDI','IDTABKEY');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRODINDI','CPROWORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CDCODATT),'PRODINDI','IDCODATT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IDVALDAT),'PRODINDI','IDVALDAT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IDVALNUM),'PRODINDI','IDVALNUM');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL,'CPROWNUM',this.w_CPROWNUM,'IDDESATT',this.w_CDDESATT,'IDTIPATT',this.w_CDTIPATT,'IDCHKOBB',this.w_CDCHKOBB,'IDVALATT',this.w_IDVALATT,'IDTABKEY',this.w_CDTABKEY,'CPROWORD',this.w_CPROWORD,'IDCODATT',this.w_CDCODATT,'IDVALDAT',this.w_IDVALDAT,'IDVALNUM',this.w_IDVALNUM)
                insert into (i_cTable) (IDSERIAL,CPROWNUM,IDDESATT,IDTIPATT,IDCHKOBB,IDVALATT,IDTABKEY,CPROWORD,IDCODATT,IDVALDAT,IDVALNUM &i_ccchkf. );
                   values (;
                     this.w_IDSERIAL;
                     ,this.w_CPROWNUM;
                     ,this.w_CDDESATT;
                     ,this.w_CDTIPATT;
                     ,this.w_CDCHKOBB;
                     ,this.w_IDVALATT;
                     ,this.w_CDTABKEY;
                     ,this.w_CPROWORD;
                     ,this.w_CDCODATT;
                     ,this.w_IDVALDAT;
                     ,this.w_IDVALNUM;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
            * --- Incrementa contatore
            this.w_COUNTER = this.w_COUNTER + 1
          enddo
          if this.w_OLDSETCENTURY <> "ON"
            SET CENTURY OFF
          endif
          if this.w_CDMODALL="S" and this.w_NATTRXML>1 and NOT EMPTY(this.oParentObject.w_UploadDocumentsXML)
            if not empty(this.w_MVCODCON)
              * --- Aggiungo i dati per creare rapporto e contatto su Infinity
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANDESCRI,ANDESCR2,ANINDIRI,ANINDIR2,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION,ANCODFIS,ANPARIVA,AN_EMAIL,ANPERFIS,ANTELEFO,ANNUMCEL,ANTELFAX,ANFLPRIV,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANPRONAS,ANLOCNAS,ANFORGIU"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANDESCRI,ANDESCR2,ANINDIRI,ANINDIR2,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION,ANCODFIS,ANPARIVA,AN_EMAIL,ANPERFIS,ANTELEFO,ANNUMCEL,ANTELFAX,ANFLPRIV,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANPRONAS,ANLOCNAS,ANFORGIU;
                  from (i_cTable) where;
                      ANTIPCON = this.w_MVTIPCON;
                      and ANCODICE = this.w_MVCODCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_ANDESCRI = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
                this.w_ANDESCR2 = NVL(cp_ToDate(_read_.ANDESCR2),cp_NullValue(_read_.ANDESCR2))
                this.w_ANINDIRI = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
                this.w_ANINDIR2 = NVL(cp_ToDate(_read_.ANINDIR2),cp_NullValue(_read_.ANINDIR2))
                this.w_AN___CAP = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
                this.w_ANLOCALI = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
                this.w_ANPROVIN = NVL(cp_ToDate(_read_.ANPROVIN),cp_NullValue(_read_.ANPROVIN))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
                this.w_ANCODFIS = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
                this.w_ANPARIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
                this.w_AN_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
                this.w_ANPERFIS = NVL(cp_ToDate(_read_.ANPERFIS),cp_NullValue(_read_.ANPERFIS))
                this.w_ANTELEFO = NVL(cp_ToDate(_read_.ANTELEFO),cp_NullValue(_read_.ANTELEFO))
                this.w_ANNUMCEL = NVL(cp_ToDate(_read_.ANNUMCEL),cp_NullValue(_read_.ANNUMCEL))
                this.w_ANTELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
                this.w_ANFLPRIV = NVL(cp_ToDate(_read_.ANFLPRIV),cp_NullValue(_read_.ANFLPRIV))
                this.w_ANCOGNOM = NVL(cp_ToDate(_read_.ANCOGNOM),cp_NullValue(_read_.ANCOGNOM))
                this.w_AN__NOME = NVL(cp_ToDate(_read_.AN__NOME),cp_NullValue(_read_.AN__NOME))
                this.w_AN_SESSO = NVL(cp_ToDate(_read_.AN_SESSO),cp_NullValue(_read_.AN_SESSO))
                this.w_ANDATNAS = NVL(cp_ToDate(_read_.ANDATNAS),cp_NullValue(_read_.ANDATNAS))
                this.w_ANPRONAS = NVL(cp_ToDate(_read_.ANPRONAS),cp_NullValue(_read_.ANPRONAS))
                this.w_ANLOCNAS = NVL(cp_ToDate(_read_.ANLOCNAS),cp_NullValue(_read_.ANLOCNAS))
                this.w_ANFORGIU = NVL(cp_ToDate(_read_.ANFORGIU),cp_NullValue(_read_.ANFORGIU))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if not empty(this.w_ANPARIVA+this.w_ANCODFIS+this.w_AN_EMAIL) and (this.w_ANPERFIS<>"S" or not empty(this.w_ANCOGNOM) and not empty(this.w_AN__NOME))
                * --- Aggiungo 33 righe
                dimension attr_xml(this.w_NATTRXML + 33, 2)
                * --- 1
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "Relcon"
                attr_xml[this.w_NATTRXML,2] = "_GetCodCon()"
                * --- 2
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "<entity>"
                attr_xml[this.w_NATTRXML,2] = iif(this.w_ANPERFIS="S", '"gsfr_aperson" ', '"gsfr_acompany" ')
                attr_xml[this.w_NATTRXML,2] = attr_xml[this.w_NATTRXML,2] + 'searchKey="' + iif(empty(this.w_ANPARIVA), iif(empty(this.w_ANCODFIS), "OFMAIL", "COFISCALCODE"), "COIVA") + '" update="S"'
                if this.w_ANPERFIS<>"S"
                  * --- 3
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "COTITLE"
                  attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANDESCRI)
                  * --- 4
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "COTITLEADD"
                  attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANDESCR2)
                endif
                * --- 5
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "COFORGIU"
                attr_xml[this.w_NATTRXML,2] = iif(this.w_ANPERFIS="S", "PER", Evl(this.w_ANFORGIU,"ALT"))
                * --- 6
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "COIVA"
                attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANPARIVA)
                * --- 7
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "COFISCALCODE"
                attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANCODFIS)
                if this.w_ANPERFIS="S"
                  * --- 8
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "COSURNAME"
                  attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANCOGNOM)
                  * --- 9
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "CONAME"
                  attr_xml[this.w_NATTRXML,2] = alltrim(this.w_AN__NOME)
                  * --- 10
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "COSEX"
                  attr_xml[this.w_NATTRXML,2] = alltrim(this.w_AN_SESSO)
                  * --- 11
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "COBORNDATE"
                  attr_xml[this.w_NATTRXML,2] = "@EXVL@" +CHRTRAN(DTOC(this.w_ANDATNAS),"/","-")
                  * --- 12
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "COPROBORN"
                  attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANPRONAS)
                  * --- 13
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "COBORNLOC"
                  attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANLOCNAS)
                endif
                * --- Aggiungo i dati della sede principale
                * --- 14
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "<m_Detail_Row>"
                attr_xml[this.w_NATTRXML,2] = "gsfr_doffice"
                * --- 15
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "OFDESCRI"
                attr_xml[this.w_NATTRXML,2] = "Principale"
                * --- 16
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "OFCODELOC"
                attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANLOCALI)
                * --- 17
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "OFADDRESS"
                attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANINDIRI)
                * --- 18
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "OFADDRESS2"
                attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANINDIR2)
                * --- 19
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "OFPROV"
                attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANPROVIN)
                * --- 20
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "OFCAP"
                attr_xml[this.w_NATTRXML,2] = alltrim(this.w_AN___CAP)
                * --- 21
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "OFMAIL"
                * --- al massimo un indirizzo mail
                attr_xml[this.w_NATTRXML,2] = alltrim(iif(";"$this.w_AN_EMAIL, left(this.w_AN_EMAIL, At(";",this.w_AN_EMAIL)-1), this.w_AN_EMAIL))
                this.w_NFTEL = " "
                if NOT empty(this.w_ANTELEFO)
                  * --- 22
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "OFPHONETYP"
                  attr_xml[this.w_NATTRXML,2] = "F"
                  * --- 23
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "OFPHONE"
                  attr_xml[this.w_NATTRXML,2] = alltrim( this.w_ANTELEFO)
                  this.w_NFTEL = "2"
                endif
                if NOT empty(this.w_ANNUMCEL)
                  * --- 24
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "OFPHONETYP" + Alltrim(this.w_NFTEL)
                  attr_xml[this.w_NATTRXML,2] = "C"
                  * --- 25
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "OFPHONE" + Alltrim(this.w_NFTEL)
                  attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANNUMCEL)
                  this.w_NFTEL = CHRTRAN(this.w_NFTEL," 23","234")
                endif
                if NOT empty(this.w_ANTELFAX)
                  * --- 26
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "OFPHONETYP" + Alltrim(this.w_NFTEL)
                  attr_xml[this.w_NATTRXML,2] = "X"
                  * --- 27
                  this.w_NATTRXML = this.w_NATTRXML+1
                  attr_xml[this.w_NATTRXML,1] = "OFPHONE" + Alltrim(this.w_NFTEL)
                  attr_xml[this.w_NATTRXML,2] = alltrim(this.w_ANTELFAX)
                  this.w_NFTEL = CHRTRAN(this.w_NFTEL," 23","234")
                endif
                * --- 28
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "OFPRIMARY"
                attr_xml[this.w_NATTRXML,2] = "S"
                * --- 29
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "OFOFFICEID"
                attr_xml[this.w_NATTRXML,2] = "0000000010"
                * --- 30
                * --- Aggiungo il rapporto
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "<entity>"
                attr_xml[this.w_NATTRXML,2] = ["gsfr_akeysog" update="S"]
                * --- 31
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "KSTIPSOG"
                attr_xml[this.w_NATTRXML,2] = iif(this.w_MVTIPCON="F","FOR","CLI")
                * --- 32
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "KSFLFOLD"
                * --- E' l'informazione necessaria per creare la cartella "Folder" sul DMS per il rapporto.
                *     0 = Non abilito check e quindi non creo cartella 
                *     1 = Abilito check e quindi creo cartella
                attr_xml[this.w_NATTRXML,2] = "0"
                * --- 33
                this.w_NATTRXML = this.w_NATTRXML+1
                attr_xml[this.w_NATTRXML,1] = "KSCODSOG"
                attr_xml[this.w_NATTRXML,2] = this.w_MVTIPCON + alltrim(this.w_MVCODCON)
              endif
            endif
            this.w_XMLdscr = xmldesc( @attr_xml )
            if NOT EMPTY(this.w_XMLdscr)
              if this.pORIGINE = "PR"
                STRTOFILE(this.w_XMLdscr, this.oParentObject.w_UploadDocumentsXML)
                this.oParentObject.w_UploadDocumentsXML = ""
              else
                this.oParentObject.w_XMLdescrittore = this.w_XMLdscr
              endif
              this.w_XMLdscr = ""
            endif
          endif
          * --- Verifico se � stato richiesto di creare un documento Word/Open Office
          if vartype(this.pTIPOPER)="C" and this.pTIPOPER=="D" and file(this.w_pQuemod)
            this.w_QUERYMOD = this.w_pQuemod
            GSUT_BRQ(this,this.w_IDSERIAL,this.pCLASSEDOCU,iif(empty(this.w_CDTABKEY),this.pTABLENAME,this.w_CDTABKEY),this.w_FIL_BRQ,this.w_QUERYMOD,this.w_TmpPat)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if g_DMIP<>"S" AND this.w_CDMODALL<>"S"
            if this.w_CHIEDIATTRIBUTI AND !this.pNoVisMsg
              this.w_MID_APER = "S"
              this.w_GSUT_AID = GSUT_AID(this)
              this.w_GSUT_AID.w_IDSERIAL = this.w_IDSERIAL
              this.w_GSUT_AID.QueryKeySet("IDSERIAL="+chr(39)+ this.w_IDSERIAL + chr(39) ,"")     
              this.w_GSUT_AID.LoadRecWarn()     
              if this.w_ISALT and this.pOperazione$"C-S"
                this.w_GSUT_AID.w_FLEDIT = .T.
              endif
              this.w_GSUT_AID.ecpEDIT()     
              this.w_GSUT_AID.w_OLDPAT = IIF(this.w_GSUT_AID.w_IDMODALL="F",this.w_GSUT_AID.w_IDPATALL,Alltrim(ADDBS(JUSTPATH(this.w_GSUT_AID.w_IDORIFIL))))
              this.w_GSUT_AID.w_OLDFIL = this.w_GSUT_AID.w_IDNOMFIL
              this.w_GSUT_AID.w_PATHPRA = iif(! this.pOperazione$"D-N" ,this.w_PATHPRA,this.w_GSUT_AID.w_OLDPAT)
              if this.w_ISALT and this.pTABLENAME="CAN_TIER"
                * --- Se nuovo documento e siamo in AlterEgo Top
                if this.w_PARENTCLASS="TGSUT_BFG" and this.pOperazione="N" OR this.w_PARENTCLASS="TGSUT_BCV" and this.pOperazione="D"
                  this.w_COD_PRAT = this.pKEYRECORD
                  if NOT EMPTY(this.w_COD_PRAT)
                    * --- Legge la data di archiviazione della pratica
                    * --- Read from CAN_TIER
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "CNDTOBSO"+;
                        " from "+i_cTable+" CAN_TIER where ";
                            +"CNCODCAN = "+cp_ToStrODBC(this.w_COD_PRAT);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        CNDTOBSO;
                        from (i_cTable) where;
                            CNCODCAN = this.w_COD_PRAT;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_DATA_ARCH = NVL(cp_ToDate(_read_.CNDTOBSO),cp_NullValue(_read_.CNDTOBSO))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    * --- Se provengo dalla pratica conta flag pubblica presente in essa alltrimenti vale la classe
                    this.w_PUBWEB = this.w_CNPUBWEB
                    * --- Se la pratica non � archiviata
                    if EMPTY(this.w_DATA_ARCH)
                      this.w_GSUT_AID.w_COD_PRE = this.oParentObject.w_CODPRE
                      this.w_GSUT_AID.w_COD_RAG = this.oParentObject.w_RAGPRE
                      this.w_GSUT_AID.w_COD_PRA = this.w_COD_PRAT
                    endif
                  endif
                endif
                if this.w_PARENTCLASS="TGSUT_BFI" AND vartype(this.oParentObject.oParentObject)="O" AND lower(this.oParentObject.oParentObject.class)="tgsal1bxl"
                  * --- Dati atto firmato
                  this.w_GSUT_AID.w_IDTIPBST = "D"
                  this.w_OBJCTRL = this.w_GSUT_AID.GetCtrl("w_IDTIPBST")
                  this.w_OBJCTRL.SetRadio()     
                  this.w_OBJCTRL = .null.
                endif
              endif
              this.w_GSUT_AID.bupdated = .t.
            endif
            if this.w_ISALT and g_AGEN="S" and inlist(this.w_PARENTCLASS, "TGSAL_BXL", "TGSAL1BXL", "TGSAL_BCI") and (Not Empty(this.oParentObject.w_CODPRE) or Not Empty(this.oParentObject.w_RAGPRE))
              GSAG_BPI(this,this.w_CODPRAT,this.oParentObject.w_CODPRE,this.oParentObject.w_RAGPRE,"")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Completa ....
            if ! this.w_CHIEDIATTRIBUTI and not(this.pSILENTE) and this.pOperazione<>"B"
              this.w_posizpunto = rat("\", this.w_TmpPat)
              this.w_TmpPat = rtrim(substr(this.w_TmpPat,this.w_posizpunto+1))
              if Empty(this.pKEYRECORD)
                this.w_Ok = AH_ERRORMSG("File %1 archiviato correttamente",64,"", Alltrim(this.w_TmpPat))
              else
                do case
                  case this.w_CDMODALL="F"
                    this.w_Ok = AH_ERRORMSG("� stato associato il file %1 a %2Cartella: %3",64,"", Alltrim(this.w_TmpPat), alltrim(this.pKEYRECORD) + chr(13),rtrim(this.w_PERCORSOARCH))
                  case this.w_CDMODALL="E"
                    this.w_Ok = AH_ERRORMSG("� stato associato il file %1 a %2",64,"", Alltrim(this.w_TmpPat), alltrim(this.pKEYRECORD))
                  otherwise
                    this.w_Ok = AH_ERRORMSG("� stato collegato il file %1 a %2",64,"", Alltrim(this.w_TmpPat), alltrim(this.pKEYRECORD))
                endcase
              endif
            endif
            if !this.pNoVisMsg AND TYPE("w_GESTIONE.cprg")="C" AND ((UPPER(this.w_GESTIONE.cprg)="GSUT_KFG" AND this.pOperazione$"NU") OR (UPPER(this.w_GESTIONE.cprg)<>"GSUT_KFG" AND this.pOperazione="D")) AND this.w_MID_APER="S"
              * --- Il messaggio viene visualizzato  dal master/detail GSUT_AID (Indice documenti) - solo se quest'ultimo � stato lanciato
              this.w_GSUT_AID.VisuMess = .t.
              this.w_GSUT_AID.VisPat = this.w_TmpPat
            endif
          endif
        endif
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_CODERRORE
    return
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_GESTEXT = Upper(Justext(this.pDOCUMENTO))
    * --- Verifico se per l'estensione del file selezionato, esiste un'apposita gestione per tipologia e classe allegato (questa verifica viene fatta solo
    *     se non ho attivato almeno un check di completa path nella classe)
    * --- Select from EXT_ENS
    i_nConn=i_TableProp[this.EXT_ENS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.EXT_ENS_idx,2],.t.,this.EXT_ENS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select EXTIPALL,EXCLAALL,EXCODICE,EXESTCBI  from "+i_cTable+" EXT_ENS ";
          +" where EXCODICE = "+cp_ToStrODBC(this.w_GESTEXT)+"";
           ,"_Curs_EXT_ENS")
    else
      select EXTIPALL,EXCLAALL,EXCODICE,EXESTCBI from (i_cTable);
       where EXCODICE = this.w_GESTEXT;
        into cursor _Curs_EXT_ENS
    endif
    if used('_Curs_EXT_ENS')
      select _Curs_EXT_ENS
      locate for 1=1
      do while not(eof())
      this.w_CODTIPOL = _Curs_EXT_ENS.EXTIPALL
      this.w_CODCLASS = _Curs_EXT_ENS.EXCLAALL
      this.w_TIPCBI = _Curs_EXT_ENS.EXESTCBI
        select _Curs_EXT_ENS
        continue
      enddo
      use
    endif
    * --- Il  controllo viene eseguito solo per archiviazione 'manuale' e non da processo documentale
    if this.pORIGINE="AR"
      * --- Verifico se richiesta l'univocit� dell'allegato
      this.w_CLAALLE = this.w_CODCLASS
      this.w_CHIAVE = this.pKEYRECORD
      this.w_ARCHIVIO = this.pTABLENAME
      * --- Select from GSUT_BCU
      do vq_exec with 'GSUT_BCU',this,'_Curs_GSUT_BCU','',.f.,.t.
      if used('_Curs_GSUT_BCU')
        select _Curs_GSUT_BCU
        locate for 1=1
        do while not(eof())
        this.w_CHKUNIVOC = .F.
          select _Curs_GSUT_BCU
          continue
        enddo
        use
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NON_INS_TIP_CLA = this.w_ISALT AND this.w_OBJATTRclass="TGSUT_BSO" AND EMPTY(this.pOBJATTRIBUTI.w_CNPATHFA)
    if ! this.w_CHKUNIVOC 
      if !this.pNoVisMsg
        * --- Nessun messaggio se sotto transazione
        AH_ERRORMSG("E' gi� stato allegato un file, a causa dell'univocit� della classe quest'ultima verr� sbiancata!",48,"","")
      endif
      this.w_SBIANCCLASS = "X"
    else
      this.w_SBIANCCLASS = False
    endif
    if this.w_ISALT and this.pOperazione$"D-N" 
      this.w_PERCORSOARCH = this.w_PATHBASE
    else
      this.w_PERCORSOARCH = alltrim(DCMPATAR(this.w_PATHBASE, this.w_CRITERIORAGGR, this.w_DATARAGGR,this.pCLASSEDOCU, this.w_CODTIPOL, this.w_CODCLASS, this.pDOCUMENTO, this.w_SBIANCCLASS, this.w_INSPATIP, this.w_INSPACLA,this.w_NOCODAZ, this.w_NON_INS_TIP_CLA))
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea la cartella specificata nella variabile w_FOLDER
    *     Imposta la variabile w_ERRORE in caso di errore.
    w_ERRORE = .F.
    w_ErrorHandler = on("ERROR")
    on error w_ERRORE = .T.
    md (this.w_FOLDER)
    on error &w_ErrorHandler
    if w_ERRORE
      if !this.pNoVisMsg
        * --- Nessun messaggio se sotto transazione
        AH_ERRORMSG("Impossibile creare la cartella %1",48,"",this.w_FOLDER)
      endif
      this.w_CODERRORE = 41
      i_retcode = 'stop'
      i_retval = this.w_CODERRORE
      return
    else
      if !this.pNoVisMsg
        * --- Nessun messaggio se sotto transazione
        ah_msg("Creata nuova cartella %1",.t.,.f.,.f., rtrim(this.w_FOLDER))
      endif
    endif
  endproc


  procedure CaratteriDOS
    param w_APPO
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Il nome utilizzato per il file non � valido, o perch� contiene caratteri non consentiti o perch� � pi� lungo di 256 caratteri. 
    *     I nomi di file non possono contenere i caratteri \, /, ", :, *, ?, <, > e |.
    if this.w_CDMODALL<>"L"
      m.w_APPO = Chrtran(m.w_APPO, '\/":*?<>|', "_________")
      if !this.w_ISALT
        m.w_APPO = chrtran(m.w_APPO, "^&", "__")
        do case
          case this.w_CDMODALL="I"
            m.w_APPO = Chrtran(upper(m.w_APPO), upper("!,;�$%=�@[]{}�#������"), "_______________IEEOAU")
          case this.w_CDMODALL="S"
            m.w_APPO = Chrtran(upper(m.w_APPO), upper(",;"), "__")
        endcase
      endif
    endif
    return(m.w_APPO)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pArrayParam,pIdSerial)
    this.pArrayParam=pArrayParam
    this.pIdSerial=pIdSerial
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='PRODCLAS'
    this.cWorkTables[3]='PROMINDI'
    this.cWorkTables[4]='PRODINDI'
    this.cWorkTables[5]='EXT_ENS'
    this.cWorkTables[6]='CLA_ALLE'
    this.cWorkTables[7]='CAN_TIER'
    this.cWorkTables[8]='CONTROPA'
    this.cWorkTables[9]='TIP_ALLE'
    this.cWorkTables[10]='PAR_ALTE'
    this.cWorkTables[11]='DOC_MAST'
    this.cWorkTables[12]='PNT_MAST'
    this.cWorkTables[13]='CONTI'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_PRODCLAS')
      use in _Curs_PRODCLAS
    endif
    if used('_Curs_Query_gsut_bba')
      use in _Curs_Query_gsut_bba
    endif
    if used('_Curs__d__d__DOCM_EXE_QUERY_chkprocd')
      use in _Curs__d__d__DOCM_EXE_QUERY_chkprocd
    endif
    if used('_Curs_EXT_ENS')
      use in _Curs_EXT_ENS
    endif
    if used('_Curs_GSUT_BCU')
      use in _Curs_GSUT_BCU
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsut_bba
  * ===============================================================================
    * AH_CreateFolder() - Crea la cartella passata come parametro
    *
    Function AH_CreateFolder(pDirectory)
    private lOldErr,lRet
    * On Error
    lOldErr=ON("Error")
    * Init Risultato
    lRet = True
    * Crea
    on error lRet=False
    *
    pDirectory=JUSTPATH(ADDBS(pDirectory))
    *
    MD (pDirectory)
    *
    if not(Empty(lOldErr))
      on error &lOldErr
    else
      on error
    endif   
    * 
    if not(lRet)
      = Ah_ErrorMsg("Impossibile creare la cartella %1",16,,pDirectory)
    endif
    * Risultato
    Return (lRet)
    endfunc
    *====================================================================================
    * controlla se il file zip passato contiene file in uno dei formati grafici elencati
    * l'indicazione del formato dei file grafici contenuti � presente in fondo al nome e,
    * se trovata, viene restituita.
    *
    * NOTA: questa funzione � replicata anche nella gsdm_bpd
    *
    * BMP - JPG - JPEG - PNG - GIF - TIF - TIFF 
    *____________________________________________________________________________________
    FUNCTION check_grafico(pDoc)
     IF ("_BMP_"$pDoc)
      RETURN "_BMP_"
     ENDIF
     IF ("_JPG_"$pDoc)
      RETURN "_JPG_"
     ENDIF
     IF ("_JPEG_"$pDoc)
      RETURN "_JPEG_"
     ENDIF
     IF ("_PNG_"$pDoc)
      RETURN "_PNG_"
     ENDIF
     IF ("_GIF_"$pDoc)
      RETURN "_GIF_"
     ENDIF
     IF ("_TIF_"$pDoc)
      RETURN "_TIF_"
     ENDIF
     IF ("_TIFF_"$pDoc)
      RETURN "_TIFF_"
     ENDIF
     RETURN ""
    ENDFUN
    *____________________________________________________________________________________
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pArrayParam,pIdSerial"
endproc
