* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kpt                                                        *
*              Valorizzazione protocollo telematico                            *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-12-21                                                      *
* Last revis.: 2015-01-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kpt",oParentObject))

* --- Class definition
define class tgscg_kpt as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 420
  Height = 135
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-23"
  HelpContextID=149562729
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  COC_MAST_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gscg_kpt"
  cComment = "Valorizzazione protocollo telematico"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CONFERMA = .F.
  w_DIPROTEC = space(17)
  o_DIPROTEC = space(17)
  w_DIPRODOC = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kptPag1","gscg_kpt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDIPROTEC_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_kpt
    This.bUpdated=.T.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CONFERMA=.f.
      .w_DIPROTEC=space(17)
      .w_DIPRODOC=0
      .w_CONFERMA=oParentObject.w_CONFERMA
      .w_DIPROTEC=oParentObject.w_DIPROTEC
      .w_DIPRODOC=oParentObject.w_DIPRODOC
        .w_CONFERMA = .t.
    endwith
    this.DoRTCalc(2,3,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.w_DIPROTEC=.w_DIPROTEC
      .oParentObject.w_DIPRODOC=.w_DIPRODOC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CONFERMA = .t.
        .DoRTCalc(2,2,.t.)
        if .o_DIPROTEC<>.w_DIPROTEC
            .w_DIPRODOC = 0
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDIPROTEC_1_2.value==this.w_DIPROTEC)
      this.oPgFrm.Page1.oPag.oDIPROTEC_1_2.value=this.w_DIPROTEC
    endif
    if not(this.oPgFrm.Page1.oPag.oDIPRODOC_1_3.value==this.w_DIPRODOC)
      this.oPgFrm.Page1.oPag.oDIPRODOC_1_3.value=this.w_DIPRODOC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DIPROTEC)) or not(VAL(.w_DIPROTEC)<>0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIPROTEC_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DIPROTEC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DIPRODOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIPRODOC_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DIPRODOC)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DIPROTEC = this.w_DIPROTEC
    return

enddefine

* --- Define pages as container
define class tgscg_kptPag1 as StdContainer
  Width  = 416
  height = 135
  stdWidth  = 416
  stdheight = 135
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDIPROTEC_1_2 as StdField with uid="XFZXBWGJXS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DIPROTEC", cQueryName = "DIPROTEC",;
    bObbl = .t. , nPag = 1, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico (prima parte)",;
    HelpContextID = 6105209,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=166, Top=33, cSayPict='"99999999999999999"', cGetPict='"99999999999999999"', InputMask=replicate('X',17), Alignment=1

  func oDIPROTEC_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_DIPROTEC)<>0)
    endwith
    return bRes
  endfunc

  add object oDIPRODOC_1_3 as StdField with uid="BVQNORKVAD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DIPRODOC", cQueryName = "DIPRODOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico (Parte seconda)",;
    HelpContextID = 262330247,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=318, Top=33, cSayPict='"999999"', cGetPict='"999999"'


  add object oBtn_1_4 as StdButton with uid="JNTOFUXNOR",left=293, top=81, width=48,height=45,;
    CpPicture="BMP\ok.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare";
    , HelpContextID = 209818646;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_DIPROTEC) And Not Empty(.w_DIPRODOC))
      endwith
    endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="RGOUFEOBUS",left=346, top=81, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 209818646;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_6 as StdString with uid="BMLKAAVFQY",Visible=.t., Left=10, Top=36,;
    Alignment=1, Width=152, Height=18,;
    Caption="Protocollo telematico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="RKSOLNRTSH",Visible=.t., Left=305, Top=34,;
    Alignment=0, Width=12, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kpt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
