* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_blz                                                        *
*              TABELLA LOCALIZZAZIONI ATTIVE                                   *
*                                                                              *
*      Author: Zucchetti S.P.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-07                                                      *
* Last revis.: 2010-04-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_blz",oParentObject)
return(i_retval)

define class tgsar_blz as StdBatch
  * --- Local variables
  * --- WorkFile variables
  AZIENDA_idx=0
  LOCALIZZAZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione tabella riferimenti localizzazioni attive
    do case
      case this.oParentObject.currentEvent="FormLoad"
        * --- Create temporary table LOCALIZZAZ
        i_nIdx=cp_AddTableDef('LOCALIZZAZ') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.AZIENDA_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"AZTIPLOC as LZCODICE,AZIVACOF as LZLOCDES "," from "+i_cTable;
              +" where 1=0";
              )
        this.LOCALIZZAZ_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Inserisco la localizzazione Italia (Default)
        * --- Insert into LOCALIZZAZ
        i_nConn=i_TableProp[this.LOCALIZZAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOCALIZZAZ_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOCALIZZAZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LZCODICE"+",LZLOCDES"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC("ITA"),'LOCALIZZAZ','LZCODICE');
          +","+cp_NullLink(cp_ToStrODBC("Italia"),'LOCALIZZAZ','LZLOCDES');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LZCODICE',"ITA",'LZLOCDES',"Italia")
          insert into (i_cTable) (LZCODICE,LZLOCDES &i_ccchkf. );
             values (;
               "ITA";
               ,"Italia";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Verifico se attiva la localizzazione Spagna
        if g_LESP = "S"
          * --- Insert into LOCALIZZAZ
          i_nConn=i_TableProp[this.LOCALIZZAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOCALIZZAZ_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOCALIZZAZ_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LZCODICE"+",LZLOCDES"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC("ESP"),'LOCALIZZAZ','LZCODICE');
            +","+cp_NullLink(cp_ToStrODBC("Spagna"),'LOCALIZZAZ','LZLOCDES');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LZCODICE',"ESP",'LZLOCDES',"Spagna")
            insert into (i_cTable) (LZCODICE,LZLOCDES &i_ccchkf. );
               values (;
                 "ESP";
                 ,"Spagna";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        * --- Verifico se attiva la localizzazione Romania
        if g_LRON = "S"
          * --- Insert into LOCALIZZAZ
          i_nConn=i_TableProp[this.LOCALIZZAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOCALIZZAZ_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOCALIZZAZ_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LZCODICE"+",LZLOCDES"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC("RON"),'LOCALIZZAZ','LZCODICE');
            +","+cp_NullLink(cp_ToStrODBC("Romania"),'LOCALIZZAZ','LZLOCDES');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LZCODICE',"RON",'LZLOCDES',"Romania")
            insert into (i_cTable) (LZCODICE,LZLOCDES &i_ccchkf. );
               values (;
                 "RON";
                 ,"Romania";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        * --- Insert into LOCALIZZAZ
        i_nConn=i_TableProp[this.LOCALIZZAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOCALIZZAZ_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOCALIZZAZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LZCODICE"+",LZLOCDES"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC("ALT"),'LOCALIZZAZ','LZCODICE');
          +","+cp_NullLink(cp_ToStrODBC("Altro"),'LOCALIZZAZ','LZLOCDES');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LZCODICE',"ALT",'LZLOCDES',"Altro")
          insert into (i_cTable) (LZCODICE,LZLOCDES &i_ccchkf. );
             values (;
               "ALT";
               ,"Altro";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.currentEvent="Done"
        * --- Elimino la tabella temporanea
        * --- Drop temporary table LOCALIZZAZ
        i_nIdx=cp_GetTableDefIdx('LOCALIZZAZ')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('LOCALIZZAZ')
        endif
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='*LOCALIZZAZ'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
