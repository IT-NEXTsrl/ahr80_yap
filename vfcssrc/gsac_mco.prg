* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_mco                                                        *
*              Contratti di acquisto                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_117]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-15                                                      *
* Last revis.: 2015-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsac_mco"))

* --- Class definition
define class tgsac_mco as StdTrsForm
  Top    = 1
  Left   = 4

  * --- Standard Properties
  Width  = 730
  Height = 403+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-09"
  HelpContextID=171284631
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=52

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CON_TRAM_IDX = 0
  CON_TRAD_IDX = 0
  ART_ICOL_IDX = 0
  CON_COSC_IDX = 0
  CONTI_IDX = 0
  GRUMERC_IDX = 0
  VALUTE_IDX = 0
  UNIMIS_IDX = 0
  cFile = "CON_TRAM"
  cFileDetail = "CON_TRAD"
  cKeySelect = "CONUMERO"
  cQueryFilter="COTIPCLF='F'"
  cKeyWhere  = "CONUMERO=this.w_CONUMERO"
  cKeyDetail  = "CONUMERO=this.w_CONUMERO and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"CONUMERO="+cp_ToStrODBC(this.w_CONUMERO)';

  cKeyDetailWhereODBC = '"CONUMERO="+cp_ToStrODBC(this.w_CONUMERO)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CON_TRAD.CONUMERO="+cp_ToStrODBC(this.w_CONUMERO)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CON_TRAD.CPROWNUM '
  cPrg = "gsac_mco"
  cComment = "Contratti di acquisto"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 9
  icon = "movi.ico"
  cAutoZoom = 'GSAC_MCO'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COTIPCLF = space(1)
  w_CONUMERO = space(15)
  w_CODATCON = ctod('  /  /  ')
  w_CODATINI = ctod('  /  /  ')
  w_CODATFIN = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_COCODCLF = space(15)
  o_COCODCLF = space(15)
  w_DESCLF = space(40)
  w_CLFVAL = space(3)
  w_CODESCON = space(50)
  w_COCODVAL = space(3)
  w_DESVAL = space(35)
  w_COAFFIDA = 0
  w_DECTOT = 0
  o_DECTOT = 0
  w_COIVALIS = space(1)
  w_COQUANTI = space(1)
  w_COFLUCOA = space(1)
  w_CALCPICT = 0
  w_CO__TIPO = space(1)
  w_CPROWNUM = 0
  w_COGRUMER = space(5)
  o_COGRUMER = space(5)
  w_COCODART = space(20)
  o_COCODART = space(20)
  w_DESGRU = space(35)
  w_DESART = space(40)
  w_DESGRAR = space(45)
  w_COQTAMIN = 0
  w_COLOTMUL = 0
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_COGIOAPP = 0
  w_CODART = space(20)
  w_COPRIORI = 0
  w_COPERRIP = 0
  w_CODMAG = space(5)
  w_CODDIS = space(20)
  w_COFLARTI = space(1)
  w_NORIGHE = .F.
  w_COPRCOST = space(1)
  w_OBSART = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_COUNIMIS = space(3)
  w_FLSERG = space(1)
  w_RIGHERIP = .F.
  w_CONTRNUMROW = 0
  w_QUANTI = 0
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0
  w_TIPART = space(2)
  w_PROPRE = space(1)

  * --- Children pointers
  GSVE_MSO = .NULL.
  GSAC_MCL = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CON_TRAM','gsac_mco')
    stdPageFrame::Init()
    *set procedure to GSVE_MSO additive
    *set procedure to GSAC_MCL additive
    with this
      .Pages(1).addobject("oPag","tgsac_mcoPag1","gsac_mco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Contratti di acquisto")
      .Pages(1).HelpContextID = 39062549
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCONUMERO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSVE_MSO
    *release procedure GSAC_MCL
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CON_COSC'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='UNIMIS'
    this.cWorkTables[7]='CON_TRAM'
    this.cWorkTables[8]='CON_TRAD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CON_TRAM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CON_TRAM_IDX,3]
  return

  function CreateChildren()
    this.GSVE_MSO = CREATEOBJECT('stdDynamicChild',this,'GSVE_MSO',this.oPgFrm.Page1.oPag.oLinkPC_2_4)
    this.GSVE_MSO.createrealchild()
    this.GSAC_MCL = CREATEOBJECT('stdLazyChild',this,'GSAC_MCL')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSVE_MSO)
      this.GSVE_MSO.DestroyChildrenChain()
      this.GSVE_MSO=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_4')
    if !ISNULL(this.GSAC_MCL)
      this.GSAC_MCL.DestroyChildrenChain()
      this.GSAC_MCL=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSVE_MSO.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAC_MCL.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSVE_MSO.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAC_MCL.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSVE_MSO.NewDocument()
    this.GSAC_MCL.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSVE_MSO.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_CONUMERO,"COCODICE";
             ,.w_CPROWNUM,"CONUMROW";
             )
      .GSAC_MCL.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_CONUMERO,"MCCODCON";
             ,.w_CPROWNUM,"MCNUMROW";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CONUMERO = NVL(CONUMERO,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CON_TRAM where CONUMERO=KeySet.CONUMERO
    *
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2],this.bLoadRecFilter,this.CON_TRAM_IDX,"gsac_mco")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CON_TRAM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CON_TRAM.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CON_TRAD.","CON_TRAM.")
      i_cTable = i_cTable+' CON_TRAM '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CONUMERO',this.w_CONUMERO  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_DESCLF = space(40)
        .w_CLFVAL = space(3)
        .w_DESVAL = space(35)
        .w_DECTOT = 0
        .w_CODMAG = space(5)
        .w_NORIGHE = .F.
        .w_DTOBSO = ctod("  /  /  ")
        .w_FLSERG = space(1)
        .w_RIGHERIP = .F.
        .w_CONTRNUMROW = 0
        .w_QUANTI = 0
        .w_COTIPCLF = NVL(COTIPCLF,space(1))
        .w_CONUMERO = NVL(CONUMERO,space(15))
        .w_CODATCON = NVL(cp_ToDate(CODATCON),ctod("  /  /  "))
        .w_CODATINI = NVL(cp_ToDate(CODATINI),ctod("  /  /  "))
        .w_CODATFIN = NVL(cp_ToDate(CODATFIN),ctod("  /  /  "))
        .w_OBTEST = .w_CODATFIN
        .w_COCODCLF = NVL(COCODCLF,space(15))
          if link_1_7_joined
            this.w_COCODCLF = NVL(ANCODICE107,NVL(this.w_COCODCLF,space(15)))
            this.w_DESCLF = NVL(ANDESCRI107,space(40))
            this.w_CLFVAL = NVL(ANCODVAL107,space(3))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO107),ctod("  /  /  "))
            this.w_CODMAG = NVL(ANMAGTER107,space(5))
          else
          .link_1_7('Load')
          endif
        .w_CODESCON = NVL(CODESCON,space(50))
        .w_COCODVAL = NVL(COCODVAL,space(3))
          if link_1_11_joined
            this.w_COCODVAL = NVL(VACODVAL111,NVL(this.w_COCODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL111,space(35))
            this.w_DECTOT = NVL(VADECUNI111,0)
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO111),ctod("  /  /  "))
          else
          .link_1_11('Load')
          endif
        .w_COAFFIDA = NVL(COAFFIDA,0)
        .w_COIVALIS = NVL(COIVALIS,space(1))
        .w_COQUANTI = NVL(COQUANTI,space(1))
        .w_COFLUCOA = NVL(COFLUCOA,space(1))
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CO__TIPO = NVL(CO__TIPO,space(1))
        .w_COFLARTI = NVL(COFLARTI,space(1))
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        cp_LoadRecExtFlds(this,'CON_TRAM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CON_TRAD where CONUMERO=KeySet.CONUMERO
      *                            and CPROWNUM=KeySet.CPROWNUM
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsac_mco
      * --- Setta Ordine per Gr.Merceologico, Articolo, Variante
      i_cOrder = 'order by COCODART, COGRUMER '
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CON_TRAD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAD_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CON_TRAD')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CON_TRAD.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CON_TRAD"
        link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CONUMERO',this.w_CONUMERO  )
        select * from (i_cTable) CON_TRAD where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESGRU = space(35)
          .w_DESART = space(40)
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_CODDIS = space(20)
          .w_OBSART = ctod("  /  /  ")
          .w_DATOBSO = ctod("  /  /  ")
          .w_TIPART = space(2)
          .w_PROPRE = space(1)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_COGRUMER = NVL(COGRUMER,space(5))
          if link_2_2_joined
            this.w_COGRUMER = NVL(GMCODICE202,NVL(this.w_COGRUMER,space(5)))
            this.w_DESGRU = NVL(GMDESCRI202,space(35))
          else
          .link_2_2('Load')
          endif
          .w_COCODART = NVL(COCODART,space(20))
          if link_2_3_joined
            this.w_COCODART = NVL(ARCODART203,NVL(this.w_COCODART,space(20)))
            this.w_DESART = NVL(ARDESART203,space(40))
            this.w_OBSART = NVL(cp_ToDate(ARDTOBSO203),ctod("  /  /  "))
            this.w_CODDIS = NVL(ARCODDIS203,space(20))
            this.w_UNMIS1 = NVL(ARUNMIS1203,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2203,space(3))
            this.w_FLSERG = NVL(ARFLSERG203,space(1))
            this.w_COUNIMIS = NVL(ARUNMIS1203,space(3))
            this.w_CODART = NVL(ARCODART203,space(20))
            this.w_TIPART = NVL(ARTIPART203,space(2))
            this.w_PROPRE = NVL(ARPROPRE203,space(1))
          else
          .link_2_3('Load')
          endif
        .w_DESGRAR = IIF(EMPTY(.w_COGRUMER), .w_DESART, .w_DESGRU)
          .w_COQTAMIN = NVL(COQTAMIN,0)
          .w_COLOTMUL = NVL(COLOTMUL,0)
          .w_COGIOAPP = NVL(COGIOAPP,0)
        .w_CODART = .w_COCODART
          .w_COPRIORI = NVL(COPRIORI,0)
          .w_COPERRIP = NVL(COPERRIP,0)
          .w_COPRCOST = NVL(COPRCOST,space(1))
          .w_COUNIMIS = NVL(COUNIMIS,space(3))
          * evitabile
          *.link_2_20('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_OBTEST = .w_CODATFIN
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsac_mco
    if this.w_COTIPCLF<>'F'
      this.BlankRec()
    endif
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_COTIPCLF=space(1)
      .w_CONUMERO=space(15)
      .w_CODATCON=ctod("  /  /  ")
      .w_CODATINI=ctod("  /  /  ")
      .w_CODATFIN=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_COCODCLF=space(15)
      .w_DESCLF=space(40)
      .w_CLFVAL=space(3)
      .w_CODESCON=space(50)
      .w_COCODVAL=space(3)
      .w_DESVAL=space(35)
      .w_COAFFIDA=0
      .w_DECTOT=0
      .w_COIVALIS=space(1)
      .w_COQUANTI=space(1)
      .w_COFLUCOA=space(1)
      .w_CALCPICT=0
      .w_CO__TIPO=space(1)
      .w_CPROWNUM=0
      .w_COGRUMER=space(5)
      .w_COCODART=space(20)
      .w_DESGRU=space(35)
      .w_DESART=space(40)
      .w_DESGRAR=space(45)
      .w_COQTAMIN=0
      .w_COLOTMUL=0
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_COGIOAPP=0
      .w_CODART=space(20)
      .w_COPRIORI=0
      .w_COPERRIP=0
      .w_CODMAG=space(5)
      .w_CODDIS=space(20)
      .w_COFLARTI=space(1)
      .w_NORIGHE=.f.
      .w_COPRCOST=space(1)
      .w_OBSART=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_COUNIMIS=space(3)
      .w_FLSERG=space(1)
      .w_RIGHERIP=.f.
      .w_CONTRNUMROW=0
      .w_QUANTI=0
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      .w_TIPART=space(2)
      .w_PROPRE=space(1)
      if .cFunction<>"Filter"
        .w_COTIPCLF = 'F'
        .DoRTCalc(2,2,.f.)
        .w_CODATCON = i_datsys
        .w_CODATINI = I_DATSYS
        .DoRTCalc(5,5,.f.)
        .w_OBTEST = .w_CODATFIN
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_COCODCLF))
         .link_1_7('Full')
        endif
        .DoRTCalc(8,10,.f.)
        .w_COCODVAL = IIF(EMPTY(.w_CLFVAL), g_PERVAL, .w_CLFVAL)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_COCODVAL))
         .link_1_11('Full')
        endif
        .DoRTCalc(12,14,.f.)
        .w_COIVALIS = 'N'
        .w_COQUANTI = 'S'
        .w_COFLUCOA = ' '
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CO__TIPO = 'C'
        .DoRTCalc(20,20,.f.)
        .w_COGRUMER = SPACE(5)
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_COGRUMER))
         .link_2_2('Full')
        endif
        .w_COCODART = SPACE(20)
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_COCODART))
         .link_2_3('Full')
        endif
        .DoRTCalc(23,24,.f.)
        .w_DESGRAR = IIF(EMPTY(.w_COGRUMER), .w_DESART, .w_DESGRU)
        .DoRTCalc(26,30,.f.)
        .w_CODART = .w_COCODART
        .DoRTCalc(32,35,.f.)
        .w_COFLARTI = ' '
        .w_NORIGHE = .F.
        .w_COPRCOST = 'N'
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .DoRTCalc(39,41,.f.)
        .w_COUNIMIS = .w_UNMIS1
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_COUNIMIS))
         .link_2_20('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(43,43,.f.)
        .w_RIGHERIP = .F.
      endif
    endwith
    cp_BlankRecExtFlds(this,'CON_TRAM')
    this.DoRTCalc(45,52,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCONUMERO_1_2.enabled = i_bVal
      .Page1.oPag.oCODATCON_1_3.enabled = i_bVal
      .Page1.oPag.oCODATINI_1_4.enabled = i_bVal
      .Page1.oPag.oCODATFIN_1_5.enabled = i_bVal
      .Page1.oPag.oCOCODCLF_1_7.enabled = i_bVal
      .Page1.oPag.oCODESCON_1_10.enabled = i_bVal
      .Page1.oPag.oCOCODVAL_1_11.enabled = i_bVal
      .Page1.oPag.oCOAFFIDA_1_13.enabled = i_bVal
      .Page1.oPag.oCOPRCOST_2_18.enabled = i_bVal
      .Page1.oPag.oCOUNIMIS_2_20.enabled = i_bVal
      .Page1.oPag.oObj_1_32.enabled = i_bVal
      .Page1.oPag.oObj_1_33.enabled = i_bVal
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      .Page1.oPag.oObj_1_36.enabled = i_bVal
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCONUMERO_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCONUMERO_1_2.enabled = .t.
        .Page1.oPag.oCODATCON_1_3.enabled = .t.
        .Page1.oPag.oCODATINI_1_4.enabled = .t.
        .Page1.oPag.oCODATFIN_1_5.enabled = .t.
        .Page1.oPag.oCOCODCLF_1_7.enabled = .t.
      endif
    endwith
    this.GSVE_MSO.SetStatus(i_cOp)
    this.GSAC_MCL.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CON_TRAM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSVE_MSO.SetChildrenStatus(i_cOp)
  *  this.GSAC_MCL.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPCLF,"COTIPCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CONUMERO,"CONUMERO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATCON,"CODATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATINI,"CODATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATFIN,"CODATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODCLF,"COCODCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODESCON,"CODESCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODVAL,"COCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COAFFIDA,"COAFFIDA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COIVALIS,"COIVALIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COQUANTI,"COQUANTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLUCOA,"COFLUCOA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CO__TIPO,"CO__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLARTI,"COFLARTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsac_mco
    * --- aggiunge alla Chiave ulteriore filtro su Ciclo
    IF NOT EMPTY(i_cWhere)
       IF AT('COTIPCLF', UPPER(i_cWhere))=0
          i_cWhere=i_cWhere+" and COTIPCLF='F'"
       ENDIF
    ENDIF
    
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    i_lTable = "CON_TRAM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CON_TRAM_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAC_SCO with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWNUM N(4);
      ,t_COGRUMER C(5);
      ,t_COCODART C(20);
      ,t_DESGRAR C(45);
      ,t_COQTAMIN N(12,3);
      ,t_COLOTMUL N(12,3);
      ,t_COGIOAPP N(3);
      ,t_COPRIORI N(1);
      ,t_COPRCOST N(3);
      ,t_COUNIMIS C(3);
      ,CPROWNUM N(10);
      ,t_DESGRU C(35);
      ,t_DESART C(40);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_CODART C(20);
      ,t_COPERRIP N(3);
      ,t_CODDIS C(20);
      ,t_OBSART D(8);
      ,t_DATOBSO D(8);
      ,t_TIPART C(2);
      ,t_PROPRE C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsac_mcobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1.controlsource=this.cTrsName+'.t_CPROWNUM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOGRUMER_2_2.controlsource=this.cTrsName+'.t_COGRUMER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODART_2_3.controlsource=this.cTrsName+'.t_COCODART'
    this.oPgFRm.Page1.oPag.oDESGRAR_2_7.controlsource=this.cTrsName+'.t_DESGRAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOQTAMIN_2_8.controlsource=this.cTrsName+'.t_COQTAMIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOLOTMUL_2_9.controlsource=this.cTrsName+'.t_COLOTMUL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOGIOAPP_2_12.controlsource=this.cTrsName+'.t_COGIOAPP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOPRIORI_2_14.controlsource=this.cTrsName+'.t_COPRIORI'
    this.oPgFRm.Page1.oPag.oCOPRCOST_2_18.controlsource=this.cTrsName+'.t_COPRCOST'
    this.oPgFRm.Page1.oPag.oCOUNIMIS_2_20.controlsource=this.cTrsName+'.t_COUNIMIS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(7)
    this.AddVLine(67)
    this.AddVLine(219)
    this.AddVLine(310)
    this.AddVLine(401)
    this.AddVLine(437)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CON_TRAM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CON_TRAM')
        i_extval=cp_InsertValODBCExtFlds(this,'CON_TRAM')
        local i_cFld
        i_cFld=" "+;
                  "(COTIPCLF,CONUMERO,CODATCON,CODATINI,CODATFIN"+;
                  ",COCODCLF,CODESCON,COCODVAL,COAFFIDA,COIVALIS"+;
                  ",COQUANTI,COFLUCOA,CO__TIPO,COFLARTI,UTCC"+;
                  ",UTDC,UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_COTIPCLF)+;
                    ","+cp_ToStrODBC(this.w_CONUMERO)+;
                    ","+cp_ToStrODBC(this.w_CODATCON)+;
                    ","+cp_ToStrODBC(this.w_CODATINI)+;
                    ","+cp_ToStrODBC(this.w_CODATFIN)+;
                    ","+cp_ToStrODBCNull(this.w_COCODCLF)+;
                    ","+cp_ToStrODBC(this.w_CODESCON)+;
                    ","+cp_ToStrODBCNull(this.w_COCODVAL)+;
                    ","+cp_ToStrODBC(this.w_COAFFIDA)+;
                    ","+cp_ToStrODBC(this.w_COIVALIS)+;
                    ","+cp_ToStrODBC(this.w_COQUANTI)+;
                    ","+cp_ToStrODBC(this.w_COFLUCOA)+;
                    ","+cp_ToStrODBC(this.w_CO__TIPO)+;
                    ","+cp_ToStrODBC(this.w_COFLARTI)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CON_TRAM')
        i_extval=cp_InsertValVFPExtFlds(this,'CON_TRAM')
        cp_CheckDeletedKey(i_cTable,0,'CONUMERO',this.w_CONUMERO)
        INSERT INTO (i_cTable);
              (COTIPCLF,CONUMERO,CODATCON,CODATINI,CODATFIN,COCODCLF,CODESCON,COCODVAL,COAFFIDA,COIVALIS,COQUANTI,COFLUCOA,CO__TIPO,COFLARTI,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_COTIPCLF;
                  ,this.w_CONUMERO;
                  ,this.w_CODATCON;
                  ,this.w_CODATINI;
                  ,this.w_CODATFIN;
                  ,this.w_COCODCLF;
                  ,this.w_CODESCON;
                  ,this.w_COCODVAL;
                  ,this.w_COAFFIDA;
                  ,this.w_COIVALIS;
                  ,this.w_COQUANTI;
                  ,this.w_COFLUCOA;
                  ,this.w_CO__TIPO;
                  ,this.w_COFLARTI;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CON_TRAD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAD_IDX,2])
      *
      * insert into CON_TRAD
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CONUMERO,COGRUMER,COCODART,COQTAMIN,COLOTMUL"+;
                  ",COGIOAPP,COPRIORI,COPERRIP,COPRCOST,COUNIMIS,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CONUMERO)+","+cp_ToStrODBCNull(this.w_COGRUMER)+","+cp_ToStrODBCNull(this.w_COCODART)+","+cp_ToStrODBC(this.w_COQTAMIN)+","+cp_ToStrODBC(this.w_COLOTMUL)+;
             ","+cp_ToStrODBC(this.w_COGIOAPP)+","+cp_ToStrODBC(this.w_COPRIORI)+","+cp_ToStrODBC(this.w_COPERRIP)+","+cp_ToStrODBC(this.w_COPRCOST)+","+cp_ToStrODBCNull(this.w_COUNIMIS)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CONUMERO',this.w_CONUMERO,'CPROWNUM',this.w_CPROWNUM)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CONUMERO,this.w_COGRUMER,this.w_COCODART,this.w_COQTAMIN,this.w_COLOTMUL"+;
                ",this.w_COGIOAPP,this.w_COPRIORI,this.w_COPERRIP,this.w_COPRCOST,this.w_COUNIMIS,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CON_TRAM
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CON_TRAM')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " COTIPCLF="+cp_ToStrODBC(this.w_COTIPCLF)+;
             ",CODATCON="+cp_ToStrODBC(this.w_CODATCON)+;
             ",CODATINI="+cp_ToStrODBC(this.w_CODATINI)+;
             ",CODATFIN="+cp_ToStrODBC(this.w_CODATFIN)+;
             ",COCODCLF="+cp_ToStrODBCNull(this.w_COCODCLF)+;
             ",CODESCON="+cp_ToStrODBC(this.w_CODESCON)+;
             ",COCODVAL="+cp_ToStrODBCNull(this.w_COCODVAL)+;
             ",COAFFIDA="+cp_ToStrODBC(this.w_COAFFIDA)+;
             ",COIVALIS="+cp_ToStrODBC(this.w_COIVALIS)+;
             ",COQUANTI="+cp_ToStrODBC(this.w_COQUANTI)+;
             ",COFLUCOA="+cp_ToStrODBC(this.w_COFLUCOA)+;
             ",CO__TIPO="+cp_ToStrODBC(this.w_CO__TIPO)+;
             ",COFLARTI="+cp_ToStrODBC(this.w_COFLARTI)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CON_TRAM')
          i_cWhere = cp_PKFox(i_cTable  ,'CONUMERO',this.w_CONUMERO  )
          UPDATE (i_cTable) SET;
              COTIPCLF=this.w_COTIPCLF;
             ,CODATCON=this.w_CODATCON;
             ,CODATINI=this.w_CODATINI;
             ,CODATFIN=this.w_CODATFIN;
             ,COCODCLF=this.w_COCODCLF;
             ,CODESCON=this.w_CODESCON;
             ,COCODVAL=this.w_COCODVAL;
             ,COAFFIDA=this.w_COAFFIDA;
             ,COIVALIS=this.w_COIVALIS;
             ,COQUANTI=this.w_COQUANTI;
             ,COFLUCOA=this.w_COFLUCOA;
             ,CO__TIPO=this.w_CO__TIPO;
             ,COFLARTI=this.w_COFLARTI;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_COGRUMER) OR NOT EMPTY(t_COCODART)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CON_TRAD_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAD_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSVE_MSO.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_CONUMERO,"COCODICE";
                     ,this.w_CPROWNUM,"CONUMROW";
                     )
              this.GSAC_MCL.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_CONUMERO,"MCCODCON";
                     ,this.w_CPROWNUM,"MCNUMROW";
                     )
              this.GSVE_MSO.mDelete()
              this.GSAC_MCL.mDelete()
              *
              * delete from CON_TRAD
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CON_TRAD
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " COGRUMER="+cp_ToStrODBCNull(this.w_COGRUMER)+;
                     ",COCODART="+cp_ToStrODBCNull(this.w_COCODART)+;
                     ",COQTAMIN="+cp_ToStrODBC(this.w_COQTAMIN)+;
                     ",COLOTMUL="+cp_ToStrODBC(this.w_COLOTMUL)+;
                     ",COGIOAPP="+cp_ToStrODBC(this.w_COGIOAPP)+;
                     ",COPRIORI="+cp_ToStrODBC(this.w_COPRIORI)+;
                     ",COPERRIP="+cp_ToStrODBC(this.w_COPERRIP)+;
                     ",COPRCOST="+cp_ToStrODBC(this.w_COPRCOST)+;
                     ",COUNIMIS="+cp_ToStrODBCNull(this.w_COUNIMIS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      COGRUMER=this.w_COGRUMER;
                     ,COCODART=this.w_COCODART;
                     ,COQTAMIN=this.w_COQTAMIN;
                     ,COLOTMUL=this.w_COLOTMUL;
                     ,COGIOAPP=this.w_COGIOAPP;
                     ,COPRIORI=this.w_COPRIORI;
                     ,COPERRIP=this.w_COPERRIP;
                     ,COPRCOST=this.w_COPRCOST;
                     ,COUNIMIS=this.w_COUNIMIS;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (NOT EMPTY(t_COGRUMER) OR NOT EMPTY(t_COCODART))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSVE_MSO.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_CONUMERO,"COCODICE";
             ,this.w_CPROWNUM,"CONUMROW";
             )
        this.GSVE_MSO.mReplace()
        this.GSAC_MCL.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_CONUMERO,"MCCODCON";
             ,this.w_CPROWNUM,"MCNUMROW";
             )
        this.GSAC_MCL.mReplace()
        this.GSVE_MSO.bSaveContext=.f.
        this.GSAC_MCL.bSaveContext=.f.
      endscan
     this.GSVE_MSO.bSaveContext=.t.
     this.GSAC_MCL.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_COGRUMER) OR NOT EMPTY(t_COCODART)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSVE_MSO : Deleting
        this.GSVE_MSO.bSaveContext=.f.
        this.GSVE_MSO.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CONUMERO,"COCODICE";
               ,this.w_CPROWNUM,"CONUMROW";
               )
        this.GSVE_MSO.bSaveContext=.t.
        this.GSVE_MSO.mDelete()
        * --- GSAC_MCL : Deleting
        this.GSAC_MCL.bSaveContext=.f.
        this.GSAC_MCL.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CONUMERO,"MCCODCON";
               ,this.w_CPROWNUM,"MCNUMROW";
               )
        this.GSAC_MCL.bSaveContext=.t.
        this.GSAC_MCL.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CON_TRAD_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAD_IDX,2])
        *
        * delete CON_TRAD
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
        *
        * delete CON_TRAM
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_COGRUMER) OR NOT EMPTY(t_COCODART)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    if i_bUpd
      with this
          .w_COTIPCLF = 'F'
        .DoRTCalc(2,5,.t.)
          .w_OBTEST = .w_CODATFIN
        .DoRTCalc(7,10,.t.)
        if .o_COCODCLF<>.w_COCODCLF
          .w_COCODVAL = IIF(EMPTY(.w_CLFVAL), g_PERVAL, .w_CLFVAL)
          .link_1_11('Full')
        endif
        .DoRTCalc(12,14,.t.)
          .w_COIVALIS = 'N'
          .w_COQUANTI = 'S'
          .w_COFLUCOA = ' '
        if .o_DECTOT<>.w_DECTOT
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
          .w_CO__TIPO = 'C'
        .DoRTCalc(20,20,.t.)
        if .o_COCODART<>.w_COCODART
          .w_COGRUMER = SPACE(5)
          .link_2_2('Full')
        endif
        if .o_COGRUMER<>.w_COGRUMER
          .w_COCODART = SPACE(20)
          .link_2_3('Full')
        endif
        .DoRTCalc(23,24,.t.)
          .w_DESGRAR = IIF(EMPTY(.w_COGRUMER), .w_DESART, .w_DESGRU)
        .DoRTCalc(26,30,.t.)
        if .o_COCODART<>.w_COCODART
          .w_CODART = .w_COCODART
        endif
        .DoRTCalc(32,37,.t.)
        if .o_COCODART<>.w_COCODART
          .w_COPRCOST = 'N'
        endif
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .DoRTCalc(39,41,.t.)
        if .o_COCODART<>.w_COCODART
          .w_COUNIMIS = .w_UNMIS1
          .link_2_20('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(43,52,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DESGRU with this.w_DESGRU
      replace t_DESART with this.w_DESART
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_CODART with this.w_CODART
      replace t_COPERRIP with this.w_COPERRIP
      replace t_CODDIS with this.w_CODDIS
      replace t_OBSART with this.w_OBSART
      replace t_DATOBSO with this.w_DATOBSO
      replace t_TIPART with this.w_TIPART
      replace t_PROPRE with this.w_PROPRE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOGRUMER_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOGRUMER_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOCODART_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOCODART_2_3.mCond()
    this.oPgFrm.Page1.oPag.oCOPRCOST_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCOPRCOST_2_18.mCond()
    this.oPgFrm.Page1.oPag.oCOUNIMIS_2_20.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCOUNIMIS_2_20.mCond()
    this.GSVE_MSO.enabled = this.oPgFrm.Page1.oPag.oLinkPC_2_4.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_2_16.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_16.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSAC_MCL.visible")=='L' And this.GSAC_MCL.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_16.enabled
      this.GSAC_MCL.HideChildrenChain()
    endif 
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oCOUNIMIS_2_20.visible=!this.oPgFrm.Page1.oPag.oCOUNIMIS_2_20.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCODCLF
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANMAGTER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCLF;
                     ,'ANCODICE',trim(this.w_COCODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANMAGTER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANMAGTER";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANMAGTER;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCODCLF_1_7'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANMAGTER";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANMAGTER;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANMAGTER";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANMAGTER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANMAGTER";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCLF;
                       ,'ANCODICE',this.w_COCODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANMAGTER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
      this.w_CLFVAL = NVL(_Link_.ANCODVAL,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_CODMAG = NVL(_Link_.ANMAGTER,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_COCODCLF = space(15)
      endif
      this.w_DESCLF = space(40)
      this.w_CLFVAL = space(3)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CODMAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_OBTEST<=.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto")
        endif
        this.w_COCODCLF = space(15)
        this.w_DESCLF = space(40)
        this.w_CLFVAL = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CODMAG = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.ANCODICE as ANCODICE107"+ ",link_1_7.ANDESCRI as ANDESCRI107"+ ",link_1_7.ANCODVAL as ANCODVAL107"+ ",link_1_7.ANDTOBSO as ANDTOBSO107"+ ",link_1_7.ANMAGTER as ANMAGTER107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on CON_TRAM.COCODCLF=link_1_7.ANCODICE"+" and CON_TRAM.COTIPCLF=link_1_7.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and CON_TRAM.COCODCLF=link_1_7.ANCODICE(+)"'+'+" and CON_TRAM.COTIPCLF=link_1_7.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCODVAL
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_COCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECUNI,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_COCODVAL))
          select VACODVAL,VADESVAL,VADECUNI,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_COCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECUNI,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_COCODVAL)+"%");

            select VACODVAL,VADESVAL,VADECUNI,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCOCODVAL_1_11'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECUNI,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECUNI,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECUNI,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_COCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_COCODVAL)
            select VACODVAL,VADESVAL,VADECUNI,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DECTOT = NVL(_Link_.VADECUNI,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DECTOT = 0
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
        endif
        this.w_COCODVAL = space(3)
        this.w_DESVAL = space(35)
        this.w_DECTOT = 0
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.VACODVAL as VACODVAL111"+ ",link_1_11.VADESVAL as VADESVAL111"+ ",link_1_11.VADECUNI as VADECUNI111"+ ",link_1_11.VADTOBSO as VADTOBSO111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on CON_TRAM.COCODVAL=link_1_11.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and CON_TRAM.COCODVAL=link_1_11.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COGRUMER
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COGRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_COGRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_COGRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COGRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COGRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCOGRUMER_2_2'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COGRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_COGRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_COGRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COGRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COGRUMER = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COGRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.GMCODICE as GMCODICE202"+ ","+cp_TransLinkFldName('link_2_2.GMDESCRI')+" as GMDESCRI202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on CON_TRAD.COGRUMER=link_2_2.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and CON_TRAD.COGRUMER=link_2_2.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCODART
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_COCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARCODDIS,ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARPROPRE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_COCODART))
          select ARCODART,ARDESART,ARDTOBSO,ARCODDIS,ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARPROPRE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_COCODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARCODDIS,ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARPROPRE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_COCODART)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARCODDIS,ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARPROPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCOCODART_2_3'),i_cWhere,'GSMA_BZA',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARCODDIS,ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARPROPRE";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARCODDIS,ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARPROPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARCODDIS,ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARPROPRE";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_COCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_COCODART)
            select ARCODART,ARDESART,ARDTOBSO,ARCODDIS,ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARPROPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_OBSART = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_CODDIS = NVL(_Link_.ARCODDIS,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_COUNIMIS = NVL(_Link_.ARUNMIS1,space(3))
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_PROPRE = NVL(_Link_.ARPROPRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_OBSART = ctod("  /  /  ")
      this.w_CODDIS = space(20)
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_FLSERG = space(1)
      this.w_COUNIMIS = space(3)
      this.w_CODART = space(20)
      this.w_TIPART = space(2)
      this.w_PROPRE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_OBSART,.w_OBTEST,'Articolo Obsoleto!')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_COCODART = space(20)
        this.w_DESART = space(40)
        this.w_OBSART = ctod("  /  /  ")
        this.w_CODDIS = space(20)
        this.w_UNMIS1 = space(3)
        this.w_UNMIS2 = space(3)
        this.w_FLSERG = space(1)
        this.w_COUNIMIS = space(3)
        this.w_CODART = space(20)
        this.w_TIPART = space(2)
        this.w_PROPRE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 11 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+11<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ARCODART as ARCODART203"+ ",link_2_3.ARDESART as ARDESART203"+ ",link_2_3.ARDTOBSO as ARDTOBSO203"+ ",link_2_3.ARCODDIS as ARCODDIS203"+ ",link_2_3.ARUNMIS1 as ARUNMIS1203"+ ",link_2_3.ARUNMIS2 as ARUNMIS2203"+ ",link_2_3.ARFLSERG as ARFLSERG203"+ ",link_2_3.ARUNMIS1 as ARUNMIS1203"+ ",link_2_3.ARCODART as ARCODART203"+ ",link_2_3.ARTIPART as ARTIPART203"+ ",link_2_3.ARPROPRE as ARPROPRE203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on CON_TRAD.COCODART=link_2_3.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+11
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and CON_TRAD.COCODART=link_2_3.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+11
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COUNIMIS
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_COUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_COUNIMIS))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oCOUNIMIS_2_20'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSMA1QUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_COUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_COUNIMIS)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COUNIMIS = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_COUNIMIS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUMLIS(.w_COCODART, IIF(.w_FLSERG='S', '***', .w_COUNIMIS), .w_UNMIS1, .w_UNMIS2) AND NOT EMPTY(.w_COUNIMIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_COUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCONUMERO_1_2.value==this.w_CONUMERO)
      this.oPgFrm.Page1.oPag.oCONUMERO_1_2.value=this.w_CONUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATCON_1_3.value==this.w_CODATCON)
      this.oPgFrm.Page1.oPag.oCODATCON_1_3.value=this.w_CODATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATINI_1_4.value==this.w_CODATINI)
      this.oPgFrm.Page1.oPag.oCODATINI_1_4.value=this.w_CODATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATFIN_1_5.value==this.w_CODATFIN)
      this.oPgFrm.Page1.oPag.oCODATFIN_1_5.value=this.w_CODATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODCLF_1_7.value==this.w_COCODCLF)
      this.oPgFrm.Page1.oPag.oCOCODCLF_1_7.value=this.w_COCODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_8.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_8.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCON_1_10.value==this.w_CODESCON)
      this.oPgFrm.Page1.oPag.oCODESCON_1_10.value=this.w_CODESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODVAL_1_11.value==this.w_COCODVAL)
      this.oPgFrm.Page1.oPag.oCOCODVAL_1_11.value=this.w_COCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_12.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_12.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOAFFIDA_1_13.value==this.w_COAFFIDA)
      this.oPgFrm.Page1.oPag.oCOAFFIDA_1_13.value=this.w_COAFFIDA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRAR_2_7.value==this.w_DESGRAR)
      this.oPgFrm.Page1.oPag.oDESGRAR_2_7.value=this.w_DESGRAR
      replace t_DESGRAR with this.oPgFrm.Page1.oPag.oDESGRAR_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPRCOST_2_18.RadioValue()==this.w_COPRCOST)
      this.oPgFrm.Page1.oPag.oCOPRCOST_2_18.SetRadio()
      replace t_COPRCOST with this.oPgFrm.Page1.oPag.oCOPRCOST_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCOUNIMIS_2_20.value==this.w_COUNIMIS)
      this.oPgFrm.Page1.oPag.oCOUNIMIS_2_20.value=this.w_COUNIMIS
      replace t_COUNIMIS with this.oPgFrm.Page1.oPag.oCOUNIMIS_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1.value==this.w_CPROWNUM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1.value=this.w_CPROWNUM
      replace t_CPROWNUM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOGRUMER_2_2.value==this.w_COGRUMER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOGRUMER_2_2.value=this.w_COGRUMER
      replace t_COGRUMER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOGRUMER_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODART_2_3.value==this.w_COCODART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODART_2_3.value=this.w_COCODART
      replace t_COCODART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODART_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOQTAMIN_2_8.value==this.w_COQTAMIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOQTAMIN_2_8.value=this.w_COQTAMIN
      replace t_COQTAMIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOQTAMIN_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOLOTMUL_2_9.value==this.w_COLOTMUL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOLOTMUL_2_9.value=this.w_COLOTMUL
      replace t_COLOTMUL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOLOTMUL_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOGIOAPP_2_12.value==this.w_COGIOAPP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOGIOAPP_2_12.value=this.w_COGIOAPP
      replace t_COGIOAPP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOGIOAPP_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOPRIORI_2_14.value==this.w_COPRIORI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOPRIORI_2_14.value=this.w_COPRIORI
      replace t_COPRIORI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOPRIORI_2_14.value
    endif
    cp_SetControlsValueExtFlds(this,'CON_TRAM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CONUMERO))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCONUMERO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CONUMERO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODATCON))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODATCON_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODATCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODATINI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODATINI_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CODATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODATFIN) or not(NOT .w_CODATFIN<.w_CODATINI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODATFIN_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CODATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data fine contratto inferiore alla data di inizio")
          case   (empty(.w_COCODCLF) or not(EMPTY(.w_DATOBSO) OR .w_OBTEST<=.w_DATOBSO))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOCODCLF_1_7.SetFocus()
            i_bnoObbl = !empty(.w_COCODCLF)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto")
          case   (empty(.w_COCODVAL) or not(CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOCODVAL_1_11.SetFocus()
            i_bnoObbl = !empty(.w_COCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsac_mco
      this.NotifyEvent('CheckRighe')
      If i_bRes And this.w_NORIGHE
         * -- Errore
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg = Ah_MsgFormat("Impossibile salvare il contratto senza righe di dettaglio")
      Endif
      If i_bRes And this.w_RIGHERIP
         * -- Errore
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg = ah_msgformat("Esiste gi� una riga di scaglione con la stessa quantit� (quantit� %1).%0Impossibile salvare il contratto.",alltrim(str(this.w_QUANTI)))
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(CHKDTOBS(.w_OBSART,.w_OBTEST,'Articolo Obsoleto!')) and (EMPTY(.w_COGRUMER)) and not(empty(.w_COCODART)) and (NOT EMPTY(.w_COGRUMER) OR NOT EMPTY(.w_COCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODART_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        case   (empty(.w_COUNIMIS) or not(CHKUMLIS(.w_COCODART, IIF(.w_FLSERG='S', '***', .w_COUNIMIS), .w_UNMIS1, .w_UNMIS2) AND NOT EMPTY(.w_COUNIMIS))) and (!Empty(.w_COCODART) and .w_COFLUCOA <>'S') and (NOT EMPTY(.w_COGRUMER) OR NOT EMPTY(.w_COCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oCOUNIMIS_2_20
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
      endcase
      i_bRes = i_bRes .and. .GSVE_MSO.CheckForm()
      i_bRes = i_bRes .and. .GSAC_MCL.CheckForm()
      if NOT EMPTY(.w_COGRUMER) OR NOT EMPTY(.w_COCODART)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COCODCLF = this.w_COCODCLF
    this.o_DECTOT = this.w_DECTOT
    this.o_COGRUMER = this.w_COGRUMER
    this.o_COCODART = this.w_COCODART
    * --- GSVE_MSO : Depends On
    this.GSVE_MSO.SaveDependsOn()
    * --- GSAC_MCL : Depends On
    this.GSAC_MCL.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_COGRUMER) OR NOT EMPTY(t_COCODART))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_COGRUMER=space(5)
      .w_COCODART=space(20)
      .w_DESGRU=space(35)
      .w_DESART=space(40)
      .w_DESGRAR=space(45)
      .w_COQTAMIN=0
      .w_COLOTMUL=0
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_COGIOAPP=0
      .w_CODART=space(20)
      .w_COPRIORI=0
      .w_COPERRIP=0
      .w_CODDIS=space(20)
      .w_COPRCOST=space(1)
      .w_OBSART=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_COUNIMIS=space(3)
      .w_TIPART=space(2)
      .w_PROPRE=space(1)
      .DoRTCalc(1,20,.f.)
        .w_COGRUMER = SPACE(5)
      .DoRTCalc(21,21,.f.)
      if not(empty(.w_COGRUMER))
        .link_2_2('Full')
      endif
        .w_COCODART = SPACE(20)
      .DoRTCalc(22,22,.f.)
      if not(empty(.w_COCODART))
        .link_2_3('Full')
      endif
      .DoRTCalc(23,24,.f.)
        .w_DESGRAR = IIF(EMPTY(.w_COGRUMER), .w_DESART, .w_DESGRU)
      .DoRTCalc(26,30,.f.)
        .w_CODART = .w_COCODART
      .DoRTCalc(32,37,.f.)
        .w_COPRCOST = 'N'
      .DoRTCalc(39,41,.f.)
        .w_COUNIMIS = .w_UNMIS1
      .DoRTCalc(42,42,.f.)
      if not(empty(.w_COUNIMIS))
        .link_2_20('Full')
      endif
    endwith
    this.DoRTCalc(43,52,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWNUM = t_CPROWNUM
    this.w_COGRUMER = t_COGRUMER
    this.w_COCODART = t_COCODART
    this.w_DESGRU = t_DESGRU
    this.w_DESART = t_DESART
    this.w_DESGRAR = t_DESGRAR
    this.w_COQTAMIN = t_COQTAMIN
    this.w_COLOTMUL = t_COLOTMUL
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_COGIOAPP = t_COGIOAPP
    this.w_CODART = t_CODART
    this.w_COPRIORI = t_COPRIORI
    this.w_COPERRIP = t_COPERRIP
    this.w_CODDIS = t_CODDIS
    this.w_COPRCOST = this.oPgFrm.Page1.oPag.oCOPRCOST_2_18.RadioValue(.t.)
    this.w_OBSART = t_OBSART
    this.w_DATOBSO = t_DATOBSO
    this.w_COUNIMIS = t_COUNIMIS
    this.w_TIPART = t_TIPART
    this.w_PROPRE = t_PROPRE
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_COGRUMER with this.w_COGRUMER
    replace t_COCODART with this.w_COCODART
    replace t_DESGRU with this.w_DESGRU
    replace t_DESART with this.w_DESART
    replace t_DESGRAR with this.w_DESGRAR
    replace t_COQTAMIN with this.w_COQTAMIN
    replace t_COLOTMUL with this.w_COLOTMUL
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_COGIOAPP with this.w_COGIOAPP
    replace t_CODART with this.w_CODART
    replace t_COPRIORI with this.w_COPRIORI
    replace t_COPERRIP with this.w_COPERRIP
    replace t_CODDIS with this.w_CODDIS
    replace t_COPRCOST with this.oPgFrm.Page1.oPag.oCOPRCOST_2_18.ToRadio()
    replace t_OBSART with this.w_OBSART
    replace t_DATOBSO with this.w_DATOBSO
    replace t_COUNIMIS with this.w_COUNIMIS
    replace t_TIPART with this.w_TIPART
    replace t_PROPRE with this.w_PROPRE
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsac_mcoPag1 as StdContainer
  Width  = 726
  height = 403
  stdWidth  = 726
  stdheight = 403
  resizeXpos=199
  resizeYpos=266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCONUMERO_1_2 as StdField with uid="UOURHEFMCD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CONUMERO", cQueryName = "CONUMERO",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice identificativo del contratto",;
    HelpContextID = 195048331,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=86, Top=11, InputMask=replicate('X',15)

  add object oCODATCON_1_3 as StdField with uid="UOCPWJCGJQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODATCON", cQueryName = "CODATCON",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di stipulazione del contratto",;
    HelpContextID = 222614412,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=274, Top=11

  add object oCODATINI_1_4 as StdField with uid="AKZXWBXBOX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODATINI", cQueryName = "CODATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit� del contratto",;
    HelpContextID = 121951121,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=518, Top=11

  add object oCODATFIN_1_5 as StdField with uid="GBWXRPYZKG",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODATFIN", cQueryName = "CODATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data fine contratto inferiore alla data di inizio",;
    ToolTipText = "Data di termine validit� del contratto",;
    HelpContextID = 96152692,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=624, Top=11

  func oCODATFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT .w_CODATFIN<.w_CODATINI)
    endwith
    return bRes
  endfunc

  add object oCOCODCLF_1_7 as StdField with uid="CRZYQRFSIV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_COCODCLF", cQueryName = "COCODCLF",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente o obsoleto",;
    ToolTipText = "Codice del fornitore associato al contratto",;
    HelpContextID = 29957228,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=86, Top=39, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCODCLF"

  func oCOCODCLF_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODCLF_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODCLF_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCODCLF_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oCOCODCLF_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_COCODCLF
    i_obj.ecpSave()
  endproc

  add object oDESCLF_1_8 as StdField with uid="DDPAMPGLMY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 180481482,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=223, Top=39, InputMask=replicate('X',40)

  add object oCODESCON_1_10 as StdField with uid="QNXPRAWNXD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODESCON", cQueryName = "CODESCON",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del contratto",;
    HelpContextID = 223400844,;
   bGlobalFont=.t.,;
    Height=21, Width=422, Left=86, Top=67, InputMask=replicate('X',50)

  add object oCOCODVAL_1_11 as StdField with uid="BRVTRTWDFM",rtseq=11,rtrep=.f.,;
    cFormVar = "w_COCODVAL", cQueryName = "COCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o incongruente o obsoleta",;
    ToolTipText = "Codice della valuta a cui � riferito il contratto",;
    HelpContextID = 188146574,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=86, Top=95, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_COCODVAL"

  func oCOCODVAL_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODVAL_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODVAL_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCOCODVAL_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oCOCODVAL_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_COCODVAL
    i_obj.ecpSave()
  endproc

  add object oDESVAL_1_12 as StdField with uid="MXYZEFSRIV",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 90107338,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=138, Top=95, InputMask=replicate('X',35)

  add object oCOAFFIDA_1_13 as StdField with uid="WSEOVXINCK",rtseq=13,rtrep=.f.,;
    cFormVar = "w_COAFFIDA", cQueryName = "COAFFIDA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Livello di affidabilit� del fornitore (da 0 a 9)",;
    HelpContextID = 136315801,;
   bGlobalFont=.t.,;
    Height=21, Width=25, Left=506, Top=95, cSayPict='"9"', cGetPict='"9"'


  add object oObj_1_32 as cp_runprogram with uid="WBWHMHMZGQ",left=5, top=418, width=227,height=23,;
    caption='GSVE_BRN(R)',;
   bGlobalFont=.t.,;
    prg="GSVE_BRN('R')",;
    cEvent = "CheckRighe",;
    nPag=1;
    , HelpContextID = 227330764


  add object oObj_1_33 as cp_runprogram with uid="MBFXUHBLVN",left=7, top=442, width=226,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSVE_BRN('N')",;
    cEvent = "w_COCODART Changed",;
    nPag=1;
    , HelpContextID = 187588634


  add object oObj_1_34 as cp_runprogram with uid="ZKOMECJMYE",left=244, top=442, width=230,height=19,;
    caption='GSVE_BRND)',;
   bGlobalFont=.t.,;
    prg="GSVE_BRN('D')",;
    cEvent = "w_CODATFIN Changed",;
    nPag=1;
    , HelpContextID = 227508748


  add object oObj_1_36 as cp_runprogram with uid="PPXGESGGTB",left=249, top=417, width=226,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSVE_BRN('C')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 187588634


  add object oObj_1_39 as cp_runprogram with uid="UUXBPKXIXW",left=4, top=464, width=226,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSVE_BRN('G')",;
    cEvent = "w_COGRUMER Changed",;
    nPag=1;
    , HelpContextID = 187588634


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=149, width=470,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="COGRUMER",Label1="Gr.merc.",Field2="COCODART",Label2="Articolo",Field3="COQTAMIN",Label3="Qta minima",Field4="COLOTMUL",Label4="Lotto multiplo",Field5="COGIOAPP",Label5="G.app.",Field6="COPRIORI",Label6="Prior.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49495430

  add object oStr_1_18 as StdString with uid="XFZRNTZFKU",Visible=.t., Left=213, Top=11,;
    Alignment=1, Width=59, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="QVPAIQSFPW",Visible=.t., Left=8, Top=67,;
    Alignment=1, Width=76, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="PDFYHSNFBL",Visible=.t., Left=355, Top=11,;
    Alignment=1, Width=160, Height=15,;
    Caption="Validit� dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="UUEOAHVCNB",Visible=.t., Left=596, Top=11,;
    Alignment=1, Width=26, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="UMPFHDRAYC",Visible=.t., Left=8, Top=95,;
    Alignment=1, Width=76, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="PBKCKHUNDL",Visible=.t., Left=493, Top=132,;
    Alignment=0, Width=225, Height=15,;
    Caption="Scaglioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="KWUKIRFVAD",Visible=.t., Left=8, Top=11,;
    Alignment=1, Width=76, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="BQRVAYJCOE",Visible=.t., Left=8, Top=39,;
    Alignment=1, Width=76, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="JLFAEJDUUM",Visible=.t., Left=399, Top=95,;
    Alignment=1, Width=104, Height=18,;
    Caption="Affidabilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="CVREWCNBFD",Visible=.t., Left=261, Top=377,;
    Alignment=1, Width=27, Height=16,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (Empty(.w_COCODART))
    endwith
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsve_mso",lower(this.oContained.GSVE_MSO.class))=0
        this.oContained.GSVE_MSO.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=168,;
    width=467+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=169,width=466+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='GRUMERC|ART_ICOL|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESGRAR_2_7.Refresh()
      this.Parent.oCOPRCOST_2_18.Refresh()
      this.Parent.oCOUNIMIS_2_20.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='GRUMERC'
        oDropInto=this.oBodyCol.oRow.oCOGRUMER_2_2
      case cFile='ART_ICOL'
        oDropInto=this.oBodyCol.oRow.oCOCODART_2_3
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_4 as stdDynamicChildContainer with uid="YWUFZBHAWO",bOnScreen=.t.,width=247,height=256,;
   left=477, top=149;


  func oLinkPC_2_4.mCond()
    with this.Parent.oContained
      return (.w_COQUANTI='S' AND .w_COFLUCOA<>'S' AND .cFunction<>'Query')
    endwith
  endfunc

  add object oDESGRAR_2_7 as StdTrsField with uid="PDETMTJAUQ",rtseq=25,rtrep=.t.,;
    cFormVar="w_DESGRAR",value=space(45),enabled=.f.,;
    HelpContextID = 257813962,;
    cTotal="", bFixedPos=.t., cQueryName = "DESGRAR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=343, Left=3, Top=348, InputMask=replicate('X',45)

  add object oLinkPC_2_16 as StdButton with uid="DYYZIZLHBK",width=48,height=45,;
   left=423, top=348,;
    CpPicture="bmp\lotti.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere all'elenco dei materiali forniti direttamente dal terzista (solo articoli con distinta base)";
    , HelpContextID = 87931454;
    , TabStop=.f.,Caption='\<Materiali';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_16.Click()
      this.Parent.oContained.GSAC_MCL.LinkPCClick()
    endproc

  func oLinkPC_2_16.mCond()
    with this.Parent.oContained
      return (g_COLA='S' AND NOT EMPTY(.w_COCODCLF) AND (NOT EMPTY(.w_CODDIS) OR g_PRFA='S' AND .w_TIPART='FS'))
    endwith
  endfunc

  add object oCOPRCOST_2_18 as StdTrsCheck with uid="DEWUGWWWUT",rtrep=.t.,;
    cFormVar="w_COPRCOST",  caption="Considera per costificazione",;
    HelpContextID = 37950342,;
    Left=3, Top=375,;
    cTotal="", cQueryName = "COPRCOST",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCOPRCOST_2_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COPRCOST,&i_cF..t_COPRCOST),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCOPRCOST_2_18.GetRadio()
    this.Parent.oContained.w_COPRCOST = this.RadioValue()
    return .t.
  endfunc

  func oCOPRCOST_2_18.ToRadio()
    this.Parent.oContained.w_COPRCOST=trim(this.Parent.oContained.w_COPRCOST)
    return(;
      iif(this.Parent.oContained.w_COPRCOST=='S',1,;
      0))
  endfunc

  func oCOPRCOST_2_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOPRCOST_2_18.mCond()
    with this.Parent.oContained
      return (g_COLA='S' AND NOT EMPTY(.w_COCODCLF) AND (NOT EMPTY(.w_CODDIS) OR g_PRFA='S' AND .w_TIPART='FS'))
    endwith
  endfunc

  add object oCOUNIMIS_2_20 as StdTrsField with uid="OKUPEKZMXG",rtseq=42,rtrep=.t.,;
    cFormVar="w_COUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura a cui il contratto si riferisce",;
    HelpContextID = 202980473,;
    cTotal="", bFixedPos=.t., cQueryName = "COUNIMIS",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=291, Top=376, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_COUNIMIS"

  func oCOUNIMIS_2_20.mCond()
    with this.Parent.oContained
      return (!Empty(.w_COCODART) and .w_COFLUCOA <>'S')
    endwith
  endfunc

  func oCOUNIMIS_2_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_COCODART))
    endwith
    endif
  endfunc

  func oCOUNIMIS_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOUNIMIS_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oCOUNIMIS_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oCOUNIMIS_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSMA1QUM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oCOUNIMIS_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_COUNIMIS
    i_obj.ecpSave()
  endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsac_mcoBodyRow as CPBodyRowCnt
  Width=457
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWNUM_2_1 as StdTrsField with uid="WHOYTMQXDK",rtseq=20,rtrep=.t.,;
    cFormVar="w_CPROWNUM",value=0,isprimarykey=.t.,;
    HelpContextID = 33944205,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=1, Width=1, Left=-2, Top=0

  add object oCOGRUMER_2_2 as StdTrsField with uid="IHLKLHDWFN",rtseq=21,rtrep=.t.,;
    cFormVar="w_COGRUMER",value=space(5),;
    ToolTipText = "Gruppo merceologico associato al contratto",;
    HelpContextID = 52667272,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_COGRUMER"

  func oCOGRUMER_2_2.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_COCODART))
    endwith
  endfunc

  func oCOGRUMER_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOGRUMER_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCOGRUMER_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCOGRUMER_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oCOGRUMER_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_COGRUMER
    i_obj.ecpSave()
  endproc

  add object oCOCODART_2_3 as StdTrsField with uid="BJCWCPHMVA",rtseq=22,rtrep=.t.,;
    cFormVar="w_COCODART",value=space(20),;
    ToolTipText = "Codice articolo associato al contratto",;
    HelpContextID = 3597190,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=152, Left=60, Top=0, cSayPict=[p_ART], cGetPict=[p_ART], InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_COCODART"

  func oCOCODART_2_3.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_COGRUMER))
    endwith
  endfunc

  func oCOCODART_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODART_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCOCODART_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCOCODART_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'',this.parent.oContained
  endproc
  proc oCOCODART_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_COCODART
    i_obj.ecpSave()
  endproc

  add object oCOQTAMIN_2_8 as StdTrsField with uid="PUVULHURYD",rtseq=26,rtrep=.t.,;
    cFormVar="w_COQTAMIN",value=0,;
    ToolTipText = "Quantit� minima acquistabile",;
    HelpContextID = 194968692,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=90, Left=213, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]

  add object oCOLOTMUL_2_9 as StdTrsField with uid="EJCUYIQQWT",rtseq=27,rtrep=.t.,;
    cFormVar="w_COLOTMUL",value=0,;
    ToolTipText = "Quantit� ordinabile in lotti di n.",;
    HelpContextID = 53891982,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=90, Left=304, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]

  add object oCOGIOAPP_2_12 as StdTrsField with uid="SGDPFMVXCX",rtseq=30,rtrep=.t.,;
    cFormVar="w_COGIOAPP",value=0,;
    ToolTipText = "Numero di giorni necessario all'approvvigionamento dell'articolo",;
    HelpContextID = 260875146,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=395, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oCOPRIORI_2_14 as StdTrsField with uid="CTIBNWXMML",rtseq=32,rtrep=.t.,;
    cFormVar="w_COPRIORI",value=0,;
    ToolTipText = "Livello di priorit� (da 0 a 9)",;
    HelpContextID = 31658897,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=432, Top=0, cSayPict=["9"], cGetPict=["9"]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWNUM_2_1.When()
    return(.t.)
  proc oCPROWNUM_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWNUM_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=8
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="COTIPCLF='F'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".CONUMERO=CON_TRAD.CONUMERO";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsac_mco','CON_TRAM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CONUMERO=CON_TRAM.CONUMERO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
