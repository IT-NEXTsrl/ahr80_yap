* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bnu                                                        *
*              Elimina i null                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_47]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-10                                                      *
* Last revis.: 2000-01-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bnu",oParentObject)
return(i_retval)

define class tgsut_bnu as StdBatch
  * --- Local variables
  w_NOMARC = space(10)
  iNConn = 0
  w_NOMTBL = space(10)
  i_NT = 0
  w_TYPFLD = space(10)
  w_TIPOPE = space(10)
  w_NOMFLD = space(10)
  w_LENFLD = 0
  w_MESS = space(10)
  w_DECFLD = 0
  w_OK = .f.
  w_OPERAZ = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina i NULL dai Campi Numerici (da GSUT_KNU)
    * --- codice azienda da sistemare
    * --- Test sul tipo Database
    if EMPTY(CP_DBTYPE) OR EMPTY(CP_ODBCCONN)
      ah_ErrorMsg("IL DATABASE INSTALLATO NON PREVEDE VALORI NULLI",,"")
      i_retcode = 'stop'
      return
    endif
    if NOT cp_ReadXdc()
      ah_ErrorMsg("IMPOSSIBILE LEGGERE DIZIONARIO DATI",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Legge Tabella Aziende
    i_i = cp_SQL(i_ServerConn[1,2],"select PHNAME, FILENAME from cpttbls","ZOOMTBL")
    if USED("ZOOMTBL")
      SELECT ZOOMTBL
      GO TOP
      SCAN FOR AT("xxx", NVL(PHNAME, " "))<>0 AND NOT EMPTY(NVL(FILENAME," "))
      this.w_NOMARC = UPPER(ALLTRIM(FILENAME))
      if this.oParentObject.w_FLCONF<>"S" OR ah_YesNo("Confermi scansione tabella: %1","",this.w_NOMARC)
        * --- Try
        local bErr_026E32A0
        bErr_026E32A0=bTrsErr
        this.Try_026E32A0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_Msg("Errore: scansione tabella: %1%0Campo: %2",.T.,.F.,.F., this.w_NOMARC,this.w_NOMFLD)
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_026E32A0
        * --- End
      endif
      * --- Chiudo la tabella
      cp_CloseTable(this.w_NOMARC)
      SELECT ZOOMTBL
      ENDSCAN
      SELECT ZOOMTBL
      USE
    endif
    ah_Msg("Scansione tabelle completata",.T.)
  endproc
  proc Try_026E32A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.i_NT = cp_OpenTable(this.w_NOMARC, IIF(this.oParentObject.w_FLCONF="S", .F., .T.))
    if this.i_NT<>0
      this.iNConn = i_TableProp[this.i_NT,3]
      if this.iNConn<>0
        FOR L_j = 1 TO i_dcx.GetFieldsCount(this.w_NOMARC)
        this.w_NOMFLD = ALLTRIM(i_dcx.GetFieldName(this.w_NOMARC,L_j))
        this.w_TYPFLD = ALLTRIM(i_dcx.GetFieldType(this.w_NOMARC,L_j))
        this.w_LENFLD = i_dcx.GetFieldLen(this.w_NOMARC,L_j)
        this.w_DECFLD = i_dcx.GetFieldDec(this.w_NOMARC,L_j)
        if this.w_TYPFLD="N" OR (this.w_TYPFLD="C" AND this.w_LENFLD=1 AND this.oParentObject.w_FLNUME<>"S")
          ah_Msg("Scansione tabella: %1%0Campo: %2",.T.,.F.,.F., this.w_NOMARC, this.w_NOMFLD)
          this.w_NOMTBL = ALLTRIM(this.oParentObject.w_CODAZI)+this.w_NOMARC
          this.w_TIPOPE = this.w_NOMFLD + " = "+ IIF(this.w_TYPFLD="N", "0 ", "' '")
          this.w_OPERAZ = "UPDATE " + this.w_NOMTBL + " SET " + this.w_TIPOPE + " WHERE " + this.w_NOMFLD + " IS NULL"
          this.w_OK = (cp_TrsSQL(this.iNConn, this.w_OPERAZ))
        endif
        NEXT
        * --- Pulizia campi null su CPCCCHK
        if this.oParentObject.w_FLCPCCCHK="S"
          this.w_NOMTBL = ALLTRIM(this.oParentObject.w_CODAZI)+this.w_NOMARC
          this.w_OPERAZ = "UPDATE " + this.w_NOMTBL + " SET cpccchk = 'aaaaaaaaaa' WHERE cpccchk IS NULL"
          this.w_OK = (cp_TrsSQL(this.iNConn, this.w_OPERAZ))
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
