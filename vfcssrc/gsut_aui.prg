* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_aui                                                        *
*              Messaggi                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-11-19                                                      *
* Last revis.: 2014-09-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_aui"))

* --- Class definition
define class tgsut_aui as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 642
  Height = 344+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-18"
  HelpContextID=192330903
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  ANA_CUID_IDX = 0
  cFile = "ANA_CUID"
  cKeySelect = "AUCODUID"
  cKeyWhere  = "AUCODUID=this.w_AUCODUID"
  cKeyWhereODBC = '"AUCODUID="+cp_ToStrODBC(this.w_AUCODUID)';

  cKeyWhereODBCqualified = '"ANA_CUID.AUCODUID="+cp_ToStrODBC(this.w_AUCODUID)';

  cPrg = "gsut_aui"
  cComment = "Messaggi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AUCODUID = space(100)
  w_AU__FROM = space(100)
  w_AUOGGETT = space(0)
  w_AUDATINV = ctot('')
  w_AU__FRO2 = space(100)
  w_AUOGGET2 = space(0)
  w_AUDATIN2 = ctod('  /  /  ')
  w_AUNOMFIL = space(100)
  w_AUMOTIVO = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ANA_CUID','gsut_aui')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_auiPag1","gsut_aui",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Messaggi")
      .Pages(1).HelpContextID = 117352657
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAUCODUID_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ANA_CUID'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANA_CUID_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANA_CUID_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_AUCODUID = NVL(AUCODUID,space(100))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ANA_CUID where AUCODUID=KeySet.AUCODUID
    *
    i_nConn = i_TableProp[this.ANA_CUID_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANA_CUID_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANA_CUID')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANA_CUID.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANA_CUID '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AUCODUID',this.w_AUCODUID  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AUCODUID = NVL(AUCODUID,space(100))
        .w_AU__FROM = NVL(AU__FROM,space(100))
        .w_AUOGGETT = NVL(AUOGGETT,space(0))
        .w_AUDATINV = NVL(AUDATINV,ctot(""))
        .w_AU__FRO2 = NVL(AU__FRO2,space(100))
        .w_AUOGGET2 = NVL(AUOGGET2,space(0))
        .w_AUDATIN2 = NVL(cp_ToDate(AUDATIN2),ctod("  /  /  "))
        .w_AUNOMFIL = NVL(AUNOMFIL,space(100))
        .w_AUMOTIVO = NVL(AUMOTIVO,space(0))
        cp_LoadRecExtFlds(this,'ANA_CUID')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AUCODUID = space(100)
      .w_AU__FROM = space(100)
      .w_AUOGGETT = space(0)
      .w_AUDATINV = ctot("")
      .w_AU__FRO2 = space(100)
      .w_AUOGGET2 = space(0)
      .w_AUDATIN2 = ctod("  /  /  ")
      .w_AUNOMFIL = space(100)
      .w_AUMOTIVO = space(0)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANA_CUID')
    this.DoRTCalc(1,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oAUCODUID_1_1.enabled = i_bVal
      .Page1.oPag.oAU__FROM_1_4.enabled = i_bVal
      .Page1.oPag.oAUOGGETT_1_5.enabled = i_bVal
      .Page1.oPag.oAUDATINV_1_8.enabled = i_bVal
      .Page1.oPag.oAU__FRO2_1_10.enabled = i_bVal
      .Page1.oPag.oAUOGGET2_1_11.enabled = i_bVal
      .Page1.oPag.oAUNOMFIL_1_14.enabled = i_bVal
      .Page1.oPag.oAUMOTIVO_1_17.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oAUCODUID_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oAUCODUID_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ANA_CUID',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANA_CUID_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AUCODUID,"AUCODUID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AU__FROM,"AU__FROM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AUOGGETT,"AUOGGETT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AUDATINV,"AUDATINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AU__FRO2,"AU__FRO2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AUOGGET2,"AUOGGET2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AUDATIN2,"AUDATIN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AUNOMFIL,"AUNOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AUMOTIVO,"AUMOTIVO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANA_CUID_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANA_CUID_IDX,2])
    i_lTable = "ANA_CUID"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ANA_CUID_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANA_CUID_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANA_CUID_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ANA_CUID_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ANA_CUID
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANA_CUID')
        i_extval=cp_InsertValODBCExtFlds(this,'ANA_CUID')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AUCODUID,AU__FROM,AUOGGETT,AUDATINV,AU__FRO2"+;
                  ",AUOGGET2,AUDATIN2,AUNOMFIL,AUMOTIVO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AUCODUID)+;
                  ","+cp_ToStrODBC(this.w_AU__FROM)+;
                  ","+cp_ToStrODBC(this.w_AUOGGETT)+;
                  ","+cp_ToStrODBC(this.w_AUDATINV)+;
                  ","+cp_ToStrODBC(this.w_AU__FRO2)+;
                  ","+cp_ToStrODBC(this.w_AUOGGET2)+;
                  ","+cp_ToStrODBC(this.w_AUDATIN2)+;
                  ","+cp_ToStrODBC(this.w_AUNOMFIL)+;
                  ","+cp_ToStrODBC(this.w_AUMOTIVO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANA_CUID')
        i_extval=cp_InsertValVFPExtFlds(this,'ANA_CUID')
        cp_CheckDeletedKey(i_cTable,0,'AUCODUID',this.w_AUCODUID)
        INSERT INTO (i_cTable);
              (AUCODUID,AU__FROM,AUOGGETT,AUDATINV,AU__FRO2,AUOGGET2,AUDATIN2,AUNOMFIL,AUMOTIVO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AUCODUID;
                  ,this.w_AU__FROM;
                  ,this.w_AUOGGETT;
                  ,this.w_AUDATINV;
                  ,this.w_AU__FRO2;
                  ,this.w_AUOGGET2;
                  ,this.w_AUDATIN2;
                  ,this.w_AUNOMFIL;
                  ,this.w_AUMOTIVO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ANA_CUID_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANA_CUID_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ANA_CUID_IDX,i_nConn)
      *
      * update ANA_CUID
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ANA_CUID')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " AU__FROM="+cp_ToStrODBC(this.w_AU__FROM)+;
             ",AUOGGETT="+cp_ToStrODBC(this.w_AUOGGETT)+;
             ",AUDATINV="+cp_ToStrODBC(this.w_AUDATINV)+;
             ",AU__FRO2="+cp_ToStrODBC(this.w_AU__FRO2)+;
             ",AUOGGET2="+cp_ToStrODBC(this.w_AUOGGET2)+;
             ",AUDATIN2="+cp_ToStrODBC(this.w_AUDATIN2)+;
             ",AUNOMFIL="+cp_ToStrODBC(this.w_AUNOMFIL)+;
             ",AUMOTIVO="+cp_ToStrODBC(this.w_AUMOTIVO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ANA_CUID')
        i_cWhere = cp_PKFox(i_cTable  ,'AUCODUID',this.w_AUCODUID  )
        UPDATE (i_cTable) SET;
              AU__FROM=this.w_AU__FROM;
             ,AUOGGETT=this.w_AUOGGETT;
             ,AUDATINV=this.w_AUDATINV;
             ,AU__FRO2=this.w_AU__FRO2;
             ,AUOGGET2=this.w_AUOGGET2;
             ,AUDATIN2=this.w_AUDATIN2;
             ,AUNOMFIL=this.w_AUNOMFIL;
             ,AUMOTIVO=this.w_AUMOTIVO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANA_CUID_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANA_CUID_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ANA_CUID_IDX,i_nConn)
      *
      * delete ANA_CUID
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AUCODUID',this.w_AUCODUID  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANA_CUID_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANA_CUID_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAUCODUID_1_1.value==this.w_AUCODUID)
      this.oPgFrm.Page1.oPag.oAUCODUID_1_1.value=this.w_AUCODUID
    endif
    if not(this.oPgFrm.Page1.oPag.oAU__FROM_1_4.value==this.w_AU__FROM)
      this.oPgFrm.Page1.oPag.oAU__FROM_1_4.value=this.w_AU__FROM
    endif
    if not(this.oPgFrm.Page1.oPag.oAUOGGETT_1_5.value==this.w_AUOGGETT)
      this.oPgFrm.Page1.oPag.oAUOGGETT_1_5.value=this.w_AUOGGETT
    endif
    if not(this.oPgFrm.Page1.oPag.oAUDATINV_1_8.value==this.w_AUDATINV)
      this.oPgFrm.Page1.oPag.oAUDATINV_1_8.value=this.w_AUDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oAU__FRO2_1_10.value==this.w_AU__FRO2)
      this.oPgFrm.Page1.oPag.oAU__FRO2_1_10.value=this.w_AU__FRO2
    endif
    if not(this.oPgFrm.Page1.oPag.oAUOGGET2_1_11.value==this.w_AUOGGET2)
      this.oPgFrm.Page1.oPag.oAUOGGET2_1_11.value=this.w_AUOGGET2
    endif
    if not(this.oPgFrm.Page1.oPag.oAUNOMFIL_1_14.value==this.w_AUNOMFIL)
      this.oPgFrm.Page1.oPag.oAUNOMFIL_1_14.value=this.w_AUNOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oAUMOTIVO_1_17.value==this.w_AUMOTIVO)
      this.oPgFrm.Page1.oPag.oAUMOTIVO_1_17.value=this.w_AUMOTIVO
    endif
    cp_SetControlsValueExtFlds(this,'ANA_CUID')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  func CanEdit()
    local i_res
    i_res=1=0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione non consentita"))
    endif
    return(i_res)
  func CanAdd()
    local i_res
    i_res=1=0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione non consentita"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsut_auiPag1 as StdContainer
  Width  = 638
  height = 344
  stdWidth  = 638
  stdheight = 344
  resizeXpos=403
  resizeYpos=118
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAUCODUID_1_1 as StdField with uid="KHOXYXRWWB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_AUCODUID", cQueryName = "AUCODUID",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 183876022,;
   bGlobalFont=.t.,;
    Height=21, Width=351, Left=80, Top=25, InputMask=replicate('X',100)

  add object oAU__FROM_1_4 as StdField with uid="KFYNLTMLRS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_AU__FROM", cQueryName = "AU__FROM",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Mittente",;
    HelpContextID = 37488211,;
   bGlobalFont=.t.,;
    Height=21, Width=550, Left=79, Top=117, InputMask=replicate('X',100)

  add object oAUOGGETT_1_5 as StdMemo with uid="ZEEIHEQEJE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_AUOGGETT", cQueryName = "AUOGGETT",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 181205414,;
   bGlobalFont=.t.,;
    Height=57, Width=550, Left=79, Top=48, bdisablefrasimodello=.t.

  add object oAUDATINV_1_8 as StdField with uid="MYPAYCZZEZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_AUDATINV", cQueryName = "AUDATINV",;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 167532124,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=504, Top=23

  add object oAU__FRO2_1_10 as StdField with uid="CWHNEFCSXZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_AU__FRO2", cQueryName = "AU__FRO2",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Mittente",;
    HelpContextID = 37488184,;
   bGlobalFont=.t.,;
    Height=21, Width=550, Left=79, Top=219, InputMask=replicate('X',100)

  add object oAUOGGET2_1_11 as StdMemo with uid="CUMWZDBWLA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AUOGGET2", cQueryName = "AUOGGET2",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 181205448,;
   bGlobalFont=.t.,;
    Height=57, Width=550, Left=79, Top=150, bdisablefrasimodello=.t.

  add object oAUNOMFIL_1_14 as StdField with uid="DOXZABLJPM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_AUNOMFIL", cQueryName = "AUNOMFIL",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Notifica:",;
    HelpContextID = 157616558,;
   bGlobalFont=.t.,;
    Height=21, Width=549, Left=79, Top=252, InputMask=replicate('X',100)

  add object oAUMOTIVO_1_17 as StdMemo with uid="JAKHSLYDGR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_AUMOTIVO", cQueryName = "AUMOTIVO",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 99948971,;
   bGlobalFont=.t.,;
    Height=57, Width=550, Left=79, Top=283

  add object oStr_1_2 as StdString with uid="FRMIQPBJXS",Visible=.t., Left=15, Top=29,;
    Alignment=1, Width=64, Height=18,;
    Caption="Codice Uid:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="BQBSKCOHJG",Visible=.t., Left=34, Top=121,;
    Alignment=1, Width=45, Height=18,;
    Caption="Mittente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="DCDAXLTPRC",Visible=.t., Left=33, Top=52,;
    Alignment=1, Width=46, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="ULCCNKHFRW",Visible=.t., Left=444, Top=27,;
    Alignment=1, Width=57, Height=18,;
    Caption="Data invio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="DJAEOTESOC",Visible=.t., Left=34, Top=223,;
    Alignment=1, Width=45, Height=18,;
    Caption="Mittente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="NSRTTWLKOP",Visible=.t., Left=33, Top=154,;
    Alignment=1, Width=46, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="ZGFBUVAOAM",Visible=.t., Left=32, Top=256,;
    Alignment=1, Width=47, Height=18,;
    Caption="Allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="DARDARGBWD",Visible=.t., Left=22, Top=284,;
    Alignment=1, Width=57, Height=18,;
    Caption="Motivo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_aui','ANA_CUID','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AUCODUID=ANA_CUID.AUCODUID";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
