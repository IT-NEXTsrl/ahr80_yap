* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_sen                                                        *
*              Stampa entit�                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [101] [VRS_25]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-22                                                      *
* Last revis.: 2013-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_sen",oParentObject))

* --- Class definition
define class tgsar_sen as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 638
  Height = 136
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-11-08"
  HelpContextID=211191959
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  ENT_MAST_IDX = 0
  cPrg = "gsar_sen"
  cComment = "Stampa entit�"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ENTIPENT = space(1)
  w_ENCODICE = space(15)
  w_ENDESCRI = space(50)
  w_TIPENT = space(1)
  w_NOMPRG = space(10)
  w_desprg = space(35)
  * --- Area Manuale = Declare Variables
  * --- gsar_sen
  * --- Per gestione sicurezza (a seconda del parametro)
  * --- gestisco e mostro all'utente un nome di maschera diverso..
  func getSecuritycode()
     return(IIF( this.oParentObject='V', 'gsva_sen', this.cPrg))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_senPag1","gsar_sen",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oENCODICE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ENT_MAST'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ENTIPENT=space(1)
      .w_ENCODICE=space(15)
      .w_ENDESCRI=space(50)
      .w_TIPENT=space(1)
      .w_NOMPRG=space(10)
      .w_desprg=space(35)
        .w_ENTIPENT = iif(type('this.oParentObject')='C' , .oParentObject , 'V' )
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ENCODICE))
          .link_1_3('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_NOMPRG = upper(.GetSecuritycode())
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate(.w_NOMPRG)
        .w_desprg = IIF(iif(type('this.oParentObject')='C' , .oParentObject , 'V' )='V',Ah_MsgFormat('Stampa entit� EDI'),Ah_Msgformat("Stampa entit� della logistica "))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(.w_NOMPRG)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        this.Calculate_BFHATBCQHN()
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(.w_NOMPRG)
    endwith
  return

  proc Calculate_AXROSJSFQU()
    with this
          * --- Cambia caption form
          .cComment = AH_MSGFORMAT(.w_DESPRG)
    endwith
  endproc
  proc Calculate_BFHATBCQHN()
    with this
          * --- Cambia cprg
          .cPrg = lower(.w_NOMPRG)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_AXROSJSFQU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ENCODICE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENT_MAST_IDX,3]
    i_lTable = "ENT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2], .t., this.ENT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ENCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZE',True,'ENT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ENCODICE like "+cp_ToStrODBC(trim(this.w_ENCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select ENCODICE,ENDESCRI,ENTIPENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ENCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ENCODICE',trim(this.w_ENCODICE))
          select ENCODICE,ENDESCRI,ENTIPENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ENCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ENCODICE)==trim(_Link_.ENCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ENDESCRI like "+cp_ToStrODBC(trim(this.w_ENCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select ENCODICE,ENDESCRI,ENTIPENT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ENDESCRI like "+cp_ToStr(trim(this.w_ENCODICE)+"%");

            select ENCODICE,ENDESCRI,ENTIPENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ENCODICE) and !this.bDontReportError
            deferred_cp_zoom('ENT_MAST','*','ENCODICE',cp_AbsName(oSource.parent,'oENCODICE_1_3'),i_cWhere,'GSAR_BZE',"Entit�",'GSVAVMEN.ENT_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,ENDESCRI,ENTIPENT";
                     +" from "+i_cTable+" "+i_lTable+" where ENCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',oSource.xKey(1))
            select ENCODICE,ENDESCRI,ENTIPENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ENCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,ENDESCRI,ENTIPENT";
                   +" from "+i_cTable+" "+i_lTable+" where ENCODICE="+cp_ToStrODBC(this.w_ENCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',this.w_ENCODICE)
            select ENCODICE,ENDESCRI,ENTIPENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ENCODICE = NVL(_Link_.ENCODICE,space(15))
      this.w_ENDESCRI = NVL(_Link_.ENDESCRI,space(50))
      this.w_TIPENT = NVL(_Link_.ENTIPENT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ENCODICE = space(15)
      endif
      this.w_ENDESCRI = space(50)
      this.w_TIPENT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPENT=.w_ENTIPENT or Empty(.w_ENCODICE)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice entit� non ammesso o incongruente!")
        endif
        this.w_ENCODICE = space(15)
        this.w_ENDESCRI = space(50)
        this.w_TIPENT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.ENCODICE,1)
      cp_ShowWarn(i_cKey,this.ENT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ENCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oENCODICE_1_3.value==this.w_ENCODICE)
      this.oPgFrm.Page1.oPag.oENCODICE_1_3.value=this.w_ENCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oENDESCRI_1_4.value==this.w_ENDESCRI)
      this.oPgFrm.Page1.oPag.oENDESCRI_1_4.value=this.w_ENDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPENT=.w_ENTIPENT or Empty(.w_ENCODICE))  and not(empty(.w_ENCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oENCODICE_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice entit� non ammesso o incongruente!")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_senPag1 as StdContainer
  Width  = 634
  height = 136
  stdWidth  = 634
  stdheight = 136
  resizeXpos=522
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oENCODICE_1_3 as StdField with uid="RBZAFSBJHX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ENCODICE", cQueryName = "ENCODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice entit� non ammesso o incongruente!",;
    ToolTipText = "Codice entit�",;
    HelpContextID = 97907829,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=136, Top=21, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ENT_MAST", cZoomOnZoom="GSAR_BZE", oKey_1_1="ENCODICE", oKey_1_2="this.w_ENCODICE"

  func oENCODICE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oENCODICE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oENCODICE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENT_MAST','*','ENCODICE',cp_AbsName(this.parent,'oENCODICE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZE',"Entit�",'GSVAVMEN.ENT_MAST_VZM',this.parent.oContained
  endproc
  proc oENCODICE_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ENCODICE=this.parent.oContained.w_ENCODICE
     i_obj.ecpSave()
  endproc

  add object oENDESCRI_1_4 as StdField with uid="OBUOQNIKWQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ENDESCRI", cQueryName = "ENDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 183493745,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=258, Top=21, InputMask=replicate('X',50)


  add object oBtn_1_7 as StdButton with uid="OTAFFIMBBS",left=522, top=83, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 234733034;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="LZKQEBMGJC",left=573, top=83, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 234733034;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_10 as cp_outputCombo with uid="ZLERXIVPWT",left=136, top=53, width=485,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 147681306

  add object oStr_1_2 as StdString with uid="FBXUDZQNPK",Visible=.t., Left=10, Top=22,;
    Alignment=1, Width=124, Height=18,;
    Caption="Entit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="KURZTBJWHQ",Visible=.t., Left=10, Top=54,;
    Alignment=1, Width=124, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_sen','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
