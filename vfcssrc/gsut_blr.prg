* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_blr                                                        *
*              Lettura descrizone chiave record                                *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-07                                                      *
* Last revis.: 2009-06-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pKEY,pTable
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_blr",oParentObject,m.pKEY,m.pTable)
return(i_retval)

define class tgsut_blr as StdBatch
  * --- Local variables
  pKEY = space(30)
  pTable = space(8)
  w_DESC = space(50)
  w_LENDESC = 0
  * --- WorkFile variables
  CAN_TIER_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per la lettura della descrizione associata ad una chiave record, passata come parametro
    *      
    *      - pKEY Chiave record
    *      - pTABLE archivio su cui effetuare la lettura del campo descrizione
    do case
      case this.pTABLE == "CAN_TIER"
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNDESCAN"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.pKEY);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNDESCAN;
            from (i_cTable) where;
                CNCODCAN = this.pKEY;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESC = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      otherwise
        this.w_DESC = "Tabella non gestita"
    endcase
    this.w_LENDESC = LEN(ALLTRIM(this.w_DESC))
    * --- Converto i caratteri non ammessi nel nome file
    this.w_DESC = CHRTRAN(this.w_DESC,[^\/:*?<>|&.' ],'----------------')
    this.w_DESC = LEFT(this.w_DESC,this.w_LENDESC)
    * --- Ritorno la descrizione letta
    descrec = this.w_DESC
  endproc


  proc Init(oParentObject,pKEY,pTable)
    this.pKEY=pKEY
    this.pTable=pTable
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAN_TIER'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pKEY,pTable"
endproc
