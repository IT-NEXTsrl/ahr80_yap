* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bma                                                        *
*              Traduce descrizioni moduli                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_9]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-08-29                                                      *
* Last revis.: 2006-09-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bma",oParentObject)
return(i_retval)

define class tgsut_bma as StdBatch
  * --- Local variables
  w_MOCODMOD = space(4)
  w_MODESMOD = space(50)
  w_MOTRADES = space(50)
  w_MOORDINA = 0
  * --- WorkFile variables
  ELENMODU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LANCIATO DA GSUT_KSU, TRADUCE I NOMI DEI MODULI DA MOSTRARE NELLA COMBO DELLA MASCHERA
    * --- Select from ELENMODU
    i_nConn=i_TableProp[this.ELENMODU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELENMODU_idx,2],.t.,this.ELENMODU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ELENMODU ";
           ,"_Curs_ELENMODU")
    else
      select * from (i_cTable);
        into cursor _Curs_ELENMODU
    endif
    if used('_Curs_ELENMODU')
      select _Curs_ELENMODU
      locate for 1=1
      do while not(eof())
      this.w_MOCODMOD = NVL( MOCODMOD , SPACE( 4 ) )
      this.w_MODESMOD = NVL( MODESMOD , SPACE( 50 ) )
      this.w_MOTRADES = LEFT( AH_MSGFORMAT( this.w_MODESMOD ) + SPACE( 50 ) , 50 )
      * --- Write into ELENMODU
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ELENMODU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ELENMODU_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ELENMODU_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MOTRADES ="+cp_NullLink(cp_ToStrODBC(this.w_MOTRADES),'ELENMODU','MOTRADES');
            +i_ccchkf ;
        +" where ";
            +"MOCODMOD = "+cp_ToStrODBC(this.w_MOCODMOD);
               )
      else
        update (i_cTable) set;
            MOTRADES = this.w_MOTRADES;
            &i_ccchkf. ;
         where;
            MOCODMOD = this.w_MOCODMOD;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_ELENMODU
        continue
      enddo
      use
    endif
    * --- Tradotti i moduli, ordina secondo la descrizione tradotta
    this.w_MOORDINA = 0
    * --- Select from ELENMODU
    i_nConn=i_TableProp[this.ELENMODU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELENMODU_idx,2],.t.,this.ELENMODU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ELENMODU ";
          +" order by MOTRADES";
           ,"_Curs_ELENMODU")
    else
      select * from (i_cTable);
       order by MOTRADES;
        into cursor _Curs_ELENMODU
    endif
    if used('_Curs_ELENMODU')
      select _Curs_ELENMODU
      locate for 1=1
      do while not(eof())
      this.w_MOCODMOD = NVL( MOCODMOD , SPACE( 4 ) )
      this.w_MOORDINA = this.w_MOORDINA + 10
      * --- Write into ELENMODU
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ELENMODU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ELENMODU_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ELENMODU_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MOORDINA ="+cp_NullLink(cp_ToStrODBC(this.w_MOORDINA),'ELENMODU','MOORDINA');
            +i_ccchkf ;
        +" where ";
            +"MOCODMOD = "+cp_ToStrODBC(this.w_MOCODMOD);
               )
      else
        update (i_cTable) set;
            MOORDINA = this.w_MOORDINA;
            &i_ccchkf. ;
         where;
            MOCODMOD = this.w_MOCODMOD;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_ELENMODU
        continue
      enddo
      use
    endif
    * --- Assegna il valore a MOORDINA in modo da avere la voce
    *     'Tutti i moduli' al primo posto e 
    *     'Personalizzazioni' all'ultimo posto
    this.w_MOCODMOD = "9999"
    this.w_MOORDINA = this.w_MOORDINA + 10
    * --- Write into ELENMODU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ELENMODU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELENMODU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ELENMODU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MOORDINA ="+cp_NullLink(cp_ToStrODBC(this.w_MOORDINA),'ELENMODU','MOORDINA');
          +i_ccchkf ;
      +" where ";
          +"MOCODMOD = "+cp_ToStrODBC(this.w_MOCODMOD);
             )
    else
      update (i_cTable) set;
          MOORDINA = this.w_MOORDINA;
          &i_ccchkf. ;
       where;
          MOCODMOD = this.w_MOCODMOD;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_MOCODMOD = "0000"
    this.w_MOORDINA = 0
    * --- Write into ELENMODU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ELENMODU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELENMODU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ELENMODU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MOORDINA ="+cp_NullLink(cp_ToStrODBC(this.w_MOORDINA),'ELENMODU','MOORDINA');
          +i_ccchkf ;
      +" where ";
          +"MOCODMOD = "+cp_ToStrODBC(this.w_MOCODMOD);
             )
    else
      update (i_cTable) set;
          MOORDINA = this.w_MOORDINA;
          &i_ccchkf. ;
       where;
          MOCODMOD = this.w_MOCODMOD;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ELENMODU'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ELENMODU')
      use in _Curs_ELENMODU
    endif
    if used('_Curs_ELENMODU')
      use in _Curs_ELENMODU
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
