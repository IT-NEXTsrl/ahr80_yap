* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bgf                                                        *
*              Generazione file IVA fisco azienda                              *
*                                                                              *
*      Author: Ballerini Emiliano                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39][VRS_64]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-01                                                      *
* Last revis.: 2011-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC,pCODSTR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bgf",oParentObject,m.pEXEC,m.pCODSTR)
return(i_retval)

define class tgscg_bgf as StdBatch
  * --- Local variables
  w_LCODTOT = space(10)
  w_TIPTOT = space(1)
  w_CODICE = space(10)
  w_CODFON = space(10)
  w_TIPFONT = space(1)
  w_NOMEVQR = space(254)
  w_FLDECI = space(1)
  w_FILE_TXT = space(254)
  w_LINEFEED = space(2)
  w_TMPS = space(254)
  w_HANDLEFILE = 0
  pEXEC = space(1)
  pCODSTR = space(15)
  w_CODTOT = space(10)
  w_PARAM = space(1)
  w_GEST_MANU = .NULL.
  w_NUMREC = 0
  w_PADRE = .NULL.
  w_INDICE = 0
  w_TIPDIM = space(2)
  w_TIPDAT = space(1)
  w_TIPPER = space(1)
  w_ARLINK = space(15)
  w_FORMUL = space(254)
  w_CODMIS = space(15)
  INDICE = 0
  w_OCCUR = 0
  w_OCCUR1 = 0
  w_OCCUR2 = 0
  w_CODFAZ = space(10)
  w_IMPORTO = 0
  TmpSave = space(100)
  w_DIFFE = 0
  w_TIPO = space(1)
  w_LIMPORTO = 0
  w_LCODFAZ = space(10)
  w_LCODMIS = space(15)
  w_INDMIS = 0
  w_INDTOT = 0
  w_STRINGA = space(50)
  w_LOOP = 0
  w_TOTADDE = space(26)
  w_RIMPORTO = space(18)
  w_ARLEN = 0
  w_NIMPORTO = 0
  w_dimensione = 0
  w_LIMPORTO = 0
  * --- WorkFile variables
  TOT_MAST_idx=0
  DET_DIME_idx=0
  TOT_DETT_idx=0
  ZOOMPART_idx=0
  TAB_FONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera File Esportazione Totalizzatori IVA per fisco azienda
    * --- G: Generazione file IVA fisco azienda GSCG_KGF
    *     C: Calcolo Totalizzatore da GSCG_MTI
    *     B: Bilancio IVA GSCG_BGB
    * --- Codice struttura da applicare (valorizzato da bilancio)
    * --- Dichiaro Array di Lavoro
    this.w_PARAM = this.pEXEC
    if this.w_PARAM<>"B"
      this.w_FILE_TXT = Alltrim( this.oParentObject.w_PATH ) + Alltrim( this.oParentObject.w_FILE )
    endif
     
 Dimension Totalizzatori(1,2),aMisure(1),aValori(1),aDimensioni(1),aCodfaz(1),aRisultati(1,2),aRemove(1)
    * --- Create temporary table ZOOMPART
    i_nIdx=cp_AddTableDef('ZOOMPART') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSCG0BGF',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.ZOOMPART_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from GSCG_BGF
    do vq_exec with 'GSCG_BGF',this,'_Curs_GSCG_BGF','',.f.,.t.
    if used('_Curs_GSCG_BGF')
      select _Curs_GSCG_BGF
      locate for 1=1
      do while not(eof())
      this.w_TIPTOT = _Curs_GSCG_BGF.TITIPTOT
      this.w_CODTOT = _Curs_GSCG_BGF.TICODICE
      this.w_LCODMIS = Nvl(_Curs_GSCG_BGF.TICODMIS,Space(15))
      this.w_CODICE = _Curs_GSCG_BGF.TICODICE
      this.w_TIPFONT = _Curs_GSCG_BGF.FO__TIPO
      this.w_NOMEVQR = _Curs_GSCG_BGF.FO__NOME
      this.w_FLDECI = Nvl(_Curs_GSCG_BGF.TIFLDECI,"N")
      this.w_CODFON = _Curs_GSCG_BGF.FOCODICE
      this.w_CODFAZ = Nvl(_Curs_GSCG_BGF.TICODFAZ,"")
      this.w_INDICE = 1
      ah_Msg("Elaborazione Totalizzatore %1...",.T.,.F.,.F.,this.w_codtot)
      this.w_INDTOT = IIF(this.w_TIPTOT="D",0,cp_ROUND(ASCAN(totalizzatori,alltrim(this.w_CODTOT)+"."+alltrim(this.w_LCODMIS))/2,0))
      if this.w_INDTOT=0
        * --- Elaboro intero totalizzatore
        do case
          case this.w_TIPTOT="S"
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            Faz_engine(this.w_TIPFONT,this.w_NOMEVQR,@aMisure,@aDimensioni,@aValori,@aCodFaz,@aRisultati,@aRemove)
            this.w_dimensione = Alen(aMisure)
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.w_TIPTOT="C"
            * --- Calcolo Totalizzatore composto
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.w_TIPTOT="M" and this.pEXEC="B"
            * --- Cerco importo totalizzatore nel relativo dettaglio della gestione bilancio
            this.w_LCODTOT = this.w_CODTOT
            this.Pag6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Insert into ZOOMPART
            i_nConn=i_TableProp[this.ZOOMPART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ZOOMPART_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CODFAZ"+",IMPORTO"+",FLDECI"+",CODMIS"+",CODTOT"+",TIPTOT"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_CODFAZ),'ZOOMPART','CODFAZ');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LIMPORTO),'ZOOMPART','IMPORTO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLDECI),'ZOOMPART','FLDECI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LCODMIS),'ZOOMPART','CODMIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODTOT),'ZOOMPART','CODTOT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_TIPTOT),'ZOOMPART','TIPTOT');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CODFAZ',this.w_CODFAZ,'IMPORTO',this.w_LIMPORTO,'FLDECI',this.w_FLDECI,'CODMIS',this.w_LCODMIS,'CODTOT',this.w_CODTOT,'TIPTOT',this.w_TIPTOT)
              insert into (i_cTable) (CODFAZ,IMPORTO,FLDECI,CODMIS,CODTOT,TIPTOT &i_ccchkf. );
                 values (;
                   this.w_CODFAZ;
                   ,this.w_LIMPORTO;
                   ,this.w_FLDECI;
                   ,this.w_LCODMIS;
                   ,this.w_CODTOT;
                   ,this.w_TIPTOT;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          otherwise
            * --- Totalizzatore Descrittivo\Manuali
            *     se eseguo calcolo i totalizzatori manuali vengono caricati con importo a zero
            * --- Insert into ZOOMPART
            i_nConn=i_TableProp[this.ZOOMPART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ZOOMPART_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CODFAZ"+",IMPORTO"+",FLDECI"+",CODMIS"+",CODTOT"+",TIPTOT"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_CODFAZ),'ZOOMPART','CODFAZ');
              +","+cp_NullLink(cp_ToStrODBC(0),'ZOOMPART','IMPORTO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLDECI),'ZOOMPART','FLDECI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LCODMIS),'ZOOMPART','CODMIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODTOT),'ZOOMPART','CODTOT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_TIPTOT),'ZOOMPART','TIPTOT');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CODFAZ',this.w_CODFAZ,'IMPORTO',0,'FLDECI',this.w_FLDECI,'CODMIS',this.w_LCODMIS,'CODTOT',this.w_CODTOT,'TIPTOT',this.w_TIPTOT)
              insert into (i_cTable) (CODFAZ,IMPORTO,FLDECI,CODMIS,CODTOT,TIPTOT &i_ccchkf. );
                 values (;
                   this.w_CODFAZ;
                   ,0;
                   ,this.w_FLDECI;
                   ,this.w_LCODMIS;
                   ,this.w_CODTOT;
                   ,this.w_TIPTOT;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
        endcase
      endif
        select _Curs_GSCG_BGF
        continue
      enddo
      use
    endif
    do case
      case this.pEXEC="G"
        * --- Creazione File Fisco Azienda
        if Not File(this.w_FILE_TXT) Or ah_YesNo("Il file esiste gi�, si desidera sovrascriverlo?")
          * --- Avvio la generazione del file TXT
          this.w_HANDLEFILE = FCREATE(this.w_FILE_TXT)
          if this.w_HANDLEFILE < 0
            ah_ErrorMsg("Errore nella creazione del file TXT %1.","!","",this.w_FILE_TXT)
            * --- Drop temporary table ZOOMPART
            i_nIdx=cp_GetTableDefIdx('ZOOMPART')
            if i_nIdx<>0
              cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
              cp_RemoveTableDef('ZOOMPART')
            endif
            i_retcode = 'stop'
            return
          endif
          this.w_LINEFEED = CHR(10)
          * --- Select from GSCG1BGF
          do vq_exec with 'GSCG1BGF',this,'_Curs_GSCG1BGF','',.f.,.t.
          if used('_Curs_GSCG1BGF')
            select _Curs_GSCG1BGF
            locate for 1=1
            do while not(eof())
            this.w_FLDECI = _Curs_GSCG1BGF.FLDECI
            this.w_TMPS = Alltrim(str(this.oParentObject.w_ANNO))
            * --- 4 identificativo Dichiarazione Iva annuale
            *     codice Faz comprendente 
            *     -Quadro 2 caratteri
            *     -Numero riga 3 caratteri
            *     -Numero colonna 3 caratteri
            this.w_TMPS = Alltrim(this.w_TMPS)+"4" + Left(alltrim(_Curs_GSCG1BGF.CODFAZ)+Space(8),8)
            this.w_IMPORTO = Nvl(_Curs_GSCG1BGF.IMPORTO, 0 )
            * --- segno : + oppure -. Se non impostato vale +
            this.w_TMPS = this.w_TMPS+IIF(SIGN(this.w_IMPORTO)=-1,"-","+")
            * --- L'importo va arrotondato all'unit� di euro...
            this.w_LIMPORTO = cp_Round( this.w_IMPORTO , 0)
            if this.w_FLDECI="S"
              if (this.w_IMPORTO-this.w_LIMPORTO)<>0
                this.w_TMPS = this.w_TMPS + "00000000000"
              else
                this.w_TMPS = this.w_TMPS + Right("00000000000"+ALLTRIM(STRTRAN(Str(this.w_LIMPORTO,11,0),"-","")),11)
              endif
            else
              this.w_TMPS = this.w_TMPS + Right("00000000000"+ALLTRIM(STRTRAN(Str(this.w_LIMPORTO,11,0),"-","")),11)
            endif
            * --- valuta di ingresso : E (Euro)
            this.w_TMPS = this.w_TMPS +"E"
            * --- descrizione : 40 caratteri alfanumerico
            this.w_TMPS = this.w_TMPS + Space(40)
            if this.w_FLDECI="S" AND (this.w_IMPORTO-this.w_LIMPORTO)<>0
              * --- Eventuali importo con decimali se  impostato flag decimali e importo con decimali
              this.w_TMPS = this.w_TMPS + Right("00000000000"+ALLTRIM(STRTRAN(Str(int(this.w_IMPORTO),11,0),"-","")),11)
              this.w_TMPS = this.w_TMPS + Right("00"+ALLTRIM(STRTRAN(Str((ABS(this.w_IMPORTO)-ABS(int(this.w_IMPORTO)))*100,2,2),"-","")),2)
            else
              this.w_TMPS = this.w_TMPS + Space(13)
            endif
            FWRITE(this.w_HANDLEFILE,this.w_TMPS +this.w_LINEFEED)
              select _Curs_GSCG1BGF
              continue
            enddo
            use
          endif
          * --- Aggiungere controllo sulla chiusura, per vedere se � andato tutto a buon fine?
          if not (FCLOSE(this.w_HANDLEFILE))
            ah_ErrorMsg("Errore in chiusura del file %1.%0Operazione annullata","!","",this.w_FILE_TXT)
          else
            ah_ErrorMsg("File generato con successo","i","")
          endif
        endif
      case this.pEXEC="C"
        this.w_PADRE = This.oParentObject
        this.oParentObject.w_ZOOMMIS.cCpQueryName = "QUERY\GSCG_SZF"
        this.w_PADRE.NotifyEvent("Esegui")     
        this.oParentObject.w_ZOOMMIS.cCpQueryName = "QUERY\GSCG0BGF"
      case this.pEXEC="B"
        * --- Preparo cursore da ripassare al GSCG_BGB
        vq_exec("..\exe\query\gscg_bgb",This,"curbilancio")
    endcase
    * --- Drop temporary table ZOOMPART
    i_nIdx=cp_GetTableDefIdx('ZOOMPART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('ZOOMPART')
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_INDICE = 1
    * --- Dichiaro Array di Lavoro
     
 Dimension aDimensioni(1) 
 Dimension aValori(1) 
 Dimension aRemove(1)
    * --- Select from GSCG2BGF
    do vq_exec with 'GSCG2BGF',this,'_Curs_GSCG2BGF','',.f.,.t.
    if used('_Curs_GSCG2BGF')
      select _Curs_GSCG2BGF
      locate for 1=1
      do while not(eof())
      this.w_TIPDIM = _Curs_GSCG2BGF.DITIPDIM
      this.w_TIPDAT = _Curs_GSCG2BGF.DITIPDAT
      this.w_ARLINK = Nvl(DIARLINK,Space(15)) 
      if ALEN(aDimensioni)<this.w_INDICE
         
 Dimension aDimensioni(this.w_INDICE)
      endif
      if ALEN(aValori)<this.w_INDICE
         
 Dimension aValori(this.w_INDICE) 
 Dimension aRemove(this.w_INDICE)
      endif
       
 aDimensioni(this.w_INDICE)=_Curs_GSCG2BGF.DICODICE 
 aRemove(this.w_INDICE)=_Curs_GSCG2BGF.DIFLZERO
      do case
        case this.w_TIPDIM = "LB"
          * --- Libera
          do case
            case this.w_TIPDAT="C"
              * --- carattere
              aValori(this.w_INDICE)=Alltrim(Nvl(_Curs_GSCG2BGF.DIVALCHR," "))
            case this.w_TIPDAT="N"
              * --- Numerico
              aValori(this.w_INDICE)=Nvl(_Curs_GSCG2BGF.DIVALNUM,0)
            case this.w_TIPDAT="D"
              * --- Data
              aValori(this.w_INDICE)=cp_todate(_Curs_GSCG2BGF.DIVALDAT)
          endcase
        case this.w_TIPDIM="LK"
          * --- Link
          do case
            case this.w_ARLINK="VOCIIVA"
              aValori(this.w_INDICE)=Nvl(_Curs_GSCG2BGF.DIVALLIN," ")
            case this.w_ARLINK="CAU_CONT"
              aValori(this.w_INDICE)=Nvl(_Curs_GSCG2BGF.DIVALCAU," ")
            case this.w_ARLINK="CONTI"
              aValori(this.w_INDICE)=Nvl(_Curs_GSCG2BGF.DIVALCON," ")
          endcase
        case this.w_TIPDIM="CC"
          * --- Combo
          do case
            case this.w_TIPDAT="C"
              * --- Carattere
              aValori(this.w_INDICE)=Alltrim(Nvl(_Curs_GSCG2BGF.DIVLCOMC," "))
            case this.w_TIPDAT="N"
              * --- Numerica
              aValori(this.w_INDICE)=Nvl(_Curs_GSCG2BGF.DIVLCOMN,0)
          endcase
        case this.w_TIPDIM="CI"
          * --- data iniziale
          if Not empty(nvl(_Curs_GSCG2BGF.DICODTOT," "))
            aValori(this.w_INDICE)=_Curs_GSCG2BGF.DIVALDAT
          else
            aValori(this.w_INDICE)=this.oParentObject.w_DATINI
          endif
        case this.w_TIPDIM="CF"
          * --- data finale
          if Not empty(nvl(_Curs_GSCG2BGF.DICODTOT," "))
            aValori(this.w_INDICE)=_Curs_GSCG2BGF.DIVALDAT
          else
            aValori(this.w_INDICE)=this.oParentObject.w_DATFIN
          endif
        case this.w_TIPDIM="AN"
          * --- Anno , anno specifico
          do case
            case this.w_TIPDAT="C"
              if Not empty(nvl(_Curs_GSCG2BGF.DICODTOT," "))
                aValori(this.w_INDICE)=Alltrim(_Curs_GSCG2BGF.DIVALCHR)
              else
                aValori(this.w_INDICE)=iif(this.oParentObject.w_ANNO>0,Alltrim(Str(this.oParentObject.w_ANNO)),"    ")
              endif
            case this.w_TIPDAT="N"
              if Not empty(nvl(_Curs_GSCG2BGF.DICODTOT," "))
                aValori(this.w_INDICE)=_Curs_GSCG2BGF.DIVALNUM
              else
                aValori(this.w_INDICE)=this.oParentObject.w_ANNO
              endif
          endcase
        case this.w_TIPDIM="PE"
          if Not empty(nvl(_Curs_GSCG2BGF.DICODTOT," "))
            aValori(this.w_INDICE)=_Curs_GSCG2BGF.DIVALNUM
          else
            aValori(this.w_INDICE)=this.oParentObject.w_PERIODO
          endif
        case this.w_TIPDIM="AT"
          if Not empty(nvl(_Curs_GSCG2BGF.DICODTOT," "))
            aValori(this.w_INDICE)=Nvl(_Curs_GSCG2BGF.DIVALATT," ")
          else
            aValori(this.w_INDICE)=this.oParentObject.w_CODATT
          endif
      endcase
      this.w_INDICE = this.w_INDICE+1
        select _Curs_GSCG2BGF
        continue
      enddo
      use
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_INDICE = 1
    * --- Select from TOT_DETT
    i_nConn=i_TableProp[this.TOT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TOT_DETT_idx,2],.t.,this.TOT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select TICODMIS,TICODFAZ,TICODCOM  from "+i_cTable+" TOT_DETT ";
          +" where TICODICE="+cp_ToStrODBC(this.w_CODICE)+"";
           ,"_Curs_TOT_DETT")
    else
      select TICODMIS,TICODFAZ,TICODCOM from (i_cTable);
       where TICODICE=this.w_CODICE;
        into cursor _Curs_TOT_DETT
    endif
    if used('_Curs_TOT_DETT')
      select _Curs_TOT_DETT
      locate for 1=1
      do while not(eof())
      if ALEN(aMisure)<this.w_INDICE
         
 Dimension aMisure(this.w_INDICE) 
 Dimension aCodfaz(this.w_INDICE)
      endif
       
 aMisure(this.w_INDICE)=IIF(this.w_TIPTOT="S",_Curs_TOT_DETT.TICODMIS,_Curs_TOT_DETT.TICODCOM) 
 aCodFaz(this.w_INDICE)=_Curs_TOT_DETT.TICODFAZ
      this.w_INDICE = this.w_INDICE+1
        select _Curs_TOT_DETT
        continue
      enddo
      use
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- indice array dei risultati del totalizzatore complesso
    this.w_INDMIS = 1
    * --- Select from TOT_DETT
    i_nConn=i_TableProp[this.TOT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TOT_DETT_idx,2],.t.,this.TOT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select TICODCOM,TICODFAZ,TIFORMUL  from "+i_cTable+" TOT_DETT ";
          +" where TICODICE="+cp_ToStrODBC(this.w_CODICE)+"";
           ,"_Curs_TOT_DETT")
    else
      select TICODCOM,TICODFAZ,TIFORMUL from (i_cTable);
       where TICODICE=this.w_CODICE;
        into cursor _Curs_TOT_DETT
    endif
    if used('_Curs_TOT_DETT')
      select _Curs_TOT_DETT
      locate for 1=1
      do while not(eof())
      this.w_FORMUL = Nvl(_Curs_TOT_DETT.TIFORMUL,"")
      this.w_CODFAZ = Nvl(_Curs_TOT_DETT.TICODFAZ,"")
      this.w_CODMIS = Nvl(_Curs_TOT_DETT.TICODCOM,"")
       
 DIMENSION aRisultati(this.w_INDMIS,2) 
 aRisultati[this.w_INDMIS,2]=this.w_CODFAZ
      if Not Empty(this.w_FORMUL) 
        * --- Ricerco il segno '{' il quale indica che ho inserito una costante
        this.TmpSave = ALLTRIM(this.w_FORMUL)
        this.w_OCCUR = RATC("{",this.TmpSave)
        this.w_OCCUR1 = RATC("}",this.TmpSave)
        * --- Controllo nel caso di totalizzatori ripetuti per evitare LOOP
        this.w_LOOP = 100
        do while this.w_OCCUR<>0 AND this.w_LOOP>0
          this.w_STRINGA = SUBSTR(this.TmpSave,this.w_occur+1,this.w_occur1-this.w_occur-1)
          this.w_OCCUR2 = RATC(".",this.w_STRINGA)
          this.w_DIFFE = this.w_OCCUR2-this.w_OCCUR
          this.w_CODICE = ALLTRIM(SUBSTR(this.w_STRINGA,1,this.w_OCCUR2-1))
          this.w_LCODMIS = ALLTRIM(SUBSTR(this.w_STRINGA,this.w_OCCUR2+1,LEN(this.w_STRINGA)))
          this.w_INDTOT = cp_ROUND(ASCAN(totalizzatori,alltrim(this.w_CODICE)+"."+alltrim(this.w_LCODMIS))/2,0)
          if this.w_INDTOT=0
            * --- Read from TOT_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TOT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TOT_MAST_idx,2],.t.,this.TOT_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TITIPTOT,TI_FONTE"+;
                " from "+i_cTable+" TOT_MAST where ";
                    +"TICODICE = "+cp_ToStrODBC(this.w_CODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TITIPTOT,TI_FONTE;
                from (i_cTable) where;
                    TICODICE = this.w_CODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TIPO = NVL(cp_ToDate(_read_.TITIPTOT),cp_NullValue(_read_.TITIPTOT))
              this.w_CODFON = NVL(cp_ToDate(_read_.TI_FONTE),cp_NullValue(_read_.TI_FONTE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            do case
              case this.w_TIPO="C"
                * --- Nel caso di totalizzatore composto devo eseguire read su campo misura composto
                * --- Read from TOT_DETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.TOT_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TOT_DETT_idx,2],.t.,this.TOT_DETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "TIFORMUL,TICODFAZ"+;
                    " from "+i_cTable+" TOT_DETT where ";
                        +"TICODICE = "+cp_ToStrODBC(this.w_CODICE);
                        +" and TICODCOM = "+cp_ToStrODBC(this.w_LCODMIS);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    TIFORMUL,TICODFAZ;
                    from (i_cTable) where;
                        TICODICE = this.w_CODICE;
                        and TICODCOM = this.w_LCODMIS;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  w_LFORMUL = NVL(cp_ToDate(_read_.TIFORMUL),cp_NullValue(_read_.TIFORMUL))
                  this.w_LCODFAZ = NVL(cp_ToDate(_read_.TICODFAZ),cp_NullValue(_read_.TICODFAZ))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if i_rows<>0
                  * --- Sostituisco misura con la relativa formula
                  this.TmpSave = SUBSTR(this.TMPSAVE,1,this.w_OCCUR-1)+Alltrim(w_LFORMUL)+SUBSTR(this.TMPSAVE,this.w_OCCUR1+1,LEN(this.TMPSAVE))
                else
                  * --- esco dal ciclo
                  this.w_LOOP = 0
                endif
              case this.w_TIPO="S" OR this.w_TIPO="M" 
                if this.w_TIPO="S"
                  * --- Read from TOT_DETT
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.TOT_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.TOT_DETT_idx,2],.t.,this.TOT_DETT_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "TIFORMUL,TICODFAZ"+;
                      " from "+i_cTable+" TOT_DETT where ";
                          +"TICODICE = "+cp_ToStrODBC(this.w_CODICE);
                          +" and TICODMIS = "+cp_ToStrODBC(this.w_LCODMIS);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      TIFORMUL,TICODFAZ;
                      from (i_cTable) where;
                          TICODICE = this.w_CODICE;
                          and TICODMIS = this.w_LCODMIS;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    w_LFORMUL = NVL(cp_ToDate(_read_.TIFORMUL),cp_NullValue(_read_.TIFORMUL))
                    this.w_LCODFAZ = NVL(cp_ToDate(_read_.TICODFAZ),cp_NullValue(_read_.TICODFAZ))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                else
                  * --- Read from TOT_DETT
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.TOT_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.TOT_DETT_idx,2],.t.,this.TOT_DETT_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "TICODFAZ"+;
                      " from "+i_cTable+" TOT_DETT where ";
                          +"TICODICE = "+cp_ToStrODBC(this.w_CODICE);
                          +" and TICODCOM = "+cp_ToStrODBC(this.w_LCODMIS);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      TICODFAZ;
                      from (i_cTable) where;
                          TICODICE = this.w_CODICE;
                          and TICODCOM = this.w_LCODMIS;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_LCODFAZ = NVL(cp_ToDate(_read_.TICODFAZ),cp_NullValue(_read_.TICODFAZ))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                if i_ROWS<>0
                  if this.w_TIPO="S"
                    * --- Read from TAB_FONT
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.TAB_FONT_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.TAB_FONT_idx,2],.t.,this.TAB_FONT_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "FO__TIPO,FO__NOME"+;
                        " from "+i_cTable+" TAB_FONT where ";
                            +"FOCODICE = "+cp_ToStrODBC(this.w_CODFON);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        FO__TIPO,FO__NOME;
                        from (i_cTable) where;
                            FOCODICE = this.w_CODFON;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_TIPFONT = NVL(cp_ToDate(_read_.FO__TIPO),cp_NullValue(_read_.FO__TIPO))
                      this.w_NOMEVQR = NVL(cp_ToDate(_read_.FO__NOME),cp_NullValue(_read_.FO__NOME))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    this.Pag2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                     
 aMisure(1)=this.w_LCODMIS 
 aCodfaz(1)=this.w_LCODFAZ
                    * --- Leggo tipologia della fonte da passare alla funzione
                    * --- L'array dei risultati in questo caso ha una sola riga perch� la funzione
                    *     verr� applicata solo su una misura
                     
 Dimension aRisSemplice(1,2)
                    Faz_engine(this.w_TIPFONT,this.w_NOMEVQR,@aMisure,@aDimensioni,@aValori,@aCodfaz,@aRisSemplice,@aRemove)
                    * --- Sostituisco totalizzatore semplice con relativo importo nella formula
                    this.w_LIMPORTO = aRisSemplice[1,1]
                  else
                    this.w_LCODTOT = this.w_CODICE
                    if this.pEXEC="B"
                      this.Pag6()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    else
                      this.w_LIMPORTO = 0
                    endif
                  endif
                  * --- determina la parte di formula ovvero <codtot.misura>
                  *     da sostituire con (importo)
                  this.w_TOTADDE = SUBSTR(this.TMPSAVE,this.w_OCCUR,this.w_OCCUR1-this.w_OCCUR+1)
                  * --- Sotituisco la virgola con il punto per valutare correttamente la formula
                  if Nvl(this.w_LIMPORTO,0)<>0
                    this.w_RIMPORTO = STRTRAN(Alltrim(str(this.w_LIMPORTO,18,4)),",",".")
                    this.w_NIMPORTO = this.w_LIMPORTO
                  else
                    this.w_NIMPORTO = 0
                    this.w_RIMPORTO = "0"
                  endif
                  this.w_ARLEN = alen(totalizzatori,1)+1
                  if ASCAN(totalizzatori,Alltrim(this.w_CODICE)+"."+Alltrim(this.w_LCODMIS))=0
                    * --- Memorizzo Risultato per non rieseguire il calcolo se non gi� presente
                     
 Dimension Totalizzatori(this.w_ARLEN,2) 
 Totalizzatori[this.w_ARLEN,1]=Alltrim(this.w_CODICE)+"."+Alltrim(this.w_LCODMIS) 
 Totalizzatori[this.w_ARLEN,2]=this.w_NIMPORTO
                  endif
                  this.TmpSave = strtran(this.TMPSAVE,this.w_TOTADDE,"("+Alltrim(this.w_RIMPORTO)+")")
                else
                  * --- esco dal ciclo
                  this.w_LOOP = 0
                endif
              otherwise
                * --- esco dal ciclo
                this.w_LOOP = 0
            endcase
          else
            * --- ho gi� incotrato la misura del totalizzatore sostituisco nella formula il relativo importo
            this.w_TOTADDE = SUBSTR(this.TMPSAVE,this.w_OCCUR,this.w_OCCUR1-this.w_OCCUR+1)
            this.w_RIMPORTO = STRTRAN(Alltrim(str(Totalizzatori[this.w_indtot,2],18,4)),",",".")
            this.TmpSave = strtran(this.TMPSAVE,this.w_TOTADDE,"("+Alltrim(Nvl(this.w_RIMPORTO,"0"))+")")
          endif
          this.w_OCCUR = RATC("{",this.TmpSave)
          this.w_OCCUR1 = RATC("}",this.TmpSave)
          this.w_LOOP = this.w_LOOP-1
        enddo
        * --- Valuto la formula 
         
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t.
        TmpVal = this.tmpsave
        * --- Valutazione dell'errore
        this.w_IMPORTO = &TmpVal
         
 On Error &L_OLDERROR
        if L_Err
          ah_ErrorMsg("Errore nell'applicazione della formula %1 con messaggio di errore: %2",,"",Alltrim(this.w_FORMUL),Message())
          i_retcode = 'stop'
          return
        endif
        if left(alltrim(str(this.w_IMPORTO,20,5)),1)="*"
          ah_ErrorMsg("Errore nell'applicazione della formula %1 valore %2 non corretto, misura non valutata",,"",Alltrim(this.w_FORMUL),alltrim(TmpVal))
        else
          * --- Aggiorno array dei risultati del totalizzatori composto
           
 aRisultati(this.w_INDMIS,1)=this.w_IMPORTO
        endif
      endif
      this.w_INDMIS = this.w_INDMIS + 1
        select _Curs_TOT_DETT
        continue
      enddo
      use
    endif
    if alen(aRisultati)>0
      this.w_dimensione = this.w_INDMIS
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisco record nella tabella di appoggio
    if this.w_TIPTOT="C"
      this.w_CODICE = this.w_CODTOT
      * --- nel caso di totalizzatore composto devo ricaricare array misure
       
 Dimension aMisure(1) 
 aMisure=.f.
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if ALEN(aRisultati)>0
      * --- Inserisco Record Nellaq Tabella di genetrazione del file
       
 For i=1 To Alen(aMisure)
      this.w_CODMIS = aMisure[i]
      this.w_IMPORTO = IIF(Type("aRisultati[i,1]")="N",aRisultati[i,1],0)
      this.w_CODFAZ = aRisultati[i,2]
      if Alen(Totalizzatori,1)<i
        Dimension Totalizzatori(i,2)
      endif
      if ASCAN(totalizzatori,Alltrim(this.w_CODICE)+"."+Alltrim(aMisure[i]))=0
        * --- Memorizzo Risultato per non rieseguire il calcolo se non gi� presente
         
 Totalizzatori[i,1]=Alltrim(this.w_CODICE)+"."+Alltrim(aMisure[i]) 
 Totalizzatori[i,2]=this.w_IMPORTO
      endif
      * --- Insert into ZOOMPART
      i_nConn=i_TableProp[this.ZOOMPART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ZOOMPART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CODFAZ"+",IMPORTO"+",FLDECI"+",CODMIS"+",CODTOT"+",TIPTOT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CODFAZ),'ZOOMPART','CODFAZ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPORTO),'ZOOMPART','IMPORTO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLDECI),'ZOOMPART','FLDECI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODMIS),'ZOOMPART','CODMIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODTOT),'ZOOMPART','CODTOT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPTOT),'ZOOMPART','TIPTOT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CODFAZ',this.w_CODFAZ,'IMPORTO',this.w_IMPORTO,'FLDECI',this.w_FLDECI,'CODMIS',this.w_CODMIS,'CODTOT',this.w_CODTOT,'TIPTOT',this.w_TIPTOT)
        insert into (i_cTable) (CODFAZ,IMPORTO,FLDECI,CODMIS,CODTOT,TIPTOT &i_ccchkf. );
           values (;
             this.w_CODFAZ;
             ,this.w_IMPORTO;
             ,this.w_FLDECI;
             ,this.w_CODMIS;
             ,this.w_CODTOT;
             ,this.w_TIPTOT;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
       
 Endfor
    endif
    * --- Azzero array
     
 Dimension aMisure(1),aValori(1),aDimensioni(1),aCodfaz(1),aRisultati(1,2),aRisSemplice(1,2) 
 aMisure=.f. 
 aValori=.f. 
 acodfaz=.f. 
 aDimensioni=.f. 
 aRisSemplice=.f. 
 aRisultati=.f.
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_LIMPORTO = 0
    this.w_GEST_MANU = This.oparentobject.oparentobject.oparentobject.GSCG_MDM
    this.w_GEST_MANU.Markpos()     
    this.w_NUMREC = this.w_GEST_MANU.search("NVL(t_MRCODTOT, SPACE(10)) = "+cp_tostr( this.w_LCODTOT ) + "AND NVL(t_MRCODMIS, SPACE(10)) = "+cp_tostr( this.w_LCODMIS )+" And Not Deleted() ")
    if this.w_NUMREC <>-1
      this.w_GEST_MANU.SetRow(this.w_NUMREC)     
      this.w_LIMPORTO = this.w_GEST_MANU.Get("t_MRIMPORT")
    endif
    this.w_GEST_MANU.Repos()     
  endproc


  proc Init(oParentObject,pEXEC,pCODSTR)
    this.pEXEC=pEXEC
    this.pCODSTR=pCODSTR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='TOT_MAST'
    this.cWorkTables[2]='DET_DIME'
    this.cWorkTables[3]='TOT_DETT'
    this.cWorkTables[4]='*ZOOMPART'
    this.cWorkTables[5]='TAB_FONT'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GSCG_BGF')
      use in _Curs_GSCG_BGF
    endif
    if used('_Curs_GSCG1BGF')
      use in _Curs_GSCG1BGF
    endif
    if used('_Curs_GSCG2BGF')
      use in _Curs_GSCG2BGF
    endif
    if used('_Curs_TOT_DETT')
      use in _Curs_TOT_DETT
    endif
    if used('_Curs_TOT_DETT')
      use in _Curs_TOT_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC,pCODSTR"
endproc
