* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_sdt                                                        *
*              Stampa disponibilit� nel tempo                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_65]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-21                                                      *
* Last revis.: 2014-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsor_sdt",oParentObject))

* --- Class definition
define class tgsor_sdt as StdForm
  Top    = 18
  Left   = 19

  * --- Standard Properties
  Width  = 597
  Height = 398
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-07"
  HelpContextID=74017129
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  MAGAZZIN_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  CONTI_IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gsor_sdt"
  cComment = "Stampa disponibilit� nel tempo"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SELORD = space(10)
  w_SELART = space(10)
  w_ANTIPCON = space(10)
  w_CODMAG = space(5)
  w_DESMAG = space(30)
  w_CODFAM = space(5)
  w_DESFAM = space(35)
  w_CODGRU = space(5)
  w_DESGRU = space(35)
  w_CODCAT = space(5)
  w_DESCAT = space(35)
  w_CODMAR = space(5)
  w_DESMAR = space(35)
  w_CODPRO = space(15)
  w_CODFOR = space(15)
  w_DESPRO = space(40)
  w_CODINI = space(20)
  w_DESART = space(40)
  w_TIPO = space(2)
  w_CODFIN = space(20)
  w_DATSIS = ctod('  /  /  ')
  o_DATSIS = ctod('  /  /  ')
  w_SELMOV = space(1)
  w_MD = ctod('  /  /  ')
  w_MA = ctod('  /  /  ')
  w_GRUPPO = space(1)
  w_SELPER = 0
  w_OREP = space(50)
  w_OQRY = space(50)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESFOR = space(60)
  w_DESART2 = space(40)
  * --- Area Manuale = Declare Variables
  * --- gsor_sdt
  * --- Disabilita il metodo salva sulla Toolbar
  proc SetCPToolbar()
             doDefault()
             oCpToolBar.b4.enabled=.f.
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsor_sdtPag1","gsor_sdt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELORD_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='FAM_ARTI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='CATEGOMO'
    this.cWorkTables[6]='MARCHI'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='AZIENDA'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      do GSOR_BDN with this
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsor_sdt
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SELORD=space(10)
      .w_SELART=space(10)
      .w_ANTIPCON=space(10)
      .w_CODMAG=space(5)
      .w_DESMAG=space(30)
      .w_CODFAM=space(5)
      .w_DESFAM=space(35)
      .w_CODGRU=space(5)
      .w_DESGRU=space(35)
      .w_CODCAT=space(5)
      .w_DESCAT=space(35)
      .w_CODMAR=space(5)
      .w_DESMAR=space(35)
      .w_CODPRO=space(15)
      .w_CODFOR=space(15)
      .w_DESPRO=space(40)
      .w_CODINI=space(20)
      .w_DESART=space(40)
      .w_TIPO=space(2)
      .w_CODFIN=space(20)
      .w_DATSIS=ctod("  /  /  ")
      .w_SELMOV=space(1)
      .w_MD=ctod("  /  /  ")
      .w_MA=ctod("  /  /  ")
      .w_GRUPPO=space(1)
      .w_SELPER=0
      .w_OREP=space(50)
      .w_OQRY=space(50)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESFOR=space(60)
      .w_DESART2=space(40)
        .w_SELORD = 'A'
        .w_SELART = 'T'
        .w_ANTIPCON = 'F'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODMAG))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_CODFAM))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_CODGRU))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_CODCAT))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_CODMAR))
          .link_1_12('Full')
        endif
        .DoRTCalc(13,14,.f.)
        if not(empty(.w_CODPRO))
          .link_1_14('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CODFOR))
          .link_1_15('Full')
        endif
        .DoRTCalc(16,17,.f.)
        if not(empty(.w_CODINI))
          .link_1_17('Full')
        endif
        .DoRTCalc(18,20,.f.)
        if not(empty(.w_CODFIN))
          .link_1_20('Full')
        endif
        .w_DATSIS = i_datsys
        .w_SELMOV = 'F'
        .w_MD = g_iniese
        .w_MA = g_finese
        .w_GRUPPO = 'M'
        .w_SELPER = 99
          .DoRTCalc(27,28,.f.)
        .w_OBTEST = .w_DATSIS
    endwith
    this.DoRTCalc(30,32,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,28,.t.)
        if .o_DATSIS<>.w_DATSIS
            .w_OBTEST = .w_DATSIS
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(30,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODMAG
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_4'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_CODMAG = space(5)
        this.w_DESMAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_1_6'),i_cWhere,'GSAR_AFA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_CODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_CODGRU))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODGRU)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCODGRU_1_8'),i_cWhere,'GSAR_AGM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_CODGRU)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCAT_1_10'),i_cWhere,'GSAR_AOM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAR
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_CODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oCODMAR_1_12'),i_cWhere,'GSAR_AMH',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_CODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_CODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAR = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRO
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODPRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_ANTIPCON;
                     ,'ANCODICE',trim(this.w_CODPRO))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODPRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODPRO)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_ANTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODPRO_1_14'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODPRO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ANTIPCON;
                       ,'ANCODICE',this.w_CODPRO)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRO = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPRO = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRO = space(15)
      endif
      this.w_DESPRO = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_CODPRO = space(15)
        this.w_DESPRO = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFOR
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_ANTIPCON;
                     ,'ANCODICE',trim(this.w_CODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_ANTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFOR_1_15'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ANTIPCON;
                       ,'ANCODICE',this.w_CODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(60))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODFOR = space(15)
      endif
      this.w_DESFOR = space(60)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_CODFOR = space(15)
        this.w_DESFOR = space(60)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODINI))
          select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODINI)+"%");

            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODINI_1_17'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSOR0ADI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODINI)
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_TIPO = NVL(_Link_.ARTIPART,space(2))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESART = space(40)
      this.w_TIPO = space(2)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO $ 'PF-SE-MP-PH-MC-MA-IM-FS') and (.w_CODINI<=.w_CODFIN OR EMPTY(.w_CODFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non di tipo articolo oppure obsoleto o inesistente")
        endif
        this.w_CODINI = space(20)
        this.w_DESART = space(40)
        this.w_TIPO = space(2)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODFIN))
          select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODFIN)+"%");

            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODFIN_1_20'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSOR0ADI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODFIN)
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESART2 = NVL(_Link_.ARDESART,space(40))
      this.w_TIPO = NVL(_Link_.ARTIPART,space(2))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESART2 = space(40)
      this.w_TIPO = space(2)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO $ 'PF-SE-MP-PH-MC-MA-IM-FS') and (.w_CODINI<=.w_CODFIN  OR EMPTY(.w_CODINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non di tipo articolo oppure obsoleto o inesistente")
        endif
        this.w_CODFIN = space(20)
        this.w_DESART2 = space(40)
        this.w_TIPO = space(2)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELORD_1_1.RadioValue()==this.w_SELORD)
      this.oPgFrm.Page1.oPag.oSELORD_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELART_1_2.RadioValue()==this.w_SELART)
      this.oPgFrm.Page1.oPag.oSELART_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_4.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_4.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_5.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_5.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAM_1_6.value==this.w_CODFAM)
      this.oPgFrm.Page1.oPag.oCODFAM_1_6.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_7.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_7.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRU_1_8.value==this.w_CODGRU)
      this.oPgFrm.Page1.oPag.oCODGRU_1_8.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_9.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_9.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAT_1_10.value==this.w_CODCAT)
      this.oPgFrm.Page1.oPag.oCODCAT_1_10.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_11.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_11.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAR_1_12.value==this.w_CODMAR)
      this.oPgFrm.Page1.oPag.oCODMAR_1_12.value=this.w_CODMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAR_1_13.value==this.w_DESMAR)
      this.oPgFrm.Page1.oPag.oDESMAR_1_13.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRO_1_14.value==this.w_CODPRO)
      this.oPgFrm.Page1.oPag.oCODPRO_1_14.value=this.w_CODPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFOR_1_15.value==this.w_CODFOR)
      this.oPgFrm.Page1.oPag.oCODFOR_1_15.value=this.w_CODFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRO_1_16.value==this.w_DESPRO)
      this.oPgFrm.Page1.oPag.oDESPRO_1_16.value=this.w_DESPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_17.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_17.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_18.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_18.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_20.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_20.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSIS_1_21.value==this.w_DATSIS)
      this.oPgFrm.Page1.oPag.oDATSIS_1_21.value=this.w_DATSIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSELMOV_1_22.RadioValue()==this.w_SELMOV)
      this.oPgFrm.Page1.oPag.oSELMOV_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMD_1_23.value==this.w_MD)
      this.oPgFrm.Page1.oPag.oMD_1_23.value=this.w_MD
    endif
    if not(this.oPgFrm.Page1.oPag.oMA_1_24.value==this.w_MA)
      this.oPgFrm.Page1.oPag.oMA_1_24.value=this.w_MA
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUPPO_1_25.RadioValue()==this.w_GRUPPO)
      this.oPgFrm.Page1.oPag.oGRUPPO_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELPER_1_26.RadioValue()==this.w_SELPER)
      this.oPgFrm.Page1.oPag.oSELPER_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_50.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_50.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART2_1_51.value==this.w_DESART2)
      this.oPgFrm.Page1.oPag.oDESART2_1_51.value=this.w_DESART2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPRO_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODFOR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFOR_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO $ 'PF-SE-MP-PH-MC-MA-IM-FS') and (.w_CODINI<=.w_CODFIN OR EMPTY(.w_CODFIN)))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non di tipo articolo oppure obsoleto o inesistente")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO $ 'PF-SE-MP-PH-MC-MA-IM-FS') and (.w_CODINI<=.w_CODFIN  OR EMPTY(.w_CODINI)))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non di tipo articolo oppure obsoleto o inesistente")
          case   not(.w_MD<=.w_MA)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMD_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(.w_MD<=.w_MA)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMA_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATSIS = this.w_DATSIS
    return

enddefine

* --- Define pages as container
define class tgsor_sdtPag1 as StdContainer
  Width  = 593
  height = 398
  stdWidth  = 593
  stdheight = 398
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSELORD_1_1 as StdCombo with uid="HSCHDDLOQP",rtseq=1,rtrep=.f.,left=281,top=9,width=108,height=21;
    , ToolTipText = "Consente di selezionare l'ordinamento di stampa";
    , HelpContextID = 183852762;
    , cFormVar="w_SELORD",RowSource=""+"Per articolo,"+"Per descrizione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELORD_1_1.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'D',;
    space(10))))
  endfunc
  func oSELORD_1_1.GetRadio()
    this.Parent.oContained.w_SELORD = this.RadioValue()
    return .t.
  endfunc

  func oSELORD_1_1.SetRadio()
    this.Parent.oContained.w_SELORD=trim(this.Parent.oContained.w_SELORD)
    this.value = ;
      iif(this.Parent.oContained.w_SELORD=='A',1,;
      iif(this.Parent.oContained.w_SELORD=='D',2,;
      0))
  endfunc


  add object oSELART_1_2 as StdCombo with uid="QJGQNNFQAL",rtseq=2,rtrep=.f.,left=448,top=9,width=139,height=21;
    , ToolTipText = "Consente di stampare tutti gli articoli o solo quelli con disponibilit� negativa";
    , HelpContextID = 83665190;
    , cFormVar="w_SELART",RowSource=""+"Non disponibili,"+"Tutti,"+"Disponibili,"+"Con disponibilit� negativa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELART_1_2.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'T',;
    iif(this.value =3,'D',;
    iif(this.value =4,'M',;
    space(10))))))
  endfunc
  func oSELART_1_2.GetRadio()
    this.Parent.oContained.w_SELART = this.RadioValue()
    return .t.
  endfunc

  func oSELART_1_2.SetRadio()
    this.Parent.oContained.w_SELART=trim(this.Parent.oContained.w_SELART)
    this.value = ;
      iif(this.Parent.oContained.w_SELART=='N',1,;
      iif(this.Parent.oContained.w_SELART=='T',2,;
      iif(this.Parent.oContained.w_SELART=='D',3,;
      iif(this.Parent.oContained.w_SELART=='M',4,;
      0))))
  endfunc

  add object oCODMAG_1_4 as StdField with uid="CYKZRHOOGR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice del magazzino da considerare (vuoto = no selezione)",;
    HelpContextID = 151508442,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=116, Top=48, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_5 as StdField with uid="OQLASLYQMI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 151449546,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=295, Top=48, InputMask=replicate('X',30)

  add object oCODFAM_1_6 as StdField with uid="OWODRFKNQJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della famiglia da considerare (vuoto = no selezione)",;
    HelpContextID = 51303898,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=116, Top=75, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"",'',this.parent.oContained
  endproc
  proc oCODFAM_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CODFAM
     i_obj.ecpSave()
  endproc

  add object oDESFAM_1_7 as StdField with uid="JVJOZTMZLR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 51245002,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=295, Top=75, InputMask=replicate('X',35)

  add object oCODGRU_1_8 as StdField with uid="VAVIYLSFLM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del gruppo merceologico da considerare (vuoto = no selezione)",;
    HelpContextID = 100805158,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=116, Top=102, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_CODGRU"

  func oCODGRU_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCODGRU_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"",'',this.parent.oContained
  endproc
  proc oCODGRU_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_CODGRU
     i_obj.ecpSave()
  endproc

  add object oDESGRU_1_9 as StdField with uid="RIFPNAZBGN",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 100864054,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=295, Top=102, InputMask=replicate('X',35)

  add object oCODCAT_1_10 as StdField with uid="NQOAUVYKRJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria omogenea da considerare (vuoto = no selezione)",;
    HelpContextID = 65940006,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=116, Top=129, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAT_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCAT_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"",'',this.parent.oContained
  endproc
  proc oCODCAT_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CODCAT
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_11 as StdField with uid="BPKTKUPYXF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 65998902,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=295, Top=129, InputMask=replicate('X',35)

  add object oCODMAR_1_12 as StdField with uid="JUXNFKCFFV",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODMAR", cQueryName = "CODMAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della marca da considerare (vuoto = no selezione)",;
    HelpContextID = 33040934,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=116, Top=156, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_CODMAR"

  func oCODMAR_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAR_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAR_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oCODMAR_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"",'',this.parent.oContained
  endproc
  proc oCODMAR_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_CODMAR
     i_obj.ecpSave()
  endproc

  add object oDESMAR_1_13 as StdField with uid="JPLRRPQPOO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 33099830,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=295, Top=156, InputMask=replicate('X',35)

  add object oCODPRO_1_14 as StdField with uid="GGDAIGWMQX",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODPRO", cQueryName = "CODPRO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice del produttore da considerare",;
    HelpContextID = 731686,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=116, Top=183, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ANTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODPRO"

  func oCODPRO_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRO_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRO_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ANTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_ANTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODPRO_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oCODPRO_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_ANTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODPRO
     i_obj.ecpSave()
  endproc

  add object oCODFOR_1_15 as StdField with uid="JTVPJHTTJA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODFOR", cQueryName = "CODFOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice del fornitore abituale da considerare",;
    HelpContextID = 47262246,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=116, Top=209, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ANTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFOR"

  func oCODFOR_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFOR_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFOR_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ANTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_ANTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFOR_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oCODFOR_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_ANTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFOR
     i_obj.ecpSave()
  endproc

  add object oDESPRO_1_16 as StdField with uid="KJMMLKKBVO",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESPRO", cQueryName = "DESPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 790582,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=295, Top=183, InputMask=replicate('X',40)

  add object oCODINI_1_17 as StdField with uid="UJEUWSKIFL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non di tipo articolo oppure obsoleto o inesistente",;
    ToolTipText = "Codice dell'articolo di inizio selezione (vuoto= tutti gli articoli)",;
    HelpContextID = 104584666,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=116, Top=236, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODINI"

  func oCODINI_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODINI_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSOR0ADI.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oDESART_1_18 as StdField with uid="NBPNAMZNHP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 83693622,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=295, Top=236, InputMask=replicate('X',40)

  add object oCODFIN_1_20 as StdField with uid="PQKMCWLFLP",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non di tipo articolo oppure obsoleto o inesistente",;
    ToolTipText = "Codice dell'articolo di fine selezione (vuoto= tutti gli articoli)",;
    HelpContextID = 26138074,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=116, Top=263, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODFIN_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSOR0ADI.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oDATSIS_1_21 as StdField with uid="PIWUKJOOYL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DATSIS", cQueryName = "DATSIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data precedente al primo periodo non scaduto",;
    HelpContextID = 58661942,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=116, Top=12


  add object oSELMOV_1_22 as StdCombo with uid="DWUBYWXKSL",rtseq=22,rtrep=.f.,left=116,top=293,width=142,height=21;
    , ToolTipText = "Movimenti da stampare";
    , HelpContextID = 114860326;
    , cFormVar="w_SELMOV",RowSource=""+"Solo futuri,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELMOV_1_22.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oSELMOV_1_22.GetRadio()
    this.Parent.oContained.w_SELMOV = this.RadioValue()
    return .t.
  endfunc

  func oSELMOV_1_22.SetRadio()
    this.Parent.oContained.w_SELMOV=trim(this.Parent.oContained.w_SELMOV)
    this.value = ;
      iif(this.Parent.oContained.w_SELMOV=='F',1,;
      iif(this.Parent.oContained.w_SELMOV=='T',2,;
      0))
  endfunc

  add object oMD_1_23 as StdField with uid="YYEEVSMQWC",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MD", cQueryName = "MD",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data dalla quale devono essere considerati i movimenti",;
    HelpContextID = 73998394,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=116, Top=320

  func oMD_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MD<=.w_MA)
    endwith
    return bRes
  endfunc

  add object oMA_1_24 as StdField with uid="RITRMOHIYB",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MA", cQueryName = "MA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data fino alla quale devono essere considerati i movimenti",;
    HelpContextID = 73999162,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=116, Top=346

  func oMA_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MD<=.w_MA)
    endwith
    return bRes
  endfunc


  add object oGRUPPO_1_25 as StdCombo with uid="BZINFGERJM",rtseq=25,rtrep=.f.,left=445,top=293,width=142,height=20;
    , HelpContextID = 1295002;
    , cFormVar="w_GRUPPO",RowSource=""+"Per magazzino,"+"Per gruppo magazzino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGRUPPO_1_25.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'G',;
    space(1))))
  endfunc
  func oGRUPPO_1_25.GetRadio()
    this.Parent.oContained.w_GRUPPO = this.RadioValue()
    return .t.
  endfunc

  func oGRUPPO_1_25.SetRadio()
    this.Parent.oContained.w_GRUPPO=trim(this.Parent.oContained.w_GRUPPO)
    this.value = ;
      iif(this.Parent.oContained.w_GRUPPO=='M',1,;
      iif(this.Parent.oContained.w_GRUPPO=='G',2,;
      0))
  endfunc


  add object oSELPER_1_26 as StdCombo with uid="UUBIOESZGZ",rtseq=26,rtrep=.f.,left=445,top=319,width=142,height=21;
    , ToolTipText = "Periodo di raggruppamento";
    , HelpContextID = 37462310;
    , cFormVar="w_SELPER",RowSource=""+"Dettaglio movimenti,"+"Giornaliero,"+"Settimanale,"+"Mensile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELPER_1_26.RadioValue()
    return(iif(this.value =1,99,;
    iif(this.value =2,1,;
    iif(this.value =3,7,;
    iif(this.value =4,30,;
    0)))))
  endfunc
  func oSELPER_1_26.GetRadio()
    this.Parent.oContained.w_SELPER = this.RadioValue()
    return .t.
  endfunc

  func oSELPER_1_26.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SELPER==99,1,;
      iif(this.Parent.oContained.w_SELPER==1,2,;
      iif(this.Parent.oContained.w_SELPER==7,3,;
      iif(this.Parent.oContained.w_SELPER==30,4,;
      0))))
  endfunc


  add object oBtn_1_27 as StdButton with uid="YPKRVSMNZX",left=443, top=348, width=48,height=45,;
    CpPicture="BMP\GRAFICO.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il grafico della situazione articolo";
    , HelpContextID = 93527398;
    , caption='\<Grafico';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        GSOR_BDN(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CODINI) AND .w_CODINI=.w_CODFIN and NOT EMPTY(.w_CODMAG)  and .w_GRUPPO='M')
      endwith
    endif
  endfunc


  add object oBtn_1_28 as StdButton with uid="SYHSQONFQJ",left=491, top=348, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 67772454;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        GSOR_BDN(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="SDYSSLXFZT",left=539, top=348, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 66699706;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESFOR_1_50 as StdField with uid="TEDTZSNYVT",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 47321142,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=295, Top=207, InputMask=replicate('X',60)

  add object oDESART2_1_51 as StdField with uid="ARFSAOHKKX",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESART2", cQueryName = "DESART2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 83693622,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=295, Top=263, InputMask=replicate('X',40)

  add object oStr_1_32 as StdString with uid="LNFVPAMMKN",Visible=.t., Left=13, Top=48,;
    Alignment=1, Width=101, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="CCPXDPDUEF",Visible=.t., Left=13, Top=75,;
    Alignment=1, Width=101, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="MJXMINFMJL",Visible=.t., Left=5, Top=102,;
    Alignment=1, Width=109, Height=15,;
    Caption="Gr. merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="MZHDUZVRSZ",Visible=.t., Left=4, Top=129,;
    Alignment=1, Width=110, Height=15,;
    Caption="Cat. omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="XXLSPOGDHW",Visible=.t., Left=13, Top=156,;
    Alignment=1, Width=101, Height=15,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="EKEIDLKKWG",Visible=.t., Left=13, Top=183,;
    Alignment=1, Width=101, Height=15,;
    Caption="Produttore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="FQDZAXJBTX",Visible=.t., Left=197, Top=14,;
    Alignment=1, Width=82, Height=15,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="YKDZJTPOEX",Visible=.t., Left=13, Top=14,;
    Alignment=1, Width=101, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="VNZFMLNFAF",Visible=.t., Left=302, Top=319,;
    Alignment=1, Width=140, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="AAJYRWGHRE",Visible=.t., Left=401, Top=14,;
    Alignment=1, Width=45, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="XHKJUFSKXP",Visible=.t., Left=13, Top=236,;
    Alignment=1, Width=101, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="XNEPQDDYQW",Visible=.t., Left=13, Top=293,;
    Alignment=1, Width=101, Height=15,;
    Caption="Movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="EVPPCZYPPJ",Visible=.t., Left=13, Top=320,;
    Alignment=1, Width=101, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="PORNNACODQ",Visible=.t., Left=13, Top=346,;
    Alignment=1, Width=101, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="MHGYPPKAOK",Visible=.t., Left=338, Top=293,;
    Alignment=1, Width=104, Height=15,;
    Caption="Raggruppamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="YUHACVPLRW",Visible=.t., Left=13, Top=209,;
    Alignment=1, Width=101, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="FAZGXDXVDP",Visible=.t., Left=13, Top=263,;
    Alignment=1, Width=101, Height=18,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsor_sdt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
