* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_szm                                                        *
*              Visualizza movimenti di magazzino                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_132]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2013-08-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_szm",oParentObject))

* --- Class definition
define class tgsma_szm as StdForm
  Top    = 1
  Left   = 9

  * --- Standard Properties
  Width  = 799
  Height = 504+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-08-08"
  HelpContextID=26576023
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=51

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  ART_ICOL_IDX = 0
  AZIENDA_IDX = 0
  MAGAZZIN_IDX = 0
  CAM_AGAZ_IDX = 0
  CAN_TIER_IDX = 0
  ATTIMAST_IDX = 0
  ATTIVITA_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  cPrg = "gsma_szm"
  cComment = "Visualizza movimenti di magazzino"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CODMAG = space(5)
  o_CODMAG = space(5)
  w_CODART = space(20)
  o_CODART = space(20)
  w_DESART = space(40)
  w_CODART2 = space(20)
  o_CODART2 = space(20)
  w_DESART2 = space(40)
  w_SALDOPR = space(1)
  o_SALDOPR = space(1)
  w_SERIALE = space(10)
  w_TOTCAR = 0
  w_CLIFOR = space(35)
  w_TOTSCA = 0
  w_NUMROW = 0
  w_UNIMIS = space(3)
  w_NUMRIF = 0
  w_DESMAG = space(30)
  w_FLDOCU = space(1)
  w_FLMOVI = space(1)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  o_CODCON = space(15)
  w_CAUSEL = space(5)
  w_DESCAU = space(35)
  w_FLELGM = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPATT = space(1)
  w_CODCOM = space(15)
  o_CODCOM = space(15)
  w_DESCON = space(40)
  w_CODATT = space(15)
  w_DESCOM = space(40)
  w_DESATT = space(35)
  w_SALDOFIN = 0
  w_SALDOINI = 0
  w_TIPART = space(2)
  w_QTAMOV2 = 0
  w_CAUMAG = space(35)
  w_UM2 = space(3)
  w_CODFAM = space(5)
  w_DESFAM = space(35)
  w_CODGRU = space(5)
  w_DESGRU = space(35)
  w_CODCAT = space(5)
  w_DESCAT = space(35)
  w_CODMAR = space(5)
  w_DESMAR = space(35)
  w_DCODART = space(20)
  w_MCODART = space(20)
  w_DCODART2 = space(20)
  w_MCODART2 = space(20)
  w_DESCRI = space(35)
  w_ZoomMag = .NULL.
  w_COLORE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_szmPag1","gsma_szm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsma_szmPag2","gsma_szm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomMag = this.oPgFrm.Pages(1).oPag.ZoomMag
    this.w_COLORE = this.oPgFrm.Pages(1).oPag.COLORE
    DoDefault()
    proc Destroy()
      this.w_ZoomMag = .NULL.
      this.w_COLORE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='CAM_AGAZ'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='ATTIMAST'
    this.cWorkTables[8]='ATTIVITA'
    this.cWorkTables[9]='FAM_ARTI'
    this.cWorkTables[10]='GRUMERC'
    this.cWorkTables[11]='CATEGOMO'
    this.cWorkTables[12]='MARCHI'
    return(this.OpenAllTables(12))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CODMAG=space(5)
      .w_CODART=space(20)
      .w_DESART=space(40)
      .w_CODART2=space(20)
      .w_DESART2=space(40)
      .w_SALDOPR=space(1)
      .w_SERIALE=space(10)
      .w_TOTCAR=0
      .w_CLIFOR=space(35)
      .w_TOTSCA=0
      .w_NUMROW=0
      .w_UNIMIS=space(3)
      .w_NUMRIF=0
      .w_DESMAG=space(30)
      .w_FLDOCU=space(1)
      .w_FLMOVI=space(1)
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_CAUSEL=space(5)
      .w_DESCAU=space(35)
      .w_FLELGM=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPATT=space(1)
      .w_CODCOM=space(15)
      .w_DESCON=space(40)
      .w_CODATT=space(15)
      .w_DESCOM=space(40)
      .w_DESATT=space(35)
      .w_SALDOFIN=0
      .w_SALDOINI=0
      .w_TIPART=space(2)
      .w_QTAMOV2=0
      .w_CAUMAG=space(35)
      .w_UM2=space(3)
      .w_CODFAM=space(5)
      .w_DESFAM=space(35)
      .w_CODGRU=space(5)
      .w_DESGRU=space(35)
      .w_CODCAT=space(5)
      .w_DESCAT=space(35)
      .w_CODMAR=space(5)
      .w_DESMAR=space(35)
      .w_DCODART=space(20)
      .w_MCODART=space(20)
      .w_DCODART2=space(20)
      .w_MCODART2=space(20)
      .w_DESCRI=space(35)
        .w_CODAZI = i_CODAZI
        .w_DATINI = g_INIESE
        .w_DATFIN = g_FINESE
        .w_CODMAG = g_MAGAZI
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODMAG))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODART))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_CODART2 = .w_CODART
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODART2))
          .link_1_7('Full')
        endif
          .DoRTCalc(8,8,.f.)
        .w_SALDOPR = IIF(EMPTY(.w_SALDOPR),'S',IIF(EMPTY(.w_CODMAG) OR .w_CODART<>.w_CODART2,'',.w_SALDOPR))
      .oPgFrm.Page1.oPag.ZoomMag.Calculate()
        .w_SERIALE = .w_ZoomMag.getVar('SERIAL')
          .DoRTCalc(11,11,.f.)
        .w_CLIFOR = .w_ZoomMag.getVar('DESCON')
          .DoRTCalc(13,13,.f.)
        .w_NUMROW = .w_ZoomMag.getVar('CPROWNUM')
          .DoRTCalc(15,15,.f.)
        .w_NUMRIF = .w_ZoomMag.getVar('NUMRIF')
          .DoRTCalc(17,17,.f.)
        .w_FLDOCU = 'S'
        .w_FLMOVI = 'S'
        .w_TIPCON = ''
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_CODCON))
          .link_2_4('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_CAUSEL))
          .link_2_5('Full')
        endif
          .DoRTCalc(23,23,.f.)
        .w_FLELGM = 'S'
        .w_OBTEST = i_INIDAT
          .DoRTCalc(26,26,.f.)
        .w_TIPATT = 'A'
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_CODCOM))
          .link_2_13('Full')
        endif
          .DoRTCalc(29,29,.f.)
        .w_CODATT = SPACE(15)
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_CODATT))
          .link_2_17('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate(IIF(.w_CODART<>.w_CODART2 OR .w_FLELGM='T' OR .w_SALDOPR<>'S','',Ah_msgformat('Saldo al %1:',dtoc(.w_DATINI-1) )))
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate(IIF(.w_CODART<>.w_CODART2 OR .w_FLELGM='T' OR .w_SALDOPR<>'S','',Ah_msgformat('Saldo al %1:',dtoc(.w_DATFIN) )))
          .DoRTCalc(31,35,.f.)
        .w_QTAMOV2 = .w_ZoomMag.getVar('QTAMOV')
        .w_CAUMAG = .w_ZoomMag.getVar('CMDESCRI')
        .w_UM2 = .w_ZoomMag.getVar('UNIMIS')
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_CODFAM))
          .link_2_23('Full')
        endif
        .DoRTCalc(40,41,.f.)
        if not(empty(.w_CODGRU))
          .link_2_25('Full')
        endif
        .DoRTCalc(42,43,.f.)
        if not(empty(.w_CODCAT))
          .link_2_27('Full')
        endif
        .DoRTCalc(44,45,.f.)
        if not(empty(.w_CODMAR))
          .link_2_29('Full')
        endif
          .DoRTCalc(46,46,.f.)
        .w_DCODART = IIF(.w_FLDOCU='S', .w_CODART, REPL("@", 20))
        .w_MCODART = IIF(.w_FLMOVI='S', .w_CODART, REPL("@", 20))
        .w_DCODART2 = IIF(.w_FLDOCU='S', .w_CODART2, REPL("@", 20))
        .w_MCODART2 = IIF(.w_FLMOVI='S', .w_CODART2, REPL("@", 20))
        .w_DESCRI = .w_ZoomMag.getVar('DESART')
      .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
      .oPgFrm.Page1.oPag.COLORE.Calculate('     ', RGB(0,0,0), RGB(196,255,255))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_CODART<>.w_CODART
            .w_CODART2 = .w_CODART
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.t.)
        if .o_CODMAG<>.w_CODMAG.or. .o_CODART<>.w_CODART.or. .o_CODART2<>.w_CODART2
            .w_SALDOPR = IIF(EMPTY(.w_SALDOPR),'S',IIF(EMPTY(.w_CODMAG) OR .w_CODART<>.w_CODART2,'',.w_SALDOPR))
        endif
        .oPgFrm.Page1.oPag.ZoomMag.Calculate()
            .w_SERIALE = .w_ZoomMag.getVar('SERIAL')
        .DoRTCalc(11,11,.t.)
            .w_CLIFOR = .w_ZoomMag.getVar('DESCON')
        .DoRTCalc(13,13,.t.)
            .w_NUMROW = .w_ZoomMag.getVar('CPROWNUM')
        .DoRTCalc(15,15,.t.)
            .w_NUMRIF = .w_ZoomMag.getVar('NUMRIF')
        .DoRTCalc(17,29,.t.)
        if .o_CODCOM<>.w_CODCOM
            .w_CODATT = SPACE(15)
          .link_2_17('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(IIF(.w_CODART<>.w_CODART2 OR .w_FLELGM='T' OR .w_SALDOPR<>'S','',Ah_msgformat('Saldo al %1:',dtoc(.w_DATINI-1) )))
        if .o_SALDOPR<>.w_SALDOPR
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(IIF(.w_CODART<>.w_CODART2 OR .w_FLELGM='T' OR .w_SALDOPR<>'S','',Ah_msgformat('Saldo al %1:',dtoc(.w_DATFIN) )))
        endif
        .DoRTCalc(31,35,.t.)
            .w_QTAMOV2 = .w_ZoomMag.getVar('QTAMOV')
            .w_CAUMAG = .w_ZoomMag.getVar('CMDESCRI')
            .w_UM2 = .w_ZoomMag.getVar('UNIMIS')
        if .o_CODART<>.w_CODART.or. .o_CODART2<>.w_CODART2
          .link_2_23('Full')
        endif
        .DoRTCalc(40,40,.t.)
        if .o_CODART<>.w_CODART.or. .o_CODART2<>.w_CODART2
          .link_2_25('Full')
        endif
        .DoRTCalc(42,42,.t.)
        if .o_CODART<>.w_CODART.or. .o_CODART2<>.w_CODART2
          .link_2_27('Full')
        endif
        .DoRTCalc(44,44,.t.)
        if .o_CODART<>.w_CODART.or. .o_CODART2<>.w_CODART2
          .link_2_29('Full')
        endif
        .DoRTCalc(46,46,.t.)
            .w_DCODART = IIF(.w_FLDOCU='S', .w_CODART, REPL("@", 20))
            .w_MCODART = IIF(.w_FLMOVI='S', .w_CODART, REPL("@", 20))
            .w_DCODART2 = IIF(.w_FLDOCU='S', .w_CODART2, REPL("@", 20))
            .w_MCODART2 = IIF(.w_FLMOVI='S', .w_CODART2, REPL("@", 20))
            .w_DESCRI = .w_ZoomMag.getVar('DESART')
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.COLORE.Calculate('     ', RGB(0,0,0), RGB(196,255,255))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomMag.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(IIF(.w_CODART<>.w_CODART2 OR .w_FLELGM='T' OR .w_SALDOPR<>'S','',Ah_msgformat('Saldo al %1:',dtoc(.w_DATINI-1) )))
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(IIF(.w_CODART<>.w_CODART2 OR .w_FLELGM='T' OR .w_SALDOPR<>'S','',Ah_msgformat('Saldo al %1:',dtoc(.w_DATFIN) )))
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.COLORE.Calculate('     ', RGB(0,0,0), RGB(196,255,255))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSALDOPR_1_9.enabled = this.oPgFrm.Page1.oPag.oSALDOPR_1_9.mCond()
    this.oPgFrm.Page2.oPag.oCODCON_2_4.enabled = this.oPgFrm.Page2.oPag.oCODCON_2_4.mCond()
    this.oPgFrm.Page2.oPag.oCODCOM_2_13.enabled = this.oPgFrm.Page2.oPag.oCODCOM_2_13.mCond()
    this.oPgFrm.Page2.oPag.oCODATT_2_17.enabled = this.oPgFrm.Page2.oPag.oCODATT_2_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSALDOPR_1_9.visible=!this.oPgFrm.Page1.oPag.oSALDOPR_1_9.mHide()
    this.oPgFrm.Page1.oPag.oTOTCAR_1_15.visible=!this.oPgFrm.Page1.oPag.oTOTCAR_1_15.mHide()
    this.oPgFrm.Page1.oPag.oTOTSCA_1_19.visible=!this.oPgFrm.Page1.oPag.oTOTSCA_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_12.visible=!this.oPgFrm.Page2.oPag.oStr_2_12.mHide()
    this.oPgFrm.Page2.oPag.oCODCOM_2_13.visible=!this.oPgFrm.Page2.oPag.oCODCOM_2_13.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_16.visible=!this.oPgFrm.Page2.oPag.oStr_2_16.mHide()
    this.oPgFrm.Page2.oPag.oCODATT_2_17.visible=!this.oPgFrm.Page2.oPag.oCODATT_2_17.mHide()
    this.oPgFrm.Page2.oPag.oDESCOM_2_18.visible=!this.oPgFrm.Page2.oPag.oDESCOM_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_19.visible=!this.oPgFrm.Page2.oPag.oStr_2_19.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_20.visible=!this.oPgFrm.Page2.oPag.oStr_2_20.mHide()
    this.oPgFrm.Page2.oPag.oDESATT_2_21.visible=!this.oPgFrm.Page2.oPag.oDESATT_2_21.mHide()
    this.oPgFrm.Page1.oPag.oSALDOFIN_1_30.visible=!this.oPgFrm.Page1.oPag.oSALDOFIN_1_30.mHide()
    this.oPgFrm.Page1.oPag.oSALDOINI_1_31.visible=!this.oPgFrm.Page1.oPag.oSALDOINI_1_31.mHide()
    this.oPgFrm.Page1.oPag.oQTAMOV2_1_35.visible=!this.oPgFrm.Page1.oPag.oQTAMOV2_1_35.mHide()
    this.oPgFrm.Page1.oPag.oUM2_1_37.visible=!this.oPgFrm.Page1.oPag.oUM2_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page2.oPag.oCODFAM_2_23.visible=!this.oPgFrm.Page2.oPag.oCODFAM_2_23.mHide()
    this.oPgFrm.Page2.oPag.oDESFAM_2_24.visible=!this.oPgFrm.Page2.oPag.oDESFAM_2_24.mHide()
    this.oPgFrm.Page2.oPag.oCODGRU_2_25.visible=!this.oPgFrm.Page2.oPag.oCODGRU_2_25.mHide()
    this.oPgFrm.Page2.oPag.oDESGRU_2_26.visible=!this.oPgFrm.Page2.oPag.oDESGRU_2_26.mHide()
    this.oPgFrm.Page2.oPag.oCODCAT_2_27.visible=!this.oPgFrm.Page2.oPag.oCODCAT_2_27.mHide()
    this.oPgFrm.Page2.oPag.oDESCAT_2_28.visible=!this.oPgFrm.Page2.oPag.oDESCAT_2_28.mHide()
    this.oPgFrm.Page2.oPag.oCODMAR_2_29.visible=!this.oPgFrm.Page2.oPag.oCODMAR_2_29.mHide()
    this.oPgFrm.Page2.oPag.oDESMAR_2_30.visible=!this.oPgFrm.Page2.oPag.oDESMAR_2_30.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_31.visible=!this.oPgFrm.Page2.oPag.oStr_2_31.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_32.visible=!this.oPgFrm.Page2.oPag.oStr_2_32.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_33.visible=!this.oPgFrm.Page2.oPag.oStr_2_33.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_34.visible=!this.oPgFrm.Page2.oPag.oStr_2_34.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI_1_45.visible=!this.oPgFrm.Page1.oPag.oDESCRI_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomMag.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
      .oPgFrm.Page1.oPag.COLORE.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODMAG
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_4'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_CODMAG = space(5)
        this.w_DESMAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_5'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSMA_SAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_UNIMIS = NVL(_Link_.ARUNMIS1,space(3))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_UNIMIS = space(3)
      this.w_TIPART = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_UNIMIS = space(3)
        this.w_TIPART = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART2
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART2)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART2))
          select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART2)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART2)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART2)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART2) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART2_1_7'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSMA_SAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART2)
            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART2 = NVL(_Link_.ARCODART,space(20))
      this.w_DESART2 = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_UNIMIS = NVL(_Link_.ARUNMIS1,space(3))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODART2 = space(20)
      endif
      this.w_DESART2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_UNIMIS = space(3)
      this.w_TIPART = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS' AND  ((UPPER(.w_CODART) <= UPPER(.w_CODART2)) or (empty(.w_CODART2)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_CODART2 = space(20)
        this.w_DESART2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_UNIMIS = space(3)
        this.w_TIPART = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_2_4'),i_cWhere,'GSAR_BZC',"Clienti/fornitori/conti",'GSCG1MPN.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUSEL
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CAUSEL)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CAUSEL))
          select CMCODICE,CMDESCRI,CMDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUSEL)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_CAUSEL)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_CAUSEL)+"%");

            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAUSEL) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCAUSEL_2_5'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUSEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUSEL)
            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSEL = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CMDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSEL = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale di magazzino inesistente oppure obsoleta")
        endif
        this.w_CAUSEL = space(5)
        this.w_DESCAU = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_2_13'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_CODCOM = space(15)
        this.w_DESCOM = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_2_17'),i_cWhere,'GSPC_BZZ',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( FADESCRI like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%")+cp_TransWhereFldName('FADESCRI',trim(this.w_CODFAM))+")";

            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStr(trim(this.w_CODFAM)+"%");

            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_2_23'),i_cWhere,'GSAR_AFA',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_CODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_CODGRU))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODGRU)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( GMDESCRI like "+cp_ToStrODBC(trim(this.w_CODGRU)+"%")+cp_TransWhereFldName('GMDESCRI',trim(this.w_CODGRU))+")";

            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GMDESCRI like "+cp_ToStr(trim(this.w_CODGRU)+"%");

            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCODGRU_2_25'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_CODGRU)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_2_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( OMDESCRI like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%")+cp_TransWhereFldName('OMDESCRI',trim(this.w_CODCAT))+")";

            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" OMDESCRI like "+cp_ToStr(trim(this.w_CODCAT)+"%");

            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCAT_2_27'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAR
  func Link_2_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_CODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStr(trim(this.w_CODMAR)+"%");

            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oCODMAR_2_29'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_CODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_CODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAR = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_2.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_2.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_3.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_3.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_4.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_4.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_5.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_5.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_6.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_6.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART2_1_7.value==this.w_CODART2)
      this.oPgFrm.Page1.oPag.oCODART2_1_7.value=this.w_CODART2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART2_1_8.value==this.w_DESART2)
      this.oPgFrm.Page1.oPag.oDESART2_1_8.value=this.w_DESART2
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDOPR_1_9.RadioValue()==this.w_SALDOPR)
      this.oPgFrm.Page1.oPag.oSALDOPR_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCAR_1_15.value==this.w_TOTCAR)
      this.oPgFrm.Page1.oPag.oTOTCAR_1_15.value=this.w_TOTCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIFOR_1_18.value==this.w_CLIFOR)
      this.oPgFrm.Page1.oPag.oCLIFOR_1_18.value=this.w_CLIFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTSCA_1_19.value==this.w_TOTSCA)
      this.oPgFrm.Page1.oPag.oTOTSCA_1_19.value=this.w_TOTSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_22.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_22.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_28.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_28.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oFLDOCU_2_1.RadioValue()==this.w_FLDOCU)
      this.oPgFrm.Page2.oPag.oFLDOCU_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLMOVI_2_2.RadioValue()==this.w_FLMOVI)
      this.oPgFrm.Page2.oPag.oFLMOVI_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPCON_2_3.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page2.oPag.oTIPCON_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON_2_4.value==this.w_CODCON)
      this.oPgFrm.Page2.oPag.oCODCON_2_4.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUSEL_2_5.value==this.w_CAUSEL)
      this.oPgFrm.Page2.oPag.oCAUSEL_2_5.value=this.w_CAUSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_2_6.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_2_6.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oFLELGM_2_7.RadioValue()==this.w_FLELGM)
      this.oPgFrm.Page2.oPag.oFLELGM_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCOM_2_13.value==this.w_CODCOM)
      this.oPgFrm.Page2.oPag.oCODCOM_2_13.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON_2_14.value==this.w_DESCON)
      this.oPgFrm.Page2.oPag.oDESCON_2_14.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCODATT_2_17.value==this.w_CODATT)
      this.oPgFrm.Page2.oPag.oCODATT_2_17.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_2_18.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_2_18.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT_2_21.value==this.w_DESATT)
      this.oPgFrm.Page2.oPag.oDESATT_2_21.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDOFIN_1_30.value==this.w_SALDOFIN)
      this.oPgFrm.Page1.oPag.oSALDOFIN_1_30.value=this.w_SALDOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDOINI_1_31.value==this.w_SALDOINI)
      this.oPgFrm.Page1.oPag.oSALDOINI_1_31.value=this.w_SALDOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAMOV2_1_35.value==this.w_QTAMOV2)
      this.oPgFrm.Page1.oPag.oQTAMOV2_1_35.value=this.w_QTAMOV2
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUMAG_1_36.value==this.w_CAUMAG)
      this.oPgFrm.Page1.oPag.oCAUMAG_1_36.value=this.w_CAUMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oUM2_1_37.value==this.w_UM2)
      this.oPgFrm.Page1.oPag.oUM2_1_37.value=this.w_UM2
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFAM_2_23.value==this.w_CODFAM)
      this.oPgFrm.Page2.oPag.oCODFAM_2_23.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAM_2_24.value==this.w_DESFAM)
      this.oPgFrm.Page2.oPag.oDESFAM_2_24.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODGRU_2_25.value==this.w_CODGRU)
      this.oPgFrm.Page2.oPag.oCODGRU_2_25.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU_2_26.value==this.w_DESGRU)
      this.oPgFrm.Page2.oPag.oDESGRU_2_26.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCAT_2_27.value==this.w_CODCAT)
      this.oPgFrm.Page2.oPag.oCODCAT_2_27.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT_2_28.value==this.w_DESCAT)
      this.oPgFrm.Page2.oPag.oDESCAT_2_28.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMAR_2_29.value==this.w_CODMAR)
      this.oPgFrm.Page2.oPag.oCODMAR_2_29.value=this.w_CODMAR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAR_2_30.value==this.w_DESMAR)
      this.oPgFrm.Page2.oPag.oDESMAR_2_30.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_45.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_45.value=this.w_DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DATINI)) or not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DATFIN)) or not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS')  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS' AND  ((UPPER(.w_CODART) <= UPPER(.w_CODART2)) or (empty(.w_CODART2))))  and not(empty(.w_CODART2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART2_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and (.w_TIPCON $ 'CF')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CAUSEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCAUSEL_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale di magazzino inesistente oppure obsoleta")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(NOT((g_COMM='S'  OR (g_PERCCR='S' AND g_PERCAN='S'))))  and ((g_COMM='S' OR (g_PERCCR='S' AND g_PERCAN='S')))  and not(empty(.w_CODCOM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCOM_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODMAG = this.w_CODMAG
    this.o_CODART = this.w_CODART
    this.o_CODART2 = this.w_CODART2
    this.o_SALDOPR = this.w_SALDOPR
    this.o_CODCON = this.w_CODCON
    this.o_CODCOM = this.w_CODCOM
    return

enddefine

* --- Define pages as container
define class tgsma_szmPag1 as StdContainer
  Width  = 799
  height = 505
  stdWidth  = 799
  stdheight = 505
  resizeXpos=606
  resizeYpos=362
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_2 as StdField with uid="LTAVXFVRYS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di inizio selezione (vuota=no selezione)",;
    HelpContextID = 3929546,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=96, Top=8

  func oDATINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_3 as StdField with uid="GQFGRUUXEG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di fine selezione (vuota=no selezione)",;
    HelpContextID = 193918410,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=96, Top=35

  func oDATFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oCODMAG_1_4 as StdField with uid="PLXJMKGUOC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Magazzino selezionato",;
    HelpContextID = 50915290,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=96, Top=62, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oCODART_1_5 as StdField with uid="HLUYFRZCXR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
    ToolTipText = "Articolo selezionato",;
    HelpContextID = 84207578,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=239, Top=8, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSMA_SAR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oDESART_1_6 as StdField with uid="DHPYTCRITK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 84148682,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=390, Top=8, InputMask=replicate('X',40)

  add object oCODART2_1_7 as StdField with uid="OEPPCSVAVH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODART2", cQueryName = "CODART2",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
    ToolTipText = "Articolo selezionato",;
    HelpContextID = 84207578,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=239, Top=35, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART2"

  func oCODART2_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART2_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART2_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART2_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSMA_SAR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART2_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART2
     i_obj.ecpSave()
  endproc

  add object oDESART2_1_8 as StdField with uid="YXMLRSMWQS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESART2", cQueryName = "DESART2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 84148682,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=390, Top=35, InputMask=replicate('X',40)

  add object oSALDOPR_1_9 as StdCheck with uid="TNHKSPXGIO",rtseq=9,rtrep=.f.,left=666, top=6, caption="Saldo progressivo",;
    ToolTipText = "Se attivo calcola saldo progressivo",;
    HelpContextID = 154236122,;
    cFormVar="w_SALDOPR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSALDOPR_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSALDOPR_1_9.GetRadio()
    this.Parent.oContained.w_SALDOPR = this.RadioValue()
    return .t.
  endfunc

  func oSALDOPR_1_9.SetRadio()
    this.Parent.oContained.w_SALDOPR=trim(this.Parent.oContained.w_SALDOPR)
    this.value = ;
      iif(this.Parent.oContained.w_SALDOPR=='S',1,;
      0)
  endfunc

  func oSALDOPR_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODMAG))
    endwith
   endif
  endfunc

  func oSALDOPR_1_9.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODMAG) OR .w_CODART<>.w_CODART2)
    endwith
  endfunc


  add object oBtn_1_10 as StdButton with uid="UQAYNXETPV",left=745, top=40, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 64937194;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        do GSMA_BVM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="EMKIAWJZRA",left=30, top=460, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento/movimento di magazzino associato";
    , HelpContextID = 253832863;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_SERIALE, .w_NUMRIF)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="BGNXKKRDGI",left=747, top=460, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 33893446;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomMag as cp_zoombox with uid="PKHRFVRZNE",left=3, top=88, width=794,height=320,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="MVM_MAST",cZoomFile="GSMA_SZM",bOptions=.f.,bQueryOnLoad=.f.,cZoomOnZoom="GSZM_BCC",bRetriveAllRows=.f.,bAdvOptions=.t.,bReadOnly=.t.,cMenuFile="",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 63861786

  add object oTOTCAR_1_15 as StdField with uid="PXOYXUGPHK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_TOTCAR", cQueryName = "TOTCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale carichi dei movimenti selezionati",;
    HelpContextID = 135390922,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=430, Top=410, cSayPict="v_PQ(14)", cGetPict="v_PV(14)"

  func oTOTCAR_1_15.mHide()
    with this.Parent.oContained
      return (.w_CODART<>.w_CODART2 OR .w_FLELGM='T')
    endwith
  endfunc

  add object oCLIFOR_1_18 as StdField with uid="CGXMNFWRZN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CLIFOR", cQueryName = "CLIFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Rag.sociale del cliente/fornitore",;
    HelpContextID = 120560346,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=81, Top=410, InputMask=replicate('X',35)

  add object oTOTSCA_1_19 as StdField with uid="OPMSKFZSCC",rtseq=13,rtrep=.f.,;
    cFormVar = "w_TOTSCA", cQueryName = "TOTSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale scarichi dei movimenti selezionati",;
    HelpContextID = 149022410,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=519, Top=410, cSayPict="v_PQ(14)", cGetPict="v_PV(14)"

  func oTOTSCA_1_19.mHide()
    with this.Parent.oContained
      return (.w_CODART<>.w_CODART2 OR .w_FLELGM='T')
    endwith
  endfunc

  add object oUNIMIS_1_22 as StdField with uid="RMHINXKLNQ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura principale di riferimento",;
    HelpContextID = 109615034,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=658, Top=410, InputMask=replicate('X',3)

  add object oDESMAG_1_28 as StdField with uid="LXZNXWAOMN",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 50856394,;
   bGlobalFont=.t.,;
    Height=21, Width=232, Left=165, Top=62, InputMask=replicate('X',30)


  add object oObj_1_29 as cp_runprogram with uid="EJGYZWXGXP",left=-3, top=512, width=164,height=19,;
    caption='GSMA_BVM',;
   bGlobalFont=.t.,;
    prg="GSMA_BVM",;
    cEvent = "LanciaART",;
    nPag=1;
    , HelpContextID = 104092493

  add object oSALDOFIN_1_30 as StdField with uid="FZCAZJEESC",rtseq=33,rtrep=.f.,;
    cFormVar = "w_SALDOFIN", cQueryName = "SALDOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo alla data di fine selezione",;
    HelpContextID = 214862708,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=517, Top=435, cSayPict="v_PQ(14)", cGetPict="v_PV(14)"

  func oSALDOFIN_1_30.mHide()
    with this.Parent.oContained
      return (.w_CODART<>.w_CODART2 OR .w_FLELGM='T' OR .w_SALDOPR<>'S')
    endwith
  endfunc

  add object oSALDOINI_1_31 as StdField with uid="ONPPBBDTPK",rtseq=34,rtrep=.f.,;
    cFormVar = "w_SALDOINI", cQueryName = "SALDOINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo al periodo precedente",;
    HelpContextID = 265194351,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=517, Top=62, cSayPict="v_PQ(14)", cGetPict="v_PV(14)"

  func oSALDOINI_1_31.mHide()
    with this.Parent.oContained
      return (.w_CODART<>.w_CODART2 OR .w_FLELGM='T' OR .w_SALDOPR<>'S')
    endwith
  endfunc


  add object oObj_1_32 as cp_calclbl with uid="IMBWALWWYF",left=400, top=65, width=115,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 63861786


  add object oObj_1_33 as cp_calclbl with uid="FUJTXXTHPR",left=400, top=438, width=115,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 63861786

  add object oQTAMOV2_1_35 as StdField with uid="QEXZIHXPLW",rtseq=36,rtrep=.f.,;
    cFormVar = "w_QTAMOV2", cQueryName = "QTAMOV2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� movimentata nell'unit� di misura del movimento",;
    HelpContextID = 53023226,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=696, Top=435

  func oQTAMOV2_1_35.mHide()
    with this.Parent.oContained
      return (.w_ZoomMag.getVar('QTAMOV')=0)
    endwith
  endfunc

  add object oCAUMAG_1_36 as StdField with uid="MTFESZWSXY",rtseq=37,rtrep=.f.,;
    cFormVar = "w_CAUMAG", cQueryName = "CAUMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Causale di magazzino",;
    HelpContextID = 50849242,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=81, Top=435, InputMask=replicate('X',35)

  add object oUM2_1_37 as StdField with uid="PJLALWKCCC",rtseq=38,rtrep=.f.,;
    cFormVar = "w_UM2", cQueryName = "UM2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura utilizzata nel movimento",;
    HelpContextID = 26801990,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=658, Top=435, InputMask=replicate('X',3)

  func oUM2_1_37.mHide()
    with this.Parent.oContained
      return (.w_ZoomMag.getVar('UNIMIS')=.w_UNIMIS OR EMPTY(Nvl(.w_ZoomMag.getVar('UNIMIS'),'   ')))
    endwith
  endfunc

  add object oDESCRI_1_45 as StdField with uid="BWZAPFCICZ",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione dell'articolo",;
    HelpContextID = 131530,;
   bGlobalFont=.t.,;
    Height=21, Width=253, Left=361, Top=410, InputMask=replicate('X',35)

  func oDESCRI_1_45.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc


  add object oObj_1_47 as cp_runprogram with uid="DTCMDYJPBQ",left=164, top=512, width=350,height=19,;
    caption='GSAR_BZM(w_SERIALE w_NUMRIF)',;
   bGlobalFont=.t.,;
    prg="GSAR_BZM(w_SERIALE, w_NUMRIF)",;
    cEvent = "w_zoommag selected",;
    nPag=1;
    , HelpContextID = 182694239


  add object COLORE as cp_calclbl with uid="WHVDTETLZN",left=619, top=65, width=28,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 63861786

  add object oStr_1_16 as StdString with uid="XFMLZQKRGB",Visible=.t., Left=175, Top=7,;
    Alignment=1, Width=62, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="QVDTSBUJZV",Visible=.t., Left=13, Top=410,;
    Alignment=1, Width=66, Height=15,;
    Caption="Cli./for.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="MLHBNYESZZ",Visible=.t., Left=617, Top=410,;
    Alignment=1, Width=39, Height=18,;
    Caption="U.M.1:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="WFSLOEXJVM",Visible=.t., Left=362, Top=410,;
    Alignment=1, Width=63, Height=15,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_CODART<>.w_CODART2 OR .w_FLELGM='T')
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="DTVEASFBBT",Visible=.t., Left=33, Top=8,;
    Alignment=1, Width=61, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="ZTIGJHQMBR",Visible=.t., Left=34, Top=35,;
    Alignment=1, Width=60, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="WKFDBFDADS",Visible=.t., Left=2, Top=62,;
    Alignment=1, Width=92, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="GQOVRYEVJP",Visible=.t., Left=617, Top=436,;
    Alignment=1, Width=39, Height=18,;
    Caption="U.M.2:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_ZoomMag.getVar('UNIMIS')=.w_UNIMIS OR EMPTY(Nvl(.w_ZoomMag.getVar('UNIMIS'),'   ')))
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="UNQAQTUHXR",Visible=.t., Left=3, Top=436,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="OSDTYNEQSK",Visible=.t., Left=171, Top=35,;
    Alignment=1, Width=66, Height=18,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="WTSICGRKEU",Visible=.t., Left=331, Top=410,;
    Alignment=1, Width=29, Height=18,;
    Caption="Art:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="LUGNFBYLPF",Visible=.t., Left=650, Top=66,;
    Alignment=0, Width=89, Height=17,;
    Caption="Doc. provvisorio"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgsma_szmPag2 as StdContainer
  Width  = 799
  height = 505
  stdWidth  = 799
  stdheight = 505
  resizeXpos=485
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLDOCU_2_1 as StdCheck with uid="TLDVEFVKVJ",rtseq=18,rtrep=.f.,left=158, top=13, caption="Documenti",;
    ToolTipText = "Se attivo: seleziona i movimenti di magazzino da documenti",;
    HelpContextID = 82242218,;
    cFormVar="w_FLDOCU", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLDOCU_2_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLDOCU_2_1.GetRadio()
    this.Parent.oContained.w_FLDOCU = this.RadioValue()
    return .t.
  endfunc

  func oFLDOCU_2_1.SetRadio()
    this.Parent.oContained.w_FLDOCU=trim(this.Parent.oContained.w_FLDOCU)
    this.value = ;
      iif(this.Parent.oContained.w_FLDOCU=='S',1,;
      0)
  endfunc

  add object oFLMOVI_2_2 as StdCheck with uid="RUSJPRWUMG",rtseq=19,rtrep=.f.,left=158, top=33, caption="Movimenti manuali",;
    ToolTipText = "Se attivo: seleziona i movimenti di magazzino manuali",;
    HelpContextID = 263609002,;
    cFormVar="w_FLMOVI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLMOVI_2_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLMOVI_2_2.GetRadio()
    this.Parent.oContained.w_FLMOVI = this.RadioValue()
    return .t.
  endfunc

  func oFLMOVI_2_2.SetRadio()
    this.Parent.oContained.w_FLMOVI=trim(this.Parent.oContained.w_FLMOVI)
    this.value = ;
      iif(this.Parent.oContained.w_FLMOVI=='S',1,;
      0)
  endfunc


  add object oTIPCON_2_3 as StdCombo with uid="DARIYNSFKZ",value=3,rtseq=20,rtrep=.f.,left=158,top=66,width=111,height=21;
    , ToolTipText = "Tipo movimenti da selezionare, clienti, fornitori, conti generici";
    , HelpContextID = 187837642;
    , cFormVar="w_TIPCON",RowSource=""+"Clienti,"+"Fornitori,"+"No selezione", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPCON_2_3.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oTIPCON_2_3.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_2_3.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      iif(this.Parent.oContained.w_TIPCON=='',3,;
      0)))
  endfunc

  func oTIPCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_2_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCON_2_4 as StdField with uid="NANNYIVVGY",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Cliente/fornitore/conto di selezione scadenze",;
    HelpContextID = 187885530,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=158, Top=98, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCON $ 'CF')
    endwith
   endif
  endfunc

  func oCODCON_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori/conti",'GSCG1MPN.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oCAUSEL_2_5 as StdField with uid="PBQCFKGGJJ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CAUSEL", cQueryName = "CAUSEL",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale di magazzino inesistente oppure obsoleta",;
    ToolTipText = "Causale di magazzino di selezione (spazio=no selezione)",;
    HelpContextID = 230811098,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=158, Top=127, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CAUSEL"

  func oCAUSEL_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUSEL_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUSEL_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCAUSEL_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oCAUSEL_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CAUSEL
     i_obj.ecpSave()
  endproc

  add object oDESCAU_2_6 as StdField with uid="MNDCHOKQAL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 85066186,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=296, Top=127, InputMask=replicate('X',35)


  add object oFLELGM_2_7 as StdCombo with uid="YNUKLDEVCP",value=2,rtseq=24,rtrep=.f.,left=158,top=156,width=111,height=21;
    , ToolTipText = "Tipo movimenti da selezionare: fiscali, non fiscali o tutti";
    , HelpContextID = 212458154;
    , cFormVar="w_FLELGM",RowSource=""+"Fiscali,"+"Non fiscali,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFLELGM_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,' ',;
    iif(this.value =3,'T',;
    ' '))))
  endfunc
  func oFLELGM_2_7.GetRadio()
    this.Parent.oContained.w_FLELGM = this.RadioValue()
    return .t.
  endfunc

  func oFLELGM_2_7.SetRadio()
    this.Parent.oContained.w_FLELGM=trim(this.Parent.oContained.w_FLELGM)
    this.value = ;
      iif(this.Parent.oContained.w_FLELGM=='S',1,;
      iif(this.Parent.oContained.w_FLELGM=='',2,;
      iif(this.Parent.oContained.w_FLELGM=='T',3,;
      0)))
  endfunc

  add object oCODCOM_2_13 as StdField with uid="ZXHBCURSDU",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice commessa di selezione (spazio=no selezione)",;
    HelpContextID = 204662746,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=158, Top=185, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_COMM='S' OR (g_PERCCR='S' AND g_PERCAN='S')))
    endwith
   endif
  endfunc

  func oCODCOM_2_13.mHide()
    with this.Parent.oContained
      return (NOT((g_COMM='S'  OR (g_PERCCR='S' AND g_PERCAN='S'))))
    endwith
  endfunc

  func oCODCOM_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_2_17('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oCODCOM_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODCOM
     i_obj.ecpSave()
  endproc

  add object oDESCON_2_14 as StdField with uid="CPGLKVKXTI",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 187826634,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=296, Top=98, InputMask=replicate('X',40)

  add object oCODATT_2_17 as StdField with uid="TVGXSNNBPH",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di selezione (spazio=no selezione)",;
    HelpContextID = 82110426,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=158, Top=214, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S' AND NOT EMPTY(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODATT_2_17.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' or EMPTY(.w_CODCOM))
    endwith
  endfunc

  func oCODATT_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit�",'',this.parent.oContained
  endproc
  proc oCODATT_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_CODATT
     i_obj.ecpSave()
  endproc

  add object oDESCOM_2_18 as StdField with uid="WIMAAWIWPE",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 204603850,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=296, Top=185, InputMask=replicate('X',40)

  func oDESCOM_2_18.mHide()
    with this.Parent.oContained
      return (NOT((g_COMM='S' OR (g_PERCCR='S' AND g_PERCAN='S'))))
    endwith
  endfunc

  add object oDESATT_2_21 as StdField with uid="MMTKYHTIWS",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 82051530,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=296, Top=214, InputMask=replicate('X',35)

  func oDESATT_2_21.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' OR EMPTY(.w_CODCOM))
    endwith
  endfunc

  add object oCODFAM_2_23 as StdField with uid="CAZYAQAKNG",rtseq=39,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Famiglia dell'articolo",;
    HelpContextID = 219146202,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=158, Top=243, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_2_23.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc

  func oCODFAM_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglie articoli",'',this.parent.oContained
  endproc
  proc oCODFAM_2_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CODFAM
     i_obj.ecpSave()
  endproc

  add object oDESFAM_2_24 as StdField with uid="SKXLGQCZIL",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 219087306,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=296, Top=243, InputMask=replicate('X',35)

  func oDESFAM_2_24.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc

  add object oCODGRU_2_25 as StdField with uid="NFSMTFLWOP",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico dell'articolo",;
    HelpContextID = 67037146,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=158, Top=272, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_CODGRU"

  func oCODGRU_2_25.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc

  func oCODGRU_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCODGRU_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oCODGRU_2_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_CODGRU
     i_obj.ecpSave()
  endproc

  add object oDESGRU_2_26 as StdField with uid="QHMLHWIKGG",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 66978250,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=296, Top=272, InputMask=replicate('X',35)

  func oDESGRU_2_26.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc

  add object oCODCAT_2_27 as StdField with uid="YJKKTHAHNY",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria omogenea dell'articolo",;
    HelpContextID = 101902298,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=158, Top=301, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_2_27.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc

  func oCODCAT_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_2_27.ecpDrop(oSource)
    this.Parent.oContained.link_2_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAT_2_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCAT_2_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCODCAT_2_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CODCAT
     i_obj.ecpSave()
  endproc

  add object oDESCAT_2_28 as StdField with uid="YUTGXVVCVO",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 101843402,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=296, Top=301, InputMask=replicate('X',35)

  func oDESCAT_2_28.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc

  add object oCODMAR_2_29 as StdField with uid="VELVVYXNNT",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CODMAR", cQueryName = "CODMAR",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca dell'articolo",;
    HelpContextID = 134801370,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=158, Top=330, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_CODMAR"

  func oCODMAR_2_29.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc

  func oCODMAR_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAR_2_29.ecpDrop(oSource)
    this.Parent.oContained.link_2_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAR_2_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oCODMAR_2_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oCODMAR_2_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_CODMAR
     i_obj.ecpSave()
  endproc

  add object oDESMAR_2_30 as StdField with uid="RBQZZJAURK",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 134742474,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=296, Top=330, InputMask=replicate('X',35)

  func oDESMAR_2_30.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc

  add object oStr_2_8 as StdString with uid="QLTZGTLZZI",Visible=.t., Left=12, Top=127,;
    Alignment=1, Width=143, Height=15,;
    Caption="Causale magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="JJWUPTZRYN",Visible=.t., Left=12, Top=185,;
    Alignment=1, Width=143, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_12.mHide()
    with this.Parent.oContained
      return (NOT((g_COMM='S'  OR (g_PERCCR='S' AND g_PERCAN='S'))))
    endwith
  endfunc

  add object oStr_2_15 as StdString with uid="PXYAPZVAGQ",Visible=.t., Left=12, Top=66,;
    Alignment=1, Width=143, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="IRAUNCMSAY",Visible=.t., Left=12, Top=214,;
    Alignment=1, Width=143, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_16.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' OR EMPTY(.w_CODCOM))
    endwith
  endfunc

  add object oStr_2_19 as StdString with uid="ECYYLEOAKD",Visible=.t., Left=12, Top=98,;
    Alignment=1, Width=143, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_2_19.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='F')
    endwith
  endfunc

  add object oStr_2_20 as StdString with uid="PRCOBXZBQH",Visible=.t., Left=12, Top=98,;
    Alignment=1, Width=143, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_2_20.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'F')
    endwith
  endfunc

  add object oStr_2_22 as StdString with uid="YCMDUNYENW",Visible=.t., Left=49, Top=158,;
    Alignment=1, Width=106, Height=18,;
    Caption="Movimenti fiscali:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="YITRMLQGNR",Visible=.t., Left=55, Top=244,;
    Alignment=1, Width=100, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  func oStr_2_31.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc

  add object oStr_2_32 as StdString with uid="TCCQFNGLGY",Visible=.t., Left=46, Top=273,;
    Alignment=1, Width=109, Height=15,;
    Caption="Gr. merceologico:"  ;
  , bGlobalFont=.t.

  func oStr_2_32.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc

  add object oStr_2_33 as StdString with uid="EVOPVZITOG",Visible=.t., Left=45, Top=302,;
    Alignment=1, Width=110, Height=15,;
    Caption="Cat. omogenea:"  ;
  , bGlobalFont=.t.

  func oStr_2_33.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc

  add object oStr_2_34 as StdString with uid="VZMAUGNOHW",Visible=.t., Left=55, Top=331,;
    Alignment=1, Width=100, Height=15,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  func oStr_2_34.mHide()
    with this.Parent.oContained
      return (.w_CODART=.w_CODART2)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_szm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
