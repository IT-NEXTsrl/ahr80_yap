* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bd1                                                        *
*              Elabora compilazione distinte                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_203]                                            *
*                                                                              *
*     Version: 1.0                                                             *
* Date creat.: 1998-01-29                                                      *
* Last revis.: 2018-10-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bd1",oParentObject,m.pEXEC)
return(i_retval)

define class tgste_bd1 as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_ABICOD = space(5)
  w_LFLBANC = space(1)
  w_LCODABI = space(5)
  w_LFLCABI = space(1)
  w_LDITISCA = space(1)
  w_APPO = space(10)
  w_TOTA = 0
  w_INIZ = .f.
  w_SCRIVO = 0
  w_TOTDIS = 0
  w_TROPPO = .f.
  w_TOTAA = 0
  w_TPAR = 0
  w_NUMC = space(15)
  w_SPAR = 0
  w_PROP = 0
  w_CHIAVE = space(50)
  w_OCHIAVE = space(50)
  w_CAMBIO = .f.
  w_CONTA = 0
  w_NUMERO = 0
  w_EFFETTO = 0
  w_PTDATVAL = ctod("  /  /  ")
  w_TIPMAN = space(0)
  w_CONTROL = .NULL.
  w_PADRE = .NULL.
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_PTSERIAL = space(10)
  w_DINUMDIS = space(10)
  w_PTROWORD = 0
  w_DINUMERO = 0
  w_CPROWNUM = 0
  w_DI__ANNO = space(4)
  w_PTTOTIMP = 0
  w_DICOSCOM = 0
  w_PTDATSCA = ctod("  /  /  ")
  w_PTCAOVAL = 0
  w_PTNUMPAR = space(31)
  w_PTCAOAPE = 0
  w_PTBANAPP = space(10)
  w_PTDATAPE = ctod("  /  /  ")
  w_PTBANNOS = space(15)
  w_PTNUMDOC = 0
  w_PTMODPAG = space(10)
  w_PTALFDOC = space(10)
  w_PTTIPCON = space(1)
  w_PTDATDOC = ctod("  /  /  ")
  w_PTCODCON = space(15)
  w_PTCODAGE = space(5)
  w_PT_SEGNO = space(1)
  w_PTIMPDOC = 0
  w_PTCODVAL = space(3)
  w_PTFLIMPE = space(2)
  w_PTFLINDI = space(1)
  w_PTFLRAGG = space(1)
  w_PTDESRIG = space(50)
  w_PTFLVABD = space(1)
  w_OSERIAL = space(10)
  w_OROWORD = 0
  w_OROWNUM = 0
  w_PTNUMCOR = space(25)
  w_CODABI = space(5)
  w_PUNT = 0
  w_KEYPAR = space(40)
  w_PARRES = 0
  w_TOTRAG = 0
  w_MSG_ERR = space(254)
  * --- WorkFile variables
  DIS_TINT_idx=0
  PAR_TITE_idx=0
  BAN_CONTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Compilazione Distinte (da GSTE_MCD)
    * --- Valorizzazioni
    *     G : genera distinte
    *     C : calcola importo partite disponibili ed esegue controlli
    * --- Totale Importo Disponibile dalle Partite/Scadenze
    this.w_TIPMAN = this.oParentObject.w_DITIPMAN
    if this.pEXEC="Z"
      * --- Aggiorno la Combo al cambio del tipo distinta
      this.oParentObject.w_CATIPDIS = this.oParentObject.w_TIPDIS
      this.w_CONTROL = This.oParentObject.GetCtrl("w_DICAUBON")
      this.w_CONTROL.Popola()     
      * --- Rilancio la query per sapere se ho un valore per la causale
      this.oParentObject.w_TEST1 = this.w_CONTROL.LISTCOUNT>0
      This.oParentObject.o_Caurif="     "
    else
      * --- Importo Residuo ancora da Assegnare riferito alla Valuta del Conto
      * --- Notifica alla ecpSave il Successo o il Fallimento della scrittura Distinte
      this.w_PADRE = This.oParentObject
      this.w_PADRE.OkGene = .F.
      * --- Oggetto per messaggi incrementali
      this.w_oMess=createobject("Ah_Message")
      * --- Controlli Preliminari
      do case
        case EMPTY(this.oParentObject.w_CAURIF)
          ah_ErrorMsg("Causale distinta non definita",,"")
          i_retcode = 'stop'
          return
        case EMPTY(this.oParentObject.w_CODVAL)
          ah_ErrorMsg("Codice valuta non definito",,"")
          i_retcode = 'stop'
          return
        case this.oParentObject.w_CAOVAL=0
          ah_ErrorMsg("Cambio scadenze non definito",,"")
          i_retcode = 'stop'
          return
      endcase
      this.oParentObject.w_TOTPAR = 0
      * --- Il filtro sulla data documento lo applico sulla GSTE0KAI, visto 
      *     che, a norma, scadenza appartenenti alla stessa partita debbono
      *     avere questo campo uguale...
      GSTE_BPA (this, this.oParentObject.w_SCAINI , this.oParentObject.w_SCAFIN , this.oParentObject.w_TIPCON , this.oParentObject.w_CODCON, this.oParentObject.w_CODCON, this.oParentObject.w_CODVAL , "", this.oParentObject.w_FLRB1,this.oParentObject.w_FLBO1,this.oParentObject.w_FLRD1,this.oParentObject.w_FLRI1,this.oParentObject.w_FLMA1,this.oParentObject.w_FLCA1, this.oParentObject.w_NUMDIS+this.oParentObject.w_SCELTA, "S", cp_CharToDate("  -  -  ") )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      vq_exec("query\GSTE0KAI.VQR",this,"PAR_APE")
      * --- Drop temporary table TMP_PART_APE
      i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_PART_APE')
      endif
      if RECCOUNT("PAR_APE")>0
        Cur = WrCursor("PAR_APE")
        * --- Eseguo Calcolo Parte Aperta
        GSTE_BCP(this,"D","PAR_APE",this.oParentObject.w_TIPSCA, this.oParentObject.w_FLBANC , this.oParentObject.w_TIPDIS )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Filtro per pagamento della distinta e per Ns Banca, lo faccio adesso perch� prima devo calcolare la parte residua
      *     Eseguo una go top per problema riscontrato in Oracle\Db2 nella select successiva che ripescava record eliminati nella
      *     precedente delete
      select Par_ape 
 Go top
       
 Select * ,DTOC(CP_TODATE(DATSCA))+NVL(TIPCON," ")+; 
 NVL(CODCON,SPACE(15))+NVL(MODPAG,SPACE(10))+; 
 IIF(this.oParentObject.w_FLBANC="A", NVL(BANAPP,SPACE(10)), SPACE(10))+; 
 IIF(this.oParentObject.w_FLBANC="S", NVL(BANNOS,SPACE(15)), SPACE(15)) + IIF(NVL(ANFLRAGG," ") $ "S-T","",SYS(2015)) + IIF(this.oParentObject.w_TIPDIS $ "BO-SC",Nvl(PTNUMCOR,space(25)),Space(25)) AS KEYPAR; 
 From PAR_APE Where TIPPAG IN (this.oParentObject.w_FLRD1,this.oParentObject.w_FLBO1,this.oParentObject.w_FLCA1,this.oParentObject.w_FLRI1,this.oParentObject.w_FLMA1,this.oParentObject.w_FLRB1) ; 
 And (NVL(BANNOS,SPACE(15))=this.oParentObject.w_CODBAN OR EMPTY(this.oParentObject.w_CODBAN)) AND Not Deleted() ; 
 And (Empty( this.oParentObject.w_DATDOCINI ) Or Cp_Todate(DatDoc)>=this.oParentObject.w_DATDOCINI) ; 
 And (Empty( this.oParentObject.w_DATDOCFIN ) Or Cp_Todate(DatDoc)<=this.oParentObject.w_DATDOCFIN) ; 
 And (this.oParentObject.w_NUMDOCINI=0 Or Nvl(NumDoc,0)>=this.oParentObject.w_NUMDOCINI) ; 
 And (this.oParentObject.w_NUMDOCFIN=0 Or Nvl(NumDoc,0)<=this.oParentObject.w_NUMDOCFIN) ; 
 And (Empty( this.oParentObject.w_ALFDOCINI) Or Nvl(AlfDoc,Space(10))>=this.oParentObject.w_ALFDOCINI) ; 
 And (Empty( this.oParentObject.w_ALFDOCFIN) Or Nvl(AlfDoc,Space(10))<=this.oParentObject.w_ALFDOCFIN) ; 
 Into Cursor SelePart NoFilter
      if this.oParentObject.w_IMPMIN<>0
        * --- Applico filtro importo minimo
         
 SELECT KEYPAR,; 
 Sum(Totimp*iif(TipCon="C",1,-1)*iif( Segno="D" ,1,-1 )*IIF( TipCon= this.oParentObject.w_TIPSCA , 1 , -1 ) ) As TOTRAG; 
 From SELEPART group by KEYPAR ORDER BY KEYPAR; 
 Having Abs(TOTRAG) >= this.oParentObject.w_IMPMIN; 
 INTO CURSOR Tmp_Min NOFILTER
        WrCursor("SELEPART")
         
 Delete From Selepart Where KEYPAR Not in (Select KEYPAR from Tmp_Min)
        if USED("Tmp_Min")
           
 Select Tmp_Min 
 Use
        endif
      endif
      if this.oParentObject.w_FLCABI="S"
         
 Select t_codabi From ( this.w_PADRE.ctrsname) where NOT EMPTY(t_DICODCON) into cursor ABI NoFilter
        if RECCOUNT("ABI") <>0
          WrCursor("SELEPART")
           
 Delete From Selepart Where CODABI Not in (Select t_codabi from ABI)
        endif
      endif
      if USED("ABI")
         
 Select ABI 
 Use
      endif
      Use in Par_Ape
      * --- Elimino le Scadenze non congruenti
      *     ==========================================================
      * --- Elimino le Scadenze non congruenti
      b=Wrcursor("Selepart")
      if USED("Selepart")
        * --- Al termine Legge il Totale Importi Disponibili sulle Partite
        SELECT Selepart
        GO TOP
        * --- Verifica se ci sono record con Banca di Appoggio/Nostra Banca
        * --- Solo le Partite che hanno la Banca di Appoggio Cliente Valorizzata
        do case
          case this.oParentObject.w_FLBANC="A" Or this.oParentObject.w_FLBANC="S"
            if this.pEXEC="C"
              COUNT FOR IIF( this.oParentObject.w_FLBANC="A" , EMPTY(NVL(BANAPP, " ")) ,EMPTY(NVL(BANNOS, " ")) ) TO this.w_CONTA
              if this.w_CONTA<>0
                if this.oParentObject.w_FLBANC="A"
                  this.w_APPO = "Esistono %1 scadenze senza la banca d'appoggio%0Tali scadenze non verranno considerate durante l'elaborazione"
                else
                  this.w_APPO = "Esistono %1 scadenze senza la nostra banca di presentazione%0Tali scadenze non verranno considerate durante l'elaborazione"
                endif
                ah_ErrorMsg(this.w_APPO,,"", ALLTRIM(STR(this.w_CONTA)), )
              endif
              * --- Calcola Totale delle Partite disponibili
               
 Sum TOTIMP* IIF( (this.oParentObject.w_TIPSCA="C" And Segno="A") Or (this.oParentObject.w_TIPSCA="F" And Segno="D") ,-1 ,1 ) ; 
 To this.oParentObject.w_TOTPAR For Not Deleted() And NOT IIF( this.oParentObject.w_FLBANC="A" , EMPTY(NVL(BANAPP, " ")) ,EMPTY(NVL(BANNOS, " ")) ) 
            else
               
 DELETE FROM Selepart ; 
 WHERE NVL(TOTIMP, 0)=0 OR NVL(CAOVAL,0)=0 OR NVL(CAOAPE,0)=0 OR EMPTY(NVL(TIPPAG," ")) ; 
 OR IIF( this.oParentObject.w_FLBANC="A" , EMPTY(NVL(BANAPP, " ")) ,EMPTY(NVL(BANNOS, " ")) ) OR ; 
 (this.oParentObject.w_TIPDIS $ "BO-SC" AND this.oParentObject.w_TIPSCA="F" AND this.oParentObject.w_FLIBAN="S" AND (EMPTY(NVL(AN__IBAN, "")) OR EMPTY(NVL(AN__BBAN, "")))) 
 
 Sum TOTIMP* IIF( (this.oParentObject.w_TIPSCA="C" And Segno="A") Or (this.oParentObject.w_TIPSCA="F" And Segno="D") ,-1 ,1 ) ; 
 To this.oParentObject.w_TOTPAR For Not Deleted()
            endif
          case .T.
            * --- Calcola Totale delle Partite disponibili
             
 Sum TOTIMP* IIF( (this.oParentObject.w_TIPSCA="C" And Segno="A") Or (this.oParentObject.w_TIPSCA="F" And Segno="D") ,-1 ,1 ) ; 
 To this.oParentObject.w_TOTPAR For Not Deleted()
        endcase
      endif
      * --- Solo le Partite che hanno la Banca di Appoggio Cliente Valorizzata
      if this.oParentObject.w_TOTPAR=0
        ah_ErrorMsg("Non esistono importi partite/scadenze da assegnare",,"")
        if USED("SELEPART")
           
 Select Selepart 
 Use
        endif
        i_retcode = 'stop'
        return
      endif
      if this.pEXEC="G"
        this.w_TROPPO = .F.
        this.w_CONTA = 0
        this.w_NUMERO = 0
        * --- Try
        local bErr_050CFB20
        bErr_050CFB20=bTrsErr
        this.Try_050CFB20()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("%1%0%0Nessuna distinta generata",,"", i_ERRMSG)
          this.w_SCRIVO = -1
          this.w_TROPPO = .F.
        endif
        bTrsErr=bTrsErr or bErr_050CFB20
        * --- End
        if this.w_SCRIVO=0
          if this.w_TROPPO
            this.w_APPO = "Nessuna distinta generata%0Importo partite troppo alto per gli importi proposti in distinta"
          else
            this.w_APPO = "Nessuna distinta generata"
          endif
          ah_ErrorMsg(this.w_APPO,,"")
        endif
      else
        if USED("SELEPART")
           
 Select Selepart 
 Use
        endif
      endif
    endif
  endproc
  proc Try_050CFB20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_SCRIVO = 0
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_PADRE.OkGene = .T.
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Distinte
    * --- Potrebbe essere rimasto aperto a seguito di una raise...
    if used("T1")
      select T1
      use
    endif
    if this.oParentObject.w_FLRAGG="S"
      * --- Tutte le partite raggruppate devono finire nella stessa distinta
       
 SELECT * , NVL(TIPCON," ")+; 
 NVL(CODCON,SPACE(15))+NVL(MODPAG,SPACE(10))+; 
 IIF(this.oParentObject.w_FLBANC="A", NVL(BANAPP,SPACE(10)), SPACE(10))+; 
 IIF(this.oParentObject.w_FLBANC="S", NVL(BANNOS,SPACE(15)), SPACE(15)) + IIF(this.oParentObject.w_TIPDIS $ "BO-SC",Nvl(PTNUMCOR,Space(25)),Space(25)) AS CHIAVE; 
 FROM SELEPART INTO CURSOR T1 ORDER BY CHIAVE NOFILTER
      * --- Creo Cursore di Appoggio Raggruppato
       
 SELECT NVL(TIPCON," ")+; 
 NVL(CODCON,SPACE(15))+NVL(MODPAG,SPACE(10))+; 
 IIF(this.oParentObject.w_FLBANC="A", NVL(BANAPP,SPACE(10)), SPACE(10))+; 
 IIF(this.oParentObject.w_FLBANC="S", NVL(BANNOS,SPACE(15)), SPACE(15)) + IIF(this.oParentObject.w_TIPDIS $ "BO-SC",Nvl(PTNUMCOR,Space(25)),Space(25)) AS CHIAVE,Count(*) AS NUMRAG,; 
 Sum(Totimp*iif(TipCon="C",1,-1)*iif( Segno="D" ,1,-1 )*IIF( TipCon= this.oParentObject.w_TIPSCA , 1 , -1 ) ) As TOTRAG; 
 From SELEPART group by CHIAVE ORDER BY CHIAVE; 
 INTO CURSOR TEMP NOFILTER
      * --- Inserisco importo di raggruppamento nelle relative righe
       
 SELECT T1.* ,Abs(TEMP.TOTRAG) AS TOTRAG,TEMP.NUMRAG AS NUMRAG FROM T1; 
 INNER JOIN TEMP ON (T1.CHIAVE=TEMP.CHIAVE) INTO CURSOR T1
      SELECT TEMP 
 USE
    else
      SELECT * , TOTIMP AS TOTRAG,1 AS NUMRAG," " AS CHIAVE FROM SELEPART INTO CURSOR T1
    endif
    do case
      case this.oParentObject.w_TIPSEL="C"
        * --- Ordino il Cursore per Importo crescente
        SELECT * FROM T1 INTO CURSOR T1 ORDER BY TOTRAG,TOTIMP NOFILTER
      case this.oParentObject.w_TIPSEL="D"
        * --- Ordino il Cursore per Data Scadenza
        SELECT * FROM T1 INTO CURSOR T1 ORDER BY DATSCA,CODCON,TOTIMP NOFILTER
      case .T.
        * --- Ordino il Cursore per Importo decrescente
        SELECT * FROM T1 INTO CURSOR T1 ORDER BY TOTRAG DESC,TOTIMP NOFILTER
    endcase
    SELECT SELEPART 
 USE
    * --- Rende riscrivibile il Risultato della Query
    A=WrCursor ("T1")
    SELECT T1
    this.w_CPROWNUM = 0
    this.w_TPAR = RECCOUNT("T1")
    this.w_SPAR = 0
    this.w_TOTAA = 0
    this.oParentObject.w_TOTRES = this.oParentObject.w_TOTPAR
    * --- Ciclo sull'Elenco dei Conti con Importi Valorizzati
    this.w_PADRE.MarkPos()     
     
 Select (this.w_PADRE.cTrsName) 
 Go Top
    SCAN FOR t_DIIMPPRO<>0 AND NOT EMPTY(NVL(t_DICODCON," ")) AND NOT DELETED()
    * --- Legge i Dati del Temporaneo
    this.w_PADRE.WorkFromTrs()     
    this.w_NUMC = t_DICODCON
    this.w_PROP = t_DIIMPPRO
    this.w_PARRES = t_DIIMPPRO
    this.w_DICOSCOM = NVL(t_DISPECOM, 0)
    this.w_CODABI = NVL(t_CODABI, Space(5))
    this.w_TOTA = 0
    this.w_TOTDIS = 0
    this.w_INIZ = .F.
    this.w_OCHIAVE = "@@@@@%%%%%"
    ah_Msg("Elaborazione conto: %1",.T.,.F.,.F., this.w_NUMC)
    SELECT T1
    GO TOP
     
 SCAN FOR NOT EMPTY(NVL(DATSCA," ")) AND NOT EMPTY(NVL(NUMPAR," ")) AND NOT EMPTY(NVL(CODCON," ")); 
 AND NVL(TOTIMP,0)<>0 AND ((this.oParentObject.w_FLRAGG="S" AND this.w_PARRES>= Nvl(TOTRAG,0)) OR this.oParentObject.w_FLRAGG<>"S" ) AND ; 
 (Empty(this.oParentObject.w_FLCABI) OR (this.oParentObject.w_FLCABI="S" AND this.w_CODABI=Nvl(CODABI,Space(5)))) AND IIF(this.oParentObject.w_TIPSCA="F" AND this.oParentObject.w_TIPDIS="RB",BANNOS=this.w_NUMC,.T.)
    if this.oParentObject.w_FLRAGG="S" AND T1.NUMRAG>1
      * --- Nel caso di Righe Raggruppate schedulo prima le righe dello stesso raggruppamento 
      *     salvando il puntatore di riga del cursore per ripristinarlo alla fine
      this.w_TOTRAG = NVL(T1.TOTRAG,0)
      this.w_KEYPAR = T1.CHIAVE
       
 Select T1
      this.w_PUNT = Recno("T1")
       
 Go Top 
 Scan For this.w_KEYPAR=T1.CHIAVE
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
       
 ENDSCAN 
 GoTo this.w_PUNT
      * --- Decurto dal residuo l'intero ammontare delle partite raggruppate
      this.w_PARRES = this.w_PARRES-this.w_TOTRAG
    else
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Decurto dal residuo l'importo della singola partita...
      this.w_PARRES = this.w_PARRES-this.w_PTTOTIMP
    endif
    ENDSCAN
    * --- Carica il Temporaneo dei Dati
    this.oParentObject.w_DIIMPCAL = this.w_TOTA
    this.w_PADRE.TrsFromWork()     
    this.w_TOTAA = this.w_TOTAA + this.w_TOTA
    * --- A seguito della creazione della distinta verifico se contiene effetti negativi..
    vq_exec("Query\GSTE_BD1_1.VQR",this,"TEST_DIST")
    this.w_MSG_ERR = GSTE_BCR(This , "C" , "TEST_DIST" , this.oParentObject.w_FLBANC , this.oParentObject.w_TIPDIS , this.oParentObject.w_TIPSCA)
    Use In TEST_DIST 
    * --- Se recupero un effetto negativo esco dalla Scan ed annullo tutto
    if Not Empty( this.w_MSG_ERR )
      Exit
    endif
    ENDSCAN
    if Not Empty( this.w_MSG_ERR )
      * --- Raise
      i_Error=this.w_MSG_ERR
      return
    endif
    if used("T1")
      select T1
      use
    endif
    * --- Questa Parte derivata dal Metodo LoadRec
    this.w_PADRE.RePos()     
    this.bUpdateParentObject=.F.
    * --- Ricalcola il Residuo
    WAIT CLEAR
    this.oParentObject.w_TOTALI = this.w_TOTAA
    this.oParentObject.w_TOTRES = this.oParentObject.w_TOTRES - this.oParentObject.w_TOTALI
    * --- Se l'ordine di assegnazione partite � per data scadenza
    if this.oParentObject.w_TIPSEL="D" 
      if this.w_SCRIVO>0 AND this.w_CONTA>0 AND this.w_NUMERO>1
        ah_ErrorMsg("Importi proposti troppo bassi%0Alcuni effetti relativi allo stesso cliente/fornitore sono stati inseriti in distinte diverse",,"")
      endif
    endif
    if this.w_SCRIVO>0
      this.w_oPart = this.w_oMess.AddMsgPartNL("N. %1 distinte generate%0%0Partite selezionate: %2 su un totale di %3")
      this.w_oPart.AddParam(ALLTRIM(STR(this.w_SCRIVO)))     
      this.w_oPart.AddParam(ALLTRIM(STR(this.w_SPAR)))     
      this.w_oPart.AddParam(ALLTRIM(STR(this.w_TPAR)))     
      this.w_oPart = this.w_oMess.AddMsgPart("%1%0Importo totale in distinta: %2%0Importo totale residuo: %3%0%1%0%0Confermi la presente elaborazione?")
      this.w_oPart.AddParam(REPL("_",50))     
      this.w_oPart.AddParam(ALLTRIM(STR(this.w_TOTAA,18,this.oParentObject.w_DECTOT)))     
      this.w_oPart.AddParam(ALLTRIM(STR(this.oParentObject.w_TOTRES,18,this.oParentObject.w_DECTOT)))     
      if NOT this.w_oMess.ah_YesNo()
        * --- Azzera tutto
        * --- Raise
        i_Error=ah_Msgformat("Operazione annullata")
        return
      endif
    else
      if this.oParentObject.w_FLRAGG="S"
        this.w_TROPPO = .T.
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se la partita in esame � una nota di credito raggruppata allora
    *     cambio il segno. Identifico una nota di credito se il segno
    *     della partita � contrario al segno generale della distinta..
    if (this.oParentObject.w_TIPSCA="C" And Segno="A") Or (this.oParentObject.w_TIPSCA="F" And Segno="D")
      this.w_PTTOTIMP = -TOTIMP
    else
      this.w_PTTOTIMP = TOTIMP
    endif
    this.w_CHIAVE = tipcon+ codcon
    * --- Se il totale distinta supera l'importo proposto sulla banca la scadenza
    *     non � considerata.
    *     Se attivo check raggruppata, non occorre verificare se le partite
    *     superano il totale proposto sul conto banca (w_PARRES nella SCAN 
    *     svolge questo test)
    if this.w_TOTA+this.w_PTTOTIMP > this.w_PROP And this.oParentObject.w_FLRAGG<>"S"
      * --- Raggiunto Max Consentito legge la Partita Successiva
      this.w_TROPPO = .T.
      * --- Se il conto � lo stesso incrementa il contatore per il messaggio 
      if this.w_OCHIAVE=this.w_CHIAVE OR this.w_OCHIAVE="@@@@@%%%%%"
        this.w_CONTA = this.w_CONTA +1
      endif
    else
      this.w_OCHIAVE = this.w_CHIAVE
      this.w_SPAR = this.w_SPAR + 1
      this.w_TOTA = this.w_TOTA + this.w_PTTOTIMP
      this.w_TOTDIS = this.w_TOTDIS + this.w_PTTOTIMP
      this.w_EFFETTO = 0
      if Not this.w_INIZ
        * --- Nuovo Progressivo Distinte
        this.w_DINUMDIS = SPACE(10)
        this.w_DINUMERO = 0
        this.w_DI__ANNO = STR(YEAR(this.oParentObject.w_DATDIS),4,0)
        this.w_CPROWNUM = 0
        * --- Carica Nuova Distinta
        i_Conn=i_TableProp[this.DIS_TINT_IDX, 3]
        cp_NextTableProg(this, i_Conn, "PRDIS", "i_codazi,w_DINUMDIS")
        cp_NextTableProg(this, i_Conn, "PNDIS", "i_codazi,w_DI__ANNO,w_DINUMERO")
        * --- Carica Nuova Distinta
        * --- Try
        local bErr_0505E050
        bErr_0505E050=bTrsErr
        this.Try_0505E050()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Raise
          i_Error=ah_Msgformat("Impossibile creare nuova distinta")
          return
        endif
        bTrsErr=bTrsErr or bErr_0505E050
        * --- End
        this.w_INIZ = .T.
        this.w_SCRIVO = this.w_SCRIVO+1
        SELECT T1
      endif
      * --- Assegna il Numero Distinta alla Partita (e Banca di Presentazione)
      this.w_PTSERIAL = this.w_DINUMDIS
      this.w_PTROWORD = -2
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      this.w_PTDATSCA = CP_TODATE(DATSCA)
      this.w_PTNUMPAR = NVL(NUMPAR, SPACE(31))
      this.w_PTTIPCON = TIPCON
      this.w_PTCODCON = CODCON
      this.w_PTCODVAL = CODVAL
      this.w_PTCODAGE = NVL(CODAGE,SPACE(5))
      this.w_PT_SEGNO = IIF(this.oParentObject.w_TIPSCA="C", "A", "D")
      if this.w_PTTOTIMP<0
        this.w_PT_SEGNO = IIF(this.w_PT_SEGNO="D", "A", "D")
      endif
      this.w_PTTOTIMP = Abs( this.w_PTTOTIMP )
      this.w_PTMODPAG = NVL(MODPAG, " ")
      this.w_PTBANNOS = NVL(BANNOS, " ")
      this.w_PTBANAPP = NVL(BANAPP, " ")
      this.w_PTCAOVAL = NVL(CAOVAL, 0)
      this.w_PTCAOAPE = NVL(CAOAPE, 0)
      this.w_PTDATAPE = CP_TODATE(DATAPE)
      this.w_PTNUMDOC = NVL(NUMDOC, 0)
      this.w_PTALFDOC = NVL(ALFDOC, Space(10))
      this.w_PTDATDOC = CP_TODATE(DATDOC)
      this.w_PTIMPDOC = NVL(IMPDOC, 0)
      this.w_PTFLIMPE = IIF(EMPTY(this.oParentObject.w_CODCAU) AND g_COGE="S", "DI", "  ")
      this.w_PTFLINDI = IIF(EMPTY(NVL(FLINDI, 0)), " ", "S")
      this.w_PTFLRAGG = NVL(FLRAGG," ")
      this.w_PTFLVABD = NVL(FLVABD," ")
      this.w_PTDESRIG = IIF(ALLTRIM(this.oParentObject.w_DICAUBON)$"48000-27000", " ",NVL(DESRIG, " "))
      this.w_PTNUMCOR = NVL(PTNUMCOR,space(25))
      this.w_OSERIAL = PTSERIAL
      this.w_OROWORD = PTROWORD
      this.w_OROWNUM = CPROWNUM
      this.w_PTDATVAL = IIF(this.oParentObject.w_DATEFF="S",CP_TODATE(DATSCA),CP_TODATE(this.oParentObject.w_DATVAL))
      ah_Msg("Aggiornamento scadenza del: %1",.T.,.F.,.F., DTOC(this.w_PTDATSCA) )
      * --- Creo la partita legata alla distinta (PTROWORD=-2)
      * --- Insert into PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PTFLSOSP"+",PTFLCRSA"+",PTNUMEFF"+",PTNUMDIS"+",CPROWNUM"+",PTDATREG"+",PTNUMRIF"+",PTORDRIF"+",PTSERRIF"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTCODAGE"+",PTCODCON"+",PTCODVAL"+",PTDATAPE"+",PTDATDOC"+",PTDATSCA"+",PTDESRIG"+",PTFLIMPE"+",PTFLINDI"+",PTFLRAGG"+",PTFLVABD"+",PTIMPDOC"+",PTMODPAG"+",PTNUMCOR"+",PTNUMDOC"+",PTNUMPAR"+",PTROWORD"+",PTSERIAL"+",PTTIPCON"+",PTTOTIMP"+",PTDATVAL"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
        +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLCRSA');
        +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTNUMEFF');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATDIS),'PAR_TITE','PTDATREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OROWNUM),'PAR_TITE','PTNUMRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OROWORD),'PAR_TITE','PTORDRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'PAR_TITE','PTSERRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLIMPE),'PAR_TITE','PTFLIMPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLINDI),'PAR_TITE','PTFLINDI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLRAGG),'PAR_TITE','PTFLRAGG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'PAR_TITE','PTROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'PAR_TITE','PTSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATVAL),'PAR_TITE','PTDATVAL');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PTFLSOSP'," ",'PTFLCRSA',"S",'PTNUMEFF',0,'PTNUMDIS',SPACE(10),'CPROWNUM',this.w_CPROWNUM,'PTDATREG',this.oParentObject.w_DATDIS,'PTNUMRIF',this.w_OROWNUM,'PTORDRIF',this.w_OROWORD,'PTSERRIF',this.w_OSERIAL,'PT_SEGNO',this.w_PT_SEGNO,'PTALFDOC',this.w_PTALFDOC,'PTBANAPP',this.w_PTBANAPP)
        insert into (i_cTable) (PTFLSOSP,PTFLCRSA,PTNUMEFF,PTNUMDIS,CPROWNUM,PTDATREG,PTNUMRIF,PTORDRIF,PTSERRIF,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTCODAGE,PTCODCON,PTCODVAL,PTDATAPE,PTDATDOC,PTDATSCA,PTDESRIG,PTFLIMPE,PTFLINDI,PTFLRAGG,PTFLVABD,PTIMPDOC,PTMODPAG,PTNUMCOR,PTNUMDOC,PTNUMPAR,PTROWORD,PTSERIAL,PTTIPCON,PTTOTIMP,PTDATVAL &i_ccchkf. );
           values (;
             " ";
             ,"S";
             ,0;
             ,SPACE(10);
             ,this.w_CPROWNUM;
             ,this.oParentObject.w_DATDIS;
             ,this.w_OROWNUM;
             ,this.w_OROWORD;
             ,this.w_OSERIAL;
             ,this.w_PT_SEGNO;
             ,this.w_PTALFDOC;
             ,this.w_PTBANAPP;
             ,this.w_PTBANNOS;
             ,this.w_PTCAOAPE;
             ,this.w_PTCAOVAL;
             ,this.w_PTCODAGE;
             ,this.w_PTCODCON;
             ,this.w_PTCODVAL;
             ,this.w_PTDATAPE;
             ,this.w_PTDATDOC;
             ,this.w_PTDATSCA;
             ,this.w_PTDESRIG;
             ,this.w_PTFLIMPE;
             ,this.w_PTFLINDI;
             ,this.w_PTFLRAGG;
             ,this.w_PTFLVABD;
             ,this.w_PTIMPDOC;
             ,this.w_PTMODPAG;
             ,this.w_PTNUMCOR;
             ,this.w_PTNUMDOC;
             ,this.w_PTNUMPAR;
             ,this.w_PTROWORD;
             ,this.w_PTSERIAL;
             ,this.w_PTTIPCON;
             ,this.w_PTTOTIMP;
             ,this.w_PTDATVAL;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore in inserimento partite'
        return
      endif
      * --- Per non rendere piu' selezionabile la partita ai successivi passaggi
       
 SELECT T1 
 REPLACE TOTIMP WITH 0
    endif
  endproc
  proc Try_0505E050()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DIS_TINT
    i_nConn=i_TableProp[this.DIS_TINT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DIS_TINT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DINUMDIS"+",DIDATDIS"+",DI__ANNO"+",DINUMERO"+",DIDATVAL"+",DICODESE"+",DITIPDIS"+",DITIPSCA"+",DIDESCRI"+",DICODVAL"+",DICAOVAL"+",DISCAINI"+",DISCAFIN"+",DISCOTRA"+",DIBANRIF"+",DICAURIF"+",DICOSCOM"+",DIFLDEFI"+",DIFLAVVI"+",DIDATAVV"+",DICODCAU"+",DIRIFCON"+",DICAUBON"+",DIDATCON"+",DITIPMAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_DINUMDIS),'DIS_TINT','DINUMDIS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATDIS),'DIS_TINT','DIDATDIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DI__ANNO),'DIS_TINT','DI__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DINUMERO),'DIS_TINT','DINUMERO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATVAL),'DIS_TINT','DIDATVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'DIS_TINT','DICODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPDIS),'DIS_TINT','DITIPDIS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPSCA),'DIS_TINT','DITIPSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESCRI),'DIS_TINT','DIDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODVAL),'DIS_TINT','DICODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CAOVAL),'DIS_TINT','DICAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCAINI),'DIS_TINT','DISCAINI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCAFIN),'DIS_TINT','DISCAFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCOTRA),'DIS_TINT','DISCOTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMC),'DIS_TINT','DIBANRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CAURIF),'DIS_TINT','DICAURIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DICOSCOM),'DIS_TINT','DICOSCOM');
      +","+cp_NullLink(cp_ToStrODBC("N"),'DIS_TINT','DIFLDEFI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FLAVVI),'DIS_TINT','DIFLAVVI');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DIS_TINT','DIDATAVV');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCAU),'DIS_TINT','DICODCAU');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DIS_TINT','DIRIFCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DICAUBON),'DIS_TINT','DICAUBON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATVAL),'DIS_TINT','DIDATCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DITIPMAN),'DIS_TINT','DITIPMAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DINUMDIS',this.w_DINUMDIS,'DIDATDIS',this.oParentObject.w_DATDIS,'DI__ANNO',this.w_DI__ANNO,'DINUMERO',this.w_DINUMERO,'DIDATVAL',this.oParentObject.w_DATVAL,'DICODESE',this.oParentObject.w_CODESE,'DITIPDIS',this.oParentObject.w_TIPDIS,'DITIPSCA',this.oParentObject.w_TIPSCA,'DIDESCRI',this.oParentObject.w_DESCRI,'DICODVAL',this.oParentObject.w_CODVAL,'DICAOVAL',this.oParentObject.w_CAOVAL,'DISCAINI',this.oParentObject.w_SCAINI)
      insert into (i_cTable) (DINUMDIS,DIDATDIS,DI__ANNO,DINUMERO,DIDATVAL,DICODESE,DITIPDIS,DITIPSCA,DIDESCRI,DICODVAL,DICAOVAL,DISCAINI,DISCAFIN,DISCOTRA,DIBANRIF,DICAURIF,DICOSCOM,DIFLDEFI,DIFLAVVI,DIDATAVV,DICODCAU,DIRIFCON,DICAUBON,DIDATCON,DITIPMAN &i_ccchkf. );
         values (;
           this.w_DINUMDIS;
           ,this.oParentObject.w_DATDIS;
           ,this.w_DI__ANNO;
           ,this.w_DINUMERO;
           ,this.oParentObject.w_DATVAL;
           ,this.oParentObject.w_CODESE;
           ,this.oParentObject.w_TIPDIS;
           ,this.oParentObject.w_TIPSCA;
           ,this.oParentObject.w_DESCRI;
           ,this.oParentObject.w_CODVAL;
           ,this.oParentObject.w_CAOVAL;
           ,this.oParentObject.w_SCAINI;
           ,this.oParentObject.w_SCAFIN;
           ,this.oParentObject.w_SCOTRA;
           ,this.w_NUMC;
           ,this.oParentObject.w_CAURIF;
           ,this.w_DICOSCOM;
           ,"N";
           ,this.oParentObject.w_FLAVVI;
           ,cp_CharToDate("  -  -  ");
           ,this.oParentObject.w_CODCAU;
           ,SPACE(10);
           ,this.oParentObject.w_DICAUBON;
           ,this.oParentObject.w_DATVAL;
           ,this.oParentObject.w_DITIPMAN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento distinte'
      return
    endif
    this.w_NUMERO = this.w_NUMERO + 1
    this.w_EFFETTO = this.w_EFFETTO+1
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DIS_TINT'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='BAN_CONTI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
