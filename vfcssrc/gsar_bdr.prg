* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bdr                                                        *
*              Divisione ragione sociale                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-21                                                      *
* Last revis.: 2008-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pGest
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bdr",oParentObject,m.pGest)
return(i_retval)

define class tgsar_bdr as StdBatch
  * --- Local variables
  pGest = space(1)
  COGNOME = space(40)
  NOME = space(40)
  TMP_RAGSOC = space(40)
  PosizSpazio = 0
  w_Ok = .f.
  w_StringaMess = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                            Divide la ragione sociale in cognome e nome
    * --- pGest = N :  Nominativi
    *                    C  :  Clienti
    *                    F  :  Fornitori
    *                    S :   Soggetti esterni pratica
    *                    A :   Controlli antiriciclaggio in GSAR_ANO
    if this.pGest="A"
      * --- Controlli per antiriciclaggio se attivo il relativo modulo (solo in Alter Ego Top)
      if this.oParentObject.w_RESCHK # -1 AND g_ANTI="S" AND !EMPTY(this.oParentObject.w_NOTIPDOC)
        this.w_Ok = .T.
        this.w_StringaMess = ah_Msgformat("Mancano i seguenti estremi del documento di identificazione:%0")
        if EMPTY(this.oParentObject.w_NONUMDOC)
          this.w_StringaMess = this.w_StringaMess+ah_Msgformat("Numero%0")
          this.w_Ok = .F.
        endif
        if EMPTY(this.oParentObject.w_NODATRIL)
          this.w_StringaMess = this.w_StringaMess+ah_Msgformat("Data rilascio%0")
          this.w_Ok = .F.
        endif
        if EMPTY(this.oParentObject.w_NOAUTLOC)
          this.w_StringaMess = this.w_StringaMess+ah_Msgformat("Autorit� e localit� di rilascio%0")
          this.w_Ok = .F.
        endif
        if !this.w_OK
          this.w_StringaMess = this.w_StringaMess+ah_Msgformat("Procedere con il salvataggio?")
          this.w_OK = ah_YesNo("%1", "", this.w_StringaMess)
          if !this.w_OK
            this.oParentObject.w_RESCHK = -1
            i_retcode = 'stop'
            return
          endif
        endif
      endif
    else
      if g_APPLICATION = "ADHOC REVOLUTION"
        do case
          case this.pGest = "N" AND this.oParentObject.w_NOSOGGET="PF"
            * --- Nominativi
            this.TMP_RAGSOC = this.oParentObject.w_NODESCRI
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_NOCOGNOM = this.COGNOME
            this.oParentObject.w_NO__NOME = this.NOME
          case this.pGest $ "CF" AND this.oParentObject.w_ANPERFIS="S"
            * --- Clienti/Fornitori
            this.TMP_RAGSOC = this.oParentObject.w_ANDESCRI
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_ANCOGNOM = this.COGNOME
            this.oParentObject.w_AN__NOME = this.NOME
          case this.pGest = "S" AND this.oParentObject.w_SOGGET="PF"
            * --- Soggetti esterni pratica
            this.TMP_RAGSOC = this.oParentObject.w_DESNOM
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_COGNOM = this.COGNOME
            this.oParentObject.w_NOME = this.NOME
        endcase
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.TMP_RAGSOC = ALLTRIM(this.TMP_RAGSOC)
    this.PosizSpazio = AT(space(1), this.TMP_RAGSOC)
    this.COGNOME = IIF(this.PosizSpazio<>0, SUBSTR(this.TMP_RAGSOC, 1, this.PosizSpazio - 1), this.TMP_RAGSOC)
    this.NOME = IIF(this.PosizSpazio<>0, ALLTRIM(SUBSTR(this.TMP_RAGSOC, this.PosizSpazio + 1)), SPACE(50))
  endproc


  proc Init(oParentObject,pGest)
    this.pGest=pGest
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pGest"
endproc
