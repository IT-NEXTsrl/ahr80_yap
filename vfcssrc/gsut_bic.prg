* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bic                                                        *
*              Installazione componenti                                        *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-08-27                                                      *
* Last revis.: 2015-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bic",oParentObject)
return(i_retval)

define class tgsut_bic as StdBatch
  * --- Local variables
  w_SYSPATH = space(254)
  w_LibName = space(100)
  w_RegistraLib = .f.
  w_ProcessaLib = .f.
  w_LicenzaLib = space(254)
  w_nComponents = 0
  w_Counter = 0
  w_ShellExec = 0
  w_FILEORIG = space(10)
  w_FILEDEST = space(10)
  w_RITARDO = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Registrazione DLL
    if !this.oParentObject.w_Agenda="S" AND !this.oParentObject.w_PUBLISH="S" AND !this.oParentObject.w_BCPDF417="S" AND !this.oParentObject.w_BCSENDTO="S" and !this.oParentObject.w_LEGGIPEC="S"
      ah_ErrorMsg("Non sono stati selezionati i componenti da installare")
      i_retcode = 'stop'
      return
    endif
    * --- Gestione errori: dobbiamo gestire L_OldErr come statement perch� la ON ERROR segnala errore con l'espansione di macro
    PRIVATE L_OldErr, L_Err
    L_OldErr = ON("ERROR")
    L_Err = .T.
    * --- =====================================================
    * --- Verifico se sono su un sistema a 64bit
    if Directory(GETENV("WINDIR")+ "\SysWOW64\")
      * --- Ricavo il path della cartella SYSWOW64
      this.w_SYSPATH=ADDBS( ADDBS(GETENV("WINDIR"))+ "SysWOW64")
    else
      * --- Ricavo il path della cartella SYSTEM32
      this.w_SYSPATH=ADDBS( ADDBS(GETENV("WINDIR"))+ "SYSTEM32")
    endif
    * --- =====================================================
    * --- Colonne array:
    *     1=nome libreria
    *     2=richiesta registrazione regsvr32
    *     3=eventuale file di licenza da copiare
    *     4=da processare
    Dimension aComponents(1,4)
    * --- Definizione componenti
    this.w_nComponents = 0
    * --- Ocx per AGENDA
    *     ctCalendar, ctContact, ctDate, ctDropDate, ctList, ctMDay, ctMonth, ctSchedule, ctYear
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="ctCalendar.ocx"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_AGENDA="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="ctContact.ocx"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_AGENDA="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="ctDate.ocx"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_AGENDA="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="ctDropDate.ocx"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_AGENDA="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="ctList.ocx"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_AGENDA="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="ctMDay.ocx"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_AGENDA="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="ctMonth.ocx"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_AGENDA="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="ctSchedule.ocx"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)="ctS8License.dpf"
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_AGENDA="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="ctYear.ocx"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_AGENDA="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="CCubeX40.ocx"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_PUBLISH="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="IBPublisher20.ocx"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_PUBLISH="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="InfoReader.ocx"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_PUBLISH="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="PDF417Font.dll"
    aComponents(this.w_nComponents,2)=.F.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= .T.
    * --- dynwrap.dll
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="dynwrap.dll"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_BCSENDTO="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="PDF.dll"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (this.oParentObject.w_PUBLISH="S")
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,4)
    aComponents(this.w_nComponents,1)="OpenPop.dll"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= (Isalt() and this.oParentObject.w_LEGGIPEC="S")
    * --- ====== Registrazione componenti ==================================================================
    this.w_Counter = 1
    do while this.w_Counter<=this.w_nComponents
      * --- Libreria corrente
      this.w_LibName = aComponents(this.w_Counter,1)
      this.w_RegistraLib = aComponents(this.w_Counter,2)
      this.w_LicenzaLib = aComponents(this.w_Counter,3)
      this.w_ProcessaLib = aComponents(this.w_Counter,4)
      if this.w_ProcessaLib
        * --- Se il file � gi� presente, cancella, deregistra, rimette e registra (se necessario)
        if cp_fileexist(this.w_SysPath+this.w_LibName)
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Not(cp_fileexist(this.w_SysPath+this.w_LibName))
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_RITARDO = Seconds() + 2
        wait window "Installazione componenti in corso . . ." nowait noclear
        do while seconds() <= this.w_RITARDO
          * --- Aspetto
        enddo
        wait clear
      endif
      * --- Aggiorna contatore
      this.w_Counter = this.w_Counter+1
    enddo
    ah_Msg("Installazione librerie e componenti DLL completata",.T.)
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Ah_MsgFormat("Disinstallo %1",this.w_LibName)
    * --- Gestione errori
    L_Err = .F.
    ON ERROR L_Err=.T.
    this.w_FILEDEST = this.w_SYSPATH+this.w_LibName
    * --- Eventuale deregistrazione libreria
    if this.w_RegistraLib
      this.w_ShellExec = Ah_ShellEx("", "REGSVR32.exe", "/S /U "+this.w_FILEDEST)
    endif
    * --- Cancellazione fisica libreria
    if cp_fileexist(this.w_FILEDEST)
      ON ERROR L_Err=.T.
      DELETE FILE (this.w_FILEDEST) RECYCLE
      if Not(Empty(this.w_LicenzaLib)) and cp_fileexist(this.w_SYSPATH+this.w_LicenzaLib) and !L_Err
        this.w_FILEDEST = this.w_SYSPATH+this.w_LicenzaLib
        DELETE FILE (this.w_FILEDEST) RECYCLE
      endif
    endif
    ON ERROR &L_OldErr
    if L_Err
      ah_ErrorMsg("Libreria %1 in uso, chiudere la sessione e ripetere l'installazione componenti",,"",this.w_LibName)
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_ADHOCONE
      this.w_FILEORIG = "..\..\DLL\"+this.w_LibName
    else
      this.w_FILEORIG = "..\DLL\"+this.w_LibName
    endif
    if cp_fileexist(this.w_FILEORIG)
      Ah_MsgFormat("Installo %1",this.w_LibName)
      * --- Gestione errori
      L_Err = .F.
      ON ERROR L_Err=.T.
      * --- Copia libreria
      this.w_FILEDEST = this.w_SYSPATH+this.w_LibName
      COPY FILE (this.w_FILEORIG) TO (this.w_FILEDEST)
      * --- Copia eventuale file di licenza
      if Not(L_Err) and Not(Empty(this.w_LicenzaLib))
        this.w_FILEORIG = "..\DLL\"+this.w_LicenzaLib
        if cp_fileexist(this.w_FILEORIG)
          this.w_FILEDEST = this.w_SYSPATH+this.w_LicenzaLib
          COPY FILE (this.w_FILEORIG) TO (this.w_FILEDEST)
        endif
      endif
      ON ERROR &L_OldErr
      * --- Gestione errori
      if L_Err
        ah_ErrorMsg("Libreria %1 in uso, chiudere la sessione e ripetere l'installazione componenti",,"",this.w_LibName)
        i_retcode = 'stop'
        return
      endif
      if this.w_RegistraLib
        this.w_ShellExec = Ah_ShellEx("", "REGSVR32.exe", "/S "+this.w_SYSPATH+this.w_LibName)
      endif
    else
      ah_ErrorMsg("Libreria %1 non trovata in cartella DLL, operazione sospesa",,"",this.w_LibName)
      i_retcode = 'stop'
      return
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
