* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bvl                                                        *
*              Variazione listino prezzi                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_441]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-05                                                      *
* Last revis.: 2010-01-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bvl",oParentObject)
return(i_retval)

define class tgsar_bvl as StdBatch
  * --- Local variables
  w_CRITER = space(2)
  w_LISTRIFE = space(5)
  w_DATARIFE = ctod("  /  /  ")
  w_CODMAG = space(5)
  w_ESERCI = space(4)
  w_INNUMINV = 0
  w_RICALC = space(1)
  w_ORIGSCA = space(1)
  w_DATLISIN = ctod("  /  /  ")
  w_DATLISFI = ctod("  /  /  ")
  w_LISTINO = space(5)
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_RICPE1 = 0
  w_LORNET = space(1)
  w_LORNET1 = space(1)
  w_CAMBIO = 0
  w_CAMBIO1 = 0
  w_CAMBIO2 = 0
  w_DECTOT = 0
  w_RICVA1 = 0
  w_ARROT1 = 0
  w_VALOR1IN = 0
  w_ARROT2 = 0
  w_VALOR2IN = 0
  w_ARROT3 = 0
  w_VALOR3IN = 0
  w_ARROT4 = 0
  w_CREALIS = space(1)
  w_AGSCO = space(1)
  w_MOLTIP = 0
  w_MOLTI2 = 0
  w_TIPUMI = 0
  NoName = space(0)
  w_CODART = space(20)
  w_MESS = space(10)
  w_LISCONT1 = 0
  w_LISCONT2 = 0
  w_LISCONT3 = 0
  w_LISCONT4 = 0
  w_OK = .f.
  w_CODICE = space(20)
  * --- WorkFile variables
  LIS_SCAG_idx=0
  LIS_TINI_idx=0
  RIC_PREZ_idx=0
  TMPART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dalla maschera Variazione Listino Prezzi (GSMA_KVL)
    * --- Inizializzazione variabili globali.
    this.w_CRITER = this.oparentobject.w_CRITER
    this.w_LISTRIFE = this.oparentobject.w_LISTRIFE
    this.w_DATARIFE = this.oparentobject.w_DATARIFE
    this.w_CODMAG = this.oparentobject.w_CODMAG
    this.w_ESERCI = this.oparentobject.w_ESERCI
    this.w_INNUMINV = this.oparentobject.w_INNUMINV
    this.w_RICALC = this.oparentobject.w_RICALC
    this.w_ORIGSCA = this.oparentobject.w_ORIGSCA
    this.w_DATLISIN = this.oparentobject.w_DATLISIN
    this.w_DATLISFI = this.oparentobject.w_DATLISFI
    this.w_LISTINO = this.oparentobject.w_LISTINO
    this.w_SCONT1 = this.oparentobject.w_SCONT1
    this.w_SCONT2 = this.oparentobject.w_SCONT2
    this.w_SCONT3 = this.oparentobject.w_SCONT3
    this.w_SCONT4 = this.oparentobject.w_SCONT4
    this.w_RICPE1 = this.oparentobject.w_RICPE1
    this.w_LORNET = this.oparentobject.w_LORNET
    this.w_LORNET1 = this.oparentobject.w_LORNET1
    this.w_CAMBIO = this.oparentobject.w_CAMBIO
    this.w_CAMBIO1 = this.oparentobject.w_CAMBIO1
    this.w_CAMBIO2 = this.oparentobject.w_CAMBIO2
    this.w_DECTOT = this.oparentobject.w_DECTOT
    this.w_RICVA1 = this.oparentobject.w_RICVA1
    this.w_ARROT1 = this.oparentobject.w_ARROT1
    this.w_ARROT2 = this.oparentobject.w_ARROT2
    this.w_ARROT3 = this.oparentobject.w_ARROT3
    this.w_ARROT4 = this.oparentobject.w_ARROT4
    this.w_VALOR1IN = this.oparentobject.w_VALORIN
    this.w_VALOR2IN = this.oparentobject.w_VALOR2IN
    this.w_VALOR3IN = this.oparentobject.w_VALOR3IN
    this.w_CREALIS = this.oparentobject.w_CREALIS
    this.w_AGSCO = this.oparentobject.w_AGSCO
    this.w_MOLTIP = this.oparentobject.w_MOLTIP
    this.w_MOLTI2 = this.oparentobject.w_MOLTI2
    this.w_TIPUMI = this.oparentobject.w_TIPUMI
    * --- Inizializzazione variabili Locali
    this.w_LISCONT1 = this.w_SCONT1
    this.w_LISCONT2 = this.w_SCONT2
    this.w_LISCONT3 = this.w_SCONT3
    this.w_LISCONT4 = this.w_SCONT4
    * --- Controllo campi obbligatori
    do case
      case EMPTY(this.w_LISTINO)
        ah_ErrorMsg("Listino da aggiornare non selezionato")
        i_retcode = 'stop'
        return
      case EMPTY(this.w_DATLISIN)
        ah_ErrorMsg("La data di inizio validit� del listino non � stata selezionata")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_DATLISFI)
        ah_ErrorMsg("La data di fine validit� del listino non � stata selezionata")
        i_retcode = 'stop'
        return
      case LEFT(this.w_CRITER,1)="I" AND EMPTY(this.w_INNUMINV)
        ah_ErrorMsg("L'inventario di riferimento non � stato selezionato")
        i_retcode = 'stop'
        return
      case LEFT(this.w_CRITER,1)="U" AND EMPTY(this.w_CODMAG)
        ah_ErrorMsg("Il magazzino di riferimento non � stato selezionato")
        i_retcode = 'stop'
        return
    endcase
    if EMPTY(this.oParentObject.w_CODARTIN) AND EMPTY(this.oParentObject.w_CODARTFI) AND EMPTY(this.oParentObject.w_CODIVA) AND EMPTY(this.oParentObject.w_GRUPMERC) AND EMPTY(this.oParentObject.w_CATOMOGE) AND EMPTY(this.oParentObject.w_MARCA) AND EMPTY(this.oParentObject.w_FAMIGLIA) AND EMPTY(this.oParentObject.w_FORABIT) AND EMPTY(this.oParentObject.w_FORULTC) AND EMPTY(this.oParentObject.w_CLARICA)
      if ! ah_Yesno("Attenzione, tutti i prezzi del listino verranno variati, si desidera confermare?")
        i_retcode = 'stop'
        return
      endif
    endif
    if LEFT(this.w_CRITER,1)="L"
      * --- Create temporary table TMPART
      i_nIdx=cp_AddTableDef('TMPART') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('GSAR_BVL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPART_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table TMPART
      i_nIdx=cp_AddTableDef('TMPART') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('GSAR1BVL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPART_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Faccio una read fittizia per avere i_rows
    * --- Read from TMPART
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPART_idx,2],.t.,this.TMPART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CODART"+;
        " from "+i_cTable+" TMPART where ";
            +"1 = "+cp_ToStrODBC(1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CODART;
        from (i_cTable) where;
            1 = 1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODICE = NVL(cp_ToDate(_read_.CODART),cp_NullValue(_read_.CODART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_rows>0
      GSAR_BAG(this,"V")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if !this.w_OK 
        * --- Se non ha aggiornato /inserito niente lo segnalo
        ah_ErrorMsg("Per le selezioni impostate non esistono listini-prezzi da aggiornare")
      endif
    else
      ah_ErrorMsg("Per le selezioni impostate non esistono listini-prezzi da aggiornare")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='LIS_SCAG'
    this.cWorkTables[2]='LIS_TINI'
    this.cWorkTables[3]='RIC_PREZ'
    this.cWorkTables[4]='*TMPART'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
