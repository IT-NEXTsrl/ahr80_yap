* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bzp                                                        *
*              Prima nota da schede analitica                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-21                                                      *
* Last revis.: 2000-02-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL,pTIPMOV,pALFA,pNUMERO,pDATA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bzp",oParentObject,m.pSERIAL,m.pTIPMOV,m.pALFA,m.pNUMERO,m.pDATA)
return(i_retval)

define class tgsca_bzp as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  pTIPMOV = space(10)
  pALFA = space(1)
  pNUMERO = 0
  pDATA = ctod("  /  /  ")
  w_PNOTA = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lancia la Prima Nota o la Manutenzione Analitica dalla visualizzazione delle schede di Analitica (da GSCA_SZM)
    * --- chiave della Prima Nota
    * --- Questo oggetto sar� definito come Prima Nota / Manutenzione
    do case
      case g_COGE="S" AND LEFT(this.pTIPMOV,5)="(P.N."
        GSAR_BZP(this,this.pSERIAL)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case LEFT(this.pTIPMOV,5)="(Man."
        this.w_PNOTA = GSCA_ACM()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PNOTA.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PNOTA.w_CMCODICE = this.pSERIAL
        * --- creo il curosre delle solo chiavi
        this.w_PNOTA.QueryKeySet("CMCODICE='"+this.pSERIAL+ "'","")     
        * --- mi metto in interrogazione
        this.w_PNOTA.LoadRec()     
      case g_ACQU="S" AND LEFT(this.pTIPMOV,5)="(D.A."
        * --- Documento di Acquisto
        GSAR_BZM(this,this.pSERIAL, -20)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case g_VEND="S" AND LEFT(this.pTIPMOV,5)="(D.V."
        * --- Documento di Vendita
        GSAR_BZM(this,this.pSERIAL, -20)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case g_ACQU<>"S" AND LEFT(this.pTIPMOV,5)="(D.A."
        * --- Documento di Acquisto generati dal ciclo vendite
        GSAR_BZM(this,this.pSERIAL, -20)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pSERIAL,pTIPMOV,pALFA,pNUMERO,pDATA)
    this.pSERIAL=pSERIAL
    this.pTIPMOV=pTIPMOV
    this.pALFA=pALFA
    this.pNUMERO=pNUMERO
    this.pDATA=pDATA
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL,pTIPMOV,pALFA,pNUMERO,pDATA"
endproc
