* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_asc                                                        *
*              Scadenze diverse                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_197]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-27                                                      *
* Last revis.: 2014-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsar_asc
* --- Il parametro pOrigine pu� valere 'S' o 'D'
* --- 'S' se Scadenze Diverse, 'D' se Documenti Confermati
PARAMETER pOrigine
* --- Fine Area Manuale
return(createobject("tgsar_asc"))

* --- Class definition
define class tgsar_asc as StdForm
  Top    = 8
  Left   = 3

  * --- Standard Properties
  Width  = 802
  Height = 441+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-30"
  HelpContextID=109672297
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=56

  * --- Constant Properties
  SCA_VARI_IDX = 0
  CONTI_IDX = 0
  PAR_TITE_IDX = 0
  VALUTE_IDX = 0
  PAG_AMEN_IDX = 0
  BAN_CHE_IDX = 0
  COC_MAST_IDX = 0
  AGENTI_IDX = 0
  AZIENDA_IDX = 0
  cFile = "SCA_VARI"
  cKeySelect = "SCCODICE,SCCODSEC"
  cKeyWhere  = "SCCODICE=this.w_SCCODICE and SCCODSEC=this.w_SCCODSEC"
  cKeyWhereODBC = '"SCCODICE="+cp_ToStrODBC(this.w_SCCODICE)';
      +'+" and SCCODSEC="+cp_ToStrODBC(this.w_SCCODSEC)';

  cKeyWhereODBCqualified = '"SCA_VARI.SCCODICE="+cp_ToStrODBC(this.w_SCCODICE)';
      +'+" and SCA_VARI.SCCODSEC="+cp_ToStrODBC(this.w_SCCODSEC)';

  cPrg = "gsar_asc"
  cComment = "Scadenze diverse"
  icon = "anag.ico"
  cAutoZoom = 'GSTE0ASC'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SCCODICE = space(10)
  w_ORIGINE = space(1)
  o_ORIGINE = space(1)
  w_SCDATREG = ctod('  /  /  ')
  o_SCDATREG = ctod('  /  /  ')
  w_SCNUMDOC = 0
  w_SCALFDOC = space(10)
  w_SCDATDOC = ctod('  /  /  ')
  o_SCDATDOC = ctod('  /  /  ')
  w_SCDESCRI = space(35)
  w_SCTIPCLF = space(1)
  o_SCTIPCLF = space(1)
  w_PFLCRSA = space(1)
  o_PFLCRSA = space(1)
  w_SCTIPSCA = space(1)
  w_SCCODSEC = 0
  w_CODAZI = space(5)
  w_SCVALNAZ = space(3)
  w_OBTEST = ctod('  /  /  ')
  w_SCDATVAL = ctod('  /  /  ')
  o_SCDATVAL = ctod('  /  /  ')
  w_SCCODCLF = space(15)
  o_SCCODCLF = space(15)
  w_PARTSN = space(1)
  w_RAGSOC = space(40)
  w_VALUTA = space(3)
  w_FLVEBD = space(1)
  w_FLACBD = space(1)
  w_SCFLVABD = space(1)
  w_SCCODPAG = space(5)
  w_DESPAG = space(30)
  w_SCCODAGE = space(5)
  w_DESAGE = space(35)
  w_SCCODBAN = space(10)
  w_DESBAN = space(50)
  w_DESNOS = space(35)
  w_SCCODVAL = space(3)
  o_SCCODVAL = space(3)
  w_SIMVAL = space(5)
  w_DECTOT = 0
  o_DECTOT = 0
  w_CAOVAL = 0
  w_SCCAOVAL = 0
  w_SCIMPSCA = 0
  w_SCFLIMPG = space(1)
  w_CALCPICT = 0
  w_DESAPP = space(35)
  w_DATOBSO = ctod('  /  /  ')
  w_MESCL1 = 0
  w_MESCL2 = 0
  w_GIOSC1 = 0
  w_GIOSC2 = 0
  w_GIOFIS = 0
  w_TIPOPE = space(4)
  w_SCBANNOS = space(15)
  w_DTOBSO = ctod('  /  /  ')
  w_SCRIFDCO = space(10)
  w_AGEOBSO = ctod('  /  /  ')
  w_RESCHK = 0
  w_NUMCOR = space(25)
  w_SERDOC = space(10)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_SCCODICE = this.W_SCCODICE

  * --- Children pointers
  GSTE_MPA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar_asc
  Origine = ''
  
  func getSecuritycode()
    return(lower('GSAR_ASC,'+this.Origine))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SCA_VARI','gsar_asc')
    stdPageFrame::Init()
    *set procedure to GSTE_MPA additive
    with this
      .Pages(1).addobject("oPag","tgsar_ascPag1","gsar_asc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Scadenze")
      .Pages(1).HelpContextID = 30342773
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSTE_MPA
    * --- Area Manuale = Init Page Frame
    * --- gsar_asc
    
    WITH THIS.PARENT
      .Origine = pOrigine
    Do case
    case .Origine = 'S'
        .cComment =CP_TRANSLATE('Scadenze diverse')
    case  .Origine = 'A'
        .cComment =CP_TRANSLATE('Gestione incassi')
    case .Origine = 'D'
       .cComment = CP_TRANSLATE('Documenti confermati')
    endcase
    Endwith
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='PAG_AMEN'
    this.cWorkTables[5]='BAN_CHE'
    this.cWorkTables[6]='COC_MAST'
    this.cWorkTables[7]='AGENTI'
    this.cWorkTables[8]='AZIENDA'
    this.cWorkTables[9]='SCA_VARI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SCA_VARI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SCA_VARI_IDX,3]
  return

  function CreateChildren()
    this.GSTE_MPA = CREATEOBJECT('stdDynamicChild',this,'GSTE_MPA',this.oPgFrm.Page1.oPag.oLinkPC_1_39)
    this.GSTE_MPA.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSTE_MPA)
      this.GSTE_MPA.DestroyChildrenChain()
      this.GSTE_MPA=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_39')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSTE_MPA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSTE_MPA.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSTE_MPA.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSTE_MPA.SetKey(;
            .w_SCCODICE,"PTSERIAL";
            ,.w_SCCODSEC,"PTROWORD";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSTE_MPA.ChangeRow(this.cRowID+'      1',1;
             ,.w_SCCODICE,"PTSERIAL";
             ,.w_SCCODSEC,"PTROWORD";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSTE_MPA)
        i_f=.GSTE_MPA.BuildFilter()
        if !(i_f==.GSTE_MPA.cQueryFilter)
          i_fnidx=.GSTE_MPA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSTE_MPA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSTE_MPA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSTE_MPA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSTE_MPA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SCCODICE = NVL(SCCODICE,space(10))
      .w_SCCODSEC = NVL(SCCODSEC,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    local link_1_27_joined
    link_1_27_joined=.f.
    local link_1_30_joined
    link_1_30_joined=.f.
    local link_1_64_joined
    link_1_64_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SCA_VARI where SCCODICE=KeySet.SCCODICE
    *                            and SCCODSEC=KeySet.SCCODSEC
    *
    i_nConn = i_TableProp[this.SCA_VARI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SCA_VARI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SCA_VARI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SCA_VARI '
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_27_joined=this.AddJoinedLink_1_27(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_30_joined=this.AddJoinedLink_1_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_64_joined=this.AddJoinedLink_1_64(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SCCODICE',this.w_SCCODICE  ,'SCCODSEC',this.w_SCCODSEC  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ORIGINE = this.Origine
        .w_CODAZI = i_codazi
        .w_PARTSN = space(1)
        .w_RAGSOC = space(40)
        .w_VALUTA = space(3)
        .w_FLVEBD = space(1)
        .w_FLACBD = space(1)
        .w_DESPAG = space(30)
        .w_DESAGE = space(35)
        .w_DESBAN = space(50)
        .w_DESNOS = space(35)
        .w_SIMVAL = space(5)
        .w_DECTOT = 0
        .w_CAOVAL = 0
        .w_DESAPP = space(35)
        .w_DATOBSO = ctod("  /  /  ")
        .w_MESCL1 = 0
        .w_MESCL2 = 0
        .w_GIOSC1 = 0
        .w_GIOSC2 = 0
        .w_GIOFIS = 0
        .w_DTOBSO = ctod("  /  /  ")
        .w_AGEOBSO = ctod("  /  /  ")
        .w_RESCHK = 0
        .w_NUMCOR = space(25)
        .w_SERDOC = space(10)
        .w_SCCODICE = NVL(SCCODICE,space(10))
        .op_SCCODICE = .w_SCCODICE
        .w_SCDATREG = NVL(cp_ToDate(SCDATREG),ctod("  /  /  "))
        .w_SCNUMDOC = NVL(SCNUMDOC,0)
        .w_SCALFDOC = NVL(SCALFDOC,space(10))
        .w_SCDATDOC = NVL(cp_ToDate(SCDATDOC),ctod("  /  /  "))
        .w_SCDESCRI = NVL(SCDESCRI,space(35))
        .w_SCTIPCLF = NVL(SCTIPCLF,space(1))
        .w_PFLCRSA = iif(.w_ORIGINE='A','S','C')
        .w_SCTIPSCA = NVL(SCTIPSCA,space(1))
        .w_SCCODSEC = NVL(SCCODSEC,0)
          .link_1_12('Load')
        .w_SCVALNAZ = NVL(SCVALNAZ,space(3))
        .w_OBTEST = .w_SCDATDOC
        .w_SCDATVAL = NVL(cp_ToDate(SCDATVAL),ctod("  /  /  "))
        .w_SCCODCLF = NVL(SCCODCLF,space(15))
          if link_1_16_joined
            this.w_SCCODCLF = NVL(ANCODICE116,NVL(this.w_SCCODCLF,space(15)))
            this.w_RAGSOC = NVL(ANDESCRI116,space(40))
            this.w_SCCODBAN = NVL(ANCODBAN116,space(10))
            this.w_SCCODPAG = NVL(ANCODPAG116,space(5))
            this.w_PARTSN = NVL(ANPARTSN116,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO116),ctod("  /  /  "))
            this.w_MESCL1 = NVL(AN1MESCL116,0)
            this.w_MESCL2 = NVL(AN2MESCL116,0)
            this.w_GIOSC1 = NVL(ANGIOSC1116,0)
            this.w_GIOSC2 = NVL(ANGIOSC2116,0)
            this.w_GIOFIS = NVL(ANGIOFIS116,0)
            this.w_VALUTA = NVL(ANCODVAL116,space(3))
            this.w_SCBANNOS = NVL(ANCODBA2116,space(15))
            this.w_SCCODAGE = NVL(ANCODAG1116,space(5))
            this.w_FLACBD = NVL(ANFLACBD116,space(1))
            this.w_NUMCOR = NVL(ANNUMCOR116,space(25))
          else
          .link_1_16('Load')
          endif
        .w_SCFLVABD = NVL(SCFLVABD,space(1))
        .w_SCCODPAG = NVL(SCCODPAG,space(5))
          if link_1_23_joined
            this.w_SCCODPAG = NVL(PACODICE123,NVL(this.w_SCCODPAG,space(5)))
            this.w_DESPAG = NVL(PADESCRI123,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(PADTOBSO123),ctod("  /  /  "))
          else
          .link_1_23('Load')
          endif
        .w_SCCODAGE = NVL(SCCODAGE,space(5))
          if link_1_25_joined
            this.w_SCCODAGE = NVL(AGCODAGE125,NVL(this.w_SCCODAGE,space(5)))
            this.w_DESAGE = NVL(AGDESAGE125,space(35))
            this.w_AGEOBSO = NVL(cp_ToDate(AGDTOBSO125),ctod("  /  /  "))
          else
          .link_1_25('Load')
          endif
        .w_SCCODBAN = NVL(SCCODBAN,space(10))
          if link_1_27_joined
            this.w_SCCODBAN = NVL(BACODBAN127,NVL(this.w_SCCODBAN,space(10)))
            this.w_DESBAN = NVL(BADESBAN127,space(50))
          else
          .link_1_27('Load')
          endif
        .w_SCCODVAL = NVL(SCCODVAL,space(3))
          if link_1_30_joined
            this.w_SCCODVAL = NVL(VACODVAL130,NVL(this.w_SCCODVAL,space(3)))
            this.w_DESAPP = NVL(VADESVAL130,space(35))
            this.w_DECTOT = NVL(VADECTOT130,0)
            this.w_SIMVAL = NVL(VASIMVAL130,space(5))
            this.w_CAOVAL = NVL(VACAOVAL130,0)
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO130),ctod("  /  /  "))
          else
          .link_1_30('Load')
          endif
        .w_SCCAOVAL = NVL(SCCAOVAL,0)
        .w_SCIMPSCA = NVL(SCIMPSCA,0)
        .w_SCFLIMPG = NVL(SCFLIMPG,space(1))
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_TIPOPE = this.cFunction
        .w_SCBANNOS = NVL(SCBANNOS,space(15))
          if link_1_64_joined
            this.w_SCBANNOS = NVL(BACODBAN164,NVL(this.w_SCBANNOS,space(15)))
            this.w_DESNOS = NVL(BADESCRI164,space(35))
          else
          .link_1_64('Load')
          endif
        .w_SCRIFDCO = NVL(SCRIFDCO,space(10))
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'SCA_VARI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SCCODICE = space(10)
      .w_ORIGINE = space(1)
      .w_SCDATREG = ctod("  /  /  ")
      .w_SCNUMDOC = 0
      .w_SCALFDOC = space(10)
      .w_SCDATDOC = ctod("  /  /  ")
      .w_SCDESCRI = space(35)
      .w_SCTIPCLF = space(1)
      .w_PFLCRSA = space(1)
      .w_SCTIPSCA = space(1)
      .w_SCCODSEC = 0
      .w_CODAZI = space(5)
      .w_SCVALNAZ = space(3)
      .w_OBTEST = ctod("  /  /  ")
      .w_SCDATVAL = ctod("  /  /  ")
      .w_SCCODCLF = space(15)
      .w_PARTSN = space(1)
      .w_RAGSOC = space(40)
      .w_VALUTA = space(3)
      .w_FLVEBD = space(1)
      .w_FLACBD = space(1)
      .w_SCFLVABD = space(1)
      .w_SCCODPAG = space(5)
      .w_DESPAG = space(30)
      .w_SCCODAGE = space(5)
      .w_DESAGE = space(35)
      .w_SCCODBAN = space(10)
      .w_DESBAN = space(50)
      .w_DESNOS = space(35)
      .w_SCCODVAL = space(3)
      .w_SIMVAL = space(5)
      .w_DECTOT = 0
      .w_CAOVAL = 0
      .w_SCCAOVAL = 0
      .w_SCIMPSCA = 0
      .w_SCFLIMPG = space(1)
      .w_CALCPICT = 0
      .w_DESAPP = space(35)
      .w_DATOBSO = ctod("  /  /  ")
      .w_MESCL1 = 0
      .w_MESCL2 = 0
      .w_GIOSC1 = 0
      .w_GIOSC2 = 0
      .w_GIOFIS = 0
      .w_TIPOPE = space(4)
      .w_SCBANNOS = space(15)
      .w_DTOBSO = ctod("  /  /  ")
      .w_SCRIFDCO = space(10)
      .w_AGEOBSO = ctod("  /  /  ")
      .w_RESCHK = 0
      .w_NUMCOR = space(25)
      .w_SERDOC = space(10)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_ORIGINE = this.Origine
        .w_SCDATREG = i_datsys
          .DoRTCalc(4,5,.f.)
        .w_SCDATDOC = iif(.w_ORIGINE='A' AND g_COGE<>'S' AND isalt(),.w_SCDATREG,i_datsys)
          .DoRTCalc(7,7,.f.)
        .w_SCTIPCLF = 'C'
        .w_PFLCRSA = iif(.w_ORIGINE='A','S','C')
        .w_SCTIPSCA = IIF((.w_SCTIPCLF='C' AND .w_PFLCRSA='S') or (.w_SCTIPCLF='F' AND .w_PFLCRSA<>'S') ,'F', IIF((.w_SCTIPCLF='F' AND .w_PFLCRSA='S') or (.w_SCTIPCLF='C' AND .w_PFLCRSA<>'S') ,'C', 'C'))
        .w_SCCODSEC = -1
        .w_CODAZI = i_codazi
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_CODAZI))
          .link_1_12('Full')
          endif
        .w_SCVALNAZ = g_PERVAL
        .w_OBTEST = .w_SCDATDOC
        .w_SCDATVAL = .w_SCDATDOC
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_SCCODCLF))
          .link_1_16('Full')
          endif
          .DoRTCalc(17,21,.f.)
        .w_SCFLVABD = IIF(.w_SCTIPCLF='C', .w_FLVEBD, IIF(.w_SCTIPCLF='F', .w_FLACBD, ' '))
        .DoRTCalc(23,23,.f.)
          if not(empty(.w_SCCODPAG))
          .link_1_23('Full')
          endif
        .DoRTCalc(24,25,.f.)
          if not(empty(.w_SCCODAGE))
          .link_1_25('Full')
          endif
          .DoRTCalc(26,26,.f.)
        .w_SCCODBAN = .w_SCCODBAN
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_SCCODBAN))
          .link_1_27('Full')
          endif
          .DoRTCalc(28,29,.f.)
        .w_SCCODVAL = iif(EMPTY(.w_VALUTA),g_PERVAL,.w_VALUTA)
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_SCCODVAL))
          .link_1_30('Full')
          endif
          .DoRTCalc(31,33,.f.)
        .w_SCCAOVAL = GETCAM(.w_SCCODVAL, .w_SCDATDOC, 7)
          .DoRTCalc(35,35,.f.)
        .w_SCFLIMPG = 'G'
        .w_CALCPICT = DEFPIC(.w_DECTOT)
          .DoRTCalc(38,44,.f.)
        .w_TIPOPE = this.cFunction
        .w_SCBANNOS = .w_SCBANNOS
        .DoRTCalc(46,46,.f.)
          if not(empty(.w_SCBANNOS))
          .link_1_64('Full')
          endif
          .DoRTCalc(47,49,.f.)
        .w_RESCHK = 0
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'SCA_VARI')
    this.DoRTCalc(51,56,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SCA_VARI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SESCA","i_codazi,w_SCCODICE")
      .op_codazi = .w_codazi
      .op_SCCODICE = .w_SCCODICE
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSCCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oSCDATREG_1_3.enabled = i_bVal
      .Page1.oPag.oSCNUMDOC_1_4.enabled = i_bVal
      .Page1.oPag.oSCALFDOC_1_5.enabled = i_bVal
      .Page1.oPag.oSCDATDOC_1_6.enabled = i_bVal
      .Page1.oPag.oSCDESCRI_1_7.enabled = i_bVal
      .Page1.oPag.oSCTIPCLF_1_8.enabled = i_bVal
      .Page1.oPag.oPFLCRSA_1_9.enabled = i_bVal
      .Page1.oPag.oSCTIPSCA_1_10.enabled = i_bVal
      .Page1.oPag.oSCCODCLF_1_16.enabled = i_bVal
      .Page1.oPag.oSCFLVABD_1_22.enabled = i_bVal
      .Page1.oPag.oSCCODPAG_1_23.enabled = i_bVal
      .Page1.oPag.oSCCODAGE_1_25.enabled = i_bVal
      .Page1.oPag.oSCCODBAN_1_27.enabled = i_bVal
      .Page1.oPag.oSCCODVAL_1_30.enabled = i_bVal
      .Page1.oPag.oSCCAOVAL_1_34.enabled = i_bVal
      .Page1.oPag.oSCIMPSCA_1_35.enabled = i_bVal
      .Page1.oPag.oSCBANNOS_1_64.enabled = i_bVal
      .Page1.oPag.oBtn_1_41.enabled = i_bVal
      .Page1.oPag.oBtn_1_75.enabled = .Page1.oPag.oBtn_1_75.mCond()
      .Page1.oPag.oObj_1_77.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSCCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSCCODICE_1_1.enabled = .t.
      endif
    endwith
    this.GSTE_MPA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'SCA_VARI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSTE_MPA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SCA_VARI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODICE,"SCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDATREG,"SCDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCNUMDOC,"SCNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCALFDOC,"SCALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDATDOC,"SCDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDESCRI,"SCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCTIPCLF,"SCTIPCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCTIPSCA,"SCTIPSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODSEC,"SCCODSEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCVALNAZ,"SCVALNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDATVAL,"SCDATVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODCLF,"SCCODCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCFLVABD,"SCFLVABD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODPAG,"SCCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODAGE,"SCCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODBAN,"SCCODBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODVAL,"SCCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCAOVAL,"SCCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCIMPSCA,"SCIMPSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCFLIMPG,"SCFLIMPG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCBANNOS,"SCBANNOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCRIFDCO,"SCRIFDCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SCA_VARI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2])
    i_lTable = "SCA_VARI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SCA_VARI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSTE_SSV with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SCA_VARI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SCA_VARI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SESCA","i_codazi,w_SCCODICE")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SCA_VARI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SCA_VARI')
        i_extval=cp_InsertValODBCExtFlds(this,'SCA_VARI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SCCODICE,SCDATREG,SCNUMDOC,SCALFDOC,SCDATDOC"+;
                  ",SCDESCRI,SCTIPCLF,SCTIPSCA,SCCODSEC,SCVALNAZ"+;
                  ",SCDATVAL,SCCODCLF,SCFLVABD,SCCODPAG,SCCODAGE"+;
                  ",SCCODBAN,SCCODVAL,SCCAOVAL,SCIMPSCA,SCFLIMPG"+;
                  ",SCBANNOS,SCRIFDCO,UTCC,UTCV,UTDC"+;
                  ",UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_SCCODICE)+;
                  ","+cp_ToStrODBC(this.w_SCDATREG)+;
                  ","+cp_ToStrODBC(this.w_SCNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_SCALFDOC)+;
                  ","+cp_ToStrODBC(this.w_SCDATDOC)+;
                  ","+cp_ToStrODBC(this.w_SCDESCRI)+;
                  ","+cp_ToStrODBC(this.w_SCTIPCLF)+;
                  ","+cp_ToStrODBC(this.w_SCTIPSCA)+;
                  ","+cp_ToStrODBC(this.w_SCCODSEC)+;
                  ","+cp_ToStrODBC(this.w_SCVALNAZ)+;
                  ","+cp_ToStrODBC(this.w_SCDATVAL)+;
                  ","+cp_ToStrODBCNull(this.w_SCCODCLF)+;
                  ","+cp_ToStrODBC(this.w_SCFLVABD)+;
                  ","+cp_ToStrODBCNull(this.w_SCCODPAG)+;
                  ","+cp_ToStrODBCNull(this.w_SCCODAGE)+;
                  ","+cp_ToStrODBCNull(this.w_SCCODBAN)+;
                  ","+cp_ToStrODBCNull(this.w_SCCODVAL)+;
                  ","+cp_ToStrODBC(this.w_SCCAOVAL)+;
                  ","+cp_ToStrODBC(this.w_SCIMPSCA)+;
                  ","+cp_ToStrODBC(this.w_SCFLIMPG)+;
                  ","+cp_ToStrODBCNull(this.w_SCBANNOS)+;
                  ","+cp_ToStrODBC(this.w_SCRIFDCO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SCA_VARI')
        i_extval=cp_InsertValVFPExtFlds(this,'SCA_VARI')
        cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_SCCODICE,'SCCODSEC',this.w_SCCODSEC)
        INSERT INTO (i_cTable);
              (SCCODICE,SCDATREG,SCNUMDOC,SCALFDOC,SCDATDOC,SCDESCRI,SCTIPCLF,SCTIPSCA,SCCODSEC,SCVALNAZ,SCDATVAL,SCCODCLF,SCFLVABD,SCCODPAG,SCCODAGE,SCCODBAN,SCCODVAL,SCCAOVAL,SCIMPSCA,SCFLIMPG,SCBANNOS,SCRIFDCO,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SCCODICE;
                  ,this.w_SCDATREG;
                  ,this.w_SCNUMDOC;
                  ,this.w_SCALFDOC;
                  ,this.w_SCDATDOC;
                  ,this.w_SCDESCRI;
                  ,this.w_SCTIPCLF;
                  ,this.w_SCTIPSCA;
                  ,this.w_SCCODSEC;
                  ,this.w_SCVALNAZ;
                  ,this.w_SCDATVAL;
                  ,this.w_SCCODCLF;
                  ,this.w_SCFLVABD;
                  ,this.w_SCCODPAG;
                  ,this.w_SCCODAGE;
                  ,this.w_SCCODBAN;
                  ,this.w_SCCODVAL;
                  ,this.w_SCCAOVAL;
                  ,this.w_SCIMPSCA;
                  ,this.w_SCFLIMPG;
                  ,this.w_SCBANNOS;
                  ,this.w_SCRIFDCO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SCA_VARI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SCA_VARI_IDX,i_nConn)
      *
      * update SCA_VARI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SCA_VARI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SCDATREG="+cp_ToStrODBC(this.w_SCDATREG)+;
             ",SCNUMDOC="+cp_ToStrODBC(this.w_SCNUMDOC)+;
             ",SCALFDOC="+cp_ToStrODBC(this.w_SCALFDOC)+;
             ",SCDATDOC="+cp_ToStrODBC(this.w_SCDATDOC)+;
             ",SCDESCRI="+cp_ToStrODBC(this.w_SCDESCRI)+;
             ",SCTIPCLF="+cp_ToStrODBC(this.w_SCTIPCLF)+;
             ",SCTIPSCA="+cp_ToStrODBC(this.w_SCTIPSCA)+;
             ",SCVALNAZ="+cp_ToStrODBC(this.w_SCVALNAZ)+;
             ",SCDATVAL="+cp_ToStrODBC(this.w_SCDATVAL)+;
             ",SCCODCLF="+cp_ToStrODBCNull(this.w_SCCODCLF)+;
             ",SCFLVABD="+cp_ToStrODBC(this.w_SCFLVABD)+;
             ",SCCODPAG="+cp_ToStrODBCNull(this.w_SCCODPAG)+;
             ",SCCODAGE="+cp_ToStrODBCNull(this.w_SCCODAGE)+;
             ",SCCODBAN="+cp_ToStrODBCNull(this.w_SCCODBAN)+;
             ",SCCODVAL="+cp_ToStrODBCNull(this.w_SCCODVAL)+;
             ",SCCAOVAL="+cp_ToStrODBC(this.w_SCCAOVAL)+;
             ",SCIMPSCA="+cp_ToStrODBC(this.w_SCIMPSCA)+;
             ",SCFLIMPG="+cp_ToStrODBC(this.w_SCFLIMPG)+;
             ",SCBANNOS="+cp_ToStrODBCNull(this.w_SCBANNOS)+;
             ",SCRIFDCO="+cp_ToStrODBC(this.w_SCRIFDCO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SCA_VARI')
        i_cWhere = cp_PKFox(i_cTable  ,'SCCODICE',this.w_SCCODICE  ,'SCCODSEC',this.w_SCCODSEC  )
        UPDATE (i_cTable) SET;
              SCDATREG=this.w_SCDATREG;
             ,SCNUMDOC=this.w_SCNUMDOC;
             ,SCALFDOC=this.w_SCALFDOC;
             ,SCDATDOC=this.w_SCDATDOC;
             ,SCDESCRI=this.w_SCDESCRI;
             ,SCTIPCLF=this.w_SCTIPCLF;
             ,SCTIPSCA=this.w_SCTIPSCA;
             ,SCVALNAZ=this.w_SCVALNAZ;
             ,SCDATVAL=this.w_SCDATVAL;
             ,SCCODCLF=this.w_SCCODCLF;
             ,SCFLVABD=this.w_SCFLVABD;
             ,SCCODPAG=this.w_SCCODPAG;
             ,SCCODAGE=this.w_SCCODAGE;
             ,SCCODBAN=this.w_SCCODBAN;
             ,SCCODVAL=this.w_SCCODVAL;
             ,SCCAOVAL=this.w_SCCAOVAL;
             ,SCIMPSCA=this.w_SCIMPSCA;
             ,SCFLIMPG=this.w_SCFLIMPG;
             ,SCBANNOS=this.w_SCBANNOS;
             ,SCRIFDCO=this.w_SCRIFDCO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSTE_MPA : Saving
      this.GSTE_MPA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_SCCODICE,"PTSERIAL";
             ,this.w_SCCODSEC,"PTROWORD";
             )
      this.GSTE_MPA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
    this.Calculate_CLWGYDDTEA()
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    this.Calculate_HGIVCBBXSR()
    * --- GSTE_MPA : Deleting
    this.GSTE_MPA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_SCCODICE,"PTSERIAL";
           ,this.w_SCCODSEC,"PTROWORD";
           )
    this.GSTE_MPA.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SCA_VARI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SCA_VARI_IDX,i_nConn)
      *
      * delete SCA_VARI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SCCODICE',this.w_SCCODICE  ,'SCCODSEC',this.w_SCCODSEC  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SCA_VARI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_SCDATREG<>.w_SCDATREG
            .w_SCDATDOC = iif(.w_ORIGINE='A' AND g_COGE<>'S' AND isalt(),.w_SCDATREG,i_datsys)
        endif
        .DoRTCalc(7,8,.t.)
        if .o_ORIGINE<>.w_ORIGINE
            .w_PFLCRSA = iif(.w_ORIGINE='A','S','C')
        endif
        if .o_SCTIPCLF<>.w_SCTIPCLF.or. .o_PFLCRSA<>.w_PFLCRSA
            .w_SCTIPSCA = IIF((.w_SCTIPCLF='C' AND .w_PFLCRSA='S') or (.w_SCTIPCLF='F' AND .w_PFLCRSA<>'S') ,'F', IIF((.w_SCTIPCLF='F' AND .w_PFLCRSA='S') or (.w_SCTIPCLF='C' AND .w_PFLCRSA<>'S') ,'C', 'C'))
        endif
        .DoRTCalc(11,11,.t.)
          .link_1_12('Full')
        .DoRTCalc(13,13,.t.)
            .w_OBTEST = .w_SCDATDOC
            .w_SCDATVAL = .w_SCDATDOC
        .DoRTCalc(16,21,.t.)
        if .o_SCTIPCLF<>.w_SCTIPCLF.or. .o_SCCODCLF<>.w_SCCODCLF
            .w_SCFLVABD = IIF(.w_SCTIPCLF='C', .w_FLVEBD, IIF(.w_SCTIPCLF='F', .w_FLACBD, ' '))
        endif
        if .o_SCCODCLF<>.w_SCCODCLF
          .link_1_23('Full')
        endif
        .DoRTCalc(24,24,.t.)
        if .o_SCCODCLF<>.w_SCCODCLF
          .link_1_25('Full')
        endif
        .DoRTCalc(26,26,.t.)
        if .o_SCCODCLF<>.w_SCCODCLF
          .link_1_27('Full')
        endif
        .DoRTCalc(28,29,.t.)
        if .o_SCCODCLF<>.w_SCCODCLF
            .w_SCCODVAL = iif(EMPTY(.w_VALUTA),g_PERVAL,.w_VALUTA)
          .link_1_30('Full')
        endif
        .DoRTCalc(31,33,.t.)
        if .o_SCCODVAL<>.w_SCCODVAL.or. .o_SCDATDOC<>.w_SCDATDOC.or. .o_SCDATVAL<>.w_SCDATVAL
            .w_SCCAOVAL = GETCAM(.w_SCCODVAL, .w_SCDATDOC, 7)
        endif
        .DoRTCalc(35,36,.t.)
        if .o_DECTOT<>.w_DECTOT
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(38,44,.t.)
            .w_TIPOPE = this.cFunction
        if .o_SCCODCLF<>.w_SCCODCLF
          .link_1_64('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SESCA","i_codazi,w_SCCODICE")
          .op_SCCODICE = .w_SCCODICE
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(47,56,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
    endwith
  return

  proc Calculate_ITRQQGYOLH()
    with this
          * --- Controlli alla checkform (gsar_bks)
          GSAR_BKS(this;
              ,'Controlli';
             )
    endwith
  endproc
  proc Calculate_CLWGYDDTEA()
    with this
          * --- Aggiorno data reg. partite (gsar_bks)
          GSAR_BKS(this;
              ,'Agg_Dat_reg';
             )
    endwith
  endproc
  proc Calculate_KPKNCRHLSK()
    with this
          * --- Controllo cancellazione gsar_bkx
          GSAR_BKX(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_HGIVCBBXSR()
    with this
          * --- Aggiornamento in cancellazione
          GSAR_BKS(this;
              ,'Delete';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSCTIPCLF_1_8.enabled = this.oPgFrm.Page1.oPag.oSCTIPCLF_1_8.mCond()
    this.oPgFrm.Page1.oPag.oPFLCRSA_1_9.enabled = this.oPgFrm.Page1.oPag.oPFLCRSA_1_9.mCond()
    this.oPgFrm.Page1.oPag.oSCCODCLF_1_16.enabled = this.oPgFrm.Page1.oPag.oSCCODCLF_1_16.mCond()
    this.oPgFrm.Page1.oPag.oSCCODAGE_1_25.enabled = this.oPgFrm.Page1.oPag.oSCCODAGE_1_25.mCond()
    this.oPgFrm.Page1.oPag.oSCCODBAN_1_27.enabled = this.oPgFrm.Page1.oPag.oSCCODBAN_1_27.mCond()
    this.oPgFrm.Page1.oPag.oSCCAOVAL_1_34.enabled = this.oPgFrm.Page1.oPag.oSCCAOVAL_1_34.mCond()
    this.oPgFrm.Page1.oPag.oSCBANNOS_1_64.enabled = this.oPgFrm.Page1.oPag.oSCBANNOS_1_64.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSCNUMDOC_1_4.visible=!this.oPgFrm.Page1.oPag.oSCNUMDOC_1_4.mHide()
    this.oPgFrm.Page1.oPag.oSCALFDOC_1_5.visible=!this.oPgFrm.Page1.oPag.oSCALFDOC_1_5.mHide()
    this.oPgFrm.Page1.oPag.oSCDATDOC_1_6.visible=!this.oPgFrm.Page1.oPag.oSCDATDOC_1_6.mHide()
    this.oPgFrm.Page1.oPag.oPFLCRSA_1_9.visible=!this.oPgFrm.Page1.oPag.oPFLCRSA_1_9.mHide()
    this.oPgFrm.Page1.oPag.oSCTIPSCA_1_10.visible=!this.oPgFrm.Page1.oPag.oSCTIPSCA_1_10.mHide()
    this.oPgFrm.Page1.oPag.oSCFLVABD_1_22.visible=!this.oPgFrm.Page1.oPag.oSCFLVABD_1_22.mHide()
    this.oPgFrm.Page1.oPag.oSCCODPAG_1_23.visible=!this.oPgFrm.Page1.oPag.oSCCODPAG_1_23.mHide()
    this.oPgFrm.Page1.oPag.oDESPAG_1_24.visible=!this.oPgFrm.Page1.oPag.oDESPAG_1_24.mHide()
    this.oPgFrm.Page1.oPag.oSCCODAGE_1_25.visible=!this.oPgFrm.Page1.oPag.oSCCODAGE_1_25.mHide()
    this.oPgFrm.Page1.oPag.oDESAGE_1_26.visible=!this.oPgFrm.Page1.oPag.oDESAGE_1_26.mHide()
    this.oPgFrm.Page1.oPag.oSCCODBAN_1_27.visible=!this.oPgFrm.Page1.oPag.oSCCODBAN_1_27.mHide()
    this.oPgFrm.Page1.oPag.oDESBAN_1_28.visible=!this.oPgFrm.Page1.oPag.oDESBAN_1_28.mHide()
    this.oPgFrm.Page1.oPag.oDESNOS_1_29.visible=!this.oPgFrm.Page1.oPag.oDESNOS_1_29.mHide()
    this.oPgFrm.Page1.oPag.oSCIMPSCA_1_35.visible=!this.oPgFrm.Page1.oPag.oSCIMPSCA_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_41.visible=!this.oPgFrm.Page1.oPag.oBtn_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oSCBANNOS_1_64.visible=!this.oPgFrm.Page1.oPag.oSCBANNOS_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_75.visible=!this.oPgFrm.Page1.oPag.oBtn_1_75.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Controlli")
          .Calculate_ITRQQGYOLH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete start")
          .Calculate_KPKNCRHLSK()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_77.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLVEBD";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZFLVEBD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_FLVEBD = NVL(_Link_.AZFLVEBD,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_FLVEBD = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCODCLF
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SCCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODBAN,ANCODPAG,ANPARTSN,ANDTOBSO,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS,ANCODVAL,ANCODBA2,ANCODAG1,ANFLACBD,ANNUMCOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SCTIPCLF;
                     ,'ANCODICE',trim(this.w_SCCODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCODBAN,ANCODPAG,ANPARTSN,ANDTOBSO,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS,ANCODVAL,ANCODBA2,ANCODAG1,ANFLACBD,ANNUMCOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_SCCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODBAN,ANCODPAG,ANPARTSN,ANDTOBSO,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS,ANCODVAL,ANCODBA2,ANCODAG1,ANFLACBD,ANNUMCOR";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_SCCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_SCTIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCODBAN,ANCODPAG,ANPARTSN,ANDTOBSO,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS,ANCODVAL,ANCODBA2,ANCODAG1,ANFLACBD,ANNUMCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCCODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSCCODCLF_1_16'),i_cWhere,'GSAR_BZC',"Clienti/fornitori/conti (partite)",'GSTE_ASC.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SCTIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODBAN,ANCODPAG,ANPARTSN,ANDTOBSO,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS,ANCODVAL,ANCODBA2,ANCODAG1,ANFLACBD,ANNUMCOR";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODBAN,ANCODPAG,ANPARTSN,ANDTOBSO,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS,ANCODVAL,ANCODBA2,ANCODAG1,ANFLACBD,ANNUMCOR;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente, obsoleto oppure non gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODBAN,ANCODPAG,ANPARTSN,ANDTOBSO,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS,ANCODVAL,ANCODBA2,ANCODAG1,ANFLACBD,ANNUMCOR";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODBAN,ANCODPAG,ANPARTSN,ANDTOBSO,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS,ANCODVAL,ANCODBA2,ANCODAG1,ANFLACBD,ANNUMCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODBAN,ANCODPAG,ANPARTSN,ANDTOBSO,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS,ANCODVAL,ANCODBA2,ANCODAG1,ANFLACBD,ANNUMCOR";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SCCODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SCTIPCLF;
                       ,'ANCODICE',this.w_SCCODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODBAN,ANCODPAG,ANPARTSN,ANDTOBSO,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS,ANCODVAL,ANCODBA2,ANCODAG1,ANFLACBD,ANNUMCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_RAGSOC = NVL(_Link_.ANDESCRI,space(40))
      this.w_SCCODBAN = NVL(_Link_.ANCODBAN,space(10))
      this.w_SCCODPAG = NVL(_Link_.ANCODPAG,space(5))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_MESCL1 = NVL(_Link_.AN1MESCL,0)
      this.w_MESCL2 = NVL(_Link_.AN2MESCL,0)
      this.w_GIOSC1 = NVL(_Link_.ANGIOSC1,0)
      this.w_GIOSC2 = NVL(_Link_.ANGIOSC2,0)
      this.w_GIOFIS = NVL(_Link_.ANGIOFIS,0)
      this.w_VALUTA = NVL(_Link_.ANCODVAL,space(3))
      this.w_SCBANNOS = NVL(_Link_.ANCODBA2,space(15))
      this.w_SCCODAGE = NVL(_Link_.ANCODAG1,space(5))
      this.w_FLACBD = NVL(_Link_.ANFLACBD,space(1))
      this.w_NUMCOR = NVL(_Link_.ANNUMCOR,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODCLF = space(15)
      endif
      this.w_RAGSOC = space(40)
      this.w_SCCODBAN = space(10)
      this.w_SCCODPAG = space(5)
      this.w_PARTSN = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_MESCL1 = 0
      this.w_MESCL2 = 0
      this.w_GIOSC1 = 0
      this.w_GIOSC2 = 0
      this.w_GIOFIS = 0
      this.w_VALUTA = space(3)
      this.w_SCBANNOS = space(15)
      this.w_SCCODAGE = space(5)
      this.w_FLACBD = space(1)
      this.w_NUMCOR = space(25)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes= (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_PARTSN='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente, obsoleto oppure non gestito a partite")
        endif
        this.w_SCCODCLF = space(15)
        this.w_RAGSOC = space(40)
        this.w_SCCODBAN = space(10)
        this.w_SCCODPAG = space(5)
        this.w_PARTSN = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_MESCL1 = 0
        this.w_MESCL2 = 0
        this.w_GIOSC1 = 0
        this.w_GIOSC2 = 0
        this.w_GIOFIS = 0
        this.w_VALUTA = space(3)
        this.w_SCBANNOS = space(15)
        this.w_SCCODAGE = space(5)
        this.w_FLACBD = space(1)
        this.w_NUMCOR = space(25)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 16 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+16<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.ANCODICE as ANCODICE116"+ ",link_1_16.ANDESCRI as ANDESCRI116"+ ",link_1_16.ANCODBAN as ANCODBAN116"+ ",link_1_16.ANCODPAG as ANCODPAG116"+ ",link_1_16.ANPARTSN as ANPARTSN116"+ ",link_1_16.ANDTOBSO as ANDTOBSO116"+ ",link_1_16.AN1MESCL as AN1MESCL116"+ ",link_1_16.AN2MESCL as AN2MESCL116"+ ",link_1_16.ANGIOSC1 as ANGIOSC1116"+ ",link_1_16.ANGIOSC2 as ANGIOSC2116"+ ",link_1_16.ANGIOFIS as ANGIOFIS116"+ ",link_1_16.ANCODVAL as ANCODVAL116"+ ",link_1_16.ANCODBA2 as ANCODBA2116"+ ",link_1_16.ANCODAG1 as ANCODAG1116"+ ",link_1_16.ANFLACBD as ANFLACBD116"+ ",link_1_16.ANNUMCOR as ANNUMCOR116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on SCA_VARI.SCCODCLF=link_1_16.ANCODICE"+" and SCA_VARI.SCTIPCLF=link_1_16.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+16
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and SCA_VARI.SCCODCLF=link_1_16.ANCODICE(+)"'+'+" and SCA_VARI.SCTIPCLF=link_1_16.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+16
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCODPAG
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_SCCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_SCCODPAG))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_SCCODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_SCCODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_SCCODPAG)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oSCCODPAG_1_23'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_SCCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_SCCODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_SCCODPAG = space(5)
        this.w_DESPAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.PACODICE as PACODICE123"+ ","+cp_TransLinkFldName('link_1_23.PADESCRI')+" as PADESCRI123"+ ",link_1_23.PADTOBSO as PADTOBSO123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on SCA_VARI.SCCODPAG=link_1_23.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and SCA_VARI.SCCODPAG=link_1_23.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCODAGE
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_SCCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_SCCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oSCCODAGE_1_25'),i_cWhere,'GSAR_AGE',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_SCCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_SCCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_AGEOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_AGEOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_AGEOBSO) OR .w_AGEOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente inesistente o obsoleto")
        endif
        this.w_SCCODAGE = space(5)
        this.w_DESAGE = space(35)
        this.w_AGEOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.AGCODAGE as AGCODAGE125"+ ",link_1_25.AGDESAGE as AGDESAGE125"+ ",link_1_25.AGDTOBSO as AGDTOBSO125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on SCA_VARI.SCCODAGE=link_1_25.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and SCA_VARI.SCCODAGE=link_1_25.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCODBAN
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_SCCODBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_SCCODBAN))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCODBAN) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oSCCODBAN_1_27'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_SCCODBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_SCCODBAN)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODBAN = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODBAN = space(10)
      endif
      this.w_DESBAN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_27(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.BAN_CHE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_27.BACODBAN as BACODBAN127"+ ",link_1_27.BADESBAN as BADESBAN127"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_27 on SCA_VARI.SCCODBAN=link_1_27.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_27"
          i_cKey=i_cKey+'+" and SCA_VARI.SCCODBAN=link_1_27.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCODVAL
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_SCCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_SCCODVAL))
          select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_SCCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_SCCODVAL)+"%");

            select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oSCCODVAL_1_30'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_SCCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_SCCODVAL)
            select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODVAL = space(3)
      endif
      this.w_DESAPP = space(35)
      this.w_DECTOT = 0
      this.w_SIMVAL = space(5)
      this.w_CAOVAL = 0
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
        endif
        this.w_SCCODVAL = space(3)
        this.w_DESAPP = space(35)
        this.w_DECTOT = 0
        this.w_SIMVAL = space(5)
        this.w_CAOVAL = 0
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_30.VACODVAL as VACODVAL130"+ ",link_1_30.VADESVAL as VADESVAL130"+ ",link_1_30.VADECTOT as VADECTOT130"+ ",link_1_30.VASIMVAL as VASIMVAL130"+ ",link_1_30.VACAOVAL as VACAOVAL130"+ ",link_1_30.VADTOBSO as VADTOBSO130"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_30 on SCA_VARI.SCCODVAL=link_1_30.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_30"
          i_cKey=i_cKey+'+" and SCA_VARI.SCCODVAL=link_1_30.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCBANNOS
  func Link_1_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCBANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_SCBANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_SCBANNOS))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCBANNOS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_SCBANNOS)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_SCBANNOS)+"%");

            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCBANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oSCBANNOS_1_64'),i_cWhere,'GSTE_ACB',"Conti banca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCBANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_SCBANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_SCBANNOS)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCBANNOS = NVL(_Link_.BACODBAN,space(15))
      this.w_DESNOS = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SCBANNOS = space(15)
      endif
      this.w_DESNOS = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCBANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_64(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_64.BACODBAN as BACODBAN164"+ ",link_1_64.BADESCRI as BADESCRI164"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_64 on SCA_VARI.SCBANNOS=link_1_64.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_64"
          i_cKey=i_cKey+'+" and SCA_VARI.SCBANNOS=link_1_64.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCCODICE_1_1.value==this.w_SCCODICE)
      this.oPgFrm.Page1.oPag.oSCCODICE_1_1.value=this.w_SCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDATREG_1_3.value==this.w_SCDATREG)
      this.oPgFrm.Page1.oPag.oSCDATREG_1_3.value=this.w_SCDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oSCNUMDOC_1_4.value==this.w_SCNUMDOC)
      this.oPgFrm.Page1.oPag.oSCNUMDOC_1_4.value=this.w_SCNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCALFDOC_1_5.value==this.w_SCALFDOC)
      this.oPgFrm.Page1.oPag.oSCALFDOC_1_5.value=this.w_SCALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDATDOC_1_6.value==this.w_SCDATDOC)
      this.oPgFrm.Page1.oPag.oSCDATDOC_1_6.value=this.w_SCDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDESCRI_1_7.value==this.w_SCDESCRI)
      this.oPgFrm.Page1.oPag.oSCDESCRI_1_7.value=this.w_SCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCTIPCLF_1_8.RadioValue()==this.w_SCTIPCLF)
      this.oPgFrm.Page1.oPag.oSCTIPCLF_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPFLCRSA_1_9.RadioValue()==this.w_PFLCRSA)
      this.oPgFrm.Page1.oPag.oPFLCRSA_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCTIPSCA_1_10.RadioValue()==this.w_SCTIPSCA)
      this.oPgFrm.Page1.oPag.oSCTIPSCA_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODCLF_1_16.value==this.w_SCCODCLF)
      this.oPgFrm.Page1.oPag.oSCCODCLF_1_16.value=this.w_SCCODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOC_1_18.value==this.w_RAGSOC)
      this.oPgFrm.Page1.oPag.oRAGSOC_1_18.value=this.w_RAGSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCFLVABD_1_22.RadioValue()==this.w_SCFLVABD)
      this.oPgFrm.Page1.oPag.oSCFLVABD_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODPAG_1_23.value==this.w_SCCODPAG)
      this.oPgFrm.Page1.oPag.oSCCODPAG_1_23.value=this.w_SCCODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_24.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_24.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODAGE_1_25.value==this.w_SCCODAGE)
      this.oPgFrm.Page1.oPag.oSCCODAGE_1_25.value=this.w_SCCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_26.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_26.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODBAN_1_27.value==this.w_SCCODBAN)
      this.oPgFrm.Page1.oPag.oSCCODBAN_1_27.value=this.w_SCCODBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBAN_1_28.value==this.w_DESBAN)
      this.oPgFrm.Page1.oPag.oDESBAN_1_28.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOS_1_29.value==this.w_DESNOS)
      this.oPgFrm.Page1.oPag.oDESNOS_1_29.value=this.w_DESNOS
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODVAL_1_30.value==this.w_SCCODVAL)
      this.oPgFrm.Page1.oPag.oSCCODVAL_1_30.value=this.w_SCCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_31.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_31.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCAOVAL_1_34.value==this.w_SCCAOVAL)
      this.oPgFrm.Page1.oPag.oSCCAOVAL_1_34.value=this.w_SCCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSCIMPSCA_1_35.value==this.w_SCIMPSCA)
      this.oPgFrm.Page1.oPag.oSCIMPSCA_1_35.value=this.w_SCIMPSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oSCBANNOS_1_64.value==this.w_SCBANNOS)
      this.oPgFrm.Page1.oPag.oSCBANNOS_1_64.value=this.w_SCBANNOS
    endif
    cp_SetControlsValueExtFlds(this,'SCA_VARI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SCCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_SCCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SCDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCDATREG_1_3.SetFocus()
            i_bnoObbl = !empty(.w_SCDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_SCDATDOC)) or not(CHPNTDOC(.w_SCDATREG, .w_SCDATDOC)))  and not(.w_ORIGINE='A' AND g_COGE<>'S' AND isalt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCDATDOC_1_6.SetFocus()
            i_bnoObbl = !empty(.w_SCDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_SCCODCLF)) or not( (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_PARTSN='S'))  and (Empty(.w_SERDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCCODCLF_1_16.SetFocus()
            i_bnoObbl = !empty(.w_SCCODCLF)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente, obsoleto oppure non gestito a partite")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(.w_ORIGINE='A')  and not(empty(.w_SCCODPAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCCODPAG_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   not(EMPTY(.w_AGEOBSO) OR .w_AGEOBSO>.w_OBTEST)  and not(isalt())  and (.w_SCTIPCLF='C')  and not(empty(.w_SCCODAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCCODAGE_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice agente inesistente o obsoleto")
          case   ((empty(.w_SCCODVAL)) or not(CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCCODVAL_1_30.SetFocus()
            i_bnoObbl = !empty(.w_SCCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
          case   (empty(.w_SCCAOVAL))  and (GETVALUT(g_PERVAL, 'VADATEUR')>=.w_SCDATDOC OR .w_CAOVAL=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCCAOVAL_1_34.SetFocus()
            i_bnoObbl = !empty(.w_SCCAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSTE_MPA.CheckForm()
      if i_bres
        i_bres=  .GSTE_MPA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsar_asc
      if i_bRes=.t.
         .w_RESCHK=0
           .NotifyEvent('Controlli')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ORIGINE = this.w_ORIGINE
    this.o_SCDATREG = this.w_SCDATREG
    this.o_SCDATDOC = this.w_SCDATDOC
    this.o_SCTIPCLF = this.w_SCTIPCLF
    this.o_PFLCRSA = this.w_PFLCRSA
    this.o_SCDATVAL = this.w_SCDATVAL
    this.o_SCCODCLF = this.w_SCCODCLF
    this.o_SCCODVAL = this.w_SCCODVAL
    this.o_DECTOT = this.w_DECTOT
    * --- GSTE_MPA : Depends On
    this.GSTE_MPA.SaveDependsOn()
    return

  func CanEdit()
    local i_res
    i_res=GSAR_BVC( This , this.w_SCCODICE , 'S' )
    if !i_res
      cp_ErrorMsg('MSG_CANNOT_EDIT')
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=GSAR_BVC( This , this.w_SCCODICE , 'S' )
    if !i_res
      cp_ErrorMsg('MSG_CANNOT_DELETE')
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsar_ascPag1 as StdContainer
  Width  = 798
  height = 441
  stdWidth  = 798
  stdheight = 441
  resizeXpos=380
  resizeYpos=337
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCCODICE_1_1 as StdField with uid="OGNASYKREE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SCCODICE", cQueryName = "SCCODICE",nZero=10,;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice progressivo della scadenza",;
    HelpContextID = 118096235,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=112, Top=7, cSayPict="'9999999999'", cGetPict="'9999999999'", InputMask=replicate('X',10)

  add object oSCDATREG_1_3 as StdField with uid="GIYSXYXMQG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SCDATREG", cQueryName = "SCDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione scadenza diversa",;
    HelpContextID = 16519533,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=251, Top=7

  add object oSCNUMDOC_1_4 as StdField with uid="ATQEBPZXSO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCNUMDOC", cQueryName = "SCNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento di riferimento",;
    HelpContextID = 224349847,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=453, Top=7, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oSCNUMDOC_1_4.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A' AND g_COGE<>'S' AND isalt())
    endwith
  endfunc

  add object oSCALFDOC_1_5 as StdField with uid="GQFGFOAMJA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SCALFDOC", cQueryName = "SCALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Alfa del documento di riferimento",;
    HelpContextID = 232332951,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=581, Top=7, InputMask=replicate('X',10)

  func oSCALFDOC_1_5.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A' AND g_COGE<>'S' AND isalt())
    endwith
  endfunc

  add object oSCDATDOC_1_6 as StdField with uid="ZPJZTYYAQX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SCDATDOC", cQueryName = "SCDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento di riferimento",;
    HelpContextID = 218361495,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=708, Top=7

  func oSCDATDOC_1_6.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A' AND g_COGE<>'S' AND isalt())
    endwith
  endfunc

  func oSCDATDOC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHPNTDOC(.w_SCDATREG, .w_SCDATDOC))
    endwith
    return bRes
  endfunc

  add object oSCDESCRI_1_7 as StdField with uid="TUQUPFIQOO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SCDESCRI", cQueryName = "SCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della scadenza",;
    HelpContextID = 32510319,;
   bGlobalFont=.t.,;
    Height=21, Width=358, Left=112, Top=32, InputMask=replicate('X',35)


  add object oSCTIPCLF_1_8 as StdCombo with uid="ECLUWFZFMP",rtseq=8,rtrep=.f.,left=112,top=57,width=75,height=21;
    , ToolTipText = "Tipo conto G=generico; C=clienti; F=fornitori";
    , HelpContextID = 238743188;
    , cFormVar="w_SCTIPCLF",RowSource=""+"Conto,"+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSCTIPCLF_1_8.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oSCTIPCLF_1_8.GetRadio()
    this.Parent.oContained.w_SCTIPCLF = this.RadioValue()
    return .t.
  endfunc

  func oSCTIPCLF_1_8.SetRadio()
    this.Parent.oContained.w_SCTIPCLF=trim(this.Parent.oContained.w_SCTIPCLF)
    this.value = ;
      iif(this.Parent.oContained.w_SCTIPCLF=='G',1,;
      iif(this.Parent.oContained.w_SCTIPCLF=='C',2,;
      iif(this.Parent.oContained.w_SCTIPCLF=='F',3,;
      0)))
  endfunc

  func oSCTIPCLF_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ORIGINE<>'A'  OR g_COGE='S' AND isalt())
    endwith
   endif
  endfunc

  proc oSCTIPCLF_1_8.mAfter
    with this.Parent.oContained
      .w_SCTIPSCA=IIF(.w_SCTIPCLF='G','F',.w_SCTIPCLF)
    endwith
  endproc

  func oSCTIPCLF_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_SCCODCLF)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oPFLCRSA_1_9 as StdCombo with uid="ANCGDPPYEQ",rtseq=9,rtrep=.f.,left=267,top=57,width=76,height=21;
    , HelpContextID = 31364086;
    , cFormVar="w_PFLCRSA",RowSource=""+"Crea,"+"Salda", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPFLCRSA_1_9.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oPFLCRSA_1_9.GetRadio()
    this.Parent.oContained.w_PFLCRSA = this.RadioValue()
    return .t.
  endfunc

  func oPFLCRSA_1_9.SetRadio()
    this.Parent.oContained.w_PFLCRSA=trim(this.Parent.oContained.w_PFLCRSA)
    this.value = ;
      iif(this.Parent.oContained.w_PFLCRSA=='C',1,;
      iif(this.Parent.oContained.w_PFLCRSA=='S',2,;
      0))
  endfunc

  func oPFLCRSA_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ORIGINE<>'A'  OR g_COGE='S' AND isalt())
    endwith
   endif
  endfunc

  func oPFLCRSA_1_9.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  proc oPFLCRSA_1_9.mAfter
    with this.Parent.oContained
      .GSTE_MPA.mEnableControls()
    endwith
  endproc


  add object oSCTIPSCA_1_10 as StdCombo with uid="FIGFXCTQZK",rtseq=10,rtrep=.f.,left=395,top=57,width=75,height=21;
    , ToolTipText = "Tipo scadenza";
    , HelpContextID = 29692263;
    , cFormVar="w_SCTIPSCA",RowSource=""+"Dare,"+"Avere", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSCTIPSCA_1_10.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oSCTIPSCA_1_10.GetRadio()
    this.Parent.oContained.w_SCTIPSCA = this.RadioValue()
    return .t.
  endfunc

  func oSCTIPSCA_1_10.SetRadio()
    this.Parent.oContained.w_SCTIPSCA=trim(this.Parent.oContained.w_SCTIPSCA)
    this.value = ;
      iif(this.Parent.oContained.w_SCTIPSCA=='C',1,;
      iif(this.Parent.oContained.w_SCTIPSCA=='F',2,;
      0))
  endfunc

  func oSCTIPSCA_1_10.mHide()
    with this.Parent.oContained
      return (.w_PFLCRSA='S')
    endwith
  endfunc

  add object oSCCODCLF_1_16 as StdField with uid="LCGTVSQDPL",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SCCODCLF", cQueryName = "SCCODCLF",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente, obsoleto oppure non gestito a partite",;
    ToolTipText = "Codice debitore / creditore",;
    HelpContextID = 251002516,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=112, Top=85, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SCTIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_SCCODCLF"

  func oSCCODCLF_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_SERDOC))
    endwith
   endif
  endfunc

  func oSCCODCLF_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODCLF_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODCLF_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SCTIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SCTIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSCCODCLF_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori/conti (partite)",'GSTE_ASC.CONTI_VZM',this.parent.oContained
  endproc
  proc oSCCODCLF_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SCTIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_SCCODCLF
     i_obj.ecpSave()
  endproc

  add object oRAGSOC_1_18 as StdField with uid="WGFAFHAXKI",rtseq=18,rtrep=.f.,;
    cFormVar = "w_RAGSOC", cQueryName = "RAGSOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 239190250,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=254, Top=86, InputMask=replicate('X',40)

  add object oSCFLVABD_1_22 as StdCheck with uid="NCKOQWPTOQ",rtseq=22,rtrep=.f.,left=581, top=86, caption="Beni deperibili",;
    ToolTipText = "Flag beni deperibili",;
    HelpContextID = 2568554,;
    cFormVar="w_SCFLVABD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCFLVABD_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSCFLVABD_1_22.GetRadio()
    this.Parent.oContained.w_SCFLVABD = this.RadioValue()
    return .t.
  endfunc

  func oSCFLVABD_1_22.SetRadio()
    this.Parent.oContained.w_SCFLVABD=trim(this.Parent.oContained.w_SCFLVABD)
    this.value = ;
      iif(this.Parent.oContained.w_SCFLVABD=='S',1,;
      0)
  endfunc

  func oSCFLVABD_1_22.mHide()
    with this.Parent.oContained
      return (.w_Origine = 'D' or isalt())
    endwith
  endfunc

  add object oSCCODPAG_1_23 as StdField with uid="BRDKCPRMZT",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SCCODPAG", cQueryName = "SCCODPAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice del pagamento per la generazione delle scadenze",;
    HelpContextID = 235536749,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=112, Top=111, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_SCCODPAG"

  func oSCCODPAG_1_23.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A')
    endwith
  endfunc

  func oSCCODPAG_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODPAG_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODPAG_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oSCCODPAG_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oSCCODPAG_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_SCCODPAG
     i_obj.ecpSave()
  endproc

  add object oDESPAG_1_24 as StdField with uid="CNEOZOXUTO",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 186908106,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=254, Top=112, InputMask=replicate('X',30)

  func oDESPAG_1_24.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A')
    endwith
  endfunc

  add object oSCCODAGE_1_25 as StdField with uid="PVCKJYROBL",rtseq=25,rtrep=.f.,;
    cFormVar = "w_SCCODAGE", cQueryName = "SCCODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente inesistente o obsoleto",;
    ToolTipText = "Codice agente associato al cliente",;
    HelpContextID = 252313963,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=112, Top=137, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_SCCODAGE"

  func oSCCODAGE_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPCLF='C')
    endwith
   endif
  endfunc

  func oSCCODAGE_1_25.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  func oSCCODAGE_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODAGE_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODAGE_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oSCCODAGE_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Elenco agenti",'',this.parent.oContained
  endproc
  proc oSCCODAGE_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_SCCODAGE
     i_obj.ecpSave()
  endproc

  add object oDESAGE_1_26 as StdField with uid="YWMPZOSMQY",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 215154122,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=254, Top=138, InputMask=replicate('X',35)

  func oDESAGE_1_26.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oSCCODBAN_1_27 as StdField with uid="LYCPNKMXQV",rtseq=27,rtrep=.f.,;
    cFormVar = "w_SCCODBAN", cQueryName = "SCCODBAN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice della banca di appoggio per la generazione delle scadenze diverse",;
    HelpContextID = 655732,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=112, Top=163, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_SCCODBAN"

  func oSCCODBAN_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPCLF $ 'CF')
    endwith
   endif
  endfunc

  func oSCCODBAN_1_27.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A')
    endwith
  endfunc

  func oSCCODBAN_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODBAN_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODBAN_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oSCCODBAN_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oSCCODBAN_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_SCCODBAN
     i_obj.ecpSave()
  endproc

  add object oDESBAN_1_28 as StdField with uid="OUPRHOSFRB",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 70385098,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=254, Top=164, InputMask=replicate('X',50)

  func oDESBAN_1_28.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A')
    endwith
  endfunc

  add object oDESNOS_1_29 as StdField with uid="FFBZUJXBKM",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESNOS", cQueryName = "DESNOS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 28967478,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=254, Top=190, InputMask=replicate('X',35)

  func oDESNOS_1_29.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A')
    endwith
  endfunc

  add object oSCCODVAL_1_30 as StdField with uid="CVXSPTXEUV",rtseq=30,rtrep=.f.,;
    cFormVar = "w_SCCODVAL", cQueryName = "SCCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o incongruente o obsoleta",;
    ToolTipText = "Codice valuta delle scadenze",;
    HelpContextID = 67764594,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=112, Top=215, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_SCCODVAL"

  func oSCCODVAL_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODVAL_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODVAL_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oSCCODVAL_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oSCCODVAL_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_SCCODVAL
     i_obj.ecpSave()
  endproc

  add object oSIMVAL_1_31 as StdField with uid="ZHDNFXTMCJ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 102652122,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=172, Top=216, InputMask=replicate('X',5)

  add object oSCCAOVAL_1_34 as StdField with uid="YNABPLUUIZ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_SCCAOVAL", cQueryName = "SCCAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio della valuta scadenze",;
    HelpContextID = 78381426,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=406, Top=216, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oSCCAOVAL_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (GETVALUT(g_PERVAL, 'VADATEUR')>=.w_SCDATDOC OR .w_CAOVAL=0)
    endwith
   endif
  endfunc

  add object oSCIMPSCA_1_35 as StdField with uid="TCLCAWLOGM",rtseq=35,rtrep=.f.,;
    cFormVar = "w_SCIMPSCA", cQueryName = "SCIMPSCA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo totale scadenza",;
    HelpContextID = 29909351,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=652, Top=216, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oSCIMPSCA_1_35.mHide()
    with this.Parent.oContained
      return (.w_PFLCRSA='S' or (.w_ORIGINE='A' AND g_COGE<>'S' AND isalt()))
    endwith
  endfunc


  add object oLinkPC_1_39 as stdDynamicChildContainer with uid="DYPIAOOLWG",left=3, top=247, width=791, height=192, bOnScreen=.t.;



  add object oBtn_1_41 as StdButton with uid="XDIQWRXMPO",left=732, top=162, width=48,height=45,;
    CpPicture="BMP\AUTO.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per calcolo automatico scadenze";
    , HelpContextID = 215002662;
    , Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      with this.Parent.oContained
        .GSTE_MPA.NotifyEvent("Calcola")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_41.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_PFLCRSA='S')
     endwith
    endif
  endfunc

  add object oSCBANNOS_1_64 as StdField with uid="UPBKAHENIN",rtseq=46,rtrep=.f.,;
    cFormVar = "w_SCBANNOS", cQueryName = "SCBANNOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Ns. C/C. comunicato a fornitore per RB o a cliente per bonifico",;
    HelpContextID = 56888967,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=112, Top=189, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_SCBANNOS"

  func oSCBANNOS_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPCLF $ 'CF')
    endwith
   endif
  endfunc

  func oSCBANNOS_1_64.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A')
    endwith
  endfunc

  func oSCBANNOS_1_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_64('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCBANNOS_1_64.ecpDrop(oSource)
    this.Parent.oContained.link_1_64('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCBANNOS_1_64.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oSCBANNOS_1_64'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banca",'',this.parent.oContained
  endproc
  proc oSCBANNOS_1_64.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_SCBANNOS
     i_obj.ecpSave()
  endproc


  add object oBtn_1_75 as StdButton with uid="ROXQRMEEWR",left=732, top=36, width=48,height=45,;
    CpPicture="BMP\Documenti.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento di origine";
    , HelpContextID = 242520038;
    , Caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_75.Click()
      with this.Parent.oContained
        GSAR_BKX(this.Parent.oContained,"O")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_75.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SCRIFDCO))
      endwith
    endif
  endfunc

  func oBtn_1_75.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_SCRIFDCO) Or .w_Origine='S')
     endwith
    endif
  endfunc


  add object oObj_1_77 as cp_runprogram with uid="AETCMLBHTW",left=5, top=482, width=238,height=28,;
    caption='GSAR_BKS(Change)',;
   bGlobalFont=.t.,;
    prg="GSAR_BKS('Change')",;
    cEvent = "w_PFLCRSA Changed",;
    nPag=1;
    , HelpContextID = 308834

  add object oStr_1_37 as StdString with uid="EIGKTXWVTG",Visible=.t., Left=2, Top=7,;
    Alignment=1, Width=108, Height=15,;
    Caption="Codice scadenza:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="NUMCUXMVGQ",Visible=.t., Left=558, Top=216,;
    Alignment=1, Width=92, Height=15,;
    Caption="Importo totale:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_PFLCRSA='S' or (.w_ORIGINE='A' AND g_COGE<>'S' AND isalt()))
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="GDPNMAUJZH",Visible=.t., Left=2, Top=111,;
    Alignment=1, Width=108, Height=15,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="OJLPWPRIWL",Visible=.t., Left=338, Top=216,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="GUAJMLTWWM",Visible=.t., Left=402, Top=7,;
    Alignment=1, Width=48, Height=15,;
    Caption="Doc.N.:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A' AND g_COGE<>'S' AND isalt())
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="CMIRATJETH",Visible=.t., Left=668, Top=7,;
    Alignment=1, Width=38, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A' AND g_COGE<>'S' AND isalt())
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="JQGVRJUUVX",Visible=.t., Left=571, Top=7,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A' AND g_COGE<>'S' AND isalt())
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="FKKXMBEZLH",Visible=.t., Left=2, Top=163,;
    Alignment=1, Width=108, Height=15,;
    Caption="Banca appoggio:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="ZPTUOQVOQL",Visible=.t., Left=2, Top=32,;
    Alignment=1, Width=108, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="FNHXNPSNNY",Visible=.t., Left=2, Top=215,;
    Alignment=1, Width=108, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="DIYUPOMFXJ",Visible=.t., Left=2, Top=56,;
    Alignment=1, Width=108, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="MJYZRSJUXC",Visible=.t., Left=2, Top=85,;
    Alignment=1, Width=108, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_SCTIPCLF<>'C')
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="JDZCQKRCGH",Visible=.t., Left=2, Top=85,;
    Alignment=1, Width=108, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (.w_SCTIPCLF<>'G')
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="DGKQGMWIMP",Visible=.t., Left=2, Top=85,;
    Alignment=1, Width=108, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (.w_SCTIPCLF<>'F')
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="NIQVPKOGKZ",Visible=.t., Left=344, Top=57,;
    Alignment=1, Width=49, Height=18,;
    Caption="Segno:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_PFLCRSA='S')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="RSZFZTEHGA",Visible=.t., Left=2, Top=189,;
    Alignment=1, Width=108, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A')
    endwith
  endfunc

  add object oStr_1_67 as StdString with uid="ASMOHPDTRF",Visible=.t., Left=2, Top=137,;
    Alignment=1, Width=108, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="LIIVPSDZLG",Visible=.t., Left=218, Top=7,;
    Alignment=1, Width=30, Height=18,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_70 as StdString with uid="SEURTUCAKB",Visible=.t., Left=234, Top=57,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_asc','SCA_VARI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SCCODICE=SCA_VARI.SCCODICE";
  +" and "+i_cAliasName2+".SCCODSEC=SCA_VARI.SCCODSEC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
