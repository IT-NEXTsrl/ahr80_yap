* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kai                                                        *
*              Abbinamento partite/scadenze                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_228]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-01                                                      *
* Last revis.: 2014-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kai",oParentObject))

* --- Class definition
define class tgste_kai as StdForm
  Top    = 88
  Left   = 4

  * --- Standard Properties
  Width  = 798
  Height = 391+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-04"
  HelpContextID=132789353
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=52

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  COC_MAST_IDX = 0
  cPrg = "gste_kai"
  cComment = "Abbinamento partite/scadenze"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPO = space(1)
  o_TIPO = space(1)
  w_CODCON = space(15)
  w_DESCRI = space(40)
  w_CODCON1 = space(15)
  w_TIPPAR = space(1)
  o_TIPPAR = space(1)
  w_CODBAN1 = space(15)
  w_TIPMAN = space(1)
  w_TIPEFF = space(10)
  w_FLPART = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_CODEFF = space(15)
  w_DESBAN1 = space(35)
  w_DESEFF = space(40)
  w_SELEZI = space(1)
  w_TESCAR = .F.
  w_NUMDIS = space(10)
  o_NUMDIS = space(10)
  w_TIPSCA = space(1)
  w_CODCAU = space(5)
  w_TIPSOT = space(1)
  w_TIPBAN = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_CODVAL = space(3)
  w_FLRD1 = space(2)
  w_FLRI1 = space(2)
  w_FLMA1 = space(2)
  w_FLRB1 = space(2)
  w_FLBO1 = space(2)
  w_FLCA1 = space(2)
  w_SCAINI1 = ctod('  /  /  ')
  w_SCAFIN1 = ctod('  /  /  ')
  w_CODVAL1 = space(3)
  w_TIPSCA1 = space(1)
  w_FLBANC1 = space(1)
  w_CODABI1 = space(5)
  w_FLCABI1 = space(1)
  w_BANRIF = space(15)
  w_TIPDIS = space(2)
  w_FLSELE = 0
  w_DESCR1 = space(40)
  w_SCELTA = space(10)
  w_TIPO1 = space(1)
  w_CODBAN2 = space(15)
  w_DESBAN2 = space(35)
  w_FLIBAN1 = space(1)
  w_IMPORTO = 0
  w_DISERIAL = space(10)
  w_TIPPAR1 = space(1)
  o_TIPPAR1 = space(1)
  w_CODEFF1 = space(15)
  w_DESEFF1 = space(40)
  w_FLPART1 = space(1)

  * --- Children pointers
  GSTE_MCF = .NULL.
  w_CalcZoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gste_kai
  * Disabilito la croce in alto a destra, per evitare la
  * chiamata alla Queryunload che provocherebbe la chiamata
  * alla SAVE_gste_mcf con conseguente messaggio (Salva modifiche..)
  closable=.f.
  
  * Serve per bloccare il messaggio se si preme esc
  * Nella ecpQuit() cFunction vale Filter e con la propriet� assign
  * la cFunction assume questo valore
  * Testando xvalue viene data la possibilit� di modificare il valore delle propriet�
  PROC cFunction_assign(xvalue)
     this.cFunction=xvalue
       if xvalue='Filter'
         this.GSTE_MCF.bUpdated=.f.
       endif
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kaiPag1","gste_kai",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Partite/Scadenze")
      .Pages(2).addobject("oPag","tgste_kaiPag2","gste_kai",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Elenco clienti/fornitori")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CalcZoom = this.oPgFrm.Pages(1).oPag.CalcZoom
    DoDefault()
    proc Destroy()
      this.w_CalcZoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='COC_MAST'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSTE_MCF = CREATEOBJECT('stdDynamicChild',this,'GSTE_MCF',this.oPgFrm.Page2.oPag.oLinkPC_2_3)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSTE_MCF)
      this.GSTE_MCF.DestroyChildrenChain()
      this.GSTE_MCF=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_3')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSTE_MCF.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSTE_MCF.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSTE_MCF.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSTE_MCF.SetKey(;
            .w_NUMDIS,"DICODDIS";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSTE_MCF.ChangeRow(this.cRowID+'      1',1;
             ,.w_NUMDIS,"DICODDIS";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSTE_MCF)
        i_f=.GSTE_MCF.BuildFilter()
        if !(i_f==.GSTE_MCF.cQueryFilter)
          i_fnidx=.GSTE_MCF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSTE_MCF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSTE_MCF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSTE_MCF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSTE_MCF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSTE_MCF(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSTE_MCF.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSTE_MCF(i_ask)
    if this.GSTE_MCF.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (ELENCO CLI/FOR)'))
      cp_BeginTrs()
      this.GSTE_MCF.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_kai
            * istanzio immediatamente il figlio della seconda pagina
            * perch� viene richiamato per i calcoli
             if Upper(this.GSTE_MCF.class)='STDDYNAMICCHILD'
               This.oPgFrm.Pages[2].opag.uienable(.T.)
               This.oPgFrm.ActivePage=1
             Endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPO=space(1)
      .w_CODCON=space(15)
      .w_DESCRI=space(40)
      .w_CODCON1=space(15)
      .w_TIPPAR=space(1)
      .w_CODBAN1=space(15)
      .w_TIPMAN=space(1)
      .w_TIPEFF=space(10)
      .w_FLPART=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_CODEFF=space(15)
      .w_DESBAN1=space(35)
      .w_DESEFF=space(40)
      .w_SELEZI=space(1)
      .w_TESCAR=.f.
      .w_NUMDIS=space(10)
      .w_TIPSCA=space(1)
      .w_CODCAU=space(5)
      .w_TIPSOT=space(1)
      .w_TIPBAN=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_CODVAL=space(3)
      .w_FLRD1=space(2)
      .w_FLRI1=space(2)
      .w_FLMA1=space(2)
      .w_FLRB1=space(2)
      .w_FLBO1=space(2)
      .w_FLCA1=space(2)
      .w_SCAINI1=ctod("  /  /  ")
      .w_SCAFIN1=ctod("  /  /  ")
      .w_CODVAL1=space(3)
      .w_TIPSCA1=space(1)
      .w_FLBANC1=space(1)
      .w_CODABI1=space(5)
      .w_FLCABI1=space(1)
      .w_BANRIF=space(15)
      .w_TIPDIS=space(2)
      .w_FLSELE=0
      .w_DESCR1=space(40)
      .w_SCELTA=space(10)
      .w_TIPO1=space(1)
      .w_CODBAN2=space(15)
      .w_DESBAN2=space(35)
      .w_FLIBAN1=space(1)
      .w_IMPORTO=0
      .w_DISERIAL=space(10)
      .w_TIPPAR1=space(1)
      .w_CODEFF1=space(15)
      .w_DESEFF1=space(40)
      .w_FLPART1=space(1)
      .w_TIPO=oParentObject.w_TIPO
      .w_NUMDIS=oParentObject.w_NUMDIS
      .w_TIPSCA=oParentObject.w_TIPSCA
      .w_CODCAU=oParentObject.w_CODCAU
      .w_SCAINI=oParentObject.w_SCAINI
      .w_SCAFIN=oParentObject.w_SCAFIN
      .w_CODVAL=oParentObject.w_CODVAL
      .w_FLRD1=oParentObject.w_FLRD1
      .w_FLRI1=oParentObject.w_FLRI1
      .w_FLMA1=oParentObject.w_FLMA1
      .w_FLRB1=oParentObject.w_FLRB1
      .w_FLBO1=oParentObject.w_FLBO1
      .w_FLCA1=oParentObject.w_FLCA1
      .w_FLBANC1=oParentObject.w_FLBANC1
      .w_CODABI1=oParentObject.w_CODABI1
      .w_FLCABI1=oParentObject.w_FLCABI1
      .w_BANRIF=oParentObject.w_BANRIF
      .w_TIPDIS=oParentObject.w_TIPDIS
      .w_FLIBAN1=oParentObject.w_FLIBAN1
      .w_DISERIAL=oParentObject.w_DISERIAL
          .DoRTCalc(1,1,.f.)
        .w_CODCON = SPACE(15)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODCON))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_CODCON1 = SPACE(15)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODCON1))
          .link_1_4('Full')
        endif
        .w_TIPPAR = 'T'
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODBAN1))
          .link_1_6('Full')
        endif
        .w_TIPMAN = THIS.OPARENTOBJECT.OPARENTOBJECT .w_DITIPMAN
        .w_TIPEFF = 'G'
          .DoRTCalc(9,10,.f.)
        .w_CODEFF = SPACE(15)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODEFF))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,12,.f.)
        .w_DESEFF = SPACE(40)
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.CalcZoom.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .w_TESCAR = .F.
          .DoRTCalc(16,19,.f.)
        .w_TIPBAN = 'G'
        .w_OBTEST = .w_SCAINI
          .DoRTCalc(22,30,.f.)
        .w_SCAINI1 = .w_SCAINI
        .w_SCAFIN1 = .w_SCAFIN
        .w_CODVAL1 = .w_CODVAL
        .w_TIPSCA1 = .w_TIPSCA
          .DoRTCalc(35,39,.f.)
        .w_FLSELE = 0
      .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
      .GSTE_MCF.NewDocument()
      .GSTE_MCF.ChangeRow('1',1,.w_NUMDIS,"DICODDIS")
      if not(.GSTE_MCF.bLoaded)
        .GSTE_MCF.SetKey(.w_NUMDIS,"DICODDIS")
      endif
          .DoRTCalc(41,42,.f.)
        .w_TIPO1 = THIS.OPARENTOBJECT.OPARENTOBJECT .w_TIPCON
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_CODBAN2))
          .link_2_5('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
          .DoRTCalc(45,48,.f.)
        .w_TIPPAR1 = 'T'
        .w_CODEFF1 = SPACE(15)
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_CODEFF1))
          .link_2_9('Full')
        endif
        .w_DESEFF1 = SPACE(40)
    endwith
    this.DoRTCalc(52,52,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSTE_MCF.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSTE_MCF.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIPO=.w_TIPO
      .oParentObject.w_NUMDIS=.w_NUMDIS
      .oParentObject.w_TIPSCA=.w_TIPSCA
      .oParentObject.w_CODCAU=.w_CODCAU
      .oParentObject.w_SCAINI=.w_SCAINI
      .oParentObject.w_SCAFIN=.w_SCAFIN
      .oParentObject.w_CODVAL=.w_CODVAL
      .oParentObject.w_FLRD1=.w_FLRD1
      .oParentObject.w_FLRI1=.w_FLRI1
      .oParentObject.w_FLMA1=.w_FLMA1
      .oParentObject.w_FLRB1=.w_FLRB1
      .oParentObject.w_FLBO1=.w_FLBO1
      .oParentObject.w_FLCA1=.w_FLCA1
      .oParentObject.w_FLBANC1=.w_FLBANC1
      .oParentObject.w_CODABI1=.w_CODABI1
      .oParentObject.w_FLCABI1=.w_FLCABI1
      .oParentObject.w_BANRIF=.w_BANRIF
      .oParentObject.w_TIPDIS=.w_TIPDIS
      .oParentObject.w_FLIBAN1=.w_FLIBAN1
      .oParentObject.w_DISERIAL=.w_DISERIAL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_TIPO<>.w_TIPO
            .w_CODCON = SPACE(15)
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.t.)
        if .o_TIPO<>.w_TIPO
            .w_CODCON1 = SPACE(15)
          .link_1_4('Full')
        endif
        .DoRTCalc(5,7,.t.)
            .w_TIPEFF = 'G'
        .DoRTCalc(9,10,.t.)
        if .o_TIPPAR<>.w_TIPPAR
            .w_CODEFF = SPACE(15)
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.t.)
        if .o_TIPPAR<>.w_TIPPAR
            .w_DESEFF = SPACE(40)
        endif
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .DoRTCalc(14,14,.t.)
            .w_TESCAR = .F.
        .DoRTCalc(16,19,.t.)
            .w_TIPBAN = 'G'
        .DoRTCalc(21,30,.t.)
            .w_SCAINI1 = .w_SCAINI
            .w_SCAFIN1 = .w_SCAFIN
            .w_CODVAL1 = .w_CODVAL
            .w_TIPSCA1 = .w_TIPSCA
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .DoRTCalc(35,49,.t.)
        if .o_TIPPAR1<>.w_TIPPAR1
            .w_CODEFF1 = SPACE(15)
          .link_2_9('Full')
        endif
        if .o_TIPPAR1<>.w_TIPPAR1
            .w_DESEFF1 = SPACE(40)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_NUMDIS<>.o_NUMDIS
          .Save_GSTE_MCF(.t.)
          .GSTE_MCF.NewDocument()
          .GSTE_MCF.ChangeRow('1',1,.w_NUMDIS,"DICODDIS")
          if not(.GSTE_MCF.bLoaded)
            .GSTE_MCF.SetKey(.w_NUMDIS,"DICODDIS")
          endif
        endif
      endwith
      this.DoRTCalc(52,52,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCON_1_2.enabled = this.oPgFrm.Page1.oPag.oCODCON_1_2.mCond()
    this.oPgFrm.Page1.oPag.oCODCON1_1_4.enabled = this.oPgFrm.Page1.oPag.oCODCON1_1_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPMAN_1_7.visible=!this.oPgFrm.Page1.oPag.oTIPMAN_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCODEFF_1_11.visible=!this.oPgFrm.Page1.oPag.oCODEFF_1_11.mHide()
    this.oPgFrm.Page1.oPag.oDESEFF_1_13.visible=!this.oPgFrm.Page1.oPag.oDESEFF_1_13.mHide()
    this.oPgFrm.Page2.oPag.oCODEFF1_2_9.visible=!this.oPgFrm.Page2.oPag.oCODEFF1_2_9.mHide()
    this.oPgFrm.Page2.oPag.oDESEFF1_2_10.visible=!this.oPgFrm.Page2.oPag.oDESEFF1_2_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_13.visible=!this.oPgFrm.Page2.oPag.oStr_2_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.CalcZoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCON
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_2'),i_cWhere,'GSAR_BZC',"Clienti / fornitori /conti",'CONTIZOOM1.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON1
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_CODCON1))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON1)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON1)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON1) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON1_1_4'),i_cWhere,'GSAR_BZC',"Clienti / fornitori /conti",'CONTIZOOM1.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON1);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_CODCON1)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON1 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON1 = space(15)
      endif
      this.w_DESCR1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBAN1
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBAN1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CODBAN1)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CODBAN1))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBAN1)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_CODBAN1)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_CODBAN1)+"%");

            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODBAN1) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCODBAN1_1_6'),i_cWhere,'GSTE_ACB',"Conti banca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBAN1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CODBAN1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CODBAN1)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBAN1 = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBAN1 = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODBAN1 = space(15)
      endif
      this.w_DESBAN1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBAN1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODEFF
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODEFF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODEFF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPEFF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPEFF;
                     ,'ANCODICE',trim(this.w_CODEFF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODEFF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODEFF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPEFF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODEFF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPEFF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODEFF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODEFF_1_11'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPEFF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPEFF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODEFF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODEFF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPEFF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPEFF;
                       ,'ANCODICE',this.w_CODEFF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODEFF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESEFF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_FLPART = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODEFF = space(15)
      endif
      this.w_DESEFF = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLPART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_FLPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODEFF = space(15)
        this.w_DESEFF = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLPART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODEFF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBAN2
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBAN2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CODBAN2)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CODBAN2))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBAN2)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_CODBAN2)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_CODBAN2)+"%");

            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODBAN2) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCODBAN2_2_5'),i_cWhere,'GSTE_ACB',"Conti banca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBAN2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CODBAN2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CODBAN2)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBAN2 = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBAN2 = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODBAN2 = space(15)
      endif
      this.w_DESBAN2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBAN2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODEFF1
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODEFF1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODEFF1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPEFF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPEFF;
                     ,'ANCODICE',trim(this.w_CODEFF1))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODEFF1)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODEFF1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPEFF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODEFF1)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPEFF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODEFF1) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODEFF1_2_9'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPEFF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPEFF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODEFF1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODEFF1);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPEFF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPEFF;
                       ,'ANCODICE',this.w_CODEFF1)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODEFF1 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESEFF1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_FLPART1 = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODEFF1 = space(15)
      endif
      this.w_DESEFF1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLPART1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_FLPART1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODEFF1 = space(15)
        this.w_DESEFF1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLPART1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODEFF1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_1.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_2.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_2.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_3.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_3.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON1_1_4.value==this.w_CODCON1)
      this.oPgFrm.Page1.oPag.oCODCON1_1_4.value=this.w_CODCON1
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPPAR_1_5.RadioValue()==this.w_TIPPAR)
      this.oPgFrm.Page1.oPag.oTIPPAR_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODBAN1_1_6.value==this.w_CODBAN1)
      this.oPgFrm.Page1.oPag.oCODBAN1_1_6.value=this.w_CODBAN1
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPMAN_1_7.RadioValue()==this.w_TIPMAN)
      this.oPgFrm.Page1.oPag.oTIPMAN_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODEFF_1_11.value==this.w_CODEFF)
      this.oPgFrm.Page1.oPag.oCODEFF_1_11.value=this.w_CODEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBAN1_1_12.value==this.w_DESBAN1)
      this.oPgFrm.Page1.oPag.oDESBAN1_1_12.value=this.w_DESBAN1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESEFF_1_13.value==this.w_DESEFF)
      this.oPgFrm.Page1.oPag.oDESEFF_1_13.value=this.w_DESEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_15.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR1_1_48.value==this.w_DESCR1)
      this.oPgFrm.Page1.oPag.oDESCR1_1_48.value=this.w_DESCR1
    endif
    if not(this.oPgFrm.Page2.oPag.oCODBAN2_2_5.value==this.w_CODBAN2)
      this.oPgFrm.Page2.oPag.oCODBAN2_2_5.value=this.w_CODBAN2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESBAN2_2_6.value==this.w_DESBAN2)
      this.oPgFrm.Page2.oPag.oDESBAN2_2_6.value=this.w_DESBAN2
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPORTO_1_53.value==this.w_IMPORTO)
      this.oPgFrm.Page1.oPag.oIMPORTO_1_53.value=this.w_IMPORTO
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPPAR1_2_8.RadioValue()==this.w_TIPPAR1)
      this.oPgFrm.Page2.oPag.oTIPPAR1_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODEFF1_2_9.value==this.w_CODEFF1)
      this.oPgFrm.Page2.oPag.oCODEFF1_2_9.value=this.w_CODEFF1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESEFF1_2_10.value==this.w_DESEFF1)
      this.oPgFrm.Page2.oPag.oDESEFF1_2_10.value=this.w_DESEFF1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_FLPART))  and not(.w_TIPPAR<>'C')  and not(empty(.w_CODEFF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODEFF_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_FLPART1))  and not(.w_TIPPAR1<>'C')  and not(empty(.w_CODEFF1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODEFF1_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSTE_MCF.CheckForm()
      if i_bres
        i_bres=  .GSTE_MCF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPO = this.w_TIPO
    this.o_TIPPAR = this.w_TIPPAR
    this.o_NUMDIS = this.w_NUMDIS
    this.o_TIPPAR1 = this.w_TIPPAR1
    * --- GSTE_MCF : Depends On
    this.GSTE_MCF.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgste_kaiPag1 as StdContainer
  Width  = 794
  height = 391
  stdWidth  = 794
  stdheight = 391
  resizeXpos=600
  resizeYpos=211
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPO_1_1 as StdCombo with uid="HFTBHBAKBJ",value=3,rtseq=1,rtrep=.f.,left=97,top=10,width=78,height=21;
    , ToolTipText = "Tipo conto di selezione";
    , HelpContextID = 127264202;
    , cFormVar="w_TIPO",RowSource=""+"Cliente,"+"Fornitore,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_1_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oTIPO_1_1.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_1.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='C',1,;
      iif(this.Parent.oContained.w_TIPO=='F',2,;
      iif(this.Parent.oContained.w_TIPO=='',3,;
      0)))
  endfunc

  func oTIPO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_1_2('Full')
      endif
      if .not. empty(.w_CODCON1)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCON_1_2 as StdField with uid="AIVARYGOQI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente fornitore",;
    HelpContextID = 189620006,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=293, Top=10, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPO $ 'CFG')
    endwith
   endif
  endfunc

  func oCODCON_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti / fornitori /conti",'CONTIZOOM1.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_3 as StdField with uid="FNQPEVDWEX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 108938550,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=427, Top=10, InputMask=replicate('X',40)

  add object oCODCON1_1_4 as StdField with uid="XSMSRKGQOY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODCON1", cQueryName = "CODCON1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente fornitore",;
    HelpContextID = 189620006,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=293, Top=35, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON1"

  func oCODCON1_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPO $ 'CFG')
    endwith
   endif
  endfunc

  func oCODCON1_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON1_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON1_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON1_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti / fornitori /conti",'CONTIZOOM1.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON1_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON1
     i_obj.ecpSave()
  endproc


  add object oTIPPAR_1_5 as StdCombo with uid="FJGKCMJXKZ",rtseq=5,rtrep=.f.,left=97,top=54,width=78,height=21;
    , ToolTipText = "Tipo partite: chiuse (con la contabilizzazione indiretta), aperte (escludendo quelle derivanti dalla contabilizzazione indiretta)";
    , HelpContextID = 242948662;
    , cFormVar="w_TIPPAR",RowSource=""+"Chiuse,"+"Aperte,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPPAR_1_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPPAR_1_5.GetRadio()
    this.Parent.oContained.w_TIPPAR = this.RadioValue()
    return .t.
  endfunc

  func oTIPPAR_1_5.SetRadio()
    this.Parent.oContained.w_TIPPAR=trim(this.Parent.oContained.w_TIPPAR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPPAR=='C',1,;
      iif(this.Parent.oContained.w_TIPPAR=='A',2,;
      iif(this.Parent.oContained.w_TIPPAR=='T',3,;
      0)))
  endfunc

  add object oCODBAN1_1_6 as StdField with uid="BTYBCMPDML",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODBAN1", cQueryName = "CODBAN1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Ns. C/C. comunicato a fornitore per RB o a cliente per bonifico",;
    HelpContextID = 174874406,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=293, Top=60, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CODBAN1"

  func oCODBAN1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBAN1_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBAN1_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCODBAN1_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banca",'',this.parent.oContained
  endproc
  proc oCODBAN1_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CODBAN1
     i_obj.ecpSave()
  endproc


  add object oTIPMAN_1_7 as StdCombo with uid="DLBXYBEPMG",rtseq=7,rtrep=.f.,left=97,top=82,width=78,height=21, enabled=.f.;
    , ToolTipText = "Tipo SDD a cui devono appartenere le partite da inserire in distinta";
    , HelpContextID = 175643190;
    , cFormVar="w_TIPMAN",RowSource=""+"CORE,"+"B2B,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPMAN_1_7.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'B',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPMAN_1_7.GetRadio()
    this.Parent.oContained.w_TIPMAN = this.RadioValue()
    return .t.
  endfunc

  func oTIPMAN_1_7.SetRadio()
    this.Parent.oContained.w_TIPMAN=trim(this.Parent.oContained.w_TIPMAN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPMAN=='C',1,;
      iif(this.Parent.oContained.w_TIPMAN=='B',2,;
      iif(this.Parent.oContained.w_TIPMAN=='T',3,;
      0)))
  endfunc

  func oTIPMAN_1_7.mHide()
    with this.Parent.oContained
      return (.w_FLRI1<>'RI')
    endwith
  endfunc

  add object oCODEFF_1_11 as StdField with uid="BJCCKRRFIH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODEFF", cQueryName = "CODEFF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto effetti attivi da contabilizzazione indiretta effetti",;
    HelpContextID = 46096166,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=293, Top=85, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPEFF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODEFF"

  func oCODEFF_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPPAR<>'C')
    endwith
  endfunc

  func oCODEFF_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODEFF_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODEFF_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPEFF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPEFF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODEFF_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCODEFF_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPEFF
     i_obj.w_ANCODICE=this.parent.oContained.w_CODEFF
     i_obj.ecpSave()
  endproc

  add object oDESBAN1_1_12 as StdField with uid="PPISEPNBCE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESBAN1", cQueryName = "DESBAN1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 174933302,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=427, Top=60, InputMask=replicate('X',35)

  add object oDESEFF_1_13 as StdField with uid="ZTAGAZANCI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESEFF", cQueryName = "DESEFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 46155062,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=427, Top=85, InputMask=replicate('X',40)

  func oDESEFF_1_13.mHide()
    with this.Parent.oContained
      return (.w_TIPPAR<>'C')
    endwith
  endfunc


  add object oBtn_1_14 as StdButton with uid="VQOIOXFDSR",left=744, top=11, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 44132886;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSTE_BAI(this.Parent.oContained,"CALC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_15 as StdRadio with uid="ZBKHPAZPPX",rtseq=14,rtrep=.f.,left=10, top=346, width=133,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_15.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 117429798
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 117429798
      this.Buttons(2).Top=15
      this.SetAll("Width",131)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_15.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_15.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_16 as StdButton with uid="HZMWVAOJPN",left=695, top=346, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per abbinare le partite selezionate alla distinta";
    , HelpContextID = 132760602;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSTE_BAI(this.Parent.oContained,"AGGIO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="GFOYFENFSM",left=744, top=346, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 125471930;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object CalcZoom as cp_szoombox with uid="ONSHUZMTSI",left=1, top=109, width=797,height=235,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PAR_TITE",cZoomFile="GSTE_KAI",bQueryOnLoad=.f.,bOptions=.f.,bQueryOnDblClick=.t.,bReadOnly=.f.,bAdvOptions=.f.,cMenuFile="",cZoomOnZoom="",bNoZoomGridShape=.f.,;
    cEvent = "Abbina",;
    nPag=1;
    , HelpContextID = 45208294


  add object oObj_1_19 as cp_runprogram with uid="OOFZJBQMXG",left=367, top=422, width=244,height=21,;
    caption='GSTE_BAI(SELE)',;
   bGlobalFont=.t.,;
    prg='GSTE_BAI("SELE")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 233900335


  add object oObj_1_47 as cp_runprogram with uid="UXGXHPXMHM",left=367, top=403, width=183,height=19,;
    caption='GSTE_BAI(CALC)',;
   bGlobalFont=.t.,;
    prg='GSTE_BAI("CALC")',;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 231782703

  add object oDESCR1_1_48 as StdField with uid="SJMIHCXFII",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESCR1", cQueryName = "DESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 243156278,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=427, Top=35, InputMask=replicate('X',40)

  add object oIMPORTO_1_53 as StdField with uid="YPRBLYBBJV",rtseq=47,rtrep=.f.,;
    cFormVar = "w_IMPORTO", cQueryName = "IMPORTO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25828742,;
   bGlobalFont=.t.,;
    Height=21, Width=129, Left=462, Top=351, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"


  add object oObj_1_54 as cp_runprogram with uid="VNXVORMCTZ",left=367, top=442, width=244,height=21,;
    caption='GSTE_BAI(CHEK)',;
   bGlobalFont=.t.,;
    prg='GSTE_BAI("CHEK")',;
    cEvent = "w_calczoom row checked, w_calczoom row unchecked",;
    nPag=1;
    , HelpContextID = 239741231

  add object oStr_1_20 as StdString with uid="RHIEMUIROZ",Visible=.t., Left=204, Top=60,;
    Alignment=1, Width=86, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="HVDOPGRIMD",Visible=.t., Left=16, Top=10,;
    Alignment=1, Width=79, Height=15,;
    Caption="Selezioni:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="LISHOXLMQF",Visible=.t., Left=256, Top=10,;
    Alignment=1, Width=34, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="PBNGYIJRCM",Visible=.t., Left=271, Top=35,;
    Alignment=1, Width=19, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="HZNAGVALMR",Visible=.t., Left=200, Top=351,;
    Alignment=1, Width=260, Height=18,;
    Caption="Totale scadenze selezionate:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="CGOFLZRUHL",Visible=.t., Left=40, Top=57,;
    Alignment=1, Width=55, Height=18,;
    Caption="Partite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="KFFSXBSQRT",Visible=.t., Left=204, Top=85,;
    Alignment=1, Width=86, Height=18,;
    Caption="Conto effetti:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_TIPPAR<>'C')
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="GFBCBEDXDK",Visible=.t., Left=34, Top=85,;
    Alignment=1, Width=61, Height=18,;
    Caption="Tipo SDD:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (.w_FLRI1<>'RI')
    endwith
  endfunc
enddefine
define class tgste_kaiPag2 as StdContainer
  Width  = 794
  height = 391
  stdWidth  = 794
  stdheight = 391
  resizeXpos=480
  resizeYpos=278
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_2_2 as StdButton with uid="OQGYEWWBIT",left=681, top=13, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , HelpContextID = 44132886;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      with this.Parent.oContained
        GSTE_BAI(this.Parent.oContained,"CARI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oLinkPC_2_3 as stdDynamicChildContainer with uid="EWSAJJAOUL",left=13, top=64, width=716, height=318, bOnScreen=.t.;


  add object oCODBAN2_2_5 as StdField with uid="LMJRXUDBCV",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CODBAN2", cQueryName = "CODBAN2",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Ns. C/C. comunicato a fornitore per RB o a cliente per bonifico",;
    HelpContextID = 174874406,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=97, Top=13, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CODBAN2"

  func oCODBAN2_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBAN2_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBAN2_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCODBAN2_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banca",'',this.parent.oContained
  endproc
  proc oCODBAN2_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CODBAN2
     i_obj.ecpSave()
  endproc

  add object oDESBAN2_2_6 as StdField with uid="JPAKDAKFWD",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESBAN2", cQueryName = "DESBAN2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 174933302,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=243, Top=13, InputMask=replicate('X',35)


  add object oTIPPAR1_2_8 as StdCombo with uid="CCMKMNMFVV",rtseq=49,rtrep=.f.,left=97,top=38,width=69,height=21;
    , ToolTipText = "Tipo partite: chiuse (con la contabilizzazione indiretta), aperte (escludendo quelle derivanti dalla contabilizzazione indiretta)";
    , HelpContextID = 242948662;
    , cFormVar="w_TIPPAR1",RowSource=""+"Chiuse,"+"Aperte,"+"Tutte", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPPAR1_2_8.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPPAR1_2_8.GetRadio()
    this.Parent.oContained.w_TIPPAR1 = this.RadioValue()
    return .t.
  endfunc

  func oTIPPAR1_2_8.SetRadio()
    this.Parent.oContained.w_TIPPAR1=trim(this.Parent.oContained.w_TIPPAR1)
    this.value = ;
      iif(this.Parent.oContained.w_TIPPAR1=='C',1,;
      iif(this.Parent.oContained.w_TIPPAR1=='A',2,;
      iif(this.Parent.oContained.w_TIPPAR1=='T',3,;
      0)))
  endfunc

  add object oCODEFF1_2_9 as StdField with uid="FUNLLPXVCJ",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CODEFF1", cQueryName = "CODEFF1",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto effetti attivi da contabilizzazione indiretta effetti",;
    HelpContextID = 46096166,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=262, Top=38, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPEFF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODEFF1"

  func oCODEFF1_2_9.mHide()
    with this.Parent.oContained
      return (.w_TIPPAR1<>'C')
    endwith
  endfunc

  func oCODEFF1_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODEFF1_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODEFF1_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPEFF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPEFF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODEFF1_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCODEFF1_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPEFF
     i_obj.w_ANCODICE=this.parent.oContained.w_CODEFF1
     i_obj.ecpSave()
  endproc

  add object oDESEFF1_2_10 as StdField with uid="QHXUZDGMLL",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESEFF1", cQueryName = "DESEFF1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 46155062,;
   bGlobalFont=.t.,;
    Height=21, Width=283, Left=393, Top=38, InputMask=replicate('X',40)

  func oDESEFF1_2_10.mHide()
    with this.Parent.oContained
      return (.w_TIPPAR1<>'C')
    endwith
  endfunc

  add object oStr_2_7 as StdString with uid="PYSZVUIXXK",Visible=.t., Left=8, Top=13,;
    Alignment=1, Width=86, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="QJNDZDZMMC",Visible=.t., Left=39, Top=39,;
    Alignment=1, Width=55, Height=18,;
    Caption="Partite:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="DPTVOABNAH",Visible=.t., Left=175, Top=39,;
    Alignment=1, Width=86, Height=18,;
    Caption="Conto effetti:"  ;
  , bGlobalFont=.t.

  func oStr_2_13.mHide()
    with this.Parent.oContained
      return (.w_TIPPAR1<>'C')
    endwith
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gste_mcf",lower(this.oContained.GSTE_MCF.class))=0
        this.oContained.GSTE_MCF.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kai','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
