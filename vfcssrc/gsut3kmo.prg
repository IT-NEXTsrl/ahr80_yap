* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut3kmo                                                        *
*              Modifica politiche accessi                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-07-06                                                      *
* Last revis.: 2008-07-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut3kmo",oParentObject))

* --- Class definition
define class tgsut3kmo as StdForm
  Top    = 7
  Left   = 9

  * --- Standard Properties
  Width  = 513
  Height = 222
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-11"
  HelpContextID=65715351
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut3kmo"
  cComment = "Modifica politiche accessi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PROGRAM = space(20)
  w_PROGDES = space(50)
  w_PRPARAME = space(15)
  w_PROGRAMOLD = space(20)
  w_PROGDESOLD = space(50)
  w_PRPARAMEOLD = space(15)
  w_OK = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut3kmoPag1","gsut3kmo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPROGRAM_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PROGRAM=space(20)
      .w_PROGDES=space(50)
      .w_PRPARAME=space(15)
      .w_PROGRAMOLD=space(20)
      .w_PROGDESOLD=space(50)
      .w_PRPARAMEOLD=space(15)
      .w_OK=space(1)
      .w_PROGRAM=oParentObject.w_PROGRAM
      .w_PROGDES=oParentObject.w_PROGDES
      .w_PRPARAME=oParentObject.w_PRPARAME
      .w_OK=oParentObject.w_OK
          .DoRTCalc(1,3,.f.)
        .w_PROGRAMOLD = .w_PROGRAM
        .w_PROGDESOLD = .w_PROGDES
        .w_PRPARAMEOLD = .w_PRPARAME
        .w_OK = 'S'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PROGRAM=.w_PROGRAM
      .oParentObject.w_PROGDES=.w_PROGDES
      .oParentObject.w_PRPARAME=.w_PRPARAME
      .oParentObject.w_OK=.w_OK
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPROGRAM_1_1.value==this.w_PROGRAM)
      this.oPgFrm.Page1.oPag.oPROGRAM_1_1.value=this.w_PROGRAM
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGDES_1_2.value==this.w_PROGDES)
      this.oPgFrm.Page1.oPag.oPROGDES_1_2.value=this.w_PROGDES
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPARAME_1_3.value==this.w_PRPARAME)
      this.oPgFrm.Page1.oPag.oPRPARAME_1_3.value=this.w_PRPARAME
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGRAMOLD_1_4.value==this.w_PROGRAMOLD)
      this.oPgFrm.Page1.oPag.oPROGRAMOLD_1_4.value=this.w_PROGRAMOLD
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGDESOLD_1_5.value==this.w_PROGDESOLD)
      this.oPgFrm.Page1.oPag.oPROGDESOLD_1_5.value=this.w_PROGDESOLD
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPARAMEOLD_1_6.value==this.w_PRPARAMEOLD)
      this.oPgFrm.Page1.oPag.oPRPARAMEOLD_1_6.value=this.w_PRPARAMEOLD
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut3kmoPag1 as StdContainer
  Width  = 509
  height = 222
  stdWidth  = 509
  stdheight = 222
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPROGRAM_1_1 as StdField with uid="UZGFFNSKHA",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PROGRAM", cQueryName = "PROGRAM",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Funzione/voce di men�",;
    HelpContextID = 173474806,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=85, Top=118, InputMask=replicate('X',20)

  add object oPROGDES_1_2 as StdField with uid="SGXJEWJUZQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PROGDES", cQueryName = "PROGDES",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 42531850,;
   bGlobalFont=.t.,;
    Height=21, Width=252, Left=242, Top=118, InputMask=replicate('X',50)

  add object oPRPARAME_1_3 as StdField with uid="WMNACBIJVA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PRPARAME", cQueryName = "PRPARAME",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Parametro",;
    HelpContextID = 173085755,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=85, Top=141, InputMask=replicate('X',15)

  add object oPROGRAMOLD_1_4 as StdField with uid="LYTLLNRHYT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PROGRAMOLD", cQueryName = "PROGRAMOLD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Funzione/voce di men�",;
    HelpContextID = 173493509,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=85, Top=31, InputMask=replicate('X',20)

  add object oPROGDESOLD_1_5 as StdField with uid="DNHFMBJQUX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PROGDESOLD", cQueryName = "PROGDESOLD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 42513147,;
   bGlobalFont=.t.,;
    Height=21, Width=252, Left=242, Top=31, InputMask=replicate('X',50)

  add object oPRPARAMEOLD_1_6 as StdField with uid="LCCFIGANMJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PRPARAMEOLD", cQueryName = "PRPARAMEOLD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Parametro",;
    HelpContextID = 173385003,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=85, Top=54, InputMask=replicate('X',15)


  add object oBtn_1_15 as StdButton with uid="MJIKIMPCXE",left=395, top=174, width=48,height=45,;
    CpPicture="bmp\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Salva";
    , HelpContextID = 65715446;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="IPVICJNRQW",left=449, top=174, width=48,height=45,;
    CpPicture="bmp\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 65715446;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_7 as StdString with uid="WTAGENJPXF",Visible=.t., Left=4, Top=32,;
    Alignment=1, Width=78, Height=18,;
    Caption="Funzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="POKRXHDKFG",Visible=.t., Left=21, Top=55,;
    Alignment=1, Width=61, Height=18,;
    Caption="Parametro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="VAFYLTDIXT",Visible=.t., Left=21, Top=118,;
    Alignment=1, Width=61, Height=18,;
    Caption="Funzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="IGGFZXKQML",Visible=.t., Left=21, Top=141,;
    Alignment=1, Width=61, Height=18,;
    Caption="Parametro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="OZYQHQOUSP",Visible=.t., Left=19, Top=95,;
    Alignment=0, Width=98, Height=16,;
    Caption="Nuovi valori"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="QACCHVLOBD",Visible=.t., Left=19, Top=8,;
    Alignment=0, Width=85, Height=16,;
    Caption="Vecchi valori"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_12 as StdBox with uid="KTPBIVUYPS",left=14, top=23, width=486,height=61

  add object oBox_1_14 as StdBox with uid="OTRASRKNII",left=14, top=110, width=486,height=61
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut3kmo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
