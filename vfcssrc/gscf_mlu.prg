* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_mlu                                                        *
*              Notifica utenti                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-02-07                                                      *
* Last revis.: 2012-02-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscf_mlu")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscf_mlu")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscf_mlu")
  return

* --- Class definition
define class tgscf_mlu as StdPCForm
  Width  = 404
  Height = 239
  Top    = 10
  Left   = 229
  cComment = "Notifica utenti"
  cPrg = "gscf_mlu"
  HelpContextID=214578537
  add object cnt as tcgscf_mlu
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscf_mlu as PCContext
  w_OASERIAL = space(10)
  w_OASTAMP = space(10)
  w_OACODUTE = 0
  w_OANAMUTE = space(20)
  w_OAPOSTIN = space(1)
  w_OAREC_ID = 0
  w_OA__MAIL = space(1)
  proc Save(i_oFrom)
    this.w_OASERIAL = i_oFrom.w_OASERIAL
    this.w_OASTAMP = i_oFrom.w_OASTAMP
    this.w_OACODUTE = i_oFrom.w_OACODUTE
    this.w_OANAMUTE = i_oFrom.w_OANAMUTE
    this.w_OAPOSTIN = i_oFrom.w_OAPOSTIN
    this.w_OAREC_ID = i_oFrom.w_OAREC_ID
    this.w_OA__MAIL = i_oFrom.w_OA__MAIL
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_OASERIAL = this.w_OASERIAL
    i_oTo.w_OASTAMP = this.w_OASTAMP
    i_oTo.w_OACODUTE = this.w_OACODUTE
    i_oTo.w_OANAMUTE = this.w_OANAMUTE
    i_oTo.w_OAPOSTIN = this.w_OAPOSTIN
    i_oTo.w_OAREC_ID = this.w_OAREC_ID
    i_oTo.w_OA__MAIL = this.w_OA__MAIL
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscf_mlu as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 404
  Height = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-02-23"
  HelpContextID=214578537
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  LOGCNTUT_IDX = 0
  cpusers_IDX = 0
  cFile = "LOGCNTUT"
  cKeySelect = "OASERIAL,OASTAMP,OAREC_ID"
  cKeyWhere  = "OASERIAL=this.w_OASERIAL and OASTAMP=this.w_OASTAMP and OAREC_ID=this.w_OAREC_ID"
  cKeyDetail  = "OASERIAL=this.w_OASERIAL and OASTAMP=this.w_OASTAMP and OACODUTE=this.w_OACODUTE and OAREC_ID=this.w_OAREC_ID"
  cKeyWhereODBC = '"OASERIAL="+cp_ToStrODBC(this.w_OASERIAL)';
      +'+" and OASTAMP="+cp_ToStrODBC(this.w_OASTAMP)';
      +'+" and OAREC_ID="+cp_ToStrODBC(this.w_OAREC_ID)';

  cKeyDetailWhereODBC = '"OASERIAL="+cp_ToStrODBC(this.w_OASERIAL)';
      +'+" and OASTAMP="+cp_ToStrODBC(this.w_OASTAMP)';
      +'+" and OACODUTE="+cp_ToStrODBC(this.w_OACODUTE)';
      +'+" and OAREC_ID="+cp_ToStrODBC(this.w_OAREC_ID)';

  cKeyWhereODBCqualified = '"LOGCNTUT.OASERIAL="+cp_ToStrODBC(this.w_OASERIAL)';
      +'+" and LOGCNTUT.OASTAMP="+cp_ToStrODBC(this.w_OASTAMP)';
      +'+" and LOGCNTUT.OAREC_ID="+cp_ToStrODBC(this.w_OAREC_ID)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gscf_mlu"
  cComment = "Notifica utenti"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_OASERIAL = space(10)
  w_OASTAMP = space(10)
  w_OACODUTE = 0
  w_OANAMUTE = space(20)
  w_OAPOSTIN = space(1)
  w_OAREC_ID = 0
  w_OA__MAIL = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscf_mluPag1","gscf_mlu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='LOGCNTUT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.LOGCNTUT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.LOGCNTUT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscf_mlu'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from LOGCNTUT where OASERIAL=KeySet.OASERIAL
    *                            and OASTAMP=KeySet.OASTAMP
    *                            and OACODUTE=KeySet.OACODUTE
    *                            and OAREC_ID=KeySet.OAREC_ID
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.LOGCNTUT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOGCNTUT_IDX,2],this.bLoadRecFilter,this.LOGCNTUT_IDX,"gscf_mlu")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('LOGCNTUT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "LOGCNTUT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' LOGCNTUT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'OASERIAL',this.w_OASERIAL  ,'OASTAMP',this.w_OASTAMP  ,'OAREC_ID',this.w_OAREC_ID  )
      select * from (i_cTable) LOGCNTUT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OASERIAL = NVL(OASERIAL,space(10))
        .w_OASTAMP = NVL(OASTAMP,space(10))
        .w_OAREC_ID = NVL(OAREC_ID,0)
        cp_LoadRecExtFlds(this,'LOGCNTUT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_OANAMUTE = space(20)
          .w_OACODUTE = NVL(OACODUTE,0)
          .link_2_1('Load')
          .w_OAPOSTIN = NVL(OAPOSTIN,space(1))
          .w_OA__MAIL = NVL(OA__MAIL,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace OACODUTE with .w_OACODUTE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_OASERIAL=space(10)
      .w_OASTAMP=space(10)
      .w_OACODUTE=0
      .w_OANAMUTE=space(20)
      .w_OAPOSTIN=space(1)
      .w_OAREC_ID=0
      .w_OA__MAIL=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_OACODUTE))
         .link_2_1('Full')
        endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'LOGCNTUT')
    this.DoRTCalc(4,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'LOGCNTUT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.LOGCNTUT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OASERIAL,"OASERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OASTAMP,"OASTAMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OAREC_ID,"OAREC_ID",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_OACODUTE N(4);
      ,t_OANAMUTE C(20);
      ,t_OAPOSTIN N(3);
      ,t_OA__MAIL N(3);
      ,OACODUTE N(4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscf_mlubodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOACODUTE_2_1.controlsource=this.cTrsName+'.t_OACODUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOANAMUTE_2_2.controlsource=this.cTrsName+'.t_OANAMUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOAPOSTIN_2_3.controlsource=this.cTrsName+'.t_OAPOSTIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOA__MAIL_2_4.controlsource=this.cTrsName+'.t_OA__MAIL'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOACODUTE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.LOGCNTUT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOGCNTUT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.LOGCNTUT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOGCNTUT_IDX,2])
      *
      * insert into LOGCNTUT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'LOGCNTUT')
        i_extval=cp_InsertValODBCExtFlds(this,'LOGCNTUT')
        i_cFldBody=" "+;
                  "(OASERIAL,OASTAMP,OACODUTE,OAPOSTIN,OAREC_ID"+;
                  ",OA__MAIL,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_OASERIAL)+","+cp_ToStrODBC(this.w_OASTAMP)+","+cp_ToStrODBCNull(this.w_OACODUTE)+","+cp_ToStrODBC(this.w_OAPOSTIN)+","+cp_ToStrODBC(this.w_OAREC_ID)+;
             ","+cp_ToStrODBC(this.w_OA__MAIL)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'LOGCNTUT')
        i_extval=cp_InsertValVFPExtFlds(this,'LOGCNTUT')
        cp_CheckDeletedKey(i_cTable,0,'OASERIAL',this.w_OASERIAL,'OASTAMP',this.w_OASTAMP,'OACODUTE',this.w_OACODUTE,'OAREC_ID',this.w_OAREC_ID)
        INSERT INTO (i_cTable) (;
                   OASERIAL;
                  ,OASTAMP;
                  ,OACODUTE;
                  ,OAPOSTIN;
                  ,OAREC_ID;
                  ,OA__MAIL;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_OASERIAL;
                  ,this.w_OASTAMP;
                  ,this.w_OACODUTE;
                  ,this.w_OAPOSTIN;
                  ,this.w_OAREC_ID;
                  ,this.w_OA__MAIL;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.LOGCNTUT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOGCNTUT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_OACODUTE))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'LOGCNTUT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and OACODUTE="+cp_ToStrODBC(&i_TN.->OACODUTE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'LOGCNTUT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and OACODUTE=&i_TN.->OACODUTE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_OACODUTE))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and OACODUTE="+cp_ToStrODBC(&i_TN.->OACODUTE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and OACODUTE=&i_TN.->OACODUTE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace OACODUTE with this.w_OACODUTE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update LOGCNTUT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'LOGCNTUT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " OAPOSTIN="+cp_ToStrODBC(this.w_OAPOSTIN)+;
                     ",OA__MAIL="+cp_ToStrODBC(this.w_OA__MAIL)+;
                     ",OACODUTE="+cp_ToStrODBC(this.w_OACODUTE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and OACODUTE="+cp_ToStrODBC(OACODUTE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'LOGCNTUT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      OAPOSTIN=this.w_OAPOSTIN;
                     ,OA__MAIL=this.w_OA__MAIL;
                     ,OACODUTE=this.w_OACODUTE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and OACODUTE=&i_TN.->OACODUTE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.LOGCNTUT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOGCNTUT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_OACODUTE))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete LOGCNTUT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and OACODUTE="+cp_ToStrODBC(&i_TN.->OACODUTE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and OACODUTE=&i_TN.->OACODUTE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_OACODUTE))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.LOGCNTUT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOGCNTUT_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=OACODUTE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OACODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_OACODUTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_OACODUTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_OACODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oOACODUTE_2_1'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OACODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_OACODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_OACODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OACODUTE = NVL(_Link_.code,0)
      this.w_OANAMUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OACODUTE = 0
      endif
      this.w_OANAMUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OACODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOACODUTE_2_1.value==this.w_OACODUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOACODUTE_2_1.value=this.w_OACODUTE
      replace t_OACODUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOACODUTE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOANAMUTE_2_2.value==this.w_OANAMUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOANAMUTE_2_2.value=this.w_OANAMUTE
      replace t_OANAMUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOANAMUTE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOAPOSTIN_2_3.RadioValue()==this.w_OAPOSTIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOAPOSTIN_2_3.SetRadio()
      replace t_OAPOSTIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOAPOSTIN_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOA__MAIL_2_4.RadioValue()==this.w_OA__MAIL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOA__MAIL_2_4.SetRadio()
      replace t_OA__MAIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOA__MAIL_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'LOGCNTUT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_OACODUTE))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_OACODUTE)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_OACODUTE=0
      .w_OANAMUTE=space(20)
      .w_OAPOSTIN=space(1)
      .w_OA__MAIL=space(1)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_OACODUTE))
        .link_2_1('Full')
      endif
    endwith
    this.DoRTCalc(4,7,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_OACODUTE = t_OACODUTE
    this.w_OANAMUTE = t_OANAMUTE
    this.w_OAPOSTIN = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOAPOSTIN_2_3.RadioValue(.t.)
    this.w_OA__MAIL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOA__MAIL_2_4.RadioValue(.t.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_OACODUTE with this.w_OACODUTE
    replace t_OANAMUTE with this.w_OANAMUTE
    replace t_OAPOSTIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOAPOSTIN_2_3.ToRadio()
    replace t_OA__MAIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOA__MAIL_2_4.ToRadio()
    if i_srv='A'
      replace OACODUTE with this.w_OACODUTE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscf_mluPag1 as StdContainer
  Width  = 400
  height = 239
  stdWidth  = 400
  stdheight = 239
  resizeYpos=180
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oStr_1_3 as StdString with uid="LNBKWYWICO",Visible=.t., Left=10, Top=13,;
    Alignment=0, Width=43, Height=18,;
    Caption="Utente"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="YWJOVLZYFX",Visible=.t., Left=216, Top=13,;
    Alignment=0, Width=66, Height=18,;
    Caption="post-IN"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="AYGKOPFQPO",Visible=.t., Left=292, Top=13,;
    Alignment=0, Width=52, Height=18,;
    Caption="mail"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=33,;
    width=366+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=34,width=365+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='cpusers|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='cpusers'
        oDropInto=this.oBodyCol.oRow.oOACODUTE_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oBox_2_5 as StdBox with uid="PKDXFYGTSI",left=7, top=8, width=374,height=27

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscf_mluBodyRow as CPBodyRowCnt
  Width=356
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oOACODUTE_2_1 as StdTrsField with uid="XRNWIFCQOK",rtseq=3,rtrep=.t.,;
    cFormVar="w_OACODUTE",value=0,isprimarykey=.t.,;
    HelpContextID = 214516011,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"], bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_OACODUTE"

  func oOACODUTE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oOACODUTE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oOACODUTE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oOACODUTE_2_1.readonly and this.parent.oOACODUTE_2_1.isprimarykey)
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oOACODUTE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
   endif
  endproc

  add object oOANAMUTE_2_2 as StdTrsField with uid="WQKYBPFAYO",rtseq=4,rtrep=.t.,;
    cFormVar="w_OANAMUTE",value=space(20),enabled=.f.,;
    HelpContextID = 223080747,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=42, Top=0, InputMask=replicate('X',20)

  add object oOAPOSTIN_2_3 as StdTrsCombo with uid="AXPMSIGVXL",rtrep=.t.,;
    cFormVar="w_OAPOSTIN", RowSource=""+"-,"+"Da inviare,"+"Inviato" , ;
    HelpContextID = 54914764,;
    Height=21, Width=72, Left=203, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oOAPOSTIN_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..OAPOSTIN,&i_cF..t_OAPOSTIN),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    iif(xVal =3,'I',;
    space(1)))))
  endfunc
  func oOAPOSTIN_2_3.GetRadio()
    this.Parent.oContained.w_OAPOSTIN = this.RadioValue()
    return .t.
  endfunc

  func oOAPOSTIN_2_3.ToRadio()
    this.Parent.oContained.w_OAPOSTIN=trim(this.Parent.oContained.w_OAPOSTIN)
    return(;
      iif(this.Parent.oContained.w_OAPOSTIN=='N',1,;
      iif(this.Parent.oContained.w_OAPOSTIN=='S',2,;
      iif(this.Parent.oContained.w_OAPOSTIN=='I',3,;
      0))))
  endfunc

  func oOAPOSTIN_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oOA__MAIL_2_4 as StdTrsCombo with uid="QMJFZJDDWG",rtrep=.t.,;
    cFormVar="w_OA__MAIL", RowSource=""+"-,"+"Da inviare,"+"Inviato" , ;
    HelpContextID = 110427854,;
    Height=21, Width=72, Left=279, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oOA__MAIL_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..OA__MAIL,&i_cF..t_OA__MAIL),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    iif(xVal =3,'I',;
    space(1)))))
  endfunc
  func oOA__MAIL_2_4.GetRadio()
    this.Parent.oContained.w_OA__MAIL = this.RadioValue()
    return .t.
  endfunc

  func oOA__MAIL_2_4.ToRadio()
    this.Parent.oContained.w_OA__MAIL=trim(this.Parent.oContained.w_OA__MAIL)
    return(;
      iif(this.Parent.oContained.w_OA__MAIL=='N',1,;
      iif(this.Parent.oContained.w_OA__MAIL=='S',2,;
      iif(this.Parent.oContained.w_OA__MAIL=='I',3,;
      0))))
  endfunc

  func oOA__MAIL_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oOACODUTE_2_1.When()
    return(.t.)
  proc oOACODUTE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oOACODUTE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscf_mlu','LOGCNTUT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".OASERIAL=LOGCNTUT.OASERIAL";
  +" and "+i_cAliasName2+".OASTAMP=LOGCNTUT.OASTAMP";
  +" and "+i_cAliasName2+".OAREC_ID=LOGCNTUT.OAREC_ID";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
