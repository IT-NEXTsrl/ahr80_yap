* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bms                                                        *
*              Cerca movimenti articolo                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-16                                                      *
* Last revis.: 2013-12-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPROV
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bms",oParentObject,m.pPROV)
return(i_retval)

define class tgsma_bms as StdBatch
  * --- Local variables
  pPROV = space(3)
  w_SLCODART = space(20)
  w_SCCODART = space(20)
  w_MESS = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Alla Cancellazione Saldi, verifica se esistono Movimenti associati (da GSMA_ASA)
    do case
      case this.pPROV="ART"
        * --- Saldi articolo
        this.w_SLCODART = this.oParentObject.w_SLCODICE
        vq_exec("QUERY\GSMA_BMS.VQR",this,"RESULT")
        sum RESULT.QUANTI to L_TOTAMOVI
        if L_TOTAMOVI > 0
          this.w_MESS = ah_msgformat("Non � possibile eseguire la cancellazione in quanto esistono movimenti dell'articolo")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.W_MESS
        endif
      case this.pPROV="COM"
        * --- Saldi Commessa
        this.w_SCCODART = this.oParentObject.w_SCCODICE
        vq_exec("QUERY\GSMACBMS.VQR",this,"RESULT")
        sum RESULT.QUANTI to L_TOTAMOVI
        if L_TOTAMOVI > 0
          this.W_MESS = "Non � possibile eseguire la cancellazione in quanto esistono movimenti dell'articolo e del codice commessa nel magazzino specificato"
          this.W_MESS = AH_MsgFormat(this.W_MESS)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.W_MESS
        endif
    endcase
  endproc


  proc Init(oParentObject,pPROV)
    this.pPROV=pPROV
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPROV"
endproc
