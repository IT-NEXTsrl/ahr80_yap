* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsil_bil                                                        *
*              Importa listini                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_404]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-21                                                      *
* Last revis.: 2016-11-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsil_bil",oParentObject)
return(i_retval)

define class tgsil_bil as StdBatch
  * --- Local variables
  w_CODLIS = space(5)
  w_VALO = space(10)
  w_VALO1 = space(10)
  w_APPO = space(10)
  w_DELIM = space(10)
  w_POSINI = 0
  w_POSFIN = 0
  w_NUMRIG = 0
  w_CODCAM = space(8)
  w_NUMLIS = 0
  w_TIPSTR = space(1)
  w_LUNSTR = 0
  w_ERRORE = space(1)
  w_MESS = space(40)
  w_RECORD = space(10)
  w_NUREC = 0
  w_CNT = 0
  LIS = space(10)
  Messaggio = space(10)
  w_DECLIS = 0
  w_DECUNI = 0
  w_VALLIS = space(3)
  w_TEST = .f.
  w_INIZIO = 0
  w_DATA = ctod("  /  /  ")
  w_FLCHKVON = space(1)
  w_STRCHK = space(254)
  w_NRIGATT = 0
  w_oERRORLOG = .NULL.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_INART = 0
  w_AGGART = 0
  w_KEYINS = 0
  w_AGGKEY = 0
  w_LISINS = 0
  w_SCAINS = 0
  w_UNIINS = 0
  w_PRIMA_RIGA = .f.
  w_DIVI = space(200)
  w_ARCODART = space(20)
  w_ARDESART = space(40)
  w_ARDESSUP = space(0)
  w_ARUNMIS1 = space(3)
  w_AROPERAT = space(1)
  w_ARMOLTIP = 0
  w_ARUNMIS2 = space(3)
  w_ARGRUMER = space(5)
  w_ARCODFAM = space(5)
  w_ARGRUPRO = space(5)
  w_ARCATSCM = space(5)
  w_ARCODALT = space(20)
  w_ARTIPMA1 = space(5)
  w_ARTIPMA2 = space(5)
  w_ARCODIVA = space(5)
  w_ARFLINVE = space(1)
  w_ARFLESAU = space(1)
  w_ARCATOMO = space(5)
  w_ARCATCON = space(5)
  w_ARTIPART = space(2)
  w_ARFLSTCO = space(1)
  w_ARPESNET = 0
  w_ARPESNE2 = 0
  w_ARPESLOR = 0
  w_ARPESLO2 = 0
  w_ARDESVOL = space(15)
  w_ARDESVO2 = space(15)
  w_ARUMVOLU = space(3)
  w_ARUMVOL2 = space(3)
  w_ARTPCONF = space(3)
  w_ARTPCON2 = space(3)
  w_ARCOCOL1 = 0
  w_ARCOCOL2 = 0
  w_ARNOMENC = space(8)
  w_ARUMSUPP = space(3)
  w_ARMOLSUP = 0
  w_ARSTASUP = space(1)
  w_ARDIMLUN = 0
  w_ARDIMLU2 = 0
  w_ARDIMLAR = 0
  w_ARDIMLA2 = 0
  w_ARDIMALT = 0
  w_ARDIMAL2 = 0
  w_ARUMDIME = space(3)
  w_ARUMDIM2 = space(3)
  w_ARDTINVA = ctod("  /  /  ")
  w_ARDTOBSO = ctod("  /  /  ")
  w_ARCODMAR = space(5)
  w_ARCODRIC = space(5)
  w_ARCODCLA = space(3)
  w_ARFLDISP = space(1)
  w_ARFLCONA = space(1)
  w_ARFLCON2 = space(1)
  w_ARTIPBAR = space(1)
  w_ARVOCCEN = space(15)
  w_ARTIPGES = space(1)
  w_ARPROPRE = space(1)
  w_ARCODDIS = space(20)
  w_ARFLCOMP = space(1)
  w_ARCAUZIO = space(20)
  w_ARIMBREN = space(1)
  w_ARIMBCAU = space(1)
  w_ARCAUZI2 = space(20)
  w_ARIMBRE2 = space(1)
  w_ARIMBCA2 = space(1)
  w_ARTIPRIF = space(1)
  w_ARCODVAR = space(5)
  w_ARFLLOTT = space(1)
  w_ARFLUBIC = space(1)
  w_ARCODPRO = space(15)
  w_ARCODFOR = space(15)
  w_ARCODCAA = space(5)
  w_ARFLESIS = space(1)
  w_ARFLRIVA = space(1)
  w_ARPROINT = space(1)
  w_ARPROEST = space(1)
  w_ARPROCLA = space(1)
  w_ARMAGPRE = space(5)
  w_ARCODPLA = space(5)
  w_ARCLAPRE = space(5)
  w_ARCLARIF = space(5)
  w_ARFLCOMM = space(1)
  w_ARFLGIS4 = space(1)
  w_ARPUBWEB = space(1)
  w_ARGESMAT = space(1)
  w_ARCLAMAT = space(5)
  w_ARCENCOS = space(15)
  w_ARFLUMVA = space(1)
  w_ARPREZUM = space(1)
  w_ARFLCESP = space(1)
  w_ARCATCES = space(15)
  w_ARDATINT = space(1)
  w_ARTIPOPE = space(10)
  w_ARCATOPE = space(2)
  w_ARARTPOS = space(1)
  w_ARCLALOT = space(5)
  w_ARCODGRU = space(5)
  w_ARCODREP = space(3)
  w_ARCODSOT = space(5)
  w_ARDISLOT = space(1)
  w_ARFLDISC = space(1)
  w_ARFLESUL = space(1)
  w_ARFLLMAG = space(1)
  w_ARFLPECO = space(1)
  w_ARFLSERG = space(1)
  w_ARFLUSEP = space(1)
  w_ARPZCON2 = 0
  w_ARPZCONF = 0
  w_ARSTACOD = space(1)
  w_ARTIPCO1 = space(5)
  w_ARTIPCO2 = space(5)
  w_ARTIPPRE = space(1)
  w_ARTIPSER = space(1)
  w_ARVOCRIC = space(15)
  w_ARDATINT = space(1)
  w_ORDESART = space(40)
  w_ORDESSUP = space(0)
  w_ORUNMIS1 = space(3)
  w_OROPERAT = space(1)
  w_ORMOLTIP = 0
  w_ORUNMIS2 = space(3)
  w_ORGRUMER = space(5)
  w_ORCODFAM = space(5)
  w_ORGRUPRO = space(5)
  w_ORCATSCM = space(5)
  w_ORCODALT = space(20)
  w_ORTIPMA1 = space(5)
  w_ORTIPMA2 = space(5)
  w_ORCODIVA = space(5)
  w_ORFLINVE = space(1)
  w_ORFLESAU = space(1)
  w_ORCATOMO = space(5)
  w_ORCATCON = space(5)
  w_ORTIPART = space(2)
  w_ORFLSTCO = space(1)
  w_ORPESNET = 0
  w_ORPESNE2 = 0
  w_ORPESLOR = 0
  w_ORPESLO2 = 0
  w_ORDESVOL = space(15)
  w_ORDESVO2 = space(15)
  w_ORUMVOLU = space(3)
  w_ORUMVOL2 = space(3)
  w_ORTPCONF = space(3)
  w_ORTPCON2 = space(3)
  w_ORCOCOL1 = 0
  w_ORCOCOL2 = 0
  w_ORNOMENC = space(8)
  w_ORUMSUPP = space(3)
  w_ORMOLSUP = 0
  w_ORSTASUP = space(1)
  w_ORDIMLUN = 0
  w_ORDIMLU2 = 0
  w_ORDIMLAR = 0
  w_ORDIMLA2 = 0
  w_ORDIMALT = 0
  w_ORDIMAL2 = 0
  w_ORUMDIME = space(3)
  w_ORUMDIM2 = space(3)
  w_ORDTINVA = ctod("  /  /  ")
  w_ORDTOBSO = ctod("  /  /  ")
  w_ORCODMAR = space(5)
  w_ORCODRIC = space(5)
  w_ORCODCLA = space(3)
  w_ORFLDISP = space(1)
  w_ORFLCONA = space(1)
  w_ORFLCON2 = space(1)
  w_ORTIPBAR = space(1)
  w_ORVOCCEN = space(15)
  w_ORTIPGES = space(1)
  w_ORPROPRE = space(1)
  w_ORCODDIS = space(20)
  w_ORFLCOMP = space(1)
  w_ORCAUZIO = space(20)
  w_ORIMBREN = space(1)
  w_ORIMBCAU = space(1)
  w_ORCAUZI2 = space(20)
  w_ORIMBRE2 = space(1)
  w_ORIMBCA2 = space(1)
  w_ORTIPRIF = space(1)
  w_ORCODVAR = space(5)
  w_ORFLLOTT = space(1)
  w_ORFLUBIC = space(1)
  w_ORCODPRO = space(15)
  w_ORCODFOR = space(15)
  w_ORCODCAA = space(5)
  w_ORFLESIS = space(1)
  w_ORFLRIVA = space(1)
  w_ORPROINT = space(1)
  w_ORPROEST = space(1)
  w_ORPROCLA = space(1)
  w_ORMAGPRE = space(5)
  w_ORCODPLA = space(5)
  w_ORCLAPRE = space(5)
  w_ORCLARIF = space(5)
  w_ORFLCOMM = space(1)
  w_ORFLGIS4 = space(1)
  w_ORPUBWEB = space(1)
  w_ORGESMAT = space(1)
  w_ORCLAMAT = space(5)
  w_ORCENCOS = space(15)
  w_ORFLUMVA = space(1)
  w_ORPREZUM = space(1)
  w_ORFLCESP = space(1)
  w_ORCATCES = space(15)
  w_ORDATINT = space(1)
  w_ORTIPOPE = space(10)
  w_ORCATOPE = space(2)
  w_ORARTPOS = space(1)
  w_ORCLALOT = space(5)
  w_ORCODGRU = space(5)
  w_ORCODREP = space(3)
  w_ORCODSOT = space(5)
  w_ORDISLOT = space(1)
  w_ORFLDISC = space(1)
  w_ORFLESUL = space(1)
  w_ORFLLMAG = space(1)
  w_ORFLPECO = space(1)
  w_ORFLSERG = space(1)
  w_ORFLUSEP = space(1)
  w_ORPZCON2 = 0
  w_ORPZCONF = 0
  w_ORSTACOD = space(1)
  w_ORTIPCO1 = space(5)
  w_ORTIPCO2 = space(5)
  w_ORTIPPRE = space(1)
  w_ORTIPSER = space(1)
  w_ORVOCRIC = space(15)
  w_ORDATINT = space(1)
  w_CACODICE = space(20)
  w_CADESART = space(40)
  w_CADESSUP = space(0)
  w_CACODART = space(20)
  w_CATIPCON = space(1)
  w_CACODCON = space(15)
  w_CA__TIPO = space(1)
  w_CATIPBAR = space(1)
  w_CAFLSTAM = space(1)
  w_CAUNIMIS = space(3)
  w_CAOPERAT = space(1)
  w_CAMOLTIP = 0
  w_CADTINVA = ctod("  /  /  ")
  w_CADTOBSO = ctod("  /  /  ")
  w_CATIPMA3 = space(5)
  w_CAPESNE3 = 0
  w_CAPESLO3 = 0
  w_CADESVO3 = space(15)
  w_CAUMVOL3 = space(3)
  w_CATPCON3 = space(3)
  w_CAPZCON3 = 0
  w_CACOCOL3 = 0
  w_CADIMLU3 = 0
  w_CADIMLA3 = 0
  w_CADIMAL3 = 0
  w_CAUMDIM3 = space(3)
  w_CAFLCON3 = space(1)
  w_CACODVAR = space(20)
  w_CACAUZI3 = space(20)
  w_CAIMBRE3 = space(1)
  w_CAIMBCA3 = space(1)
  w_CACONFIG = space(1)
  w_CACONATO = space(1)
  w_CACONCAR = space(1)
  w_CAGESCAR = space(1)
  w_CARIFCLA = space(41)
  w_CAKEYSAL = space(40)
  w_CACODCEN = space(15)
  w_CAFLUMVA = space(1)
  w_CAPREZUM = space(1)
  w_CAFLIMBA = space(1)
  w_CALENSCF = 0
  w_CAPUBWEB = space(1)
  w_CAPZCON3 = 0
  w_CATIPCO3 = space(5)
  w_OADESART = space(40)
  w_OADESSUP = space(0)
  w_OACODART = space(20)
  w_OATIPCON = space(1)
  w_OACODCON = space(15)
  w_OA__TIPO = space(1)
  w_OATIPBAR = space(1)
  w_OAFLSTAM = space(1)
  w_OAUNIMIS = space(3)
  w_OAOPERAT = space(1)
  w_OAMOLTIP = 0
  w_OADTINVA = ctod("  /  /  ")
  w_OADTOBSO = ctod("  /  /  ")
  w_OATIPMA3 = space(5)
  w_OAPESNE3 = 0
  w_OAPESLO3 = 0
  w_OADESVO3 = space(15)
  w_OAUMVOL3 = space(3)
  w_OATPCON3 = space(3)
  w_OAPZCON3 = 0
  w_OACOCOL3 = 0
  w_OADIMLU3 = 0
  w_OADIMLA3 = 0
  w_OADIMAL3 = 0
  w_OAUMDIM3 = space(3)
  w_OAFLCON3 = space(1)
  w_OACODVAR = space(20)
  w_OACAUZI3 = space(20)
  w_OAIMBRE3 = space(1)
  w_OAIMBCA3 = space(1)
  w_OACONFIG = space(1)
  w_OACONATO = space(1)
  w_OACONCAR = space(1)
  w_OAGESCAR = space(1)
  w_OARIFCLA = space(41)
  w_OAKEYSAL = space(40)
  w_OACODCEN = space(15)
  w_OAFLUMVA = space(1)
  w_OAPREZUM = space(1)
  w_OAFLIMBA = space(1)
  w_OALENSCF = 0
  w_OAPUBWEB = space(1)
  w_OAPZCON3 = 0
  w_OATIPCO3 = space(5)
  w_CA__TIPO1 = space(1)
  w_CATIPBAR1 = space(1)
  w_CAOPERAT1 = space(1)
  w_CAMOLTIP1 = 0
  w_PRSCOMIN = 0
  w_PRSCOMAX = 0
  w_PRDISMIN = 0
  w_PRQTAMIN = 0
  w_PRLOTRIO = 0
  w_PRPUNRIO = 0
  w_PRGIOAPP = 0
  w_PRGIOINV = 0
  w_PRCOSSTA = 0
  w_PRTIPCON = space(1)
  w_PRCODPRO = space(15)
  w_PRCODFOR = space(15)
  w_PRCODART = space(20)
  w_PRCODVAR = space(20)
  w_PRKEYVAR = space(20)
  w_PRKEYSAL = space(40)
  w_PRSAFELT = 0
  w_PRGIOAPP = 0
  w_PRCOERSS = 0
  w_PRGIOCOP = 0
  w_PRPUNLOT = space(1)
  w_PRLISCOS = space(5)
  w_PUUTEELA = 0
  w_PRESECOS = space(4)
  w_PRINVCOS = space(6)
  w_PRCSLAVO = 0
  w_PRCSMATE = 0
  w_PRCSSPGE = 0
  w_PRLOTSTD = 0
  w_PRDATCOS = ctod("  /  /  ")
  w_PRCULAVO = 0
  w_PRCUMATE = 0
  w_PRCUSPGE = 0
  w_PRLOTULT = 0
  w_PRDATULT = ctod("  /  /  ")
  w_PRCMMATE = 0
  w_PRCMLAVO = 0
  w_PRCMSPGE = 0
  w_PRLOTMED = 0
  w_PRDATMED = ctod("  /  /  ")
  w_PRCLLAVO = 0
  w_PRCLMATE = 0
  w_PRCLSPGE = 0
  w_PRLOTLIS = 0
  w_PRDATLIS = ctod("  /  /  ")
  w_PRLOWLEV = 0
  w_PRCOEFLT = 0
  w_PRLEAMPS = 0
  w_PRPERSCA = 0
  w_ORSCOMIN = 0
  w_ORSCOMAX = 0
  w_ORDISMIN = 0
  w_ORQTAMIN = 0
  w_ORLOTRIO = 0
  w_ORPUNRIO = 0
  w_ORGIOAPP = 0
  w_ORGIOINV = 0
  w_ORCOSSTA = 0
  w_ORTIPCON = space(1)
  w_ORCODPRO = space(15)
  w_ORCODFOR = space(15)
  w_ORCODART = space(20)
  w_ORCODVAR = space(20)
  w_ORKEYVAR = space(20)
  w_ORKEYSAL = space(40)
  w_ORSAFELT = 0
  w_ORGIOAPP = 0
  w_ORCOERSS = 0
  w_ORGIOCOP = 0
  w_ORPUNLOT = space(1)
  w_ORLISCOS = space(5)
  w_OUUTEELA = 0
  w_ORESECOS = space(4)
  w_ORINVCOS = space(6)
  w_ORCSLAVO = 0
  w_ORCSMATE = 0
  w_ORCSSPGE = 0
  w_ORLOTSTD = 0
  w_ORDATCOS = ctod("  /  /  ")
  w_ORCULAVO = 0
  w_ORCUMATE = 0
  w_ORCUSPGE = 0
  w_ORLOTULT = 0
  w_ORDATULT = ctod("  /  /  ")
  w_ORCMMATE = 0
  w_ORCMLAVO = 0
  w_ORCMSPGE = 0
  w_ORLOTMED = 0
  w_ORDATMED = ctod("  /  /  ")
  w_ORCLLAVO = 0
  w_ORCLMATE = 0
  w_ORCLSPGE = 0
  w_ORLOTLIS = 0
  w_ORDATLIS = ctod("  /  /  ")
  w_ORLOWLEV = 0
  w_ORCOEFLT = 0
  w_ORLEAMPS = 0
  w_ORPERSCA = 0
  w_UMCODICE = space(3)
  w_UMDESCRI = space(35)
  w_UMFLFRAZ = space(1)
  w_UMMODUM2 = space(1)
  w_OMDESCRI = space(35)
  w_OMFLFRAZ = space(1)
  w_OMMODUM2 = space(1)
  w_LICODART = space(20)
  w_CPROWNUM = 0
  w_LIUNIMIS = space(3)
  w_LIPREZZO = 0
  w_LIQUANTI = 0
  w_LIROWNUM = 0
  w_LISCONT1 = 0
  w_LISCONT2 = 0
  w_LISCONT3 = 0
  w_LISCONT4 = 0
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_OKINS = .f.
  w_NOERR = .f.
  w_OKINSART = .f.
  w_OKINSDAT = .f.
  w_CODRICER = space(20)
  w_TOTART = 0
  w_NEWROWNUM = 0
  w_TESTROW = 0
  w_LOOP = 0
  w_INSOK = .f.
  w_FLSCAGLI = space(1)
  * --- WorkFile variables
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  LIS_SCAG_idx=0
  LIS_TINI_idx=0
  PARMLIST_idx=0
  PAR_RIOR_idx=0
  STRUTABE_idx=0
  UNIMIS_idx=0
  VALUTE_idx=0
  LISTINI_idx=0
  DET_VARI_idx=0
  PARATRAS_idx=0
  SALDIART_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine lanciata da GSIL_KIL
    this.Messaggio = ""
    * --- Test Inserimento Nuovi Articoli
    this.w_TEST = .F.
    * --- Controllo Esistenza Data di Inizio (o Fine) Validit�
    if EMPTY(this.oParentObject.w_LIDATATT)
      ah_ErrorMsg("Data di inizio validit� non definita","!","")
      i_retcode = 'stop'
      return
    else
      if EMPTY(this.oParentObject.w_LIDATDIS)
        ah_ErrorMsg("Data di fine validit� non definita","!","")
        i_retcode = 'stop'
        return
      endif
    endif
    this.oParentObject.w_PATH = ALLTRIM(this.oParentObject.w_PATH)
    if NOT FILE(this.oParentObject.w_PATH)
      ah_ErrorMsg("Non trovo il file dei listini da importare","!","")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_DIMREC=0
      ah_ErrorMsg("Dimensione record file da importare incongruente","!","")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_AGGPRE="S" AND (this.oParentObject.w_FLLIS1="S" AND EMPTY(this.oParentObject.w_CODLIS1))
      ah_ErrorMsg("Nessun listino assegnato per l'import di %1","!","", this.oParentObject.w_STRLIS1)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_AGGPRE="S" AND (this.oParentObject.w_FLLIS2="S" AND EMPTY(this.oParentObject.w_CODLIS2))
      ah_ErrorMsg("Nessun listino assegnato per l'import di %1","!","", this.oParentObject.w_STRLIS2)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_AGGPRE="S" AND (this.oParentObject.w_FLLIS3="S" AND EMPTY(this.oParentObject.w_CODLIS3))
      ah_ErrorMsg("Nessun listino assegnato per l'import di %1","!","", this.oParentObject.w_STRLIS3)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_AGGPRE="S" AND (this.oParentObject.w_FLLIS4="S" AND EMPTY(this.oParentObject.w_CODLIS4))
      ah_ErrorMsg("Nessun listino assegnato per l'import di %1","!","", this.oParentObject.w_STRLIS4)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_AGGPRE="S" AND (this.oParentObject.w_FLLIS5="S" AND EMPTY(this.oParentObject.w_CODLIS5))
      ah_ErrorMsg("Nessun listino assegnato per l'import di %1","!","", this.oParentObject.w_STRLIS5)
      i_retcode = 'stop'
      return
    endif
    this.w_DATA = Date()
    this.w_INIZIO = Seconds()
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Istanzio Oggetto Messaggio Incrementale
    this.w_oMESS=createobject("ah_message")
    if this.oParentObject.w_FLIMPTRS="S"
      * --- begin transaction
      cp_BeginTrs()
    endif
    * --- Setto la variabile locale per il controllo valori con il vaore impostato 
    *     sulla maschera dall'utente
    this.w_FLCHKVON = this.oParentObject.w_FLCHKVAL
    * --- Array Contenente i Riferimenti alle Qta, Prezzi, Data In,Data Fin dei Listini 1..10
    DIMENSION LIS[20]
    * --- Cursore per la stampa degli errori
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Leggo Categorie Listini
    vq_exec("QUERY\GSIL_BCL.VQR",this,"CATLIS")
    if USED("CATLIS")
      this.w_CNT = 0
      SELECT CATLIS
      if RECCOUNT()>0
        GO TOP
        SCAN FOR NVL(FLLIST, " ")="S"
        this.w_CNT = this.w_CNT + 1
        REPLACE NUMLIS WITH this.w_CNT
        ENDSCAN 
      else
        ah_ErrorMsg("Categoria listini non definita","stop","")
        i_retcode = 'stop'
        return
      endif
      this.w_NOERR = .T.
    endif
    * --- Valori Predefiniti
    vq_exec("QUERY\GSIL_BPL.VQR",this,"PREDEF")
    * --- Inizia Ciclo sul File di Importazione
    nHandle=FOPEN(this.oParentObject.w_PATH)
    if nHandle>-1
      FileDim=FSEEK(nHandle,0,2)
      nHandle=FCLOSE(nHandle)
      nHandle=FOPEN(this.oParentObject.w_PATH)
    endif
    if nHandle=-1
      ah_ErrorMsg("Errore apertura file import listini","!","")
    else
      * --- w_NUREC righe file txt elaborate
      *     w_INART articoli inseriti
      *     w_KEYINS codici di ricerca inseriti
      *     w_LISINS listini inseriti
      *     w_SCAINS scaglioni inseriti
      *     w_UNIINS unit� di misura inserite
      this.w_NUREC = 0
      this.w_INART = 0
      this.w_KEYINS = 0
      this.w_AGGKEY = 0
      this.w_LISINS = 0
      this.w_SCAINS = 0
      this.w_UNIINS = 0
      this.w_AGGART = 0
      this.w_NRIGATT = this.oParentObject.w_NRRIGT
      * --- Se devo escludere prima riga
      this.w_PRIMA_RIGA = this.oParentObject.w_FLRIGT="S"
      do while NOT FEOF(nHandle)
        this.w_RECORD = FGETS(nHandle, 600)
        if this.w_PRIMA_RIGA
          do while this.w_NRIGATT>0
            this.w_RECORD = FGETS(nHandle, 600)
            this.w_NRIGATT = this.w_NRIGATT-1
          enddo
          this.w_PRIMA_RIGA = .F.
        endif
        this.w_RECORD = LEFT(this.w_RECORD+SPACE(this.oParentObject.w_DIMREC), this.oParentObject.w_DIMREC)
        this.w_NUREC = this.w_NUREC + 1
        ah_Msg("Elaborazione record: %1", .T.,.F.,.F.,ALLTRIM(STR(this.w_NUREC))+IIF(FileDim>0,"("+ALLTRIM(STR((this.w_NUREC/(FileDim/this.oParentObject.w_DIMREC)*100)))+"%)"," ") )
        if NOT EMPTY(this.w_RECORD)
          this.w_NOERR = .T.
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_NOERR
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      enddo
      if NOT FCLOSE(nHandle)
        ah_ErrorMsg("Errore chiusura file import listini","!","")
      endif
      this.w_DIVI = Repl( "-" ,50 )
      * --- Se elaborazione lanciata a cavallo di due giorni allora
      if this.w_DATA=Date()
        do case
          case FLOOR((Seconds()- this.w_INIZIO)/60 )=1
            this.w_oMESS.addmsgpartNL("Importazione terminata dopo 1 minuto")     
          case FLOOR( (Seconds()- this.w_INIZIO)/60 )>1
            this.w_oPART = this.w_oMESS.addmsgpartNL("Importazione terminata dopo %1 minuti")
            this.w_oPART.addParam(Alltrim(Str( (Seconds()- this.w_INIZIO)/60 )) )     
          otherwise
            this.w_oMESS.addmsgpartNL("Importazione terminata")     
        endcase
      else
        * --- Se a cavallo di due giorni determino il tempo per arrivare a mezzanotte
        *     (86400 - seconds() ) pi� i secondi dalla mezzanotte
        do case
          case FLOOR( (Seconds()- this.w_INIZIO)/60 )=1
            this.w_oMESS.addmsgpartNL("Importazione terminata dopo 1 minuto")     
          case FLOOR((Seconds()- this.w_INIZIO)/60 )>1
            this.w_oPART = this.w_oMESS.addmsgpartNL("Importazione terminata dopo %1 minuti")
            this.w_oPART.addParam(Alltrim(Str( ((86400-this.w_INIZIO)+ seconds())/60 )) )     
          otherwise
            this.w_oMESS.addmsgpartNL("Importazione terminata")     
        endcase
      endif
      this.w_oPART = this.w_oMESS.addmsgpartNL("%1")
      this.w_oPART.addParam(this.w_DIVI)     
      if this.w_INART>0
        if this.w_KEYINS>0
          if this.w_INART=1
            this.w_oMESS.addmsgpartNL("Inserito 1 articolo e relativi codici di ricerca")     
          else
            this.w_oPART = this.w_oMESS.addmsgpartNL("Inseriti %1 articoli e relativi codici di ricerca")
            this.w_oPART.addParam(Alltrim(Str( this.w_INART )))     
          endif
        else
          if this.w_INART=1
            this.w_oMESS.addmsgpartNL("Inserito 1 articolo")     
          else
            this.w_oPART = this.w_oMESS.addmsgpartNL("Inseriti %1 articoli")
            this.w_oPART.addParam(Alltrim(Str( this.w_INART )))     
          endif
        endif
      endif
      if this.w_LISINS>0
        if this.w_LISINS=1
          this.w_oMESS.addmsgpartNL("Inserito 1 listino articolo")     
        else
          this.w_oPART = this.w_oMESS.addmsgpartNL("Inseriti %1 listini articolo")
          this.w_oPART.addParam(Alltrim(Str( this.w_LISINS )))     
        endif
      endif
      if this.w_SCAINS>0
        this.w_oPART = this.w_oMESS.addmsgpartNL("Inseriti %1 scaglioni listini articolo")
        this.w_oPART.addParam(Alltrim(Str( this.w_SCAINS )))     
      endif
      if this.w_UNIINS>0
        this.w_oPART = this.w_oMESS.addmsgpartNL("Inseriti %1 unit� di misura")
        this.w_oPART.addParam(Alltrim(Str( this.w_UNIINS )))     
      endif
      if this.w_AGGART>0
        if this.w_AGGART=1
          this.w_oMESS.addmsgpartNL("Modificati dati anagrafici di 1 articolo")     
        else
          this.w_oPART = this.w_oMESS.addmsgpartNL("Modificati dati anagrafici di %1 articoli")
          this.w_oPART.addParam(Alltrim(Str( this.w_AGGART )))     
        endif
      endif
      if this.w_oErrorLog.IsFullLog()
        this.w_oPART = this.w_oMESS.addmsgpartNL("Numero errori riscontrati: %1")
        this.w_oPART.addParam(Alltrim(Str( this.w_oErrorLog.NumRecLog() )))     
        this.w_oPART.addParam(Alltrim(Str(this.w_NUREC)))     
      endif
      if this.w_INART + this.w_LISINS + this.w_SCAINS+ this.w_UNIINS +this.w_AGGART=0
        this.w_oPART = this.w_oMESS.addmsgpartNL("Nessuna operazione eseguita")
        this.w_oPART.addParam(Alltrim(Str(this.w_NUREC)))     
      endif
      if this.oParentObject.w_FLIMPTRS="S"
        if this.w_oErrorLog.IsFullLog()
          if ah_yesno("Si sono verificati degli errori in fase di import dei listini. Confermare l'aggiornamento del DB?")
            * --- commit
            cp_EndTrs(.t.)
            this.w_oERRORLOG.AddMsgLog("Import con errori. L'utente ha confermato l'aggiornamento del database")     
          else
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            this.w_oERRORLOG.AddMsgLog("Import con errori. Annullato aggiornamento del database")     
          endif
        else
          if this.w_NUREC=1
            this.w_oPART = this.w_oMESS.addmsgpartNL("Su %1 riga esaminata")
          else
            this.w_oPART = this.w_oMESS.addmsgpartNL("Su %1 righe esaminate")
          endif
          this.w_oPART.addParam(Alltrim(Str(this.w_NUREC)))     
          * --- commit
          cp_EndTrs(.t.)
        endif
      endif
      this.w_oPART = this.w_oMESS.addmsgpartNL("%1")
      this.w_oPART.addParam(this.w_DIVI)     
      this.w_oMESS.ah_ErrorMsg("i")
      this.w_oERRORLOG.PrintLog(this,"Log errori import")     
    endif
    * --- Chiude i Cursori
    if USED("CATLIS")
      SELECT CATLIS
      USE
    endif
    if USED("PREDEF")
      SELECT PREDEF
      USE
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ART_ICOL variabili per i nuovi valori
    this.w_ARCODART = SPACE(20)
    this.w_ARDESART = SPACE(40)
    this.w_ARDESSUP = " "
    this.w_ARUNMIS1 = SPACE(3)
    this.w_AROPERAT = " "
    this.w_ARMOLTIP = 0
    this.w_ARUNMIS2 = Space(3)
    this.w_ARGRUMER = SPACE(5)
    this.w_ARCODFAM = SPACE(5)
    this.w_ARGRUPRO = SPACE(5)
    this.w_ARCATSCM = SPACE(5)
    this.w_ARCODALT = SPACE(20)
    this.w_ARTIPMA1 = Space(5)
    this.w_ARTIPMA2 = Space(5)
    this.w_ARCODIVA = SPACE(5)
    this.w_ARFLINVE = " "
    this.w_ARFLESAU = " "
    this.w_ARCATOMO = SPACE(5)
    this.w_ARCATCON = SPACE(5)
    this.w_ARTIPART = SPACE(2)
    this.w_ARFLSTCO = IIF(g_APPLICATION="ad hoc ENTERPRISE",.NULL.," ")
    this.w_ARPESNET = 0
    this.w_ARPESNE2 = 0
    this.w_ARPESLOR = 0
    this.w_ARPESLO2 = 0
    this.w_ARDESVOL = SPACE(15)
    this.w_ARDESVO2 = SPACE(15)
    this.w_ARUMVOLU = space(3)
    this.w_ARUMVOL2 = space(3)
    this.w_ARTPCONF = space(3)
    this.w_ARTPCON2 = space(3)
    this.w_ARCOCOL1 = 0
    this.w_ARCOCOL2 = 0
    this.w_ARNOMENC = SPACE(8)
    this.w_ARUMSUPP = space(3)
    this.w_ARMOLSUP = 0
    this.w_ARSTASUP = " "
    this.w_ARDIMLUN = 0
    this.w_ARDIMLU2 = 0
    this.w_ARDIMLAR = 0
    this.w_ARDIMLA2 = 0
    this.w_ARDIMALT = 0
    this.w_ARDIMAL2 = 0
    this.w_ARUMDIME = space(3)
    this.w_ARUMDIM2 = space(3)
    this.w_ARDTINVA = cp_CharToDate("  -  -  ")
    this.w_ARDTOBSO = cp_CharToDate("  -  -  ")
    this.w_ARCODMAR = SPACE(5)
    this.w_ARCODRIC = space(5)
    this.w_ARCODCLA = space(3)
    this.w_ARFLDISP = " "
    this.w_ARFLCONA = " "
    this.w_ARFLCON2 = " "
    this.w_ARTIPBAR = " "
    this.w_ARVOCCEN = SPACE(15)
    this.w_ARTIPGES = " "
    this.w_ARPROPRE = " "
    this.w_ARCODDIS = SPACE(20)
    this.w_ARFLCOMP = " "
    this.w_ARCAUZIO = .NULL.
    this.w_ARIMBREN = SPACE(1)
    this.w_ARIMBCAU = SPACE(1)
    this.w_ARCAUZI2 = .NULL.
    this.w_ARIMBRE2 = SPACE(1)
    this.w_ARIMBCA2 = SPACE(1)
    this.w_ARTIPRIF = " "
    this.w_ARCODVAR = SPACE(5)
    this.w_ARFLLOTT = SPACE(1)
    this.w_ARFLUBIC = SPACE(1)
    this.w_ARCODPRO = SPACE(15)
    this.w_ARCODFOR = SPACE(15)
    this.w_ARCODCAA = SPACE(5)
    this.w_ARFLESIS = " "
    this.w_ARFLRIVA = " "
    this.w_ARPROINT = "N"
    this.w_ARPROEST = "S"
    this.w_ARPROCLA = SPACE(1)
    this.w_ARMAGPRE = SPACE(5)
    this.w_ARCODPLA = SPACE(5)
    this.w_ARCLAPRE = SPACE(5)
    this.w_ARCLARIF = SPACE(5)
    this.w_ARFLCOMM = "N"
    this.w_ARFLGIS4 = SPACE(1)
    this.w_ARPUBWEB = SPACE(1)
    this.w_ARGESMAT = SPACE(1)
    this.w_ARCLAMAT = SPACE(5)
    this.w_ARCENCOS = SPACE(15)
    this.w_ARFLUMVA = SPACE(1)
    this.w_ARPREZUM = SPACE(1)
    this.w_ARFLCESP = SPACE(1)
    this.w_ARCATCES = SPACE(15)
    this.w_ARDATINT = SPACE(1)
    this.w_ARTIPOPE = space(10)
    this.w_ARCATOPE = SPACE(2)
    this.w_ARARTPOS = " "
    this.w_ARCLALOT = Space(5)
    this.w_ARCODGRU = Space(5)
    this.w_ARCODREP = Space(3)
    this.w_ARCODSOT = Space(5)
    this.w_ARDISLOT = "S"
    this.w_ARFLDISC = " "
    this.w_ARFLESUL = "N"
    this.w_ARFLLMAG = " "
    this.w_ARFLPECO = "C"
    this.w_ARFLSERG = " "
    this.w_ARFLUSEP = " "
    this.w_ARPZCON2 = 0
    this.w_ARPZCONF = 0
    this.w_ARSTACOD = " "
    this.w_ARTIPCO1 = Space(5)
    this.w_ARTIPCO2 = Space(5)
    this.w_ARTIPPRE = "A"
    this.w_ARTIPSER = " "
    this.w_ARVOCRIC = Space(15)
    this.w_ARDATINT = SPACE(1)
    * --- ART_ICOL variabili per leggere i vecchi valori
    * --- KEY_ARTI variabili per i nuovi valori
    this.w_CACODICE = SPACE(20)
    this.w_CADESART = SPACE(40)
    this.w_CADESSUP = " "
    this.w_CACODART = SPACE(20)
    this.w_CATIPCON = " "
    this.w_CACODCON = SPACE(15)
    this.w_CA__TIPO = " "
    this.w_CATIPBAR = " "
    this.w_CAFLSTAM = " "
    this.w_CAUNIMIS = SPACE(3)
    this.w_CAOPERAT = "*"
    this.w_CAMOLTIP = 0
    this.w_CADTINVA = cp_CharToDate("  -  -  ")
    this.w_CADTOBSO = cp_CharToDate("  -  -  ")
    this.w_CATIPMA3 = SPACE(5)
    this.w_CAPESNE3 = 0
    this.w_CAPESLO3 = 0
    this.w_CADESVO3 = SPACE(15)
    this.w_CAUMVOL3 = SPACE(3)
    this.w_CATPCON3 = SPACE(3)
    this.w_CAPZCON3 = 0
    this.w_CACOCOL3 = 0
    this.w_CADIMLU3 = 0
    this.w_CADIMLA3 = 0
    this.w_CADIMAL3 = 0
    this.w_CAUMDIM3 = SPACE(3)
    this.w_CAFLCON3 = " "
    this.w_CACODVAR = SPACE(20)
    this.w_CACAUZI3 = SPACE(20)
    this.w_CAIMBRE3 = SPACE(1)
    this.w_CAIMBCA3 = SPACE(1)
    this.w_CACONFIG = "N"
    this.w_CACONATO = SPACE(1)
    this.w_CACONCAR = "N"
    this.w_CAGESCAR = SPACE(1)
    this.w_CARIFCLA = SPACE(41)
    this.w_CAKEYSAL = SPACE(40)
    this.w_CACODCEN = SPACE(15)
    this.w_CAFLUMVA = SPACE(1)
    this.w_CAPREZUM = SPACE(1)
    this.w_CAFLIMBA = "N"
    this.w_CALENSCF = 0
    this.w_CAPUBWEB = "N"
    this.w_CAPZCON3 = 0
    this.w_CATIPCO3 = Space(5)
    * --- KEY_ARTI variabili per leggere i vecchi valori
    * --- Variabili per il Codice di Ricerca uguale all'articolo
    this.w_CA__TIPO1 = "R"
    this.w_CATIPBAR1 = "0"
    this.w_CAOPERAT1 = "*"
    this.w_CAMOLTIP1 = 1
    * --- PAR_RIOR variabili per i nuovi valori
    this.w_PRSCOMIN = 0
    this.w_PRSCOMAX = 0
    this.w_PRDISMIN = 0
    this.w_PRQTAMIN = 0
    this.w_PRLOTRIO = 0
    this.w_PRPUNRIO = 0
    this.w_PRGIOAPP = 0
    this.w_PRGIOINV = 0
    this.w_PRCOSSTA = 0
    this.w_PRTIPCON = "F"
    this.w_PRCODPRO = SPACE(15)
    this.w_PRCODFOR = SPACE(15)
    this.w_PRCODART = SPACE(20)
    this.w_PRCODVAR = SPACE(20)
    this.w_PRKEYVAR = SPACE(20)
    this.w_PRKEYSAL = SPACE(40)
    this.w_PRSAFELT = 0
    this.w_PRGIOAPP = 0
    this.w_PRCOERSS = 0
    this.w_PRGIOCOP = 0
    this.w_PRPUNLOT = SPACE(1)
    this.w_PRLISCOS = SPACE(5)
    this.w_PUUTEELA = 0
    this.w_PRESECOS = SPACE(4)
    this.w_PRINVCOS = SPACE(6)
    this.w_PRCSLAVO = 0
    this.w_PRCSMATE = 0
    this.w_PRCSSPGE = 0
    this.w_PRLOTSTD = 0
    this.w_PRDATCOS = cp_CharToDate("  -  -  ")
    this.w_PRCULAVO = 0
    this.w_PRCUMATE = 0
    this.w_PRCUSPGE = 0
    this.w_PRLOTULT = 0
    this.w_PRDATULT = cp_CharToDate("  -  -  ")
    this.w_PRCMMATE = 0
    this.w_PRCMLAVO = 0
    this.w_PRCMSPGE = 0
    this.w_PRLOTMED = 0
    this.w_PRDATMED = cp_CharToDate("  -  -  ")
    this.w_PRCLLAVO = 0
    this.w_PRCLMATE = 0
    this.w_PRCLSPGE = 0
    this.w_PRLOTLIS = 0
    this.w_PRDATLIS = cp_CharToDate("  -  -  ")
    this.w_PRLOWLEV = 0
    this.w_PRCOEFLT = 0
    this.w_PRLEAMPS = 0
    this.w_PRPERSCA = 0
    * --- PAR_RIOR variabili per leggere i vecchi valori
    * --- UNIMIS variabili per i nuovi valori
    this.w_UMCODICE = SPACE(3)
    this.w_UMDESCRI = SPACE(35)
    this.w_UMFLFRAZ = " "
    this.w_UMMODUM2 = " "
    * --- UNIMIS variabili per leggere i vecchi valori
    * --- LIS_TINI
    this.w_LICODART = SPACE(20)
    this.w_CPROWNUM = 0
    this.w_LIUNIMIS = SPACE(3)
    * --- LIS_SCAG
    this.w_LIPREZZO = 0
    this.w_LIQUANTI = 0
    this.w_LIROWNUM = 0
    this.w_LISCONT1 = 0
    this.w_LISCONT2 = 0
    this.w_LISCONT3 = 0
    this.w_LISCONT4 = 0
    this.w_SCONT1 = 0
    this.w_SCONT2 = 0
    this.w_SCONT3 = 0
    this.w_SCONT4 = 0
    * --- Vettore per i Listini
    this.LIS = 0
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora ciascun Record del Cursore
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    SELECT CATLIS
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODCAM,""))
    this.w_VALO = ""
    this.w_POSINI = NVL(POSINI, 0)
    this.w_POSFIN = NVL(POSFIN, 0)
    this.w_CODCAM = NVL(CODCAM, "")
    this.w_NUMLIS = NVL(NUMLIS, 0)
    this.w_TIPSTR = NVL(TIPSTR, " ")
    this.w_LUNSTR = NVL(LUNSTR, 0)
    this.w_DECLIS = NVL(DECLIS, 0)
    if this.w_POSINI>0 AND this.w_POSFIN<=this.oParentObject.w_DIMREC AND this.w_POSINI<=this.w_POSFIN AND this.w_TIPSTR $ "CNDM" AND this.w_NUMLIS<=10
      this.w_VALO = SUBSTR(this.w_RECORD, this.w_POSINI, (this.w_POSFIN+1)-this.w_POSINI)
      this.w_APPO = this.w_VALO
      do case
        case this.w_TIPSTR="N"
          this.w_VALO = STRTRAN(this.w_VALO,".","")
          this.w_VALO = STRTRAN(this.w_VALO,",","")
          this.w_VALO = VAL(this.w_VALO)
          this.w_VALO = this.w_VALO/(10^this.w_DECLIS)
        case this.w_TIPSTR="D"
          this.w_VALO = cp_CharToDate(this.w_VALO)
        case this.w_TIPSTR="C"
          this.w_VALO = LEFT(this.w_VALO, this.w_LUNSTR)
      endcase
      if this.w_NUMLIS>0 
        y = this.w_NUMLIS
        LIS[y] = this.w_VALO
        if this.w_FLCHKVON="S"
          this.w_oPART = this.w_oMESS.addmsgpart("Riga: %1%0Campo: %2%0Valore %3")
          this.w_oPART.addParam(CHR(9)+ALLTRIM(STR(this.w_NUREC)))     
          this.w_oPART.addParam(CHR(9)+UPPER(ALLTRIM(this.w_CODCAM)))     
          this.w_oPART.addParam(CHR(9))     
          do case
            case type("this.w_VALO")="C"
              this.w_oPART = this.w_oMESS.addmsgpartNL("%1")
              this.w_oPART.addParam(this.w_VALO)     
            case type("this.w_VALO")="N"
              this.w_oPART = this.w_oMESS.addmsgpartNL("%1")
              this.w_oPART.addParam(ALLTRIM(STR(this.w_VALO,20,this.w_DECLIS)))     
            case type("this.w_VALO")="D"
              this.w_oPART = this.w_oMESS.addmsgpartNL("%1")
              this.w_oPART.addParam(ALLTRIM(DTOC(this.w_VALO)))     
            case type("this.w_VALO")="L"
              if this.w_VALO
                this.w_oMESS.addmsgpartNL("VERO")     
              else
                this.w_oMESS.addmsgpartNL("FALSO")     
              endif
            otherwise
              this.w_oMESS.addmsgpartNL("*** Non visualizzabile ***")     
          endcase
          this.w_oMESS.addmsgpart("Si desidera proseguire nella verifica?")     
          if NOT this.w_oMESS.ah_YesNo()
            this.w_FLCHKVON = "N"
          endif
        endif
      else
        NN = UPPER(ALLTRIM(this.w_CODCAM))
        this.Messaggio = "Corretta"
        OldErr = ON("ERROR")
        ON ERROR this.Messaggio = "Errata"
        if this.w_FLCHKVON="S"
          this.w_oPART = this.w_oMESS.addmsgpart("Riga: %1%0Campo: %2%0Valore: %3")
          this.w_oPART.addParam(CHR(9)+ALLTRIM(STR(this.w_NUREC)))     
          this.w_oPART.addParam(CHR(9)+ALLTRIM(NN))     
          this.w_oPART.addParam(CHR(9))     
          do case
            case type("this.w_VALO")="C"
              this.w_oPART = this.w_oMESS.addmsgpartNL("%1")
              this.w_oPART.addParam(this.w_VALO)     
            case type("this.w_VALO")="N"
              this.w_oPART = this.w_oMESS.addmsgpartNL("%1")
              this.w_oPART.addParam(ALLTRIM(STR(this.w_VALO,20,this.w_DECLIS)))     
            case type("this.w_VALO")="D"
              this.w_oPART = this.w_oMESS.addmsgpartNL("%1")
              this.w_oPART.addParam(ALLTRIM(DTOC(this.w_VALO)))     
            case type("this.w_VALO")="L"
              if this.w_VALO
                this.w_oMESS.addmsgpartNL("VERO")     
              else
                this.w_oMESS.addmsgpartNL("FALSO")     
              endif
            otherwise
              this.w_oMESS.addmsgpartNL("*** Non visualizzabile ***")     
          endcase
          this.w_oMESS.addmsgpart("Si desidera proseguire nella verifica?")     
          if NOT this.w_oMESS.ah_YesNo()
            this.w_FLCHKVON = "N"
          endif
        endif
        if Type("this.w_&NN") <>"U"
          this.w_&NN = this.w_VALO
        else
          this.w_oERRORLOG.AddMsgLog("Campo: %1 non gestito.",NN)     
          this.w_NOERR = .F.
        endif
        ON ERROR &OldErr
      endif
    endif
    SELECT CATLIS
    ENDSCAN 
    if NOT EMPTY(this.w_ARCODART)
      * --- Legge Parametri Listini
      SELECT PREDEF
      GO TOP
      SCAN FOR NOT EMPTY(NVL(NOMCAM,"")) AND NVL(FLTRAS, " ") $ "PFT"
      this.w_VALO = NVL(VALFIS, " ")
      this.w_TIPSTR = NVL(TIPSTR, " ")
      this.w_LUNSTR = NVL(LUNSTR, 0)
      this.w_NUMRIG = NVL(NUMRIG, 0)
      do case
        case this.w_TIPSTR="N"
          this.w_VALO = VAL(this.w_VALO)
        case this.w_TIPSTR="D"
          this.w_VALO = cp_CharToDate(this.w_VALO)
        case this.w_TIPSTR="C"
          this.w_VALO = LEFT(this.w_VALO, this.w_LUNSTR)
      endcase
      NN = UPPER(ALLTRIM(NOMCAM))
      this.Messaggio = "Corretta"
      OldErr = ON("ERROR")
      ON ERROR this.Messaggio = "Errata"
      do case
        case FLTRAS = "F" OR (FLTRAS="P" AND (Type("this.w_&NN") ="U" OR EMPTY(this.w_&NN)))
          if Type("this.w_&NN") <>"U"
            this.w_&NN = this.w_VALO
          else
            this.w_oERRORLOG.AddMsgLog("Campo: %1 non gestito.",NN)     
            this.w_NOERR = .F.
          endif
        case FLTRAS="T"
          this.w_VALO = NVL(VALFIS, " ")
          if empty(this.w_VALO)
            if Type("this.w_&NN") <>"U"
              this.w_VALO = this.w_&NN
            else
              this.w_oERRORLOG.AddMsgLog("Campo: %1 non gestito.",NN)     
              this.w_NOERR = .F.
            endif
          endif
          if this.Messaggio<>"Errata"
            * --- Legge Trascodifiche
            * --- Read from PARATRAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PARATRAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PARATRAS_idx,2],.t.,this.PARATRAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PTNEWVAL"+;
                " from "+i_cTable+" PARATRAS where ";
                    +"PTCODCAT = "+cp_ToStrODBC(this.oParentObject.w_CATLIS);
                    +" and PTROWNUM = "+cp_ToStrODBC(this.w_NUMRIG);
                    +" and PTOLDVAL = "+cp_ToStrODBC(this.w_VALO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PTNEWVAL;
                from (i_cTable) where;
                    PTCODCAT = this.oParentObject.w_CATLIS;
                    and PTROWNUM = this.w_NUMRIG;
                    and PTOLDVAL = this.w_VALO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_VALO1 = NVL(cp_ToDate(_read_.PTNEWVAL),cp_NullValue(_read_.PTNEWVAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            do case
              case this.w_TIPSTR="N"
                this.w_VALO1 = VAL(this.w_VALO1)
              case this.w_TIPSTR="D"
                this.w_VALO1 = cp_CharToDate(this.w_VALO1)
              case this.w_TIPSTR="C"
                this.w_VALO1 = ALLTRIM(LEFT(this.w_VALO1, this.w_LUNSTR))
            endcase
            if Type("this.w_&NN") <>"U"
              this.w_&NN = this.w_VALO1
            else
              this.w_oERRORLOG.AddMsgLog("Campo: %1 non gestito.",NN)     
              this.w_NOERR = .F.
            endif
          endif
      endcase
      ON ERROR &OldErr
      SELECT PREDEF
      ENDSCAN 
      * --- Il Campo w_ARCOCOL1 (Confezioni per Collo viene utilizzato per contenere il Moltiplicatore Prezzo dei listini METEL)
      * --- Per ottenere il prezzo unitario (rispetto all'unit� di misura presente nel file) � necessario dividere il prezzo per il moltiplicatore prezzo
      if NOT EMPTY(this.w_ARCOCOL1)
        j=1
        * --- y contiene il numero di listini presenti nel tracciato
        if j<=y
          * --- Lettura dei decimali unitari della valuta per arrotondamento.
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADECUNI"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALLIS1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADECUNI;
              from (i_cTable) where;
                  VACODVAL = this.oParentObject.w_VALLIS1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Il prezzo unitario � ottenuto dividendo il prezzo per il moltiplicatore prezzo (arrotondato ai decimali unitari della valuta del listino)
          LIS[j] = cp_ROUND(LIS[j]/this.w_ARCOCOL1, this.w_DECUNI)
          j=j+1
          if j<=y
            * --- Lettura dei decimali unitari della valuta per arrotondamento.
            * --- Read from VALUTE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VALUTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VADECUNI"+;
                " from "+i_cTable+" VALUTE where ";
                    +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALLIS2);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VADECUNI;
                from (i_cTable) where;
                    VACODVAL = this.oParentObject.w_VALLIS2;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Il prezzo unitario � ottenuto dividendo il prezzo per il moltiplicatore prezzo (arrotondato ai decimali unitari della valuta del listino)
            LIS[j] = cp_ROUND(LIS[j]/this.w_ARCOCOL1, this.w_DECUNI)
            j=j+1
            if j<=y
              * --- Lettura dei decimali unitari della valuta per arrotondamento.
              * --- Read from VALUTE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VALUTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "VADECUNI"+;
                  " from "+i_cTable+" VALUTE where ";
                      +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALLIS3);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  VADECUNI;
                  from (i_cTable) where;
                      VACODVAL = this.oParentObject.w_VALLIS3;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Il prezzo unitario � ottenuto dividendo il prezzo per il moltiplicatore prezzo (arrotondato ai decimali unitari della valuta del listino)
              LIS[j] = cp_ROUND(LIS[j]/this.w_ARCOCOL1, this.w_DECUNI)
              j=j+1
              if j<=y
                * --- Lettura dei decimali unitari della valuta per arrotondamento.
                * --- Read from VALUTE
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.VALUTE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "VADECUNI"+;
                    " from "+i_cTable+" VALUTE where ";
                        +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALLIS4);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    VADECUNI;
                    from (i_cTable) where;
                        VACODVAL = this.oParentObject.w_VALLIS4;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Il prezzo unitario � ottenuto dividendo il prezzo per il moltiplicatore prezzo (arrotondato ai decimali unitari della valuta del listino)
                LIS[j] = cp_ROUND(LIS[j]/this.w_ARCOCOL1, this.w_DECUNI)
                j=j+1
                if j<=y
                  * --- Lettura dei decimali unitari della valuta per arrotondamento.
                  * --- Read from VALUTE
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.VALUTE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "VADECUNI"+;
                      " from "+i_cTable+" VALUTE where ";
                          +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALLIS5);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      VADECUNI;
                      from (i_cTable) where;
                          VACODVAL = this.oParentObject.w_VALLIS5;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  * --- Il prezzo unitario � ottenuto dividendo il prezzo per il moltiplicatore prezzo (arrotondato ai decimali unitari della valuta del listino)
                  LIS[j] = cp_ROUND(LIS[j]/this.w_ARCOCOL1, this.w_DECUNI)
                  j=j+1
                endif
              endif
            endif
          endif
        endif
        * --- La variabile viene azzerata: non � possibile importare un valore per il campo Confezioni per Collo;
        * --- il valore presente nel campo viene lasciato invariato (se � stato modificato manualmente, con un'importazione non viene sovrascritto)
        this.w_ARCOCOL1 = 0
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Database
    this.w_CODRICER = SPACE(20)
    * --- Scrittura Unit� di Misura
    if NOT EMPTY(this.w_UMCODICE)
      this.w_OKINS = .F.
      if this.oParentObject.w_INSART="S"
        * --- Try
        local bErr_05A0C810
        bErr_05A0C810=bTrsErr
        this.Try_05A0C810()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_UNIINS = this.w_UNIINS - 1
        endif
        bTrsErr=bTrsErr or bErr_05A0C810
        * --- End
      endif
      if this.w_OKINS=.F. AND this.oParentObject.w_AGGDAT="S"
        * --- Try
        local bErr_05A07590
        bErr_05A07590=bTrsErr
        this.Try_05A07590()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_oERRORLOG.AddMsgLog("Articolo: %1 fallita la scrittura nella tabella unit� di misura (UNIMIS) %2",this.w_ARCODART,Message() )     
          this.w_NOERR = .F.
        endif
        bTrsErr=bTrsErr or bErr_05A07590
        * --- End
      endif
    endif
    * --- Scrittura Articoli
    if NOT EMPTY(this.w_ARCODART) AND this.w_NOERR
      this.w_OKINS = .F.
      this.w_OKINSDAT = .F.
      * --- Prima eseguo una ricerca all'interno dei codici di ricerca per vedere se questo articolo � presente come codice associato ad un altro articolo,
      *     la ricerca consiste nella lettura, se trovo un codice di ricerca che ha questo codice articolo,
      *     allora il codice dell'articolo associato diventa il codice articolo che devo aggiornare.
      *     w_ARCODART  codice articolo da file  diventa codice articolo da aggiornare se codice gi� presente
      *     w_CODRICER   si valorizza con codice articolo da file solo se codice di ricerca gi� presente
      *     w_CACODICE    codice di ricerca particolare
      * --- Try
      local bErr_058E2160
      bErr_058E2160=bTrsErr
      this.Try_058E2160()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_CACODART = SPACE(20)
      endif
      bTrsErr=bTrsErr or bErr_058E2160
      * --- End
      if NOT EMPTY(this.w_CACODART)
        * --- Ho trovato un codice di ricerca con lo stesso codice dell'articolo
        if this.w_ARCODART<>this.w_CACODART
          this.w_CODRICER = this.w_ARCODART
          this.w_ARCODART = this.w_CACODART
        endif
      endif
      if this.oParentObject.w_INSART="S"
        * --- Try
        local bErr_059D9770
        bErr_059D9770=bTrsErr
        this.Try_059D9770()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_oERRORLOG.AddMsgLog("Articolo: %1 fallita la scrittura nella tabella articoli (ART_ICOL) %2",this.w_ARCODART,Message() )     
        endif
        bTrsErr=bTrsErr or bErr_059D9770
        * --- End
        * --- Dati Articolo per valorizzazione
        if g_APPLICATION<>"ad hoc ENTERPRISE"
          * --- Try
          local bErr_059DA850
          bErr_059DA850=bTrsErr
          this.Try_059DA850()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_059DA850
          * --- End
        endif
      endif
      if NOT this.w_OKINS AND this.oParentObject.w_AGGDAT="S"
        * --- Try
        local bErr_0596C610
        bErr_0596C610=bTrsErr
        this.Try_0596C610()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_oERRORLOG.AddMsgLog("Articolo: %1 fallita la scrittura nella tabella articoli (ART_ICOL) %2",this.w_ARCODART,Message() )     
          this.w_NOERR = .F.
        endif
        bTrsErr=bTrsErr or bErr_0596C610
        * --- End
        this.w_CACODICE = IIF(EMPTY(this.w_CACODICE), this.w_ARCODART, this.w_CACODICE)
        this.w_CACODART = this.w_ARCODART
        this.w_LICODART = this.w_ARCODART
      endif
      * --- Dati Articolo per valorizzazione
      if NOT this.w_OKINSDAT AND this.oParentObject.w_AGGDAT="S"
        if g_APPLICATION="ad hoc ENTERPRISE"
          if NOT EMPTY(this.w_ARCODVAR)
            * --- Select from DET_VARI
            i_nConn=i_TableProp[this.DET_VARI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DET_VARI_idx,2],.t.,this.DET_VARI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" DET_VARI ";
                  +" where DVCODVAR="+cp_ToStrODBC(this.w_ARCODVAR)+"";
                   ,"_Curs_DET_VARI")
            else
              select * from (i_cTable);
               where DVCODVAR=this.w_ARCODVAR;
                into cursor _Curs_DET_VARI
            endif
            if used('_Curs_DET_VARI')
              select _Curs_DET_VARI
              locate for 1=1
              do while not(eof())
              * --- Try
              local bErr_058E6C60
              bErr_058E6C60=bTrsErr
              this.Try_058E6C60()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
                this.w_oERRORLOG.AddMsgLog("Articolo: %1 variante %2 fallita la scrittura nella tabella dati articolo per valoriz. (PAR_RIOR) %3",this.w_ARCODART,_Curs_DET_VARI.DVCODICE, Message() )     
                this.w_NOERR = .F.
              endif
              bTrsErr=bTrsErr or bErr_058E6C60
              * --- End
                select _Curs_DET_VARI
                continue
              enddo
              use
            endif
          else
            * --- Try
            local bErr_05907EF0
            bErr_05907EF0=bTrsErr
            this.Try_05907EF0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              this.w_oERRORLOG.AddMsgLog("Articolo: %1 fallita la scrittura nella tabella dati articolo per valoriz. (PAR_RIOR) %2",this.w_ARCODART, Message() )     
              this.w_NOERR = .F.
            endif
            bTrsErr=bTrsErr or bErr_05907EF0
            * --- End
          endif
        else
          * --- Try
          local bErr_05958778
          bErr_05958778=bTrsErr
          this.Try_05958778()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            this.w_oERRORLOG.AddMsgLog("Articolo: %1 fallita la scrittura nella tabella dati articolo per valoriz. (PAR_RIOR) %2",this.w_ARCODART, Message() )     
            this.w_NOERR = .F.
          endif
          bTrsErr=bTrsErr or bErr_05958778
          * --- End
        endif
      endif
    endif
    * --- Scrittura Codici di Ricerca
    if NOT EMPTY(this.w_CACODICE) AND this.w_NOERR
      this.w_OKINS = .F.
      this.w_OKINSART = .F.
      if this.oParentObject.w_INSART="S"
        this.w_CAKEYSAL = LEFT(ALLTRIM(this.w_CACODART)+SPACE(20),20)+REPLICATE("#",20)
        * --- Verifico la presenza del codice di ricerca
        * --- Try
        local bErr_058C97A0
        bErr_058C97A0=bTrsErr
        this.Try_058C97A0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          i_Rows=-1
        endif
        bTrsErr=bTrsErr or bErr_058C97A0
        * --- End
        if i_Rows=0
          * --- Try
          local bErr_058E87C0
          bErr_058E87C0=bTrsErr
          this.Try_058E87C0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_058E87C0
          * --- End
        endif
        * --- Se l'articolo � stato inserito (non aggiornato) allora oltre al Codice di Ricerca gi� inserito (BARCODE)
        *     inserisco un codice di ricerca uguale al codice articolo (altrimenti non sarebbe utilizzabile)
        * --- Try
        local bErr_058CA910
        bErr_058CA910=bTrsErr
        this.Try_058CA910()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_058CA910
        * --- End
      endif
      if this.w_OKINS=.F. AND this.oParentObject.w_AGGDAT="S" AND ((this.w_OKINS=.T. AND this.oParentObject.w_INSART="S") OR this.oParentObject.w_INSART<>"S")
        * --- Try
        local bErr_058BF9C8
        bErr_058BF9C8=bTrsErr
        this.Try_058BF9C8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_oERRORLOG.AddMsgLog("Articolo: %1 fallita la scrittura nella tabella codici articoli/servizi (KEY_ARTI) %2",this.w_ARCODART, Message() )     
          this.w_NOERR = .F.
        endif
        bTrsErr=bTrsErr or bErr_058BF9C8
        * --- End
        this.w_LICODART = this.w_CACODART
      endif
      if this.w_OKINSART=.F. AND this.oParentObject.w_AGGDAT="S"
        * --- Se l'articolo � stato aggiornato allora oltre al Codice di Ricerca gi� variato (BARCODE)
        * --- modifico il codice di ricerca uguale al codice articolo (Per l'utente questo � l'articolo)
        * --- Try
        local bErr_058899C8
        bErr_058899C8=bTrsErr
        this.Try_058899C8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_oERRORLOG.AddMsgLog("Articolo: %1 fallita la scrittura (dell'articolo) nella tabella codici articoli/servizi (KEY_ARTI) %2",this.w_ARCODART, Message() )     
          this.w_NOERR = .F.
        endif
        bTrsErr=bTrsErr or bErr_058899C8
        * --- End
      endif
      if NOT EMPTY(this.w_CODRICER) AND this.oParentObject.w_AGGDAT="S" AND ((this.w_OKINS=.T. AND this.oParentObject.w_INSART="S") OR this.oParentObject.w_INSART<>"S")
        * --- Deve essere aggiornato anche il codice di ricerca avente come codice il codice articolo originario
        * --- Try
        local bErr_0582BCC0
        bErr_0582BCC0=bTrsErr
        this.Try_0582BCC0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_oERRORLOG.AddMsgLog("Articolo: %1 fallita la scrittura (dell'articolo originario) nella tabella codici articoli/servizi (KEY_ARTI) %2",this.w_ARCODART, Message() )     
          this.w_NOERR = .F.
        endif
        bTrsErr=bTrsErr or bErr_0582BCC0
        * --- End
      endif
    endif
    * --- Scrittura Listini e Scaglioni
    if NOT EMPTY(this.w_ARCODART) AND this.oParentObject.w_AGGPRE="S" AND this.w_NOERR
      * --- Se non ho attivo il check inserisci articolo l'articolo nel file di testo
      *     potrebbe non esistere, quindi non carico il listino.
      if this.oParentObject.w_INSART<>"S"
        * --- Verifico se l'articolo esiste
        this.w_LICODART = SPACE(20)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARCODART"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARCODART;
            from (i_cTable) where;
                ARCODART = this.w_ARCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LICODART = NVL(cp_ToDate(_read_.ARCODART),cp_NullValue(_read_.ARCODART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        this.w_LICODART = this.w_ARCODART
      endif
      if NOT EMPTY(this.w_LICODART)
        this.w_NEWROWNUM = 0
        this.w_LOOP = 1
        do while this.w_LOOP<=5
          this.w_CODLIS = " "
          do case
            case NOT EMPTY(this.oParentObject.w_CODLIS1) AND this.w_LOOP=1
              this.w_CODLIS = this.oParentObject.w_CODLIS1
              this.w_SCONT1 = IIF(this.oParentObject.w_FLSCON1="S",this.w_LISCONT1,0)
              this.w_SCONT2 = IIF(this.oParentObject.w_FLSCON1="S",this.w_LISCONT2,0)
              this.w_SCONT3 = IIF(this.oParentObject.w_FLSCON1="S",this.w_LISCONT3,0)
              this.w_SCONT4 = IIF(this.oParentObject.w_FLSCON1="S",this.w_LISCONT4,0)
            case NOT EMPTY(this.oParentObject.w_CODLIS2) AND this.w_LOOP=2
              this.w_CODLIS = this.oParentObject.w_CODLIS2
              this.w_SCONT1 = IIF(this.oParentObject.w_FLSCON2="S",this.w_LISCONT1,0)
              this.w_SCONT2 = IIF(this.oParentObject.w_FLSCON2="S",this.w_LISCONT2,0)
              this.w_SCONT3 = IIF(this.oParentObject.w_FLSCON2="S",this.w_LISCONT3,0)
              this.w_SCONT4 = IIF(this.oParentObject.w_FLSCON2="S",this.w_LISCONT4,0)
            case NOT EMPTY(this.oParentObject.w_CODLIS3) AND this.w_LOOP=3
              this.w_CODLIS = this.oParentObject.w_CODLIS3
              this.w_SCONT1 = IIF(this.oParentObject.w_FLSCON3="S",this.w_LISCONT1,0)
              this.w_SCONT2 = IIF(this.oParentObject.w_FLSCON3="S",this.w_LISCONT2,0)
              this.w_SCONT3 = IIF(this.oParentObject.w_FLSCON3="S",this.w_LISCONT3,0)
              this.w_SCONT4 = IIF(this.oParentObject.w_FLSCON3="S",this.w_LISCONT4,0)
            case NOT EMPTY(this.oParentObject.w_CODLIS4) AND this.w_LOOP=4
              this.w_CODLIS = this.oParentObject.w_CODLIS4
              this.w_SCONT1 = IIF(this.oParentObject.w_FLSCON4="S",this.w_LISCONT1,0)
              this.w_SCONT2 = IIF(this.oParentObject.w_FLSCON4="S",this.w_LISCONT2,0)
              this.w_SCONT3 = IIF(this.oParentObject.w_FLSCON4="S",this.w_LISCONT3,0)
              this.w_SCONT4 = IIF(this.oParentObject.w_FLSCON4="S",this.w_LISCONT4,0)
            case NOT EMPTY(this.oParentObject.w_CODLIS5) AND this.w_LOOP=5
              this.w_CODLIS = this.oParentObject.w_CODLIS5
              this.w_SCONT1 = IIF(this.oParentObject.w_FLSCON5="S",this.w_LISCONT1,0)
              this.w_SCONT2 = IIF(this.oParentObject.w_FLSCON5="S",this.w_LISCONT2,0)
              this.w_SCONT3 = IIF(this.oParentObject.w_FLSCON5="S",this.w_LISCONT3,0)
              this.w_SCONT4 = IIF(this.oParentObject.w_FLSCON5="S",this.w_LISCONT4,0)
          endcase
          if NOT EMPTY(this.w_CODLIS) AND (this.oParentObject.w_FLIMLSZR="S" OR LIS[this.w_LOOP]>0)
            this.w_LIPREZZO = LIS[this.w_LOOP] 
            this.w_TESTROW = 0
            if g_APPLICATION="ad hoc ENTERPRISE"
              * --- Read from LIS_TINI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.LIS_TINI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CPROWNUM"+;
                  " from "+i_cTable+" LIS_TINI where ";
                      +"LICODART = "+cp_ToStrODBC(this.w_LICODART);
                      +" and LICODLIS = "+cp_ToStrODBC(this.w_CODLIS);
                      +" and LIDATATT = "+cp_ToStrODBC(this.oParentObject.w_LIDATATT);
                      +" and LIDATDIS = "+cp_ToStrODBC(this.oParentObject.w_LIDATDIS);
                      +" and LIUNIMIS = "+cp_ToStrODBC(this.w_LIUNIMIS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CPROWNUM;
                  from (i_cTable) where;
                      LICODART = this.w_LICODART;
                      and LICODLIS = this.w_CODLIS;
                      and LIDATATT = this.oParentObject.w_LIDATATT;
                      and LIDATDIS = this.oParentObject.w_LIDATDIS;
                      and LIUNIMIS = this.w_LIUNIMIS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_TESTROW = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = 'Fallita la lettura di un listino!'
                return
              endif
              select (i_nOldArea)
            else
              * --- Read from LIS_TINI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.LIS_TINI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CPROWNUM"+;
                  " from "+i_cTable+" LIS_TINI where ";
                      +"LICODART = "+cp_ToStrODBC(this.w_LICODART);
                      +" and LICODLIS = "+cp_ToStrODBC(this.w_CODLIS);
                      +" and LIDATATT = "+cp_ToStrODBC(this.oParentObject.w_LIDATATT);
                      +" and LIDATDIS = "+cp_ToStrODBC(this.oParentObject.w_LIDATDIS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CPROWNUM;
                  from (i_cTable) where;
                      LICODART = this.w_LICODART;
                      and LICODLIS = this.w_CODLIS;
                      and LIDATATT = this.oParentObject.w_LIDATATT;
                      and LIDATDIS = this.oParentObject.w_LIDATDIS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_TESTROW = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = 'Fallita la lettura di un listino!'
                return
              endif
              select (i_nOldArea)
            endif
            * --- Verifico se per l'articolo il listino esiste..
            *     Se si elimino gli scaglioni ad esso assocciati per ricaricarli
            if i_Rows<>0 AND this.w_TESTROW<>0
              if g_APPLICATION="ad hoc ENTERPRISE"
                * --- Delete from LIS_TINI
                i_nConn=i_TableProp[this.LIS_TINI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"LICODART = "+cp_ToStrODBC(this.w_LICODART);
                        +" and LICODLIS = "+cp_ToStrODBC(this.w_CODLIS);
                        +" and LIUNIMIS = "+cp_ToStrODBC(this.w_LIUNIMIS);
                         )
                else
                  delete from (i_cTable) where;
                        LICODART = this.w_LICODART;
                        and LICODLIS = this.w_CODLIS;
                        and LIUNIMIS = this.w_LIUNIMIS;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
              else
                * --- Delete from LIS_SCAG
                i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"LICODART = "+cp_ToStrODBC(this.w_LICODART);
                        +" and LIROWNUM = "+cp_ToStrODBC(this.w_TESTROW);
                         )
                else
                  delete from (i_cTable) where;
                        LICODART = this.w_LICODART;
                        and LIROWNUM = this.w_TESTROW;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error='Fallita la cancellazione di uno scaglione!'
                  return
                endif
              endif
              this.w_CPROWNUM = this.w_TESTROW
            else
              * --- Se il listino non � presente determino l'ultimo cprownum,
              *     da qua in poi ogni nuovo numero di riga sar� ottenuto 
              *     sommando 1 a questo valore
              *     w_NEWROWNUM=0 significa che non ho ancora incontrato, per questo
              *     articolo un nuovo listino
              if this.w_NEWROWNUM=0
                this.w_TESTROW = 0
                * --- Select from LIS_TINI
                i_nConn=i_TableProp[this.LIS_TINI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select Max( CPROWNUM ) As RIGA  from "+i_cTable+" LIS_TINI ";
                      +" where LICODART= "+cp_ToStrODBC(this.w_LICODART)+"";
                       ,"_Curs_LIS_TINI")
                else
                  select Max( CPROWNUM ) As RIGA from (i_cTable);
                   where LICODART= this.w_LICODART;
                    into cursor _Curs_LIS_TINI
                endif
                if used('_Curs_LIS_TINI')
                  select _Curs_LIS_TINI
                  locate for 1=1
                  do while not(eof())
                  this.w_TESTROW = NVL( _Curs_LIS_TINI.RIGA, 0)
                    select _Curs_LIS_TINI
                    continue
                  enddo
                  use
                endif
                this.w_CPROWNUM = this.w_TESTROW
                * --- Da qui in poi non ricalcolo pi� l'ultimo CPROWNUM
                this.w_NEWROWNUM = 1
              endif
              this.w_CPROWNUM = this.w_CPROWNUM+1
              * --- Try
              local bErr_03C12CB8
              bErr_03C12CB8=bTrsErr
              this.Try_03C12CB8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
                this.w_LISINS = this.w_LISINS - 1
                this.w_oERRORLOG.AddMsgLog("Articolo: %1 fallita la scrittura nella tabella listini articoli (LIS_TINI). Listino: %2 %3",this.w_LICODART, this.w_CODLIS,Message() )     
                this.w_NOERR = .F.
              endif
              bTrsErr=bTrsErr or bErr_03C12CB8
              * --- End
            endif
            if g_APPLICATION="ad hoc ENTERPRISE"
              * --- Read from LISTINI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.LISTINI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "LSQUANTI"+;
                  " from "+i_cTable+" LISTINI where ";
                      +"LSCODLIS = "+cp_ToStrODBC(this.w_CODLIS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  LSQUANTI;
                  from (i_cTable) where;
                      LSCODLIS = this.w_CODLIS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_FLSCAGLI = NVL(cp_ToDate(_read_.LSQUANTI),cp_NullValue(_read_.LSQUANTI))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_FLSCAGLI="S"
                this.w_oERRORLOG.AddMsgLog("Articolo: %1 fallita la scrittura nella tabella listini (LIS_TINI). Listino: %2. Il listino � gestito a scaglioni",this.w_LICODART, this.w_CODLIS)     
              endif
            endif
            * --- Read from LIS_TINI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.LIS_TINI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CPROWNUM"+;
                " from "+i_cTable+" LIS_TINI where ";
                    +"LICODART = "+cp_ToStrODBC(this.w_LICODART);
                    +" and LICODLIS = "+cp_ToStrODBC(this.w_CODLIS);
                    +" and LIDATATT = "+cp_ToStrODBC(this.oParentObject.w_LIDATATT);
                    +" and LIDATDIS = "+cp_ToStrODBC(this.oParentObject.w_LIDATDIS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CPROWNUM;
                from (i_cTable) where;
                    LICODART = this.w_LICODART;
                    and LICODLIS = this.w_CODLIS;
                    and LIDATATT = this.oParentObject.w_LIDATATT;
                    and LIDATDIS = this.oParentObject.w_LIDATDIS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TESTROW = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = 'Fallita la lettura di un listino!'
              return
            endif
            select (i_nOldArea)
            * --- Verifico se per l'articolo il listino esiste..
            *     Se si elimino gli scaglioni ad esso assocciati per ricaricarli
            if this.w_NOERR
              * --- Try
              local bErr_03C16D38
              bErr_03C16D38=bTrsErr
              this.Try_03C16D38()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
                if g_APPLICATION="ad hoc ENTERPRISE"
                  this.w_oERRORLOG.AddMsgLog("Articolo: %1 fallita la scrittura nella tabella listini (LIS_TINI). Listino: %2 %3",this.w_LICODART, this.w_CODLIS,Message() )     
                  this.w_LISINS = this.w_LISINS - 1
                else
                  this.w_oERRORLOG.AddMsgLog("Articolo: %1 fallita la scrittura nella tabella scaglioni listini (LIS_SCAG). Listino: %2 %3",this.w_LICODART, this.w_CODLIS,Message() )     
                  this.w_SCAINS = this.w_SCAINS - 1
                endif
                this.w_NOERR = .F.
              endif
              bTrsErr=bTrsErr or bErr_03C16D38
              * --- End
            endif
          endif
          this.w_LOOP = this.w_LOOP + 1
        enddo
      endif
    endif
  endproc
  proc Try_05A0C810()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_UNIINS = this.w_UNIINS + 1
    * --- Insert into UNIMIS
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UNIMIS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"UMCODICE"+",UMDESCRI"+",UMFLFRAZ"+",UMMODUM2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_UMCODICE),'UNIMIS','UMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UMDESCRI),'UNIMIS','UMDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UMFLFRAZ),'UNIMIS','UMFLFRAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UMMODUM2),'UNIMIS','UMMODUM2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'UMCODICE',this.w_UMCODICE,'UMDESCRI',this.w_UMDESCRI,'UMFLFRAZ',this.w_UMFLFRAZ,'UMMODUM2',this.w_UMMODUM2)
      insert into (i_cTable) (UMCODICE,UMDESCRI,UMFLFRAZ,UMMODUM2 &i_ccchkf. );
         values (;
           this.w_UMCODICE;
           ,this.w_UMDESCRI;
           ,this.w_UMFLFRAZ;
           ,this.w_UMMODUM2;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Fallito inserimento di una nuova unit� di misura!'
      return
    endif
    this.w_OKINS = .T.
    return
  proc Try_05A07590()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from UNIMIS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UMDESCRI,UMFLFRAZ,UMMODUM2"+;
        " from "+i_cTable+" UNIMIS where ";
            +"UMCODICE = "+cp_ToStrODBC(this.w_UMCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UMDESCRI,UMFLFRAZ,UMMODUM2;
        from (i_cTable) where;
            UMCODICE = this.w_UMCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OMDESCRI = NVL(cp_ToDate(_read_.UMDESCRI),cp_NullValue(_read_.UMDESCRI))
      this.w_OMFLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
      this.w_OMMODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_UMDESCRI = IIF(EMPTY(this.w_UMDESCRI),this.w_OMDESCRI,this.w_UMDESCRI)
    this.w_UMFLFRAZ = IIF(EMPTY(this.w_UMFLFRAZ),this.w_OMFLFRAZ,this.w_UMFLFRAZ)
    this.w_UMMODUM2 = IIF(EMPTY(this.w_UMMODUM2),this.w_OMMODUM2,this.w_UMMODUM2)
    * --- Write into UNIMIS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.UNIMIS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"UMDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_UMDESCRI),'UNIMIS','UMDESCRI');
      +",UMFLFRAZ ="+cp_NullLink(cp_ToStrODBC(this.w_UMFLFRAZ),'UNIMIS','UMFLFRAZ');
      +",UMMODUM2 ="+cp_NullLink(cp_ToStrODBC(this.w_UMMODUM2),'UNIMIS','UMMODUM2');
          +i_ccchkf ;
      +" where ";
          +"UMCODICE = "+cp_ToStrODBC(this.w_UMCODICE);
             )
    else
      update (i_cTable) set;
          UMDESCRI = this.w_UMDESCRI;
          ,UMFLFRAZ = this.w_UMFLFRAZ;
          ,UMMODUM2 = this.w_UMMODUM2;
          &i_ccchkf. ;
       where;
          UMCODICE = this.w_UMCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Fallita la scrittura di una unit� di misura!'
      return
    endif
    return
  proc Try_058E2160()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODART"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_ARCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODART;
        from (i_cTable) where;
            CACODICE = this.w_ARCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CACODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = 'Fallita la lettura di un Codici di Ricerca'
      return
    endif
    select (i_nOldArea)
    return
  proc Try_059D9770()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_059DC3E0
    bErr_059DC3E0=bTrsErr
    this.Try_059DC3E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      i_Rows=-1
    endif
    bTrsErr=bTrsErr or bErr_059DC3E0
    * --- End
    if i_Rows=0
      this.w_ARFLLOTT = IIF(EMPTY(this.w_ARFLLOTT),"N",this.w_ARFLLOTT)
      this.w_ARCATOPE = IIF(EMPTY(this.w_ARCATOPE),"AR",this.w_ARCATOPE)
      this.w_ARPROPRE = IIF(EMPTY(this.w_ARPROPRE),"E",this.w_ARPROPRE)
      if g_APPLICATION="ad hoc ENTERPRISE"
        * --- Insert into ART_ICOL
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ARCATCON"+",ARCATOMO"+",ARCATSCM"+",ARCOCOL1"+",ARCOCOL2"+",ARCODART"+",ARCODCLA"+",ARCODFAM"+",ARCODIVA"+",ARCODMAR"+",ARCODRIC"+",ARDESART"+",ARDESSUP"+",ARDESVO2"+",ARDESVOL"+",ARDIMAL2"+",ARDIMALT"+",ARDIMLA2"+",ARDIMLAR"+",ARDIMLU2"+",ARDIMLUN"+",ARDTINVA"+",ARDTOBSO"+",ARFLCON2"+",ARFLCONA"+",ARFLDISP"+",ARFLESAU"+",ARFLINVE"+",ARFLSTCO"+",ARGRUMER"+",ARGRUPRO"+",ARMOLSUP"+",ARMOLTIP"+",ARNOMENC"+",AROPERAT"+",ARPESLO2"+",ARPESLOR"+",ARPESNE2"+",ARPESNET"+",ARPROPRE"+",ARPZCON2"+",ARPZCONF"+",ARSTASUP"+",ARTIPART"+",ARTIPBAR"+",ARTIPGES"+",ARTIPMA1"+",ARTIPMA2"+",ARTPCON2"+",ARTPCONF"+",ARUMDIM2"+",ARUMDIME"+",ARUMSUPP"+",ARUMVOL2"+",ARUMVOLU"+",ARUNMIS1"+",ARUNMIS2"+",ARCAUZIO"+",ARIMBREN"+",ARIMBCAU"+",ARCAUZI2"+",ARIMBRE2"+",ARIMBCA2"+",ARTIPRIF"+",ARCODVAR"+",ARFLLOTT"+",ARFLUBIC"+",ARCODCAA"+",ARFLESIS"+",ARFLRIVA"+",ARPROINT"+",ARPROEST"+",ARPROCLA"+",ARMAGPRE"+",ARCODPLA"+",ARCLAPRE"+",ARCLARIF"+",ARFLCOMM"+",ARFLGIS4"+",ARPUBWEB"+",ARGESMAT"+",ARCLAMAT"+",ARCENCOS"+",ARFLUMVA"+",ARPREZUM"+",ARFLCESP"+",ARCATCES"+",ARDATINT"+",ARTIPOPE"+",ARCATOPE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_ARCATCON),'ART_ICOL','ARCATCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCATOMO),'ART_ICOL','ARCATOMO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCATSCM),'ART_ICOL','ARCATSCM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCOCOL1),'ART_ICOL','ARCOCOL1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCOCOL2),'ART_ICOL','ARCOCOL2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'ART_ICOL','ARCODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODCLA),'ART_ICOL','ARCODCLA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODFAM),'ART_ICOL','ARCODFAM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODIVA),'ART_ICOL','ARCODIVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODMAR),'ART_ICOL','ARCODMAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODRIC),'ART_ICOL','ARCODRIC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'ART_ICOL','ARDESART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'ART_ICOL','ARDESSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESVO2),'ART_ICOL','ARDESVO2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESVOL),'ART_ICOL','ARDESVOL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDIMAL2),'ART_ICOL','ARDIMAL2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDIMALT),'ART_ICOL','ARDIMALT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLA2),'ART_ICOL','ARDIMLA2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLAR),'ART_ICOL','ARDIMLAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLU2),'ART_ICOL','ARDIMLU2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLUN),'ART_ICOL','ARDIMLUN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDTINVA),'ART_ICOL','ARDTINVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDTOBSO),'ART_ICOL','ARDTOBSO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLCON2),'ART_ICOL','ARFLCON2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLCONA),'ART_ICOL','ARFLCONA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLDISP),'ART_ICOL','ARFLDISP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLESAU),'ART_ICOL','ARFLESAU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLINVE),'ART_ICOL','ARFLINVE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLSTCO),'ART_ICOL','ARFLSTCO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARGRUMER),'ART_ICOL','ARGRUMER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARGRUPRO),'ART_ICOL','ARGRUPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARMOLSUP),'ART_ICOL','ARMOLSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARMOLTIP),'ART_ICOL','ARMOLTIP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARNOMENC),'ART_ICOL','ARNOMENC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_AROPERAT),'ART_ICOL','AROPERAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPESLO2),'ART_ICOL','ARPESLO2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPESLOR),'ART_ICOL','ARPESLOR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPESNE2),'ART_ICOL','ARPESNE2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPESNET),'ART_ICOL','ARPESNET');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPROPRE),'ART_ICOL','ARPROPRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPZCON2),'ART_ICOL','ARPZCON2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPZCONF),'ART_ICOL','ARPZCONF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARSTASUP),'ART_ICOL','ARSTASUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPART),'ART_ICOL','ARTIPART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPBAR),'ART_ICOL','ARTIPBAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPGES),'ART_ICOL','ARTIPGES');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPMA1),'ART_ICOL','ARTIPMA1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPMA2),'ART_ICOL','ARTIPMA2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTPCON2),'ART_ICOL','ARTPCON2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTPCONF),'ART_ICOL','ARTPCONF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUMDIM2),'ART_ICOL','ARUMDIM2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUMDIME),'ART_ICOL','ARUMDIME');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUMSUPP),'ART_ICOL','ARUMSUPP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUMVOL2),'ART_ICOL','ARUMVOL2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUMVOLU),'ART_ICOL','ARUMVOLU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUNMIS1),'ART_ICOL','ARUNMIS1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUNMIS2),'ART_ICOL','ARUNMIS2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCAUZIO),'ART_ICOL','ARCAUZIO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARIMBREN),'ART_ICOL','ARIMBREN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARIMBCAU),'ART_ICOL','ARIMBCAU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCAUZI2),'ART_ICOL','ARCAUZI2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARIMBRE2),'ART_ICOL','ARIMBRE2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARIMBCA2),'ART_ICOL','ARIMBCA2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPRIF),'ART_ICOL','ARTIPRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODVAR),'ART_ICOL','ARCODVAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLLOTT),'ART_ICOL','ARFLLOTT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLUBIC),'ART_ICOL','ARFLUBIC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODCAA),'ART_ICOL','ARCODCAA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLESIS),'ART_ICOL','ARFLESIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLRIVA),'ART_ICOL','ARFLRIVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPROINT),'ART_ICOL','ARPROINT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPROEST),'ART_ICOL','ARPROEST');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPROCLA),'ART_ICOL','ARPROCLA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARMAGPRE),'ART_ICOL','ARMAGPRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODPLA),'ART_ICOL','ARCODPLA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCLAPRE),'ART_ICOL','ARCLAPRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCLARIF),'ART_ICOL','ARCLARIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLCOMM),'ART_ICOL','ARFLCOMM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLGIS4),'ART_ICOL','ARFLGIS4');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPUBWEB),'ART_ICOL','ARPUBWEB');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARGESMAT),'ART_ICOL','ARGESMAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCLAMAT),'ART_ICOL','ARCLAMAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCENCOS),'ART_ICOL','ARCENCOS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLUMVA),'ART_ICOL','ARFLUMVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPREZUM),'ART_ICOL','ARPREZUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLCESP),'ART_ICOL','ARFLCESP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCATCES),'ART_ICOL','ARCATCES');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDATINT),'ART_ICOL','ARDATINT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPOPE),'ART_ICOL','ARTIPOPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCATOPE),'ART_ICOL','ARCATOPE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ARCATCON',this.w_ARCATCON,'ARCATOMO',this.w_ARCATOMO,'ARCATSCM',this.w_ARCATSCM,'ARCOCOL1',this.w_ARCOCOL1,'ARCOCOL2',this.w_ARCOCOL2,'ARCODART',this.w_ARCODART,'ARCODCLA',this.w_ARCODCLA,'ARCODFAM',this.w_ARCODFAM,'ARCODIVA',this.w_ARCODIVA,'ARCODMAR',this.w_ARCODMAR,'ARCODRIC',this.w_ARCODRIC,'ARDESART',this.w_ARDESART)
          insert into (i_cTable) (ARCATCON,ARCATOMO,ARCATSCM,ARCOCOL1,ARCOCOL2,ARCODART,ARCODCLA,ARCODFAM,ARCODIVA,ARCODMAR,ARCODRIC,ARDESART,ARDESSUP,ARDESVO2,ARDESVOL,ARDIMAL2,ARDIMALT,ARDIMLA2,ARDIMLAR,ARDIMLU2,ARDIMLUN,ARDTINVA,ARDTOBSO,ARFLCON2,ARFLCONA,ARFLDISP,ARFLESAU,ARFLINVE,ARFLSTCO,ARGRUMER,ARGRUPRO,ARMOLSUP,ARMOLTIP,ARNOMENC,AROPERAT,ARPESLO2,ARPESLOR,ARPESNE2,ARPESNET,ARPROPRE,ARPZCON2,ARPZCONF,ARSTASUP,ARTIPART,ARTIPBAR,ARTIPGES,ARTIPMA1,ARTIPMA2,ARTPCON2,ARTPCONF,ARUMDIM2,ARUMDIME,ARUMSUPP,ARUMVOL2,ARUMVOLU,ARUNMIS1,ARUNMIS2,ARCAUZIO,ARIMBREN,ARIMBCAU,ARCAUZI2,ARIMBRE2,ARIMBCA2,ARTIPRIF,ARCODVAR,ARFLLOTT,ARFLUBIC,ARCODCAA,ARFLESIS,ARFLRIVA,ARPROINT,ARPROEST,ARPROCLA,ARMAGPRE,ARCODPLA,ARCLAPRE,ARCLARIF,ARFLCOMM,ARFLGIS4,ARPUBWEB,ARGESMAT,ARCLAMAT,ARCENCOS,ARFLUMVA,ARPREZUM,ARFLCESP,ARCATCES,ARDATINT,ARTIPOPE,ARCATOPE &i_ccchkf. );
             values (;
               this.w_ARCATCON;
               ,this.w_ARCATOMO;
               ,this.w_ARCATSCM;
               ,this.w_ARCOCOL1;
               ,this.w_ARCOCOL2;
               ,this.w_ARCODART;
               ,this.w_ARCODCLA;
               ,this.w_ARCODFAM;
               ,this.w_ARCODIVA;
               ,this.w_ARCODMAR;
               ,this.w_ARCODRIC;
               ,this.w_ARDESART;
               ,this.w_ARDESSUP;
               ,this.w_ARDESVO2;
               ,this.w_ARDESVOL;
               ,this.w_ARDIMAL2;
               ,this.w_ARDIMALT;
               ,this.w_ARDIMLA2;
               ,this.w_ARDIMLAR;
               ,this.w_ARDIMLU2;
               ,this.w_ARDIMLUN;
               ,this.w_ARDTINVA;
               ,this.w_ARDTOBSO;
               ,this.w_ARFLCON2;
               ,this.w_ARFLCONA;
               ,this.w_ARFLDISP;
               ,this.w_ARFLESAU;
               ,this.w_ARFLINVE;
               ,this.w_ARFLSTCO;
               ,this.w_ARGRUMER;
               ,this.w_ARGRUPRO;
               ,this.w_ARMOLSUP;
               ,this.w_ARMOLTIP;
               ,this.w_ARNOMENC;
               ,this.w_AROPERAT;
               ,this.w_ARPESLO2;
               ,this.w_ARPESLOR;
               ,this.w_ARPESNE2;
               ,this.w_ARPESNET;
               ,this.w_ARPROPRE;
               ,this.w_ARPZCON2;
               ,this.w_ARPZCONF;
               ,this.w_ARSTASUP;
               ,this.w_ARTIPART;
               ,this.w_ARTIPBAR;
               ,this.w_ARTIPGES;
               ,this.w_ARTIPMA1;
               ,this.w_ARTIPMA2;
               ,this.w_ARTPCON2;
               ,this.w_ARTPCONF;
               ,this.w_ARUMDIM2;
               ,this.w_ARUMDIME;
               ,this.w_ARUMSUPP;
               ,this.w_ARUMVOL2;
               ,this.w_ARUMVOLU;
               ,this.w_ARUNMIS1;
               ,this.w_ARUNMIS2;
               ,this.w_ARCAUZIO;
               ,this.w_ARIMBREN;
               ,this.w_ARIMBCAU;
               ,this.w_ARCAUZI2;
               ,this.w_ARIMBRE2;
               ,this.w_ARIMBCA2;
               ,this.w_ARTIPRIF;
               ,this.w_ARCODVAR;
               ,this.w_ARFLLOTT;
               ,this.w_ARFLUBIC;
               ,this.w_ARCODCAA;
               ,this.w_ARFLESIS;
               ,this.w_ARFLRIVA;
               ,this.w_ARPROINT;
               ,this.w_ARPROEST;
               ,this.w_ARPROCLA;
               ,this.w_ARMAGPRE;
               ,this.w_ARCODPLA;
               ,this.w_ARCLAPRE;
               ,this.w_ARCLARIF;
               ,this.w_ARFLCOMM;
               ,this.w_ARFLGIS4;
               ,this.w_ARPUBWEB;
               ,this.w_ARGESMAT;
               ,this.w_ARCLAMAT;
               ,this.w_ARCENCOS;
               ,this.w_ARFLUMVA;
               ,this.w_ARPREZUM;
               ,this.w_ARFLCESP;
               ,this.w_ARCATCES;
               ,this.w_ARDATINT;
               ,this.w_ARTIPOPE;
               ,this.w_ARCATOPE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Fallito inserimento di un nuovo articolo!'
          return
        endif
      else
        * --- Insert into ART_ICOL
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ARARTPOS"+",ARCATCON"+",ARCATOMO"+",ARCATSCM"+",ARCLALOT"+",ARCOCOL1"+",ARCOCOL2"+",ARCODART"+",ARCODCLA"+",ARCODDIS"+",ARCODFAM"+",ARCODGRU"+",ARCODIVA"+",ARCODMAR"+",ARCODREP"+",ARCODRIC"+",ARCODSOT"+",ARDESART"+",ARDESSUP"+",ARDESVO2"+",ARDESVOL"+",ARDIMAL2"+",ARDIMALT"+",ARDIMLA2"+",ARDIMLAR"+",ARDIMLU2"+",ARDIMLUN"+",ARDISLOT"+",ARDTINVA"+",ARDTOBSO"+",ARFLCOMP"+",ARFLCON2"+",ARFLCONA"+",ARFLDISC"+",ARFLDISP"+",ARFLESAU"+",ARFLESUL"+",ARFLINVE"+",ARFLLMAG"+",ARFLPECO"+",ARFLSERG"+",ARFLSTCO"+",ARFLUSEP"+",ARGRUMER"+",ARGRUPRO"+",ARMOLSUP"+",ARMOLTIP"+",ARNOMENC"+",AROPERAT"+",ARPESLO2"+",ARPESLOR"+",ARPESNE2"+",ARPESNET"+",ARPROPRE"+",ARPZCON2"+",ARPZCONF"+",ARSTACOD"+",ARSTASUP"+",ARTIPART"+",ARTIPPKR"+",ARTIPBAR"+",ARTIPCO1"+",ARTIPCO2"+",ARTIPGES"+",ARTIPMA1"+",ARTIPMA2"+",ARTIPPRE"+",ARTIPSER"+",ARTPCON2"+",ARTPCONF"+",ARUMDIM2"+",ARUMDIME"+",ARUMSUPP"+",ARUMVOL2"+",ARUMVOLU"+",ARUNMIS1"+",ARUNMIS2"+",ARVOCCEN"+",ARVOCRIC"+",ARCLAMAT"+",ARGESMAT"+",ARFLLOTT"+",ARDATINT"+",ARTIPOPE"+",ARCATOPE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_ARARTPOS),'ART_ICOL','ARARTPOS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCATCON),'ART_ICOL','ARCATCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCATOMO),'ART_ICOL','ARCATOMO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCATSCM),'ART_ICOL','ARCATSCM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCLALOT),'ART_ICOL','ARCLALOT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCOCOL1),'ART_ICOL','ARCOCOL1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCOCOL2),'ART_ICOL','ARCOCOL2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'ART_ICOL','ARCODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODCLA),'ART_ICOL','ARCODCLA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODDIS),'ART_ICOL','ARCODDIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODFAM),'ART_ICOL','ARCODFAM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODGRU),'ART_ICOL','ARCODGRU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODIVA),'ART_ICOL','ARCODIVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODMAR),'ART_ICOL','ARCODMAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODREP),'ART_ICOL','ARCODREP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODRIC),'ART_ICOL','ARCODRIC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODSOT),'ART_ICOL','ARCODSOT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'ART_ICOL','ARDESART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'ART_ICOL','ARDESSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESVO2),'ART_ICOL','ARDESVO2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESVOL),'ART_ICOL','ARDESVOL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDIMAL2),'ART_ICOL','ARDIMAL2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDIMALT),'ART_ICOL','ARDIMALT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLA2),'ART_ICOL','ARDIMLA2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLAR),'ART_ICOL','ARDIMLAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLU2),'ART_ICOL','ARDIMLU2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLUN),'ART_ICOL','ARDIMLUN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDISLOT),'ART_ICOL','ARDISLOT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDTINVA),'ART_ICOL','ARDTINVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDTOBSO),'ART_ICOL','ARDTOBSO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLCOMP),'ART_ICOL','ARFLCOMP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLCON2),'ART_ICOL','ARFLCON2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLCONA),'ART_ICOL','ARFLCONA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLDISC),'ART_ICOL','ARFLDISC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLDISP),'ART_ICOL','ARFLDISP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLESAU),'ART_ICOL','ARFLESAU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLESUL),'ART_ICOL','ARFLESUL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLINVE),'ART_ICOL','ARFLINVE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLLMAG),'ART_ICOL','ARFLLMAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLPECO),'ART_ICOL','ARFLPECO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLSERG),'ART_ICOL','ARFLSERG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLSTCO),'ART_ICOL','ARFLSTCO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLUSEP),'ART_ICOL','ARFLUSEP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARGRUMER),'ART_ICOL','ARGRUMER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARGRUPRO),'ART_ICOL','ARGRUPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARMOLSUP),'ART_ICOL','ARMOLSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARMOLTIP),'ART_ICOL','ARMOLTIP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARNOMENC),'ART_ICOL','ARNOMENC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_AROPERAT),'ART_ICOL','AROPERAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPESLO2),'ART_ICOL','ARPESLO2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPESLOR),'ART_ICOL','ARPESLOR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPESNE2),'ART_ICOL','ARPESNE2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPESNET),'ART_ICOL','ARPESNET');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPROPRE),'ART_ICOL','ARPROPRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPZCON2),'ART_ICOL','ARPZCON2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARPZCONF),'ART_ICOL','ARPZCONF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARSTACOD),'ART_ICOL','ARSTACOD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARSTASUP),'ART_ICOL','ARSTASUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPART),'ART_ICOL','ARTIPART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPART),'ART_ICOL','ARTIPPKR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPBAR),'ART_ICOL','ARTIPBAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPCO1),'ART_ICOL','ARTIPCO1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPCO2),'ART_ICOL','ARTIPCO2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPGES),'ART_ICOL','ARTIPGES');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPMA1),'ART_ICOL','ARTIPMA1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPMA2),'ART_ICOL','ARTIPMA2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPPRE),'ART_ICOL','ARTIPPRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPSER),'ART_ICOL','ARTIPSER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTPCON2),'ART_ICOL','ARTPCON2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTPCONF),'ART_ICOL','ARTPCONF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUMDIM2),'ART_ICOL','ARUMDIM2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUMDIME),'ART_ICOL','ARUMDIME');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUMSUPP),'ART_ICOL','ARUMSUPP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUMVOL2),'ART_ICOL','ARUMVOL2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUMVOLU),'ART_ICOL','ARUMVOLU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUNMIS1),'ART_ICOL','ARUNMIS1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUNMIS2),'ART_ICOL','ARUNMIS2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARVOCCEN),'ART_ICOL','ARVOCCEN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARVOCRIC),'ART_ICOL','ARVOCRIC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCLAMAT),'ART_ICOL','ARCLAMAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARGESMAT),'ART_ICOL','ARGESMAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLLOTT),'ART_ICOL','ARFLLOTT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARDATINT),'ART_ICOL','ARDATINT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPOPE),'ART_ICOL','ARTIPOPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARCATOPE),'ART_ICOL','ARCATOPE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ARARTPOS',this.w_ARARTPOS,'ARCATCON',this.w_ARCATCON,'ARCATOMO',this.w_ARCATOMO,'ARCATSCM',this.w_ARCATSCM,'ARCLALOT',this.w_ARCLALOT,'ARCOCOL1',this.w_ARCOCOL1,'ARCOCOL2',this.w_ARCOCOL2,'ARCODART',this.w_ARCODART,'ARCODCLA',this.w_ARCODCLA,'ARCODDIS',this.w_ARCODDIS,'ARCODFAM',this.w_ARCODFAM,'ARCODGRU',this.w_ARCODGRU)
          insert into (i_cTable) (ARARTPOS,ARCATCON,ARCATOMO,ARCATSCM,ARCLALOT,ARCOCOL1,ARCOCOL2,ARCODART,ARCODCLA,ARCODDIS,ARCODFAM,ARCODGRU,ARCODIVA,ARCODMAR,ARCODREP,ARCODRIC,ARCODSOT,ARDESART,ARDESSUP,ARDESVO2,ARDESVOL,ARDIMAL2,ARDIMALT,ARDIMLA2,ARDIMLAR,ARDIMLU2,ARDIMLUN,ARDISLOT,ARDTINVA,ARDTOBSO,ARFLCOMP,ARFLCON2,ARFLCONA,ARFLDISC,ARFLDISP,ARFLESAU,ARFLESUL,ARFLINVE,ARFLLMAG,ARFLPECO,ARFLSERG,ARFLSTCO,ARFLUSEP,ARGRUMER,ARGRUPRO,ARMOLSUP,ARMOLTIP,ARNOMENC,AROPERAT,ARPESLO2,ARPESLOR,ARPESNE2,ARPESNET,ARPROPRE,ARPZCON2,ARPZCONF,ARSTACOD,ARSTASUP,ARTIPART,ARTIPPKR,ARTIPBAR,ARTIPCO1,ARTIPCO2,ARTIPGES,ARTIPMA1,ARTIPMA2,ARTIPPRE,ARTIPSER,ARTPCON2,ARTPCONF,ARUMDIM2,ARUMDIME,ARUMSUPP,ARUMVOL2,ARUMVOLU,ARUNMIS1,ARUNMIS2,ARVOCCEN,ARVOCRIC,ARCLAMAT,ARGESMAT,ARFLLOTT,ARDATINT,ARTIPOPE,ARCATOPE &i_ccchkf. );
             values (;
               this.w_ARARTPOS;
               ,this.w_ARCATCON;
               ,this.w_ARCATOMO;
               ,this.w_ARCATSCM;
               ,this.w_ARCLALOT;
               ,this.w_ARCOCOL1;
               ,this.w_ARCOCOL2;
               ,this.w_ARCODART;
               ,this.w_ARCODCLA;
               ,this.w_ARCODDIS;
               ,this.w_ARCODFAM;
               ,this.w_ARCODGRU;
               ,this.w_ARCODIVA;
               ,this.w_ARCODMAR;
               ,this.w_ARCODREP;
               ,this.w_ARCODRIC;
               ,this.w_ARCODSOT;
               ,this.w_ARDESART;
               ,this.w_ARDESSUP;
               ,this.w_ARDESVO2;
               ,this.w_ARDESVOL;
               ,this.w_ARDIMAL2;
               ,this.w_ARDIMALT;
               ,this.w_ARDIMLA2;
               ,this.w_ARDIMLAR;
               ,this.w_ARDIMLU2;
               ,this.w_ARDIMLUN;
               ,this.w_ARDISLOT;
               ,this.w_ARDTINVA;
               ,this.w_ARDTOBSO;
               ,this.w_ARFLCOMP;
               ,this.w_ARFLCON2;
               ,this.w_ARFLCONA;
               ,this.w_ARFLDISC;
               ,this.w_ARFLDISP;
               ,this.w_ARFLESAU;
               ,this.w_ARFLESUL;
               ,this.w_ARFLINVE;
               ,this.w_ARFLLMAG;
               ,this.w_ARFLPECO;
               ,this.w_ARFLSERG;
               ,this.w_ARFLSTCO;
               ,this.w_ARFLUSEP;
               ,this.w_ARGRUMER;
               ,this.w_ARGRUPRO;
               ,this.w_ARMOLSUP;
               ,this.w_ARMOLTIP;
               ,this.w_ARNOMENC;
               ,this.w_AROPERAT;
               ,this.w_ARPESLO2;
               ,this.w_ARPESLOR;
               ,this.w_ARPESNE2;
               ,this.w_ARPESNET;
               ,this.w_ARPROPRE;
               ,this.w_ARPZCON2;
               ,this.w_ARPZCONF;
               ,this.w_ARSTACOD;
               ,this.w_ARSTASUP;
               ,this.w_ARTIPART;
               ,this.w_ARTIPART;
               ,this.w_ARTIPBAR;
               ,this.w_ARTIPCO1;
               ,this.w_ARTIPCO2;
               ,this.w_ARTIPGES;
               ,this.w_ARTIPMA1;
               ,this.w_ARTIPMA2;
               ,this.w_ARTIPPRE;
               ,this.w_ARTIPSER;
               ,this.w_ARTPCON2;
               ,this.w_ARTPCONF;
               ,this.w_ARUMDIM2;
               ,this.w_ARUMDIME;
               ,this.w_ARUMSUPP;
               ,this.w_ARUMVOL2;
               ,this.w_ARUMVOLU;
               ,this.w_ARUNMIS1;
               ,this.w_ARUNMIS2;
               ,this.w_ARVOCCEN;
               ,this.w_ARVOCRIC;
               ,this.w_ARCLAMAT;
               ,this.w_ARGESMAT;
               ,this.w_ARFLLOTT;
               ,this.w_ARDATINT;
               ,this.w_ARTIPOPE;
               ,this.w_ARCATOPE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Fallito inserimento di un nuovo articolo!'
          return
        endif
      endif
      this.w_CACODICE = IIF(EMPTY(this.w_CACODICE), this.w_ARCODART, this.w_CACODICE)
      this.w_CACODART = this.w_ARCODART
      this.w_LICODART = this.w_ARCODART
      this.w_OKINS = .T.
      this.w_TEST = .T.
      this.w_INART = this.w_INART + 1
    endif
    return
  proc Try_059DA850()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_RIOR
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODART"+",PRCODFOR"+",PRCODPRO"+",PRCOSSTA"+",PRDISMIN"+",PRGIOAPP"+",PRGIOINV"+",PRLEAMPS"+",PRLOTRIO"+",PRPERSCA"+",PRPUNRIO"+",PRQTAMIN"+",PRSCOMAX"+",PRSCOMIN"+",PRTIPCON"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'PAR_RIOR','PRCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRCODFOR),'PAR_RIOR','PRCODFOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRCODPRO),'PAR_RIOR','PRCODPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRCOSSTA),'PAR_RIOR','PRCOSSTA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRDISMIN),'PAR_RIOR','PRDISMIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRGIOAPP),'PAR_RIOR','PRGIOAPP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRGIOINV),'PAR_RIOR','PRGIOINV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRLEAMPS),'PAR_RIOR','PRLEAMPS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRLOTRIO),'PAR_RIOR','PRLOTRIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRPERSCA),'PAR_RIOR','PRPERSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRPUNRIO),'PAR_RIOR','PRPUNRIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRQTAMIN),'PAR_RIOR','PRQTAMIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRSCOMAX),'PAR_RIOR','PRSCOMAX');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRSCOMIN),'PAR_RIOR','PRSCOMIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRTIPCON),'PAR_RIOR','PRTIPCON');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_ARCODART,'PRCODFOR',this.w_PRCODFOR,'PRCODPRO',this.w_PRCODPRO,'PRCOSSTA',this.w_PRCOSSTA,'PRDISMIN',this.w_PRDISMIN,'PRGIOAPP',this.w_PRGIOAPP,'PRGIOINV',this.w_PRGIOINV,'PRLEAMPS',this.w_PRLEAMPS,'PRLOTRIO',this.w_PRLOTRIO,'PRPERSCA',this.w_PRPERSCA,'PRPUNRIO',this.w_PRPUNRIO,'PRQTAMIN',this.w_PRQTAMIN)
      insert into (i_cTable) (PRCODART,PRCODFOR,PRCODPRO,PRCOSSTA,PRDISMIN,PRGIOAPP,PRGIOINV,PRLEAMPS,PRLOTRIO,PRPERSCA,PRPUNRIO,PRQTAMIN,PRSCOMAX,PRSCOMIN,PRTIPCON &i_ccchkf. );
         values (;
           this.w_ARCODART;
           ,this.w_PRCODFOR;
           ,this.w_PRCODPRO;
           ,this.w_PRCOSSTA;
           ,this.w_PRDISMIN;
           ,this.w_PRGIOAPP;
           ,this.w_PRGIOINV;
           ,this.w_PRLEAMPS;
           ,this.w_PRLOTRIO;
           ,this.w_PRPERSCA;
           ,this.w_PRPUNRIO;
           ,this.w_PRQTAMIN;
           ,this.w_PRSCOMAX;
           ,this.w_PRSCOMIN;
           ,this.w_PRTIPCON;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Fallito inserimento di un nuovo dato articolo per valoriz.!'
      return
    endif
    this.w_OKINSDAT = .T.
    return
  proc Try_0596C610()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if g_APPLICATION="ad hoc ENTERPRISE"
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              ARCODART = this.w_ARCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ORDESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
        this.w_ORDESSUP = NVL(cp_ToDate(_read_.ARDESSUP),cp_NullValue(_read_.ARDESSUP))
        this.w_ORUNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
        this.w_OROPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
        this.w_ORMOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
        this.w_ORUNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
        this.w_ORGRUMER = NVL(cp_ToDate(_read_.ARGRUMER),cp_NullValue(_read_.ARGRUMER))
        this.w_ORCODFAM = NVL(cp_ToDate(_read_.ARCODFAM),cp_NullValue(_read_.ARCODFAM))
        this.w_ORGRUPRO = NVL(cp_ToDate(_read_.ARGRUPRO),cp_NullValue(_read_.ARGRUPRO))
        this.w_ORCATSCM = NVL(cp_ToDate(_read_.ARCATSCM),cp_NullValue(_read_.ARCATSCM))
        this.w_ORTIPMA1 = NVL(cp_ToDate(_read_.ARTIPMA1),cp_NullValue(_read_.ARTIPMA1))
        this.w_ORTIPMA2 = NVL(cp_ToDate(_read_.ARTIPMA2),cp_NullValue(_read_.ARTIPMA2))
        this.w_ORCODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
        this.w_ORFLINVE = NVL(cp_ToDate(_read_.ARFLINVE),cp_NullValue(_read_.ARFLINVE))
        this.w_ORFLESAU = NVL(cp_ToDate(_read_.ARFLESAU),cp_NullValue(_read_.ARFLESAU))
        this.w_ORCATOMO = NVL(cp_ToDate(_read_.ARCATOMO),cp_NullValue(_read_.ARCATOMO))
        this.w_ORCATCON = NVL(cp_ToDate(_read_.ARCATCON),cp_NullValue(_read_.ARCATCON))
        this.w_ORTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
        this.w_ORFLSTCO = NVL(cp_ToDate(_read_.ARFLSTCO),cp_NullValue(_read_.ARFLSTCO))
        this.w_ORPESNET = NVL(cp_ToDate(_read_.ARPESNET),cp_NullValue(_read_.ARPESNET))
        this.w_ORPESNE2 = NVL(cp_ToDate(_read_.ARPESNE2),cp_NullValue(_read_.ARPESNE2))
        this.w_ORPESLOR = NVL(cp_ToDate(_read_.ARPESLOR),cp_NullValue(_read_.ARPESLOR))
        this.w_ORPESLO2 = NVL(cp_ToDate(_read_.ARPESLO2),cp_NullValue(_read_.ARPESLO2))
        this.w_ORDESVOL = NVL(cp_ToDate(_read_.ARDESVOL),cp_NullValue(_read_.ARDESVOL))
        this.w_ORDESVO2 = NVL(cp_ToDate(_read_.ARDESVO2),cp_NullValue(_read_.ARDESVO2))
        this.w_ORUMVOLU = NVL(cp_ToDate(_read_.ARUMVOLU),cp_NullValue(_read_.ARUMVOLU))
        this.w_ORUMVOL2 = NVL(cp_ToDate(_read_.ARUMVOL2),cp_NullValue(_read_.ARUMVOL2))
        this.w_ORTPCONF = NVL(cp_ToDate(_read_.ARTPCONF),cp_NullValue(_read_.ARTPCONF))
        this.w_ORTPCON2 = NVL(cp_ToDate(_read_.ARTPCON2),cp_NullValue(_read_.ARTPCON2))
        this.w_ORPZCONF = NVL(cp_ToDate(_read_.ARPZCONF),cp_NullValue(_read_.ARPZCONF))
        this.w_ORPZCON2 = NVL(cp_ToDate(_read_.ARPZCON2),cp_NullValue(_read_.ARPZCON2))
        this.w_ORCOCOL1 = NVL(cp_ToDate(_read_.ARCOCOL1),cp_NullValue(_read_.ARCOCOL1))
        this.w_ORCOCOL2 = NVL(cp_ToDate(_read_.ARCOCOL2),cp_NullValue(_read_.ARCOCOL2))
        this.w_ORNOMENC = NVL(cp_ToDate(_read_.ARNOMENC),cp_NullValue(_read_.ARNOMENC))
        this.w_ORUMSUPP = NVL(cp_ToDate(_read_.ARUMSUPP),cp_NullValue(_read_.ARUMSUPP))
        this.w_ORMOLSUP = NVL(cp_ToDate(_read_.ARMOLSUP),cp_NullValue(_read_.ARMOLSUP))
        this.w_ORSTASUP = NVL(cp_ToDate(_read_.ARSTASUP),cp_NullValue(_read_.ARSTASUP))
        this.w_ORDIMLUN = NVL(cp_ToDate(_read_.ARDIMLUN),cp_NullValue(_read_.ARDIMLUN))
        this.w_ORDIMLU2 = NVL(cp_ToDate(_read_.ARDIMLU2),cp_NullValue(_read_.ARDIMLU2))
        this.w_ORDIMLAR = NVL(cp_ToDate(_read_.ARDIMLAR),cp_NullValue(_read_.ARDIMLAR))
        this.w_ORDIMLA2 = NVL(cp_ToDate(_read_.ARDIMLA2),cp_NullValue(_read_.ARDIMLA2))
        this.w_ORDIMALT = NVL(cp_ToDate(_read_.ARDIMALT),cp_NullValue(_read_.ARDIMALT))
        this.w_ORDIMAL2 = NVL(cp_ToDate(_read_.ARDIMAL2),cp_NullValue(_read_.ARDIMAL2))
        this.w_ORUMDIME = NVL(cp_ToDate(_read_.ARUMDIME),cp_NullValue(_read_.ARUMDIME))
        this.w_ORUMDIM2 = NVL(cp_ToDate(_read_.ARUMDIM2),cp_NullValue(_read_.ARUMDIM2))
        this.w_ORDTINVA = NVL(cp_ToDate(_read_.ARDTINVA),cp_NullValue(_read_.ARDTINVA))
        this.w_ORDTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
        this.w_ORCODMAR = NVL(cp_ToDate(_read_.ARCODMAR),cp_NullValue(_read_.ARCODMAR))
        this.w_ORCODRIC = NVL(cp_ToDate(_read_.ARCODRIC),cp_NullValue(_read_.ARCODRIC))
        this.w_ORCODCLA = NVL(cp_ToDate(_read_.ARCODCLA),cp_NullValue(_read_.ARCODCLA))
        this.w_ORFLDISP = NVL(cp_ToDate(_read_.ARFLDISP),cp_NullValue(_read_.ARFLDISP))
        this.w_ORFLCONA = NVL(cp_ToDate(_read_.ARFLCONA),cp_NullValue(_read_.ARFLCONA))
        this.w_ORFLCON2 = NVL(cp_ToDate(_read_.ARFLCON2),cp_NullValue(_read_.ARFLCON2))
        this.w_ORTIPBAR = NVL(cp_ToDate(_read_.ARTIPBAR),cp_NullValue(_read_.ARTIPBAR))
        this.w_ORTIPGES = NVL(cp_ToDate(_read_.ARTIPGES),cp_NullValue(_read_.ARTIPGES))
        this.w_ORPROPRE = NVL(cp_ToDate(_read_.ARPROPRE),cp_NullValue(_read_.ARPROPRE))
        this.w_ORCAUZIO = NVL(cp_ToDate(_read_.ARCAUZIO),cp_NullValue(_read_.ARCAUZIO))
        this.w_ORIMBREN = NVL(cp_ToDate(_read_.ARIMBREN),cp_NullValue(_read_.ARIMBREN))
        this.w_ORIMBCAU = NVL(cp_ToDate(_read_.ARIMBCAU),cp_NullValue(_read_.ARIMBCAU))
        this.w_ORCAUZI2 = NVL(cp_ToDate(_read_.ARCAUZI2),cp_NullValue(_read_.ARCAUZI2))
        this.w_ORIMBRE2 = NVL(cp_ToDate(_read_.ARIMBRE2),cp_NullValue(_read_.ARIMBRE2))
        this.w_ORIMBCA2 = NVL(cp_ToDate(_read_.ARIMBCA2),cp_NullValue(_read_.ARIMBCA2))
        this.w_ORTIPRIF = NVL(cp_ToDate(_read_.ARTIPRIF),cp_NullValue(_read_.ARTIPRIF))
        this.w_ORCODVAR = NVL(cp_ToDate(_read_.ARCODVAR),cp_NullValue(_read_.ARCODVAR))
        this.w_ORFLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
        this.w_ORFLUBIC = NVL(cp_ToDate(_read_.ARFLUBIC),cp_NullValue(_read_.ARFLUBIC))
        this.w_ORCODCAA = NVL(cp_ToDate(_read_.ARCODCAA),cp_NullValue(_read_.ARCODCAA))
        this.w_ORFLESIS = NVL(cp_ToDate(_read_.ARFLESIS),cp_NullValue(_read_.ARFLESIS))
        this.w_ORFLRIVA = NVL(cp_ToDate(_read_.ARFLRIVA),cp_NullValue(_read_.ARFLRIVA))
        this.w_ORPROINT = NVL(cp_ToDate(_read_.ARPROINT),cp_NullValue(_read_.ARPROINT))
        this.w_ORPROEST = NVL(cp_ToDate(_read_.ARPROEST),cp_NullValue(_read_.ARPROEST))
        this.w_ORPROCLA = NVL(cp_ToDate(_read_.ARPROCLA),cp_NullValue(_read_.ARPROCLA))
        this.w_ORMAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
        this.w_ORCODPLA = NVL(cp_ToDate(_read_.ARCODPLA),cp_NullValue(_read_.ARCODPLA))
        this.w_ORCLAPRE = NVL(cp_ToDate(_read_.ARCLAPRE),cp_NullValue(_read_.ARCLAPRE))
        this.w_ORCLARIF = NVL(cp_ToDate(_read_.ARCLARIF),cp_NullValue(_read_.ARCLARIF))
        this.w_ORFLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
        this.w_ORFLGIS4 = NVL(cp_ToDate(_read_.ARFLGIS4),cp_NullValue(_read_.ARFLGIS4))
        this.w_ORPUBWEB = NVL(cp_ToDate(_read_.ARPUBWEB),cp_NullValue(_read_.ARPUBWEB))
        this.w_ORGESMAT = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
        this.w_ORCLAMAT = NVL(cp_ToDate(_read_.ARCLAMAT),cp_NullValue(_read_.ARCLAMAT))
        this.w_ORCENCOS = NVL(cp_ToDate(_read_.ARCENCOS),cp_NullValue(_read_.ARCENCOS))
        this.w_ORFLUMVA = NVL(cp_ToDate(_read_.ARFLUMVA),cp_NullValue(_read_.ARFLUMVA))
        this.w_ORPREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
        this.w_ORFLCESP = NVL(cp_ToDate(_read_.ARFLCESP),cp_NullValue(_read_.ARFLCESP))
        this.w_ORCATCES = NVL(cp_ToDate(_read_.ARCATCES),cp_NullValue(_read_.ARCATCES))
        this.w_ORDATINT = NVL(cp_ToDate(_read_.ARDATINT),cp_NullValue(_read_.ARDATINT))
        this.w_ORTIPOPE = NVL(cp_ToDate(_read_.ARTIPOPE),cp_NullValue(_read_.ARTIPOPE))
        this.w_ORCATOPE = NVL(cp_ToDate(_read_.ARCATOPE),cp_NullValue(_read_.ARCATOPE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              ARCODART = this.w_ARCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ORARTPOS = NVL(cp_ToDate(_read_.ARARTPOS),cp_NullValue(_read_.ARARTPOS))
        this.w_ORCATCON = NVL(cp_ToDate(_read_.ARCATCON),cp_NullValue(_read_.ARCATCON))
        this.w_ORCATOMO = NVL(cp_ToDate(_read_.ARCATOMO),cp_NullValue(_read_.ARCATOMO))
        this.w_ORCATSCM = NVL(cp_ToDate(_read_.ARCATSCM),cp_NullValue(_read_.ARCATSCM))
        this.w_ORCLALOT = NVL(cp_ToDate(_read_.ARCLALOT),cp_NullValue(_read_.ARCLALOT))
        this.w_ORCOCOL1 = NVL(cp_ToDate(_read_.ARCOCOL1),cp_NullValue(_read_.ARCOCOL1))
        this.w_ORCOCOL2 = NVL(cp_ToDate(_read_.ARCOCOL2),cp_NullValue(_read_.ARCOCOL2))
        this.w_ORCODCLA = NVL(cp_ToDate(_read_.ARCODCLA),cp_NullValue(_read_.ARCODCLA))
        this.w_ORCODDIS = NVL(cp_ToDate(_read_.ARCODDIS),cp_NullValue(_read_.ARCODDIS))
        this.w_ORCODFAM = NVL(cp_ToDate(_read_.ARCODFAM),cp_NullValue(_read_.ARCODFAM))
        this.w_ORCODGRU = NVL(cp_ToDate(_read_.ARCODGRU),cp_NullValue(_read_.ARCODGRU))
        this.w_ORCODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
        this.w_ORCODMAR = NVL(cp_ToDate(_read_.ARCODMAR),cp_NullValue(_read_.ARCODMAR))
        this.w_ORCODREP = NVL(cp_ToDate(_read_.ARCODREP),cp_NullValue(_read_.ARCODREP))
        this.w_ORCODRIC = NVL(cp_ToDate(_read_.ARCODRIC),cp_NullValue(_read_.ARCODRIC))
        this.w_ORCODSOT = NVL(cp_ToDate(_read_.ARCODSOT),cp_NullValue(_read_.ARCODSOT))
        this.w_ORDESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
        this.w_ORDESSUP = NVL(cp_ToDate(_read_.ARDESSUP),cp_NullValue(_read_.ARDESSUP))
        this.w_ORDESVO2 = NVL(cp_ToDate(_read_.ARDESVO2),cp_NullValue(_read_.ARDESVO2))
        this.w_ORDESVOL = NVL(cp_ToDate(_read_.ARDESVOL),cp_NullValue(_read_.ARDESVOL))
        this.w_ORDIMAL2 = NVL(cp_ToDate(_read_.ARDIMAL2),cp_NullValue(_read_.ARDIMAL2))
        this.w_ORDIMALT = NVL(cp_ToDate(_read_.ARDIMALT),cp_NullValue(_read_.ARDIMALT))
        this.w_ORDIMLA2 = NVL(cp_ToDate(_read_.ARDIMLA2),cp_NullValue(_read_.ARDIMLA2))
        this.w_ORDIMLAR = NVL(cp_ToDate(_read_.ARDIMLAR),cp_NullValue(_read_.ARDIMLAR))
        this.w_ORDIMLU2 = NVL(cp_ToDate(_read_.ARDIMLU2),cp_NullValue(_read_.ARDIMLU2))
        this.w_ORDIMLUN = NVL(cp_ToDate(_read_.ARDIMLUN),cp_NullValue(_read_.ARDIMLUN))
        this.w_ORDISLOT = NVL(cp_ToDate(_read_.ARDISLOT),cp_NullValue(_read_.ARDISLOT))
        this.w_ORDTINVA = NVL(cp_ToDate(_read_.ARDTINVA),cp_NullValue(_read_.ARDTINVA))
        this.w_ORDTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
        this.w_ORFLCOMP = NVL(cp_ToDate(_read_.ARFLCOMP),cp_NullValue(_read_.ARFLCOMP))
        this.w_ORFLCON2 = NVL(cp_ToDate(_read_.ARFLCON2),cp_NullValue(_read_.ARFLCON2))
        this.w_ORFLCONA = NVL(cp_ToDate(_read_.ARFLCONA),cp_NullValue(_read_.ARFLCONA))
        this.w_ORFLDISC = NVL(cp_ToDate(_read_.ARFLDISC),cp_NullValue(_read_.ARFLDISC))
        this.w_ORFLDISP = NVL(cp_ToDate(_read_.ARFLDISP),cp_NullValue(_read_.ARFLDISP))
        this.w_ORFLESAU = NVL(cp_ToDate(_read_.ARFLESAU),cp_NullValue(_read_.ARFLESAU))
        this.w_ORFLESUL = NVL(cp_ToDate(_read_.ARFLESUL),cp_NullValue(_read_.ARFLESUL))
        this.w_ORFLINVE = NVL(cp_ToDate(_read_.ARFLINVE),cp_NullValue(_read_.ARFLINVE))
        this.w_ORFLLMAG = NVL(cp_ToDate(_read_.ARFLLMAG),cp_NullValue(_read_.ARFLLMAG))
        this.w_ORFLPECO = NVL(cp_ToDate(_read_.ARFLPECO),cp_NullValue(_read_.ARFLPECO))
        this.w_ORFLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
        this.w_ORFLSTCO = NVL(cp_ToDate(_read_.ARFLSTCO),cp_NullValue(_read_.ARFLSTCO))
        this.w_ORFLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
        this.w_ORGRUMER = NVL(cp_ToDate(_read_.ARGRUMER),cp_NullValue(_read_.ARGRUMER))
        this.w_ORGRUPRO = NVL(cp_ToDate(_read_.ARGRUPRO),cp_NullValue(_read_.ARGRUPRO))
        this.w_ORMOLSUP = NVL(cp_ToDate(_read_.ARMOLSUP),cp_NullValue(_read_.ARMOLSUP))
        this.w_ORMOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
        this.w_ORNOMENC = NVL(cp_ToDate(_read_.ARNOMENC),cp_NullValue(_read_.ARNOMENC))
        this.w_OROPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
        this.w_ORPESLO2 = NVL(cp_ToDate(_read_.ARPESLO2),cp_NullValue(_read_.ARPESLO2))
        this.w_ORPESLOR = NVL(cp_ToDate(_read_.ARPESLOR),cp_NullValue(_read_.ARPESLOR))
        this.w_ORPESNE2 = NVL(cp_ToDate(_read_.ARPESNE2),cp_NullValue(_read_.ARPESNE2))
        this.w_ORPESNET = NVL(cp_ToDate(_read_.ARPESNET),cp_NullValue(_read_.ARPESNET))
        this.w_ORPROPRE = NVL(cp_ToDate(_read_.ARPROPRE),cp_NullValue(_read_.ARPROPRE))
        this.w_ORPZCON2 = NVL(cp_ToDate(_read_.ARPZCON2),cp_NullValue(_read_.ARPZCON2))
        this.w_ORPZCONF = NVL(cp_ToDate(_read_.ARPZCONF),cp_NullValue(_read_.ARPZCONF))
        this.w_ORSTACOD = NVL(cp_ToDate(_read_.ARSTACOD),cp_NullValue(_read_.ARSTACOD))
        this.w_ORSTASUP = NVL(cp_ToDate(_read_.ARSTASUP),cp_NullValue(_read_.ARSTASUP))
        this.w_ORTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
        this.w_ORTIPBAR = NVL(cp_ToDate(_read_.ARTIPBAR),cp_NullValue(_read_.ARTIPBAR))
        this.w_ORTIPCO1 = NVL(cp_ToDate(_read_.ARTIPCO1),cp_NullValue(_read_.ARTIPCO1))
        this.w_ORTIPCO2 = NVL(cp_ToDate(_read_.ARTIPCO2),cp_NullValue(_read_.ARTIPCO2))
        this.w_ORTIPGES = NVL(cp_ToDate(_read_.ARTIPGES),cp_NullValue(_read_.ARTIPGES))
        this.w_ORTIPMA1 = NVL(cp_ToDate(_read_.ARTIPMA1),cp_NullValue(_read_.ARTIPMA1))
        this.w_ORTIPMA2 = NVL(cp_ToDate(_read_.ARTIPMA2),cp_NullValue(_read_.ARTIPMA2))
        this.w_ORTIPPRE = NVL(cp_ToDate(_read_.ARTIPPRE),cp_NullValue(_read_.ARTIPPRE))
        this.w_ORTIPSER = NVL(cp_ToDate(_read_.ARTIPSER),cp_NullValue(_read_.ARTIPSER))
        this.w_ORTPCON2 = NVL(cp_ToDate(_read_.ARTPCON2),cp_NullValue(_read_.ARTPCON2))
        this.w_ORTPCONF = NVL(cp_ToDate(_read_.ARTPCONF),cp_NullValue(_read_.ARTPCONF))
        this.w_ORUMDIM2 = NVL(cp_ToDate(_read_.ARUMDIM2),cp_NullValue(_read_.ARUMDIM2))
        this.w_ORUMDIME = NVL(cp_ToDate(_read_.ARUMDIME),cp_NullValue(_read_.ARUMDIME))
        this.w_ORUMSUPP = NVL(cp_ToDate(_read_.ARUMSUPP),cp_NullValue(_read_.ARUMSUPP))
        this.w_ORUMVOL2 = NVL(cp_ToDate(_read_.ARUMVOL2),cp_NullValue(_read_.ARUMVOL2))
        this.w_ORUMVOLU = NVL(cp_ToDate(_read_.ARUMVOLU),cp_NullValue(_read_.ARUMVOLU))
        this.w_ORUNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
        this.w_ORUNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
        this.w_ORVOCCEN = NVL(cp_ToDate(_read_.ARVOCCEN),cp_NullValue(_read_.ARVOCCEN))
        this.w_ORVOCRIC = NVL(cp_ToDate(_read_.ARVOCRIC),cp_NullValue(_read_.ARVOCRIC))
        this.w_ORFLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
        this.w_ORCLAMAT = NVL(cp_ToDate(_read_.ARCLAMAT),cp_NullValue(_read_.ARCLAMAT))
        this.w_ORGESMAT = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
        this.w_ORDATINT = NVL(cp_ToDate(_read_.ARDATINT),cp_NullValue(_read_.ARDATINT))
        this.w_ORTIPOPE = NVL(cp_ToDate(_read_.ARTIPOPE),cp_NullValue(_read_.ARTIPOPE))
        this.w_ORCATOPE = NVL(cp_ToDate(_read_.ARCATOPE    ),cp_NullValue(_read_.ARCATOPE    ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_TOTART = 0
    this.w_ARCODART = IIF(EMPTY(this.w_ARCODART),this.w_ORCODART,this.w_ARCODART)
    this.w_ARDESART = IIF(EMPTY(this.w_ARDESART),this.w_ORDESART,this.w_ARDESART)
    this.w_ARDESSUP = IIF(EMPTY(this.w_ARDESSUP),this.w_ORDESSUP,this.w_ARDESSUP)
    if this.w_ORUNMIS1 <> this.w_ARUNMIS1
      * --- Consente Modifica 1 Unit� di misura solo se articolo non movimentato
      * --- Select from SALDIART
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COUNT(*) AS TOTART  from "+i_cTable+" SALDIART ";
            +" where SLCODICE="+cp_ToStrODBC(this.w_ARCODART)+"";
             ,"_Curs_SALDIART")
      else
        select COUNT(*) AS TOTART from (i_cTable);
         where SLCODICE=this.w_ARCODART;
          into cursor _Curs_SALDIART
      endif
      if used('_Curs_SALDIART')
        select _Curs_SALDIART
        locate for 1=1
        do while not(eof())
        this.w_TOTART = NVL(TOTART,0)
          select _Curs_SALDIART
          continue
        enddo
        use
      endif
    endif
    if this.w_TOTART > 0 
      this.w_oERRORLOG.AddMsgLog("Articolo: %1 Impossibile modificare la prima unit� di misura, l'articolo � presente nell'archivio saldi", ALLTRIM(this.w_ARCODART))     
      this.w_NOERR = .F.
      this.w_ARUNMIS1 = this.w_ORUNMIS1
    else
      this.w_ARUNMIS1 = IIF(EMPTY(this.w_ARUNMIS1),this.w_ORUNMIS1,this.w_ARUNMIS1)
    endif
    this.w_AROPERAT = IIF(EMPTY(this.w_AROPERAT),this.w_OROPERAT,this.w_AROPERAT)
    this.w_ARMOLTIP = IIF(EMPTY(this.w_ARMOLTIP),this.w_ORMOLTIP,this.w_ARMOLTIP)
    this.w_ARUNMIS2 = IIF(EMPTY(this.w_ARUNMIS2),this.w_ORUNMIS2,this.w_ARUNMIS2)
    this.w_ARGRUMER = IIF(EMPTY(this.w_ARGRUMER),this.w_ORGRUMER,this.w_ARGRUMER)
    this.w_ARCODFAM = IIF(EMPTY(this.w_ARCODFAM),this.w_ORCODFAM,this.w_ARCODFAM)
    this.w_ARGRUPRO = IIF(EMPTY(this.w_ARGRUPRO),this.w_ORGRUPRO,this.w_ARGRUPRO)
    this.w_ARCATSCM = IIF(EMPTY(this.w_ARCATSCM),this.w_ORCATSCM,this.w_ARCATSCM)
    this.w_ARTIPMA1 = IIF(EMPTY(this.w_ARTIPMA1),this.w_ORTIPMA1,this.w_ARTIPMA1)
    this.w_ARTIPMA2 = IIF(EMPTY(this.w_ARTIPMA2),this.w_ORTIPMA2,this.w_ARTIPMA2)
    this.w_ARCODIVA = IIF(EMPTY(this.w_ARCODIVA),this.w_ORCODIVA,this.w_ARCODIVA)
    this.w_ARFLINVE = IIF(EMPTY(this.w_ARFLINVE),this.w_ORFLINVE,this.w_ARFLINVE)
    this.w_ARFLESAU = IIF(EMPTY(this.w_ARFLESAU),this.w_ORFLESAU,this.w_ARFLESAU)
    this.w_ARCATOMO = IIF(EMPTY(this.w_ARCATOMO),this.w_ORCATOMO,this.w_ARCATOMO)
    this.w_ARCATCON = IIF(EMPTY(this.w_ARCATCON),this.w_ORCATCON,this.w_ARCATCON)
    this.w_ARTIPART = IIF(EMPTY(this.w_ARTIPART),this.w_ORTIPART,this.w_ARTIPART)
    this.w_ARFLSTCO = IIF(EMPTY(this.w_ARFLSTCO),this.w_ORFLSTCO,this.w_ARFLSTCO)
    this.w_ARPESNET = IIF(EMPTY(this.w_ARPESNET),this.w_ORPESNET,this.w_ARPESNET)
    this.w_ARPESNE2 = IIF(EMPTY(this.w_ARPESNE2),this.w_ORPESNE2,this.w_ARPESNE2)
    this.w_ARPESLOR = IIF(EMPTY(this.w_ARPESLOR),this.w_ORPESLOR,this.w_ARPESLOR)
    this.w_ARPESLO2 = IIF(EMPTY(this.w_ARPESLO2),this.w_ORPESLO2,this.w_ARPESLO2)
    this.w_ARDESVOL = IIF(EMPTY(this.w_ARDESVOL),this.w_ORDESVOL,this.w_ARDESVOL)
    this.w_ARDESVO2 = IIF(EMPTY(this.w_ARDESVO2),this.w_ORDESVO2,this.w_ARDESVO2)
    this.w_ARUMVOLU = IIF(EMPTY(this.w_ARUMVOLU),this.w_ORUMVOLU,this.w_ARUMVOLU)
    this.w_ARUMVOL2 = IIF(EMPTY(this.w_ARUMVOL2),this.w_ORUMVOL2,this.w_ARUMVOL2)
    this.w_ARTPCONF = IIF(EMPTY(this.w_ARTPCONF),this.w_ORTPCONF,this.w_ARTPCONF)
    this.w_ARTPCON2 = IIF(EMPTY(this.w_ARTPCON2),this.w_ORTPCON2,this.w_ARTPCON2)
    this.w_ARCOCOL1 = IIF(EMPTY(this.w_ARCOCOL1),this.w_ORCOCOL1,this.w_ARCOCOL1)
    this.w_ARCOCOL2 = IIF(EMPTY(this.w_ARCOCOL2),this.w_ORCOCOL2,this.w_ARCOCOL2)
    this.w_ARNOMENC = IIF(EMPTY(this.w_ARNOMENC),this.w_ORNOMENC,this.w_ARNOMENC)
    this.w_ARUMSUPP = IIF(EMPTY(this.w_ARUMSUPP),this.w_ORUMSUPP,this.w_ARUMSUPP)
    this.w_ARMOLSUP = IIF(EMPTY(this.w_ARMOLSUP),this.w_ORMOLSUP,this.w_ARMOLSUP)
    this.w_ARSTASUP = IIF(EMPTY(this.w_ARSTASUP),this.w_ORSTASUP,this.w_ARSTASUP)
    this.w_ARDIMLUN = IIF(EMPTY(this.w_ARDIMLUN),this.w_ORDIMLUN,this.w_ARDIMLUN)
    this.w_ARDIMLU2 = IIF(EMPTY(this.w_ARDIMLU2),this.w_ORDIMLU2,this.w_ARDIMLU2)
    this.w_ARDIMLAR = IIF(EMPTY(this.w_ARDIMLAR),this.w_ORDIMLAR,this.w_ARDIMLAR)
    this.w_ARDIMLA2 = IIF(EMPTY(this.w_ARDIMLA2),this.w_ORDIMLA2,this.w_ARDIMLA2)
    this.w_ARDIMALT = IIF(EMPTY(this.w_ARDIMALT),this.w_ORDIMALT,this.w_ARDIMALT)
    this.w_ARDIMAL2 = IIF(EMPTY(this.w_ARDIMAL2),this.w_ORDIMAL2,this.w_ARDIMAL2)
    this.w_ARUMDIME = IIF(EMPTY(this.w_ARUMDIME),this.w_ORUMDIME,this.w_ARUMDIME)
    this.w_ARUMDIM2 = IIF(EMPTY(this.w_ARUMDIM2),this.w_ORUMDIM2,this.w_ARUMDIM2)
    this.w_ARDTINVA = IIF(EMPTY(this.w_ARDTINVA),this.w_ORDTINVA,this.w_ARDTINVA)
    this.w_ARDTOBSO = IIF(EMPTY(this.w_ARDTOBSO),this.w_ORDTOBSO,this.w_ARDTOBSO)
    this.w_ARCODMAR = IIF(EMPTY(this.w_ARCODMAR),this.w_ORCODMAR,this.w_ARCODMAR)
    this.w_ARCODRIC = IIF(EMPTY(this.w_ARCODRIC),this.w_ORCODRIC,this.w_ARCODRIC)
    this.w_ARCODCLA = IIF(EMPTY(this.w_ARCODCLA),this.w_ORCODCLA,this.w_ARCODCLA)
    this.w_ARFLDISP = IIF(EMPTY(this.w_ARFLDISP),this.w_ORFLDISP,this.w_ARFLDISP)
    this.w_ARFLCONA = IIF(EMPTY(this.w_ARFLCONA),this.w_ORFLCONA,this.w_ARFLCONA)
    this.w_ARFLCON2 = IIF(EMPTY(this.w_ARFLCON2),this.w_ORFLCON2,this.w_ARFLCON2)
    this.w_ARTIPBAR = IIF(EMPTY(this.w_ARTIPBAR),this.w_ORTIPBAR,this.w_ARTIPBAR)
    this.w_ARVOCCEN = IIF(EMPTY(this.w_ARVOCCEN),this.w_ORVOCCEN,this.w_ARVOCCEN)
    this.w_ARTIPGES = IIF(EMPTY(this.w_ARTIPGES),this.w_ORTIPGES,this.w_ARTIPGES)
    this.w_ARPROPRE = IIF(EMPTY(this.w_ARPROPRE),this.w_ORPROPRE,this.w_ARPROPRE)
    this.w_ARCODDIS = IIF(EMPTY(this.w_ARCODDIS),this.w_ORCODDIS,this.w_ARCODDIS)
    this.w_ARFLCOMP = IIF(EMPTY(this.w_ARFLCOMP),this.w_ORFLCOMP,this.w_ARFLCOMP)
    this.w_ARCAUZIO = IIF(EMPTY(this.w_ARCAUZIO),this.w_ORCAUZIO,this.w_ARCAUZIO)
    this.w_ARIMBREN = IIF(EMPTY(this.w_ARIMBREN),this.w_ORIMBREN,this.w_ARIMBREN)
    this.w_ARIMBCAU = IIF(EMPTY(this.w_ARIMBCAU),this.w_ORIMBCAU,this.w_ARIMBCAU)
    this.w_ARCAUZI2 = IIF(EMPTY(this.w_ARCAUZI2),this.w_ORCAUZI2,this.w_ARCAUZI2)
    this.w_ARIMBRE2 = IIF(EMPTY(this.w_ARIMBRE2),this.w_ORIMBRE2,this.w_ARIMBRE2)
    this.w_ARIMBCA2 = IIF(EMPTY(this.w_ARIMBCA2),this.w_ORIMBCA2,this.w_ARIMBCA2)
    this.w_ARTIPRIF = IIF(EMPTY(this.w_ARTIPRIF),this.w_ORTIPRIF,this.w_ARTIPRIF)
    this.w_ARCODVAR = IIF(EMPTY(this.w_ARCODVAR),this.w_ORCODVAR,this.w_ARCODVAR)
    this.w_ARFLLOTT = IIF(EMPTY(this.w_ARFLLOTT),this.w_ORFLLOTT,this.w_ARFLLOTT)
    this.w_ARFLUBIC = IIF(EMPTY(this.w_ARFLUBIC),this.w_ORFLUBIC,this.w_ARFLUBIC)
    this.w_ARCODPRO = IIF(EMPTY(this.w_ARCODPRO),this.w_ORCODPRO,this.w_ARCODPRO)
    this.w_ARCODFOR = IIF(EMPTY(this.w_ARCODFOR),this.w_ORCODFOR,this.w_ARCODFOR)
    this.w_ARCODCAA = IIF(EMPTY(this.w_ARCODCAA),this.w_ORCODCAA,this.w_ARCODCAA)
    this.w_ARFLESIS = IIF(EMPTY(this.w_ARFLESIS),this.w_ORFLESIS,this.w_ARFLESIS)
    this.w_ARFLRIVA = IIF(EMPTY(this.w_ARFLRIVA),this.w_ORFLRIVA,this.w_ARFLRIVA)
    this.w_ARPROINT = IIF(EMPTY(this.w_ARPROINT),this.w_ORPROINT,this.w_ARPROINT)
    this.w_ARPROEST = IIF(EMPTY(this.w_ARPROEST),this.w_ORPROEST,this.w_ARPROEST)
    this.w_ARPROCLA = IIF(EMPTY(this.w_ARPROCLA),this.w_ORPROCLA,this.w_ARPROCLA)
    this.w_ARMAGPRE = IIF(EMPTY(this.w_ARMAGPRE),this.w_ORMAGPRE,this.w_ARMAGPRE)
    this.w_ARCODPLA = IIF(EMPTY(this.w_ARCODPLA),this.w_ORCODPLA,this.w_ARCODPLA)
    this.w_ARCLAPRE = IIF(EMPTY(this.w_ARCLAPRE),this.w_ORCLAPRE,this.w_ARCLAPRE)
    this.w_ARCLARIF = IIF(EMPTY(this.w_ARCLARIF),this.w_ORCLARIF,this.w_ARCLARIF)
    this.w_ARFLCOMM = IIF(EMPTY(this.w_ARFLCOMM),this.w_ORFLCOMM,this.w_ARFLCOMM)
    this.w_ARFLGIS4 = IIF(EMPTY(this.w_ARFLGIS4),this.w_ORFLGIS4,this.w_ARFLGIS4)
    this.w_ARPUBWEB = IIF(EMPTY(this.w_ARPUBWEB),this.w_ORPUBWEB,this.w_ARPUBWEB)
    this.w_ARGESMAT = IIF(EMPTY(this.w_ARGESMAT),this.w_ORGESMAT,this.w_ARGESMAT)
    this.w_ARCLAMAT = IIF(EMPTY(this.w_ARCLAMAT),this.w_ORCLAMAT,this.w_ARCLAMAT)
    this.w_ARCENCOS = IIF(EMPTY(this.w_ARCENCOS),this.w_ORCENCOS,this.w_ARCENCOS)
    this.w_ARFLUMVA = IIF(EMPTY(this.w_ARFLUMVA),this.w_ORFLUMVA,this.w_ARFLUMVA)
    this.w_ARPREZUM = IIF(EMPTY(this.w_ARPREZUM),this.w_ORPREZUM,this.w_ARPREZUM)
    this.w_ARFLCESP = IIF(EMPTY(this.w_ARFLCESP),this.w_ORFLCESP,this.w_ARFLCESP)
    this.w_ARCATCES = IIF(EMPTY(this.w_ARCATCES),this.w_ORCATCES,this.w_ARCATCES)
    this.w_ARDATINT = IIF(EMPTY(this.w_ARDATINT),this.w_ORDATINT,this.w_ARDATINT)
    this.w_ARTIPOPE = IIF(EMPTY(this.w_ARTIPOPE),this.w_ORTIPOPE,this.w_ARTIPOPE)
    this.w_ARCATOPE = IIF(EMPTY(this.w_ARCATOPE),this.w_ORCATOPE,this.w_ARCATOPE)
    this.w_ARARTPOS = IIF(EMPTY(this.w_ARARTPOS),this.w_ORARTPOS,this.w_ARARTPOS)
    this.w_ARCLALOT = IIF(EMPTY(this.w_ARCLALOT),this.w_ORCLALOT,this.w_ARCLALOT)
    this.w_ARCODGRU = IIF(EMPTY(this.w_ARCODGRU),this.w_ORCODGRU,this.w_ARCODGRU)
    this.w_ARCODREP = IIF(EMPTY(this.w_ARCODREP),this.w_ORCODREP,this.w_ARCODREP)
    this.w_ARCODSOT = IIF(EMPTY(this.w_ARCODSOT),this.w_ORCODSOT,this.w_ARCODSOT)
    this.w_ARDISLOT = IIF(EMPTY(this.w_ARDISLOT),this.w_ORDISLOT,this.w_ARDISLOT)
    this.w_ARFLDISC = IIF(EMPTY(this.w_ARFLDISC),this.w_ORFLDISC,this.w_ARFLDISC)
    this.w_ARFLESUL = IIF(EMPTY(this.w_ARFLESUL),this.w_ORFLESUL,this.w_ARFLESUL)
    this.w_ARFLLMAG = IIF(EMPTY(this.w_ARFLLMAG),this.w_ORFLLMAG,this.w_ARFLLMAG)
    this.w_ARFLPECO = IIF(EMPTY(this.w_ARFLPECO),this.w_ORFLPECO,this.w_ARFLPECO)
    this.w_ARFLSERG = IIF(EMPTY(this.w_ARFLSERG),this.w_ORFLSERG,this.w_ARFLSERG)
    this.w_ARFLUSEP = IIF(EMPTY(this.w_ARFLUSEP),this.w_ORFLUSEP,this.w_ARFLUSEP)
    this.w_ARPZCON2 = IIF(EMPTY(this.w_ARPZCON2),this.w_ORPZCON2,this.w_ARPZCON2)
    this.w_ARPZCONF = IIF(EMPTY(this.w_ARPZCONF),this.w_ORPZCONF,this.w_ARPZCONF)
    this.w_ARSTACOD = IIF(EMPTY(this.w_ARSTACOD),this.w_ORSTACOD,this.w_ARSTACOD)
    this.w_ARTIPCO1 = IIF(EMPTY(this.w_ARTIPCO1),this.w_ORTIPCO1,this.w_ARTIPCO1)
    this.w_ARTIPCO2 = IIF(EMPTY(this.w_ARTIPCO2),this.w_ORTIPCO2,this.w_ARTIPCO2)
    this.w_ARTIPPRE = IIF(EMPTY(this.w_ARTIPPRE),this.w_ORTIPPRE,this.w_ARTIPPRE)
    this.w_ARTIPSER = IIF(EMPTY(this.w_ARTIPSER),this.w_ORTIPSER,this.w_ARTIPSER)
    this.w_ARVOCRIC = IIF(EMPTY(this.w_ARVOCRIC),this.w_ORVOCRIC,this.w_ARVOCRIC)
    this.w_ARDATINT = IIF(EMPTY(this.w_ARDATINT),this.w_ORDATINT,this.w_ARDATINT)
    if g_APPLICATION="ad hoc ENTERPRISE"
      * --- Write into ART_ICOL
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ARCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_ARCATCON),'ART_ICOL','ARCATCON');
        +",ARCATOMO ="+cp_NullLink(cp_ToStrODBC(this.w_ARCATOMO),'ART_ICOL','ARCATOMO');
        +",ARCATSCM ="+cp_NullLink(cp_ToStrODBC(this.w_ARCATSCM),'ART_ICOL','ARCATSCM');
        +",ARCOCOL1 ="+cp_NullLink(cp_ToStrODBC(this.w_ARCOCOL1),'ART_ICOL','ARCOCOL1');
        +",ARCOCOL2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARCOCOL2),'ART_ICOL','ARCOCOL2');
        +",ARCODCLA ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODCLA),'ART_ICOL','ARCODCLA');
        +",ARCODFAM ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODFAM),'ART_ICOL','ARCODFAM');
        +",ARCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODIVA),'ART_ICOL','ARCODIVA');
        +",ARCODMAR ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODMAR),'ART_ICOL','ARCODMAR');
        +",ARCODRIC ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODRIC),'ART_ICOL','ARCODRIC');
        +",ARDESART ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'ART_ICOL','ARDESART');
        +",ARDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'ART_ICOL','ARDESSUP');
        +",ARDESVO2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESVO2),'ART_ICOL','ARDESVO2');
        +",ARDESVOL ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESVOL),'ART_ICOL','ARDESVOL');
        +",ARDIMAL2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARDIMAL2),'ART_ICOL','ARDIMAL2');
        +",ARDIMALT ="+cp_NullLink(cp_ToStrODBC(this.w_ARDIMALT),'ART_ICOL','ARDIMALT');
        +",ARDIMLA2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLA2),'ART_ICOL','ARDIMLA2');
        +",ARDIMLAR ="+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLAR),'ART_ICOL','ARDIMLAR');
        +",ARDIMLU2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLU2),'ART_ICOL','ARDIMLU2');
        +",ARDIMLUN ="+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLUN),'ART_ICOL','ARDIMLUN');
        +",ARDTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_ARDTINVA),'ART_ICOL','ARDTINVA');
        +",ARDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_ARDTOBSO),'ART_ICOL','ARDTOBSO');
        +",ARFLCON2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLCON2),'ART_ICOL','ARFLCON2');
        +",ARFLCONA ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLCONA),'ART_ICOL','ARFLCONA');
        +",ARFLDISP ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLDISP),'ART_ICOL','ARFLDISP');
        +",ARFLESAU ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLESAU),'ART_ICOL','ARFLESAU');
        +",ARFLINVE ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLINVE),'ART_ICOL','ARFLINVE');
        +",ARFLSTCO ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLSTCO),'ART_ICOL','ARFLSTCO');
        +",ARGRUMER ="+cp_NullLink(cp_ToStrODBC(this.w_ARGRUMER),'ART_ICOL','ARGRUMER');
        +",ARGRUPRO ="+cp_NullLink(cp_ToStrODBC(this.w_ARGRUPRO),'ART_ICOL','ARGRUPRO');
        +",ARMOLSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARMOLSUP),'ART_ICOL','ARMOLSUP');
        +",ARMOLTIP ="+cp_NullLink(cp_ToStrODBC(this.w_ARMOLTIP),'ART_ICOL','ARMOLTIP');
        +",ARNOMENC ="+cp_NullLink(cp_ToStrODBC(this.w_ARNOMENC),'ART_ICOL','ARNOMENC');
        +",AROPERAT ="+cp_NullLink(cp_ToStrODBC(this.w_AROPERAT),'ART_ICOL','AROPERAT');
        +",ARPESLO2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARPESLO2),'ART_ICOL','ARPESLO2');
        +",ARPESLOR ="+cp_NullLink(cp_ToStrODBC(this.w_ARPESLOR),'ART_ICOL','ARPESLOR');
        +",ARPESNE2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARPESNE2),'ART_ICOL','ARPESNE2');
        +",ARPESNET ="+cp_NullLink(cp_ToStrODBC(this.w_ARPESNET),'ART_ICOL','ARPESNET');
        +",ARPROPRE ="+cp_NullLink(cp_ToStrODBC(this.w_ARPROPRE),'ART_ICOL','ARPROPRE');
        +",ARPZCON2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARPZCON2),'ART_ICOL','ARPZCON2');
        +",ARPZCONF ="+cp_NullLink(cp_ToStrODBC(this.w_ARPZCONF),'ART_ICOL','ARPZCONF');
        +",ARSTASUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARSTASUP),'ART_ICOL','ARSTASUP');
        +",ARTIPART ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPART),'ART_ICOL','ARTIPART');
        +",ARTIPBAR ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPBAR),'ART_ICOL','ARTIPBAR');
        +",ARTIPGES ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPGES),'ART_ICOL','ARTIPGES');
        +",ARTIPMA1 ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPMA1),'ART_ICOL','ARTIPMA1');
        +",ARTIPMA2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPMA2),'ART_ICOL','ARTIPMA2');
        +",ARTPCON2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARTPCON2),'ART_ICOL','ARTPCON2');
        +",ARTPCONF ="+cp_NullLink(cp_ToStrODBC(this.w_ARTPCONF),'ART_ICOL','ARTPCONF');
        +",ARUMDIM2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARUMDIM2),'ART_ICOL','ARUMDIM2');
        +",ARUMDIME ="+cp_NullLink(cp_ToStrODBC(this.w_ARUMDIME),'ART_ICOL','ARUMDIME');
        +",ARUMSUPP ="+cp_NullLink(cp_ToStrODBC(this.w_ARUMSUPP),'ART_ICOL','ARUMSUPP');
        +",ARUMVOL2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARUMVOL2),'ART_ICOL','ARUMVOL2');
        +",ARUMVOLU ="+cp_NullLink(cp_ToStrODBC(this.w_ARUMVOLU),'ART_ICOL','ARUMVOLU');
        +",ARUNMIS2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARUNMIS2),'ART_ICOL','ARUNMIS2');
        +",ARCAUZIO ="+cp_NullLink(cp_ToStrODBC(this.w_ARCAUZIO),'ART_ICOL','ARCAUZIO');
        +",ARIMBREN ="+cp_NullLink(cp_ToStrODBC(this.w_ARIMBREN),'ART_ICOL','ARIMBREN');
        +",ARIMBCAU ="+cp_NullLink(cp_ToStrODBC(this.w_ARIMBCAU),'ART_ICOL','ARIMBCAU');
        +",ARCAUZI2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARCAUZI2),'ART_ICOL','ARCAUZI2');
        +",ARIMBRE2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARIMBRE2),'ART_ICOL','ARIMBRE2');
        +",ARIMBCA2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARIMBCA2),'ART_ICOL','ARIMBCA2');
        +",ARTIPRIF ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPRIF),'ART_ICOL','ARTIPRIF');
        +",ARFLLOTT ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLLOTT),'ART_ICOL','ARFLLOTT');
        +",ARFLUBIC ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLUBIC),'ART_ICOL','ARFLUBIC');
        +",ARCODCAA ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODCAA),'ART_ICOL','ARCODCAA');
        +",ARFLESIS ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLESIS),'ART_ICOL','ARFLESIS');
        +",ARFLRIVA ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLRIVA),'ART_ICOL','ARFLRIVA');
        +",ARPROINT ="+cp_NullLink(cp_ToStrODBC(this.w_ARPROINT),'ART_ICOL','ARPROINT');
        +",ARPROEST ="+cp_NullLink(cp_ToStrODBC(this.w_ARPROEST),'ART_ICOL','ARPROEST');
        +",ARPROCLA ="+cp_NullLink(cp_ToStrODBC(this.w_ARPROCLA),'ART_ICOL','ARPROCLA');
        +",ARCLAPRE ="+cp_NullLink(cp_ToStrODBC(this.w_ARCLAPRE),'ART_ICOL','ARCLAPRE');
        +",ARCLARIF ="+cp_NullLink(cp_ToStrODBC(this.w_ARCLARIF),'ART_ICOL','ARCLARIF');
        +",ARFLGIS4 ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLGIS4),'ART_ICOL','ARFLGIS4');
        +",ARPUBWEB ="+cp_NullLink(cp_ToStrODBC(this.w_ARPUBWEB),'ART_ICOL','ARPUBWEB');
        +",ARGESMAT ="+cp_NullLink(cp_ToStrODBC(this.w_ARGESMAT),'ART_ICOL','ARGESMAT');
        +",ARCENCOS ="+cp_NullLink(cp_ToStrODBC(this.w_ARCENCOS),'ART_ICOL','ARCENCOS');
        +",ARFLUMVA ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLUMVA),'ART_ICOL','ARFLUMVA');
        +",ARPREZUM ="+cp_NullLink(cp_ToStrODBC(this.w_ARPREZUM),'ART_ICOL','ARPREZUM');
        +",ARFLCESP ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLCESP),'ART_ICOL','ARFLCESP');
        +",ARCATCES ="+cp_NullLink(cp_ToStrODBC(this.w_ARCATCES),'ART_ICOL','ARCATCES');
        +",ARDATINT ="+cp_NullLink(cp_ToStrODBC(this.w_ARDATINT),'ART_ICOL','ARDATINT');
        +",ARTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPOPE),'ART_ICOL','ARTIPOPE');
        +",ARCATOPE ="+cp_NullLink(cp_ToStrODBC(this.w_ARCATOPE),'ART_ICOL','ARCATOPE');
            +i_ccchkf ;
        +" where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
               )
      else
        update (i_cTable) set;
            ARCATCON = this.w_ARCATCON;
            ,ARCATOMO = this.w_ARCATOMO;
            ,ARCATSCM = this.w_ARCATSCM;
            ,ARCOCOL1 = this.w_ARCOCOL1;
            ,ARCOCOL2 = this.w_ARCOCOL2;
            ,ARCODCLA = this.w_ARCODCLA;
            ,ARCODFAM = this.w_ARCODFAM;
            ,ARCODIVA = this.w_ARCODIVA;
            ,ARCODMAR = this.w_ARCODMAR;
            ,ARCODRIC = this.w_ARCODRIC;
            ,ARDESART = this.w_ARDESART;
            ,ARDESSUP = this.w_ARDESSUP;
            ,ARDESVO2 = this.w_ARDESVO2;
            ,ARDESVOL = this.w_ARDESVOL;
            ,ARDIMAL2 = this.w_ARDIMAL2;
            ,ARDIMALT = this.w_ARDIMALT;
            ,ARDIMLA2 = this.w_ARDIMLA2;
            ,ARDIMLAR = this.w_ARDIMLAR;
            ,ARDIMLU2 = this.w_ARDIMLU2;
            ,ARDIMLUN = this.w_ARDIMLUN;
            ,ARDTINVA = this.w_ARDTINVA;
            ,ARDTOBSO = this.w_ARDTOBSO;
            ,ARFLCON2 = this.w_ARFLCON2;
            ,ARFLCONA = this.w_ARFLCONA;
            ,ARFLDISP = this.w_ARFLDISP;
            ,ARFLESAU = this.w_ARFLESAU;
            ,ARFLINVE = this.w_ARFLINVE;
            ,ARFLSTCO = this.w_ARFLSTCO;
            ,ARGRUMER = this.w_ARGRUMER;
            ,ARGRUPRO = this.w_ARGRUPRO;
            ,ARMOLSUP = this.w_ARMOLSUP;
            ,ARMOLTIP = this.w_ARMOLTIP;
            ,ARNOMENC = this.w_ARNOMENC;
            ,AROPERAT = this.w_AROPERAT;
            ,ARPESLO2 = this.w_ARPESLO2;
            ,ARPESLOR = this.w_ARPESLOR;
            ,ARPESNE2 = this.w_ARPESNE2;
            ,ARPESNET = this.w_ARPESNET;
            ,ARPROPRE = this.w_ARPROPRE;
            ,ARPZCON2 = this.w_ARPZCON2;
            ,ARPZCONF = this.w_ARPZCONF;
            ,ARSTASUP = this.w_ARSTASUP;
            ,ARTIPART = this.w_ARTIPART;
            ,ARTIPBAR = this.w_ARTIPBAR;
            ,ARTIPGES = this.w_ARTIPGES;
            ,ARTIPMA1 = this.w_ARTIPMA1;
            ,ARTIPMA2 = this.w_ARTIPMA2;
            ,ARTPCON2 = this.w_ARTPCON2;
            ,ARTPCONF = this.w_ARTPCONF;
            ,ARUMDIM2 = this.w_ARUMDIM2;
            ,ARUMDIME = this.w_ARUMDIME;
            ,ARUMSUPP = this.w_ARUMSUPP;
            ,ARUMVOL2 = this.w_ARUMVOL2;
            ,ARUMVOLU = this.w_ARUMVOLU;
            ,ARUNMIS2 = this.w_ARUNMIS2;
            ,ARCAUZIO = this.w_ARCAUZIO;
            ,ARIMBREN = this.w_ARIMBREN;
            ,ARIMBCAU = this.w_ARIMBCAU;
            ,ARCAUZI2 = this.w_ARCAUZI2;
            ,ARIMBRE2 = this.w_ARIMBRE2;
            ,ARIMBCA2 = this.w_ARIMBCA2;
            ,ARTIPRIF = this.w_ARTIPRIF;
            ,ARFLLOTT = this.w_ARFLLOTT;
            ,ARFLUBIC = this.w_ARFLUBIC;
            ,ARCODCAA = this.w_ARCODCAA;
            ,ARFLESIS = this.w_ARFLESIS;
            ,ARFLRIVA = this.w_ARFLRIVA;
            ,ARPROINT = this.w_ARPROINT;
            ,ARPROEST = this.w_ARPROEST;
            ,ARPROCLA = this.w_ARPROCLA;
            ,ARCLAPRE = this.w_ARCLAPRE;
            ,ARCLARIF = this.w_ARCLARIF;
            ,ARFLGIS4 = this.w_ARFLGIS4;
            ,ARPUBWEB = this.w_ARPUBWEB;
            ,ARGESMAT = this.w_ARGESMAT;
            ,ARCENCOS = this.w_ARCENCOS;
            ,ARFLUMVA = this.w_ARFLUMVA;
            ,ARPREZUM = this.w_ARPREZUM;
            ,ARFLCESP = this.w_ARFLCESP;
            ,ARCATCES = this.w_ARCATCES;
            ,ARDATINT = this.w_ARDATINT;
            ,ARTIPOPE = this.w_ARTIPOPE;
            ,ARCATOPE = this.w_ARCATOPE;
            &i_ccchkf. ;
         where;
            ARCODART = this.w_ARCODART;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Fallita la scrittura di un articolo!'
        return
      endif
    else
      * --- Write into ART_ICOL
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ARARTPOS ="+cp_NullLink(cp_ToStrODBC(this.w_ARARTPOS),'ART_ICOL','ARARTPOS');
        +",ARCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_ARCATCON),'ART_ICOL','ARCATCON');
        +",ARCATOMO ="+cp_NullLink(cp_ToStrODBC(this.w_ARCATOMO),'ART_ICOL','ARCATOMO');
        +",ARCATSCM ="+cp_NullLink(cp_ToStrODBC(this.w_ARCATSCM),'ART_ICOL','ARCATSCM');
        +",ARCLALOT ="+cp_NullLink(cp_ToStrODBC(this.w_ARCLALOT),'ART_ICOL','ARCLALOT');
        +",ARCOCOL1 ="+cp_NullLink(cp_ToStrODBC(this.w_ARCOCOL1),'ART_ICOL','ARCOCOL1');
        +",ARCOCOL2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARCOCOL2),'ART_ICOL','ARCOCOL2');
        +",ARCODCLA ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODCLA),'ART_ICOL','ARCODCLA');
        +",ARCODDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODDIS),'ART_ICOL','ARCODDIS');
        +",ARCODFAM ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODFAM),'ART_ICOL','ARCODFAM');
        +",ARCODGRU ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODGRU),'ART_ICOL','ARCODGRU');
        +",ARCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODIVA),'ART_ICOL','ARCODIVA');
        +",ARCODMAR ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODMAR),'ART_ICOL','ARCODMAR');
        +",ARCODREP ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODREP),'ART_ICOL','ARCODREP');
        +",ARCODRIC ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODRIC),'ART_ICOL','ARCODRIC');
        +",ARCODSOT ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODSOT),'ART_ICOL','ARCODSOT');
        +",ARDESART ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'ART_ICOL','ARDESART');
        +",ARDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'ART_ICOL','ARDESSUP');
        +",ARDESVO2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESVO2),'ART_ICOL','ARDESVO2');
        +",ARDESVOL ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESVOL),'ART_ICOL','ARDESVOL');
        +",ARDIMAL2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARDIMAL2),'ART_ICOL','ARDIMAL2');
        +",ARDIMALT ="+cp_NullLink(cp_ToStrODBC(this.w_ARDIMALT),'ART_ICOL','ARDIMALT');
        +",ARDIMLA2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLA2),'ART_ICOL','ARDIMLA2');
        +",ARDIMLAR ="+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLAR),'ART_ICOL','ARDIMLAR');
        +",ARDIMLU2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLU2),'ART_ICOL','ARDIMLU2');
        +",ARDIMLUN ="+cp_NullLink(cp_ToStrODBC(this.w_ARDIMLUN),'ART_ICOL','ARDIMLUN');
        +",ARDISLOT ="+cp_NullLink(cp_ToStrODBC(this.w_ARDISLOT),'ART_ICOL','ARDISLOT');
        +",ARDTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_ARDTINVA),'ART_ICOL','ARDTINVA');
        +",ARDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_ARDTOBSO),'ART_ICOL','ARDTOBSO');
        +",ARFLCOMP ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLCOMP),'ART_ICOL','ARFLCOMP');
        +",ARFLCON2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLCON2),'ART_ICOL','ARFLCON2');
        +",ARFLCONA ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLCONA),'ART_ICOL','ARFLCONA');
        +",ARFLDISC ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLDISC),'ART_ICOL','ARFLDISC');
        +",ARFLDISP ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLDISP),'ART_ICOL','ARFLDISP');
        +",ARFLESAU ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLESAU),'ART_ICOL','ARFLESAU');
        +",ARFLESUL ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLESUL),'ART_ICOL','ARFLESUL');
        +",ARFLINVE ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLINVE),'ART_ICOL','ARFLINVE');
        +",ARFLLMAG ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLLMAG),'ART_ICOL','ARFLLMAG');
        +",ARFLPECO ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLPECO),'ART_ICOL','ARFLPECO');
        +",ARFLSERG ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLSERG),'ART_ICOL','ARFLSERG');
        +",ARFLSTCO ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLSTCO),'ART_ICOL','ARFLSTCO');
        +",ARFLUSEP ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLUSEP),'ART_ICOL','ARFLUSEP');
        +",ARGRUMER ="+cp_NullLink(cp_ToStrODBC(this.w_ARGRUMER),'ART_ICOL','ARGRUMER');
        +",ARGRUPRO ="+cp_NullLink(cp_ToStrODBC(this.w_ARGRUPRO),'ART_ICOL','ARGRUPRO');
        +",ARMOLSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARMOLSUP),'ART_ICOL','ARMOLSUP');
        +",ARMOLTIP ="+cp_NullLink(cp_ToStrODBC(this.w_ARMOLTIP),'ART_ICOL','ARMOLTIP');
        +",ARNOMENC ="+cp_NullLink(cp_ToStrODBC(this.w_ARNOMENC),'ART_ICOL','ARNOMENC');
        +",AROPERAT ="+cp_NullLink(cp_ToStrODBC(this.w_AROPERAT),'ART_ICOL','AROPERAT');
        +",ARPESLO2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARPESLO2),'ART_ICOL','ARPESLO2');
        +",ARPESLOR ="+cp_NullLink(cp_ToStrODBC(this.w_ARPESLOR),'ART_ICOL','ARPESLOR');
        +",ARPESNE2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARPESNE2),'ART_ICOL','ARPESNE2');
        +",ARPESNET ="+cp_NullLink(cp_ToStrODBC(this.w_ARPESNET),'ART_ICOL','ARPESNET');
        +",ARPROPRE ="+cp_NullLink(cp_ToStrODBC(this.w_ARPROPRE),'ART_ICOL','ARPROPRE');
        +",ARPZCON2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARPZCON2),'ART_ICOL','ARPZCON2');
        +",ARPZCONF ="+cp_NullLink(cp_ToStrODBC(this.w_ARPZCONF),'ART_ICOL','ARPZCONF');
        +",ARSTACOD ="+cp_NullLink(cp_ToStrODBC(this.w_ARSTACOD),'ART_ICOL','ARSTACOD');
        +",ARSTASUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARSTASUP),'ART_ICOL','ARSTASUP');
        +",ARTIPART ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPART),'ART_ICOL','ARTIPART');
        +",ARTIPBAR ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPBAR),'ART_ICOL','ARTIPBAR');
        +",ARTIPCO1 ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPCO1),'ART_ICOL','ARTIPCO1');
        +",ARTIPCO2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPCO2),'ART_ICOL','ARTIPCO2');
        +",ARTIPGES ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPGES),'ART_ICOL','ARTIPGES');
        +",ARTIPMA1 ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPMA1),'ART_ICOL','ARTIPMA1');
        +",ARTIPMA2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPMA2),'ART_ICOL','ARTIPMA2');
        +",ARTIPPRE ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPPRE),'ART_ICOL','ARTIPPRE');
        +",ARTIPSER ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPSER),'ART_ICOL','ARTIPSER');
        +",ARTPCON2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARTPCON2),'ART_ICOL','ARTPCON2');
        +",ARTPCONF ="+cp_NullLink(cp_ToStrODBC(this.w_ARTPCONF),'ART_ICOL','ARTPCONF');
        +",ARUMDIM2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARUMDIM2),'ART_ICOL','ARUMDIM2');
        +",ARUMDIME ="+cp_NullLink(cp_ToStrODBC(this.w_ARUMDIME),'ART_ICOL','ARUMDIME');
        +",ARUMSUPP ="+cp_NullLink(cp_ToStrODBC(this.w_ARUMSUPP),'ART_ICOL','ARUMSUPP');
        +",ARUMVOL2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARUMVOL2),'ART_ICOL','ARUMVOL2');
        +",ARUMVOLU ="+cp_NullLink(cp_ToStrODBC(this.w_ARUMVOLU),'ART_ICOL','ARUMVOLU');
        +",ARUNMIS1 ="+cp_NullLink(cp_ToStrODBC(this.w_ARUNMIS1),'ART_ICOL','ARUNMIS1');
        +",ARUNMIS2 ="+cp_NullLink(cp_ToStrODBC(this.w_ARUNMIS2),'ART_ICOL','ARUNMIS2');
        +",ARVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_ARVOCCEN),'ART_ICOL','ARVOCCEN');
        +",ARVOCRIC ="+cp_NullLink(cp_ToStrODBC(this.w_ARVOCRIC),'ART_ICOL','ARVOCRIC');
        +",ARCLAMAT ="+cp_NullLink(cp_ToStrODBC(this.w_ARCLAMAT),'ART_ICOL','ARCLAMAT');
        +",ARGESMAT ="+cp_NullLink(cp_ToStrODBC(this.w_ARGESMAT),'ART_ICOL','ARGESMAT');
        +",ARFLLOTT ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLLOTT),'ART_ICOL','ARFLLOTT');
        +",ARDATINT ="+cp_NullLink(cp_ToStrODBC(this.w_ARDATINT),'ART_ICOL','ARDATINT');
        +",ARTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPOPE),'ART_ICOL','ARTIPOPE');
        +",ARCATOPE ="+cp_NullLink(cp_ToStrODBC(this.w_ARCATOPE),'ART_ICOL','ARCATOPE');
            +i_ccchkf ;
        +" where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
               )
      else
        update (i_cTable) set;
            ARARTPOS = this.w_ARARTPOS;
            ,ARCATCON = this.w_ARCATCON;
            ,ARCATOMO = this.w_ARCATOMO;
            ,ARCATSCM = this.w_ARCATSCM;
            ,ARCLALOT = this.w_ARCLALOT;
            ,ARCOCOL1 = this.w_ARCOCOL1;
            ,ARCOCOL2 = this.w_ARCOCOL2;
            ,ARCODCLA = this.w_ARCODCLA;
            ,ARCODDIS = this.w_ARCODDIS;
            ,ARCODFAM = this.w_ARCODFAM;
            ,ARCODGRU = this.w_ARCODGRU;
            ,ARCODIVA = this.w_ARCODIVA;
            ,ARCODMAR = this.w_ARCODMAR;
            ,ARCODREP = this.w_ARCODREP;
            ,ARCODRIC = this.w_ARCODRIC;
            ,ARCODSOT = this.w_ARCODSOT;
            ,ARDESART = this.w_ARDESART;
            ,ARDESSUP = this.w_ARDESSUP;
            ,ARDESVO2 = this.w_ARDESVO2;
            ,ARDESVOL = this.w_ARDESVOL;
            ,ARDIMAL2 = this.w_ARDIMAL2;
            ,ARDIMALT = this.w_ARDIMALT;
            ,ARDIMLA2 = this.w_ARDIMLA2;
            ,ARDIMLAR = this.w_ARDIMLAR;
            ,ARDIMLU2 = this.w_ARDIMLU2;
            ,ARDIMLUN = this.w_ARDIMLUN;
            ,ARDISLOT = this.w_ARDISLOT;
            ,ARDTINVA = this.w_ARDTINVA;
            ,ARDTOBSO = this.w_ARDTOBSO;
            ,ARFLCOMP = this.w_ARFLCOMP;
            ,ARFLCON2 = this.w_ARFLCON2;
            ,ARFLCONA = this.w_ARFLCONA;
            ,ARFLDISC = this.w_ARFLDISC;
            ,ARFLDISP = this.w_ARFLDISP;
            ,ARFLESAU = this.w_ARFLESAU;
            ,ARFLESUL = this.w_ARFLESUL;
            ,ARFLINVE = this.w_ARFLINVE;
            ,ARFLLMAG = this.w_ARFLLMAG;
            ,ARFLPECO = this.w_ARFLPECO;
            ,ARFLSERG = this.w_ARFLSERG;
            ,ARFLSTCO = this.w_ARFLSTCO;
            ,ARFLUSEP = this.w_ARFLUSEP;
            ,ARGRUMER = this.w_ARGRUMER;
            ,ARGRUPRO = this.w_ARGRUPRO;
            ,ARMOLSUP = this.w_ARMOLSUP;
            ,ARMOLTIP = this.w_ARMOLTIP;
            ,ARNOMENC = this.w_ARNOMENC;
            ,AROPERAT = this.w_AROPERAT;
            ,ARPESLO2 = this.w_ARPESLO2;
            ,ARPESLOR = this.w_ARPESLOR;
            ,ARPESNE2 = this.w_ARPESNE2;
            ,ARPESNET = this.w_ARPESNET;
            ,ARPROPRE = this.w_ARPROPRE;
            ,ARPZCON2 = this.w_ARPZCON2;
            ,ARPZCONF = this.w_ARPZCONF;
            ,ARSTACOD = this.w_ARSTACOD;
            ,ARSTASUP = this.w_ARSTASUP;
            ,ARTIPART = this.w_ARTIPART;
            ,ARTIPBAR = this.w_ARTIPBAR;
            ,ARTIPCO1 = this.w_ARTIPCO1;
            ,ARTIPCO2 = this.w_ARTIPCO2;
            ,ARTIPGES = this.w_ARTIPGES;
            ,ARTIPMA1 = this.w_ARTIPMA1;
            ,ARTIPMA2 = this.w_ARTIPMA2;
            ,ARTIPPRE = this.w_ARTIPPRE;
            ,ARTIPSER = this.w_ARTIPSER;
            ,ARTPCON2 = this.w_ARTPCON2;
            ,ARTPCONF = this.w_ARTPCONF;
            ,ARUMDIM2 = this.w_ARUMDIM2;
            ,ARUMDIME = this.w_ARUMDIME;
            ,ARUMSUPP = this.w_ARUMSUPP;
            ,ARUMVOL2 = this.w_ARUMVOL2;
            ,ARUMVOLU = this.w_ARUMVOLU;
            ,ARUNMIS1 = this.w_ARUNMIS1;
            ,ARUNMIS2 = this.w_ARUNMIS2;
            ,ARVOCCEN = this.w_ARVOCCEN;
            ,ARVOCRIC = this.w_ARVOCRIC;
            ,ARCLAMAT = this.w_ARCLAMAT;
            ,ARGESMAT = this.w_ARGESMAT;
            ,ARFLLOTT = this.w_ARFLLOTT;
            ,ARDATINT = this.w_ARDATINT;
            ,ARTIPOPE = this.w_ARTIPOPE;
            ,ARCATOPE = this.w_ARCATOPE;
            &i_ccchkf. ;
         where;
            ARCODART = this.w_ARCODART;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Fallita la scrittura di un articolo!'
        return
      endif
    endif
    if i_Rows>0
      this.w_AGGART = this.w_AGGART+1
    endif
    return
  proc Try_058E6C60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from PAR_RIOR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" PAR_RIOR where ";
            +"PRCODART = "+cp_ToStrODBC(this.w_ARCODART);
            +" and PRCODVAR = "+cp_ToStrODBC(_Curs_DET_VARI.DVCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            PRCODART = this.w_ARCODART;
            and PRCODVAR = _Curs_DET_VARI.DVCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ORSCOMIN = NVL(cp_ToDate(_read_.PRSCOMIN),cp_NullValue(_read_.PRSCOMIN))
      this.w_ORSCOMAX = NVL(cp_ToDate(_read_.PRSCOMAX),cp_NullValue(_read_.PRSCOMAX))
      this.w_ORDISMIN = NVL(cp_ToDate(_read_.PRDISMIN),cp_NullValue(_read_.PRDISMIN))
      this.w_ORQTAMIN = NVL(cp_ToDate(_read_.PRQTAMIN),cp_NullValue(_read_.PRQTAMIN))
      this.w_ORLOTRIO = NVL(cp_ToDate(_read_.PRLOTRIO),cp_NullValue(_read_.PRLOTRIO))
      this.w_ORPUNRIO = NVL(cp_ToDate(_read_.PRPUNRIO),cp_NullValue(_read_.PRPUNRIO))
      this.w_ORGIOAPP = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
      this.w_ORGIOINV = NVL(cp_ToDate(_read_.PRGIOINV),cp_NullValue(_read_.PRGIOINV))
      this.w_ORCOSSTA = NVL(cp_ToDate(_read_.PRCOSSTA),cp_NullValue(_read_.PRCOSSTA))
      this.w_ORTIPCON = NVL(cp_ToDate(_read_.PRTIPCON),cp_NullValue(_read_.PRTIPCON))
      this.w_ORCODPRO = NVL(cp_ToDate(_read_.PRCODPRO),cp_NullValue(_read_.PRCODPRO))
      this.w_ORCODFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
      this.w_ORKEYVAR = NVL(cp_ToDate(_read_.PRKEYVAR),cp_NullValue(_read_.PRKEYVAR))
      this.w_ORKEYSAL = NVL(cp_ToDate(_read_.PRKEYSAL),cp_NullValue(_read_.PRKEYSAL))
      this.w_ORSAFELT = NVL(cp_ToDate(_read_.PRSAFELT),cp_NullValue(_read_.PRSAFELT))
      this.w_ORGIOAPP = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
      this.w_ORCOERSS = NVL(cp_ToDate(_read_.PRCOERSS),cp_NullValue(_read_.PRCOERSS))
      this.w_ORGIOCOP = NVL(cp_ToDate(_read_.PRGIOCOP),cp_NullValue(_read_.PRGIOCOP))
      this.w_ORPUNLOT = NVL(cp_ToDate(_read_.PRPUNLOT),cp_NullValue(_read_.PRPUNLOT))
      this.w_ORLISCOS = NVL(cp_ToDate(_read_.PRLISCOS),cp_NullValue(_read_.PRLISCOS))
      this.w_PUUTEELA = NVL(cp_ToDate(_read_.PUUTEELA),cp_NullValue(_read_.PUUTEELA))
      this.w_ORESECOS = NVL(cp_ToDate(_read_.PRESECOS),cp_NullValue(_read_.PRESECOS))
      this.w_ORINVCOS = NVL(cp_ToDate(_read_.PRINVCOS),cp_NullValue(_read_.PRINVCOS))
      this.w_ORCSLAVO = NVL(cp_ToDate(_read_.PRCSLAVO),cp_NullValue(_read_.PRCSLAVO))
      this.w_ORCSMATE = NVL(cp_ToDate(_read_.PRCSMATE),cp_NullValue(_read_.PRCSMATE))
      this.w_ORCSSPGE = NVL(cp_ToDate(_read_.PRCSSPGE),cp_NullValue(_read_.PRCSSPGE))
      this.w_ORLOTSTD = NVL(cp_ToDate(_read_.PRLOTSTD),cp_NullValue(_read_.PRLOTSTD))
      this.w_ORDATCOS = NVL(cp_ToDate(_read_.PRDATCOS),cp_NullValue(_read_.PRDATCOS))
      this.w_ORCULAVO = NVL(cp_ToDate(_read_.PRCULAVO),cp_NullValue(_read_.PRCULAVO))
      this.w_ORCUMATE = NVL(cp_ToDate(_read_.PRCUMATE),cp_NullValue(_read_.PRCUMATE))
      this.w_ORCUSPGE = NVL(cp_ToDate(_read_.PRCUSPGE),cp_NullValue(_read_.PRCUSPGE))
      this.w_ORLOTULT = NVL(cp_ToDate(_read_.PRLOTULT),cp_NullValue(_read_.PRLOTULT))
      this.w_ORDATULT = NVL(cp_ToDate(_read_.PRDATULT),cp_NullValue(_read_.PRDATULT))
      this.w_ORCMMATE = NVL(cp_ToDate(_read_.PRCMMATE),cp_NullValue(_read_.PRCMMATE))
      this.w_ORCMLAVO = NVL(cp_ToDate(_read_.PRCMLAVO),cp_NullValue(_read_.PRCMLAVO))
      this.w_ORCMSPGE = NVL(cp_ToDate(_read_.PRCMSPGE),cp_NullValue(_read_.PRCMSPGE))
      this.w_ORLOTMED = NVL(cp_ToDate(_read_.PRLOTMED),cp_NullValue(_read_.PRLOTMED))
      this.w_ORDATMED = NVL(cp_ToDate(_read_.PRDATMED),cp_NullValue(_read_.PRDATMED))
      this.w_ORCLLAVO = NVL(cp_ToDate(_read_.PRCLLAVO),cp_NullValue(_read_.PRCLLAVO))
      this.w_ORCLMATE = NVL(cp_ToDate(_read_.PRCLMATE),cp_NullValue(_read_.PRCLMATE))
      this.w_ORCLSPGE = NVL(cp_ToDate(_read_.PRCLSPGE),cp_NullValue(_read_.PRCLSPGE))
      this.w_ORLOTLIS = NVL(cp_ToDate(_read_.PRLOTLIS),cp_NullValue(_read_.PRLOTLIS))
      this.w_ORDATLIS = NVL(cp_ToDate(_read_.PRDATLIS),cp_NullValue(_read_.PRDATLIS))
      this.w_ORLOWLEV = NVL(cp_ToDate(_read_.PRLOWLEV),cp_NullValue(_read_.PRLOWLEV))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PRSCOMIN = IIF(EMPTY(this.w_PRSCOMIN),this.w_ORSCOMIN,this.w_PRSCOMIN)
    this.w_PRSCOMAX = IIF(EMPTY(this.w_PRSCOMAX),this.w_ORSCOMAX,this.w_PRSCOMAX)
    this.w_PRDISMIN = IIF(EMPTY(this.w_PRDISMIN),this.w_ORDISMIN,this.w_PRDISMIN)
    this.w_PRQTAMIN = IIF(EMPTY(this.w_PRQTAMIN),this.w_ORQTAMIN,this.w_PRQTAMIN)
    this.w_PRLOTRIO = IIF(EMPTY(this.w_PRLOTRIO),this.w_ORLOTRIO,this.w_PRLOTRIO)
    this.w_PRPUNRIO = IIF(EMPTY(this.w_PRPUNRIO),this.w_ORPUNRIO,this.w_PRPUNRIO)
    this.w_PRGIOAPP = IIF(EMPTY(this.w_PRGIOAPP),this.w_ORGIOAPP,this.w_PRGIOAPP)
    this.w_PRGIOINV = IIF(EMPTY(this.w_PRGIOINV),this.w_ORGIOINV,this.w_PRGIOINV)
    this.w_PRCOSSTA = IIF(EMPTY(this.w_PRCOSSTA),this.w_ORCOSSTA,this.w_PRCOSSTA)
    this.w_PRTIPCON = IIF(EMPTY(this.w_PRTIPCON),this.w_ORTIPCON,this.w_PRTIPCON)
    this.w_PRCODPRO = IIF(EMPTY(this.w_PRCODPRO),this.w_ORCODPRO,this.w_PRCODPRO)
    this.w_PRCODFOR = IIF(EMPTY(this.w_PRCODFOR),this.w_ORCODFOR,this.w_PRCODFOR)
    this.w_PRCODART = IIF(EMPTY(this.w_PRCODART),this.w_ORCODART,this.w_PRCODART)
    this.w_PRCODVAR = IIF(EMPTY(this.w_PRCODVAR),this.w_ORCODVAR,this.w_PRCODVAR)
    this.w_PRKEYVAR = IIF(EMPTY(this.w_PRKEYVAR),this.w_ORKEYVAR,this.w_PRKEYVAR)
    this.w_PRKEYSAL = IIF(EMPTY(this.w_PRKEYSAL),this.w_ORKEYSAL,this.w_PRKEYSAL)
    this.w_PRSAFELT = IIF(EMPTY(this.w_PRSAFELT),this.w_ORSAFELT,this.w_PRSAFELT)
    this.w_PRGIOAPP = IIF(EMPTY(this.w_PRGIOAPP),this.w_ORGIOAPP,this.w_PRGIOAPP)
    this.w_PRCOERSS = IIF(EMPTY(this.w_PRCOERSS),this.w_ORCOERSS,this.w_PRCOERSS)
    this.w_PRGIOCOP = IIF(EMPTY(this.w_PRGIOCOP),this.w_ORGIOCOP,this.w_PRGIOCOP)
    this.w_PRPUNLOT = IIF(EMPTY(this.w_PRPUNLOT),this.w_ORPUNLOT,this.w_PRPUNLOT)
    this.w_PRLISCOS = IIF(EMPTY(this.w_PRLISCOS),this.w_ORLISCOS,this.w_PRLISCOS)
    this.w_PUUTEELA = IIF(EMPTY(this.w_PUUTEELA),this.w_OUUTEELA,this.w_PUUTEELA)
    this.w_PRESECOS = IIF(EMPTY(this.w_PRESECOS),this.w_ORESECOS,this.w_PRESECOS)
    this.w_PRINVCOS = IIF(EMPTY(this.w_PRINVCOS),this.w_ORINVCOS,this.w_PRINVCOS)
    this.w_PRCSLAVO = IIF(EMPTY(this.w_PRCSLAVO),this.w_ORCSLAVO,this.w_PRCSLAVO)
    this.w_PRCSMATE = IIF(EMPTY(this.w_PRCSMATE),this.w_ORCSMATE,this.w_PRCSMATE)
    this.w_PRCSSPGE = IIF(EMPTY(this.w_PRCSSPGE),this.w_ORCSSPGE,this.w_PRCSSPGE)
    this.w_PRLOTSTD = IIF(EMPTY(this.w_PRLOTSTD),this.w_ORLOTSTD,this.w_PRLOTSTD)
    this.w_PRDATCOS = IIF(EMPTY(this.w_PRDATCOS),this.w_ORDATCOS,this.w_PRDATCOS)
    this.w_PRCULAVO = IIF(EMPTY(this.w_PRCULAVO),this.w_ORCULAVO,this.w_PRCULAVO)
    this.w_PRCUMATE = IIF(EMPTY(this.w_PRCUMATE),this.w_ORCUMATE,this.w_PRCUMATE)
    this.w_PRCUSPGE = IIF(EMPTY(this.w_PRCUSPGE),this.w_ORCUSPGE,this.w_PRCUSPGE)
    this.w_PRLOTULT = IIF(EMPTY(this.w_PRLOTULT),this.w_ORLOTULT,this.w_PRLOTULT)
    this.w_PRDATULT = IIF(EMPTY(this.w_PRDATULT),this.w_ORDATULT,this.w_PRDATULT)
    this.w_PRCMMATE = IIF(EMPTY(this.w_PRCMMATE),this.w_ORCMMATE,this.w_PRCMMATE)
    this.w_PRCMLAVO = IIF(EMPTY(this.w_PRCMLAVO),this.w_ORCMLAVO,this.w_PRCMLAVO)
    this.w_PRCMSPGE = IIF(EMPTY(this.w_PRCMSPGE),this.w_ORCMSPGE,this.w_PRCMSPGE)
    this.w_PRLOTMED = IIF(EMPTY(this.w_PRLOTMED),this.w_ORLOTMED,this.w_PRLOTMED)
    this.w_PRDATMED = IIF(EMPTY(this.w_PRDATMED),this.w_ORDATMED,this.w_PRDATMED)
    this.w_PRCLLAVO = IIF(EMPTY(this.w_PRCLLAVO),this.w_ORCLLAVO,this.w_PRCLLAVO)
    this.w_PRCLMATE = IIF(EMPTY(this.w_PRCLMATE),this.w_ORCLMATE,this.w_PRCLMATE)
    this.w_PRCLSPGE = IIF(EMPTY(this.w_PRCLSPGE),this.w_ORCLSPGE,this.w_PRCLSPGE)
    this.w_PRLOTLIS = IIF(EMPTY(this.w_PRLOTLIS),this.w_ORLOTLIS,this.w_PRLOTLIS)
    this.w_PRDATLIS = IIF(EMPTY(this.w_PRDATLIS),this.w_ORDATLIS,this.w_PRDATLIS)
    this.w_PRLOWLEV = IIF(EMPTY(this.w_PRLOWLEV),this.w_ORLOWLEV,this.w_PRLOWLEV)
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRSCOMIN ="+cp_NullLink(cp_ToStrODBC(this.w_PRSCOMIN),'PAR_RIOR','PRSCOMIN');
      +",PRSCOMAX ="+cp_NullLink(cp_ToStrODBC(this.w_PRSCOMAX),'PAR_RIOR','PRSCOMAX');
      +",PRDISMIN ="+cp_NullLink(cp_ToStrODBC(this.w_PRDISMIN),'PAR_RIOR','PRDISMIN');
      +",PRQTAMIN ="+cp_NullLink(cp_ToStrODBC(this.w_PRQTAMIN),'PAR_RIOR','PRQTAMIN');
      +",PRLOTRIO ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOTRIO),'PAR_RIOR','PRLOTRIO');
      +",PRPUNRIO ="+cp_NullLink(cp_ToStrODBC(this.w_PRPUNRIO),'PAR_RIOR','PRPUNRIO');
      +",PRGIOAPP ="+cp_NullLink(cp_ToStrODBC(this.w_PRGIOAPP),'PAR_RIOR','PRGIOAPP');
      +",PRGIOINV ="+cp_NullLink(cp_ToStrODBC(this.w_PRGIOINV),'PAR_RIOR','PRGIOINV');
      +",PRCOSSTA ="+cp_NullLink(cp_ToStrODBC(this.w_PRCOSSTA),'PAR_RIOR','PRCOSSTA');
      +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_PRTIPCON),'PAR_RIOR','PRTIPCON');
      +",PRCODPRO ="+cp_NullLink(cp_ToStrODBC(this.w_PRCODPRO),'PAR_RIOR','PRCODPRO');
      +",PRCODFOR ="+cp_NullLink(cp_ToStrODBC(this.w_PRCODFOR),'PAR_RIOR','PRCODFOR');
      +",PRKEYVAR ="+cp_NullLink(cp_ToStrODBC(this.w_PRKEYVAR),'PAR_RIOR','PRKEYVAR');
      +",PRKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_PRKEYSAL),'PAR_RIOR','PRKEYSAL');
      +",PRSAFELT ="+cp_NullLink(cp_ToStrODBC(this.w_PRSAFELT),'PAR_RIOR','PRSAFELT');
      +",PRCOERSS ="+cp_NullLink(cp_ToStrODBC(this.w_PRCOERSS),'PAR_RIOR','PRCOERSS');
      +",PRGIOCOP ="+cp_NullLink(cp_ToStrODBC(this.w_PRGIOCOP),'PAR_RIOR','PRGIOCOP');
      +",PRPUNLOT ="+cp_NullLink(cp_ToStrODBC(this.w_PRPUNLOT),'PAR_RIOR','PRPUNLOT');
      +",PRLISCOS ="+cp_NullLink(cp_ToStrODBC(this.w_PRLISCOS),'PAR_RIOR','PRLISCOS');
      +",PUUTEELA ="+cp_NullLink(cp_ToStrODBC(this.w_PUUTEELA),'PAR_RIOR','PUUTEELA');
      +",PRESECOS ="+cp_NullLink(cp_ToStrODBC(this.w_PRESECOS),'PAR_RIOR','PRESECOS');
      +",PRINVCOS ="+cp_NullLink(cp_ToStrODBC(this.w_PRINVCOS),'PAR_RIOR','PRINVCOS');
      +",PRCSLAVO ="+cp_NullLink(cp_ToStrODBC(this.w_PRCSLAVO),'PAR_RIOR','PRCSLAVO');
      +",PRCSMATE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCSMATE),'PAR_RIOR','PRCSMATE');
      +",PRCSSPGE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCSSPGE),'PAR_RIOR','PRCSSPGE');
      +",PRLOTSTD ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOTSTD),'PAR_RIOR','PRLOTSTD');
      +",PRDATCOS ="+cp_NullLink(cp_ToStrODBC(this.w_PRDATCOS),'PAR_RIOR','PRDATCOS');
      +",PRCULAVO ="+cp_NullLink(cp_ToStrODBC(this.w_PRCULAVO),'PAR_RIOR','PRCULAVO');
      +",PRCUMATE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCUMATE),'PAR_RIOR','PRCUMATE');
      +",PRCUSPGE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCUSPGE),'PAR_RIOR','PRCUSPGE');
      +",PRLOTULT ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOTULT),'PAR_RIOR','PRLOTULT');
      +",PRDATULT ="+cp_NullLink(cp_ToStrODBC(this.w_PRDATULT),'PAR_RIOR','PRDATULT');
      +",PRCMMATE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCMMATE),'PAR_RIOR','PRCMMATE');
      +",PRCMLAVO ="+cp_NullLink(cp_ToStrODBC(this.w_PRCMLAVO),'PAR_RIOR','PRCMLAVO');
      +",PRCMSPGE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCMSPGE),'PAR_RIOR','PRCMSPGE');
      +",PRLOTMED ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOTMED),'PAR_RIOR','PRLOTMED');
      +",PRDATMED ="+cp_NullLink(cp_ToStrODBC(this.w_PRDATMED),'PAR_RIOR','PRDATMED');
      +",PRCLLAVO ="+cp_NullLink(cp_ToStrODBC(this.w_PRCLLAVO),'PAR_RIOR','PRCLLAVO');
      +",PRCLMATE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCLMATE),'PAR_RIOR','PRCLMATE');
      +",PRCLSPGE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCLSPGE),'PAR_RIOR','PRCLSPGE');
      +",PRLOTLIS ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOTLIS),'PAR_RIOR','PRLOTLIS');
      +",PRDATLIS ="+cp_NullLink(cp_ToStrODBC(this.w_PRDATLIS),'PAR_RIOR','PRDATLIS');
      +",PRLOWLEV ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOWLEV),'PAR_RIOR','PRLOWLEV');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_ARCODART);
          +" and PRCODVAR = "+cp_ToStrODBC(_Curs_DET_VARI.DVCODICE);
             )
    else
      update (i_cTable) set;
          PRSCOMIN = this.w_PRSCOMIN;
          ,PRSCOMAX = this.w_PRSCOMAX;
          ,PRDISMIN = this.w_PRDISMIN;
          ,PRQTAMIN = this.w_PRQTAMIN;
          ,PRLOTRIO = this.w_PRLOTRIO;
          ,PRPUNRIO = this.w_PRPUNRIO;
          ,PRGIOAPP = this.w_PRGIOAPP;
          ,PRGIOINV = this.w_PRGIOINV;
          ,PRCOSSTA = this.w_PRCOSSTA;
          ,PRTIPCON = this.w_PRTIPCON;
          ,PRCODPRO = this.w_PRCODPRO;
          ,PRCODFOR = this.w_PRCODFOR;
          ,PRKEYVAR = this.w_PRKEYVAR;
          ,PRKEYSAL = this.w_PRKEYSAL;
          ,PRSAFELT = this.w_PRSAFELT;
          ,PRCOERSS = this.w_PRCOERSS;
          ,PRGIOCOP = this.w_PRGIOCOP;
          ,PRPUNLOT = this.w_PRPUNLOT;
          ,PRLISCOS = this.w_PRLISCOS;
          ,PUUTEELA = this.w_PUUTEELA;
          ,PRESECOS = this.w_PRESECOS;
          ,PRINVCOS = this.w_PRINVCOS;
          ,PRCSLAVO = this.w_PRCSLAVO;
          ,PRCSMATE = this.w_PRCSMATE;
          ,PRCSSPGE = this.w_PRCSSPGE;
          ,PRLOTSTD = this.w_PRLOTSTD;
          ,PRDATCOS = this.w_PRDATCOS;
          ,PRCULAVO = this.w_PRCULAVO;
          ,PRCUMATE = this.w_PRCUMATE;
          ,PRCUSPGE = this.w_PRCUSPGE;
          ,PRLOTULT = this.w_PRLOTULT;
          ,PRDATULT = this.w_PRDATULT;
          ,PRCMMATE = this.w_PRCMMATE;
          ,PRCMLAVO = this.w_PRCMLAVO;
          ,PRCMSPGE = this.w_PRCMSPGE;
          ,PRLOTMED = this.w_PRLOTMED;
          ,PRDATMED = this.w_PRDATMED;
          ,PRCLLAVO = this.w_PRCLLAVO;
          ,PRCLMATE = this.w_PRCLMATE;
          ,PRCLSPGE = this.w_PRCLSPGE;
          ,PRLOTLIS = this.w_PRLOTLIS;
          ,PRDATLIS = this.w_PRDATLIS;
          ,PRLOWLEV = this.w_PRLOWLEV;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_ARCODART;
          and PRCODVAR = _Curs_DET_VARI.DVCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Fallita la scrittura di un dato articolo per valoriz.!'
      return
    endif
    return
  proc Try_05907EF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from PAR_RIOR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" PAR_RIOR where ";
            +"PRCODART = "+cp_ToStrODBC(this.w_ARCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            PRCODART = this.w_ARCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ORSCOMIN = NVL(cp_ToDate(_read_.PRSCOMIN),cp_NullValue(_read_.PRSCOMIN))
      this.w_ORSCOMAX = NVL(cp_ToDate(_read_.PRSCOMAX),cp_NullValue(_read_.PRSCOMAX))
      this.w_ORDISMIN = NVL(cp_ToDate(_read_.PRDISMIN),cp_NullValue(_read_.PRDISMIN))
      this.w_ORQTAMIN = NVL(cp_ToDate(_read_.PRQTAMIN),cp_NullValue(_read_.PRQTAMIN))
      this.w_ORLOTRIO = NVL(cp_ToDate(_read_.PRLOTRIO),cp_NullValue(_read_.PRLOTRIO))
      this.w_ORPUNRIO = NVL(cp_ToDate(_read_.PRPUNRIO),cp_NullValue(_read_.PRPUNRIO))
      this.w_ORGIOAPP = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
      this.w_ORGIOINV = NVL(cp_ToDate(_read_.PRGIOINV),cp_NullValue(_read_.PRGIOINV))
      this.w_ORCOSSTA = NVL(cp_ToDate(_read_.PRCOSSTA),cp_NullValue(_read_.PRCOSSTA))
      this.w_ORTIPCON = NVL(cp_ToDate(_read_.PRTIPCON),cp_NullValue(_read_.PRTIPCON))
      this.w_ORCODPRO = NVL(cp_ToDate(_read_.PRCODPRO),cp_NullValue(_read_.PRCODPRO))
      this.w_ORCODFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
      this.w_ORCODVAR = NVL(cp_ToDate(_read_.PRCODVAR),cp_NullValue(_read_.PRCODVAR))
      this.w_ORKEYVAR = NVL(cp_ToDate(_read_.PRKEYVAR),cp_NullValue(_read_.PRKEYVAR))
      this.w_ORKEYSAL = NVL(cp_ToDate(_read_.PRKEYSAL),cp_NullValue(_read_.PRKEYSAL))
      this.w_ORSAFELT = NVL(cp_ToDate(_read_.PRSAFELT),cp_NullValue(_read_.PRSAFELT))
      this.w_ORGIOAPP = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
      this.w_ORCOERSS = NVL(cp_ToDate(_read_.PRCOERSS),cp_NullValue(_read_.PRCOERSS))
      this.w_ORGIOCOP = NVL(cp_ToDate(_read_.PRGIOCOP),cp_NullValue(_read_.PRGIOCOP))
      this.w_ORPUNLOT = NVL(cp_ToDate(_read_.PRPUNLOT),cp_NullValue(_read_.PRPUNLOT))
      this.w_ORLISCOS = NVL(cp_ToDate(_read_.PRLISCOS),cp_NullValue(_read_.PRLISCOS))
      this.w_PUUTEELA = NVL(cp_ToDate(_read_.PUUTEELA),cp_NullValue(_read_.PUUTEELA))
      this.w_ORESECOS = NVL(cp_ToDate(_read_.PRESECOS),cp_NullValue(_read_.PRESECOS))
      this.w_ORINVCOS = NVL(cp_ToDate(_read_.PRINVCOS),cp_NullValue(_read_.PRINVCOS))
      this.w_ORCSLAVO = NVL(cp_ToDate(_read_.PRCSLAVO),cp_NullValue(_read_.PRCSLAVO))
      this.w_ORCSMATE = NVL(cp_ToDate(_read_.PRCSMATE),cp_NullValue(_read_.PRCSMATE))
      this.w_ORCSSPGE = NVL(cp_ToDate(_read_.PRCSSPGE),cp_NullValue(_read_.PRCSSPGE))
      this.w_ORLOTSTD = NVL(cp_ToDate(_read_.PRLOTSTD),cp_NullValue(_read_.PRLOTSTD))
      this.w_ORDATCOS = NVL(cp_ToDate(_read_.PRDATCOS),cp_NullValue(_read_.PRDATCOS))
      this.w_ORCULAVO = NVL(cp_ToDate(_read_.PRCULAVO),cp_NullValue(_read_.PRCULAVO))
      this.w_ORCUMATE = NVL(cp_ToDate(_read_.PRCUMATE),cp_NullValue(_read_.PRCUMATE))
      this.w_ORCUSPGE = NVL(cp_ToDate(_read_.PRCUSPGE),cp_NullValue(_read_.PRCUSPGE))
      this.w_ORLOTULT = NVL(cp_ToDate(_read_.PRLOTULT),cp_NullValue(_read_.PRLOTULT))
      this.w_ORDATULT = NVL(cp_ToDate(_read_.PRDATULT),cp_NullValue(_read_.PRDATULT))
      this.w_ORCMMATE = NVL(cp_ToDate(_read_.PRCMMATE),cp_NullValue(_read_.PRCMMATE))
      this.w_ORCMLAVO = NVL(cp_ToDate(_read_.PRCMLAVO),cp_NullValue(_read_.PRCMLAVO))
      this.w_ORCMSPGE = NVL(cp_ToDate(_read_.PRCMSPGE),cp_NullValue(_read_.PRCMSPGE))
      this.w_ORLOTMED = NVL(cp_ToDate(_read_.PRLOTMED),cp_NullValue(_read_.PRLOTMED))
      this.w_ORDATMED = NVL(cp_ToDate(_read_.PRDATMED),cp_NullValue(_read_.PRDATMED))
      this.w_ORCLLAVO = NVL(cp_ToDate(_read_.PRCLLAVO),cp_NullValue(_read_.PRCLLAVO))
      this.w_ORCLMATE = NVL(cp_ToDate(_read_.PRCLMATE),cp_NullValue(_read_.PRCLMATE))
      this.w_ORCLSPGE = NVL(cp_ToDate(_read_.PRCLSPGE),cp_NullValue(_read_.PRCLSPGE))
      this.w_ORLOTLIS = NVL(cp_ToDate(_read_.PRLOTLIS),cp_NullValue(_read_.PRLOTLIS))
      this.w_ORDATLIS = NVL(cp_ToDate(_read_.PRDATLIS),cp_NullValue(_read_.PRDATLIS))
      this.w_ORLOWLEV = NVL(cp_ToDate(_read_.PRLOWLEV),cp_NullValue(_read_.PRLOWLEV))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PRSCOMIN = IIF(EMPTY(this.w_PRSCOMIN),this.w_ORSCOMIN,this.w_PRSCOMIN)
    this.w_PRSCOMAX = IIF(EMPTY(this.w_PRSCOMAX),this.w_ORSCOMAX,this.w_PRSCOMAX)
    this.w_PRDISMIN = IIF(EMPTY(this.w_PRDISMIN),this.w_ORDISMIN,this.w_PRDISMIN)
    this.w_PRQTAMIN = IIF(EMPTY(this.w_PRQTAMIN),this.w_ORQTAMIN,this.w_PRQTAMIN)
    this.w_PRLOTRIO = IIF(EMPTY(this.w_PRLOTRIO),this.w_ORLOTRIO,this.w_PRLOTRIO)
    this.w_PRPUNRIO = IIF(EMPTY(this.w_PRPUNRIO),this.w_ORPUNRIO,this.w_PRPUNRIO)
    this.w_PRGIOAPP = IIF(EMPTY(this.w_PRGIOAPP),this.w_ORGIOAPP,this.w_PRGIOAPP)
    this.w_PRGIOINV = IIF(EMPTY(this.w_PRGIOINV),this.w_ORGIOINV,this.w_PRGIOINV)
    this.w_PRCOSSTA = IIF(EMPTY(this.w_PRCOSSTA),this.w_ORCOSSTA,this.w_PRCOSSTA)
    this.w_PRTIPCON = IIF(EMPTY(this.w_PRTIPCON),this.w_ORTIPCON,this.w_PRTIPCON)
    this.w_PRCODPRO = IIF(EMPTY(this.w_PRCODPRO),this.w_ORCODPRO,this.w_PRCODPRO)
    this.w_PRCODFOR = IIF(EMPTY(this.w_PRCODFOR),this.w_ORCODFOR,this.w_PRCODFOR)
    this.w_PRCODART = IIF(EMPTY(this.w_PRCODART),this.w_ORCODART,this.w_PRCODART)
    this.w_PRCODVAR = IIF(EMPTY(this.w_PRCODVAR),this.w_ORCODVAR,this.w_PRCODVAR)
    this.w_PRKEYVAR = IIF(EMPTY(this.w_PRKEYVAR),this.w_ORKEYVAR,this.w_PRKEYVAR)
    this.w_PRKEYSAL = IIF(EMPTY(this.w_PRKEYSAL),this.w_ORKEYSAL,this.w_PRKEYSAL)
    this.w_PRSAFELT = IIF(EMPTY(this.w_PRSAFELT),this.w_ORSAFELT,this.w_PRSAFELT)
    this.w_PRGIOAPP = IIF(EMPTY(this.w_PRGIOAPP),this.w_ORGIOAPP,this.w_PRGIOAPP)
    this.w_PRCOERSS = IIF(EMPTY(this.w_PRCOERSS),this.w_ORCOERSS,this.w_PRCOERSS)
    this.w_PRGIOCOP = IIF(EMPTY(this.w_PRGIOCOP),this.w_ORGIOCOP,this.w_PRGIOCOP)
    this.w_PRPUNLOT = IIF(EMPTY(this.w_PRPUNLOT),this.w_ORPUNLOT,this.w_PRPUNLOT)
    this.w_PRLISCOS = IIF(EMPTY(this.w_PRLISCOS),this.w_ORLISCOS,this.w_PRLISCOS)
    this.w_PUUTEELA = IIF(EMPTY(this.w_PUUTEELA),this.w_OUUTEELA,this.w_PUUTEELA)
    this.w_PRESECOS = IIF(EMPTY(this.w_PRESECOS),this.w_ORESECOS,this.w_PRESECOS)
    this.w_PRINVCOS = IIF(EMPTY(this.w_PRINVCOS),this.w_ORINVCOS,this.w_PRINVCOS)
    this.w_PRCSLAVO = IIF(EMPTY(this.w_PRCSLAVO),this.w_ORCSLAVO,this.w_PRCSLAVO)
    this.w_PRCSMATE = IIF(EMPTY(this.w_PRCSMATE),this.w_ORCSMATE,this.w_PRCSMATE)
    this.w_PRCSSPGE = IIF(EMPTY(this.w_PRCSSPGE),this.w_ORCSSPGE,this.w_PRCSSPGE)
    this.w_PRLOTSTD = IIF(EMPTY(this.w_PRLOTSTD),this.w_ORLOTSTD,this.w_PRLOTSTD)
    this.w_PRDATCOS = IIF(EMPTY(this.w_PRDATCOS),this.w_ORDATCOS,this.w_PRDATCOS)
    this.w_PRCULAVO = IIF(EMPTY(this.w_PRCULAVO),this.w_ORCULAVO,this.w_PRCULAVO)
    this.w_PRCUMATE = IIF(EMPTY(this.w_PRCUMATE),this.w_ORCUMATE,this.w_PRCUMATE)
    this.w_PRCUSPGE = IIF(EMPTY(this.w_PRCUSPGE),this.w_ORCUSPGE,this.w_PRCUSPGE)
    this.w_PRLOTULT = IIF(EMPTY(this.w_PRLOTULT),this.w_ORLOTULT,this.w_PRLOTULT)
    this.w_PRDATULT = IIF(EMPTY(this.w_PRDATULT),this.w_ORDATULT,this.w_PRDATULT)
    this.w_PRCMMATE = IIF(EMPTY(this.w_PRCMMATE),this.w_ORCMMATE,this.w_PRCMMATE)
    this.w_PRCMLAVO = IIF(EMPTY(this.w_PRCMLAVO),this.w_ORCMLAVO,this.w_PRCMLAVO)
    this.w_PRCMSPGE = IIF(EMPTY(this.w_PRCMSPGE),this.w_ORCMSPGE,this.w_PRCMSPGE)
    this.w_PRLOTMED = IIF(EMPTY(this.w_PRLOTMED),this.w_ORLOTMED,this.w_PRLOTMED)
    this.w_PRDATMED = IIF(EMPTY(this.w_PRDATMED),this.w_ORDATMED,this.w_PRDATMED)
    this.w_PRCLLAVO = IIF(EMPTY(this.w_PRCLLAVO),this.w_ORCLLAVO,this.w_PRCLLAVO)
    this.w_PRCLMATE = IIF(EMPTY(this.w_PRCLMATE),this.w_ORCLMATE,this.w_PRCLMATE)
    this.w_PRCLSPGE = IIF(EMPTY(this.w_PRCLSPGE),this.w_ORCLSPGE,this.w_PRCLSPGE)
    this.w_PRLOTLIS = IIF(EMPTY(this.w_PRLOTLIS),this.w_ORLOTLIS,this.w_PRLOTLIS)
    this.w_PRDATLIS = IIF(EMPTY(this.w_PRDATLIS),this.w_ORDATLIS,this.w_PRDATLIS)
    this.w_PRLOWLEV = IIF(EMPTY(this.w_PRLOWLEV),this.w_ORLOWLEV,this.w_PRLOWLEV)
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRSCOMIN ="+cp_NullLink(cp_ToStrODBC(this.w_PRSCOMIN),'PAR_RIOR','PRSCOMIN');
      +",PRSCOMAX ="+cp_NullLink(cp_ToStrODBC(this.w_PRSCOMAX),'PAR_RIOR','PRSCOMAX');
      +",PRDISMIN ="+cp_NullLink(cp_ToStrODBC(this.w_PRDISMIN),'PAR_RIOR','PRDISMIN');
      +",PRQTAMIN ="+cp_NullLink(cp_ToStrODBC(this.w_PRQTAMIN),'PAR_RIOR','PRQTAMIN');
      +",PRLOTRIO ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOTRIO),'PAR_RIOR','PRLOTRIO');
      +",PRPUNRIO ="+cp_NullLink(cp_ToStrODBC(this.w_PRPUNRIO),'PAR_RIOR','PRPUNRIO');
      +",PRGIOAPP ="+cp_NullLink(cp_ToStrODBC(this.w_PRGIOAPP),'PAR_RIOR','PRGIOAPP');
      +",PRGIOINV ="+cp_NullLink(cp_ToStrODBC(this.w_PRGIOINV),'PAR_RIOR','PRGIOINV');
      +",PRCOSSTA ="+cp_NullLink(cp_ToStrODBC(this.w_PRCOSSTA),'PAR_RIOR','PRCOSSTA');
      +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_PRTIPCON),'PAR_RIOR','PRTIPCON');
      +",PRCODPRO ="+cp_NullLink(cp_ToStrODBC(this.w_PRCODPRO),'PAR_RIOR','PRCODPRO');
      +",PRCODFOR ="+cp_NullLink(cp_ToStrODBC(this.w_PRCODFOR),'PAR_RIOR','PRCODFOR');
      +",PRCODVAR ="+cp_NullLink(cp_ToStrODBC(this.w_PRCODVAR),'PAR_RIOR','PRCODVAR');
      +",PRKEYVAR ="+cp_NullLink(cp_ToStrODBC(this.w_PRKEYVAR),'PAR_RIOR','PRKEYVAR');
      +",PRKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_PRKEYSAL),'PAR_RIOR','PRKEYSAL');
      +",PRSAFELT ="+cp_NullLink(cp_ToStrODBC(this.w_PRSAFELT),'PAR_RIOR','PRSAFELT');
      +",PRCOERSS ="+cp_NullLink(cp_ToStrODBC(this.w_PRCOERSS),'PAR_RIOR','PRCOERSS');
      +",PRGIOCOP ="+cp_NullLink(cp_ToStrODBC(this.w_PRGIOCOP),'PAR_RIOR','PRGIOCOP');
      +",PRPUNLOT ="+cp_NullLink(cp_ToStrODBC(this.w_PRPUNLOT),'PAR_RIOR','PRPUNLOT');
      +",PRLISCOS ="+cp_NullLink(cp_ToStrODBC(this.w_PRLISCOS),'PAR_RIOR','PRLISCOS');
      +",PUUTEELA ="+cp_NullLink(cp_ToStrODBC(this.w_PUUTEELA),'PAR_RIOR','PUUTEELA');
      +",PRESECOS ="+cp_NullLink(cp_ToStrODBC(this.w_PRESECOS),'PAR_RIOR','PRESECOS');
      +",PRINVCOS ="+cp_NullLink(cp_ToStrODBC(this.w_PRINVCOS),'PAR_RIOR','PRINVCOS');
      +",PRCSLAVO ="+cp_NullLink(cp_ToStrODBC(this.w_PRCSLAVO),'PAR_RIOR','PRCSLAVO');
      +",PRCSMATE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCSMATE),'PAR_RIOR','PRCSMATE');
      +",PRCSSPGE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCSSPGE),'PAR_RIOR','PRCSSPGE');
      +",PRLOTSTD ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOTSTD),'PAR_RIOR','PRLOTSTD');
      +",PRDATCOS ="+cp_NullLink(cp_ToStrODBC(this.w_PRDATCOS),'PAR_RIOR','PRDATCOS');
      +",PRCULAVO ="+cp_NullLink(cp_ToStrODBC(this.w_PRCULAVO),'PAR_RIOR','PRCULAVO');
      +",PRCUMATE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCUMATE),'PAR_RIOR','PRCUMATE');
      +",PRCUSPGE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCUSPGE),'PAR_RIOR','PRCUSPGE');
      +",PRLOTULT ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOTULT),'PAR_RIOR','PRLOTULT');
      +",PRDATULT ="+cp_NullLink(cp_ToStrODBC(this.w_PRDATULT),'PAR_RIOR','PRDATULT');
      +",PRCMMATE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCMMATE),'PAR_RIOR','PRCMMATE');
      +",PRCMLAVO ="+cp_NullLink(cp_ToStrODBC(this.w_PRCMLAVO),'PAR_RIOR','PRCMLAVO');
      +",PRCMSPGE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCMSPGE),'PAR_RIOR','PRCMSPGE');
      +",PRLOTMED ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOTMED),'PAR_RIOR','PRLOTMED');
      +",PRDATMED ="+cp_NullLink(cp_ToStrODBC(this.w_PRDATMED),'PAR_RIOR','PRDATMED');
      +",PRCLLAVO ="+cp_NullLink(cp_ToStrODBC(this.w_PRCLLAVO),'PAR_RIOR','PRCLLAVO');
      +",PRCLMATE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCLMATE),'PAR_RIOR','PRCLMATE');
      +",PRCLSPGE ="+cp_NullLink(cp_ToStrODBC(this.w_PRCLSPGE),'PAR_RIOR','PRCLSPGE');
      +",PRLOTLIS ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOTLIS),'PAR_RIOR','PRLOTLIS');
      +",PRDATLIS ="+cp_NullLink(cp_ToStrODBC(this.w_PRDATLIS),'PAR_RIOR','PRDATLIS');
      +",PRLOWLEV ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOWLEV),'PAR_RIOR','PRLOWLEV');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_ARCODART);
             )
    else
      update (i_cTable) set;
          PRSCOMIN = this.w_PRSCOMIN;
          ,PRSCOMAX = this.w_PRSCOMAX;
          ,PRDISMIN = this.w_PRDISMIN;
          ,PRQTAMIN = this.w_PRQTAMIN;
          ,PRLOTRIO = this.w_PRLOTRIO;
          ,PRPUNRIO = this.w_PRPUNRIO;
          ,PRGIOAPP = this.w_PRGIOAPP;
          ,PRGIOINV = this.w_PRGIOINV;
          ,PRCOSSTA = this.w_PRCOSSTA;
          ,PRTIPCON = this.w_PRTIPCON;
          ,PRCODPRO = this.w_PRCODPRO;
          ,PRCODFOR = this.w_PRCODFOR;
          ,PRCODVAR = this.w_PRCODVAR;
          ,PRKEYVAR = this.w_PRKEYVAR;
          ,PRKEYSAL = this.w_PRKEYSAL;
          ,PRSAFELT = this.w_PRSAFELT;
          ,PRCOERSS = this.w_PRCOERSS;
          ,PRGIOCOP = this.w_PRGIOCOP;
          ,PRPUNLOT = this.w_PRPUNLOT;
          ,PRLISCOS = this.w_PRLISCOS;
          ,PUUTEELA = this.w_PUUTEELA;
          ,PRESECOS = this.w_PRESECOS;
          ,PRINVCOS = this.w_PRINVCOS;
          ,PRCSLAVO = this.w_PRCSLAVO;
          ,PRCSMATE = this.w_PRCSMATE;
          ,PRCSSPGE = this.w_PRCSSPGE;
          ,PRLOTSTD = this.w_PRLOTSTD;
          ,PRDATCOS = this.w_PRDATCOS;
          ,PRCULAVO = this.w_PRCULAVO;
          ,PRCUMATE = this.w_PRCUMATE;
          ,PRCUSPGE = this.w_PRCUSPGE;
          ,PRLOTULT = this.w_PRLOTULT;
          ,PRDATULT = this.w_PRDATULT;
          ,PRCMMATE = this.w_PRCMMATE;
          ,PRCMLAVO = this.w_PRCMLAVO;
          ,PRCMSPGE = this.w_PRCMSPGE;
          ,PRLOTMED = this.w_PRLOTMED;
          ,PRDATMED = this.w_PRDATMED;
          ,PRCLLAVO = this.w_PRCLLAVO;
          ,PRCLMATE = this.w_PRCLMATE;
          ,PRCLSPGE = this.w_PRCLSPGE;
          ,PRLOTLIS = this.w_PRLOTLIS;
          ,PRDATLIS = this.w_PRDATLIS;
          ,PRLOWLEV = this.w_PRLOWLEV;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_ARCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Fallita la scrittura di un dato articolo per valoriz.!'
      return
    endif
    return
  proc Try_05958778()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from PAR_RIOR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PRCODFOR,PRCODPRO,PRCOSSTA,PRDISMIN,PRGIOAPP,PRGIOINV,PRLEAMPS,PRLOTRIO,PRPERSCA,PRPUNRIO,PRQTAMIN,PRSCOMAX,PRSCOMIN,PRTIPCON"+;
        " from "+i_cTable+" PAR_RIOR where ";
            +"PRCODART = "+cp_ToStrODBC(this.w_ARCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PRCODFOR,PRCODPRO,PRCOSSTA,PRDISMIN,PRGIOAPP,PRGIOINV,PRLEAMPS,PRLOTRIO,PRPERSCA,PRPUNRIO,PRQTAMIN,PRSCOMAX,PRSCOMIN,PRTIPCON;
        from (i_cTable) where;
            PRCODART = this.w_ARCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ORCODFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
      this.w_ORCODPRO = NVL(cp_ToDate(_read_.PRCODPRO),cp_NullValue(_read_.PRCODPRO))
      this.w_ORCOSSTA = NVL(cp_ToDate(_read_.PRCOSSTA),cp_NullValue(_read_.PRCOSSTA))
      this.w_ORDISMIN = NVL(cp_ToDate(_read_.PRDISMIN),cp_NullValue(_read_.PRDISMIN))
      this.w_ORGIOAPP = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
      this.w_ORGIOINV = NVL(cp_ToDate(_read_.PRGIOINV),cp_NullValue(_read_.PRGIOINV))
      this.w_ORLEAMPS = NVL(cp_ToDate(_read_.PRLEAMPS),cp_NullValue(_read_.PRLEAMPS))
      this.w_ORLOTRIO = NVL(cp_ToDate(_read_.PRLOTRIO),cp_NullValue(_read_.PRLOTRIO))
      this.w_ORPERSCA = NVL(cp_ToDate(_read_.PRPERSCA),cp_NullValue(_read_.PRPERSCA))
      this.w_ORPUNRIO = NVL(cp_ToDate(_read_.PRPUNRIO),cp_NullValue(_read_.PRPUNRIO))
      this.w_ORQTAMIN = NVL(cp_ToDate(_read_.PRQTAMIN),cp_NullValue(_read_.PRQTAMIN))
      this.w_ORSCOMAX = NVL(cp_ToDate(_read_.PRSCOMAX),cp_NullValue(_read_.PRSCOMAX))
      this.w_ORSCOMIN = NVL(cp_ToDate(_read_.PRSCOMIN),cp_NullValue(_read_.PRSCOMIN))
      this.w_ORTIPCON = NVL(cp_ToDate(_read_.PRTIPCON),cp_NullValue(_read_.PRTIPCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PRSCOMIN = IIF(EMPTY(this.w_PRSCOMIN),this.w_ORSCOMIN,this.w_PRSCOMIN)
    this.w_PRSCOMAX = IIF(EMPTY(this.w_PRSCOMAX),this.w_ORSCOMAX,this.w_PRSCOMAX)
    this.w_PRDISMIN = IIF(EMPTY(this.w_PRDISMIN),this.w_ORDISMIN,this.w_PRDISMIN)
    this.w_PRQTAMIN = IIF(EMPTY(this.w_PRQTAMIN),this.w_ORQTAMIN,this.w_PRQTAMIN)
    this.w_PRLOTRIO = IIF(EMPTY(this.w_PRLOTRIO),this.w_ORLOTRIO,this.w_PRLOTRIO)
    this.w_PRPUNRIO = IIF(EMPTY(this.w_PRPUNRIO),this.w_ORPUNRIO,this.w_PRPUNRIO)
    this.w_PRGIOAPP = IIF(EMPTY(this.w_PRGIOAPP),this.w_ORGIOAPP,this.w_PRGIOAPP)
    this.w_PRGIOINV = IIF(EMPTY(this.w_PRGIOINV),this.w_ORGIOINV,this.w_PRGIOINV)
    this.w_PRCOSSTA = IIF(EMPTY(this.w_PRCOSSTA),this.w_ORCOSSTA,this.w_PRCOSSTA)
    this.w_PRTIPCON = IIF(EMPTY(this.w_PRTIPCON),this.w_ORTIPCON,this.w_PRTIPCON)
    this.w_PRCODPRO = IIF(EMPTY(this.w_PRCODPRO),this.w_ORCODPRO,this.w_PRCODPRO)
    this.w_PRCODFOR = IIF(EMPTY(this.w_PRCODFOR),this.w_ORCODFOR,this.w_PRCODFOR)
    this.w_PRCOEFLT = IIF(EMPTY(this.w_PRCOEFLT),this.w_ORCOEFLT,this.w_PRCOEFLT)
    this.w_PRLEAMPS = IIF(EMPTY(this.w_PRLEAMPS),this.w_ORLEAMPS,this.w_PRLEAMPS)
    this.w_PRPERSCA = IIF(EMPTY(this.w_PRPERSCA),this.w_ORPERSCA,this.w_PRPERSCA)
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCODFOR ="+cp_NullLink(cp_ToStrODBC(this.w_PRCODFOR),'PAR_RIOR','PRCODFOR');
      +",PRCODPRO ="+cp_NullLink(cp_ToStrODBC(this.w_PRCODPRO),'PAR_RIOR','PRCODPRO');
      +",PRCOSSTA ="+cp_NullLink(cp_ToStrODBC(this.w_PRCOSSTA),'PAR_RIOR','PRCOSSTA');
      +",PRDISMIN ="+cp_NullLink(cp_ToStrODBC(this.w_PRDISMIN),'PAR_RIOR','PRDISMIN');
      +",PRGIOAPP ="+cp_NullLink(cp_ToStrODBC(this.w_PRGIOAPP),'PAR_RIOR','PRGIOAPP');
      +",PRGIOINV ="+cp_NullLink(cp_ToStrODBC(this.w_PRGIOINV),'PAR_RIOR','PRGIOINV');
      +",PRLEAMPS ="+cp_NullLink(cp_ToStrODBC(this.w_PRLEAMPS),'PAR_RIOR','PRLEAMPS');
      +",PRLOTRIO ="+cp_NullLink(cp_ToStrODBC(this.w_PRLOTRIO),'PAR_RIOR','PRLOTRIO');
      +",PRPERSCA ="+cp_NullLink(cp_ToStrODBC(this.w_PRPERSCA),'PAR_RIOR','PRPERSCA');
      +",PRPUNRIO ="+cp_NullLink(cp_ToStrODBC(this.w_PRPUNRIO),'PAR_RIOR','PRPUNRIO');
      +",PRQTAMIN ="+cp_NullLink(cp_ToStrODBC(this.w_PRQTAMIN),'PAR_RIOR','PRQTAMIN');
      +",PRSCOMAX ="+cp_NullLink(cp_ToStrODBC(this.w_PRSCOMAX),'PAR_RIOR','PRSCOMAX');
      +",PRSCOMIN ="+cp_NullLink(cp_ToStrODBC(this.w_PRSCOMIN),'PAR_RIOR','PRSCOMIN');
      +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_PRTIPCON),'PAR_RIOR','PRTIPCON');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_ARCODART);
             )
    else
      update (i_cTable) set;
          PRCODFOR = this.w_PRCODFOR;
          ,PRCODPRO = this.w_PRCODPRO;
          ,PRCOSSTA = this.w_PRCOSSTA;
          ,PRDISMIN = this.w_PRDISMIN;
          ,PRGIOAPP = this.w_PRGIOAPP;
          ,PRGIOINV = this.w_PRGIOINV;
          ,PRLEAMPS = this.w_PRLEAMPS;
          ,PRLOTRIO = this.w_PRLOTRIO;
          ,PRPERSCA = this.w_PRPERSCA;
          ,PRPUNRIO = this.w_PRPUNRIO;
          ,PRQTAMIN = this.w_PRQTAMIN;
          ,PRSCOMAX = this.w_PRSCOMAX;
          ,PRSCOMIN = this.w_PRSCOMIN;
          ,PRTIPCON = this.w_PRTIPCON;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_ARCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Fallita la scrittura di un dato articolo per valoriz.!'
      return
    endif
    return
  proc Try_058C97A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            CACODICE = this.w_CACODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return
  proc Try_058E87C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if g_APPLICATION="ad hoc ENTERPRISE"
      GSMA_BCV(this,"I")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NOERR = .F.
      if not btrserr
        this.w_LICODART = this.w_CACODART
        this.w_OKINS = .T.
        this.w_KEYINS = this.w_KEYINS + 1
        this.w_NOERR = .T.
      endif
    else
      if this.w_CACODICE<>this.w_ARCODART
        * --- Inserisco solo codice di ricerca particolare poich� la 
        *     insert relativa al codice = al codice articolo ha caratteristiche diverse
        * --- Insert into KEY_ARTI
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.KEY_ARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CA__TIPO"+",CACOCOL3"+",CACODART"+",CACODCON"+",CACODICE"+",CADESART"+",CADESSUP"+",CADESVO3"+",CADIMAL3"+",CADIMLA3"+",CADIMLU3"+",CADTINVA"+",CADTOBSO"+",CAFLCON3"+",CAFLIMBA"+",CAFLSTAM"+",CALENSCF"+",CAMOLTIP"+",CAOPERAT"+",CAPESLO3"+",CAPESNE3"+",CAPUBWEB"+",CAPZCON3"+",CATIPBAR"+",CATIPCO3"+",CATIPCON"+",CATIPMA3"+",CATPCON3"+",CAUMDIM3"+",CAUMVOL3"+",CAUNIMIS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CA__TIPO),'KEY_ARTI','CA__TIPO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CACOCOL3),'KEY_ARTI','CACOCOL3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CACODART),'KEY_ARTI','CACODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CACODCON),'KEY_ARTI','CACODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CACODICE),'KEY_ARTI','CACODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CADESART),'KEY_ARTI','CADESART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CADESSUP),'KEY_ARTI','CADESSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CADESVO3),'KEY_ARTI','CADESVO3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CADIMAL3),'KEY_ARTI','CADIMAL3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CADIMLA3),'KEY_ARTI','CADIMLA3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CADIMLU3),'KEY_ARTI','CADIMLU3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CADTINVA),'KEY_ARTI','CADTINVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CADTOBSO),'KEY_ARTI','CADTOBSO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLCON3),'KEY_ARTI','CAFLCON3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLIMBA),'KEY_ARTI','CAFLIMBA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAM),'KEY_ARTI','CAFLSTAM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CALENSCF),'KEY_ARTI','CALENSCF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAMOLTIP),'KEY_ARTI','CAMOLTIP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAOPERAT),'KEY_ARTI','CAOPERAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAPESLO3),'KEY_ARTI','CAPESLO3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAPESNE3),'KEY_ARTI','CAPESNE3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAPUBWEB),'KEY_ARTI','CAPUBWEB');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAPZCON3),'KEY_ARTI','CAPZCON3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPBAR),'KEY_ARTI','CATIPBAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPCO3),'KEY_ARTI','CATIPCO3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPCON),'KEY_ARTI','CATIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPMA3),'KEY_ARTI','CATIPMA3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CATPCON3),'KEY_ARTI','CATPCON3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAUMDIM3),'KEY_ARTI','CAUMDIM3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAUMVOL3),'KEY_ARTI','CAUMVOL3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAUNIMIS),'KEY_ARTI','CAUNIMIS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CA__TIPO',this.w_CA__TIPO,'CACOCOL3',this.w_CACOCOL3,'CACODART',this.w_CACODART,'CACODCON',this.w_CACODCON,'CACODICE',this.w_CACODICE,'CADESART',this.w_CADESART,'CADESSUP',this.w_CADESSUP,'CADESVO3',this.w_CADESVO3,'CADIMAL3',this.w_CADIMAL3,'CADIMLA3',this.w_CADIMLA3,'CADIMLU3',this.w_CADIMLU3,'CADTINVA',this.w_CADTINVA)
          insert into (i_cTable) (CA__TIPO,CACOCOL3,CACODART,CACODCON,CACODICE,CADESART,CADESSUP,CADESVO3,CADIMAL3,CADIMLA3,CADIMLU3,CADTINVA,CADTOBSO,CAFLCON3,CAFLIMBA,CAFLSTAM,CALENSCF,CAMOLTIP,CAOPERAT,CAPESLO3,CAPESNE3,CAPUBWEB,CAPZCON3,CATIPBAR,CATIPCO3,CATIPCON,CATIPMA3,CATPCON3,CAUMDIM3,CAUMVOL3,CAUNIMIS &i_ccchkf. );
             values (;
               this.w_CA__TIPO;
               ,this.w_CACOCOL3;
               ,this.w_CACODART;
               ,this.w_CACODCON;
               ,this.w_CACODICE;
               ,this.w_CADESART;
               ,this.w_CADESSUP;
               ,this.w_CADESVO3;
               ,this.w_CADIMAL3;
               ,this.w_CADIMLA3;
               ,this.w_CADIMLU3;
               ,this.w_CADTINVA;
               ,this.w_CADTOBSO;
               ,this.w_CAFLCON3;
               ,this.w_CAFLIMBA;
               ,this.w_CAFLSTAM;
               ,this.w_CALENSCF;
               ,this.w_CAMOLTIP;
               ,this.w_CAOPERAT;
               ,this.w_CAPESLO3;
               ,this.w_CAPESNE3;
               ,this.w_CAPUBWEB;
               ,this.w_CAPZCON3;
               ,this.w_CATIPBAR;
               ,this.w_CATIPCO3;
               ,this.w_CATIPCON;
               ,this.w_CATIPMA3;
               ,this.w_CATPCON3;
               ,this.w_CAUMDIM3;
               ,this.w_CAUMVOL3;
               ,this.w_CAUNIMIS;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Fallito inserimento di un nuovo codice di ricerca!'
          return
        endif
        this.w_LICODART = this.w_CACODART
        this.w_OKINS = .T.
        this.w_KEYINS = this.w_KEYINS + 1
      endif
    endif
    return
  proc Try_058CA910()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- I 5 campi (CACODICE, CADESART, CADESSUP, CACODART, CAUNIMIS) della tabella KEY_ARTI
    *     sono trattati come se ic codice di ricerca fosse un articolo. Per l'utente questo � un Articolo.
    if g_APPLICATION<>"ad hoc ENTERPRISE"
      * --- Insert into KEY_ARTI
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.KEY_ARTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CACODART"+",CACODICE"+",CADESART"+",CADESSUP"+",CA__TIPO"+",CACOCOL3"+",CACODCON"+",CADESVO3"+",CADIMAL3"+",CADIMLA3"+",CADIMLU3"+",CADTINVA"+",CADTOBSO"+",CAFLCON3"+",CAFLSTAM"+",CAMOLTIP"+",CAOPERAT"+",CAPESLO3"+",CAPESNE3"+",CAPZCON3"+",CATIPBAR"+",CATIPCON"+",CATIPMA3"+",CATPCON3"+",CAUMDIM3"+",CAUMVOL3"+",CAUNIMIS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'KEY_ARTI','CACODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'KEY_ARTI','CACODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'KEY_ARTI','CADESART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'KEY_ARTI','CADESSUP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CA__TIPO1),'KEY_ARTI','CA__TIPO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CACOCOL3),'KEY_ARTI','CACOCOL3');
        +","+cp_NullLink(cp_ToStrODBC(Space(15)),'KEY_ARTI','CACODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CADESVO3),'KEY_ARTI','CADESVO3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CADIMAL3),'KEY_ARTI','CADIMAL3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CADIMLA3),'KEY_ARTI','CADIMLA3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CADIMLU3),'KEY_ARTI','CADIMLU3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CADTINVA),'KEY_ARTI','CADTINVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CADTOBSO),'KEY_ARTI','CADTOBSO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLCON3),'KEY_ARTI','CAFLCON3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAM),'KEY_ARTI','CAFLSTAM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAMOLTIP1),'KEY_ARTI','CAMOLTIP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAOPERAT1),'KEY_ARTI','CAOPERAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAPESLO3),'KEY_ARTI','CAPESLO3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAPESNE3),'KEY_ARTI','CAPESNE3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAPZCON3),'KEY_ARTI','CAPZCON3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPBAR1),'KEY_ARTI','CATIPBAR');
        +","+cp_NullLink(cp_ToStrODBC("R"),'KEY_ARTI','CATIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPMA3),'KEY_ARTI','CATIPMA3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CATPCON3),'KEY_ARTI','CATPCON3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAUMDIM3),'KEY_ARTI','CAUMDIM3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAUMVOL3),'KEY_ARTI','CAUMVOL3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAUNIMIS),'KEY_ARTI','CAUNIMIS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CACODART',this.w_ARCODART,'CACODICE',this.w_ARCODART,'CADESART',this.w_ARDESART,'CADESSUP',this.w_ARDESSUP,'CA__TIPO',this.w_CA__TIPO1,'CACOCOL3',this.w_CACOCOL3,'CACODCON',Space(15),'CADESVO3',this.w_CADESVO3,'CADIMAL3',this.w_CADIMAL3,'CADIMLA3',this.w_CADIMLA3,'CADIMLU3',this.w_CADIMLU3,'CADTINVA',this.w_CADTINVA)
        insert into (i_cTable) (CACODART,CACODICE,CADESART,CADESSUP,CA__TIPO,CACOCOL3,CACODCON,CADESVO3,CADIMAL3,CADIMLA3,CADIMLU3,CADTINVA,CADTOBSO,CAFLCON3,CAFLSTAM,CAMOLTIP,CAOPERAT,CAPESLO3,CAPESNE3,CAPZCON3,CATIPBAR,CATIPCON,CATIPMA3,CATPCON3,CAUMDIM3,CAUMVOL3,CAUNIMIS &i_ccchkf. );
           values (;
             this.w_ARCODART;
             ,this.w_ARCODART;
             ,this.w_ARDESART;
             ,this.w_ARDESSUP;
             ,this.w_CA__TIPO1;
             ,this.w_CACOCOL3;
             ,Space(15);
             ,this.w_CADESVO3;
             ,this.w_CADIMAL3;
             ,this.w_CADIMLA3;
             ,this.w_CADIMLU3;
             ,this.w_CADTINVA;
             ,this.w_CADTOBSO;
             ,this.w_CAFLCON3;
             ,this.w_CAFLSTAM;
             ,this.w_CAMOLTIP1;
             ,this.w_CAOPERAT1;
             ,this.w_CAPESLO3;
             ,this.w_CAPESNE3;
             ,this.w_CAPZCON3;
             ,this.w_CATIPBAR1;
             ,"R";
             ,this.w_CATIPMA3;
             ,this.w_CATPCON3;
             ,this.w_CAUMDIM3;
             ,this.w_CAUMVOL3;
             ,this.w_CAUNIMIS;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Fallito inserimento di un nuovo codice di ricerca!'
        return
      endif
    endif
    this.w_KEYINS = this.w_KEYINS + 1
    this.w_OKINSART = .T.
    return
  proc Try_058BF9C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if g_APPLICATION="ad hoc ENTERPRISE"
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              CACODICE = this.w_CACODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_OACODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        this.w_OADESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
        this.w_OADESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
        this.w_OATIPCON = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
        this.w_OACODCON = NVL(cp_ToDate(_read_.CACODCON),cp_NullValue(_read_.CACODCON))
        this.w_OA__TIPO = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
        this.w_OATIPBAR = NVL(cp_ToDate(_read_.CATIPBAR),cp_NullValue(_read_.CATIPBAR))
        this.w_OAFLSTAM = NVL(cp_ToDate(_read_.CAFLSTAM),cp_NullValue(_read_.CAFLSTAM))
        this.w_OAUNIMIS = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
        this.w_OAOPERAT = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
        this.w_OAMOLTIP = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
        this.w_OADTINVA = NVL(cp_ToDate(_read_.CADTINVA),cp_NullValue(_read_.CADTINVA))
        this.w_OADTOBSO = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
        this.w_OATIPMA3 = NVL(cp_ToDate(_read_.CATIPMA3),cp_NullValue(_read_.CATIPMA3))
        this.w_OAPESNE3 = NVL(cp_ToDate(_read_.CAPESNE3),cp_NullValue(_read_.CAPESNE3))
        this.w_OAPESLO3 = NVL(cp_ToDate(_read_.CAPESLO3),cp_NullValue(_read_.CAPESLO3))
        this.w_OADESVO3 = NVL(cp_ToDate(_read_.CADESVO3),cp_NullValue(_read_.CADESVO3))
        this.w_OAUMVOL3 = NVL(cp_ToDate(_read_.CAUMVOL3),cp_NullValue(_read_.CAUMVOL3))
        this.w_OATPCON3 = NVL(cp_ToDate(_read_.CATPCON3),cp_NullValue(_read_.CATPCON3))
        this.w_OAPZCON3 = NVL(cp_ToDate(_read_.CAPZCON3),cp_NullValue(_read_.CAPZCON3))
        this.w_OACOCOL3 = NVL(cp_ToDate(_read_.CACOCOL3),cp_NullValue(_read_.CACOCOL3))
        this.w_OADIMLU3 = NVL(cp_ToDate(_read_.CADIMLU3),cp_NullValue(_read_.CADIMLU3))
        this.w_OADIMLA3 = NVL(cp_ToDate(_read_.CADIMLA3),cp_NullValue(_read_.CADIMLA3))
        this.w_OADIMAL3 = NVL(cp_ToDate(_read_.CADIMAL3),cp_NullValue(_read_.CADIMAL3))
        this.w_OAUMDIM3 = NVL(cp_ToDate(_read_.CAUMDIM3),cp_NullValue(_read_.CAUMDIM3))
        this.w_OAFLCON3 = NVL(cp_ToDate(_read_.CAFLCON3),cp_NullValue(_read_.CAFLCON3))
        this.w_OACODVAR = NVL(cp_ToDate(_read_.CACODVAR),cp_NullValue(_read_.CACODVAR))
        this.w_OACAUZI3 = NVL(cp_ToDate(_read_.CACAUZI3),cp_NullValue(_read_.CACAUZI3))
        this.w_OAIMBRE3 = NVL(cp_ToDate(_read_.CAIMBRE3),cp_NullValue(_read_.CAIMBRE3))
        this.w_OAIMBCA3 = NVL(cp_ToDate(_read_.CAIMBCA3),cp_NullValue(_read_.CAIMBCA3))
        this.w_OACONFIG = NVL(cp_ToDate(_read_.CACONFIG),cp_NullValue(_read_.CACONFIG))
        this.w_OACONATO = NVL(cp_ToDate(_read_.CACONATO),cp_NullValue(_read_.CACONATO))
        this.w_OACONCAR = NVL(cp_ToDate(_read_.CACONCAR),cp_NullValue(_read_.CACONCAR))
        this.w_OAGESCAR = NVL(cp_ToDate(_read_.CAGESCAR),cp_NullValue(_read_.CAGESCAR))
        this.w_OARIFCLA = NVL(cp_ToDate(_read_.CARIFCLA),cp_NullValue(_read_.CARIFCLA))
        this.w_OAKEYSAL = NVL(cp_ToDate(_read_.CAKEYSAL),cp_NullValue(_read_.CAKEYSAL))
        this.w_OACODCEN = NVL(cp_ToDate(_read_.CACODCEN),cp_NullValue(_read_.CACODCEN))
        this.w_OAFLUMVA = NVL(cp_ToDate(_read_.CAFLUMVA),cp_NullValue(_read_.CAFLUMVA))
        this.w_OAPREZUM = NVL(cp_ToDate(_read_.CAPREZUM),cp_NullValue(_read_.CAPREZUM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              CACODICE = this.w_CACODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_OA__TIPO = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
        this.w_OACOCOL3 = NVL(cp_ToDate(_read_.CACOCOL3),cp_NullValue(_read_.CACOCOL3))
        this.w_OACODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        this.w_OACODCON = NVL(cp_ToDate(_read_.CACODCON),cp_NullValue(_read_.CACODCON))
        this.w_OACODVAR = NVL(cp_ToDate(_read_.CACODVAR),cp_NullValue(_read_.CACODVAR))
        this.w_OADESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
        this.w_OADESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
        this.w_OADESVO3 = NVL(cp_ToDate(_read_.CADESVO3),cp_NullValue(_read_.CADESVO3))
        this.w_OADIMAL3 = NVL(cp_ToDate(_read_.CADIMAL3),cp_NullValue(_read_.CADIMAL3))
        this.w_OADIMLA3 = NVL(cp_ToDate(_read_.CADIMLA3),cp_NullValue(_read_.CADIMLA3))
        this.w_OADIMLU3 = NVL(cp_ToDate(_read_.CADIMLU3),cp_NullValue(_read_.CADIMLU3))
        this.w_OADTINVA = NVL(cp_ToDate(_read_.CADTINVA),cp_NullValue(_read_.CADTINVA))
        this.w_OADTOBSO = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
        this.w_OAFLCON3 = NVL(cp_ToDate(_read_.CAFLCON3),cp_NullValue(_read_.CAFLCON3))
        this.w_OAFLIMBA = NVL(cp_ToDate(_read_.CAFLIMBA),cp_NullValue(_read_.CAFLIMBA))
        this.w_OAFLSTAM = NVL(cp_ToDate(_read_.CAFLSTAM),cp_NullValue(_read_.CAFLSTAM))
        this.w_OALENSCF = NVL(cp_ToDate(_read_.CALENSCF),cp_NullValue(_read_.CALENSCF))
        this.w_OAMOLTIP = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
        this.w_OAOPERAT = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
        this.w_OAPESLO3 = NVL(cp_ToDate(_read_.CAPESLO3),cp_NullValue(_read_.CAPESLO3))
        this.w_OAPESNE3 = NVL(cp_ToDate(_read_.CAPESNE3),cp_NullValue(_read_.CAPESNE3))
        this.w_OAPUBWEB = NVL(cp_ToDate(_read_.CAPUBWEB),cp_NullValue(_read_.CAPUBWEB))
        this.w_OAPZCON3 = NVL(cp_ToDate(_read_.CAPZCON3),cp_NullValue(_read_.CAPZCON3))
        this.w_OATIPBAR = NVL(cp_ToDate(_read_.CATIPBAR),cp_NullValue(_read_.CATIPBAR))
        this.w_CATIPCO3 = NVL(cp_ToDate(_read_.CATIPCO3),cp_NullValue(_read_.CATIPCO3))
        this.w_OATIPCON = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
        this.w_OATIPMA3 = NVL(cp_ToDate(_read_.CATIPMA3),cp_NullValue(_read_.CATIPMA3))
        this.w_OATPCON3 = NVL(cp_ToDate(_read_.CATPCON3),cp_NullValue(_read_.CATPCON3))
        this.w_OAUMDIM3 = NVL(cp_ToDate(_read_.CAUMDIM3),cp_NullValue(_read_.CAUMDIM3))
        this.w_OAUMVOL3 = NVL(cp_ToDate(_read_.CAUMVOL3),cp_NullValue(_read_.CAUMVOL3))
        this.w_OAUNIMIS = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_CADESART = IIF(EMPTY(this.w_CADESART),this.w_OADESART,this.w_CADESART)
    this.w_CADESSUP = IIF(EMPTY(this.w_CADESSUP),this.w_OADESSUP,this.w_CADESSUP)
    this.w_CACODART = IIF(EMPTY(this.w_CACODART),this.w_OACODART,this.w_CACODART)
    this.w_CATIPCON = IIF(EMPTY(this.w_CATIPCON),this.w_OATIPCON,this.w_CATIPCON)
    this.w_CACODCON = IIF(!this.w_CATIPCON$"CF", SPACE(15), IIF(EMPTY(this.w_CACODCON), this.w_OACODCON, this.w_CACODCON))
    this.w_CA__TIPO = IIF(EMPTY(this.w_CA__TIPO),this.w_OA__TIPO,this.w_CA__TIPO)
    this.w_CATIPBAR = IIF(EMPTY(this.w_CATIPBAR),this.w_OATIPBAR,this.w_CATIPBAR)
    this.w_CAFLSTAM = IIF(EMPTY(this.w_CAFLSTAM),this.w_OAFLSTAM,this.w_CAFLSTAM)
    this.w_CAUNIMIS = IIF(EMPTY(this.w_CAUNIMIS),this.w_OAUNIMIS,this.w_CAUNIMIS)
    this.w_CAOPERAT = IIF(EMPTY(this.w_CAOPERAT),this.w_OAOPERAT,this.w_CAOPERAT)
    this.w_CAMOLTIP = IIF(EMPTY(this.w_CAMOLTIP),this.w_OAMOLTIP,this.w_CAMOLTIP)
    this.w_CADTINVA = IIF(EMPTY(this.w_CADTINVA),this.w_OADTINVA,this.w_CADTINVA)
    this.w_CADTOBSO = IIF(EMPTY(this.w_CADTOBSO),this.w_OADTOBSO,this.w_CADTOBSO)
    this.w_CATIPMA3 = IIF(EMPTY(this.w_CATIPMA3),this.w_OATIPMA3,this.w_CATIPMA3)
    this.w_CAPESNE3 = IIF(EMPTY(this.w_CAPESNE3),this.w_OAPESNE3,this.w_CAPESNE3)
    this.w_CAPESLO3 = IIF(EMPTY(this.w_CAPESLO3),this.w_OAPESLO3,this.w_CAPESLO3)
    this.w_CADESVO3 = IIF(EMPTY(this.w_CADESVO3),this.w_OADESVO3,this.w_CADESVO3)
    this.w_CAUMVOL3 = IIF(EMPTY(this.w_CAUMVOL3),this.w_OAUMVOL3,this.w_CAUMVOL3)
    this.w_CATPCON3 = IIF(EMPTY(this.w_CATPCON3),this.w_OATPCON3,this.w_CATPCON3)
    this.w_CACOCOL3 = IIF(EMPTY(this.w_CACOCOL3),this.w_OACOCOL3,this.w_CACOCOL3)
    this.w_CADIMLU3 = IIF(EMPTY(this.w_CADIMLU3),this.w_OADIMLU3,this.w_CADIMLU3)
    this.w_CADIMLA3 = IIF(EMPTY(this.w_CADIMLA3),this.w_OADIMLA3,this.w_CADIMLA3)
    this.w_CADIMAL3 = IIF(EMPTY(this.w_CADIMAL3),this.w_OADIMAL3,this.w_CADIMAL3)
    this.w_CAUMDIM3 = IIF(EMPTY(this.w_CAUMDIM3),this.w_OAUMDIM3,this.w_CAUMDIM3)
    this.w_CAFLCON3 = IIF(EMPTY(this.w_CAFLCON3),this.w_OAFLCON3,this.w_CAFLCON3)
    this.w_CACODVAR = IIF(EMPTY(this.w_CACODVAR),this.w_OACODVAR,this.w_CACODVAR)
    this.w_CACAUZI3 = IIF(EMPTY(this.w_CACAUZI3),this.w_OACAUZI3,this.w_CACAUZI3)
    this.w_CAIMBRE3 = IIF(EMPTY(this.w_CAIMBRE3),this.w_OAIMBRE3,this.w_CAIMBRE3)
    this.w_CAIMBCA3 = IIF(EMPTY(this.w_CAIMBCA3),this.w_OAIMBCA3,this.w_CAIMBCA3)
    this.w_CACONFIG = IIF(EMPTY(this.w_CACONFIG),this.w_OACONFIG,this.w_CACONFIG)
    this.w_CACONATO = IIF(EMPTY(this.w_CACONATO),this.w_OACONATO,this.w_CACONATO)
    this.w_CACONCAR = IIF(EMPTY(this.w_CACONCAR),this.w_OACONCAR,this.w_CACONCAR)
    this.w_CAGESCAR = IIF(EMPTY(this.w_CAGESCAR),this.w_OAGESCAR,this.w_CAGESCAR)
    this.w_CARIFCLA = IIF(EMPTY(this.w_CARIFCLA),this.w_OARIFCLA,this.w_CARIFCLA)
    this.w_CAKEYSAL = IIF(EMPTY(this.w_CAKEYSAL),this.w_OAKEYSAL,this.w_CAKEYSAL)
    this.w_CACODCEN = IIF(EMPTY(this.w_CACODCEN),this.w_OACODCEN,this.w_CACODCEN)
    this.w_CAFLUMVA = IIF(EMPTY(this.w_CAFLUMVA),this.w_OAFLUMVA,this.w_CAFLUMVA)
    this.w_CAPREZUM = IIF(EMPTY(this.w_CAPREZUM),this.w_OAPREZUM,this.w_CAPREZUM)
    this.w_CAFLIMBA = IIF(EMPTY(this.w_CAFLIMBA),this.w_OAFLIMBA,this.w_CAFLIMBA)
    this.w_CALENSCF = IIF(EMPTY(this.w_CALENSCF),this.w_OALENSCF,this.w_CALENSCF)
    this.w_CAPUBWEB = IIF(EMPTY(this.w_CAPUBWEB),this.w_OAPUBWEB,this.w_CAPUBWEB)
    this.w_CAPZCON3 = IIF(EMPTY(this.w_CAPZCON3),this.w_OAPZCON3,this.w_CAPZCON3)
    this.w_CATIPCO3 = IIF(EMPTY(this.w_CATIPCO3),this.w_OATIPCO3,this.w_CATIPCO3)
    this.w_CARIFCLA = IIF(EMPTY(NVL(this.w_CARIFCLA, " ")),.NULL., this.w_CARIFCLA)
    if g_APPLICATION="ad hoc ENTERPRISE"
      * --- Write into KEY_ARTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CADESART ="+cp_NullLink(cp_ToStrODBC(this.w_CADESART),'KEY_ARTI','CADESART');
        +",CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_CADESSUP),'KEY_ARTI','CADESSUP');
        +",CACODCON ="+cp_NullLink(cp_ToStrODBC(this.w_CACODCON),'KEY_ARTI','CACODCON');
        +",CATIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPCON),'KEY_ARTI','CATIPCON');
        +",CA__TIPO ="+cp_NullLink(cp_ToStrODBC(this.w_CA__TIPO),'KEY_ARTI','CA__TIPO');
        +",CATIPBAR ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPBAR),'KEY_ARTI','CATIPBAR');
        +",CAFLSTAM ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAM),'KEY_ARTI','CAFLSTAM');
        +",CAUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_CAUNIMIS),'KEY_ARTI','CAUNIMIS');
        +",CAOPERAT ="+cp_NullLink(cp_ToStrODBC(this.w_CAOPERAT),'KEY_ARTI','CAOPERAT');
        +",CAMOLTIP ="+cp_NullLink(cp_ToStrODBC(this.w_CAMOLTIP),'KEY_ARTI','CAMOLTIP');
        +",CADTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_CADTINVA),'KEY_ARTI','CADTINVA');
        +",CADTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_CADTOBSO),'KEY_ARTI','CADTOBSO');
        +",CATIPMA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPMA3),'KEY_ARTI','CATIPMA3');
        +",CAPESNE3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPESNE3),'KEY_ARTI','CAPESNE3');
        +",CAPESLO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPESLO3),'KEY_ARTI','CAPESLO3');
        +",CADESVO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADESVO3),'KEY_ARTI','CADESVO3');
        +",CAUMVOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAUMVOL3),'KEY_ARTI','CAUMVOL3');
        +",CATPCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATPCON3),'KEY_ARTI','CATPCON3');
        +",CAPZCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPZCON3),'KEY_ARTI','CAPZCON3');
        +",CACOCOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CACOCOL3),'KEY_ARTI','CACOCOL3');
        +",CADIMLU3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMLU3),'KEY_ARTI','CADIMLU3');
        +",CADIMLA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMLA3),'KEY_ARTI','CADIMLA3');
        +",CADIMAL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMAL3),'KEY_ARTI','CADIMAL3');
        +",CAUMDIM3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAUMDIM3),'KEY_ARTI','CAUMDIM3');
        +",CAFLCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLCON3),'KEY_ARTI','CAFLCON3');
        +",CACAUZI3 ="+cp_NullLink(cp_ToStrODBC(this.w_CACAUZI3),'KEY_ARTI','CACAUZI3');
        +",CAIMBRE3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAIMBRE3),'KEY_ARTI','CAIMBRE3');
        +",CAIMBCA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAIMBCA3),'KEY_ARTI','CAIMBCA3');
        +",CACONFIG ="+cp_NullLink(cp_ToStrODBC(this.w_CACONFIG),'KEY_ARTI','CACONFIG');
        +",CACONATO ="+cp_NullLink(cp_ToStrODBC(this.w_CACONATO),'KEY_ARTI','CACONATO');
        +",CACONCAR ="+cp_NullLink(cp_ToStrODBC(this.w_CACONCAR),'KEY_ARTI','CACONCAR');
        +",CAGESCAR ="+cp_NullLink(cp_ToStrODBC(this.w_CAGESCAR),'KEY_ARTI','CAGESCAR');
        +",CARIFCLA ="+cp_NullLink(cp_ToStrODBC(this.w_CARIFCLA),'KEY_ARTI','CARIFCLA');
        +",CACODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_CACODCEN),'KEY_ARTI','CACODCEN');
        +",CAFLUMVA ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLUMVA),'KEY_ARTI','CAFLUMVA');
        +",CAPREZUM ="+cp_NullLink(cp_ToStrODBC(this.w_CAPREZUM),'KEY_ARTI','CAPREZUM');
            +i_ccchkf ;
        +" where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
               )
      else
        update (i_cTable) set;
            CADESART = this.w_CADESART;
            ,CADESSUP = this.w_CADESSUP;
            ,CACODCON = this.w_CACODCON;
            ,CATIPCON = this.w_CATIPCON;
            ,CA__TIPO = this.w_CA__TIPO;
            ,CATIPBAR = this.w_CATIPBAR;
            ,CAFLSTAM = this.w_CAFLSTAM;
            ,CAUNIMIS = this.w_CAUNIMIS;
            ,CAOPERAT = this.w_CAOPERAT;
            ,CAMOLTIP = this.w_CAMOLTIP;
            ,CADTINVA = this.w_CADTINVA;
            ,CADTOBSO = this.w_CADTOBSO;
            ,CATIPMA3 = this.w_CATIPMA3;
            ,CAPESNE3 = this.w_CAPESNE3;
            ,CAPESLO3 = this.w_CAPESLO3;
            ,CADESVO3 = this.w_CADESVO3;
            ,CAUMVOL3 = this.w_CAUMVOL3;
            ,CATPCON3 = this.w_CATPCON3;
            ,CAPZCON3 = this.w_CAPZCON3;
            ,CACOCOL3 = this.w_CACOCOL3;
            ,CADIMLU3 = this.w_CADIMLU3;
            ,CADIMLA3 = this.w_CADIMLA3;
            ,CADIMAL3 = this.w_CADIMAL3;
            ,CAUMDIM3 = this.w_CAUMDIM3;
            ,CAFLCON3 = this.w_CAFLCON3;
            ,CACAUZI3 = this.w_CACAUZI3;
            ,CAIMBRE3 = this.w_CAIMBRE3;
            ,CAIMBCA3 = this.w_CAIMBCA3;
            ,CACONFIG = this.w_CACONFIG;
            ,CACONATO = this.w_CACONATO;
            ,CACONCAR = this.w_CACONCAR;
            ,CAGESCAR = this.w_CAGESCAR;
            ,CARIFCLA = this.w_CARIFCLA;
            ,CACODCEN = this.w_CACODCEN;
            ,CAFLUMVA = this.w_CAFLUMVA;
            ,CAPREZUM = this.w_CAPREZUM;
            &i_ccchkf. ;
         where;
            CACODICE = this.w_CACODICE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Fallita la scrittura di un codice di ricerca!'
        return
      endif
    else
      * --- Write into KEY_ARTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CA__TIPO ="+cp_NullLink(cp_ToStrODBC(this.w_CA__TIPO),'KEY_ARTI','CA__TIPO');
        +",CACOCOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CACOCOL3),'KEY_ARTI','CACOCOL3');
        +",CACODCON ="+cp_NullLink(cp_ToStrODBC(this.w_CACODCON),'KEY_ARTI','CACODCON');
        +",CATIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPCON),'KEY_ARTI','CATIPCON');
        +",CADESART ="+cp_NullLink(cp_ToStrODBC(this.w_CADESART),'KEY_ARTI','CADESART');
        +",CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_CADESSUP),'KEY_ARTI','CADESSUP');
        +",CADESVO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADESVO3),'KEY_ARTI','CADESVO3');
        +",CADIMAL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMAL3),'KEY_ARTI','CADIMAL3');
        +",CADIMLA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMLA3),'KEY_ARTI','CADIMLA3');
        +",CADIMLU3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMLU3),'KEY_ARTI','CADIMLU3');
        +",CADTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_CADTINVA),'KEY_ARTI','CADTINVA');
        +",CADTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_CADTOBSO),'KEY_ARTI','CADTOBSO');
        +",CAFLCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLCON3),'KEY_ARTI','CAFLCON3');
        +",CAFLIMBA ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLIMBA),'KEY_ARTI','CAFLIMBA');
        +",CAFLSTAM ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAM),'KEY_ARTI','CAFLSTAM');
        +",CALENSCF ="+cp_NullLink(cp_ToStrODBC(this.w_CALENSCF),'KEY_ARTI','CALENSCF');
        +",CAMOLTIP ="+cp_NullLink(cp_ToStrODBC(this.w_CAMOLTIP),'KEY_ARTI','CAMOLTIP');
        +",CAOPERAT ="+cp_NullLink(cp_ToStrODBC(this.w_CAOPERAT),'KEY_ARTI','CAOPERAT');
        +",CAPESLO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPESLO3),'KEY_ARTI','CAPESLO3');
        +",CAPESNE3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPESNE3),'KEY_ARTI','CAPESNE3');
        +",CAPUBWEB ="+cp_NullLink(cp_ToStrODBC(this.w_CAPUBWEB),'KEY_ARTI','CAPUBWEB');
        +",CAPZCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPZCON3),'KEY_ARTI','CAPZCON3');
        +",CATIPBAR ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPBAR),'KEY_ARTI','CATIPBAR');
        +",CATIPCO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPCO3),'KEY_ARTI','CATIPCO3');
        +",CATIPMA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPMA3),'KEY_ARTI','CATIPMA3');
        +",CATPCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATPCON3),'KEY_ARTI','CATPCON3');
        +",CAUMDIM3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAUMDIM3),'KEY_ARTI','CAUMDIM3');
        +",CAUMVOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAUMVOL3),'KEY_ARTI','CAUMVOL3');
        +",CAUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_CAUNIMIS),'KEY_ARTI','CAUNIMIS');
            +i_ccchkf ;
        +" where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
               )
      else
        update (i_cTable) set;
            CA__TIPO = this.w_CA__TIPO;
            ,CACOCOL3 = this.w_CACOCOL3;
            ,CACODCON = this.w_CACODCON;
            ,CATIPCON = this.w_CATIPCON;
            ,CADESART = this.w_CADESART;
            ,CADESSUP = this.w_CADESSUP;
            ,CADESVO3 = this.w_CADESVO3;
            ,CADIMAL3 = this.w_CADIMAL3;
            ,CADIMLA3 = this.w_CADIMLA3;
            ,CADIMLU3 = this.w_CADIMLU3;
            ,CADTINVA = this.w_CADTINVA;
            ,CADTOBSO = this.w_CADTOBSO;
            ,CAFLCON3 = this.w_CAFLCON3;
            ,CAFLIMBA = this.w_CAFLIMBA;
            ,CAFLSTAM = this.w_CAFLSTAM;
            ,CALENSCF = this.w_CALENSCF;
            ,CAMOLTIP = this.w_CAMOLTIP;
            ,CAOPERAT = this.w_CAOPERAT;
            ,CAPESLO3 = this.w_CAPESLO3;
            ,CAPESNE3 = this.w_CAPESNE3;
            ,CAPUBWEB = this.w_CAPUBWEB;
            ,CAPZCON3 = this.w_CAPZCON3;
            ,CATIPBAR = this.w_CATIPBAR;
            ,CATIPCO3 = this.w_CATIPCO3;
            ,CATIPMA3 = this.w_CATIPMA3;
            ,CATPCON3 = this.w_CATPCON3;
            ,CAUMDIM3 = this.w_CAUMDIM3;
            ,CAUMVOL3 = this.w_CAUMVOL3;
            ,CAUNIMIS = this.w_CAUNIMIS;
            &i_ccchkf. ;
         where;
            CACODICE = this.w_CACODICE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Fallita la scrittura di un codice di ricerca!'
        return
      endif
    endif
    if i_Rows>0
      this.w_AGGKEY = this.w_AGGKEY+1
    endif
    return
  proc Try_058899C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- I 5 campi (CACODICE, CADESART, CADESSUP, CACODART, CAUNIMIS) della tabella KEY_ARTI
    * --- sono trattati come se il codice di ricerca fosse un articolo. Per l'utente questo � un Articolo.
    if g_APPLICATION="ad hoc ENTERPRISE"
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_ARCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              CACODICE = this.w_ARCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_OACODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        this.w_OADESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
        this.w_OADESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
        this.w_OATIPCON = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
        this.w_OACODCON = NVL(cp_ToDate(_read_.CACODCON),cp_NullValue(_read_.CACODCON))
        this.w_OA__TIPO = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
        this.w_OATIPBAR = NVL(cp_ToDate(_read_.CATIPBAR),cp_NullValue(_read_.CATIPBAR))
        this.w_OAFLSTAM = NVL(cp_ToDate(_read_.CAFLSTAM),cp_NullValue(_read_.CAFLSTAM))
        this.w_OAUNIMIS = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
        this.w_OAOPERAT = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
        this.w_OAMOLTIP = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
        this.w_OADTINVA = NVL(cp_ToDate(_read_.CADTINVA),cp_NullValue(_read_.CADTINVA))
        this.w_OADTOBSO = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
        this.w_OATIPMA3 = NVL(cp_ToDate(_read_.CATIPMA3),cp_NullValue(_read_.CATIPMA3))
        this.w_OAPESNE3 = NVL(cp_ToDate(_read_.CAPESNE3),cp_NullValue(_read_.CAPESNE3))
        this.w_OAPESLO3 = NVL(cp_ToDate(_read_.CAPESLO3),cp_NullValue(_read_.CAPESLO3))
        this.w_OADESVO3 = NVL(cp_ToDate(_read_.CADESVO3),cp_NullValue(_read_.CADESVO3))
        this.w_OAUMVOL3 = NVL(cp_ToDate(_read_.CAUMVOL3),cp_NullValue(_read_.CAUMVOL3))
        this.w_OATPCON3 = NVL(cp_ToDate(_read_.CATPCON3),cp_NullValue(_read_.CATPCON3))
        this.w_OAPZCON3 = NVL(cp_ToDate(_read_.CAPZCON3),cp_NullValue(_read_.CAPZCON3))
        this.w_OACOCOL3 = NVL(cp_ToDate(_read_.CACOCOL3),cp_NullValue(_read_.CACOCOL3))
        this.w_OADIMLU3 = NVL(cp_ToDate(_read_.CADIMLU3),cp_NullValue(_read_.CADIMLU3))
        this.w_OADIMLA3 = NVL(cp_ToDate(_read_.CADIMLA3),cp_NullValue(_read_.CADIMLA3))
        this.w_OADIMAL3 = NVL(cp_ToDate(_read_.CADIMAL3),cp_NullValue(_read_.CADIMAL3))
        this.w_OAUMDIM3 = NVL(cp_ToDate(_read_.CAUMDIM3),cp_NullValue(_read_.CAUMDIM3))
        this.w_OAFLCON3 = NVL(cp_ToDate(_read_.CAFLCON3),cp_NullValue(_read_.CAFLCON3))
        this.w_OACODVAR = NVL(cp_ToDate(_read_.CACODVAR),cp_NullValue(_read_.CACODVAR))
        this.w_OACAUZI3 = NVL(cp_ToDate(_read_.CACAUZI3),cp_NullValue(_read_.CACAUZI3))
        this.w_OAIMBRE3 = NVL(cp_ToDate(_read_.CAIMBRE3),cp_NullValue(_read_.CAIMBRE3))
        this.w_OAIMBCA3 = NVL(cp_ToDate(_read_.CAIMBCA3),cp_NullValue(_read_.CAIMBCA3))
        this.w_OACONFIG = NVL(cp_ToDate(_read_.CACONFIG),cp_NullValue(_read_.CACONFIG))
        this.w_OACONATO = NVL(cp_ToDate(_read_.CACONATO),cp_NullValue(_read_.CACONATO))
        this.w_OACONCAR = NVL(cp_ToDate(_read_.CACONCAR),cp_NullValue(_read_.CACONCAR))
        this.w_OAGESCAR = NVL(cp_ToDate(_read_.CAGESCAR),cp_NullValue(_read_.CAGESCAR))
        this.w_OARIFCLA = NVL(cp_ToDate(_read_.CARIFCLA),cp_NullValue(_read_.CARIFCLA))
        this.w_OAKEYSAL = NVL(cp_ToDate(_read_.CAKEYSAL),cp_NullValue(_read_.CAKEYSAL))
        this.w_OACODCEN = NVL(cp_ToDate(_read_.CACODCEN),cp_NullValue(_read_.CACODCEN))
        this.w_OAFLUMVA = NVL(cp_ToDate(_read_.CAFLUMVA),cp_NullValue(_read_.CAFLUMVA))
        this.w_OAPREZUM = NVL(cp_ToDate(_read_.CAPREZUM),cp_NullValue(_read_.CAPREZUM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART,CADESART,CADESSUP,CATIPCON,CACODCON,CA__TIPO,CATIPBAR,CAFLSTAM,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTINVA,CADTOBSO,CATIPMA3,CAPESNE3,CAPESLO3,CADESVO3,CAUMVOL3,CATPCON3,CAPZCON3,CACOCOL3,CADIMLU3,CADIMLA3,CADIMAL3,CAUMDIM3,CAFLCON3,CACODVAR"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_ARCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART,CADESART,CADESSUP,CATIPCON,CACODCON,CA__TIPO,CATIPBAR,CAFLSTAM,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTINVA,CADTOBSO,CATIPMA3,CAPESNE3,CAPESLO3,CADESVO3,CAUMVOL3,CATPCON3,CAPZCON3,CACOCOL3,CADIMLU3,CADIMLA3,CADIMAL3,CAUMDIM3,CAFLCON3,CACODVAR;
          from (i_cTable) where;
              CACODICE = this.w_ARCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_OACODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        this.w_OADESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
        this.w_OADESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
        this.w_OATIPCON = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
        this.w_OACODCON = NVL(cp_ToDate(_read_.CACODCON),cp_NullValue(_read_.CACODCON))
        this.w_OA__TIPO = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
        this.w_OATIPBAR = NVL(cp_ToDate(_read_.CATIPBAR),cp_NullValue(_read_.CATIPBAR))
        this.w_OAFLSTAM = NVL(cp_ToDate(_read_.CAFLSTAM),cp_NullValue(_read_.CAFLSTAM))
        this.w_OAUNIMIS = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
        this.w_OAOPERAT = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
        this.w_OAMOLTIP = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
        this.w_OADTINVA = NVL(cp_ToDate(_read_.CADTINVA),cp_NullValue(_read_.CADTINVA))
        this.w_OADTOBSO = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
        this.w_OATIPMA3 = NVL(cp_ToDate(_read_.CATIPMA3),cp_NullValue(_read_.CATIPMA3))
        this.w_OAPESNE3 = NVL(cp_ToDate(_read_.CAPESNE3),cp_NullValue(_read_.CAPESNE3))
        this.w_OAPESLO3 = NVL(cp_ToDate(_read_.CAPESLO3),cp_NullValue(_read_.CAPESLO3))
        this.w_OADESVO3 = NVL(cp_ToDate(_read_.CADESVO3),cp_NullValue(_read_.CADESVO3))
        this.w_OAUMVOL3 = NVL(cp_ToDate(_read_.CAUMVOL3),cp_NullValue(_read_.CAUMVOL3))
        this.w_OATPCON3 = NVL(cp_ToDate(_read_.CATPCON3),cp_NullValue(_read_.CATPCON3))
        this.w_OAPZCON3 = NVL(cp_ToDate(_read_.CAPZCON3),cp_NullValue(_read_.CAPZCON3))
        this.w_OACOCOL3 = NVL(cp_ToDate(_read_.CACOCOL3),cp_NullValue(_read_.CACOCOL3))
        this.w_OADIMLU3 = NVL(cp_ToDate(_read_.CADIMLU3),cp_NullValue(_read_.CADIMLU3))
        this.w_OADIMLA3 = NVL(cp_ToDate(_read_.CADIMLA3),cp_NullValue(_read_.CADIMLA3))
        this.w_OADIMAL3 = NVL(cp_ToDate(_read_.CADIMAL3),cp_NullValue(_read_.CADIMAL3))
        this.w_OAUMDIM3 = NVL(cp_ToDate(_read_.CAUMDIM3),cp_NullValue(_read_.CAUMDIM3))
        this.w_OAFLCON3 = NVL(cp_ToDate(_read_.CAFLCON3),cp_NullValue(_read_.CAFLCON3))
        this.w_OACODVAR = NVL(cp_ToDate(_read_.CACODVAR),cp_NullValue(_read_.CACODVAR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_ARDESART = IIF(EMPTY(this.w_ARDESART),this.w_OADESART,this.w_ARDESART)
    this.w_ARDESSUP = IIF(EMPTY(this.w_ARDESSUP),this.w_OADESSUP,this.w_ARDESSUP)
    this.w_CATIPCON = this.w_OATIPCON
    this.w_CACODCON = IIF(!this.w_OATIPCON$"CF", SPACE(15), IIF(EMPTY(this.w_CACODCON), this.w_OACODCON, this.w_CACODCON))
    this.w_CA__TIPO = IIF(EMPTY(this.w_CA__TIPO),this.w_OA__TIPO,this.w_CA__TIPO)
    this.w_CATIPBAR = IIF(EMPTY(this.w_CATIPBAR),this.w_OATIPBAR,this.w_CATIPBAR)
    this.w_CAFLSTAM = IIF(EMPTY(this.w_CAFLSTAM),this.w_OAFLSTAM,this.w_CAFLSTAM)
    this.w_CAUNIMIS = IIF(EMPTY(this.w_CAUNIMIS),this.w_OAUNIMIS,this.w_CAUNIMIS)
    this.w_CAOPERAT = IIF(EMPTY(this.w_CAOPERAT),this.w_OAOPERAT,this.w_CAOPERAT)
    this.w_CAMOLTIP = IIF(EMPTY(this.w_CAMOLTIP),this.w_OAMOLTIP,this.w_CAMOLTIP)
    this.w_CADTINVA = IIF(EMPTY(this.w_CADTINVA),this.w_OADTINVA,this.w_CADTINVA)
    this.w_CADTOBSO = IIF(EMPTY(this.w_CADTOBSO),this.w_OADTOBSO,this.w_CADTOBSO)
    this.w_CATIPMA3 = IIF(EMPTY(this.w_CATIPMA3),this.w_OATIPMA3,this.w_CATIPMA3)
    this.w_CAPESNE3 = IIF(EMPTY(this.w_CAPESNE3),this.w_OAPESNE3,this.w_CAPESNE3)
    this.w_CAPESLO3 = IIF(EMPTY(this.w_CAPESLO3),this.w_OAPESLO3,this.w_CAPESLO3)
    this.w_CADESVO3 = IIF(EMPTY(this.w_CADESVO3),this.w_OADESVO3,this.w_CADESVO3)
    this.w_CAUMVOL3 = IIF(EMPTY(this.w_CAUMVOL3),this.w_OAUMVOL3,this.w_CAUMVOL3)
    this.w_CATPCON3 = IIF(EMPTY(this.w_CATPCON3),this.w_OATPCON3,this.w_CATPCON3)
    this.w_CAPZCON3 = IIF(EMPTY(this.w_CAPZCON3),this.w_OAPZCON3,this.w_CAPZCON3)
    this.w_CACOCOL3 = IIF(EMPTY(this.w_CACOCOL3),this.w_OACOCOL3,this.w_CACOCOL3)
    this.w_CADIMLU3 = IIF(EMPTY(this.w_CADIMLU3),this.w_OADIMLU3,this.w_CADIMLU3)
    this.w_CADIMLA3 = IIF(EMPTY(this.w_CADIMLA3),this.w_OADIMLA3,this.w_CADIMLA3)
    this.w_CADIMAL3 = IIF(EMPTY(this.w_CADIMAL3),this.w_OADIMAL3,this.w_CADIMAL3)
    this.w_CAUMDIM3 = IIF(EMPTY(this.w_CAUMDIM3),this.w_OAUMDIM3,this.w_CAUMDIM3)
    this.w_CAFLCON3 = IIF(EMPTY(this.w_CAFLCON3),this.w_OAFLCON3,this.w_CAFLCON3)
    this.w_CACODVAR = IIF(EMPTY(this.w_CACODVAR),this.w_OACODVAR,this.w_CACODVAR)
    this.w_CACAUZI3 = IIF(EMPTY(this.w_CACAUZI3),this.w_OACAUZI3,this.w_CACAUZI3)
    this.w_CAIMBRE3 = IIF(EMPTY(this.w_CAIMBRE3),this.w_OAIMBRE3,this.w_CAIMBRE3)
    this.w_CAIMBCA3 = IIF(EMPTY(this.w_CAIMBCA3),this.w_OAIMBCA3,this.w_CAIMBCA3)
    this.w_CACONFIG = IIF(EMPTY(this.w_CACONFIG),this.w_OACONFIG,this.w_CACONFIG)
    this.w_CACONATO = IIF(EMPTY(this.w_CACONATO),this.w_OACONATO,this.w_CACONATO)
    this.w_CACONCAR = IIF(EMPTY(this.w_CACONCAR),this.w_OACONCAR,this.w_CACONCAR)
    this.w_CAGESCAR = IIF(EMPTY(this.w_CAGESCAR),this.w_OAGESCAR,this.w_CAGESCAR)
    this.w_CARIFCLA = IIF(EMPTY(this.w_CARIFCLA),this.w_OARIFCLA,this.w_CARIFCLA)
    this.w_CAKEYSAL = IIF(EMPTY(this.w_CAKEYSAL),this.w_OAKEYSAL,this.w_CAKEYSAL)
    this.w_CACODCEN = IIF(EMPTY(this.w_CACODCEN),this.w_OACODCEN,this.w_CACODCEN)
    this.w_CAFLUMVA = IIF(EMPTY(this.w_CAFLUMVA),this.w_OAFLUMVA,this.w_CAFLUMVA)
    this.w_CAPREZUM = IIF(EMPTY(this.w_CAPREZUM),this.w_OAPREZUM,this.w_CAPREZUM)
    this.w_CARIFCLA = IIF(EMPTY(NVL(this.w_CARIFCLA, " ")),.NULL., this.w_CARIFCLA)
    if g_APPLICATION="ad hoc ENTERPRISE"
      * --- Write into KEY_ARTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CADESART ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'KEY_ARTI','CADESART');
        +",CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'KEY_ARTI','CADESSUP');
        +",CACODCON ="+cp_NullLink(cp_ToStrODBC(this.w_CACODCON),'KEY_ARTI','CACODCON');
        +",CATIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPCON),'KEY_ARTI','CATIPCON');
        +",CA__TIPO ="+cp_NullLink(cp_ToStrODBC(this.w_CA__TIPO1),'KEY_ARTI','CA__TIPO');
        +",CATIPBAR ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPBAR1),'KEY_ARTI','CATIPBAR');
        +",CAFLSTAM ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAM),'KEY_ARTI','CAFLSTAM');
        +",CAUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_CAUNIMIS),'KEY_ARTI','CAUNIMIS');
        +",CAOPERAT ="+cp_NullLink(cp_ToStrODBC(this.w_CAOPERAT1),'KEY_ARTI','CAOPERAT');
        +",CAMOLTIP ="+cp_NullLink(cp_ToStrODBC(this.w_CAMOLTIP1),'KEY_ARTI','CAMOLTIP');
        +",CADTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_CADTINVA),'KEY_ARTI','CADTINVA');
        +",CADTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_CADTOBSO),'KEY_ARTI','CADTOBSO');
        +",CATIPMA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPMA3),'KEY_ARTI','CATIPMA3');
        +",CAPESNE3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPESNE3),'KEY_ARTI','CAPESNE3');
        +",CAPESLO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPESLO3),'KEY_ARTI','CAPESLO3');
        +",CADESVO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADESVO3),'KEY_ARTI','CADESVO3');
        +",CAUMVOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAUMVOL3),'KEY_ARTI','CAUMVOL3');
        +",CATPCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATPCON3),'KEY_ARTI','CATPCON3');
        +",CAPZCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPZCON3),'KEY_ARTI','CAPZCON3');
        +",CACOCOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CACOCOL3),'KEY_ARTI','CACOCOL3');
        +",CADIMLU3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMLU3),'KEY_ARTI','CADIMLU3');
        +",CADIMLA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMLA3),'KEY_ARTI','CADIMLA3');
        +",CADIMAL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMAL3),'KEY_ARTI','CADIMAL3');
        +",CAUMDIM3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAUMDIM3),'KEY_ARTI','CAUMDIM3');
        +",CAFLCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLCON3),'KEY_ARTI','CAFLCON3');
        +",CACAUZI3 ="+cp_NullLink(cp_ToStrODBC(this.w_CACAUZI3),'KEY_ARTI','CACAUZI3');
        +",CAIMBRE3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAIMBRE3),'KEY_ARTI','CAIMBRE3');
        +",CAIMBCA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAIMBCA3),'KEY_ARTI','CAIMBCA3');
        +",CACONFIG ="+cp_NullLink(cp_ToStrODBC(this.w_CACONFIG),'KEY_ARTI','CACONFIG');
        +",CACONATO ="+cp_NullLink(cp_ToStrODBC(this.w_CACONATO),'KEY_ARTI','CACONATO');
        +",CACONCAR ="+cp_NullLink(cp_ToStrODBC(this.w_CACONCAR),'KEY_ARTI','CACONCAR');
        +",CAGESCAR ="+cp_NullLink(cp_ToStrODBC(this.w_CAGESCAR),'KEY_ARTI','CAGESCAR');
        +",CARIFCLA ="+cp_NullLink(cp_ToStrODBC(this.w_CARIFCLA),'KEY_ARTI','CARIFCLA');
        +",CAKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_CAKEYSAL),'KEY_ARTI','CAKEYSAL');
        +",CACODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_CACODCEN),'KEY_ARTI','CACODCEN');
        +",CAFLUMVA ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLUMVA),'KEY_ARTI','CAFLUMVA');
        +",CAPREZUM ="+cp_NullLink(cp_ToStrODBC(this.w_CAPREZUM),'KEY_ARTI','CAPREZUM');
            +i_ccchkf ;
        +" where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_ARCODART);
               )
      else
        update (i_cTable) set;
            CADESART = this.w_ARDESART;
            ,CADESSUP = this.w_ARDESSUP;
            ,CACODCON = this.w_CACODCON;
            ,CATIPCON = this.w_CATIPCON;
            ,CA__TIPO = this.w_CA__TIPO1;
            ,CATIPBAR = this.w_CATIPBAR1;
            ,CAFLSTAM = this.w_CAFLSTAM;
            ,CAUNIMIS = this.w_CAUNIMIS;
            ,CAOPERAT = this.w_CAOPERAT1;
            ,CAMOLTIP = this.w_CAMOLTIP1;
            ,CADTINVA = this.w_CADTINVA;
            ,CADTOBSO = this.w_CADTOBSO;
            ,CATIPMA3 = this.w_CATIPMA3;
            ,CAPESNE3 = this.w_CAPESNE3;
            ,CAPESLO3 = this.w_CAPESLO3;
            ,CADESVO3 = this.w_CADESVO3;
            ,CAUMVOL3 = this.w_CAUMVOL3;
            ,CATPCON3 = this.w_CATPCON3;
            ,CAPZCON3 = this.w_CAPZCON3;
            ,CACOCOL3 = this.w_CACOCOL3;
            ,CADIMLU3 = this.w_CADIMLU3;
            ,CADIMLA3 = this.w_CADIMLA3;
            ,CADIMAL3 = this.w_CADIMAL3;
            ,CAUMDIM3 = this.w_CAUMDIM3;
            ,CAFLCON3 = this.w_CAFLCON3;
            ,CACAUZI3 = this.w_CACAUZI3;
            ,CAIMBRE3 = this.w_CAIMBRE3;
            ,CAIMBCA3 = this.w_CAIMBCA3;
            ,CACONFIG = this.w_CACONFIG;
            ,CACONATO = this.w_CACONATO;
            ,CACONCAR = this.w_CACONCAR;
            ,CAGESCAR = this.w_CAGESCAR;
            ,CARIFCLA = this.w_CARIFCLA;
            ,CAKEYSAL = this.w_CAKEYSAL;
            ,CACODCEN = this.w_CACODCEN;
            ,CAFLUMVA = this.w_CAFLUMVA;
            ,CAPREZUM = this.w_CAPREZUM;
            &i_ccchkf. ;
         where;
            CACODICE = this.w_ARCODART;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Fallita la scrittura di un codice di ricerca!'
        return
      endif
    else
      * --- Write into KEY_ARTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CADESART ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'KEY_ARTI','CADESART');
        +",CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'KEY_ARTI','CADESSUP');
        +",CACODCON ="+cp_NullLink(cp_ToStrODBC(this.w_CACODCON),'KEY_ARTI','CACODCON');
        +",CATIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPCON),'KEY_ARTI','CATIPCON');
        +",CA__TIPO ="+cp_NullLink(cp_ToStrODBC(this.w_CA__TIPO1),'KEY_ARTI','CA__TIPO');
        +",CATIPBAR ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPBAR1),'KEY_ARTI','CATIPBAR');
        +",CAFLSTAM ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAM),'KEY_ARTI','CAFLSTAM');
        +",CAUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_CAUNIMIS),'KEY_ARTI','CAUNIMIS');
        +",CAOPERAT ="+cp_NullLink(cp_ToStrODBC(this.w_CAOPERAT1),'KEY_ARTI','CAOPERAT');
        +",CAMOLTIP ="+cp_NullLink(cp_ToStrODBC(this.w_CAMOLTIP1),'KEY_ARTI','CAMOLTIP');
        +",CADTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_CADTINVA),'KEY_ARTI','CADTINVA');
        +",CADTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_CADTOBSO),'KEY_ARTI','CADTOBSO');
        +",CATIPMA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPMA3),'KEY_ARTI','CATIPMA3');
        +",CAPESNE3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPESNE3),'KEY_ARTI','CAPESNE3');
        +",CAPESLO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPESLO3),'KEY_ARTI','CAPESLO3');
        +",CADESVO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADESVO3),'KEY_ARTI','CADESVO3');
        +",CAUMVOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAUMVOL3),'KEY_ARTI','CAUMVOL3');
        +",CATPCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATPCON3),'KEY_ARTI','CATPCON3');
        +",CAPZCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPZCON3),'KEY_ARTI','CAPZCON3');
        +",CACOCOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CACOCOL3),'KEY_ARTI','CACOCOL3');
        +",CADIMLU3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMLU3),'KEY_ARTI','CADIMLU3');
        +",CADIMLA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMLA3),'KEY_ARTI','CADIMLA3');
        +",CADIMAL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMAL3),'KEY_ARTI','CADIMAL3');
        +",CAUMDIM3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAUMDIM3),'KEY_ARTI','CAUMDIM3');
            +i_ccchkf ;
        +" where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_ARCODART);
               )
      else
        update (i_cTable) set;
            CADESART = this.w_ARDESART;
            ,CADESSUP = this.w_ARDESSUP;
            ,CACODCON = this.w_CACODCON;
            ,CATIPCON = this.w_CATIPCON;
            ,CA__TIPO = this.w_CA__TIPO1;
            ,CATIPBAR = this.w_CATIPBAR1;
            ,CAFLSTAM = this.w_CAFLSTAM;
            ,CAUNIMIS = this.w_CAUNIMIS;
            ,CAOPERAT = this.w_CAOPERAT1;
            ,CAMOLTIP = this.w_CAMOLTIP1;
            ,CADTINVA = this.w_CADTINVA;
            ,CADTOBSO = this.w_CADTOBSO;
            ,CATIPMA3 = this.w_CATIPMA3;
            ,CAPESNE3 = this.w_CAPESNE3;
            ,CAPESLO3 = this.w_CAPESLO3;
            ,CADESVO3 = this.w_CADESVO3;
            ,CAUMVOL3 = this.w_CAUMVOL3;
            ,CATPCON3 = this.w_CATPCON3;
            ,CAPZCON3 = this.w_CAPZCON3;
            ,CACOCOL3 = this.w_CACOCOL3;
            ,CADIMLU3 = this.w_CADIMLU3;
            ,CADIMLA3 = this.w_CADIMLA3;
            ,CADIMAL3 = this.w_CADIMAL3;
            ,CAUMDIM3 = this.w_CAUMDIM3;
            &i_ccchkf. ;
         where;
            CACODICE = this.w_ARCODART;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Fallita la scrittura di un codice di ricerca!'
        return
      endif
    endif
    if i_Rows>0
      this.w_AGGKEY = this.w_AGGKEY+1
    endif
    return
  proc Try_0582BCC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if g_APPLICATION="ad hoc ENTERPRISE"
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODRICER);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              CACODICE = this.w_CODRICER;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_OACODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        this.w_OADESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
        this.w_OADESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
        this.w_OATIPCON = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
        this.w_OACODCON = NVL(cp_ToDate(_read_.CACODCON),cp_NullValue(_read_.CACODCON))
        this.w_OA__TIPO = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
        this.w_OATIPBAR = NVL(cp_ToDate(_read_.CATIPBAR),cp_NullValue(_read_.CATIPBAR))
        this.w_OAFLSTAM = NVL(cp_ToDate(_read_.CAFLSTAM),cp_NullValue(_read_.CAFLSTAM))
        this.w_OAUNIMIS = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
        this.w_OAOPERAT = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
        this.w_OAMOLTIP = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
        this.w_OADTINVA = NVL(cp_ToDate(_read_.CADTINVA),cp_NullValue(_read_.CADTINVA))
        this.w_OADTOBSO = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
        this.w_OATIPMA3 = NVL(cp_ToDate(_read_.CATIPMA3),cp_NullValue(_read_.CATIPMA3))
        this.w_OAPESNE3 = NVL(cp_ToDate(_read_.CAPESNE3),cp_NullValue(_read_.CAPESNE3))
        this.w_OAPESLO3 = NVL(cp_ToDate(_read_.CAPESLO3),cp_NullValue(_read_.CAPESLO3))
        this.w_OADESVO3 = NVL(cp_ToDate(_read_.CADESVO3),cp_NullValue(_read_.CADESVO3))
        this.w_OAUMVOL3 = NVL(cp_ToDate(_read_.CAUMVOL3),cp_NullValue(_read_.CAUMVOL3))
        this.w_OATPCON3 = NVL(cp_ToDate(_read_.CATPCON3),cp_NullValue(_read_.CATPCON3))
        this.w_OAPZCON3 = NVL(cp_ToDate(_read_.CAPZCON3),cp_NullValue(_read_.CAPZCON3))
        this.w_OACOCOL3 = NVL(cp_ToDate(_read_.CACOCOL3),cp_NullValue(_read_.CACOCOL3))
        this.w_OADIMLU3 = NVL(cp_ToDate(_read_.CADIMLU3),cp_NullValue(_read_.CADIMLU3))
        this.w_OADIMLA3 = NVL(cp_ToDate(_read_.CADIMLA3),cp_NullValue(_read_.CADIMLA3))
        this.w_OADIMAL3 = NVL(cp_ToDate(_read_.CADIMAL3),cp_NullValue(_read_.CADIMAL3))
        this.w_OAUMDIM3 = NVL(cp_ToDate(_read_.CAUMDIM3),cp_NullValue(_read_.CAUMDIM3))
        this.w_OAFLCON3 = NVL(cp_ToDate(_read_.CAFLCON3),cp_NullValue(_read_.CAFLCON3))
        this.w_OACODVAR = NVL(cp_ToDate(_read_.CACODVAR),cp_NullValue(_read_.CACODVAR))
        this.w_OACAUZI3 = NVL(cp_ToDate(_read_.CACAUZI3),cp_NullValue(_read_.CACAUZI3))
        this.w_OAIMBRE3 = NVL(cp_ToDate(_read_.CAIMBRE3),cp_NullValue(_read_.CAIMBRE3))
        this.w_OAIMBCA3 = NVL(cp_ToDate(_read_.CAIMBCA3),cp_NullValue(_read_.CAIMBCA3))
        this.w_OACONFIG = NVL(cp_ToDate(_read_.CACONFIG),cp_NullValue(_read_.CACONFIG))
        this.w_OACONATO = NVL(cp_ToDate(_read_.CACONATO),cp_NullValue(_read_.CACONATO))
        this.w_OACONCAR = NVL(cp_ToDate(_read_.CACONCAR),cp_NullValue(_read_.CACONCAR))
        this.w_OAGESCAR = NVL(cp_ToDate(_read_.CAGESCAR),cp_NullValue(_read_.CAGESCAR))
        this.w_OARIFCLA = NVL(cp_ToDate(_read_.CARIFCLA),cp_NullValue(_read_.CARIFCLA))
        this.w_OAKEYSAL = NVL(cp_ToDate(_read_.CAKEYSAL),cp_NullValue(_read_.CAKEYSAL))
        this.w_OACODCEN = NVL(cp_ToDate(_read_.CACODCEN),cp_NullValue(_read_.CACODCEN))
        this.w_OAFLUMVA = NVL(cp_ToDate(_read_.CAFLUMVA),cp_NullValue(_read_.CAFLUMVA))
        this.w_OAPREZUM = NVL(cp_ToDate(_read_.CAPREZUM),cp_NullValue(_read_.CAPREZUM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART,CADESART,CADESSUP,CATIPCON,CACODCON,CA__TIPO,CATIPBAR,CAFLSTAM,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTINVA,CADTOBSO,CATIPMA3,CAPESNE3,CAPESLO3,CADESVO3,CAUMVOL3,CATPCON3,CAPZCON3,CACOCOL3,CADIMLU3,CADIMLA3,CADIMAL3,CAUMDIM3,CAFLCON3,CACODVAR"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODRICER);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART,CADESART,CADESSUP,CATIPCON,CACODCON,CA__TIPO,CATIPBAR,CAFLSTAM,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTINVA,CADTOBSO,CATIPMA3,CAPESNE3,CAPESLO3,CADESVO3,CAUMVOL3,CATPCON3,CAPZCON3,CACOCOL3,CADIMLU3,CADIMLA3,CADIMAL3,CAUMDIM3,CAFLCON3,CACODVAR;
          from (i_cTable) where;
              CACODICE = this.w_CODRICER;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_OACODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        this.w_OADESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
        this.w_OADESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
        this.w_OATIPCON = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
        this.w_OACODCON = NVL(cp_ToDate(_read_.CACODCON),cp_NullValue(_read_.CACODCON))
        this.w_OA__TIPO = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
        this.w_OATIPBAR = NVL(cp_ToDate(_read_.CATIPBAR),cp_NullValue(_read_.CATIPBAR))
        this.w_OAFLSTAM = NVL(cp_ToDate(_read_.CAFLSTAM),cp_NullValue(_read_.CAFLSTAM))
        this.w_OAUNIMIS = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
        this.w_OAOPERAT = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
        this.w_OAMOLTIP = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
        this.w_OADTINVA = NVL(cp_ToDate(_read_.CADTINVA),cp_NullValue(_read_.CADTINVA))
        this.w_OADTOBSO = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
        this.w_OATIPMA3 = NVL(cp_ToDate(_read_.CATIPMA3),cp_NullValue(_read_.CATIPMA3))
        this.w_OAPESNE3 = NVL(cp_ToDate(_read_.CAPESNE3),cp_NullValue(_read_.CAPESNE3))
        this.w_OAPESLO3 = NVL(cp_ToDate(_read_.CAPESLO3),cp_NullValue(_read_.CAPESLO3))
        this.w_OADESVO3 = NVL(cp_ToDate(_read_.CADESVO3),cp_NullValue(_read_.CADESVO3))
        this.w_OAUMVOL3 = NVL(cp_ToDate(_read_.CAUMVOL3),cp_NullValue(_read_.CAUMVOL3))
        this.w_OATPCON3 = NVL(cp_ToDate(_read_.CATPCON3),cp_NullValue(_read_.CATPCON3))
        this.w_OAPZCON3 = NVL(cp_ToDate(_read_.CAPZCON3),cp_NullValue(_read_.CAPZCON3))
        this.w_OACOCOL3 = NVL(cp_ToDate(_read_.CACOCOL3),cp_NullValue(_read_.CACOCOL3))
        this.w_OADIMLU3 = NVL(cp_ToDate(_read_.CADIMLU3),cp_NullValue(_read_.CADIMLU3))
        this.w_OADIMLA3 = NVL(cp_ToDate(_read_.CADIMLA3),cp_NullValue(_read_.CADIMLA3))
        this.w_OADIMAL3 = NVL(cp_ToDate(_read_.CADIMAL3),cp_NullValue(_read_.CADIMAL3))
        this.w_OAUMDIM3 = NVL(cp_ToDate(_read_.CAUMDIM3),cp_NullValue(_read_.CAUMDIM3))
        this.w_OAFLCON3 = NVL(cp_ToDate(_read_.CAFLCON3),cp_NullValue(_read_.CAFLCON3))
        this.w_OACODVAR = NVL(cp_ToDate(_read_.CACODVAR),cp_NullValue(_read_.CACODVAR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_ARDESART = IIF(EMPTY(this.w_ARDESART),this.w_OADESART,this.w_ARDESART)
    this.w_ARDESSUP = IIF(EMPTY(this.w_ARDESSUP),this.w_OADESSUP,this.w_ARDESSUP)
    this.w_CATIPCON = IIF(EMPTY(this.w_CATIPCON),this.w_OATIPCON,this.w_CATIPCON)
    this.w_CACODCON = IIF(!this.w_CATIPCON$"CF", SPACE(15), IIF(EMPTY(this.w_CACODCON), this.w_OACODCON, this.w_CACODCON))
    this.w_CA__TIPO = IIF(EMPTY(this.w_CA__TIPO),this.w_OA__TIPO,this.w_CA__TIPO)
    this.w_CATIPBAR = IIF(EMPTY(this.w_CATIPBAR),this.w_OATIPBAR,this.w_CATIPBAR)
    this.w_CAFLSTAM = IIF(EMPTY(this.w_CAFLSTAM),this.w_OAFLSTAM,this.w_CAFLSTAM)
    this.w_CAUNIMIS = IIF(EMPTY(this.w_CAUNIMIS),this.w_OAUNIMIS,this.w_CAUNIMIS)
    this.w_CAOPERAT = IIF(EMPTY(this.w_CAOPERAT),this.w_OAOPERAT,this.w_CAOPERAT)
    this.w_CAMOLTIP = IIF(EMPTY(this.w_CAMOLTIP),this.w_OAMOLTIP,this.w_CAMOLTIP)
    this.w_CADTINVA = IIF(EMPTY(this.w_CADTINVA),this.w_OADTINVA,this.w_CADTINVA)
    this.w_CADTOBSO = IIF(EMPTY(this.w_CADTOBSO),this.w_OADTOBSO,this.w_CADTOBSO)
    this.w_CATIPMA3 = IIF(EMPTY(this.w_CATIPMA3),this.w_OATIPMA3,this.w_CATIPMA3)
    this.w_CAPESNE3 = IIF(EMPTY(this.w_CAPESNE3),this.w_OAPESNE3,this.w_CAPESNE3)
    this.w_CAPESLO3 = IIF(EMPTY(this.w_CAPESLO3),this.w_OAPESLO3,this.w_CAPESLO3)
    this.w_CADESVO3 = IIF(EMPTY(this.w_CADESVO3),this.w_OADESVO3,this.w_CADESVO3)
    this.w_CAUMVOL3 = IIF(EMPTY(this.w_CAUMVOL3),this.w_OAUMVOL3,this.w_CAUMVOL3)
    this.w_CATPCON3 = IIF(EMPTY(this.w_CATPCON3),this.w_OATPCON3,this.w_CATPCON3)
    this.w_CAPZCON3 = IIF(EMPTY(this.w_CAPZCON3),this.w_OAPZCON3,this.w_CAPZCON3)
    this.w_CACOCOL3 = IIF(EMPTY(this.w_CACOCOL3),this.w_OACOCOL3,this.w_CACOCOL3)
    this.w_CADIMLU3 = IIF(EMPTY(this.w_CADIMLU3),this.w_OADIMLU3,this.w_CADIMLU3)
    this.w_CADIMLA3 = IIF(EMPTY(this.w_CADIMLA3),this.w_OADIMLA3,this.w_CADIMLA3)
    this.w_CADIMAL3 = IIF(EMPTY(this.w_CADIMAL3),this.w_OADIMAL3,this.w_CADIMAL3)
    this.w_CAUMDIM3 = IIF(EMPTY(this.w_CAUMDIM3),this.w_OAUMDIM3,this.w_CAUMDIM3)
    this.w_CAFLCON3 = IIF(EMPTY(this.w_CAFLCON3),this.w_OAFLCON3,this.w_CAFLCON3)
    this.w_CACODVAR = IIF(EMPTY(this.w_CACODVAR),this.w_OACODVAR,this.w_CACODVAR)
    this.w_CACAUZI3 = IIF(EMPTY(this.w_CACAUZI3),this.w_OACAUZI3,this.w_CACAUZI3)
    this.w_CAIMBRE3 = IIF(EMPTY(this.w_CAIMBRE3),this.w_OAIMBRE3,this.w_CAIMBRE3)
    this.w_CAIMBCA3 = IIF(EMPTY(this.w_CAIMBCA3),this.w_OAIMBCA3,this.w_CAIMBCA3)
    this.w_CACONFIG = IIF(EMPTY(this.w_CACONFIG),this.w_OACONFIG,this.w_CACONFIG)
    this.w_CACONATO = IIF(EMPTY(this.w_CACONATO),this.w_OACONATO,this.w_CACONATO)
    this.w_CACONCAR = IIF(EMPTY(this.w_CACONCAR),this.w_OACONCAR,this.w_CACONCAR)
    this.w_CAGESCAR = IIF(EMPTY(this.w_CAGESCAR),this.w_OAGESCAR,this.w_CAGESCAR)
    this.w_CARIFCLA = IIF(EMPTY(this.w_CARIFCLA),this.w_OARIFCLA,this.w_CARIFCLA)
    this.w_CAKEYSAL = IIF(EMPTY(this.w_CAKEYSAL),this.w_OAKEYSAL,this.w_CAKEYSAL)
    this.w_CACODCEN = IIF(EMPTY(this.w_CACODCEN),this.w_OACODCEN,this.w_CACODCEN)
    this.w_CAFLUMVA = IIF(EMPTY(this.w_CAFLUMVA),this.w_OAFLUMVA,this.w_CAFLUMVA)
    this.w_CAPREZUM = IIF(EMPTY(this.w_CAPREZUM),this.w_OAPREZUM,this.w_CAPREZUM)
    this.w_CARIFCLA = IIF(EMPTY(NVL(this.w_CARIFCLA, " ")),.NULL., this.w_CARIFCLA)
    if g_APPLICATION="ad hoc ENTERPRISE"
      * --- Write into KEY_ARTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CADESART ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'KEY_ARTI','CADESART');
        +",CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'KEY_ARTI','CADESSUP');
        +",CACODCON ="+cp_NullLink(cp_ToStrODBC(this.w_CACODCON),'KEY_ARTI','CACODCON');
        +",CATIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPCON),'KEY_ARTI','CATIPCON');
        +",CA__TIPO ="+cp_NullLink(cp_ToStrODBC(this.w_CA__TIPO1),'KEY_ARTI','CA__TIPO');
        +",CATIPBAR ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPBAR1),'KEY_ARTI','CATIPBAR');
        +",CAFLSTAM ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAM),'KEY_ARTI','CAFLSTAM');
        +",CAUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_CAUNIMIS),'KEY_ARTI','CAUNIMIS');
        +",CAOPERAT ="+cp_NullLink(cp_ToStrODBC(this.w_CAOPERAT1),'KEY_ARTI','CAOPERAT');
        +",CAMOLTIP ="+cp_NullLink(cp_ToStrODBC(this.w_CAMOLTIP1),'KEY_ARTI','CAMOLTIP');
        +",CADTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_CADTINVA),'KEY_ARTI','CADTINVA');
        +",CADTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_CADTOBSO),'KEY_ARTI','CADTOBSO');
        +",CATIPMA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPMA3),'KEY_ARTI','CATIPMA3');
        +",CAPESNE3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPESNE3),'KEY_ARTI','CAPESNE3');
        +",CAPESLO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPESLO3),'KEY_ARTI','CAPESLO3');
        +",CADESVO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADESVO3),'KEY_ARTI','CADESVO3');
        +",CAUMVOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAUMVOL3),'KEY_ARTI','CAUMVOL3');
        +",CATPCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATPCON3),'KEY_ARTI','CATPCON3');
        +",CAPZCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPZCON3),'KEY_ARTI','CAPZCON3');
        +",CACOCOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CACOCOL3),'KEY_ARTI','CACOCOL3');
        +",CADIMLU3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMLU3),'KEY_ARTI','CADIMLU3');
        +",CADIMLA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMLA3),'KEY_ARTI','CADIMLA3');
        +",CADIMAL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMAL3),'KEY_ARTI','CADIMAL3');
        +",CAUMDIM3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAUMDIM3),'KEY_ARTI','CAUMDIM3');
        +",CAFLCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLCON3),'KEY_ARTI','CAFLCON3');
        +",CACAUZI3 ="+cp_NullLink(cp_ToStrODBC(this.w_CACAUZI3),'KEY_ARTI','CACAUZI3');
        +",CAIMBRE3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAIMBRE3),'KEY_ARTI','CAIMBRE3');
        +",CAIMBCA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAIMBCA3),'KEY_ARTI','CAIMBCA3');
        +",CACONFIG ="+cp_NullLink(cp_ToStrODBC(this.w_CACONFIG),'KEY_ARTI','CACONFIG');
        +",CACONATO ="+cp_NullLink(cp_ToStrODBC(this.w_CACONATO),'KEY_ARTI','CACONATO');
        +",CACONCAR ="+cp_NullLink(cp_ToStrODBC(this.w_CACONCAR),'KEY_ARTI','CACONCAR');
        +",CAGESCAR ="+cp_NullLink(cp_ToStrODBC(this.w_CAGESCAR),'KEY_ARTI','CAGESCAR');
        +",CARIFCLA ="+cp_NullLink(cp_ToStrODBC(this.w_CARIFCLA),'KEY_ARTI','CARIFCLA');
        +",CAKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_CAKEYSAL),'KEY_ARTI','CAKEYSAL');
        +",CACODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_CACODCEN),'KEY_ARTI','CACODCEN');
        +",CAFLUMVA ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLUMVA),'KEY_ARTI','CAFLUMVA');
        +",CAPREZUM ="+cp_NullLink(cp_ToStrODBC(this.w_CAPREZUM),'KEY_ARTI','CAPREZUM');
            +i_ccchkf ;
        +" where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CODRICER);
               )
      else
        update (i_cTable) set;
            CADESART = this.w_ARDESART;
            ,CADESSUP = this.w_ARDESSUP;
            ,CACODCON = this.w_CACODCON;
            ,CATIPCON = this.w_CATIPCON;
            ,CA__TIPO = this.w_CA__TIPO1;
            ,CATIPBAR = this.w_CATIPBAR1;
            ,CAFLSTAM = this.w_CAFLSTAM;
            ,CAUNIMIS = this.w_CAUNIMIS;
            ,CAOPERAT = this.w_CAOPERAT1;
            ,CAMOLTIP = this.w_CAMOLTIP1;
            ,CADTINVA = this.w_CADTINVA;
            ,CADTOBSO = this.w_CADTOBSO;
            ,CATIPMA3 = this.w_CATIPMA3;
            ,CAPESNE3 = this.w_CAPESNE3;
            ,CAPESLO3 = this.w_CAPESLO3;
            ,CADESVO3 = this.w_CADESVO3;
            ,CAUMVOL3 = this.w_CAUMVOL3;
            ,CATPCON3 = this.w_CATPCON3;
            ,CAPZCON3 = this.w_CAPZCON3;
            ,CACOCOL3 = this.w_CACOCOL3;
            ,CADIMLU3 = this.w_CADIMLU3;
            ,CADIMLA3 = this.w_CADIMLA3;
            ,CADIMAL3 = this.w_CADIMAL3;
            ,CAUMDIM3 = this.w_CAUMDIM3;
            ,CAFLCON3 = this.w_CAFLCON3;
            ,CACAUZI3 = this.w_CACAUZI3;
            ,CAIMBRE3 = this.w_CAIMBRE3;
            ,CAIMBCA3 = this.w_CAIMBCA3;
            ,CACONFIG = this.w_CACONFIG;
            ,CACONATO = this.w_CACONATO;
            ,CACONCAR = this.w_CACONCAR;
            ,CAGESCAR = this.w_CAGESCAR;
            ,CARIFCLA = this.w_CARIFCLA;
            ,CAKEYSAL = this.w_CAKEYSAL;
            ,CACODCEN = this.w_CACODCEN;
            ,CAFLUMVA = this.w_CAFLUMVA;
            ,CAPREZUM = this.w_CAPREZUM;
            &i_ccchkf. ;
         where;
            CACODICE = this.w_CODRICER;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Fallita la scrittura di un codice di ricerca!'
        return
      endif
    else
      * --- Write into KEY_ARTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CADESART ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'KEY_ARTI','CADESART');
        +",CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'KEY_ARTI','CADESSUP');
        +",CACODCON ="+cp_NullLink(cp_ToStrODBC(this.w_CACODCON),'KEY_ARTI','CACODCON');
        +",CATIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPCON),'KEY_ARTI','CATIPCON');
        +",CA__TIPO ="+cp_NullLink(cp_ToStrODBC(this.w_CA__TIPO1),'KEY_ARTI','CA__TIPO');
        +",CATIPBAR ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPBAR1),'KEY_ARTI','CATIPBAR');
        +",CAFLSTAM ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAM),'KEY_ARTI','CAFLSTAM');
        +",CAUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_CAUNIMIS),'KEY_ARTI','CAUNIMIS');
        +",CAOPERAT ="+cp_NullLink(cp_ToStrODBC(this.w_CAOPERAT1),'KEY_ARTI','CAOPERAT');
        +",CAMOLTIP ="+cp_NullLink(cp_ToStrODBC(this.w_CAMOLTIP1),'KEY_ARTI','CAMOLTIP');
        +",CADTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_CADTINVA),'KEY_ARTI','CADTINVA');
        +",CADTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_CADTOBSO),'KEY_ARTI','CADTOBSO');
        +",CATIPMA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPMA3),'KEY_ARTI','CATIPMA3');
        +",CAPESNE3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPESNE3),'KEY_ARTI','CAPESNE3');
        +",CAPESLO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPESLO3),'KEY_ARTI','CAPESLO3');
        +",CADESVO3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADESVO3),'KEY_ARTI','CADESVO3');
        +",CAUMVOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAUMVOL3),'KEY_ARTI','CAUMVOL3');
        +",CATPCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATPCON3),'KEY_ARTI','CATPCON3');
        +",CAPZCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPZCON3),'KEY_ARTI','CAPZCON3');
        +",CACOCOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CACOCOL3),'KEY_ARTI','CACOCOL3');
        +",CADIMLU3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMLU3),'KEY_ARTI','CADIMLU3');
        +",CADIMLA3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMLA3),'KEY_ARTI','CADIMLA3');
        +",CADIMAL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CADIMAL3),'KEY_ARTI','CADIMAL3');
        +",CAUMDIM3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAUMDIM3),'KEY_ARTI','CAUMDIM3');
        +",CAFLCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLCON3),'KEY_ARTI','CAFLCON3');
            +i_ccchkf ;
        +" where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CODRICER);
               )
      else
        update (i_cTable) set;
            CADESART = this.w_ARDESART;
            ,CADESSUP = this.w_ARDESSUP;
            ,CACODCON = this.w_CACODCON;
            ,CATIPCON = this.w_CATIPCON;
            ,CA__TIPO = this.w_CA__TIPO1;
            ,CATIPBAR = this.w_CATIPBAR1;
            ,CAFLSTAM = this.w_CAFLSTAM;
            ,CAUNIMIS = this.w_CAUNIMIS;
            ,CAOPERAT = this.w_CAOPERAT1;
            ,CAMOLTIP = this.w_CAMOLTIP1;
            ,CADTINVA = this.w_CADTINVA;
            ,CADTOBSO = this.w_CADTOBSO;
            ,CATIPMA3 = this.w_CATIPMA3;
            ,CAPESNE3 = this.w_CAPESNE3;
            ,CAPESLO3 = this.w_CAPESLO3;
            ,CADESVO3 = this.w_CADESVO3;
            ,CAUMVOL3 = this.w_CAUMVOL3;
            ,CATPCON3 = this.w_CATPCON3;
            ,CAPZCON3 = this.w_CAPZCON3;
            ,CACOCOL3 = this.w_CACOCOL3;
            ,CADIMLU3 = this.w_CADIMLU3;
            ,CADIMLA3 = this.w_CADIMLA3;
            ,CADIMAL3 = this.w_CADIMAL3;
            ,CAUMDIM3 = this.w_CAUMDIM3;
            ,CAFLCON3 = this.w_CAFLCON3;
            &i_ccchkf. ;
         where;
            CACODICE = this.w_CODRICER;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Fallita la scrittura di un codice di ricerca!'
        return
      endif
    endif
    if i_Rows>0
      this.w_AGGKEY = this.w_AGGKEY+1
    endif
    return
  proc Try_03C12CB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_LISINS = this.w_LISINS + 1
    if NOT EMPTY(this.w_LIUNIMIS)
      this.w_ARUNMIS1 = this.w_LIUNIMIS
    else
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARUNMIS1"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_LICODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARUNMIS1;
          from (i_cTable) where;
              ARCODART = this.w_LICODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ARUNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if g_APPLICATION="ad hoc ENTERPRISE"
      * --- Insert into LIS_TINI
      i_nConn=i_TableProp[this.LIS_TINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LICODART"+",LICODLIS"+",LIDATATT"+",LIDATDIS"+",CPROWNUM"+",LAPREZZO"+",LIUNIMIS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_LICODART),'LIS_TINI','LICODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODLIS),'LIS_TINI','LICODLIS');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LIDATATT),'LIS_TINI','LIDATATT');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LIDATDIS),'LIS_TINI','LIDATDIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'LIS_TINI','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LIPREZZO),'LIS_TINI','LAPREZZO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ARUNMIS1),'LIS_TINI','LIUNIMIS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_LICODART,'LICODLIS',this.w_CODLIS,'LIDATATT',this.oParentObject.w_LIDATATT,'LIDATDIS',this.oParentObject.w_LIDATDIS,'CPROWNUM',this.w_CPROWNUM,'LAPREZZO',this.w_LIPREZZO,'LIUNIMIS',this.w_ARUNMIS1)
        insert into (i_cTable) (LICODART,LICODLIS,LIDATATT,LIDATDIS,CPROWNUM,LAPREZZO,LIUNIMIS &i_ccchkf. );
           values (;
             this.w_LICODART;
             ,this.w_CODLIS;
             ,this.oParentObject.w_LIDATATT;
             ,this.oParentObject.w_LIDATDIS;
             ,this.w_CPROWNUM;
             ,this.w_LIPREZZO;
             ,this.w_ARUNMIS1;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Fallito inserimento di un nuovo listino!'
        return
      endif
    else
      * --- Insert into LIS_TINI
      i_nConn=i_TableProp[this.LIS_TINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LICODART"+",LICODLIS"+",LIDATATT"+",LIDATDIS"+",CPROWNUM"+",LIUNIMIS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_LICODART),'LIS_TINI','LICODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODLIS),'LIS_TINI','LICODLIS');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LIDATATT),'LIS_TINI','LIDATATT');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LIDATDIS),'LIS_TINI','LIDATDIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'LIS_TINI','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ARUNMIS1),'LIS_TINI','LIUNIMIS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_LICODART,'LICODLIS',this.w_CODLIS,'LIDATATT',this.oParentObject.w_LIDATATT,'LIDATDIS',this.oParentObject.w_LIDATDIS,'CPROWNUM',this.w_CPROWNUM,'LIUNIMIS',this.w_ARUNMIS1)
        insert into (i_cTable) (LICODART,LICODLIS,LIDATATT,LIDATDIS,CPROWNUM,LIUNIMIS &i_ccchkf. );
           values (;
             this.w_LICODART;
             ,this.w_CODLIS;
             ,this.oParentObject.w_LIDATATT;
             ,this.oParentObject.w_LIDATDIS;
             ,this.w_CPROWNUM;
             ,this.w_ARUNMIS1;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Fallito inserimento di un nuovo listino!'
        return
      endif
    endif
    this.w_INSOK = .T.
    return
  proc Try_03C16D38()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if g_APPLICATION="ad hoc ENTERPRISE"
      if not this.w_INSOK AND this.w_FLSCAGLI<>"S"
        if NOT EMPTY(this.w_LIUNIMIS)
          this.w_ARUNMIS1 = this.w_LIUNIMIS
        else
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARUNMIS1"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_LICODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARUNMIS1;
              from (i_cTable) where;
                  ARCODART = this.w_LICODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ARUNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Insert into LIS_TINI
        i_nConn=i_TableProp[this.LIS_TINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LICODART"+",LICODLIS"+",LIDATATT"+",LIDATDIS"+",CPROWNUM"+",LAPREZZO"+",LIUNIMIS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_LICODART),'LIS_TINI','LICODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODLIS),'LIS_TINI','LICODLIS');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LIDATATT),'LIS_TINI','LIDATATT');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LIDATDIS),'LIS_TINI','LIDATDIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'LIS_TINI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LIPREZZO),'LIS_TINI','LAPREZZO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ARUNMIS1),'LIS_TINI','LIUNIMIS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_LICODART,'LICODLIS',this.w_CODLIS,'LIDATATT',this.oParentObject.w_LIDATATT,'LIDATDIS',this.oParentObject.w_LIDATDIS,'CPROWNUM',this.w_CPROWNUM,'LAPREZZO',this.w_LIPREZZO,'LIUNIMIS',this.w_ARUNMIS1)
          insert into (i_cTable) (LICODART,LICODLIS,LIDATATT,LIDATDIS,CPROWNUM,LAPREZZO,LIUNIMIS &i_ccchkf. );
             values (;
               this.w_LICODART;
               ,this.w_CODLIS;
               ,this.oParentObject.w_LIDATATT;
               ,this.oParentObject.w_LIDATDIS;
               ,this.w_CPROWNUM;
               ,this.w_LIPREZZO;
               ,this.w_ARUNMIS1;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Fallito inserimento di un nuovo listino!'
          return
        endif
        this.w_LISINS = this.w_LISINS + 1
      endif
    else
      * --- Insert into LIS_SCAG
      i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_LICODART),'LIS_SCAG','LICODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'LIS_SCAG','LIROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LIQUANTI),'LIS_SCAG','LIQUANTI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LIPREZZO),'LIS_SCAG','LIPREZZO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT1),'LIS_SCAG','LISCONT1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT2),'LIS_SCAG','LISCONT2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT3),'LIS_SCAG','LISCONT3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT4),'LIS_SCAG','LISCONT4');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_LICODART,'LIROWNUM',this.w_CPROWNUM,'LIQUANTI',this.w_LIQUANTI,'LIPREZZO',this.w_LIPREZZO,'LISCONT1',this.w_SCONT1,'LISCONT2',this.w_SCONT2,'LISCONT3',this.w_SCONT3,'LISCONT4',this.w_SCONT4)
        insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
           values (;
             this.w_LICODART;
             ,this.w_CPROWNUM;
             ,this.w_LIQUANTI;
             ,this.w_LIPREZZO;
             ,this.w_SCONT1;
             ,this.w_SCONT2;
             ,this.w_SCONT3;
             ,this.w_SCONT4;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Fallito inserimento di un nuovo scaglione!'
        return
      endif
      this.w_SCAINS = this.w_SCAINS + 1
    endif
    return
  proc Try_059DC3E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            ARCODART = this.w_ARCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='LIS_SCAG'
    this.cWorkTables[4]='LIS_TINI'
    this.cWorkTables[5]='PARMLIST'
    this.cWorkTables[6]='PAR_RIOR'
    this.cWorkTables[7]='STRUTABE'
    this.cWorkTables[8]='UNIMIS'
    this.cWorkTables[9]='VALUTE'
    this.cWorkTables[10]='LISTINI'
    this.cWorkTables[11]='DET_VARI'
    this.cWorkTables[12]='PARATRAS'
    this.cWorkTables[13]='SALDIART'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_SALDIART')
      use in _Curs_SALDIART
    endif
    if used('_Curs_DET_VARI')
      use in _Curs_DET_VARI
    endif
    if used('_Curs_LIS_TINI')
      use in _Curs_LIS_TINI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsil_bil
  * --- Area Manuale = Apre BAR_CODE (P)
  i_barc = cplu_opf("BAR_CODE",'')
  * --- Fine Area Manuale
  
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
