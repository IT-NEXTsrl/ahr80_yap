* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_login                                                        *
*              Login adhoc Enterprise/Revolution                               *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-03-12                                                      *
* Last revis.: 2016-03-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_EntParam,w_MENU
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcp_login",oParentObject,m.w_EntParam,m.w_MENU)
return(i_retval)

define class tcp_login as StdBatch
  * --- Local variables
  w_EntParam = space(3)
  w_MENU = .f.
  w_TmpN = 0
  w_TmpN2 = 0
  w_ESCENABLED = .f.
  w_SILENTCONN = space(250)
  w_RESULT = space(250)
  w_CONNATTEMPT = 0
  w_CONNDELAY = 0
  w_COUNT = 0
  w_PATH = space(250)
  w_MSG = space(10)
  w_SSOID = space(40)
  b_VALUTE = .f.
  w_UTESILENT = 0
  w_PWDSILENT = space(20)
  w_LEN = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue il Login Iniziale della Procedura
    * --- Controllo parametri
    this.w_EntParam = IIF(Type("w_EntParam")<>"C","XXX", this.w_EntParam)
    if this.w_EntParam="TIM"
      Createobject("deferred_login")
      i_retcode = 'stop'
      return
    endif
    this.w_MENU = iif(type("w_MENU")<>"L", .F., this.w_MENU)
    * --- esegue
    if i_codute=1 AND "CHG"<>this.w_EntParam
      * --- Se Azienda Vuota, Crea la prima volta gli Utenti e l'Azienda
      i_codute=0
      if ah_YesNo("%0Attenzione:%0%0Utenti e azienda non installati%0Vuoi generarli?")
        if g_INIZ
          actkey(2)
        endif
        * --- Passando il Parametro .T. imposta come Modale
        CP_SEC(.T.)
        do GSUT_KIA with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Se Creazione OK Seleziona dati Utente/Azienta (con pulsante di escape abilitato)
        this.w_ESCENABLED = .T.
        if IsRevi()
          do GSRV_UTE with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          do GS___UTE with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if i_codute<>0
          * --- Zucchetti Aulla - problema di errato caricamento xdc per i_cmoudles incompleto
          StartApplication()
        endif
      endif
      * --- Carico le impostazioni interfaccia utente..
      if i_CODUTE<>0
        GSUT_BEU(this, "Utente" )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      * --- Seleziona dati Utente/Azienta
      * --- Controllo Gestioni aperte. In tal caso non permetto la selezione Azienda
      this.w_MSG = OpenForm(.T.)
      if Not Empty(this.w_Msg)
        ah_ErrorMsg("Gestione %1 aperta. Chiudere prima tutte le gestioni",,"", this.w_Msg)
        i_retcode = 'stop'
        return
      endif
      * --- Distruggo l'application Bar affinch� salvi eventuali bottoni inseriti
      *     al suo interno...
      if VarType(oDesktopBar) = "O"
         
 oDesktopBar.Destroy() 
 oDesktopBar = .null. 
 release odesktopbar
      endif
      if this.w_MENU
        * --- Seleziona dati Utente/Azienta da Menu'
        this.w_ESCENABLED = .F.
        * --- n.b.: w_ESCENABLED viene acceso all'F10 in GS___BAZ
        if IsRevi()
          do GSRV_UTE with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          do GS___UTE with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- associazione SSOID
        if Vartype(g_REVI)="C" And g_REVI="S" And Empty(cp_SingleSignOnUser(i_codute))
          cp_AddSingleSignOnMap(i_codute, GenerateKey(40))
        endif
      else
        if g_INIZ
          actkey(2)
        endif
        if NOT EMPTY(p_SilentConnect)
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if IsRevi() And Vartype(p_EntryKey)="C" And !Empty(p_EntryKey)
          this.w_SSOID = ""
          Revi_Login(This, p_EntryKey)
        endif
        * --- Se sono all'ingresso nella procedura non faccio niente di particolare (pulsante di escape abilitato)
        this.w_ESCENABLED = .T.
        if IsRevi()
          do GSRV_UTE with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          do GS___UTE with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if i_codute<>0
          * --- Zucchetti Aulla - problema di errato caricamento xdc per i_cmoudles incompleto
          StartApplication()
          * --- associazione SSOID
          if Vartype(g_REVI)="C" And g_REVI="S"
            do case
              case Vartype(p_EntryKey)="C" And !Empty(p_EntryKey)
                Revi_Login(This, p_EntryKey, this.w_UTESILENT)
              case Empty(cp_SingleSignOnUser(i_codute))
                cp_AddSingleSignOnMap(i_codute, GenerateKey(40))
            endcase
          endif
        endif
      endif
      * --- Carico le impostazioni interfaccia utente..
      if i_CODUTE<>0
        GSUT_BEU(this, "Utente" )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if g_INIZ = .F. AND this.w_EntParam = "CHG"
        * --- Rilascio i gruppi associati all'utente se eravamo gi� nella procedura
        * --- in modo da reimpostarli per il nuovo utente
        RELEASE i_aUsrGrps
        * --- Se non sono entrato la Prima Volta allora ho Cambiato l'Azienda/Utente da dentro il Menu'
        * --- Setto  il Menu' dell'applicazione (g_INIZ inizializzata a .T. nel CP3START)
        if TYPE("oschedtimer")<>"U"
           RELEASE oschedtimer
        endif
         PUBLIC proclogin 
 proclogin=.T.
        RELEASE proclogin
        CP_BACKGROUNDMASK()
        CP_MENU()
        CP_DESK()
      endif
      * --- --Ricalcola array valute
      if i_codute<>0
        this.b_VALUTE = CAVALUTE()
      endif
    endif
    * --- Traduco i Tooltip della toolbar
    oCpToolBar.LocalizeString()
    if VarType(odesktopbar)="O"
      oDesktopbar.LocalizeString()
    endif
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if i_CODUTE<>0
      * --- Variabile per codifica numerica impianti
       
 PUBLIC g_IMNUME 
 g_IMNUME=" "
      GSUT_BNI()
      * --- Se attiva la logistica remota carico l'elenco di autonumber da gestire
      *     sulla sede...
      if g_APPLICATION="ADHOC REVOLUTION" And g_LORE="S"
        l_sPrg="CALPROGSED()"
        &L_sPrg
      endif
    endif
    if vartype(g_user_right)="N"
      release g_user_right
      cp_set_get_right(.t.)
    endif
    * --- **Inizio nasconde menu al utente schedulatore  
    if Type("g_UserScheduler")="L" AND g_UserScheduler
       SET SYSMENU to 
 DEFINE MENU vuoto BAR AT LINE 0 
 DEFINE PAD convpad OF vuoto PROMPT "" 
 ACTIVATE MENU vuoto 
 RELEASE MENU vuoto 
    endif
    * --- La maskera di gestione dei flussi e autorizzazioni � laciata dal GSUT_BEU; 
    *     se si esegue adesso impedisce l'aperutra del menu nel tema standard
    if "CHG"=this.w_EntParam
      GSUT_BEU(this,"Open")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Ad Hoc Service: Secondo processo di Ad Hoc non visibile per esecuzione parallela di procedure
    *     
    *     Viene attivato solo se dichiarata la relativa variabile pubblica nella tabella di configurazione dell'ambiente
    *     e l'utente loggato � amministratore o Addon iRevolution attivo
    * --- Carica configurazione
    GSUT_BHS(this,"ReadCnf")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Vartype(bEnableAHService)=="L" And bEnableAHService And i_codute<>0 And (cp_IsAdministrator() Or (Vartype(g_REVI)="C" And g_REVI="S")) And (Vartype(g_UserScheduler)<>"L" Or !g_UserScheduler) And !g_LOCKALL And !g_DEMO
      if ah_StartService()
        * --- Attivazione timer per elaborazioni KPI in BackGround
        GSUT_BEC(this,"ITMR")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      ah_StopService()
      if Vartype(bEnableAHService)=="L"
        bEnableAHService = .F.
      endif
    endif
    * --- iRevolution
    if Vartype(g_REVI)="C" And g_REVI="S"
      revi_ddeserver()
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione della connessione silente
    * --- Verifichiamo se la stringa � criptata e eventualemnte la decriptiamo
    if g_CRYPTSilentConnect
      this.w_SILENTCONN = ALLTRIM(CifraCnf( ALLTRIM(p_SilentConnect) , "D" ))
    else
      this.w_SILENTCONN = ALLTRIM(p_SilentConnect)
    endif
    * --- Estraiamo dalla stringa di connessione i parametri
    local i_CODUTE_old,g_CODESE_old,i_CODAZI_old,g_CODBUN_old,PWD 
 PWD="" 
 i_CODUTE_old = i_CODUTE 
 g_CODESE_old = g_CODESE 
 i_CODAZI_old = i_CODAZI 
 g_CODBUN_old = g_CODBUN 
 i_CODUTE=0 
 g_CODESE="" 
 i_CODAZI="" 
 g_CODBUN=""
    do while NOT EMPTY(this.w_SILENTCONN)
      this.w_LEN = iif(AT(";",this.w_SILENTCONN)=0,LEN(this.w_SILENTCONN)+1,AT(";",this.w_SILENTCONN))
      cmdsilent=LEFT(this.w_SILENTCONN,this.w_LEN-1) 
 &cmdsilent
      this.w_SILENTCONN = RIGHT(this.w_SILENTCONN,LEN(this.w_SILENTCONN)-this.w_LEN)
    enddo
    this.w_PWDSILENT = IIF(EMPTY(PWD),"",PWD)
    this.w_UTESILENT = iif(i_CODUTE<>0,i_CODUTE,0)
     
 g_CODESE = iif(EMPTY(g_CODESE),g_CODESE_old,g_CODESE) 
 = Cp_ChangeAzi( iif(empty(i_CODAZI),i_CODAZI_old,i_CODAZI) ) 
 g_CODBUN = iif(empty(g_CODBUN),g_CODBUN_old,g_CODBUN)
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if i_CODUTE<>0 AND g_AGEN="S"
      * --- Controllo sul resoconto giornaliero incompleto dell'utente: in caso di blocco pone i_codute a 0
      GSUT_BPS(this,"B")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_EntParam,w_MENU)
    this.w_EntParam=w_EntParam
    this.w_MENU=w_MENU
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- cp_login
  func getSecurityCode()
      LOCAL nameprg
      nameprg='CP_LOGIN'
      IF VARTYPE(this.oParentObject)='C'
        nameprg=nameprg+','+this.oParentObject
      endif
      if TYPE("this.oParentObject")='L'
        nameprg=nameprg+','+iif(nvl(this.oParentObject,.f.),'.t.','.f.')
      endif
      nameprg=nameprg+iif(VARTYPE(this.w_EntParam)='L', '', ','+alltrim(this.w_EntParam))
      nameprg=nameprg+iif(VARTYPE(this.w_MENU)='L',iif(this.w_MENU,',.t.',',.f.'),'')
      return(nameprg)
  
  Enddefine
  
  Define Class deferred_login As Timer
      * --- solito trucco per poter eseguire delle operazioni in ritardo
      Interval=200
      oThis=.Null.
      Proc Init()
          This.oThis=This
      Proc Timer()
          do CP_LOGIN with .F.,'CHG',.T.  
          This.oThis=.Null.
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_EntParam,w_MENU"
endproc
