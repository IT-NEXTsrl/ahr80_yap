* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mas                                                        *
*              Aggiorna saldi bilancio UE                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-23                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mas")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mas")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mas")
  return

* --- Class definition
define class tgscg_mas as StdPCForm
  Width  = 605
  Height = 226
  Top    = 10
  Left   = 10
  cComment = "Aggiorna saldi bilancio UE"
  cPrg = "gscg_mas"
  HelpContextID=130688361
  add object cnt as tcgscg_mas
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mas as PCContext
  w_TRROWORD = 0
  w_TRDESVOC = space(50)
  w_TRFLMACO = space(1)
  w_TRCODMAS = space(15)
  w_TRCODCON = space(15)
  w_TR_SEGNO = space(1)
  w_TR__DAVE = space(1)
  w_TRIMPDAR = 0
  w_TRIMPAVE = 0
  w_FLAGAVE = space(10)
  w_FLAGDAR = space(10)
  w_TRCODESE = space(5)
  w_TRSERIAL = space(10)
  w_TRCODCON = space(15)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODVOC = space(15)
  w_ROWORD = 0
  w_TRCODVOC = space(15)
  proc Save(i_oFrom)
    this.w_TRROWORD = i_oFrom.w_TRROWORD
    this.w_TRDESVOC = i_oFrom.w_TRDESVOC
    this.w_TRFLMACO = i_oFrom.w_TRFLMACO
    this.w_TRCODMAS = i_oFrom.w_TRCODMAS
    this.w_TRCODCON = i_oFrom.w_TRCODCON
    this.w_TR_SEGNO = i_oFrom.w_TR_SEGNO
    this.w_TR__DAVE = i_oFrom.w_TR__DAVE
    this.w_TRIMPDAR = i_oFrom.w_TRIMPDAR
    this.w_TRIMPAVE = i_oFrom.w_TRIMPAVE
    this.w_FLAGAVE = i_oFrom.w_FLAGAVE
    this.w_FLAGDAR = i_oFrom.w_FLAGDAR
    this.w_TRCODESE = i_oFrom.w_TRCODESE
    this.w_TRSERIAL = i_oFrom.w_TRSERIAL
    this.w_TRCODCON = i_oFrom.w_TRCODCON
    this.w_TIPCON = i_oFrom.w_TIPCON
    this.w_CODCON = i_oFrom.w_CODCON
    this.w_CODVOC = i_oFrom.w_CODVOC
    this.w_ROWORD = i_oFrom.w_ROWORD
    this.w_TRCODVOC = i_oFrom.w_TRCODVOC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_TRROWORD = this.w_TRROWORD
    i_oTo.w_TRDESVOC = this.w_TRDESVOC
    i_oTo.w_TRFLMACO = this.w_TRFLMACO
    i_oTo.w_TRCODMAS = this.w_TRCODMAS
    i_oTo.w_TRCODCON = this.w_TRCODCON
    i_oTo.w_TR_SEGNO = this.w_TR_SEGNO
    i_oTo.w_TR__DAVE = this.w_TR__DAVE
    i_oTo.w_TRIMPDAR = this.w_TRIMPDAR
    i_oTo.w_TRIMPAVE = this.w_TRIMPAVE
    i_oTo.w_FLAGAVE = this.w_FLAGAVE
    i_oTo.w_FLAGDAR = this.w_FLAGDAR
    i_oTo.w_TRCODESE = this.w_TRCODESE
    i_oTo.w_TRSERIAL = this.w_TRSERIAL
    i_oTo.w_TRCODCON = this.w_TRCODCON
    i_oTo.w_TIPCON = this.w_TIPCON
    i_oTo.w_CODCON = this.w_CODCON
    i_oTo.w_CODVOC = this.w_CODVOC
    i_oTo.w_ROWORD = this.w_ROWORD
    i_oTo.w_TRCODVOC = this.w_TRCODVOC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mas as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 605
  Height = 226
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=130688361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  AGG_SALD_IDX = 0
  CONTI_IDX = 0
  BIL_DETT_IDX = 0
  BIL_MAST_IDX = 0
  MASTRI_IDX = 0
  cFile = "AGG_SALD"
  cKeySelect = "TRROWORD,TRCODVOC"
  cKeyWhere  = "TRROWORD=this.w_TRROWORD and TRCODVOC=this.w_TRCODVOC"
  cKeyDetail  = "TRROWORD=this.w_TRROWORD and TRCODVOC=this.w_TRCODVOC"
  cKeyWhereODBC = '"TRROWORD="+cp_ToStrODBC(this.w_TRROWORD)';
      +'+" and TRCODVOC="+cp_ToStrODBC(this.w_TRCODVOC)';

  cKeyDetailWhereODBC = '"TRROWORD="+cp_ToStrODBC(this.w_TRROWORD)';
      +'+" and TRCODVOC="+cp_ToStrODBC(this.w_TRCODVOC)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"AGG_SALD.TRROWORD="+cp_ToStrODBC(this.w_TRROWORD)';
      +'+" and AGG_SALD.TRCODVOC="+cp_ToStrODBC(this.w_TRCODVOC)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'AGG_SALD.CPROWNUM '
  cPrg = "gscg_mas"
  cComment = "Aggiorna saldi bilancio UE"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TRROWORD = 0
  w_TRDESVOC = space(50)
  w_TRFLMACO = space(1)
  o_TRFLMACO = space(1)
  w_TRCODMAS = space(15)
  w_TRCODCON = space(15)
  w_TR_SEGNO = space(1)
  w_TR__DAVE = space(1)
  w_TRIMPDAR = 0
  o_TRIMPDAR = 0
  w_TRIMPAVE = 0
  o_TRIMPAVE = 0
  w_FLAGAVE = space(10)
  w_FLAGDAR = space(10)
  w_TRCODESE = space(5)
  w_TRSERIAL = space(10)
  w_TRCODCON = space(15)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODVOC = space(15)
  w_ROWORD = 0
  w_TRCODVOC = space(15)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_masPag1","gscg_mas",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='BIL_DETT'
    this.cWorkTables[3]='BIL_MAST'
    this.cWorkTables[4]='MASTRI'
    this.cWorkTables[5]='AGG_SALD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AGG_SALD_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AGG_SALD_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mas'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from AGG_SALD where TRROWORD=KeySet.TRROWORD
    *                            and TRCODVOC=KeySet.TRCODVOC
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.AGG_SALD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_SALD_IDX,2],this.bLoadRecFilter,this.AGG_SALD_IDX,"gscg_mas")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AGG_SALD')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AGG_SALD.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AGG_SALD '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TRROWORD',this.w_TRROWORD  ,'TRCODVOC',this.w_TRCODVOC  )
      select * from (i_cTable) AGG_SALD where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TRROWORD = NVL(TRROWORD,0)
        .w_TRDESVOC = NVL(TRDESVOC,space(50))
        .w_TRCODESE = NVL(TRCODESE,space(5))
          * evitabile
          *.link_1_3('Load')
        .w_TRSERIAL = NVL(TRSERIAL,space(10))
        .w_CODVOC = This.oparentobject .w_TRCODICE
        .w_ROWORD = This.oparentobject .w_CPROWNUM
        .w_TRCODVOC = NVL(TRCODVOC,space(15))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'AGG_SALD')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_TIPCON = space(1)
          .w_CODCON = space(15)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_TRFLMACO = NVL(TRFLMACO,space(1))
          .w_TRCODMAS = NVL(TRCODMAS,space(15))
          * evitabile
          *.link_2_2('Load')
          .w_TRCODCON = NVL(TRCODCON,space(15))
          * evitabile
          *.link_2_3('Load')
          .w_TR_SEGNO = NVL(TR_SEGNO,space(1))
          .w_TR__DAVE = NVL(TR__DAVE,space(1))
          .w_TRIMPDAR = NVL(TRIMPDAR,0)
          .w_TRIMPAVE = NVL(TRIMPAVE,0)
        .w_FLAGAVE = IIF(.w_TRIMPAVE<>0,'=',' ')
        .w_FLAGDAR = IIF(.w_TRIMPDAR<>0,'=',' ')
          .w_TRCODCON = NVL(TRCODCON,space(15))
          * evitabile
          *.link_2_10('Load')
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
          select (this.cTrsName)
          append blank
          replace TRFLMACO with .w_TRFLMACO
          replace TRCODCON with .w_TRCODCON
          replace TRIMPDAR with .w_TRIMPDAR
          replace TRIMPAVE with .w_TRIMPAVE
          replace FLAGAVE with .w_FLAGAVE
          replace FLAGDAR with .w_FLAGDAR
          replace TRSERIAL with .w_TRSERIAL
          replace TRCODCON with .w_TRCODCON
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CODVOC = This.oparentobject .w_TRCODICE
        .w_ROWORD = This.oparentobject .w_CPROWNUM
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TRROWORD=0
      .w_TRDESVOC=space(50)
      .w_TRFLMACO=space(1)
      .w_TRCODMAS=space(15)
      .w_TRCODCON=space(15)
      .w_TR_SEGNO=space(1)
      .w_TR__DAVE=space(1)
      .w_TRIMPDAR=0
      .w_TRIMPAVE=0
      .w_FLAGAVE=space(10)
      .w_FLAGDAR=space(10)
      .w_TRCODESE=space(5)
      .w_TRSERIAL=space(10)
      .w_TRCODCON=space(15)
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_CODVOC=space(15)
      .w_ROWORD=0
      .w_TRCODVOC=space(15)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_TRCODMAS))
         .link_2_2('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_TRCODCON))
         .link_2_3('Full')
        endif
        .DoRTCalc(6,9,.f.)
        .w_FLAGAVE = IIF(.w_TRIMPAVE<>0,'=',' ')
        .w_FLAGDAR = IIF(.w_TRIMPDAR<>0,'=',' ')
        .w_TRCODESE = This.oparentobject.oparentobject .w_CODESE
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_TRCODESE))
         .link_1_3('Full')
        endif
        .DoRTCalc(13,14,.f.)
        if not(empty(.w_TRCODCON))
         .link_2_10('Full')
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
        .DoRTCalc(15,16,.f.)
        .w_CODVOC = This.oparentobject .w_TRCODICE
        .w_ROWORD = This.oparentobject .w_CPROWNUM
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'AGG_SALD')
    this.DoRTCalc(19,19,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'AGG_SALD',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AGG_SALD_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRROWORD,"TRROWORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRDESVOC,"TRDESVOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODESE,"TRCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRSERIAL,"TRSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODVOC,"TRCODVOC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_TRFLMACO C(1);
      ,t_TRCODMAS C(15);
      ,t_TRCODCON C(15);
      ,t_TRIMPDAR N(18,4);
      ,t_TRIMPAVE N(18,4);
      ,TRFLMACO C(1);
      ,TRCODCON C(15);
      ,TRIMPDAR N(18,4);
      ,TRIMPAVE N(18,4);
      ,FLAGAVE C(10);
      ,FLAGDAR C(10);
      ,TRSERIAL C(10);
      ,CPROWNUM N(10);
      ,t_TR_SEGNO C(1);
      ,t_TR__DAVE C(1);
      ,t_FLAGAVE C(10);
      ,t_FLAGDAR C(10);
      ,t_TIPCON C(1);
      ,t_CODCON C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_masbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRFLMACO_2_1.controlsource=this.cTrsName+'.t_TRFLMACO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODMAS_2_2.controlsource=this.cTrsName+'.t_TRCODMAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODCON_2_3.controlsource=this.cTrsName+'.t_TRCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRIMPDAR_2_6.controlsource=this.cTrsName+'.t_TRIMPDAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRIMPAVE_2_7.controlsource=this.cTrsName+'.t_TRIMPAVE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(38)
    this.AddVLine(172)
    this.AddVLine(296)
    this.AddVLine(438)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRFLMACO_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.AGG_SALD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_SALD_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AGG_SALD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGG_SALD_IDX,2])
      *
      * insert into AGG_SALD
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AGG_SALD')
        i_extval=cp_InsertValODBCExtFlds(this,'AGG_SALD')
        i_cFldBody=" "+;
                  "(TRROWORD,TRDESVOC,TRFLMACO,TRCODMAS,TRCODCON"+;
                  ",TR_SEGNO,TR__DAVE,TRIMPDAR,TRIMPAVE,TRCODESE"+;
                  ",TRSERIAL,TRCODVOC,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TRROWORD)+","+cp_ToStrODBC(this.w_TRDESVOC)+","+cp_ToStrODBC(this.w_TRFLMACO)+","+cp_ToStrODBCNull(this.w_TRCODMAS)+","+cp_ToStrODBCNull(this.w_TRCODCON)+;
             ","+cp_ToStrODBC(this.w_TR_SEGNO)+","+cp_ToStrODBC(this.w_TR__DAVE)+","+cp_ToStrODBC(this.w_TRIMPDAR)+","+cp_ToStrODBC(this.w_TRIMPAVE)+","+cp_ToStrODBCNull(this.w_TRCODESE)+;
             ","+cp_ToStrODBC(this.w_TRSERIAL)+","+cp_ToStrODBC(this.w_TRCODVOC)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AGG_SALD')
        i_extval=cp_InsertValVFPExtFlds(this,'AGG_SALD')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'TRROWORD',this.w_TRROWORD,'TRCODVOC',this.w_TRCODVOC)
        INSERT INTO (i_cTable) (;
                   TRROWORD;
                  ,TRDESVOC;
                  ,TRFLMACO;
                  ,TRCODMAS;
                  ,TRCODCON;
                  ,TR_SEGNO;
                  ,TR__DAVE;
                  ,TRIMPDAR;
                  ,TRIMPAVE;
                  ,TRCODESE;
                  ,TRSERIAL;
                  ,TRCODVOC;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_TRROWORD;
                  ,this.w_TRDESVOC;
                  ,this.w_TRFLMACO;
                  ,this.w_TRCODMAS;
                  ,this.w_TRCODCON;
                  ,this.w_TR_SEGNO;
                  ,this.w_TR__DAVE;
                  ,this.w_TRIMPDAR;
                  ,this.w_TRIMPAVE;
                  ,this.w_TRCODESE;
                  ,this.w_TRSERIAL;
                  ,this.w_TRCODVOC;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.AGG_SALD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGG_SALD_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (Not Empty(t_TRCODCON)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'AGG_SALD')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " TRDESVOC="+cp_ToStrODBC(this.w_TRDESVOC)+;
                 ",TRCODESE="+cp_ToStrODBCNull(this.w_TRCODESE)+;
                 ",TRSERIAL="+cp_ToStrODBC(this.w_TRSERIAL)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'AGG_SALD')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  TRDESVOC=this.w_TRDESVOC;
                 ,TRCODESE=this.w_TRCODESE;
                 ,TRSERIAL=this.w_TRSERIAL;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (Not Empty(t_TRCODCON)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update AGG_SALD
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'AGG_SALD')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " TRDESVOC="+cp_ToStrODBC(this.w_TRDESVOC)+;
                     ",TRFLMACO="+cp_ToStrODBC(this.w_TRFLMACO)+;
                     ",TRCODMAS="+cp_ToStrODBCNull(this.w_TRCODMAS)+;
                     ",TRCODCON="+cp_ToStrODBCNull(this.w_TRCODCON)+;
                     ",TR_SEGNO="+cp_ToStrODBC(this.w_TR_SEGNO)+;
                     ",TR__DAVE="+cp_ToStrODBC(this.w_TR__DAVE)+;
                     ",TRIMPDAR="+cp_ToStrODBC(this.w_TRIMPDAR)+;
                     ",TRIMPAVE="+cp_ToStrODBC(this.w_TRIMPAVE)+;
                     ",TRCODESE="+cp_ToStrODBCNull(this.w_TRCODESE)+;
                     ",TRSERIAL="+cp_ToStrODBC(this.w_TRSERIAL)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'AGG_SALD')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      TRDESVOC=this.w_TRDESVOC;
                     ,TRFLMACO=this.w_TRFLMACO;
                     ,TRCODMAS=this.w_TRCODMAS;
                     ,TRCODCON=this.w_TRCODCON;
                     ,TR_SEGNO=this.w_TR_SEGNO;
                     ,TR__DAVE=this.w_TR__DAVE;
                     ,TRIMPDAR=this.w_TRIMPDAR;
                     ,TRIMPAVE=this.w_TRIMPAVE;
                     ,TRCODESE=this.w_TRCODESE;
                     ,TRSERIAL=this.w_TRSERIAL;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2,i_cOp3

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.BIL_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BIL_DETT_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..FLAGDAR,space(10))==this.w_FLAGDAR;
              and NVL(&i_cF..TRIMPDAR,0)==this.w_TRIMPDAR;
              and NVL(&i_cF..FLAGAVE,space(10))==this.w_FLAGAVE;
              and NVL(&i_cF..TRIMPAVE,0)==this.w_TRIMPAVE;
              and NVL(&i_cF..CPROWNUM*10,.null.)==this.w_CPROWNUM*10;
              and NVL(&i_cF..TRCODCON,space(15))==this.w_TRCODCON;
              and NVL(&i_cF..TRSERIAL,space(10))==this.w_TRSERIAL;
              and NVL(&i_cF..TRFLMACO,space(1))==this.w_TRFLMACO;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..FLAGDAR,space(10)),'SLIMPDAR','',NVL(&i_cF..TRIMPDAR,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..FLAGAVE,space(10)),'SLIMPAVE','',NVL(&i_cF..TRIMPAVE,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..TRCODCON,space(15))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" SLIMPDAR="+i_cOp1+","           +" SLIMPAVE="+i_cOp2+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SLCODCON="+cp_ToStrODBC(NVL(&i_cF..TRCODCON,space(15)));
             +" AND SLSERIAL="+cp_ToStrODBC(NVL(&i_cF..TRSERIAL,space(10)));
             +" AND SLFLMACO="+cp_ToStrODBC(NVL(&i_cF..TRFLMACO,space(1)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..FLAGDAR,'SLIMPDAR',i_cF+'.TRIMPDAR',&i_cF..TRIMPDAR,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..FLAGAVE,'SLIMPAVE',i_cF+'.TRIMPAVE',&i_cF..TRIMPAVE,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SLSERIAL',&i_cF..TRSERIAL;
                 ,'SLFLMACO',&i_cF..TRFLMACO;
                 ,'SLCODCON',&i_cF..TRCODCON)
      UPDATE (i_cTable) SET ;
           SLIMPDAR=&i_cOp1.  ,;
           SLIMPAVE=&i_cOp2.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2,i_cOp3

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.BIL_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BIL_DETT_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..FLAGDAR,space(10))==this.w_FLAGDAR;
              and NVL(&i_cF..TRIMPDAR,0)==this.w_TRIMPDAR;
              and NVL(&i_cF..FLAGAVE,space(10))==this.w_FLAGAVE;
              and NVL(&i_cF..TRIMPAVE,0)==this.w_TRIMPAVE;
              and NVL(&i_cF..CPROWNUM*10,.null.)==this.w_CPROWNUM*10;
              and NVL(&i_cF..TRCODCON,space(15))==this.w_TRCODCON;
              and NVL(&i_cF..TRSERIAL,space(10))==this.w_TRSERIAL;
              and NVL(&i_cF..TRFLMACO,space(1))==this.w_TRFLMACO;

      i_cOp1=cp_SetTrsOp(this.w_FLAGDAR,'SLIMPDAR','this.w_TRIMPDAR',this.w_TRIMPDAR,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLAGAVE,'SLIMPAVE','this.w_TRIMPAVE',this.w_TRIMPAVE,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_TRCODCON)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" SLIMPDAR="+i_cOp1  +",";
         +" SLIMPAVE="+i_cOp2  +",";
         +" CPROWORD="+cp_ToStrODBC(this.w_CPROWNUM*10)  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SLCODCON="+cp_ToStrODBC(this.w_TRCODCON);
           +" AND SLSERIAL="+cp_ToStrODBC(this.w_TRSERIAL);
           +" AND SLFLMACO="+cp_ToStrODBC(this.w_TRFLMACO);
           )
        if i_nModRow<1 .and. .not. empty(this.w_TRCODCON)
        i_cOp1=cp_SetTrsOp(this.w_FLAGDAR,'SLIMPDAR','this.w_TRIMPDAR',this.w_TRIMPDAR,'insert',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_FLAGAVE,'SLIMPAVE','this.w_TRIMPAVE',this.w_TRIMPAVE,'insert',i_nConn)
          =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" (SLCODCON,SLSERIAL,SLFLMACO  ;
             ,SLIMPDAR"+",SLIMPAVE"+",CPROWORD,CPCCCHK) VALUES ("+cp_ToStrODBC(this.w_TRCODCON)+","+cp_ToStrODBC(this.w_TRSERIAL)+","+cp_ToStrODBC(this.w_TRFLMACO)  ;
             +","+i_cOp1;
             +","+i_cOp2;
             +","+cp_ToStrODBC(this.w_CPROWNUM*10);
             +","+cp_ToStrODBC(cp_NewCCChk())+")")
        endif
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_FLAGDAR,'SLIMPDAR','this.w_TRIMPDAR',this.w_TRIMPDAR,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_FLAGAVE,'SLIMPAVE','this.w_TRIMPAVE',this.w_TRIMPAVE,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SLSERIAL',this.w_TRSERIAL;
                 ,'SLFLMACO',this.w_TRFLMACO;
                 ,'SLCODCON',this.w_TRCODCON)
      UPDATE (i_cTable) SET;
           SLIMPDAR=&i_cOp1.  ,;
           SLIMPAVE=&i_cOp2.  ,;
           CPROWORD=this.w_CPROWNUM*10  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
      i_nModRow = _tally
      if i_nModRow<1 .and. .not. empty(this.w_TRCODCON)
        i_cOp1=cp_SetTrsOp(this.w_FLAGDAR,'SLIMPDAR','this.w_TRIMPDAR',this.w_TRIMPDAR,'insert',0)
        i_cOp2=cp_SetTrsOp(this.w_FLAGAVE,'SLIMPAVE','this.w_TRIMPAVE',this.w_TRIMPAVE,'insert',0)
      cp_CheckDeletedKey(i_cTable,0,'SLSERIAL',this.w_TRSERIAL,'SLFLMACO',this.w_TRFLMACO,'SLCODCON',this.w_TRCODCON)
        INSERT INTO (i_cTable) (SLCODCON,SLSERIAL,SLFLMACO  ;
         ,SLIMPDAR,SLIMPAVE,CPROWORD,CPCCCHK) VALUES (this.w_TRCODCON,this.w_TRSERIAL,this.w_TRFLMACO  ;
           ,&i_cOp1.  ;
           ,&i_cOp2.  ;
           ,this.w_CPROWNUM*10  ,cp_NewCCChk())
      endif
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AGG_SALD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGG_SALD_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (Not Empty(t_TRCODCON)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete AGG_SALD
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (Not Empty(t_TRCODCON)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AGG_SALD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_SALD_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_2_2('Full')
        if .o_TRFLMACO<>.w_TRFLMACO
          .link_2_3('Full')
        endif
        .DoRTCalc(6,9,.t.)
        if .o_TRIMPAVE<>.w_TRIMPAVE
          .w_FLAGAVE = IIF(.w_TRIMPAVE<>0,'=',' ')
        endif
        if .o_TRIMPDAR<>.w_TRIMPDAR
          .w_FLAGDAR = IIF(.w_TRIMPDAR<>0,'=',' ')
        endif
          .w_TRCODESE = This.oparentobject.oparentobject .w_CODESE
          .link_1_3('Full')
        .DoRTCalc(13,13,.t.)
          .link_2_10('Full')
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
        .DoRTCalc(15,16,.t.)
          .w_CODVOC = This.oparentobject .w_TRCODICE
          .w_ROWORD = This.oparentobject .w_CPROWNUM
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TR_SEGNO with this.w_TR_SEGNO
      replace t_TR__DAVE with this.w_TR__DAVE
      replace t_FLAGAVE with this.w_FLAGAVE
      replace t_FLAGDAR with this.w_FLAGDAR
      replace t_TRCODCON with this.w_TRCODCON
      replace t_TIPCON with this.w_TIPCON
      replace t_CODCON with this.w_CODCON
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCODCON_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCODCON_2_3.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TRCODMAS
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODMAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODMAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_TRCODMAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_TRCODMAS)
            select MCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODMAS = NVL(_Link_.MCCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODMAS = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODMAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRCODCON
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_TRCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TRFLMACO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCONSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TRFLMACO;
                     ,'ANCODICE',trim(this.w_TRCODCON))
          select ANTIPCON,ANCODICE,ANCONSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oTRCODCON_2_3'),i_cWhere,'GSAR_BZC',"Conti\clienti\fornitori",'GSCG_MAS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TRFLMACO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANCONSUP;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto non appartenente al dettaglio voce indicato nella struttura")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TRFLMACO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_TRCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TRFLMACO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TRFLMACO;
                       ,'ANCODICE',this.w_TRCODCON)
            select ANTIPCON,ANCODICE,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_TRCODMAS = NVL(_Link_.ANCONSUP,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODCON = space(15)
      endif
      this.w_TRCODMAS = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=chkapvoc(.w_ROWORD,.w_CODVOC,.w_TRFLMACO,.w_TRCODCON)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto non appartenente al dettaglio voce indicato nella struttura")
        endif
        this.w_TRCODCON = space(15)
        this.w_TRCODMAS = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRCODESE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BIL_MAST_IDX,3]
    i_lTable = "BIL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BIL_MAST_IDX,2], .t., this.BIL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BIL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SLCODESE,SLSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where SLCODESE="+cp_ToStrODBC(this.w_TRCODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SLCODESE',this.w_TRCODESE)
            select SLCODESE,SLSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODESE = NVL(_Link_.SLCODESE,space(5))
      this.w_TRSERIAL = NVL(_Link_.SLSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODESE = space(5)
      endif
      this.w_TRSERIAL = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BIL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.SLCODESE,1)
      cp_ShowWarn(i_cKey,this.BIL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRCODCON
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BIL_DETT_IDX,3]
    i_lTable = "BIL_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BIL_DETT_IDX,2], .t., this.BIL_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BIL_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SLSERIAL,SLFLMACO,SLCODCON";
                   +" from "+i_cTable+" "+i_lTable+" where SLCODCON="+cp_ToStrODBC(this.w_TRCODCON);
                   +" and SLSERIAL="+cp_ToStrODBC(this.w_TRSERIAL);
                   +" and SLFLMACO="+cp_ToStrODBC(this.w_TRFLMACO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SLSERIAL',this.w_TRSERIAL;
                       ,'SLFLMACO',this.w_TRFLMACO;
                       ,'SLCODCON',this.w_TRCODCON)
            select SLSERIAL,SLFLMACO,SLCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
    else
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BIL_DETT_IDX,2])+'\'+cp_ToStr(_Link_.SLSERIAL,1)+'\'+cp_ToStr(_Link_.SLFLMACO,1)+'\'+cp_ToStr(_Link_.SLCODCON,1)
      cp_ShowWarn(i_cKey,this.BIL_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRFLMACO_2_1.value==this.w_TRFLMACO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRFLMACO_2_1.value=this.w_TRFLMACO
      replace t_TRFLMACO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRFLMACO_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODMAS_2_2.value==this.w_TRCODMAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODMAS_2_2.value=this.w_TRCODMAS
      replace t_TRCODMAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODMAS_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODCON_2_3.value==this.w_TRCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODCON_2_3.value=this.w_TRCODCON
      replace t_TRCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODCON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRIMPDAR_2_6.value==this.w_TRIMPDAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRIMPDAR_2_6.value=this.w_TRIMPDAR
      replace t_TRIMPDAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRIMPDAR_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRIMPAVE_2_7.value==this.w_TRIMPAVE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRIMPAVE_2_7.value=this.w_TRIMPAVE
      replace t_TRIMPAVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRIMPAVE_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'AGG_SALD')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_TRFLMACO $ 'C-F-G') and (Not Empty(.w_TRCODCON))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRFLMACO_2_1
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(chkapvoc(.w_ROWORD,.w_CODVOC,.w_TRFLMACO,.w_TRCODCON)) and (Not Empty(.w_TRFLMACO)) and not(empty(.w_TRCODCON)) and (Not Empty(.w_TRCODCON))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODCON_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Conto non appartenente al dettaglio voce indicato nella struttura")
      endcase
      if Not Empty(.w_TRCODCON)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TRFLMACO = this.w_TRFLMACO
    this.o_TRIMPDAR = this.w_TRIMPDAR
    this.o_TRIMPAVE = this.w_TRIMPAVE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(Not Empty(t_TRCODCON))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=.F.
    if !i_bRes
      cp_ErrorMsg("Operazione non consentita!","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_TRFLMACO=space(1)
      .w_TRCODMAS=space(15)
      .w_TRCODCON=space(15)
      .w_TR_SEGNO=space(1)
      .w_TR__DAVE=space(1)
      .w_TRIMPDAR=0
      .w_TRIMPAVE=0
      .w_FLAGAVE=space(10)
      .w_FLAGDAR=space(10)
      .w_TRCODCON=space(15)
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_TRCODMAS))
        .link_2_2('Full')
      endif
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_TRCODCON))
        .link_2_3('Full')
      endif
      .DoRTCalc(6,9,.f.)
        .w_FLAGAVE = IIF(.w_TRIMPAVE<>0,'=',' ')
        .w_FLAGDAR = IIF(.w_TRIMPDAR<>0,'=',' ')
      .DoRTCalc(12,14,.f.)
      if not(empty(.w_TRCODCON))
        .link_2_10('Full')
      endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
    endwith
    this.DoRTCalc(15,19,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_TRFLMACO = t_TRFLMACO
    this.w_TRCODMAS = t_TRCODMAS
    this.w_TRCODCON = t_TRCODCON
    this.w_TR_SEGNO = t_TR_SEGNO
    this.w_TR__DAVE = t_TR__DAVE
    this.w_TRIMPDAR = t_TRIMPDAR
    this.w_TRIMPAVE = t_TRIMPAVE
    this.w_FLAGAVE = t_FLAGAVE
    this.w_FLAGDAR = t_FLAGDAR
    this.w_TRCODCON = t_TRCODCON
    this.w_TIPCON = t_TIPCON
    this.w_CODCON = t_CODCON
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_TRFLMACO with this.w_TRFLMACO
    replace t_TRCODMAS with this.w_TRCODMAS
    replace t_TRCODCON with this.w_TRCODCON
    replace t_TR_SEGNO with this.w_TR_SEGNO
    replace t_TR__DAVE with this.w_TR__DAVE
    replace t_TRIMPDAR with this.w_TRIMPDAR
    replace t_TRIMPAVE with this.w_TRIMPAVE
    replace t_FLAGAVE with this.w_FLAGAVE
    replace t_FLAGDAR with this.w_FLAGDAR
    replace t_TRCODCON with this.w_TRCODCON
    replace t_TIPCON with this.w_TIPCON
    replace t_CODCON with this.w_CODCON
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_masPag1 as StdContainer
  Width  = 601
  height = 226
  stdWidth  = 601
  stdheight = 226
  resizeXpos=273
  resizeYpos=124
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=8, width=587,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="TRFLMACO",Label1="Tipo",Field2="TRCODCON",Label2="Codice conto",Field3="TRCODMAS",Label3="Mastro",Field4="TRIMPDAR",Label4="Dare",Field5="TRIMPAVE",Label5="Avere",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185402490

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=28,;
    width=588+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=29,width=587+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CONTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oTRCODCON_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_masBodyRow as CPBodyRowCnt
  Width=578
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oTRFLMACO_2_1 as StdTrsField with uid="NFBOSMLUKP",rtseq=3,rtrep=.t.,;
    cFormVar="w_TRFLMACO",value=space(1),;
    HelpContextID = 240554629,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=23, Left=3, Top=0, cSayPict=['!'], cGetPict=['!'], InputMask=replicate('X',1)

  func oTRFLMACO_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TRFLMACO $ 'C-F-G')
      if .not. empty(.w_TRCODCON)
        bRes2=.link_2_3('Full')
      endif
      if .not. empty(.w_TRCODCON)
        bRes2=.link_2_10('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oTRCODMAS_2_2 as StdTrsField with uid="YOGJFPZGKX",rtseq=4,rtrep=.t.,;
    cFormVar="w_TRCODMAS",value=space(15),enabled=.f.,;
    HelpContextID = 104242551,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=171, Top=0, InputMask=replicate('X',15), cLinkFile="MASTRI", oKey_1_1="MCCODICE", oKey_1_2="this.w_TRCODMAS"

  func oTRCODMAS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oTRCODCON_2_3 as StdTrsField with uid="TYXHMUUTLD",rtseq=5,rtrep=.t.,;
    cFormVar="w_TRCODCON",value=space(15),;
    HelpContextID = 3579260,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Conto non appartenente al dettaglio voce indicato nella struttura",;
   bGlobalFont=.t.,;
    Height=17, Width=123, Left=38, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TRFLMACO", oKey_2_1="ANCODICE", oKey_2_2="this.w_TRCODCON"

  func oTRCODCON_2_3.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_TRFLMACO))
    endwith
  endfunc

  func oTRCODCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCODCON_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oTRCODCON_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TRFLMACO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TRFLMACO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oTRCODCON_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti\clienti\fornitori",'GSCG_MAS.CONTI_VZM',this.parent.oContained
  endproc
  proc oTRCODCON_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TRFLMACO
     i_obj.w_ANCODICE=this.parent.oContained.w_TRCODCON
    i_obj.ecpSave()
  endproc

  add object oTRIMPDAR_2_6 as StdTrsField with uid="NBRTILYFDY",rtseq=8,rtrep=.t.,;
    cFormVar="w_TRIMPDAR",value=0,;
    HelpContextID = 242761080,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=292, Top=0, cSayPict=[v_PV(38+VVP)], cGetPict=[v_GV(38+VVP)]

  add object oTRIMPAVE_2_7 as StdTrsField with uid="FSVXEQFSQU",rtseq=9,rtrep=.t.,;
    cFormVar="w_TRIMPAVE",value=0,;
    HelpContextID = 243778171,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=434, Top=0, cSayPict=[v_PV(38+VVP)], cGetPict=[v_GV(38+VVP)]

  add object oObj_2_11 as cp_runprogram with uid="JTINESMTIF",width=192,height=24,;
   left=-2, top=203,;
    caption='GSCG_BTR(C)',;
   bGlobalFont=.t.,;
    prg="GSCG_BTR('C')",;
    cEvent = "Blank",;
    nPag=2;
    , HelpContextID = 7616568
  add object oLast as LastKeyMover
  * ---
  func oTRFLMACO_2_1.When()
    return(.t.)
  proc oTRFLMACO_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oTRFLMACO_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mas','AGG_SALD','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TRROWORD=AGG_SALD.TRROWORD";
  +" and "+i_cAliasName2+".TRCODVOC=AGG_SALD.TRCODVOC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
