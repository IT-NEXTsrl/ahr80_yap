* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_gvf                                                        *
*              Gadget Visual Mask                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-03-31                                                      *
* Last revis.: 2015-12-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_gvf",oParentObject))

* --- Class definition
define class tgsut_gvf as StdGadgetForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 199
  Height = 165
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-03"
  HelpContextID=53035881
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_gvf"
  cComment = "Gadget Visual Mask"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_VFNAME = space(100)
  w_FONTCLR = 0
  w_TITLE = space(254)
  o_TITLE = space(254)
  w_RET = .F.
  w_VFOBJ = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_gvf
  *--- ApplyTheme
  Proc ApplyTheme()
     DoDefault()
     Local l_oldErr, l_OnErrorMsg
     With This
       m.l_OnErrorMsg = ""
       m.l_oldErr = On("Error")
       On Error m.l_OnErrorMsg=ah_MsgFormat("%1%0%2",Iif(Empty(m.l_OnErrorMsg), "Aggiornamento gadget:",m.l_OnErrorMsg),Message())
       
       .w_FONTCLR = Iif(.nFontColor<>-1, .nFontColor, Rgb(243,243,243))
       
       On Error &l_oldErr
       If !Empty(m.l_OnErrorMsg) && c'� stato un errore di programma
          .cAlertMsg = Iif(Empty(.cAlertMsg), m.l_OnErrorMsg, ah_MsgFormat("%1%0%2",.cAlertMsg,m.l_OnErrorMsg))
       Endif
       
       .mCalc(.t.)
       .SaveDependsOn()
     Endwith
  EndProc
  * --- Fine Area Manuale
  *--- Define Header
  add object oHeader as cp_headergadget with Width=this.Width, Left=0, Top=0, nNumPages=1 
  Height = This.Height + this.oHeader.Height

  * --- Define Page Frame
  add object oPgFrm as cp_oPgFrm with PageCount=1, Top=this.oHeader.Height, Width=this.Width, Height=this.Height-this.oHeader.Height
  proc oPgFrm.Init
    cp_oPgFrm::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_gvfPag1","gsut_gvf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_VFOBJ = this.oPgFrm.Pages(1).oPag.VFOBJ
    DoDefault()
    proc Destroy()
      this.w_VFOBJ = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VFNAME=space(100)
      .w_FONTCLR=0
      .w_TITLE=space(254)
      .w_RET=.f.
      .oPgFrm.Page1.oPag.VFOBJ.Calculate(.w_VFNAME)
          .DoRTCalc(1,1,.f.)
        .w_FONTCLR = Rgb(243,243,243)
    endwith
    this.DoRTCalc(3,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.VFOBJ.Calculate(.w_VFNAME)
      	*--- Assegnamento caption ad oggetto header
          If .o_TITLE <> .w_TITLE
			   .oHeader.Calculate(.oPgFrm.Page1.oPag.oContained.w_TITLE)
          EndIf
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.VFOBJ.Calculate(.w_VFNAME)
    endwith
  return

  proc Calculate_LSNUCPBSNU()
    with this
          * --- Dimensioni gadget massimizzato
          .w_RET = CalcSize(This)
    endwith
  endproc
  proc Calculate_QQYAJZWBHK()
    with this
          * --- Verifica esistenza file.VFM
          .cAlertMsg = Iif(Empty(.w_VFNAME) Or !cp_FileExist(ForceExt(.w_VFNAME,'VFM')), ah_MsgFormat('Maschera non definita inesistente:%0"%1"', Alltrim(.w_VFNAME)), '')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.VFOBJ.Event(cEvent)
        if lower(cEvent)==lower("GadgetMaximize Init") or lower(cEvent)==lower("GadgetMinimize Init")
          .Calculate_LSNUCPBSNU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("GadgetArranged")
          .Calculate_QQYAJZWBHK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_gvf
    If (Lower(cEvent)='gadgetarranged' Or Lower(cEvent)='w_vfobj cnt created')
       If This.w_VFObj.Visible
         This.w_TITLE = iif(Type("This.w_VFOBJ.cnt.Caption")="U" Or Empty(This.w_VFOBJ.cnt.Caption),This.w_TITLE,This.w_VFOBJ.cnt.Caption)
         This.mCalc(.T.)
       Endif
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TITLE = this.w_TITLE
    return

enddefine

* --- Define pages as container
define class tgsut_gvfPag1 as StdContainer
  Width  = 199
  height = 165
  stdWidth  = 199
  stdheight = 165
  resizeXpos=105
  resizeYpos=83
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object VFOBJ as cp_vfmviewer with uid="NOUUWTPERP",left=-2, top=0, width=198,height=169,;
    caption='Object',;
   bGlobalFont=.t.,;
    cEvent = "GadgetArranged,GadgetOnDemand,GadgetOnTimer,oheader RefreshOnDemand",;
    nPag=1;
    , HelpContextID = 124961766
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_gvf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_gvf
Proc CalcSize(pParent)
  With m.pParent
     If .w_VFObj.Visible && solo se � stata caricata una maschera
       If .MaxWidth=-1 And .MaxHeight=-1
         local nWidth,nHeight
         *--- Header e testata mask
         m.nHeight = .w_VFObj.cnt.Height + .oHeader.Height
         m.nWidth = .w_VFObj.cnt.Width       
         .MaxWidth = m.nWidth
         .MaxHeight = m.nHeight
       Else
         .MaxWidth = -1
         .MaxHeight = -1
      Endif
    Endif
  EndWith
EndProc
* --- Fine Area Manuale
