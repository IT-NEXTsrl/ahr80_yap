* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bcd                                                        *
*              Calcola date trimestre prec.                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_13]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-06-07                                                      *
* Last revis.: 2002-06-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bcd",oParentObject,m.pParame)
return(i_retval)

define class tgscg_bcd as StdBatch
  * --- Local variables
  pParame = space(1)
  w_ANNOPREC = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSCG_KIA
    * ---  - Parametro I = Inizializza Anno e Periodo e Date Trimestre Precedente in funzione della Data Ultimo Storno
    * ---  - Parametro C = Calcola Date Trimestre Precedente al variare dell'Anno o del Periodo
    do case
      case this.pParame="I"
        * ---  - Parametro I = Inizializza Anno e Periodo e Date Trimestre Precedente in funzione della Data Ultimo Storno
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZDATAUT"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZDATAUT;
            from (i_cTable) where;
                AZCODAZI = i_codazi;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DATAUT = NVL(cp_ToDate(_read_.AZDATAUT),cp_NullValue(_read_.AZDATAUT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.oParentObject.w_DATAUT)
          this.oParentObject.w_ANNO = ALLTRIM(STR(YEAR(this.oParentObject.w_DATAUT),4,0))
          do case
            case day(this.oParentObject.w_DATAUT)=31 and month(this.oParentObject.w_DATAUT)=3 
              this.oParentObject.w_NUMPER = IIF(g_TIPDEN="T", 2, 6)
              this.oParentObject.w_DATINI = cp_CharToDate("01-01-" + this.oParentObject.w_ANNO)
              this.oParentObject.w_DATFIN = cp_CharToDate("31-03-" + this.oParentObject.w_ANNO)
            case day(this.oParentObject.w_DATAUT)=30 and month(this.oParentObject.w_DATAUT)=6
              this.oParentObject.w_NUMPER = IIF(g_TIPDEN="T", 3, 9)
              this.oParentObject.w_DATINI = cp_CharToDate("01-04-" + this.oParentObject.w_ANNO)
              this.oParentObject.w_DATFIN = cp_CharToDate("30-06-" + this.oParentObject.w_ANNO)
            case day(this.oParentObject.w_DATAUT)=30 and month(this.oParentObject.w_DATAUT)=9
              this.oParentObject.w_NUMPER = IIF(g_TIPDEN="T", 4, 12)
              this.oParentObject.w_DATINI = cp_CharToDate("01-07-" + this.oParentObject.w_ANNO)
              this.oParentObject.w_DATFIN = cp_CharToDate("30-09-" + this.oParentObject.w_ANNO)
            case day(this.oParentObject.w_DATAUT)=31 and month(this.oParentObject.w_DATAUT)=12
              this.oParentObject.w_NUMPER = IIF(g_TIPDEN="T", 1, 3)
              this.oParentObject.w_DATINI = cp_CharToDate("01-10-" + this.oParentObject.w_ANNO)
              this.oParentObject.w_DATFIN = cp_CharToDate("31-12-" + this.oParentObject.w_ANNO)
              this.oParentObject.w_ANNO = ALLTRIM(STR(VAL(this.oParentObject.w_ANNO)+1,4,0))
            otherwise
              * --- Nel caso di Data Ultimo Storno incongruente viene inizializzzato solo l'Anno (tramite la data di sistema)
              this.oParentObject.w_ANNO = ALLTRIM(STR(YEAR(i_DATSYS),4,0))
              this.oParentObject.w_NUMPER = 0
              this.oParentObject.w_DATINI = cp_CharToDate("  -  -  ")
              this.oParentObject.w_DATFIN = cp_CharToDate("  -  -  ")
              ah_ErrorMsg("Controllare i parametri IVA.%0Data ultimo storno incongruente",,"")
          endcase
        else
          * --- Nel caso di Data Ultimo Storno vuota viene inizializzzato solo l'Anno (tramite la data di sistema)
          this.oParentObject.w_ANNO = ALLTRIM(STR(YEAR(i_DATSYS),4,0))
          this.oParentObject.w_NUMPER = 0
          this.oParentObject.w_DATINI = cp_CharToDate("  -  -  ")
          this.oParentObject.w_DATFIN = cp_CharToDate("  -  -  ")
        endif
      case this.pParame="C"
        * ---  - Parametro C = Calcola Date Trimestre Precedente al variare dell'Anno o del Periodo
        if NOT EMPTY(this.oParentObject.w_ANNO) AND NOT EMPTY(this.oParentObject.w_NUMPER)
          if G_TIPDEN="T" OR MOD(this.oParentObject.w_NUMPER, 3)=0
            do case
              case (g_TIPDEN="T" AND this.oParentObject.w_NUMPER=4) OR (g_TIPDEN="M" AND this.oParentObject.w_NUMPER=12)
                this.oParentObject.w_DATINI = cp_CharToDate("01-07-" + this.oParentObject.w_ANNO)
                this.oParentObject.w_DATFIN = cp_CharToDate("30-09-" + this.oParentObject.w_ANNO)
              case (g_TIPDEN="T" AND this.oParentObject.w_NUMPER=3) OR (g_TIPDEN="M" AND this.oParentObject.w_NUMPER=9)
                this.oParentObject.w_DATINI = cp_CharToDate("01-04-" + this.oParentObject.w_ANNO)
                this.oParentObject.w_DATFIN = cp_CharToDate("30-06-" + this.oParentObject.w_ANNO)
              case (g_TIPDEN="T" AND this.oParentObject.w_NUMPER=2) OR (g_TIPDEN="M" AND this.oParentObject.w_NUMPER=6)
                this.oParentObject.w_DATINI = cp_CharToDate("01-01-" + this.oParentObject.w_ANNO)
                this.oParentObject.w_DATFIN = cp_CharToDate("31-03-" + this.oParentObject.w_ANNO)
              case (g_TIPDEN="T" AND this.oParentObject.w_NUMPER=1) OR (g_TIPDEN="M" AND this.oParentObject.w_NUMPER=3)
                this.w_ANNOPREC = VAL(this.oParentObject.w_ANNO)-1
                this.oParentObject.w_DATINI = cp_CharToDate("01-10-" + RIGHT("0000"+ALLTRIM(STR(this.w_ANNOPREC)), 4))
                this.oParentObject.w_DATFIN = cp_CharToDate("31-12-" + RIGHT("0000"+ALLTRIM(STR(this.w_ANNOPREC)), 4))
            endcase
          else
            this.oParentObject.w_DATINI = cp_CharToDate("  -  -  ")
            this.oParentObject.w_DATFIN = cp_CharToDate("  -  -  ")
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
