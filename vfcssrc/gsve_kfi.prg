* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kfi                                                        *
*              Ricalcola codici I.V.A. (Filtri aggiuntivi)                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-09-13                                                      *
* Last revis.: 2011-09-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kfi",oParentObject))

* --- Class definition
define class tgsve_kfi as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 610
  Height = 349
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-09-16"
  HelpContextID=48902761
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsve_kfi"
  cComment = "Ricalcola codici I.V.A. (Filtri aggiuntivi)"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_RIIVDTIN = ctod('  /  /  ')
  o_RIIVDTIN = ctod('  /  /  ')
  w_RIIVDTFI = ctod('  /  /  ')
  w_SELEZI = space(1)
  o_SELEZI = space(1)
  w_RIIVLSIV = space(0)
  w_ZoomCodIva = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kfiPag1","gsve_kfi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRIIVDTIN_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomCodIva = this.oPgFrm.Pages(1).oPag.ZoomCodIva
    DoDefault()
    proc Destroy()
      this.w_ZoomCodIva = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RIIVDTIN=ctod("  /  /  ")
      .w_RIIVDTFI=ctod("  /  /  ")
      .w_SELEZI=space(1)
      .w_RIIVLSIV=space(0)
      .w_RIIVDTIN=oParentObject.w_RIIVDTIN
      .w_RIIVDTFI=oParentObject.w_RIIVDTFI
      .w_RIIVLSIV=oParentObject.w_RIIVLSIV
        .w_RIIVDTIN = cp_chartodate("  -  -    ")
      .oPgFrm.Page1.oPag.ZoomCodIva.Calculate()
          .DoRTCalc(2,2,.f.)
        .w_SELEZI = 'D'
        .w_RIIVLSIV = ""
      .oPgFrm.Page1.oPag.oObj_1_12.Calculate(Ah_msgFormat("Attenzione non applicare nessuna selezione di filtro%0significa ricalcolare i codici iva per tutte le righe dei documenti importati"))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_RIIVDTIN=.w_RIIVDTIN
      .oParentObject.w_RIIVDTFI=.w_RIIVDTFI
      .oParentObject.w_RIIVLSIV=.w_RIIVLSIV
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_RIIVDTIN<>.w_RIIVDTIN
            .w_RIIVDTFI = .w_RIIVDTIN
        endif
        .oPgFrm.Page1.oPag.ZoomCodIva.Calculate()
        if .o_SELEZI<>.w_SELEZI
          .Calculate_MUMFCOBXKJ()
        endif
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(Ah_msgFormat("Attenzione non applicare nessuna selezione di filtro%0significa ricalcolare i codici iva per tutte le righe dei documenti importati"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomCodIva.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(Ah_msgFormat("Attenzione non applicare nessuna selezione di filtro%0significa ricalcolare i codici iva per tutte le righe dei documenti importati"))
    endwith
  return

  proc Calculate_UAYKKGWNFL()
    with this
          * --- Calcola variabile elanco codici iva
          .oParentObject.w_RIIVLSIV = GSVE_BFI(this, "LIST")
    endwith
  endproc
  proc Calculate_MUMFCOBXKJ()
    with this
          * --- Seleziona tutto
          SelezionaTutto(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomCodIva.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_UAYKKGWNFL()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRIIVDTIN_1_3.value==this.w_RIIVDTIN)
      this.oPgFrm.Page1.oPag.oRIIVDTIN_1_3.value=this.w_RIIVDTIN
    endif
    if not(this.oPgFrm.Page1.oPag.oRIIVDTFI_1_4.value==this.w_RIIVDTFI)
      this.oPgFrm.Page1.oPag.oRIIVDTFI_1_4.value=this.w_RIIVDTFI
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_6.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_RIIVDTIN<=.w_RIIVDTFI)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIIVDTFI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RIIVDTIN = this.w_RIIVDTIN
    this.o_SELEZI = this.w_SELEZI
    return

enddefine

* --- Define pages as container
define class tgsve_kfiPag1 as StdContainer
  Width  = 606
  height = 349
  stdWidth  = 606
  stdheight = 349
  resizeXpos=600
  resizeYpos=166
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRIIVDTIN_1_3 as StdField with uid="IJKEACIWAZ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RIIVDTIN", cQueryName = "RIIVDTIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento iniziale per ricalcolo iva",;
    HelpContextID = 172970908,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=141, Top=7

  add object oRIIVDTFI_1_4 as StdField with uid="BFNFQRSJQS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RIIVDTFI", cQueryName = "RIIVDTFI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento finale per ricalcolo iva",;
    HelpContextID = 172970913,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=359, Top=7

  func oRIIVDTFI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RIIVDTIN<=.w_RIIVDTFI)
    endwith
    return bRes
  endfunc


  add object ZoomCodIva as cp_szoombox with uid="NLFASIYMZX",left=0, top=30, width=607,height=269,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSAR0AIV",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cTable="VOCIIVA",cZoomOnZoom="GSAR_AIV",cMenuFile="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 129094886

  add object oSELEZI_1_6 as StdRadio with uid="RDFUYVXOXW",rtseq=3,rtrep=.f.,left=8, top=308, width=129,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 201316390
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 201316390
      this.Buttons(2).Top=15
      this.SetAll("Width",127)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_6.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_6.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_7 as StdButton with uid="VHHERDESXR",left=499, top=301, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 48882202;
    , tabstop=.f., caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="INZUXVURLA",left=550, top=301, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare";
    , HelpContextID = 96826950;
    , tabstop=.f., caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_12 as cp_calclbl with uid="IYMYWKRDMR",left=139, top=305, width=356,height=37,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",alignment=0,FontSize=8,FontItalic=.t.,ForeColor=255,;
    nPag=1;
    , HelpContextID = 129094886

  add object oStr_1_1 as StdString with uid="EODXTVSEVM",Visible=.t., Left=15, Top=7,;
    Alignment=1, Width=121, Height=18,;
    Caption="Da data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="TECEQROKYJ",Visible=.t., Left=235, Top=7,;
    Alignment=1, Width=120, Height=18,;
    Caption="a data documento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kfi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsve_kfi
proc SelezionaTutto(oObj)
UPDATE ( oObj.w_ZoomCodIva.cCursor ) SET XCHK = iif(oObj.w_SELEZI='S',1,0)
select ( oObj.w_ZoomCodIva.cCursor )
go top
endproc
* --- Fine Area Manuale
