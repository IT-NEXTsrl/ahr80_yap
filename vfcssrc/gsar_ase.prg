* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ase                                                        *
*              Sedi azienda                                                    *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_40]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-06-25                                                      *
* Last revis.: 2014-10-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_ase"))

* --- Class definition
define class tgsar_ase as StdForm
  Top    = 0
  Left   = 6

  * --- Standard Properties
  Width  = 541
  Height = 336+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-28"
  HelpContextID=109672297
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  SEDIAZIE_IDX = 0
  AZIENDA_IDX = 0
  DIPENDEN_IDX = 0
  cFile = "SEDIAZIE"
  cKeySelect = "SECODAZI,SECODDES"
  cKeyWhere  = "SECODAZI=this.w_SECODAZI and SECODDES=this.w_SECODDES"
  cKeyWhereODBC = '"SECODAZI="+cp_ToStrODBC(this.w_SECODAZI)';
      +'+" and SECODDES="+cp_ToStrODBC(this.w_SECODDES)';

  cKeyWhereODBCqualified = '"SEDIAZIE.SECODAZI="+cp_ToStrODBC(this.w_SECODAZI)';
      +'+" and SEDIAZIE.SECODDES="+cp_ToStrODBC(this.w_SECODDES)';

  cPrg = "gsar_ase"
  cComment = "Sedi azienda"
  icon = "anag.ico"
  cAutoZoom = 'GSAR_ASE'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SECODAZI = space(5)
  w_SECODDES = space(5)
  w_SENOMSED = space(40)
  w_SETIPRIF = space(2)
  o_SETIPRIF = space(2)
  w_SEDESSTA = space(1)
  w_SEINDIRI = space(35)
  w_SE___CAP = space(8)
  w_SENATGIU = space(2)
  w_SELOCALI = space(30)
  w_SEPROVIN = space(2)
  w_SETELEFO = space(18)
  w_SE_EMAIL = space(50)
  w_SEUNIORG = space(1)
  w_SE_EMPEC = space(254)
  w_SEPERSRE = space(5)
  w_COGNOM = space(40)
  w_NOME = space(40)
  w_SE__NOTE = space(40)
  w_MESS_ERR = space(100)
  w_SEPREMAP = space(1)
  w_SEDESPRE = space(1)
  w_OLDMAPPE = space(5)
  w_OLDDESPRE = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SEDIAZIE','gsar_ase')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_asePag1","gsar_ase",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Sede")
      .Pages(1).HelpContextID = 102616282
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSECODDES_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='DIPENDEN'
    this.cWorkTables[3]='SEDIAZIE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SEDIAZIE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SEDIAZIE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SECODAZI = NVL(SECODAZI,space(5))
      .w_SECODDES = NVL(SECODDES,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_26_joined
    link_1_26_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SEDIAZIE where SECODAZI=KeySet.SECODAZI
    *                            and SECODDES=KeySet.SECODDES
    *
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SEDIAZIE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SEDIAZIE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SEDIAZIE '
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SECODAZI',this.w_SECODAZI  ,'SECODDES',this.w_SECODDES  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_COGNOM = space(40)
        .w_NOME = space(40)
        .w_MESS_ERR = space(100)
        .w_OLDMAPPE = space(5)
        .w_OLDDESPRE = space(5)
        .w_SECODAZI = NVL(SECODAZI,space(5))
          * evitabile
          *.link_1_1('Load')
        .w_SECODDES = NVL(SECODDES,space(5))
        .w_SENOMSED = NVL(SENOMSED,space(40))
        .w_SETIPRIF = NVL(SETIPRIF,space(2))
        .w_SEDESSTA = NVL(SEDESSTA,space(1))
        .w_SEINDIRI = NVL(SEINDIRI,space(35))
        .w_SE___CAP = NVL(SE___CAP,space(8))
        .w_SENATGIU = NVL(SENATGIU,space(2))
        .w_SELOCALI = NVL(SELOCALI,space(30))
        .w_SEPROVIN = NVL(SEPROVIN,space(2))
        .w_SETELEFO = NVL(SETELEFO,space(18))
        .w_SE_EMAIL = NVL(SE_EMAIL,space(50))
        .w_SEUNIORG = NVL(SEUNIORG,space(1))
        .w_SE_EMPEC = NVL(SE_EMPEC,space(254))
        .w_SEPERSRE = NVL(SEPERSRE,space(5))
          if link_1_26_joined
            this.w_SEPERSRE = NVL(DPCODICE126,NVL(this.w_SEPERSRE,space(5)))
            this.w_COGNOM = NVL(DPCOGNOM126,space(40))
            this.w_NOME = NVL(DPNOME126,space(40))
          else
          .link_1_26('Load')
          endif
        .w_SE__NOTE = NVL(SE__NOTE,space(40))
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .w_SEPREMAP = NVL(SEPREMAP,space(1))
        .w_SEDESPRE = NVL(SEDESPRE,space(1))
        cp_LoadRecExtFlds(this,'SEDIAZIE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_ase
    if this.w_SEUNIORG <> 'S'
      this.BlankRec()
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SECODAZI = space(5)
      .w_SECODDES = space(5)
      .w_SENOMSED = space(40)
      .w_SETIPRIF = space(2)
      .w_SEDESSTA = space(1)
      .w_SEINDIRI = space(35)
      .w_SE___CAP = space(8)
      .w_SENATGIU = space(2)
      .w_SELOCALI = space(30)
      .w_SEPROVIN = space(2)
      .w_SETELEFO = space(18)
      .w_SE_EMAIL = space(50)
      .w_SEUNIORG = space(1)
      .w_SE_EMPEC = space(254)
      .w_SEPERSRE = space(5)
      .w_COGNOM = space(40)
      .w_NOME = space(40)
      .w_SE__NOTE = space(40)
      .w_MESS_ERR = space(100)
      .w_SEPREMAP = space(1)
      .w_SEDESPRE = space(1)
      .w_OLDMAPPE = space(5)
      .w_OLDDESPRE = space(5)
      if .cFunction<>"Filter"
        .w_SECODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_SECODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,3,.f.)
        .w_SETIPRIF = "GE"
          .DoRTCalc(5,7,.f.)
        .w_SENATGIU = iif(.w_SETIPRIF = 'DF',.w_SENATGIU,'  ')
          .DoRTCalc(9,12,.f.)
        .w_SEUNIORG = 'S'
        .DoRTCalc(14,15,.f.)
          if not(empty(.w_SEPERSRE))
          .link_1_26('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
          .DoRTCalc(16,19,.f.)
        .w_SEPREMAP = "N"
      endif
    endwith
    cp_BlankRecExtFlds(this,'SEDIAZIE')
    this.DoRTCalc(21,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSECODDES_1_2.enabled = i_bVal
      .Page1.oPag.oSENOMSED_1_4.enabled = i_bVal
      .Page1.oPag.oSETIPRIF_1_5.enabled = i_bVal
      .Page1.oPag.oSEDESSTA_1_6.enabled = i_bVal
      .Page1.oPag.oSEINDIRI_1_8.enabled = i_bVal
      .Page1.oPag.oSE___CAP_1_10.enabled = i_bVal
      .Page1.oPag.oSENATGIU_1_11.enabled = i_bVal
      .Page1.oPag.oSELOCALI_1_13.enabled = i_bVal
      .Page1.oPag.oSEPROVIN_1_15.enabled = i_bVal
      .Page1.oPag.oSETELEFO_1_17.enabled = i_bVal
      .Page1.oPag.oSE_EMAIL_1_19.enabled = i_bVal
      .Page1.oPag.oSE_EMPEC_1_25.enabled = i_bVal
      .Page1.oPag.oSEPERSRE_1_26.enabled = i_bVal
      .Page1.oPag.oSE__NOTE_1_29.enabled = i_bVal
      .Page1.oPag.oSEPREMAP_1_33.enabled = i_bVal
      .Page1.oPag.oSEDESPRE_1_35.enabled = i_bVal
      .Page1.oPag.oObj_1_31.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSECODDES_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSECODDES_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SEDIAZIE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SECODAZI,"SECODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SECODDES,"SECODDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SENOMSED,"SENOMSED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SETIPRIF,"SETIPRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SEDESSTA,"SEDESSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SEINDIRI,"SEINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SE___CAP,"SE___CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SENATGIU,"SENATGIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SELOCALI,"SELOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SEPROVIN,"SEPROVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SETELEFO,"SETELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SE_EMAIL,"SE_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SEUNIORG,"SEUNIORG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SE_EMPEC,"SE_EMPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SEPERSRE,"SEPERSRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SE__NOTE,"SE__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SEPREMAP,"SEPREMAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SEDESPRE,"SEDESPRE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    i_lTable = "SEDIAZIE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SEDIAZIE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SEDIAZIE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SEDIAZIE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SEDIAZIE')
        i_extval=cp_InsertValODBCExtFlds(this,'SEDIAZIE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SECODAZI,SECODDES,SENOMSED,SETIPRIF,SEDESSTA"+;
                  ",SEINDIRI,SE___CAP,SENATGIU,SELOCALI,SEPROVIN"+;
                  ",SETELEFO,SE_EMAIL,SEUNIORG,SE_EMPEC,SEPERSRE"+;
                  ",SE__NOTE,SEPREMAP,SEDESPRE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_SECODAZI)+;
                  ","+cp_ToStrODBC(this.w_SECODDES)+;
                  ","+cp_ToStrODBC(this.w_SENOMSED)+;
                  ","+cp_ToStrODBC(this.w_SETIPRIF)+;
                  ","+cp_ToStrODBC(this.w_SEDESSTA)+;
                  ","+cp_ToStrODBC(this.w_SEINDIRI)+;
                  ","+cp_ToStrODBC(this.w_SE___CAP)+;
                  ","+cp_ToStrODBC(this.w_SENATGIU)+;
                  ","+cp_ToStrODBC(this.w_SELOCALI)+;
                  ","+cp_ToStrODBC(this.w_SEPROVIN)+;
                  ","+cp_ToStrODBC(this.w_SETELEFO)+;
                  ","+cp_ToStrODBC(this.w_SE_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_SEUNIORG)+;
                  ","+cp_ToStrODBC(this.w_SE_EMPEC)+;
                  ","+cp_ToStrODBCNull(this.w_SEPERSRE)+;
                  ","+cp_ToStrODBC(this.w_SE__NOTE)+;
                  ","+cp_ToStrODBC(this.w_SEPREMAP)+;
                  ","+cp_ToStrODBC(this.w_SEDESPRE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SEDIAZIE')
        i_extval=cp_InsertValVFPExtFlds(this,'SEDIAZIE')
        cp_CheckDeletedKey(i_cTable,0,'SECODAZI',this.w_SECODAZI,'SECODDES',this.w_SECODDES)
        INSERT INTO (i_cTable);
              (SECODAZI,SECODDES,SENOMSED,SETIPRIF,SEDESSTA,SEINDIRI,SE___CAP,SENATGIU,SELOCALI,SEPROVIN,SETELEFO,SE_EMAIL,SEUNIORG,SE_EMPEC,SEPERSRE,SE__NOTE,SEPREMAP,SEDESPRE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SECODAZI;
                  ,this.w_SECODDES;
                  ,this.w_SENOMSED;
                  ,this.w_SETIPRIF;
                  ,this.w_SEDESSTA;
                  ,this.w_SEINDIRI;
                  ,this.w_SE___CAP;
                  ,this.w_SENATGIU;
                  ,this.w_SELOCALI;
                  ,this.w_SEPROVIN;
                  ,this.w_SETELEFO;
                  ,this.w_SE_EMAIL;
                  ,this.w_SEUNIORG;
                  ,this.w_SE_EMPEC;
                  ,this.w_SEPERSRE;
                  ,this.w_SE__NOTE;
                  ,this.w_SEPREMAP;
                  ,this.w_SEDESPRE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SEDIAZIE_IDX,i_nConn)
      *
      * update SEDIAZIE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SEDIAZIE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SENOMSED="+cp_ToStrODBC(this.w_SENOMSED)+;
             ",SETIPRIF="+cp_ToStrODBC(this.w_SETIPRIF)+;
             ",SEDESSTA="+cp_ToStrODBC(this.w_SEDESSTA)+;
             ",SEINDIRI="+cp_ToStrODBC(this.w_SEINDIRI)+;
             ",SE___CAP="+cp_ToStrODBC(this.w_SE___CAP)+;
             ",SENATGIU="+cp_ToStrODBC(this.w_SENATGIU)+;
             ",SELOCALI="+cp_ToStrODBC(this.w_SELOCALI)+;
             ",SEPROVIN="+cp_ToStrODBC(this.w_SEPROVIN)+;
             ",SETELEFO="+cp_ToStrODBC(this.w_SETELEFO)+;
             ",SE_EMAIL="+cp_ToStrODBC(this.w_SE_EMAIL)+;
             ",SEUNIORG="+cp_ToStrODBC(this.w_SEUNIORG)+;
             ",SE_EMPEC="+cp_ToStrODBC(this.w_SE_EMPEC)+;
             ",SEPERSRE="+cp_ToStrODBCNull(this.w_SEPERSRE)+;
             ",SE__NOTE="+cp_ToStrODBC(this.w_SE__NOTE)+;
             ",SEPREMAP="+cp_ToStrODBC(this.w_SEPREMAP)+;
             ",SEDESPRE="+cp_ToStrODBC(this.w_SEDESPRE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SEDIAZIE')
        i_cWhere = cp_PKFox(i_cTable  ,'SECODAZI',this.w_SECODAZI  ,'SECODDES',this.w_SECODDES  )
        UPDATE (i_cTable) SET;
              SENOMSED=this.w_SENOMSED;
             ,SETIPRIF=this.w_SETIPRIF;
             ,SEDESSTA=this.w_SEDESSTA;
             ,SEINDIRI=this.w_SEINDIRI;
             ,SE___CAP=this.w_SE___CAP;
             ,SENATGIU=this.w_SENATGIU;
             ,SELOCALI=this.w_SELOCALI;
             ,SEPROVIN=this.w_SEPROVIN;
             ,SETELEFO=this.w_SETELEFO;
             ,SE_EMAIL=this.w_SE_EMAIL;
             ,SEUNIORG=this.w_SEUNIORG;
             ,SE_EMPEC=this.w_SE_EMPEC;
             ,SEPERSRE=this.w_SEPERSRE;
             ,SE__NOTE=this.w_SE__NOTE;
             ,SEPREMAP=this.w_SEPREMAP;
             ,SEDESPRE=this.w_SEDESPRE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SEDIAZIE_IDX,i_nConn)
      *
      * delete SEDIAZIE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SECODAZI',this.w_SECODAZI  ,'SECODDES',this.w_SECODDES  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,7,.t.)
        if .o_SETIPRIF<>.w_SETIPRIF
            .w_SENATGIU = iif(.w_SETIPRIF = 'DF',.w_SENATGIU,'  ')
        endif
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
    endwith
  return

  proc Calculate_IIBDILRJCF()
    with this
          * --- GSAR_BSM sede predefinita per le mappe/merce
          GSAR_BSM(this;
              ,"UPD";
             )
    endwith
  endproc
  proc Calculate_KLGKRSDFRW()
    with this
          * --- GSAR_BSM sede predefinita per le mappe/merce
          GSAR_BSM(this;
              ,"MSG";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSENATGIU_1_11.visible=!this.oPgFrm.Page1.oPag.oSENATGIU_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oSEDESPRE_1_35.visible=!this.oPgFrm.Page1.oPag.oSEDESPRE_1_35.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
        if lower(cEvent)==lower("Insert end") or lower(cEvent)==lower("Update end")
          .Calculate_IIBDILRJCF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_KLGKRSDFRW()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SECODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SECODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SECODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_SECODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_SECODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SECODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SECODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SECODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEPERSRE
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEPERSRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_SEPERSRE)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_SEPERSRE))
          select DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SEPERSRE)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SEPERSRE) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oSEPERSRE_1_26'),i_cWhere,'',"Risorse umane",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEPERSRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_SEPERSRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_SEPERSRE)
            select DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEPERSRE = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOM = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SEPERSRE = space(5)
      endif
      this.w_COGNOM = space(40)
      this.w_NOME = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEPERSRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.DPCODICE as DPCODICE126"+ ",link_1_26.DPCOGNOM as DPCOGNOM126"+ ",link_1_26.DPNOME as DPNOME126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on SEDIAZIE.SEPERSRE=link_1_26.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and SEDIAZIE.SEPERSRE=link_1_26.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSECODDES_1_2.value==this.w_SECODDES)
      this.oPgFrm.Page1.oPag.oSECODDES_1_2.value=this.w_SECODDES
    endif
    if not(this.oPgFrm.Page1.oPag.oSENOMSED_1_4.value==this.w_SENOMSED)
      this.oPgFrm.Page1.oPag.oSENOMSED_1_4.value=this.w_SENOMSED
    endif
    if not(this.oPgFrm.Page1.oPag.oSETIPRIF_1_5.RadioValue()==this.w_SETIPRIF)
      this.oPgFrm.Page1.oPag.oSETIPRIF_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSEDESSTA_1_6.RadioValue()==this.w_SEDESSTA)
      this.oPgFrm.Page1.oPag.oSEDESSTA_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSEINDIRI_1_8.value==this.w_SEINDIRI)
      this.oPgFrm.Page1.oPag.oSEINDIRI_1_8.value=this.w_SEINDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSE___CAP_1_10.value==this.w_SE___CAP)
      this.oPgFrm.Page1.oPag.oSE___CAP_1_10.value=this.w_SE___CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oSENATGIU_1_11.value==this.w_SENATGIU)
      this.oPgFrm.Page1.oPag.oSENATGIU_1_11.value=this.w_SENATGIU
    endif
    if not(this.oPgFrm.Page1.oPag.oSELOCALI_1_13.value==this.w_SELOCALI)
      this.oPgFrm.Page1.oPag.oSELOCALI_1_13.value=this.w_SELOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oSEPROVIN_1_15.value==this.w_SEPROVIN)
      this.oPgFrm.Page1.oPag.oSEPROVIN_1_15.value=this.w_SEPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSETELEFO_1_17.value==this.w_SETELEFO)
      this.oPgFrm.Page1.oPag.oSETELEFO_1_17.value=this.w_SETELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oSE_EMAIL_1_19.value==this.w_SE_EMAIL)
      this.oPgFrm.Page1.oPag.oSE_EMAIL_1_19.value=this.w_SE_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oSE_EMPEC_1_25.value==this.w_SE_EMPEC)
      this.oPgFrm.Page1.oPag.oSE_EMPEC_1_25.value=this.w_SE_EMPEC
    endif
    if not(this.oPgFrm.Page1.oPag.oSEPERSRE_1_26.value==this.w_SEPERSRE)
      this.oPgFrm.Page1.oPag.oSEPERSRE_1_26.value=this.w_SEPERSRE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOGNOM_1_27.value==this.w_COGNOM)
      this.oPgFrm.Page1.oPag.oCOGNOM_1_27.value=this.w_COGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oNOME_1_28.value==this.w_NOME)
      this.oPgFrm.Page1.oPag.oNOME_1_28.value=this.w_NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oSE__NOTE_1_29.value==this.w_SE__NOTE)
      this.oPgFrm.Page1.oPag.oSE__NOTE_1_29.value=this.w_SE__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oSEPREMAP_1_33.RadioValue()==this.w_SEPREMAP)
      this.oPgFrm.Page1.oPag.oSEPREMAP_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSEDESPRE_1_35.RadioValue()==this.w_SEDESPRE)
      this.oPgFrm.Page1.oPag.oSEDESPRE_1_35.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'SEDIAZIE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SECODDES))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSECODDES_1_2.SetFocus()
            i_bnoObbl = !empty(.w_SECODDES)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SETIPRIF = this.w_SETIPRIF
    return

  func CanDelete()
    local i_res
    i_res=GSAR_BSE( This,  'D', this.w_SECODDES )
    if !i_res
      cp_ErrorMsg(thisform.msgFmt(""+ This.w_mess_err+""))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsar_asePag1 as StdContainer
  Width  = 537
  height = 336
  stdWidth  = 537
  stdheight = 336
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSECODDES_1_2 as StdField with uid="MMBKZWWHVJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SECODDES", cQueryName = "SECODAZI,SECODDES",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 34210681,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=140, Top=13, InputMask=replicate('X',5)

  add object oSENOMSED_1_4 as StdField with uid="CWTDAASYNY",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SENOMSED", cQueryName = "SENOMSED",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome sede",;
    HelpContextID = 26915690,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=140, Top=39, InputMask=replicate('X',40)


  add object oSETIPRIF_1_5 as StdCombo with uid="YFYEQHHAWD",rtseq=4,rtrep=.f.,left=140,top=66,width=167,height=21;
    , ToolTipText = "Tipo di riferimento";
    , HelpContextID = 255519892;
    , cFormVar="w_SETIPRIF",RowSource=""+"Generica,"+"Sede legale,"+"Sede amministrativa,"+"Domicilio fiscale,"+"Filiale,"+"Agenzia,"+"Unit� operativa,"+"Dipartimento,"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSETIPRIF_1_5.RadioValue()
    return(iif(this.value =1,"GE",;
    iif(this.value =2,"SL",;
    iif(this.value =3,"SA",;
    iif(this.value =4,"DF",;
    iif(this.value =5,"FI",;
    iif(this.value =6,"AG",;
    iif(this.value =7,"UO",;
    iif(this.value =8,"DI",;
    iif(this.value =9,"AL",;
    space(2)))))))))))
  endfunc
  func oSETIPRIF_1_5.GetRadio()
    this.Parent.oContained.w_SETIPRIF = this.RadioValue()
    return .t.
  endfunc

  func oSETIPRIF_1_5.SetRadio()
    this.Parent.oContained.w_SETIPRIF=trim(this.Parent.oContained.w_SETIPRIF)
    this.value = ;
      iif(this.Parent.oContained.w_SETIPRIF=="GE",1,;
      iif(this.Parent.oContained.w_SETIPRIF=="SL",2,;
      iif(this.Parent.oContained.w_SETIPRIF=="SA",3,;
      iif(this.Parent.oContained.w_SETIPRIF=="DF",4,;
      iif(this.Parent.oContained.w_SETIPRIF=="FI",5,;
      iif(this.Parent.oContained.w_SETIPRIF=="AG",6,;
      iif(this.Parent.oContained.w_SETIPRIF=="UO",7,;
      iif(this.Parent.oContained.w_SETIPRIF=="DI",8,;
      iif(this.Parent.oContained.w_SETIPRIF=="AL",9,;
      0)))))))))
  endfunc

  add object oSEDESSTA_1_6 as StdCheck with uid="ZVMPKGLAZX",rtseq=5,rtrep=.f.,left=325, top=63, caption="Destinatario stampa predefinito",;
    ToolTipText = "Se attivo, la sede risulter� di default predefinita quale destinataria di stampa dell'attestazione di pagamento sul modello F24",;
    HelpContextID = 32510823,;
    cFormVar="w_SEDESSTA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSEDESSTA_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSEDESSTA_1_6.GetRadio()
    this.Parent.oContained.w_SEDESSTA = this.RadioValue()
    return .t.
  endfunc

  func oSEDESSTA_1_6.SetRadio()
    this.Parent.oContained.w_SEDESSTA=trim(this.Parent.oContained.w_SEDESSTA)
    this.value = ;
      iif(this.Parent.oContained.w_SEDESSTA=='S',1,;
      0)
  endfunc

  add object oSEINDIRI_1_8 as StdField with uid="NTZPJVEZQT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SEINDIRI", cQueryName = "SEINDIRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo",;
    HelpContextID = 118055791,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=140, Top=92, InputMask=replicate('X',35)

  add object oSE___CAP_1_10 as StdField with uid="FVLWBIORXK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SE___CAP", cQueryName = "SE___CAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice di avviamento postale",;
    HelpContextID = 46908278,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=140, Top=117, InputMask=replicate('X',8), bHasZoom = .t. 

  proc oSE___CAP_1_10.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_SE___CAP",".w_SELOCALI",".w_SEPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSENATGIU_1_11 as StdField with uid="PNPDOUOBJE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SENATGIU", cQueryName = "SENATGIU",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Natura giuridica",;
    HelpContextID = 167988357,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=399, Top=117, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oSENATGIU_1_11.mHide()
    with this.Parent.oContained
      return (.w_SETIPRIF # 'DF')
    endwith
  endfunc

  add object oSELOCALI_1_13 as StdField with uid="FJTNKNXMYT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SELOCALI", cQueryName = "SELOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit�",;
    HelpContextID = 17132689,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=140, Top=141, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oSELOCALI_1_13.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_SE___CAP",".w_SELOCALI",".w_SEPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSEPROVIN_1_15 as StdField with uid="SIJSVVGLUW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SEPROVIN", cQueryName = "SEPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 188886156,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=399, Top=141, InputMask=replicate('X',2), bHasZoom = .t. 

  proc oSEPROVIN_1_15.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_SEPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSETELEFO_1_17 as StdField with uid="RQRRKRTKIN",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SETELEFO", cQueryName = "SETELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Telefono",;
    HelpContextID = 58790773,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=140, Top=166, InputMask=replicate('X',18)

  add object oSE_EMAIL_1_19 as StdField with uid="WVEEDMHLUT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SE_EMAIL", cQueryName = "SE_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo e-mail",;
    HelpContextID = 7224462,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=140, Top=191, InputMask=replicate('X',50)

  add object oSE_EMPEC_1_25 as StdField with uid="NLUSBRNQFD",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SE_EMPEC", cQueryName = "SE_EMPEC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo e-mail PEC",;
    HelpContextID = 244433769,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=140, Top=215, InputMask=replicate('X',254)

  add object oSEPERSRE_1_26 as StdField with uid="UGBZSLTRPM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_SEPERSRE", cQueryName = "SEPERSRE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Persona responsabile",;
    HelpContextID = 31511403,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=140, Top=239, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_SEPERSRE"

  func oSEPERSRE_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oSEPERSRE_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSEPERSRE_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oSEPERSRE_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Risorse umane",'',this.parent.oContained
  endproc

  add object oCOGNOM_1_27 as StdField with uid="VKWXKUOZDN",rtseq=16,rtrep=.f.,;
    cFormVar = "w_COGNOM", cQueryName = "COGNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Cognome della persona responsabile",;
    HelpContextID = 196693030,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=203, Top=239, InputMask=replicate('X',40)

  add object oNOME_1_28 as StdField with uid="ZZYEAJBXHM",rtseq=17,rtrep=.f.,;
    cFormVar = "w_NOME", cQueryName = "NOME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome della persona responsabile",;
    HelpContextID = 104813354,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=203, Top=263, InputMask=replicate('X',40)

  add object oSE__NOTE_1_29 as StdField with uid="PHSVVKCCFX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_SE__NOTE", cQueryName = "SE__NOTE",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Note di consegna",;
    HelpContextID = 230409067,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=140, Top=287, InputMask=replicate('X',40)


  add object oObj_1_31 as cp_runprogram with uid="AZEWIDNDYQ",left=-2, top=346, width=246,height=33,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BAS",;
    cEvent = "w_SEDESSTA Changed",;
    nPag=1;
    , HelpContextID = 68325350

  add object oSEPREMAP_1_33 as StdCheck with uid="ZEUBURZSOG",rtseq=20,rtrep=.f.,left=141, top=313, caption="Sede preferita per le mappe",;
    ToolTipText = "Sede preferita per le mappe",;
    HelpContextID = 186504054,;
    cFormVar="w_SEPREMAP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSEPREMAP_1_33.RadioValue()
    return(iif(this.value =1,"S",;
    space(1)))
  endfunc
  func oSEPREMAP_1_33.GetRadio()
    this.Parent.oContained.w_SEPREMAP = this.RadioValue()
    return .t.
  endfunc

  func oSEPREMAP_1_33.SetRadio()
    this.Parent.oContained.w_SEPREMAP=trim(this.Parent.oContained.w_SEPREMAP)
    this.value = ;
      iif(this.Parent.oContained.w_SEPREMAP=="S",1,;
      0)
  endfunc

  add object oSEDESPRE_1_35 as StdCheck with uid="DLQAEXZKGN",rtseq=21,rtrep=.f.,left=325, top=313, caption="Sede preferita di consegna merce",;
    ToolTipText = "Sede preferita di consegna merce",;
    HelpContextID = 250614635,;
    cFormVar="w_SEDESPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSEDESPRE_1_35.RadioValue()
    return(iif(this.value =1,"S",;
    space(1)))
  endfunc
  func oSEDESPRE_1_35.GetRadio()
    this.Parent.oContained.w_SEDESPRE = this.RadioValue()
    return .t.
  endfunc

  func oSEDESPRE_1_35.SetRadio()
    this.Parent.oContained.w_SEDESPRE=trim(this.Parent.oContained.w_SEDESPRE)
    this.value = ;
      iif(this.Parent.oContained.w_SEDESPRE=="S",1,;
      0)
  endfunc

  func oSEDESPRE_1_35.mHide()
    with this.Parent.oContained
      return (not isahr())
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="TOWCVSBZIY",Visible=.t., Left=65, Top=17,;
    Alignment=1, Width=74, Height=18,;
    Caption="Codice sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="RXLGILIKYN",Visible=.t., Left=60, Top=43,;
    Alignment=1, Width=79, Height=18,;
    Caption="Nome sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="VFQLOMQYYX",Visible=.t., Left=82, Top=96,;
    Alignment=1, Width=57, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="BDUHYJBTGO",Visible=.t., Left=73, Top=121,;
    Alignment=1, Width=66, Height=18,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="FWOUPIXDGS",Visible=.t., Left=79, Top=145,;
    Alignment=1, Width=60, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="NSPLFERGRY",Visible=.t., Left=365, Top=145,;
    Alignment=1, Width=33, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="MZCRCWWHDA",Visible=.t., Left=88, Top=170,;
    Alignment=1, Width=51, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="EZCPPVUPKB",Visible=.t., Left=43, Top=195,;
    Alignment=1, Width=96, Height=18,;
    Caption="Indirizzo e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="REHCHRQAGV",Visible=.t., Left=45, Top=68,;
    Alignment=1, Width=94, Height=18,;
    Caption="Tipo riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="VQKFGMMBEG",Visible=.t., Left=36, Top=290,;
    Alignment=1, Width=103, Height=18,;
    Caption="Note di consegna:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="IFLPVVBNIJ",Visible=.t., Left=4, Top=239,;
    Alignment=1, Width=135, Height=18,;
    Caption="Persona responsabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="FBCVPMUPKN",Visible=.t., Left=308, Top=121,;
    Alignment=1, Width=90, Height=18,;
    Caption="Natura giuridica:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_SETIPRIF # 'DF')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="SXMYSLGOEV",Visible=.t., Left=15, Top=218,;
    Alignment=1, Width=124, Height=18,;
    Caption="Indirizzo e-mail PEC:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ase','SEDIAZIE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SECODAZI=SEDIAZIE.SECODAZI";
  +" and "+i_cAliasName2+".SECODDES=SEDIAZIE.SECODDES";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
