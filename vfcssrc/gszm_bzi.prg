* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bzi                                                        *
*              Menu cntestuale dett. inventari                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_5]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-14                                                      *
* Last revis.: 2005-09-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bzi",oParentObject)
return(i_retval)

define class tgszm_bzi as StdBatch
  * --- Local variables
  w_DINUMINV = space(6)
  w_DICODESE = space(4)
  w_DICODICE = space(20)
  w_OBJECT = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tasto destro su dettagli inventari. Ho bisogno di un batch poich� il caricamento 
    *     � inibito nel caso di inventario presente come inventario di riferimento su ul'altro inventario
    * --- Assegno le chiavi della tabella
    this.w_DINUMINV = g_oMenu.getbyindexkeyvalue(1)
    this.w_DICODESE = g_oMenu.getbyindexkeyvalue(2)
    this.w_DICODICE = g_oMenu.getbyindexkeyvalue(3)
    this.w_OBJECT = GSMA_AID()
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    this.w_OBJECT.EcpFilter()     
    * --- Valorizzo i campi chiave
    this.w_OBJECT.w_DINUMINV = this.w_DINUMINV
    this.w_OBJECT.w_DICODESE = this.w_DICODESE
    this.w_OBJECT.w_DICODICE = this.w_DICODICE
    * --- Carico il record richiesto
    this.w_OBJECT.EcpSave()     
    do case
      case g_oMenu.cBatchType="M"
        this.w_OBJECT.EcpEdit()     
      case g_oMenu.cBatchType="L"
        this.w_OBJECT.EcpLoad()     
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
