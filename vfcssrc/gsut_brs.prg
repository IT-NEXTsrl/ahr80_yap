* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_brs                                                        *
*              Rss feed                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-01                                                      *
* Last revis.: 2013-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper,pFolderPath
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_brs",oParentObject,m.pOper,m.pFolderPath)
return(i_retval)

define class tgsut_brs as StdBatch
  * --- Local variables
  pOper = space(1)
  pFolderPath = space(254)
  w_FEEDMNG = .NULL.
  w_FOLDER = .NULL.
  w_COUNT = 0
  w_PATH = space(254)
  w_FEED = .NULL.
  w_MAXITEM = 0
  w_PARENT = space(254)
  w_SAVE = space(1)
  w_NFOLDER = space(254)
  w_OBJ = .NULL.
  w_ITEM = .NULL.
  w_ITEM = .NULL.
  w_RSSITEMDATE = ctod("  /  /  ")
  w_RSSITEMTITLE = space(240)
  w_RSSITEMFEED = space(240)
  w_RSSITEMAUTHOR = space(240)
  w_RSSITEMLINK = space(0)
  w_RSSITEMDESC = space(0)
  w_RSSITEMISREAD = space(1)
  w_RSSFEEDPATH = space(254)
  w_RSSUIDITEM = 0
  * --- WorkFile variables
  RSS_FEED_idx=0
  TMPRSSITEM_idx=0
  RSS_DESK_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- RSS Feed
    * --- GSUT_ARS
    * --- GSUT_KRS
    do case
      case this.pOper="S"
        if this.oParentObject.w_SUBSCRIBE = "S"
          if Empty(this.oParentObject.w_RFDESCRI)
            ah_ErrorMsg("Descrizione feed non specificata%0Impossibile sottoscrivere")
            i_retcode = 'stop'
            return
          endif
          if Empty(this.oParentObject.w_RFRSSURL)
            ah_ErrorMsg("URL feed non specificato%0Impossibile sottoscrivere")
            i_retcode = 'stop'
            return
          endif
          this.w_FEEDMNG=createobject("FeedsManager")
          if Type("pFolderPath") = "C" And !Empty(this.pFolderPath)
            this.w_FOLDER = this.w_FEEDMNG.GetFolder(Alltrim(this.pFolderPath))
            if IsNull(this.w_FOLDER)
              this.w_FOLDER = this.w_FEEDMNG.RootFolder
            endif
          else
            this.w_FOLDER = this.w_FEEDMNG.RootFolder
          endif
          if !this.w_FOLDER.ExistsFeed(AllTrim(this.oParentObject.w_RFDESCRI))
            btrserr=.F. 
 on error btrserr=.T.
            this.w_FOLDER.CreateFeed(Alltrim(this.oParentObject.w_RFDESCRI), Alltrim(this.oParentObject.w_RFRSSURL))     
            if btrserr
              ah_ErrorMsg("Impossibile sottoscrivere, verificare l'Url inserito") 
 on error 
            endif
          else
            ah_ErrorMsg("Il feed � gi� esistente%0Impossibile sottoscrivere")
          endif
          * --- Se visualizza rss aggiorno la treeview
          if Lower(This.oParentObject.Class)=="tgsut_krs"
            GSUT_BRS(This.oParentObject, "T")
          endif
        endif
      case this.pOper="T" Or this.pOper="I"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.pOper = "I"
          * --- Creo tabella temporanea zoom
          * --- Create temporary table TMPRSSITEM
          i_nIdx=cp_AddTableDef('TMPRSSITEM') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSUT1KRS.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPRSSITEM_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Svuoto il controllo HTML
          this.oParentObject.w_HTML.OBROWSER.Navigate("about:blank")     
        endif
      case this.pOper="C"
        * --- Poich� utilizzo la solita routine ho bisogno di recuperare IDX della tabella temporanea
        *     utilizzando la funzione cp_GetTableDefIdx. Se fossero state due routine diverse non
        *     sarebbe stato necessario
        this.TMPRSSITEM_idx = cp_GetTableDefIdx("TMPRSSITEM")
        * --- Delete from TMPRSSITEM
        i_nConn=i_TableProp[this.TMPRSSITEM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPRSSITEM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        this.w_FEEDMNG=createobject("FeedsManager")
        this.w_PATH = this.oParentObject.w_TREEVIEW.OTREE.SelectedItem.FullPath
        this.w_PATH = SUBSTR(this.w_PATH, AT("\", this.w_PATH, 2) + 1)
        do case
          case this.oParentObject.w_TIPO = "D" Or this.oParentObject.w_TIPO = "R" Or this.oParentObject.w_TIPO = "S"
            if this.oParentObject.w_TIPO = "R" Or this.oParentObject.w_TIPO = "S"
              this.w_FOLDER = this.w_FEEDMNG.RootFolder
            else
              this.w_FOLDER = this.w_FEEDMNG.GetFolder(this.w_PATH)
            endif
            This.GetFolderFeeds(this.w_FOLDER)
          case this.oParentObject.w_TIPO = "F"
            this.w_FEED = this.w_FEEDMNG.GetFeed(this.w_PATH)
            This.GetItems(this.w_FEED)
        endcase
        * --- Refresh della griglia
        This.oParentObject.NotifyEvent("Aggiorna")
      case this.pOper = "V"
        this.oParentObject.w_HTML.OBROWSER.Document.Close()     
        this.oParentObject.w_HTML.OBROWSER.Document.Write(This.GetHTML(this.oParentObject.w_ZOOM.cCursor))     
      case this.pOper = "D"
        this.w_FEEDMNG=createobject("FeedsManager")
        this.w_PATH = this.oParentObject.w_TREEVIEW.OTREE.SelectedItem.FullPath
        this.w_PATH = SUBSTR(this.w_PATH, AT("\", this.w_PATH, 2) + 1)
        do case
          case this.oParentObject.w_TIPO = "D" Or this.oParentObject.w_TIPO = "R"
            if this.oParentObject.w_TIPO = "R" Or this.oParentObject.w_TIPO = "S"
              this.w_FOLDER = this.w_FEEDMNG.RootFolder
            else
              this.w_FOLDER = this.w_FEEDMNG.GetFolder(this.w_PATH)
            endif
            Local loFeeds 
 m.loFeeds = this.w_FOLDER.Feeds
            FOR EACH this.w_Feed IN m.loFeeds
            * --- Read from RSS_FEED
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.RSS_FEED_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RSS_FEED_idx,2],.t.,this.RSS_FEED_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "RFMAXITM"+;
                " from "+i_cTable+" RSS_FEED where ";
                    +"RFRSSURL = "+cp_ToStrODBC(this.oParentObject.w_RFRSSURL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                RFMAXITM;
                from (i_cTable) where;
                    RFRSSURL = this.oParentObject.w_RFRSSURL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MAXITEM = NVL(cp_ToDate(_read_.RFMAXITM),cp_NullValue(_read_.RFMAXITM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_FEED.MaxItemCount = IIF(this.w_MAXITEM=0, g_MaxItemFeedCount, this.w_MAXITEM)
            l_ErrDL = .F. 
 l_oldErrorDL = ON("Error") 
 On error l_ErrDL = .T.
            ah_msg("Download in corso...", .T., .T.)
            this.w_FEED.Download()     
            Wait Clear
            On error &l_OldErrorDL
            if l_ErrDL
              ah_ErrorMsg("Si � verificato un errore durante il download")
              Exit
            endif
            ENDFOR
            m.loFeeds = .NULL.
          case this.oParentObject.w_TIPO = "F"
            this.w_FEED = this.w_FEEDMNG.GetFeed(this.w_PATH)
            * --- Read from RSS_FEED
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.RSS_FEED_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RSS_FEED_idx,2],.t.,this.RSS_FEED_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "RFMAXITM"+;
                " from "+i_cTable+" RSS_FEED where ";
                    +"RFRSSURL = "+cp_ToStrODBC(this.oParentObject.w_RFRSSURL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                RFMAXITM;
                from (i_cTable) where;
                    RFRSSURL = this.oParentObject.w_RFRSSURL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MAXITEM = NVL(cp_ToDate(_read_.RFMAXITM),cp_NullValue(_read_.RFMAXITM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_FEED.MaxItemCount = IIF(this.w_MAXITEM=0, g_MaxItemFeedCount, this.w_MAXITEM)
            l_ErrDL = .F. 
 l_oldErrorDL = ON("Error") 
 On error l_ErrDL = .T.
            ah_msg("Download in corso...", .T., .T.)
            this.w_FEED.Download()     
            Wait Clear
            On error &l_OldErrorDL
            if l_ErrDL
              ah_ErrorMsg("Si � verificato un errore durante il download")
            endif
        endcase
        * --- Refresh della griglia
        Select (this.oParentObject.w_ZOOM.cCursor)
        GO TOP
        this.oParentObject.w_ZOOM.Grd.Refresh()     
      case this.pOper = "P"
        this.w_FEEDMNG=createobject("FeedsManager")
        this.w_PATH = this.oParentObject.w_TREEVIEW.OTREE.SelectedItem.FullPath
        this.w_PATH = SUBSTR(this.w_PATH, AT("\", this.w_PATH, 2) + 1)
        this.w_PARENT = "Root\" + this.w_PATH
        this.w_SAVE = "N"
        this.w_NFOLDER = ""
        do GSUT2KRS with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_NFOLDER = Alltrim(this.w_NFOLDER)
        * --- Se SAVE = 'S' ho confermato la maschera
        if this.w_SAVE = "S"
          if this.oParentObject.w_TIPO = "R"
            this.w_FOLDER = this.w_FEEDMNG.RootFolder
          else
            this.w_FOLDER = this.w_FEEDMNG.GetFolder(this.w_PATH)
          endif
          if Empty(this.w_NFOLDER)
            Ah_ErrorMsg("Nome folder non specificato%0Impossibile creare il folder",48,"")
          else
            if this.w_FOLDER.ExistsSubFolder(this.w_NFOLDER)
              Ah_ErrorMsg("Folder gi� esistente%0Impossibile creare il folder",48,"")
            else
              this.w_FOLDER.CreateSubfolder(this.w_NFOLDER)     
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          this.w_FOLDER = .NULL.
        endif
        this.w_FEEDMNG = .NULL.
      case this.pOper = "F"
        this.w_OBJ = GSUT_ARS()
        if !(this.w_OBJ.bSec1)
          Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
          this.w_OBJ = .NULL.
          i_retcode = 'stop'
          return
        endif
        this.w_OBJ.ecpLoad()     
        this.w_PATH = this.oParentObject.w_TREEVIEW.OTREE.SelectedItem.FullPath
        if AT("\", this.w_PATH, 2) = 0
          this.w_PATH = ""
        endif
        this.w_PATH = SUBSTR(this.w_PATH, AT("\", this.w_PATH, 2) + 1)
        this.w_OBJ.w_FOLDERPATH = this.w_PATH
        this.w_OBJ.w_SUBSCRIBE = "S"
        this.w_OBJ.mCalc(.t.)     
        this.w_OBJ = .NULL.
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper = "E"
        this.w_FEEDMNG=createobject("FeedsManager")
        this.w_PATH = this.oParentObject.w_TREEVIEW.OTREE.SelectedItem.FullPath
        this.w_PATH = SUBSTR(this.w_PATH, AT("\", this.w_PATH, 2) + 1)
        do case
          case this.oParentObject.w_TIPO = "D"
            if ah_yesno("Eliminare la cartella %1?", "", this.w_PATH)
              this.w_FEEDMNG.DeleteFolder(this.w_PATH)     
            endif
          case this.oParentObject.w_TIPO = "F"
            if ah_yesno("Eliminare la sottoscrizione al feed %1?", "", this.w_PATH)
              this.w_FEEDMNG.DeleteFeed(this.w_PATH)     
            endif
        endcase
        this.w_FEEDMNG = .NULL.
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper = "X"
        USE IN SELECT("TMPINFOTREEVIEW")
        * --- Drop temporary table TMPRSSITEM
        i_nIdx=cp_GetTableDefIdx('TMPRSSITEM')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPRSSITEM')
        endif
      case this.pOper = "N"
        if ah_YesNo("Eliminare definitivamente le notizie selezionate?")
          Select * from (this.oParentObject.w_ZOOM.cCursor) Where xChk=1 into cursor "__FeedToDelete__"
          if Used("__FeedToDelete__")
            Scan
            this.w_FEEDMNG=createobject("FeedsManager")
            this.w_FEED = this.w_FEEDMNG.GetFeed(Alltrim(__FeedToDelete__.PATH))
            this.w_ITEM = this.w_FEED.GetItem(__FeedToDelete__.UIDFEED)
            this.w_ITEM.Delete()     
            EndScan
          endif
          Use In Select("__FeedToDelete__")
          this.w_FEEDMNG = .NULL.
          GSUT_BRS(This.oParentObject, "C")
        endif
      case this.pOper = "Z"
        UPDATE (this.oParentObject.w_ZOOM.cCursor) SET xChk = IIF(this.oParentObject.w_SELEZI="S", 1, 0) 
 Select (this.oParentObject.w_ZOOM.cCursor) 
 Go Top
      case this.pOper = "R" Or this.pOper = "U"
        Select * from (this.oParentObject.w_ZOOM.cCursor) Where xChk=1 into cursor "__FeedToDelete__"
        if Used("__FeedToDelete__")
          Scan
          this.w_FEEDMNG=createobject("FeedsManager")
          this.w_FEED = this.w_FEEDMNG.GetFeed(Alltrim(__FeedToDelete__.PATH))
          this.w_ITEM = this.w_FEED.GetItem(__FeedToDelete__.UIDFEED)
          this.w_ITEM.IsRead = (this.pOper="R")
          EndScan
        endif
        Use In Select("__FeedToDelete__")
        this.w_FEEDMNG = .NULL.
        GSUT_BRS(This.oParentObject, "C")
      case this.pOper = "L"
        Select * from (this.oParentObject.w_ZOOM.cCursor) Where xChk=1 into cursor "__FeedToDelete__"
        if Used("__FeedToDelete__")
          Scan
          * --- Delete from RSS_DESK
          i_nConn=i_TableProp[this.RSS_DESK_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RSS_DESK_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"RDCODICE = "+cp_ToStrODBC(__FeedToDelete__.RDCODICE);
                  +" and RDUTEGRP = "+cp_ToStrODBC(__FeedToDelete__.UTEGRP);
                  +" and CPROWNUM = "+cp_ToStrODBC(__FeedToDelete__.CPROWNUM);
                   )
          else
            delete from (i_cTable) where;
                  RDCODICE = __FeedToDelete__.RDCODICE;
                  and RDUTEGRP = __FeedToDelete__.UTEGRP;
                  and CPROWNUM = __FeedToDelete__.CPROWNUM;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          EndScan
        endif
        Use In Select("__FeedToDelete__")
        This.oParentObject.NotifyEvent("Ricerca")
    endcase
    this.w_FOLDER = .NULL.
    this.w_FEEDMNG = .NULL.
    this.w_FEED = .NULL.
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa pagina � richiamata dall'area manuale GetItem()
    * --- Insert into TMPRSSITEM
    i_nConn=i_TableProp[this.TMPRSSITEM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPRSSITEM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPRSSITEM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DATEFEED"+",TITLE"+",FEED"+",AUTHOR"+",LINK"+",DESCRIPT"+",ISREAD"+",PATH"+",UIDFEED"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_RSSITEMDATE),'TMPRSSITEM','DATEFEED');
      +","+cp_NullLink(cp_ToStrODBC(left (this.w_RSSITEMTITLE,254)),'TMPRSSITEM','TITLE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSSITEMFEED),'TMPRSSITEM','FEED');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSSITEMAUTHOR),'TMPRSSITEM','AUTHOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSSITEMLINK),'TMPRSSITEM','LINK');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSSITEMDESC),'TMPRSSITEM','DESCRIPT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSSITEMISREAD),'TMPRSSITEM','ISREAD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSSFEEDPATH),'TMPRSSITEM','PATH');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSSUIDITEM),'TMPRSSITEM','UIDFEED');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DATEFEED',this.w_RSSITEMDATE,'TITLE',left (this.w_RSSITEMTITLE,254),'FEED',this.w_RSSITEMFEED,'AUTHOR',this.w_RSSITEMAUTHOR,'LINK',this.w_RSSITEMLINK,'DESCRIPT',this.w_RSSITEMDESC,'ISREAD',this.w_RSSITEMISREAD,'PATH',this.w_RSSFEEDPATH,'UIDFEED',this.w_RSSUIDITEM)
      insert into (i_cTable) (DATEFEED,TITLE,FEED,AUTHOR,LINK,DESCRIPT,ISREAD,PATH,UIDFEED &i_ccchkf. );
         values (;
           this.w_RSSITEMDATE;
           ,left (this.w_RSSITEMTITLE,254);
           ,this.w_RSSITEMFEED;
           ,this.w_RSSITEMAUTHOR;
           ,this.w_RSSITEMLINK;
           ,this.w_RSSITEMDESC;
           ,this.w_RSSITEMISREAD;
           ,this.w_RSSFEEDPATH;
           ,this.w_RSSUIDITEM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LVLKEY: Chiave
    *     FNAME: Nome Feed
    *     CPBMPNAME: Icona
    *     FURL: URL Feed
    *     TIPO: "D" Folder, "F" Feed, "U" Unsubscribe, "" SystemFolder 
    CREATE CURSOR TMPINFOTREEVIEW (LVLKEY C(100), FNAME C(100), CPBmpName C(254), FURL C(254), FPATH C(254), TIPO C(1))
    INDEX ON LVLKEY TAG LVLKEY COLLATE "MACHINE"
    INSERT INTO TMPINFOTREEVIEW (LVLKEY, CPBmpName, FNAME, TIPO) VALUES ("001", "Bmp\RSSFolder.bmp", "RSS Feeds", "S")
    this.w_FEEDMNG=createobject("FeedsManager")
    this.w_FOLDER = this.w_FEEDMNG.RootFolder
    * --- Poich� uso la ricorsione utilizzo una area manuale
    This.LoadRSS_BRS(this.w_FOLDER, "TMPINFOTREEVIEW", "001.001")
    INSERT INTO TMPINFOTREEVIEW (LVLKEY, CPBmpName, FNAME, TIPO) VALUES ("001.002", "Bmp\aperturafile.bmp", "RSS Feeds unsubscribe", "")
    * --- Select from RSS_FEED
    i_nConn=i_TableProp[this.RSS_FEED_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RSS_FEED_idx,2],.t.,this.RSS_FEED_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select RFDESCRI,RFRSSURL  from "+i_cTable+" RSS_FEED ";
          +" order by RFDESCRI";
           ,"_Curs_RSS_FEED")
    else
      select RFDESCRI,RFRSSURL from (i_cTable);
       order by RFDESCRI;
        into cursor _Curs_RSS_FEED
    endif
    if used('_Curs_RSS_FEED')
      select _Curs_RSS_FEED
      locate for 1=1
      do while not(eof())
       
 L_NEXIST = 0 
 SELECT TMPINFOTREEVIEW 
 COUNT FOR Upper(Alltrim(TMPINFOTREEVIEW.FURL))=Upper(Alltrim(_Curs_RSS_FEED.RFRSSURL)) TO L_NEXIST
      if m.L_NEXIST=0
        this.w_COUNT = this.w_COUNT + 1
        INSERT INTO TMPINFOTREEVIEW (LVLKEY, CPBmpName, FNAME, FURL, TIPO) VALUES ("001.002."+Right("000"+Alltrim(Str(this.w_COUNT)),3), "Bmp\RSSFeed2.bmp", Alltrim(_Curs_RSS_FEED.RFDESCRI), _Curs_RSS_FEED.RFRSSURL, "U")
      endif
        select _Curs_RSS_FEED
        continue
      enddo
      use
    endif
    * --- Aggiorna la treeview
    if this.pOper <> "I"
      This.oParentObject.NotifyEvent("TreeViewAgg")
    endif
    this.oParentObject.w_TREEVIEW.ExpandAll(.T.)     
  endproc


  proc Init(oParentObject,pOper,pFolderPath)
    this.pOper=pOper
    this.pFolderPath=pFolderPath
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='RSS_FEED'
    this.cWorkTables[2]='*TMPRSSITEM'
    this.cWorkTables[3]='RSS_DESK'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_RSS_FEED')
      use in _Curs_RSS_FEED
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsut_brs
  Procedure LoadRSS_BRS(oFeedFolder, pRecordSource, pLvlKey)
    Local l_FeedItem, l_Feed, lcFeedName, l_Feeds, loSubFolders, l_count, l_LvlKey, l_Tipo
    l_count = 1
    If !m.oFeedFolder.IsRoot
      m.lcFeedName = oFeedFolder.Name
      l_Tipo = "D"
    Else
      *--- Root
      m.lcFeedName = "RSS Feeds subscribe" 
      l_Tipo = "R"
    Endif
    Insert into (pRecordSource) (LVLKEY, CPBmpName, FNAME, FURL, TIPO) Values (m.pLvlKey, "Bmp\aperturafile.bmp", m.lcFeedName, "", m.l_Tipo)
    
    m.loSubFolders = m.oFeedFolder.SubFolders
    For Each m.loSubFolder In m.loSubFolders
      l_count = m.l_count + 1
        l_LvlKey = m.pLvlKey+"."+Right("000"+Alltrim(Str(m.l_count)), 3)
        This.LoadRSS_BRS(m.loSubFolder, m.pRecordSource, m.l_LvlKey)
    Next
    
    l_Feeds = m.oFeedFolder.Feeds
    For Each l_Feed In m.l_Feeds
      l_count = m.l_count + 1
      l_LvlKey = m.pLvlKey+"."+Right("000"+Alltrim(Str(m.l_count)), 3)
      m.lcFeedURL = ""
      m.lcFeedName = m.l_Feed.Name
      m.lcFeedURL = m.l_Feed.URL
      Insert into (pRecordSource) (LVLKEY, CPBmpName, FNAME, FURL, TIPO) Values (m.l_LvlKey, "Bmp\RSSFeed.bmp", m.lcFeedName, m.lcFeedURL, "F")
    Endfor
  Endproc
  
  Proc GetFolderFeeds
    LPARAMETERS toFolder
    LOCAL loSubFolders, loSubFolder, loFeeds, loFeed, lnReturn
    m.loFeeds = m.toFolder.Feeds
    m.lnReturn = 0
    FOR EACH m.loFeed IN m.loFeeds
      This.GetItems(m.loFeed)
      m.lnReturn = m.lnReturn + 1
    NEXT
    m.loSubFolders = m.toFolder.SubFolders
    FOR EACH m.loSubFolder IN m.loSubFolders
      m.lnReturn = m.lnReturn + This.GetFolderFeeds(m.loSubFolder)
    NEXT
    RETURN m.lnReturn
  EndProc
  
  Proc GetItems
    LPARAMETERS toFeed
    LOCAL loItems, loItem, lcFeedName, lnReturn, lcFeedPath, l_count, l_OldErr, l_Err
  
    m.lnReturn = 0
    m.lcFeedName = m.toFeed.Name
    m.lcFeedPath = m.toFeed.Path
    m.loItems = m.toFeed.Items
    l_count = 0
    l_OldErr = ON("Error")
    l_Err = .F.
    On error l_Err = .T.
    *FOR EACH m.loItem IN m.loItems
    FOR m.l_count = 0 to m.toFeed.ItemCount
      l_Err = .F.
      m.loItem = m.loItems.Item(m.l_count)
      m.lnReturn = m.lnReturn + 1
      This.w_RSSITEMDATE = m.loItem.PubDate
      This.w_RSSITEMTITLE = Alltrim(Chrtran(m.loItem.Title, Chr(10)+Chr(13)+Chr(9),""))
      This.w_RSSITEMFEED = m.lcFeedName
      This.w_RSSITEMAUTHOR = m.loItem.author
      This.w_RSSITEMLINK = m.loItem.link
      This.w_RSSITEMDESC = Left(m.loItem.Description, 4000)
      This.w_RSSITEMISREAD = IIF(m.loItem.IsRead, "S", "N")
      This.w_RSSFEEDPATH = m.lcFeedPath
      This.w_RSSUIDITEM = m.loItem.ofeeditem.LocalId
      If !l_Err
        *--- Esegue insert nella tabella temporanea TMPRSSITEM utilizzata dallo zoom
        This.Pag2()
      Endif
    NEXT
    On error &l_OldErr
    RETURN m.lnReturn
  Endproc  
  
  Proc GetHTML(lcGridRecordSource)
  LOCAL lcHTML
  m.lcHTML = ""
  IF !EOF(m.lcGridRecordSource)
  	TEXT TO m.lcHTML TEXTMERGE NOSHOW PRETEXT 7
  	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  	<html>
  		<head>
  			<title><<EVALUATE(m.lcGridRecordSource + ".Title")>></title>
  			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  			<style type="text/css">body {margin:0; padding:0; background:#fff; color:#000}td,body,.form {font-family:Tahoma; letter-spacing:1.1lm;font-size: 10pt}A {color:#336699; text-decoration: none;}</style>
  		</head>
  	   <body scroll="no">
  			<table height="100%" width="100%" border="0">
  			  <tr>
  				<td height="50" style="border: 1px solid #FF9933;" bgcolor="#FFCC66">
  					<div style="height:100%;width:100%;padding:10px 10px;"> 
  						<font size="1" face="Verdana, Arial, Helvetica, sans-serif">
  	        				<strong>Feed:</strong> 
  					      	<<EVALUATE(m.lcGridRecordSource + ".Feed")>><br>
  	        				<strong>Author:</strong> 
  					      	<<EVALUATE(m.lcGridRecordSource + ".Author")>><br>
  					      	<strong>Title:</strong>
  					      	<a href="<<EVALUATE(m.lcGridRecordSource + ".Link")>>" target="_blank">
  					      		<<EVALUATE(m.lcGridRecordSource + ".Title")>>
  					      	</a>
  				      	</font>
  			      	</div>
  				</td>
  			  </tr>
  			  <tr> 
  			    <td>
  	          		<div style="height:100%;width:100%;overflow:auto;padding:10px 10px;">
  			    		<font face="Verdana, Arial, Helvetica, sans-serif" font size="2">
  							<<EVALUATE(m.lcGridRecordSource + ".Descript")>><br>
  						</font>
                 		</div>
  			    </td>
  			  </tr>
  			</table>
  		</body>
  	</html>
  	ENDTEXT
  ENDIF
  
  RETURN m.lcHTML
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper,pFolderPath"
endproc
