* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kdg                                                        *
*              Manutenzione campo GESTGUID                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-03-18                                                      *
* Last revis.: 2013-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kdg",oParentObject))

* --- Class definition
define class tgsma_kdg as StdForm
  Top    = 0
  Left   = -1

  * --- Standard Properties
  Width  = 807
  Height = 578
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-12-11"
  HelpContextID=82475881
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsma_kdg"
  cComment = "Manutenzione campo GESTGUID"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_GESTGUID = space(14)
  w_ARCODART = space(20)
  w_ARTIPART = space(2)
  w_ZGUID = .NULL.
  w_ZATTRIB = .NULL.
  w_ZARTICO = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kdgPag1","gsma_kdg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZGUID = this.oPgFrm.Pages(1).oPag.ZGUID
    this.w_ZATTRIB = this.oPgFrm.Pages(1).oPag.ZATTRIB
    this.w_ZARTICO = this.oPgFrm.Pages(1).oPag.ZARTICO
    DoDefault()
    proc Destroy()
      this.w_ZGUID = .NULL.
      this.w_ZATTRIB = .NULL.
      this.w_ZARTICO = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GESTGUID=space(14)
      .w_ARCODART=space(20)
      .w_ARTIPART=space(2)
      .oPgFrm.Page1.oPag.ZGUID.Calculate()
        .w_GESTGUID = .w_ZGUID.getvar("GESTGUID")
      .oPgFrm.Page1.oPag.ZATTRIB.Calculate(.w_GESTGUID)
      .oPgFrm.Page1.oPag.ZARTICO.Calculate(.w_GESTGUID)
        .w_ARCODART = .w_ZARTICO.getvar("ARCODART")
        .w_ARTIPART = .w_ZARTICO.getvar("ARTIPART")
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZGUID.Calculate()
            .w_GESTGUID = .w_ZGUID.getvar("GESTGUID")
        .oPgFrm.Page1.oPag.ZATTRIB.Calculate(.w_GESTGUID)
        .oPgFrm.Page1.oPag.ZARTICO.Calculate(.w_GESTGUID)
            .w_ARCODART = .w_ZARTICO.getvar("ARCODART")
            .w_ARTIPART = .w_ZARTICO.getvar("ARTIPART")
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZGUID.Calculate()
        .oPgFrm.Page1.oPag.ZATTRIB.Calculate(.w_GESTGUID)
        .oPgFrm.Page1.oPag.ZARTICO.Calculate(.w_GESTGUID)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_15.visible=!this.oPgFrm.Page1.oPag.oBtn_1_15.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_16.visible=!this.oPgFrm.Page1.oPag.oBtn_1_16.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_17.visible=!this.oPgFrm.Page1.oPag.oBtn_1_17.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_18.visible=!this.oPgFrm.Page1.oPag.oBtn_1_18.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZGUID.Event(cEvent)
      .oPgFrm.Page1.oPag.ZATTRIB.Event(cEvent)
      .oPgFrm.Page1.oPag.ZARTICO.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsma_kdgPag1 as StdContainer
  Width  = 803
  height = 578
  stdWidth  = 803
  stdheight = 578
  resizeXpos=605
  resizeYpos=459
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZGUID as cp_zoombox with uid="CSEIWPIZWS",left=6, top=17, width=217,height=216,;
    caption='ZGUID',;
   bGlobalFont=.t.,;
    cZoomFile="GSMA_KDG",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.t.,bReadOnly=.t.,cMenuFile="",cTable="ART_ICOL",cZoomOnZoom="",bQueryOnDblClick=.t.,bRetriveAllRows=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 6020714


  add object ZATTRIB as cp_szoombox with uid="DDSWIHIWMS",left=224, top=17, width=521,height=216,;
    caption='ZATTRIB',;
   bGlobalFont=.t.,;
    cZoomFile="GSAR2KGG",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.t.,bReadOnly=.t.,cMenuFile="",cTable="ASS_ATTR",cZoomOnZoom="",bQueryOnDblClick=.t.,bRetriveAllRows=.f.,bNoZoomGridShape=.f.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 160369558


  add object ZARTICO as cp_szoombox with uid="GQRYAHMNDQ",left=6, top=286, width=793,height=241,;
    caption='ZARTICO',;
   bGlobalFont=.t.,;
    cZoomFile="GSMA1KDG",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.t.,bReadOnly=.t.,cMenuFile="",cTable="ART_ICOL",cZoomOnZoom="",bQueryOnDblClick=.t.,bRetriveAllRows=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 50260886


  add object oBtn_1_8 as StdButton with uid="BRVGHGZAYB",left=748, top=17, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue la ricerca";
    , HelpContextID = 77741565;
    , caption='\<Ricerca';
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      with this.Parent.oContained
        .notifyevent("Interroga")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="GQMUPTTTCY",left=226, top=237, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutti gli attributi";
    , HelpContextID = 8470038;
    , caption='\<Seleziona';
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        update (.w_ZATTRIB.cCursor) set xchk=1
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="ZCYHLDNLVF",left=279, top=237, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutti gli attributi";
    , HelpContextID = 8470038;
    , caption='\<Deselez.';
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      with this.Parent.oContained
        update (.w_ZATTRIB.cCursor) set xchk=0
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="GUQBLQMZBT",left=332, top=237, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione di tutti gli attributi";
    , HelpContextID = 8470038;
    , caption='\<Inv. Sel.';
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      with this.Parent.oContained
        update (.w_ZATTRIB.cCursor) set xchk=1-xchk
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="JWNEWBZIVQ",left=6, top=530, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutti gli intestatari";
    , HelpContextID = 8470038;
    , caption='Se\<leziona';
  , bGlobalFont=.t.

    proc oBtn_1_12.Click()
      with this.Parent.oContained
        update (.w_ZARTICO.cCursor) set xchk=1
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="ZCDFGEJBGP",left=59, top=530, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutti gli intestatari";
    , HelpContextID = 8470038;
    , caption='Desele\<z.';
  , bGlobalFont=.t.

    proc oBtn_1_13.Click()
      with this.Parent.oContained
        update (.w_ZARTICO.cCursor) set xchk=0
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="GQRKBGNJGG",left=112, top=530, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione di tutti gli intestatari";
    , HelpContextID = 8470038;
    , caption='Inv\<. Sel.';
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      with this.Parent.oContained
        update (.w_ZARTICO.cCursor) set xchk=1-xchk
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="CQGSBKYLVQ",left=208, top=530, width=48,height=45,;
    CpPicture="BMP\mostra.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire l'articolo selezionato";
    , HelpContextID = 11943285;
    , caption='\<Articoli';
  , bGlobalFont=.t.

    proc oBtn_1_15.Click()
      with this.Parent.oContained
        do opengest with "A", "GSMA_AAR",  "ARCODART", .w_ARCODART
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_ARCODART) or not inlist(.w_ARTIPART,'PF','SE','MP','MC','MA','IM','PH'))
     endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="GYWOMGGEVP",left=208, top=530, width=48,height=45,;
    CpPicture="BMP\mostra.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire il servizio selezionato";
    , HelpContextID = 64810603;
    , caption='Ser\<vizi';
  , bGlobalFont=.t.

    proc oBtn_1_16.Click()
      with this.Parent.oContained
        do opengest with "A", "GSMA_AAS",  "ARCODART", .w_ARCODART
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_ARCODART) or not inlist(.w_ARTIPART,'FM','FO','DE'))
     endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="MYUSUCOLFL",left=208, top=530, width=48,height=45,;
    CpPicture="BMP\mostra.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire l'articolo composto/kit  selezionato";
    , HelpContextID = 81972058;
    , caption='\<Kit';
  , bGlobalFont=.t.

    proc oBtn_1_17.Click()
      with this.Parent.oContained
        do opengest with "A", "GSMA_AAC",  "ARCODART", .w_ARCODART
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_ARCODART) or .w_ARTIPART<>'AC')
     endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="TGDROFUKHE",left=626, top=530, width=48,height=45,;
    CpPicture="bmp\tattrib.ico", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la gestione aggiornamento attributi commerciali";
    , HelpContextID = 208821961;
    , caption='A\<gg. Attr.';
  , bGlobalFont=.t.

    proc oBtn_1_18.Click()
      do gsve_kcc with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not isAhe())
     endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="TMLQHKZUII",left=695, top=530, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'associazione tra clienti fornitori e attributi selezionati";
    , HelpContextID = 82455322;
    , Caption='\<Ok';
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSMA_BDG(this.Parent.oContained,"ASSEGNA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="THVPLOIFWZ",left=749, top=530, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 240244486;
    , Caption='\<Esci';
  , bGlobalFont=.t.

    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="NXGOLMQGRJ",Visible=.t., Left=6, Top=1,;
    Alignment=0, Width=116, Height=18,;
    Caption="Elenco GESTGUID"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="WRFOGXYTBB",Visible=.t., Left=226, Top=1,;
    Alignment=0, Width=122, Height=18,;
    Caption="Attributi associati"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="MDBQALMEBP",Visible=.t., Left=6, Top=270,;
    Alignment=0, Width=192, Height=18,;
    Caption="Articoli/Servizi da aggiornare"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kdg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
