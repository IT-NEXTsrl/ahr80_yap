* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_miv                                                        *
*              Registrazioni IVA                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_92]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-20                                                      *
* Last revis.: 2016-04-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_miv")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_miv")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_miv")
  return

* --- Class definition
define class tgscg_miv as StdPCForm
  Width  = 502
  Height = 170
  Top    = 6
  Left   = 12
  cComment = "Registrazioni IVA"
  cPrg = "gscg_miv"
  HelpContextID=264906089
  add object cnt as tcgscg_miv
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_miv as PCContext
  w_IVSERIAL = space(10)
  w_FLCOMP1 = space(1)
  w_GIACAR = space(1)
  w_DATREG = space(8)
  w_DATAINIZIO = space(8)
  w_TIPREG = space(10)
  w_DIFDOC = 0
  w_OBTEST = space(8)
  w_IVCODIVA = space(5)
  w_CODIVA = space(5)
  w_PERIVA = 0
  w_IVTIPCON = space(1)
  w_IVCODCON = space(15)
  w_IVCODCON = space(15)
  w_IVIMPONI = 0
  w_IMPONI = 0
  w_IVIMPIVA = 0
  w_IVTIPREG = space(1)
  w_IVTIPREG = space(1)
  w_IVNUMREG = 0
  w_IVPERIND = 0
  w_DESIVA = space(35)
  w_IVCODCOI = space(15)
  w_CTIPREG = space(1)
  w_IVOBSO = space(8)
  w_IVFLOMAG = space(1)
  w_IVCFLOMA = space(1)
  w_IVTIPCOP = space(1)
  w_PLAIVA = space(1)
  w_IVCONTRO = space(15)
  w_DTOBSO = space(8)
  w_TEST = space(1)
  w_REVCHA = space(1)
  w_DESCON = space(40)
  w_TOTIMP = 0
  w_TOTIVA = 0
  w_TOTDOC = 0
  w_CODCON = space(15)
  w_CODCOI = space(15)
  w_REGMAR = space(1)
  w_CONTOIVA = space(10)
  proc Save(i_oFrom)
    this.w_IVSERIAL = i_oFrom.w_IVSERIAL
    this.w_FLCOMP1 = i_oFrom.w_FLCOMP1
    this.w_GIACAR = i_oFrom.w_GIACAR
    this.w_DATREG = i_oFrom.w_DATREG
    this.w_DATAINIZIO = i_oFrom.w_DATAINIZIO
    this.w_TIPREG = i_oFrom.w_TIPREG
    this.w_DIFDOC = i_oFrom.w_DIFDOC
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_IVCODIVA = i_oFrom.w_IVCODIVA
    this.w_CODIVA = i_oFrom.w_CODIVA
    this.w_PERIVA = i_oFrom.w_PERIVA
    this.w_IVTIPCON = i_oFrom.w_IVTIPCON
    this.w_IVCODCON = i_oFrom.w_IVCODCON
    this.w_IVCODCON = i_oFrom.w_IVCODCON
    this.w_IVIMPONI = i_oFrom.w_IVIMPONI
    this.w_IMPONI = i_oFrom.w_IMPONI
    this.w_IVIMPIVA = i_oFrom.w_IVIMPIVA
    this.w_IVTIPREG = i_oFrom.w_IVTIPREG
    this.w_IVTIPREG = i_oFrom.w_IVTIPREG
    this.w_IVNUMREG = i_oFrom.w_IVNUMREG
    this.w_IVPERIND = i_oFrom.w_IVPERIND
    this.w_DESIVA = i_oFrom.w_DESIVA
    this.w_IVCODCOI = i_oFrom.w_IVCODCOI
    this.w_CTIPREG = i_oFrom.w_CTIPREG
    this.w_IVOBSO = i_oFrom.w_IVOBSO
    this.w_IVFLOMAG = i_oFrom.w_IVFLOMAG
    this.w_IVCFLOMA = i_oFrom.w_IVCFLOMA
    this.w_IVTIPCOP = i_oFrom.w_IVTIPCOP
    this.w_PLAIVA = i_oFrom.w_PLAIVA
    this.w_IVCONTRO = i_oFrom.w_IVCONTRO
    this.w_DTOBSO = i_oFrom.w_DTOBSO
    this.w_TEST = i_oFrom.w_TEST
    this.w_REVCHA = i_oFrom.w_REVCHA
    this.w_DESCON = i_oFrom.w_DESCON
    this.w_TOTIMP = i_oFrom.w_TOTIMP
    this.w_TOTIVA = i_oFrom.w_TOTIVA
    this.w_TOTDOC = i_oFrom.w_TOTDOC
    this.w_CODCON = i_oFrom.w_CODCON
    this.w_CODCOI = i_oFrom.w_CODCOI
    this.w_REGMAR = i_oFrom.w_REGMAR
    this.w_CONTOIVA = i_oFrom.w_CONTOIVA
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_IVSERIAL = this.w_IVSERIAL
    i_oTo.w_FLCOMP1 = this.w_FLCOMP1
    i_oTo.w_GIACAR = this.w_GIACAR
    i_oTo.w_DATREG = this.w_DATREG
    i_oTo.w_DATAINIZIO = this.w_DATAINIZIO
    i_oTo.w_TIPREG = this.w_TIPREG
    i_oTo.w_DIFDOC = this.w_DIFDOC
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_IVCODIVA = this.w_IVCODIVA
    i_oTo.w_CODIVA = this.w_CODIVA
    i_oTo.w_PERIVA = this.w_PERIVA
    i_oTo.w_IVTIPCON = this.w_IVTIPCON
    i_oTo.w_IVCODCON = this.w_IVCODCON
    i_oTo.w_IVCODCON = this.w_IVCODCON
    i_oTo.w_IVIMPONI = this.w_IVIMPONI
    i_oTo.w_IMPONI = this.w_IMPONI
    i_oTo.w_IVIMPIVA = this.w_IVIMPIVA
    i_oTo.w_IVTIPREG = this.w_IVTIPREG
    i_oTo.w_IVTIPREG = this.w_IVTIPREG
    i_oTo.w_IVNUMREG = this.w_IVNUMREG
    i_oTo.w_IVPERIND = this.w_IVPERIND
    i_oTo.w_DESIVA = this.w_DESIVA
    i_oTo.w_IVCODCOI = this.w_IVCODCOI
    i_oTo.w_CTIPREG = this.w_CTIPREG
    i_oTo.w_IVOBSO = this.w_IVOBSO
    i_oTo.w_IVFLOMAG = this.w_IVFLOMAG
    i_oTo.w_IVCFLOMA = this.w_IVCFLOMA
    i_oTo.w_IVTIPCOP = this.w_IVTIPCOP
    i_oTo.w_PLAIVA = this.w_PLAIVA
    i_oTo.w_IVCONTRO = this.w_IVCONTRO
    i_oTo.w_DTOBSO = this.w_DTOBSO
    i_oTo.w_TEST = this.w_TEST
    i_oTo.w_REVCHA = this.w_REVCHA
    i_oTo.w_DESCON = this.w_DESCON
    i_oTo.w_TOTIMP = this.w_TOTIMP
    i_oTo.w_TOTIVA = this.w_TOTIVA
    i_oTo.w_TOTDOC = this.w_TOTDOC
    i_oTo.w_CODCON = this.w_CODCON
    i_oTo.w_CODCOI = this.w_CODCOI
    i_oTo.w_REGMAR = this.w_REGMAR
    i_oTo.w_CONTOIVA = this.w_CONTOIVA
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_miv as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 502
  Height = 170
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-04-06"
  HelpContextID=264906089
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PNT_IVA_IDX = 0
  CONTI_IDX = 0
  VOCIIVA_IDX = 0
  cFile = "PNT_IVA"
  cKeySelect = "IVSERIAL"
  cKeyWhere  = "IVSERIAL=this.w_IVSERIAL"
  cKeyDetail  = "IVSERIAL=this.w_IVSERIAL"
  cKeyWhereODBC = '"IVSERIAL="+cp_ToStrODBC(this.w_IVSERIAL)';

  cKeyDetailWhereODBC = '"IVSERIAL="+cp_ToStrODBC(this.w_IVSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PNT_IVA.IVSERIAL="+cp_ToStrODBC(this.w_IVSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PNT_IVA.CPROWNUM '
  cPrg = "gscg_miv"
  cComment = "Registrazioni IVA"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 4
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_IVSERIAL = space(10)
  w_FLCOMP1 = space(1)
  w_GIACAR = space(1)
  w_DATREG = ctod('  /  /  ')
  w_DATAINIZIO = ctod('  /  /  ')
  w_TIPREG = space(10)
  w_DIFDOC = 0
  w_OBTEST = ctod('  /  /  ')
  w_IVCODIVA = space(5)
  o_IVCODIVA = space(5)
  w_CODIVA = space(5)
  w_PERIVA = 0
  w_IVTIPCON = space(1)
  w_IVCODCON = space(15)
  w_IVCODCON = space(15)
  w_IVIMPONI = 0
  o_IVIMPONI = 0
  w_IMPONI = 0
  w_IVIMPIVA = 0
  w_IVTIPREG = space(1)
  w_IVTIPREG = space(1)
  w_IVNUMREG = 0
  w_IVPERIND = 0
  w_DESIVA = space(35)
  w_IVCODCOI = space(15)
  w_CTIPREG = space(1)
  w_IVOBSO = ctod('  /  /  ')
  w_IVFLOMAG = space(1)
  w_IVCFLOMA = space(1)
  w_IVTIPCOP = space(1)
  w_PLAIVA = space(1)
  w_IVCONTRO = space(15)
  w_DTOBSO = ctod('  /  /  ')
  w_TEST = .F.
  w_REVCHA = space(1)
  w_DESCON = space(40)
  w_TOTIMP = 0
  w_TOTIVA = 0
  w_TOTDOC = 0
  w_CODCON = space(15)
  w_CODCOI = space(15)
  w_REGMAR = space(1)
  w_CONTOIVA = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mivPag1","gscg_miv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPREG_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VOCIIVA'
    this.cWorkTables[3]='PNT_IVA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PNT_IVA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PNT_IVA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_miv'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_22_joined
    link_2_22_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PNT_IVA where IVSERIAL=KeySet.IVSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PNT_IVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_IVA_IDX,2],this.bLoadRecFilter,this.PNT_IVA_IDX,"gscg_miv")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PNT_IVA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PNT_IVA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PNT_IVA '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_22_joined=this.AddJoinedLink_2_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IVSERIAL',this.w_IVSERIAL  )
      select * from (i_cTable) PNT_IVA where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_FLCOMP1 = THIS.oParentObject .w_FLCOMP
        .w_GIACAR = IIF(.cFunction='Load', ' ', 'C')
        .w_DATREG = this.oParentObject .w_PNDATREG
        .w_DATAINIZIO = iif(type('this.oParentObject .w_DATINITRA')='D',iif(empty(this.oParentObject .w_DATINITRA),i_FINDAT,this.oParentObject .w_DATINITRA),ctod('  -  -    '))
        .w_DIFDOC = 0
        .w_TOTIMP = 0
        .w_TOTIVA = 0
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_IVSERIAL = NVL(IVSERIAL,space(10))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .w_OBTEST = this.oparentobject.w_OBTEST
        .w_TOTDOC = .w_TOTIMP+.w_TOTIVA
        .w_CODCON = IIF(NOT EMPTY(.w_IVCODCON), .w_IVCODCON, .w_CODCON)
        .w_CODCOI = IIF(NOT EMPTY(.w_IVCODCOI), .w_IVCODCOI, .w_CODCOI)
        cp_LoadRecExtFlds(this,'PNT_IVA')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTIMP = 0
      this.w_TOTIVA = 0
      scan
        with this
        .w_TIPREG = THIS.oParentObject .w_PNTIPREG
          .w_PERIVA = 0
          .w_DESIVA = space(35)
          .w_IVOBSO = ctod("  /  /  ")
          .w_PLAIVA = space(1)
          .w_DTOBSO = ctod("  /  /  ")
        .w_TEST = .T.
          .w_REVCHA = space(1)
          .w_DESCON = space(40)
          .w_REGMAR = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_IVCODIVA = NVL(IVCODIVA,space(5))
          if link_2_1_joined
            this.w_IVCODIVA = NVL(IVCODIVA201,NVL(this.w_IVCODIVA,space(5)))
            this.w_DESIVA = NVL(IVDESIVA201,space(35))
            this.w_IVPERIND = NVL(IVPERIND201,0)
            this.w_PERIVA = NVL(IVPERIVA201,0)
            this.w_IVOBSO = NVL(cp_ToDate(IVDTOBSO201),ctod("  /  /  "))
            this.w_PLAIVA = NVL(IVPLAIVA201,space(1))
            this.w_REVCHA = NVL(IVREVCHA201,space(1))
            this.w_REGMAR = NVL(IVREGMAR201,space(1))
          else
          .link_2_1('Load')
          endif
        .w_CODIVA = .w_IVCODIVA
          .w_IVTIPCON = NVL(IVTIPCON,space(1))
          .w_IVCODCON = NVL(IVCODCON,space(15))
          * evitabile
          *.link_2_5('Load')
          .w_IVCODCON = NVL(IVCODCON,space(15))
          * evitabile
          *.link_2_6('Load')
          .w_IVIMPONI = NVL(IVIMPONI,0)
        .w_IMPONI = .w_IVIMPONI
          .w_IVIMPIVA = NVL(IVIMPIVA,0)
          .w_IVTIPREG = NVL(IVTIPREG,space(1))
          .w_IVTIPREG = NVL(IVTIPREG,space(1))
          .w_IVNUMREG = NVL(IVNUMREG,0)
          .w_IVPERIND = NVL(IVPERIND,0)
          .w_IVCODCOI = NVL(IVCODCOI,space(15))
        .w_CTIPREG = .w_IVTIPREG
          .w_IVFLOMAG = NVL(IVFLOMAG,space(1))
          .w_IVCFLOMA = NVL(IVCFLOMA,space(1))
          .w_IVTIPCOP = NVL(IVTIPCOP,space(1))
          .w_IVCONTRO = NVL(IVCONTRO,space(15))
          if link_2_22_joined
            this.w_IVCONTRO = NVL(ANCODICE222,NVL(this.w_IVCONTRO,space(15)))
            this.w_DESCON = NVL(ANDESCRI222,space(40))
            this.w_DTOBSO = NVL(cp_ToDate(ANDTOBSO222),ctod("  /  /  "))
          else
          .link_2_22('Load')
          endif
        .w_CONTOIVA = .w_IVCODIVA
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTIMP = .w_TOTIMP+.w_IMPONI
          .w_TOTIVA = .w_TOTIVA+.w_IVIMPIVA
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .w_OBTEST = this.oparentobject.w_OBTEST
        .w_TOTDOC = .w_TOTIMP+.w_TOTIVA
        .w_CODCON = IIF(NOT EMPTY(.w_IVCODCON), .w_IVCODCON, .w_CODCON)
        .w_CODCOI = IIF(NOT EMPTY(.w_IVCODCOI), .w_IVCODCOI, .w_CODCOI)
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_IVSERIAL=space(10)
      .w_FLCOMP1=space(1)
      .w_GIACAR=space(1)
      .w_DATREG=ctod("  /  /  ")
      .w_DATAINIZIO=ctod("  /  /  ")
      .w_TIPREG=space(10)
      .w_DIFDOC=0
      .w_OBTEST=ctod("  /  /  ")
      .w_IVCODIVA=space(5)
      .w_CODIVA=space(5)
      .w_PERIVA=0
      .w_IVTIPCON=space(1)
      .w_IVCODCON=space(15)
      .w_IVCODCON=space(15)
      .w_IVIMPONI=0
      .w_IMPONI=0
      .w_IVIMPIVA=0
      .w_IVTIPREG=space(1)
      .w_IVTIPREG=space(1)
      .w_IVNUMREG=0
      .w_IVPERIND=0
      .w_DESIVA=space(35)
      .w_IVCODCOI=space(15)
      .w_CTIPREG=space(1)
      .w_IVOBSO=ctod("  /  /  ")
      .w_IVFLOMAG=space(1)
      .w_IVCFLOMA=space(1)
      .w_IVTIPCOP=space(1)
      .w_PLAIVA=space(1)
      .w_IVCONTRO=space(15)
      .w_DTOBSO=ctod("  /  /  ")
      .w_TEST=.f.
      .w_REVCHA=space(1)
      .w_DESCON=space(40)
      .w_TOTIMP=0
      .w_TOTIVA=0
      .w_TOTDOC=0
      .w_CODCON=space(15)
      .w_CODCOI=space(15)
      .w_REGMAR=space(1)
      .w_CONTOIVA=space(10)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,1,.f.)
        .w_FLCOMP1 = THIS.oParentObject .w_FLCOMP
        .w_GIACAR = IIF(.cFunction='Load', ' ', 'C')
        .w_DATREG = this.oParentObject .w_PNDATREG
        .w_DATAINIZIO = iif(type('this.oParentObject .w_DATINITRA')='D',iif(empty(this.oParentObject .w_DATINITRA),i_FINDAT,this.oParentObject .w_DATINITRA),ctod('  -  -    '))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_TIPREG = THIS.oParentObject .w_PNTIPREG
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .DoRTCalc(7,7,.f.)
        .w_OBTEST = this.oparentobject.w_OBTEST
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_IVCODIVA))
         .link_2_1('Full')
        endif
        .w_CODIVA = .w_IVCODIVA
        .DoRTCalc(11,11,.f.)
        .w_IVTIPCON = 'G'
        .w_IVCODCON = iif(this.oparentobject.w_PNTIPREG = .w_IVTIPREG, this.oparentobject.w_CCCONIVA, .w_IVCODCON)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_IVCODCON))
         .link_2_5('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_IVCODCON))
         .link_2_6('Full')
        endif
        .DoRTCalc(15,15,.f.)
        .w_IMPONI = .w_IVIMPONI
        .DoRTCalc(17,17,.f.)
        .w_IVTIPREG = THIS.oParentObject .w_PNTIPREG
        .w_IVTIPREG = THIS.oParentObject .w_PNTIPREG
        .w_IVNUMREG = THIS.oParentObject .w_PNNUMREG
        .DoRTCalc(21,23,.f.)
        .w_CTIPREG = .w_IVTIPREG
        .DoRTCalc(25,25,.f.)
        .w_IVFLOMAG = 'X'
        .w_IVCFLOMA = .w_IVFLOMAG
        .w_IVTIPCOP = 'G'
        .DoRTCalc(29,30,.f.)
        if not(empty(.w_IVCONTRO))
         .link_2_22('Full')
        endif
        .DoRTCalc(31,31,.f.)
        .w_TEST = .T.
        .DoRTCalc(33,36,.f.)
        .w_TOTDOC = .w_TOTIMP+.w_TOTIVA
        .w_CODCON = IIF(NOT EMPTY(.w_IVCODCON), .w_IVCODCON, .w_CODCON)
        .w_CODCOI = IIF(NOT EMPTY(.w_IVCODCOI), .w_IVCODCOI, .w_CODCOI)
        .DoRTCalc(40,40,.f.)
        .w_CONTOIVA = .w_IVCODIVA
      endif
    endwith
    cp_BlankRecExtFlds(this,'PNT_IVA')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oTIPREG_1_11.enabled = i_bVal
      .Page1.oPag.oIVFLOMAG_2_18.enabled = i_bVal
      .Page1.oPag.oIVTIPCOP_2_20.enabled = i_bVal
      .Page1.oPag.oIVCONTRO_2_22.enabled = i_bVal
      .Page1.oPag.oObj_1_8.enabled = i_bVal
      .Page1.oPag.oObj_1_9.enabled = i_bVal
      .Page1.oPag.oObj_1_10.enabled = i_bVal
      .Page1.oPag.oObj_1_12.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PNT_IVA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PNT_IVA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVSERIAL,"IVSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- gscg_miv
    **aggiunta chiamata a cp_buildwhere per gestire i filtri F12 anche sui campi del castelletto
    local ogg
    **bisogna leggere il valore direttamente dalla casella di testo poich� le variabili di work non sono ancora aggiornate
    ogg=this.getbodyctrl("w_IVCODIVA")
    i_cFlt = cp_BuildWhere(i_cFlt, ogg.value,"IVCODIVA",i_nConn)
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_TIPREG N(3);
      ,t_IVCODIVA C(5);
      ,t_IVIMPONI N(18,4);
      ,t_IVIMPIVA N(18,4);
      ,t_IVTIPREG N(3);
      ,t_IVNUMREG N(2);
      ,t_IVPERIND N(3);
      ,t_DESIVA C(35);
      ,t_IVFLOMAG N(3);
      ,t_IVTIPCOP C(1);
      ,t_IVCONTRO C(15);
      ,t_DESCON C(40);
      ,CPROWNUM N(10);
      ,t_CODIVA C(5);
      ,t_PERIVA N(5,2);
      ,t_IVTIPCON C(1);
      ,t_IVCODCON C(15);
      ,t_IMPONI N(18,4);
      ,t_IVCODCOI C(15);
      ,t_CTIPREG C(1);
      ,t_IVOBSO D(8);
      ,t_IVCFLOMA C(1);
      ,t_PLAIVA C(1);
      ,t_DTOBSO D(8);
      ,t_TEST L(1);
      ,t_REVCHA C(1);
      ,t_REGMAR C(1);
      ,t_CONTOIVA C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mivbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIVCODIVA_2_1.controlsource=this.cTrsName+'.t_IVCODIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIVIMPONI_2_7.controlsource=this.cTrsName+'.t_IVIMPONI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIVIMPIVA_2_9.controlsource=this.cTrsName+'.t_IVIMPIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_10.controlsource=this.cTrsName+'.t_IVTIPREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_11.controlsource=this.cTrsName+'.t_IVTIPREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIVNUMREG_2_12.controlsource=this.cTrsName+'.t_IVNUMREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIVPERIND_2_13.controlsource=this.cTrsName+'.t_IVPERIND'
    this.oPgFRm.Page1.oPag.oDESIVA_2_14.controlsource=this.cTrsName+'.t_DESIVA'
    this.oPgFRm.Page1.oPag.oIVFLOMAG_2_18.controlsource=this.cTrsName+'.t_IVFLOMAG'
    this.oPgFRm.Page1.oPag.oIVTIPCOP_2_20.controlsource=this.cTrsName+'.t_IVTIPCOP'
    this.oPgFRm.Page1.oPag.oIVCONTRO_2_22.controlsource=this.cTrsName+'.t_IVCONTRO'
    this.oPgFRm.Page1.oPag.oDESCON_2_26.controlsource=this.cTrsName+'.t_DESCON'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(65)
    this.AddVLine(196)
    this.AddVLine(325)
    this.AddVLine(391)
    this.AddVLine(418)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVCODIVA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PNT_IVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_IVA_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PNT_IVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_IVA_IDX,2])
      *
      * insert into PNT_IVA
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PNT_IVA')
        i_extval=cp_InsertValODBCExtFlds(this,'PNT_IVA')
        i_cFldBody=" "+;
                  "(IVSERIAL,IVCODIVA,IVTIPCON,IVCODCON,IVIMPONI"+;
                  ",IVIMPIVA,IVTIPREG,IVNUMREG,IVPERIND,IVCODCOI"+;
                  ",IVFLOMAG,IVCFLOMA,IVTIPCOP,IVCONTRO,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_IVSERIAL)+","+cp_ToStrODBCNull(this.w_IVCODIVA)+","+cp_ToStrODBC(this.w_IVTIPCON)+","+cp_ToStrODBCNull(this.w_IVCODCON)+","+cp_ToStrODBC(this.w_IVIMPONI)+;
             ","+cp_ToStrODBC(this.w_IVIMPIVA)+","+cp_ToStrODBC(this.w_IVTIPREG)+","+cp_ToStrODBC(this.w_IVNUMREG)+","+cp_ToStrODBC(this.w_IVPERIND)+","+cp_ToStrODBC(this.w_IVCODCOI)+;
             ","+cp_ToStrODBC(this.w_IVFLOMAG)+","+cp_ToStrODBC(this.w_IVCFLOMA)+","+cp_ToStrODBC(this.w_IVTIPCOP)+","+cp_ToStrODBCNull(this.w_IVCONTRO)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PNT_IVA')
        i_extval=cp_InsertValVFPExtFlds(this,'PNT_IVA')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'IVSERIAL',this.w_IVSERIAL)
        INSERT INTO (i_cTable) (;
                   IVSERIAL;
                  ,IVCODIVA;
                  ,IVTIPCON;
                  ,IVCODCON;
                  ,IVIMPONI;
                  ,IVIMPIVA;
                  ,IVTIPREG;
                  ,IVNUMREG;
                  ,IVPERIND;
                  ,IVCODCOI;
                  ,IVFLOMAG;
                  ,IVCFLOMA;
                  ,IVTIPCOP;
                  ,IVCONTRO;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_IVSERIAL;
                  ,this.w_IVCODIVA;
                  ,this.w_IVTIPCON;
                  ,this.w_IVCODCON;
                  ,this.w_IVIMPONI;
                  ,this.w_IVIMPIVA;
                  ,this.w_IVTIPREG;
                  ,this.w_IVNUMREG;
                  ,this.w_IVPERIND;
                  ,this.w_IVCODCOI;
                  ,this.w_IVFLOMAG;
                  ,this.w_IVCFLOMA;
                  ,this.w_IVTIPCOP;
                  ,this.w_IVCONTRO;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- gscg_miv
    * Per Build 49 - Elusione check riga piena
    Update (this.cTrsName) Set t_TEST=.f.
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PNT_IVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_IVA_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_IVCODIVA<>SPACE(5) AND (t_IVIMPONI<>0 OR t_IVIMPIVA<>0 OR t_IVCFLOMA$'SZ' OR t_TEST)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PNT_IVA')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PNT_IVA')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_IVCODIVA<>SPACE(5) AND (t_IVIMPONI<>0 OR t_IVIMPIVA<>0 OR t_IVCFLOMA$'SZ' OR t_TEST)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PNT_IVA
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PNT_IVA')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " IVCODIVA="+cp_ToStrODBCNull(this.w_IVCODIVA)+;
                     ",IVTIPCON="+cp_ToStrODBC(this.w_IVTIPCON)+;
                     ",IVCODCON="+cp_ToStrODBCNull(this.w_IVCODCON)+;
                     ",IVIMPONI="+cp_ToStrODBC(this.w_IVIMPONI)+;
                     ",IVIMPIVA="+cp_ToStrODBC(this.w_IVIMPIVA)+;
                     ",IVTIPREG="+cp_ToStrODBC(this.w_IVTIPREG)+;
                     ",IVNUMREG="+cp_ToStrODBC(this.w_IVNUMREG)+;
                     ",IVPERIND="+cp_ToStrODBC(this.w_IVPERIND)+;
                     ",IVCODCOI="+cp_ToStrODBC(this.w_IVCODCOI)+;
                     ",IVFLOMAG="+cp_ToStrODBC(this.w_IVFLOMAG)+;
                     ",IVCFLOMA="+cp_ToStrODBC(this.w_IVCFLOMA)+;
                     ",IVTIPCOP="+cp_ToStrODBC(this.w_IVTIPCOP)+;
                     ",IVCONTRO="+cp_ToStrODBCNull(this.w_IVCONTRO)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PNT_IVA')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      IVCODIVA=this.w_IVCODIVA;
                     ,IVTIPCON=this.w_IVTIPCON;
                     ,IVCODCON=this.w_IVCODCON;
                     ,IVIMPONI=this.w_IVIMPONI;
                     ,IVIMPIVA=this.w_IVIMPIVA;
                     ,IVTIPREG=this.w_IVTIPREG;
                     ,IVNUMREG=this.w_IVNUMREG;
                     ,IVPERIND=this.w_IVPERIND;
                     ,IVCODCOI=this.w_IVCODCOI;
                     ,IVFLOMAG=this.w_IVFLOMAG;
                     ,IVCFLOMA=this.w_IVCFLOMA;
                     ,IVTIPCOP=this.w_IVTIPCOP;
                     ,IVCONTRO=this.w_IVCONTRO;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gscg_miv
    * Per Build 49 - Elusione check riga piena
    Update (this.cTrsName) Set t_TEST=.t.
     * --- Necessario per allineare variabili di work dopo la scan del
     * --- temporaneo eseguita nella GSCG_MIV.mReplace
     this.WorkFromTrs()
    
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PNT_IVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_IVA_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_IVCODIVA<>SPACE(5) AND (t_IVIMPONI<>0 OR t_IVIMPIVA<>0 OR t_IVCFLOMA$'SZ' OR t_TEST)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PNT_IVA
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_IVCODIVA<>SPACE(5) AND (t_IVIMPONI<>0 OR t_IVIMPIVA<>0 OR t_IVCFLOMA$'SZ' OR t_TEST)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PNT_IVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_IVA_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .DoRTCalc(1,7,.t.)
          .w_OBTEST = this.oparentobject.w_OBTEST
        .DoRTCalc(9,9,.t.)
          .w_CODIVA = .w_IVCODIVA
        .DoRTCalc(11,11,.t.)
          .w_IVTIPCON = 'G'
          .w_IVCODCON = iif(this.oparentobject.w_PNTIPREG = .w_IVTIPREG, this.oparentobject.w_CCCONIVA, .w_IVCODCON)
          .link_2_5('Full')
          .link_2_6('Full')
        .DoRTCalc(15,15,.t.)
        if .o_IVIMPONI<>.w_IVIMPONI
          .w_TOTIMP = .w_TOTIMP-.w_imponi
          .w_IMPONI = .w_IVIMPONI
          .w_TOTIMP = .w_TOTIMP+.w_imponi
        endif
        .DoRTCalc(17,23,.t.)
          .w_CTIPREG = .w_IVTIPREG
        .DoRTCalc(25,26,.t.)
          .w_IVCFLOMA = .w_IVFLOMAG
        .DoRTCalc(28,36,.t.)
          .w_TOTDOC = .w_TOTIMP+.w_TOTIVA
          .w_CODCON = IIF(NOT EMPTY(.w_IVCODCON), .w_IVCODCON, .w_CODCON)
          .w_CODCOI = IIF(NOT EMPTY(.w_IVCODCOI), .w_IVCODCOI, .w_CODCOI)
        .DoRTCalc(40,40,.t.)
        if .o_IVCODIVA<>.w_IVCODIVA
          .w_CONTOIVA = .w_IVCODIVA
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CODIVA with this.w_CODIVA
      replace t_PERIVA with this.w_PERIVA
      replace t_IVTIPCON with this.w_IVTIPCON
      replace t_IVCODCON with this.w_IVCODCON
      replace t_IVCODCON with this.w_IVCODCON
      replace t_IMPONI with this.w_IMPONI
      replace t_IVCODCOI with this.w_IVCODCOI
      replace t_CTIPREG with this.w_CTIPREG
      replace t_IVOBSO with this.w_IVOBSO
      replace t_IVCFLOMA with this.w_IVCFLOMA
      replace t_PLAIVA with this.w_PLAIVA
      replace t_DTOBSO with this.w_DTOBSO
      replace t_TEST with this.w_TEST
      replace t_REVCHA with this.w_REVCHA
      replace t_REGMAR with this.w_REGMAR
      replace t_CONTOIVA with this.w_CONTOIVA
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_ASFLZRKQYI()
    with this
          * --- tipo registro
          .w_IVTIPREG = .w_TIPREG
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIVIMPONI_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIVIMPONI_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIVIMPIVA_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIVIMPIVA_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIVTIPREG_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIVTIPREG_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIVTIPREG_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIVTIPREG_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIVNUMREG_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIVNUMREG_2_12.mCond()
    this.oPgFrm.Page1.oPag.oIVFLOMAG_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oIVFLOMAG_2_18.mCond()
    this.oPgFrm.Page1.oPag.oIVTIPCOP_2_20.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oIVTIPCOP_2_20.mCond()
    this.oPgFrm.Page1.oPag.oIVCONTRO_2_22.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oIVCONTRO_2_22.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPREG_1_11.visible=!this.oPgFrm.Page1.oPag.oTIPREG_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_10.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_10.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_11.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_11.mHide()
    this.oPgFrm.Page1.oPag.oIVTIPCOP_2_20.visible=!this.oPgFrm.Page1.oPag.oIVTIPCOP_2_20.mHide()
    this.oPgFrm.Page1.oPag.oIVCONTRO_2_22.visible=!this.oPgFrm.Page1.oPag.oIVCONTRO_2_22.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_2_26.visible=!this.oPgFrm.Page1.oPag.oDESCON_2_26.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
        if lower(cEvent)==lower("w_TIPREG Changed")
          .Calculate_ASFLZRKQYI()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IVCODIVA
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_IVCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIND,IVPERIVA,IVDTOBSO,IVPLAIVA,IVREVCHA,IVREGMAR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_IVCODIVA))
          select IVCODIVA,IVDESIVA,IVPERIND,IVPERIVA,IVDTOBSO,IVPLAIVA,IVREVCHA,IVREGMAR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IVCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_IVCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIND,IVPERIVA,IVDTOBSO,IVPLAIVA,IVREVCHA,IVREGMAR";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_IVCODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVPERIND,IVPERIVA,IVDTOBSO,IVPLAIVA,IVREVCHA,IVREGMAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IVCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oIVCODIVA_2_1'),i_cWhere,'GSAR_AIV',"Codici IVA",'GSCG_IVA.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIND,IVPERIVA,IVDTOBSO,IVPLAIVA,IVREVCHA,IVREGMAR";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVPERIND,IVPERIVA,IVDTOBSO,IVPLAIVA,IVREVCHA,IVREGMAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIND,IVPERIVA,IVDTOBSO,IVPLAIVA,IVREVCHA,IVREGMAR";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_IVCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_IVCODIVA)
            select IVCODIVA,IVDESIVA,IVPERIND,IVPERIVA,IVDTOBSO,IVPLAIVA,IVREVCHA,IVREGMAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_IVPERIND = NVL(_Link_.IVPERIND,0)
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
      this.w_IVOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
      this.w_PLAIVA = NVL(_Link_.IVPLAIVA,space(1))
      this.w_REVCHA = NVL(_Link_.IVREVCHA,space(1))
      this.w_REGMAR = NVL(_Link_.IVREGMAR,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_IVCODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_IVPERIND = 0
      this.w_PERIVA = 0
      this.w_IVOBSO = ctod("  /  /  ")
      this.w_PLAIVA = space(1)
      this.w_REVCHA = space(1)
      this.w_REGMAR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IVOBSO>this.oparentobject.w_OBTEST OR EMPTY(.w_IVOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_IVCODIVA = space(5)
        this.w_DESIVA = space(35)
        this.w_IVPERIND = 0
        this.w_PERIVA = 0
        this.w_IVOBSO = ctod("  /  /  ")
        this.w_PLAIVA = space(1)
        this.w_REVCHA = space(1)
        this.w_REGMAR = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.IVCODIVA as IVCODIVA201"+ ",link_2_1.IVDESIVA as IVDESIVA201"+ ",link_2_1.IVPERIND as IVPERIND201"+ ",link_2_1.IVPERIVA as IVPERIVA201"+ ",link_2_1.IVDTOBSO as IVDTOBSO201"+ ",link_2_1.IVPLAIVA as IVPLAIVA201"+ ",link_2_1.IVREVCHA as IVREVCHA201"+ ",link_2_1.IVREGMAR as IVREGMAR201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on PNT_IVA.IVCODIVA=link_2_1.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and PNT_IVA.IVCODIVA=link_2_1.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IVCODCON
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_IVCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_IVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_IVTIPCON;
                       ,'ANCODICE',this.w_IVCODCON)
            select ANTIPCON,ANCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVCODCON = NVL(_Link_.ANCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_IVCODCON = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IVCODCON
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_IVCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_IVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_IVTIPCON;
                       ,'ANCODICE',this.w_IVCODCON)
            select ANTIPCON,ANCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVCODCON = NVL(_Link_.ANCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_IVCODCON = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IVCONTRO
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVCONTRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_IVCONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_IVTIPCOP);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_IVTIPCOP;
                     ,'ANCODICE',trim(this.w_IVCONTRO))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IVCONTRO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_IVCONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_IVTIPCOP);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_IVCONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_IVTIPCOP);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IVCONTRO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oIVCONTRO_2_22'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori/conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_IVTIPCOP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_IVTIPCOP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVCONTRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_IVCONTRO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_IVTIPCOP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_IVTIPCOP;
                       ,'ANCODICE',this.w_IVCONTRO)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVCONTRO = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_IVCONTRO = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVCONTRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_22.ANCODICE as ANCODICE222"+ ",link_2_22.ANDESCRI as ANDESCRI222"+ ",link_2_22.ANDTOBSO as ANDTOBSO222"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_22 on PNT_IVA.IVCONTRO=link_2_22.ANCODICE"+" and PNT_IVA.IVTIPCOP=link_2_22.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_22"
          i_cKey=i_cKey+'+" and PNT_IVA.IVCONTRO=link_2_22.ANCODICE(+)"'+'+" and PNT_IVA.IVTIPCOP=link_2_22.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTIPREG_1_11.RadioValue()==this.w_TIPREG)
      this.oPgFrm.Page1.oPag.oTIPREG_1_11.SetRadio()
      replace t_TIPREG with this.oPgFrm.Page1.oPag.oTIPREG_1_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_2_14.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_2_14.value=this.w_DESIVA
      replace t_DESIVA with this.oPgFrm.Page1.oPag.oDESIVA_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIVFLOMAG_2_18.RadioValue()==this.w_IVFLOMAG)
      this.oPgFrm.Page1.oPag.oIVFLOMAG_2_18.SetRadio()
      replace t_IVFLOMAG with this.oPgFrm.Page1.oPag.oIVFLOMAG_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIVTIPCOP_2_20.value==this.w_IVTIPCOP)
      this.oPgFrm.Page1.oPag.oIVTIPCOP_2_20.value=this.w_IVTIPCOP
      replace t_IVTIPCOP with this.oPgFrm.Page1.oPag.oIVTIPCOP_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIVCONTRO_2_22.value==this.w_IVCONTRO)
      this.oPgFrm.Page1.oPag.oIVCONTRO_2_22.value=this.w_IVCONTRO
      replace t_IVCONTRO with this.oPgFrm.Page1.oPag.oIVCONTRO_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_2_26.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_2_26.value=this.w_DESCON
      replace t_DESCON with this.oPgFrm.Page1.oPag.oDESCON_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMP_3_1.value==this.w_TOTIMP)
      this.oPgFrm.Page1.oPag.oTOTIMP_3_1.value=this.w_TOTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIVA_3_2.value==this.w_TOTIVA)
      this.oPgFrm.Page1.oPag.oTOTIVA_3_2.value=this.w_TOTIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVCODIVA_2_1.value==this.w_IVCODIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVCODIVA_2_1.value=this.w_IVCODIVA
      replace t_IVCODIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVCODIVA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVIMPONI_2_7.value==this.w_IVIMPONI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVIMPONI_2_7.value=this.w_IVIMPONI
      replace t_IVIMPONI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVIMPONI_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVIMPIVA_2_9.value==this.w_IVIMPIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVIMPIVA_2_9.value=this.w_IVIMPIVA
      replace t_IVIMPIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVIMPIVA_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_10.RadioValue()==this.w_IVTIPREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_10.SetRadio()
      replace t_IVTIPREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_11.RadioValue()==this.w_IVTIPREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_11.SetRadio()
      replace t_IVTIPREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVNUMREG_2_12.value==this.w_IVNUMREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVNUMREG_2_12.value=this.w_IVNUMREG
      replace t_IVNUMREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVNUMREG_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVPERIND_2_13.value==this.w_IVPERIND)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVPERIND_2_13.value=this.w_IVPERIND
      replace t_IVPERIND with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVPERIND_2_13.value
    endif
    cp_SetControlsValueExtFlds(this,'PNT_IVA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_miv
      IF NOT GSCG_BIK (this, .T.)
        i_bRes = .f.
        i_bnoChk = .f.
        i_cErrorMsg = AH_MSGFORMAT ("Impossibile azzerare importi di una riga del castelletto IVA in variazione. %0Occorre eliminarla")
      endif  
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_IVOBSO>this.oparentobject.w_OBTEST OR EMPTY(.w_IVOBSO)) and not(empty(.w_IVCODIVA)) and (.w_IVCODIVA<>SPACE(5) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0 OR .w_IVCFLOMA$'SZ' OR .w_TEST))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVCODIVA_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        case   not(NOT (NOT EMPTY(.w_IVCODIVA) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0)) OR CHKREGIV(.w_IVTIPREG,.w_IVNUMREG)=1) and (NOT EMPTY(.w_IVCODIVA) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0)) and (.w_IVCODIVA<>SPACE(5) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0 OR .w_IVCFLOMA$'SZ' OR .w_TEST))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVNUMREG_2_12
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Il registro � associato a pi� attivit� o a nessuna attivit�")
        case   not(.w_IVTIPCOP$"CFG") and (g_DETCON='S' AND NOT EMPTY(.w_IVCODIVA) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0 OR .w_IVCFLOMA='S')) and (.w_IVCODIVA<>SPACE(5) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0 OR .w_IVCFLOMA$'SZ' OR .w_TEST))
          .oNewFocus=.oPgFrm.Page1.oPag.oIVTIPCOP_2_20
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if .w_IVCODIVA<>SPACE(5) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0 OR .w_IVCFLOMA$'SZ' OR .w_TEST)
        * --- Area Manuale = Check Row
        * --- gscg_miv
        if isahr() and inlist(.cFunction,'Load','Edit') and g_TRAEXP $ 'A-C-G' and .w_DATREG >= .w_DATAINIZIO and empty(.w_IVCONTRO)
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = Ah_MsgFormat("Contropartita non definita su riga castelletto IVA")
        endif  
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IVCODIVA = this.w_IVCODIVA
    this.o_IVIMPONI = this.w_IVIMPONI
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_IVCODIVA<>SPACE(5) AND (t_IVIMPONI<>0 OR t_IVIMPIVA<>0 OR t_IVCFLOMA$'SZ' OR t_TEST))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_IVCODIVA=space(5)
      .w_CODIVA=space(5)
      .w_PERIVA=0
      .w_IVTIPCON=space(1)
      .w_IVCODCON=space(15)
      .w_IVCODCON=space(15)
      .w_IVIMPONI=0
      .w_IMPONI=0
      .w_IVIMPIVA=0
      .w_IVTIPREG=space(1)
      .w_IVTIPREG=space(1)
      .w_IVNUMREG=0
      .w_IVPERIND=0
      .w_DESIVA=space(35)
      .w_IVCODCOI=space(15)
      .w_CTIPREG=space(1)
      .w_IVOBSO=ctod("  /  /  ")
      .w_IVFLOMAG=space(1)
      .w_IVCFLOMA=space(1)
      .w_IVTIPCOP=space(1)
      .w_PLAIVA=space(1)
      .w_IVCONTRO=space(15)
      .w_DTOBSO=ctod("  /  /  ")
      .w_TEST=.f.
      .w_REVCHA=space(1)
      .w_DESCON=space(40)
      .w_REGMAR=space(1)
      .w_CONTOIVA=space(10)
      .DoRTCalc(1,5,.f.)
        .w_TIPREG = THIS.oParentObject .w_PNTIPREG
      .DoRTCalc(7,9,.f.)
      if not(empty(.w_IVCODIVA))
        .link_2_1('Full')
      endif
        .w_CODIVA = .w_IVCODIVA
      .DoRTCalc(11,11,.f.)
        .w_IVTIPCON = 'G'
        .w_IVCODCON = iif(this.oparentobject.w_PNTIPREG = .w_IVTIPREG, this.oparentobject.w_CCCONIVA, .w_IVCODCON)
      .DoRTCalc(13,13,.f.)
      if not(empty(.w_IVCODCON))
        .link_2_5('Full')
      endif
      .DoRTCalc(14,14,.f.)
      if not(empty(.w_IVCODCON))
        .link_2_6('Full')
      endif
      .DoRTCalc(15,15,.f.)
        .w_IMPONI = .w_IVIMPONI
      .DoRTCalc(17,17,.f.)
        .w_IVTIPREG = THIS.oParentObject .w_PNTIPREG
        .w_IVTIPREG = THIS.oParentObject .w_PNTIPREG
        .w_IVNUMREG = THIS.oParentObject .w_PNNUMREG
      .DoRTCalc(21,23,.f.)
        .w_CTIPREG = .w_IVTIPREG
      .DoRTCalc(25,25,.f.)
        .w_IVFLOMAG = 'X'
        .w_IVCFLOMA = .w_IVFLOMAG
        .w_IVTIPCOP = 'G'
      .DoRTCalc(29,30,.f.)
      if not(empty(.w_IVCONTRO))
        .link_2_22('Full')
      endif
      .DoRTCalc(31,31,.f.)
        .w_TEST = .T.
      .DoRTCalc(33,40,.f.)
        .w_CONTOIVA = .w_IVCODIVA
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_IVCODIVA = t_IVCODIVA
    this.w_CODIVA = t_CODIVA
    this.w_PERIVA = t_PERIVA
    this.w_IVTIPCON = t_IVTIPCON
    this.w_IVCODCON = t_IVCODCON
    this.w_IVCODCON = t_IVCODCON
    this.w_IVIMPONI = t_IVIMPONI
    this.w_IMPONI = t_IMPONI
    this.w_IVIMPIVA = t_IVIMPIVA
    this.w_IVTIPREG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_10.RadioValue(.t.)
    this.w_IVTIPREG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_11.RadioValue(.t.)
    this.w_IVNUMREG = t_IVNUMREG
    this.w_IVPERIND = t_IVPERIND
    this.w_DESIVA = t_DESIVA
    this.w_IVCODCOI = t_IVCODCOI
    this.w_CTIPREG = t_CTIPREG
    this.w_IVOBSO = t_IVOBSO
    this.w_IVFLOMAG = this.oPgFrm.Page1.oPag.oIVFLOMAG_2_18.RadioValue(.t.)
    this.w_IVCFLOMA = t_IVCFLOMA
    this.w_IVTIPCOP = t_IVTIPCOP
    this.w_PLAIVA = t_PLAIVA
    this.w_IVCONTRO = t_IVCONTRO
    this.w_DTOBSO = t_DTOBSO
    this.w_TEST = t_TEST
    this.w_REVCHA = t_REVCHA
    this.w_DESCON = t_DESCON
    this.w_REGMAR = t_REGMAR
    this.w_CONTOIVA = t_CONTOIVA
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_TIPREG with this.oPgFrm.Page1.oPag.oTIPREG_1_11.ToRadio()
    replace t_IVCODIVA with this.w_IVCODIVA
    replace t_CODIVA with this.w_CODIVA
    replace t_PERIVA with this.w_PERIVA
    replace t_IVTIPCON with this.w_IVTIPCON
    replace t_IVCODCON with this.w_IVCODCON
    replace t_IVCODCON with this.w_IVCODCON
    replace t_IVIMPONI with this.w_IVIMPONI
    replace t_IMPONI with this.w_IMPONI
    replace t_IVIMPIVA with this.w_IVIMPIVA
    replace t_IVTIPREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_10.ToRadio()
    replace t_IVTIPREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVTIPREG_2_11.ToRadio()
    replace t_IVNUMREG with this.w_IVNUMREG
    replace t_IVPERIND with this.w_IVPERIND
    replace t_DESIVA with this.w_DESIVA
    replace t_IVCODCOI with this.w_IVCODCOI
    replace t_CTIPREG with this.w_CTIPREG
    replace t_IVOBSO with this.w_IVOBSO
    replace t_IVFLOMAG with this.oPgFrm.Page1.oPag.oIVFLOMAG_2_18.ToRadio()
    replace t_IVCFLOMA with this.w_IVCFLOMA
    replace t_IVTIPCOP with this.w_IVTIPCOP
    replace t_PLAIVA with this.w_PLAIVA
    replace t_IVCONTRO with this.w_IVCONTRO
    replace t_DTOBSO with this.w_DTOBSO
    replace t_TEST with this.w_TEST
    replace t_REVCHA with this.w_REVCHA
    replace t_DESCON with this.w_DESCON
    replace t_REGMAR with this.w_REGMAR
    replace t_CONTOIVA with this.w_CONTOIVA
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTIMP = .w_TOTIMP-.w_imponi
        .w_TOTIVA = .w_TOTIVA-.w_ivimpiva
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mivPag1 as StdContainer
  Width  = 498
  height = 170
  stdWidth  = 498
  stdheight = 170
  resizeYpos=62
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="HBNWIGPNUX",left=5, top=1, width=487,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="IVCODIVA",Label1="Cod.IVA",Field2="IVIMPONI",Label2="Imponibile",Field3="IVIMPIVA",Label3="Imposta",Field4="IVTIPREG",Label4="Reg.IVA",Field5="IVNUMREG",Label5="N.",Field6="IVPERIND",Label6="%Ind.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 181527014


  add object oObj_1_8 as cp_runprogram with uid="BICXLLOMEF",left=1, top=237, width=335,height=18,;
    caption='GSCG_BSC',;
   bGlobalFont=.t.,;
    prg="GSCG_BSC",;
    cEvent = "w_IVIMPONI Changed,w_IVCODIVA Changed",;
    nPag=1;
    , HelpContextID = 141648553


  add object oObj_1_9 as cp_runprogram with uid="YDOTNSKSZW",left=1, top=271, width=335,height=18,;
    caption='GSCG_BP2',;
   bGlobalFont=.t.,;
    prg="GSCG_BP2",;
    cEvent = "CaricaAuto",;
    nPag=1;
    , HelpContextID = 141648536


  add object oObj_1_10 as cp_runprogram with uid="DZESDYCBOM",left=1, top=220, width=335,height=18,;
    caption='GSCG_BIK',;
   bGlobalFont=.t.,;
    prg="GSCG_BIK",;
    cEvent = "Insert row start,Update row start,Controlla",;
    nPag=1;
    , HelpContextID = 126786895


  add object oTIPREG_1_11 as StdCombo with uid="XWMIDGUXHE",rtseq=6,rtrep=.t.,left=386,top=127,width=108,height=21;
    , ToolTipText = "Tipo registro IVA";
    , HelpContextID = 199043382;
    , cFormVar="w_TIPREG",RowSource=""+"Vendite,"+"Acquisti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPREG_1_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TIPREG,&i_cF..t_TIPREG),this.value)
    return(iif(xVal =1,'V',;
    iif(xVal =2,'A',;
    space(10))))
  endfunc
  func oTIPREG_1_11.GetRadio()
    this.Parent.oContained.w_TIPREG = this.RadioValue()
    return .t.
  endfunc

  func oTIPREG_1_11.ToRadio()
    this.Parent.oContained.w_TIPREG=trim(this.Parent.oContained.w_TIPREG)
    return(;
      iif(this.Parent.oContained.w_TIPREG=='V',1,;
      iif(this.Parent.oContained.w_TIPREG=='A',2,;
      0)))
  endfunc

  func oTIPREG_1_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTIPREG_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not islron())
    endwith
    endif
  endfunc


  add object oObj_1_12 as cp_runprogram with uid="DGTQBBTTMZ",left=1, top=254, width=335,height=18,;
    caption='GSCG_BP4',;
   bGlobalFont=.t.,;
    prg="GSCG_BP4",;
    cEvent = "DiffConv",;
    nPag=1;
    , HelpContextID = 141648538

  add object oStr_1_7 as StdString with uid="IIJGSWECJG",Visible=.t., Left=331, Top=101,;
    Alignment=1, Width=53, Height=18,;
    Caption="Omagg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="UIZBPMGLBA",Visible=.t., Left=2, Top=148,;
    Alignment=1, Width=63, Height=18,;
    Caption="Controp.:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (g_DETCON<>'S')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="HFEXGAPETX",Visible=.t., Left=3, Top=124,;
    Alignment=1, Width=62, Height=18,;
    Caption="Descr. :"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="PHOXCQQWOE",Visible=.t., Left=321, Top=127,;
    Alignment=1, Width=63, Height=17,;
    Caption="Reg. IVA:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (Not islron())
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=21,;
    width=481+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*4*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=22,width=480+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*4*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VOCIIVA|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oTIPREG_1_11.Refresh()
      this.Parent.oDESIVA_2_14.Refresh()
      this.Parent.oIVFLOMAG_2_18.Refresh()
      this.Parent.oIVTIPCOP_2_20.Refresh()
      this.Parent.oIVCONTRO_2_22.Refresh()
      this.Parent.oDESCON_2_26.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VOCIIVA'
        oDropInto=this.oBodyCol.oRow.oIVCODIVA_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESIVA_2_14 as StdTrsField with uid="YHIGXPNBWB",rtseq=22,rtrep=.t.,;
    cFormVar="w_DESIVA",value=space(35),enabled=.f.,;
    HelpContextID = 115627062,;
    cTotal="", bFixedPos=.t., cQueryName = "DESIVA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=249, Left=66, Top=122, InputMask=replicate('X',35)

  add object oIVFLOMAG_2_18 as StdTrsCombo with uid="CSSQLFZSBG",rtrep=.t.,;
    cFormVar="w_IVFLOMAG", RowSource=""+"No,"+"I=imponibile,"+"O=imp.+IVA,"+"S=sc.merce,"+"Z=imp.riga 0" , ;
    ToolTipText = "Specifica se l'imponibile e/o IVA � omaggio",;
    HelpContextID = 41326029,;
    Height=25, Width=93, Left=386, Top=99,;
    cTotal="", cQueryName = "IVFLOMAG",;
    bObbl = .f. , nPag=2  , tabstop=.f.;
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oIVFLOMAG_2_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IVFLOMAG,&i_cF..t_IVFLOMAG),this.value)
    return(iif(xVal =1,'X',;
    iif(xVal =2,'I',;
    iif(xVal =3,'E',;
    iif(xVal =4,'S',;
    iif(xVal =5,'Z',;
    space(1)))))))
  endfunc
  func oIVFLOMAG_2_18.GetRadio()
    this.Parent.oContained.w_IVFLOMAG = this.RadioValue()
    return .t.
  endfunc

  func oIVFLOMAG_2_18.ToRadio()
    this.Parent.oContained.w_IVFLOMAG=trim(this.Parent.oContained.w_IVFLOMAG)
    return(;
      iif(this.Parent.oContained.w_IVFLOMAG=='X',1,;
      iif(this.Parent.oContained.w_IVFLOMAG=='I',2,;
      iif(this.Parent.oContained.w_IVFLOMAG=='E',3,;
      iif(this.Parent.oContained.w_IVFLOMAG=='S',4,;
      iif(this.Parent.oContained.w_IVFLOMAG=='Z',5,;
      0))))))
  endfunc

  func oIVFLOMAG_2_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oIVFLOMAG_2_18.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_IVCODIVA))
    endwith
  endfunc

  add object oIVTIPCOP_2_20 as StdTrsField with uid="BQNZHJCPHX",rtseq=28,rtrep=.t.,;
    cFormVar="w_IVTIPCOP",value=space(1),;
    ToolTipText = "Tipo conto di contropartita associato alla riga IVA",;
    HelpContextID = 142898646,;
    cTotal="", bFixedPos=.t., cQueryName = "IVTIPCOP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=66, Top=145, cSayPict=["!"], cGetPict=["!"], InputMask=replicate('X',1)

  func oIVTIPCOP_2_20.mCond()
    with this.Parent.oContained
      return (g_DETCON='S' AND NOT EMPTY(.w_IVCODIVA) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0 OR .w_IVCFLOMA='S'))
    endwith
  endfunc

  func oIVTIPCOP_2_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DETCON<>'S')
    endwith
    endif
  endfunc

  func oIVTIPCOP_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IVTIPCOP$"CFG")
      if .not. empty(.w_IVCONTRO)
        bRes2=.link_2_22('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oIVCONTRO_2_22 as StdTrsField with uid="TUHYRUACIM",rtseq=30,rtrep=.t.,;
    cFormVar="w_IVCONTRO",value=space(15),;
    ToolTipText = "Conto di contropartita associato alla riga IVA",;
    HelpContextID = 157902293,;
    cTotal="", bFixedPos=.t., cQueryName = "IVCONTRO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=123, Left=88, Top=145, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_IVTIPCOP", oKey_2_1="ANCODICE", oKey_2_2="this.w_IVCONTRO"

  func oIVCONTRO_2_22.mCond()
    with this.Parent.oContained
      return (g_DETCON='S' AND NOT EMPTY(.w_IVCODIVA) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0 OR .w_IVCFLOMA='S'))
    endwith
  endfunc

  func oIVCONTRO_2_22.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DETCON<>'S')
    endwith
    endif
  endfunc

  func oIVCONTRO_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oIVCONTRO_2_22.ecpDrop(oSource)
    this.Parent.oContained.link_2_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oIVCONTRO_2_22.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_IVTIPCOP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_IVTIPCOP)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oIVCONTRO_2_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori/conti",'',this.parent.oContained
  endproc
  proc oIVCONTRO_2_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_IVTIPCOP
     i_obj.w_ANCODICE=this.parent.oContained.w_IVCONTRO
    i_obj.ecpSave()
  endproc

  add object oDESCON_2_26 as StdTrsField with uid="XDCNBPXFIX",rtseq=34,rtrep=.t.,;
    cFormVar="w_DESCON",value=space(40),enabled=.f.,;
    HelpContextID = 57562166,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCON",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=199, Left=214, Top=145, InputMask=replicate('X',40)

  func oDESCON_2_26.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DETCON<>'S')
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTIMP_3_1 as StdField with uid="QVJRRFBAJI",rtseq=35,rtrep=.f.,;
    cFormVar="w_TOTIMP",value=0,enabled=.f.,;
    ToolTipText = "Totale degli importi inseriti sulla colonna imponibile",;
    HelpContextID = 89419574,;
    cQueryName = "TOTIMP",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=66, Top=99, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oTOTIVA_3_2 as StdField with uid="ZIZPBYAZCG",rtseq=36,rtrep=.f.,;
    cFormVar="w_TOTIVA",value=0,enabled=.f.,;
    ToolTipText = "Totale degli importi inseriti sulla colonna IVA",;
    HelpContextID = 115633974,;
    cQueryName = "TOTIVA",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=199, Top=99, cSayPict=[v_PV(38+VVP)], cGetPict=[v_GV(80)]
enddefine

* --- Defining Body row
define class tgscg_mivBodyRow as CPBodyRowCnt
  Width=471
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oIVCODIVA_2_1 as StdTrsField with uid="MIKYYDYVSH",rtseq=9,rtrep=.t.,;
    cFormVar="w_IVCODIVA",value=space(5),;
    ToolTipText = "Codice IVA",;
    HelpContextID = 231302599,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_IVCODIVA"

  proc oIVCODIVA_2_1.mBefore
    with this.Parent.oContained
      IIF(.w_GIACAR=' ', .NotifyEvent('CaricaAuto'), '')
    endwith
  endproc

  func oIVCODIVA_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oIVCODIVA_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oIVCODIVA_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oIVCODIVA_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'GSCG_IVA.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oIVCODIVA_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_IVCODIVA
    i_obj.ecpSave()
  endproc

  add object oIVIMPONI_2_7 as StdTrsField with uid="ZIEFOFSNHP",rtseq=15,rtrep=.t.,;
    cFormVar="w_IVIMPONI",value=0,;
    ToolTipText = "Imponibile (F9=converte l'importo inserito in valuta in base al cambio)",;
    HelpContextID = 76006863,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=60, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)], bHasZoom = .t. 

  func oIVIMPONI_2_7.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_IVCODIVA))
    endwith
  endfunc

  proc oIVIMPONI_2_7.mZoom
      with this.Parent.oContained
        GSCG_BIV(this.Parent.oContained,"IMPONI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oIVIMPIVA_2_9 as StdTrsField with uid="YNMXPZEQJT",rtseq=17,rtrep=.t.,;
    cFormVar="w_IVIMPIVA",value=0,;
    ToolTipText = "Imposta",;
    HelpContextID = 243779015,;
    cTotal = "this.Parent.oContained.w_totiva", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=190, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(38+VVP)], bHasZoom = .t. 

  func oIVIMPIVA_2_9.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_IVCODIVA) and .w_PERIVA<>0)
    endwith
  endfunc

  proc oIVIMPIVA_2_9.mZoom
      with this.Parent.oContained
        GSCG_BIV(this.Parent.oContained,"IMPONI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oIVTIPREG_2_10 as StdTrsCombo with uid="PXEVAJOCTQ",rtrep=.t.,;
    cFormVar="w_IVTIPREG", RowSource=""+"Ven =vendite,"+"Acq =acquisti,"+"CSC =corr.scorporo,"+"CVE =corr.ventilaz." , ;
    ToolTipText = "Tipo registro IVA",;
    HelpContextID = 126121421,;
    Height=22, Width=64, Left=319, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oIVTIPREG_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IVTIPREG,&i_cF..t_IVTIPREG),this.value)
    return(iif(xVal =1,'V',;
    iif(xVal =2,'A',;
    iif(xVal =3,'C',;
    iif(xVal =4,'E',;
    space(1))))))
  endfunc
  func oIVTIPREG_2_10.GetRadio()
    this.Parent.oContained.w_IVTIPREG = this.RadioValue()
    return .t.
  endfunc

  func oIVTIPREG_2_10.ToRadio()
    this.Parent.oContained.w_IVTIPREG=trim(this.Parent.oContained.w_IVTIPREG)
    return(;
      iif(this.Parent.oContained.w_IVTIPREG=='V',1,;
      iif(this.Parent.oContained.w_IVTIPREG=='A',2,;
      iif(this.Parent.oContained.w_IVTIPREG=='C',3,;
      iif(this.Parent.oContained.w_IVTIPREG=='E',4,;
      0)))))
  endfunc

  func oIVTIPREG_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oIVTIPREG_2_10.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_IVCODIVA) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0) AND Not islron())
    endwith
  endfunc

  func oIVTIPREG_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER( g_APPLICATION ) <> "AD HOC ENTERPRISE")
    endwith
    endif
  endfunc

  add object oIVTIPREG_2_11 as StdTrsCombo with uid="XVEKTYFYQN",rtrep=.t.,;
    cFormVar="w_IVTIPREG", RowSource=""+"Ven =vendite,"+"Acq =acquisti,"+"CSC =corr.scorporo" , ;
    ToolTipText = "Tipo registro IVA (F9=ricalcola automatismi IVA)",;
    HelpContextID = 126121421,;
    Height=22, Width=64, Left=319, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oIVTIPREG_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IVTIPREG,&i_cF..t_IVTIPREG),this.value)
    return(iif(xVal =1,'V',;
    iif(xVal =2,'A',;
    iif(xVal =3,'C',;
    space(1)))))
  endfunc
  func oIVTIPREG_2_11.GetRadio()
    this.Parent.oContained.w_IVTIPREG = this.RadioValue()
    return .t.
  endfunc

  func oIVTIPREG_2_11.ToRadio()
    this.Parent.oContained.w_IVTIPREG=trim(this.Parent.oContained.w_IVTIPREG)
    return(;
      iif(this.Parent.oContained.w_IVTIPREG=='V',1,;
      iif(this.Parent.oContained.w_IVTIPREG=='A',2,;
      iif(this.Parent.oContained.w_IVTIPREG=='C',3,;
      0))))
  endfunc

  func oIVTIPREG_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oIVTIPREG_2_11.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_IVCODIVA) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0))
    endwith
  endfunc

  func oIVTIPREG_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE")
    endwith
    endif
  endfunc

  add object oIVNUMREG_2_12 as StdTrsField with uid="BPIUIQHSPF",rtseq=20,rtrep=.t.,;
    cFormVar="w_IVNUMREG",value=0,;
    ToolTipText = "Numero registro IVA",;
    HelpContextID = 123737549,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Il registro � associato a pi� attivit� o a nessuna attivit�",;
   bGlobalFont=.t.,;
    Height=17, Width=24, Left=386, Top=0, cSayPict=["99"], cGetPict=["99"], tabstop=.f.

  func oIVNUMREG_2_12.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_IVCODIVA) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0))
    endwith
  endfunc

  func oIVNUMREG_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT (NOT EMPTY(.w_IVCODIVA) AND (.w_IVIMPONI<>0 OR .w_IVIMPIVA<>0)) OR CHKREGIV(.w_IVTIPREG,.w_IVNUMREG)=1)
    endwith
    return bRes
  endfunc

  add object oIVPERIND_2_13 as StdTrsField with uid="ZAZPKKOMVY",rtseq=21,rtrep=.t.,;
    cFormVar="w_IVPERIND",value=0,enabled=.f.,;
    ToolTipText = "Percentuale di indeducibilit�",;
    HelpContextID = 23054902,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=54, Left=412, Top=0
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oIVCODIVA_2_1.When()
    return(.t.)
  proc oIVCODIVA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oIVCODIVA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=3
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_miv','PNT_IVA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IVSERIAL=PNT_IVA.IVSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
