* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bpa                                                        *
*              Gestione parametri PDA                                          *
*                                                                              *
*      Author: Zucchetti TAM Srl                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_123]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-18                                                      *
* Last revis.: 2017-04-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bpa",oParentObject,m.Azione)
return(i_retval)

define class tgsac_bpa as StdBatch
  * --- Local variables
  Azione = space(1)
  PunPAD = .NULL.
  w_COLORI = space(10)
  w_PPCOLSUG = 0
  w_PPCOLMIS = 0
  w_PPCOLCON = 0
  TmpN = 0
  w_PPCOLPIA = 0
  w_MSG = space(10)
  w_PPCOLMIS = 0
  w_NMAXPE = 0
  w_PPCOLMCO = 0
  w_OLDCOM = space(1)
  w_OLDCODCOM = space(15)
  * --- WorkFile variables
  PAR_PROD_idx=0
  MAGAZZIN_idx=0
  TAB_CALE_idx=0
  CENCOST_idx=0
  CAN_TIER_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Maschera Parametri PDA (da GSAC_KPA)
    * --- Variabili maschera da valorizzare
    * --- Codice Fisso di Ricerca
    this.PunPAD = this.oParentObject
    this.w_COLORI = "SFOSUG-COLSUG-COLCON-SFOCON-SFOPIA-COLPIA-PDACOLMIS-PDASFOMIS-SFOMCO"
    do case
      case this.Azione = "LOAD"
        * --- Apertura della maschera - Legge Parametri e scrive variabili
        * --- Try
        local bErr_047DAB98
        bErr_047DAB98=bTrsErr
        this.Try_047DAB98()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Parametri non ancora caricati
          ah_ErrorMsg("Tabella parametri PDA non caricata","!","")
        endif
        bTrsErr=bTrsErr or bErr_047DAB98
        * --- End
        * --- Lettura eventuale Magazzino associato al Sottoscorta
        if NOT EMPTY(this.oParentObject.w_PPMAGPRO)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDESMAG,MGDISMAG"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_PPMAGPRO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDESMAG,MGDISMAG;
              from (i_cTable) where;
                  MGCODMAG = this.oParentObject.w_PPMAGPRO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESMAG = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
            this.oParentObject.w_DISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCALSTA)
          * --- Read from TAB_CALE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TAB_CALE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TAB_CALE_idx,2],.t.,this.TAB_CALE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TCDESCRI"+;
              " from "+i_cTable+" TAB_CALE where ";
                  +"TCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PPCALSTA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TCDESCRI;
              from (i_cTable) where;
                  TCCODICE = this.oParentObject.w_PPCALSTA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCAL = NVL(cp_ToDate(_read_.TCDESCRI),cp_NullValue(_read_.TCDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCENCOS)
          * --- Read from CENCOST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CENCOST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2],.t.,this.CENCOST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCDESPIA,CCDTOBSO"+;
              " from "+i_cTable+" CENCOST where ";
                  +"CC_CONTO = "+cp_ToStrODBC(this.oParentObject.w_PPCENCOS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCDESPIA,CCDTOBSO;
              from (i_cTable) where;
                  CC_CONTO = this.oParentObject.w_PPCENCOS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCEN = NVL(cp_ToDate(_read_.CCDESPIA),cp_NullValue(_read_.CCDESPIA))
            this.oParentObject.w_DATOBSO = NVL(cp_ToDate(_read_.CCDTOBSO),cp_NullValue(_read_.CCDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_PPCODCOM)
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNDESCAN"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_PPCODCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNDESCAN;
              from (i_cTable) where;
                  CNCODCAN = this.oParentObject.w_PPCODCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCOM = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      case this.Azione = "SALVA"
        * --- Ricalcola colori
        this.w_PPCOLSUG = this.oParentObject.w_COLSUG*(rgb(255,255,255)+1) + this.oParentObject.w_SFOSUG
        this.w_PPCOLCON = this.oParentObject.w_COLCON*(rgb(255,255,255)+1) + this.oParentObject.w_SFOCON
        this.w_PPCOLPIA = this.oParentObject.w_COLPIA*(rgb(255,255,255)+1) + this.oParentObject.w_SFOPIA
        this.w_PPCOLMIS = this.oParentObject.w_PDACOLMIS*(rgb(255,255,255)+1) + this.oParentObject.w_PDASFOMIS
        this.w_PPCOLMCO = this.oParentObject.w_SFOMCO
        this.oParentObject.w_PPNMAXPE = iif(this.oParentObject.w_PPNMAXPE=0,54,this.oParentObject.w_PPNMAXPE)
        this.w_NMAXPE = this.oParentObject.w_PPNUMGIO+this.oParentObject.w_PPNUMSET+this.oParentObject.w_PPNUMMES+this.oParentObject.w_PPNUMTRI
        if this.w_NMAXPE > this.oParentObject.w_PPNMAXPE
          this.w_MSG = "La somma del numero di periodi per la definizione dei bidoni temporali � %1%0Valore massimo consentito: %2"
          ah_ErrorMsg(this.w_MSG,"STOP","", str(this.w_NMAXPE,3,0), str(this.oParentObject.w_PPNMAXPE,3,0))
          i_retcode = 'stop'
          return
        endif
        * --- Effettuo il controllo per vedere se � cambiato il flag PPSALCOM
        * --- Read from PAR_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPSALCOM"+;
            " from "+i_cTable+" PAR_PROD where ";
                +"PPCODICE = "+cp_ToStrODBC("AA");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPSALCOM;
            from (i_cTable) where;
                PPCODICE = "AA";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDCOM = NVL(cp_ToDate(_read_.PPSALCOM),cp_NullValue(_read_.PPSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_OLDCOM="P" AND this.oParentObject.w_PPSALCOM="S"
          this.w_MSG = "La modifica effettuata all'opzione di pianificazione per gli articoli gestiti a commessa (impostata su gestione saldi commessa) eliminer� in modo definitivo le associazioni contenute all'interno della tabella pegging di secondo livello."
          this.w_MSG = this.w_MSG+"%0Tali associazioni non potranno essere in alcun modo ripristinate"
          this.w_MSG = this.w_MSG+"%0Prima di proseguire con l'aggiornamento � consigliato effettuare il backup del database.%0%0Si desidera sospendere l'operazione ed eseguire il backup?"
          if ah_yesno(this.w_MSG)
            this.w_MSG = "Salvataggio interrotto dall'utente!%0Per eseguire il backup accedere al menu sistema->Amministrazione sistema->backup database."
            ah_ErrorMsg(this.w_MSG,64,"")
            i_retcode = 'stop'
            return
          else
            this.w_MSG = "L'utente ha scelto di proseguire senza effettuare il backup del database.%0Sar� comunque possibile effettuare il backup prima dell'elaborazione dell'MRP"
            this.w_MSG = this.w_MSG+"%0Per eseguire il backup accedere al menu sistema->Amministrazione sistema->backup database."
            ah_ErrorMsg(this.w_MSG,64,"")
          endif
        endif
        * --- Try
        local bErr_047D9A58
        bErr_047D9A58=bTrsErr
        this.Try_047D9A58()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_047D9A58
        * --- End
        * --- Try
        local bErr_047DA028
        bErr_047DA028=bTrsErr
        this.Try_047DA028()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Impossibile aggiornare la tabella parametri PDA","STOP","")
        endif
        bTrsErr=bTrsErr or bErr_047DA028
        * --- End
      case this.Azione $ this.w_COLORI
        * --- Modifica il colore richiesto
        this.TmpN = GetColor()
        if this.TmpN<>-1
          pRit = "w_" + this.Azione
          this.oParentObject.&pRit = this.TmpN
        endif
    endcase
  endproc
  proc Try_047DAB98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPNUMGIO,PPNUMSET,PPNUMMES,PPNUMTRI,PPCOLMPS,PPFONMPS,PPCOLSUG,PPCOLCON,PPCOLPIA,PPCOLMIS,PPCOLMCO,PPNMAXPE,PPMAGPRO,PP_CICLI,PP___DTF,PPNOSCSM,PPCALSTA,PPCRIFOR,PPCRIELA,PPBLOMRP,PPFLCCOS,PPCENCOS,PPSALCOM,PPCODCOM,PPFLAROB"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("AA");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPNUMGIO,PPNUMSET,PPNUMMES,PPNUMTRI,PPCOLMPS,PPFONMPS,PPCOLSUG,PPCOLCON,PPCOLPIA,PPCOLMIS,PPCOLMCO,PPNMAXPE,PPMAGPRO,PP_CICLI,PP___DTF,PPNOSCSM,PPCALSTA,PPCRIFOR,PPCRIELA,PPBLOMRP,PPFLCCOS,PPCENCOS,PPSALCOM,PPCODCOM,PPFLAROB;
        from (i_cTable) where;
            PPCODICE = "AA";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_PPNUMGIO = NVL(cp_ToDate(_read_.PPNUMGIO),cp_NullValue(_read_.PPNUMGIO))
      this.oParentObject.w_PPNUMSET = NVL(cp_ToDate(_read_.PPNUMSET),cp_NullValue(_read_.PPNUMSET))
      this.oParentObject.w_PPNUMMES = NVL(cp_ToDate(_read_.PPNUMMES),cp_NullValue(_read_.PPNUMMES))
      this.oParentObject.w_PPNUMTRI = NVL(cp_ToDate(_read_.PPNUMTRI),cp_NullValue(_read_.PPNUMTRI))
      this.oParentObject.w_PPCOLMPS = NVL(cp_ToDate(_read_.PPCOLMPS),cp_NullValue(_read_.PPCOLMPS))
      this.oParentObject.w_PPFONMPS = NVL(cp_ToDate(_read_.PPFONMPS),cp_NullValue(_read_.PPFONMPS))
      this.w_PPCOLSUG = NVL(cp_ToDate(_read_.PPCOLSUG),cp_NullValue(_read_.PPCOLSUG))
      this.w_PPCOLCON = NVL(cp_ToDate(_read_.PPCOLCON),cp_NullValue(_read_.PPCOLCON))
      this.w_PPCOLPIA = NVL(cp_ToDate(_read_.PPCOLPIA),cp_NullValue(_read_.PPCOLPIA))
      this.w_PPCOLMIS = NVL(cp_ToDate(_read_.PPCOLMIS),cp_NullValue(_read_.PPCOLMIS))
      this.w_PPCOLMCO = NVL(cp_ToDate(_read_.PPCOLMCO),cp_NullValue(_read_.PPCOLMCO))
      this.oParentObject.w_PPNMAXPE = NVL(cp_ToDate(_read_.PPNMAXPE),cp_NullValue(_read_.PPNMAXPE))
      this.oParentObject.w_PPMAGPRO = NVL(cp_ToDate(_read_.PPMAGPRO),cp_NullValue(_read_.PPMAGPRO))
      this.oParentObject.w_PP_CICLI = NVL(cp_ToDate(_read_.PP_CICLI),cp_NullValue(_read_.PP_CICLI))
      this.oParentObject.w_PP___DTF = NVL(cp_ToDate(_read_.PP___DTF),cp_NullValue(_read_.PP___DTF))
      this.oParentObject.w_PPNOSCSM = NVL(cp_ToDate(_read_.PPNOSCSM),cp_NullValue(_read_.PPNOSCSM))
      this.oParentObject.w_PPCALSTA = NVL(cp_ToDate(_read_.PPCALSTA),cp_NullValue(_read_.PPCALSTA))
      this.oParentObject.w_CRIFOR = NVL(cp_ToDate(_read_.PPCRIFOR),cp_NullValue(_read_.PPCRIFOR))
      this.oParentObject.w_CRIELA = NVL(cp_ToDate(_read_.PPCRIELA),cp_NullValue(_read_.PPCRIELA))
      this.oParentObject.w_PPBLOMRP = NVL(cp_ToDate(_read_.PPBLOMRP),cp_NullValue(_read_.PPBLOMRP))
      this.oParentObject.w_PPFLCCOS = NVL(cp_ToDate(_read_.PPFLCCOS),cp_NullValue(_read_.PPFLCCOS))
      this.oParentObject.w_PPCENCOS = NVL(cp_ToDate(_read_.PPCENCOS),cp_NullValue(_read_.PPCENCOS))
      this.oParentObject.w_PPSALCOM = NVL(cp_ToDate(_read_.PPSALCOM),cp_NullValue(_read_.PPSALCOM))
      this.oParentObject.w_PPCODCOM = NVL(cp_ToDate(_read_.PPCODCOM),cp_NullValue(_read_.PPCODCOM))
      this.oParentObject.w_PPFLAROB = NVL(cp_ToDate(_read_.PPFLAROB),cp_NullValue(_read_.PPFLAROB))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Per non ricalcolare PP___DTF
    this.oParentObject.o_PP_CICLI = this.oParentObject.w_PP_CICLI
    * --- Colori di default
    this.w_PPCOLSUG = iif(this.w_PPCOLSUG=0,rgb(255,255,255),this.w_PPCOLSUG)
    this.w_PPCOLCON = iif(this.w_PPCOLCON=0,rgb(255,255,255),this.w_PPCOLCON)
    this.w_PPCOLPIA = iif(this.w_PPCOLPIA=0,rgb(255,255,255),this.w_PPCOLPIA)
    this.w_PPCOLMIS = iif(this.w_PPCOLMIS=0,rgb(255,255,255),this.w_PPCOLMIS)
    this.w_PPCOLMCO = iif(this.w_PPCOLMCO=0,rgb(255,255,255),this.w_PPCOLMCO)
    this.oParentObject.w_SFOSUG = mod(this.w_PPCOLSUG,(rgb(255,255,255)+1))
    this.oParentObject.w_COLSUG = int(this.w_PPCOLSUG/(rgb(255,255,255)+1))
    this.oParentObject.w_SFOCON = mod(this.w_PPCOLCON,(rgb(255,255,255)+1))
    this.oParentObject.w_COLCON = int(this.w_PPCOLCON/(rgb(255,255,255)+1))
    this.oParentObject.w_SFOPIA = mod(this.w_PPCOLPIA,(rgb(255,255,255)+1))
    this.oParentObject.w_COLPIA = int(this.w_PPCOLPIA/(rgb(255,255,255)+1))
    this.oParentObject.w_SFOMCO = this.w_PPCOLMCO
    this.oParentObject.w_PDACOLMIS = int(this.w_PPCOLMIS/(rgb(255,255,255)+1))
    this.oParentObject.w_PDASFOMIS = mod(this.w_PPCOLMIS,(rgb(255,255,255)+1))
    return
  proc Try_047D9A58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_PROD
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PPCODICE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("AA"),'PAR_PROD','PPCODICE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PPCODICE',"AA")
      insert into (i_cTable) (PPCODICE &i_ccchkf. );
         values (;
           "AA";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_047DA028()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCODCOM"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("AA");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCODCOM;
        from (i_cTable) where;
            PPCODICE = "AA";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OLDCODCOM = NVL(cp_ToDate(_read_.PPCODCOM),cp_NullValue(_read_.PPCODCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not this.w_OLDCODCOM==this.oParentObject.w_PPCODCOM
      this.w_MSG = "Attenzione! A seguito della modifica della commessa di default sar� necessario eseguire la procedura di ricostruzione dei saldi di magazzino"
      ah_ErrorMsg(this.w_MSG,64,"")
    endif
    * --- Write into PAR_PROD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PPFONMPS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPFONMPS),'PAR_PROD','PPFONMPS');
      +",PPCOLMPS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCOLMPS),'PAR_PROD','PPCOLMPS');
      +",PPNUMTRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPNUMTRI),'PAR_PROD','PPNUMTRI');
      +",PPNUMMES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPNUMMES),'PAR_PROD','PPNUMMES');
      +",PPNUMGIO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPNUMGIO),'PAR_PROD','PPNUMGIO');
      +",PPNUMSET ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPNUMSET),'PAR_PROD','PPNUMSET');
      +",PPCOLSUG ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLSUG),'PAR_PROD','PPCOLSUG');
      +",PPCOLCON ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLCON),'PAR_PROD','PPCOLCON');
      +",PPCOLPIA ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLPIA),'PAR_PROD','PPCOLPIA');
      +",PPCOLMIS ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLMIS),'PAR_PROD','PPCOLMIS');
      +",PPCOLMCO ="+cp_NullLink(cp_ToStrODBC(this.w_PPCOLMCO),'PAR_PROD','PPCOLMCO');
      +",PPNMAXPE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPNMAXPE),'PAR_PROD','PPNMAXPE');
      +",PPMAGPRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPMAGPRO),'PAR_PROD','PPMAGPRO');
      +",PP_CICLI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PP_CICLI),'PAR_PROD','PP_CICLI');
      +",PP___DTF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PP___DTF),'PAR_PROD','PP___DTF');
      +",PPNOSCSM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPNOSCSM),'PAR_PROD','PPNOSCSM');
      +",PPCALSTA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCALSTA),'PAR_PROD','PPCALSTA');
      +",PPCRIFOR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CRIFOR),'PAR_PROD','PPCRIFOR');
      +",PPCRIELA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CRIELA),'PAR_PROD','PPCRIELA');
      +",PPBLOMRP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPBLOMRP),'PAR_PROD','PPBLOMRP');
      +",PPCENCOS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCENCOS),'PAR_PROD','PPCENCOS');
      +",PPFLCCOS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPFLCCOS),'PAR_PROD','PPFLCCOS');
      +",PPSALCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPSALCOM),'PAR_PROD','PPSALCOM');
      +",PPCODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPCODCOM),'PAR_PROD','PPCODCOM');
      +",PPFLAROB ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PPFLAROB),'PAR_PROD','PPFLAROB');
          +i_ccchkf ;
      +" where ";
          +"PPCODICE = "+cp_ToStrODBC("AA");
             )
    else
      update (i_cTable) set;
          PPFONMPS = this.oParentObject.w_PPFONMPS;
          ,PPCOLMPS = this.oParentObject.w_PPCOLMPS;
          ,PPNUMTRI = this.oParentObject.w_PPNUMTRI;
          ,PPNUMMES = this.oParentObject.w_PPNUMMES;
          ,PPNUMGIO = this.oParentObject.w_PPNUMGIO;
          ,PPNUMSET = this.oParentObject.w_PPNUMSET;
          ,PPCOLSUG = this.w_PPCOLSUG;
          ,PPCOLCON = this.w_PPCOLCON;
          ,PPCOLPIA = this.w_PPCOLPIA;
          ,PPCOLMIS = this.w_PPCOLMIS;
          ,PPCOLMCO = this.w_PPCOLMCO;
          ,PPNMAXPE = this.oParentObject.w_PPNMAXPE;
          ,PPMAGPRO = this.oParentObject.w_PPMAGPRO;
          ,PP_CICLI = this.oParentObject.w_PP_CICLI;
          ,PP___DTF = this.oParentObject.w_PP___DTF;
          ,PPNOSCSM = this.oParentObject.w_PPNOSCSM;
          ,PPCALSTA = this.oParentObject.w_PPCALSTA;
          ,PPCRIFOR = this.oParentObject.w_CRIFOR;
          ,PPCRIELA = this.oParentObject.w_CRIELA;
          ,PPBLOMRP = this.oParentObject.w_PPBLOMRP;
          ,PPCENCOS = this.oParentObject.w_PPCENCOS;
          ,PPFLCCOS = this.oParentObject.w_PPFLCCOS;
          ,PPSALCOM = this.oParentObject.w_PPSALCOM;
          ,PPCODCOM = this.oParentObject.w_PPCODCOM;
          ,PPFLAROB = this.oParentObject.w_PPFLAROB;
          &i_ccchkf. ;
       where;
          PPCODICE = "AA";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    g_PDACOM = this.oParentObject.w_PPSALCOM="P"
    if vartype(g_PPCODCOM)="U"
       public g_PPCODCOM
    endif
    g_PPCODCOM = this.oParentObject.w_PPCODCOM
    return


  proc Init(oParentObject,Azione)
    this.Azione=Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='TAB_CALE'
    this.cWorkTables[4]='CENCOST'
    this.cWorkTables[5]='CAN_TIER'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Azione"
endproc
