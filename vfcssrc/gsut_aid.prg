* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_aid                                                        *
*              Indici documenti                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-14                                                      *
* Last revis.: 2014-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsut_aid
Parameters pCaller
* --- Fine Area Manuale
return(createobject("tgsut_aid"))

* --- Class definition
define class tgsut_aid as StdForm
  Top    = 3
  Left   = 9

  * --- Standard Properties
  Width  = 799
  Height = 353+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-22"
  HelpContextID=8995689
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=87

  * --- Constant Properties
  PROMINDI_IDX = 0
  PROMCLAS_IDX = 0
  cpusers_IDX = 0
  TIP_ALLE_IDX = 0
  CLA_ALLE_IDX = 0
  STATISOS_IDX = 0
  CPUSERS_IDX = 0
  RAG_TIPI_IDX = 0
  cFile = "PROMINDI"
  cKeySelect = "IDSERIAL"
  cKeyWhere  = "IDSERIAL=this.w_IDSERIAL"
  cKeyWhereODBC = '"IDSERIAL="+cp_ToStrODBC(this.w_IDSERIAL)';

  cKeyWhereODBCqualified = '"PROMINDI.IDSERIAL="+cp_ToStrODBC(this.w_IDSERIAL)';

  cPrg = "gsut_aid"
  cComment = "Indici documenti "
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FLEDIT = .F.
  w_IDSERIAL = space(20)
  o_IDSERIAL = space(20)
  w_IDFLPROV = space(10)
  w_IDCLADOC = space(15)
  w_OLDPAT = space(220)
  w_OLDFIL = space(100)
  w_DESCCLA = space(70)
  w_IDORIGIN = space(200)
  o_IDORIGIN = space(200)
  w_IDOGGETT = space(220)
  w_ID_TESTO = space(0)
  w_IDTIPFIL = space(10)
  w_IDUTECRE = 0
  w_IDDATCRE = ctod('  /  /  ')
  w_DESUTE = space(20)
  w_IDMODALL = space(1)
  o_IDMODALL = space(1)
  w_IDTIPALL = space(5)
  o_IDTIPALL = space(5)
  w_IDALLPRI = space(1)
  o_IDALLPRI = space(1)
  w_IDERASEF = space(1)
  w_IDPUBWEB = space(1)
  w_IDCLAALL = space(5)
  o_IDCLAALL = space(5)
  w_EDITABLE = .F.
  w_IDWEBAPP = space(1)
  o_IDWEBAPP = space(1)
  w_IDFLGFAE = space(1)
  o_IDFLGFAE = space(1)
  w_IDCODCBI = space(4)
  w_IDDATRAG = ctod('  /  /  ')
  w_IDNOMFIL = space(100)
  o_IDNOMFIL = space(100)
  w_IDDOCFIR = space(220)
  w_PATSTD = space(254)
  w_TIPRAG = space(1)
  w_DESTIPAL = space(50)
  w_DESCLA = space(50)
  w_IDPATALL = space(254)
  w_IDDATINI = ctod('  /  /  ')
  w_IDDATOBS = ctod('  /  /  ')
  w_IDCODPRO = space(10)
  w_DESCPRO = space(40)
  w_IDKEYVAL = space(100)
  w_INSPATIP = space(1)
  w_INSPACLA = space(1)
  w_PATTIP = space(1)
  w_PATCLA = space(1)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTCV = 0
  w_UTDV = ctot('')
  w_UNIVOC = .F.
  w_ARCHIVIO = space(20)
  w_IDORIFIL = space(100)
  o_IDORIFIL = space(100)
  w_PATHCLA = space(1)
  w_PATHTIP = space(1)
  w_INDPUBWEB = space(1)
  w_FIRMDIG = space(1)
  w_IDFLHIDE = space(1)
  w_FILEALT = space(100)
  o_FILEALT = space(100)
  w_ALIASFILE = space(15)
  w_FLGDES = space(1)
  w_IDUTEEXP = 0
  w_DESUTEEX = space(20)
  w_ORIFIL = space(100)
  w_IDZIPNAM = space(254)
  w_IDSTASOS = space(18)
  w_IDCRCALL = space(20)
  w_IDWEBFIL = space(254)
  w_IDWEBRET = space(15)
  w_IDWEBERR = space(0)
  w_PATHPRA = space(100)
  w_IDDATZIP = ctot('')
  w_IDWEBAPP = space(1)
  w_COD_PRE = space(20)
  w_COD_RAG = space(10)
  w_COD_PRA = space(20)
  w_ESE_DEL = .F.
  w_IDORACRE = space(8)
  w_OldWebApp = space(1)
  w_CDERASEF = space(1)
  w_Oldogg = space(220)
  w_P_TIPGEN = space(10)
  w_P_RAGGEN = space(10)
  w_EST_GEN = space(254)
  w_P_TIPFIR = space(10)
  w_P_RAGFIR = space(10)
  w_EST_FIR = space(254)
  w_P_TIPZIP = space(10)
  w_P_RAGZIP = space(10)
  w_EST_ZIP = space(254)
  w_IDTIPBST = space(1)
  w_IDFLDEPO = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_IDSERIAL = this.W_IDSERIAL

  * --- Children pointers
  GSUT_MAA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_aid
  * Variabile per attivare i campi relavitivi alla tipologia e classe allegato
  AttivaTip = .f.
  Caller=.null.
  chiusura=.t.
  
  * Variabili per visualizzare il messaggio "Vuoi aprire il documento generato?"
  VisuMess = .f.
  VisPat = ''
  
  proc ECPSave()
   dodefault()
   if this.chiusura and type('this.Caller')='O' and lower(this.Caller.class)='tgsut_bba'
       if type('this.caller.oParentObject.oParentObject.Class') = 'C'
         this.Caller.oparentobject.oparentobject.Notifyevent('Ricerca')
       endif
      this.ecpquit()
   endif
   if this.chiusura and type('this.Caller')='O' and lower(this.Caller.class)='tgsut_bfg' and this.caller.w_Operazione == 'I'
      * Eseguo il refresh dello zoom
      * prima verifico se � stato inserito un archivio nella maschera 'GSUT_KFG'
      if ! empty(nvl(this.caller.oParentObject.w_ARCHIVIO,' '))
         GSUT_BES(this.caller.oParentObject,this.caller.oParentObject.w_ARCHIVIO,this.caller.oParentObject.w_CHIAVE)
      else
         GSUT_BES(this.caller.oParentObject,"","")
      endif
   endif
  endproc
  
  PROC ECPQUIT()
    if this.cfunction='Edit' and type('this.Caller')='O' and lower(this.Caller.class)='tgsut_bba'
      this.cfunction='Query'
      this.ecpDelete()
      this.NotifyEvent("Done")
      this.Hide()
      this.Release()
    else
   if vartype(this.chiusura)='L' and this.chiusura
       dodefault()
        if type('this.caller.oParentObject.oParentObject.Class') = 'C' and upper(this.caller.oParentObject.oParentObject.Class) == "TGSUT_KFG"
          * Eseguo il refresh dello zoom
           GSUT_BES(this.caller.oParentObject.oParentObject,"","")
        endif
   endif
    endif
  EndProc
  
  oldmodall=' '
  oldnomfil=' '
  
  * Variabile per la memorizzazione del path originario del file catturato
  origfilepath='  '
  *Variabile per la verifica della modifica del path di archiviazione dell'allegato
  pathmodified=.f.
  *Variabile assegnamento classe primaria allegato
  classeallorig=' '
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PROMINDI','gsut_aid')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_aidPag1","gsut_aid",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Indici documenti")
      .Pages(1).HelpContextID = 203740605
      .Pages(2).addobject("oPag","tgsut_aidPag2","gsut_aid",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Informazioni allegato")
      .Pages(2).HelpContextID = 250045260
      .Pages(3).addobject("oPag","tgsut_aidPag3","gsut_aid",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Attributi di ricerca")
      .Pages(3).HelpContextID = 223839421
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIDSERIAL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsut_aid
    with this.parent
     .AttivaTip = iif(Type('pCaller')='O',.t.,.f.)
     .Caller=iif(Type('pCaller')='O',pCaller,.null.)
     .closable=.F.
    endwith
    
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='cpusers'
    this.cWorkTables[3]='TIP_ALLE'
    this.cWorkTables[4]='CLA_ALLE'
    this.cWorkTables[5]='STATISOS'
    this.cWorkTables[6]='CPUSERS'
    this.cWorkTables[7]='RAG_TIPI'
    this.cWorkTables[8]='PROMINDI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PROMINDI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PROMINDI_IDX,3]
  return

  function CreateChildren()
    this.GSUT_MAA = CREATEOBJECT('stdDynamicChild',this,'GSUT_MAA',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSUT_MAA)
      this.GSUT_MAA.DestroyChildrenChain()
      this.GSUT_MAA=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSUT_MAA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSUT_MAA.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSUT_MAA.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSUT_MAA.SetKey(;
            .w_IDSERIAL,"IDSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSUT_MAA.ChangeRow(this.cRowID+'      1',1;
             ,.w_IDSERIAL,"IDSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSUT_MAA)
        i_f=.GSUT_MAA.BuildFilter()
        if !(i_f==.GSUT_MAA.cQueryFilter)
          i_fnidx=.GSUT_MAA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSUT_MAA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSUT_MAA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSUT_MAA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSUT_MAA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_IDSERIAL = NVL(IDSERIAL,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PROMINDI where IDSERIAL=KeySet.IDSERIAL
    *
    i_nConn = i_TableProp[this.PROMINDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMINDI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PROMINDI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PROMINDI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PROMINDI '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IDSERIAL',this.w_IDSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_FLEDIT = .f.
        .w_OLDPAT = space(220)
        .w_OLDFIL = .w_IDNOMFIL
        .w_DESCCLA = space(70)
        .w_DESUTE = space(20)
        .w_EDITABLE = .T.
        .w_PATSTD = space(254)
        .w_TIPRAG = space(1)
        .w_DESTIPAL = space(50)
        .w_DESCLA = space(50)
        .w_DESCPRO = space(40)
        .w_INSPATIP = space(1)
        .w_INSPACLA = space(1)
        .w_PATTIP = space(1)
        .w_PATCLA = space(1)
        .w_UNIVOC = .f.
        .w_ARCHIVIO = space(20)
        .w_PATHCLA = space(1)
        .w_PATHTIP = space(1)
        .w_INDPUBWEB = space(1)
        .w_FIRMDIG = space(1)
        .w_FILEALT = space(100)
        .w_ALIASFILE = space(15)
        .w_FLGDES = space(1)
        .w_DESUTEEX = space(20)
        .w_PATHPRA = space(100)
        .w_COD_PRE = space(20)
        .w_COD_RAG = space(10)
        .w_COD_PRA = space(20)
        .w_OldWebApp = space(1)
        .w_CDERASEF = space(1)
        .w_Oldogg = space(220)
        .w_P_TIPGEN = 'G'
        .w_P_RAGGEN = space(10)
        .w_EST_GEN = space(254)
        .w_P_TIPFIR = 'F'
        .w_P_RAGFIR = space(10)
        .w_EST_FIR = space(254)
        .w_P_TIPZIP = 'Z'
        .w_P_RAGZIP = space(10)
        .w_EST_ZIP = space(254)
        .w_IDSERIAL = NVL(IDSERIAL,space(20))
        .op_IDSERIAL = .w_IDSERIAL
        .w_IDFLPROV = NVL(IDFLPROV,space(10))
        .w_IDCLADOC = NVL(IDCLADOC,space(15))
          if link_1_4_joined
            this.w_IDCLADOC = NVL(CDCODCLA104,NVL(this.w_IDCLADOC,space(15)))
            this.w_DESCCLA = NVL(CDDESCLA104,space(70))
            this.w_PATSTD = NVL(CDPATSTD104,space(254))
            this.w_TIPRAG = NVL(CDTIPRAG104,space(1))
            this.w_IDMODALL = NVL(CDMODALL104,space(1))
            this.w_PATHTIP = NVL(CDCOMTIP104,space(1))
            this.w_PATHCLA = NVL(CDCOMCLA104,space(1))
            this.w_IDTIPALL = NVL(CDTIPALL104,space(5))
            this.w_IDCLAALL = NVL(CDCLAALL104,space(5))
            this.w_ARCHIVIO = NVL(CDRIFTAB104,space(20))
            this.w_FIRMDIG = NVL(CDFIRDIG104,space(1))
            this.w_ALIASFILE = NVL(CDALIASF104,space(15))
            this.w_FLGDES = NVL(CDFLGDES104,space(1))
            this.w_CDERASEF = NVL(CDERASEF104,space(1))
          else
          .link_1_4('Load')
          endif
        .w_IDORIGIN = NVL(IDORIGIN,space(200))
        .w_IDOGGETT = NVL(IDOGGETT,space(220))
        .w_ID_TESTO = NVL(ID_TESTO,space(0))
        .w_IDTIPFIL = NVL(IDTIPFIL,space(10))
        .w_IDUTECRE = NVL(IDUTECRE,0)
          .link_1_21('Load')
        .w_IDDATCRE = NVL(cp_ToDate(IDDATCRE),ctod("  /  /  "))
        .w_IDMODALL = NVL(IDMODALL,space(1))
        .w_IDTIPALL = NVL(IDTIPALL,space(5))
          if link_2_2_joined
            this.w_IDTIPALL = NVL(TACODICE202,NVL(this.w_IDTIPALL,space(5)))
            this.w_DESTIPAL = NVL(TADESCRI202,space(50))
          else
          .link_2_2('Load')
          endif
        .w_IDALLPRI = NVL(IDALLPRI,space(1))
        .w_IDERASEF = NVL(IDERASEF,space(1))
        .w_IDPUBWEB = NVL(IDPUBWEB,space(1))
        .w_IDCLAALL = NVL(IDCLAALL,space(5))
          if link_2_6_joined
            this.w_IDCLAALL = NVL(TACODCLA206,NVL(this.w_IDCLAALL,space(5)))
            this.w_INDPUBWEB = NVL(TDPUBWEB206,space(1))
            this.w_DESCLA = NVL(TACLADES206,space(50))
          else
          .link_2_6('Load')
          endif
        .w_IDWEBAPP = NVL(IDWEBAPP,space(1))
        .w_IDFLGFAE = NVL(IDFLGFAE,space(1))
        .w_IDCODCBI = NVL(IDCODCBI,space(4))
        .w_IDDATRAG = NVL(cp_ToDate(IDDATRAG),ctod("  /  /  "))
        .w_IDNOMFIL = NVL(IDNOMFIL,space(100))
        .w_IDDOCFIR = NVL(IDDOCFIR,space(220))
        .w_IDPATALL = NVL(IDPATALL,space(254))
        .w_IDDATINI = NVL(cp_ToDate(IDDATINI),ctod("  /  /  "))
        .w_IDDATOBS = NVL(cp_ToDate(IDDATOBS),ctod("  /  /  "))
        .w_IDCODPRO = NVL(IDCODPRO,space(10))
        .w_IDKEYVAL = NVL(IDKEYVAL,space(100))
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_31.Calculate()
        .w_IDORIFIL = NVL(IDORIFIL,space(100))
        .w_IDFLHIDE = NVL(IDFLHIDE,space(1))
        .w_IDUTEEXP = NVL(IDUTEEXP,0)
          .link_1_50('Load')
        .w_ORIFIL = JUSTFNAME(.w_IDORIFIL)
        .w_IDZIPNAM = NVL(IDZIPNAM,space(254))
        .w_IDSTASOS = NVL(IDSTASOS,space(18))
        .w_IDCRCALL = NVL(IDCRCALL,space(20))
        .w_IDWEBFIL = NVL(IDWEBFIL,space(254))
        .w_IDWEBRET = NVL(IDWEBRET,space(15))
        .w_IDWEBERR = NVL(IDWEBERR,space(0))
        .oPgFrm.Page2.oPag.oObj_2_48.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(iif(.w_idFLPROV='S', ah_msgformat("Confermato"), ah_msgformat("Provvisorio")),0)
        .w_IDDATZIP = NVL(IDDATZIP,ctot(""))
        .w_IDWEBAPP = NVL(IDWEBAPP,space(1))
        .w_ESE_DEL = .f.
        .w_IDORACRE = NVL(IDORACRE,space(8))
          .link_2_56('Load')
          .link_2_57('Load')
          .link_2_59('Load')
          .link_2_60('Load')
          .link_2_62('Load')
          .link_2_63('Load')
        .w_IDTIPBST = NVL(IDTIPBST,space(1))
        .w_IDFLDEPO = NVL(IDFLDEPO,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'PROMINDI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsut_aid
    this.oldmodall = this.w_IDMODALL
    this.oldnomfil = this.w_IDNOMFIL
    this.origfilepath = this.w_IDPATALL
    this.classeallorig = this.w_IDCLAALL
    THIS.NOTIFYEVENT('w_IDMODALL Changed')
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLEDIT = .f.
      .w_IDSERIAL = space(20)
      .w_IDFLPROV = space(10)
      .w_IDCLADOC = space(15)
      .w_OLDPAT = space(220)
      .w_OLDFIL = space(100)
      .w_DESCCLA = space(70)
      .w_IDORIGIN = space(200)
      .w_IDOGGETT = space(220)
      .w_ID_TESTO = space(0)
      .w_IDTIPFIL = space(10)
      .w_IDUTECRE = 0
      .w_IDDATCRE = ctod("  /  /  ")
      .w_DESUTE = space(20)
      .w_IDMODALL = space(1)
      .w_IDTIPALL = space(5)
      .w_IDALLPRI = space(1)
      .w_IDERASEF = space(1)
      .w_IDPUBWEB = space(1)
      .w_IDCLAALL = space(5)
      .w_EDITABLE = .f.
      .w_IDWEBAPP = space(1)
      .w_IDFLGFAE = space(1)
      .w_IDCODCBI = space(4)
      .w_IDDATRAG = ctod("  /  /  ")
      .w_IDNOMFIL = space(100)
      .w_IDDOCFIR = space(220)
      .w_PATSTD = space(254)
      .w_TIPRAG = space(1)
      .w_DESTIPAL = space(50)
      .w_DESCLA = space(50)
      .w_IDPATALL = space(254)
      .w_IDDATINI = ctod("  /  /  ")
      .w_IDDATOBS = ctod("  /  /  ")
      .w_IDCODPRO = space(10)
      .w_DESCPRO = space(40)
      .w_IDKEYVAL = space(100)
      .w_INSPATIP = space(1)
      .w_INSPACLA = space(1)
      .w_PATTIP = space(1)
      .w_PATCLA = space(1)
      .w_UTCC = 0
      .w_UTDC = ctot("")
      .w_UTCV = 0
      .w_UTDV = ctot("")
      .w_UNIVOC = .f.
      .w_ARCHIVIO = space(20)
      .w_IDORIFIL = space(100)
      .w_PATHCLA = space(1)
      .w_PATHTIP = space(1)
      .w_INDPUBWEB = space(1)
      .w_FIRMDIG = space(1)
      .w_IDFLHIDE = space(1)
      .w_FILEALT = space(100)
      .w_ALIASFILE = space(15)
      .w_FLGDES = space(1)
      .w_IDUTEEXP = 0
      .w_DESUTEEX = space(20)
      .w_ORIFIL = space(100)
      .w_IDZIPNAM = space(254)
      .w_IDSTASOS = space(18)
      .w_IDCRCALL = space(20)
      .w_IDWEBFIL = space(254)
      .w_IDWEBRET = space(15)
      .w_IDWEBERR = space(0)
      .w_PATHPRA = space(100)
      .w_IDDATZIP = ctot("")
      .w_IDWEBAPP = space(1)
      .w_COD_PRE = space(20)
      .w_COD_RAG = space(10)
      .w_COD_PRA = space(20)
      .w_ESE_DEL = .f.
      .w_IDORACRE = space(8)
      .w_OldWebApp = space(1)
      .w_CDERASEF = space(1)
      .w_Oldogg = space(220)
      .w_P_TIPGEN = space(10)
      .w_P_RAGGEN = space(10)
      .w_EST_GEN = space(254)
      .w_P_TIPFIR = space(10)
      .w_P_RAGFIR = space(10)
      .w_EST_FIR = space(254)
      .w_P_TIPZIP = space(10)
      .w_P_RAGZIP = space(10)
      .w_EST_ZIP = space(254)
      .w_IDTIPBST = space(1)
      .w_IDFLDEPO = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_IDFLPROV = 'N'
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_IDCLADOC))
          .link_1_4('Full')
          endif
          .DoRTCalc(5,5,.f.)
        .w_OLDFIL = .w_IDNOMFIL
          .DoRTCalc(7,7,.f.)
        .w_IDORIGIN = IIF(empty(.w_IDORIFIL), space(100), left(substr(.w_IDORIFIL ,RAT('\', .w_IDORIFIL)+1),100))
          .DoRTCalc(9,10,.f.)
        .w_IDTIPFIL = lower(substr(.w_IDNOMFIL, rat(".", .w_IDNOMFIL)))
        .w_IDUTECRE = i_CODUTE
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_IDUTECRE))
          .link_1_21('Full')
          endif
        .w_IDDATCRE = i_DATSYS
          .DoRTCalc(14,14,.f.)
        .w_IDMODALL = iif(g_AEDS='S', 'E', 'F')
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_IDTIPALL))
          .link_2_2('Full')
          endif
        .w_IDALLPRI = ' '
        .DoRTCalc(18,20,.f.)
          if not(empty(.w_IDCLAALL))
          .link_2_6('Full')
          endif
        .w_EDITABLE = .T.
          .DoRTCalc(22,22,.f.)
        .w_IDFLGFAE = 'N'
          .DoRTCalc(24,24,.f.)
        .w_IDDATRAG = i_DATSYS
        .w_IDNOMFIL = icase(.w_IDMODALL='E' or .oldmodall='E' or .w_IDMODALL='I' or .oldmodall='I', .w_IDNOMFIL, .w_FLEDIT and .w_IDMODALL='F', JUSTFNAME(.w_IDORIFIL), left(alltrim(IIF(empty(.w_IDORIFIL), space(100),  IIF(.w_IDMODALL='F',.w_IDSERIAL+'_','')+substr(.w_IDORIFIL ,RAT('\', .w_IDORIFIL)+1))),100))
          .DoRTCalc(27,31,.f.)
        .w_IDPATALL = iif(.cFunction='Edit' and !.w_FLEDIT,.w_IDPATALL,dcmpatar( iif(Not Empty(.w_PATHPRA),.w_PATHPRA,.w_PATSTD) ,.w_TIPRAG , .w_IDDATRAG,  .w_IDCLADOC , .w_IDTIPALL, .w_IDCLAALL,.w_IDNOMFIL,'N','S','S',.t. ))
        .w_IDDATINI = i_DATSYS
        .w_IDDATOBS = i_findat
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_31.Calculate()
        .DoRTCalc(35,57,.f.)
          if not(empty(.w_IDUTEEXP))
          .link_1_50('Full')
          endif
          .DoRTCalc(58,58,.f.)
        .w_ORIFIL = JUSTFNAME(.w_IDORIFIL)
          .DoRTCalc(60,60,.f.)
        .w_IDSTASOS = 'NOT_SOS'
          .DoRTCalc(62,63,.f.)
        .w_IDWEBRET = ' '
        .oPgFrm.Page2.oPag.oObj_2_48.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(iif(.w_idFLPROV='S', ah_msgformat("Confermato"), ah_msgformat("Provvisorio")),0)
          .DoRTCalc(65,71,.f.)
        .w_ESE_DEL = .f.
          .DoRTCalc(73,76,.f.)
        .w_P_TIPGEN = 'G'
        .DoRTCalc(77,77,.f.)
          if not(empty(.w_P_TIPGEN))
          .link_2_56('Full')
          endif
        .DoRTCalc(78,78,.f.)
          if not(empty(.w_P_RAGGEN))
          .link_2_57('Full')
          endif
          .DoRTCalc(79,79,.f.)
        .w_P_TIPFIR = 'F'
        .DoRTCalc(80,80,.f.)
          if not(empty(.w_P_TIPFIR))
          .link_2_59('Full')
          endif
        .DoRTCalc(81,81,.f.)
          if not(empty(.w_P_RAGFIR))
          .link_2_60('Full')
          endif
          .DoRTCalc(82,82,.f.)
        .w_P_TIPZIP = 'Z'
        .DoRTCalc(83,83,.f.)
          if not(empty(.w_P_TIPZIP))
          .link_2_62('Full')
          endif
        .DoRTCalc(84,84,.f.)
          if not(empty(.w_P_RAGZIP))
          .link_2_63('Full')
          endif
          .DoRTCalc(85,85,.f.)
        .w_IDTIPBST = ' '
      endif
    endwith
    cp_BlankRecExtFlds(this,'PROMINDI')
    this.DoRTCalc(87,87,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PROMINDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMINDI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEIDO","i_CODAZI,w_IDSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_IDSERIAL = .w_IDSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oIDSERIAL_1_2.enabled = !i_bVal
      .Page1.oPag.oIDCLADOC_1_4.enabled = i_bVal
      .Page1.oPag.oIDORIGIN_1_11.enabled = i_bVal
      .Page1.oPag.oIDOGGETT_1_12.enabled = i_bVal
      .Page1.oPag.oID_TESTO_1_13.enabled = i_bVal
      .Page1.oPag.oIDDATCRE_1_22.enabled = i_bVal
      .Page2.oPag.oIDTIPALL_2_2.enabled = i_bVal
      .Page2.oPag.oIDALLPRI_2_3.enabled = i_bVal
      .Page2.oPag.oIDPUBWEB_2_5.enabled = i_bVal
      .Page2.oPag.oIDCLAALL_2_6.enabled = i_bVal
      .Page2.oPag.oIDWEBAPP_2_8.enabled = i_bVal
      .Page2.oPag.oIDFLGFAE_2_9.enabled = i_bVal
      .Page2.oPag.oIDCODCBI_2_10.enabled = i_bVal
      .Page2.oPag.oIDDATRAG_2_11.enabled = i_bVal
      .Page2.oPag.oIDNOMFIL_2_12.enabled = i_bVal
      .Page2.oPag.oIDPATALL_2_25.enabled = i_bVal
      .Page2.oPag.oIDDATINI_2_27.enabled = i_bVal
      .Page2.oPag.oIDDATOBS_2_28.enabled = i_bVal
      .Page2.oPag.oIDWEBERR_2_46.enabled = i_bVal
      .Page1.oPag.oIDORACRE_1_68.enabled = i_bVal
      .Page1.oPag.oIDTIPBST_1_71.enabled = i_bVal
      .Page1.oPag.oIDFLDEPO_1_73.enabled = i_bVal
      .Page1.oPag.oBtn_1_17.enabled = .Page1.oPag.oBtn_1_17.mCond()
      .Page1.oPag.oBtn_1_20.enabled = .Page1.oPag.oBtn_1_20.mCond()
      .Page1.oPag.oBtn_1_58.enabled = .Page1.oPag.oBtn_1_58.mCond()
      .Page1.oPag.oBtn_1_60.enabled = .Page1.oPag.oBtn_1_60.mCond()
      .Page1.oPag.oObj_1_38.enabled = i_bVal
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      .Page1.oPag.oObj_1_40.enabled = i_bVal
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      .Page2.oPag.oObj_2_31.enabled = i_bVal
      .Page2.oPag.oObj_2_48.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oIDCLADOC_1_4.enabled = .t.
        .Page1.oPag.oIDORIGIN_1_11.enabled = .t.
        .Page1.oPag.oIDDATCRE_1_22.enabled = .t.
        .Page2.oPag.oIDNOMFIL_2_12.enabled = .t.
      endif
    endwith
    this.GSUT_MAA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PROMINDI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSUT_MAA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PROMINDI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDSERIAL,"IDSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDFLPROV,"IDFLPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDCLADOC,"IDCLADOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDORIGIN,"IDORIGIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDOGGETT,"IDOGGETT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ID_TESTO,"ID_TESTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDTIPFIL,"IDTIPFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDUTECRE,"IDUTECRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDDATCRE,"IDDATCRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDMODALL,"IDMODALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDTIPALL,"IDTIPALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDALLPRI,"IDALLPRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDERASEF,"IDERASEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDPUBWEB,"IDPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDCLAALL,"IDCLAALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDWEBAPP,"IDWEBAPP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDFLGFAE,"IDFLGFAE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDCODCBI,"IDCODCBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDDATRAG,"IDDATRAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDNOMFIL,"IDNOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDDOCFIR,"IDDOCFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDPATALL,"IDPATALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDDATINI,"IDDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDDATOBS,"IDDATOBS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDCODPRO,"IDCODPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDKEYVAL,"IDKEYVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDORIFIL,"IDORIFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDFLHIDE,"IDFLHIDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDUTEEXP,"IDUTEEXP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDZIPNAM,"IDZIPNAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDSTASOS,"IDSTASOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDCRCALL,"IDCRCALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDWEBFIL,"IDWEBFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDWEBRET,"IDWEBRET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDWEBERR,"IDWEBERR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDDATZIP,"IDDATZIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDWEBAPP,"IDWEBAPP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDORACRE,"IDORACRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDTIPBST,"IDTIPBST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDFLDEPO,"IDFLDEPO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PROMINDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMINDI_IDX,2])
    i_lTable = "PROMINDI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PROMINDI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PROMINDI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMINDI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PROMINDI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEIDO","i_CODAZI,w_IDSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PROMINDI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PROMINDI')
        i_extval=cp_InsertValODBCExtFlds(this,'PROMINDI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(IDSERIAL,IDFLPROV,IDCLADOC,IDORIGIN,IDOGGETT"+;
                  ",ID_TESTO,IDTIPFIL,IDUTECRE,IDDATCRE,IDMODALL"+;
                  ",IDTIPALL,IDALLPRI,IDERASEF,IDPUBWEB,IDCLAALL"+;
                  ",IDWEBAPP,IDFLGFAE,IDCODCBI,IDDATRAG,IDNOMFIL"+;
                  ",IDDOCFIR,IDPATALL,IDDATINI,IDDATOBS,IDCODPRO"+;
                  ",IDKEYVAL,UTCC,UTDC,UTCV,UTDV"+;
                  ",IDORIFIL,IDFLHIDE,IDUTEEXP,IDZIPNAM,IDSTASOS"+;
                  ",IDCRCALL,IDWEBFIL,IDWEBRET,IDWEBERR,IDDATZIP"+;
                  ",IDORACRE,IDTIPBST,IDFLDEPO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_IDSERIAL)+;
                  ","+cp_ToStrODBC(this.w_IDFLPROV)+;
                  ","+cp_ToStrODBCNull(this.w_IDCLADOC)+;
                  ","+cp_ToStrODBC(this.w_IDORIGIN)+;
                  ","+cp_ToStrODBC(this.w_IDOGGETT)+;
                  ","+cp_ToStrODBC(this.w_ID_TESTO)+;
                  ","+cp_ToStrODBC(this.w_IDTIPFIL)+;
                  ","+cp_ToStrODBCNull(this.w_IDUTECRE)+;
                  ","+cp_ToStrODBC(this.w_IDDATCRE)+;
                  ","+cp_ToStrODBC(this.w_IDMODALL)+;
                  ","+cp_ToStrODBCNull(this.w_IDTIPALL)+;
                  ","+cp_ToStrODBC(this.w_IDALLPRI)+;
                  ","+cp_ToStrODBC(this.w_IDERASEF)+;
                  ","+cp_ToStrODBC(this.w_IDPUBWEB)+;
                  ","+cp_ToStrODBCNull(this.w_IDCLAALL)+;
                  ","+cp_ToStrODBC(this.w_IDWEBAPP)+;
                  ","+cp_ToStrODBC(this.w_IDFLGFAE)+;
                  ","+cp_ToStrODBC(this.w_IDCODCBI)+;
                  ","+cp_ToStrODBC(this.w_IDDATRAG)+;
                  ","+cp_ToStrODBC(this.w_IDNOMFIL)+;
                  ","+cp_ToStrODBC(this.w_IDDOCFIR)+;
                  ","+cp_ToStrODBC(this.w_IDPATALL)+;
                  ","+cp_ToStrODBC(this.w_IDDATINI)+;
                  ","+cp_ToStrODBC(this.w_IDDATOBS)+;
                  ","+cp_ToStrODBC(this.w_IDCODPRO)+;
                  ","+cp_ToStrODBC(this.w_IDKEYVAL)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_IDORIFIL)+;
                  ","+cp_ToStrODBC(this.w_IDFLHIDE)+;
                  ","+cp_ToStrODBCNull(this.w_IDUTEEXP)+;
                  ","+cp_ToStrODBC(this.w_IDZIPNAM)+;
                  ","+cp_ToStrODBC(this.w_IDSTASOS)+;
                  ","+cp_ToStrODBC(this.w_IDCRCALL)+;
                  ","+cp_ToStrODBC(this.w_IDWEBFIL)+;
                  ","+cp_ToStrODBC(this.w_IDWEBRET)+;
                  ","+cp_ToStrODBC(this.w_IDWEBERR)+;
                  ","+cp_ToStrODBC(this.w_IDDATZIP)+;
                  ","+cp_ToStrODBC(this.w_IDORACRE)+;
                  ","+cp_ToStrODBC(this.w_IDTIPBST)+;
                  ","+cp_ToStrODBC(this.w_IDFLDEPO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PROMINDI')
        i_extval=cp_InsertValVFPExtFlds(this,'PROMINDI')
        cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL)
        INSERT INTO (i_cTable);
              (IDSERIAL,IDFLPROV,IDCLADOC,IDORIGIN,IDOGGETT,ID_TESTO,IDTIPFIL,IDUTECRE,IDDATCRE,IDMODALL,IDTIPALL,IDALLPRI,IDERASEF,IDPUBWEB,IDCLAALL,IDWEBAPP,IDFLGFAE,IDCODCBI,IDDATRAG,IDNOMFIL,IDDOCFIR,IDPATALL,IDDATINI,IDDATOBS,IDCODPRO,IDKEYVAL,UTCC,UTDC,UTCV,UTDV,IDORIFIL,IDFLHIDE,IDUTEEXP,IDZIPNAM,IDSTASOS,IDCRCALL,IDWEBFIL,IDWEBRET,IDWEBERR,IDDATZIP,IDORACRE,IDTIPBST,IDFLDEPO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_IDSERIAL;
                  ,this.w_IDFLPROV;
                  ,this.w_IDCLADOC;
                  ,this.w_IDORIGIN;
                  ,this.w_IDOGGETT;
                  ,this.w_ID_TESTO;
                  ,this.w_IDTIPFIL;
                  ,this.w_IDUTECRE;
                  ,this.w_IDDATCRE;
                  ,this.w_IDMODALL;
                  ,this.w_IDTIPALL;
                  ,this.w_IDALLPRI;
                  ,this.w_IDERASEF;
                  ,this.w_IDPUBWEB;
                  ,this.w_IDCLAALL;
                  ,this.w_IDWEBAPP;
                  ,this.w_IDFLGFAE;
                  ,this.w_IDCODCBI;
                  ,this.w_IDDATRAG;
                  ,this.w_IDNOMFIL;
                  ,this.w_IDDOCFIR;
                  ,this.w_IDPATALL;
                  ,this.w_IDDATINI;
                  ,this.w_IDDATOBS;
                  ,this.w_IDCODPRO;
                  ,this.w_IDKEYVAL;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTCV;
                  ,this.w_UTDV;
                  ,this.w_IDORIFIL;
                  ,this.w_IDFLHIDE;
                  ,this.w_IDUTEEXP;
                  ,this.w_IDZIPNAM;
                  ,this.w_IDSTASOS;
                  ,this.w_IDCRCALL;
                  ,this.w_IDWEBFIL;
                  ,this.w_IDWEBRET;
                  ,this.w_IDWEBERR;
                  ,this.w_IDDATZIP;
                  ,this.w_IDORACRE;
                  ,this.w_IDTIPBST;
                  ,this.w_IDFLDEPO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PROMINDI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMINDI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PROMINDI_IDX,i_nConn)
      *
      * update PROMINDI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PROMINDI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " IDFLPROV="+cp_ToStrODBC(this.w_IDFLPROV)+;
             ",IDCLADOC="+cp_ToStrODBCNull(this.w_IDCLADOC)+;
             ",IDORIGIN="+cp_ToStrODBC(this.w_IDORIGIN)+;
             ",IDOGGETT="+cp_ToStrODBC(this.w_IDOGGETT)+;
             ",ID_TESTO="+cp_ToStrODBC(this.w_ID_TESTO)+;
             ",IDTIPFIL="+cp_ToStrODBC(this.w_IDTIPFIL)+;
             ",IDUTECRE="+cp_ToStrODBCNull(this.w_IDUTECRE)+;
             ",IDDATCRE="+cp_ToStrODBC(this.w_IDDATCRE)+;
             ",IDMODALL="+cp_ToStrODBC(this.w_IDMODALL)+;
             ",IDTIPALL="+cp_ToStrODBCNull(this.w_IDTIPALL)+;
             ",IDALLPRI="+cp_ToStrODBC(this.w_IDALLPRI)+;
             ",IDERASEF="+cp_ToStrODBC(this.w_IDERASEF)+;
             ",IDPUBWEB="+cp_ToStrODBC(this.w_IDPUBWEB)+;
             ",IDCLAALL="+cp_ToStrODBCNull(this.w_IDCLAALL)+;
             ",IDWEBAPP="+cp_ToStrODBC(this.w_IDWEBAPP)+;
             ",IDFLGFAE="+cp_ToStrODBC(this.w_IDFLGFAE)+;
             ",IDCODCBI="+cp_ToStrODBC(this.w_IDCODCBI)+;
             ",IDDATRAG="+cp_ToStrODBC(this.w_IDDATRAG)+;
             ",IDNOMFIL="+cp_ToStrODBC(this.w_IDNOMFIL)+;
             ",IDDOCFIR="+cp_ToStrODBC(this.w_IDDOCFIR)+;
             ",IDPATALL="+cp_ToStrODBC(this.w_IDPATALL)+;
             ",IDDATINI="+cp_ToStrODBC(this.w_IDDATINI)+;
             ",IDDATOBS="+cp_ToStrODBC(this.w_IDDATOBS)+;
             ",IDCODPRO="+cp_ToStrODBC(this.w_IDCODPRO)+;
             ",IDKEYVAL="+cp_ToStrODBC(this.w_IDKEYVAL)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",IDORIFIL="+cp_ToStrODBC(this.w_IDORIFIL)+;
             ",IDFLHIDE="+cp_ToStrODBC(this.w_IDFLHIDE)+;
             ",IDUTEEXP="+cp_ToStrODBCNull(this.w_IDUTEEXP)+;
             ",IDZIPNAM="+cp_ToStrODBC(this.w_IDZIPNAM)+;
             ",IDSTASOS="+cp_ToStrODBC(this.w_IDSTASOS)+;
             ",IDCRCALL="+cp_ToStrODBC(this.w_IDCRCALL)+;
             ",IDWEBFIL="+cp_ToStrODBC(this.w_IDWEBFIL)+;
             ",IDWEBRET="+cp_ToStrODBC(this.w_IDWEBRET)+;
             ",IDWEBERR="+cp_ToStrODBC(this.w_IDWEBERR)+;
             ",IDDATZIP="+cp_ToStrODBC(this.w_IDDATZIP)+;
             ",IDORACRE="+cp_ToStrODBC(this.w_IDORACRE)+;
             ",IDTIPBST="+cp_ToStrODBC(this.w_IDTIPBST)+;
             ",IDFLDEPO="+cp_ToStrODBC(this.w_IDFLDEPO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PROMINDI')
        i_cWhere = cp_PKFox(i_cTable  ,'IDSERIAL',this.w_IDSERIAL  )
        UPDATE (i_cTable) SET;
              IDFLPROV=this.w_IDFLPROV;
             ,IDCLADOC=this.w_IDCLADOC;
             ,IDORIGIN=this.w_IDORIGIN;
             ,IDOGGETT=this.w_IDOGGETT;
             ,ID_TESTO=this.w_ID_TESTO;
             ,IDTIPFIL=this.w_IDTIPFIL;
             ,IDUTECRE=this.w_IDUTECRE;
             ,IDDATCRE=this.w_IDDATCRE;
             ,IDMODALL=this.w_IDMODALL;
             ,IDTIPALL=this.w_IDTIPALL;
             ,IDALLPRI=this.w_IDALLPRI;
             ,IDERASEF=this.w_IDERASEF;
             ,IDPUBWEB=this.w_IDPUBWEB;
             ,IDCLAALL=this.w_IDCLAALL;
             ,IDWEBAPP=this.w_IDWEBAPP;
             ,IDFLGFAE=this.w_IDFLGFAE;
             ,IDCODCBI=this.w_IDCODCBI;
             ,IDDATRAG=this.w_IDDATRAG;
             ,IDNOMFIL=this.w_IDNOMFIL;
             ,IDDOCFIR=this.w_IDDOCFIR;
             ,IDPATALL=this.w_IDPATALL;
             ,IDDATINI=this.w_IDDATINI;
             ,IDDATOBS=this.w_IDDATOBS;
             ,IDCODPRO=this.w_IDCODPRO;
             ,IDKEYVAL=this.w_IDKEYVAL;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTCV=this.w_UTCV;
             ,UTDV=this.w_UTDV;
             ,IDORIFIL=this.w_IDORIFIL;
             ,IDFLHIDE=this.w_IDFLHIDE;
             ,IDUTEEXP=this.w_IDUTEEXP;
             ,IDZIPNAM=this.w_IDZIPNAM;
             ,IDSTASOS=this.w_IDSTASOS;
             ,IDCRCALL=this.w_IDCRCALL;
             ,IDWEBFIL=this.w_IDWEBFIL;
             ,IDWEBRET=this.w_IDWEBRET;
             ,IDWEBERR=this.w_IDWEBERR;
             ,IDDATZIP=this.w_IDDATZIP;
             ,IDORACRE=this.w_IDORACRE;
             ,IDTIPBST=this.w_IDTIPBST;
             ,IDFLDEPO=this.w_IDFLDEPO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSUT_MAA : Saving
      this.GSUT_MAA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_IDSERIAL,"IDSERIAL";
             )
      this.GSUT_MAA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSUT_MAA : Deleting
    this.GSUT_MAA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_IDSERIAL,"IDSERIAL";
           )
    this.GSUT_MAA.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PROMINDI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMINDI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PROMINDI_IDX,i_nConn)
      *
      * delete PROMINDI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'IDSERIAL',this.w_IDSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PROMINDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMINDI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_IDORIFIL<>.w_IDORIFIL.or. .o_IDMODALL<>.w_IDMODALL
            .w_IDORIGIN = IIF(empty(.w_IDORIFIL), space(100), left(substr(.w_IDORIFIL ,RAT('\', .w_IDORIFIL)+1),100))
        endif
        .DoRTCalc(9,10,.t.)
        if .o_IDNOMFIL<>.w_IDNOMFIL
            .w_IDTIPFIL = lower(substr(.w_IDNOMFIL, rat(".", .w_IDNOMFIL)))
        endif
          .link_1_21('Full')
        .DoRTCalc(13,25,.t.)
        if .o_IDORIFIL<>.w_IDORIFIL.or. .o_IDMODALL<>.w_IDMODALL.or. .o_FILEALT<>.w_FILEALT
            .w_IDNOMFIL = icase(.w_IDMODALL='E' or .oldmodall='E' or .w_IDMODALL='I' or .oldmodall='I', .w_IDNOMFIL, .w_FLEDIT and .w_IDMODALL='F', JUSTFNAME(.w_IDORIFIL), left(alltrim(IIF(empty(.w_IDORIFIL), space(100),  IIF(.w_IDMODALL='F',.w_IDSERIAL+'_','')+substr(.w_IDORIFIL ,RAT('\', .w_IDORIFIL)+1))),100))
        endif
        .DoRTCalc(27,31,.t.)
        if .o_IDCLAALL<>.w_IDCLAALL.or. .o_IDTIPALL<>.w_IDTIPALL
            .w_IDPATALL = iif(.cFunction='Edit' and !.w_FLEDIT,.w_IDPATALL,dcmpatar( iif(Not Empty(.w_PATHPRA),.w_PATHPRA,.w_PATSTD) ,.w_TIPRAG , .w_IDDATRAG,  .w_IDCLADOC , .w_IDTIPALL, .w_IDCLAALL,.w_IDNOMFIL,'N','S','S',.t. ))
        endif
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_31.Calculate()
        if .o_IDMODALL<>.w_IDMODALL
          .Calculate_RCLUVGMNSW()
        endif
        if .o_IDALLPRI<>.w_IDALLPRI
          .Calculate_KXMOXCSVWA()
        endif
        if .o_IDORIGIN<>.w_IDORIGIN
          .Calculate_TXFQVOBVNO()
        endif
        if .o_FILEALT<>.w_FILEALT
          .Calculate_HSTVEECMGY()
        endif
        .DoRTCalc(33,56,.t.)
          .link_1_50('Full')
        .DoRTCalc(58,58,.t.)
            .w_ORIFIL = JUSTFNAME(.w_IDORIFIL)
        .oPgFrm.Page2.oPag.oObj_2_48.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(iif(.w_idFLPROV='S', ah_msgformat("Confermato"), ah_msgformat("Provvisorio")),0)
        .DoRTCalc(60,71,.t.)
        if .o_IDSERIAL<>.w_IDSERIAL
            .w_ESE_DEL = .f.
        endif
        if .o_IDWEBAPP<>.w_IDWEBAPP
          .Calculate_JVXUUUBPGT()
        endif
        .DoRTCalc(73,76,.t.)
          .link_2_56('Full')
          .link_2_57('Full')
        .DoRTCalc(79,79,.t.)
          .link_2_59('Full')
          .link_2_60('Full')
        .DoRTCalc(82,82,.t.)
          .link_2_62('Full')
          .link_2_63('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SEIDO","i_CODAZI,w_IDSERIAL")
          .op_IDSERIAL = .w_IDSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(85,87,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_31.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_48.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(iif(.w_idFLPROV='S', ah_msgformat("Confermato"), ah_msgformat("Provvisorio")),0)
    endwith
  return

  proc Calculate_RCLUVGMNSW()
    with this
          * --- Gestione cambio modalit� di archiviazione allegato
          MODIFICA(this;
             )
    endwith
  endproc
  proc Calculate_KXMOXCSVWA()
    with this
          * --- Controllo su allegato principale
          ALLEGPRIN(this;
             )
    endwith
  endproc
  proc Calculate_VAKAVNXIOW()
    with this
          * --- Verifica univocit� allegato
          GSUT_BUN(this;
              ,'U';
             )
    endwith
  endproc
  proc Calculate_TXFQVOBVNO()
    with this
          * --- Assegnamento dell'estensione al nome del file originale,se mancante
          .w_IDORIGIN = iif(empty(justext(.w_IDORIGIN)) and Not Empty(.w_IDORIGIN),alltrim(.w_IDORIGIN)+.w_IDTIPFIL,.w_IDORIGIN)
          .w_IDOGGETT = iif(.w_FLEDIT and Empty(.w_IDOGGETT),iif(.w_IDMODALL='F',.w_IDORIGIN,.w_ORIFIL),.w_IDOGGETT)
          .w_IDNOMFIL = IIF(.w_FLEDIT AND .oldmodall='L' AND .w_IDMODALL='F',.w_IDORIGIN,.w_IDNOMFIL)
    endwith
  endproc
  proc Calculate_HSTVEECMGY()
    with this
          * --- Nome file
          .w_IDNOMFIL = iif(not empty(.w_FILEALT),.w_FILEALT,.w_IDNOMFIL)
    endwith
  endproc
  proc Calculate_CYNHZSLIUK()
    with this
          * --- Verifica esistenza file
          GSUT_BUN(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_OUKQJHSOKI()
    with this
          * --- Verifica esistenza file
          GSUT_BUN(this;
              ,'D';
             )
    endwith
  endproc
  proc Calculate_TYIOAJXYFS()
    with this
          * --- Inizializza OldWebApp
          .w_OldWebApp = .w_IDWEBAPP
          .w_Oldogg = .w_IDOGGETT
    endwith
  endproc
  proc Calculate_JVXUUUBPGT()
    with this
          * --- Flag di cancella file - aggiornato con valore della classe
          .w_IDERASEF = IIF(.w_IDWEBAPP$'AS',.w_CDERASEF,.w_IDERASEF)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oIDCLADOC_1_4.enabled = this.oPgFrm.Page1.oPag.oIDCLADOC_1_4.mCond()
    this.oPgFrm.Page1.oPag.oIDORIGIN_1_11.enabled = this.oPgFrm.Page1.oPag.oIDORIGIN_1_11.mCond()
    this.oPgFrm.Page2.oPag.oIDTIPALL_2_2.enabled = this.oPgFrm.Page2.oPag.oIDTIPALL_2_2.mCond()
    this.oPgFrm.Page2.oPag.oIDCLAALL_2_6.enabled = this.oPgFrm.Page2.oPag.oIDCLAALL_2_6.mCond()
    this.oPgFrm.Page2.oPag.oIDWEBAPP_2_8.enabled = this.oPgFrm.Page2.oPag.oIDWEBAPP_2_8.mCond()
    this.oPgFrm.Page2.oPag.oIDDATRAG_2_11.enabled = this.oPgFrm.Page2.oPag.oIDDATRAG_2_11.mCond()
    this.oPgFrm.Page2.oPag.oIDNOMFIL_2_12.enabled = this.oPgFrm.Page2.oPag.oIDNOMFIL_2_12.mCond()
    this.oPgFrm.Page2.oPag.oIDPATALL_2_25.enabled = this.oPgFrm.Page2.oPag.oIDPATALL_2_25.mCond()
    this.oPgFrm.Page1.oPag.oIDTIPBST_1_71.enabled = this.oPgFrm.Page1.oPag.oIDTIPBST_1_71.mCond()
    this.oPgFrm.Page1.oPag.oIDFLDEPO_1_73.enabled = this.oPgFrm.Page1.oPag.oIDFLDEPO_1_73.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show2
    i_show2=not(g_docm<>'S')
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Attributi di ricerca"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oIDORIGIN_1_11.visible=!this.oPgFrm.Page1.oPag.oIDORIGIN_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_17.visible=!this.oPgFrm.Page1.oPag.oBtn_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_20.visible=!this.oPgFrm.Page1.oPag.oBtn_1_20.mHide()
    this.oPgFrm.Page2.oPag.oIDERASEF_2_4.visible=!this.oPgFrm.Page2.oPag.oIDERASEF_2_4.mHide()
    this.oPgFrm.Page2.oPag.oIDPUBWEB_2_5.visible=!this.oPgFrm.Page2.oPag.oIDPUBWEB_2_5.mHide()
    this.oPgFrm.Page2.oPag.oIDWEBAPP_2_8.visible=!this.oPgFrm.Page2.oPag.oIDWEBAPP_2_8.mHide()
    this.oPgFrm.Page2.oPag.oIDFLGFAE_2_9.visible=!this.oPgFrm.Page2.oPag.oIDFLGFAE_2_9.mHide()
    this.oPgFrm.Page2.oPag.oIDCODCBI_2_10.visible=!this.oPgFrm.Page2.oPag.oIDCODCBI_2_10.mHide()
    this.oPgFrm.Page2.oPag.oIDDATRAG_2_11.visible=!this.oPgFrm.Page2.oPag.oIDDATRAG_2_11.mHide()
    this.oPgFrm.Page2.oPag.oIDDOCFIR_2_13.visible=!this.oPgFrm.Page2.oPag.oIDDOCFIR_2_13.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_15.visible=!this.oPgFrm.Page2.oPag.oStr_2_15.mHide()
    this.oPgFrm.Page2.oPag.oPATSTD_2_16.visible=!this.oPgFrm.Page2.oPag.oPATSTD_2_16.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_17.visible=!this.oPgFrm.Page2.oPag.oStr_2_17.mHide()
    this.oPgFrm.Page2.oPag.oTIPRAG_2_18.visible=!this.oPgFrm.Page2.oPag.oTIPRAG_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_24.visible=!this.oPgFrm.Page2.oPag.oStr_2_24.mHide()
    this.oPgFrm.Page2.oPag.oIDPATALL_2_25.visible=!this.oPgFrm.Page2.oPag.oIDPATALL_2_25.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_26.visible=!this.oPgFrm.Page2.oPag.oStr_2_26.mHide()
    this.oPgFrm.Page3.oPag.oLinkPC_3_1.visible=!this.oPgFrm.Page3.oPag.oLinkPC_3_1.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_39.visible=!this.oPgFrm.Page2.oPag.oStr_2_39.mHide()
    this.oPgFrm.Page1.oPag.oIDUTEEXP_1_50.visible=!this.oPgFrm.Page1.oPag.oIDUTEEXP_1_50.mHide()
    this.oPgFrm.Page1.oPag.oDESUTEEX_1_51.visible=!this.oPgFrm.Page1.oPag.oDESUTEEX_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oORIFIL_1_53.visible=!this.oPgFrm.Page1.oPag.oORIFIL_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oIDZIPNAM_1_55.visible=!this.oPgFrm.Page1.oPag.oIDZIPNAM_1_55.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oIDSTASOS_1_57.visible=!this.oPgFrm.Page1.oPag.oIDSTASOS_1_57.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_43.visible=!this.oPgFrm.Page2.oPag.oStr_2_43.mHide()
    this.oPgFrm.Page2.oPag.oIDWEBFIL_2_44.visible=!this.oPgFrm.Page2.oPag.oIDWEBFIL_2_44.mHide()
    this.oPgFrm.Page2.oPag.oIDWEBRET_2_45.visible=!this.oPgFrm.Page2.oPag.oIDWEBRET_2_45.mHide()
    this.oPgFrm.Page2.oPag.oIDWEBERR_2_46.visible=!this.oPgFrm.Page2.oPag.oIDWEBERR_2_46.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_47.visible=!this.oPgFrm.Page2.oPag.oStr_2_47.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_58.visible=!this.oPgFrm.Page1.oPag.oBtn_1_58.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_49.visible=!this.oPgFrm.Page2.oPag.oStr_2_49.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_60.visible=!this.oPgFrm.Page1.oPag.oBtn_1_60.mHide()
    this.oPgFrm.Page1.oPag.oIDDATZIP_1_62.visible=!this.oPgFrm.Page1.oPag.oIDDATZIP_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page2.oPag.oIDWEBAPP_2_51.visible=!this.oPgFrm.Page2.oPag.oIDWEBAPP_2_51.mHide()
    this.oPgFrm.Page1.oPag.oIDORACRE_1_68.visible=!this.oPgFrm.Page1.oPag.oIDORACRE_1_68.mHide()
    this.oPgFrm.Page1.oPag.oIDTIPBST_1_71.visible=!this.oPgFrm.Page1.oPag.oIDTIPBST_1_71.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_72.visible=!this.oPgFrm.Page1.oPag.oStr_1_72.mHide()
    this.oPgFrm.Page1.oPag.oIDFLDEPO_1_73.visible=!this.oPgFrm.Page1.oPag.oIDFLDEPO_1_73.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsut_aid
    If cEvent='Edit Aborted'
       this.VisuMess = .f.
       this.VisPat = ''
    endif
    if cevent='Delete end'
       this.NotifyEvent("Cancella")
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_31.Event(cEvent)
        if lower(cEvent)==lower("Univocit�")
          .Calculate_VAKAVNXIOW()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_48.Event(cEvent)
        if lower(cEvent)==lower("Creafile")
          .Calculate_CYNHZSLIUK()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
        if lower(cEvent)==lower("Record Updated")
          .Calculate_OUKQJHSOKI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_TYIOAJXYFS()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_aid
    IF Upper(CEVENT)='INIT'
      if Upper(this.GSUT_MAA.class)='STDDYNAMICCHILD'
       This.oPgFrm.Pages[3].opag.uienable(.T.)
       This.oPgFrm.ActivePage=1
      Endif
    Endif
    
    if Upper(cevent)='EDIT STARTED' and empty(nvl(this.w_IDORIFIL,' '))
         obj=this.getctrl('w_IDMODALL')
         obj.enabled=.f.
         obj=.null.
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IDCLADOC
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IDCLADOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsut_baf',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_IDCLADOC)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDPATSTD,CDTIPRAG,CDMODALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDCLAALL,CDRIFTAB,CDFIRDIG,CDALIASF,CDFLGDES,CDERASEF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_IDCLADOC))
          select CDCODCLA,CDDESCLA,CDPATSTD,CDTIPRAG,CDMODALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDCLAALL,CDRIFTAB,CDFIRDIG,CDALIASF,CDFLGDES,CDERASEF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IDCLADOC)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IDCLADOC) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oIDCLADOC_1_4'),i_cWhere,'gsut_baf',"Classi librerie immagini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDPATSTD,CDTIPRAG,CDMODALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDCLAALL,CDRIFTAB,CDFIRDIG,CDALIASF,CDFLGDES,CDERASEF";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDPATSTD,CDTIPRAG,CDMODALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDCLAALL,CDRIFTAB,CDFIRDIG,CDALIASF,CDFLGDES,CDERASEF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IDCLADOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDPATSTD,CDTIPRAG,CDMODALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDCLAALL,CDRIFTAB,CDFIRDIG,CDALIASF,CDFLGDES,CDERASEF";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_IDCLADOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_IDCLADOC)
            select CDCODCLA,CDDESCLA,CDPATSTD,CDTIPRAG,CDMODALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDCLAALL,CDRIFTAB,CDFIRDIG,CDALIASF,CDFLGDES,CDERASEF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IDCLADOC = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCCLA = NVL(_Link_.CDDESCLA,space(70))
      this.w_PATSTD = NVL(_Link_.CDPATSTD,space(254))
      this.w_TIPRAG = NVL(_Link_.CDTIPRAG,space(1))
      this.w_IDMODALL = NVL(_Link_.CDMODALL,space(1))
      this.w_PATHTIP = NVL(_Link_.CDCOMTIP,space(1))
      this.w_PATHCLA = NVL(_Link_.CDCOMCLA,space(1))
      this.w_IDTIPALL = NVL(_Link_.CDTIPALL,space(5))
      this.w_IDCLAALL = NVL(_Link_.CDCLAALL,space(5))
      this.w_ARCHIVIO = NVL(_Link_.CDRIFTAB,space(20))
      this.w_FIRMDIG = NVL(_Link_.CDFIRDIG,space(1))
      this.w_ALIASFILE = NVL(_Link_.CDALIASF,space(15))
      this.w_FLGDES = NVL(_Link_.CDFLGDES,space(1))
      this.w_CDERASEF = NVL(_Link_.CDERASEF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_IDCLADOC = space(15)
      endif
      this.w_DESCCLA = space(70)
      this.w_PATSTD = space(254)
      this.w_TIPRAG = space(1)
      this.w_IDMODALL = space(1)
      this.w_PATHTIP = space(1)
      this.w_PATHCLA = space(1)
      this.w_IDTIPALL = space(5)
      this.w_IDCLAALL = space(5)
      this.w_ARCHIVIO = space(20)
      this.w_FIRMDIG = space(1)
      this.w_ALIASFILE = space(15)
      this.w_FLGDES = space(1)
      this.w_CDERASEF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=iif(lower(.cFunction)='load', ( SUBSTR( CHKPECLA( .w_IDCLADOC, i_CODUTE), 2, 1 ) = 'S' ) , .t.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per archiviare documenti nella classe selezionata")
        endif
        this.w_IDCLADOC = space(15)
        this.w_DESCCLA = space(70)
        this.w_PATSTD = space(254)
        this.w_TIPRAG = space(1)
        this.w_IDMODALL = space(1)
        this.w_PATHTIP = space(1)
        this.w_PATHCLA = space(1)
        this.w_IDTIPALL = space(5)
        this.w_IDCLAALL = space(5)
        this.w_ARCHIVIO = space(20)
        this.w_FIRMDIG = space(1)
        this.w_ALIASFILE = space(15)
        this.w_FLGDES = space(1)
        this.w_CDERASEF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IDCLADOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 14 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PROMCLAS_IDX,3] and i_nFlds+14<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.CDCODCLA as CDCODCLA104"+ ",link_1_4.CDDESCLA as CDDESCLA104"+ ",link_1_4.CDPATSTD as CDPATSTD104"+ ",link_1_4.CDTIPRAG as CDTIPRAG104"+ ",link_1_4.CDMODALL as CDMODALL104"+ ",link_1_4.CDCOMTIP as CDCOMTIP104"+ ",link_1_4.CDCOMCLA as CDCOMCLA104"+ ",link_1_4.CDTIPALL as CDTIPALL104"+ ",link_1_4.CDCLAALL as CDCLAALL104"+ ",link_1_4.CDRIFTAB as CDRIFTAB104"+ ",link_1_4.CDFIRDIG as CDFIRDIG104"+ ",link_1_4.CDALIASF as CDALIASF104"+ ",link_1_4.CDFLGDES as CDFLGDES104"+ ",link_1_4.CDERASEF as CDERASEF104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on PROMINDI.IDCLADOC=link_1_4.CDCODCLA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and PROMINDI.IDCLADOC=link_1_4.CDCODCLA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IDUTECRE
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IDUTECRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IDUTECRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_IDUTECRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_IDUTECRE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IDUTECRE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_IDUTECRE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IDUTECRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IDTIPALL
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_lTable = "TIP_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2], .t., this.TIP_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IDTIPALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'TIP_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODICE like "+cp_ToStrODBC(trim(this.w_IDTIPALL)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',trim(this.w_IDTIPALL))
          select TACODICE,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IDTIPALL)==trim(_Link_.TACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IDTIPALL) and !this.bDontReportError
            deferred_cp_zoom('TIP_ALLE','*','TACODICE',cp_AbsName(oSource.parent,'oIDTIPALL_2_2'),i_cWhere,'GSUT_MTA',"Tipologie allegato",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1))
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IDTIPALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(this.w_IDTIPALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_IDTIPALL)
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IDTIPALL = NVL(_Link_.TACODICE,space(5))
      this.w_DESTIPAL = NVL(_Link_.TADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_IDTIPALL = space(5)
      endif
      this.w_DESTIPAL = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IDTIPALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_ALLE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.TACODICE as TACODICE202"+ ",link_2_2.TADESCRI as TADESCRI202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on PROMINDI.IDTIPALL=link_2_2.TACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and PROMINDI.IDTIPALL=link_2_2.TACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IDCLAALL
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
    i_lTable = "CLA_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2], .t., this.CLA_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IDCLAALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'CLA_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODCLA like "+cp_ToStrODBC(trim(this.w_IDCLAALL)+"%");
                   +" and TACODICE="+cp_ToStrODBC(this.w_IDTIPALL);

          i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TDPUBWEB,TACLADES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE,TACODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',this.w_IDTIPALL;
                     ,'TACODCLA',trim(this.w_IDCLAALL))
          select TACODICE,TACODCLA,TDPUBWEB,TACLADES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE,TACODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IDCLAALL)==trim(_Link_.TACODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IDCLAALL) and !this.bDontReportError
            deferred_cp_zoom('CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(oSource.parent,'oIDCLAALL_2_6'),i_cWhere,'GSUT_MTA',"Classi allegato",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_IDTIPALL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TDPUBWEB,TACLADES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TACODICE,TACODCLA,TDPUBWEB,TACLADES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TDPUBWEB,TACLADES";
                     +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TACODICE="+cp_ToStrODBC(this.w_IDTIPALL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1);
                       ,'TACODCLA',oSource.xKey(2))
            select TACODICE,TACODCLA,TDPUBWEB,TACLADES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IDCLAALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TDPUBWEB,TACLADES";
                   +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(this.w_IDCLAALL);
                   +" and TACODICE="+cp_ToStrODBC(this.w_IDTIPALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_IDTIPALL;
                       ,'TACODCLA',this.w_IDCLAALL)
            select TACODICE,TACODCLA,TDPUBWEB,TACLADES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IDCLAALL = NVL(_Link_.TACODCLA,space(5))
      this.w_INDPUBWEB = NVL(_Link_.TDPUBWEB,space(1))
      this.w_DESCLA = NVL(_Link_.TACLADES,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_IDCLAALL = space(5)
      endif
      this.w_INDPUBWEB = space(1)
      this.w_DESCLA = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)+'\'+cp_ToStr(_Link_.TACODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IDCLAALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLA_ALLE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.TACODCLA as TACODCLA206"+ ",link_2_6.TDPUBWEB as TDPUBWEB206"+ ",link_2_6.TACLADES as TACLADES206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on PROMINDI.IDCLAALL=link_2_6.TACODCLA"+" and PROMINDI.IDTIPALL=link_2_6.TACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and PROMINDI.IDCLAALL=link_2_6.TACODCLA(+)"'+'+" and PROMINDI.IDTIPALL=link_2_6.TACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IDUTEEXP
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IDUTEEXP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IDUTEEXP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_IDUTEEXP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_IDUTEEXP)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IDUTEEXP = NVL(_Link_.CODE,0)
      this.w_DESUTEEX = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_IDUTEEXP = 0
      endif
      this.w_DESUTEEX = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IDUTEEXP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=P_TIPGEN
  func Link_2_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_lTable = "RAG_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2], .t., this.RAG_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_P_TIPGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_P_TIPGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RATIPBST,RASERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where RATIPBST="+cp_ToStrODBC(this.w_P_TIPGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RATIPBST',this.w_P_TIPGEN)
            select RATIPBST,RASERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_P_TIPGEN = NVL(_Link_.RATIPBST,space(10))
      this.w_P_RAGGEN = NVL(_Link_.RASERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_P_TIPGEN = space(10)
      endif
      this.w_P_RAGGEN = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.RATIPBST,1)
      cp_ShowWarn(i_cKey,this.RAG_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_P_TIPGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=P_RAGGEN
  func Link_2_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_lTable = "RAG_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2], .t., this.RAG_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_P_RAGGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_P_RAGGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RATIPBST,RASERIAL,RAESTENS";
                   +" from "+i_cTable+" "+i_lTable+" where RASERIAL="+cp_ToStrODBC(this.w_P_RAGGEN);
                   +" and RATIPBST="+cp_ToStrODBC('G');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RATIPBST','G';
                       ,'RASERIAL',this.w_P_RAGGEN)
            select RATIPBST,RASERIAL,RAESTENS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_P_RAGGEN = NVL(_Link_.RASERIAL,space(10))
      this.w_EST_GEN = NVL(_Link_.RAESTENS,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_P_RAGGEN = space(10)
      endif
      this.w_EST_GEN = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.RATIPBST,1)+'\'+cp_ToStr(_Link_.RASERIAL,1)
      cp_ShowWarn(i_cKey,this.RAG_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_P_RAGGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=P_TIPFIR
  func Link_2_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_lTable = "RAG_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2], .t., this.RAG_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_P_TIPFIR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_P_TIPFIR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RATIPBST,RASERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where RATIPBST="+cp_ToStrODBC(this.w_P_TIPFIR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RATIPBST',this.w_P_TIPFIR)
            select RATIPBST,RASERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_P_TIPFIR = NVL(_Link_.RATIPBST,space(10))
      this.w_P_RAGFIR = NVL(_Link_.RASERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_P_TIPFIR = space(10)
      endif
      this.w_P_RAGFIR = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.RATIPBST,1)
      cp_ShowWarn(i_cKey,this.RAG_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_P_TIPFIR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=P_RAGFIR
  func Link_2_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_lTable = "RAG_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2], .t., this.RAG_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_P_RAGFIR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_P_RAGFIR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RATIPBST,RASERIAL,RAESTENS";
                   +" from "+i_cTable+" "+i_lTable+" where RASERIAL="+cp_ToStrODBC(this.w_P_RAGFIR);
                   +" and RATIPBST="+cp_ToStrODBC('F');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RATIPBST','F';
                       ,'RASERIAL',this.w_P_RAGFIR)
            select RATIPBST,RASERIAL,RAESTENS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_P_RAGFIR = NVL(_Link_.RASERIAL,space(10))
      this.w_EST_FIR = NVL(_Link_.RAESTENS,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_P_RAGFIR = space(10)
      endif
      this.w_EST_FIR = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.RATIPBST,1)+'\'+cp_ToStr(_Link_.RASERIAL,1)
      cp_ShowWarn(i_cKey,this.RAG_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_P_RAGFIR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=P_TIPZIP
  func Link_2_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_lTable = "RAG_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2], .t., this.RAG_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_P_TIPZIP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_P_TIPZIP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RATIPBST,RASERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where RATIPBST="+cp_ToStrODBC(this.w_P_TIPZIP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RATIPBST',this.w_P_TIPZIP)
            select RATIPBST,RASERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_P_TIPZIP = NVL(_Link_.RATIPBST,space(10))
      this.w_P_RAGZIP = NVL(_Link_.RASERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_P_TIPZIP = space(10)
      endif
      this.w_P_RAGZIP = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.RATIPBST,1)
      cp_ShowWarn(i_cKey,this.RAG_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_P_TIPZIP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=P_RAGZIP
  func Link_2_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_lTable = "RAG_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2], .t., this.RAG_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_P_RAGZIP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_P_RAGZIP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RATIPBST,RASERIAL,RAESTENS";
                   +" from "+i_cTable+" "+i_lTable+" where RASERIAL="+cp_ToStrODBC(this.w_P_RAGZIP);
                   +" and RATIPBST="+cp_ToStrODBC('Z');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RATIPBST','Z';
                       ,'RASERIAL',this.w_P_RAGZIP)
            select RATIPBST,RASERIAL,RAESTENS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_P_RAGZIP = NVL(_Link_.RASERIAL,space(10))
      this.w_EST_ZIP = NVL(_Link_.RAESTENS,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_P_RAGZIP = space(10)
      endif
      this.w_EST_ZIP = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.RATIPBST,1)+'\'+cp_ToStr(_Link_.RASERIAL,1)
      cp_ShowWarn(i_cKey,this.RAG_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_P_RAGZIP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIDSERIAL_1_2.value==this.w_IDSERIAL)
      this.oPgFrm.Page1.oPag.oIDSERIAL_1_2.value=this.w_IDSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oIDCLADOC_1_4.value==this.w_IDCLADOC)
      this.oPgFrm.Page1.oPag.oIDCLADOC_1_4.value=this.w_IDCLADOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCLA_1_7.value==this.w_DESCCLA)
      this.oPgFrm.Page1.oPag.oDESCCLA_1_7.value=this.w_DESCCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oIDORIGIN_1_11.value==this.w_IDORIGIN)
      this.oPgFrm.Page1.oPag.oIDORIGIN_1_11.value=this.w_IDORIGIN
    endif
    if not(this.oPgFrm.Page1.oPag.oIDOGGETT_1_12.value==this.w_IDOGGETT)
      this.oPgFrm.Page1.oPag.oIDOGGETT_1_12.value=this.w_IDOGGETT
    endif
    if not(this.oPgFrm.Page1.oPag.oID_TESTO_1_13.value==this.w_ID_TESTO)
      this.oPgFrm.Page1.oPag.oID_TESTO_1_13.value=this.w_ID_TESTO
    endif
    if not(this.oPgFrm.Page1.oPag.oIDTIPFIL_1_19.value==this.w_IDTIPFIL)
      this.oPgFrm.Page1.oPag.oIDTIPFIL_1_19.value=this.w_IDTIPFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oIDUTECRE_1_21.value==this.w_IDUTECRE)
      this.oPgFrm.Page1.oPag.oIDUTECRE_1_21.value=this.w_IDUTECRE
    endif
    if not(this.oPgFrm.Page1.oPag.oIDDATCRE_1_22.value==this.w_IDDATCRE)
      this.oPgFrm.Page1.oPag.oIDDATCRE_1_22.value=this.w_IDDATCRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_25.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_25.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oIDMODALL_2_1.RadioValue()==this.w_IDMODALL)
      this.oPgFrm.Page2.oPag.oIDMODALL_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIDTIPALL_2_2.value==this.w_IDTIPALL)
      this.oPgFrm.Page2.oPag.oIDTIPALL_2_2.value=this.w_IDTIPALL
    endif
    if not(this.oPgFrm.Page2.oPag.oIDALLPRI_2_3.RadioValue()==this.w_IDALLPRI)
      this.oPgFrm.Page2.oPag.oIDALLPRI_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIDERASEF_2_4.RadioValue()==this.w_IDERASEF)
      this.oPgFrm.Page2.oPag.oIDERASEF_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIDPUBWEB_2_5.RadioValue()==this.w_IDPUBWEB)
      this.oPgFrm.Page2.oPag.oIDPUBWEB_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIDCLAALL_2_6.value==this.w_IDCLAALL)
      this.oPgFrm.Page2.oPag.oIDCLAALL_2_6.value=this.w_IDCLAALL
    endif
    if not(this.oPgFrm.Page2.oPag.oIDWEBAPP_2_8.RadioValue()==this.w_IDWEBAPP)
      this.oPgFrm.Page2.oPag.oIDWEBAPP_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIDFLGFAE_2_9.RadioValue()==this.w_IDFLGFAE)
      this.oPgFrm.Page2.oPag.oIDFLGFAE_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIDCODCBI_2_10.value==this.w_IDCODCBI)
      this.oPgFrm.Page2.oPag.oIDCODCBI_2_10.value=this.w_IDCODCBI
    endif
    if not(this.oPgFrm.Page2.oPag.oIDDATRAG_2_11.value==this.w_IDDATRAG)
      this.oPgFrm.Page2.oPag.oIDDATRAG_2_11.value=this.w_IDDATRAG
    endif
    if not(this.oPgFrm.Page2.oPag.oIDNOMFIL_2_12.value==this.w_IDNOMFIL)
      this.oPgFrm.Page2.oPag.oIDNOMFIL_2_12.value=this.w_IDNOMFIL
    endif
    if not(this.oPgFrm.Page2.oPag.oIDDOCFIR_2_13.value==this.w_IDDOCFIR)
      this.oPgFrm.Page2.oPag.oIDDOCFIR_2_13.value=this.w_IDDOCFIR
    endif
    if not(this.oPgFrm.Page2.oPag.oPATSTD_2_16.value==this.w_PATSTD)
      this.oPgFrm.Page2.oPag.oPATSTD_2_16.value=this.w_PATSTD
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPRAG_2_18.RadioValue()==this.w_TIPRAG)
      this.oPgFrm.Page2.oPag.oTIPRAG_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESTIPAL_2_22.value==this.w_DESTIPAL)
      this.oPgFrm.Page2.oPag.oDESTIPAL_2_22.value=this.w_DESTIPAL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLA_2_23.value==this.w_DESCLA)
      this.oPgFrm.Page2.oPag.oDESCLA_2_23.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page2.oPag.oIDPATALL_2_25.value==this.w_IDPATALL)
      this.oPgFrm.Page2.oPag.oIDPATALL_2_25.value=this.w_IDPATALL
    endif
    if not(this.oPgFrm.Page2.oPag.oIDDATINI_2_27.value==this.w_IDDATINI)
      this.oPgFrm.Page2.oPag.oIDDATINI_2_27.value=this.w_IDDATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oIDDATOBS_2_28.value==this.w_IDDATOBS)
      this.oPgFrm.Page2.oPag.oIDDATOBS_2_28.value=this.w_IDDATOBS
    endif
    if not(this.oPgFrm.Page1.oPag.oIDUTEEXP_1_50.value==this.w_IDUTEEXP)
      this.oPgFrm.Page1.oPag.oIDUTEEXP_1_50.value=this.w_IDUTEEXP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTEEX_1_51.value==this.w_DESUTEEX)
      this.oPgFrm.Page1.oPag.oDESUTEEX_1_51.value=this.w_DESUTEEX
    endif
    if not(this.oPgFrm.Page1.oPag.oORIFIL_1_53.value==this.w_ORIFIL)
      this.oPgFrm.Page1.oPag.oORIFIL_1_53.value=this.w_ORIFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oIDZIPNAM_1_55.value==this.w_IDZIPNAM)
      this.oPgFrm.Page1.oPag.oIDZIPNAM_1_55.value=this.w_IDZIPNAM
    endif
    if not(this.oPgFrm.Page1.oPag.oIDSTASOS_1_57.RadioValue()==this.w_IDSTASOS)
      this.oPgFrm.Page1.oPag.oIDSTASOS_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIDWEBFIL_2_44.value==this.w_IDWEBFIL)
      this.oPgFrm.Page2.oPag.oIDWEBFIL_2_44.value=this.w_IDWEBFIL
    endif
    if not(this.oPgFrm.Page2.oPag.oIDWEBRET_2_45.RadioValue()==this.w_IDWEBRET)
      this.oPgFrm.Page2.oPag.oIDWEBRET_2_45.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIDWEBERR_2_46.value==this.w_IDWEBERR)
      this.oPgFrm.Page2.oPag.oIDWEBERR_2_46.value=this.w_IDWEBERR
    endif
    if not(this.oPgFrm.Page1.oPag.oIDDATZIP_1_62.value==this.w_IDDATZIP)
      this.oPgFrm.Page1.oPag.oIDDATZIP_1_62.value=this.w_IDDATZIP
    endif
    if not(this.oPgFrm.Page2.oPag.oIDWEBAPP_2_51.RadioValue()==this.w_IDWEBAPP)
      this.oPgFrm.Page2.oPag.oIDWEBAPP_2_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIDORACRE_1_68.value==this.w_IDORACRE)
      this.oPgFrm.Page1.oPag.oIDORACRE_1_68.value=this.w_IDORACRE
    endif
    if not(this.oPgFrm.Page1.oPag.oIDTIPBST_1_71.RadioValue()==this.w_IDTIPBST)
      this.oPgFrm.Page1.oPag.oIDTIPBST_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIDFLDEPO_1_73.RadioValue()==this.w_IDFLDEPO)
      this.oPgFrm.Page1.oPag.oIDFLDEPO_1_73.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'PROMINDI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not((! empty(.w_IDORIFIL)  and .w_IDMODALL = 'L') or ( (!empty(.w_IDORIGIN) or ISALT() )  and  .w_IDMODALL='F') or .w_IDMODALL='E' OR .w_IDMODALL='I')
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile confermare! Nessun file � stato allegato.")
          case   ((empty(.w_IDCLADOC)) or not(iif(lower(.cFunction)='load', ( SUBSTR( CHKPECLA( .w_IDCLADOC, i_CODUTE), 2, 1 ) = 'S' ) , .t.)))  and (lower(.cFunction)='load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIDCLADOC_1_4.SetFocus()
            i_bnoObbl = !empty(.w_IDCLADOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per archiviare documenti nella classe selezionata")
          case   (empty(.w_IDDATCRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIDDATCRE_1_22.SetFocus()
            i_bnoObbl = !empty(.w_IDDATCRE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_IDCODCBI))  and not(g_FAEL<>'S' or .w_IDFLGFAE<>'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oIDCODCBI_2_10.SetFocus()
            i_bnoObbl = !empty(.w_IDCODCBI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_IDDATRAG))  and not((lower(.cFunction)<>'load' or .w_TIPRAG='N') or .w_IDMODALL<>'F' Or .w_IDWEBAPP='S')  and (.w_TIPRAG<>'N')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oIDDATRAG_2_11.SetFocus()
            i_bnoObbl = !empty(.w_IDDATRAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_IDNOMFIL))  and (.w_FLEDIT)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oIDNOMFIL_2_12.SetFocus()
            i_bnoObbl = !empty(.w_IDNOMFIL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_IDPATALL))  and not(.w_IDMODALL<>'F' AND NOT ISALT())  and (.w_FLEDIT)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oIDPATALL_2_25.SetFocus()
            i_bnoObbl = !empty(.w_IDPATALL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_IDDATINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oIDDATINI_2_27.SetFocus()
            i_bnoObbl = !empty(.w_IDDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_IDDATOBS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oIDDATOBS_2_28.SetFocus()
            i_bnoObbl = !empty(.w_IDDATOBS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_IDTIPBST) OR EMPTY(.w_IDTIPFIL) OR UPPER(ALLTRIM(.w_IDTIPFIL))$UPPER(ALLTRIM(.w_EST_FIR)) OR (.w_IDTIPBST='G' AND UPPER(ALLTRIM(.w_IDTIPFIL))$(UPPER(ALLTRIM(.w_EST_GEN))+UPPER(ALLTRIM(.w_EST_FIR))+UPPER(ALLTRIM(.w_EST_ZIP)))))  and not(!IsAlt())  and (IsAlt() AND UPPER(ALLTRIM(.w_IDTIPFIL))$(UPPER(ALLTRIM(.w_EST_GEN))+UPPER(ALLTRIM(.w_EST_ZIP))+UPPER(ALLTRIM(.w_EST_FIR))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIDTIPBST_1_71.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare una tipologia allegato conforme all'estensione del file!")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSUT_MAA.CheckForm()
      if i_bres
        i_bres=  .GSUT_MAA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsut_aid
      Private w_oArea
      w_oArea = select()
      * passa sul cursore della movimentazione
      this.GSUT_MAA.MarkPos()
      this.GSUT_MAA.Exec_Select('New_Trs','t_IDTIPATT,t_IDVALDAT,t_IDCODATT,t_IDVALNUM,t_IDVALATT,t_CPROWORD','t_IDCHKOBB=1 and not empty(t_IDDESATT) and ((t_IDTIPATT=3 and empty(t_IDVALDAT)) or (t_IDTIPATT=1 and empty(t_IDVALNUM)) or(t_IDTIPATT=2 and empty(t_IDVALATT)))','','','')
      if used('New_Trs')
        scan
          do case
           case t_IDTIPATT=3 and empty(t_IDVALDAT)
             i_bRes = .F.
             =ah_ErrorMsg("Riga %1 - attributo %2 obbligatorio %0Occorre indicare un valore per l'attributo",48,'',str(t_CPROWORD,4,0), alltrim(t_IDCODATT))
           case t_IDTIPATT=1 and empty(t_IDVALNUM)
             i_bRes = .F.
             =ah_ErrorMsg("Riga %1 - attributo %2 obbligatorio %0Occorre indicare un valore per l'attributo",48,'',str(t_CPROWORD,4,0),alltrim(t_IDCODATT))
           case t_IDTIPATT=2 and empty(t_IDVALATT)
             i_bRes = .F.
             =ah_ErrorMsg("Riga %1 - attributo %2 obbligatorio %0Occorre indicare un valore per l'attributo",48,'', str(t_CPROWORD,4,0), alltrim(t_IDCODATT))
          endcase
        endscan
        *Chiudo il cursore creato
        select('New_Trs')
        use
      endif
      this.GSUT_MAA.Repos()
      
      * Ripristina ambiente
      
      if not empty(w_oArea)
        select (w_oArea)
      endif
      
      if this.w_FLEDIT and Isalt()
         This.Notifyevent('Creafile')
      endif
      
      *Verifico univocit�
      if ! this.w_UNIVOC
        *Blocco il salvataggio
        i_bres=.f.
      endif
      
      *Verifico correttezza nel valore assegnato alla modalit� di archiviazione
      if empty(this.w_IDMODALL)
        *Valore non ammesso, blocco il salvataggio
        =ah_ErrorMsg("Valore non ammesso per la modalit� di archiviazione.%0Impossibile salvare.",48,'')
        i_bres=.f.
      endif
      
      
      *Verifico se � stato modificato il path di archiviazione allegato
      *La copia � svotla prima della scrittura sul database
      *nella convinzione che la scrittura di questo dato sul database
      * non possa nella normalit� dare errori
      * l'alternativa � spostare questo pezzo di codice al termine della transazione con
      * cmq il rischio di salvare il nuovo path sul database e non riuscire a salvare il file
      * nella nuova posizione (questa eventualit� � stata giudicata pi� probabile rispetto alla prima)
      if this.w_univoc and ((upper(this.origfilepath) <> upper(this.w_IDPATALL))  or (upper(this.classeallorig) <> upper(this.w_IDCLAALL)))
         pathmodified = .t.
         local handleerror
          if .not.directory(alltrim( this.w_IDPATALL)) and this.w_IDMODALL='F'
             handleerror= on ('Error')
             on error i_bRes = .f.
             md (this.w_IDPATALL)
             on error &handleerror
             if not i_bRes
              i_cErrorMsg =Ah_MsgFormat("Errore durante la creazione della nuova cartella")
              i_bnoChk=.f.
             Endif
          endif
          if this.w_IDMODALL='F' and !empty(this.origfilepath) and Not Empty(justDrive(this.origfilepath))
            *Copio il file nella nuova destinazione
            fileorig=alltrim(this.origfilepath)+alltrim(this.w_IDNOMFIL)
            TmpPat=alltrim(this.w_IDPATALL)+alltrim(this.w_IDNOMFIL)
            if fileorig <> TmpPat
           	  handleerror= on ('Error')
           	  on error i_bRes = .f.	
      	      copy file (fileorig) to (TmpPat)
          	  *Cancello il file originario
      	      erase (fileorig)
          	  on error &handleerror
      	      if not i_bRes
           	    i_cErrorMsg =Ah_MsgFormat("Errore durante la copia del file")
      	        i_bnoChk=.f.
              endif
            endif
          endif
      endif
      
      *Controllo su inserimento campo IDOGGETT (Descrizione)
      if empty(this.w_IDOGGETT) and !ah_yesno('Descrizione mancante, confermi ugualmente?')
         *Blocco il salvataggio
         i_bres=.f.
         *Non faccio eseguire la ecpQuit() / Blocco l'Esc
         this.chiusura=.f.
      else
         this.chiusura=.t.
      endif
      if i_bres and type('this.Caller')='O' and vartype(this.Caller.pTIPOPER) = 'C' and this.Caller.pTIPOPER == 'D'
        if ah_yesno('Vuoi aprire il documento generato?')
           do GSUT_BBI with this,'INSPRESTAZ'
           do GSUT_BBI with this,'VISUALIZZA'
        else
           do GSUT_BBI with this,'INSPRESTAZ'
        endif
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IDSERIAL = this.w_IDSERIAL
    this.o_IDORIGIN = this.w_IDORIGIN
    this.o_IDMODALL = this.w_IDMODALL
    this.o_IDTIPALL = this.w_IDTIPALL
    this.o_IDALLPRI = this.w_IDALLPRI
    this.o_IDCLAALL = this.w_IDCLAALL
    this.o_IDWEBAPP = this.w_IDWEBAPP
    this.o_IDFLGFAE = this.w_IDFLGFAE
    this.o_IDNOMFIL = this.w_IDNOMFIL
    this.o_IDORIFIL = this.w_IDORIFIL
    this.o_FILEALT = this.w_FILEALT
    * --- GSUT_MAA : Depends On
    this.GSUT_MAA.SaveDependsOn()
    return

  func CanEdit()
    local i_res
    i_res=this.w_IDFLPROV='N'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, indice definitivo impossibile modificare"))
    endif
    return(i_res)
  func CanAdd()
    local i_res
    i_res=.F.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione non consentita!"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=SUBSTR( CHKPECLA( this.w_IDCLADOC, i_CODUTE), 3, 1 ) = 'S' and this.w_IDFLPROV='N'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Non si possiedono i diritti per cancellare il file selezionato o indice definitivo"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsut_aidPag1 as StdContainer
  Width  = 796
  height = 355
  stdWidth  = 796
  stdheight = 355
  resizeXpos=498
  resizeYpos=171
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIDSERIAL_1_2 as StdField with uid="HUVBEAKTJD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_IDSERIAL", cQueryName = "IDSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice identificativo documento",;
    HelpContextID = 232863186,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=170, Left=153, Top=12, InputMask=replicate('X',20)

  add object oIDCLADOC_1_4 as StdField with uid="ASKBVZAQNB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_IDCLADOC", cQueryName = "IDCLADOC",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida o non si possiedono le autorizzazioni per archiviare documenti nella classe selezionata",;
    ToolTipText = "Classe",;
    HelpContextID = 136890935,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=153, Top=42, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="gsut_baf", oKey_1_1="CDCODCLA", oKey_1_2="this.w_IDCLADOC"

  func oIDCLADOC_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (lower(.cFunction)='load')
    endwith
   endif
  endfunc

  func oIDCLADOC_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oIDCLADOC_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIDCLADOC_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oIDCLADOC_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'gsut_baf',"Classi librerie immagini",'',this.parent.oContained
  endproc
  proc oIDCLADOC_1_4.mZoomOnZoom
    local i_obj
    i_obj=gsut_baf()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_IDCLADOC
     i_obj.ecpSave()
  endproc

  add object oDESCCLA_1_7 as StdField with uid="HBHFQDDLOE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCCLA", cQueryName = "DESCCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(70), bMultilanguage =  .f.,;
    HelpContextID = 267335222,;
   bGlobalFont=.t.,;
    Height=21, Width=443, Left=288, Top=42, InputMask=replicate('X',70)

  add object oIDORIGIN_1_11 as StdField with uid="QKJMVWXTHH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_IDORIGIN", cQueryName = "IDORIGIN",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Nome file di origine",;
    HelpContextID = 77728300,;
   bGlobalFont=.t.,;
    Height=21, Width=578, Left=154, Top=71, InputMask=replicate('X',200)

  func oIDORIGIN_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IDMODALL = 'F')
    endwith
   endif
  endfunc

  func oIDORIGIN_1_11.mHide()
    with this.Parent.oContained
      return (.w_IDMODALL='L')
    endwith
  endfunc

  add object oIDOGGETT_1_12 as StdField with uid="VNTZVLSXNH",rtseq=9,rtrep=.f.,;
    cFormVar = "w_IDOGGETT", cQueryName = "IDOGGETT",;
    bObbl = .f. , nPag = 1, value=space(220), bMultilanguage =  .f.,;
    ToolTipText = "Descizione file archiviato",;
    HelpContextID = 154334682,;
   bGlobalFont=.t.,;
    Height=21, Width=578, Left=154, Top=101, InputMask=replicate('X',220)

  add object oID_TESTO_1_13 as StdMemo with uid="UEVAXZHJJM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ID_TESTO", cQueryName = "ID_TESTO",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note",;
    HelpContextID = 119600597,;
   bGlobalFont=.t.,;
    Height=70, Width=578, Left=154, Top=129


  add object oBtn_1_17 as StdButton with uid="FRWQZCDQNV",left=735, top=78, width=48,height=45,;
    CpPicture="BMP\codici.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il documento archiviato";
    , HelpContextID = 135833150;
    , caption='\<Apri file';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSUT_BBI(this.Parent.oContained,"VISUALIZZA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_IDNOMFIL))
      endwith
    endif
  endfunc

  func oBtn_1_17.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_IDNOMFIL) or .w_IDFLPROV='S' )
     endwith
    endif
  endfunc

  add object oIDTIPFIL_1_19 as StdField with uid="CBRWHFFASG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_IDTIPFIL", cQueryName = "IDTIPFIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Tipo file (estensione)",;
    HelpContextID = 87734830,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=154, Top=221, InputMask=replicate('X',10)


  add object oBtn_1_20 as StdButton with uid="XEREXGIALZ",left=154, top=247, width=48,height=45,;
    CpPicture="BMP\SBLOCCA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rendere provvisorio l'indice";
    , HelpContextID = 152888358;
    , caption='\<Sblocca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSUT_BBI(this.Parent.oContained,"SBLOCCA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!(.w_IDFLPROV='S' and .cFunction='Query' and cp_IsAdministrator()))
     endwith
    endif
  endfunc

  add object oIDUTECRE_1_21 as StdField with uid="WCIDMUKHBL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_IDUTECRE", cQueryName = "IDUTECRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha archiviato il file",;
    HelpContextID = 119559627,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=404, Top=221, cSayPict='"9999"', cGetPict='"9999"', cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_IDUTECRE"

  func oIDUTECRE_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oIDDATCRE_1_22 as StdField with uid="OWTFNBRCDR",rtseq=13,rtrep=.f.,;
    cFormVar = "w_IDDATCRE", cQueryName = "IDDATCRE",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di archiviazione",;
    HelpContextID = 133973451,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=251, Top=221

  add object oDESUTE_1_25 as StdField with uid="FEAPYIBFXP",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 99535306,;
   bGlobalFont=.t.,;
    Height=21, Width=277, Left=455, Top=221, InputMask=replicate('X',20)


  add object oObj_1_38 as cp_runprogram with uid="RZTKRYQDKL",left=-11, top=435, width=271,height=23,;
    caption='GSUT_BBI(CARICA)',;
   bGlobalFont=.t.,;
    prg="GSUT_BBI('CARICA')",;
    cEvent = "w_IDCLADOC Changed",;
    nPag=1;
    , HelpContextID = 262584920


  add object oObj_1_39 as cp_runprogram with uid="DEOWBAGCWR",left=357, top=447, width=239,height=23,;
    caption='GSUT_BBI(ARFISICA)',;
   bGlobalFont=.t.,;
    prg="GSUT_BBI('ARFISICA')",;
    cEvent = "Record Inserted",;
    nPag=1;
    , HelpContextID = 6556286


  add object oObj_1_40 as cp_runprogram with uid="SKGZFLKHPY",left=357, top=471, width=239,height=23,;
    caption='GSUT_BBI(CAFISICA)',;
   bGlobalFont=.t.,;
    prg="GSUT_BBI('CAFISICA')",;
    cEvent = "Delete end,Cancella",;
    nPag=1;
    , HelpContextID = 6625406


  add object oObj_1_41 as cp_runprogram with uid="BFBHMULLLV",left=-11, top=459, width=271,height=23,;
    caption='GSUT_BBI(MESSAGGIO)',;
   bGlobalFont=.t.,;
    prg="GSUT_BBI('MESSAGGIO')",;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 28645370

  add object oIDUTEEXP_1_50 as StdField with uid="FPHDGXBATE",rtseq=57,rtrep=.f.,;
    cFormVar = "w_IDUTEEXP", cQueryName = "IDUTEEXP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha esportato il file",;
    HelpContextID = 153114070,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=405, Top=268, cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_IDUTEEXP"

  func oIDUTEEXP_1_50.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_IDUTEEXP))
    endwith
  endfunc

  func oIDUTEEXP_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESUTEEX_1_51 as StdField with uid="WOXMCYYJHY",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DESUTEEX", cQueryName = "DESUTEEX",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 168900238,;
   bGlobalFont=.t.,;
    Height=21, Width=277, Left=455, Top=268, InputMask=replicate('X',20)

  func oDESUTEEX_1_51.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_IDUTEEXP))
    endwith
  endfunc

  add object oORIFIL_1_53 as StdField with uid="NEZATGKSTH",rtseq=59,rtrep=.f.,;
    cFormVar = "w_ORIFIL", cQueryName = "ORIFIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "File di origine",;
    HelpContextID = 5350374,;
   bGlobalFont=.t.,;
    Height=21, Width=578, Left=154, Top=71, InputMask=replicate('X',100)

  func oORIFIL_1_53.mHide()
    with this.Parent.oContained
      return (.w_IDMODALL<>'L')
    endwith
  endfunc

  add object oIDZIPNAM_1_55 as StdField with uid="RXFSKLMTTC",rtseq=60,rtrep=.f.,;
    cFormVar = "w_IDZIPNAM", cQueryName = "IDZIPNAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "percorso e nome file dove il documento � stato esportato",;
    HelpContextID = 46507475,;
   bGlobalFont=.t.,;
    Height=21, Width=561, Left=154, Top=297, InputMask=replicate('X',254)

  func oIDZIPNAM_1_55.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_IDUTEEXP))
    endwith
  endfunc


  add object oIDSTASOS_1_57 as StdTableCombo with uid="UJPVJHMGZI",rtseq=61,rtrep=.f.,left=154,top=324,width=153,height=21, enabled=.f.;
    , ToolTipText = "Stato del documento in SOStitutiva";
    , HelpContextID = 153078311;
    , cFormVar="w_IDSTASOS",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='STATISOS',cKey='STATOVAL',cValue='STATODES',cOrderBy='STATOVAL',xDefault=space(18);
  , bGlobalFont=.t.


  func oIDSTASOS_1_57.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oBtn_1_58 as StdButton with uid="LFBXPECAMW",left=735, top=127, width=48,height=45,;
    CpPicture="bmp\upload.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare upload del documento archiviato";
    , HelpContextID = 167572550;
    , caption='\<Upload';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_58.Click()
      with this.Parent.oContained
        GSUT_BBI(this.Parent.oContained,"UPLOAD")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_58.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_IDNOMFIL))
      endwith
    endif
  endfunc

  func oBtn_1_58.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_IDMODALL<>'I')
     endwith
    endif
  endfunc


  add object oBtn_1_60 as StdButton with uid="ABLEPSGPSI",left=154, top=247, width=48,height=45,;
    CpPicture="BMP\BLOCCA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare l'indice";
    , HelpContextID = 118562070;
    , caption='\<Blocca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_60.Click()
      with this.Parent.oContained
        GSUT_BBI(this.Parent.oContained,"BLOCCA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_60.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!(.w_IDFLPROV='N' and .cFunction='Query'))
     endwith
    endif
  endfunc


  add object oObj_1_61 as cp_calclbl with uid="ZDBEVQXYEF",left=207, top=261, width=112,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 169001958

  add object oIDDATZIP_1_62 as StdField with uid="FQVVCZXRGS",rtseq=67,rtrep=.f.,;
    cFormVar = "w_IDDATZIP", cQueryName = "IDDATZIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    ToolTipText = "Data creazione dello zip",;
    HelpContextID = 17021482,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=590, Top=322

  func oIDDATZIP_1_62.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_IDUTEEXP))
    endwith
  endfunc

  add object oIDORACRE_1_68 as StdField with uid="DVNKYREXSI",rtseq=73,rtrep=.f.,;
    cFormVar = "w_IDORACRE", cQueryName = "IDORACRE",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Ora di archiviazione",;
    HelpContextID = 115209675,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=334, Top=221, InputMask=replicate('X',8)

  func oIDORACRE_1_68.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load' AND EMPTY(NVL(.w_IDORACRE, ' ')))
    endwith
  endfunc


  add object oIDTIPBST_1_71 as StdCombo with uid="HKOQVOCEJP",value=11,rtseq=86,rtrep=.f.,left=154,top=324,width=136,height=22;
    , ToolTipText = "Tipologia allegato per busta";
    , HelpContextID = 113591770;
    , cFormVar="w_IDTIPBST",RowSource=""+"Atto principale,"+"Dati atto,"+"Allegato semplice,"+"Nota iscrizione,"+"Procura alle liti,"+"Ricevuta telematica,"+"Rel. notificazione,"+"Scans. atto notificato,"+"Msg PEC tra avvocati,"+"Ricev.avv.consegna,"+"", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Selezionare una tipologia allegato conforme all'estensione del file!";
  , bGlobalFont=.t.


  func oIDTIPBST_1_71.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,'G',;
    iif(this.value =4,'N',;
    iif(this.value =5,'L',;
    iif(this.value =6,'T',;
    iif(this.value =7,'X',;
    iif(this.value =8,'S',;
    iif(this.value =9,'A',;
    iif(this.value =10,'R',;
    iif(this.value =11,' ',;
    space(1)))))))))))))
  endfunc
  func oIDTIPBST_1_71.GetRadio()
    this.Parent.oContained.w_IDTIPBST = this.RadioValue()
    return .t.
  endfunc

  func oIDTIPBST_1_71.SetRadio()
    this.Parent.oContained.w_IDTIPBST=trim(this.Parent.oContained.w_IDTIPBST)
    this.value = ;
      iif(this.Parent.oContained.w_IDTIPBST=='P',1,;
      iif(this.Parent.oContained.w_IDTIPBST=='D',2,;
      iif(this.Parent.oContained.w_IDTIPBST=='G',3,;
      iif(this.Parent.oContained.w_IDTIPBST=='N',4,;
      iif(this.Parent.oContained.w_IDTIPBST=='L',5,;
      iif(this.Parent.oContained.w_IDTIPBST=='T',6,;
      iif(this.Parent.oContained.w_IDTIPBST=='X',7,;
      iif(this.Parent.oContained.w_IDTIPBST=='S',8,;
      iif(this.Parent.oContained.w_IDTIPBST=='A',9,;
      iif(this.Parent.oContained.w_IDTIPBST=='R',10,;
      iif(this.Parent.oContained.w_IDTIPBST=='',11,;
      0)))))))))))
  endfunc

  func oIDTIPBST_1_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt() AND UPPER(ALLTRIM(.w_IDTIPFIL))$(UPPER(ALLTRIM(.w_EST_GEN))+UPPER(ALLTRIM(.w_EST_ZIP))+UPPER(ALLTRIM(.w_EST_FIR))))
    endwith
   endif
  endfunc

  func oIDTIPBST_1_71.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  func oIDTIPBST_1_71.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_IDTIPBST) OR EMPTY(.w_IDTIPFIL) OR UPPER(ALLTRIM(.w_IDTIPFIL))$UPPER(ALLTRIM(.w_EST_FIR)) OR (.w_IDTIPBST='G' AND UPPER(ALLTRIM(.w_IDTIPFIL))$(UPPER(ALLTRIM(.w_EST_GEN))+UPPER(ALLTRIM(.w_EST_FIR))+UPPER(ALLTRIM(.w_EST_ZIP)))))
    endwith
    return bRes
  endfunc

  add object oIDFLDEPO_1_73 as StdCheck with uid="SJSXTUJEJK",rtseq=87,rtrep=.f.,left=306, top=324, caption="Documento depositato",;
    ToolTipText = "Documento depositato",;
    HelpContextID = 116955691,;
    cFormVar="w_IDFLDEPO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIDFLDEPO_1_73.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oIDFLDEPO_1_73.GetRadio()
    this.Parent.oContained.w_IDFLDEPO = this.RadioValue()
    return .t.
  endfunc

  func oIDFLDEPO_1_73.SetRadio()
    this.Parent.oContained.w_IDFLDEPO=trim(this.Parent.oContained.w_IDFLDEPO)
    this.value = ;
      iif(this.Parent.oContained.w_IDFLDEPO=='S',1,;
      0)
  endfunc

  func oIDFLDEPO_1_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oIDFLDEPO_1_73.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="GTSXQIGKIV",Visible=.t., Left=4, Top=42,;
    Alignment=1, Width=145, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (g_DOCM <> 'S')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="JUAAIODMXZ",Visible=.t., Left=24, Top=13,;
    Alignment=1, Width=125, Height=18,;
    Caption="Seriale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="LVYXUTTFCZ",Visible=.t., Left=16, Top=42,;
    Alignment=1, Width=133, Height=18,;
    Caption="Classe libreria immag.:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (g_DOCM = 'S')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="ELQJIVNQME",Visible=.t., Left=3, Top=125,;
    Alignment=1, Width=147, Height=18,;
    Caption="Note aggiuntive:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="ZFJUVWATXH",Visible=.t., Left=54, Top=102,;
    Alignment=1, Width=96, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="YFPPCAOAUL",Visible=.t., Left=1, Top=72,;
    Alignment=1, Width=149, Height=18,;
    Caption="Nome file originale:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_IDMODALL='E' OR .w_IDMODALL='I')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="MRVITEFHCX",Visible=.t., Left=25, Top=72,;
    Alignment=1, Width=125, Height=18,;
    Caption="Indice archiviazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (.w_IDMODALL<>'E' AND .w_IDMODALL<>'I')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="SGUNMNDGTW",Visible=.t., Left=403, Top=202,;
    Alignment=0, Width=123, Height=18,;
    Caption="Utente archiviazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="PQWGEYQVAX",Visible=.t., Left=251, Top=202,;
    Alignment=0, Width=120, Height=18,;
    Caption="Data archiviazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="TALQXJVWZI",Visible=.t., Left=154, Top=202,;
    Alignment=0, Width=74, Height=18,;
    Caption="Tipo file"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="VQYBKGQCMT",Visible=.t., Left=3, Top=323,;
    Alignment=1, Width=147, Height=18,;
    Caption="Stato SOS:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="LLDCQRXTQD",Visible=.t., Left=403, Top=249,;
    Alignment=0, Width=278, Height=18,;
    Caption="Utente proprietario esportazione"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_IDUTEEXP))
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="MSICULBQFD",Visible=.t., Left=343, Top=14,;
    Alignment=0, Width=386, Height=18,;
    Caption="Documento esportato per aggiornamento"    , forecolor=RGB(255,0,0);
  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_IDUTEEXP))
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="QNBDJOORCV",Visible=.t., Left=3, Top=300,;
    Alignment=1, Width=147, Height=18,;
    Caption="Archivio d'esportazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_IDUTEEXP))
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="NGIMNOMSFH",Visible=.t., Left=455, Top=323,;
    Alignment=1, Width=133, Height=18,;
    Caption="Data creazione archivio:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_IDUTEEXP))
    endwith
  endfunc

  add object oStr_1_72 as StdString with uid="PZZCQGNBBR",Visible=.t., Left=3, Top=323,;
    Alignment=1, Width=147, Height=18,;
    Caption="Tipologia per busta:"  ;
  , bGlobalFont=.t.

  func oStr_1_72.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc
enddefine
define class tgsut_aidPag2 as StdContainer
  Width  = 796
  height = 355
  stdWidth  = 796
  stdheight = 355
  resizeXpos=401
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oIDMODALL_2_1 as StdCombo with uid="ZLEVCIMQZX",rtseq=15,rtrep=.f.,left=164,top=16,width=194,height=21, enabled=.f.;
    , tabstop=.f.;
    , ToolTipText = "Tipo archiviazione: F=copia file, L=collegamento, E=Extended document server";
    , HelpContextID = 183839278;
    , cFormVar="w_IDMODALL",RowSource=""+"Collegamento,"+"Copia file,"+"Extended document server,"+"Infinity D.M.S.", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oIDMODALL_2_1.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'F',;
    iif(this.value =3,'E',;
    iif(this.value =4,'I',;
    space(1))))))
  endfunc
  func oIDMODALL_2_1.GetRadio()
    this.Parent.oContained.w_IDMODALL = this.RadioValue()
    return .t.
  endfunc

  func oIDMODALL_2_1.SetRadio()
    this.Parent.oContained.w_IDMODALL=trim(this.Parent.oContained.w_IDMODALL)
    this.value = ;
      iif(this.Parent.oContained.w_IDMODALL=='L',1,;
      iif(this.Parent.oContained.w_IDMODALL=='F',2,;
      iif(this.Parent.oContained.w_IDMODALL=='E',3,;
      iif(this.Parent.oContained.w_IDMODALL=='I',4,;
      0))))
  endfunc

  add object oIDTIPALL_2_2 as StdField with uid="RYJDCLQAYR",rtseq=16,rtrep=.f.,;
    cFormVar = "w_IDTIPALL", cQueryName = "IDTIPALL",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia dell'allegato",;
    HelpContextID = 171620910,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=164, Top=51, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_IDTIPALL"

  func oIDTIPALL_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PATHTIP<>'S' and .w_PATHCLA <> 'S')
    endwith
   endif
  endfunc

  func oIDTIPALL_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
      if .not. empty(.w_IDCLAALL)
        bRes2=.link_2_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oIDTIPALL_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIDTIPALL_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_ALLE','*','TACODICE',cp_AbsName(this.parent,'oIDTIPALL_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"Tipologie allegato",'',this.parent.oContained
  endproc
  proc oIDTIPALL_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TACODICE=this.parent.oContained.w_IDTIPALL
     i_obj.ecpSave()
  endproc

  add object oIDALLPRI_2_3 as StdCheck with uid="HZJJZVZCYN",rtseq=17,rtrep=.f.,left=483, top=48, caption="Allegato principale",;
    ToolTipText = "Se attivo l'allegato � quello principale",;
    HelpContextID = 75961807,;
    cFormVar="w_IDALLPRI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oIDALLPRI_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oIDALLPRI_2_3.GetRadio()
    this.Parent.oContained.w_IDALLPRI = this.RadioValue()
    return .t.
  endfunc

  func oIDALLPRI_2_3.SetRadio()
    this.Parent.oContained.w_IDALLPRI=trim(this.Parent.oContained.w_IDALLPRI)
    this.value = ;
      iif(this.Parent.oContained.w_IDALLPRI=='S',1,;
      0)
  endfunc

  add object oIDERASEF_2_4 as StdCheck with uid="CURFQFWQFV",rtseq=18,rtrep=.f.,left=652, top=15, caption="Cancella file", enabled=.f.,;
    ToolTipText = "Se attivo il file viene cancellato dopo la pubblicazione",;
    HelpContextID = 115168716,;
    cFormVar="w_IDERASEF", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oIDERASEF_2_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIDERASEF_2_4.GetRadio()
    this.Parent.oContained.w_IDERASEF = this.RadioValue()
    return .t.
  endfunc

  func oIDERASEF_2_4.SetRadio()
    this.Parent.oContained.w_IDERASEF=trim(this.Parent.oContained.w_IDERASEF)
    this.value = ;
      iif(this.Parent.oContained.w_IDERASEF=='S',1,;
      0)
  endfunc

  func oIDERASEF_2_4.mHide()
    with this.Parent.oContained
      return (.w_IDWEBAPP='N')
    endwith
  endfunc

  add object oIDPUBWEB_2_5 as StdCheck with uid="ICQXXWPZSP",rtseq=19,rtrep=.f.,left=652, top=48, caption="Pubblica su web",;
    HelpContextID = 183567816,;
    cFormVar="w_IDPUBWEB", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oIDPUBWEB_2_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIDPUBWEB_2_5.GetRadio()
    this.Parent.oContained.w_IDPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oIDPUBWEB_2_5.SetRadio()
    this.Parent.oContained.w_IDPUBWEB=trim(this.Parent.oContained.w_IDPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_IDPUBWEB=='S',1,;
      0)
  endfunc

  func oIDPUBWEB_2_5.mHide()
    with this.Parent.oContained
      return (g_CPIN='S')
    endwith
  endfunc

  add object oIDCLAALL_2_6 as StdField with uid="KNDULROQTL",rtseq=20,rtrep=.f.,;
    cFormVar = "w_IDCLAALL", cQueryName = "IDCLAALL",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe allegato associata alla tipologia",;
    HelpContextID = 187222574,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=164, Top=76, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_IDTIPALL", oKey_2_1="TACODCLA", oKey_2_2="this.w_IDCLAALL"

  func oIDCLAALL_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PATHCLA <> 'S' and .w_EDITABLE)
    endwith
   endif
  endfunc

  func oIDCLAALL_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oIDCLAALL_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIDCLAALL_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLA_ALLE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStrODBC(this.Parent.oContained.w_IDTIPALL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStr(this.Parent.oContained.w_IDTIPALL)
    endif
    do cp_zoom with 'CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(this.parent,'oIDCLAALL_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"Classi allegato",'',this.parent.oContained
  endproc
  proc oIDCLAALL_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TACODICE=w_IDTIPALL
     i_obj.w_TACODCLA=this.parent.oContained.w_IDCLAALL
     i_obj.ecpSave()
  endproc


  add object oIDWEBAPP_2_8 as StdCombo with uid="GHELIWOPQE",rtseq=22,rtrep=.f.,left=479,top=16,width=161,height=21;
    , ToolTipText = "Selezionare la modalit� di pubblicazione sul web";
    , HelpContextID = 186550826;
    , cFormVar="w_IDWEBAPP",RowSource=""+"Archiviazione Web,"+"Archiviazione Std,"+"Archiviazione Std e Web", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oIDWEBAPP_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oIDWEBAPP_2_8.GetRadio()
    this.Parent.oContained.w_IDWEBAPP = this.RadioValue()
    return .t.
  endfunc

  func oIDWEBAPP_2_8.SetRadio()
    this.Parent.oContained.w_IDWEBAPP=trim(this.Parent.oContained.w_IDWEBAPP)
    this.value = ;
      iif(this.Parent.oContained.w_IDWEBAPP=='S',1,;
      iif(this.Parent.oContained.w_IDWEBAPP=='N',2,;
      iif(this.Parent.oContained.w_IDWEBAPP=='A',3,;
      0)))
  endfunc

  func oIDWEBAPP_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((IsAlt() and g_CPIN='S' and g_PRZI='S') OR .w_FLEDIT)
    endwith
   endif
  endfunc

  func oIDWEBAPP_2_8.mHide()
    with this.Parent.oContained
      return (g_CPIN#'S')
    endwith
  endfunc

  add object oIDFLGFAE_2_9 as StdCheck with uid="QCMLGPUXBS",rtseq=23,rtrep=.f.,left=483, top=74, caption="Formato elettronico",;
    ToolTipText = "Se attivo indice utilizzato nel formato elettronico",;
    HelpContextID = 171402699,;
    cFormVar="w_IDFLGFAE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oIDFLGFAE_2_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIDFLGFAE_2_9.GetRadio()
    this.Parent.oContained.w_IDFLGFAE = this.RadioValue()
    return .t.
  endfunc

  func oIDFLGFAE_2_9.SetRadio()
    this.Parent.oContained.w_IDFLGFAE=trim(this.Parent.oContained.w_IDFLGFAE)
    this.value = ;
      iif(this.Parent.oContained.w_IDFLGFAE=='S',1,;
      0)
  endfunc

  func oIDFLGFAE_2_9.mHide()
    with this.Parent.oContained
      return (g_FAEL<>'S')
    endwith
  endfunc

  add object oIDCODCBI_2_10 as StdField with uid="MLNWHYIVST",rtseq=24,rtrep=.f.,;
    cFormVar = "w_IDCODCBI", cQueryName = "IDCODCBI",;
    bObbl = .t. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Estensione CBI",;
    HelpContextID = 118109647,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=734, Top=75, InputMask=replicate('X',4)

  func oIDCODCBI_2_10.mHide()
    with this.Parent.oContained
      return (g_FAEL<>'S' or .w_IDFLGFAE<>'S')
    endwith
  endfunc

  add object oIDDATRAG_2_11 as StdField with uid="XZHFKMBTFD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_IDDATRAG", cQueryName = "IDDATRAG",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento per raggruppamento (composizione cartella di archiviazione)",;
    HelpContextID = 117196237,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=699, Top=104

  func oIDDATRAG_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPRAG<>'N')
    endwith
   endif
  endfunc

  func oIDDATRAG_2_11.mHide()
    with this.Parent.oContained
      return ((lower(.cFunction)<>'load' or .w_TIPRAG='N') or .w_IDMODALL<>'F' Or .w_IDWEBAPP='S')
    endwith
  endfunc

  add object oIDNOMFIL_2_12 as StdField with uid="IXSYWZMIXC",rtseq=26,rtrep=.f.,;
    cFormVar = "w_IDNOMFIL", cQueryName = "IDORIGIN,IDNOMFIL",;
    bObbl = .t. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Nome file archiviato",;
    HelpContextID = 90511918,;
   bGlobalFont=.t.,;
    Height=21, Width=592, Left=164, Top=157, InputMask=replicate('X',100)

  func oIDNOMFIL_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLEDIT)
    endwith
   endif
  endfunc

  add object oIDDOCFIR_2_13 as StdField with uid="MCZUYMSDLE",rtseq=27,rtrep=.f.,;
    cFormVar = "w_IDDOCFIR", cQueryName = "IDDOCFIR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(220), bMultilanguage =  .f.,;
    ToolTipText = "Nome documento firmato",;
    HelpContextID = 101038632,;
   bGlobalFont=.t.,;
    Height=21, Width=592, Left=164, Top=184, InputMask=replicate('X',220)

  func oIDDOCFIR_2_13.mHide()
    with this.Parent.oContained
      return (.w_FIRMDIG # 'S')
    endwith
  endfunc

  add object oPATSTD_2_16 as StdField with uid="QGMAPFHHII",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PATSTD", cQueryName = "PATSTD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso standard di memorizzazione (esclusi raggruppamenti)",;
    HelpContextID = 116440330,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=164, Top=103, InputMask=replicate('X',254)

  func oPATSTD_2_16.mHide()
    with this.Parent.oContained
      return (.w_IDMODALL<>'F' Or .w_IDWEBAPP='S')
    endwith
  endfunc


  add object oTIPRAG_2_18 as StdCombo with uid="WJZJMHDSIM",rtseq=29,rtrep=.f.,left=564,top=104,width=117,height=21, enabled=.f.;
    , ToolTipText = "Tipo raggruppamento";
    , HelpContextID = 86111434;
    , cFormVar="w_TIPRAG",RowSource=""+"Nessuno,"+"Settimana,"+"Mese,"+"Trimestre,"+"Quadrimestre,"+"Semestre,"+"Anno", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPRAG_2_18.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'W',;
    iif(this.value =3,'M',;
    iif(this.value =4,'T',;
    iif(this.value =5,'Q',;
    iif(this.value =6,'S',;
    iif(this.value =7,'A',;
    space(1)))))))))
  endfunc
  func oTIPRAG_2_18.GetRadio()
    this.Parent.oContained.w_TIPRAG = this.RadioValue()
    return .t.
  endfunc

  func oTIPRAG_2_18.SetRadio()
    this.Parent.oContained.w_TIPRAG=trim(this.Parent.oContained.w_TIPRAG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPRAG=='N',1,;
      iif(this.Parent.oContained.w_TIPRAG=='W',2,;
      iif(this.Parent.oContained.w_TIPRAG=='M',3,;
      iif(this.Parent.oContained.w_TIPRAG=='T',4,;
      iif(this.Parent.oContained.w_TIPRAG=='Q',5,;
      iif(this.Parent.oContained.w_TIPRAG=='S',6,;
      iif(this.Parent.oContained.w_TIPRAG=='A',7,;
      0)))))))
  endfunc

  func oTIPRAG_2_18.mHide()
    with this.Parent.oContained
      return (.w_IDMODALL<>'F' Or .w_IDWEBAPP='S')
    endwith
  endfunc

  add object oDESTIPAL_2_22 as StdField with uid="NQGRLVRCPD",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESTIPAL", cQueryName = "DESTIPAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 73414274,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=223, Top=51, InputMask=replicate('X',50)

  add object oDESCLA_2_23 as StdField with uid="GESEGTXLVJ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 176212426,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=223, Top=76, InputMask=replicate('X',50)

  add object oIDPATALL_2_25 as StdField with uid="SCIZQDUQUE",rtseq=32,rtrep=.f.,;
    cFormVar = "w_IDPATALL", cQueryName = "IDPATALL",;
    bObbl = .t. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Path assoluto dell'allegato",;
    HelpContextID = 167967278,;
   bGlobalFont=.t.,;
    Height=21, Width=592, Left=164, Top=130, InputMask=replicate('X',254)

  func oIDPATALL_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLEDIT)
    endwith
   endif
  endfunc

  func oIDPATALL_2_25.mHide()
    with this.Parent.oContained
      return (.w_IDMODALL<>'F' AND NOT ISALT())
    endwith
  endfunc

  add object oIDDATINI_2_27 as StdField with uid="BEYUIBIMRM",rtseq=33,rtrep=.f.,;
    cFormVar = "w_IDDATINI", cQueryName = "IDDATINI",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 33798705,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=164, Top=304

  add object oIDDATOBS_2_28 as StdField with uid="DSXEPZTYUN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_IDDATOBS", cQueryName = "IDDATOBS",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine validit�",;
    HelpContextID = 66864601,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=283, Top=304


  add object oObj_2_31 as cp_runprogram with uid="MCFTSNNHJL",left=0, top=375, width=312,height=19,;
    caption='GSUT_BCT(w_IDTIPALL,C)',;
   bGlobalFont=.t.,;
    prg="GSUT_BCT(w_IDTIPALL,'C')",;
    cEvent = "w_IDTIPALL Changed",;
    nPag=2;
    , HelpContextID = 229695130

  add object oIDWEBFIL_2_44 as StdField with uid="GSHELMFCPD",rtseq=63,rtrep=.f.,;
    cFormVar = "w_IDWEBFIL", cQueryName = "IDWEBFIL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome file Web Application",;
    HelpContextID = 102664750,;
   bGlobalFont=.t.,;
    Height=21, Width=592, Left=164, Top=210, InputMask=replicate('X',254)

  func oIDWEBFIL_2_44.mHide()
    with this.Parent.oContained
      return (.w_IDWEBAPP<>'S' AND .w_IDWEBAPP<>'A')
    endwith
  endfunc


  add object oIDWEBRET_2_45 as StdCombo with uid="FWPBBTBWKJ",value=1,rtseq=64,rtrep=.f.,left=164,top=240,width=144,height=21, enabled=.f.;
    , ToolTipText = "Stato conferma archiviazione web (S=inviato, E=errori nell'invio, Blank=da inviare)";
    , HelpContextID = 98661850;
    , cFormVar="w_IDWEBRET",RowSource=""+"Da inviare,"+"Archiviato,"+"Errore di archiviazione", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oIDWEBRET_2_45.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    space(15)))))
  endfunc
  func oIDWEBRET_2_45.GetRadio()
    this.Parent.oContained.w_IDWEBRET = this.RadioValue()
    return .t.
  endfunc

  func oIDWEBRET_2_45.SetRadio()
    this.Parent.oContained.w_IDWEBRET=trim(this.Parent.oContained.w_IDWEBRET)
    this.value = ;
      iif(this.Parent.oContained.w_IDWEBRET=='',1,;
      iif(this.Parent.oContained.w_IDWEBRET=='S',2,;
      iif(this.Parent.oContained.w_IDWEBRET=='E',3,;
      0)))
  endfunc

  func oIDWEBRET_2_45.mHide()
    with this.Parent.oContained
      return (.w_IDMODALL<>'I' AND .w_IDWEBRET<>'E')
    endwith
  endfunc

  add object oIDWEBERR_2_46 as StdMemo with uid="ZVCZJPVRTI",rtseq=65,rtrep=.f.,;
    cFormVar = "w_IDWEBERR", cQueryName = "IDWEBERR",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Messaggio di errore dell'archiviazione web",;
    HelpContextID = 148993496,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=445, Left=312, Top=236, readonly = .t.

  func oIDWEBERR_2_46.mHide()
    with this.Parent.oContained
      return (.w_IDWEBRET<>'E')
    endwith
  endfunc


  add object oObj_2_48 as cp_runprogram with uid="MKPCCFMJSF",left=276, top=429, width=185,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSUT_BCT(w_IDTIPALL,'D')",;
    cEvent = "w_IDCLAALL Changed",;
    nPag=2;
    , HelpContextID = 169001958


  add object oIDWEBAPP_2_51 as StdCombo with uid="SJEJMATVSW",rtseq=68,rtrep=.f.,left=479,top=16,width=161,height=21, enabled=.f.;
    , ToolTipText = "Se attivo l'allegato viene pubblicato sul web";
    , HelpContextID = 186550826;
    , cFormVar="w_IDWEBAPP",RowSource=""+"Da trasferire,"+"In trasferimento,"+"Trasferito", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oIDWEBAPP_2_51.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func oIDWEBAPP_2_51.GetRadio()
    this.Parent.oContained.w_IDWEBAPP = this.RadioValue()
    return .t.
  endfunc

  func oIDWEBAPP_2_51.SetRadio()
    this.Parent.oContained.w_IDWEBAPP=trim(this.Parent.oContained.w_IDWEBAPP)
    this.value = ;
      iif(this.Parent.oContained.w_IDWEBAPP=='N',1,;
      iif(this.Parent.oContained.w_IDWEBAPP=='D',2,;
      iif(this.Parent.oContained.w_IDWEBAPP=='X',3,;
      0)))
  endfunc

  func oIDWEBAPP_2_51.mHide()
    with this.Parent.oContained
      return (g_DMIP#'S')
    endwith
  endfunc

  add object oStr_2_14 as StdString with uid="IUHVLLXJHQ",Visible=.t., Left=1, Top=160,;
    Alignment=1, Width=159, Height=18,;
    Caption="Nome file archiviato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="QVLFYLUYRX",Visible=.t., Left=59, Top=103,;
    Alignment=1, Width=101, Height=18,;
    Caption="Percorso std:"  ;
  , bGlobalFont=.t.

  func oStr_2_15.mHide()
    with this.Parent.oContained
      return (.w_IDMODALL<>'F'  Or .w_IDWEBAPP='S')
    endwith
  endfunc

  add object oStr_2_17 as StdString with uid="SUPLQUSCIL",Visible=.t., Left=489, Top=104,;
    Alignment=1, Width=73, Height=18,;
    Caption="Raggrupp.:"  ;
  , bGlobalFont=.t.

  func oStr_2_17.mHide()
    with this.Parent.oContained
      return (.w_IDMODALL<>'F' Or .w_IDWEBAPP='S')
    endwith
  endfunc

  add object oStr_2_19 as StdString with uid="KEBEDIBBMP",Visible=.t., Left=8, Top=16,;
    Alignment=1, Width=152, Height=18,;
    Caption="Modalit� archiviazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="AAEVWYLGWM",Visible=.t., Left=47, Top=53,;
    Alignment=1, Width=113, Height=18,;
    Caption="Tipologia allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="FOFKAPFTUY",Visible=.t., Left=61, Top=78,;
    Alignment=1, Width=99, Height=18,;
    Caption="Classe allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="FAMMXNZGXH",Visible=.t., Left=32, Top=131,;
    Alignment=1, Width=128, Height=18,;
    Caption="Percorso assoluto:"  ;
  , bGlobalFont=.t.

  func oStr_2_24.mHide()
    with this.Parent.oContained
      return (.w_IDMODALL<>'F' AND NOT ISALT())
    endwith
  endfunc

  add object oStr_2_26 as StdString with uid="DWORWGPSDJ",Visible=.t., Left=637, Top=77,;
    Alignment=1, Width=94, Height=18,;
    Caption="Estensione CBI:"  ;
  , bGlobalFont=.t.

  func oStr_2_26.mHide()
    with this.Parent.oContained
      return (g_FAEL<>'S' or .w_IDFLGFAE<>'S')
    endwith
  endfunc

  add object oStr_2_29 as StdString with uid="HFPPCKEDWC",Visible=.t., Left=167, Top=285,;
    Alignment=0, Width=99, Height=18,;
    Caption="Inizio validit�"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="QUQYXLRLVG",Visible=.t., Left=283, Top=285,;
    Alignment=0, Width=98, Height=18,;
    Caption="Fine validit�"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="FBWBGLTSZW",Visible=.t., Left=1, Top=188,;
    Alignment=1, Width=159, Height=18,;
    Caption="Nome file firmato:"  ;
  , bGlobalFont=.t.

  func oStr_2_39.mHide()
    with this.Parent.oContained
      return (.w_FIRMDIG # 'S')
    endwith
  endfunc

  add object oStr_2_43 as StdString with uid="KPQPIXABIA",Visible=.t., Left=427, Top=16,;
    Alignment=1, Width=49, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_2_43.mHide()
    with this.Parent.oContained
      return (g_CPIN#'S' and g_DMIP#'S')
    endwith
  endfunc

  add object oStr_2_47 as StdString with uid="NTLHJUSXYM",Visible=.t., Left=1, Top=214,;
    Alignment=1, Width=159, Height=18,;
    Caption="Nome file Web:"  ;
  , bGlobalFont=.t.

  func oStr_2_47.mHide()
    with this.Parent.oContained
      return (.w_IDWEBAPP<>'S' AND .w_IDWEBAPP<>'A')
    endwith
  endfunc

  add object oStr_2_49 as StdString with uid="VMYLKWCPCQ",Visible=.t., Left=49, Top=240,;
    Alignment=1, Width=112, Height=18,;
    Caption="Stato archiv. web:"  ;
  , bGlobalFont=.t.

  func oStr_2_49.mHide()
    with this.Parent.oContained
      return (.w_IDMODALL<>'I' AND .w_IDWEBRET<>'E')
    endwith
  endfunc
enddefine
define class tgsut_aidPag3 as StdContainer
  Width  = 796
  height = 355
  stdWidth  = 796
  stdheight = 355
  resizeXpos=306
  resizeYpos=141
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="HSNPGHVISJ",left=-1, top=2, width=797, height=353, bOnScreen=.t.;


  func oLinkPC_3_1.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DOCM <> 'S')
     endwith
    endif
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsut_maa",lower(this.oContained.GSUT_MAA.class))=0
        this.oContained.GSUT_MAA.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_aid','PROMINDI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IDSERIAL=PROMINDI.IDSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_aid
procedure MODIFICA(obj)
  * modifico i campi e gestisco l'allegato
  if obj.cFunction = 'Edit' and (obj.w_IDMODALL='E' or obj.oldmodall='E')
    *Caso non gestito ripristino la modalit� iniziale
    obj.w_IDMODALL = obj.oldmodall
    ah_ErrorMsg('Cambio di modalit� non consentito',48,' ')
    return
  endif
  obj.NotifyEvent('Univocit�')
  if obj.w_UNIVOC
   local pathstd,pathfile,obj2,keyrec
   ErrorHandler = on('ERROR')
   on error ah_ErrorMsg('Si � verificato un errore durante il cambio della modalit�',16,' ')
   do case
     case obj.oldmodall='F' and obj.w_IDMODALL='L'
       * In w_IDORIFIL ho gi� il percorso assoluto ed il nome del file originario
       pathfile = "'"+alltrim(obj.w_IDPATALL)+alltrim(obj.oldnomfil)+"'"
       * Cancello la copia del file originario
       erase &pathfile
       obj.w_IDPATALL = ' '
       obj.w_IDORIGIN = ' '
     case obj.oldmodall='L' and obj.w_IDMODALL='F'
       If isalt()
         pathstd = DCMPATAR(iif(Not empty(obj.w_PATHPRA),obj.w_PATHPRA,obj.w_PATSTD), obj.w_TIPRAG, obj.w_IDDATRAG,obj.w_IDCLADOC,'','','','','','',.t.)
        else
         pathstd =DCMPATAR(sys(5)+'\', obj.w_TIPRAG, obj.w_IDDATRAG,obj.w_IDCLADOC)
        endif
       obj.w_IDPATALL = pathstd
       if not directory(alltrim(pathstd))
         md(pathstd)
       endif
       * Ricavo il nome del file
         obj.w_IDORIGIN = substr(obj.w_IDORIFIL,rat('\',obj.w_IDORIFIL)+1)
         if IsAlt() 
           if obj.w_FLGDES = 'S'
             keyrec = alltrim(obj.GSUT_MAA.w_IDVALATT)
             * Inizio lettura descrizione chiave record
             descrec = ' '
             gsut_blr(.null.,keyrec,'CAN_TIER')
             * Fine lettura descrizione chiave record
             pathfile= alltrim(pathstd + obj.w_IDSERIAL+'_'+alltrim(obj.w_ALIASFILE)+'_'+alltrim(descrec))+'.'+JUSTEXT(obj.w_IDORIGIN)
           else            
             pathfile= alltrim(pathstd + obj.w_IDSERIAL+'_'+alltrim(obj.w_ALIASFILE)+'_'+obj.w_IDORIGIN)
           endif
            if !obj.w_FLEDIT
           obj.w_FILEALT = JustFname(pathfile)
            Endif
        else
         pathfile= alltrim(pathstd + obj.w_IDSERIAL+'_'+obj.w_IDORIGIN)
       endif
       if !obj.w_FLEDIT
       copy file (obj.w_IDORIFIL) to (pathfile)
       endif
       * Assegno le chioccioline a w_IDORIFIL per non fare segnalare un messaggio di errore incogruente
       obj.w_IDORIFIL = '@@@'
   endcase
   on error &ErrorHandler
  endif
  * Rendo non editabile la combo di selezione della modalit� di archiviazione solo nei casi di 'copia' o 'collegamento'
  if obj.w_IDMODALL  $ 'F-L'
    obj2 = obj.getctrl('w_IDMODALL')
    obj2.enabled=.F.
    obj2=.null.
    * rendo non editabile la classe allegato al cambio di modalit� di archiviazione
    obj.w_EDITABLE = .F.
  endif
Endproc

Procedure ALLEGPRIN(Obj)
 if Obj.w_IDALLPRI='S'
   do vq_exec with 'QUERY\ALGPRI',Obj,'PRINC','',.f.,.t.
   Select PRINC
   if Reccount() > 0
     Obj.w_IDALLPRI='N'
     =Ah_ErrorMsg("Esiste gi� un allegato principale!",48,'')
   endif
   use
  endif
Endproc
* --- Fine Area Manuale
