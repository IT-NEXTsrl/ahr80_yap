* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bke                                                        *
*              Generazione file per bilancio esterno                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-11-26                                                      *
* Last revis.: 2014-01-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bke",oParentObject)
return(i_retval)

define class tgscg_bke as StdBatch
  * --- Local variables
  w_TIPCON = space(1)
  w_ANRISESE = space(1)
  w_CODICE = space(15)
  w_SEGNO = space(1)
  w_CODDITTA = space(4)
  w_CONTO = space(15)
  w_CODFISCALE = space(16)
  w_PARTITAIVA = space(20)
  w_GESTIONALE = space(20)
  w_CODESE = space(4)
  hFILE = 0
  w_CPATH = space(150)
  w_APERTURA = .f.
  w_CHIUSURA = .f.
  w_CODICECONTO = space(30)
  w_DESCRICONTO = space(30)
  w_IMPORTO = 0
  w_CORPO = space(200)
  w_NIMPORTO = space(20)
  w_CIMPORTO = space(50)
  w_OKGEN = 0
  w_TESTATA = space(200)
  w_SALDO = 0
  * --- WorkFile variables
  TMPMOVIMAST_idx=0
  CONTROPA_idx=0
  AZIENDA_idx=0
  CONTI_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LEGGO I MOVIMENTI COMPRESI FRA LE DATE DI STAMPA E DI COMPETENZA DELL'ESERCIZIO
    *     SE SPECIFICATI, APPLICO I FILTRI PER B.U. E MOVIMENTI PROVVISORI
    * --- LEGGO I MOVIMENTI PRECEDENTI ALLA DATA DI INIZIO STAMPA CON COMPETENZA DELL'ESERCIZIO IN CORSO
    *     QUESTA QUERY SI PUO' EVITARE SE LA DATA DI INIZIO STAMPA COINCIDE CON L'INIZIO DELL'ESERCIZIO
    this.hFile = 0
    ah_msg("Lettura movimenti compresi fra le date",.t.,.t.)
    if this.oParentObject.w_ESSALDI="S"
      * --- Create temporary table TMPMOVIMAST
      i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_GFGJHGBHRO[2]
      indexes_GFGJHGBHRO[1]='PNTIPCON,PNCODCON'
      indexes_GFGJHGBHRO[2]='MCCODICE'
      vq_exec('query\b_movimenti1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_GFGJHGBHRO,.f.)
      this.TMPMOVIMAST_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table TMPMOVIMAST
      i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_ADVXRVXPYE[2]
      indexes_ADVXRVXPYE[1]='PNTIPCON,PNCODCON'
      indexes_ADVXRVXPYE[2]='MCCODICE'
      vq_exec('query\b_movimenti',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_ADVXRVXPYE,.f.)
      this.TMPMOVIMAST_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    if this.oParentObject.w_ESPREC="S" and this.oParentObject.w_PROVVI<>"S"
      * --- INIZIO LA LETTURA DEI SALDI DELL'ESERCIZIO PRECEDENTE
      *     1- VERIFICO SE PER L'ESERCIZIO IN CORSO E' STATA FATTA L'APERTURA
      * --- Entro a verificare se c'� apertura anche nel caso che sto 
      *     stampando tutti i movimenti ma non esiste esercizio precedente
      *     QUESTO CASO SI VERIFICA SOLO NELLA STAMPA CONFRONTO
      ah_msg("Verifica effettuazione apertura conti",.t.,.t.)
      * --- Insert into TMPMOVIMAST
      i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldoa",this.TMPMOVIMAST_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      if i_Rows > 0
        this.w_APERTURA = .T.
      endif
      if NOT this.w_APERTURA 
        * --- 2- SE NON E' STATA FATTA L'APERTURA DEI CONTI, LEGGIAMO I SALDI DAI MOVIMENTI DELL'ESERCIZIO PRECEDENDE
        *     DALLA LETTURA ESCLUDIAMO I MOVIMENTI DI CHIUSURA
        if NOT EMPTY(NVL(this.oParentObject.w_ESEPRE,""))
          ah_msg("Lettura movimenti dell'esercizio precedente",.t.,.t.)
          * --- Write into TMPMOVIMAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="PNTIPCON  ,PNCODCON"
            do vq_exec with 'gscg1kbe',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMOVIMAST_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPMOVIMAST.PNTIPCON   = _t2.PNTIPCON  ";
                    +" and "+"TMPMOVIMAST.PNCODCON = _t2.PNCODCON";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SALDO = _t2.SALDO";
                +i_ccchkf;
                +" from "+i_cTable+" TMPMOVIMAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPMOVIMAST.PNTIPCON   = _t2.PNTIPCON  ";
                    +" and "+"TMPMOVIMAST.PNCODCON = _t2.PNCODCON";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIMAST, "+i_cQueryTable+" _t2 set ";
                +"TMPMOVIMAST.SALDO = _t2.SALDO";
                +Iif(Empty(i_ccchkf),"",",TMPMOVIMAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPMOVIMAST.PNTIPCON   = t2.PNTIPCON  ";
                    +" and "+"TMPMOVIMAST.PNCODCON = t2.PNCODCON";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIMAST set (";
                +"SALDO";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.SALDO";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPMOVIMAST.PNTIPCON   = _t2.PNTIPCON  ";
                    +" and "+"TMPMOVIMAST.PNCODCON = _t2.PNCODCON";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIMAST set ";
                +"SALDO = _t2.SALDO";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".PNTIPCON   = "+i_cQueryTable+".PNTIPCON  ";
                    +" and "+i_cTable+".PNCODCON = "+i_cQueryTable+".PNCODCON";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SALDO = (select SALDO from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Insert into TMPMOVIMAST
          i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscg2kbe",this.TMPMOVIMAST_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        if i_Rows=0
          * --- non ci sono movimenti nell'esercizio precedente per cui provo a leggere il movimento di apertura
          ah_msg("Verifica effettuazione apertura conti",.t.,.t.)
          * --- Insert into TMPMOVIMAST
          i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura",this.TMPMOVIMAST_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        if EMPTY(this.oParentObject.w_CODBUN) AND EMPTY(this.oParentObject.w_SUPERBU)
          * --- LEGGIAMO ANCHE I MOVIMENTI FUORI LINEA CHE DEVONO POI ESSERE SOMMATI.
          ah_msg("Lettura saldi fuori linea",.t.,.t.)
          if this.oParentObject.w_VALAPP=this.oParentObject.w_PRVALNAZ
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_movfuori",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_movfuori1",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      endif
    endif
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCODIDE"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCODIDE;
        from (i_cTable) where;
            COCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODDITTA = NVL(cp_ToDate(_read_.COCODIDE),cp_NullValue(_read_.COCODIDE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCOFAZI,AZPIVAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCOFAZI,AZPIVAZI;
        from (i_cTable) where;
            AZCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODFISCALE = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      this.w_PARTITAIVA = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CODESE = g_CODESE
    this.w_CODDITTA = SUBSTR(this.w_CODDITTA,1,4)
    this.w_CODFISCALE = SUBSTR(this.w_CODFISCALE,1,16)
    if ISAHE()
      this.w_GESTIONALE = "AD HOC ENTERPRISE   "
    else
      this.w_GESTIONALE = "AD HOC REVOLUTION   "
    endif
    this.w_TESTATA = "T"+this.w_GESTIONALE+"BE"+SPACE(1)+this.oParentObject.w_ESE+this.w_CODDITTA+this.w_CODFISCALE+this.w_PARTITAIVA+SPACE(90)+"000000000000000000000000"+SPACE(60)+"000000"+SPACE(107)+"000000000000"+","+"00"+SPACE(1)+"000000000000"+","+"00"+SPACE(1)+"3"+SPACE(16)+"000000000000"+","+"00"+SPACE(1)+"000000000000"+","+"00"+SPACE(201)+"0000"+SPACE(30)+"000000000000000000000000"+SPACE(30)+"000000000000000000"+","+"00"+SPACE(1)+"0"+SPACE(5)
    * --- Select from B_CHIUSURA
    do vq_exec with 'B_CHIUSURA',this,'_Curs_B_CHIUSURA','',.f.,.t.
    if used('_Curs_B_CHIUSURA')
      select _Curs_B_CHIUSURA
      locate for 1=1
      do while not(eof())
      this.w_CHIUSURA = IIF(NVL(NUMERO,0)>0,.T.,.F.)
        select _Curs_B_CHIUSURA
        continue
      enddo
      use
    endif
    this.w_CPATH = Alltrim( this.oParentObject.w_PATH ) + Alltrim( this.oParentObject.w_FILE )
    this.w_OKGEN = 0
    * --- Select from gscg_bke
    do vq_exec with 'gscg_bke',this,'_Curs_gscg_bke','',.f.,.t.
    if used('_Curs_gscg_bke')
      select _Curs_gscg_bke
      locate for 1=1
      if not(eof())
      do while not(eof())
        select _Curs_gscg_bke
        continue
      enddo
      else
        this.w_OKGEN = 2
        select _Curs_gscg_bke
      endif
      use
    endif
    if this.w_OKGEN<>2
      if Not File( this.w_CPATH ) or AH_YESNO( "File gi� esistente, si desidera sovrascriverlo?" ) 
        this.hFile = FCREATE(this.w_CPATH,0)
        * --- Controllo creazione file di appoggio
        if this.hFile=-1
          ah_Msg("Impossibile creare il file di appoggio",.t.)
          return
        endif
        * --- Creazione testata
        FWRITE(this.hFile,this.w_TESTATA,LEN(this.w_TESTATA))
        FWRITE(this.hFile,CHR(13)+CHR(10),2)
        if this.oParentObject.w_DETCLFR="S"
          * --- Select from gscg_bke
          do vq_exec with 'gscg_bke',this,'_Curs_gscg_bke','',.f.,.t.
          if used('_Curs_gscg_bke')
            select _Curs_gscg_bke
            locate for 1=1
            if not(eof())
            do while not(eof())
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
              select _Curs_gscg_bke
              continue
            enddo
            else
              this.w_OKGEN = 0
              select _Curs_gscg_bke
            endif
            use
          endif
        else
          * --- Select from GSCG2BKE
          do vq_exec with 'GSCG2BKE',this,'_Curs_GSCG2BKE','',.f.,.t.
          if used('_Curs_GSCG2BKE')
            select _Curs_GSCG2BKE
            locate for 1=1
            do while not(eof())
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
              select _Curs_GSCG2BKE
              continue
            enddo
            use
          endif
        endif
        FCLOSE(this.hFile)
      else
        this.w_OKGEN = 0
      endif
    endif
    do case
      case this.w_OKGEN=2
        ah_ErrorMsg("Non ci sono dati da generare","i","")
      case this.w_OKGEN=1
        ah_ErrorMsg("File generato con successo","i","")
      case this.w_OKGEN=0
        ah_ErrorMsg("Operazione annullata","i","")
    endcase
    * --- Drop temporary table TMPMOVIMAST
    i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMOVIMAST')
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_DETCLFR="S"
      this.w_OKGEN = 1
      this.w_TIPCON = _Curs_GSCG_BKE.PNTIPCON
      this.w_CODICE = _Curs_GSCG_BKE.PNCODCON
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANRISESE"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANRISESE;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPCON;
              and ANCODICE = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANRISESE = NVL(cp_ToDate(_read_.ANRISESE),cp_NullValue(_read_.ANRISESE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CONTO = _Curs_GSCG_BKE.PNCODCON+SPACE(30)
      this.w_CODICECONTO = LEFT(this.w_CONTO,30)
      this.w_DESCRICONTO = SUBSTR(_Curs_GSCG_BKE.DESCRI,1,30)
      this.w_IMPORTO = _Curs_GSCG_BKE.IMPORTO
      this.w_SALDO = _Curs_GSCG_BKE.SALDO
      if this.oParentObject.w_ESPREC="S" 
        this.w_IMPORTO = this.w_IMPORTO+this.w_SALDO
      endif
      this.w_SEGNO = IIF(this.w_IMPORTO>0,"+","-")
      this.w_IMPORTO = ABS(this.w_IMPORTO)
      this.w_CIMPORTO = STR(this.w_IMPORTO,18,2)
      this.w_CIMPORTO = "000000000000000000000"+alltrim(this.w_CIMPORTO)
      this.w_NIMPORTO = RIGHT(this.w_CIMPORTO,15)
      this.w_CORPO = "B"+this.w_CODICECONTO+this.w_DESCRICONTO+this.w_NIMPORTO+this.w_SEGNO
      if this.w_ANRISESE<>"S"
        FWRITE(this.hFile,this.w_CORPO,LEN(this.w_CORPO))
        FWRITE(this.hFile,CHR(13)+CHR(10),2)
      endif
    else
      this.w_OKGEN = 1
      this.w_TIPCON = _Curs_GSCG2BKE.PNTIPCON
      this.w_CODICE = _Curs_GSCG2BKE.PNCODCON
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANRISESE"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANRISESE;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPCON;
              and ANCODICE = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANRISESE = NVL(cp_ToDate(_read_.ANRISESE),cp_NullValue(_read_.ANRISESE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CONTO = IIF(_Curs_GSCG2BKE.PNTIPCON="G" ,_Curs_GSCG2BKE.PNCODCON+SPACE(30),_Curs_GSCG2BKE.MCCODICE+SPACE(30))
      this.w_CODICECONTO = LEFT(this.w_CONTO,30)
      this.w_DESCRICONTO = SUBSTR(_Curs_GSCG2BKE.DESCRI,1,30)
      this.w_IMPORTO = _Curs_GSCG2BKE.IMPORTO
      if this.oParentObject.w_ESPREC="S"
        this.w_IMPORTO = _Curs_GSCG2BKE.TOTALE
      endif
      this.w_SEGNO = IIF(this.w_IMPORTO>0,"+","-")
      this.w_IMPORTO = ABS(this.w_IMPORTO)
      this.w_CIMPORTO = STR(this.w_IMPORTO,18,2)
      this.w_CIMPORTO = "000000000000000000000"+alltrim(this.w_CIMPORTO)
      this.w_NIMPORTO = RIGHT(this.w_CIMPORTO,15)
      this.w_CORPO = "B"+this.w_CODICECONTO+this.w_DESCRICONTO+this.w_NIMPORTO+this.w_SEGNO
      if this.w_ANRISESE<>"S"
        FWRITE(this.hFile,this.w_CORPO,LEN(this.w_CORPO))
        FWRITE(this.hFile,CHR(13)+CHR(10),2)
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='*TMPMOVIMAST'
    this.cWorkTables[2]='CONTROPA'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='CONTI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_B_CHIUSURA')
      use in _Curs_B_CHIUSURA
    endif
    if used('_Curs_gscg_bke')
      use in _Curs_gscg_bke
    endif
    if used('_Curs_gscg_bke')
      use in _Curs_gscg_bke
    endif
    if used('_Curs_GSCG2BKE')
      use in _Curs_GSCG2BKE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
