* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bma                                                        *
*              Stampa movimenti di analitica                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_114]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-03                                                      *
* Last revis.: 2014-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bma",oParentObject)
return(i_retval)

define class tgsca_bma as StdBatch
  * --- Local variables
  * --- WorkFile variables
  TMP_ANA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Movimenti di Analitica (da GSCA_SMA)
    *     GSCA_SMA.VQR => Manuali Originari
    *     GSCA1SMA.VQR => Primanota Originari
    *     GSCA4SMA.VQR => Documenti Originari
    *     GSCA2SMA.VQR => Manuali Ripartiti
    *     GSCA3SMA.VQR => Primanota Ripartiti
    *     GSCA5SMA.VQR => Documenti Ripartiti
    * --- Tipo: (E=Effettivo - P=Previsionale)
    * --- Prove: (T=Tutti - O=Originari - E=Escluso Ripartiti)
    * --- Origine: (A=Manuali - P=Primanota - C=Documenti - T=Tutti)
    * --- Provconf: (N=Confermato - S=Provvisorio)
    if empty( this.oParentObject.w_DATA1 ) OR empty( this.oParentObject.w_DATA2 )
      ah_ErrorMsg("Intervallo di date non valido",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Creazione Tabella Temporanea
    * --- Create temporary table TMP_ANA
    i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca_sma',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_ANA_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- sistemazione del conto contabile (contropartita) per i documenti, combinando la categoria contabile articolo con la categoria contabile cliente/fornitore
    * --- Write into TMP_ANA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ANA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ANA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MRSERRIF,MRORDRIF,MRNUMRIF,PROVIENE,ORDINE,MRCODICE"
      do vq_exec with 'gsca7sma',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ANA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_ANA.MRSERRIF = _t2.MRSERRIF";
              +" and "+"TMP_ANA.MRORDRIF = _t2.MRORDRIF";
              +" and "+"TMP_ANA.MRNUMRIF = _t2.MRNUMRIF";
              +" and "+"TMP_ANA.PROVIENE = _t2.PROVIENE";
              +" and "+"TMP_ANA.ORDINE = _t2.ORDINE";
              +" and "+"TMP_ANA.MRCODICE = _t2.MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNCODCON = _t2.PNCODCO2";
          +",ANDESCRI = _t2.ANDESCR2";
          +i_ccchkf;
          +" from "+i_cTable+" TMP_ANA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_ANA.MRSERRIF = _t2.MRSERRIF";
              +" and "+"TMP_ANA.MRORDRIF = _t2.MRORDRIF";
              +" and "+"TMP_ANA.MRNUMRIF = _t2.MRNUMRIF";
              +" and "+"TMP_ANA.PROVIENE = _t2.PROVIENE";
              +" and "+"TMP_ANA.ORDINE = _t2.ORDINE";
              +" and "+"TMP_ANA.MRCODICE = _t2.MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA, "+i_cQueryTable+" _t2 set ";
          +"TMP_ANA.PNCODCON = _t2.PNCODCO2";
          +",TMP_ANA.ANDESCRI = _t2.ANDESCR2";
          +Iif(Empty(i_ccchkf),"",",TMP_ANA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_ANA.MRSERRIF = t2.MRSERRIF";
              +" and "+"TMP_ANA.MRORDRIF = t2.MRORDRIF";
              +" and "+"TMP_ANA.MRNUMRIF = t2.MRNUMRIF";
              +" and "+"TMP_ANA.PROVIENE = t2.PROVIENE";
              +" and "+"TMP_ANA.ORDINE = t2.ORDINE";
              +" and "+"TMP_ANA.MRCODICE = t2.MRCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA set (";
          +"PNCODCON,";
          +"ANDESCRI";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.PNCODCO2,";
          +"t2.ANDESCR2";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_ANA.MRSERRIF = _t2.MRSERRIF";
              +" and "+"TMP_ANA.MRORDRIF = _t2.MRORDRIF";
              +" and "+"TMP_ANA.MRNUMRIF = _t2.MRNUMRIF";
              +" and "+"TMP_ANA.PROVIENE = _t2.PROVIENE";
              +" and "+"TMP_ANA.ORDINE = _t2.ORDINE";
              +" and "+"TMP_ANA.MRCODICE = _t2.MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA set ";
          +"PNCODCON = _t2.PNCODCO2";
          +",ANDESCRI = _t2.ANDESCR2";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MRSERRIF = "+i_cQueryTable+".MRSERRIF";
              +" and "+i_cTable+".MRORDRIF = "+i_cQueryTable+".MRORDRIF";
              +" and "+i_cTable+".MRNUMRIF = "+i_cQueryTable+".MRNUMRIF";
              +" and "+i_cTable+".PROVIENE = "+i_cQueryTable+".PROVIENE";
              +" and "+i_cTable+".ORDINE = "+i_cQueryTable+".ORDINE";
              +" and "+i_cTable+".MRCODICE = "+i_cQueryTable+".MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNCODCON = (select PNCODCO2 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",ANDESCRI = (select ANDESCR2 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Filtro sul conto contabile (non pu� essere effettuato nelle query)
    if NOT EMPTY(this.oParentObject.w_PNCODCON)
      * --- Delete from TMP_ANA
      i_nConn=i_TableProp[this.TMP_ANA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_ANA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PNCODCON <> "+cp_ToStrODBC(this.oParentObject.w_PNCODCON);
               )
      else
        delete from (i_cTable) where;
              PNCODCON <> this.oParentObject.w_PNCODCON;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    vq_exec(alltrim(this.oParentObject.w_OQRY),this,"__tmp__")
    * --- Variabili utilizzate nel Report
    L_TIPO=this.oParentObject.w_TIPO
    L_TIPOCF=this.oParentObject.w_TIPOCF
    L_CODCON1=this.oParentObject.w_CODCON
    L_PNCODCON=this.oParentObject.w_PNCODCON
    L_PROVE=this.oParentObject.w_PROVE
    L_ORIGINE=this.oParentObject.w_ORIGINE
    L_PROVCONF=this.oParentObject.w_PROVCONF
    L_MRCODCOM=this.oParentObject.w_MRCODCOM
    L_MRCODICE=this.oParentObject.w_MRCODICE
    L_MRCODVOC=this.oParentObject.w_MRCODVOC
    L_DATA1=this.oParentObject.w_DATA1
    L_DATA2=this.oParentObject.w_DATA2
    L_NUMER1=this.oParentObject.w_NUMER1
    L_NUMER2=this.oParentObject.w_NUMER2
    L_LIVE1=this.oParentObject.w_LIVE1
    L_LIVE2=this.oParentObject.w_LIVE2
    * --- Lancio Stampa Output Utente
    CP_CHPRN( ALLTRIM(this.oParentObject.w_OREP), " ", this )
    * --- Eliminazione Tabella Temporanea
    * --- Drop temporary table TMP_ANA
    i_nIdx=cp_GetTableDefIdx('TMP_ANA')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ANA')
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMP_ANA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
