* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bpr                                                        *
*              Rinumerazione protocollo                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_168]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-11                                                      *
* Last revis.: 2002-01-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bpr",oParentObject)
return(i_retval)

define class tgscg_bpr as StdBatch
  * --- Local variables
  w_ALFPRO = space(2)
  w_ALFPR1 = space(2)
  w_DATREG = ctod("  /  /  ")
  w_DATREG1 = ctod("  /  /  ")
  w_NUMPRO = 0
  w_NUMPR1 = 0
  w_ORIGIN = space(1)
  w_SERIAL = space(10)
  w_FLINTR = space(2)
  w_RIFDOC = space(10)
  w_PROT = space(10)
  w_ATTIPREG = space(1)
  w_OK = 0
  w_CODKEY = space(50)
  w_CATAZI = space(5)
  w_CONT = 0
  w_TROV = .f.
  w_ELAB = .f.
  w_MESS = space(20)
  w_PUNT = 0
  w_okprot = .f.
  * --- WorkFile variables
  ATTIDETT_idx=0
  DOC_MAST_idx=0
  PNT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rielabora Numerazione Protocollo (ds GSCG_KPR)
    this.w_CONT = 0
    * --- Controlli Preliminari
    if YEAR(this.oParentObject.w_DATINI) <> (this.oParentObject.w_CODANN)
      ah_ErrorMsg("La data iniziale non � congruente con l'anno impostato",,"")
      i_retcode = 'stop'
      return
    endif
    ah_Msg("Ricerca documenti",.T.)
    * --- Primo controllo su congruit� con data ultima stampa registro Iva
    this.w_TROV = .F.
    this.w_ELAB = .F.
    vq_exec("query\GSCG2QPR.VQR",this,"INTRA")
    if RECCOUNT()>0
      * --- Trovata almeno una registrazione INTRA
      * --- Select from ATTIDETT
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ATDATSTA  from "+i_cTable+" ATTIDETT ";
            +" where ATDATSTA >= "+cp_ToStrODBC(this.oParentObject.w_DATINI)+"";
             ,"_Curs_ATTIDETT")
      else
        select ATDATSTA from (i_cTable);
         where ATDATSTA >= this.oParentObject.w_DATINI;
          into cursor _Curs_ATTIDETT
      endif
      if used('_Curs_ATTIDETT')
        select _Curs_ATTIDETT
        locate for 1=1
        do while not(eof())
        this.w_TROV = .T.
          select _Curs_ATTIDETT
          continue
        enddo
        use
      endif
      if this.w_TROV=.T.
        ah_ErrorMsg("La data di inizio selezione � minore o uguale all'ultima stampa registri IVA, impossibile elaborare",,"")
      endif
    else
      * --- Select from ATTIDETT
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ATDATSTA  from "+i_cTable+" ATTIDETT ";
            +" where ATDATSTA >= "+cp_ToStrODBC(this.oParentObject.w_DATINI)+" AND ATTIPREG='A'";
             ,"_Curs_ATTIDETT")
      else
        select ATDATSTA from (i_cTable);
         where ATDATSTA >= this.oParentObject.w_DATINI AND ATTIPREG="A";
          into cursor _Curs_ATTIDETT
      endif
      if used('_Curs_ATTIDETT')
        select _Curs_ATTIDETT
        locate for 1=1
        do while not(eof())
        this.w_TROV = .T.
          select _Curs_ATTIDETT
          continue
        enddo
        use
      endif
      if this.w_TROV=.T.
        ah_ErrorMsg("La data di inizio selezione � minore o uguale all'ultima stampa registri IVA acquisti, impossibile elaborare",,"")
      endif
    endif
    if this.w_TROV=.F.
      vq_exec("query\GSCG_QPR.VQR",this,"TOTREG")
      * --- Testa se si verifica un Errore sotto Transazione
      this.w_OK = 1
      * --- Inizializza cursore ultimi numeri protocollo
      CREATE CURSOR ULTPRO (ULTSER C(10), ULTNUM N(15,0))
      * --- Try
      local bErr_03D608C0
      bErr_03D608C0=bTrsErr
      this.Try_03D608C0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Errore durante l'elaborazione, operazione abbandonata",,"")
        this.w_OK = -1
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_03D608C0
      * --- End
      if this.w_OK>0
        if USED("ULTPRO")
          SELECT ULTPRO
          GO TOP
          SCAN
          this.w_ALFPRO = NVL(ULTSER,"  ")
          this.w_NUMPRO = NVL(ULTNUM, 0)
          this.w_CODKEY = "prog\PRPRO\"+ chr(39) + LEFT(ALLTRIM(i_CODAZI)+ SPACE(5),5)+ chr(39)
          this.w_CODKEY = ALLTRIM(this.w_CODKEY) + "\" + chr(39) + LEFT(ALLTRIM(STR(this.oParentObject.w_CODANN))+ SPACE(4),4) + chr(39)
          this.w_CODKEY = ALLTRIM(this.w_CODKEY)+"\"+chr(39) + LEFT(ALLTRIM("AC")+SPACE(2),2) + chr(39) + "\" 
          this.w_CODKEY = ALLTRIM(this.w_CODKEY) + chr(39) + LEFT(ALLTRIM(this.w_ALFPRO)+ SPACE(2),2)+chr(39)
          * --- Esegue l'aggiornamento nella tabella cpwarn
          i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
          this.w_OK = sqlexec(i_Conn,"UPDATE CPWARN SET AUTONUM="+cp_ToStrODBC(this.w_NUMPRO)+" WHERE TABLECODE="+cp_ToStrODBC(this.w_CODKEY))
          if this.w_OK<0
            ah_ErrorMsg("Errore di aggiornamento. Aggiornare manualmente la serie %1 con il numero protocollo: %2",,"",ALLTRIM(this.w_ALFPRO),ALLTRIM(STR(this.w_NUMPRO,15)))
          endif
          SELECT ULTPRO
          ENDSCAN
        endif
      endif
      if this.w_ELAB=.T. 
        ah_ErrorMsg("Rinumerazione protocolli completata",,"")
      else
        ah_ErrorMsg("Per le selezioni impostate non ci sono protocolli da rinumerare",,"")
      endif
    endif
    if USED("INTRA")
      SELECT INTRA
      USE
    endif
    if USED("ULTPRO")
      SELECT ULTPRO
      USE
    endif
    if USED("TOTREG")
      SELECT TOTREG
      USE
    endif
  endproc
  proc Try_03D608C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Per ciascuna serie estrae il numero di protocollo pi� alto 
    * --- della data immediatamente precedente la data di inizio riprotocollazione
    this.w_PROT = "@#"
    this.w_NUMPRO = this.oParentObject.w_SELPRO
    this.w_okprot = .T.
    SELECT TOTREG
    GO TOP
    SCAN FOR NOT EMPTY(CP_TODATE(DATREG))
    this.w_ALFPRO = NVL(ALFPRO,"  ")
    if this.w_ALFPRO<>this.w_PROT AND this.w_PROT<>"@#"
      * --- Cambio Serie Protocollo reinizializza il Numero
       
 INSERT INTO ULTPRO (ULTSER, ULTNUM) VALUES (this.w_PROT, this.w_NUMPRO)
      this.w_NUMPRO = this.oParentObject.w_SELPRO
    endif
    if this.w_ALFPRO<>this.w_PROT
      if this.oParentObject.w_INIPRO>0
        SELECT TOTREG
        this.w_PUNT = RECNO()
        LOCATE FOR this.w_ALFPRO=NVL(ALFPRO,Space(10)) AND this.oParentObject.w_INIPRO=NVL(NUMPRO,0)
        if Not Found()
          * --- Nel caso in cui non trovo il numero protocollo di inizio
          *     non devo protocollare e riporto il puntatore del cursore prima della locate
           
 Select TOTREG 
 Go this.w_PUNT
          this.w_okprot = .F.
        else
          * --- Posso protocollare la nuova serie
          this.w_okprot = .T.
        endif
      endif
    endif
    this.w_DATREG = DATREG
    this.w_ORIGIN = NVL(ORIGIN, " ")
    this.w_SERIAL = SERIAL
    this.w_RIFDOC = NVL(RIFDOC, SPACE(10))
    this.w_PROT = this.w_ALFPRO
    if this.w_okprot
      if this.w_DATREG<this.oParentObject.w_DATINI AND this.oParentObject.w_SELPRO>=0
        * --- Memorizza il Protocollo piu' alto precedente il periodo da considerare
        this.w_NUMPRO = NVL(NUMPRO, 0)
      else
        * --- Se Data Maggiore o Uguale incrementa di 1 il num .pro.
        this.w_ELAB = .T.
        this.w_NUMPRO = this.w_NUMPRO + 1
        if this.w_ORIGIN="D"
          * --- Aggiorna il num.pro. dei documenti
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVNUMEST ="+cp_NullLink(cp_ToStrODBC(this.w_NUMPRO),'DOC_MAST','MVNUMEST');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                MVNUMEST = this.w_NUMPRO;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Aggiorna il num.pro. della primanota
          * --- Write into PNT_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PNT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PNNUMPRO ="+cp_NullLink(cp_ToStrODBC(this.w_NUMPRO),'PNT_MAST','PNNUMPRO');
                +i_ccchkf ;
            +" where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                PNNUMPRO = this.w_NUMPRO;
                &i_ccchkf. ;
             where;
                PNSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if VAL(this.w_RIFDOC) >0
            * --- Aggiorna  anche il documento d'origine contabilizzato
            * --- Write into DOC_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVNUMEST ="+cp_NullLink(cp_ToStrODBC(this.w_NUMPRO),'DOC_MAST','MVNUMEST');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_RIFDOC);
                     )
            else
              update (i_cTable) set;
                  MVNUMEST = this.w_NUMPRO;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_RIFDOC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
    endif
    SELECT TOTREG
    ENDSCAN
    if this.w_PROT<>"@#"
      * --- Cambio Serie Protocollo reinizializza il Numero
      INSERT INTO ULTPRO (ULTSER, ULTNUM) VALUES (this.w_PROT, this.w_NUMPRO)
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ATTIDETT'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='PNT_MAST'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
