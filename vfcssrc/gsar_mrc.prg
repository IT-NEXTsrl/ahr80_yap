* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mrc                                                        *
*              Tabella ripartizioni                                            *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_16]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2012-10-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mrc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mrc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mrc")
  return

* --- Class definition
define class tgsar_mrc as StdPCForm
  Width  = 555
  Height = 196
  Top    = 19
  Left   = 61
  cComment = "Tabella ripartizioni"
  cPrg = "gsar_mrc"
  HelpContextID=113866601
  add object cnt as tcgsar_mrc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mrc as PCContext
  w_MRTIPCON = space(1)
  w_MRCODICE = space(15)
  w_CCTAGG = space(1)
  w_MRCODVOC = space(15)
  w_DESVOC = space(40)
  w_MRCODCDC = space(15)
  w_DESPIA = space(40)
  w_MRPARAME = 0
  w_PERCEN = 0
  w_NUMLIV = 0
  w_DATOBSO = space(8)
  w_TOTPAR = 0
  w_TOTPER = 0
  w_OBTEST = space(8)
  proc Save(i_oFrom)
    this.w_MRTIPCON = i_oFrom.w_MRTIPCON
    this.w_MRCODICE = i_oFrom.w_MRCODICE
    this.w_CCTAGG = i_oFrom.w_CCTAGG
    this.w_MRCODVOC = i_oFrom.w_MRCODVOC
    this.w_DESVOC = i_oFrom.w_DESVOC
    this.w_MRCODCDC = i_oFrom.w_MRCODCDC
    this.w_DESPIA = i_oFrom.w_DESPIA
    this.w_MRPARAME = i_oFrom.w_MRPARAME
    this.w_PERCEN = i_oFrom.w_PERCEN
    this.w_NUMLIV = i_oFrom.w_NUMLIV
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_TOTPAR = i_oFrom.w_TOTPAR
    this.w_TOTPER = i_oFrom.w_TOTPER
    this.w_OBTEST = i_oFrom.w_OBTEST
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MRTIPCON = this.w_MRTIPCON
    i_oTo.w_MRCODICE = this.w_MRCODICE
    i_oTo.w_CCTAGG = this.w_CCTAGG
    i_oTo.w_MRCODVOC = this.w_MRCODVOC
    i_oTo.w_DESVOC = this.w_DESVOC
    i_oTo.w_MRCODCDC = this.w_MRCODCDC
    i_oTo.w_DESPIA = this.w_DESPIA
    i_oTo.w_MRPARAME = this.w_MRPARAME
    i_oTo.w_PERCEN = this.w_PERCEN
    i_oTo.w_NUMLIV = this.w_NUMLIV
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_TOTPAR = this.w_TOTPAR
    i_oTo.w_TOTPER = this.w_TOTPER
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mrc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 555
  Height = 196
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-19"
  HelpContextID=113866601
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  COLLCENT_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  cFile = "COLLCENT"
  cKeySelect = "MRTIPCON,MRCODICE"
  cKeyWhere  = "MRTIPCON=this.w_MRTIPCON and MRCODICE=this.w_MRCODICE"
  cKeyDetail  = "MRTIPCON=this.w_MRTIPCON and MRCODICE=this.w_MRCODICE and MRCODVOC=this.w_MRCODVOC and MRCODCDC=this.w_MRCODCDC"
  cKeyWhereODBC = '"MRTIPCON="+cp_ToStrODBC(this.w_MRTIPCON)';
      +'+" and MRCODICE="+cp_ToStrODBC(this.w_MRCODICE)';

  cKeyDetailWhereODBC = '"MRTIPCON="+cp_ToStrODBC(this.w_MRTIPCON)';
      +'+" and MRCODICE="+cp_ToStrODBC(this.w_MRCODICE)';
      +'+" and MRCODVOC="+cp_ToStrODBC(this.w_MRCODVOC)';
      +'+" and MRCODCDC="+cp_ToStrODBC(this.w_MRCODCDC)';

  cKeyWhereODBCqualified = '"COLLCENT.MRTIPCON="+cp_ToStrODBC(this.w_MRTIPCON)';
      +'+" and COLLCENT.MRCODICE="+cp_ToStrODBC(this.w_MRCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mrc"
  cComment = "Tabella ripartizioni"
  i_nRowNum = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MRTIPCON = space(1)
  w_MRCODICE = space(15)
  w_CCTAGG = space(1)
  w_MRCODVOC = space(15)
  w_DESVOC = space(40)
  w_MRCODCDC = space(15)
  w_DESPIA = space(40)
  w_MRPARAME = 0
  w_PERCEN = 0
  w_NUMLIV = 0
  w_DATOBSO = ctod('  /  /  ')
  w_TOTPAR = 0
  w_TOTPER = 0
  w_OBTEST = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mrcPag1","gsar_mrc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CENCOST'
    this.cWorkTables[2]='VOC_COST'
    this.cWorkTables[3]='COLLCENT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.COLLCENT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.COLLCENT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mrc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from COLLCENT where MRTIPCON=KeySet.MRTIPCON
    *                            and MRCODICE=KeySet.MRCODICE
    *                            and MRCODVOC=KeySet.MRCODVOC
    *                            and MRCODCDC=KeySet.MRCODCDC
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.COLLCENT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COLLCENT_IDX,2],this.bLoadRecFilter,this.COLLCENT_IDX,"gsar_mrc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('COLLCENT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "COLLCENT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' COLLCENT '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MRTIPCON',this.w_MRTIPCON  ,'MRCODICE',this.w_MRCODICE  )
      select * from (i_cTable) COLLCENT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTPAR = 0
        .w_TOTPER = 0
        .w_MRTIPCON = NVL(MRTIPCON,space(1))
        .w_MRCODICE = NVL(MRCODICE,space(15))
        .w_CCTAGG = this.oParentObject .w_ANCCTAGG
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'COLLCENT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTPAR = 0
      this.w_TOTPER = 0
      scan
        with this
          .w_DESVOC = space(40)
          .w_DESPIA = space(40)
          .w_PERCEN = 0
          .w_NUMLIV = 0
          .w_DATOBSO = ctod("  /  /  ")
        .w_OBTEST = i_DATSYS
          .w_MRCODVOC = NVL(MRCODVOC,space(15))
          if link_2_1_joined
            this.w_MRCODVOC = NVL(VCCODICE201,NVL(this.w_MRCODVOC,space(15)))
            this.w_DESVOC = NVL(VCDESCRI201,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(VCDTOBSO201),ctod("  /  /  "))
          else
          .link_2_1('Load')
          endif
          .w_MRCODCDC = NVL(MRCODCDC,space(15))
          if link_2_3_joined
            this.w_MRCODCDC = NVL(CC_CONTO203,NVL(this.w_MRCODCDC,space(15)))
            this.w_DESPIA = NVL(CCDESPIA203,space(40))
            this.w_NUMLIV = NVL(CCNUMLIV203,0)
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO203),ctod("  /  /  "))
          else
          .link_2_3('Load')
          endif
          .w_MRPARAME = NVL(MRPARAME,0)
        .oPgFrm.Page1.oPag.oObj_2_8.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTPAR = .w_TOTPAR+.w_MRPARAME
          .w_TOTPER = .w_TOTPER+.w_PERCEN
          replace MRCODVOC with .w_MRCODVOC
          replace MRCODCDC with .w_MRCODCDC
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CCTAGG = this.oParentObject .w_ANCCTAGG
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_MRTIPCON=space(1)
      .w_MRCODICE=space(15)
      .w_CCTAGG=space(1)
      .w_MRCODVOC=space(15)
      .w_DESVOC=space(40)
      .w_MRCODCDC=space(15)
      .w_DESPIA=space(40)
      .w_MRPARAME=0
      .w_PERCEN=0
      .w_NUMLIV=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_TOTPAR=0
      .w_TOTPER=0
      .w_OBTEST=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_CCTAGG = this.oParentObject .w_ANCCTAGG
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_MRCODVOC))
         .link_2_1('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_MRCODCDC))
         .link_2_3('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_2_8.Calculate()
        .DoRTCalc(7,13,.f.)
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'COLLCENT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_2_8.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'COLLCENT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.COLLCENT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRTIPCON,"MRTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCODICE,"MRCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MRCODVOC C(15);
      ,t_DESVOC C(40);
      ,t_MRCODCDC C(15);
      ,t_DESPIA C(40);
      ,t_MRPARAME N(18,4);
      ,t_PERCEN N(6,2);
      ,t_NUMLIV N(2);
      ,MRCODVOC C(15);
      ,MRCODCDC C(15);
      ,t_DATOBSO D(8);
      ,t_OBTEST D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mrcbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1.controlsource=this.cTrsName+'.t_MRCODVOC'
    this.oPgFRm.Page1.oPag.oDESVOC_2_2.controlsource=this.cTrsName+'.t_DESVOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCDC_2_3.controlsource=this.cTrsName+'.t_MRCODCDC'
    this.oPgFRm.Page1.oPag.oDESPIA_2_4.controlsource=this.cTrsName+'.t_DESPIA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRPARAME_2_5.controlsource=this.cTrsName+'.t_MRPARAME'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPERCEN_2_6.controlsource=this.cTrsName+'.t_PERCEN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNUMLIV_2_7.controlsource=this.cTrsName+'.t_NUMLIV'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(140)
    this.AddVLine(284)
    this.AddVLine(322)
    this.AddVLine(466)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.COLLCENT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COLLCENT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.COLLCENT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COLLCENT_IDX,2])
      *
      * insert into COLLCENT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'COLLCENT')
        i_extval=cp_InsertValODBCExtFlds(this,'COLLCENT')
        i_cFldBody=" "+;
                  "(MRTIPCON,MRCODICE,MRCODVOC,MRCODCDC,MRPARAME,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MRTIPCON)+","+cp_ToStrODBC(this.w_MRCODICE)+","+cp_ToStrODBCNull(this.w_MRCODVOC)+","+cp_ToStrODBCNull(this.w_MRCODCDC)+","+cp_ToStrODBC(this.w_MRPARAME)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'COLLCENT')
        i_extval=cp_InsertValVFPExtFlds(this,'COLLCENT')
        cp_CheckDeletedKey(i_cTable,0,'MRTIPCON',this.w_MRTIPCON,'MRCODICE',this.w_MRCODICE,'MRCODVOC',this.w_MRCODVOC,'MRCODCDC',this.w_MRCODCDC)
        INSERT INTO (i_cTable) (;
                   MRTIPCON;
                  ,MRCODICE;
                  ,MRCODVOC;
                  ,MRCODCDC;
                  ,MRPARAME;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MRTIPCON;
                  ,this.w_MRCODICE;
                  ,this.w_MRCODVOC;
                  ,this.w_MRCODCDC;
                  ,this.w_MRPARAME;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- gsar_mrc
    this.oparentobject.notifyevent('Controllo')
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.COLLCENT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COLLCENT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (Not Empty(t_MRCODCDC) AND Not Empty(t_MRCODVOC)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'COLLCENT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and MRCODVOC="+cp_ToStrODBC(&i_TN.->MRCODVOC)+;
                 " and MRCODCDC="+cp_ToStrODBC(&i_TN.->MRCODCDC)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'COLLCENT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and MRCODVOC=&i_TN.->MRCODVOC;
                      and MRCODCDC=&i_TN.->MRCODCDC;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (Not Empty(t_MRCODCDC) AND Not Empty(t_MRCODVOC)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and MRCODVOC="+cp_ToStrODBC(&i_TN.->MRCODVOC)+;
                            " and MRCODCDC="+cp_ToStrODBC(&i_TN.->MRCODCDC)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and MRCODVOC=&i_TN.->MRCODVOC;
                            and MRCODCDC=&i_TN.->MRCODCDC;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace MRCODVOC with this.w_MRCODVOC
              replace MRCODCDC with this.w_MRCODCDC
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update COLLCENT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'COLLCENT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MRPARAME="+cp_ToStrODBC(this.w_MRPARAME)+;
                     ",MRCODVOC="+cp_ToStrODBC(this.w_MRCODVOC)+;
                     ",MRCODCDC="+cp_ToStrODBC(this.w_MRCODCDC)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and MRCODVOC="+cp_ToStrODBC(MRCODVOC)+;
                             " and MRCODCDC="+cp_ToStrODBC(MRCODCDC)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'COLLCENT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MRPARAME=this.w_MRPARAME;
                     ,MRCODVOC=this.w_MRCODVOC;
                     ,MRCODCDC=this.w_MRCODCDC;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and MRCODVOC=&i_TN.->MRCODVOC;
                                      and MRCODCDC=&i_TN.->MRCODCDC;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.COLLCENT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COLLCENT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (Not Empty(t_MRCODCDC) AND Not Empty(t_MRCODVOC)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete COLLCENT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and MRCODVOC="+cp_ToStrODBC(&i_TN.->MRCODVOC)+;
                            " and MRCODCDC="+cp_ToStrODBC(&i_TN.->MRCODCDC)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and MRCODVOC=&i_TN.->MRCODVOC;
                              and MRCODCDC=&i_TN.->MRCODCDC;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (Not Empty(t_MRCODCDC) AND Not Empty(t_MRCODVOC)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.COLLCENT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COLLCENT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .w_CCTAGG = this.oParentObject .w_ANCCTAGG
        .oPgFrm.Page1.oPag.oObj_2_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DATOBSO with this.w_DATOBSO
      replace t_OBTEST with this.w_OBTEST
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_8.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRCODCDC_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRCODCDC_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRPARAME_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRPARAME_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_2_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MRCODVOC
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODVOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_MRCODVOC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_MRCODVOC))
          select VCCODICE,VCDESCRI,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODVOC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODVOC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oMRCODVOC_2_1'),i_cWhere,'GSCA_AVC',"Voci di costo o ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODVOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_MRCODVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_MRCODVOC)
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODVOC = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODVOC = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MRCODVOC = space(15)
        this.w_DESVOC = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODVOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.VCCODICE as VCCODICE201"+ ",link_2_1.VCDESCRI as VCDESCRI201"+ ",link_2_1.VCDTOBSO as VCDTOBSO201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on COLLCENT.MRCODVOC=link_2_1.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and COLLCENT.MRCODVOC=link_2_1.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRCODCDC
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODCDC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_MRCODCDC)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_MRCODCDC))
          select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODCDC)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_MRCODCDC)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_MRCODCDC)+"%");

            select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MRCODCDC) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oMRCODCDC_2_3'),i_cWhere,'GSCA_ACC',"Centri di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODCDC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_MRCODCDC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_MRCODCDC)
            select CC_CONTO,CCDESPIA,CCNUMLIV,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODCDC = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_NUMLIV = NVL(_Link_.CCNUMLIV,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODCDC = space(15)
      endif
      this.w_DESPIA = space(40)
      this.w_NUMLIV = 0
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MRCODCDC = space(15)
        this.w_DESPIA = space(40)
        this.w_NUMLIV = 0
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODCDC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CC_CONTO as CC_CONTO203"+ ",link_2_3.CCDESPIA as CCDESPIA203"+ ",link_2_3.CCNUMLIV as CCNUMLIV203"+ ",link_2_3.CCDTOBSO as CCDTOBSO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on COLLCENT.MRCODCDC=link_2_3.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and COLLCENT.MRCODCDC=link_2_3.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESVOC_2_2.value==this.w_DESVOC)
      this.oPgFrm.Page1.oPag.oDESVOC_2_2.value=this.w_DESVOC
      replace t_DESVOC with this.oPgFrm.Page1.oPag.oDESVOC_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIA_2_4.value==this.w_DESPIA)
      this.oPgFrm.Page1.oPag.oDESPIA_2_4.value=this.w_DESPIA
      replace t_DESPIA with this.oPgFrm.Page1.oPag.oDESPIA_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTPAR_3_1.value==this.w_TOTPAR)
      this.oPgFrm.Page1.oPag.oTOTPAR_3_1.value=this.w_TOTPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTPER_3_2.value==this.w_TOTPER)
      this.oPgFrm.Page1.oPag.oTOTPER_3_2.value=this.w_TOTPER
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1.value==this.w_MRCODVOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1.value=this.w_MRCODVOC
      replace t_MRCODVOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCDC_2_3.value==this.w_MRCODCDC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCDC_2_3.value=this.w_MRCODCDC
      replace t_MRCODCDC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCDC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRPARAME_2_5.value==this.w_MRPARAME)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRPARAME_2_5.value=this.w_MRPARAME
      replace t_MRPARAME with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRPARAME_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPERCEN_2_6.value==this.w_PERCEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPERCEN_2_6.value=this.w_PERCEN
      replace t_PERCEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPERCEN_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMLIV_2_7.value==this.w_NUMLIV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMLIV_2_7.value=this.w_NUMLIV
      replace t_NUMLIV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMLIV_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'COLLCENT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(empty(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and not(empty(.w_MRCODVOC)) and (Not Empty(.w_MRCODCDC) AND Not Empty(.w_MRCODVOC))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        case   not(empty(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and (.oParentObject .w_ANCCTAGG $ 'AM') and not(empty(.w_MRCODCDC)) and (Not Empty(.w_MRCODCDC) AND Not Empty(.w_MRCODVOC))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCDC_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        case   not(.w_MRPARAME>0 OR .w_CCTAGG='M') and (.oParentObject .w_ANCCTAGG $ 'AM') and (Not Empty(.w_MRCODCDC) AND Not Empty(.w_MRCODVOC))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRPARAME_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Parametro non valorizzato")
      endcase
      if Not Empty(.w_MRCODCDC) AND Not Empty(.w_MRCODVOC)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(Not Empty(t_MRCODCDC) AND Not Empty(t_MRCODVOC))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MRCODVOC=space(15)
      .w_DESVOC=space(40)
      .w_MRCODCDC=space(15)
      .w_DESPIA=space(40)
      .w_MRPARAME=0
      .w_PERCEN=0
      .w_NUMLIV=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_MRCODVOC))
        .link_2_1('Full')
      endif
      .DoRTCalc(5,6,.f.)
      if not(empty(.w_MRCODCDC))
        .link_2_3('Full')
      endif
        .oPgFrm.Page1.oPag.oObj_2_8.Calculate()
      .DoRTCalc(7,13,.f.)
        .w_OBTEST = i_DATSYS
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MRCODVOC = t_MRCODVOC
    this.w_DESVOC = t_DESVOC
    this.w_MRCODCDC = t_MRCODCDC
    this.w_DESPIA = t_DESPIA
    this.w_MRPARAME = t_MRPARAME
    this.w_PERCEN = t_PERCEN
    this.w_NUMLIV = t_NUMLIV
    this.w_DATOBSO = t_DATOBSO
    this.w_OBTEST = t_OBTEST
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MRCODVOC with this.w_MRCODVOC
    replace t_DESVOC with this.w_DESVOC
    replace t_MRCODCDC with this.w_MRCODCDC
    replace t_DESPIA with this.w_DESPIA
    replace t_MRPARAME with this.w_MRPARAME
    replace t_PERCEN with this.w_PERCEN
    replace t_NUMLIV with this.w_NUMLIV
    replace t_DATOBSO with this.w_DATOBSO
    replace t_OBTEST with this.w_OBTEST
    if i_srv='A'
      replace MRCODVOC with this.w_MRCODVOC
      replace MRCODCDC with this.w_MRCODCDC
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTPAR = .w_TOTPAR-.w_mrparame
        .w_TOTPER = .w_TOTPER-.w_percen
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mrcPag1 as StdContainer
  Width  = 551
  height = 196
  stdWidth  = 551
  stdheight = 196
  resizeXpos=543
  resizeYpos=101
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=4, width=542,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="MRCODVOC",Label1="Voce costo/ricavo",Field2="MRCODCDC",Label2="Centro di costo o ricavo",Field3="NUMLIV",Label3="Liv.",Field4="MRPARAME",Label4="Parametro",Field5="PERCEN",Label5="Percent.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202224250

  add object oStr_1_3 as StdString with uid="CBAWJVONWQ",Visible=.t., Left=276, Top=146,;
    Alignment=1, Width=45, Height=15,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="HWKEJVKKVG",Visible=.t., Left=4, Top=146,;
    Alignment=1, Width=48, Height=15,;
    Caption="Voce:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="DCBVVVEKGV",Visible=.t., Left=4, Top=175,;
    Alignment=1, Width=48, Height=15,;
    Caption="C/ C.R.:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=23,;
    width=538+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=24,width=537+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VOC_COST|CENCOST|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESVOC_2_2.Refresh()
      this.Parent.oDESPIA_2_4.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VOC_COST'
        oDropInto=this.oBodyCol.oRow.oMRCODVOC_2_1
      case cFile='CENCOST'
        oDropInto=this.oBodyCol.oRow.oMRCODCDC_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oDESVOC_2_2 as StdTrsField with uid="SBVSCFZMGR",rtseq=5,rtrep=.t.,;
    cFormVar="w_DESVOC",value=space(40),enabled=.f.,;
    HelpContextID = 243137994,;
    cTotal="", bFixedPos=.t., cQueryName = "DESVOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=54, Top=147, InputMask=replicate('X',40)

  add object oDESPIA_2_4 as StdTrsField with uid="LZHLKNKOXY",rtseq=7,rtrep=.t.,;
    cFormVar="w_DESPIA",value=space(40),enabled=.f.,;
    HelpContextID = 14941642,;
    cTotal="", bFixedPos=.t., cQueryName = "DESPIA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=54, Top=171, InputMask=replicate('X',40)

  add object oObj_2_8 as cp_runprogram with uid="DSHLMFNBSR",width=255,height=23,;
   left=82, top=200,;
    caption='GSAR_BCP',;
   bGlobalFont=.t.,;
    prg="GSAR_BCP",;
    cEvent = "w_MRPARAME Changed,Load",;
    nPag=2;
    , HelpContextID = 24965302

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTPAR_3_1 as StdField with uid="ZWSJVHUGLO",rtseq=12,rtrep=.f.,;
    cFormVar="w_TOTPAR",value=0,enabled=.f.,;
    HelpContextID = 6546122,;
    cQueryName = "TOTPAR",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=324, Top=146, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oTOTPER_3_2 as StdField with uid="CTNAZXXZIQ",rtseq=13,rtrep=.f.,;
    cFormVar="w_TOTPER",value=0,enabled=.f.,;
    HelpContextID = 2351818,;
    cQueryName = "TOTPER",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=467, Top=146, cSayPict=["999.99"], cGetPict=["999.99"]
enddefine

* --- Defining Body row
define class tgsar_mrcBodyRow as CPBodyRowCnt
  Width=528
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMRCODVOC_2_1 as StdTrsField with uid="LBFUPNAMKE",rtseq=4,rtrep=.t.,;
    cFormVar="w_MRCODVOC",value=space(15),isprimarykey=.t.,;
    ToolTipText = "Codice voce di costo o ricavo",;
    HelpContextID = 204861431,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=-2, Top=0, cSayPict=[p_MCE], cGetPict=[p_MCE], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_MRCODVOC"

  func oMRCODVOC_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODVOC_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMRCODVOC_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oMRCODVOC_2_1.readonly and this.parent.oMRCODVOC_2_1.isprimarykey)
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oMRCODVOC_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo o ricavo",'',this.parent.oContained
   endif
  endproc
  proc oMRCODVOC_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_MRCODVOC
    i_obj.ecpSave()
  endproc

  add object oMRCODCDC_2_3 as StdTrsField with uid="HJAXAJROGT",rtseq=6,rtrep=.t.,;
    cFormVar="w_MRCODCDC",value=space(15),isprimarykey=.t.,;
    ToolTipText = "Codice centro di costo collegato al conto",;
    HelpContextID = 13242377,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=140, Top=0, cSayPict=[p_CEN], cGetPict=[p_CEN], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_MRCODCDC"

  func oMRCODCDC_2_3.mCond()
    with this.Parent.oContained
      return (.oParentObject .w_ANCCTAGG $ 'AM')
    endwith
  endfunc

  func oMRCODCDC_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODCDC_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMRCODCDC_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oMRCODCDC_2_3.readonly and this.parent.oMRCODCDC_2_3.isprimarykey)
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oMRCODCDC_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo",'',this.parent.oContained
   endif
  endproc
  proc oMRCODCDC_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_MRCODCDC
    i_obj.ecpSave()
  endproc

  add object oMRPARAME_2_5 as StdTrsField with uid="AZZSOIVYZI",rtseq=8,rtrep=.t.,;
    cFormVar="w_MRPARAME",value=0,;
    ToolTipText = "Parametro di suddivisione del costo",;
    HelpContextID = 6496245,;
    cTotal = "this.Parent.oContained.w_totpar", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Parametro non valorizzato",;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=321, Top=0, cSayPict=["99,999,999,999.9999"], cGetPict=["99,999,999,999.9999"]

  func oMRPARAME_2_5.mCond()
    with this.Parent.oContained
      return (.oParentObject .w_ANCCTAGG $ 'AM')
    endwith
  endfunc

  func oMRPARAME_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MRPARAME>0 OR .w_CCTAGG='M')
    endwith
    return bRes
  endfunc

  add object oPERCEN_2_6 as StdTrsField with uid="XSTIGAVIHY",rtseq=9,rtrep=.t.,;
    cFormVar="w_PERCEN",value=0,enabled=.f.,;
    HelpContextID = 70323466,;
    cTotal = "this.Parent.oContained.w_totper", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=465, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oNUMLIV_2_7 as StdTrsField with uid="VDRFMNGCIO",rtseq=10,rtrep=.t.,;
    cFormVar="w_NUMLIV",value=0,enabled=.f.,;
    HelpContextID = 68661974,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=30, Left=286, Top=0
  add object oLast as LastKeyMover
  * ---
  func oMRCODVOC_2_1.When()
    return(.t.)
  proc oMRCODVOC_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMRCODVOC_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mrc','COLLCENT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MRTIPCON=COLLCENT.MRTIPCON";
  +" and "+i_cAliasName2+".MRCODICE=COLLCENT.MRCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
