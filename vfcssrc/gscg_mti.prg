* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mti                                                        *
*              Totalizzatori IVA                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-23                                                      *
* Last revis.: 2014-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_mti"))

* --- Class definition
define class tgscg_mti as StdTrsForm
  Top    = 11
  Left   = 10

  * --- Standard Properties
  Width  = 661
  Height = 375+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-30"
  HelpContextID=188078743
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  TOT_MAST_IDX = 0
  TOT_DETT_IDX = 0
  TAB_FONT_IDX = 0
  TAB_MISU_IDX = 0
  cFile = "TOT_MAST"
  cFileDetail = "TOT_DETT"
  cKeySelect = "TICODICE"
  cKeyWhere  = "TICODICE=this.w_TICODICE"
  cKeyDetail  = "TICODICE=this.w_TICODICE"
  cKeyWhereODBC = '"TICODICE="+cp_ToStrODBC(this.w_TICODICE)';

  cKeyDetailWhereODBC = '"TICODICE="+cp_ToStrODBC(this.w_TICODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"TOT_DETT.TICODICE="+cp_ToStrODBC(this.w_TICODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'TOT_DETT.CPROWORD '
  cPrg = "gscg_mti"
  cComment = "Totalizzatori IVA"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TICODICE = space(10)
  o_TICODICE = space(10)
  w_TIDESCRI = space(40)
  w_TITIPTOT = space(1)
  o_TITIPTOT = space(1)
  w_TI_FONTE = space(10)
  o_TI_FONTE = space(10)
  w_TIFLDECI = space(1)
  w_CPROWORD = 0
  w_TICODMIS = space(15)
  w_TIMFONTE = space(10)
  w_TICODCOM = space(15)
  w_TIDESMIS = space(80)
  w_CODMIS = space(15)
  w_CODCOM = space(15)
  w_TITIPMIS = space(1)
  w_TICODFAZ = space(15)
  w_TIFORMUL = space(254)
  w_DESFONT = space(40)
  w_HASEVENT = .F.
  w_HASEVCOP = space(50)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0

  * --- Children pointers
  GSCG_MDD = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_mti
  * --- Gestiti i soli eventi Modifica, cancellazione, inserimento
  * --- Lanciato solo se in stato di Query per prevenire il controllo quando si
  * --- ri preme il tasto in modifica / cariacamento
  * --- F6 controlla evasione righe associate alla stessa riga di origine
  Func ah_HasCPEvents(i_cOp)
  	This.w_HASEVCOP=i_cop
  	If ((Upper(i_cop)='ECPEDIT' Or Upper(i_cop)='ECPDELETE') And this.cFunction='Query') Or (Upper(This.cFunction)$"EDIT|LOAD" And Upper(i_cop)='ECPF6')
  		this.NotifyEvent('HasEvent')
  		return(this.w_HASEVENT)
  	Else		
  		return(.t.)
  	endif
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TOT_MAST','gscg_mti')
    stdPageFrame::Init()
    *set procedure to GSCG_MDD additive
    with this
      .Pages(1).addobject("oPag","tgscg_mtiPag1","gscg_mti",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Totalizzatori")
      .Pages(1).HelpContextID = 40368576
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTICODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCG_MDD
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='TAB_FONT'
    this.cWorkTables[2]='TAB_MISU'
    this.cWorkTables[3]='TOT_MAST'
    this.cWorkTables[4]='TOT_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TOT_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TOT_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSCG_MDD = CREATEOBJECT('stdLazyChild',this,'GSCG_MDD')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_MDD)
      this.GSCG_MDD.DestroyChildrenChain()
      this.GSCG_MDD=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_MDD.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_MDD.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_MDD.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSCG_MDD.ChangeRow(this.cRowID+'      1',1;
             ,.w_TICODICE,"DICODTOT";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_TICODICE = NVL(TICODICE,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from TOT_MAST where TICODICE=KeySet.TICODICE
    *
    i_nConn = i_TableProp[this.TOT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TOT_MAST_IDX,2],this.bLoadRecFilter,this.TOT_MAST_IDX,"gscg_mti")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TOT_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TOT_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"TOT_DETT.","TOT_MAST.")
      i_cTable = i_cTable+' TOT_MAST '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TICODICE',this.w_TICODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_DESFONT = space(40)
        .w_HASEVENT = .f.
        .w_HASEVCOP = space(50)
        .w_TICODICE = NVL(TICODICE,space(10))
        .w_TIDESCRI = NVL(TIDESCRI,space(40))
        .w_TITIPTOT = NVL(TITIPTOT,space(1))
        .w_TI_FONTE = NVL(TI_FONTE,space(10))
          if link_1_4_joined
            this.w_TI_FONTE = NVL(FOCODICE104,NVL(this.w_TI_FONTE,space(10)))
            this.w_DESFONT = NVL(FODESCRI104,space(40))
          else
          .link_1_4('Load')
          endif
        .w_TIFLDECI = NVL(TIFLDECI,space(1))
        .oPgFrm.Page1.oPag.oObj_3_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        cp_LoadRecExtFlds(this,'TOT_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from TOT_DETT where TICODICE=KeySet.TICODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.TOT_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TOT_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('TOT_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "TOT_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" TOT_DETT"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'TICODICE',this.w_TICODICE  )
        select * from (i_cTable) TOT_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_TICODMIS = NVL(TICODMIS,space(15))
          * evitabile
          *.link_2_2('Load')
          .w_TIMFONTE = NVL(TIMFONTE,space(10))
          .w_TICODCOM = NVL(TICODCOM,space(15))
          .w_TIDESMIS = NVL(TIDESMIS,space(80))
        .w_CODMIS = .w_TICODMIS
        .w_CODCOM = .w_TICODCOM
          .w_TITIPMIS = NVL(TITIPMIS,space(1))
          .w_TICODFAZ = NVL(TICODFAZ,space(15))
          .w_TIFORMUL = NVL(TIFORMUL,space(254))
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_3_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_7.enabled = .oPgFrm.Page1.oPag.oBtn_1_7.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_8.enabled = .oPgFrm.Page1.oPag.oBtn_1_8.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TICODICE=space(10)
      .w_TIDESCRI=space(40)
      .w_TITIPTOT=space(1)
      .w_TI_FONTE=space(10)
      .w_TIFLDECI=space(1)
      .w_CPROWORD=10
      .w_TICODMIS=space(15)
      .w_TIMFONTE=space(10)
      .w_TICODCOM=space(15)
      .w_TIDESMIS=space(80)
      .w_CODMIS=space(15)
      .w_CODCOM=space(15)
      .w_TITIPMIS=space(1)
      .w_TICODFAZ=space(15)
      .w_TIFORMUL=space(254)
      .w_DESFONT=space(40)
      .w_HASEVENT=.f.
      .w_HASEVCOP=space(50)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_TITIPTOT = 'S'
        .w_TI_FONTE = Space(10)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_TI_FONTE))
         .link_1_4('Full')
        endif
        .w_TIFLDECI = 'N'
        .DoRTCalc(6,6,.f.)
        .w_TICODMIS = Space(15)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_TICODMIS))
         .link_2_2('Full')
        endif
        .w_TIMFONTE = .w_TI_FONTE
        .w_TICODCOM = Space(15)
        .w_TIDESMIS = SPACE(80)
        .w_CODMIS = .w_TICODMIS
        .w_CODCOM = .w_TICODCOM
        .w_TITIPMIS = 'G'
        .w_TICODFAZ = SPACE(15)
        .w_TIFORMUL = ''
        .oPgFrm.Page1.oPag.oObj_3_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TOT_MAST')
    this.DoRTCalc(16,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTICODICE_1_1.enabled = i_bVal
      .Page1.oPag.oTIDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oTITIPTOT_1_3.enabled = i_bVal
      .Page1.oPag.oTI_FONTE_1_4.enabled = i_bVal
      .Page1.oPag.oTIFLDECI_1_5.enabled = i_bVal
      .Page1.oPag.oTITIPMIS_2_8.enabled = i_bVal
      .Page1.oPag.oTIFORMUL_2_10.enabled = i_bVal
      .Page1.oPag.oBtn_1_7.enabled = .Page1.oPag.oBtn_1_7.mCond()
      .Page1.oPag.oBtn_1_8.enabled = .Page1.oPag.oBtn_1_8.mCond()
      .Page1.oPag.oBtn_1_13.enabled = i_bVal
      .Page1.oPag.oObj_3_1.enabled = i_bVal
      .Page1.oPag.oObj_1_14.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTICODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTICODICE_1_1.enabled = .t.
      endif
    endwith
    this.GSCG_MDD.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'TOT_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_MDD.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TOT_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TICODICE,"TICODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TIDESCRI,"TIDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TITIPTOT,"TITIPTOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TI_FONTE,"TI_FONTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TIFLDECI,"TIFLDECI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TOT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TOT_MAST_IDX,2])
    i_lTable = "TOT_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TOT_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCG_STI with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_TICODMIS C(15);
      ,t_TICODCOM C(15);
      ,t_TIDESMIS C(80);
      ,t_TITIPMIS N(3);
      ,t_TICODFAZ C(15);
      ,t_TIFORMUL C(254);
      ,CPROWNUM N(10);
      ,t_TIMFONTE C(10);
      ,t_CODMIS C(15);
      ,t_CODCOM C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mtibodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTICODMIS_2_2.controlsource=this.cTrsName+'.t_TICODMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTICODCOM_2_4.controlsource=this.cTrsName+'.t_TICODCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTIDESMIS_2_5.controlsource=this.cTrsName+'.t_TIDESMIS'
    this.oPgFRm.Page1.oPag.oTITIPMIS_2_8.controlsource=this.cTrsName+'.t_TITIPMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTICODFAZ_2_9.controlsource=this.cTrsName+'.t_TICODFAZ'
    this.oPgFRm.Page1.oPag.oTIFORMUL_2_10.controlsource=this.cTrsName+'.t_TIFORMUL'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(45)
    this.AddVLine(167)
    this.AddVLine(504)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TOT_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TOT_MAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TOT_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TOT_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'TOT_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(TICODICE,TIDESCRI,TITIPTOT,TI_FONTE,TIFLDECI"+;
                  ",UTCC,UTDC,UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_TICODICE)+;
                    ","+cp_ToStrODBC(this.w_TIDESCRI)+;
                    ","+cp_ToStrODBC(this.w_TITIPTOT)+;
                    ","+cp_ToStrODBCNull(this.w_TI_FONTE)+;
                    ","+cp_ToStrODBC(this.w_TIFLDECI)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TOT_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'TOT_MAST')
        cp_CheckDeletedKey(i_cTable,0,'TICODICE',this.w_TICODICE)
        INSERT INTO (i_cTable);
              (TICODICE,TIDESCRI,TITIPTOT,TI_FONTE,TIFLDECI,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_TICODICE;
                  ,this.w_TIDESCRI;
                  ,this.w_TITIPTOT;
                  ,this.w_TI_FONTE;
                  ,this.w_TIFLDECI;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TOT_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TOT_DETT_IDX,2])
      *
      * insert into TOT_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(TICODICE,CPROWORD,TICODMIS,TIMFONTE,TICODCOM"+;
                  ",TIDESMIS,TITIPMIS,TICODFAZ,TIFORMUL,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TICODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_TICODMIS)+","+cp_ToStrODBC(this.w_TIMFONTE)+","+cp_ToStrODBC(this.w_TICODCOM)+;
             ","+cp_ToStrODBC(this.w_TIDESMIS)+","+cp_ToStrODBC(this.w_TITIPMIS)+","+cp_ToStrODBC(this.w_TICODFAZ)+","+cp_ToStrODBC(this.w_TIFORMUL)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'TICODICE',this.w_TICODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_TICODICE,this.w_CPROWORD,this.w_TICODMIS,this.w_TIMFONTE,this.w_TICODCOM"+;
                ",this.w_TIDESMIS,this.w_TITIPMIS,this.w_TICODFAZ,this.w_TIFORMUL,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.TOT_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TOT_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update TOT_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'TOT_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " TIDESCRI="+cp_ToStrODBC(this.w_TIDESCRI)+;
             ",TITIPTOT="+cp_ToStrODBC(this.w_TITIPTOT)+;
             ",TI_FONTE="+cp_ToStrODBCNull(this.w_TI_FONTE)+;
             ",TIFLDECI="+cp_ToStrODBC(this.w_TIFLDECI)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'TOT_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'TICODICE',this.w_TICODICE  )
          UPDATE (i_cTable) SET;
              TIDESCRI=this.w_TIDESCRI;
             ,TITIPTOT=this.w_TITIPTOT;
             ,TI_FONTE=this.w_TI_FONTE;
             ,TIFLDECI=this.w_TIFLDECI;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_TICODMIS)) OR not(Empty(t_TICODCOM))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.TOT_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.TOT_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from TOT_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TOT_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",TICODMIS="+cp_ToStrODBCNull(this.w_TICODMIS)+;
                     ",TIMFONTE="+cp_ToStrODBC(this.w_TIMFONTE)+;
                     ",TICODCOM="+cp_ToStrODBC(this.w_TICODCOM)+;
                     ",TIDESMIS="+cp_ToStrODBC(this.w_TIDESMIS)+;
                     ",TITIPMIS="+cp_ToStrODBC(this.w_TITIPMIS)+;
                     ",TICODFAZ="+cp_ToStrODBC(this.w_TICODFAZ)+;
                     ",TIFORMUL="+cp_ToStrODBC(this.w_TIFORMUL)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,TICODMIS=this.w_TICODMIS;
                     ,TIMFONTE=this.w_TIMFONTE;
                     ,TICODCOM=this.w_TICODCOM;
                     ,TIDESMIS=this.w_TIDESMIS;
                     ,TITIPMIS=this.w_TITIPMIS;
                     ,TICODFAZ=this.w_TICODFAZ;
                     ,TIFORMUL=this.w_TIFORMUL;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSCG_MDD : Saving
      this.GSCG_MDD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_TICODICE,"DICODTOT";
             )
      this.GSCG_MDD.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gscg_mti
    if not(bTrsErr)
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCG_MDD : Deleting
    this.GSCG_MDD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_TICODICE,"DICODTOT";
           )
    this.GSCG_MDD.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_TICODMIS)) OR not(Empty(t_TICODCOM))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.TOT_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.TOT_DETT_IDX,2])
        *
        * delete TOT_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.TOT_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.TOT_MAST_IDX,2])
        *
        * delete TOT_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_TICODMIS)) OR not(Empty(t_TICODCOM))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gscg_mti
    if not(bTrsErr)
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
    endif
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TOT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TOT_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_TITIPTOT<>.w_TITIPTOT
          .w_TI_FONTE = Space(10)
          .link_1_4('Full')
        endif
        if .o_TITIPTOT<>.w_TITIPTOT
          .w_TIFLDECI = 'N'
        endif
        .DoRTCalc(6,6,.t.)
        if .o_TITIPTOT<>.w_TITIPTOT
          .w_TICODMIS = Space(15)
          .link_2_2('Full')
        endif
        if .o_TI_FONTE<>.w_TI_FONTE
          .w_TIMFONTE = .w_TI_FONTE
        endif
        if .o_TITIPTOT<>.w_TITIPTOT
          .w_TICODCOM = Space(15)
        endif
        if .o_TITIPTOT<>.w_TITIPTOT
          .w_TIDESMIS = SPACE(80)
        endif
        if .o_TICODICE<>.w_TICODICE
          .w_CODMIS = .w_TICODMIS
        endif
        if .o_TICODICE<>.w_TICODICE
          .w_CODCOM = .w_TICODCOM
        endif
        .DoRTCalc(13,13,.t.)
        if .o_TITIPTOT<>.w_TITIPTOT
          .w_TICODFAZ = SPACE(15)
        endif
        if .o_TITIPTOT<>.w_TITIPTOT
          .w_TIFORMUL = ''
        endif
        .oPgFrm.Page1.oPag.oObj_3_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TIMFONTE with this.w_TIMFONTE
      replace t_CODMIS with this.w_CODMIS
      replace t_CODCOM with this.w_CODCOM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_3_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTITIPTOT_1_3.enabled = this.oPgFrm.Page1.oPag.oTITIPTOT_1_3.mCond()
    this.oPgFrm.Page1.oPag.oTI_FONTE_1_4.enabled = this.oPgFrm.Page1.oPag.oTI_FONTE_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_6.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_6.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTICODCOM_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTICODCOM_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTIDESMIS_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTIDESMIS_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTICODFAZ_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTICODFAZ_2_9.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTI_FONTE_1_4.visible=!this.oPgFrm.Page1.oPag.oTI_FONTE_1_4.mHide()
    this.oPgFrm.Page1.oPag.oTIFLDECI_1_5.visible=!this.oPgFrm.Page1.oPag.oTIFLDECI_1_5.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_8.visible=!this.oPgFrm.Page1.oPag.oBtn_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oDESFONT_1_11.visible=!this.oPgFrm.Page1.oPag.oDESFONT_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_13.visible=!this.oPgFrm.Page1.oPag.oBtn_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODMIS_2_2.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODMIS_2_2.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODCOM_2_4.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODCOM_2_4.mHide()
    this.oPgFrm.Page1.oPag.oTITIPMIS_2_8.visible=!this.oPgFrm.Page1.oPag.oTITIPMIS_2_8.mHide()
    this.oPgFrm.Page1.oPag.oTIFORMUL_2_10.visible=!this.oPgFrm.Page1.oPag.oTIFORMUL_2_10.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_3_1.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TI_FONTE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_FONT_IDX,3]
    i_lTable = "TAB_FONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2], .t., this.TAB_FONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TI_FONTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TAB_FONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FOCODICE like "+cp_ToStrODBC(trim(this.w_TI_FONTE)+"%");

          i_ret=cp_SQL(i_nConn,"select FOCODICE,FODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FOCODICE',trim(this.w_TI_FONTE))
          select FOCODICE,FODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TI_FONTE)==trim(_Link_.FOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TI_FONTE) and !this.bDontReportError
            deferred_cp_zoom('TAB_FONT','*','FOCODICE',cp_AbsName(oSource.parent,'oTI_FONTE_1_4'),i_cWhere,'',"Elenco fonti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODICE,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODICE',oSource.xKey(1))
            select FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TI_FONTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODICE,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(this.w_TI_FONTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODICE',this.w_TI_FONTE)
            select FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TI_FONTE = NVL(_Link_.FOCODICE,space(10))
      this.w_DESFONT = NVL(_Link_.FODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TI_FONTE = space(10)
      endif
      this.w_DESFONT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])+'\'+cp_ToStr(_Link_.FOCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_FONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TI_FONTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_FONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.FOCODICE as FOCODICE104"+ ",link_1_4.FODESCRI as FODESCRI104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on TOT_MAST.TI_FONTE=link_1_4.FOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and TOT_MAST.TI_FONTE=link_1_4.FOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TICODMIS
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_MISU_IDX,3]
    i_lTable = "TAB_MISU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_MISU_IDX,2], .t., this.TAB_MISU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_MISU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TICODMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TAB_MISU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MICODICE like "+cp_ToStrODBC(trim(this.w_TICODMIS)+"%");
                   +" and MI_FONTE="+cp_ToStrODBC(this.w_TIMFONTE);

          i_ret=cp_SQL(i_nConn,"select MI_FONTE,MICODICE,MIDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MI_FONTE,MICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MI_FONTE',this.w_TIMFONTE;
                     ,'MICODICE',trim(this.w_TICODMIS))
          select MI_FONTE,MICODICE,MIDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MI_FONTE,MICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TICODMIS)==trim(_Link_.MICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TICODMIS) and !this.bDontReportError
            deferred_cp_zoom('TAB_MISU','*','MI_FONTE,MICODICE',cp_AbsName(oSource.parent,'oTICODMIS_2_2'),i_cWhere,'',"Elenco misure",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIMFONTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MI_FONTE,MICODICE,MIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select MI_FONTE,MICODICE,MIDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MI_FONTE,MICODICE,MIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and MI_FONTE="+cp_ToStrODBC(this.w_TIMFONTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MI_FONTE',oSource.xKey(1);
                       ,'MICODICE',oSource.xKey(2))
            select MI_FONTE,MICODICE,MIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TICODMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MI_FONTE,MICODICE,MIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MICODICE="+cp_ToStrODBC(this.w_TICODMIS);
                   +" and MI_FONTE="+cp_ToStrODBC(this.w_TIMFONTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MI_FONTE',this.w_TIMFONTE;
                       ,'MICODICE',this.w_TICODMIS)
            select MI_FONTE,MICODICE,MIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TICODMIS = NVL(_Link_.MICODICE,space(15))
      this.w_TIDESMIS = NVL(_Link_.MIDESCRI,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_TICODMIS = space(15)
      endif
      this.w_TIDESMIS = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_MISU_IDX,2])+'\'+cp_ToStr(_Link_.MI_FONTE,1)+'\'+cp_ToStr(_Link_.MICODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_MISU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TICODMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTICODICE_1_1.value==this.w_TICODICE)
      this.oPgFrm.Page1.oPag.oTICODICE_1_1.value=this.w_TICODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIDESCRI_1_2.value==this.w_TIDESCRI)
      this.oPgFrm.Page1.oPag.oTIDESCRI_1_2.value=this.w_TIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTITIPTOT_1_3.RadioValue()==this.w_TITIPTOT)
      this.oPgFrm.Page1.oPag.oTITIPTOT_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTI_FONTE_1_4.value==this.w_TI_FONTE)
      this.oPgFrm.Page1.oPag.oTI_FONTE_1_4.value=this.w_TI_FONTE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIFLDECI_1_5.RadioValue()==this.w_TIFLDECI)
      this.oPgFrm.Page1.oPag.oTIFLDECI_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTITIPMIS_2_8.RadioValue()==this.w_TITIPMIS)
      this.oPgFrm.Page1.oPag.oTITIPMIS_2_8.SetRadio()
      replace t_TITIPMIS with this.oPgFrm.Page1.oPag.oTITIPMIS_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTIFORMUL_2_10.value==this.w_TIFORMUL)
      this.oPgFrm.Page1.oPag.oTIFORMUL_2_10.value=this.w_TIFORMUL
      replace t_TIFORMUL with this.oPgFrm.Page1.oPag.oTIFORMUL_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFONT_1_11.value==this.w_DESFONT)
      this.oPgFrm.Page1.oPag.oDESFONT_1_11.value=this.w_DESFONT
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODMIS_2_2.value==this.w_TICODMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODMIS_2_2.value=this.w_TICODMIS
      replace t_TICODMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODMIS_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODCOM_2_4.value==this.w_TICODCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODCOM_2_4.value=this.w_TICODCOM
      replace t_TICODCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODCOM_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIDESMIS_2_5.value==this.w_TIDESMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIDESMIS_2_5.value=this.w_TIDESMIS
      replace t_TIDESMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIDESMIS_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODFAZ_2_9.value==this.w_TICODFAZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODFAZ_2_9.value=this.w_TICODFAZ
      replace t_TICODFAZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTICODFAZ_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'TOT_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TI_FONTE))  and not(.w_TITIPTOT<>'S')  and (.cFunction='Load')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oTI_FONTE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_TI_FONTE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCG_MDD.CheckForm()
      if i_bres
        i_bres=  .GSCG_MDD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_TICODMIS)) OR not(Empty(.w_TICODCOM))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TICODICE = this.w_TICODICE
    this.o_TITIPTOT = this.w_TITIPTOT
    this.o_TI_FONTE = this.w_TI_FONTE
    * --- GSCG_MDD : Depends On
    this.GSCG_MDD.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_TICODMIS)) OR not(Empty(t_TICODCOM)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_TICODMIS=space(15)
      .w_TIMFONTE=space(10)
      .w_TICODCOM=space(15)
      .w_TIDESMIS=space(80)
      .w_CODMIS=space(15)
      .w_CODCOM=space(15)
      .w_TITIPMIS=space(1)
      .w_TICODFAZ=space(15)
      .w_TIFORMUL=space(254)
      .DoRTCalc(1,6,.f.)
        .w_TICODMIS = Space(15)
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_TICODMIS))
        .link_2_2('Full')
      endif
        .w_TIMFONTE = .w_TI_FONTE
        .w_TICODCOM = Space(15)
        .w_TIDESMIS = SPACE(80)
        .w_CODMIS = .w_TICODMIS
        .w_CODCOM = .w_TICODCOM
        .w_TITIPMIS = 'G'
        .w_TICODFAZ = SPACE(15)
        .w_TIFORMUL = ''
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
    endwith
    this.DoRTCalc(16,22,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_TICODMIS = t_TICODMIS
    this.w_TIMFONTE = t_TIMFONTE
    this.w_TICODCOM = t_TICODCOM
    this.w_TIDESMIS = t_TIDESMIS
    this.w_CODMIS = t_CODMIS
    this.w_CODCOM = t_CODCOM
    this.w_TITIPMIS = this.oPgFrm.Page1.oPag.oTITIPMIS_2_8.RadioValue(.t.)
    this.w_TICODFAZ = t_TICODFAZ
    this.w_TIFORMUL = t_TIFORMUL
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_TICODMIS with this.w_TICODMIS
    replace t_TIMFONTE with this.w_TIMFONTE
    replace t_TICODCOM with this.w_TICODCOM
    replace t_TIDESMIS with this.w_TIDESMIS
    replace t_CODMIS with this.w_CODMIS
    replace t_CODCOM with this.w_CODCOM
    replace t_TITIPMIS with this.oPgFrm.Page1.oPag.oTITIPMIS_2_8.ToRadio()
    replace t_TICODFAZ with this.w_TICODFAZ
    replace t_TIFORMUL with this.w_TIFORMUL
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mtiPag1 as StdContainer
  Width  = 657
  height = 375
  stdWidth  = 657
  stdheight = 375
  resizeXpos=310
  resizeYpos=219
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTICODICE_1_1 as StdField with uid="FHBJWUJWHR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TICODICE", cQueryName = "TICODICE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice totalizzatore",;
    HelpContextID = 121022085,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=100, Left=71, Top=20, InputMask=replicate('X',10)

  add object oTIDESCRI_1_2 as StdField with uid="DXJRMJITOR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TIDESCRI", cQueryName = "TIDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione totalizzatore",;
    HelpContextID = 61827455,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=173, Top=20, InputMask=replicate('X',40)


  add object oTITIPTOT_1_3 as StdCombo with uid="JZMBWBOPSM",rtseq=3,rtrep=.f.,left=504,top=20,width=139,height=21;
    , ToolTipText = "Tipo totalizzatore IVA (composto,descrittivo,manuale,semplice)";
    , HelpContextID = 75786634;
    , cFormVar="w_TITIPTOT",RowSource=""+"Composto,"+"Descrittivo,"+"Manuale,"+"Semplice", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTITIPTOT_1_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TITIPTOT,&i_cF..t_TITIPTOT),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'D',;
    iif(xVal =3,'M',;
    iif(xVal =4,'S',;
    space(1))))))
  endfunc
  func oTITIPTOT_1_3.GetRadio()
    this.Parent.oContained.w_TITIPTOT = this.RadioValue()
    return .t.
  endfunc

  func oTITIPTOT_1_3.ToRadio()
    this.Parent.oContained.w_TITIPTOT=trim(this.Parent.oContained.w_TITIPTOT)
    return(;
      iif(this.Parent.oContained.w_TITIPTOT=='C',1,;
      iif(this.Parent.oContained.w_TITIPTOT=='D',2,;
      iif(this.Parent.oContained.w_TITIPTOT=='M',3,;
      iif(this.Parent.oContained.w_TITIPTOT=='S',4,;
      0)))))
  endfunc

  func oTITIPTOT_1_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTITIPTOT_1_3.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  add object oTI_FONTE_1_4 as StdField with uid="EDATBDEVBL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TI_FONTE", cQueryName = "TI_FONTE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice fonte associata",;
    HelpContextID = 26076805,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=71, Top=48, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TAB_FONT", oKey_1_1="FOCODICE", oKey_1_2="this.w_TI_FONTE"

  func oTI_FONTE_1_4.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  func oTI_FONTE_1_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TITIPTOT<>'S')
    endwith
    endif
  endfunc

  func oTI_FONTE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTI_FONTE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTI_FONTE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_FONT','*','FOCODICE',cp_AbsName(this.parent,'oTI_FONTE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco fonti",'',this.parent.oContained
  endproc

  add object oTIFLDECI_1_5 as StdCheck with uid="NMXPRGAPWQ",rtseq=5,rtrep=.f.,left=70, top=74, caption="Flag importi con decimali",;
    HelpContextID = 188315265,;
    cFormVar="w_TIFLDECI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIFLDECI_1_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TIFLDECI,&i_cF..t_TIFLDECI),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oTIFLDECI_1_5.GetRadio()
    this.Parent.oContained.w_TIFLDECI = this.RadioValue()
    return .t.
  endfunc

  func oTIFLDECI_1_5.ToRadio()
    this.Parent.oContained.w_TIFLDECI=trim(this.Parent.oContained.w_TIFLDECI)
    return(;
      iif(this.Parent.oContained.w_TIFLDECI=='S',1,;
      0))
  endfunc

  func oTIFLDECI_1_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTIFLDECI_1_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TITIPTOT='D' or Islron())
    endwith
    endif
  endfunc


  add object oLinkPC_1_6 as StdButton with uid="NHDDBXOBMQ",left=505, top=48, width=48,height=45,;
    CpPicture="bmp\filtra.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere all'elenco delle dimensioni associate";
    , HelpContextID = 92440709;
    , Caption='D\<imen.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_6.Click()
      this.Parent.oContained.GSCG_MDD.LinkPCClick()
    endproc

  func oLinkPC_1_6.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_TI_FONTE) AND .w_TITIPTOT='S')
    endwith
  endfunc


  add object oBtn_1_7 as StdButton with uid="AYGEVEEZAU",left=556, top=48, width=48,height=45,;
    CpPicture="COPY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per duplicare il totalizzatore";
    , HelpContextID = 180792266;
    , Caption='\<Duplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      do gscg_kdd with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_TICODICE))
    endwith
  endfunc

  func oBtn_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction <>'Query')
    endwith
   endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="ITVMSDNBJW",left=607, top=48, width=48,height=45,;
    CpPicture="bmp\calcola.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per calcolare il valore del totalizzatore";
    , HelpContextID = 24117210;
    , Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      do gscg_szf with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction <>'Query')
    endwith
   endif
  endfunc

  add object oDESFONT_1_11 as StdField with uid="PPWXPYUANC",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESFONT", cQueryName = "DESFONT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione fonte",;
    HelpContextID = 26127306,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=173, Top=48, InputMask=replicate('X',40)

  func oDESFONT_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TITIPTOT<>'S')
    endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="BSYREWDYJA",left=595, top=329, width=48,height=45,;
    CpPicture="bmp\compone.bmp", caption="", nPag=1;
    , ToolTipText = "premere per impostare formula totalizzatore composto";
    , HelpContextID = 17142186;
    , Caption='\<Formula';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do gscg_kcf with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TITIPTOT<>'C')
    endwith
   endif
  endfunc


  add object oObj_1_14 as cp_runprogram with uid="VZVWNSSOSM",left=229, top=426, width=241,height=18,;
    caption='GSCG_BKT(B)',;
   bGlobalFont=.t.,;
    prg="GSCG_BKT('B')",;
    cEvent = "w_TI_FONTE Changed",;
    nPag=1;
    , HelpContextID = 57947962


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=103, width=642,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Riga",Field2="TICODMIS",Label2="Misura",Field3="TICODCOM",Label3="",Field4="TIDESMIS",Label4="Descrizione",Field5="TICODFAZ",Label5="Codice fisco azienda",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 32701318

  add object oStr_1_9 as StdString with uid="DSOKKISYQY",Visible=.t., Left=8, Top=50,;
    Alignment=1, Width=60, Height=18,;
    Caption="Fonte:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_TITIPTOT<>'S')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="UTMDCYLENS",Visible=.t., Left=5, Top=22,;
    Alignment=1, Width=63, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="EXZSPDQVSY",Visible=.t., Left=27, Top=352,;
    Alignment=1, Width=55, Height=18,;
    Caption="Formula:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_TITIPTOT<>'C')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="COUKWPNYPK",Visible=.t., Left=4, Top=321,;
    Alignment=1, Width=78, Height=18,;
    Caption="Tipo misura:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (! Islron() OR .w_TITIPTOT= 'D')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=122,;
    width=638+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=123,width=637+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TAB_MISU|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oTITIPMIS_2_8.Refresh()
      this.Parent.oTIFORMUL_2_10.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TAB_MISU'
        oDropInto=this.oBodyCol.oRow.oTICODMIS_2_2
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oTITIPMIS_2_8 as StdTrsCombo with uid="JRLFTNLRBM",rtrep=.t.,;
    cFormVar="w_TITIPMIS", RowSource=""+"Imponibile,"+"Imposta,"+"Generica" , ;
    ToolTipText = "Tipologia misura",;
    HelpContextID = 41653879,;
    Height=25, Width=156, Left=84, Top=321,;
    cTotal="", cQueryName = "TITIPMIS",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTITIPMIS_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TITIPMIS,&i_cF..t_TITIPMIS),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'M',;
    iif(xVal =3,'G',;
    space(1)))))
  endfunc
  func oTITIPMIS_2_8.GetRadio()
    this.Parent.oContained.w_TITIPMIS = this.RadioValue()
    return .t.
  endfunc

  func oTITIPMIS_2_8.ToRadio()
    this.Parent.oContained.w_TITIPMIS=trim(this.Parent.oContained.w_TITIPMIS)
    return(;
      iif(this.Parent.oContained.w_TITIPMIS=='I',1,;
      iif(this.Parent.oContained.w_TITIPMIS=='M',2,;
      iif(this.Parent.oContained.w_TITIPMIS=='G',3,;
      0))))
  endfunc

  func oTITIPMIS_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTITIPMIS_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! Islron() OR .w_TITIPTOT= 'D')
    endwith
    endif
  endfunc

  add object oTIFORMUL_2_10 as StdTrsField with uid="LETWIAYXZH",rtseq=15,rtrep=.t.,;
    cFormVar="w_TIFORMUL",value=space(254),;
    ToolTipText = "Formula totalizzatore complesso",;
    HelpContextID = 39220862,;
    cTotal="", bFixedPos=.t., cQueryName = "TIFORMUL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=501, Left=84, Top=350, InputMask=replicate('X',254), tabstop = .f., readonly = .t.

  func oTIFORMUL_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TITIPTOT<>'C')
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oObj_3_1 as cp_runprogram with uid="AJGDZEWMQR",width=206,height=18,;
   left=5, top=425,;
    caption='GSCG_BKT(A)',;
   bGlobalFont=.t.,;
    prg="GSCG_BKT('A')",;
    cEvent = "ControlliFinali",;
    nPag=3;
    , HelpContextID = 57947706
enddefine

* --- Defining Body row
define class tgscg_mtiBodyRow as CPBodyRowCnt
  Width=628
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="WFYNCBBEIH",rtseq=6,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 268062570,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0

  add object oTICODMIS_2_2 as StdTrsField with uid="ERAJDKQNEB",rtseq=7,rtrep=.t.,;
    cFormVar="w_TICODMIS",value=space(15),;
    HelpContextID = 53913207,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=41, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TAB_MISU", oKey_1_1="MI_FONTE", oKey_1_2="this.w_TIMFONTE", oKey_2_1="MICODICE", oKey_2_2="this.w_TICODMIS"

  func oTICODMIS_2_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TITIPTOT<>'S')
    endwith
    endif
  endfunc

  func oTICODMIS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oTICODMIS_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oTICODMIS_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TAB_MISU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"MI_FONTE="+cp_ToStrODBC(this.Parent.oContained.w_TIMFONTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"MI_FONTE="+cp_ToStr(this.Parent.oContained.w_TIMFONTE)
    endif
    do cp_zoom with 'TAB_MISU','*','MI_FONTE,MICODICE',cp_AbsName(this.parent,'oTICODMIS_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco misure",'',this.parent.oContained
  endproc

  add object oTICODCOM_2_4 as StdTrsField with uid="TRXVXZRVRL",rtseq=9,rtrep=.t.,;
    cFormVar="w_TICODCOM",value=space(15),;
    ToolTipText = "Codice misura totalizzatore composto",;
    HelpContextID = 46750083,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=41, Top=0, InputMask=replicate('X',15)

  func oTICODCOM_2_4.mCond()
    with this.Parent.oContained
      return (.w_TITIPTOT<>'D')
    endwith
  endfunc

  func oTICODCOM_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TITIPTOT='S')
    endwith
    endif
  endfunc

  add object oTIDESMIS_2_5 as StdTrsField with uid="NBJPSLDEYK",rtseq=10,rtrep=.t.,;
    cFormVar="w_TIDESMIS",value=space(80),;
    HelpContextID = 38835831,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=332, Left=165, Top=0, InputMask=replicate('X',80)

  func oTIDESMIS_2_5.mCond()
    with this.Parent.oContained
      return (.w_TITIPTOT<>'D')
    endwith
  endfunc

  add object oTICODFAZ_2_9 as StdTrsField with uid="ASDASYJZQR",rtseq=14,rtrep=.t.,;
    cFormVar="w_TICODFAZ",value=space(15),;
    HelpContextID = 97081744,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=122, Left=501, Top=0, InputMask=replicate('X',15)

  func oTICODFAZ_2_9.mCond()
    with this.Parent.oContained
      return (.w_TITIPTOT<>'D')
    endwith
  endfunc

  add object oObj_2_11 as cp_runprogram with uid="FWXOQIKJOF",width=227,height=18,;
   left=-1, top=324,;
    caption='GSCG_BKT(D)',;
   bGlobalFont=.t.,;
    prg="GSCG_BKT('D')",;
    cEvent = "w_TICODMIS Changed,w_TICODCOM Changed,HasEvent",;
    nPag=2;
    , HelpContextID = 57948474
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mti','TOT_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TICODICE=TOT_MAST.TICODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
