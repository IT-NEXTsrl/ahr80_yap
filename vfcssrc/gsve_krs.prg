* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_krs                                                        *
*              Ripartizione fattura di spese                                   *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_138]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-28                                                      *
* Last revis.: 2013-03-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_krs",oParentObject))

* --- Class definition
define class tgsve_krs as StdForm
  Top    = 3
  Left   = 3

  * --- Standard Properties
  Width  = 738
  Height = 476
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-03-21"
  HelpContextID=116011625
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=46

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  DOC_MAST_IDX = 0
  cPrg = "gsve_krs"
  cComment = "Ripartizione fattura di spese"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_MVTIPDOC = space(5)
  w_MVCODCON = space(15)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod('  /  /  ')
  w_MVCAOVAL = 0
  w_DESCAU = space(35)
  w_CATDOC = space(2)
  w_FLVEAC = space(1)
  w_MVTIPCON = space(1)
  o_MVTIPCON = space(1)
  w_DESINT = space(40)
  w_FLINTEQ = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_MVSERIAL = space(10)
  w_CPROWNUM = 0
  w_MVNUMRIF = 0
  w_CPROWORD = 0
  w_MVCODICE = space(41)
  w_MVDESART = space(40)
  w_MVVALMAG = 0
  w_MVCODVAL = space(10)
  o_MVCODVAL = space(10)
  w_CALCPICT = space(10)
  w_DESCAU2 = space(35)
  w_TIPDOC = space(5)
  w_CODCON = space(15)
  w_DESINT2 = space(40)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod('  /  /  ')
  w_TIPCON = space(1)
  w_SERIAL = space(10)
  w_CODVAL = space(3)
  w_TIPOPE = space(1)
  w_COLSHOW = 0
  w_COLEDIT = 0
  w_MVFLVEAC = space(1)
  w_CALCPICP = space(10)
  w_DECUNI = 0
  w_DECTOT = 0
  w_VALNAZ = space(3)
  w_DOCSER = space(10)
  w_CAOVAL = 0
  w_VALNAZ2 = space(3)
  w_FLVALO = space(1)
  w_GLVALMAG = 0
  w_Zoom = .NULL.
  w_DISAB = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_krsPag1","gsve_krs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMVTIPDOC_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    this.w_DISAB = this.oPgFrm.Pages(1).oPag.DISAB
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      this.w_DISAB = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='DOC_MAST'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MVTIPDOC=space(5)
      .w_MVCODCON=space(15)
      .w_MVNUMDOC=0
      .w_MVALFDOC=space(10)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_MVCAOVAL=0
      .w_DESCAU=space(35)
      .w_CATDOC=space(2)
      .w_FLVEAC=space(1)
      .w_MVTIPCON=space(1)
      .w_DESINT=space(40)
      .w_FLINTEQ=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_MVSERIAL=space(10)
      .w_CPROWNUM=0
      .w_MVNUMRIF=0
      .w_CPROWORD=0
      .w_MVCODICE=space(41)
      .w_MVDESART=space(40)
      .w_MVVALMAG=0
      .w_MVCODVAL=space(10)
      .w_CALCPICT=space(10)
      .w_DESCAU2=space(35)
      .w_TIPDOC=space(5)
      .w_CODCON=space(15)
      .w_DESINT2=space(40)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_TIPCON=space(1)
      .w_SERIAL=space(10)
      .w_CODVAL=space(3)
      .w_TIPOPE=space(1)
      .w_COLSHOW=0
      .w_COLEDIT=0
      .w_MVFLVEAC=space(1)
      .w_CALCPICP=space(10)
      .w_DECUNI=0
      .w_DECTOT=0
      .w_VALNAZ=space(3)
      .w_DOCSER=space(10)
      .w_CAOVAL=0
      .w_VALNAZ2=space(3)
      .w_FLVALO=space(1)
      .w_GLVALMAG=0
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_MVTIPDOC))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_MVCODCON))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,11,.f.)
        .w_FLINTEQ = IIF ( .w_MVTIPCON='N' , ' ' , .w_MVTIPCON )
        .w_OBTEST = i_DATSYS
        .DoRTCalc(14,22,.f.)
        if not(empty(.w_MVCODVAL))
          .link_1_29('Full')
        endif
        .w_CALCPICT = DEFPIP(.w_DECTOT)
        .DoRTCalc(24,25,.f.)
        if not(empty(.w_TIPDOC))
          .link_1_35('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CODCON))
          .link_1_36('Full')
        endif
      .oPgFrm.Page1.oPag.Zoom.Calculate(.w_SERIAL)
        .DoRTCalc(27,33,.f.)
        if not(empty(.w_CODVAL))
          .link_1_49('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_50.Calculate(IIF(EMPTY(.w_CODVAL),'','Importi espressi in '+ .w_CODVAL))
        .w_TIPOPE = 'N'
      .oPgFrm.Page1.oPag.oObj_1_55.Calculate(iif(.w_TIPOPE='N','1 - Selezione Riga Spese da ripartire','Selezione Riga Spese da stornare'),IIF(Empty(.w_MVSERIAL),.w_COLEDIT,.w_COLSHOW))
        .w_COLSHOW = 0
        .w_COLEDIT = RGB(255,0,0)
      .oPgFrm.Page1.oPag.oObj_1_58.Calculate(iif(.w_TIPOPE='N','2 - Selezione documento da rivalorizzare','Documento ripartito'),IIF(!Empty(.w_MVSERIAL) And Empty(.w_SERIAL)And .w_TIPOPE='N',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_59.Calculate(iif(.w_TIPOPE='N','3 - Ripartisci le spese sulle righe del documento selezionato','Ripartizione'),IIF(!Empty(.w_SERIAL) And .w_TIPOPE='N',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_61.Calculate(IIF( Empty( .w_MVCODVAL) Or  .w_TIPOPE='S','','Valuta '+ .w_MVCODVAL))
          .DoRTCalc(37,37,.f.)
        .w_CALCPICP = DEFPIC(.w_DECUNI)
      .oPgFrm.Page1.oPag.oObj_1_64.Calculate(IIF(EMPTY(.w_MVCODVAL),'','Valore ripartito in '+ .w_MVCODVAL),RGB(27,80,180))
          .DoRTCalc(39,40,.f.)
        .w_VALNAZ = iif( .w_TIPOPE='N' , .w_Zoom.getVar('VALNAZ') , .w_VALNAZ2 )
      .oPgFrm.Page1.oPag.DISAB.Calculate(.w_MVSERIAL)
      .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_79.Calculate(IIF( Empty( .w_MVCODVAL) Or  .w_TIPOPE='N','','Valuta '+ .w_MVCODVAL))
    endwith
    this.DoRTCalc(42,46,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,11,.t.)
        if .o_MVTIPCON<>.w_MVTIPCON
            .w_FLINTEQ = IIF ( .w_MVTIPCON='N' , ' ' , .w_MVTIPCON )
        endif
        .DoRTCalc(13,21,.t.)
          .link_1_29('Full')
        if .o_MVCODVAL<>.w_MVCODVAL
            .w_CALCPICT = DEFPIP(.w_DECTOT)
        endif
        .oPgFrm.Page1.oPag.Zoom.Calculate(.w_SERIAL)
        .DoRTCalc(24,32,.t.)
          .link_1_49('Full')
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate(IIF(EMPTY(.w_CODVAL),'','Importi espressi in '+ .w_CODVAL))
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(iif(.w_TIPOPE='N','1 - Selezione Riga Spese da ripartire','Selezione Riga Spese da stornare'),IIF(Empty(.w_MVSERIAL),.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(iif(.w_TIPOPE='N','2 - Selezione documento da rivalorizzare','Documento ripartito'),IIF(!Empty(.w_MVSERIAL) And Empty(.w_SERIAL)And .w_TIPOPE='N',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(iif(.w_TIPOPE='N','3 - Ripartisci le spese sulle righe del documento selezionato','Ripartizione'),IIF(!Empty(.w_SERIAL) And .w_TIPOPE='N',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(IIF( Empty( .w_MVCODVAL) Or  .w_TIPOPE='S','','Valuta '+ .w_MVCODVAL))
        .DoRTCalc(34,37,.t.)
        if .o_MVCODVAL<>.w_MVCODVAL
            .w_CALCPICP = DEFPIC(.w_DECUNI)
        endif
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate(IIF(EMPTY(.w_MVCODVAL),'','Valore ripartito in '+ .w_MVCODVAL),RGB(27,80,180))
        .DoRTCalc(39,40,.t.)
            .w_VALNAZ = iif( .w_TIPOPE='N' , .w_Zoom.getVar('VALNAZ') , .w_VALNAZ2 )
        .oPgFrm.Page1.oPag.DISAB.Calculate(.w_MVSERIAL)
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate(IIF( Empty( .w_MVCODVAL) Or  .w_TIPOPE='N','','Valuta '+ .w_MVCODVAL))
        * --- Area Manuale = Calculate
        * --- gsve_krs
        * -- pezzo di codice per lanciare la lettura dei dati di testata
        * -- potrebbe essere lanciato dall'after query dello zoom DISAB
        * -- ma non scatta
        if .w_TIPOPE='S' And Not Empty(.w_MVSERIAL)
        			.oPgFrm.Page1.oPag.DISAB.Calculate(.w_MVSERIAL)
        			.w_DOCSER=.w_Disab.getVar('MVSERIAL')
        			.NotifyEvent('Leggidoc')
        Endif
        
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(42,46,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom.Calculate(.w_SERIAL)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate(IIF(EMPTY(.w_CODVAL),'','Importi espressi in '+ .w_CODVAL))
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate(iif(.w_TIPOPE='N','1 - Selezione Riga Spese da ripartire','Selezione Riga Spese da stornare'),IIF(Empty(.w_MVSERIAL),.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(iif(.w_TIPOPE='N','2 - Selezione documento da rivalorizzare','Documento ripartito'),IIF(!Empty(.w_MVSERIAL) And Empty(.w_SERIAL)And .w_TIPOPE='N',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(iif(.w_TIPOPE='N','3 - Ripartisci le spese sulle righe del documento selezionato','Ripartizione'),IIF(!Empty(.w_SERIAL) And .w_TIPOPE='N',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate(IIF( Empty( .w_MVCODVAL) Or  .w_TIPOPE='S','','Valuta '+ .w_MVCODVAL))
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate(IIF(EMPTY(.w_MVCODVAL),'','Valore ripartito in '+ .w_MVCODVAL),RGB(27,80,180))
        .oPgFrm.Page1.oPag.DISAB.Calculate(.w_MVSERIAL)
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate(IIF( Empty( .w_MVCODVAL) Or  .w_TIPOPE='N','','Valuta '+ .w_MVCODVAL))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMVTIPDOC_1_1.enabled = this.oPgFrm.Page1.oPag.oMVTIPDOC_1_1.mCond()
    this.oPgFrm.Page1.oPag.oMVCODCON_1_2.enabled = this.oPgFrm.Page1.oPag.oMVCODCON_1_2.mCond()
    this.oPgFrm.Page1.oPag.oMVNUMDOC_1_3.enabled = this.oPgFrm.Page1.oPag.oMVNUMDOC_1_3.mCond()
    this.oPgFrm.Page1.oPag.oMVALFDOC_1_4.enabled = this.oPgFrm.Page1.oPag.oMVALFDOC_1_4.mCond()
    this.oPgFrm.Page1.oPag.oMVDATDOC_1_5.enabled = this.oPgFrm.Page1.oPag.oMVDATDOC_1_5.mCond()
    this.oPgFrm.Page1.oPag.oMVCAOVAL_1_9.enabled = this.oPgFrm.Page1.oPag.oMVCAOVAL_1_9.mCond()
    this.oPgFrm.Page1.oPag.oTIPDOC_1_35.enabled = this.oPgFrm.Page1.oPag.oTIPDOC_1_35.mCond()
    this.oPgFrm.Page1.oPag.oCODCON_1_36.enabled = this.oPgFrm.Page1.oPag.oCODCON_1_36.mCond()
    this.oPgFrm.Page1.oPag.oNUMDOC_1_38.enabled = this.oPgFrm.Page1.oPag.oNUMDOC_1_38.mCond()
    this.oPgFrm.Page1.oPag.oALFDOC_1_40.enabled = this.oPgFrm.Page1.oPag.oALFDOC_1_40.mCond()
    this.oPgFrm.Page1.oPag.oDATDOC_1_41.enabled = this.oPgFrm.Page1.oPag.oDATDOC_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMVCAOVAL_1_9.visible=!this.oPgFrm.Page1.oPag.oMVCAOVAL_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_71.visible=!this.oPgFrm.Page1.oPag.oStr_1_71.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_77.visible=!this.oPgFrm.Page1.oPag.oStr_1_77.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_78.visible=!this.oPgFrm.Page1.oPag.oStr_1_78.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_59.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
      .oPgFrm.Page1.oPag.DISAB.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_70.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_72.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_79.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVTIPDOC
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MVTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLVALO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MVTIPDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLVALO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMVTIPDOC_1_1'),i_cWhere,'',"Elenco causali documento",'GSVE5KRS.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLVALO";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLVALO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLVALO";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MVTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MVTIPDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLVALO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAU = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_MVTIPCON = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLVALO = NVL(_Link_.TDFLVALO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPDOC = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
      this.w_MVTIPCON = space(1)
      this.w_FLVALO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATDOC $ 'FA-NC' And .w_FLVALO='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente oppure incongruente (selezionare fatture o note di credito con check modifica valore attivo)")
        endif
        this.w_MVTIPDOC = space(5)
        this.w_DESCAU = space(35)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
        this.w_MVTIPCON = space(1)
        this.w_FLVALO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODCON
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MVCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MVTIPCON;
                     ,'ANCODICE',trim(this.w_MVCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oMVCODCON_1_2'),i_cWhere,'GSAR_BZC',"Elenco intestatari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Intestario inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MVCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MVTIPCON;
                       ,'ANCODICE',this.w_MVCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESINT = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODCON = space(15)
      endif
      this.w_DESINT = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Intestario inesistente o obsoleto")
        endif
        this.w_MVCODCON = space(15)
        this.w_DESINT = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODVAL
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MVCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MVCODVAL)
            select VACODVAL,VADECTOT,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODVAL = NVL(_Link_.VACODVAL,space(10))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_MVCODVAL = space(10)
      endif
      this.w_DECTOT = 0
      this.w_CAOVAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPDOC
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC,TDFLINTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_1_35'),i_cWhere,'',"Elenco causali documento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAU2 = NVL(_Link_.TDDESDOC,space(35))
      this.w_TIPCON = NVL(_Link_.TDFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESCAU2 = space(35)
      this.w_TIPCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_36'),i_cWhere,'GSAR_BZC',"Elenco intestatari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Intestario inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESINT2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESINT2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Intestario inesistente o obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_DESINT2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMVTIPDOC_1_1.value==this.w_MVTIPDOC)
      this.oPgFrm.Page1.oPag.oMVTIPDOC_1_1.value=this.w_MVTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODCON_1_2.value==this.w_MVCODCON)
      this.oPgFrm.Page1.oPag.oMVCODCON_1_2.value=this.w_MVCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oMVNUMDOC_1_3.value==this.w_MVNUMDOC)
      this.oPgFrm.Page1.oPag.oMVNUMDOC_1_3.value=this.w_MVNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVALFDOC_1_4.value==this.w_MVALFDOC)
      this.oPgFrm.Page1.oPag.oMVALFDOC_1_4.value=this.w_MVALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATDOC_1_5.value==this.w_MVDATDOC)
      this.oPgFrm.Page1.oPag.oMVDATDOC_1_5.value=this.w_MVDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCAOVAL_1_9.value==this.w_MVCAOVAL)
      this.oPgFrm.Page1.oPag.oMVCAOVAL_1_9.value=this.w_MVCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_10.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_10.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINT_1_15.value==this.w_DESINT)
      this.oPgFrm.Page1.oPag.oDESINT_1_15.value=this.w_DESINT
    endif
    if not(this.oPgFrm.Page1.oPag.oCPROWORD_1_25.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oCPROWORD_1_25.value=this.w_CPROWORD
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODICE_1_26.value==this.w_MVCODICE)
      this.oPgFrm.Page1.oPag.oMVCODICE_1_26.value=this.w_MVCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDESART_1_27.value==this.w_MVDESART)
      this.oPgFrm.Page1.oPag.oMVDESART_1_27.value=this.w_MVDESART
    endif
    if not(this.oPgFrm.Page1.oPag.oMVVALMAG_1_28.value==this.w_MVVALMAG)
      this.oPgFrm.Page1.oPag.oMVVALMAG_1_28.value=this.w_MVVALMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU2_1_33.value==this.w_DESCAU2)
      this.oPgFrm.Page1.oPag.oDESCAU2_1_33.value=this.w_DESCAU2
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_35.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_35.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_36.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_36.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINT2_1_37.value==this.w_DESINT2)
      this.oPgFrm.Page1.oPag.oDESINT2_1_37.value=this.w_DESINT2
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_38.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_38.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOC_1_40.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oALFDOC_1_40.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDOC_1_41.value==this.w_DATDOC)
      this.oPgFrm.Page1.oPag.oDATDOC_1_41.value=this.w_DATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPE_1_52.RadioValue()==this.w_TIPOPE)
      this.oPgFrm.Page1.oPag.oTIPOPE_1_52.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CATDOC $ 'FA-NC' And .w_FLVALO='S')  and (Empty ( .w_SERIAL ))  and not(empty(.w_MVTIPDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVTIPDOC_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente oppure incongruente (selezionare fatture o note di credito con check modifica valore attivo)")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_MVTIPCON$'C-F'  And  Empty ( .w_SERIAL ))  and not(empty(.w_MVCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCODCON_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intestario inesistente o obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_TIPCON$'C-F' And Empty( .w_DOCSER )  And  .w_TIPOPE='N')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intestario inesistente o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVTIPCON = this.w_MVTIPCON
    this.o_MVCODVAL = this.w_MVCODVAL
    return

enddefine

* --- Define pages as container
define class tgsve_krsPag1 as StdContainer
  Width  = 734
  height = 476
  stdWidth  = 734
  stdheight = 476
  resizeXpos=522
  resizeYpos=358
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVTIPDOC_1_1 as StdField with uid="IKEOEKAUYW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MVTIPDOC", cQueryName = "MVTIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente oppure incongruente (selezionare fatture o note di credito con check modifica valore attivo)",;
    ToolTipText = "Causale documento",;
    HelpContextID = 228300535,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=328, Top=6, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MVTIPDOC"

  func oMVTIPDOC_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty ( .w_SERIAL ))
    endwith
   endif
  endfunc

  func oMVTIPDOC_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVTIPDOC_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVTIPDOC_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMVTIPDOC_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco causali documento",'GSVE5KRS.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oMVCODCON_1_2 as StdField with uid="AEUBJIZWWU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MVCODCON", cQueryName = "MVCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Intestario inesistente o obsoleto",;
    ToolTipText = "Codice intestatario documento",;
    HelpContextID = 257337068,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=91, Top=32, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_MVCODCON"

  func oMVCODCON_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPCON$'C-F'  And  Empty ( .w_SERIAL ))
    endwith
   endif
  endfunc

  func oMVCODCON_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODCON_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODCON_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oMVCODCON_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco intestatari",'',this.parent.oContained
  endproc
  proc oMVCODCON_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_MVCODCON
     i_obj.ecpSave()
  endproc

  add object oMVNUMDOC_1_3 as StdField with uid="FCPUVIGDIH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MVNUMDOC", cQueryName = "MVNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documeto",;
    HelpContextID = 230684407,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=91, Top=58, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oMVNUMDOC_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ( Empty ( .w_SERIAL ))
    endwith
   endif
  endfunc

  add object oMVALFDOC_1_4 as StdField with uid="UIDBCTURLH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MVALFDOC", cQueryName = "MVALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Alfa documento",;
    HelpContextID = 238667511,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=220, Top=58, InputMask=replicate('X',10)

  func oMVALFDOC_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ( Empty ( .w_SERIAL ))
    endwith
   endif
  endfunc

  add object oMVDATDOC_1_5 as StdField with uid="WITSNPMVCW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MVDATDOC", cQueryName = "MVDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 224696055,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=336, Top=58

  func oMVDATDOC_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ( Empty ( .w_SERIAL ))
    endwith
   endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="HJLORMCRLK",left=652, top=6, width=48,height=45,;
    CpPicture="BMP\SPESE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare il documento di spesa";
    , HelpContextID = 2125018;
    , caption='\<Spesa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      vx_exec("Query\GSVE1KRS.vZM",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Empty ( .w_SERIAL ))
      endwith
    endif
  endfunc

  add object oMVCAOVAL_1_9 as StdField with uid="OCWSUOTWDK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MVCAOVAL", cQueryName = "MVCAOVAL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio documento di spese",;
    HelpContextID = 72046866,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=647, Top=58, cSayPict='"99999.999999"', cGetPict='"99999.999999"', bHasZoom = .t. 

  func oMVCAOVAL_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL=0 And .w_TIPOPE='N' And Not Empty( .w_MVCODVAL ))
    endwith
   endif
  endfunc

  func oMVCAOVAL_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPOPE='S')
    endwith
  endfunc

  proc oMVCAOVAL_1_9.mZoom
    vx_exec("QUERY\GSVE6KRS.VZM",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDESCAU_1_10 as StdField with uid="MBXGJONNES",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 40781622,;
   bGlobalFont=.t.,;
    Height=21, Width=252, Left=388, Top=6, InputMask=replicate('X',35)

  add object oDESINT_1_15 as StdField with uid="MBAWEQXPCC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESINT", cQueryName = "DESINT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 38029110,;
   bGlobalFont=.t.,;
    Height=21, Width=255, Left=211, Top=32, InputMask=replicate('X',40)

  add object oCPROWORD_1_25 as StdField with uid="OJDXPLTMJN",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CPROWORD", cQueryName = "CPROWORD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero riga",;
    HelpContextID = 232407658,;
   bGlobalFont=.t.,;
    Height=21, Width=43, Left=12, Top=87, cSayPict='"99999"', cGetPict='"99999"'

  add object oMVCODICE_1_26 as StdField with uid="YSUHGEEDYI",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MVCODICE", cQueryName = "MVCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo",;
    HelpContextID = 111761675,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=59, Top=87, InputMask=replicate('X',41)

  add object oMVDESART_1_27 as StdField with uid="ASSUXTBWNO",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MVDESART", cQueryName = "MVDESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 261056794,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=17, Width=259, Left=311, Top=87, InputMask=replicate('X',40)

  add object oMVVALMAG_1_28 as StdField with uid="VPLVCVONNR",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MVVALMAG", cQueryName = "MVVALMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore netto",;
    HelpContextID = 186419469,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=573, Top=87, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oDESCAU2_1_33 as StdField with uid="CQPJXJVGFK",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCAU2", cQueryName = "DESCAU2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 40781622,;
   bGlobalFont=.t.,;
    Height=21, Width=252, Left=388, Top=122, InputMask=replicate('X',35)

  add object oTIPDOC_1_35 as StdField with uid="MIPUFJOCJH",rtseq=25,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale documento",;
    HelpContextID = 246473674,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=328, Top=122, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_DOCSER ) And  .w_TIPOPE='N')
    endwith
   endif
  endfunc

  func oTIPDOC_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco causali documento",'',this.parent.oContained
  endproc

  add object oCODCON_1_36 as StdField with uid="CRYUFLQJAB",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Intestario inesistente o obsoleto",;
    ToolTipText = "Codice intestatario documento",;
    HelpContextID = 62037722,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=91, Top=149, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCON$'C-F' And Empty( .w_DOCSER )  And  .w_TIPOPE='N')
    endwith
   endif
  endfunc

  func oCODCON_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco intestatari",'',this.parent.oContained
  endproc
  proc oCODCON_1_36.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oDESINT2_1_37 as StdField with uid="DCGGWFTSDP",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESINT2", cQueryName = "DESINT2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 38029110,;
   bGlobalFont=.t.,;
    Height=21, Width=255, Left=211, Top=149, InputMask=replicate('X',40)

  add object oNUMDOC_1_38 as StdField with uid="ELEDMCHXPD",rtseq=28,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 246482986,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=91, Top=176, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMDOC_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_DOCSER )  And  .w_TIPOPE='N')
    endwith
   endif
  endfunc


  add object oBtn_1_39 as StdButton with uid="KUFKVQCZKH",left=652, top=122, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare il documento";
    , HelpContextID = 60910614;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      with this.Parent.oContained
        GSVE_BAS(this.Parent.oContained,"DOCZOOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_39.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty ( .w_MVSERIAL ) And .w_TIPOPE='N')
      endwith
    endif
  endfunc

  add object oALFDOC_1_40 as StdField with uid="PVMBEXDPQT",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Alfa documento",;
    HelpContextID = 246514170,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=220, Top=176, InputMask=replicate('X',10)

  func oALFDOC_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_DOCSER )  And  .w_TIPOPE='N')
    endwith
   endif
  endfunc

  add object oDATDOC_1_41 as StdField with uid="CLSECBMNFO",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DATDOC", cQueryName = "DATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 246459594,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=336, Top=176

  func oDATDOC_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_DOCSER )  And  .w_TIPOPE='N')
    endwith
   endif
  endfunc


  add object Zoom as cp_zoombox with uid="XHUGTOJALG",left=9, top=230, width=709,height=191,;
    caption='Object',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.f.,cZoomFile="GSVE3KRS",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,cTable="DOC_MAST",bRetriveAllRows=.f.,cMenuFile="",cZoomOnZoom="GSZM_BZM",;
    nPag=1;
    , ToolTipText = "Elenco righe a cui � abbinabile una parte o tuto l'importo di spese";
    , HelpContextID = 61986022


  add object oObj_1_50 as cp_calclbl with uid="QJIDDTDRQC",left=537, top=212, width=178,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Importi espressi in ",Alignment=1,;
    nPag=1;
    , HelpContextID = 61986022


  add object oBtn_1_51 as StdButton with uid="VRVHRRHVHV",left=191, top=424, width=48,height=45,;
    CpPicture="BMP\RIPART.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per ripartire la fattura di spesa in base al valore di magazzino";
    , HelpContextID = 190956522;
    , caption='\<Riparti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_51.Click()
      with this.Parent.oContained
        GSVE_BAS(this.Parent.oContained,"RIPARTO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_51.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!Empty(.w_SERIAL) And .w_TIPOPE='N')
      endwith
    endif
  endfunc


  add object oTIPOPE_1_52 as StdCombo with uid="TNHNXQBVQT",rtseq=34,rtrep=.f.,left=358,top=427,width=164,height=21;
    , ToolTipText = "Ripartisce o storno le spese";
    , HelpContextID = 211149770;
    , cFormVar="w_TIPOPE",RowSource=""+"Ripartizione,"+"Annulla ripartizione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPE_1_52.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTIPOPE_1_52.GetRadio()
    this.Parent.oContained.w_TIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPE_1_52.SetRadio()
    this.Parent.oContained.w_TIPOPE=trim(this.Parent.oContained.w_TIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPE=='N',1,;
      iif(this.Parent.oContained.w_TIPOPE=='S',2,;
      0))
  endfunc


  add object oBtn_1_53 as StdButton with uid="NNCMCFSLZJ",left=598, top=424, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 115982362;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_53.Click()
      with this.Parent.oContained
        GSVE_BAS(this.Parent.oContained,"CONFERMA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_53.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!Empty(.w_SERIAL) Or ( .w_TIPOPE='S' And Not Empty( .w_MVSERIAL )))
      endwith
    endif
  endfunc


  add object oBtn_1_54 as StdButton with uid="ZSKMMZQNYO",left=652, top=424, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 108694202;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_54.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_55 as cp_calclbl with uid="NKOYTWPTXH",left=10, top=6, width=234,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="1 - Selezione Riga Spese da Riaddebitare",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 61986022


  add object oObj_1_58 as cp_calclbl with uid="GLVVQFATIL",left=10, top=122, width=234,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="2 - Selezione documento da rivalorizzare",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 61986022


  add object oObj_1_59 as cp_calclbl with uid="XREZLWNKBB",left=10, top=212, width=377,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="3 - Dividi le spese sulle righe del documento selezionato",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 61986022


  add object oObj_1_61 as cp_calclbl with uid="MUGFVIBKED",left=441, top=58, width=92,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Valuta ",Alignment=1,;
    nPag=1;
    , HelpContextID = 61986022


  add object oObj_1_64 as cp_calclbl with uid="RSTUIKTEJF",left=31, top=426, width=152,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Valore ripartito in ",;
    nPag=1;
    , HelpContextID = 61986022


  add object DISAB as cp_zoombox with uid="FZWVBZNMLH",left=9, top=230, width=709,height=191,;
    caption='Object',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.f.,cZoomFile="GSVE4KRS",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,cTable="DOC_MAST",bRetriveAllRows=.f.,cMenuFile="",cZoomOnZoom="GSZM_BZM",;
    nPag=1;
    , ToolTipText = "Elenco righe a cui � stato abbinata una sepsa";
    , HelpContextID = 61986022


  add object oObj_1_70 as cp_runprogram with uid="DQXNLCDXXN",left=0, top=478, width=221,height=19,;
    caption='GSVE_BAS',;
   bGlobalFont=.t.,;
    prg="GSVE_BAS('ZOOM')",;
    cEvent = "Init,w_TIPOPE Changed",;
    nPag=1;
    , ToolTipText = "Seleziona quale zoom visualizzare";
    , HelpContextID = 22054329


  add object oObj_1_72 as cp_runprogram with uid="JOJEIFYQWU",left=224, top=478, width=156,height=19,;
    caption='GSVE_BAS',;
   bGlobalFont=.t.,;
    prg="GSVE_BAS('TESTDOC')",;
    cEvent = "Leggidoc",;
    nPag=1;
    , ToolTipText = "Riempie i dati di testata del documento da diasbbinare";
    , HelpContextID = 22054329


  add object oObj_1_79 as cp_calclbl with uid="STAQPJSYZX",left=441, top=58, width=126,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Valuta ",Alignment=1,;
    nPag=1;
    , HelpContextID = 61986022

  add object oStr_1_8 as StdString with uid="IWRJSIHXVZ",Visible=.t., Left=251, Top=6,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="TCTDSAJPDG",Visible=.t., Left=15, Top=34,;
    Alignment=1, Width=72, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="KIKCVKNOJZ",Visible=.t., Left=310, Top=58,;
    Alignment=1, Width=22, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="FTLWREENUK",Visible=.t., Left=37, Top=58,;
    Alignment=1, Width=50, Height=18,;
    Caption="N. doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="DHUXLTSEKO",Visible=.t., Left=210, Top=58,;
    Alignment=2, Width=11, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="LVGQWKFNVT",Visible=.t., Left=251, Top=122,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="KGLGGRDDIF",Visible=.t., Left=15, Top=149,;
    Alignment=1, Width=72, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="ZSBVCQLPVV",Visible=.t., Left=310, Top=176,;
    Alignment=1, Width=22, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="BPMLDFOGIT",Visible=.t., Left=38, Top=176,;
    Alignment=1, Width=49, Height=18,;
    Caption="N. doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="LNBYQNAZQH",Visible=.t., Left=210, Top=176,;
    Alignment=2, Width=11, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="TJJNOTSZXV",Visible=.t., Left=13, Top=426,;
    Alignment=0, Width=18, Height=18,;
    Caption="(*)"  ;
  , bGlobalFont=.t.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (Empty( .w_MVCODVAL ))
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="GFMFNQVCER",Visible=.t., Left=280, Top=426,;
    Alignment=1, Width=70, Height=18,;
    Caption="Operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="RJWFLCBEFP",Visible=.t., Left=1, Top=501,;
    Alignment=0, Width=577, Height=19,;
    Caption="Attenzione ci sono due zoom sovrapposti, visualizzati a seconda del tipo operazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_71.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_77 as StdString with uid="XYIVVVRKZC",Visible=.t., Left=580, Top=58,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_77.mHide()
    with this.Parent.oContained
      return (.w_TIPOPE='S')
    endwith
  endfunc

  add object oStr_1_78 as StdString with uid="XNJWKJLQNN",Visible=.t., Left=3, Top=521,;
    Alignment=0, Width=720, Height=19,;
    Caption="Attenzione i dati letti sul link delle causali / intestatari sono riportati come campi dallo zoom se non impostata"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_78.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oBox_1_6 as StdBox with uid="WEJZXWSVWY",left=5, top=2, width=722,height=112

  add object oBox_1_31 as StdBox with uid="LYUMLYNQTA",left=5, top=112, width=722,height=97

  add object oBox_1_48 as StdBox with uid="SLCAWHITJV",left=5, top=207, width=722,height=265
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_krs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
