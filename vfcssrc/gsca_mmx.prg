* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_mmx                                                        *
*              Modifica movim. di analitica                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_221]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-21                                                      *
* Last revis.: 2017-01-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsca_mmx
* --- inizializza Picture
VVP=20*g_PERPVL
* --- Fine Area Manuale
return(createobject("tgsca_mmx"))

* --- Class definition
define class tgsca_mmx as StdTrsForm
  Top    = 51
  Left   = 20

  * --- Standard Properties
  Width  = 770
  Height = 329+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-01-10"
  HelpContextID=197821801
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=33

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  MOVICOST_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  CAN_TIER_IDX = 0
  PNT_MAST_IDX = 0
  PNT_DETT_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  cFile = "MOVICOST"
  cKeySelect = "MRSERIAL,MRROWORD"
  cKeyWhere  = "MRSERIAL=this.w_MRSERIAL and MRROWORD=this.w_MRROWORD"
  cKeyDetail  = "MRSERIAL=this.w_MRSERIAL and CPROWNUM=this.w_CPROWNUM and MRROWORD=this.w_MRROWORD"
  cKeyWhereODBC = '"MRSERIAL="+cp_ToStrODBC(this.w_MRSERIAL)';
      +'+" and MRROWORD="+cp_ToStrODBC(this.w_MRROWORD)';

  cKeyDetailWhereODBC = '"MRSERIAL="+cp_ToStrODBC(this.w_MRSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and MRROWORD="+cp_ToStrODBC(this.w_MRROWORD)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"MOVICOST.MRSERIAL="+cp_ToStrODBC(this.w_MRSERIAL)';
      +'+" and MOVICOST.MRROWORD="+cp_ToStrODBC(this.w_MRROWORD)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MOVICOST.CPROWNUM '
  cPrg = "gsca_mmx"
  cComment = "Modifica movim. di analitica"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_numreg = 0
  w_codese = space(4)
  w_codute = 0
  w_DATREG = ctod('  /  /  ')
  w_MRSERIAL = space(10)
  w_CPROWNUM = 0
  w_IMPDAR = 0
  w_MRROWORD = 0
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_MRCODVOC = space(15)
  w_MRCODICE = space(15)
  w_MRCODCOM = space(15)
  w_MRPARAME = 0
  w_DESVOC = space(40)
  w_DESPIA = space(40)
  w_DESCAN = space(30)
  w_MR_SEGNO = space(1)
  w_MRTOTIMP = 0
  w_MRINICOM = ctod('  /  /  ')
  o_MRINICOM = ctod('  /  /  ')
  w_MRFINCOM = ctod('  /  /  ')
  w_IMPRIG = 0
  w_TOTRIG = 0
  w_TOTQUA = 0
  w_TOTVIS = 0
  w_SEGTOT = space(1)
  w_TOTDIF = 0
  w_RESCHK = 0
  w_IMPAVE = 0
  w_TOTPAR = 0
  w_PERCEN = 0
  w_DECTOP = 0
  w_VALNAZ = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsca_mmx
        i_Batch =.f.
  
  * --- Salva
   proc ecpSave()
     DoDefault()
     if ! this.i_Batch and THIS.w_RESCHK=0
       this.cFunction="Query"
       this.ecpQuit()
     endif
   endproc
   proc ecpquit()
     DoDefault()
     if ! this.i_Batch
       this.cFunction="Query"
       this.Notifyevent('Done')
       This.Hide()
       This.Release()
     endif
   endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOVICOST','gsca_mmx')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsca_mmxPag1","gsca_mmx",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Modifica movimenti di analitica")
      .Pages(1).HelpContextID = 152306003
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsca_mmx
      * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='CENCOST'
    this.cWorkTables[2]='VOC_COST'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='PNT_MAST'
    this.cWorkTables[5]='PNT_DETT'
    this.cWorkTables[6]='ESERCIZI'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='MOVICOST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOVICOST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOVICOST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MRSERIAL = NVL(MRSERIAL,space(10))
      .w_MRROWORD = NVL(MRROWORD,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from MOVICOST where MRSERIAL=KeySet.MRSERIAL
    *                            and CPROWNUM=KeySet.CPROWNUM
    *                            and MRROWORD=KeySet.MRROWORD
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.MOVICOST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2],this.bLoadRecFilter,this.MOVICOST_IDX,"gsca_mmx")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOVICOST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOVICOST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOVICOST '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MRSERIAL',this.w_MRSERIAL  ,'MRROWORD',this.w_MRROWORD  )
      select * from (i_cTable) MOVICOST where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_numreg = 0
        .w_codese = g_CODESE
        .w_codute = 0
        .w_DATREG = ctod("  /  /  ")
        .w_IMPDAR = 0
        .w_DATOBSO = ctod("  /  /  ")
        .w_OBTEST = .w_DATREG
        .w_TOTRIG = 0
        .w_RESCHK = 0
        .w_IMPAVE = 0
        .w_TOTPAR = 0
        .w_DECTOP = 0
        .w_MRSERIAL = NVL(MRSERIAL,space(10))
          if link_1_5_joined
            this.w_MRSERIAL = NVL(PNSERIAL105,NVL(this.w_MRSERIAL,space(10)))
            this.w_codute = NVL(PNCODUTE105,0)
            this.w_numreg = NVL(PNNUMRER105,0)
            this.w_datreg = NVL(cp_ToDate(PNDATREG105),ctod("  /  /  "))
            this.w_codese = NVL(PNCODESE105,space(4))
          else
          .link_1_5('Load')
          endif
        .w_MRROWORD = NVL(MRROWORD,0)
          if link_1_22_joined
            this.w_MRROWORD = NVL(CPROWNUM122,NVL(this.w_MRROWORD,0))
            this.w_IMPDAR = NVL(PNIMPDAR122,0)
            this.w_IMPAVE = NVL(PNIMPAVE122,0)
          else
          .link_1_22('Load')
          endif
        .w_TOTQUA = .w_IMPDAR+.w_IMPAVE
        .w_TOTVIS = ABS(.w_TOTRIG)
        .w_SEGTOT = IIF(.w_TOTRIG<0  , 'A', 'D') 
        .w_TOTDIF = (.w_IMPDAR-.w_IMPAVE) - .w_TOTRIG
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'MOVICOST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTRIG = 0
      this.w_TOTPAR = 0
      scan
        with this
          .w_DESVOC = space(40)
          .w_DESPIA = space(40)
          .w_DESCAN = space(30)
          .w_PERCEN = 0
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_MRCODVOC = NVL(MRCODVOC,space(15))
          if link_2_1_joined
            this.w_MRCODVOC = NVL(VCCODICE201,NVL(this.w_MRCODVOC,space(15)))
            this.w_DESVOC = NVL(VCDESCRI201,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(VCDTOBSO201),ctod("  /  /  "))
          else
          .link_2_1('Load')
          endif
          .w_MRCODICE = NVL(MRCODICE,space(15))
          if link_2_2_joined
            this.w_MRCODICE = NVL(CC_CONTO202,NVL(this.w_MRCODICE,space(15)))
            this.w_DESPIA = NVL(CCDESPIA202,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO202),ctod("  /  /  "))
          else
          .link_2_2('Load')
          endif
          .w_MRCODCOM = NVL(MRCODCOM,space(15))
          if link_2_3_joined
            this.w_MRCODCOM = NVL(CNCODCAN203,NVL(this.w_MRCODCOM,space(15)))
            this.w_DESCAN = NVL(CNDESCAN203,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(CNDTOBSO203),ctod("  /  /  "))
          else
          .link_2_3('Load')
          endif
          .w_MRPARAME = NVL(MRPARAME,0)
          .w_MR_SEGNO = NVL(MR_SEGNO,space(1))
          .w_MRTOTIMP = NVL(MRTOTIMP,0)
          .w_MRINICOM = NVL(cp_ToDate(MRINICOM),ctod("  /  /  "))
          .w_MRFINCOM = NVL(cp_ToDate(MRFINCOM),ctod("  /  /  "))
        .w_IMPRIG = .w_MRTOTIMP*IIF(.w_MR_SEGNO='D',1,-1)
        .oPgFrm.Page1.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate()
        .w_VALNAZ = g_perval
          .link_2_19('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTRIG = .w_TOTRIG+.w_IMPRIG
          .w_TOTPAR = .w_TOTPAR+.w_MRPARAME*IIF(.w_MR_SEGNO='D',1,-1)
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_TOTQUA = .w_IMPDAR+.w_IMPAVE
        .w_TOTVIS = ABS(.w_TOTRIG)
        .w_SEGTOT = IIF(.w_TOTRIG<0  , 'A', 'D') 
        .w_TOTDIF = (.w_IMPDAR-.w_IMPAVE) - .w_TOTRIG
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_numreg=0
      .w_codese=space(4)
      .w_codute=0
      .w_DATREG=ctod("  /  /  ")
      .w_MRSERIAL=space(10)
      .w_CPROWNUM=0
      .w_IMPDAR=0
      .w_MRROWORD=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_MRCODVOC=space(15)
      .w_MRCODICE=space(15)
      .w_MRCODCOM=space(15)
      .w_MRPARAME=0
      .w_DESVOC=space(40)
      .w_DESPIA=space(40)
      .w_DESCAN=space(30)
      .w_MR_SEGNO=space(1)
      .w_MRTOTIMP=0
      .w_MRINICOM=ctod("  /  /  ")
      .w_MRFINCOM=ctod("  /  /  ")
      .w_IMPRIG=0
      .w_TOTRIG=0
      .w_TOTQUA=0
      .w_TOTVIS=0
      .w_SEGTOT=space(1)
      .w_TOTDIF=0
      .w_RESCHK=0
      .w_IMPAVE=0
      .w_TOTPAR=0
      .w_PERCEN=0
      .w_DECTOP=0
      .w_VALNAZ=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_codese = g_CODESE
        .DoRTCalc(3,5,.f.)
        if not(empty(.w_MRSERIAL))
         .link_1_5('Full')
        endif
        .DoRTCalc(6,8,.f.)
        if not(empty(.w_MRROWORD))
         .link_1_22('Full')
        endif
        .DoRTCalc(9,9,.f.)
        .w_OBTEST = .w_DATREG
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_MRCODVOC))
         .link_2_1('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_MRCODICE))
         .link_2_2('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_MRCODCOM))
         .link_2_3('Full')
        endif
        .DoRTCalc(14,17,.f.)
        .w_MR_SEGNO = IIF(.w_IMPAVE<>0,'A','D')
        .DoRTCalc(19,20,.f.)
        .w_MRFINCOM = iif(empty(.w_mrfincom)and not empty(.w_mrinicom),.w_mrinicom,iif(not empty(.w_MRFINCOM)and empty(.w_MRINICOM),.w_mrinicom,.w_mrfincom))
        .w_IMPRIG = .w_MRTOTIMP*IIF(.w_MR_SEGNO='D',1,-1)
        .DoRTCalc(23,23,.f.)
        .w_TOTQUA = .w_IMPDAR+.w_IMPAVE
        .w_TOTVIS = ABS(.w_TOTRIG)
        .w_SEGTOT = IIF(.w_TOTRIG<0  , 'A', 'D') 
        .w_TOTDIF = (.w_IMPDAR-.w_IMPAVE) - .w_TOTRIG
        .w_RESCHK = 0
        .oPgFrm.Page1.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate()
        .DoRTCalc(29,32,.f.)
        .w_VALNAZ = g_perval
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_VALNAZ))
         .link_2_19('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOVICOST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsca_mmx
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMRINICOM_2_10.enabled = i_bVal
      .Page1.oPag.oMRFINCOM_2_11.enabled = i_bVal
      .Page1.oPag.oObj_2_13.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.enabled = i_bVal
      .Page1.oPag.oObj_2_15.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MOVICOST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOVICOST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRSERIAL,"MRSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRROWORD,"MRROWORD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOVICOST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2])
    i_lTable = "MOVICOST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOVICOST_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MRCODVOC C(15);
      ,t_MRCODICE C(15);
      ,t_MRCODCOM C(15);
      ,t_MRPARAME N(18,4);
      ,t_DESVOC C(40);
      ,t_DESPIA C(40);
      ,t_DESCAN C(30);
      ,t_MR_SEGNO C(1);
      ,t_MRTOTIMP N(18,4);
      ,t_MRINICOM D(8);
      ,t_MRFINCOM D(8);
      ,CPROWNUM N(10);
      ,t_CPROWNUM N(4);
      ,t_IMPRIG N(18,4);
      ,t_PERCEN N(18,4);
      ,t_VALNAZ C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsca_mmxbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1.controlsource=this.cTrsName+'.t_MRCODVOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODICE_2_2.controlsource=this.cTrsName+'.t_MRCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCOM_2_3.controlsource=this.cTrsName+'.t_MRCODCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRPARAME_2_4.controlsource=this.cTrsName+'.t_MRPARAME'
    this.oPgFRm.Page1.oPag.oDESVOC_2_5.controlsource=this.cTrsName+'.t_DESVOC'
    this.oPgFRm.Page1.oPag.oDESPIA_2_6.controlsource=this.cTrsName+'.t_DESPIA'
    this.oPgFRm.Page1.oPag.oDESCAN_2_7.controlsource=this.cTrsName+'.t_DESCAN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMR_SEGNO_2_8.controlsource=this.cTrsName+'.t_MR_SEGNO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_9.controlsource=this.cTrsName+'.t_MRTOTIMP'
    this.oPgFRm.Page1.oPag.oMRINICOM_2_10.controlsource=this.cTrsName+'.t_MRINICOM'
    this.oPgFRm.Page1.oPag.oMRFINCOM_2_11.controlsource=this.cTrsName+'.t_MRFINCOM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(140)
    this.AddVLine(280)
    this.AddVLine(421)
    this.AddVLine(566)
    this.AddVLine(603)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOVICOST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOVICOST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2])
      *
      * insert into MOVICOST
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOVICOST')
        i_extval=cp_InsertValODBCExtFlds(this,'MOVICOST')
        i_cFldBody=" "+;
                  "(MRSERIAL,MRROWORD,MRCODVOC,MRCODICE,MRCODCOM"+;
                  ",MRPARAME,MR_SEGNO,MRTOTIMP,MRINICOM,MRFINCOM,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_MRSERIAL)+","+cp_ToStrODBCNull(this.w_MRROWORD)+","+cp_ToStrODBCNull(this.w_MRCODVOC)+","+cp_ToStrODBCNull(this.w_MRCODICE)+","+cp_ToStrODBCNull(this.w_MRCODCOM)+;
             ","+cp_ToStrODBC(this.w_MRPARAME)+","+cp_ToStrODBC(this.w_MR_SEGNO)+","+cp_ToStrODBC(this.w_MRTOTIMP)+","+cp_ToStrODBC(this.w_MRINICOM)+","+cp_ToStrODBC(this.w_MRFINCOM)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOVICOST')
        i_extval=cp_InsertValVFPExtFlds(this,'MOVICOST')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MRSERIAL',this.w_MRSERIAL,'CPROWNUM',this.w_CPROWNUM,'MRROWORD',this.w_MRROWORD)
        INSERT INTO (i_cTable) (;
                   MRSERIAL;
                  ,MRROWORD;
                  ,MRCODVOC;
                  ,MRCODICE;
                  ,MRCODCOM;
                  ,MRPARAME;
                  ,MR_SEGNO;
                  ,MRTOTIMP;
                  ,MRINICOM;
                  ,MRFINCOM;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MRSERIAL;
                  ,this.w_MRROWORD;
                  ,this.w_MRCODVOC;
                  ,this.w_MRCODICE;
                  ,this.w_MRCODCOM;
                  ,this.w_MRPARAME;
                  ,this.w_MR_SEGNO;
                  ,this.w_MRTOTIMP;
                  ,this.w_MRINICOM;
                  ,this.w_MRFINCOM;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MOVICOST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_CPROWNUM<>t_CPROWNUM
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) AND t_MRTOTIMP<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'MOVICOST')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'MOVICOST')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_CPROWNUM<>t_CPROWNUM
            i_bUpdAll = .t.
          endif
        endif
        scan for (t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) AND t_MRTOTIMP<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MOVICOST
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'MOVICOST')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MRCODVOC="+cp_ToStrODBCNull(this.w_MRCODVOC)+;
                     ",MRCODICE="+cp_ToStrODBCNull(this.w_MRCODICE)+;
                     ",MRCODCOM="+cp_ToStrODBCNull(this.w_MRCODCOM)+;
                     ",MRPARAME="+cp_ToStrODBC(this.w_MRPARAME)+;
                     ",MR_SEGNO="+cp_ToStrODBC(this.w_MR_SEGNO)+;
                     ",MRTOTIMP="+cp_ToStrODBC(this.w_MRTOTIMP)+;
                     ",MRINICOM="+cp_ToStrODBC(this.w_MRINICOM)+;
                     ",MRFINCOM="+cp_ToStrODBC(this.w_MRFINCOM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'MOVICOST')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MRCODVOC=this.w_MRCODVOC;
                     ,MRCODICE=this.w_MRCODICE;
                     ,MRCODCOM=this.w_MRCODCOM;
                     ,MRPARAME=this.w_MRPARAME;
                     ,MR_SEGNO=this.w_MR_SEGNO;
                     ,MRTOTIMP=this.w_MRTOTIMP;
                     ,MRINICOM=this.w_MRINICOM;
                     ,MRFINCOM=this.w_MRFINCOM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOVICOST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) AND t_MRTOTIMP<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete MOVICOST
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) AND t_MRTOTIMP<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOVICOST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOVICOST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .link_1_5('Full')
        .DoRTCalc(6,7,.t.)
          .link_1_22('Full')
        .DoRTCalc(9,20,.t.)
        if .o_MRINICOM<>.w_MRINICOM
          .w_MRFINCOM = iif(empty(.w_mrfincom)and not empty(.w_mrinicom),.w_mrinicom,iif(not empty(.w_MRFINCOM)and empty(.w_MRINICOM),.w_mrinicom,.w_mrfincom))
        endif
          .w_TOTRIG = .w_TOTRIG-.w_imprig
          .w_IMPRIG = .w_MRTOTIMP*IIF(.w_MR_SEGNO='D',1,-1)
          .w_TOTRIG = .w_TOTRIG+.w_imprig
        .DoRTCalc(23,23,.t.)
          .w_TOTQUA = .w_IMPDAR+.w_IMPAVE
          .w_TOTVIS = ABS(.w_TOTRIG)
          .w_SEGTOT = IIF(.w_TOTRIG<0  , 'A', 'D') 
          .w_TOTDIF = (.w_IMPDAR-.w_IMPAVE) - .w_TOTRIG
        .oPgFrm.Page1.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate()
        .DoRTCalc(28,32,.t.)
          .w_VALNAZ = g_perval
          .link_2_19('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CPROWNUM with this.w_CPROWNUM
      replace t_IMPRIG with this.w_IMPRIG
      replace t_PERCEN with this.w_PERCEN
      replace t_VALNAZ with this.w_VALNAZ
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate()
    endwith
  return
  proc Calculate_YHXCTTKAID()
    with this
          * --- rilegge il link
          .w_MRSERIAL = .w_MRSERIAL
          .link_1_5('Full')
          .w_MRROWORD = .w_MRROWORD
          .link_1_22('Full')
          .w_IMPAVE = .w_IMPAVE
          .w_MR_SEGNO = IIF(.w_IMPAVE<>0,'A','D')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRCODCOM_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRCODCOM_2_3.mCond()
    this.oPgFrm.Page1.oPag.oMRFINCOM_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMRFINCOM_2_11.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Rilegge")
          .Calculate_YHXCTTKAID()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_2_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsca_mmx
    if cevent='Edit Aborted'
       this.opgFRM.PageCount=1
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MRSERIAL
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_lTable = "PNT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2], .t., this.PNT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PNSERIAL,PNCODUTE,PNNUMRER,PNDATREG,PNCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where PNSERIAL="+cp_ToStrODBC(this.w_MRSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PNSERIAL',this.w_MRSERIAL)
            select PNSERIAL,PNCODUTE,PNNUMRER,PNDATREG,PNCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRSERIAL = NVL(_Link_.PNSERIAL,space(10))
      this.w_codute = NVL(_Link_.PNCODUTE,0)
      this.w_numreg = NVL(_Link_.PNNUMRER,0)
      this.w_datreg = NVL(cp_ToDate(_Link_.PNDATREG),ctod("  /  /  "))
      this.w_codese = NVL(_Link_.PNCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_MRSERIAL = space(10)
      endif
      this.w_codute = 0
      this.w_numreg = 0
      this.w_datreg = ctod("  /  /  ")
      this.w_codese = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.PNSERIAL,1)
      cp_ShowWarn(i_cKey,this.PNT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PNT_MAST_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.PNSERIAL as PNSERIAL105"+ ",link_1_5.PNCODUTE as PNCODUTE105"+ ",link_1_5.PNNUMRER as PNNUMRER105"+ ",link_1_5.PNDATREG as PNDATREG105"+ ",link_1_5.PNCODESE as PNCODESE105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on MOVICOST.MRSERIAL=link_1_5.PNSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and MOVICOST.MRSERIAL=link_1_5.PNSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRROWORD
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PNT_DETT_IDX,3]
    i_lTable = "PNT_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_DETT_IDX,2], .t., this.PNT_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRROWORD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRROWORD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PNSERIAL,CPROWNUM,PNIMPDAR,PNIMPAVE";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_MRROWORD);
                   +" and PNSERIAL="+cp_ToStrODBC(this.w_MRSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PNSERIAL',this.w_MRSERIAL;
                       ,'CPROWNUM',this.w_MRROWORD)
            select PNSERIAL,CPROWNUM,PNIMPDAR,PNIMPAVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRROWORD = NVL(_Link_.CPROWNUM,0)
      this.w_IMPDAR = NVL(_Link_.PNIMPDAR,0)
      this.w_IMPAVE = NVL(_Link_.PNIMPAVE,0)
    else
      if i_cCtrl<>'Load'
        this.w_MRROWORD = 0
      endif
      this.w_IMPDAR = 0
      this.w_IMPAVE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PNT_DETT_IDX,2])+'\'+cp_ToStr(_Link_.PNSERIAL,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.PNT_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRROWORD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PNT_DETT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PNT_DETT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.CPROWNUM as CPROWNUM122"+ ",link_1_22.PNIMPDAR as PNIMPDAR122"+ ",link_1_22.PNIMPAVE as PNIMPAVE122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on MOVICOST.MRROWORD=link_1_22.CPROWNUM"+" and MOVICOST.MRSERIAL=link_1_22.PNSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and MOVICOST.MRROWORD=link_1_22.CPROWNUM(+)"'+'+" and MOVICOST.MRSERIAL=link_1_22.PNSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRCODVOC
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODVOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_MRCODVOC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_MRCODVOC))
          select VCCODICE,VCDESCRI,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODVOC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODVOC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oMRCODVOC_2_1'),i_cWhere,'GSCA_AVC',"Voci di costo o ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODVOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_MRCODVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_MRCODVOC)
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODVOC = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODVOC = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODVOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.VCCODICE as VCCODICE201"+ ",link_2_1.VCDESCRI as VCDESCRI201"+ ",link_2_1.VCDTOBSO as VCDTOBSO201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on MOVICOST.MRCODVOC=link_2_1.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and MOVICOST.MRCODVOC=link_2_1.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRCODICE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_MRCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_MRCODICE))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODICE)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODICE) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oMRCODICE_2_2'),i_cWhere,'GSCA_ACC',"Centri di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_MRCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_MRCODICE)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODICE = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODICE = space(15)
      endif
      this.w_DESPIA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CC_CONTO as CC_CONTO202"+ ",link_2_2.CCDESPIA as CCDESPIA202"+ ",link_2_2.CCDTOBSO as CCDTOBSO202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on MOVICOST.MRCODICE=link_2_2.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and MOVICOST.MRCODICE=link_2_2.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRCODCOM
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_MRCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_MRCODCOM))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oMRCODCOM_2_3'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_MRCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_MRCODCOM)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CNCODCAN as CNCODCAN203"+ ",link_2_3.CNDESCAN as CNDESCAN203"+ ",link_2_3.CNDTOBSO as CNDTOBSO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on MOVICOST.MRCODCOM=link_2_3.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and MOVICOST.MRCODCOM=link_2_3.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(10))
      this.w_DECTOP = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(10)
      endif
      this.w_DECTOP = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.onumreg_1_1.value==this.w_numreg)
      this.oPgFrm.Page1.oPag.onumreg_1_1.value=this.w_numreg
    endif
    if not(this.oPgFrm.Page1.oPag.ocodese_1_2.value==this.w_codese)
      this.oPgFrm.Page1.oPag.ocodese_1_2.value=this.w_codese
    endif
    if not(this.oPgFrm.Page1.oPag.ocodute_1_3.value==this.w_codute)
      this.oPgFrm.Page1.oPag.ocodute_1_3.value=this.w_codute
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG_1_4.value==this.w_DATREG)
      this.oPgFrm.Page1.oPag.oDATREG_1_4.value=this.w_DATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVOC_2_5.value==this.w_DESVOC)
      this.oPgFrm.Page1.oPag.oDESVOC_2_5.value=this.w_DESVOC
      replace t_DESVOC with this.oPgFrm.Page1.oPag.oDESVOC_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIA_2_6.value==this.w_DESPIA)
      this.oPgFrm.Page1.oPag.oDESPIA_2_6.value=this.w_DESPIA
      replace t_DESPIA with this.oPgFrm.Page1.oPag.oDESPIA_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_2_7.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_2_7.value=this.w_DESCAN
      replace t_DESCAN with this.oPgFrm.Page1.oPag.oDESCAN_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMRINICOM_2_10.value==this.w_MRINICOM)
      this.oPgFrm.Page1.oPag.oMRINICOM_2_10.value=this.w_MRINICOM
      replace t_MRINICOM with this.oPgFrm.Page1.oPag.oMRINICOM_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMRFINCOM_2_11.value==this.w_MRFINCOM)
      this.oPgFrm.Page1.oPag.oMRFINCOM_2_11.value=this.w_MRFINCOM
      replace t_MRFINCOM with this.oPgFrm.Page1.oPag.oMRFINCOM_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTQUA_3_2.value==this.w_TOTQUA)
      this.oPgFrm.Page1.oPag.oTOTQUA_3_2.value=this.w_TOTQUA
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTVIS_3_3.value==this.w_TOTVIS)
      this.oPgFrm.Page1.oPag.oTOTVIS_3_3.value=this.w_TOTVIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSEGTOT_3_4.value==this.w_SEGTOT)
      this.oPgFrm.Page1.oPag.oSEGTOT_3_4.value=this.w_SEGTOT
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDIF_3_5.value==this.w_TOTDIF)
      this.oPgFrm.Page1.oPag.oTOTDIF_3_5.value=this.w_TOTDIF
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1.value==this.w_MRCODVOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1.value=this.w_MRCODVOC
      replace t_MRCODVOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODICE_2_2.value==this.w_MRCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODICE_2_2.value=this.w_MRCODICE
      replace t_MRCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCOM_2_3.value==this.w_MRCODCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCOM_2_3.value=this.w_MRCODCOM
      replace t_MRCODCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODCOM_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRPARAME_2_4.value==this.w_MRPARAME)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRPARAME_2_4.value=this.w_MRPARAME
      replace t_MRPARAME with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRPARAME_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMR_SEGNO_2_8.value==this.w_MR_SEGNO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMR_SEGNO_2_8.value=this.w_MR_SEGNO
      replace t_MR_SEGNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMR_SEGNO_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_9.value==this.w_MRTOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_9.value=this.w_MRTOTIMP
      replace t_MRTOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'MOVICOST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsca_mmx
      THIS.w_RESCHK=0
      IF THIS.w_TOTDIF<>0
         i_bnoChk=.F.
         i_bRes=.F.
         THIS.w_RESCHK=1
         i_cErrorMsg=Ah_MsgFormat("Totale diverso da primanota")
      ENDIF
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) AND t_MRTOTIMP<>0);
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(NOT (.w_MRPARAME=0)) and (.w_MRCODVOC<>SPACE(15) AND .w_MRCODICE<>SPACE(15) AND .w_MRTOTIMP<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRPARAME_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Parametro non valorizzato")
        case   not(.w_MR_SEGNO $ "DA") and (.w_MRCODVOC<>SPACE(15) AND .w_MRCODICE<>SPACE(15) AND .w_MRTOTIMP<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMR_SEGNO_2_8
          i_bRes = .f.
          i_bnoChk = .f.
        case   (empty(.w_MRFINCOM) or not(.w_mrinicom<=.w_mrfincom)) and (not empty(.w_mrinicom)) and (.w_MRCODVOC<>SPACE(15) AND .w_MRCODICE<>SPACE(15) AND .w_MRTOTIMP<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oMRFINCOM_2_11
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Data fine competenza incongruente")
      endcase
      if .w_MRCODVOC<>SPACE(15) AND .w_MRCODICE<>SPACE(15) AND .w_MRTOTIMP<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MRINICOM = this.w_MRINICOM
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) AND t_MRTOTIMP<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MRCODVOC=space(15)
      .w_MRCODICE=space(15)
      .w_MRCODCOM=space(15)
      .w_MRPARAME=0
      .w_DESVOC=space(40)
      .w_DESPIA=space(40)
      .w_DESCAN=space(30)
      .w_MR_SEGNO=space(1)
      .w_MRTOTIMP=0
      .w_MRINICOM=ctod("  /  /  ")
      .w_MRFINCOM=ctod("  /  /  ")
      .w_IMPRIG=0
      .w_PERCEN=0
      .w_VALNAZ=space(10)
      .DoRTCalc(1,11,.f.)
      if not(empty(.w_MRCODVOC))
        .link_2_1('Full')
      endif
      .DoRTCalc(12,12,.f.)
      if not(empty(.w_MRCODICE))
        .link_2_2('Full')
      endif
      .DoRTCalc(13,13,.f.)
      if not(empty(.w_MRCODCOM))
        .link_2_3('Full')
      endif
      .DoRTCalc(14,17,.f.)
        .w_MR_SEGNO = IIF(.w_IMPAVE<>0,'A','D')
      .DoRTCalc(19,20,.f.)
        .w_MRFINCOM = iif(empty(.w_mrfincom)and not empty(.w_mrinicom),.w_mrinicom,iif(not empty(.w_MRFINCOM)and empty(.w_MRINICOM),.w_mrinicom,.w_mrfincom))
        .w_IMPRIG = .w_MRTOTIMP*IIF(.w_MR_SEGNO='D',1,-1)
        .oPgFrm.Page1.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate()
      .DoRTCalc(23,32,.f.)
        .w_VALNAZ = g_perval
      .DoRTCalc(33,33,.f.)
      if not(empty(.w_VALNAZ))
        .link_2_19('Full')
      endif
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWNUM = t_CPROWNUM
    this.w_MRCODVOC = t_MRCODVOC
    this.w_MRCODICE = t_MRCODICE
    this.w_MRCODCOM = t_MRCODCOM
    this.w_MRPARAME = t_MRPARAME
    this.w_DESVOC = t_DESVOC
    this.w_DESPIA = t_DESPIA
    this.w_DESCAN = t_DESCAN
    this.w_MR_SEGNO = t_MR_SEGNO
    this.w_MRTOTIMP = t_MRTOTIMP
    this.w_MRINICOM = t_MRINICOM
    this.w_MRFINCOM = t_MRFINCOM
    this.w_IMPRIG = t_IMPRIG
    this.w_PERCEN = t_PERCEN
    this.w_VALNAZ = t_VALNAZ
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_MRCODVOC with this.w_MRCODVOC
    replace t_MRCODICE with this.w_MRCODICE
    replace t_MRCODCOM with this.w_MRCODCOM
    replace t_MRPARAME with this.w_MRPARAME
    replace t_DESVOC with this.w_DESVOC
    replace t_DESPIA with this.w_DESPIA
    replace t_DESCAN with this.w_DESCAN
    replace t_MR_SEGNO with this.w_MR_SEGNO
    replace t_MRTOTIMP with this.w_MRTOTIMP
    replace t_MRINICOM with this.w_MRINICOM
    replace t_MRFINCOM with this.w_MRFINCOM
    replace t_IMPRIG with this.w_IMPRIG
    replace t_PERCEN with this.w_PERCEN
    replace t_VALNAZ with this.w_VALNAZ
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTRIG = .w_TOTRIG-.w_imprig
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsca_mmxPag1 as StdContainer
  Width  = 766
  height = 329
  stdWidth  = 766
  stdheight = 329
  resizeXpos=215
  resizeYpos=165
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object onumreg_1_1 as StdField with uid="DCPOMIEMJU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_numreg", cQueryName = "",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 33474262,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=76, Top=13, cSayPict='"999999"', cGetPict='"999999"'

  add object ocodese_1_2 as StdField with uid="NETTCUSISG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_codese", cQueryName = "",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 13709350,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=415, Top=13, InputMask=replicate('X',4)

  add object ocodute_1_3 as StdField with uid="WBVWTNKWAO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_codute", cQueryName = "",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 15806502,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=156, Top=13, cSayPict='"9999"', cGetPict='"9999"'

  add object oDATREG_1_4 as StdField with uid="SPHTQRVHRW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATREG", cQueryName = "",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 266141750,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=260, Top=13


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=40, width=760,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="MRCODVOC",Label1="Voce costo/ricavo",Field2="MRCODICE",Label2="Centro di costo/ricavo",Field3="MRCODCOM",Label3="Commessa",Field4="MRPARAME",Label4="Parametro",Field5="MR_SEGNO",Label5="D/A",Field6="MRTOTIMP",Label6="Importo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 150166406

  add object oStr_1_8 as StdString with uid="XEIUJDRVSN",Visible=.t., Left=527, Top=224,;
    Alignment=1, Width=63, Height=15,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ARQARSQJJV",Visible=.t., Left=521, Top=277,;
    Alignment=1, Width=96, Height=15,;
    Caption="Differenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="DCPDRXZPUT",Visible=.t., Left=510, Top=251,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da primanota:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="JHLAMNJFHA",Visible=.t., Left=261, Top=307,;
    Alignment=1, Width=316, Height=15,;
    Caption="Competenza dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="RDKQALNQMV",Visible=.t., Left=661, Top=307,;
    Alignment=1, Width=20, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="NYESSGNSRO",Visible=.t., Left=4, Top=224,;
    Alignment=1, Width=93, Height=15,;
    Caption="Voce di C./R.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="QZHZTDZVIH",Visible=.t., Left=4, Top=251,;
    Alignment=1, Width=93, Height=15,;
    Caption="Centro di C./R.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="ZSKYONBFTD",Visible=.t., Left=4, Top=277,;
    Alignment=1, Width=93, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="MDBCYMZTKN",Visible=.t., Left=2, Top=13,;
    Alignment=1, Width=72, Height=15,;
    Caption="Num. reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="ATJFSXZBVD",Visible=.t., Left=199, Top=13,;
    Alignment=1, Width=59, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="CKFXSXUBEU",Visible=.t., Left=347, Top=13,;
    Alignment=1, Width=66, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="DDSMQCWCMF",Visible=.t., Left=148, Top=13,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oBox_1_7 as StdBox with uid="MXLZTBIXOI",left=140, top=44, width=0,height=173

  add object oBox_1_13 as StdBox with uid="GCVDHCAFGE",left=279, top=43, width=0,height=173

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=61,;
    width=757+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=62,width=756+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VOC_COST|CENCOST|CAN_TIER|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESVOC_2_5.Refresh()
      this.Parent.oDESPIA_2_6.Refresh()
      this.Parent.oDESCAN_2_7.Refresh()
      this.Parent.oMRINICOM_2_10.Refresh()
      this.Parent.oMRFINCOM_2_11.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VOC_COST'
        oDropInto=this.oBodyCol.oRow.oMRCODVOC_2_1
      case cFile='CENCOST'
        oDropInto=this.oBodyCol.oRow.oMRCODICE_2_2
      case cFile='CAN_TIER'
        oDropInto=this.oBodyCol.oRow.oMRCODCOM_2_3
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESVOC_2_5 as StdTrsField with uid="RGTUFPQLCI",rtseq=15,rtrep=.t.,;
    cFormVar="w_DESVOC",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione voce di costo o ricavo",;
    HelpContextID = 209777718,;
    cTotal="", bFixedPos=.t., cQueryName = "DESVOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=100, Top=225, InputMask=replicate('X',40)

  add object oDESPIA_2_6 as StdTrsField with uid="KVBWIIZOGD",rtseq=16,rtrep=.t.,;
    cFormVar="w_DESPIA",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione centro di costo o ricavo",;
    HelpContextID = 169538614,;
    cTotal="", bFixedPos=.t., cQueryName = "DESPIA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=100, Top=252, InputMask=replicate('X',40)

  add object oDESCAN_2_7 as StdTrsField with uid="JOPKQHESXB",rtseq=17,rtrep=.t.,;
    cFormVar="w_DESCAN",value=space(30),enabled=.f.,;
    ToolTipText = "Descrizione della commessa",;
    HelpContextID = 109966390,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCAN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=100, Top=279, InputMask=replicate('X',30)

  add object oMRINICOM_2_10 as StdTrsField with uid="LAGFXYUIVO",rtseq=20,rtrep=.t.,;
    cFormVar="w_MRINICOM",value=ctod("  /  /  "),;
    ToolTipText = "Data di inizio competenza; l'intervallo deve essere vuoto oppure finito",;
    HelpContextID = 202924563,;
    cTotal="", bFixedPos=.t., cQueryName = "MRINICOM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Data inizio competenza incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=580, Top=306, tabstop=.f.

  add object oMRFINCOM_2_11 as StdTrsField with uid="ZFLJTUCEAJ",rtseq=21,rtrep=.t.,;
    cFormVar="w_MRFINCOM",value=ctod("  /  /  "),;
    ToolTipText = "Data di fine competenza; l'intervallo deve essere vuoto oppure finito",;
    HelpContextID = 207827475,;
    cTotal="", bFixedPos=.t., cQueryName = "MRFINCOM",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Data fine competenza incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=684, Top=306, tabstop=.f.

  func oMRFINCOM_2_11.mCond()
    with this.Parent.oContained
      return (not empty(.w_mrinicom))
    endwith
  endfunc

  func oMRFINCOM_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_mrinicom<=.w_mrfincom)
    endwith
    return bRes
  endfunc

  add object oObj_2_13 as cp_runprogram with uid="JCBIPLCGID",width=370,height=19,;
   left=403, top=397,;
    caption='GSCG_BC2(A)',;
   bGlobalFont=.t.,;
    prg="GSCG_BC2('A')",;
    cEvent = "w_MR_SEGNO Changed,w_MRPARAME Changed",;
    nPag=2;
    , HelpContextID = 208918040

  add object oObj_2_15 as cp_runprogram with uid="RPVXELOXNV",width=370,height=19,;
   left=403, top=420,;
    caption='GSCG_BC2(D)',;
   bGlobalFont=.t.,;
    prg="GSCG_BC2('D')",;
    cEvent = "Row deleted",;
    nPag=2;
    , HelpContextID = 208918808

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTQUA_3_2 as StdField with uid="FWCFXNABCA",rtseq=24,rtrep=.f.,;
    cFormVar="w_TOTQUA",value=0,enabled=.f.,;
    ToolTipText = "Importo proveniente dalla primanota",;
    HelpContextID = 182193974,;
    cQueryName = "TOTQUA",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=620, Top=250, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oTOTVIS_3_3 as StdField with uid="OWZBTDYCXW",rtseq=25,rtrep=.f.,;
    cFormVar="w_TOTVIS",value=0,enabled=.f.,;
    HelpContextID = 203493174,;
    cQueryName = "TOTVIS",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=620, Top=224, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oSEGTOT_3_4 as StdField with uid="CHWSHHVNTD",rtseq=26,rtrep=.f.,;
    cFormVar="w_SEGTOT",value=space(1),enabled=.f.,;
    HelpContextID = 226374950,;
    cQueryName = "SEGTOT",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=592, Top=224, InputMask=replicate('X',1)

  add object oTOTDIF_3_5 as StdField with uid="KMBZNQBRDE",rtseq=27,rtrep=.f.,;
    cFormVar="w_TOTDIF",value=0,enabled=.f.,;
    HelpContextID = 252645174,;
    cQueryName = "TOTDIF",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=620, Top=276, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oBox_3_6 as StdBox with uid="DYZOMMTWXT",left=566, top=43, width=1,height=173

  add object oBox_3_7 as StdBox with uid="ITTUADPTWB",left=421, top=43, width=1,height=173

  add object oBox_3_8 as StdBox with uid="JCLGSWVFXE",left=604, top=43, width=1,height=173
enddefine

* --- Defining Body row
define class tgsca_mmxBodyRow as CPBodyRowCnt
  Width=747
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMRCODVOC_2_1 as StdTrsField with uid="UIDDAPITPZ",rtseq=11,rtrep=.t.,;
    cFormVar="w_MRCODVOC",value=space(15),;
    ToolTipText = "Codice voce di costo o ricavo",;
    HelpContextID = 248054281,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=-2, Top=0, cSayPict=[p_MCE], cGetPict=[p_MCE], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_MRCODVOC"

  func oMRCODVOC_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODVOC_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMRCODVOC_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oMRCODVOC_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo o ricavo",'',this.parent.oContained
  endproc
  proc oMRCODVOC_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_MRCODVOC
    i_obj.ecpSave()
  endproc

  add object oMRCODICE_2_2 as StdTrsField with uid="OMJYAJEIEX",rtseq=12,rtrep=.t.,;
    cFormVar="w_MRCODICE",value=space(15),;
    ToolTipText = "Codice centro di costo e ricavo",;
    HelpContextID = 29950475,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=137, Top=0, cSayPict=[p_CEN], cGetPict=[p_CEN], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_MRCODICE"

  func oMRCODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODICE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMRCODICE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oMRCODICE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo",'',this.parent.oContained
  endproc
  proc oMRCODICE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_MRCODICE
    i_obj.ecpSave()
  endproc

  add object oMRCODCOM_2_3 as StdTrsField with uid="DYNITUPAPW",rtseq=13,rtrep=.t.,;
    cFormVar="w_MRCODCOM",value=space(15),;
    ToolTipText = "Codice commessa",;
    HelpContextID = 197722643,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=278, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_MRCODCOM"

  func oMRCODCOM_2_3.mCond()
    with this.Parent.oContained
      return (g_PERCAN='S')
    endwith
  endfunc

  func oMRCODCOM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODCOM_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMRCODCOM_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oMRCODCOM_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oMRCODCOM_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_MRCODCOM
    i_obj.ecpSave()
  endproc

  add object oMRPARAME_2_4 as StdTrsField with uid="QUZRGUBCNX",rtseq=14,rtrep=.t.,;
    cFormVar="w_MRPARAME",value=0,;
    ToolTipText = "Eventuale parametro di suddivisione del costo",;
    HelpContextID = 177984011,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Parametro non valorizzato",;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=419, Top=0, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  proc oMRPARAME_2_4.mDefault
    with this.Parent.oContained
      if empty(.w_MRPARAME)
        .w_MRPARAME = 1
      endif
    endwith
  endproc

  func oMRPARAME_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT (.w_MRPARAME=0))
    endwith
    return bRes
  endfunc

  add object oMR_SEGNO_2_8 as StdTrsField with uid="PXYBFUSTBJ",rtseq=18,rtrep=.t.,;
    cFormVar="w_MR_SEGNO",value=space(1),;
    ToolTipText = "Sezione importo: D= dare; A =avere",;
    HelpContextID = 266256917,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=32, Left=564, Top=0, cSayPict=["!"], cGetPict=["!"], InputMask=replicate('X',1)

  func oMR_SEGNO_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MR_SEGNO $ "DA")
    endwith
    return bRes
  endfunc

  add object oMRTOTIMP_2_9 as StdTrsField with uid="TWOHHIGMOA",rtseq=19,rtrep=.t.,;
    cFormVar="w_MRTOTIMP",value=0,;
    ToolTipText = "Importo",;
    HelpContextID = 46797334,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=142, Left=600, Top=0, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oObj_2_14 as cp_runprogram with uid="OKBVDPAGGU",width=265,height=19,;
   left=398, top=312,;
    caption='GSCG_BC2(C)',;
   bGlobalFont=.t.,;
    prg="GSCG_BC2('C')",;
    cEvent = "w_MRTOTIMP Changed",;
    nPag=2;
    , HelpContextID = 208918552
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oMRCODVOC_2_1.When()
    return(.t.)
  proc oMRCODVOC_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMRCODVOC_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsca_mmx','MOVICOST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MRSERIAL=MOVICOST.MRSERIAL";
  +" and "+i_cAliasName2+".MRROWORD=MOVICOST.MRROWORD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsca_mmx
proc Esci(parent)
     parent.notifyevent('Done')
  return
endproc
* --- Fine Area Manuale
