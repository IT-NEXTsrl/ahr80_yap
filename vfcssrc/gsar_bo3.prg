* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bo3                                                        *
*              Autonumber cliente                                              *
*                                                                              *
*      Author: Mimmo Fasano                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-10                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bo3",oParentObject,m.pOPER)
return(i_retval)

define class tgsar_bo3 as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_OBJECT = .NULL.
  w_PROG = .NULL.
  w_GSAR_MAL = .NULL.
  w_GSAR_MCO = .NULL.
  w_NCPERSON = space(125)
  w_NC_EMAIL = space(254)
  w_NC_EMPEC = space(254)
  w_NCTELEFO = space(20)
  w_NCNUMCEL = space(20)
  w_NC_SKYPE = space(50)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiusta il Codice Cliente/Fornitore calcolato dall'Autonumber alla effettiva lunghezza della Picture Parametrica p_NOM (da GSAR_KCC)
    if this.pOPER="A"
      if g_OFNUME="S"
        if LEN(ALLTRIM(p_CLF))<>0
          this.oParentObject.w_ANCODICE = RIGHT(this.oParentObject.w_ANCODICE, LEN(ALLTRIM(p_CLF)))
        endif
      endif
    else
      this.w_OBJECT = GSAR_ACL()
      this.w_OBJECT.EcpLoad()     
      this.w_OBJECT.w_ANTIPCON = "C"
      this.w_OBJECT.w_ANDESCRI = this.oParentObject.w_CCDESCRI
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANDESCRI")
      this.w_PROG.Value = this.oParentObject.w_CCDESCRI
      this.w_OBJECT.w_ANDESCR2 = this.oParentObject.w_CCDESCR2
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANDESCR2")
      this.w_PROG.Value = this.oParentObject.w_CCDESCR2
      this.w_OBJECT.w_ANINDIRI = LEFT(this.oParentObject.w_CCINDIRI,35)
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANINDIRI")
      this.w_PROG.Value = this.oParentObject.w_CCINDIRI
      this.w_OBJECT.w_ANINDIR2 = left(this.oParentObject.w_CCNOINDI_2,35)
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANINDIR2")
      this.w_PROG.Value = this.oParentObject.w_CCINDIR2
      this.w_OBJECT.w_AN___CAP = this.oParentObject.w_CC___CAP
      this.w_PROG = this.w_OBJECT.GetcTRL("w_AN___CAP")
      this.w_PROG.Value = this.oParentObject.w_CC___CAP
      this.w_OBJECT.w_ANLOCALI = this.oParentObject.w_CCLOCALI
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANLOCALI")
      this.w_PROG.Value = this.oParentObject.w_CCLOCALI
      this.w_OBJECT.w_ANPROVIN = this.oParentObject.w_CCPROVIN
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANPROVIN")
      this.w_PROG.Value = this.oParentObject.w_CCPROVIN
      this.w_OBJECT.w_ANNAZION = iif(empty(nvl(this.oParentObject.w_CCNAZION,space(3))),space(3),this.oParentObject.w_CCNAZION)
      = setvaluelinked ( "M" , this.w_OBJECT , "w_ANNAZION" , this.oParentObject.w_CCNAZION)
      this.w_OBJECT.w_ANPARIVA = this.oParentObject.w_CCPARIVA
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANPARIVA")
      this.w_PROG.Value = this.oParentObject.w_CCPARIVA
      this.w_OBJECT.w_ANCODFIS = this.oParentObject.w_CCCODFIS
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANCODFIS")
      this.w_PROG.Value = this.oParentObject.w_CCCODFIS
      this.w_OBJECT.w_ANTELEFO = this.oParentObject.w_CCTELEFO
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANTELEFO")
      this.w_PROG.Value = this.oParentObject.w_CCTELEFO
      this.w_OBJECT.w_ANTELFAX = this.oParentObject.w_CCTELFAX
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANTELFAX")
      this.w_PROG.Value = this.oParentObject.w_CCTELFAX
      this.w_OBJECT.w_ANINDWEB = LEFT(this.oParentObject.w_CCINDWEB,50)
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANINDWEB")
      this.w_PROG.Value = this.oParentObject.w_CCINDWEB
      this.w_OBJECT.w_AN_EMAIL = this.oParentObject.w_CC_EMAIL
      this.w_PROG = this.w_OBJECT.GetcTRL("w_AN_EMAIL")
      this.w_PROG.Value = this.oParentObject.w_CC_EMAIL
      this.w_OBJECT.w_AN_EMPEC = this.oParentObject.w_CC_EMPEC
      this.w_PROG = this.w_OBJECT.GetcTRL("w_AN_EMPEC")
      this.w_PROG.Value = this.oParentObject.w_CC_EMPEC
      this.w_OBJECT.w_ANDTINVA = this.oParentObject.w_CCDTINVA
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANDTINVA")
      this.w_PROG.Value = this.oParentObject.w_CCDTINVA
      this.w_OBJECT.w_ANDTOBSO = this.oParentObject.w_CCDTOBSO
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANDTOBSO")
      this.w_PROG.Value = this.oParentObject.w_CCDTOBSO
      this.w_OBJECT.w_ANCODZON = this.oParentObject.w_CCCODZON
      = setvaluelinked ( "M" , this.w_OBJECT , "w_ANCODZON" , this.oParentObject.w_CCCODZON)
      this.w_OBJECT.w_ANCODLIN = this.oParentObject.w_CCCODLIN
      = setvaluelinked ( "M" , this.w_OBJECT , "w_ANCODLIN" , this.oParentObject.w_CCCODLIN)
      this.w_OBJECT.w_ANCODVAL = this.oParentObject.w_CCCODVAL
      = setvaluelinked ( "M" , this.w_OBJECT , "w_ANCODVAL" , this.oParentObject.w_CCCODVAL)
      this.w_OBJECT.w_ANCODAG1 = this.oParentObject.w_CCCODAGE
      = setvaluelinked ( "M" , this.w_OBJECT , "w_ANCODAG1" , this.oParentObject.w_CCCODAGE)
      this.w_OBJECT.w_ANCODSAL = this.oParentObject.w_CCCODSAL
      if ISAHE()
        this.w_OBJECT.GSAR_MAL.LinkPCClick(.T.)     
        this.w_GSAR_MAL = this.w_object.GSAR_MAL.Cnt
        this.w_OBJECT.GSAR_MAL.Hide()     
        this.w_GSAR_MAL.AddRow()     
        this.w_GSAR_MAL.w_ASCODLIS = this.oParentObject.w_NONUMLIS
        = setvaluelinked ( "D" , this.w_GSAR_MAL , "w_ASCODLIS" , this.oParentObject.w_NONUMLIS)
        this.w_GSAR_MAL.Saverow()     
        this.w_OBJECT.GSAR_MAL.ECPSAVE()     
      else
        this.w_OBJECT.w_ANNUMLIS = this.oParentObject.w_NONUMLIS
        = setvaluelinked ( "M" , this.w_OBJECT , "w_ANNUMLIS" , this.oParentObject.w_NONUMLIS)
      endif
      if Isahr()
        * --- Select from CONTATTI
        do vq_exec with 'CONTATTI',this,'_Curs_CONTATTI','',.f.,.t.
        if used('_Curs_CONTATTI')
          select _Curs_CONTATTI
          locate for 1=1
          do while not(eof())
          this.w_NCPERSON = ALLTRIM(_Curs_CONTATTI.NCPERSON)
          this.w_NC_EMAIL = ALLTRIM(_Curs_CONTATTI.NC_EMAIL)
          this.w_NC_EMPEC = ALLTRIM(_Curs_CONTATTI.NC_EMPEC)
          this.w_NCTELEFO = ALLTRIM(_Curs_CONTATTI.NCTELEFO)
          this.w_NCNUMCEL = ALLTRIM(_Curs_CONTATTI.NCNUMCEL)
          this.w_NC_SKYPE = ALLTRIM(_Curs_CONTATTI.NC_SKYPE)
          this.w_OBJECT.OPGFRM.ActivePage = 7
          this.w_GSAR_MCO = this.w_object.GSAR_MCO
          this.w_GSAR_MCO.AddRow()     
          this.w_GSAR_MCO.w_CORIFPER = this.w_NCPERSON
          this.w_GSAR_MCO.w_COINDMAI = this.w_NC_EMAIL
          this.w_GSAR_MCO.w_COINDPEC = this.w_NC_EMPEC
          this.w_GSAR_MCO.w_CONUMTEL = this.w_NCTELEFO
          this.w_GSAR_MCO.w_CONUMCEL = this.w_NCNUMCEL
          this.w_GSAR_MCO.w_CO_SKYPE = this.w_NC_SKYPE
          this.w_GSAR_MCO.Saverow()     
            select _Curs_CONTATTI
            continue
          enddo
          use
        endif
        this.w_OBJECT.OPGFRM.ActivePage = 1
        this.w_NCPERSON = ""
        this.w_NC_EMAIL = ""
        this.w_NC_EMPEC = ""
        this.w_NCTELEFO = ""
        this.w_NCNUMCEL = ""
        this.w_NC_SKYPE = ""
      endif
      this.w_OBJECT.w_ANCODBAN = this.oParentObject.w_NOCODBAN
      = setvaluelinked ( "M" , this.w_OBJECT , "w_ANCODBAN" , this.oParentObject.w_NOCODBAN)
      this.w_OBJECT.w_ANCODBA2 = this.oParentObject.w_NOCODBA2
      = setvaluelinked ( "M" , this.w_OBJECT , "w_ANCODBA2" , this.oParentObject.w_NOCODBA2)
      this.w_OBJECT.w_ANCATCOM = this.oParentObject.w_NOCATCOM
      = setvaluelinked ( "M" , this.w_OBJECT , "w_ANCATCOM" , this.oParentObject.w_NOCATCOM)
      this.w_OBJECT.w_ANCATCON = this.oParentObject.w_CCCATCON
      = setvaluelinked ( "M" , this.w_OBJECT , "w_ANCATCON" , this.oParentObject.w_CCCATCON)
      this.w_OBJECT.w_ANCONSUP = this.oParentObject.w_CCMASCON
      = setvaluelinked ( "M" , this.w_OBJECT , "w_ANCONSUP" , this.oParentObject.w_CCMASCON)
      this.w_OBJECT.w_ANCODPAG = this.oParentObject.w_CCCODPAG
      = setvaluelinked ( "M" , this.w_OBJECT , "w_ANCODPAG" , this.oParentObject.w_CCCODPAG)
      this.w_OBJECT.w_DANOM = .T.
      this.w_OBJECT.w_TIPMAS = "C"
      this.w_OBJECT.w_ANNUMCEL = this.oParentObject.w_CCNUMCEL
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANNUMCEL")
      this.w_PROG.Value = this.oParentObject.w_CCNUMCEL
      this.w_OBJECT.w_AN_SKYPE = this.oParentObject.w_CC_SKYPE
      this.w_PROG = this.w_OBJECT.GetcTRL("w_AN_SKYPE")
      this.w_PROG.Value = this.oParentObject.w_CC_SKYPE
      this.w_OBJECT.w_ANPERFIS = iif(LEFT(this.oParentObject.w_PERFIS,1)="P","S","N")
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANPERFIS")
      this.w_PROG.Value = iif(LEFT(this.oParentObject.w_PERFIS,1)="P",1,0)
      this.w_OBJECT.w_ANCOGNOM = this.oParentObject.w_CCCOGNOM
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANCOGNOM")
      this.w_PROG.Value = this.oParentObject.w_CCCOGNOM
      this.w_OBJECT.w_AN__NOME = this.oParentObject.w_CC__NOME
      this.w_PROG = this.w_OBJECT.GetcTRL("w_AN__NOME")
      this.w_PROG.Value = this.oParentObject.w_CC__NOME
      this.w_OBJECT.w_ANLOCNAS = this.oParentObject.w_CCLOCNAS
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANLOCNAS")
      this.w_PROG.Value = this.oParentObject.w_CCLOCNAS
      this.w_OBJECT.w_ANPRONAS = this.oParentObject.w_CCPRONAS
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANPRONAS")
      this.w_PROG.Value = this.oParentObject.w_CCPRONAS
      this.w_OBJECT.w_ANDATNAS = this.oParentObject.w_CCDATNAS
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANDATNAS")
      this.w_PROG.Value = this.oParentObject.w_CCDATNAS
      this.w_OBJECT.w_ANNUMCAR = this.oParentObject.w_CCNUMCAR
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANNUMCAR")
      this.w_PROG.Value = this.oParentObject.w_CCNUMCAR
      this.w_OBJECT.w_ANCHKSTA = this.oParentObject.w_CCCHKSTA
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANCHKSTA")
      this.w_PROG.Value = IIF(this.oParentObject.w_CCCHKSTA="S",1,0)
      this.w_OBJECT.w_ANCHKMAI = this.oParentObject.w_CCCHKMAI
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANCHKMAI")
      this.w_PROG.Value = IIF(this.oParentObject.w_CCCHKMAI="S",1,0)
      this.w_OBJECT.w_ANCHKPEC = this.oParentObject.w_CCCHKPEC
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANCHKPEC")
      this.w_PROG.Value = IIF(this.oParentObject.w_CCCHKPEC="S",1,0)
      this.w_OBJECT.w_ANCHKFAX = this.oParentObject.w_CCCHKFAX
      this.w_PROG = this.w_OBJECT.GetcTRL("w_ANCHKFAX")
      this.w_PROG.Value = IIF(this.oParentObject.w_CCCHKFAX="S",1,0)
      this.w_OBJECT.w_NULIV = 1
      Public g_GSAR_KCC 
 g_GSAR_KCC=This.Oparentobject
      this.w_OBJECT = .null.
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_CONTATTI')
      use in _Curs_CONTATTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
