* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kpa                                                        *
*              Situazione partita                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_47]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-25                                                      *
* Last revis.: 2007-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kpa",oParentObject))

* --- Class definition
define class tgscg_kpa as StdForm
  Top    = 122
  Left   = 6

  * --- Standard Properties
  Width  = 591
  Height = 309
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-09"
  HelpContextID=149562729
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_kpa"
  cComment = "Situazione partita"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTNUMPAR = space(31)
  w_FLSALD = space(1)
  w_PTDATSCA = ctod('  /  /  ')
  w_PTCODVAL = space(3)
  w_DATSCA = ctod('  /  /  ')
  w_TOTDAR = 0
  w_TOTAVE = 0
  w_SALDO = 0
  w_SALDO1 = 0
  w_ZoomScad = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kpaPag1","gscg_kpa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLSALD_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomScad = this.oPgFrm.Pages(1).oPag.ZoomScad
    DoDefault()
    proc Destroy()
      this.w_ZoomScad = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PTTIPCON=space(1)
      .w_PTCODCON=space(15)
      .w_PTNUMPAR=space(31)
      .w_FLSALD=space(1)
      .w_PTDATSCA=ctod("  /  /  ")
      .w_PTCODVAL=space(3)
      .w_DATSCA=ctod("  /  /  ")
      .w_TOTDAR=0
      .w_TOTAVE=0
      .w_SALDO=0
      .w_SALDO1=0
      .w_PTTIPCON=oParentObject.w_PTTIPCON
      .w_PTCODCON=oParentObject.w_PTCODCON
      .w_PTNUMPAR=oParentObject.w_PTNUMPAR
      .w_PTDATSCA=oParentObject.w_PTDATSCA
      .w_PTCODVAL=oParentObject.w_PTCODVAL
          .DoRTCalc(1,3,.f.)
        .w_FLSALD = 'P'
          .DoRTCalc(5,6,.f.)
        .w_DATSCA = cp_CharToDate('  -  -  ')
      .oPgFrm.Page1.oPag.ZoomScad.Calculate()
          .DoRTCalc(8,9,.f.)
        .w_SALDO = .w_TOTDAR-.w_TOTAVE
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .w_SALDO1 = .w_TOTAVE-.w_TOTDAR
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PTTIPCON=.w_PTTIPCON
      .oParentObject.w_PTCODCON=.w_PTCODCON
      .oParentObject.w_PTNUMPAR=.w_PTNUMPAR
      .oParentObject.w_PTDATSCA=.w_PTDATSCA
      .oParentObject.w_PTCODVAL=.w_PTCODVAL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .DoRTCalc(1,9,.t.)
            .w_SALDO = .w_TOTDAR-.w_TOTAVE
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
            .w_SALDO1 = .w_TOTAVE-.w_TOTDAR
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSALDO_1_13.visible=!this.oPgFrm.Page1.oPag.oSALDO_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oSALDO1_1_18.visible=!this.oPgFrm.Page1.oPag.oSALDO1_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomScad.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPTNUMPAR_1_4.value==this.w_PTNUMPAR)
      this.oPgFrm.Page1.oPag.oPTNUMPAR_1_4.value=this.w_PTNUMPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSALD_1_5.RadioValue()==this.w_FLSALD)
      this.oPgFrm.Page1.oPag.oFLSALD_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDAR_1_11.value==this.w_TOTDAR)
      this.oPgFrm.Page1.oPag.oTOTDAR_1_11.value=this.w_TOTDAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTAVE_1_12.value==this.w_TOTAVE)
      this.oPgFrm.Page1.oPag.oTOTAVE_1_12.value=this.w_TOTAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDO_1_13.value==this.w_SALDO)
      this.oPgFrm.Page1.oPag.oSALDO_1_13.value=this.w_SALDO
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDO1_1_18.value==this.w_SALDO1)
      this.oPgFrm.Page1.oPag.oSALDO1_1_18.value=this.w_SALDO1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kpaPag1 as StdContainer
  Width  = 587
  height = 309
  stdWidth  = 587
  stdheight = 309
  resizeXpos=285
  resizeYpos=212
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="XHAOSCQJNZ",left=530, top=260, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142245306;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPTNUMPAR_1_4 as StdField with uid="FPIIQPNGWC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PTNUMPAR", cQueryName = "PTNUMPAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(31), bMultilanguage =  .f.,;
    ToolTipText = "Numero della partita selezionata",;
    HelpContextID = 62909368,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=79, Top=9, InputMask=replicate('X',31)


  add object oFLSALD_1_5 as StdCombo with uid="PHHPRMWNIK",rtseq=4,rtrep=.f.,left=454,top=9,width=128,height=21;
    , ToolTipText = "Imposta il metodo di ricerca";
    , HelpContextID = 266577066;
    , cFormVar="w_FLSALD",RowSource=""+"Intera partita,"+"Singola scadenza,"+"Saldo scadenze", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSALD_1_5.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'S',;
    iif(this.value =3,'D',;
    ' '))))
  endfunc
  func oFLSALD_1_5.GetRadio()
    this.Parent.oContained.w_FLSALD = this.RadioValue()
    return .t.
  endfunc

  func oFLSALD_1_5.SetRadio()
    this.Parent.oContained.w_FLSALD=trim(this.Parent.oContained.w_FLSALD)
    this.value = ;
      iif(this.Parent.oContained.w_FLSALD=='P',1,;
      iif(this.Parent.oContained.w_FLSALD=='S',2,;
      iif(this.Parent.oContained.w_FLSALD=='D',3,;
      0)))
  endfunc


  add object ZoomScad as cp_zoombox with uid="FPNEZTROYA",left=1, top=34, width=591,height=222,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='PAR_TITE',cZoomFile='GSCG_MPA',bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,;
    cEvent = "Blank,Calcola",;
    nPag=1;
    , HelpContextID = 28434918

  add object oTOTDAR_1_11 as StdField with uid="GUIPOYRYPA",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TOTDAR", cQueryName = "TOTDAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale saldo dare",;
    HelpContextID = 43028682,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=158, Top=261, cSayPict="v_PV(40+VVL)", cGetPict="v_PV(40+VVL)"

  add object oTOTAVE_1_12 as StdField with uid="CPVLUCFZOJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_TOTAVE", cQueryName = "TOTAVE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale saldo avere",;
    HelpContextID = 239309002,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=287, Top=261, cSayPict="v_PV(40+VVL)", cGetPict="v_PV(40+VVL)"

  add object oSALDO_1_13 as StdField with uid="DZOXUPPTEB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SALDO", cQueryName = "SALDO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale saldo dare",;
    HelpContextID = 61939418,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=158, Top=284, cSayPict="v_PV(40+VVL)", cGetPict="v_PV(40+VVL)"

  func oSALDO_1_13.mHide()
    with this.Parent.oContained
      return ((.w_TOTDAR-.w_TOTAVE)<=0)
    endwith
  endfunc


  add object oObj_1_14 as cp_runprogram with uid="CWLSQNGRLE",left=90, top=312, width=216,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCG_BPA',;
    cEvent = "w_FLSALD Changed,Blank",;
    nPag=1;
    , HelpContextID = 28434918

  add object oSALDO1_1_18 as StdField with uid="ROADQQMHTF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SALDO1", cQueryName = "SALDO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale saldo avere",;
    HelpContextID = 45162202,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=287, Top=284, cSayPict="v_PV(40+VVL)", cGetPict="v_PV(40+VVL)"

  func oSALDO1_1_18.mHide()
    with this.Parent.oContained
      return ((.w_TOTAVE-.w_TOTDAR)<0)
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="JDXCOQHYSM",Visible=.t., Left=3, Top=9,;
    Alignment=1, Width=75, Height=15,;
    Caption="Num.partita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="ZFAFVZGFNH",Visible=.t., Left=257, Top=9,;
    Alignment=1, Width=195, Height=15,;
    Caption="Dettaglio su:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="DCPWZAMRDR",Visible=.t., Left=6, Top=283,;
    Alignment=1, Width=150, Height=15,;
    Caption="Saldo:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return ((.w_TOTDAR-.w_TOTAVE)<=0)
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="CBODSRGMGD",Visible=.t., Left=6, Top=261,;
    Alignment=1, Width=150, Height=15,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="DTIURTLPMK",Visible=.t., Left=131, Top=283,;
    Alignment=1, Width=150, Height=15,;
    Caption="Saldo:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return ((.w_TOTAVE-.w_TOTDAR)<0)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kpa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
