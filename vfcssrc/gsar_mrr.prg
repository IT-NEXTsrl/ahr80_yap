* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mrr                                                        *
*              Modelli righe riferimenti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-15                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_mrr"))

* --- Class definition
define class tgsar_mrr as StdTrsForm
  Top    = 68
  Left   = 34

  * --- Standard Properties
  Width  = 546
  Height = 403+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=113866601
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  MODMRIFE_IDX = 0
  MODDRIFE_IDX = 0
  LINGUE_IDX = 0
  cFile = "MODMRIFE"
  cFileDetail = "MODDRIFE"
  cKeySelect = "MDCODICE"
  cKeyWhere  = "MDCODICE=this.w_MDCODICE"
  cKeyDetail  = "MDCODICE=this.w_MDCODICE and MDLINGUA=this.w_MDLINGUA"
  cKeyWhereODBC = '"MDCODICE="+cp_ToStrODBC(this.w_MDCODICE)';

  cKeyDetailWhereODBC = '"MDCODICE="+cp_ToStrODBC(this.w_MDCODICE)';
      +'+" and MDLINGUA="+cp_ToStrODBC(this.w_MDLINGUA)';

  cKeyWhereODBCqualified = '"MODDRIFE.MDCODICE="+cp_ToStrODBC(this.w_MDCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mrr"
  cComment = "Modelli righe riferimenti"
  i_nRowNum = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MDCODICE = space(5)
  w_MDDESCRI = space(35)
  w_MDLINGUA = space(3)
  w_MD_NSRIF = space(254)
  w_MD_VSRIF = space(254)
  w_MD_DSRIF = space(254)
  w_DESCRI = space(35)
  w_NORIGHE = .F.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MODMRIFE','gsar_mrr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mrrPag1","gsar_mrr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Modelli righe riferimenti")
      .Pages(1).HelpContextID = 258060243
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMDCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='LINGUE'
    this.cWorkTables[2]='MODMRIFE'
    this.cWorkTables[3]='MODDRIFE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MODMRIFE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MODMRIFE_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MDCODICE = NVL(MDCODICE,space(5))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from MODMRIFE where MDCODICE=KeySet.MDCODICE
    *
    i_nConn = i_TableProp[this.MODMRIFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2],this.bLoadRecFilter,this.MODMRIFE_IDX,"gsar_mrr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MODMRIFE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MODMRIFE.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"MODDRIFE.","MODMRIFE.")
      i_cTable = i_cTable+' MODMRIFE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MDCODICE',this.w_MDCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_NORIGHE = .F.
        .w_MDCODICE = NVL(MDCODICE,space(5))
        .w_MDDESCRI = NVL(MDDESCRI,space(35))
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'MODMRIFE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from MODDRIFE where MDCODICE=KeySet.MDCODICE
      *                            and MDLINGUA=KeySet.MDLINGUA
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.MODDRIFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODDRIFE_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('MODDRIFE')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "MODDRIFE.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" MODDRIFE"
        link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'MDCODICE',this.w_MDCODICE  )
        select * from (i_cTable) MODDRIFE where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCRI = space(35)
          .w_MDLINGUA = NVL(MDLINGUA,space(3))
          if link_2_1_joined
            this.w_MDLINGUA = NVL(LUCODICE201,NVL(this.w_MDLINGUA,space(3)))
            this.w_DESCRI = NVL(LUDESCRI201,space(35))
          else
          .link_2_1('Load')
          endif
          .w_MD_NSRIF = NVL(MD_NSRIF,space(254))
          .w_MD_VSRIF = NVL(MD_VSRIF,space(254))
          .w_MD_DSRIF = NVL(MD_DSRIF,space(254))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace MDLINGUA with .w_MDLINGUA
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_MDCODICE=space(5)
      .w_MDDESCRI=space(35)
      .w_MDLINGUA=space(3)
      .w_MD_NSRIF=space(254)
      .w_MD_VSRIF=space(254)
      .w_MD_DSRIF=space(254)
      .w_DESCRI=space(35)
      .w_NORIGHE=.f.
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_MDLINGUA))
         .link_2_1('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(4,7,.f.)
        .w_NORIGHE = .F.
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MODMRIFE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMDCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oMDDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oMD_NSRIF_2_2.enabled = i_bVal
      .Page1.oPag.oMD_VSRIF_2_3.enabled = i_bVal
      .Page1.oPag.oMD_DSRIF_2_4.enabled = i_bVal
      .Page1.oPag.oObj_1_4.enabled = i_bVal
      .Page1.oPag.oObj_1_5.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMDCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMDCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MODMRIFE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MODMRIFE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDCODICE,"MDCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDDESCRI,"MDDESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MODMRIFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2])
    i_lTable = "MODMRIFE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MODMRIFE_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MDLINGUA C(3);
      ,t_MD_NSRIF C(254);
      ,t_MD_VSRIF C(254);
      ,t_MD_DSRIF C(254);
      ,t_DESCRI C(35);
      ,MDLINGUA C(3);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mrrbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMDLINGUA_2_1.controlsource=this.cTrsName+'.t_MDLINGUA'
    this.oPgFRm.Page1.oPag.oMD_NSRIF_2_2.controlsource=this.cTrsName+'.t_MD_NSRIF'
    this.oPgFRm.Page1.oPag.oMD_VSRIF_2_3.controlsource=this.cTrsName+'.t_MD_VSRIF'
    this.oPgFRm.Page1.oPag.oMD_DSRIF_2_4.controlsource=this.cTrsName+'.t_MD_DSRIF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_5.controlsource=this.cTrsName+'.t_DESCRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(89)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDLINGUA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MODMRIFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MODMRIFE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MODMRIFE')
        i_extval=cp_InsertValODBCExtFlds(this,'MODMRIFE')
        local i_cFld
        i_cFld=" "+;
                  "(MDCODICE,MDDESCRI"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_MDCODICE)+;
                    ","+cp_ToStrODBC(this.w_MDDESCRI)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MODMRIFE')
        i_extval=cp_InsertValVFPExtFlds(this,'MODMRIFE')
        cp_CheckDeletedKey(i_cTable,0,'MDCODICE',this.w_MDCODICE)
        INSERT INTO (i_cTable);
              (MDCODICE,MDDESCRI &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_MDCODICE;
                  ,this.w_MDDESCRI;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MODDRIFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODDRIFE_IDX,2])
      *
      * insert into MODDRIFE
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(MDCODICE,MDLINGUA,MD_NSRIF,MD_VSRIF,MD_DSRIF,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MDCODICE)+","+cp_ToStrODBCNull(this.w_MDLINGUA)+","+cp_ToStrODBC(this.w_MD_NSRIF)+","+cp_ToStrODBC(this.w_MD_VSRIF)+","+cp_ToStrODBC(this.w_MD_DSRIF)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'MDCODICE',this.w_MDCODICE,'MDLINGUA',this.w_MDLINGUA)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_MDCODICE,this.w_MDLINGUA,this.w_MD_NSRIF,this.w_MD_VSRIF,this.w_MD_DSRIF,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MODMRIFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update MODMRIFE
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'MODMRIFE')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " MDDESCRI="+cp_ToStrODBC(this.w_MDDESCRI)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'MODMRIFE')
          i_cWhere = cp_PKFox(i_cTable  ,'MDCODICE',this.w_MDCODICE  )
          UPDATE (i_cTable) SET;
              MDDESCRI=this.w_MDDESCRI;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_MDLINGUA))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.MODDRIFE_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.MODDRIFE_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from MODDRIFE
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and MDLINGUA="+cp_ToStrODBC(&i_TN.->MDLINGUA)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and MDLINGUA=&i_TN.->MDLINGUA;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace MDLINGUA with this.w_MDLINGUA
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MODDRIFE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MD_NSRIF="+cp_ToStrODBC(this.w_MD_NSRIF)+;
                     ",MD_VSRIF="+cp_ToStrODBC(this.w_MD_VSRIF)+;
                     ",MD_DSRIF="+cp_ToStrODBC(this.w_MD_DSRIF)+;
                     " ,MDLINGUA="+cp_ToStrODBC(this.w_MDLINGUA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and MDLINGUA="+cp_ToStrODBC(MDLINGUA)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MD_NSRIF=this.w_MD_NSRIF;
                     ,MD_VSRIF=this.w_MD_VSRIF;
                     ,MD_DSRIF=this.w_MD_DSRIF;
                     ,MDLINGUA=this.w_MDLINGUA;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and MDLINGUA=&i_TN.->MDLINGUA;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_MDLINGUA))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.MODDRIFE_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MODDRIFE_IDX,2])
        *
        * delete MODDRIFE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and MDLINGUA="+cp_ToStrODBC(&i_TN.->MDLINGUA)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and MDLINGUA=&i_TN.->MDLINGUA;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.MODMRIFE_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2])
        *
        * delete MODMRIFE
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_MDLINGUA))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MODMRIFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MDLINGUA
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDLINGUA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_MDLINGUA)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_MDLINGUA))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDLINGUA)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LUDESCRI like "+cp_ToStrODBC(trim(this.w_MDLINGUA)+"%");

            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LUDESCRI like "+cp_ToStr(trim(this.w_MDLINGUA)+"%");

            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MDLINGUA) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oMDLINGUA_2_1'),i_cWhere,'GSAR_ALG',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDLINGUA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_MDLINGUA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_MDLINGUA)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDLINGUA = NVL(_Link_.LUCODICE,space(3))
      this.w_DESCRI = NVL(_Link_.LUDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MDLINGUA = space(3)
      endif
      this.w_DESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDLINGUA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LINGUE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.LUCODICE as LUCODICE201"+ ",link_2_1.LUDESCRI as LUDESCRI201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on MODDRIFE.MDLINGUA=link_2_1.LUCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and MODDRIFE.MDLINGUA=link_2_1.LUCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMDCODICE_1_1.value==this.w_MDCODICE)
      this.oPgFrm.Page1.oPag.oMDCODICE_1_1.value=this.w_MDCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMDDESCRI_1_2.value==this.w_MDDESCRI)
      this.oPgFrm.Page1.oPag.oMDDESCRI_1_2.value=this.w_MDDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMD_NSRIF_2_2.value==this.w_MD_NSRIF)
      this.oPgFrm.Page1.oPag.oMD_NSRIF_2_2.value=this.w_MD_NSRIF
      replace t_MD_NSRIF with this.oPgFrm.Page1.oPag.oMD_NSRIF_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMD_VSRIF_2_3.value==this.w_MD_VSRIF)
      this.oPgFrm.Page1.oPag.oMD_VSRIF_2_3.value=this.w_MD_VSRIF
      replace t_MD_VSRIF with this.oPgFrm.Page1.oPag.oMD_VSRIF_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMD_DSRIF_2_4.value==this.w_MD_DSRIF)
      this.oPgFrm.Page1.oPag.oMD_DSRIF_2_4.value=this.w_MD_DSRIF
      replace t_MD_DSRIF with this.oPgFrm.Page1.oPag.oMD_DSRIF_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDLINGUA_2_1.value==this.w_MDLINGUA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDLINGUA_2_1.value=this.w_MDLINGUA
      replace t_MDLINGUA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDLINGUA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_5.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_5.value=this.w_DESCRI
      replace t_DESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'MODMRIFE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MDCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMDCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_MDCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_mrr
      this.NotifyEvent('CheckRighe')
      If i_bRes And this.w_NORIGHE
         * -- Errore
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg = Ah_MsgFormat("Impossibile salvare il modello senza righe di dettaglio")
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_MDLINGUA))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_MDLINGUA)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MDLINGUA=space(3)
      .w_MD_NSRIF=space(254)
      .w_MD_VSRIF=space(254)
      .w_MD_DSRIF=space(254)
      .w_DESCRI=space(35)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_MDLINGUA))
        .link_2_1('Full')
      endif
    endwith
    this.DoRTCalc(4,8,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MDLINGUA = t_MDLINGUA
    this.w_MD_NSRIF = t_MD_NSRIF
    this.w_MD_VSRIF = t_MD_VSRIF
    this.w_MD_DSRIF = t_MD_DSRIF
    this.w_DESCRI = t_DESCRI
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MDLINGUA with this.w_MDLINGUA
    replace t_MD_NSRIF with this.w_MD_NSRIF
    replace t_MD_VSRIF with this.w_MD_VSRIF
    replace t_MD_DSRIF with this.w_MD_DSRIF
    replace t_DESCRI with this.w_DESCRI
    if i_srv='A'
      replace MDLINGUA with this.w_MDLINGUA
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mrrPag1 as StdContainer
  Width  = 542
  height = 403
  stdWidth  = 542
  stdheight = 403
  resizeXpos=346
  resizeYpos=233
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMDCODICE_1_1 as StdField with uid="QLMVOHVNOB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MDCODICE", cQueryName = "MDCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice modello",;
    HelpContextID = 113902091,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=115, Top=24, InputMask=replicate('X',5)

  add object oMDDESCRI_1_2 as StdField with uid="JYDTUOKCJW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MDDESCRI", cQueryName = "MDDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 28316175,;
   bGlobalFont=.t.,;
    Height=21, Width=337, Left=182, Top=24, InputMask=replicate('X',35)


  add object oObj_1_4 as cp_runprogram with uid="FBHIKALFKA",left=24, top=422, width=227,height=23,;
    caption='GSAR_BRR(R)',;
   bGlobalFont=.t.,;
    prg="GSAR_BRR('R')",;
    cEvent = "CheckRighe",;
    nPag=1;
    , HelpContextID = 25154872


  add object oObj_1_5 as cp_runprogram with uid="MBFXUHBLVN",left=26, top=446, width=226,height=21,;
    caption='GSARBRR(N)',;
   bGlobalFont=.t.,;
    prg="GSAR_BRR('N')",;
    cEvent = "w_MDLINGUA Changed",;
    nPag=1;
    , HelpContextID = 263003758


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=26, top=61, width=509,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=2,Field1="MDLINGUA",Label1="Lingua",Field2="DESCRI",Label2="Descrizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202224250

  add object oStr_1_3 as StdString with uid="PLGOTGGMYR",Visible=.t., Left=22, Top=24,;
    Alignment=1, Width=90, Height=18,;
    Caption="Codice modello:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=16,top=80,;
    width=505+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=17,top=81,width=504+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='LINGUE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oMD_NSRIF_2_2.Refresh()
      this.Parent.oMD_VSRIF_2_3.Refresh()
      this.Parent.oMD_DSRIF_2_4.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='LINGUE'
        oDropInto=this.oBodyCol.oRow.oMDLINGUA_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oMD_NSRIF_2_2 as StdTrsField with uid="NAYKDNCCJF",rtseq=4,rtrep=.t.,;
    cFormVar="w_MD_NSRIF",value=space(254),;
    ToolTipText = "Formula nostro riferimento",;
    HelpContextID = 256196084,;
    cTotal="", bFixedPos=.t., cQueryName = "MD_NSRIF",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=459, Left=70, Top=321, InputMask=replicate('X',254)

  add object oMD_VSRIF_2_3 as StdTrsField with uid="IUWOVOXAKJ",rtseq=5,rtrep=.t.,;
    cFormVar="w_MD_VSRIF",value=space(254),;
    ToolTipText = "Formula vostro riferimento",;
    HelpContextID = 255671796,;
    cTotal="", bFixedPos=.t., cQueryName = "MD_VSRIF",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=459, Left=70, Top=348, InputMask=replicate('X',254)

  add object oMD_DSRIF_2_4 as StdTrsField with uid="PWDZCSODXW",rtseq=6,rtrep=.t.,;
    cFormVar="w_MD_DSRIF",value=space(254),;
    ToolTipText = "Formula riferimento descrittivo",;
    HelpContextID = 256851444,;
    cTotal="", bFixedPos=.t., cQueryName = "MD_DSRIF",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=459, Left=70, Top=375, InputMask=replicate('X',254)

  add object oStr_2_6 as StdString with uid="VOEGOMPWAP",Visible=.t., Left=13, Top=321,;
    Alignment=1, Width=55, Height=18,;
    Caption="Ns.rif.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="ENPGNXVHPG",Visible=.t., Left=18, Top=348,;
    Alignment=1, Width=50, Height=18,;
    Caption="Vs.rif.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="UDBIFIRVAM",Visible=.t., Left=9, Top=375,;
    Alignment=1, Width=60, Height=18,;
    Caption="Rif. desc.:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mrrBodyRow as CPBodyRowCnt
  Width=495
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMDLINGUA_2_1 as StdTrsField with uid="IEHFLMBZSZ",rtseq=3,rtrep=.t.,;
    cFormVar="w_MDLINGUA",value=space(3),isprimarykey=.t.,;
    ToolTipText = "Codice lingua",;
    HelpContextID = 90477063,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=-2, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_MDLINGUA"

  func oMDLINGUA_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDLINGUA_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMDLINGUA_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oMDLINGUA_2_1.readonly and this.parent.oMDLINGUA_2_1.isprimarykey)
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oMDLINGUA_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"",'',this.parent.oContained
   endif
  endproc
  proc oMDLINGUA_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_MDLINGUA
    i_obj.ecpSave()
  endproc

  add object oDESCRI_2_5 as StdTrsField with uid="PNRSOZKSRY",rtseq=7,rtrep=.t.,;
    cFormVar="w_DESCRI",value=space(35),;
    ToolTipText = "Descrizione lingua",;
    HelpContextID = 140574154,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=423, Left=67, Top=0, InputMask=replicate('X',35)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oMDLINGUA_2_1.When()
    return(.t.)
  proc oMDLINGUA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMDLINGUA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mrr','MODMRIFE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MDCODICE=MODMRIFE.MDCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
