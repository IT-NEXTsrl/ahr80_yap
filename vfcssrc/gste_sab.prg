* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_sab                                                        *
*              Stampa avviso di bonifico a fornitore                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_109]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-07                                                      *
* Last revis.: 2009-12-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_sab",oParentObject))

* --- Class definition
define class tgste_sab as StdForm
  Top    = 6
  Left   = 12

  * --- Standard Properties
  Width  = 720
  Height = 407
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-15"
  HelpContextID=124400745
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  DIS_TINT_IDX = 0
  PRO_NUME_IDX = 0
  cPrg = "gste_sab"
  cComment = "Stampa avviso di bonifico a fornitore"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AVVISO = ctod('  /  /  ')
  w_TIPO = space(2)
  w_NUMERO = space(10)
  w_NUMAVVISO = 0
  w_SERIE = space(2)
  w_DEL = ctod('  /  /  ')
  w_NUMDIS = 0
  w_ANNO = space(4)
  w_DATDIS = ctod('  /  /  ')
  w_SELEZI = space(1)
  w_RAGFOR = space(1)
  w_DETPAR = space(1)
  w_DECI = 0
  w_ONUME = 0
  o_ONUME = 0
  w_NOFILT = space(10)
  w_Zoomcc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_sabPag1","gste_sab",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNUMDIS_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoomcc = this.oPgFrm.Pages(1).oPag.Zoomcc
    DoDefault()
    proc Destroy()
      this.w_Zoomcc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='DIS_TINT'
    this.cWorkTables[2]='PRO_NUME'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSTE_BBB(this,0)
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_sab
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AVVISO=ctod("  /  /  ")
      .w_TIPO=space(2)
      .w_NUMERO=space(10)
      .w_NUMAVVISO=0
      .w_SERIE=space(2)
      .w_DEL=ctod("  /  /  ")
      .w_NUMDIS=0
      .w_ANNO=space(4)
      .w_DATDIS=ctod("  /  /  ")
      .w_SELEZI=space(1)
      .w_RAGFOR=space(1)
      .w_DETPAR=space(1)
      .w_DECI=0
      .w_ONUME=0
      .w_NOFILT=space(10)
          .DoRTCalc(1,1,.f.)
        .w_TIPO = 'BO'
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_NUMERO))
          .link_1_3('Full')
        endif
        .w_NUMAVVISO = 1
          .DoRTCalc(5,5,.f.)
        .w_DEL = i_datsys
          .DoRTCalc(7,7,.f.)
        .w_ANNO = STR(YEAR(i_datsys),4,0)
      .oPgFrm.Page1.oPag.Zoomcc.Calculate()
          .DoRTCalc(9,9,.f.)
        .w_SELEZI = 'D'
        .w_RAGFOR = ' '
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .w_DETPAR = ' '
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
          .DoRTCalc(13,14,.f.)
        .w_NOFILT = 'A'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_1_3('Full')
        .oPgFrm.Page1.oPag.Zoomcc.Calculate()
        .DoRTCalc(4,10,.t.)
        if .o_ONUME<>.w_ONUME
            .w_RAGFOR = ' '
        endif
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        if .o_ONUME<>.w_ONUME
            .w_DETPAR = ' '
        endif
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .DoRTCalc(13,14,.t.)
            .w_NOFILT = 'A'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoomcc.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oRAGFOR_1_13.visible=!this.oPgFrm.Page1.oPag.oRAGFOR_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDETPAR_1_16.visible=!this.oPgFrm.Page1.oPag.oDETPAR_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoomcc.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NUMERO
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
    i_lTable = "DIS_TINT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2], .t., this.DIS_TINT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMERO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMERO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DINUMDIS,DIDATDIS,DITIPDIS,DIDATAVV";
                   +" from "+i_cTable+" "+i_lTable+" where DINUMDIS="+cp_ToStrODBC(this.w_NUMERO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DINUMDIS',this.w_NUMERO)
            select DINUMDIS,DIDATDIS,DITIPDIS,DIDATAVV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMERO = NVL(_Link_.DINUMDIS,space(10))
      this.w_DATDIS = NVL(cp_ToDate(_Link_.DIDATDIS),ctod("  /  /  "))
      this.w_TIPO = NVL(_Link_.DITIPDIS,space(2))
      this.w_AVVISO = NVL(cp_ToDate(_Link_.DIDATAVV),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NUMERO = space(10)
      endif
      this.w_DATDIS = ctod("  /  /  ")
      this.w_TIPO = space(2)
      this.w_AVVISO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])+'\'+cp_ToStr(_Link_.DINUMDIS,1)
      cp_ShowWarn(i_cKey,this.DIS_TINT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMERO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNUMDIS_1_7.value==this.w_NUMDIS)
      this.oPgFrm.Page1.oPag.oNUMDIS_1_7.value=this.w_NUMDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_8.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_8.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIS_1_9.value==this.w_DATDIS)
      this.oPgFrm.Page1.oPag.oDATDIS_1_9.value=this.w_DATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_12.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGFOR_1_13.RadioValue()==this.w_RAGFOR)
      this.oPgFrm.Page1.oPag.oRAGFOR_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDETPAR_1_16.RadioValue()==this.w_DETPAR)
      this.oPgFrm.Page1.oPag.oDETPAR_1_16.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ONUME = this.w_ONUME
    return

enddefine

* --- Define pages as container
define class tgste_sabPag1 as StdContainer
  Width  = 716
  height = 407
  stdWidth  = 716
  stdheight = 407
  resizeXpos=470
  resizeYpos=295
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMDIS_1_7 as StdField with uid="NRWVRCSTPB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMDIS", cQueryName = "NUMDIS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero della distinta selezionata",;
    HelpContextID = 261163562,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=92, Top=37, cSayPict='"999999"', cGetPict='"999999"'

  add object oANNO_1_8 as StdField with uid="OCKCGAIIEH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento selezionato",;
    HelpContextID = 118882810,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=194, Top=37, InputMask=replicate('X',4)

  add object oDATDIS_1_9 as StdField with uid="KVNRULZWJE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATDIS", cQueryName = "DATDIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della distinta selezionata",;
    HelpContextID = 261140170,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=327, Top=37


  add object oBtn_1_10 as StdButton with uid="GXQSYKFTRR",left=663, top=37, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premi per rieseguire la ricerca";
    , HelpContextID = 52521494;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        GSTE_BBB(this.Parent.oContained,2)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NUMERO))
      endwith
    endif
  endfunc


  add object Zoomcc as cp_szoombox with uid="ZSLMRKIWND",left=4, top=89, width=707,height=237,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DIS_TINT",cZoomFile="GSTE0SAJ",bOptions=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSTE_ADI",bAdvOptions=.t.,bReadOnly=.t.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 53596902

  add object oSELEZI_1_12 as StdRadio with uid="HMSCKZEDMI",rtseq=10,rtrep=.f.,left=8, top=332, width=170,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 142617050
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 142617050
      this.Buttons(2).Top=15
      this.SetAll("Width",168)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_12.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_12.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  add object oRAGFOR_1_13 as StdCheck with uid="EFYYJBAFRO",rtseq=11,rtrep=.f.,left=92, top=62, caption="Raggruppa per fornitore",;
    ToolTipText = "Se attivo raggruppa la stampa per fornitore",;
    HelpContextID = 3112426,;
    cFormVar="w_RAGFOR", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oRAGFOR_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oRAGFOR_1_13.GetRadio()
    this.Parent.oContained.w_RAGFOR = this.RadioValue()
    return .t.
  endfunc

  func oRAGFOR_1_13.SetRadio()
    this.Parent.oContained.w_RAGFOR=trim(this.Parent.oContained.w_RAGFOR)
    this.value = ;
      iif(this.Parent.oContained.w_RAGFOR=='S',1,;
      0)
  endfunc

  func oRAGFOR_1_13.mHide()
    with this.Parent.oContained
      return (.w_ONUME=1)
    endwith
  endfunc


  add object oObj_1_14 as cp_outputCombo with uid="ZLERXIVPWT",left=330, top=332, width=381,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 53596902


  add object oBtn_1_15 as StdButton with uid="UTKSOKOLGG",left=613, top=358, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 251046618;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSTE_BBB(this.Parent.oContained,0)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDETPAR_1_16 as StdCheck with uid="LODFBZEBIH",rtseq=12,rtrep=.f.,left=327, top=62, caption="Dettaglio partite",;
    ToolTipText = "Se attivo stampa il dettaglio partite",;
    HelpContextID = 17083082,;
    cFormVar="w_DETPAR", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oDETPAR_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oDETPAR_1_16.GetRadio()
    this.Parent.oContained.w_DETPAR = this.RadioValue()
    return .t.
  endfunc

  func oDETPAR_1_16.SetRadio()
    this.Parent.oContained.w_DETPAR=trim(this.Parent.oContained.w_DETPAR)
    this.value = ;
      iif(this.Parent.oContained.w_DETPAR=='S',1,;
      0)
  endfunc

  func oDETPAR_1_16.mHide()
    with this.Parent.oContained
      return (.w_ONUME=1)
    endwith
  endfunc


  add object oBtn_1_17 as StdButton with uid="ZUIUDZBZVE",left=663, top=358, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 117083322;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_22 as cp_runprogram with uid="AVASWCAVOH",left=325, top=433, width=158,height=18,;
    caption='GSTE_BAK',;
   bGlobalFont=.t.,;
    prg="GSTE_BAK",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 254778447


  add object oObj_1_23 as cp_runprogram with uid="IWZAFGJFCH",left=1, top=433, width=158,height=18,;
    caption='GSTE_BBB(1)',;
   bGlobalFont=.t.,;
    prg="GSTE_BBB(1)",;
    cEvent = "w_SERIE Changed,Init",;
    nPag=1;
    , HelpContextID = 254597336


  add object oObj_1_24 as cp_runprogram with uid="DDWLHOAOEI",left=162, top=433, width=158,height=18,;
    caption='GSTE_BBB(3)',;
   bGlobalFont=.t.,;
    prg="GSTE_BBB(3)",;
    cEvent = "w_NUMDIS Changed",;
    nPag=1;
    , HelpContextID = 254596824


  add object oBtn_1_30 as StdButton with uid="CQEHICTOFE",left=160, top=38, width=19,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 124199722;
  , bGlobalFont=.t.

    proc oBtn_1_30.Click()
      do GSTE_KDO with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_18 as StdString with uid="GHZQNQQSBZ",Visible=.t., Left=6, Top=36,;
    Alignment=1, Width=83, Height=15,;
    Caption="Distinta n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="ZEFAXZGFCS",Visible=.t., Left=265, Top=37,;
    Alignment=1, Width=60, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="QBUNATXTIO",Visible=.t., Left=185, Top=37,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="UFYUVPDRAG",Visible=.t., Left=182, Top=332,;
    Alignment=1, Width=145, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="VFGVSVPSMI",Visible=.t., Left=5, Top=4,;
    Alignment=0, Width=395, Height=15,;
    Caption="Criteri di selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_27 as StdBox with uid="VHOODFWMPV",left=4, top=21, width=397,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_sab','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
