* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_acd                                                        *
*              Causali distinte                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_212]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-31                                                      *
* Last revis.: 2015-10-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_acd"))

* --- Class definition
define class tgste_acd as StdForm
  Top    = 11
  Left   = 59

  * --- Standard Properties
  Width  = 570
  Height = 388+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-22"
  HelpContextID=109720681
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=47

  * --- Constant Properties
  CAU_DIST_IDX = 0
  CAU_CONT_IDX = 0
  VALUTE_IDX = 0
  TIP_DIST_IDX = 0
  VASTRUTT_IDX = 0
  cFile = "CAU_DIST"
  cKeySelect = "CACODICE"
  cKeyWhere  = "CACODICE=this.w_CACODICE"
  cKeyWhereODBC = '"CACODICE="+cp_ToStrODBC(this.w_CACODICE)';

  cKeyWhereODBCqualified = '"CAU_DIST.CACODICE="+cp_ToStrODBC(this.w_CACODICE)';

  cPrg = "gste_acd"
  cComment = "Causali distinte"
  icon = "anag.ico"
  cAutoZoom = 'GSTE0ACD'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CACODICE = space(5)
  w_CADESCAU = space(35)
  w_CATIPSCD = space(1)
  o_CATIPSCD = space(1)
  w_CATIPDIS = space(2)
  o_CATIPDIS = space(2)
  w_CAFLCONT = space(1)
  w_CADIS_RB = space(2)
  w_OLDCONT = space(10)
  w_CADIS_CA = space(2)
  w_CADIS_RI = space(2)
  w_CADIS_RD = space(2)
  w_CADIS_MA = space(2)
  w_CADIS_BO = space(2)
  w_TEST = .F.
  w_CAFLIBAN = space(1)
  w_CACAUBON = space(5)
  w_CATIPCON = space(1)
  w_CAFLAVVI = space(1)
  w_CACODVAD = space(3)
  w_DESVAL = space(35)
  w_CAIMPMIN = 0
  w_CASTRUTT = space(10)
  w_CATIPMAN = space(1)
  w_DECTOT = 0
  o_DECTOT = 0
  w_CALCPICT = 0
  w_CAIMPCOM = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CACODCAU = space(5)
  o_CACODCAU = space(5)
  w_DESCAU = space(35)
  w_TIPREG = space(1)
  w_VALSIM = space(5)
  w_CODISO = space(3)
  w_CAFLBANC = space(1)
  w_CAUDIS = space(5)
  w_CACONSBF = space(1)
  w_CAFLACEF = space(1)
  w_CAFLCASP = space(1)
  w_CADATEFF = space(1)
  w_CA_NOCVS = space(1)
  w_CAFLDSPR = space(1)
  o_CAFLDSPR = space(1)
  w_CADESPAR = space(254)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_STCRISTR = space(100)
  w_STFILXSD = space(254)

  * --- Children pointers
  GSTE_MBC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAU_DIST','gste_acd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_acdPag1","gste_acd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Causale")
      .Pages(1).HelpContextID = 201359654
      .Pages(2).addobject("oPag","tgste_acdPag2","gste_acd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Banche")
      .Pages(2).HelpContextID = 90181910
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCACODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='TIP_DIST'
    this.cWorkTables[4]='VASTRUTT'
    this.cWorkTables[5]='CAU_DIST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAU_DIST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAU_DIST_IDX,3]
  return

  function CreateChildren()
    this.GSTE_MBC = CREATEOBJECT('stdDynamicChild',this,'GSTE_MBC',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.GSTE_MBC.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSTE_MBC)
      this.GSTE_MBC.DestroyChildrenChain()
      this.GSTE_MBC=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSTE_MBC.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSTE_MBC.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSTE_MBC.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSTE_MBC.SetKey(;
            .w_CACODICE,"CBCAUDIS";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSTE_MBC.ChangeRow(this.cRowID+'      1',1;
             ,.w_CACODICE,"CBCAUDIS";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSTE_MBC)
        i_f=.GSTE_MBC.BuildFilter()
        if !(i_f==.GSTE_MBC.cQueryFilter)
          i_fnidx=.GSTE_MBC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSTE_MBC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSTE_MBC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSTE_MBC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSTE_MBC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CACODICE = NVL(CACODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_32_joined
    link_1_32_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAU_DIST where CACODICE=KeySet.CACODICE
    *
    i_nConn = i_TableProp[this.CAU_DIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAU_DIST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAU_DIST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAU_DIST '
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_32_joined=this.AddJoinedLink_1_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CACODICE',this.w_CACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TEST = .F.
        .w_DESVAL = space(35)
        .w_DECTOT = 0
        .w_OBTEST = i_DATSYS
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESCAU = space(35)
        .w_TIPREG = space(1)
        .w_CODISO = g_ISONAZ
        .w_STCRISTR = space(100)
        .w_STFILXSD = space(254)
        .w_CACODICE = NVL(CACODICE,space(5))
        .w_CADESCAU = NVL(CADESCAU,space(35))
        .w_CATIPSCD = NVL(CATIPSCD,space(1))
        .w_CATIPDIS = NVL(CATIPDIS,space(2))
        .w_CAFLCONT = NVL(CAFLCONT,space(1))
        .w_CADIS_RB = NVL(CADIS_RB,space(2))
        .w_OLDCONT = .w_CAFLCONT
        .w_CADIS_CA = NVL(CADIS_CA,space(2))
        .w_CADIS_RI = NVL(CADIS_RI,space(2))
        .w_CADIS_RD = NVL(CADIS_RD,space(2))
        .w_CADIS_MA = NVL(CADIS_MA,space(2))
        .w_CADIS_BO = NVL(CADIS_BO,space(2))
        .w_CAFLIBAN = NVL(CAFLIBAN,space(1))
        .w_CACAUBON = NVL(CACAUBON,space(5))
          * evitabile
          *.link_1_15('Load')
        .w_CATIPCON = NVL(CATIPCON,space(1))
        .w_CAFLAVVI = NVL(CAFLAVVI,space(1))
        .w_CACODVAD = NVL(CACODVAD,space(3))
          if link_1_18_joined
            this.w_CACODVAD = NVL(VACODVAL118,NVL(this.w_CACODVAD,space(3)))
            this.w_DESVAL = NVL(VADESVAL118,space(35))
            this.w_DECTOT = NVL(VADECTOT118,0)
          else
          .link_1_18('Load')
          endif
        .w_CAIMPMIN = NVL(CAIMPMIN,0)
        .w_CASTRUTT = NVL(CASTRUTT,space(10))
          if link_1_21_joined
            this.w_CASTRUTT = NVL(STCODICE121,NVL(this.w_CASTRUTT,space(10)))
            this.w_STCRISTR = NVL(STCRISTR121,space(100))
            this.w_STFILXSD = NVL(STFILXSD121,space(254))
          else
          .link_1_21('Load')
          endif
        .w_CATIPMAN = NVL(CATIPMAN,space(1))
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CAIMPCOM = NVL(CAIMPCOM,0)
        .w_CACODCAU = NVL(CACODCAU,space(5))
          if link_1_32_joined
            this.w_CACODCAU = NVL(CCCODICE132,NVL(this.w_CACODCAU,space(5)))
            this.w_DESCAU = NVL(CCDESCRI132,space(35))
            this.w_TIPREG = NVL(CCTIPREG132,space(1))
          else
          .link_1_32('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .w_VALSIM = g_VALSIM
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .w_CAFLBANC = NVL(CAFLBANC,space(1))
        .w_CAUDIS = .w_CACODICE
        .w_CACONSBF = NVL(CACONSBF,space(1))
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .w_CAFLACEF = NVL(CAFLACEF,space(1))
        .w_CAFLCASP = NVL(CAFLCASP,space(1))
        .w_CADATEFF = NVL(CADATEFF,space(1))
        .w_CA_NOCVS = NVL(CA_NOCVS,space(1))
        .w_CAFLDSPR = NVL(CAFLDSPR,space(1))
        .w_CADESPAR = NVL(CADESPAR,space(254))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'CAU_DIST')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gste_acd
    This.w_CALCPICT=DEFPIC(This.w_DECTOT)
    
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_CACAUBON")
    CTRL_CONCON.Popola()
    * --- Utilizzo la mHideControls() per rendere visibile la combo
    * --- anche in interroga.
    this.mHideControls()
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CACODICE = space(5)
      .w_CADESCAU = space(35)
      .w_CATIPSCD = space(1)
      .w_CATIPDIS = space(2)
      .w_CAFLCONT = space(1)
      .w_CADIS_RB = space(2)
      .w_OLDCONT = space(10)
      .w_CADIS_CA = space(2)
      .w_CADIS_RI = space(2)
      .w_CADIS_RD = space(2)
      .w_CADIS_MA = space(2)
      .w_CADIS_BO = space(2)
      .w_TEST = .f.
      .w_CAFLIBAN = space(1)
      .w_CACAUBON = space(5)
      .w_CATIPCON = space(1)
      .w_CAFLAVVI = space(1)
      .w_CACODVAD = space(3)
      .w_DESVAL = space(35)
      .w_CAIMPMIN = 0
      .w_CASTRUTT = space(10)
      .w_CATIPMAN = space(1)
      .w_DECTOT = 0
      .w_CALCPICT = 0
      .w_CAIMPCOM = 0
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_CACODCAU = space(5)
      .w_DESCAU = space(35)
      .w_TIPREG = space(1)
      .w_VALSIM = space(5)
      .w_CODISO = space(3)
      .w_CAFLBANC = space(1)
      .w_CAUDIS = space(5)
      .w_CACONSBF = space(1)
      .w_CAFLACEF = space(1)
      .w_CAFLCASP = space(1)
      .w_CADATEFF = space(1)
      .w_CA_NOCVS = space(1)
      .w_CAFLDSPR = space(1)
      .w_CADESPAR = space(254)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_STCRISTR = space(100)
      .w_STFILXSD = space(254)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_CATIPSCD = 'C'
        .w_CATIPDIS = 'RB'
        .w_CAFLCONT = 'N'
        .w_CADIS_RB = 'RB'
        .w_OLDCONT = .w_CAFLCONT
          .DoRTCalc(8,12,.f.)
        .w_TEST = .F.
        .w_CAFLIBAN = 'S'
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_CACAUBON))
          .link_1_15('Full')
          endif
        .w_CATIPCON = 'C'
        .DoRTCalc(17,18,.f.)
          if not(empty(.w_CACODVAD))
          .link_1_18('Full')
          endif
          .DoRTCalc(19,20,.f.)
        .w_CASTRUTT = Space(10)
        .DoRTCalc(21,21,.f.)
          if not(empty(.w_CASTRUTT))
          .link_1_21('Full')
          endif
        .w_CATIPMAN = 'T'
          .DoRTCalc(23,23,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
          .DoRTCalc(25,25,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(27,28,.f.)
          if not(empty(.w_CACODCAU))
          .link_1_32('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
          .DoRTCalc(29,30,.f.)
        .w_VALSIM = g_VALSIM
        .w_CODISO = g_ISONAZ
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .w_CAFLBANC = 'A'
        .w_CAUDIS = .w_CACODICE
        .w_CACONSBF = 'C'
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
          .DoRTCalc(36,37,.f.)
        .w_CADATEFF = iif(.w_CATIPSCD='C', 'S','P')
        .w_CA_NOCVS = ' '
        .w_CAFLDSPR = ' '
        .w_CADESPAR = space(254)
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAU_DIST')
    this.DoRTCalc(42,47,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gste_acd
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_CACAUBON")
    CTRL_CONCON.Popola()
    * --- Utilizzo la mHideControls() per rendere visibile la combo
    * --- anche in interroga.
    this.mHideControls()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCACODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCADESCAU_1_2.enabled = i_bVal
      .Page1.oPag.oCATIPSCD_1_3.enabled = i_bVal
      .Page1.oPag.oCATIPDIS_1_4.enabled = i_bVal
      .Page1.oPag.oCAFLCONT_1_5.enabled = i_bVal
      .Page1.oPag.oCADIS_RB_1_6.enabled = i_bVal
      .Page1.oPag.oCADIS_CA_1_8.enabled = i_bVal
      .Page1.oPag.oCADIS_RI_1_9.enabled = i_bVal
      .Page1.oPag.oCADIS_RD_1_10.enabled = i_bVal
      .Page1.oPag.oCADIS_MA_1_11.enabled = i_bVal
      .Page1.oPag.oCADIS_BO_1_12.enabled = i_bVal
      .Page1.oPag.oCAFLIBAN_1_14.enabled = i_bVal
      .Page1.oPag.oCACAUBON_1_15.enabled = i_bVal
      .Page1.oPag.oCATIPCON_1_16.enabled = i_bVal
      .Page1.oPag.oCAFLAVVI_1_17.enabled = i_bVal
      .Page1.oPag.oCACODVAD_1_18.enabled = i_bVal
      .Page1.oPag.oCAIMPMIN_1_20.enabled = i_bVal
      .Page1.oPag.oCASTRUTT_1_21.enabled = i_bVal
      .Page1.oPag.oCATIPMAN_1_22.enabled = i_bVal
      .Page1.oPag.oCAIMPCOM_1_26.enabled = i_bVal
      .Page1.oPag.oCACODCAU_1_32.enabled = i_bVal
      .Page1.oPag.oCACONSBF_1_47.enabled = i_bVal
      .Page1.oPag.oCAFLACEF_1_55.enabled = i_bVal
      .Page1.oPag.oCAFLCASP_1_56.enabled = i_bVal
      .Page1.oPag.oCADATEFF_1_57.enabled = i_bVal
      .Page1.oPag.oCA_NOCVS_1_59.enabled = i_bVal
      .Page1.oPag.oCAFLDSPR_1_60.enabled = i_bVal
      .Page1.oPag.oBtn_1_61.enabled = .Page1.oPag.oBtn_1_61.mCond()
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      .Page1.oPag.oObj_1_44.enabled = i_bVal
      .Page1.oPag.oObj_1_49.enabled = i_bVal
      .Page1.oPag.oObj_1_51.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCACODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCACODICE_1_1.enabled = .t.
        .Page1.oPag.oCADESCAU_1_2.enabled = .t.
      endif
    endwith
    this.GSTE_MBC.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CAU_DIST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSTE_MBC.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAU_DIST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODICE,"CACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADESCAU,"CADESCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPSCD,"CATIPSCD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPDIS,"CATIPDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLCONT,"CAFLCONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADIS_RB,"CADIS_RB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADIS_CA,"CADIS_CA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADIS_RI,"CADIS_RI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADIS_RD,"CADIS_RD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADIS_MA,"CADIS_MA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADIS_BO,"CADIS_BO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLIBAN,"CAFLIBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACAUBON,"CACAUBON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPCON,"CATIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLAVVI,"CAFLAVVI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODVAD,"CACODVAD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAIMPMIN,"CAIMPMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CASTRUTT,"CASTRUTT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPMAN,"CATIPMAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAIMPCOM,"CAIMPCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODCAU,"CACODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLBANC,"CAFLBANC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACONSBF,"CACONSBF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLACEF,"CAFLACEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLCASP,"CAFLCASP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADATEFF,"CADATEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CA_NOCVS,"CA_NOCVS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLDSPR,"CAFLDSPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADESPAR,"CADESPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAU_DIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])
    i_lTable = "CAU_DIST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAU_DIST_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCG_SCD with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAU_DIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAU_DIST_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAU_DIST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAU_DIST')
        i_extval=cp_InsertValODBCExtFlds(this,'CAU_DIST')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CACODICE,CADESCAU,CATIPSCD,CATIPDIS,CAFLCONT"+;
                  ",CADIS_RB,CADIS_CA,CADIS_RI,CADIS_RD,CADIS_MA"+;
                  ",CADIS_BO,CAFLIBAN,CACAUBON,CATIPCON,CAFLAVVI"+;
                  ",CACODVAD,CAIMPMIN,CASTRUTT,CATIPMAN,CAIMPCOM"+;
                  ",CACODCAU,CAFLBANC,CACONSBF,CAFLACEF,CAFLCASP"+;
                  ",CADATEFF,CA_NOCVS,CAFLDSPR,CADESPAR,UTCC"+;
                  ",UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CACODICE)+;
                  ","+cp_ToStrODBC(this.w_CADESCAU)+;
                  ","+cp_ToStrODBC(this.w_CATIPSCD)+;
                  ","+cp_ToStrODBC(this.w_CATIPDIS)+;
                  ","+cp_ToStrODBC(this.w_CAFLCONT)+;
                  ","+cp_ToStrODBC(this.w_CADIS_RB)+;
                  ","+cp_ToStrODBC(this.w_CADIS_CA)+;
                  ","+cp_ToStrODBC(this.w_CADIS_RI)+;
                  ","+cp_ToStrODBC(this.w_CADIS_RD)+;
                  ","+cp_ToStrODBC(this.w_CADIS_MA)+;
                  ","+cp_ToStrODBC(this.w_CADIS_BO)+;
                  ","+cp_ToStrODBC(this.w_CAFLIBAN)+;
                  ","+cp_ToStrODBCNull(this.w_CACAUBON)+;
                  ","+cp_ToStrODBC(this.w_CATIPCON)+;
                  ","+cp_ToStrODBC(this.w_CAFLAVVI)+;
                  ","+cp_ToStrODBCNull(this.w_CACODVAD)+;
                  ","+cp_ToStrODBC(this.w_CAIMPMIN)+;
                  ","+cp_ToStrODBCNull(this.w_CASTRUTT)+;
                  ","+cp_ToStrODBC(this.w_CATIPMAN)+;
                  ","+cp_ToStrODBC(this.w_CAIMPCOM)+;
                  ","+cp_ToStrODBCNull(this.w_CACODCAU)+;
                  ","+cp_ToStrODBC(this.w_CAFLBANC)+;
                  ","+cp_ToStrODBC(this.w_CACONSBF)+;
                  ","+cp_ToStrODBC(this.w_CAFLACEF)+;
                  ","+cp_ToStrODBC(this.w_CAFLCASP)+;
                  ","+cp_ToStrODBC(this.w_CADATEFF)+;
                  ","+cp_ToStrODBC(this.w_CA_NOCVS)+;
                  ","+cp_ToStrODBC(this.w_CAFLDSPR)+;
                  ","+cp_ToStrODBC(this.w_CADESPAR)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAU_DIST')
        i_extval=cp_InsertValVFPExtFlds(this,'CAU_DIST')
        cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_CACODICE)
        INSERT INTO (i_cTable);
              (CACODICE,CADESCAU,CATIPSCD,CATIPDIS,CAFLCONT,CADIS_RB,CADIS_CA,CADIS_RI,CADIS_RD,CADIS_MA,CADIS_BO,CAFLIBAN,CACAUBON,CATIPCON,CAFLAVVI,CACODVAD,CAIMPMIN,CASTRUTT,CATIPMAN,CAIMPCOM,CACODCAU,CAFLBANC,CACONSBF,CAFLACEF,CAFLCASP,CADATEFF,CA_NOCVS,CAFLDSPR,CADESPAR,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CACODICE;
                  ,this.w_CADESCAU;
                  ,this.w_CATIPSCD;
                  ,this.w_CATIPDIS;
                  ,this.w_CAFLCONT;
                  ,this.w_CADIS_RB;
                  ,this.w_CADIS_CA;
                  ,this.w_CADIS_RI;
                  ,this.w_CADIS_RD;
                  ,this.w_CADIS_MA;
                  ,this.w_CADIS_BO;
                  ,this.w_CAFLIBAN;
                  ,this.w_CACAUBON;
                  ,this.w_CATIPCON;
                  ,this.w_CAFLAVVI;
                  ,this.w_CACODVAD;
                  ,this.w_CAIMPMIN;
                  ,this.w_CASTRUTT;
                  ,this.w_CATIPMAN;
                  ,this.w_CAIMPCOM;
                  ,this.w_CACODCAU;
                  ,this.w_CAFLBANC;
                  ,this.w_CACONSBF;
                  ,this.w_CAFLACEF;
                  ,this.w_CAFLCASP;
                  ,this.w_CADATEFF;
                  ,this.w_CA_NOCVS;
                  ,this.w_CAFLDSPR;
                  ,this.w_CADESPAR;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAU_DIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAU_DIST_IDX,i_nConn)
      *
      * update CAU_DIST
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAU_DIST')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CADESCAU="+cp_ToStrODBC(this.w_CADESCAU)+;
             ",CATIPSCD="+cp_ToStrODBC(this.w_CATIPSCD)+;
             ",CATIPDIS="+cp_ToStrODBC(this.w_CATIPDIS)+;
             ",CAFLCONT="+cp_ToStrODBC(this.w_CAFLCONT)+;
             ",CADIS_RB="+cp_ToStrODBC(this.w_CADIS_RB)+;
             ",CADIS_CA="+cp_ToStrODBC(this.w_CADIS_CA)+;
             ",CADIS_RI="+cp_ToStrODBC(this.w_CADIS_RI)+;
             ",CADIS_RD="+cp_ToStrODBC(this.w_CADIS_RD)+;
             ",CADIS_MA="+cp_ToStrODBC(this.w_CADIS_MA)+;
             ",CADIS_BO="+cp_ToStrODBC(this.w_CADIS_BO)+;
             ",CAFLIBAN="+cp_ToStrODBC(this.w_CAFLIBAN)+;
             ",CACAUBON="+cp_ToStrODBCNull(this.w_CACAUBON)+;
             ",CATIPCON="+cp_ToStrODBC(this.w_CATIPCON)+;
             ",CAFLAVVI="+cp_ToStrODBC(this.w_CAFLAVVI)+;
             ",CACODVAD="+cp_ToStrODBCNull(this.w_CACODVAD)+;
             ",CAIMPMIN="+cp_ToStrODBC(this.w_CAIMPMIN)+;
             ",CASTRUTT="+cp_ToStrODBCNull(this.w_CASTRUTT)+;
             ",CATIPMAN="+cp_ToStrODBC(this.w_CATIPMAN)+;
             ",CAIMPCOM="+cp_ToStrODBC(this.w_CAIMPCOM)+;
             ",CACODCAU="+cp_ToStrODBCNull(this.w_CACODCAU)+;
             ",CAFLBANC="+cp_ToStrODBC(this.w_CAFLBANC)+;
             ",CACONSBF="+cp_ToStrODBC(this.w_CACONSBF)+;
             ",CAFLACEF="+cp_ToStrODBC(this.w_CAFLACEF)+;
             ",CAFLCASP="+cp_ToStrODBC(this.w_CAFLCASP)+;
             ",CADATEFF="+cp_ToStrODBC(this.w_CADATEFF)+;
             ",CA_NOCVS="+cp_ToStrODBC(this.w_CA_NOCVS)+;
             ",CAFLDSPR="+cp_ToStrODBC(this.w_CAFLDSPR)+;
             ",CADESPAR="+cp_ToStrODBC(this.w_CADESPAR)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAU_DIST')
        i_cWhere = cp_PKFox(i_cTable  ,'CACODICE',this.w_CACODICE  )
        UPDATE (i_cTable) SET;
              CADESCAU=this.w_CADESCAU;
             ,CATIPSCD=this.w_CATIPSCD;
             ,CATIPDIS=this.w_CATIPDIS;
             ,CAFLCONT=this.w_CAFLCONT;
             ,CADIS_RB=this.w_CADIS_RB;
             ,CADIS_CA=this.w_CADIS_CA;
             ,CADIS_RI=this.w_CADIS_RI;
             ,CADIS_RD=this.w_CADIS_RD;
             ,CADIS_MA=this.w_CADIS_MA;
             ,CADIS_BO=this.w_CADIS_BO;
             ,CAFLIBAN=this.w_CAFLIBAN;
             ,CACAUBON=this.w_CACAUBON;
             ,CATIPCON=this.w_CATIPCON;
             ,CAFLAVVI=this.w_CAFLAVVI;
             ,CACODVAD=this.w_CACODVAD;
             ,CAIMPMIN=this.w_CAIMPMIN;
             ,CASTRUTT=this.w_CASTRUTT;
             ,CATIPMAN=this.w_CATIPMAN;
             ,CAIMPCOM=this.w_CAIMPCOM;
             ,CACODCAU=this.w_CACODCAU;
             ,CAFLBANC=this.w_CAFLBANC;
             ,CACONSBF=this.w_CACONSBF;
             ,CAFLACEF=this.w_CAFLACEF;
             ,CAFLCASP=this.w_CAFLCASP;
             ,CADATEFF=this.w_CADATEFF;
             ,CA_NOCVS=this.w_CA_NOCVS;
             ,CAFLDSPR=this.w_CAFLDSPR;
             ,CADESPAR=this.w_CADESPAR;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSTE_MBC : Saving
      this.GSTE_MBC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CACODICE,"CBCAUDIS";
             )
      this.GSTE_MBC.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSTE_MBC : Deleting
    this.GSTE_MBC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CACODICE,"CBCAUDIS";
           )
    this.GSTE_MBC.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAU_DIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAU_DIST_IDX,i_nConn)
      *
      * delete CAU_DIST
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CACODICE',this.w_CACODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAU_DIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_CACODCAU<>.w_CACODCAU
            .w_OLDCONT = .w_CAFLCONT
        endif
        .DoRTCalc(8,14,.t.)
        if .o_CATIPDIS<>.w_CATIPDIS
          .link_1_15('Full')
        endif
        .DoRTCalc(16,20,.t.)
        if .o_CATIPDIS<>.w_CATIPDIS
            .w_CASTRUTT = Space(10)
          .link_1_21('Full')
        endif
        .DoRTCalc(22,23,.t.)
        if .o_DECTOT<>.w_DECTOT
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .DoRTCalc(25,30,.t.)
            .w_VALSIM = g_VALSIM
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .DoRTCalc(32,33,.t.)
            .w_CAUDIS = .w_CACODICE
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .DoRTCalc(35,37,.t.)
        if .o_CATIPSCD<>.w_CATIPSCD
            .w_CADATEFF = iif(.w_CATIPSCD='C', 'S','P')
        endif
        if .o_CATIPDIS<>.w_CATIPDIS
            .w_CA_NOCVS = ' '
        endif
        if .o_CATIPDIS<>.w_CATIPDIS.or. .o_CATIPSCD<>.w_CATIPSCD
            .w_CAFLDSPR = ' '
        endif
        if .o_CAFLDSPR<>.w_CAFLDSPR
            .w_CADESPAR = space(254)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(42,47,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCAFLIBAN_1_14.enabled = this.oPgFrm.Page1.oPag.oCAFLIBAN_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCAFLAVVI_1_17.enabled = this.oPgFrm.Page1.oPag.oCAFLAVVI_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCAIMPCOM_1_26.enabled = this.oPgFrm.Page1.oPag.oCAIMPCOM_1_26.mCond()
    this.oPgFrm.Page1.oPag.oCAFLDSPR_1_60.enabled = this.oPgFrm.Page1.oPag.oCAFLDSPR_1_60.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAFLIBAN_1_14.visible=!this.oPgFrm.Page1.oPag.oCAFLIBAN_1_14.mHide()
    this.oPgFrm.Page1.oPag.oCACAUBON_1_15.visible=!this.oPgFrm.Page1.oPag.oCACAUBON_1_15.mHide()
    this.oPgFrm.Page1.oPag.oCAFLAVVI_1_17.visible=!this.oPgFrm.Page1.oPag.oCAFLAVVI_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCASTRUTT_1_21.visible=!this.oPgFrm.Page1.oPag.oCASTRUTT_1_21.mHide()
    this.oPgFrm.Page1.oPag.oCATIPMAN_1_22.visible=!this.oPgFrm.Page1.oPag.oCATIPMAN_1_22.mHide()
    this.oPgFrm.Page1.oPag.oCAIMPCOM_1_26.visible=!this.oPgFrm.Page1.oPag.oCAIMPCOM_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oCACODCAU_1_32.visible=!this.oPgFrm.Page1.oPag.oCACODCAU_1_32.mHide()
    this.oPgFrm.Page1.oPag.oDESCAU_1_33.visible=!this.oPgFrm.Page1.oPag.oDESCAU_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oVALSIM_1_40.visible=!this.oPgFrm.Page1.oPag.oVALSIM_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oCAFLACEF_1_55.visible=!this.oPgFrm.Page1.oPag.oCAFLACEF_1_55.mHide()
    this.oPgFrm.Page1.oPag.oCAFLCASP_1_56.visible=!this.oPgFrm.Page1.oPag.oCAFLCASP_1_56.mHide()
    this.oPgFrm.Page1.oPag.oCA_NOCVS_1_59.visible=!this.oPgFrm.Page1.oPag.oCA_NOCVS_1_59.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_61.visible=!this.oPgFrm.Page1.oPag.oBtn_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACAUBON
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DIST_IDX,3]
    i_lTable = "TIP_DIST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIST_IDX,2], .t., this.TIP_DIST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACAUBON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DIST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DICAUDIS like "+cp_ToStrODBC(trim(this.w_CACAUBON)+"%");
                   +" and DICODISO="+cp_ToStrODBC(this.w_CODISO);
                   +" and DITIPDIS="+cp_ToStrODBC(this.w_CATIPDIS);

          i_ret=cp_SQL(i_nConn,"select DICODISO,DITIPDIS,DICAUDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DICODISO,DITIPDIS,DICAUDIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DICODISO',this.w_CODISO;
                     ,'DITIPDIS',this.w_CATIPDIS;
                     ,'DICAUDIS',trim(this.w_CACAUBON))
          select DICODISO,DITIPDIS,DICAUDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DICODISO,DITIPDIS,DICAUDIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACAUBON)==trim(_Link_.DICAUDIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACAUBON) and !this.bDontReportError
            deferred_cp_zoom('TIP_DIST','*','DICODISO,DITIPDIS,DICAUDIS',cp_AbsName(oSource.parent,'oCACAUBON_1_15'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODISO<>oSource.xKey(1);
           .or. this.w_CATIPDIS<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DICODISO,DITIPDIS,DICAUDIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DICODISO,DITIPDIS,DICAUDIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DICODISO,DITIPDIS,DICAUDIS";
                     +" from "+i_cTable+" "+i_lTable+" where DICAUDIS="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DICODISO="+cp_ToStrODBC(this.w_CODISO);
                     +" and DITIPDIS="+cp_ToStrODBC(this.w_CATIPDIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DICODISO',oSource.xKey(1);
                       ,'DITIPDIS',oSource.xKey(2);
                       ,'DICAUDIS',oSource.xKey(3))
            select DICODISO,DITIPDIS,DICAUDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACAUBON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DICODISO,DITIPDIS,DICAUDIS";
                   +" from "+i_cTable+" "+i_lTable+" where DICAUDIS="+cp_ToStrODBC(this.w_CACAUBON);
                   +" and DICODISO="+cp_ToStrODBC(this.w_CODISO);
                   +" and DITIPDIS="+cp_ToStrODBC(this.w_CATIPDIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DICODISO',this.w_CODISO;
                       ,'DITIPDIS',this.w_CATIPDIS;
                       ,'DICAUDIS',this.w_CACAUBON)
            select DICODISO,DITIPDIS,DICAUDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACAUBON = NVL(_Link_.DICAUDIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CACAUBON = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DIST_IDX,2])+'\'+cp_ToStr(_Link_.DICODISO,1)+'\'+cp_ToStr(_Link_.DITIPDIS,1)+'\'+cp_ToStr(_Link_.DICAUDIS,1)
      cp_ShowWarn(i_cKey,this.TIP_DIST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACAUBON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODVAD
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODVAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CACODVAD)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CACODVAD))
          select VACODVAL,VADESVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODVAD)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODVAD) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCACODVAD_1_18'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODVAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CACODVAD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CACODVAD)
            select VACODVAL,VADESVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODVAD = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CACODVAD = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODVAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.VACODVAL as VACODVAL118"+ ",link_1_18.VADESVAL as VADESVAL118"+ ",link_1_18.VADECTOT as VADECTOT118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on CAU_DIST.CACODVAD=link_1_18.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and CAU_DIST.CACODVAD=link_1_18.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CASTRUTT
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CASTRUTT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_CASTRUTT)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STCRISTR,STFILXSD";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_CASTRUTT))
          select STCODICE,STCRISTR,STFILXSD;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CASTRUTT)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CASTRUTT) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oCASTRUTT_1_21'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STCRISTR,STFILXSD";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STCRISTR,STFILXSD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CASTRUTT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STCRISTR,STFILXSD";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_CASTRUTT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_CASTRUTT)
            select STCODICE,STCRISTR,STFILXSD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CASTRUTT = NVL(_Link_.STCODICE,space(10))
      this.w_STCRISTR = NVL(_Link_.STCRISTR,space(100))
      this.w_STFILXSD = NVL(_Link_.STFILXSD,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_CASTRUTT = space(10)
      endif
      this.w_STCRISTR = space(100)
      this.w_STFILXSD = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=cifracnf(Alltrim(.w_STCRISTR),'D')=Alltrim(.w_CASTRUTT)+juststem(.w_STFILXSD)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Struttura non ufficiale")
        endif
        this.w_CASTRUTT = space(10)
        this.w_STCRISTR = space(100)
        this.w_STFILXSD = space(254)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CASTRUTT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VASTRUTT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.STCODICE as STCODICE121"+ ",link_1_21.STCRISTR as STCRISTR121"+ ",link_1_21.STFILXSD as STFILXSD121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on CAU_DIST.CASTRUTT=link_1_21.STCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and CAU_DIST.CASTRUTT=link_1_21.STCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACODCAU
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CACODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CACODCAU))
          select CCCODICE,CCDESCRI,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCACODCAU_1_32'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSTE_ACD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CACODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CACODCAU)
            select CCCODICE,CCDESCRI,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_TIPREG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=g_COGE='S' AND (EMPTY(.w_CACODCAU) OR .w_TIPREG $ 'NE')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente o incongruente o obsoleta")
        endif
        this.w_CACODCAU = space(5)
        this.w_DESCAU = space(35)
        this.w_TIPREG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_32.CCCODICE as CCCODICE132"+ ",link_1_32.CCDESCRI as CCDESCRI132"+ ",link_1_32.CCTIPREG as CCTIPREG132"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_32 on CAU_DIST.CACODCAU=link_1_32.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_32"
          i_cKey=i_cKey+'+" and CAU_DIST.CACODCAU=link_1_32.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCACODICE_1_1.value==this.w_CACODICE)
      this.oPgFrm.Page1.oPag.oCACODICE_1_1.value=this.w_CACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESCAU_1_2.value==this.w_CADESCAU)
      this.oPgFrm.Page1.oPag.oCADESCAU_1_2.value=this.w_CADESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPSCD_1_3.RadioValue()==this.w_CATIPSCD)
      this.oPgFrm.Page1.oPag.oCATIPSCD_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPDIS_1_4.RadioValue()==this.w_CATIPDIS)
      this.oPgFrm.Page1.oPag.oCATIPDIS_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLCONT_1_5.RadioValue()==this.w_CAFLCONT)
      this.oPgFrm.Page1.oPag.oCAFLCONT_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADIS_RB_1_6.RadioValue()==this.w_CADIS_RB)
      this.oPgFrm.Page1.oPag.oCADIS_RB_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADIS_CA_1_8.RadioValue()==this.w_CADIS_CA)
      this.oPgFrm.Page1.oPag.oCADIS_CA_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADIS_RI_1_9.RadioValue()==this.w_CADIS_RI)
      this.oPgFrm.Page1.oPag.oCADIS_RI_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADIS_RD_1_10.RadioValue()==this.w_CADIS_RD)
      this.oPgFrm.Page1.oPag.oCADIS_RD_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADIS_MA_1_11.RadioValue()==this.w_CADIS_MA)
      this.oPgFrm.Page1.oPag.oCADIS_MA_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADIS_BO_1_12.RadioValue()==this.w_CADIS_BO)
      this.oPgFrm.Page1.oPag.oCADIS_BO_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLIBAN_1_14.RadioValue()==this.w_CAFLIBAN)
      this.oPgFrm.Page1.oPag.oCAFLIBAN_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACAUBON_1_15.RadioValue()==this.w_CACAUBON)
      this.oPgFrm.Page1.oPag.oCACAUBON_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPCON_1_16.RadioValue()==this.w_CATIPCON)
      this.oPgFrm.Page1.oPag.oCATIPCON_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLAVVI_1_17.RadioValue()==this.w_CAFLAVVI)
      this.oPgFrm.Page1.oPag.oCAFLAVVI_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODVAD_1_18.value==this.w_CACODVAD)
      this.oPgFrm.Page1.oPag.oCACODVAD_1_18.value=this.w_CACODVAD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_19.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_19.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCAIMPMIN_1_20.value==this.w_CAIMPMIN)
      this.oPgFrm.Page1.oPag.oCAIMPMIN_1_20.value=this.w_CAIMPMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCASTRUTT_1_21.RadioValue()==this.w_CASTRUTT)
      this.oPgFrm.Page1.oPag.oCASTRUTT_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPMAN_1_22.RadioValue()==this.w_CATIPMAN)
      this.oPgFrm.Page1.oPag.oCATIPMAN_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAIMPCOM_1_26.value==this.w_CAIMPCOM)
      this.oPgFrm.Page1.oPag.oCAIMPCOM_1_26.value=this.w_CAIMPCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODCAU_1_32.value==this.w_CACODCAU)
      this.oPgFrm.Page1.oPag.oCACODCAU_1_32.value=this.w_CACODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_33.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_33.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oVALSIM_1_40.value==this.w_VALSIM)
      this.oPgFrm.Page1.oPag.oVALSIM_1_40.value=this.w_VALSIM
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLBANC_1_46.RadioValue()==this.w_CAFLBANC)
      this.oPgFrm.Page1.oPag.oCAFLBANC_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUDIS_2_2.value==this.w_CAUDIS)
      this.oPgFrm.Page2.oPag.oCAUDIS_2_2.value=this.w_CAUDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCACONSBF_1_47.RadioValue()==this.w_CACONSBF)
      this.oPgFrm.Page1.oPag.oCACONSBF_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLACEF_1_55.RadioValue()==this.w_CAFLACEF)
      this.oPgFrm.Page1.oPag.oCAFLACEF_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLCASP_1_56.RadioValue()==this.w_CAFLCASP)
      this.oPgFrm.Page1.oPag.oCAFLCASP_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADATEFF_1_57.RadioValue()==this.w_CADATEFF)
      this.oPgFrm.Page1.oPag.oCADATEFF_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCA_NOCVS_1_59.RadioValue()==this.w_CA_NOCVS)
      this.oPgFrm.Page1.oPag.oCA_NOCVS_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLDSPR_1_60.RadioValue()==this.w_CAFLDSPR)
      this.oPgFrm.Page1.oPag.oCAFLDSPR_1_60.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'CAU_DIST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CACODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CACODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CADESCAU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCADESCAU_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CADESCAU)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CATIPSCD)) or not(not(.w_CATIPSCD='F' and .w_CATIPDIS='CE')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATIPSCD_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CATIPSCD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Se le scadenze sono passive il tipo distinta non pu� essere cessione crediti")
          case   ((empty(.w_CATIPDIS)) or not(not(.w_CATIPSCD='F' and .w_CATIPDIS='CE')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATIPDIS_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CATIPDIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Se le scadenze sono passive il tipo distinta non pu� essere cessione crediti")
          case   ((empty(.w_CASTRUTT)) or not(cifracnf(Alltrim(.w_STCRISTR),'D')=Alltrim(.w_CASTRUTT)+juststem(.w_STFILXSD)))  and not(! .w_CATIPDIS $ 'SC-SD-SE')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCASTRUTT_1_21.SetFocus()
            i_bnoObbl = !empty(.w_CASTRUTT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Struttura non ufficiale")
          case   not(g_COGE='S' AND (EMPTY(.w_CACODCAU) OR .w_TIPREG $ 'NE'))  and not(g_COGE<>'S')  and not(empty(.w_CACODCAU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODCAU_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente o incongruente o obsoleta")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSTE_MBC.CheckForm()
      if i_bres
        i_bres=  .GSTE_MBC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gste_acd
      * --- Controllo Tipo ntabilizzazione
      IF i_bRes AND Not ((Not Empty(.w_CACODCAU) and .w_CAFLCONT $ 'S-I') OR (Empty(.w_CACODCAU) and .w_CAFLCONT='N'))
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg = ah_MsgFormat("Tipo contabilizzazione incongruente con causale contabile impostata")
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CATIPSCD = this.w_CATIPSCD
    this.o_CATIPDIS = this.w_CATIPDIS
    this.o_DECTOT = this.w_DECTOT
    this.o_CACODCAU = this.w_CACODCAU
    this.o_CAFLDSPR = this.w_CAFLDSPR
    * --- GSTE_MBC : Depends On
    this.GSTE_MBC.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgste_acdPag1 as StdContainer
  Width  = 566
  height = 388
  stdWidth  = 566
  stdheight = 388
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACODICE_1_1 as StdField with uid="LLYWCHKZBC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CACODICE", cQueryName = "CACODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice causale distinta",;
    HelpContextID = 118047083,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=91, Top=13, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5)

  add object oCADESCAU_1_2 as StdField with uid="HOITROFYOR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CADESCAU", cQueryName = "CADESCAU",;
    bObbl = .t. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale",;
    HelpContextID = 32461179,;
   bGlobalFont=.t.,;
    Height=21, Width=393, Left=158, Top=13, InputMask=replicate('X',35)


  add object oCATIPSCD_1_3 as StdCombo with uid="UPWQYIHSDP",rtseq=3,rtrep=.f.,left=91,top=42,width=126,height=21;
    , ToolTipText = "Tipo scadenze: (attive o passive)";
    , HelpContextID = 29643114;
    , cFormVar="w_CATIPSCD",RowSource=""+"Attive,"+"Passive", bObbl = .t. , nPag = 1;
    , sErrorMsg = "Se le scadenze sono passive il tipo distinta non pu� essere cessione crediti";
  , bGlobalFont=.t.


  func oCATIPSCD_1_3.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oCATIPSCD_1_3.GetRadio()
    this.Parent.oContained.w_CATIPSCD = this.RadioValue()
    return .t.
  endfunc

  func oCATIPSCD_1_3.SetRadio()
    this.Parent.oContained.w_CATIPSCD=trim(this.Parent.oContained.w_CATIPSCD)
    this.value = ;
      iif(this.Parent.oContained.w_CATIPSCD=='C',1,;
      iif(this.Parent.oContained.w_CATIPSCD=='F',2,;
      0))
  endfunc

  func oCATIPSCD_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not(.w_CATIPSCD='F' and .w_CATIPDIS='CE'))
    endwith
    return bRes
  endfunc


  add object oCATIPDIS_1_4 as StdCombo with uid="HWYVLHZVFO",rtseq=4,rtrep=.f.,left=410,top=42,width=141,height=21;
    , ToolTipText = "Tipo distinta";
    , HelpContextID = 222015111;
    , cFormVar="w_CATIPDIS",RowSource=""+"Ric.bancaria/Ri.Ba.,"+"R.I.D.,"+"M.AV.,"+"Cambiale/tratta,"+"Bonifico nazionale,"+"Bonifico estero,"+"Rimessa diretta,"+"Cessione credito,"+"SDD - Sepa Direct Debit,"+"SCT - Sepa Credit Transfer,"+"SCT estero - Sepa Credit Transfer", bObbl = .t. , nPag = 1;
    , sErrorMsg = "Se le scadenze sono passive il tipo distinta non pu� essere cessione crediti";
  , bGlobalFont=.t.


  func oCATIPDIS_1_4.RadioValue()
    return(iif(this.value =1,'RB',;
    iif(this.value =2,'RI',;
    iif(this.value =3,'MA',;
    iif(this.value =4,'CA',;
    iif(this.value =5,'BO',;
    iif(this.value =6,'BE',;
    iif(this.value =7,'RD',;
    iif(this.value =8,'CE',;
    iif(this.value =9,'SD',;
    iif(this.value =10,'SC',;
    iif(this.value =11,'SE',;
    space(2)))))))))))))
  endfunc
  func oCATIPDIS_1_4.GetRadio()
    this.Parent.oContained.w_CATIPDIS = this.RadioValue()
    return .t.
  endfunc

  func oCATIPDIS_1_4.SetRadio()
    this.Parent.oContained.w_CATIPDIS=trim(this.Parent.oContained.w_CATIPDIS)
    this.value = ;
      iif(this.Parent.oContained.w_CATIPDIS=='RB',1,;
      iif(this.Parent.oContained.w_CATIPDIS=='RI',2,;
      iif(this.Parent.oContained.w_CATIPDIS=='MA',3,;
      iif(this.Parent.oContained.w_CATIPDIS=='CA',4,;
      iif(this.Parent.oContained.w_CATIPDIS=='BO',5,;
      iif(this.Parent.oContained.w_CATIPDIS=='BE',6,;
      iif(this.Parent.oContained.w_CATIPDIS=='RD',7,;
      iif(this.Parent.oContained.w_CATIPDIS=='CE',8,;
      iif(this.Parent.oContained.w_CATIPDIS=='SD',9,;
      iif(this.Parent.oContained.w_CATIPDIS=='SC',10,;
      iif(this.Parent.oContained.w_CATIPDIS=='SE',11,;
      0)))))))))))
  endfunc

  func oCATIPDIS_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not(.w_CATIPSCD='F' and .w_CATIPDIS='CE'))
      if .not. empty(.w_CACAUBON)
        bRes2=.link_1_15('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oCAFLCONT_1_5 as StdCombo with uid="ZDUDDLNMQU",rtseq=5,rtrep=.f.,left=91,top=71,width=126,height=21;
    , ToolTipText = "Tipo di contabilizzazione";
    , HelpContextID = 50957958;
    , cFormVar="w_CAFLCONT",RowSource=""+"Singolo effetto,"+"Intera distinta,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAFLCONT_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'I',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oCAFLCONT_1_5.GetRadio()
    this.Parent.oContained.w_CAFLCONT = this.RadioValue()
    return .t.
  endfunc

  func oCAFLCONT_1_5.SetRadio()
    this.Parent.oContained.w_CAFLCONT=trim(this.Parent.oContained.w_CAFLCONT)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLCONT=='S',1,;
      iif(this.Parent.oContained.w_CAFLCONT=='I',2,;
      iif(this.Parent.oContained.w_CAFLCONT=='N',3,;
      0)))
  endfunc

  add object oCADIS_RB_1_6 as StdCheck with uid="JMYARFOGZS",rtseq=6,rtrep=.f.,left=6, top=155, caption="R.B./RiBa",;
    ToolTipText = "Se attivo: seleziona le ricevute bancarie",;
    HelpContextID = 234049896,;
    cFormVar="w_CADIS_RB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCADIS_RB_1_6.RadioValue()
    return(iif(this.value =1,'RB',;
    'XX'))
  endfunc
  func oCADIS_RB_1_6.GetRadio()
    this.Parent.oContained.w_CADIS_RB = this.RadioValue()
    return .t.
  endfunc

  func oCADIS_RB_1_6.SetRadio()
    this.Parent.oContained.w_CADIS_RB=trim(this.Parent.oContained.w_CADIS_RB)
    this.value = ;
      iif(this.Parent.oContained.w_CADIS_RB=='RB',1,;
      0)
  endfunc

  add object oCADIS_CA_1_8 as StdCheck with uid="BSDERCHFZO",rtseq=8,rtrep=.f.,left=115, top=155, caption="Cambiali",;
    ToolTipText = "Se attivo: seleziona le cambiali/tratte",;
    HelpContextID = 234049895,;
    cFormVar="w_CADIS_CA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCADIS_CA_1_8.RadioValue()
    return(iif(this.value =1,'CA',;
    'XX'))
  endfunc
  func oCADIS_CA_1_8.GetRadio()
    this.Parent.oContained.w_CADIS_CA = this.RadioValue()
    return .t.
  endfunc

  func oCADIS_CA_1_8.SetRadio()
    this.Parent.oContained.w_CADIS_CA=trim(this.Parent.oContained.w_CADIS_CA)
    this.value = ;
      iif(this.Parent.oContained.w_CADIS_CA=='CA',1,;
      0)
  endfunc

  add object oCADIS_RI_1_9 as StdCheck with uid="NKCCVKXNGF",rtseq=9,rtrep=.f.,left=224, top=155, caption="R.I.D.",;
    ToolTipText = "Se attivo: seleziona le R.I.D.",;
    HelpContextID = 234049903,;
    cFormVar="w_CADIS_RI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCADIS_RI_1_9.RadioValue()
    return(iif(this.value =1,'RI',;
    'XX'))
  endfunc
  func oCADIS_RI_1_9.GetRadio()
    this.Parent.oContained.w_CADIS_RI = this.RadioValue()
    return .t.
  endfunc

  func oCADIS_RI_1_9.SetRadio()
    this.Parent.oContained.w_CADIS_RI=trim(this.Parent.oContained.w_CADIS_RI)
    this.value = ;
      iif(this.Parent.oContained.w_CADIS_RI=='RI',1,;
      0)
  endfunc

  add object oCADIS_RD_1_10 as StdCheck with uid="DOEONKYTMP",rtseq=10,rtrep=.f.,left=6, top=181, caption="Rim.dir.",;
    ToolTipText = "Se attivo: seleziona le rimesse dirette",;
    HelpContextID = 234049898,;
    cFormVar="w_CADIS_RD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCADIS_RD_1_10.RadioValue()
    return(iif(this.value =1,'RD',;
    'XX'))
  endfunc
  func oCADIS_RD_1_10.GetRadio()
    this.Parent.oContained.w_CADIS_RD = this.RadioValue()
    return .t.
  endfunc

  func oCADIS_RD_1_10.SetRadio()
    this.Parent.oContained.w_CADIS_RD=trim(this.Parent.oContained.w_CADIS_RD)
    this.value = ;
      iif(this.Parent.oContained.w_CADIS_RD=='RD',1,;
      0)
  endfunc

  add object oCADIS_MA_1_11 as StdCheck with uid="ROFRAPQYJJ",rtseq=11,rtrep=.f.,left=115, top=181, caption="M.AV.",;
    ToolTipText = "Se attivo: seleziona le M.AV.",;
    HelpContextID = 34385561,;
    cFormVar="w_CADIS_MA", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Se il tipo distinta � cessione credito non si pu� selezionare il tipo scadenza M";
   , bGlobalFont=.t.


  func oCADIS_MA_1_11.RadioValue()
    return(iif(this.value =1,'MA',;
    'XX'))
  endfunc
  func oCADIS_MA_1_11.GetRadio()
    this.Parent.oContained.w_CADIS_MA = this.RadioValue()
    return .t.
  endfunc

  func oCADIS_MA_1_11.SetRadio()
    this.Parent.oContained.w_CADIS_MA=trim(this.Parent.oContained.w_CADIS_MA)
    this.value = ;
      iif(this.Parent.oContained.w_CADIS_MA=='MA',1,;
      0)
  endfunc

  add object oCADIS_BO_1_12 as StdCheck with uid="HSEPUNIEQP",rtseq=12,rtrep=.f.,left=224, top=181, caption="Bonifico",;
    ToolTipText = "Se attivo: seleziona i bonifici",;
    HelpContextID = 234049909,;
    cFormVar="w_CADIS_BO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCADIS_BO_1_12.RadioValue()
    return(iif(this.value =1,'BO',;
    'XX'))
  endfunc
  func oCADIS_BO_1_12.GetRadio()
    this.Parent.oContained.w_CADIS_BO = this.RadioValue()
    return .t.
  endfunc

  func oCADIS_BO_1_12.SetRadio()
    this.Parent.oContained.w_CADIS_BO=trim(this.Parent.oContained.w_CADIS_BO)
    this.value = ;
      iif(this.Parent.oContained.w_CADIS_BO=='BO',1,;
      0)
  endfunc

  add object oCAFLIBAN_1_14 as StdCheck with uid="QGHQKYDLWU",rtseq=14,rtrep=.f.,left=224, top=72, caption="Controllo presenza IBAN/BBAN",;
    ToolTipText = "Se attivo: la procedura controlla in maniera bloccante i codici IBAN e BBAN in compilazione/manutenzione distinte",;
    HelpContextID = 5665140,;
    cFormVar="w_CAFLIBAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLIBAN_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCAFLIBAN_1_14.GetRadio()
    this.Parent.oContained.w_CAFLIBAN = this.RadioValue()
    return .t.
  endfunc

  func oCAFLIBAN_1_14.SetRadio()
    this.Parent.oContained.w_CAFLIBAN=trim(this.Parent.oContained.w_CAFLIBAN)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLIBAN=='S',1,;
      0)
  endfunc

  func oCAFLIBAN_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_CATIPSCD='F' AND .w_CATIPDIS $ 'BO-BE') OR (.w_CATIPSCD='C' AND .w_CATIPDIS $ 'RI-MA'))
    endwith
   endif
  endfunc

  func oCAFLIBAN_1_14.mHide()
    with this.Parent.oContained
      return (NOT((.w_CATIPSCD='F' AND .w_CATIPDIS $ 'BO-BE') OR (.w_CATIPSCD='C' AND .w_CATIPDIS $ 'RI-MA')))
    endwith
  endfunc


  add object oCACAUBON_1_15 as StdZTamTableCombo with uid="GGKCWJRNCX",rtseq=15,rtrep=.f.,left=91,top=101,width=126,height=21;
    , ToolTipText = "Causale di pagamento";
    , HelpContextID = 250920588;
    , cFormVar="w_CACAUBON",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="TIP_DIST";
    , cTable='QUERY\GSTECACD.VQR',cKey='DICAUDIS',cValue='DIDESCRI',cOrderBy='DIDESCRI',xDefault=space(5);
  , bGlobalFont=.t.


  func oCACAUBON_1_15.mHide()
    with this.Parent.oContained
      return (.w_TEST=.F.)
    endwith
  endfunc

  func oCACAUBON_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACAUBON_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oCATIPCON_1_16 as StdCombo with uid="ZSJOTPPWHP",value=3,rtseq=16,rtrep=.f.,left=91,top=211,width=128,height=21;
    , ToolTipText = "Filtra sulle scadenze clienti/fornitori o entrambe";
    , HelpContextID = 238792332;
    , cFormVar="w_CATIPCON",RowSource=""+"Clienti,"+"Fornitori,"+"Clienti/fornitori", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATIPCON_1_16.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oCATIPCON_1_16.GetRadio()
    this.Parent.oContained.w_CATIPCON = this.RadioValue()
    return .t.
  endfunc

  func oCATIPCON_1_16.SetRadio()
    this.Parent.oContained.w_CATIPCON=trim(this.Parent.oContained.w_CATIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_CATIPCON=='C',1,;
      iif(this.Parent.oContained.w_CATIPCON=='F',2,;
      iif(this.Parent.oContained.w_CATIPCON=='',3,;
      0)))
  endfunc

  add object oCAFLAVVI_1_17 as StdCheck with uid="KBRLVMSGCX",rtseq=17,rtrep=.f.,left=345, top=211, caption="Gestione avvisi",;
    ToolTipText = "Se attivo: abilita l'emissione degli avvisi sulla distinta",;
    HelpContextID = 64385391,;
    cFormVar="w_CAFLAVVI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLAVVI_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCAFLAVVI_1_17.GetRadio()
    this.Parent.oContained.w_CAFLAVVI = this.RadioValue()
    return .t.
  endfunc

  func oCAFLAVVI_1_17.SetRadio()
    this.Parent.oContained.w_CAFLAVVI=trim(this.Parent.oContained.w_CAFLAVVI)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLAVVI=='S',1,;
      0)
  endfunc

  func oCAFLAVVI_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE='S')
    endwith
   endif
  endfunc

  func oCAFLAVVI_1_17.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oCACODVAD_1_18 as StdField with uid="AQOEJYZDHS",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CACODVAD", cQueryName = "CACODVAD",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta di selezione",;
    HelpContextID = 67715434,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=91, Top=237, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CACODVAD"

  func oCACODVAD_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODVAD_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODVAD_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCACODVAD_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oCACODVAD_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CACODVAD
     i_obj.ecpSave()
  endproc

  add object oDESVAL_1_19 as StdField with uid="TTOSWQIDBR",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 102677194,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=153, Top=237, InputMask=replicate('X',35)

  add object oCAIMPMIN_1_20 as StdField with uid="TGSKJUJQYN",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CAIMPMIN", cQueryName = "CAIMPMIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo minimo di selezione effetti da compilazione distinte",;
    HelpContextID = 70803084,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=91, Top=265, cSayPict="v_PV[40+VVL]", cGetPict="v_GV[40+VVL]"


  add object oCASTRUTT_1_21 as StdTableCombo with uid="ACJVANRPUC",rtseq=21,rtrep=.f.,left=410,top=265,width=141,height=22;
    , ToolTipText = "Codice struttura SEPA";
    , HelpContextID = 66011514;
    , cFormVar="w_CASTRUTT",tablefilter="", bObbl = .t. , nPag = 1;
    , sErrorMsg = "Struttura non ufficiale";
    , cLinkFile="VASTRUTT";
    , cTable='query\GSTE_KVS.vqr',cKey='STCODICE',cValue='STDESCRI',cOrderBy='',xDefault=space(10);
  , bGlobalFont=.t.


  func oCASTRUTT_1_21.mHide()
    with this.Parent.oContained
      return (! .w_CATIPDIS $ 'SC-SD-SE')
    endwith
  endfunc

  func oCASTRUTT_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCASTRUTT_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oCATIPMAN_1_22 as StdCombo with uid="OZAQRRZGGB",rtseq=22,rtrep=.f.,left=410,top=296,width=140,height=21;
    , ToolTipText = "Tipo SDD a cui devono appartenere le partite da inserire in distinta";
    , HelpContextID = 197415284;
    , cFormVar="w_CATIPMAN",RowSource=""+"CORE,"+"B2B,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATIPMAN_1_22.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'B',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oCATIPMAN_1_22.GetRadio()
    this.Parent.oContained.w_CATIPMAN = this.RadioValue()
    return .t.
  endfunc

  func oCATIPMAN_1_22.SetRadio()
    this.Parent.oContained.w_CATIPMAN=trim(this.Parent.oContained.w_CATIPMAN)
    this.value = ;
      iif(this.Parent.oContained.w_CATIPMAN=='C',1,;
      iif(this.Parent.oContained.w_CATIPMAN=='B',2,;
      iif(this.Parent.oContained.w_CATIPMAN=='T',3,;
      0)))
  endfunc

  func oCATIPMAN_1_22.mHide()
    with this.Parent.oContained
      return (.w_CADIS_RI<>'RI' )
    endwith
  endfunc

  add object oCAIMPCOM_1_26 as StdField with uid="ITLOPZPGOG",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CAIMPCOM", cQueryName = "CAIMPCOM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo commissioni fisse proposto nella valuta di conto",;
    HelpContextID = 238575245,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=183, Top=364, cSayPict="v_PV[40+VVL]", cGetPict="v_GV[40+VVL]"

  func oCAIMPCOM_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CACODCAU) AND g_COGE='S')
    endwith
   endif
  endfunc

  func oCAIMPCOM_1_26.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oCACODCAU_1_32 as StdField with uid="QHYUJDCSWD",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CACODCAU", cQueryName = "CACODCAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente o incongruente o obsoleta",;
    ToolTipText = "Causale per la contabilizzazione in primanota",;
    HelpContextID = 17383803,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=119, Top=337, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CACODCAU"

  func oCACODCAU_1_32.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  func oCACODCAU_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODCAU_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODCAU_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCACODCAU_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSTE_ACD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCACODCAU_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CACODCAU
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_33 as StdField with uid="MELJMWVTBD",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 47072566,;
   bGlobalFont=.t.,;
    Height=21, Width=368, Left=183, Top=337, InputMask=replicate('X',35)

  func oDESCAU_1_33.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc


  add object oObj_1_39 as cp_runprogram with uid="ERYOGBSRKL",left=-5, top=407, width=231,height=22,;
    caption='GSTE_BCK(1)',;
   bGlobalFont=.t.,;
    prg="GSTE_BCK(1)",;
    cEvent = "w_CATIPSCD Changed",;
    nPag=1;
    , HelpContextID = 28518193

  add object oVALSIM_1_40 as StdField with uid="BLJGMEUJFQ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_VALSIM", cQueryName = "VALSIM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 77737386,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=119, Top=364, InputMask=replicate('X',5)

  func oVALSIM_1_40.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc


  add object oObj_1_44 as cp_runprogram with uid="YVTIPCJEWE",left=232, top=407, width=306,height=22,;
    caption='GSTE_BCK(2)',;
   bGlobalFont=.t.,;
    prg="GSTE_BCK(2)",;
    cEvent = "w_CATIPDIS Changed,Load,New record",;
    nPag=1;
    , HelpContextID = 28518449


  add object oCAFLBANC_1_46 as StdCombo with uid="CLRWQHYCXG",rtseq=33,rtrep=.f.,left=410,top=157,width=141,height=21, enabled=.f.;
    , ToolTipText = "Codice banca richiesta obbligatoriamente nelle scadenze della distinta";
    , HelpContextID = 18452119;
    , cFormVar="w_CAFLBANC",RowSource=""+"B.appoggio,"+"Ns.banca,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAFLBANC_1_46.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oCAFLBANC_1_46.GetRadio()
    this.Parent.oContained.w_CAFLBANC = this.RadioValue()
    return .t.
  endfunc

  func oCAFLBANC_1_46.SetRadio()
    this.Parent.oContained.w_CAFLBANC=trim(this.Parent.oContained.w_CAFLBANC)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLBANC=='A',1,;
      iif(this.Parent.oContained.w_CAFLBANC=='S',2,;
      iif(this.Parent.oContained.w_CAFLBANC=='N',3,;
      0)))
  endfunc


  add object oCACONSBF_1_47 as StdCombo with uid="YFOPZMEJKT",rtseq=35,rtrep=.f.,left=410,top=183,width=141,height=21;
    , ToolTipText = "Tipologia C/C (conto corrente o salvo buon fine)";
    , HelpContextID = 27869548;
    , cFormVar="w_CACONSBF",RowSource=""+"Conto corrente,"+"Salvo buon fine,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCACONSBF_1_47.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oCACONSBF_1_47.GetRadio()
    this.Parent.oContained.w_CACONSBF = this.RadioValue()
    return .t.
  endfunc

  func oCACONSBF_1_47.SetRadio()
    this.Parent.oContained.w_CACONSBF=trim(this.Parent.oContained.w_CACONSBF)
    this.value = ;
      iif(this.Parent.oContained.w_CACONSBF=='C',1,;
      iif(this.Parent.oContained.w_CACONSBF=='S',2,;
      iif(this.Parent.oContained.w_CACONSBF=='E',3,;
      0)))
  endfunc


  add object oObj_1_49 as cp_runprogram with uid="GFXYEODKJZ",left=-5, top=432, width=231,height=22,;
    caption='GSTE_BCK(3)',;
   bGlobalFont=.t.,;
    prg="GSTE_BCK(3)",;
    cEvent = "w_CACONSBF Changed",;
    nPag=1;
    , HelpContextID = 28518705


  add object oObj_1_51 as cp_runprogram with uid="BGLYCVZYQE",left=232, top=432, width=231,height=22,;
    caption='GSTE_BCK(4)',;
   bGlobalFont=.t.,;
    prg="GSTE_BCK(4)",;
    cEvent = "w_CAFLCONT Changed",;
    nPag=1;
    , HelpContextID = 28518961

  add object oCAFLACEF_1_55 as StdCheck with uid="AMEYPODAWB",rtseq=36,rtrep=.f.,left=224, top=101, caption="Accettazione effetto",;
    ToolTipText = "Flag accettazione effetto",;
    HelpContextID = 14053740,;
    cFormVar="w_CAFLACEF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLACEF_1_55.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCAFLACEF_1_55.GetRadio()
    this.Parent.oContained.w_CAFLACEF = this.RadioValue()
    return .t.
  endfunc

  func oCAFLACEF_1_55.SetRadio()
    this.Parent.oContained.w_CAFLACEF=trim(this.Parent.oContained.w_CAFLACEF)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLACEF=='S',1,;
      0)
  endfunc

  func oCAFLACEF_1_55.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP' OR empty(.w_CACAUBON))
    endwith
  endfunc

  add object oCAFLCASP_1_56 as StdCheck with uid="TTETNVQZGB",rtseq=37,rtrep=.f.,left=424, top=101, caption="Clausola spese",;
    ToolTipText = "Flag clausola spese",;
    HelpContextID = 251031926,;
    cFormVar="w_CAFLCASP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLCASP_1_56.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCAFLCASP_1_56.GetRadio()
    this.Parent.oContained.w_CAFLCASP = this.RadioValue()
    return .t.
  endfunc

  func oCAFLCASP_1_56.SetRadio()
    this.Parent.oContained.w_CAFLCASP=trim(this.Parent.oContained.w_CAFLCASP)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLCASP=='S',1,;
      0)
  endfunc

  func oCAFLCASP_1_56.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP' OR empty(.w_CACAUBON))
    endwith
  endfunc


  add object oCADATEFF_1_57 as StdCombo with uid="ARJMTFKHXW",rtseq=38,rtrep=.f.,left=410,top=131,width=141,height=21;
    , ToolTipText = "Data valuta effetto";
    , HelpContextID = 66802028;
    , cFormVar="w_CADATEFF",RowSource=""+"Data presentazione,"+"Data scadenza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCADATEFF_1_57.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oCADATEFF_1_57.GetRadio()
    this.Parent.oContained.w_CADATEFF = this.RadioValue()
    return .t.
  endfunc

  func oCADATEFF_1_57.SetRadio()
    this.Parent.oContained.w_CADATEFF=trim(this.Parent.oContained.w_CADATEFF)
    this.value = ;
      iif(this.Parent.oContained.w_CADATEFF=='P',1,;
      iif(this.Parent.oContained.w_CADATEFF=='S',2,;
      0))
  endfunc

  add object oCA_NOCVS_1_59 as StdCheck with uid="ZAFTYJMJCZ",rtseq=39,rtrep=.f.,left=424, top=72, caption="No CVS",;
    ToolTipText = "Se attivato la procedura in fase di creazione della distinte di bonifico estero non richiede dati CVS",;
    HelpContextID = 28967289,;
    cFormVar="w_CA_NOCVS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCA_NOCVS_1_59.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCA_NOCVS_1_59.GetRadio()
    this.Parent.oContained.w_CA_NOCVS = this.RadioValue()
    return .t.
  endfunc

  func oCA_NOCVS_1_59.SetRadio()
    this.Parent.oContained.w_CA_NOCVS=trim(this.Parent.oContained.w_CA_NOCVS)
    this.value = ;
      iif(this.Parent.oContained.w_CA_NOCVS=='S',1,;
      0)
  endfunc

  func oCA_NOCVS_1_59.mHide()
    with this.Parent.oContained
      return (.w_CATIPDIS<>'BE')
    endwith
  endfunc

  add object oCAFLDSPR_1_60 as StdCheck with uid="IESAAAIWJQ",rtseq=40,rtrep=.f.,left=91, top=292, caption="Gestisce descrizione parametrica",;
    ToolTipText = "Gestisce descrizione parametrica",;
    HelpContextID = 17199480,;
    cFormVar="w_CAFLDSPR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLDSPR_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCAFLDSPR_1_60.GetRadio()
    this.Parent.oContained.w_CAFLDSPR = this.RadioValue()
    return .t.
  endfunc

  func oCAFLDSPR_1_60.SetRadio()
    this.Parent.oContained.w_CAFLDSPR=trim(this.Parent.oContained.w_CAFLDSPR)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLDSPR=='S',1,;
      0)
  endfunc

  func oCAFLDSPR_1_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CATIPDIS<>'CA' AND .w_CATIPDIS<>'RD' AND .w_CATIPDIS<>'CE')
    endwith
   endif
  endfunc


  add object oBtn_1_61 as StdButton with uid="YDCPYGSYKS",left=316, top=294, width=22,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare le condizioni parametriche per il calcolo della descrizione";
    , HelpContextID = 109519658;
  , bGlobalFont=.t.

    proc oBtn_1_61.Click()
      do GSTE_KDP with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_61.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NVL(.w_CAFLDSPR,' ')<>'S')
     endwith
    endif
  endfunc

  add object oStr_1_24 as StdString with uid="CKRIOWLGOV",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=86, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="MDFQGQEIZN",Visible=.t., Left=11, Top=316,;
    Alignment=0, Width=423, Height=15,;
    Caption="Contabilizzazione"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="WMOCUXTYPP",Visible=.t., Left=3, Top=42,;
    Alignment=1, Width=86, Height=15,;
    Caption="Scadenze:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="MRVSEFZWBF",Visible=.t., Left=313, Top=42,;
    Alignment=1, Width=91, Height=15,;
    Caption="Tipo distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="MORAPXWRQF",Visible=.t., Left=41, Top=337,;
    Alignment=1, Width=76, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="YILVIEEWUH",Visible=.t., Left=4, Top=237,;
    Alignment=1, Width=84, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="CEQOIYBKYX",Visible=.t., Left=6, Top=133,;
    Alignment=0, Width=175, Height=15,;
    Caption="Tipi scadenza"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="XKCJOGNAMC",Visible=.t., Left=5, Top=364,;
    Alignment=1, Width=112, Height=18,;
    Caption="Commissioni fisse:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="DIMWNQWJRK",Visible=.t., Left=4, Top=211,;
    Alignment=1, Width=84, Height=15,;
    Caption="Filtro su:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="LROYMRHJVE",Visible=.t., Left=298, Top=157,;
    Alignment=1, Width=106, Height=15,;
    Caption="Banca richiesta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="YYKQQYNNYZ",Visible=.t., Left=331, Top=183,;
    Alignment=1, Width=73, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="HRAPRSJMCF",Visible=.t., Left=13, Top=71,;
    Alignment=1, Width=76, Height=18,;
    Caption="Tipo cont.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="FRHQUHRIDK",Visible=.t., Left=12, Top=101,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (.w_TEST=.F.)
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="GVSSAXDVJW",Visible=.t., Left=-3, Top=458,;
    Alignment=0, Width=393, Height=18,;
    Caption="Cacaubon ha una sua classe come control stdztamtablecombo"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="SJTYNKNDAF",Visible=.t., Left=2, Top=264,;
    Alignment=1, Width=86, Height=18,;
    Caption="Importo min.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="GVCHLDQWCX",Visible=.t., Left=258, Top=131,;
    Alignment=1, Width=146, Height=18,;
    Caption="Data valuta effetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="FOKICOTXQG",Visible=.t., Left=289, Top=264,;
    Alignment=1, Width=115, Height=18,;
    Caption="Struttura SEPA:"  ;
  , bGlobalFont=.t.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (! .w_CATIPDIS $ 'SC-SD-SE')
    endwith
  endfunc

  add object oStr_1_70 as StdString with uid="ZXHCYOQMRO",Visible=.t., Left=343, Top=299,;
    Alignment=1, Width=61, Height=18,;
    Caption="Tipo SDD:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (.w_CADIS_RI <>'RI')
    endwith
  endfunc

  add object oBox_1_38 as StdBox with uid="IKRSRGBRZH",left=8, top=332, width=548,height=1
enddefine
define class tgste_acdPag2 as StdContainer
  Width  = 566
  height = 388
  stdWidth  = 566
  stdheight = 388
  resizeYpos=194
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="OCLSCOBMYV",left=6, top=39, width=450, height=208, bOnScreen=.t.;


  add object oCAUDIS_2_2 as StdField with uid="ILUXDCAMLF",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CAUDIS", cQueryName = "CAUDIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 21979430,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=113, Top=12, InputMask=replicate('X',5)

  add object oStr_2_3 as StdString with uid="JQULHGKUUX",Visible=.t., Left=4, Top=12,;
    Alignment=1, Width=107, Height=18,;
    Caption="Causale distinta:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_acd','CAU_DIST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CACODICE=CAU_DIST.CACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gste_acd
* --- Classe per gestire la combo CACAUBON
* --- derivata dalla classe combo da tabella

define class StdZTamTableCombo as StdTableCombo

proc Init()
  IF VARTYPE(this.bNoBackColor)='U'
		This.backcolor=i_EBackColor
	ENDIF

endproc

  proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear
      i_bCharKey=type(i_fk)='C'
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()]=trim(&i_fk)
        else
          this.combovalues[recno()]=&i_fk
        endif
        skip
      enddo
      use
    endif

enddefine


* --- Fine Area Manuale
