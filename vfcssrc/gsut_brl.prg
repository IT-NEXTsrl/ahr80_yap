* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_brl                                                        *
*              Report list                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_47]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-13                                                      *
* Last revis.: 2013-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper,w_oMultiReport,w_EXCLWRD
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_brl",oParentObject,m.pOper,m.w_oMultiReport,m.w_EXCLWRD)
return(i_retval)

define class tgsut_brl as StdBatch
  * --- Local variables
  pOper = space(1)
  w_oMultiReport = .NULL.
  w_EXCLWRD = space(1)
  w_NC = space(254)
  w_PS = .NULL.
  w_PRINTER = space(254)
  w_REFRESH = .f.
  w_MOD_SEL = space(1)
  w_RETINDEX = 0
  w_POSIZIONE = 0
  w_EXCEL = .f.
  w_CMD = space(100)
  w_SplitReport = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Report List
    * --- pOper :
    *     'M'/'C' -> Lancio maschera Zoom con selezione/Zoom semplice
    *     'S' -> Salva
    *     'P' -> Cambia stampante
    *     'Z' -> Calcola zoom
    *     'D' -> Selezione/Deselezione
    * --- w_EXCLWRD :
    *     'E' -> Excel
    *     'W' -> Word
    *     '' -> Altri casi
    this.w_REFRESH = .F.
    this.w_EXCLWRD = IIF(TYPE("w_EXCLWRD")<>"C", " ", this.w_EXCLWRD)
    do case
      case this.pOper="M" OR this.pOper="C"
        * --- Apro la maschera da batch affinch� possa passare come caller 
        *     l'oggetto MultiReport e w_MOD_SEL
        this.w_MOD_SEL = this.pOper
        this.w_EXCEL = IIF(TYPE("w_EXCLWRD")="C", this.w_EXCLWRD="E",.F.)
        do GSUT_KRL with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.bUpdateParentObject=.f.
        i_retcode = 'stop'
        i_retval = this.w_RETINDEX
        return
      case this.pOper="D"
        UPDATE ( this.oParentObject.w_Zoom_Printer.cCursor ) SET XCHK = IIF(this.oParentObject.w_SELEZ="S", 1, 0)
        this.w_oMultiReport.ClearCheck()     
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="X"
        * --- Se la maschera chiamante ha il campo w_SELECTED ci memorizza il numero di record attualmente selezionati
        this.w_oMultiReport.CheckReport(this.oParentObject.w_INDICE)     
        if vartype(this.w_oMultiReport)="O" AND vartype (this.oparentobject.w_SELECTED)="N"
          this.oparentobject.w_SELECTED = this.oparentobject.w_SELECTED + 1
        endif
      case this.pOper="U"
        * --- Se la maschera chiamante ha il campo w_SELECTED ci memorizza il numero di record attualmente selezionati
        this.w_oMultiReport.UncheckReport(this.oParentObject.w_INDICE)     
        if vartype(this.w_oMultiReport)="O" AND vartype (this.oparentobject.w_SELECTED)="N"
          this.oparentobject.w_SELECTED = this.oparentobject.w_SELECTED - 1
        endif
      case this.pOper="R"
        this.w_NC = this.oParentObject.w_Zoom_Printer.cCursor
        this.w_oMultiReport.ClearCheck()     
        if vartype (this.oparentobject.w_SELECTED)="N"
          this.oparentobject.w_SELECTED = 0
        endif
        local l_oldArea, l_count 
 l_oldArea = SELECT() 
 SELECT (this.w_NC) 
 GO TOP 
 SCAN FOR XCHK=1
        this.w_oMultiReport.CheckReport(INDICE)     
        if vartype (this.oparentobject.w_SELECTED)="N"
          this.oparentobject.w_SELECTED = this.oparentobject.w_SELECTED + 1
        endif
        SELECT (this.w_NC) 
 ENDSCAN 
 SELECT (l_oldArea)
      case this.pOper="Z"
        if this.oParentObject.w_MOD_SEL="M"
          this.w_NC = this.oParentObject.w_Zoom_Printer.cCursor
        else
          this.w_NC = this.oParentObject.w_Zoom_Select.cCursor
        endif
        * --- Popolo lo zoom e seleziono i report (.T.)
        if i_bNewPrintSystem 
          if this.oParentObject.w_MOD_SEL="C"
            this.w_oMultiReport.GetInfoZoom(this.w_NC, this.oParentObject.w_MOD_SEL="M", this.oParentObject.w_EXCLWRD)     
          else
            this.w_oMultiReport.GetInfoZoom(this.w_NC, .T., "T")     
          endif
        else
          this.w_oMultiReport.GetInfoZoom(this.w_NC, this.oParentObject.w_MOD_SEL="M", this.oParentObject.w_EXCLWRD)     
        endif
        * --- Elimino i report che non hanno il modello
        if this.oParentObject.w_EXCLWRD$"WG"
          DELETE FROM (this.w_NC) WHERE EXCLWRD<>this.oParentObject.w_EXCLWRD
        endif
      case this.pOper="P"
        this.w_PRINTER = ""
        this.w_NC = this.oParentObject.w_Zoom_Printer.cCursor
        * --- conto il numero di report selezionati
        local l_oldArea, l_count 
 l_oldArea = SELECT() 
 SELECT (this.w_NC) 
 COUNT FOR XCHK=1 TO l_count 
 GO TOP 
 SELECT (l_oldArea)
        if l_count=0
          ah_errormsg("Nessun report selezionato","!")
        else
          this.w_PRINTER = GetPrinter()
        endif
        if NOT EMPTY(this.w_PRINTER)
          UPDATE (this.w_NC) SET PRINTER=UPPER(this.w_PRINTER) WHERE XCHK=1 AND !this.w_oMultiReport.GetText(INDICE)
          ah_errormsg("Aggiornati %1 report","i","",l_count)
        endif
      case this.pOper="S"
        * --- Controllo se ci sono dei report selezionati prima di salvare le impostazioni
        this.w_NC = this.oParentObject.w_Zoom_Printer.cCursor
        local l_oldArea, l_count 
 l_oldArea = SELECT() 
 SELECT (this.w_NC) 
 COUNT FOR XCHK=1 TO l_count 
 GO TOP 
 SELECT (l_oldArea)
        if l_count>0
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          if i_bNewPrintSystem
            this.w_CMD = "i_curform.oPgFrm.ActivePage=2"+CHR(13)+CHR(10)+"ah_errormsg('Selezionare almeno un report','!')"
            cp_CallAfter(this.w_CMD,20)
          endif
        endif
      case this.pOper="O"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    if vartype(this.w_oMultiReport)="O" AND this.w_REFRESH
      * --- Refresh zoom
      this.oParentObject.w_Zoom_Printer.Refresh()     
      if vartype (this.oParentObject.w_Zoom_Select) ='O' AND i_bNewPrintSystem
        this.oParentObject.w_Zoom_Select.Refresh()     
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if i_bNewPrintSystem
      * --- Prende i riferimenti del cursore di attuale per ripristinarlo all uscita
       local l_Area 
 l_Area=Select() 
 Select(this.w_NC) 
 this.w_POSIZIONE=RECNO()
    endif
    if This.oParentObject.w_MOD_SEL="M"
      this.w_NC = this.oParentObject.w_Zoom_Printer.cCursor
      * --- Aggiorno oMultiReport
      this.w_oMultiReport.SetInfoZoom(this.w_NC)     
      if TYPE ("this.oParentObject.oParentObject.oParentObject")="O"
        this.w_PS = this.oParentObject.oParentObject.oParentObject
        this.w_SplitReport = this.w_PS.oSplitReport
        this.w_SplitReport.SetInfoFromParent(this.w_oMultiReport)     
      endif
    else
      * --- Ritorno l'indice del report selezionato
      This.oParentObject.oParentObject.w_RETINDEX=This.oParentObject.w_INDICE
    endif
    if i_bNewPrintSystem
      * --- Ripristina cursore di default
      Select(this.w_NC)
      if this.w_POSIZIONE<=RECCOUNT()
        GO this.w_POSIZIONE
      endif
      Select(l_Area)
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se la maschera chiamante ha il campo w_SELECTED ci memorizza il numero di record attualmente selezionati
    if vartype(this.w_oMultiReport)="O" AND vartype (this.oparentobject.w_SELECTED)="N"
      this.w_NC = this.oParentObject.w_Zoom_Printer.cCursor
       local l_oldArea, l_numXCHK 
 l_oldArea=Select() 
 Select(this.w_NC) 
 this.w_POSIZIONE=RECNO() 
 Count for XCHK=1 to this.oparentobject.w_SELECTED
      if this.w_POSIZIONE<=RECCOUNT()
        GO this.w_POSIZIONE
      endif
       Select(l_oldArea)
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper,w_oMultiReport,w_EXCLWRD)
    this.pOper=pOper
    this.w_oMultiReport=w_oMultiReport
    this.w_EXCLWRD=w_EXCLWRD
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper,w_oMultiReport,w_EXCLWRD"
endproc
