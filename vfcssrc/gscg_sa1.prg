* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sa1                                                        *
*              Aggiornamento acconto IVA                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_27]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-21                                                      *
* Last revis.: 2008-07-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sa1",oParentObject))

* --- Class definition
define class tgscg_sa1 as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 506
  Height = 148
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-07"
  HelpContextID=144038551
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  COC_MAST_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gscg_sa1"
  cComment = "Aggiornamento acconto IVA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ATTIVER = space(5)
  w_ANNOVER = space(4)
  w_DATAVERS = ctod('  /  /  ')
  w_VERIMIN = 0
  o_VERIMIN = 0
  w_DECTOT2 = 0
  o_DECTOT2 = 0
  w_CALCPICT = 0
  w_IMPOIVA = 0
  w_IMPOVAL = space(3)
  w_CCVERSA = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_DESCRI = space(35)
  w_CONFERMA = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sa1Pag1","gscg_sa1",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATAVERS_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_sa1
    This.bUpdated=.T.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ATTIVER=space(5)
      .w_ANNOVER=space(4)
      .w_DATAVERS=ctod("  /  /  ")
      .w_VERIMIN=0
      .w_DECTOT2=0
      .w_CALCPICT=0
      .w_IMPOIVA=0
      .w_IMPOVAL=space(3)
      .w_CCVERSA=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESCRI=space(35)
      .w_CONFERMA=.f.
      .w_ATTIVER=oParentObject.w_ATTIVER
      .w_ANNOVER=oParentObject.w_ANNOVER
      .w_DATAVERS=oParentObject.w_DATAVERS
      .w_VERIMIN=oParentObject.w_VERIMIN
      .w_DECTOT2=oParentObject.w_DECTOT2
      .w_IMPOIVA=oParentObject.w_IMPOIVA
      .w_IMPOVAL=oParentObject.w_IMPOVAL
      .w_CCVERSA=oParentObject.w_CCVERSA
      .w_CONFERMA=oParentObject.w_CONFERMA
          .DoRTCalc(1,5,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT2)
        .DoRTCalc(7,9,.f.)
        if not(empty(.w_CCVERSA))
          .link_1_9('Full')
        endif
          .DoRTCalc(10,11,.f.)
        .w_CONFERMA = .t.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_ATTIVER=.w_ATTIVER
      .oParentObject.w_ANNOVER=.w_ANNOVER
      .oParentObject.w_DATAVERS=.w_DATAVERS
      .oParentObject.w_VERIMIN=.w_VERIMIN
      .oParentObject.w_DECTOT2=.w_DECTOT2
      .oParentObject.w_IMPOIVA=.w_IMPOIVA
      .oParentObject.w_IMPOVAL=.w_IMPOVAL
      .oParentObject.w_CCVERSA=.w_CCVERSA
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_DECTOT2<>.w_DECTOT2
            .w_CALCPICT = DEFPIC(.w_DECTOT2)
        endif
        if .o_VERIMIN<>.w_VERIMIN
            .w_IMPOIVA = IIF((.w_VERIMIN>.w_IMPOIVA),0,.w_IMPOIVA)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CCVERSA
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCVERSA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CCVERSA)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CCVERSA))
          select BACODBAN,BADESCRI,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCVERSA)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCVERSA) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCCVERSA_1_9'),i_cWhere,'GSTE_ACB',"Elenco conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCVERSA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CCVERSA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CCVERSA)
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCVERSA = NVL(_Link_.BACODBAN,space(15))
      this.w_DESCRI = NVL(_Link_.BADESCRI,space(35))
      this.w_OBTEST = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CCVERSA = space(15)
      endif
      this.w_DESCRI = space(35)
      this.w_OBTEST = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_OBTEST) OR .w_OBTEST>i_DATSYS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CCVERSA = space(15)
        this.w_DESCRI = space(35)
        this.w_OBTEST = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCVERSA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATAVERS_1_3.value==this.w_DATAVERS)
      this.oPgFrm.Page1.oPag.oDATAVERS_1_3.value=this.w_DATAVERS
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPOIVA_1_7.value==this.w_IMPOIVA)
      this.oPgFrm.Page1.oPag.oIMPOIVA_1_7.value=this.w_IMPOIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPOVAL_1_8.value==this.w_IMPOVAL)
      this.oPgFrm.Page1.oPag.oIMPOVAL_1_8.value=this.w_IMPOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCCVERSA_1_9.value==this.w_CCVERSA)
      this.oPgFrm.Page1.oPag.oCCVERSA_1_9.value=this.w_CCVERSA
    endif
    if not(this.oPgFrm.Page1.oPag.oOBTEST_1_10.value==this.w_OBTEST)
      this.oPgFrm.Page1.oPag.oOBTEST_1_10.value=this.w_OBTEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_13.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_13.value=this.w_DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATAVERS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAVERS_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DATAVERS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_IMPOIVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPOIVA_1_7.SetFocus()
            i_bnoObbl = !empty(.w_IMPOIVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CCVERSA)) or not((EMPTY(.w_OBTEST) OR .w_OBTEST>i_DATSYS)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCVERSA_1_9.SetFocus()
            i_bnoObbl = !empty(.w_CCVERSA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VERIMIN = this.w_VERIMIN
    this.o_DECTOT2 = this.w_DECTOT2
    return

enddefine

* --- Define pages as container
define class tgscg_sa1Pag1 as StdContainer
  Width  = 588
  height = 148
  stdWidth  = 588
  stdheight = 148
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATAVERS_1_3 as StdField with uid="IOEQHNMBWT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATAVERS", cQueryName = "DATAVERS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 214146935,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=153, Top=13

  add object oIMPOIVA_1_7 as StdField with uid="ULYYIFQCCF",rtseq=7,rtrep=.f.,;
    cFormVar = "w_IMPOIVA", cQueryName = "IMPOIVA",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210097018,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=153, Top=41, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oIMPOVAL_1_8 as StdField with uid="FMZSXEWQVX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_IMPOVAL", cQueryName = "IMPOVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 256519302,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=292, Top=41, InputMask=replicate('X',3)

  add object oCCVERSA_1_9 as StdField with uid="OUIMGYHGZK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CCVERSA", cQueryName = "CCVERSA",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 251624922,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=153, Top=73, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CCVERSA"

  func oCCVERSA_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCVERSA_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCVERSA_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCCVERSA_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenco conti banche",'',this.parent.oContained
  endproc
  proc oCCVERSA_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CCVERSA
     i_obj.ecpSave()
  endproc

  add object oOBTEST_1_10 as StdField with uid="VMLLFLMBCC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_OBTEST", cQueryName = "OBTEST",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 233807386,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=516, Top=1

  add object oDESCRI_1_13 as StdField with uid="IXHYUKBLJE",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 151104458,;
   bGlobalFont=.t.,;
    Height=21, Width=226, Left=271, Top=73, InputMask=replicate('X',35)


  add object oBtn_1_16 as StdButton with uid="MHLOKQRERQ",left=396, top=99, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 144067302;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CCVERSA) AND .w_IMPOIVA>0 AND NOT EMPTY(.w_DATAVERS))
      endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="HQVVJWRZVK",left=447, top=99, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 151355974;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_11 as StdString with uid="UXZCOHOKFT",Visible=.t., Left=51, Top=43,;
    Alignment=1, Width=101, Height=15,;
    Caption="Acconto IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="FALLSPNNID",Visible=.t., Left=9, Top=73,;
    Alignment=1, Width=143, Height=15,;
    Caption="C/C versamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="GPAYZRMKCQ",Visible=.t., Left=44, Top=13,;
    Alignment=1, Width=108, Height=15,;
    Caption="Data versamento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sa1','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
