* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bgf                                                        *
*              GENERAZIONE FILE EDI DA PRINT SYSTEM                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-26                                                      *
* Last revis.: 2015-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNUMDIS,pCODSTR,pNOMFIL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bgf",oParentObject,m.pNUMDIS,m.pCODSTR,m.pNOMFIL)
return(i_retval)

define class tgste_bgf as StdBatch
  * --- Local variables
  pNUMDIS = space(10)
  pCODSTR = space(10)
  pNOMFIL = space(254)
  * --- WorkFile variables
  STR_INTE_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione FIle EDI
     
 DECLARE ARKEY(1,3) 
 ARKEY[1,3]="DIS_TINT" 
 ARKEY[1,1]=this.pNUMDIS 
 ARKEY[1,2]="DINUMDIS"
    * --- Try
    local bErr_03998D00
    bErr_03998D00=bTrsErr
    this.Try_03998D00()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03998D00
    * --- End
  endproc
  proc Try_03998D00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if Not empty(this.pCODSTR)
      gste_bee(this,this.pCODSTR,"","","", @ARKEY,0,"S","",this.pNOMFIL)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      ah_errormsg("Attenzione! Struttura CBI/SEPA non presente")
    endif
    return


  proc Init(oParentObject,pNUMDIS,pCODSTR,pNOMFIL)
    this.pNUMDIS=pNUMDIS
    this.pCODSTR=pCODSTR
    this.pNOMFIL=pNOMFIL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='STR_INTE'
    this.cWorkTables[2]='DOC_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNUMDIS,pCODSTR,pNOMFIL"
endproc
