* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_acu                                                        *
*              Configurazione interfaccia                                      *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_132]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-04                                                      *
* Last revis.: 2016-03-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsut_acu")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsut_acu")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsut_acu")
  return

* --- Class definition
define class tgsut_acu as StdPCForm
  Width  = 713
  Height = 511+35
  Top    = 7
  Left   = 9
  cComment = "Configurazione interfaccia"
  cPrg = "gsut_acu"
  HelpContextID=109658985
  add object cnt as tcgsut_acu
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsut_acu as PCContext
  w_CICPTBAR = space(1)
  w_CIDSKBAR = space(1)
  w_CITOOBDS = space(1)
  w_CITBSIZE = 0
  w_CISTABAR = space(1)
  w_CISHWMEN = space(1)
  w_CISTASCR = space(1)
  w_CIMENFIX = space(1)
  w_CIWNDMEN = space(1)
  w_CISEARMN = space(2)
  w_XXSEARMN = space(1)
  w_XXSEARM1 = space(1)
  w_XXSEARM2 = space(1)
  w_CIFRMBUT = space(1)
  w_CIFRMPAG = space(1)
  w_CITOOLMN = space(1)
  w_CIMENIMM = space(1)
  w_CINEWPRI = space(1)
  w_CIMONFRA = space(1)
  w_CI_ILIKE = space(1)
  w_CIMINPST = 0
  w_CIMODPST = 0
  w_CICODUTE = 0
  w_DESUTE = space(20)
  w_OLDNUMREC = space(10)
  w_CISELECL = 0
  w_CIEDTCOL = 0
  w_CIOBLCOL = 0
  w_CIDSBLFC = 0
  w_CIDSBLBC = 0
  w_CIRICCOL = 0
  w_COLORCTOB = space(10)
  w_COLORCTED = space(10)
  w_COLORCTSEL = space(10)
  w_COLORFCTDSBL = space(10)
  w_COLORBCTDSBL = space(10)
  w_COLORCTRI = space(10)
  w_CIEVIZOM = 0
  w_COLORGRD = space(10)
  w_COLORSCREEN = space(10)
  w_COLORROWDTL = space(10)
  w_CISCRCOL = 0
  w_CIDTLCLR = 0
  w_CIGRIDCL = 0
  w_COLORZOM = space(10)
  w_CICOLZOM = 0
  w_CILBLFNM = space(50)
  w_CITXTFNM = space(50)
  w_CICBXFNM = space(50)
  w_CIBTNFNM = space(50)
  w_CIGRDFNM = space(50)
  w_CIPAGFNM = space(50)
  w_CILBLFSZ = 0
  w_CITXTFSZ = 0
  w_CICBXFSZ = 0
  w_CIBTNFSZ = 0
  w_CIGRDFSZ = 0
  w_CIPAGFSZ = 0
  w_CISYSFRM = space(1)
  w_PRVNUM = space(12)
  w_PRVDATA = space(8)
  w_CISETCEN = space(1)
  w_CISETDAT = space(1)
  w_CISETPNT = space(1)
  w_CISETSEP = space(1)
  w_CISETMRK = space(1)
  w_SETDAT = space(1)
  w_SETMRK = space(1)
  w_SETCEN = space(1)
  w_SETPNT = space(1)
  w_SETSEP = space(1)
  w_CIATUCOL = 0
  w_CIATTCOL = 0
  w_CIATCCOL = 0
  w_CICOLPRM = 0
  w_CICOLNNP = 0
  w_CICOLSEL = 0
  w_CICOLHDR = 0
  w_COLORATU = space(10)
  w_COLORATT = space(10)
  w_COLORATC = space(10)
  w_CIAGEVIS = 0
  w_COLORPRM = space(10)
  w_COLORNNP = space(10)
  w_COLORHDR = space(10)
  w_COLORSEL = space(10)
  w_CIORAINI = 0
  w_CIORAFIN = 0
  w_CISTAINI = 0
  w_CISTAFIN = 0
  w_CITYPBAL = space(1)
  w_COLORACF = space(10)
  w_CICOLACF = 0
  w_CIDSKMEN = space(1)
  w_CINAVSTA = space(1)
  w_CINAVNUB = 0
  w_CINAVDIM = 0
  w_CIMNAFNM = space(50)
  w_CIMNAFSZ = 0
  w_CIWMAFNM = space(50)
  w_CIWMAFSZ = 0
  w_CIOPNGST = space(50)
  w_COLORPOSTIN = space(10)
  w_CIFLCOMP = space(1)
  w_CIDSKRSS = space(1)
  w_CILBLFIT = space(1)
  w_CITXTFIT = space(1)
  w_CICBXFIT = space(1)
  w_CIBTNFIT = space(1)
  w_CIGRDFIT = space(1)
  w_CIPAGFIT = space(1)
  w_CILBLFBO = space(1)
  w_CITXTFBO = space(1)
  w_CICBXFBO = space(1)
  w_CIBTNFBO = space(1)
  w_CIGRDFBO = space(1)
  w_CIPAGFBO = space(1)
  w_CIMDIFRM = space(1)
  w_CITABMEN = space(1)
  w_CIXPTHEM = 0
  w_CIRICONT = space(1)
  w_CIBCKGRD = space(1)
  w_CIPROBAR = space(1)
  w_CI_COLON = space(1)
  w_CINOBTNI = space(1)
  w_ciwaitwd = space(1)
  w_CISHWBTN = space(1)
  w_CIMRKGRD = space(1)
  w_CICTRGRD = space(1)
  w_CIHEHEZO = 0
  w_CIHEROZO = 0
  w_CIZOESPR = space(1)
  w_CIDSPCNT = 0
  w_CICOLPST = 0
  w_CIWARTYP = space(1)
  w_CIZOOMMO = space(1)
  w_CIAGSFNM = space(50)
  w_CIAGSFSZ = 0
  w_CIAGSFIT = space(1)
  w_CIAGSFBO = space(1)
  w_CIAGIFNM = space(50)
  w_CIAGIFSZ = 0
  w_CIAGIFIT = space(1)
  w_CIAGIFBO = space(1)
  w_CIGIORHO = 0
  w_CIZOOMSC = space(1)
  w_CIDIMFRM = space(1)
  w_CIWIDFRM = 0
  w_CIHEIFRM = 0
  w_CISEARNT = space(1)
  w_CISEARNT = space(1)
  w_CISEAMNW = 0
  w_CISEADKM = 0
  w_CISEAAPB = 0
  w_COPOSREC = space(1)
  w_CINUMREC = 0
  w_CINRECEN = 0
  w_CITYPCAL = space(1)
  w_CISAVFRM = space(1)
  w_XXB1 = space(1)
  w_XXB2 = space(1)
  w_XXB3 = space(1)
  w_XXB4 = space(1)
  w_XXB5 = space(1)
  w_XXB17 = space(1)
  w_XXB6 = space(1)
  w_XXB7 = space(1)
  w_XXB8 = space(1)
  w_XXB9 = space(1)
  w_XXB10 = space(1)
  w_XXB11 = space(1)
  w_XXB12 = space(1)
  w_XXB13 = space(1)
  w_XXB14 = space(1)
  w_XXB15 = space(1)
  w_XXB16 = space(1)
  w_CITBVWBT = space(20)
  w_CIVTHEME = 0
  w_CIGADGET = space(1)
  w_OSAVFRM = space(1)
  w_OMDIFRM = space(1)
  proc Save(oFrom)
    this.w_CICPTBAR = oFrom.w_CICPTBAR
    this.w_CIDSKBAR = oFrom.w_CIDSKBAR
    this.w_CITOOBDS = oFrom.w_CITOOBDS
    this.w_CITBSIZE = oFrom.w_CITBSIZE
    this.w_CISTABAR = oFrom.w_CISTABAR
    this.w_CISHWMEN = oFrom.w_CISHWMEN
    this.w_CISTASCR = oFrom.w_CISTASCR
    this.w_CIMENFIX = oFrom.w_CIMENFIX
    this.w_CIWNDMEN = oFrom.w_CIWNDMEN
    this.w_CISEARMN = oFrom.w_CISEARMN
    this.w_XXSEARMN = oFrom.w_XXSEARMN
    this.w_XXSEARM1 = oFrom.w_XXSEARM1
    this.w_XXSEARM2 = oFrom.w_XXSEARM2
    this.w_CIFRMBUT = oFrom.w_CIFRMBUT
    this.w_CIFRMPAG = oFrom.w_CIFRMPAG
    this.w_CITOOLMN = oFrom.w_CITOOLMN
    this.w_CIMENIMM = oFrom.w_CIMENIMM
    this.w_CINEWPRI = oFrom.w_CINEWPRI
    this.w_CIMONFRA = oFrom.w_CIMONFRA
    this.w_CI_ILIKE = oFrom.w_CI_ILIKE
    this.w_CIMINPST = oFrom.w_CIMINPST
    this.w_CIMODPST = oFrom.w_CIMODPST
    this.w_CICODUTE = oFrom.w_CICODUTE
    this.w_DESUTE = oFrom.w_DESUTE
    this.w_OLDNUMREC = oFrom.w_OLDNUMREC
    this.w_CISELECL = oFrom.w_CISELECL
    this.w_CIEDTCOL = oFrom.w_CIEDTCOL
    this.w_CIOBLCOL = oFrom.w_CIOBLCOL
    this.w_CIDSBLFC = oFrom.w_CIDSBLFC
    this.w_CIDSBLBC = oFrom.w_CIDSBLBC
    this.w_CIRICCOL = oFrom.w_CIRICCOL
    this.w_COLORCTOB = oFrom.w_COLORCTOB
    this.w_COLORCTED = oFrom.w_COLORCTED
    this.w_COLORCTSEL = oFrom.w_COLORCTSEL
    this.w_COLORFCTDSBL = oFrom.w_COLORFCTDSBL
    this.w_COLORBCTDSBL = oFrom.w_COLORBCTDSBL
    this.w_COLORCTRI = oFrom.w_COLORCTRI
    this.w_CIEVIZOM = oFrom.w_CIEVIZOM
    this.w_COLORGRD = oFrom.w_COLORGRD
    this.w_COLORSCREEN = oFrom.w_COLORSCREEN
    this.w_COLORROWDTL = oFrom.w_COLORROWDTL
    this.w_CISCRCOL = oFrom.w_CISCRCOL
    this.w_CIDTLCLR = oFrom.w_CIDTLCLR
    this.w_CIGRIDCL = oFrom.w_CIGRIDCL
    this.w_COLORZOM = oFrom.w_COLORZOM
    this.w_CICOLZOM = oFrom.w_CICOLZOM
    this.w_CILBLFNM = oFrom.w_CILBLFNM
    this.w_CITXTFNM = oFrom.w_CITXTFNM
    this.w_CICBXFNM = oFrom.w_CICBXFNM
    this.w_CIBTNFNM = oFrom.w_CIBTNFNM
    this.w_CIGRDFNM = oFrom.w_CIGRDFNM
    this.w_CIPAGFNM = oFrom.w_CIPAGFNM
    this.w_CILBLFSZ = oFrom.w_CILBLFSZ
    this.w_CITXTFSZ = oFrom.w_CITXTFSZ
    this.w_CICBXFSZ = oFrom.w_CICBXFSZ
    this.w_CIBTNFSZ = oFrom.w_CIBTNFSZ
    this.w_CIGRDFSZ = oFrom.w_CIGRDFSZ
    this.w_CIPAGFSZ = oFrom.w_CIPAGFSZ
    this.w_CISYSFRM = oFrom.w_CISYSFRM
    this.w_PRVNUM = oFrom.w_PRVNUM
    this.w_PRVDATA = oFrom.w_PRVDATA
    this.w_CISETCEN = oFrom.w_CISETCEN
    this.w_CISETDAT = oFrom.w_CISETDAT
    this.w_CISETPNT = oFrom.w_CISETPNT
    this.w_CISETSEP = oFrom.w_CISETSEP
    this.w_CISETMRK = oFrom.w_CISETMRK
    this.w_SETDAT = oFrom.w_SETDAT
    this.w_SETMRK = oFrom.w_SETMRK
    this.w_SETCEN = oFrom.w_SETCEN
    this.w_SETPNT = oFrom.w_SETPNT
    this.w_SETSEP = oFrom.w_SETSEP
    this.w_CIATUCOL = oFrom.w_CIATUCOL
    this.w_CIATTCOL = oFrom.w_CIATTCOL
    this.w_CIATCCOL = oFrom.w_CIATCCOL
    this.w_CICOLPRM = oFrom.w_CICOLPRM
    this.w_CICOLNNP = oFrom.w_CICOLNNP
    this.w_CICOLSEL = oFrom.w_CICOLSEL
    this.w_CICOLHDR = oFrom.w_CICOLHDR
    this.w_COLORATU = oFrom.w_COLORATU
    this.w_COLORATT = oFrom.w_COLORATT
    this.w_COLORATC = oFrom.w_COLORATC
    this.w_CIAGEVIS = oFrom.w_CIAGEVIS
    this.w_COLORPRM = oFrom.w_COLORPRM
    this.w_COLORNNP = oFrom.w_COLORNNP
    this.w_COLORHDR = oFrom.w_COLORHDR
    this.w_COLORSEL = oFrom.w_COLORSEL
    this.w_CIORAINI = oFrom.w_CIORAINI
    this.w_CIORAFIN = oFrom.w_CIORAFIN
    this.w_CISTAINI = oFrom.w_CISTAINI
    this.w_CISTAFIN = oFrom.w_CISTAFIN
    this.w_CITYPBAL = oFrom.w_CITYPBAL
    this.w_COLORACF = oFrom.w_COLORACF
    this.w_CICOLACF = oFrom.w_CICOLACF
    this.w_CIDSKMEN = oFrom.w_CIDSKMEN
    this.w_CINAVSTA = oFrom.w_CINAVSTA
    this.w_CINAVNUB = oFrom.w_CINAVNUB
    this.w_CINAVDIM = oFrom.w_CINAVDIM
    this.w_CIMNAFNM = oFrom.w_CIMNAFNM
    this.w_CIMNAFSZ = oFrom.w_CIMNAFSZ
    this.w_CIWMAFNM = oFrom.w_CIWMAFNM
    this.w_CIWMAFSZ = oFrom.w_CIWMAFSZ
    this.w_CIOPNGST = oFrom.w_CIOPNGST
    this.w_COLORPOSTIN = oFrom.w_COLORPOSTIN
    this.w_CIFLCOMP = oFrom.w_CIFLCOMP
    this.w_CIDSKRSS = oFrom.w_CIDSKRSS
    this.w_CILBLFIT = oFrom.w_CILBLFIT
    this.w_CITXTFIT = oFrom.w_CITXTFIT
    this.w_CICBXFIT = oFrom.w_CICBXFIT
    this.w_CIBTNFIT = oFrom.w_CIBTNFIT
    this.w_CIGRDFIT = oFrom.w_CIGRDFIT
    this.w_CIPAGFIT = oFrom.w_CIPAGFIT
    this.w_CILBLFBO = oFrom.w_CILBLFBO
    this.w_CITXTFBO = oFrom.w_CITXTFBO
    this.w_CICBXFBO = oFrom.w_CICBXFBO
    this.w_CIBTNFBO = oFrom.w_CIBTNFBO
    this.w_CIGRDFBO = oFrom.w_CIGRDFBO
    this.w_CIPAGFBO = oFrom.w_CIPAGFBO
    this.w_CIMDIFRM = oFrom.w_CIMDIFRM
    this.w_CITABMEN = oFrom.w_CITABMEN
    this.w_CIXPTHEM = oFrom.w_CIXPTHEM
    this.w_CIRICONT = oFrom.w_CIRICONT
    this.w_CIBCKGRD = oFrom.w_CIBCKGRD
    this.w_CIPROBAR = oFrom.w_CIPROBAR
    this.w_CI_COLON = oFrom.w_CI_COLON
    this.w_CINOBTNI = oFrom.w_CINOBTNI
    this.w_ciwaitwd = oFrom.w_ciwaitwd
    this.w_CISHWBTN = oFrom.w_CISHWBTN
    this.w_CIMRKGRD = oFrom.w_CIMRKGRD
    this.w_CICTRGRD = oFrom.w_CICTRGRD
    this.w_CIHEHEZO = oFrom.w_CIHEHEZO
    this.w_CIHEROZO = oFrom.w_CIHEROZO
    this.w_CIZOESPR = oFrom.w_CIZOESPR
    this.w_CIDSPCNT = oFrom.w_CIDSPCNT
    this.w_CICOLPST = oFrom.w_CICOLPST
    this.w_CIWARTYP = oFrom.w_CIWARTYP
    this.w_CIZOOMMO = oFrom.w_CIZOOMMO
    this.w_CIAGSFNM = oFrom.w_CIAGSFNM
    this.w_CIAGSFSZ = oFrom.w_CIAGSFSZ
    this.w_CIAGSFIT = oFrom.w_CIAGSFIT
    this.w_CIAGSFBO = oFrom.w_CIAGSFBO
    this.w_CIAGIFNM = oFrom.w_CIAGIFNM
    this.w_CIAGIFSZ = oFrom.w_CIAGIFSZ
    this.w_CIAGIFIT = oFrom.w_CIAGIFIT
    this.w_CIAGIFBO = oFrom.w_CIAGIFBO
    this.w_CIGIORHO = oFrom.w_CIGIORHO
    this.w_CIZOOMSC = oFrom.w_CIZOOMSC
    this.w_CIDIMFRM = oFrom.w_CIDIMFRM
    this.w_CIWIDFRM = oFrom.w_CIWIDFRM
    this.w_CIHEIFRM = oFrom.w_CIHEIFRM
    this.w_CISEARNT = oFrom.w_CISEARNT
    this.w_CISEARNT = oFrom.w_CISEARNT
    this.w_CISEAMNW = oFrom.w_CISEAMNW
    this.w_CISEADKM = oFrom.w_CISEADKM
    this.w_CISEAAPB = oFrom.w_CISEAAPB
    this.w_COPOSREC = oFrom.w_COPOSREC
    this.w_CINUMREC = oFrom.w_CINUMREC
    this.w_CINRECEN = oFrom.w_CINRECEN
    this.w_CITYPCAL = oFrom.w_CITYPCAL
    this.w_CISAVFRM = oFrom.w_CISAVFRM
    this.w_XXB1 = oFrom.w_XXB1
    this.w_XXB2 = oFrom.w_XXB2
    this.w_XXB3 = oFrom.w_XXB3
    this.w_XXB4 = oFrom.w_XXB4
    this.w_XXB5 = oFrom.w_XXB5
    this.w_XXB17 = oFrom.w_XXB17
    this.w_XXB6 = oFrom.w_XXB6
    this.w_XXB7 = oFrom.w_XXB7
    this.w_XXB8 = oFrom.w_XXB8
    this.w_XXB9 = oFrom.w_XXB9
    this.w_XXB10 = oFrom.w_XXB10
    this.w_XXB11 = oFrom.w_XXB11
    this.w_XXB12 = oFrom.w_XXB12
    this.w_XXB13 = oFrom.w_XXB13
    this.w_XXB14 = oFrom.w_XXB14
    this.w_XXB15 = oFrom.w_XXB15
    this.w_XXB16 = oFrom.w_XXB16
    this.w_CITBVWBT = oFrom.w_CITBVWBT
    this.w_CIVTHEME = oFrom.w_CIVTHEME
    this.w_CIGADGET = oFrom.w_CIGADGET
    this.w_OSAVFRM = oFrom.w_OSAVFRM
    this.w_OMDIFRM = oFrom.w_OMDIFRM
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_CICPTBAR = this.w_CICPTBAR
    oTo.w_CIDSKBAR = this.w_CIDSKBAR
    oTo.w_CITOOBDS = this.w_CITOOBDS
    oTo.w_CITBSIZE = this.w_CITBSIZE
    oTo.w_CISTABAR = this.w_CISTABAR
    oTo.w_CISHWMEN = this.w_CISHWMEN
    oTo.w_CISTASCR = this.w_CISTASCR
    oTo.w_CIMENFIX = this.w_CIMENFIX
    oTo.w_CIWNDMEN = this.w_CIWNDMEN
    oTo.w_CISEARMN = this.w_CISEARMN
    oTo.w_XXSEARMN = this.w_XXSEARMN
    oTo.w_XXSEARM1 = this.w_XXSEARM1
    oTo.w_XXSEARM2 = this.w_XXSEARM2
    oTo.w_CIFRMBUT = this.w_CIFRMBUT
    oTo.w_CIFRMPAG = this.w_CIFRMPAG
    oTo.w_CITOOLMN = this.w_CITOOLMN
    oTo.w_CIMENIMM = this.w_CIMENIMM
    oTo.w_CINEWPRI = this.w_CINEWPRI
    oTo.w_CIMONFRA = this.w_CIMONFRA
    oTo.w_CI_ILIKE = this.w_CI_ILIKE
    oTo.w_CIMINPST = this.w_CIMINPST
    oTo.w_CIMODPST = this.w_CIMODPST
    oTo.w_CICODUTE = this.w_CICODUTE
    oTo.w_DESUTE = this.w_DESUTE
    oTo.w_OLDNUMREC = this.w_OLDNUMREC
    oTo.w_CISELECL = this.w_CISELECL
    oTo.w_CIEDTCOL = this.w_CIEDTCOL
    oTo.w_CIOBLCOL = this.w_CIOBLCOL
    oTo.w_CIDSBLFC = this.w_CIDSBLFC
    oTo.w_CIDSBLBC = this.w_CIDSBLBC
    oTo.w_CIRICCOL = this.w_CIRICCOL
    oTo.w_COLORCTOB = this.w_COLORCTOB
    oTo.w_COLORCTED = this.w_COLORCTED
    oTo.w_COLORCTSEL = this.w_COLORCTSEL
    oTo.w_COLORFCTDSBL = this.w_COLORFCTDSBL
    oTo.w_COLORBCTDSBL = this.w_COLORBCTDSBL
    oTo.w_COLORCTRI = this.w_COLORCTRI
    oTo.w_CIEVIZOM = this.w_CIEVIZOM
    oTo.w_COLORGRD = this.w_COLORGRD
    oTo.w_COLORSCREEN = this.w_COLORSCREEN
    oTo.w_COLORROWDTL = this.w_COLORROWDTL
    oTo.w_CISCRCOL = this.w_CISCRCOL
    oTo.w_CIDTLCLR = this.w_CIDTLCLR
    oTo.w_CIGRIDCL = this.w_CIGRIDCL
    oTo.w_COLORZOM = this.w_COLORZOM
    oTo.w_CICOLZOM = this.w_CICOLZOM
    oTo.w_CILBLFNM = this.w_CILBLFNM
    oTo.w_CITXTFNM = this.w_CITXTFNM
    oTo.w_CICBXFNM = this.w_CICBXFNM
    oTo.w_CIBTNFNM = this.w_CIBTNFNM
    oTo.w_CIGRDFNM = this.w_CIGRDFNM
    oTo.w_CIPAGFNM = this.w_CIPAGFNM
    oTo.w_CILBLFSZ = this.w_CILBLFSZ
    oTo.w_CITXTFSZ = this.w_CITXTFSZ
    oTo.w_CICBXFSZ = this.w_CICBXFSZ
    oTo.w_CIBTNFSZ = this.w_CIBTNFSZ
    oTo.w_CIGRDFSZ = this.w_CIGRDFSZ
    oTo.w_CIPAGFSZ = this.w_CIPAGFSZ
    oTo.w_CISYSFRM = this.w_CISYSFRM
    oTo.w_PRVNUM = this.w_PRVNUM
    oTo.w_PRVDATA = this.w_PRVDATA
    oTo.w_CISETCEN = this.w_CISETCEN
    oTo.w_CISETDAT = this.w_CISETDAT
    oTo.w_CISETPNT = this.w_CISETPNT
    oTo.w_CISETSEP = this.w_CISETSEP
    oTo.w_CISETMRK = this.w_CISETMRK
    oTo.w_SETDAT = this.w_SETDAT
    oTo.w_SETMRK = this.w_SETMRK
    oTo.w_SETCEN = this.w_SETCEN
    oTo.w_SETPNT = this.w_SETPNT
    oTo.w_SETSEP = this.w_SETSEP
    oTo.w_CIATUCOL = this.w_CIATUCOL
    oTo.w_CIATTCOL = this.w_CIATTCOL
    oTo.w_CIATCCOL = this.w_CIATCCOL
    oTo.w_CICOLPRM = this.w_CICOLPRM
    oTo.w_CICOLNNP = this.w_CICOLNNP
    oTo.w_CICOLSEL = this.w_CICOLSEL
    oTo.w_CICOLHDR = this.w_CICOLHDR
    oTo.w_COLORATU = this.w_COLORATU
    oTo.w_COLORATT = this.w_COLORATT
    oTo.w_COLORATC = this.w_COLORATC
    oTo.w_CIAGEVIS = this.w_CIAGEVIS
    oTo.w_COLORPRM = this.w_COLORPRM
    oTo.w_COLORNNP = this.w_COLORNNP
    oTo.w_COLORHDR = this.w_COLORHDR
    oTo.w_COLORSEL = this.w_COLORSEL
    oTo.w_CIORAINI = this.w_CIORAINI
    oTo.w_CIORAFIN = this.w_CIORAFIN
    oTo.w_CISTAINI = this.w_CISTAINI
    oTo.w_CISTAFIN = this.w_CISTAFIN
    oTo.w_CITYPBAL = this.w_CITYPBAL
    oTo.w_COLORACF = this.w_COLORACF
    oTo.w_CICOLACF = this.w_CICOLACF
    oTo.w_CIDSKMEN = this.w_CIDSKMEN
    oTo.w_CINAVSTA = this.w_CINAVSTA
    oTo.w_CINAVNUB = this.w_CINAVNUB
    oTo.w_CINAVDIM = this.w_CINAVDIM
    oTo.w_CIMNAFNM = this.w_CIMNAFNM
    oTo.w_CIMNAFSZ = this.w_CIMNAFSZ
    oTo.w_CIWMAFNM = this.w_CIWMAFNM
    oTo.w_CIWMAFSZ = this.w_CIWMAFSZ
    oTo.w_CIOPNGST = this.w_CIOPNGST
    oTo.w_COLORPOSTIN = this.w_COLORPOSTIN
    oTo.w_CIFLCOMP = this.w_CIFLCOMP
    oTo.w_CIDSKRSS = this.w_CIDSKRSS
    oTo.w_CILBLFIT = this.w_CILBLFIT
    oTo.w_CITXTFIT = this.w_CITXTFIT
    oTo.w_CICBXFIT = this.w_CICBXFIT
    oTo.w_CIBTNFIT = this.w_CIBTNFIT
    oTo.w_CIGRDFIT = this.w_CIGRDFIT
    oTo.w_CIPAGFIT = this.w_CIPAGFIT
    oTo.w_CILBLFBO = this.w_CILBLFBO
    oTo.w_CITXTFBO = this.w_CITXTFBO
    oTo.w_CICBXFBO = this.w_CICBXFBO
    oTo.w_CIBTNFBO = this.w_CIBTNFBO
    oTo.w_CIGRDFBO = this.w_CIGRDFBO
    oTo.w_CIPAGFBO = this.w_CIPAGFBO
    oTo.w_CIMDIFRM = this.w_CIMDIFRM
    oTo.w_CITABMEN = this.w_CITABMEN
    oTo.w_CIXPTHEM = this.w_CIXPTHEM
    oTo.w_CIRICONT = this.w_CIRICONT
    oTo.w_CIBCKGRD = this.w_CIBCKGRD
    oTo.w_CIPROBAR = this.w_CIPROBAR
    oTo.w_CI_COLON = this.w_CI_COLON
    oTo.w_CINOBTNI = this.w_CINOBTNI
    oTo.w_ciwaitwd = this.w_ciwaitwd
    oTo.w_CISHWBTN = this.w_CISHWBTN
    oTo.w_CIMRKGRD = this.w_CIMRKGRD
    oTo.w_CICTRGRD = this.w_CICTRGRD
    oTo.w_CIHEHEZO = this.w_CIHEHEZO
    oTo.w_CIHEROZO = this.w_CIHEROZO
    oTo.w_CIZOESPR = this.w_CIZOESPR
    oTo.w_CIDSPCNT = this.w_CIDSPCNT
    oTo.w_CICOLPST = this.w_CICOLPST
    oTo.w_CIWARTYP = this.w_CIWARTYP
    oTo.w_CIZOOMMO = this.w_CIZOOMMO
    oTo.w_CIAGSFNM = this.w_CIAGSFNM
    oTo.w_CIAGSFSZ = this.w_CIAGSFSZ
    oTo.w_CIAGSFIT = this.w_CIAGSFIT
    oTo.w_CIAGSFBO = this.w_CIAGSFBO
    oTo.w_CIAGIFNM = this.w_CIAGIFNM
    oTo.w_CIAGIFSZ = this.w_CIAGIFSZ
    oTo.w_CIAGIFIT = this.w_CIAGIFIT
    oTo.w_CIAGIFBO = this.w_CIAGIFBO
    oTo.w_CIGIORHO = this.w_CIGIORHO
    oTo.w_CIZOOMSC = this.w_CIZOOMSC
    oTo.w_CIDIMFRM = this.w_CIDIMFRM
    oTo.w_CIWIDFRM = this.w_CIWIDFRM
    oTo.w_CIHEIFRM = this.w_CIHEIFRM
    oTo.w_CISEARNT = this.w_CISEARNT
    oTo.w_CISEARNT = this.w_CISEARNT
    oTo.w_CISEAMNW = this.w_CISEAMNW
    oTo.w_CISEADKM = this.w_CISEADKM
    oTo.w_CISEAAPB = this.w_CISEAAPB
    oTo.w_COPOSREC = this.w_COPOSREC
    oTo.w_CINUMREC = this.w_CINUMREC
    oTo.w_CINRECEN = this.w_CINRECEN
    oTo.w_CITYPCAL = this.w_CITYPCAL
    oTo.w_CISAVFRM = this.w_CISAVFRM
    oTo.w_XXB1 = this.w_XXB1
    oTo.w_XXB2 = this.w_XXB2
    oTo.w_XXB3 = this.w_XXB3
    oTo.w_XXB4 = this.w_XXB4
    oTo.w_XXB5 = this.w_XXB5
    oTo.w_XXB17 = this.w_XXB17
    oTo.w_XXB6 = this.w_XXB6
    oTo.w_XXB7 = this.w_XXB7
    oTo.w_XXB8 = this.w_XXB8
    oTo.w_XXB9 = this.w_XXB9
    oTo.w_XXB10 = this.w_XXB10
    oTo.w_XXB11 = this.w_XXB11
    oTo.w_XXB12 = this.w_XXB12
    oTo.w_XXB13 = this.w_XXB13
    oTo.w_XXB14 = this.w_XXB14
    oTo.w_XXB15 = this.w_XXB15
    oTo.w_XXB16 = this.w_XXB16
    oTo.w_CITBVWBT = this.w_CITBVWBT
    oTo.w_CIVTHEME = this.w_CIVTHEME
    oTo.w_CIGADGET = this.w_CIGADGET
    oTo.w_OSAVFRM = this.w_OSAVFRM
    oTo.w_OMDIFRM = this.w_OMDIFRM
    PCContext::Load(oTo)
enddefine

define class tcgsut_acu as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 713
  Height = 511+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-09"
  HelpContextID=109658985
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=181

  * --- Constant Properties
  CONF_INT_IDX = 0
  CPUSERS_IDX = 0
  cFile = "CONF_INT"
  cKeySelect = "CICODUTE"
  cKeyWhere  = "CICODUTE=this.w_CICODUTE"
  cKeyWhereODBC = '"CICODUTE="+cp_ToStrODBC(this.w_CICODUTE)';

  cKeyWhereODBCqualified = '"CONF_INT.CICODUTE="+cp_ToStrODBC(this.w_CICODUTE)';

  cPrg = "gsut_acu"
  cComment = "Configurazione interfaccia"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CICPTBAR = space(1)
  w_CIDSKBAR = space(1)
  w_CITOOBDS = space(1)
  w_CITBSIZE = 0
  w_CISTABAR = space(1)
  w_CISHWMEN = space(1)
  o_CISHWMEN = space(1)
  w_CISTASCR = space(1)
  w_CIMENFIX = space(1)
  w_CIWNDMEN = space(1)
  w_CISEARMN = space(2)
  w_XXSEARMN = space(1)
  o_XXSEARMN = space(1)
  w_XXSEARM1 = space(1)
  o_XXSEARM1 = space(1)
  w_XXSEARM2 = space(1)
  o_XXSEARM2 = space(1)
  w_CIFRMBUT = space(1)
  o_CIFRMBUT = space(1)
  w_CIFRMPAG = space(1)
  w_CITOOLMN = space(1)
  w_CIMENIMM = space(1)
  w_CINEWPRI = space(1)
  w_CIMONFRA = space(1)
  w_CI_ILIKE = space(1)
  w_CIMINPST = 0
  w_CIMODPST = 0
  w_CICODUTE = 0
  w_DESUTE = space(20)
  w_OLDNUMREC = space(10)
  w_CISELECL = 0
  w_CIEDTCOL = 0
  w_CIOBLCOL = 0
  w_CIDSBLFC = 0
  w_CIDSBLBC = 0
  w_CIRICCOL = 0
  w_COLORCTOB = space(10)
  w_COLORCTED = space(10)
  w_COLORCTSEL = space(10)
  w_COLORFCTDSBL = space(10)
  w_COLORBCTDSBL = space(10)
  w_COLORCTRI = space(10)
  w_CIEVIZOM = 0
  w_COLORGRD = space(10)
  w_COLORSCREEN = space(10)
  w_COLORROWDTL = space(10)
  w_CISCRCOL = 0
  w_CIDTLCLR = 0
  w_CIGRIDCL = 0
  w_COLORZOM = space(10)
  w_CICOLZOM = 0
  w_CILBLFNM = space(50)
  w_CITXTFNM = space(50)
  w_CICBXFNM = space(50)
  w_CIBTNFNM = space(50)
  w_CIGRDFNM = space(50)
  w_CIPAGFNM = space(50)
  w_CILBLFSZ = 0
  w_CITXTFSZ = 0
  w_CICBXFSZ = 0
  w_CIBTNFSZ = 0
  w_CIGRDFSZ = 0
  w_CIPAGFSZ = 0
  w_CISYSFRM = space(1)
  o_CISYSFRM = space(1)
  w_PRVNUM = space(12)
  w_PRVDATA = ctod('  /  /  ')
  w_CISETCEN = space(1)
  o_CISETCEN = space(1)
  w_CISETDAT = space(1)
  o_CISETDAT = space(1)
  w_CISETPNT = space(1)
  o_CISETPNT = space(1)
  w_CISETSEP = space(1)
  o_CISETSEP = space(1)
  w_CISETMRK = space(1)
  o_CISETMRK = space(1)
  w_SETDAT = space(1)
  o_SETDAT = space(1)
  w_SETMRK = space(1)
  o_SETMRK = space(1)
  w_SETCEN = space(1)
  o_SETCEN = space(1)
  w_SETPNT = space(1)
  o_SETPNT = space(1)
  w_SETSEP = space(1)
  o_SETSEP = space(1)
  w_CIATUCOL = 0
  w_CIATTCOL = 0
  w_CIATCCOL = 0
  w_CICOLPRM = 0
  w_CICOLNNP = 0
  w_CICOLSEL = 0
  w_CICOLHDR = 0
  w_COLORATU = space(10)
  w_COLORATT = space(10)
  w_COLORATC = space(10)
  w_CIAGEVIS = 0
  w_COLORPRM = space(10)
  w_COLORNNP = space(10)
  w_COLORHDR = space(10)
  w_COLORSEL = space(10)
  w_CIORAINI = 0
  w_CIORAFIN = 0
  w_CISTAINI = 0
  w_CISTAFIN = 0
  w_CITYPBAL = space(1)
  w_COLORACF = space(10)
  w_CICOLACF = 0
  w_CIDSKMEN = space(1)
  o_CIDSKMEN = space(1)
  w_CINAVSTA = space(1)
  w_CINAVNUB = 0
  w_CINAVDIM = 0
  w_CIMNAFNM = space(50)
  w_CIMNAFSZ = 0
  w_CIWMAFNM = space(50)
  w_CIWMAFSZ = 0
  w_CIOPNGST = space(50)
  w_COLORPOSTIN = space(10)
  w_CIFLCOMP = space(1)
  w_CIDSKRSS = space(1)
  w_CILBLFIT = space(1)
  w_CITXTFIT = space(1)
  w_CICBXFIT = space(1)
  w_CIBTNFIT = space(1)
  w_CIGRDFIT = space(1)
  w_CIPAGFIT = space(1)
  w_CILBLFBO = space(1)
  w_CITXTFBO = space(1)
  w_CICBXFBO = space(1)
  w_CIBTNFBO = space(1)
  w_CIGRDFBO = space(1)
  w_CIPAGFBO = space(1)
  w_CIMDIFRM = space(1)
  o_CIMDIFRM = space(1)
  w_CITABMEN = space(1)
  w_CIXPTHEM = 0
  w_CIRICONT = space(1)
  w_CIBCKGRD = space(1)
  w_CIPROBAR = space(1)
  w_CI_COLON = space(1)
  w_CINOBTNI = space(1)
  w_ciwaitwd = space(1)
  w_CISHWBTN = space(1)
  w_CIMRKGRD = space(1)
  w_CICTRGRD = space(1)
  o_CICTRGRD = space(1)
  w_CIHEHEZO = 0
  o_CIHEHEZO = 0
  w_CIHEROZO = 0
  w_CIZOESPR = space(1)
  w_CIDSPCNT = 0
  w_CICOLPST = 0
  w_CIWARTYP = space(1)
  w_CIZOOMMO = space(1)
  w_CIAGSFNM = space(50)
  w_CIAGSFSZ = 0
  w_CIAGSFIT = space(1)
  w_CIAGSFBO = space(1)
  w_CIAGIFNM = space(50)
  w_CIAGIFSZ = 0
  w_CIAGIFIT = space(1)
  w_CIAGIFBO = space(1)
  w_CIGIORHO = 0
  w_CIZOOMSC = space(1)
  w_CIDIMFRM = space(1)
  o_CIDIMFRM = space(1)
  w_CIWIDFRM = 0
  w_CIHEIFRM = 0
  w_CISEARNT = space(1)
  o_CISEARNT = space(1)
  w_CISEARNT = space(1)
  w_CISEAMNW = 0
  w_CISEADKM = 0
  w_CISEAAPB = 0
  w_COPOSREC = space(1)
  w_CINUMREC = 0
  w_CINRECEN = 0
  w_CITYPCAL = space(1)
  w_CISAVFRM = space(1)
  w_XXB1 = space(1)
  o_XXB1 = space(1)
  w_XXB2 = space(1)
  o_XXB2 = space(1)
  w_XXB3 = space(1)
  o_XXB3 = space(1)
  w_XXB4 = space(1)
  o_XXB4 = space(1)
  w_XXB5 = space(1)
  o_XXB5 = space(1)
  w_XXB17 = space(1)
  o_XXB17 = space(1)
  w_XXB6 = space(1)
  o_XXB6 = space(1)
  w_XXB7 = space(1)
  o_XXB7 = space(1)
  w_XXB8 = space(1)
  o_XXB8 = space(1)
  w_XXB9 = space(1)
  o_XXB9 = space(1)
  w_XXB10 = space(1)
  o_XXB10 = space(1)
  w_XXB11 = space(1)
  o_XXB11 = space(1)
  w_XXB12 = space(1)
  o_XXB12 = space(1)
  w_XXB13 = space(1)
  o_XXB13 = space(1)
  w_XXB14 = space(1)
  o_XXB14 = space(1)
  w_XXB15 = space(1)
  o_XXB15 = space(1)
  w_XXB16 = space(1)
  o_XXB16 = space(1)
  w_CITBVWBT = space(20)
  w_CIVTHEME = 0
  o_CIVTHEME = 0
  w_CIGADGET = space(1)
  o_CIGADGET = space(1)
  w_OSAVFRM = space(1)
  w_OMDIFRM = space(1)
  w_XXB1IM = .NULL.
  w_XXB4IM = .NULL.
  w_XXB3IM = .NULL.
  w_XXB2IM = .NULL.
  w_XXB5IM = .NULL.
  w_XXB61IM = .NULL.
  w_XXB17IM = .NULL.
  w_XXB62IM = .NULL.
  w_XXB7IM = .NULL.
  w_XXB92IM = .NULL.
  w_XXB91IM = .NULL.
  w_XXB82IM = .NULL.
  w_XXB81IM = .NULL.
  w_XXB101IM = .NULL.
  w_XXB102IM = .NULL.
  w_XXB111IM = .NULL.
  w_XXB112IM = .NULL.
  w_XXB12IM = .NULL.
  w_XXB13IM = .NULL.
  w_XXB14IM = .NULL.
  w_XXB15IM = .NULL.
  w_XXB16IM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=8, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_acuPag1","gsut_acu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Impostazioni")
      .Pages(1).HelpContextID = 93160592
      .Pages(2).addobject("oPag","tgsut_acuPag2","gsut_acu",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Form / Control")
      .Pages(2).HelpContextID = 240453001
      .Pages(3).addobject("oPag","tgsut_acuPag3","gsut_acu",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Colori")
      .Pages(3).HelpContextID = 168620070
      .Pages(4).addobject("oPag","tgsut_acuPag4","gsut_acu",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Font")
      .Pages(4).HelpContextID = 101576618
      .Pages(5).addobject("oPag","tgsut_acuPag5","gsut_acu",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Impost. internaz.")
      .Pages(5).HelpContextID = 89528688
      .Pages(6).addobject("oPag","tgsut_acuPag6","gsut_acu",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Agenda")
      .Pages(6).HelpContextID = 19625990
      .Pages(7).addobject("oPag","tgsut_acuPag7","gsut_acu",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Desktop men�")
      .Pages(7).HelpContextID = 19505702
      .Pages(8).addobject("oPag","tgsut_acuPag8","gsut_acu",8)
      .Pages(8).oPag.Visible=.t.
      .Pages(8).Caption=cp_Translate("Barra delle applicazioni")
      .Pages(8).HelpContextID = 62423136
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCICPTBAR_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsut_acu
    If isahe() or isalt()
      local oCIVTHEME
      oCIVTHEME=this.Parent.GetCtrl("w_CIVTHEME")
      If isahe()
        oCIVTHEME.RowSource=STRTRAN(oCIVTHEME.RowSource,',AHR 7',',AHE 8')
      Else
        oCIVTHEME.RowSource=STRTRAN(oCIVTHEME.RowSource,',AHR 7','')  
      Endif
      oCIVTHEME.Init()
      oCIVTHEME=.null.
    Endif
    * --- Fine Area Manuale
  endproc
    proc Init()
      this.w_XXB1IM = this.oPgFrm.Pages(8).oPag.XXB1IM
      this.w_XXB4IM = this.oPgFrm.Pages(8).oPag.XXB4IM
      this.w_XXB3IM = this.oPgFrm.Pages(8).oPag.XXB3IM
      this.w_XXB2IM = this.oPgFrm.Pages(8).oPag.XXB2IM
      this.w_XXB5IM = this.oPgFrm.Pages(8).oPag.XXB5IM
      this.w_XXB61IM = this.oPgFrm.Pages(8).oPag.XXB61IM
      this.w_XXB17IM = this.oPgFrm.Pages(8).oPag.XXB17IM
      this.w_XXB62IM = this.oPgFrm.Pages(8).oPag.XXB62IM
      this.w_XXB7IM = this.oPgFrm.Pages(8).oPag.XXB7IM
      this.w_XXB92IM = this.oPgFrm.Pages(8).oPag.XXB92IM
      this.w_XXB91IM = this.oPgFrm.Pages(8).oPag.XXB91IM
      this.w_XXB82IM = this.oPgFrm.Pages(8).oPag.XXB82IM
      this.w_XXB81IM = this.oPgFrm.Pages(8).oPag.XXB81IM
      this.w_XXB101IM = this.oPgFrm.Pages(8).oPag.XXB101IM
      this.w_XXB102IM = this.oPgFrm.Pages(8).oPag.XXB102IM
      this.w_XXB111IM = this.oPgFrm.Pages(8).oPag.XXB111IM
      this.w_XXB112IM = this.oPgFrm.Pages(8).oPag.XXB112IM
      this.w_XXB12IM = this.oPgFrm.Pages(8).oPag.XXB12IM
      this.w_XXB13IM = this.oPgFrm.Pages(8).oPag.XXB13IM
      this.w_XXB14IM = this.oPgFrm.Pages(8).oPag.XXB14IM
      this.w_XXB15IM = this.oPgFrm.Pages(8).oPag.XXB15IM
      this.w_XXB16IM = this.oPgFrm.Pages(8).oPag.XXB16IM
      DoDefault()
    proc Destroy()
      this.w_XXB1IM = .NULL.
      this.w_XXB4IM = .NULL.
      this.w_XXB3IM = .NULL.
      this.w_XXB2IM = .NULL.
      this.w_XXB5IM = .NULL.
      this.w_XXB61IM = .NULL.
      this.w_XXB17IM = .NULL.
      this.w_XXB62IM = .NULL.
      this.w_XXB7IM = .NULL.
      this.w_XXB92IM = .NULL.
      this.w_XXB91IM = .NULL.
      this.w_XXB82IM = .NULL.
      this.w_XXB81IM = .NULL.
      this.w_XXB101IM = .NULL.
      this.w_XXB102IM = .NULL.
      this.w_XXB111IM = .NULL.
      this.w_XXB112IM = .NULL.
      this.w_XXB12IM = .NULL.
      this.w_XXB13IM = .NULL.
      this.w_XXB14IM = .NULL.
      this.w_XXB15IM = .NULL.
      this.w_XXB16IM = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='CONF_INT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONF_INT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONF_INT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsut_acu'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONF_INT where CICODUTE=KeySet.CICODUTE
    *
    i_nConn = i_TableProp[this.CONF_INT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONF_INT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONF_INT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONF_INT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CICODUTE',this.w_CICODUTE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESUTE = space(20)
        .w_OLDNUMREC = g_nNumRecent
        .w_COLORCTOB = space(10)
        .w_COLORCTED = space(10)
        .w_COLORCTSEL = space(10)
        .w_COLORFCTDSBL = space(10)
        .w_COLORBCTDSBL = space(10)
        .w_COLORCTRI = space(10)
        .w_COLORGRD = space(10)
        .w_COLORSCREEN = space(10)
        .w_COLORROWDTL = space(10)
        .w_COLORZOM = space(10)
        .w_PRVNUM = space(12)
        .w_COLORATU = space(10)
        .w_COLORATT = space(10)
        .w_COLORATC = space(10)
        .w_COLORPRM = space(10)
        .w_COLORNNP = space(10)
        .w_COLORHDR = space(10)
        .w_COLORSEL = space(10)
        .w_COLORACF = space(10)
        .w_COLORPOSTIN = space(10)
        .w_XXB1 = space(1)
        .w_XXB2 = space(1)
        .w_XXB3 = space(1)
        .w_XXB4 = space(1)
        .w_XXB5 = space(1)
        .w_XXB17 = space(1)
        .w_XXB6 = space(1)
        .w_XXB7 = space(1)
        .w_XXB8 = space(1)
        .w_XXB9 = space(1)
        .w_XXB10 = space(1)
        .w_XXB11 = space(1)
        .w_XXB12 = space(1)
        .w_XXB13 = space(1)
        .w_XXB14 = space(1)
        .w_XXB15 = space(1)
        .w_XXB16 = space(1)
        .w_OSAVFRM = space(1)
        .w_OMDIFRM = .w_CIMDIFRM
        .w_CICPTBAR = NVL(CICPTBAR,space(1))
        .w_CIDSKBAR = NVL(CIDSKBAR,space(1))
        .w_CITOOBDS = NVL(CITOOBDS,space(1))
        .w_CITBSIZE = NVL(CITBSIZE,0)
        .w_CISTABAR = NVL(CISTABAR,space(1))
        .w_CISHWMEN = NVL(CISHWMEN,space(1))
        .w_CISTASCR = NVL(CISTASCR,space(1))
        .w_CIMENFIX = NVL(CIMENFIX,space(1))
        .w_CIWNDMEN = NVL(CIWNDMEN,space(1))
        .w_CISEARMN = NVL(CISEARMN,space(2))
        .w_XXSEARMN = IIF((.w_CISHWMEN = 'S' OR .w_CIDSKMEN <> 'H') And .w_CIVTHEME<>-1, .w_XXSEARMN, '')
        .w_XXSEARM1 = IIF(.w_XXSEARMN = 'S' AND .w_CISHWMEN = 'S' And .w_CIVTHEME<>-1, .w_XXSEARM1, ' ')
        .w_XXSEARM2 = IIF(.w_XXSEARMN = 'S' AND .w_CIDSKMEN <> 'H' And .w_CIVTHEME<>-1, .w_XXSEARM2, ' ')
        .w_CIFRMBUT = NVL(CIFRMBUT,space(1))
        .w_CIFRMPAG = NVL(CIFRMPAG,space(1))
        .w_CITOOLMN = NVL(CITOOLMN,space(1))
        .w_CIMENIMM = NVL(CIMENIMM,space(1))
        .w_CINEWPRI = NVL(CINEWPRI,space(1))
        .w_CIMONFRA = NVL(CIMONFRA,space(1))
        .w_CI_ILIKE = NVL(CI_ILIKE,space(1))
        .w_CIMINPST = NVL(CIMINPST,0)
        .w_CIMODPST = NVL(CIMODPST,0)
        .w_CICODUTE = NVL(CICODUTE,0)
        .w_CISELECL = NVL(CISELECL,0)
        .w_CIEDTCOL = NVL(CIEDTCOL,0)
        .w_CIOBLCOL = NVL(CIOBLCOL,0)
        .w_CIDSBLFC = NVL(CIDSBLFC,0)
        .w_CIDSBLBC = NVL(CIDSBLBC,0)
        .w_CIRICCOL = NVL(CIRICCOL,0)
        .oPgFrm.Page3.oPag.oObj_3_11.Calculate(.w_CIRICCOL)
        .oPgFrm.Page3.oPag.oObj_3_14.Calculate(.w_CIOBLCOL)
        .oPgFrm.Page3.oPag.oObj_3_18.Calculate(.w_CIEDTCOL)
        .oPgFrm.Page3.oPag.oObj_3_19.Calculate(.w_CISELECL)
        .oPgFrm.Page3.oPag.oObj_3_24.Calculate(.w_CIDSBLFC)
        .oPgFrm.Page3.oPag.oObj_3_25.Calculate(.w_CIDSBLBC)
        .w_CIEVIZOM = NVL(CIEVIZOM,0)
        .w_CISCRCOL = NVL(CISCRCOL,0)
        .oPgFrm.Page3.oPag.oObj_3_51.Calculate(.w_CISCRCOL)
        .w_CIDTLCLR = NVL(CIDTLCLR,0)
        .oPgFrm.Page3.oPag.oObj_3_53.Calculate(.w_CIDTLCLR)
        .w_CIGRIDCL = NVL(CIGRIDCL,0)
        .oPgFrm.Page3.oPag.oObj_3_55.Calculate(.w_CIGRIDCL)
        .w_CICOLZOM = NVL(CICOLZOM,0)
        .oPgFrm.Page3.oPag.oObj_3_61.Calculate(.w_CICOLZOM)
        .w_CILBLFNM = NVL(CILBLFNM,space(50))
        .w_CITXTFNM = NVL(CITXTFNM,space(50))
        .w_CICBXFNM = NVL(CICBXFNM,space(50))
        .w_CIBTNFNM = NVL(CIBTNFNM,space(50))
        .w_CIGRDFNM = NVL(CIGRDFNM,space(50))
        .w_CIPAGFNM = NVL(CIPAGFNM,space(50))
        .w_CILBLFSZ = NVL(CILBLFSZ,0)
        .w_CITXTFSZ = NVL(CITXTFSZ,0)
        .w_CICBXFSZ = NVL(CICBXFSZ,0)
        .w_CIBTNFSZ = NVL(CIBTNFSZ,0)
        .w_CIGRDFSZ = NVL(CIGRDFSZ,0)
        .w_CIPAGFSZ = NVL(CIPAGFSZ,0)
        .w_CISYSFRM = NVL(CISYSFRM,space(1))
        .w_PRVDATA = {^2005/12/31}
        .w_CISETCEN = NVL(CISETCEN,space(1))
        .w_CISETDAT = NVL(CISETDAT,space(1))
        .w_CISETPNT = NVL(CISETPNT,space(1))
        .w_CISETSEP = NVL(CISETSEP,space(1))
        .w_CISETMRK = NVL(CISETMRK,space(1))
        .w_SETDAT = .w_CISETDAT
        .w_SETMRK = .w_CISETMRK
        .w_SETCEN = .w_CISETCEN
        .w_SETPNT = .w_CISETPNT
        .w_SETSEP = .w_CISETSEP
        .w_CIATUCOL = NVL(CIATUCOL,0)
        .w_CIATTCOL = NVL(CIATTCOL,0)
        .w_CIATCCOL = NVL(CIATCCOL,0)
        .w_CICOLPRM = NVL(CICOLPRM,0)
        .w_CICOLNNP = NVL(CICOLNNP,0)
        .w_CICOLSEL = NVL(CICOLSEL,0)
        .w_CICOLHDR = NVL(CICOLHDR,0)
        .oPgFrm.Page6.oPag.oObj_6_18.Calculate(.w_CIATUCOL)
        .oPgFrm.Page6.oPag.oObj_6_23.Calculate(.w_CIATTCOL)
        .oPgFrm.Page6.oPag.oObj_6_24.Calculate(.w_CIATCCOL)
        .w_CIAGEVIS = NVL(CIAGEVIS,0)
        .oPgFrm.Page6.oPag.oObj_6_37.Calculate(.w_CICOLPRM)
        .oPgFrm.Page6.oPag.oObj_6_38.Calculate(.w_CICOLNNP)
        .oPgFrm.Page6.oPag.oObj_6_39.Calculate(.w_CICOLHDR)
        .oPgFrm.Page6.oPag.oObj_6_40.Calculate(.w_CICOLSEL)
        .w_CIORAINI = NVL(CIORAINI,0)
        .w_CIORAFIN = NVL(CIORAFIN,0)
        .w_CISTAINI = NVL(CISTAINI,0)
        .w_CISTAFIN = NVL(CISTAFIN,0)
        .w_CITYPBAL = NVL(CITYPBAL,space(1))
        .w_CICOLACF = NVL(CICOLACF,0)
        .oPgFrm.Page6.oPag.oObj_6_56.Calculate(.w_CICOLACF)
        .w_CIDSKMEN = NVL(CIDSKMEN,space(1))
        .w_CINAVSTA = NVL(CINAVSTA,space(1))
        .w_CINAVNUB = NVL(CINAVNUB,0)
        .w_CINAVDIM = NVL(CINAVDIM,0)
        .w_CIMNAFNM = NVL(CIMNAFNM,space(50))
        .w_CIMNAFSZ = NVL(CIMNAFSZ,0)
        .w_CIWMAFNM = NVL(CIWMAFNM,space(50))
        .w_CIWMAFSZ = NVL(CIWMAFSZ,0)
        .w_CIOPNGST = NVL(CIOPNGST,space(50))
        .oPgFrm.Page3.oPag.oObj_3_67.Calculate(.w_CICOLPST)
        .w_CIFLCOMP = NVL(CIFLCOMP,space(1))
        .w_CIDSKRSS = NVL(CIDSKRSS,space(1))
        .w_CILBLFIT = NVL(CILBLFIT,space(1))
        .w_CITXTFIT = NVL(CITXTFIT,space(1))
        .w_CICBXFIT = NVL(CICBXFIT,space(1))
        .w_CIBTNFIT = NVL(CIBTNFIT,space(1))
        .w_CIGRDFIT = NVL(CIGRDFIT,space(1))
        .w_CIPAGFIT = NVL(CIPAGFIT,space(1))
        .w_CILBLFBO = NVL(CILBLFBO,space(1))
        .w_CITXTFBO = NVL(CITXTFBO,space(1))
        .w_CICBXFBO = NVL(CICBXFBO,space(1))
        .w_CIBTNFBO = NVL(CIBTNFBO,space(1))
        .w_CIGRDFBO = NVL(CIGRDFBO,space(1))
        .w_CIPAGFBO = NVL(CIPAGFBO,space(1))
        .w_CIMDIFRM = NVL(CIMDIFRM,space(1))
        .w_CITABMEN = NVL(CITABMEN,space(1))
        .w_CIXPTHEM = NVL(CIXPTHEM,0)
        .w_CIRICONT = NVL(CIRICONT,space(1))
        .w_CIBCKGRD = NVL(CIBCKGRD,space(1))
        .w_CIPROBAR = NVL(CIPROBAR,space(1))
        .w_CI_COLON = NVL(CI_COLON,space(1))
        .w_CINOBTNI = NVL(CINOBTNI,space(1))
        .w_ciwaitwd = NVL(ciwaitwd,space(1))
        .w_CISHWBTN = NVL(CISHWBTN,space(1))
        .w_CIMRKGRD = NVL(CIMRKGRD,space(1))
        .w_CICTRGRD = NVL(CICTRGRD,space(1))
        .w_CIHEHEZO = NVL(CIHEHEZO,0)
        .w_CIHEROZO = NVL(CIHEROZO,0)
        .w_CIZOESPR = NVL(CIZOESPR,space(1))
        .w_CIDSPCNT = NVL(CIDSPCNT,0)
        .w_CICOLPST = NVL(CICOLPST,0)
        .w_CIWARTYP = NVL(CIWARTYP,space(1))
        .w_CIZOOMMO = NVL(CIZOOMMO,space(1))
        .w_CIAGSFNM = NVL(CIAGSFNM,space(50))
        .w_CIAGSFSZ = NVL(CIAGSFSZ,0)
        .w_CIAGSFIT = NVL(CIAGSFIT,space(1))
        .w_CIAGSFBO = NVL(CIAGSFBO,space(1))
        .w_CIAGIFNM = NVL(CIAGIFNM,space(50))
        .w_CIAGIFSZ = NVL(CIAGIFSZ,0)
        .w_CIAGIFIT = NVL(CIAGIFIT,space(1))
        .w_CIAGIFBO = NVL(CIAGIFBO,space(1))
        .w_CIGIORHO = NVL(CIGIORHO,0)
        .w_CIZOOMSC = NVL(CIZOOMSC,space(1))
        .w_CIDIMFRM = NVL(CIDIMFRM,space(1))
        .w_CIWIDFRM = NVL(CIWIDFRM,0)
        .w_CIHEIFRM = NVL(CIHEIFRM,0)
        .w_CISEARNT = NVL(CISEARNT,space(1))
        .w_CISEARNT = NVL(CISEARNT,space(1))
        .w_CISEAMNW = IIF(.w_CIWNDMEN='S' AND .w_CISEARNT='S', 1, 0)
        .w_CISEADKM = IIF(.w_CISEARNT='S', 1, 0)
        .w_CISEAAPB = IIF(.w_CISEARNT='S', 1, 0)
        .w_COPOSREC = NVL(COPOSREC,space(1))
        .w_CINUMREC = NVL(CINUMREC,0)
        .w_CINRECEN = NVL(CINRECEN,0)
        .w_CITYPCAL = NVL(CITYPCAL,space(1))
        .w_CISAVFRM = NVL(CISAVFRM,space(1))
        .w_CITBVWBT = NVL(CITBVWBT,space(20))
        .oPgFrm.Page8.oPag.XXB1IM.Calculate()
        .oPgFrm.Page8.oPag.XXB4IM.Calculate()
        .oPgFrm.Page8.oPag.XXB3IM.Calculate()
        .oPgFrm.Page8.oPag.XXB2IM.Calculate()
        .oPgFrm.Page8.oPag.XXB5IM.Calculate()
        .oPgFrm.Page8.oPag.XXB61IM.Calculate()
        .oPgFrm.Page8.oPag.XXB17IM.Calculate()
        .oPgFrm.Page8.oPag.XXB62IM.Calculate()
        .oPgFrm.Page8.oPag.XXB7IM.Calculate()
        .oPgFrm.Page8.oPag.XXB92IM.Calculate()
        .oPgFrm.Page8.oPag.XXB91IM.Calculate()
        .oPgFrm.Page8.oPag.XXB82IM.Calculate()
        .oPgFrm.Page8.oPag.XXB81IM.Calculate()
        .oPgFrm.Page8.oPag.XXB101IM.Calculate()
        .oPgFrm.Page8.oPag.XXB102IM.Calculate()
        .oPgFrm.Page8.oPag.XXB111IM.Calculate()
        .oPgFrm.Page8.oPag.XXB112IM.Calculate()
        .oPgFrm.Page8.oPag.XXB12IM.Calculate()
        .oPgFrm.Page8.oPag.XXB13IM.Calculate()
        .oPgFrm.Page8.oPag.XXB14IM.Calculate()
        .oPgFrm.Page8.oPag.XXB15IM.Calculate()
        .oPgFrm.Page8.oPag.XXB16IM.Calculate()
        .w_CIVTHEME = NVL(CIVTHEME,0)
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .w_CIGADGET = NVL(CIGADGET,space(1))
        cp_LoadRecExtFlds(this,'CONF_INT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_CICPTBAR = space(1)
      .w_CIDSKBAR = space(1)
      .w_CITOOBDS = space(1)
      .w_CITBSIZE = 0
      .w_CISTABAR = space(1)
      .w_CISHWMEN = space(1)
      .w_CISTASCR = space(1)
      .w_CIMENFIX = space(1)
      .w_CIWNDMEN = space(1)
      .w_CISEARMN = space(2)
      .w_XXSEARMN = space(1)
      .w_XXSEARM1 = space(1)
      .w_XXSEARM2 = space(1)
      .w_CIFRMBUT = space(1)
      .w_CIFRMPAG = space(1)
      .w_CITOOLMN = space(1)
      .w_CIMENIMM = space(1)
      .w_CINEWPRI = space(1)
      .w_CIMONFRA = space(1)
      .w_CI_ILIKE = space(1)
      .w_CIMINPST = 0
      .w_CIMODPST = 0
      .w_CICODUTE = 0
      .w_DESUTE = space(20)
      .w_OLDNUMREC = space(10)
      .w_CISELECL = 0
      .w_CIEDTCOL = 0
      .w_CIOBLCOL = 0
      .w_CIDSBLFC = 0
      .w_CIDSBLBC = 0
      .w_CIRICCOL = 0
      .w_COLORCTOB = space(10)
      .w_COLORCTED = space(10)
      .w_COLORCTSEL = space(10)
      .w_COLORFCTDSBL = space(10)
      .w_COLORBCTDSBL = space(10)
      .w_COLORCTRI = space(10)
      .w_CIEVIZOM = 0
      .w_COLORGRD = space(10)
      .w_COLORSCREEN = space(10)
      .w_COLORROWDTL = space(10)
      .w_CISCRCOL = 0
      .w_CIDTLCLR = 0
      .w_CIGRIDCL = 0
      .w_COLORZOM = space(10)
      .w_CICOLZOM = 0
      .w_CILBLFNM = space(50)
      .w_CITXTFNM = space(50)
      .w_CICBXFNM = space(50)
      .w_CIBTNFNM = space(50)
      .w_CIGRDFNM = space(50)
      .w_CIPAGFNM = space(50)
      .w_CILBLFSZ = 0
      .w_CITXTFSZ = 0
      .w_CICBXFSZ = 0
      .w_CIBTNFSZ = 0
      .w_CIGRDFSZ = 0
      .w_CIPAGFSZ = 0
      .w_CISYSFRM = space(1)
      .w_PRVNUM = space(12)
      .w_PRVDATA = ctod("  /  /  ")
      .w_CISETCEN = space(1)
      .w_CISETDAT = space(1)
      .w_CISETPNT = space(1)
      .w_CISETSEP = space(1)
      .w_CISETMRK = space(1)
      .w_SETDAT = space(1)
      .w_SETMRK = space(1)
      .w_SETCEN = space(1)
      .w_SETPNT = space(1)
      .w_SETSEP = space(1)
      .w_CIATUCOL = 0
      .w_CIATTCOL = 0
      .w_CIATCCOL = 0
      .w_CICOLPRM = 0
      .w_CICOLNNP = 0
      .w_CICOLSEL = 0
      .w_CICOLHDR = 0
      .w_COLORATU = space(10)
      .w_COLORATT = space(10)
      .w_COLORATC = space(10)
      .w_CIAGEVIS = 0
      .w_COLORPRM = space(10)
      .w_COLORNNP = space(10)
      .w_COLORHDR = space(10)
      .w_COLORSEL = space(10)
      .w_CIORAINI = 0
      .w_CIORAFIN = 0
      .w_CISTAINI = 0
      .w_CISTAFIN = 0
      .w_CITYPBAL = space(1)
      .w_COLORACF = space(10)
      .w_CICOLACF = 0
      .w_CIDSKMEN = space(1)
      .w_CINAVSTA = space(1)
      .w_CINAVNUB = 0
      .w_CINAVDIM = 0
      .w_CIMNAFNM = space(50)
      .w_CIMNAFSZ = 0
      .w_CIWMAFNM = space(50)
      .w_CIWMAFSZ = 0
      .w_CIOPNGST = space(50)
      .w_COLORPOSTIN = space(10)
      .w_CIFLCOMP = space(1)
      .w_CIDSKRSS = space(1)
      .w_CILBLFIT = space(1)
      .w_CITXTFIT = space(1)
      .w_CICBXFIT = space(1)
      .w_CIBTNFIT = space(1)
      .w_CIGRDFIT = space(1)
      .w_CIPAGFIT = space(1)
      .w_CILBLFBO = space(1)
      .w_CITXTFBO = space(1)
      .w_CICBXFBO = space(1)
      .w_CIBTNFBO = space(1)
      .w_CIGRDFBO = space(1)
      .w_CIPAGFBO = space(1)
      .w_CIMDIFRM = space(1)
      .w_CITABMEN = space(1)
      .w_CIXPTHEM = 0
      .w_CIRICONT = space(1)
      .w_CIBCKGRD = space(1)
      .w_CIPROBAR = space(1)
      .w_CI_COLON = space(1)
      .w_CINOBTNI = space(1)
      .w_ciwaitwd = space(1)
      .w_CISHWBTN = space(1)
      .w_CIMRKGRD = space(1)
      .w_CICTRGRD = space(1)
      .w_CIHEHEZO = 0
      .w_CIHEROZO = 0
      .w_CIZOESPR = space(1)
      .w_CIDSPCNT = 0
      .w_CICOLPST = 0
      .w_CIWARTYP = space(1)
      .w_CIZOOMMO = space(1)
      .w_CIAGSFNM = space(50)
      .w_CIAGSFSZ = 0
      .w_CIAGSFIT = space(1)
      .w_CIAGSFBO = space(1)
      .w_CIAGIFNM = space(50)
      .w_CIAGIFSZ = 0
      .w_CIAGIFIT = space(1)
      .w_CIAGIFBO = space(1)
      .w_CIGIORHO = 0
      .w_CIZOOMSC = space(1)
      .w_CIDIMFRM = space(1)
      .w_CIWIDFRM = 0
      .w_CIHEIFRM = 0
      .w_CISEARNT = space(1)
      .w_CISEARNT = space(1)
      .w_CISEAMNW = 0
      .w_CISEADKM = 0
      .w_CISEAAPB = 0
      .w_COPOSREC = space(1)
      .w_CINUMREC = 0
      .w_CINRECEN = 0
      .w_CITYPCAL = space(1)
      .w_CISAVFRM = space(1)
      .w_XXB1 = space(1)
      .w_XXB2 = space(1)
      .w_XXB3 = space(1)
      .w_XXB4 = space(1)
      .w_XXB5 = space(1)
      .w_XXB17 = space(1)
      .w_XXB6 = space(1)
      .w_XXB7 = space(1)
      .w_XXB8 = space(1)
      .w_XXB9 = space(1)
      .w_XXB10 = space(1)
      .w_XXB11 = space(1)
      .w_XXB12 = space(1)
      .w_XXB13 = space(1)
      .w_XXB14 = space(1)
      .w_XXB15 = space(1)
      .w_XXB16 = space(1)
      .w_CITBVWBT = space(20)
      .w_CIVTHEME = 0
      .w_CIGADGET = space(1)
      .w_OSAVFRM = space(1)
      .w_OMDIFRM = space(1)
      if .cFunction<>"Filter"
        .w_CICPTBAR = 'S'
        .w_CIDSKBAR = 'S'
        .w_CITOOBDS = ' '
        .w_CITBSIZE = 32
        .w_CISTABAR = 'N'
        .w_CISHWMEN = 'S'
          .DoRTCalc(7,8,.f.)
        .w_CIWNDMEN = IIF(.w_CISHWMEN='S', .w_CIWNDMEN, '')
          .DoRTCalc(10,10,.f.)
        .w_XXSEARMN = IIF((.w_CISHWMEN = 'S' OR .w_CIDSKMEN <> 'H') And .w_CIVTHEME<>-1, .w_XXSEARMN, '')
        .w_XXSEARM1 = IIF(.w_XXSEARMN = 'S' AND .w_CISHWMEN = 'S' And .w_CIVTHEME<>-1, .w_XXSEARM1, ' ')
        .w_XXSEARM2 = IIF(.w_XXSEARMN = 'S' AND .w_CIDSKMEN <> 'H' And .w_CIVTHEME<>-1, .w_XXSEARM2, ' ')
          .DoRTCalc(14,14,.f.)
        .w_CIFRMPAG = iif(.w_CIFRMBUT='S',.w_CIFRMPAG,'N')
          .DoRTCalc(16,16,.f.)
        .w_CIMENIMM = 'N'
        .w_CINEWPRI = iif(i_bNewPrintSystem, 'S', ' ')
        .w_CIMONFRA = iif(i_MonitorFramework, 'S', ' ')
        .w_CI_ILIKE = iif(CP_DBTYPE='PostgreSQL', 'S', 'N')
        .w_CIMINPST = 10
        .w_CIMODPST = 1
          .DoRTCalc(23,24,.f.)
        .w_OLDNUMREC = g_nNumRecent
        .oPgFrm.Page3.oPag.oObj_3_11.Calculate(.w_CIRICCOL)
        .oPgFrm.Page3.oPag.oObj_3_14.Calculate(.w_CIOBLCOL)
        .oPgFrm.Page3.oPag.oObj_3_18.Calculate(.w_CIEDTCOL)
        .oPgFrm.Page3.oPag.oObj_3_19.Calculate(.w_CISELECL)
        .oPgFrm.Page3.oPag.oObj_3_24.Calculate(.w_CIDSBLFC)
        .oPgFrm.Page3.oPag.oObj_3_25.Calculate(.w_CIDSBLBC)
          .DoRTCalc(26,37,.f.)
        .w_CIEVIZOM = 0
        .oPgFrm.Page3.oPag.oObj_3_51.Calculate(.w_CISCRCOL)
        .oPgFrm.Page3.oPag.oObj_3_53.Calculate(.w_CIDTLCLR)
        .oPgFrm.Page3.oPag.oObj_3_55.Calculate(.w_CIGRIDCL)
        .oPgFrm.Page3.oPag.oObj_3_61.Calculate(.w_CICOLZOM)
          .DoRTCalc(39,52,.f.)
        .w_CILBLFSZ = 9
        .w_CITXTFSZ = 9
        .w_CICBXFSZ = 9
        .w_CIBTNFSZ = 9
        .w_CIGRDFSZ = 9
        .w_CIPAGFSZ = 9
          .DoRTCalc(59,60,.f.)
        .w_PRVDATA = {^2005/12/31}
        .w_CISETCEN = .w_SETCEN
        .w_CISETDAT = .w_SETDAT
        .w_CISETPNT = .w_SETPNT
        .w_CISETSEP = .w_SETSEP
        .w_CISETMRK = .w_SETMRK
        .w_SETDAT = .w_CISETDAT
        .w_SETMRK = .w_CISETMRK
        .w_SETCEN = .w_CISETCEN
        .w_SETPNT = .w_CISETPNT
        .w_SETSEP = .w_CISETSEP
          .DoRTCalc(72,77,.f.)
        .w_CICOLHDR = iif(.w_CIVTHEME=-1,Rgb(181, 213, 255),.w_CICOLHDR)
        .oPgFrm.Page6.oPag.oObj_6_18.Calculate(.w_CIATUCOL)
        .oPgFrm.Page6.oPag.oObj_6_23.Calculate(.w_CIATTCOL)
        .oPgFrm.Page6.oPag.oObj_6_24.Calculate(.w_CIATCCOL)
          .DoRTCalc(79,81,.f.)
        .w_CIAGEVIS = 1
        .oPgFrm.Page6.oPag.oObj_6_37.Calculate(.w_CICOLPRM)
        .oPgFrm.Page6.oPag.oObj_6_38.Calculate(.w_CICOLNNP)
        .oPgFrm.Page6.oPag.oObj_6_39.Calculate(.w_CICOLHDR)
        .oPgFrm.Page6.oPag.oObj_6_40.Calculate(.w_CICOLSEL)
          .DoRTCalc(83,86,.f.)
        .w_CIORAINI = 8
        .w_CIORAFIN = 20
        .w_CISTAINI = 8
        .w_CISTAFIN = 20
        .w_CITYPBAL = 'S'
        .oPgFrm.Page6.oPag.oObj_6_56.Calculate(.w_CICOLACF)
          .DoRTCalc(92,93,.f.)
        .w_CIDSKMEN = IIF( .w_CIGADGET='S', 'H', .w_CIDSKMEN)
          .DoRTCalc(95,95,.f.)
        .w_CINAVNUB = 7
        .w_CINAVDIM = 258
          .DoRTCalc(98,98,.f.)
        .w_CIMNAFSZ = 9
          .DoRTCalc(100,100,.f.)
        .w_CIWMAFSZ = 9
        .oPgFrm.Page3.oPag.oObj_3_67.Calculate(.w_CICOLPST)
          .DoRTCalc(102,103,.f.)
        .w_CIFLCOMP = 'S'
        .w_CIDSKRSS = 'S'
        .w_CILBLFIT = 'N'
        .w_CITXTFIT = 'N'
        .w_CICBXFIT = 'N'
        .w_CIBTNFIT = 'N'
        .w_CIGRDFIT = 'N'
        .w_CIPAGFIT = 'N'
        .w_CILBLFBO = 'N'
        .w_CITXTFBO = 'N'
        .w_CICBXFBO = 'N'
        .w_CIBTNFBO = 'N'
        .w_CIGRDFBO = 'N'
        .w_CIPAGFBO = 'N'
          .DoRTCalc(118,118,.f.)
        .w_CITABMEN = 'T'
          .DoRTCalc(120,121,.f.)
        .w_CIBCKGRD = iif(.w_CIVTHEME=-1, ' ', 'S')
          .DoRTCalc(123,124,.f.)
        .w_CINOBTNI = ' '
        .w_ciwaitwd = 'N'
        .w_CISHWBTN = 'S'
        .w_CIMRKGRD = IIF(IsAhe(), 'S', 'N')
        .w_CICTRGRD = iif(.w_CIVTHEME=-1, ' ', 'S')
        .w_CIHEHEZO = 19
        .w_CIHEROZO = 19
        .w_CIZOESPR = iif(.w_CICTRGRD='S', 'N', 'S')
        .w_CIDSPCNT = 20
          .DoRTCalc(134,134,.f.)
        .w_CIWARTYP = 'S'
        .w_CIZOOMMO = 'S'
          .DoRTCalc(137,137,.f.)
        .w_CIAGSFSZ = 8
        .w_CIAGSFIT = 'N'
        .w_CIAGSFBO = 'N'
          .DoRTCalc(141,141,.f.)
        .w_CIAGIFSZ = 8
        .w_CIAGIFIT = 'N'
        .w_CIAGIFBO = 'S'
        .w_CIGIORHO = 0
        .w_CIZOOMSC = 'N'
        .w_CIDIMFRM = IIF(.w_CIMDIFRM $ 'I-M', 'S', IIF(EMPTY(.w_CIDIMFRM), 'S' , .w_CIDIMFRM))
        .w_CIWIDFRM = iif(.w_CIDIMFRM='S', 0, .w_CIWIDFRM)
        .w_CIHEIFRM = iif(.w_CIDIMFRM='S', 0, .w_CIHEIFRM)
          .DoRTCalc(150,151,.f.)
        .w_CISEAMNW = IIF(.w_CIWNDMEN='S' AND .w_CISEARNT='S', 1, 0)
        .w_CISEADKM = IIF(.w_CISEARNT='S', 1, 0)
        .w_CISEAAPB = IIF(.w_CISEARNT='S', 1, 0)
        .w_COPOSREC = iif(.w_CISEARNT="S", alltrim(str(.w_CISEAMNW + .w_CISEADKM + .w_CISEAAPB,1,0)), "0")
        .w_CINUMREC = iif(.w_CISEARNT="S", .w_CINUMREC, 0)
        .w_CINRECEN = 10
        .w_CITYPCAL = 'S'
        .w_CISAVFRM = ' '
        .oPgFrm.Page8.oPag.XXB1IM.Calculate()
        .oPgFrm.Page8.oPag.XXB4IM.Calculate()
        .oPgFrm.Page8.oPag.XXB3IM.Calculate()
        .oPgFrm.Page8.oPag.XXB2IM.Calculate()
        .oPgFrm.Page8.oPag.XXB5IM.Calculate()
        .oPgFrm.Page8.oPag.XXB61IM.Calculate()
        .oPgFrm.Page8.oPag.XXB17IM.Calculate()
        .oPgFrm.Page8.oPag.XXB62IM.Calculate()
        .oPgFrm.Page8.oPag.XXB7IM.Calculate()
        .oPgFrm.Page8.oPag.XXB92IM.Calculate()
        .oPgFrm.Page8.oPag.XXB91IM.Calculate()
        .oPgFrm.Page8.oPag.XXB82IM.Calculate()
        .oPgFrm.Page8.oPag.XXB81IM.Calculate()
        .oPgFrm.Page8.oPag.XXB101IM.Calculate()
        .oPgFrm.Page8.oPag.XXB102IM.Calculate()
        .oPgFrm.Page8.oPag.XXB111IM.Calculate()
        .oPgFrm.Page8.oPag.XXB112IM.Calculate()
        .oPgFrm.Page8.oPag.XXB12IM.Calculate()
        .oPgFrm.Page8.oPag.XXB13IM.Calculate()
        .oPgFrm.Page8.oPag.XXB14IM.Calculate()
        .oPgFrm.Page8.oPag.XXB15IM.Calculate()
        .oPgFrm.Page8.oPag.XXB16IM.Calculate()
          .DoRTCalc(160,177,.f.)
        .w_CIVTHEME = 7
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .w_CIGADGET = IIF( .w_CIVTHEME <> -1, .w_CIGADGET, 'N')
          .DoRTCalc(180,180,.f.)
        .w_OMDIFRM = .w_CIMDIFRM
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONF_INT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCICPTBAR_1_1.enabled = i_bVal
      .Page1.oPag.oCIDSKBAR_1_2.enabled = i_bVal
      .Page1.oPag.oCITOOBDS_1_3.enabled = i_bVal
      .Page1.oPag.oCITBSIZE_1_4.enabled = i_bVal
      .Page1.oPag.oCISTABAR_1_5.enabled = i_bVal
      .Page1.oPag.oCISHWMEN_1_6.enabled = i_bVal
      .Page1.oPag.oCIMENFIX_1_8.enabled = i_bVal
      .Page1.oPag.oCIWNDMEN_1_9.enabled = i_bVal
      .Page1.oPag.oXXSEARMN_1_12.enabled = i_bVal
      .Page1.oPag.oXXSEARM1_1_13.enabled = i_bVal
      .Page1.oPag.oXXSEARM2_1_14.enabled = i_bVal
      .Page1.oPag.oCIFRMBUT_1_15.enabled = i_bVal
      .Page1.oPag.oCIFRMPAG_1_16.enabled = i_bVal
      .Page1.oPag.oCITOOLMN_1_17.enabled = i_bVal
      .Page1.oPag.oCIMENIMM_1_18.enabled = i_bVal
      .Page1.oPag.oCINEWPRI_1_19.enabled = i_bVal
      .Page1.oPag.oCIMONFRA_1_20.enabled = i_bVal
      .Page1.oPag.oCI_ILIKE_1_21.enabled = i_bVal
      .Page1.oPag.oCIMINPST_1_22.enabled = i_bVal
      .Page1.oPag.oCIMODPST_1_23.enabled = i_bVal
      .Page3.oPag.oCIEVIZOM_3_37.enabled = i_bVal
      .Page4.oPag.oCILBLFNM_4_1.enabled = i_bVal
      .Page4.oPag.oCITXTFNM_4_2.enabled = i_bVal
      .Page4.oPag.oCICBXFNM_4_3.enabled = i_bVal
      .Page4.oPag.oCIBTNFNM_4_4.enabled = i_bVal
      .Page4.oPag.oCIGRDFNM_4_5.enabled = i_bVal
      .Page4.oPag.oCIPAGFNM_4_6.enabled = i_bVal
      .Page4.oPag.oCILBLFSZ_4_13.enabled = i_bVal
      .Page4.oPag.oCITXTFSZ_4_15.enabled = i_bVal
      .Page4.oPag.oCICBXFSZ_4_17.enabled = i_bVal
      .Page4.oPag.oCIBTNFSZ_4_19.enabled = i_bVal
      .Page4.oPag.oCIGRDFSZ_4_21.enabled = i_bVal
      .Page4.oPag.oCIPAGFSZ_4_23.enabled = i_bVal
      .Page5.oPag.oCISYSFRM_5_1.enabled = i_bVal
      .Page5.oPag.oSETDAT_5_18.enabled = i_bVal
      .Page5.oPag.oSETMRK_5_19.enabled = i_bVal
      .Page5.oPag.oSETCEN_5_20.enabled = i_bVal
      .Page5.oPag.oSETPNT_5_21.enabled = i_bVal
      .Page5.oPag.oSETSEP_5_22.enabled = i_bVal
      .Page6.oPag.oCIAGEVIS_6_25.enabled_(i_bVal)
      .Page6.oPag.oCIORAINI_6_44.enabled = i_bVal
      .Page6.oPag.oCIORAFIN_6_45.enabled = i_bVal
      .Page6.oPag.oCISTAINI_6_46.enabled = i_bVal
      .Page6.oPag.oCISTAFIN_6_47.enabled = i_bVal
      .Page6.oPag.oCITYPBAL_6_48.enabled = i_bVal
      .Page7.oPag.oCIDSKMEN_7_1.enabled = i_bVal
      .Page7.oPag.oCINAVSTA_7_2.enabled = i_bVal
      .Page7.oPag.oCINAVNUB_7_3.enabled = i_bVal
      .Page7.oPag.oCINAVDIM_7_4.enabled = i_bVal
      .Page7.oPag.oCIMNAFNM_7_5.enabled = i_bVal
      .Page7.oPag.oCIMNAFSZ_7_6.enabled = i_bVal
      .Page7.oPag.oCIWMAFNM_7_7.enabled = i_bVal
      .Page7.oPag.oCIWMAFSZ_7_8.enabled = i_bVal
      .Page7.oPag.oCIOPNGST_7_17.enabled = i_bVal
      .Page6.oPag.oCIFLCOMP_6_59.enabled_(i_bVal)
      .Page7.oPag.oCIDSKRSS_7_19.enabled = i_bVal
      .Page4.oPag.oCILBLFIT_4_25.enabled = i_bVal
      .Page4.oPag.oCITXTFIT_4_26.enabled = i_bVal
      .Page4.oPag.oCICBXFIT_4_27.enabled = i_bVal
      .Page4.oPag.oCIBTNFIT_4_28.enabled = i_bVal
      .Page4.oPag.oCIGRDFIT_4_29.enabled = i_bVal
      .Page4.oPag.oCIPAGFIT_4_30.enabled = i_bVal
      .Page4.oPag.oCILBLFBO_4_31.enabled = i_bVal
      .Page4.oPag.oCITXTFBO_4_32.enabled = i_bVal
      .Page4.oPag.oCICBXFBO_4_33.enabled = i_bVal
      .Page4.oPag.oCIBTNFBO_4_34.enabled = i_bVal
      .Page4.oPag.oCIGRDFBO_4_35.enabled = i_bVal
      .Page4.oPag.oCIPAGFBO_4_36.enabled = i_bVal
      .Page2.oPag.oCIMDIFRM_2_1.enabled = i_bVal
      .Page2.oPag.oCITABMEN_2_2.enabled = i_bVal
      .Page2.oPag.oCIXPTHEM_2_3.enabled = i_bVal
      .Page2.oPag.oCIRICONT_2_4.enabled = i_bVal
      .Page2.oPag.oCIBCKGRD_2_5.enabled = i_bVal
      .Page2.oPag.oCIPROBAR_2_6.enabled = i_bVal
      .Page2.oPag.oCI_COLON_2_7.enabled = i_bVal
      .Page2.oPag.oCINOBTNI_2_8.enabled = i_bVal
      .Page2.oPag.ociwaitwd_2_9.enabled = i_bVal
      .Page2.oPag.oCISHWBTN_2_10.enabled = i_bVal
      .Page2.oPag.oCIMRKGRD_2_11.enabled = i_bVal
      .Page2.oPag.oCICTRGRD_2_12.enabled = i_bVal
      .Page2.oPag.oCIHEHEZO_2_13.enabled = i_bVal
      .Page2.oPag.oCIHEROZO_2_14.enabled = i_bVal
      .Page2.oPag.oCIZOESPR_2_15.enabled = i_bVal
      .Page2.oPag.oCIDSPCNT_2_16.enabled = i_bVal
      .Page2.oPag.oCIWARTYP_2_35.enabled = i_bVal
      .Page2.oPag.oCIZOOMMO_2_37.enabled = i_bVal
      .Page6.oPag.oCIAGSFNM_6_60.enabled = i_bVal
      .Page6.oPag.oCIAGSFSZ_6_62.enabled = i_bVal
      .Page6.oPag.oCIAGSFIT_6_64.enabled = i_bVal
      .Page6.oPag.oCIAGSFBO_6_65.enabled = i_bVal
      .Page6.oPag.oCIAGIFNM_6_68.enabled = i_bVal
      .Page6.oPag.oCIAGIFSZ_6_70.enabled = i_bVal
      .Page6.oPag.oCIAGIFIT_6_72.enabled = i_bVal
      .Page6.oPag.oCIAGIFBO_6_73.enabled = i_bVal
      .Page6.oPag.oCIGIORHO_6_74.enabled = i_bVal
      .Page2.oPag.oCIZOOMSC_2_39.enabled = i_bVal
      .Page2.oPag.oCIDIMFRM_2_42.enabled = i_bVal
      .Page2.oPag.oCIWIDFRM_2_44.enabled = i_bVal
      .Page2.oPag.oCIHEIFRM_2_45.enabled = i_bVal
      .Page1.oPag.oCISEARNT_1_45.enabled = i_bVal
      .Page1.oPag.oCISEARNT_1_46.enabled = i_bVal
      .Page1.oPag.oCISEAMNW_1_47.enabled = i_bVal
      .Page1.oPag.oCISEADKM_1_48.enabled = i_bVal
      .Page1.oPag.oCISEAAPB_1_49.enabled = i_bVal
      .Page1.oPag.oCINUMREC_1_51.enabled = i_bVal
      .Page1.oPag.oCINRECEN_1_52.enabled = i_bVal
      .Page2.oPag.oCITYPCAL_2_49.enabled = i_bVal
      .Page2.oPag.oCISAVFRM_2_51.enabled = i_bVal
      .Page8.oPag.oXXB1_8_2.enabled = i_bVal
      .Page8.oPag.oXXB2_8_3.enabled = i_bVal
      .Page8.oPag.oXXB3_8_4.enabled = i_bVal
      .Page8.oPag.oXXB4_8_5.enabled = i_bVal
      .Page8.oPag.oXXB5_8_6.enabled = i_bVal
      .Page8.oPag.oXXB17_8_7.enabled = i_bVal
      .Page8.oPag.oXXB6_8_8.enabled = i_bVal
      .Page8.oPag.oXXB7_8_9.enabled = i_bVal
      .Page8.oPag.oXXB8_8_10.enabled = i_bVal
      .Page8.oPag.oXXB9_8_11.enabled = i_bVal
      .Page8.oPag.oXXB10_8_12.enabled = i_bVal
      .Page8.oPag.oXXB11_8_13.enabled = i_bVal
      .Page8.oPag.oXXB12_8_14.enabled = i_bVal
      .Page8.oPag.oXXB13_8_15.enabled = i_bVal
      .Page8.oPag.oXXB14_8_16.enabled = i_bVal
      .Page8.oPag.oXXB15_8_17.enabled = i_bVal
      .Page8.oPag.oXXB16_8_18.enabled = i_bVal
      .Page1.oPag.oCIVTHEME_1_58.enabled = i_bVal
      .Page1.oPag.oCIGADGET_1_61.enabled = i_bVal
      .Page3.oPag.oBtn_3_7.enabled = i_bVal
      .Page3.oPag.oBtn_3_8.enabled = i_bVal
      .Page3.oPag.oBtn_3_9.enabled = i_bVal
      .Page3.oPag.oBtn_3_10.enabled = i_bVal
      .Page3.oPag.oBtn_3_12.enabled = i_bVal
      .Page3.oPag.oBtn_3_13.enabled = i_bVal
      .Page3.oPag.oBtn_3_35.enabled = i_bVal
      .Page3.oPag.oBtn_3_36.enabled = i_bVal
      .Page3.oPag.oBtn_3_38.enabled = i_bVal
      .Page3.oPag.oBtn_3_47.enabled = i_bVal
      .Page6.oPag.oBtn_6_8.enabled = i_bVal
      .Page6.oPag.oBtn_6_9.enabled = i_bVal
      .Page6.oPag.oBtn_6_10.enabled = i_bVal
      .Page6.oPag.oBtn_6_11.enabled = i_bVal
      .Page6.oPag.oBtn_6_12.enabled = i_bVal
      .Page6.oPag.oBtn_6_13.enabled = i_bVal
      .Page6.oPag.oBtn_6_14.enabled = i_bVal
      .Page6.oPag.oBtn_6_15.enabled = i_bVal
      .Page3.oPag.oBtn_3_64.enabled = i_bVal
      .Page3.oPag.oObj_3_11.enabled = i_bVal
      .Page3.oPag.oObj_3_14.enabled = i_bVal
      .Page3.oPag.oObj_3_18.enabled = i_bVal
      .Page3.oPag.oObj_3_19.enabled = i_bVal
      .Page3.oPag.oObj_3_24.enabled = i_bVal
      .Page3.oPag.oObj_3_25.enabled = i_bVal
      .Page3.oPag.oObj_3_51.enabled = i_bVal
      .Page3.oPag.oObj_3_53.enabled = i_bVal
      .Page3.oPag.oObj_3_55.enabled = i_bVal
      .Page3.oPag.oObj_3_61.enabled = i_bVal
      .Page6.oPag.oObj_6_18.enabled = i_bVal
      .Page6.oPag.oObj_6_23.enabled = i_bVal
      .Page6.oPag.oObj_6_24.enabled = i_bVal
      .Page6.oPag.oObj_6_37.enabled = i_bVal
      .Page6.oPag.oObj_6_38.enabled = i_bVal
      .Page6.oPag.oObj_6_39.enabled = i_bVal
      .Page6.oPag.oObj_6_40.enabled = i_bVal
      .Page6.oPag.oObj_6_56.enabled = i_bVal
      .Page3.oPag.oObj_3_67.enabled = i_bVal
      .Page8.oPag.XXB1IM.enabled = i_bVal
      .Page8.oPag.XXB4IM.enabled = i_bVal
      .Page8.oPag.XXB3IM.enabled = i_bVal
      .Page8.oPag.XXB2IM.enabled = i_bVal
      .Page8.oPag.XXB5IM.enabled = i_bVal
      .Page8.oPag.XXB61IM.enabled = i_bVal
      .Page8.oPag.XXB17IM.enabled = i_bVal
      .Page8.oPag.XXB62IM.enabled = i_bVal
      .Page8.oPag.XXB7IM.enabled = i_bVal
      .Page8.oPag.XXB92IM.enabled = i_bVal
      .Page8.oPag.XXB91IM.enabled = i_bVal
      .Page8.oPag.XXB82IM.enabled = i_bVal
      .Page8.oPag.XXB81IM.enabled = i_bVal
      .Page8.oPag.XXB101IM.enabled = i_bVal
      .Page8.oPag.XXB102IM.enabled = i_bVal
      .Page8.oPag.XXB111IM.enabled = i_bVal
      .Page8.oPag.XXB112IM.enabled = i_bVal
      .Page8.oPag.XXB12IM.enabled = i_bVal
      .Page8.oPag.XXB13IM.enabled = i_bVal
      .Page8.oPag.XXB14IM.enabled = i_bVal
      .Page8.oPag.XXB15IM.enabled = i_bVal
      .Page8.oPag.XXB16IM.enabled = i_bVal
      .Page1.oPag.oObj_1_59.enabled = i_bVal
      .Page1.oPag.oObj_1_60.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CONF_INT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONF_INT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICPTBAR,"CICPTBAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDSKBAR,"CIDSKBAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITOOBDS,"CITOOBDS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITBSIZE,"CITBSIZE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISTABAR,"CISTABAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISHWMEN,"CISHWMEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISTASCR,"CISTASCR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMENFIX,"CIMENFIX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIWNDMEN,"CIWNDMEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISEARMN,"CISEARMN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIFRMBUT,"CIFRMBUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIFRMPAG,"CIFRMPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITOOLMN,"CITOOLMN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMENIMM,"CIMENIMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CINEWPRI,"CINEWPRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMONFRA,"CIMONFRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CI_ILIKE,"CI_ILIKE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMINPST,"CIMINPST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMODPST,"CIMODPST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICODUTE,"CICODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISELECL,"CISELECL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIEDTCOL,"CIEDTCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIOBLCOL,"CIOBLCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDSBLFC,"CIDSBLFC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDSBLBC,"CIDSBLBC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIRICCOL,"CIRICCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIEVIZOM,"CIEVIZOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISCRCOL,"CISCRCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDTLCLR,"CIDTLCLR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIGRIDCL,"CIGRIDCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICOLZOM,"CICOLZOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CILBLFNM,"CILBLFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITXTFNM,"CITXTFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICBXFNM,"CICBXFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIBTNFNM,"CIBTNFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIGRDFNM,"CIGRDFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPAGFNM,"CIPAGFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CILBLFSZ,"CILBLFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITXTFSZ,"CITXTFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICBXFSZ,"CICBXFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIBTNFSZ,"CIBTNFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIGRDFSZ,"CIGRDFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPAGFSZ,"CIPAGFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISYSFRM,"CISYSFRM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISETCEN,"CISETCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISETDAT,"CISETDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISETPNT,"CISETPNT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISETSEP,"CISETSEP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISETMRK,"CISETMRK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIATUCOL,"CIATUCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIATTCOL,"CIATTCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIATCCOL,"CIATCCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICOLPRM,"CICOLPRM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICOLNNP,"CICOLNNP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICOLSEL,"CICOLSEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICOLHDR,"CICOLHDR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIAGEVIS,"CIAGEVIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIORAINI,"CIORAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIORAFIN,"CIORAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISTAINI,"CISTAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISTAFIN,"CISTAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITYPBAL,"CITYPBAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICOLACF,"CICOLACF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDSKMEN,"CIDSKMEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CINAVSTA,"CINAVSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CINAVNUB,"CINAVNUB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CINAVDIM,"CINAVDIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMNAFNM,"CIMNAFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMNAFSZ,"CIMNAFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIWMAFNM,"CIWMAFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIWMAFSZ,"CIWMAFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIOPNGST,"CIOPNGST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIFLCOMP,"CIFLCOMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDSKRSS,"CIDSKRSS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CILBLFIT,"CILBLFIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITXTFIT,"CITXTFIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICBXFIT,"CICBXFIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIBTNFIT,"CIBTNFIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIGRDFIT,"CIGRDFIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPAGFIT,"CIPAGFIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CILBLFBO,"CILBLFBO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITXTFBO,"CITXTFBO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICBXFBO,"CICBXFBO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIBTNFBO,"CIBTNFBO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIGRDFBO,"CIGRDFBO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPAGFBO,"CIPAGFBO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMDIFRM,"CIMDIFRM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITABMEN,"CITABMEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIXPTHEM,"CIXPTHEM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIRICONT,"CIRICONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIBCKGRD,"CIBCKGRD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPROBAR,"CIPROBAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CI_COLON,"CI_COLON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CINOBTNI,"CINOBTNI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ciwaitwd,"ciwaitwd",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISHWBTN,"CISHWBTN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIMRKGRD,"CIMRKGRD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICTRGRD,"CICTRGRD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIHEHEZO,"CIHEHEZO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIHEROZO,"CIHEROZO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIZOESPR,"CIZOESPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDSPCNT,"CIDSPCNT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICOLPST,"CICOLPST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIWARTYP,"CIWARTYP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIZOOMMO,"CIZOOMMO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIAGSFNM,"CIAGSFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIAGSFSZ,"CIAGSFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIAGSFIT,"CIAGSFIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIAGSFBO,"CIAGSFBO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIAGIFNM,"CIAGIFNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIAGIFSZ,"CIAGIFSZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIAGIFIT,"CIAGIFIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIAGIFBO,"CIAGIFBO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIGIORHO,"CIGIORHO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIZOOMSC,"CIZOOMSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDIMFRM,"CIDIMFRM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIWIDFRM,"CIWIDFRM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIHEIFRM,"CIHEIFRM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISEARNT,"CISEARNT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISEARNT,"CISEARNT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPOSREC,"COPOSREC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CINUMREC,"CINUMREC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CINRECEN,"CINRECEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITYPCAL,"CITYPCAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISAVFRM,"CISAVFRM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITBVWBT,"CITBVWBT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIVTHEME,"CIVTHEME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIGADGET,"CIGADGET",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONF_INT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONF_INT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONF_INT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONF_INT')
        i_extval=cp_InsertValODBCExtFlds(this,'CONF_INT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CICPTBAR,CIDSKBAR,CITOOBDS,CITBSIZE,CISTABAR"+;
                  ",CISHWMEN,CISTASCR,CIMENFIX,CIWNDMEN,CISEARMN"+;
                  ",CIFRMBUT,CIFRMPAG,CITOOLMN,CIMENIMM,CINEWPRI"+;
                  ",CIMONFRA,CI_ILIKE,CIMINPST,CIMODPST,CICODUTE"+;
                  ",CISELECL,CIEDTCOL,CIOBLCOL,CIDSBLFC,CIDSBLBC"+;
                  ",CIRICCOL,CIEVIZOM,CISCRCOL,CIDTLCLR,CIGRIDCL"+;
                  ",CICOLZOM,CILBLFNM,CITXTFNM,CICBXFNM,CIBTNFNM"+;
                  ",CIGRDFNM,CIPAGFNM,CILBLFSZ,CITXTFSZ,CICBXFSZ"+;
                  ",CIBTNFSZ,CIGRDFSZ,CIPAGFSZ,CISYSFRM,CISETCEN"+;
                  ",CISETDAT,CISETPNT,CISETSEP,CISETMRK,CIATUCOL"+;
                  ",CIATTCOL,CIATCCOL,CICOLPRM,CICOLNNP,CICOLSEL"+;
                  ",CICOLHDR,CIAGEVIS,CIORAINI,CIORAFIN,CISTAINI"+;
                  ",CISTAFIN,CITYPBAL,CICOLACF,CIDSKMEN,CINAVSTA"+;
                  ",CINAVNUB,CINAVDIM,CIMNAFNM,CIMNAFSZ,CIWMAFNM"+;
                  ",CIWMAFSZ,CIOPNGST,CIFLCOMP,CIDSKRSS,CILBLFIT"+;
                  ",CITXTFIT,CICBXFIT,CIBTNFIT,CIGRDFIT,CIPAGFIT"+;
                  ",CILBLFBO,CITXTFBO,CICBXFBO,CIBTNFBO,CIGRDFBO"+;
                  ",CIPAGFBO,CIMDIFRM,CITABMEN,CIXPTHEM,CIRICONT"+;
                  ",CIBCKGRD,CIPROBAR,CI_COLON,CINOBTNI,ciwaitwd"+;
                  ",CISHWBTN,CIMRKGRD,CICTRGRD,CIHEHEZO,CIHEROZO"+;
                  ",CIZOESPR,CIDSPCNT,CICOLPST,CIWARTYP,CIZOOMMO"+;
                  ",CIAGSFNM,CIAGSFSZ,CIAGSFIT,CIAGSFBO,CIAGIFNM"+;
                  ",CIAGIFSZ,CIAGIFIT,CIAGIFBO,CIGIORHO,CIZOOMSC"+;
                  ",CIDIMFRM,CIWIDFRM,CIHEIFRM,CISEARNT,COPOSREC"+;
                  ",CINUMREC,CINRECEN,CITYPCAL,CISAVFRM,CITBVWBT"+;
                  ",CIVTHEME,CIGADGET "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CICPTBAR)+;
                  ","+cp_ToStrODBC(this.w_CIDSKBAR)+;
                  ","+cp_ToStrODBC(this.w_CITOOBDS)+;
                  ","+cp_ToStrODBC(this.w_CITBSIZE)+;
                  ","+cp_ToStrODBC(this.w_CISTABAR)+;
                  ","+cp_ToStrODBC(this.w_CISHWMEN)+;
                  ","+cp_ToStrODBC(this.w_CISTASCR)+;
                  ","+cp_ToStrODBC(this.w_CIMENFIX)+;
                  ","+cp_ToStrODBC(this.w_CIWNDMEN)+;
                  ","+cp_ToStrODBC(this.w_CISEARMN)+;
                  ","+cp_ToStrODBC(this.w_CIFRMBUT)+;
                  ","+cp_ToStrODBC(this.w_CIFRMPAG)+;
                  ","+cp_ToStrODBC(this.w_CITOOLMN)+;
                  ","+cp_ToStrODBC(this.w_CIMENIMM)+;
                  ","+cp_ToStrODBC(this.w_CINEWPRI)+;
                  ","+cp_ToStrODBC(this.w_CIMONFRA)+;
                  ","+cp_ToStrODBC(this.w_CI_ILIKE)+;
                  ","+cp_ToStrODBC(this.w_CIMINPST)+;
                  ","+cp_ToStrODBC(this.w_CIMODPST)+;
                  ","+cp_ToStrODBC(this.w_CICODUTE)+;
                  ","+cp_ToStrODBC(this.w_CISELECL)+;
                  ","+cp_ToStrODBC(this.w_CIEDTCOL)+;
                  ","+cp_ToStrODBC(this.w_CIOBLCOL)+;
                  ","+cp_ToStrODBC(this.w_CIDSBLFC)+;
                  ","+cp_ToStrODBC(this.w_CIDSBLBC)+;
                  ","+cp_ToStrODBC(this.w_CIRICCOL)+;
                  ","+cp_ToStrODBC(this.w_CIEVIZOM)+;
                  ","+cp_ToStrODBC(this.w_CISCRCOL)+;
                  ","+cp_ToStrODBC(this.w_CIDTLCLR)+;
                  ","+cp_ToStrODBC(this.w_CIGRIDCL)+;
                  ","+cp_ToStrODBC(this.w_CICOLZOM)+;
                  ","+cp_ToStrODBC(this.w_CILBLFNM)+;
                  ","+cp_ToStrODBC(this.w_CITXTFNM)+;
                  ","+cp_ToStrODBC(this.w_CICBXFNM)+;
                  ","+cp_ToStrODBC(this.w_CIBTNFNM)+;
                  ","+cp_ToStrODBC(this.w_CIGRDFNM)+;
                  ","+cp_ToStrODBC(this.w_CIPAGFNM)+;
                  ","+cp_ToStrODBC(this.w_CILBLFSZ)+;
                  ","+cp_ToStrODBC(this.w_CITXTFSZ)+;
                  ","+cp_ToStrODBC(this.w_CICBXFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIBTNFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIGRDFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIPAGFSZ)+;
                  ","+cp_ToStrODBC(this.w_CISYSFRM)+;
                  ","+cp_ToStrODBC(this.w_CISETCEN)+;
                  ","+cp_ToStrODBC(this.w_CISETDAT)+;
                  ","+cp_ToStrODBC(this.w_CISETPNT)+;
                  ","+cp_ToStrODBC(this.w_CISETSEP)+;
                  ","+cp_ToStrODBC(this.w_CISETMRK)+;
                  ","+cp_ToStrODBC(this.w_CIATUCOL)+;
                  ","+cp_ToStrODBC(this.w_CIATTCOL)+;
                  ","+cp_ToStrODBC(this.w_CIATCCOL)+;
                  ","+cp_ToStrODBC(this.w_CICOLPRM)+;
                  ","+cp_ToStrODBC(this.w_CICOLNNP)+;
                  ","+cp_ToStrODBC(this.w_CICOLSEL)+;
                  ","+cp_ToStrODBC(this.w_CICOLHDR)+;
                  ","+cp_ToStrODBC(this.w_CIAGEVIS)+;
                  ","+cp_ToStrODBC(this.w_CIORAINI)+;
                  ","+cp_ToStrODBC(this.w_CIORAFIN)+;
                  ","+cp_ToStrODBC(this.w_CISTAINI)+;
                  ","+cp_ToStrODBC(this.w_CISTAFIN)+;
                  ","+cp_ToStrODBC(this.w_CITYPBAL)+;
                  ","+cp_ToStrODBC(this.w_CICOLACF)+;
                  ","+cp_ToStrODBC(this.w_CIDSKMEN)+;
                  ","+cp_ToStrODBC(this.w_CINAVSTA)+;
                  ","+cp_ToStrODBC(this.w_CINAVNUB)+;
                  ","+cp_ToStrODBC(this.w_CINAVDIM)+;
                  ","+cp_ToStrODBC(this.w_CIMNAFNM)+;
                  ","+cp_ToStrODBC(this.w_CIMNAFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIWMAFNM)+;
                  ","+cp_ToStrODBC(this.w_CIWMAFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIOPNGST)+;
                  ","+cp_ToStrODBC(this.w_CIFLCOMP)+;
                  ","+cp_ToStrODBC(this.w_CIDSKRSS)+;
                  ","+cp_ToStrODBC(this.w_CILBLFIT)+;
                  ","+cp_ToStrODBC(this.w_CITXTFIT)+;
                  ","+cp_ToStrODBC(this.w_CICBXFIT)+;
                  ","+cp_ToStrODBC(this.w_CIBTNFIT)+;
                  ","+cp_ToStrODBC(this.w_CIGRDFIT)+;
                  ","+cp_ToStrODBC(this.w_CIPAGFIT)+;
                  ","+cp_ToStrODBC(this.w_CILBLFBO)+;
                  ","+cp_ToStrODBC(this.w_CITXTFBO)+;
                  ","+cp_ToStrODBC(this.w_CICBXFBO)+;
                  ","+cp_ToStrODBC(this.w_CIBTNFBO)+;
                  ","+cp_ToStrODBC(this.w_CIGRDFBO)+;
                  ","+cp_ToStrODBC(this.w_CIPAGFBO)+;
                  ","+cp_ToStrODBC(this.w_CIMDIFRM)+;
                  ","+cp_ToStrODBC(this.w_CITABMEN)+;
                  ","+cp_ToStrODBC(this.w_CIXPTHEM)+;
                  ","+cp_ToStrODBC(this.w_CIRICONT)+;
                  ","+cp_ToStrODBC(this.w_CIBCKGRD)+;
                  ","+cp_ToStrODBC(this.w_CIPROBAR)+;
                  ","+cp_ToStrODBC(this.w_CI_COLON)+;
                  ","+cp_ToStrODBC(this.w_CINOBTNI)+;
                  ","+cp_ToStrODBC(this.w_ciwaitwd)+;
                  ","+cp_ToStrODBC(this.w_CISHWBTN)+;
                  ","+cp_ToStrODBC(this.w_CIMRKGRD)+;
                  ","+cp_ToStrODBC(this.w_CICTRGRD)+;
                  ","+cp_ToStrODBC(this.w_CIHEHEZO)+;
                  ","+cp_ToStrODBC(this.w_CIHEROZO)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBC(this.w_CIZOESPR)+;
                  ","+cp_ToStrODBC(this.w_CIDSPCNT)+;
                  ","+cp_ToStrODBC(this.w_CICOLPST)+;
                  ","+cp_ToStrODBC(this.w_CIWARTYP)+;
                  ","+cp_ToStrODBC(this.w_CIZOOMMO)+;
                  ","+cp_ToStrODBC(this.w_CIAGSFNM)+;
                  ","+cp_ToStrODBC(this.w_CIAGSFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIAGSFIT)+;
                  ","+cp_ToStrODBC(this.w_CIAGSFBO)+;
                  ","+cp_ToStrODBC(this.w_CIAGIFNM)+;
                  ","+cp_ToStrODBC(this.w_CIAGIFSZ)+;
                  ","+cp_ToStrODBC(this.w_CIAGIFIT)+;
                  ","+cp_ToStrODBC(this.w_CIAGIFBO)+;
                  ","+cp_ToStrODBC(this.w_CIGIORHO)+;
                  ","+cp_ToStrODBC(this.w_CIZOOMSC)+;
                  ","+cp_ToStrODBC(this.w_CIDIMFRM)+;
                  ","+cp_ToStrODBC(this.w_CIWIDFRM)+;
                  ","+cp_ToStrODBC(this.w_CIHEIFRM)+;
                  ","+cp_ToStrODBC(this.w_CISEARNT)+;
                  ","+cp_ToStrODBC(this.w_COPOSREC)+;
                  ","+cp_ToStrODBC(this.w_CINUMREC)+;
                  ","+cp_ToStrODBC(this.w_CINRECEN)+;
                  ","+cp_ToStrODBC(this.w_CITYPCAL)+;
                  ","+cp_ToStrODBC(this.w_CISAVFRM)+;
                  ","+cp_ToStrODBC(this.w_CITBVWBT)+;
                  ","+cp_ToStrODBC(this.w_CIVTHEME)+;
                  ","+cp_ToStrODBC(this.w_CIGADGET)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONF_INT')
        i_extval=cp_InsertValVFPExtFlds(this,'CONF_INT')
        cp_CheckDeletedKey(i_cTable,0,'CICODUTE',this.w_CICODUTE)
        INSERT INTO (i_cTable);
              (CICPTBAR,CIDSKBAR,CITOOBDS,CITBSIZE,CISTABAR,CISHWMEN,CISTASCR,CIMENFIX,CIWNDMEN,CISEARMN,CIFRMBUT,CIFRMPAG,CITOOLMN,CIMENIMM,CINEWPRI,CIMONFRA,CI_ILIKE,CIMINPST,CIMODPST,CICODUTE,CISELECL,CIEDTCOL,CIOBLCOL,CIDSBLFC,CIDSBLBC,CIRICCOL,CIEVIZOM,CISCRCOL,CIDTLCLR,CIGRIDCL,CICOLZOM,CILBLFNM,CITXTFNM,CICBXFNM,CIBTNFNM,CIGRDFNM,CIPAGFNM,CILBLFSZ,CITXTFSZ,CICBXFSZ,CIBTNFSZ,CIGRDFSZ,CIPAGFSZ,CISYSFRM,CISETCEN,CISETDAT,CISETPNT,CISETSEP,CISETMRK,CIATUCOL,CIATTCOL,CIATCCOL,CICOLPRM,CICOLNNP,CICOLSEL,CICOLHDR,CIAGEVIS,CIORAINI,CIORAFIN,CISTAINI,CISTAFIN,CITYPBAL,CICOLACF,CIDSKMEN,CINAVSTA,CINAVNUB,CINAVDIM,CIMNAFNM,CIMNAFSZ,CIWMAFNM,CIWMAFSZ,CIOPNGST,CIFLCOMP,CIDSKRSS,CILBLFIT,CITXTFIT,CICBXFIT,CIBTNFIT,CIGRDFIT,CIPAGFIT,CILBLFBO,CITXTFBO,CICBXFBO,CIBTNFBO,CIGRDFBO,CIPAGFBO,CIMDIFRM,CITABMEN,CIXPTHEM,CIRICONT,CIBCKGRD,CIPROBAR,CI_COLON,CINOBTNI,ciwaitwd,CISHWBTN,CIMRKGRD,CICTRGRD,CIHEHEZO,CIHEROZO,CIZOESPR,CIDSPCNT,CICOLPST,CIWARTYP,CIZOOMMO,CIAGSFNM,CIAGSFSZ,CIAGSFIT,CIAGSFBO,CIAGIFNM,CIAGIFSZ,CIAGIFIT,CIAGIFBO,CIGIORHO,CIZOOMSC,CIDIMFRM,CIWIDFRM,CIHEIFRM,CISEARNT,COPOSREC,CINUMREC,CINRECEN,CITYPCAL,CISAVFRM,CITBVWBT,CIVTHEME,CIGADGET  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CICPTBAR;
                  ,this.w_CIDSKBAR;
                  ,this.w_CITOOBDS;
                  ,this.w_CITBSIZE;
                  ,this.w_CISTABAR;
                  ,this.w_CISHWMEN;
                  ,this.w_CISTASCR;
                  ,this.w_CIMENFIX;
                  ,this.w_CIWNDMEN;
                  ,this.w_CISEARMN;
                  ,this.w_CIFRMBUT;
                  ,this.w_CIFRMPAG;
                  ,this.w_CITOOLMN;
                  ,this.w_CIMENIMM;
                  ,this.w_CINEWPRI;
                  ,this.w_CIMONFRA;
                  ,this.w_CI_ILIKE;
                  ,this.w_CIMINPST;
                  ,this.w_CIMODPST;
                  ,this.w_CICODUTE;
                  ,this.w_CISELECL;
                  ,this.w_CIEDTCOL;
                  ,this.w_CIOBLCOL;
                  ,this.w_CIDSBLFC;
                  ,this.w_CIDSBLBC;
                  ,this.w_CIRICCOL;
                  ,this.w_CIEVIZOM;
                  ,this.w_CISCRCOL;
                  ,this.w_CIDTLCLR;
                  ,this.w_CIGRIDCL;
                  ,this.w_CICOLZOM;
                  ,this.w_CILBLFNM;
                  ,this.w_CITXTFNM;
                  ,this.w_CICBXFNM;
                  ,this.w_CIBTNFNM;
                  ,this.w_CIGRDFNM;
                  ,this.w_CIPAGFNM;
                  ,this.w_CILBLFSZ;
                  ,this.w_CITXTFSZ;
                  ,this.w_CICBXFSZ;
                  ,this.w_CIBTNFSZ;
                  ,this.w_CIGRDFSZ;
                  ,this.w_CIPAGFSZ;
                  ,this.w_CISYSFRM;
                  ,this.w_CISETCEN;
                  ,this.w_CISETDAT;
                  ,this.w_CISETPNT;
                  ,this.w_CISETSEP;
                  ,this.w_CISETMRK;
                  ,this.w_CIATUCOL;
                  ,this.w_CIATTCOL;
                  ,this.w_CIATCCOL;
                  ,this.w_CICOLPRM;
                  ,this.w_CICOLNNP;
                  ,this.w_CICOLSEL;
                  ,this.w_CICOLHDR;
                  ,this.w_CIAGEVIS;
                  ,this.w_CIORAINI;
                  ,this.w_CIORAFIN;
                  ,this.w_CISTAINI;
                  ,this.w_CISTAFIN;
                  ,this.w_CITYPBAL;
                  ,this.w_CICOLACF;
                  ,this.w_CIDSKMEN;
                  ,this.w_CINAVSTA;
                  ,this.w_CINAVNUB;
                  ,this.w_CINAVDIM;
                  ,this.w_CIMNAFNM;
                  ,this.w_CIMNAFSZ;
                  ,this.w_CIWMAFNM;
                  ,this.w_CIWMAFSZ;
                  ,this.w_CIOPNGST;
                  ,this.w_CIFLCOMP;
                  ,this.w_CIDSKRSS;
                  ,this.w_CILBLFIT;
                  ,this.w_CITXTFIT;
                  ,this.w_CICBXFIT;
                  ,this.w_CIBTNFIT;
                  ,this.w_CIGRDFIT;
                  ,this.w_CIPAGFIT;
                  ,this.w_CILBLFBO;
                  ,this.w_CITXTFBO;
                  ,this.w_CICBXFBO;
                  ,this.w_CIBTNFBO;
                  ,this.w_CIGRDFBO;
                  ,this.w_CIPAGFBO;
                  ,this.w_CIMDIFRM;
                  ,this.w_CITABMEN;
                  ,this.w_CIXPTHEM;
                  ,this.w_CIRICONT;
                  ,this.w_CIBCKGRD;
                  ,this.w_CIPROBAR;
                  ,this.w_CI_COLON;
                  ,this.w_CINOBTNI;
                  ,this.w_ciwaitwd;
                  ,this.w_CISHWBTN;
                  ,this.w_CIMRKGRD;
                  ,this.w_CICTRGRD;
                  ,this.w_CIHEHEZO;
                  ,this.w_CIHEROZO;
                  ,this.w_CIZOESPR;
                  ,this.w_CIDSPCNT;
                  ,this.w_CICOLPST;
                  ,this.w_CIWARTYP;
                  ,this.w_CIZOOMMO;
                  ,this.w_CIAGSFNM;
                  ,this.w_CIAGSFSZ;
                  ,this.w_CIAGSFIT;
                  ,this.w_CIAGSFBO;
                  ,this.w_CIAGIFNM;
                  ,this.w_CIAGIFSZ;
                  ,this.w_CIAGIFIT;
                  ,this.w_CIAGIFBO;
                  ,this.w_CIGIORHO;
                  ,this.w_CIZOOMSC;
                  ,this.w_CIDIMFRM;
                  ,this.w_CIWIDFRM;
                  ,this.w_CIHEIFRM;
                  ,this.w_CISEARNT;
                  ,this.w_COPOSREC;
                  ,this.w_CINUMREC;
                  ,this.w_CINRECEN;
                  ,this.w_CITYPCAL;
                  ,this.w_CISAVFRM;
                  ,this.w_CITBVWBT;
                  ,this.w_CIVTHEME;
                  ,this.w_CIGADGET;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONF_INT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONF_INT_IDX,i_nConn)
      *
      * update CONF_INT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONF_INT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CICPTBAR="+cp_ToStrODBC(this.w_CICPTBAR)+;
             ",CIDSKBAR="+cp_ToStrODBC(this.w_CIDSKBAR)+;
             ",CITOOBDS="+cp_ToStrODBC(this.w_CITOOBDS)+;
             ",CITBSIZE="+cp_ToStrODBC(this.w_CITBSIZE)+;
             ",CISTABAR="+cp_ToStrODBC(this.w_CISTABAR)+;
             ",CISHWMEN="+cp_ToStrODBC(this.w_CISHWMEN)+;
             ",CISTASCR="+cp_ToStrODBC(this.w_CISTASCR)+;
             ",CIMENFIX="+cp_ToStrODBC(this.w_CIMENFIX)+;
             ",CIWNDMEN="+cp_ToStrODBC(this.w_CIWNDMEN)+;
             ",CISEARMN="+cp_ToStrODBC(this.w_CISEARMN)+;
             ",CIFRMBUT="+cp_ToStrODBC(this.w_CIFRMBUT)+;
             ",CIFRMPAG="+cp_ToStrODBC(this.w_CIFRMPAG)+;
             ",CITOOLMN="+cp_ToStrODBC(this.w_CITOOLMN)+;
             ",CIMENIMM="+cp_ToStrODBC(this.w_CIMENIMM)+;
             ",CINEWPRI="+cp_ToStrODBC(this.w_CINEWPRI)+;
             ",CIMONFRA="+cp_ToStrODBC(this.w_CIMONFRA)+;
             ",CI_ILIKE="+cp_ToStrODBC(this.w_CI_ILIKE)+;
             ",CIMINPST="+cp_ToStrODBC(this.w_CIMINPST)+;
             ",CIMODPST="+cp_ToStrODBC(this.w_CIMODPST)+;
             ",CISELECL="+cp_ToStrODBC(this.w_CISELECL)+;
             ",CIEDTCOL="+cp_ToStrODBC(this.w_CIEDTCOL)+;
             ",CIOBLCOL="+cp_ToStrODBC(this.w_CIOBLCOL)+;
             ",CIDSBLFC="+cp_ToStrODBC(this.w_CIDSBLFC)+;
             ",CIDSBLBC="+cp_ToStrODBC(this.w_CIDSBLBC)+;
             ",CIRICCOL="+cp_ToStrODBC(this.w_CIRICCOL)+;
             ",CIEVIZOM="+cp_ToStrODBC(this.w_CIEVIZOM)+;
             ",CISCRCOL="+cp_ToStrODBC(this.w_CISCRCOL)+;
             ",CIDTLCLR="+cp_ToStrODBC(this.w_CIDTLCLR)+;
             ",CIGRIDCL="+cp_ToStrODBC(this.w_CIGRIDCL)+;
             ",CICOLZOM="+cp_ToStrODBC(this.w_CICOLZOM)+;
             ",CILBLFNM="+cp_ToStrODBC(this.w_CILBLFNM)+;
             ",CITXTFNM="+cp_ToStrODBC(this.w_CITXTFNM)+;
             ",CICBXFNM="+cp_ToStrODBC(this.w_CICBXFNM)+;
             ",CIBTNFNM="+cp_ToStrODBC(this.w_CIBTNFNM)+;
             ",CIGRDFNM="+cp_ToStrODBC(this.w_CIGRDFNM)+;
             ",CIPAGFNM="+cp_ToStrODBC(this.w_CIPAGFNM)+;
             ",CILBLFSZ="+cp_ToStrODBC(this.w_CILBLFSZ)+;
             ",CITXTFSZ="+cp_ToStrODBC(this.w_CITXTFSZ)+;
             ",CICBXFSZ="+cp_ToStrODBC(this.w_CICBXFSZ)+;
             ",CIBTNFSZ="+cp_ToStrODBC(this.w_CIBTNFSZ)+;
             ",CIGRDFSZ="+cp_ToStrODBC(this.w_CIGRDFSZ)+;
             ",CIPAGFSZ="+cp_ToStrODBC(this.w_CIPAGFSZ)+;
             ",CISYSFRM="+cp_ToStrODBC(this.w_CISYSFRM)+;
             ",CISETCEN="+cp_ToStrODBC(this.w_CISETCEN)+;
             ",CISETDAT="+cp_ToStrODBC(this.w_CISETDAT)+;
             ",CISETPNT="+cp_ToStrODBC(this.w_CISETPNT)+;
             ",CISETSEP="+cp_ToStrODBC(this.w_CISETSEP)+;
             ",CISETMRK="+cp_ToStrODBC(this.w_CISETMRK)+;
             ",CIATUCOL="+cp_ToStrODBC(this.w_CIATUCOL)+;
             ",CIATTCOL="+cp_ToStrODBC(this.w_CIATTCOL)+;
             ",CIATCCOL="+cp_ToStrODBC(this.w_CIATCCOL)+;
             ",CICOLPRM="+cp_ToStrODBC(this.w_CICOLPRM)+;
             ",CICOLNNP="+cp_ToStrODBC(this.w_CICOLNNP)+;
             ",CICOLSEL="+cp_ToStrODBC(this.w_CICOLSEL)+;
             ",CICOLHDR="+cp_ToStrODBC(this.w_CICOLHDR)+;
             ",CIAGEVIS="+cp_ToStrODBC(this.w_CIAGEVIS)+;
             ",CIORAINI="+cp_ToStrODBC(this.w_CIORAINI)+;
             ",CIORAFIN="+cp_ToStrODBC(this.w_CIORAFIN)+;
             ",CISTAINI="+cp_ToStrODBC(this.w_CISTAINI)+;
             ",CISTAFIN="+cp_ToStrODBC(this.w_CISTAFIN)+;
             ",CITYPBAL="+cp_ToStrODBC(this.w_CITYPBAL)+;
             ",CICOLACF="+cp_ToStrODBC(this.w_CICOLACF)+;
             ",CIDSKMEN="+cp_ToStrODBC(this.w_CIDSKMEN)+;
             ",CINAVSTA="+cp_ToStrODBC(this.w_CINAVSTA)+;
             ",CINAVNUB="+cp_ToStrODBC(this.w_CINAVNUB)+;
             ",CINAVDIM="+cp_ToStrODBC(this.w_CINAVDIM)+;
             ",CIMNAFNM="+cp_ToStrODBC(this.w_CIMNAFNM)+;
             ",CIMNAFSZ="+cp_ToStrODBC(this.w_CIMNAFSZ)+;
             ",CIWMAFNM="+cp_ToStrODBC(this.w_CIWMAFNM)+;
             ",CIWMAFSZ="+cp_ToStrODBC(this.w_CIWMAFSZ)+;
             ",CIOPNGST="+cp_ToStrODBC(this.w_CIOPNGST)+;
             ",CIFLCOMP="+cp_ToStrODBC(this.w_CIFLCOMP)+;
             ",CIDSKRSS="+cp_ToStrODBC(this.w_CIDSKRSS)+;
             ",CILBLFIT="+cp_ToStrODBC(this.w_CILBLFIT)+;
             ",CITXTFIT="+cp_ToStrODBC(this.w_CITXTFIT)+;
             ",CICBXFIT="+cp_ToStrODBC(this.w_CICBXFIT)+;
             ",CIBTNFIT="+cp_ToStrODBC(this.w_CIBTNFIT)+;
             ",CIGRDFIT="+cp_ToStrODBC(this.w_CIGRDFIT)+;
             ",CIPAGFIT="+cp_ToStrODBC(this.w_CIPAGFIT)+;
             ",CILBLFBO="+cp_ToStrODBC(this.w_CILBLFBO)+;
             ",CITXTFBO="+cp_ToStrODBC(this.w_CITXTFBO)+;
             ",CICBXFBO="+cp_ToStrODBC(this.w_CICBXFBO)+;
             ",CIBTNFBO="+cp_ToStrODBC(this.w_CIBTNFBO)+;
             ",CIGRDFBO="+cp_ToStrODBC(this.w_CIGRDFBO)+;
             ",CIPAGFBO="+cp_ToStrODBC(this.w_CIPAGFBO)+;
             ",CIMDIFRM="+cp_ToStrODBC(this.w_CIMDIFRM)+;
             ",CITABMEN="+cp_ToStrODBC(this.w_CITABMEN)+;
             ",CIXPTHEM="+cp_ToStrODBC(this.w_CIXPTHEM)+;
             ",CIRICONT="+cp_ToStrODBC(this.w_CIRICONT)+;
             ",CIBCKGRD="+cp_ToStrODBC(this.w_CIBCKGRD)+;
             ",CIPROBAR="+cp_ToStrODBC(this.w_CIPROBAR)+;
             ",CI_COLON="+cp_ToStrODBC(this.w_CI_COLON)+;
             ",CINOBTNI="+cp_ToStrODBC(this.w_CINOBTNI)+;
             ",ciwaitwd="+cp_ToStrODBC(this.w_ciwaitwd)+;
             ",CISHWBTN="+cp_ToStrODBC(this.w_CISHWBTN)+;
             ",CIMRKGRD="+cp_ToStrODBC(this.w_CIMRKGRD)+;
             ",CICTRGRD="+cp_ToStrODBC(this.w_CICTRGRD)+;
             ",CIHEHEZO="+cp_ToStrODBC(this.w_CIHEHEZO)+;
             ",CIHEROZO="+cp_ToStrODBC(this.w_CIHEROZO)+;
             ",CIZOESPR="+cp_ToStrODBC(this.w_CIZOESPR)+;
             ""
             i_nnn=i_nnn+;
             ",CIDSPCNT="+cp_ToStrODBC(this.w_CIDSPCNT)+;
             ",CICOLPST="+cp_ToStrODBC(this.w_CICOLPST)+;
             ",CIWARTYP="+cp_ToStrODBC(this.w_CIWARTYP)+;
             ",CIZOOMMO="+cp_ToStrODBC(this.w_CIZOOMMO)+;
             ",CIAGSFNM="+cp_ToStrODBC(this.w_CIAGSFNM)+;
             ",CIAGSFSZ="+cp_ToStrODBC(this.w_CIAGSFSZ)+;
             ",CIAGSFIT="+cp_ToStrODBC(this.w_CIAGSFIT)+;
             ",CIAGSFBO="+cp_ToStrODBC(this.w_CIAGSFBO)+;
             ",CIAGIFNM="+cp_ToStrODBC(this.w_CIAGIFNM)+;
             ",CIAGIFSZ="+cp_ToStrODBC(this.w_CIAGIFSZ)+;
             ",CIAGIFIT="+cp_ToStrODBC(this.w_CIAGIFIT)+;
             ",CIAGIFBO="+cp_ToStrODBC(this.w_CIAGIFBO)+;
             ",CIGIORHO="+cp_ToStrODBC(this.w_CIGIORHO)+;
             ",CIZOOMSC="+cp_ToStrODBC(this.w_CIZOOMSC)+;
             ",CIDIMFRM="+cp_ToStrODBC(this.w_CIDIMFRM)+;
             ",CIWIDFRM="+cp_ToStrODBC(this.w_CIWIDFRM)+;
             ",CIHEIFRM="+cp_ToStrODBC(this.w_CIHEIFRM)+;
             ",CISEARNT="+cp_ToStrODBC(this.w_CISEARNT)+;
             ",COPOSREC="+cp_ToStrODBC(this.w_COPOSREC)+;
             ",CINUMREC="+cp_ToStrODBC(this.w_CINUMREC)+;
             ",CINRECEN="+cp_ToStrODBC(this.w_CINRECEN)+;
             ",CITYPCAL="+cp_ToStrODBC(this.w_CITYPCAL)+;
             ",CISAVFRM="+cp_ToStrODBC(this.w_CISAVFRM)+;
             ",CITBVWBT="+cp_ToStrODBC(this.w_CITBVWBT)+;
             ",CIVTHEME="+cp_ToStrODBC(this.w_CIVTHEME)+;
             ",CIGADGET="+cp_ToStrODBC(this.w_CIGADGET)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONF_INT')
        i_cWhere = cp_PKFox(i_cTable  ,'CICODUTE',this.w_CICODUTE  )
        UPDATE (i_cTable) SET;
              CICPTBAR=this.w_CICPTBAR;
             ,CIDSKBAR=this.w_CIDSKBAR;
             ,CITOOBDS=this.w_CITOOBDS;
             ,CITBSIZE=this.w_CITBSIZE;
             ,CISTABAR=this.w_CISTABAR;
             ,CISHWMEN=this.w_CISHWMEN;
             ,CISTASCR=this.w_CISTASCR;
             ,CIMENFIX=this.w_CIMENFIX;
             ,CIWNDMEN=this.w_CIWNDMEN;
             ,CISEARMN=this.w_CISEARMN;
             ,CIFRMBUT=this.w_CIFRMBUT;
             ,CIFRMPAG=this.w_CIFRMPAG;
             ,CITOOLMN=this.w_CITOOLMN;
             ,CIMENIMM=this.w_CIMENIMM;
             ,CINEWPRI=this.w_CINEWPRI;
             ,CIMONFRA=this.w_CIMONFRA;
             ,CI_ILIKE=this.w_CI_ILIKE;
             ,CIMINPST=this.w_CIMINPST;
             ,CIMODPST=this.w_CIMODPST;
             ,CISELECL=this.w_CISELECL;
             ,CIEDTCOL=this.w_CIEDTCOL;
             ,CIOBLCOL=this.w_CIOBLCOL;
             ,CIDSBLFC=this.w_CIDSBLFC;
             ,CIDSBLBC=this.w_CIDSBLBC;
             ,CIRICCOL=this.w_CIRICCOL;
             ,CIEVIZOM=this.w_CIEVIZOM;
             ,CISCRCOL=this.w_CISCRCOL;
             ,CIDTLCLR=this.w_CIDTLCLR;
             ,CIGRIDCL=this.w_CIGRIDCL;
             ,CICOLZOM=this.w_CICOLZOM;
             ,CILBLFNM=this.w_CILBLFNM;
             ,CITXTFNM=this.w_CITXTFNM;
             ,CICBXFNM=this.w_CICBXFNM;
             ,CIBTNFNM=this.w_CIBTNFNM;
             ,CIGRDFNM=this.w_CIGRDFNM;
             ,CIPAGFNM=this.w_CIPAGFNM;
             ,CILBLFSZ=this.w_CILBLFSZ;
             ,CITXTFSZ=this.w_CITXTFSZ;
             ,CICBXFSZ=this.w_CICBXFSZ;
             ,CIBTNFSZ=this.w_CIBTNFSZ;
             ,CIGRDFSZ=this.w_CIGRDFSZ;
             ,CIPAGFSZ=this.w_CIPAGFSZ;
             ,CISYSFRM=this.w_CISYSFRM;
             ,CISETCEN=this.w_CISETCEN;
             ,CISETDAT=this.w_CISETDAT;
             ,CISETPNT=this.w_CISETPNT;
             ,CISETSEP=this.w_CISETSEP;
             ,CISETMRK=this.w_CISETMRK;
             ,CIATUCOL=this.w_CIATUCOL;
             ,CIATTCOL=this.w_CIATTCOL;
             ,CIATCCOL=this.w_CIATCCOL;
             ,CICOLPRM=this.w_CICOLPRM;
             ,CICOLNNP=this.w_CICOLNNP;
             ,CICOLSEL=this.w_CICOLSEL;
             ,CICOLHDR=this.w_CICOLHDR;
             ,CIAGEVIS=this.w_CIAGEVIS;
             ,CIORAINI=this.w_CIORAINI;
             ,CIORAFIN=this.w_CIORAFIN;
             ,CISTAINI=this.w_CISTAINI;
             ,CISTAFIN=this.w_CISTAFIN;
             ,CITYPBAL=this.w_CITYPBAL;
             ,CICOLACF=this.w_CICOLACF;
             ,CIDSKMEN=this.w_CIDSKMEN;
             ,CINAVSTA=this.w_CINAVSTA;
             ,CINAVNUB=this.w_CINAVNUB;
             ,CINAVDIM=this.w_CINAVDIM;
             ,CIMNAFNM=this.w_CIMNAFNM;
             ,CIMNAFSZ=this.w_CIMNAFSZ;
             ,CIWMAFNM=this.w_CIWMAFNM;
             ,CIWMAFSZ=this.w_CIWMAFSZ;
             ,CIOPNGST=this.w_CIOPNGST;
             ,CIFLCOMP=this.w_CIFLCOMP;
             ,CIDSKRSS=this.w_CIDSKRSS;
             ,CILBLFIT=this.w_CILBLFIT;
             ,CITXTFIT=this.w_CITXTFIT;
             ,CICBXFIT=this.w_CICBXFIT;
             ,CIBTNFIT=this.w_CIBTNFIT;
             ,CIGRDFIT=this.w_CIGRDFIT;
             ,CIPAGFIT=this.w_CIPAGFIT;
             ,CILBLFBO=this.w_CILBLFBO;
             ,CITXTFBO=this.w_CITXTFBO;
             ,CICBXFBO=this.w_CICBXFBO;
             ,CIBTNFBO=this.w_CIBTNFBO;
             ,CIGRDFBO=this.w_CIGRDFBO;
             ,CIPAGFBO=this.w_CIPAGFBO;
             ,CIMDIFRM=this.w_CIMDIFRM;
             ,CITABMEN=this.w_CITABMEN;
             ,CIXPTHEM=this.w_CIXPTHEM;
             ,CIRICONT=this.w_CIRICONT;
             ,CIBCKGRD=this.w_CIBCKGRD;
             ,CIPROBAR=this.w_CIPROBAR;
             ,CI_COLON=this.w_CI_COLON;
             ,CINOBTNI=this.w_CINOBTNI;
             ,ciwaitwd=this.w_ciwaitwd;
             ,CISHWBTN=this.w_CISHWBTN;
             ,CIMRKGRD=this.w_CIMRKGRD;
             ,CICTRGRD=this.w_CICTRGRD;
             ,CIHEHEZO=this.w_CIHEHEZO;
             ,CIHEROZO=this.w_CIHEROZO;
             ,CIZOESPR=this.w_CIZOESPR;
             ,CIDSPCNT=this.w_CIDSPCNT;
             ,CICOLPST=this.w_CICOLPST;
             ,CIWARTYP=this.w_CIWARTYP;
             ,CIZOOMMO=this.w_CIZOOMMO;
             ,CIAGSFNM=this.w_CIAGSFNM;
             ,CIAGSFSZ=this.w_CIAGSFSZ;
             ,CIAGSFIT=this.w_CIAGSFIT;
             ,CIAGSFBO=this.w_CIAGSFBO;
             ,CIAGIFNM=this.w_CIAGIFNM;
             ,CIAGIFSZ=this.w_CIAGIFSZ;
             ,CIAGIFIT=this.w_CIAGIFIT;
             ,CIAGIFBO=this.w_CIAGIFBO;
             ,CIGIORHO=this.w_CIGIORHO;
             ,CIZOOMSC=this.w_CIZOOMSC;
             ,CIDIMFRM=this.w_CIDIMFRM;
             ,CIWIDFRM=this.w_CIWIDFRM;
             ,CIHEIFRM=this.w_CIHEIFRM;
             ,CISEARNT=this.w_CISEARNT;
             ,COPOSREC=this.w_COPOSREC;
             ,CINUMREC=this.w_CINUMREC;
             ,CINRECEN=this.w_CINRECEN;
             ,CITYPCAL=this.w_CITYPCAL;
             ,CISAVFRM=this.w_CISAVFRM;
             ,CITBVWBT=this.w_CITBVWBT;
             ,CIVTHEME=this.w_CIVTHEME;
             ,CIGADGET=this.w_CIGADGET;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONF_INT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONF_INT_IDX,i_nConn)
      *
      * delete CONF_INT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CICODUTE',this.w_CICODUTE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONF_INT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])
    if i_bUpd
      with this
        if .o_CIVTHEME<>.w_CIVTHEME
            .w_CICPTBAR = 'S'
        endif
        if .o_CIVTHEME<>.w_CIVTHEME
            .w_CIDSKBAR = 'S'
        endif
        if .o_CIVTHEME<>.w_CIVTHEME
            .w_CITOOBDS = ' '
        endif
        .DoRTCalc(4,4,.t.)
        if .o_CIVTHEME<>.w_CIVTHEME
            .w_CISTABAR = 'N'
        endif
        if .o_CIVTHEME<>.w_CIVTHEME
            .w_CISHWMEN = 'S'
        endif
        .DoRTCalc(7,8,.t.)
        if .o_CISHWMEN<>.w_CISHWMEN
            .w_CIWNDMEN = IIF(.w_CISHWMEN='S', .w_CIWNDMEN, '')
        endif
        .DoRTCalc(10,10,.t.)
        if .o_CISHWMEN<>.w_CISHWMEN.or. .o_CIDSKMEN<>.w_CIDSKMEN.or. .o_CIVTHEME<>.w_CIVTHEME
            .w_XXSEARMN = IIF((.w_CISHWMEN = 'S' OR .w_CIDSKMEN <> 'H') And .w_CIVTHEME<>-1, .w_XXSEARMN, '')
        endif
        if .o_CISHWMEN<>.w_CISHWMEN.or. .o_XXSEARMN<>.w_XXSEARMN.or. .o_CIVTHEME<>.w_CIVTHEME
            .w_XXSEARM1 = IIF(.w_XXSEARMN = 'S' AND .w_CISHWMEN = 'S' And .w_CIVTHEME<>-1, .w_XXSEARM1, ' ')
        endif
        if .o_CIDSKMEN<>.w_CIDSKMEN.or. .o_XXSEARMN<>.w_XXSEARMN.or. .o_CIVTHEME<>.w_CIVTHEME
            .w_XXSEARM2 = IIF(.w_XXSEARMN = 'S' AND .w_CIDSKMEN <> 'H' And .w_CIVTHEME<>-1, .w_XXSEARM2, ' ')
        endif
        .DoRTCalc(14,14,.t.)
        if .o_CIFRMBUT<>.w_CIFRMBUT
            .w_CIFRMPAG = iif(.w_CIFRMBUT='S',.w_CIFRMPAG,'N')
        endif
        .oPgFrm.Page3.oPag.oObj_3_11.Calculate(.w_CIRICCOL)
        .oPgFrm.Page3.oPag.oObj_3_14.Calculate(.w_CIOBLCOL)
        .oPgFrm.Page3.oPag.oObj_3_18.Calculate(.w_CIEDTCOL)
        .oPgFrm.Page3.oPag.oObj_3_19.Calculate(.w_CISELECL)
        .oPgFrm.Page3.oPag.oObj_3_24.Calculate(.w_CIDSBLFC)
        .oPgFrm.Page3.oPag.oObj_3_25.Calculate(.w_CIDSBLBC)
        .oPgFrm.Page3.oPag.oObj_3_51.Calculate(.w_CISCRCOL)
        .oPgFrm.Page3.oPag.oObj_3_53.Calculate(.w_CIDTLCLR)
        .oPgFrm.Page3.oPag.oObj_3_55.Calculate(.w_CIGRIDCL)
        .oPgFrm.Page3.oPag.oObj_3_61.Calculate(.w_CICOLZOM)
        .DoRTCalc(16,60,.t.)
        if .o_CISETDAT<>.w_CISETDAT.or. .o_CISETMRK<>.w_CISETMRK
            .w_PRVDATA = {^2005/12/31}
        endif
        if .o_SETCEN<>.w_SETCEN
            .w_CISETCEN = .w_SETCEN
        endif
        if .o_SETDAT<>.w_SETDAT
            .w_CISETDAT = .w_SETDAT
        endif
        if .o_SETPNT<>.w_SETPNT
            .w_CISETPNT = .w_SETPNT
        endif
        if .o_SETSEP<>.w_SETSEP
            .w_CISETSEP = .w_SETSEP
        endif
        if .o_SETMRK<>.w_SETMRK
            .w_CISETMRK = .w_SETMRK
        endif
        if .o_CISYSFRM<>.w_CISYSFRM
            .w_SETDAT = .w_CISETDAT
        endif
        if .o_CISYSFRM<>.w_CISYSFRM
            .w_SETMRK = .w_CISETMRK
        endif
        if .o_CISYSFRM<>.w_CISYSFRM
            .w_SETCEN = .w_CISETCEN
        endif
        if .o_CISYSFRM<>.w_CISYSFRM
            .w_SETPNT = .w_CISETPNT
        endif
        if .o_CISYSFRM<>.w_CISYSFRM
            .w_SETSEP = .w_CISETSEP
        endif
        if .o_CISYSFRM<>.w_CISYSFRM
          .Calculate_ZFZIGBWSFI()
        endif
        if .o_CISETMRK<>.w_CISETMRK.or. .o_CISETCEN<>.w_CISETCEN.or. .o_CISETDAT<>.w_CISETDAT.or. .o_CISYSFRM<>.w_CISYSFRM.or. .o_CISETPNT<>.w_CISETPNT.or. .o_CISETSEP<>.w_CISETSEP
          .Calculate_KYSQZSCIFE()
        endif
        .DoRTCalc(72,77,.t.)
        if .o_CIVTHEME<>.w_CIVTHEME
            .w_CICOLHDR = iif(.w_CIVTHEME=-1,Rgb(181, 213, 255),.w_CICOLHDR)
        endif
        .oPgFrm.Page6.oPag.oObj_6_18.Calculate(.w_CIATUCOL)
        .oPgFrm.Page6.oPag.oObj_6_23.Calculate(.w_CIATTCOL)
        .oPgFrm.Page6.oPag.oObj_6_24.Calculate(.w_CIATCCOL)
        .oPgFrm.Page6.oPag.oObj_6_37.Calculate(.w_CICOLPRM)
        .oPgFrm.Page6.oPag.oObj_6_38.Calculate(.w_CICOLNNP)
        .oPgFrm.Page6.oPag.oObj_6_39.Calculate(.w_CICOLHDR)
        .oPgFrm.Page6.oPag.oObj_6_40.Calculate(.w_CICOLSEL)
        .oPgFrm.Page6.oPag.oObj_6_56.Calculate(.w_CICOLACF)
        .DoRTCalc(79,93,.t.)
        if .o_CIGADGET<>.w_CIGADGET
            .w_CIDSKMEN = IIF( .w_CIGADGET='S', 'H', .w_CIDSKMEN)
        endif
        .oPgFrm.Page3.oPag.oObj_3_67.Calculate(.w_CICOLPST)
        .DoRTCalc(95,121,.t.)
        if .o_CIVTHEME<>.w_CIVTHEME
            .w_CIBCKGRD = iif(.w_CIVTHEME=-1, ' ', 'S')
        endif
        .DoRTCalc(123,124,.t.)
        if .o_CIVTHEME<>.w_CIVTHEME
            .w_CINOBTNI = ' '
        endif
        .DoRTCalc(126,128,.t.)
        if .o_CIVTHEME<>.w_CIVTHEME
            .w_CICTRGRD = iif(.w_CIVTHEME=-1, ' ', 'S')
        endif
        .DoRTCalc(130,131,.t.)
        if .o_CICTRGRD<>.w_CICTRGRD
            .w_CIZOESPR = iif(.w_CICTRGRD='S', 'N', 'S')
        endif
        if .o_CIHEHEZO<>.w_CIHEHEZO
          .Calculate_PCBJJRGQUD()
        endif
        .DoRTCalc(133,146,.t.)
        if .o_CIMDIFRM<>.w_CIMDIFRM
            .w_CIDIMFRM = IIF(.w_CIMDIFRM $ 'I-M', 'S', IIF(EMPTY(.w_CIDIMFRM), 'S' , .w_CIDIMFRM))
        endif
        if .o_CIDIMFRM<>.w_CIDIMFRM
            .w_CIWIDFRM = iif(.w_CIDIMFRM='S', 0, .w_CIWIDFRM)
        endif
        if .o_CIDIMFRM<>.w_CIDIMFRM
            .w_CIHEIFRM = iif(.w_CIDIMFRM='S', 0, .w_CIHEIFRM)
        endif
        .DoRTCalc(150,151,.t.)
        if .o_CISHWMEN<>.w_CISHWMEN
            .w_CISEAMNW = IIF(.w_CIWNDMEN='S' AND .w_CISEARNT='S', 1, 0)
        endif
        if .o_CISHWMEN<>.w_CISHWMEN
            .w_CISEADKM = IIF(.w_CISEARNT='S', 1, 0)
        endif
        if .o_CISHWMEN<>.w_CISHWMEN
            .w_CISEAAPB = IIF(.w_CISEARNT='S', 1, 0)
        endif
            .w_COPOSREC = iif(.w_CISEARNT="S", alltrim(str(.w_CISEAMNW + .w_CISEADKM + .w_CISEAAPB,1,0)), "0")
        if .o_CISEARNT<>.w_CISEARNT
            .w_CINUMREC = iif(.w_CISEARNT="S", .w_CINUMREC, 0)
        endif
        if .o_XXSEARM1<>.w_XXSEARM1.or. .o_XXSEARM2<>.w_XXSEARM2.or. .o_XXSEARMN<>.w_XXSEARMN
          .Calculate_DBJOBXTAYI()
        endif
        Local l_Dep1,l_Dep2,l_Dep3,l_Dep4
        l_Dep1= .o_XXB1<>.w_XXB1 .or. .o_XXB2<>.w_XXB2 .or. .o_XXB3<>.w_XXB3 .or. .o_XXB4<>.w_XXB4 .or. .o_XXB5<>.w_XXB5
        l_Dep2= .o_XXB6<>.w_XXB6 .or. .o_XXB7<>.w_XXB7 .or. .o_XXB8<>.w_XXB8 .or. .o_XXB9<>.w_XXB9 .or. .o_XXB10<>.w_XXB10
        l_Dep3= .o_XXB11<>.w_XXB11 .or. .o_XXB12<>.w_XXB12 .or. .o_XXB13<>.w_XXB13 .or. .o_XXB14<>.w_XXB14 .or. .o_XXB15<>.w_XXB15
        l_Dep4= .o_XXB16<>.w_XXB16 .or. .o_XXB17<>.w_XXB17
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3 .or. m.l_Dep4
          .Calculate_IFHJGNQNXD()
        endif
        .oPgFrm.Page8.oPag.XXB1IM.Calculate()
        .oPgFrm.Page8.oPag.XXB4IM.Calculate()
        .oPgFrm.Page8.oPag.XXB3IM.Calculate()
        .oPgFrm.Page8.oPag.XXB2IM.Calculate()
        .oPgFrm.Page8.oPag.XXB5IM.Calculate()
        .oPgFrm.Page8.oPag.XXB61IM.Calculate()
        .oPgFrm.Page8.oPag.XXB17IM.Calculate()
        .oPgFrm.Page8.oPag.XXB62IM.Calculate()
        .oPgFrm.Page8.oPag.XXB7IM.Calculate()
        .oPgFrm.Page8.oPag.XXB92IM.Calculate()
        .oPgFrm.Page8.oPag.XXB91IM.Calculate()
        .oPgFrm.Page8.oPag.XXB82IM.Calculate()
        .oPgFrm.Page8.oPag.XXB81IM.Calculate()
        .oPgFrm.Page8.oPag.XXB101IM.Calculate()
        .oPgFrm.Page8.oPag.XXB102IM.Calculate()
        .oPgFrm.Page8.oPag.XXB111IM.Calculate()
        .oPgFrm.Page8.oPag.XXB112IM.Calculate()
        .oPgFrm.Page8.oPag.XXB12IM.Calculate()
        .oPgFrm.Page8.oPag.XXB13IM.Calculate()
        .oPgFrm.Page8.oPag.XXB14IM.Calculate()
        .oPgFrm.Page8.oPag.XXB15IM.Calculate()
        .oPgFrm.Page8.oPag.XXB16IM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .DoRTCalc(157,178,.t.)
        if .o_CIVTHEME<>.w_CIVTHEME
            .w_CIGADGET = IIF( .w_CIVTHEME <> -1, .w_CIGADGET, 'N')
        endif
        if .o_CIMDIFRM<>.w_CIMDIFRM
          .Calculate_HOJSMBFXCY()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(180,181,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page3.oPag.oObj_3_11.Calculate(.w_CIRICCOL)
        .oPgFrm.Page3.oPag.oObj_3_14.Calculate(.w_CIOBLCOL)
        .oPgFrm.Page3.oPag.oObj_3_18.Calculate(.w_CIEDTCOL)
        .oPgFrm.Page3.oPag.oObj_3_19.Calculate(.w_CISELECL)
        .oPgFrm.Page3.oPag.oObj_3_24.Calculate(.w_CIDSBLFC)
        .oPgFrm.Page3.oPag.oObj_3_25.Calculate(.w_CIDSBLBC)
        .oPgFrm.Page3.oPag.oObj_3_51.Calculate(.w_CISCRCOL)
        .oPgFrm.Page3.oPag.oObj_3_53.Calculate(.w_CIDTLCLR)
        .oPgFrm.Page3.oPag.oObj_3_55.Calculate(.w_CIGRIDCL)
        .oPgFrm.Page3.oPag.oObj_3_61.Calculate(.w_CICOLZOM)
        .oPgFrm.Page6.oPag.oObj_6_18.Calculate(.w_CIATUCOL)
        .oPgFrm.Page6.oPag.oObj_6_23.Calculate(.w_CIATTCOL)
        .oPgFrm.Page6.oPag.oObj_6_24.Calculate(.w_CIATCCOL)
        .oPgFrm.Page6.oPag.oObj_6_37.Calculate(.w_CICOLPRM)
        .oPgFrm.Page6.oPag.oObj_6_38.Calculate(.w_CICOLNNP)
        .oPgFrm.Page6.oPag.oObj_6_39.Calculate(.w_CICOLHDR)
        .oPgFrm.Page6.oPag.oObj_6_40.Calculate(.w_CICOLSEL)
        .oPgFrm.Page6.oPag.oObj_6_56.Calculate(.w_CICOLACF)
        .oPgFrm.Page3.oPag.oObj_3_67.Calculate(.w_CICOLPST)
        .oPgFrm.Page8.oPag.XXB1IM.Calculate()
        .oPgFrm.Page8.oPag.XXB4IM.Calculate()
        .oPgFrm.Page8.oPag.XXB3IM.Calculate()
        .oPgFrm.Page8.oPag.XXB2IM.Calculate()
        .oPgFrm.Page8.oPag.XXB5IM.Calculate()
        .oPgFrm.Page8.oPag.XXB61IM.Calculate()
        .oPgFrm.Page8.oPag.XXB17IM.Calculate()
        .oPgFrm.Page8.oPag.XXB62IM.Calculate()
        .oPgFrm.Page8.oPag.XXB7IM.Calculate()
        .oPgFrm.Page8.oPag.XXB92IM.Calculate()
        .oPgFrm.Page8.oPag.XXB91IM.Calculate()
        .oPgFrm.Page8.oPag.XXB82IM.Calculate()
        .oPgFrm.Page8.oPag.XXB81IM.Calculate()
        .oPgFrm.Page8.oPag.XXB101IM.Calculate()
        .oPgFrm.Page8.oPag.XXB102IM.Calculate()
        .oPgFrm.Page8.oPag.XXB111IM.Calculate()
        .oPgFrm.Page8.oPag.XXB112IM.Calculate()
        .oPgFrm.Page8.oPag.XXB12IM.Calculate()
        .oPgFrm.Page8.oPag.XXB13IM.Calculate()
        .oPgFrm.Page8.oPag.XXB14IM.Calculate()
        .oPgFrm.Page8.oPag.XXB15IM.Calculate()
        .oPgFrm.Page8.oPag.XXB16IM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
    endwith
  return

  proc Calculate_UVGFCTLNGG()
    with this
          * --- Inizializza ricerca voci
          .w_XXSEARMN = iif(SUBSTR(.w_CISEARMN,1,1)='S' OR NVL(SUBSTR(.w_CISEARMN,2,1),' ')='S', 'S', 'N')
          .w_XXSEARM1 = SUBSTR(.w_CISEARMN,1,1)
          .w_XXSEARM2 = NVL(SUBSTR(.w_CISEARMN,2,1), ' ')
    endwith
  endproc
  proc Calculate_ZFZIGBWSFI()
    with this
          * --- Aggiorna impostazioni
          Aggiorna_Impostazioni(this;
             )
    endwith
  endproc
  proc Calculate_KYSQZSCIFE()
    with this
          * --- Anteprima data_numero
          Anteprima_Data_Num(this;
             )
    endwith
  endproc
  proc Calculate_JGOPDVKLEQ()
    with this
          * --- Messaggio data short
          DataShortMessage(this;
             )
    endwith
  endproc
  proc Calculate_PCBJJRGQUD()
    with this
          * --- controllo altezza testata
          .w_CIHEHEZO = iif(.w_CIHEHEZO=0, 19, .w_CIHEHEZO)
    endwith
  endproc
  proc Calculate_BFUVDZNLRH()
    with this
          * --- controllo altezza testata
          .w_CIHEROZO = iif(.w_CIHEROZO=0, 19, .w_CIHEROZO)
    endwith
  endproc
  proc Calculate_FYJERJTZSW()
    with this
          * --- COPOSREC
          .w_CISEAMNW = iif(between(.w_COPOSREC,"0","7"), bitand(val(.w_COPOSREC),1), iif(.w_CIWNDMEN='S', 1, 0))
          .w_CISEADKM = iif(between(.w_COPOSREC,"0","7"), bitand(val(.w_COPOSREC),2), iif(.w_COPOSREC $ "NE", 2,0))
          .w_CISEAAPB = iif(between(.w_COPOSREC,"0","7"), bitand(val(.w_COPOSREC),4), iif(.w_COPOSREC $ "TE", 4,0))
    endwith
  endproc
  proc Calculate_DBJOBXTAYI()
    with this
          * --- Abilita ricerca voci
          .w_CISEARMN = NVL(.w_XXSEARM1, ' ') + NVL(.w_XXSEARM2, ' ')
    endwith
  endproc
  proc Calculate_DHIICYARBX()
    with this
          * --- Inizializzo application bar
          .w_CITBVWBT = IIF(Isalt(),.w_CITBVWBT,space(20))
    endwith
  endproc
  proc Calculate_IFHJGNQNXD()
    with this
          * --- Assegnamento variabile w_CITBVWBT
          .w_CITBVWBT = iif(.w_XXB1='N','N',' ')+iif(.w_XXB2='N','N',' ')+iif(.w_XXB3='N','N',' ')+iif(.w_XXB4='N','N',' ')+iif(.w_XXB5='N','N',' ')+iif(.w_XXB6='N','N',' ')+iif(.w_XXB7='N','N',' ')+iif(.w_XXB8='N','N',' ')+iif(.w_XXB9='N','N',' ')+iif(.w_XXB10='N','N',' ')+iif(.w_XXB11='N','N',' ')+iif(.w_XXB12='N','N',' ')+iif(.w_XXB13='N','N',' ')+iif(.w_XXB14='N','N',' ')+iif(.w_XXB15='N','N',' ')+iif(.w_XXB16='N','N',' ')++iif(.w_XXB17='N','N',' ')
    endwith
  endproc
  proc Calculate_ZILJFIUKSR()
    with this
          * --- Assegnamento variabile w_XXB?
          .w_XXB1 = iif(NVL(SUBSTR(.w_CITBVWBT,1,1),'S')='N','N','S')
          .w_XXB2 = iif(NVL(SUBSTR(.w_CITBVWBT,2,1),'S')='N','N','S')
          .w_XXB3 = iif(NVL(SUBSTR(.w_CITBVWBT,3,1),'S')='N','N','S')
          .w_XXB4 = iif(NVL(SUBSTR(.w_CITBVWBT,4,1),'S')='N','N','S')
          .w_XXB5 = iif(NVL(SUBSTR(.w_CITBVWBT,5,1),'S')='N','N','S')
          .w_XXB6 = iif(NVL(SUBSTR(.w_CITBVWBT,6,1),'S')='N','N','S')
          .w_XXB7 = iif(NVL(SUBSTR(.w_CITBVWBT,7,1),'S')='N','N','S')
          .w_XXB8 = iif(NVL(SUBSTR(.w_CITBVWBT,8,1),'S')='N','N','S')
          .w_XXB9 = iif(NVL(SUBSTR(.w_CITBVWBT,9,1),'S')='N','N','S')
          .w_XXB10 = iif(NVL(SUBSTR(.w_CITBVWBT,10,1),'S')='N','N','S')
          .w_XXB11 = iif(NVL(SUBSTR(.w_CITBVWBT,11,1),'S')='N','N','S')
          .w_XXB12 = iif(NVL(SUBSTR(.w_CITBVWBT,12,1),'S')='N','N','S')
          .w_XXB13 = iif(NVL(SUBSTR(.w_CITBVWBT,13,1),'S')='N','N','S')
          .w_XXB14 = iif(NVL(SUBSTR(.w_CITBVWBT,14,1),'S')='N','N','S')
          .w_XXB15 = iif(NVL(SUBSTR(.w_CITBVWBT,15,1),'S')='N','N','S')
          .w_XXB16 = iif(NVL(SUBSTR(.w_CITBVWBT,16,1),'S')='N','N','S')
          .w_XXB17 = iif(NVL(SUBSTR(.w_CITBVWBT,17,1),'S')='N','N','S')
    endwith
  endproc
  proc Calculate_LOBEFNFCFQ()
    with this
          * --- Assegna visibilit� alle immagini
          .w_XXB1IM.VISIBLE = i_ab_btnpostin And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)
          .w_XXB2IM.VISIBLE = i_ab_btnpostin And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)
          .w_XXB3IM.VISIBLE = i_ab_btnpostin And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)
          .w_XXB4IM.VISIBLE = (i_ab_btnuser And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler))
          .w_XXB5IM.VISIBLE = (g_APPLICATION <> "ADHOC REVOLUTION" And g_WFLD='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler))
          .w_XXB61IM.VISIBLE = (g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and g_DMIP<>'S'
          .w_XXB62IM.VISIBLE = (g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and g_DMIP='S'
          .w_XXB7IM.VISIBLE = isalt() and g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)
          .w_XXB81IM.VISIBLE = (g_DMIP<>'S' and g_DOCM='S' and not ((g_JBSH='S' OR g_IZCP='S') AND g_UserScheduler)) and g_DMIP<>'S'
          .w_XXB82IM.VISIBLE = (g_DMIP<>'S' and g_DOCM='S' and not ((g_JBSH='S' OR g_IZCP='S') AND g_UserScheduler)) and g_DMIP='S'
          .w_XXB91IM.VISIBLE = (g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and g_DMIP<>'S'
          .w_XXB92IM.VISIBLE = (g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and g_DMIP='S'
          .w_XXB101IM.VISIBLE = .f. and NOT isalt() and Not((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler) and g_DMIP<>'S'
          .w_XXB102IM.VISIBLE = .f. and NOT isalt() and Not((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler) and g_DMIP='S'
          .w_XXB111IM.VISIBLE = (g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and g_DMIP<>'S'
          .w_XXB112IM.VISIBLE = (g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)) and g_DMIP='S'
          .w_XXB12IM.VISIBLE = (g_APPLICATION <> "ADHOC REVOLUTION"And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler))
          .w_XXB13IM.VISIBLE = (g_IRDR='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler))
          .w_XXB14IM.VISIBLE = ((Vartype(g_bDisableCfgGest)="U" Or !g_bDisableCfgGest) And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler) And Not Isalt())
          .w_XXB15IM.VISIBLE = g_AGEN="S" And (Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler))
          .w_XXB16IM.VISIBLE = (g_AGEN="S" And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler) And Isalt())
          .w_XXB17IM.VISIBLE = (g_REVI='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler))
    endwith
  endproc
  proc Calculate_HYGFWRPMZU()
    with this
          * --- Controllo valore CIBCKGRD
          .w_CIBCKGRD = iif(.w_CIVTHEME=-1, ' ', 'S')
    endwith
  endproc
  proc Calculate_HOJSMBFXCY()
    with this
          * --- Gestione salvataggio form in base alla modalit� di visualizzazione
      if .w_CIMDIFRM<>'S' And .w_OMDIFRM='S'
          .w_OSAVFRM = .w_CISAVFRM
      endif
      if .w_CIMDIFRM<>'S'
          .w_CISAVFRM = 'N'
      endif
      if .w_CIMDIFRM='S'
          .w_CISAVFRM = .w_OSAVFRM
      endif
          .w_OMDIFRM = .w_CIMDIFRM
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCIMENFIX_1_8.enabled = this.oPgFrm.Page1.oPag.oCIMENFIX_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCIWNDMEN_1_9.enabled = this.oPgFrm.Page1.oPag.oCIWNDMEN_1_9.mCond()
    this.oPgFrm.Page1.oPag.oXXSEARMN_1_12.enabled = this.oPgFrm.Page1.oPag.oXXSEARMN_1_12.mCond()
    this.oPgFrm.Page1.oPag.oXXSEARM1_1_13.enabled = this.oPgFrm.Page1.oPag.oXXSEARM1_1_13.mCond()
    this.oPgFrm.Page1.oPag.oXXSEARM2_1_14.enabled = this.oPgFrm.Page1.oPag.oXXSEARM2_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCIFRMPAG_1_16.enabled = this.oPgFrm.Page1.oPag.oCIFRMPAG_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCIMENIMM_1_18.enabled = this.oPgFrm.Page1.oPag.oCIMENIMM_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCINEWPRI_1_19.enabled = this.oPgFrm.Page1.oPag.oCINEWPRI_1_19.mCond()
    this.oPgFrm.Page1.oPag.oCIMONFRA_1_20.enabled = this.oPgFrm.Page1.oPag.oCIMONFRA_1_20.mCond()
    this.oPgFrm.Page5.oPag.oSETDAT_5_18.enabled = this.oPgFrm.Page5.oPag.oSETDAT_5_18.mCond()
    this.oPgFrm.Page5.oPag.oSETMRK_5_19.enabled = this.oPgFrm.Page5.oPag.oSETMRK_5_19.mCond()
    this.oPgFrm.Page5.oPag.oSETCEN_5_20.enabled = this.oPgFrm.Page5.oPag.oSETCEN_5_20.mCond()
    this.oPgFrm.Page5.oPag.oSETPNT_5_21.enabled = this.oPgFrm.Page5.oPag.oSETPNT_5_21.mCond()
    this.oPgFrm.Page5.oPag.oSETSEP_5_22.enabled = this.oPgFrm.Page5.oPag.oSETSEP_5_22.mCond()
    this.oPgFrm.Page7.oPag.oCIDSKMEN_7_1.enabled = this.oPgFrm.Page7.oPag.oCIDSKMEN_7_1.mCond()
    this.oPgFrm.Page2.oPag.oCITABMEN_2_2.enabled = this.oPgFrm.Page2.oPag.oCITABMEN_2_2.mCond()
    this.oPgFrm.Page2.oPag.oCIBCKGRD_2_5.enabled = this.oPgFrm.Page2.oPag.oCIBCKGRD_2_5.mCond()
    this.oPgFrm.Page2.oPag.oCICTRGRD_2_12.enabled = this.oPgFrm.Page2.oPag.oCICTRGRD_2_12.mCond()
    this.oPgFrm.Page2.oPag.oCIWIDFRM_2_44.enabled = this.oPgFrm.Page2.oPag.oCIWIDFRM_2_44.mCond()
    this.oPgFrm.Page2.oPag.oCIHEIFRM_2_45.enabled = this.oPgFrm.Page2.oPag.oCIHEIFRM_2_45.mCond()
    this.oPgFrm.Page1.oPag.oCISEAMNW_1_47.enabled = this.oPgFrm.Page1.oPag.oCISEAMNW_1_47.mCond()
    this.oPgFrm.Page1.oPag.oCISEADKM_1_48.enabled = this.oPgFrm.Page1.oPag.oCISEADKM_1_48.mCond()
    this.oPgFrm.Page1.oPag.oCISEAAPB_1_49.enabled = this.oPgFrm.Page1.oPag.oCISEAAPB_1_49.mCond()
    this.oPgFrm.Page1.oPag.oCINRECEN_1_52.enabled = this.oPgFrm.Page1.oPag.oCINRECEN_1_52.mCond()
    this.oPgFrm.Page2.oPag.oCISAVFRM_2_51.enabled = this.oPgFrm.Page2.oPag.oCISAVFRM_2_51.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_38.enabled = this.oPgFrm.Page3.oPag.oBtn_3_38.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(6).enabled=not(g_AGEN='N')
    this.oPgFrm.Pages(7).enabled=not(this.w_CIGADGET='S')
    this.oPgFrm.Page1.oPag.oCISTASCR_1_7.visible=!this.oPgFrm.Page1.oPag.oCISTASCR_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCINEWPRI_1_19.visible=!this.oPgFrm.Page1.oPag.oCINEWPRI_1_19.mHide()
    this.oPgFrm.Page1.oPag.oCIMONFRA_1_20.visible=!this.oPgFrm.Page1.oPag.oCIMONFRA_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCI_ILIKE_1_21.visible=!this.oPgFrm.Page1.oPag.oCI_ILIKE_1_21.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_38.visible=!this.oPgFrm.Page3.oPag.oBtn_3_38.mHide()
    this.oPgFrm.Page3.oPag.oCOLORZOM_3_58.visible=!this.oPgFrm.Page3.oPag.oCOLORZOM_3_58.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_59.visible=!this.oPgFrm.Page3.oPag.oStr_3_59.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_26.visible=!this.oPgFrm.Page5.oPag.oStr_5_26.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_20.visible=!this.oPgFrm.Page7.oPag.oStr_7_20.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_21.visible=!this.oPgFrm.Page7.oPag.oStr_7_21.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_22.visible=!this.oPgFrm.Page7.oPag.oStr_7_22.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_27.visible=!this.oPgFrm.Page5.oPag.oStr_5_27.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_25.visible=!this.oPgFrm.Page2.oPag.oStr_2_25.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_26.visible=!this.oPgFrm.Page2.oPag.oStr_2_26.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_27.visible=!this.oPgFrm.Page2.oPag.oStr_2_27.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_28.visible=!this.oPgFrm.Page2.oPag.oStr_2_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oCISEARNT_1_45.visible=!this.oPgFrm.Page1.oPag.oCISEARNT_1_45.mHide()
    this.oPgFrm.Page1.oPag.oCISEARNT_1_46.visible=!this.oPgFrm.Page1.oPag.oCISEARNT_1_46.mHide()
    this.oPgFrm.Page1.oPag.oCISEAMNW_1_47.visible=!this.oPgFrm.Page1.oPag.oCISEAMNW_1_47.mHide()
    this.oPgFrm.Page1.oPag.oCISEADKM_1_48.visible=!this.oPgFrm.Page1.oPag.oCISEADKM_1_48.mHide()
    this.oPgFrm.Page1.oPag.oCISEAAPB_1_49.visible=!this.oPgFrm.Page1.oPag.oCISEAAPB_1_49.mHide()
    this.oPgFrm.Page1.oPag.oCINUMREC_1_51.visible=!this.oPgFrm.Page1.oPag.oCINUMREC_1_51.mHide()
    this.oPgFrm.Page1.oPag.oCINRECEN_1_52.visible=!this.oPgFrm.Page1.oPag.oCINRECEN_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page8.oPag.oXXB1_8_2.visible=!this.oPgFrm.Page8.oPag.oXXB1_8_2.mHide()
    this.oPgFrm.Page8.oPag.oXXB2_8_3.visible=!this.oPgFrm.Page8.oPag.oXXB2_8_3.mHide()
    this.oPgFrm.Page8.oPag.oXXB3_8_4.visible=!this.oPgFrm.Page8.oPag.oXXB3_8_4.mHide()
    this.oPgFrm.Page8.oPag.oXXB4_8_5.visible=!this.oPgFrm.Page8.oPag.oXXB4_8_5.mHide()
    this.oPgFrm.Page8.oPag.oXXB5_8_6.visible=!this.oPgFrm.Page8.oPag.oXXB5_8_6.mHide()
    this.oPgFrm.Page8.oPag.oXXB17_8_7.visible=!this.oPgFrm.Page8.oPag.oXXB17_8_7.mHide()
    this.oPgFrm.Page8.oPag.oXXB6_8_8.visible=!this.oPgFrm.Page8.oPag.oXXB6_8_8.mHide()
    this.oPgFrm.Page8.oPag.oXXB7_8_9.visible=!this.oPgFrm.Page8.oPag.oXXB7_8_9.mHide()
    this.oPgFrm.Page8.oPag.oXXB8_8_10.visible=!this.oPgFrm.Page8.oPag.oXXB8_8_10.mHide()
    this.oPgFrm.Page8.oPag.oXXB9_8_11.visible=!this.oPgFrm.Page8.oPag.oXXB9_8_11.mHide()
    this.oPgFrm.Page8.oPag.oXXB10_8_12.visible=!this.oPgFrm.Page8.oPag.oXXB10_8_12.mHide()
    this.oPgFrm.Page8.oPag.oXXB11_8_13.visible=!this.oPgFrm.Page8.oPag.oXXB11_8_13.mHide()
    this.oPgFrm.Page8.oPag.oXXB12_8_14.visible=!this.oPgFrm.Page8.oPag.oXXB12_8_14.mHide()
    this.oPgFrm.Page8.oPag.oXXB13_8_15.visible=!this.oPgFrm.Page8.oPag.oXXB13_8_15.mHide()
    this.oPgFrm.Page8.oPag.oXXB14_8_16.visible=!this.oPgFrm.Page8.oPag.oXXB14_8_16.mHide()
    this.oPgFrm.Page8.oPag.oXXB15_8_17.visible=!this.oPgFrm.Page8.oPag.oXXB15_8_17.mHide()
    this.oPgFrm.Page8.oPag.oXXB16_8_18.visible=!this.oPgFrm.Page8.oPag.oXXB16_8_18.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("Load")
          .Calculate_UVGFCTLNGG()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.oObj_3_11.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_14.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_18.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_19.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_24.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_25.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_51.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_53.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_55.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_61.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_ZFZIGBWSFI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_KYSQZSCIFE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_SETDAT Changed")
          .Calculate_JGOPDVKLEQ()
          bRefresh=.t.
        endif
      .oPgFrm.Page6.oPag.oObj_6_18.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_23.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_24.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_37.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_38.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_39.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_40.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_56.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_67.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_PCBJJRGQUD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_BFUVDZNLRH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_FYJERJTZSW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("SetDefault")
          .Calculate_DHIICYARBX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Load") or lower(cEvent)==lower("SetDefault")
          .Calculate_ZILJFIUKSR()
          bRefresh=.t.
        endif
      .oPgFrm.Page8.oPag.XXB1IM.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_LOBEFNFCFQ()
          bRefresh=.t.
        endif
      .oPgFrm.Page8.oPag.XXB4IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB3IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB2IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB5IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB61IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB17IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB62IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB7IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB92IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB91IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB82IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB81IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB101IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB102IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB111IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB112IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB12IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB13IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB14IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB15IM.Event(cEvent)
      .oPgFrm.Page8.oPag.XXB16IM.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_59.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
        if lower(cEvent)==lower("After BCI")
          .Calculate_HYGFWRPMZU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCICPTBAR_1_1.RadioValue()==this.w_CICPTBAR)
      this.oPgFrm.Page1.oPag.oCICPTBAR_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIDSKBAR_1_2.RadioValue()==this.w_CIDSKBAR)
      this.oPgFrm.Page1.oPag.oCIDSKBAR_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCITOOBDS_1_3.RadioValue()==this.w_CITOOBDS)
      this.oPgFrm.Page1.oPag.oCITOOBDS_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCITBSIZE_1_4.RadioValue()==this.w_CITBSIZE)
      this.oPgFrm.Page1.oPag.oCITBSIZE_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCISTABAR_1_5.RadioValue()==this.w_CISTABAR)
      this.oPgFrm.Page1.oPag.oCISTABAR_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCISHWMEN_1_6.RadioValue()==this.w_CISHWMEN)
      this.oPgFrm.Page1.oPag.oCISHWMEN_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCISTASCR_1_7.RadioValue()==this.w_CISTASCR)
      this.oPgFrm.Page1.oPag.oCISTASCR_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIMENFIX_1_8.RadioValue()==this.w_CIMENFIX)
      this.oPgFrm.Page1.oPag.oCIMENFIX_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIWNDMEN_1_9.RadioValue()==this.w_CIWNDMEN)
      this.oPgFrm.Page1.oPag.oCIWNDMEN_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oXXSEARMN_1_12.RadioValue()==this.w_XXSEARMN)
      this.oPgFrm.Page1.oPag.oXXSEARMN_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oXXSEARM1_1_13.RadioValue()==this.w_XXSEARM1)
      this.oPgFrm.Page1.oPag.oXXSEARM1_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oXXSEARM2_1_14.RadioValue()==this.w_XXSEARM2)
      this.oPgFrm.Page1.oPag.oXXSEARM2_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIFRMBUT_1_15.RadioValue()==this.w_CIFRMBUT)
      this.oPgFrm.Page1.oPag.oCIFRMBUT_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIFRMPAG_1_16.RadioValue()==this.w_CIFRMPAG)
      this.oPgFrm.Page1.oPag.oCIFRMPAG_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCITOOLMN_1_17.RadioValue()==this.w_CITOOLMN)
      this.oPgFrm.Page1.oPag.oCITOOLMN_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIMENIMM_1_18.RadioValue()==this.w_CIMENIMM)
      this.oPgFrm.Page1.oPag.oCIMENIMM_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCINEWPRI_1_19.RadioValue()==this.w_CINEWPRI)
      this.oPgFrm.Page1.oPag.oCINEWPRI_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIMONFRA_1_20.RadioValue()==this.w_CIMONFRA)
      this.oPgFrm.Page1.oPag.oCIMONFRA_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCI_ILIKE_1_21.RadioValue()==this.w_CI_ILIKE)
      this.oPgFrm.Page1.oPag.oCI_ILIKE_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIMINPST_1_22.value==this.w_CIMINPST)
      this.oPgFrm.Page1.oPag.oCIMINPST_1_22.value=this.w_CIMINPST
    endif
    if not(this.oPgFrm.Page1.oPag.oCIMODPST_1_23.RadioValue()==this.w_CIMODPST)
      this.oPgFrm.Page1.oPag.oCIMODPST_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCOLORCTOB_3_16.value==this.w_COLORCTOB)
      this.oPgFrm.Page3.oPag.oCOLORCTOB_3_16.value=this.w_COLORCTOB
    endif
    if not(this.oPgFrm.Page3.oPag.oCOLORCTED_3_20.value==this.w_COLORCTED)
      this.oPgFrm.Page3.oPag.oCOLORCTED_3_20.value=this.w_COLORCTED
    endif
    if not(this.oPgFrm.Page3.oPag.oCOLORCTSEL_3_21.value==this.w_COLORCTSEL)
      this.oPgFrm.Page3.oPag.oCOLORCTSEL_3_21.value=this.w_COLORCTSEL
    endif
    if not(this.oPgFrm.Page3.oPag.oCOLORFCTDSBL_3_26.value==this.w_COLORFCTDSBL)
      this.oPgFrm.Page3.oPag.oCOLORFCTDSBL_3_26.value=this.w_COLORFCTDSBL
    endif
    if not(this.oPgFrm.Page3.oPag.oCOLORBCTDSBL_3_27.value==this.w_COLORBCTDSBL)
      this.oPgFrm.Page3.oPag.oCOLORBCTDSBL_3_27.value=this.w_COLORBCTDSBL
    endif
    if not(this.oPgFrm.Page3.oPag.oCOLORCTRI_3_33.value==this.w_COLORCTRI)
      this.oPgFrm.Page3.oPag.oCOLORCTRI_3_33.value=this.w_COLORCTRI
    endif
    if not(this.oPgFrm.Page3.oPag.oCIEVIZOM_3_37.RadioValue()==this.w_CIEVIZOM)
      this.oPgFrm.Page3.oPag.oCIEVIZOM_3_37.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCOLORGRD_3_40.value==this.w_COLORGRD)
      this.oPgFrm.Page3.oPag.oCOLORGRD_3_40.value=this.w_COLORGRD
    endif
    if not(this.oPgFrm.Page3.oPag.oCOLORSCREEN_3_43.value==this.w_COLORSCREEN)
      this.oPgFrm.Page3.oPag.oCOLORSCREEN_3_43.value=this.w_COLORSCREEN
    endif
    if not(this.oPgFrm.Page3.oPag.oCOLORROWDTL_3_49.value==this.w_COLORROWDTL)
      this.oPgFrm.Page3.oPag.oCOLORROWDTL_3_49.value=this.w_COLORROWDTL
    endif
    if not(this.oPgFrm.Page3.oPag.oCOLORZOM_3_58.value==this.w_COLORZOM)
      this.oPgFrm.Page3.oPag.oCOLORZOM_3_58.value=this.w_COLORZOM
    endif
    if not(this.oPgFrm.Page4.oPag.oCILBLFNM_4_1.RadioValue()==this.w_CILBLFNM)
      this.oPgFrm.Page4.oPag.oCILBLFNM_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCITXTFNM_4_2.RadioValue()==this.w_CITXTFNM)
      this.oPgFrm.Page4.oPag.oCITXTFNM_4_2.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCICBXFNM_4_3.RadioValue()==this.w_CICBXFNM)
      this.oPgFrm.Page4.oPag.oCICBXFNM_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCIBTNFNM_4_4.RadioValue()==this.w_CIBTNFNM)
      this.oPgFrm.Page4.oPag.oCIBTNFNM_4_4.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCIGRDFNM_4_5.RadioValue()==this.w_CIGRDFNM)
      this.oPgFrm.Page4.oPag.oCIGRDFNM_4_5.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCIPAGFNM_4_6.RadioValue()==this.w_CIPAGFNM)
      this.oPgFrm.Page4.oPag.oCIPAGFNM_4_6.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCILBLFSZ_4_13.value==this.w_CILBLFSZ)
      this.oPgFrm.Page4.oPag.oCILBLFSZ_4_13.value=this.w_CILBLFSZ
    endif
    if not(this.oPgFrm.Page4.oPag.oCITXTFSZ_4_15.value==this.w_CITXTFSZ)
      this.oPgFrm.Page4.oPag.oCITXTFSZ_4_15.value=this.w_CITXTFSZ
    endif
    if not(this.oPgFrm.Page4.oPag.oCICBXFSZ_4_17.value==this.w_CICBXFSZ)
      this.oPgFrm.Page4.oPag.oCICBXFSZ_4_17.value=this.w_CICBXFSZ
    endif
    if not(this.oPgFrm.Page4.oPag.oCIBTNFSZ_4_19.value==this.w_CIBTNFSZ)
      this.oPgFrm.Page4.oPag.oCIBTNFSZ_4_19.value=this.w_CIBTNFSZ
    endif
    if not(this.oPgFrm.Page4.oPag.oCIGRDFSZ_4_21.value==this.w_CIGRDFSZ)
      this.oPgFrm.Page4.oPag.oCIGRDFSZ_4_21.value=this.w_CIGRDFSZ
    endif
    if not(this.oPgFrm.Page4.oPag.oCIPAGFSZ_4_23.value==this.w_CIPAGFSZ)
      this.oPgFrm.Page4.oPag.oCIPAGFSZ_4_23.value=this.w_CIPAGFSZ
    endif
    if not(this.oPgFrm.Page5.oPag.oCISYSFRM_5_1.RadioValue()==this.w_CISYSFRM)
      this.oPgFrm.Page5.oPag.oCISYSFRM_5_1.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPRVNUM_5_3.value==this.w_PRVNUM)
      this.oPgFrm.Page5.oPag.oPRVNUM_5_3.value=this.w_PRVNUM
    endif
    if not(this.oPgFrm.Page5.oPag.oPRVDATA_5_4.value==this.w_PRVDATA)
      this.oPgFrm.Page5.oPag.oPRVDATA_5_4.value=this.w_PRVDATA
    endif
    if not(this.oPgFrm.Page5.oPag.oSETDAT_5_18.RadioValue()==this.w_SETDAT)
      this.oPgFrm.Page5.oPag.oSETDAT_5_18.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oSETMRK_5_19.value==this.w_SETMRK)
      this.oPgFrm.Page5.oPag.oSETMRK_5_19.value=this.w_SETMRK
    endif
    if not(this.oPgFrm.Page5.oPag.oSETCEN_5_20.RadioValue()==this.w_SETCEN)
      this.oPgFrm.Page5.oPag.oSETCEN_5_20.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oSETPNT_5_21.RadioValue()==this.w_SETPNT)
      this.oPgFrm.Page5.oPag.oSETPNT_5_21.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oSETSEP_5_22.RadioValue()==this.w_SETSEP)
      this.oPgFrm.Page5.oPag.oSETSEP_5_22.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCOLORATU_6_16.value==this.w_COLORATU)
      this.oPgFrm.Page6.oPag.oCOLORATU_6_16.value=this.w_COLORATU
    endif
    if not(this.oPgFrm.Page6.oPag.oCOLORATT_6_19.value==this.w_COLORATT)
      this.oPgFrm.Page6.oPag.oCOLORATT_6_19.value=this.w_COLORATT
    endif
    if not(this.oPgFrm.Page6.oPag.oCOLORATC_6_21.value==this.w_COLORATC)
      this.oPgFrm.Page6.oPag.oCOLORATC_6_21.value=this.w_COLORATC
    endif
    if not(this.oPgFrm.Page6.oPag.oCIAGEVIS_6_25.RadioValue()==this.w_CIAGEVIS)
      this.oPgFrm.Page6.oPag.oCIAGEVIS_6_25.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCOLORPRM_6_33.value==this.w_COLORPRM)
      this.oPgFrm.Page6.oPag.oCOLORPRM_6_33.value=this.w_COLORPRM
    endif
    if not(this.oPgFrm.Page6.oPag.oCOLORNNP_6_34.value==this.w_COLORNNP)
      this.oPgFrm.Page6.oPag.oCOLORNNP_6_34.value=this.w_COLORNNP
    endif
    if not(this.oPgFrm.Page6.oPag.oCOLORHDR_6_35.value==this.w_COLORHDR)
      this.oPgFrm.Page6.oPag.oCOLORHDR_6_35.value=this.w_COLORHDR
    endif
    if not(this.oPgFrm.Page6.oPag.oCOLORSEL_6_36.value==this.w_COLORSEL)
      this.oPgFrm.Page6.oPag.oCOLORSEL_6_36.value=this.w_COLORSEL
    endif
    if not(this.oPgFrm.Page6.oPag.oCIORAINI_6_44.RadioValue()==this.w_CIORAINI)
      this.oPgFrm.Page6.oPag.oCIORAINI_6_44.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCIORAFIN_6_45.RadioValue()==this.w_CIORAFIN)
      this.oPgFrm.Page6.oPag.oCIORAFIN_6_45.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCISTAINI_6_46.RadioValue()==this.w_CISTAINI)
      this.oPgFrm.Page6.oPag.oCISTAINI_6_46.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCISTAFIN_6_47.RadioValue()==this.w_CISTAFIN)
      this.oPgFrm.Page6.oPag.oCISTAFIN_6_47.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCITYPBAL_6_48.RadioValue()==this.w_CITYPBAL)
      this.oPgFrm.Page6.oPag.oCITYPBAL_6_48.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCOLORACF_6_53.value==this.w_COLORACF)
      this.oPgFrm.Page6.oPag.oCOLORACF_6_53.value=this.w_COLORACF
    endif
    if not(this.oPgFrm.Page7.oPag.oCIDSKMEN_7_1.RadioValue()==this.w_CIDSKMEN)
      this.oPgFrm.Page7.oPag.oCIDSKMEN_7_1.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oCINAVSTA_7_2.RadioValue()==this.w_CINAVSTA)
      this.oPgFrm.Page7.oPag.oCINAVSTA_7_2.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oCINAVNUB_7_3.RadioValue()==this.w_CINAVNUB)
      this.oPgFrm.Page7.oPag.oCINAVNUB_7_3.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oCINAVDIM_7_4.value==this.w_CINAVDIM)
      this.oPgFrm.Page7.oPag.oCINAVDIM_7_4.value=this.w_CINAVDIM
    endif
    if not(this.oPgFrm.Page7.oPag.oCIMNAFNM_7_5.RadioValue()==this.w_CIMNAFNM)
      this.oPgFrm.Page7.oPag.oCIMNAFNM_7_5.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oCIMNAFSZ_7_6.value==this.w_CIMNAFSZ)
      this.oPgFrm.Page7.oPag.oCIMNAFSZ_7_6.value=this.w_CIMNAFSZ
    endif
    if not(this.oPgFrm.Page7.oPag.oCIWMAFNM_7_7.RadioValue()==this.w_CIWMAFNM)
      this.oPgFrm.Page7.oPag.oCIWMAFNM_7_7.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oCIWMAFSZ_7_8.value==this.w_CIWMAFSZ)
      this.oPgFrm.Page7.oPag.oCIWMAFSZ_7_8.value=this.w_CIWMAFSZ
    endif
    if not(this.oPgFrm.Page7.oPag.oCIOPNGST_7_17.value==this.w_CIOPNGST)
      this.oPgFrm.Page7.oPag.oCIOPNGST_7_17.value=this.w_CIOPNGST
    endif
    if not(this.oPgFrm.Page3.oPag.oCOLORPOSTIN_3_65.value==this.w_COLORPOSTIN)
      this.oPgFrm.Page3.oPag.oCOLORPOSTIN_3_65.value=this.w_COLORPOSTIN
    endif
    if not(this.oPgFrm.Page6.oPag.oCIFLCOMP_6_59.RadioValue()==this.w_CIFLCOMP)
      this.oPgFrm.Page6.oPag.oCIFLCOMP_6_59.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oCIDSKRSS_7_19.RadioValue()==this.w_CIDSKRSS)
      this.oPgFrm.Page7.oPag.oCIDSKRSS_7_19.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCILBLFIT_4_25.RadioValue()==this.w_CILBLFIT)
      this.oPgFrm.Page4.oPag.oCILBLFIT_4_25.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCITXTFIT_4_26.RadioValue()==this.w_CITXTFIT)
      this.oPgFrm.Page4.oPag.oCITXTFIT_4_26.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCICBXFIT_4_27.RadioValue()==this.w_CICBXFIT)
      this.oPgFrm.Page4.oPag.oCICBXFIT_4_27.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCIBTNFIT_4_28.RadioValue()==this.w_CIBTNFIT)
      this.oPgFrm.Page4.oPag.oCIBTNFIT_4_28.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCIGRDFIT_4_29.RadioValue()==this.w_CIGRDFIT)
      this.oPgFrm.Page4.oPag.oCIGRDFIT_4_29.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCIPAGFIT_4_30.RadioValue()==this.w_CIPAGFIT)
      this.oPgFrm.Page4.oPag.oCIPAGFIT_4_30.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCILBLFBO_4_31.RadioValue()==this.w_CILBLFBO)
      this.oPgFrm.Page4.oPag.oCILBLFBO_4_31.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCITXTFBO_4_32.RadioValue()==this.w_CITXTFBO)
      this.oPgFrm.Page4.oPag.oCITXTFBO_4_32.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCICBXFBO_4_33.RadioValue()==this.w_CICBXFBO)
      this.oPgFrm.Page4.oPag.oCICBXFBO_4_33.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCIBTNFBO_4_34.RadioValue()==this.w_CIBTNFBO)
      this.oPgFrm.Page4.oPag.oCIBTNFBO_4_34.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCIGRDFBO_4_35.RadioValue()==this.w_CIGRDFBO)
      this.oPgFrm.Page4.oPag.oCIGRDFBO_4_35.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCIPAGFBO_4_36.RadioValue()==this.w_CIPAGFBO)
      this.oPgFrm.Page4.oPag.oCIPAGFBO_4_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIMDIFRM_2_1.RadioValue()==this.w_CIMDIFRM)
      this.oPgFrm.Page2.oPag.oCIMDIFRM_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCITABMEN_2_2.RadioValue()==this.w_CITABMEN)
      this.oPgFrm.Page2.oPag.oCITABMEN_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIXPTHEM_2_3.RadioValue()==this.w_CIXPTHEM)
      this.oPgFrm.Page2.oPag.oCIXPTHEM_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIRICONT_2_4.RadioValue()==this.w_CIRICONT)
      this.oPgFrm.Page2.oPag.oCIRICONT_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIBCKGRD_2_5.RadioValue()==this.w_CIBCKGRD)
      this.oPgFrm.Page2.oPag.oCIBCKGRD_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIPROBAR_2_6.RadioValue()==this.w_CIPROBAR)
      this.oPgFrm.Page2.oPag.oCIPROBAR_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCI_COLON_2_7.RadioValue()==this.w_CI_COLON)
      this.oPgFrm.Page2.oPag.oCI_COLON_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCINOBTNI_2_8.RadioValue()==this.w_CINOBTNI)
      this.oPgFrm.Page2.oPag.oCINOBTNI_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.ociwaitwd_2_9.RadioValue()==this.w_ciwaitwd)
      this.oPgFrm.Page2.oPag.ociwaitwd_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCISHWBTN_2_10.RadioValue()==this.w_CISHWBTN)
      this.oPgFrm.Page2.oPag.oCISHWBTN_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIMRKGRD_2_11.RadioValue()==this.w_CIMRKGRD)
      this.oPgFrm.Page2.oPag.oCIMRKGRD_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCICTRGRD_2_12.RadioValue()==this.w_CICTRGRD)
      this.oPgFrm.Page2.oPag.oCICTRGRD_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIHEHEZO_2_13.value==this.w_CIHEHEZO)
      this.oPgFrm.Page2.oPag.oCIHEHEZO_2_13.value=this.w_CIHEHEZO
    endif
    if not(this.oPgFrm.Page2.oPag.oCIHEROZO_2_14.value==this.w_CIHEROZO)
      this.oPgFrm.Page2.oPag.oCIHEROZO_2_14.value=this.w_CIHEROZO
    endif
    if not(this.oPgFrm.Page2.oPag.oCIZOESPR_2_15.RadioValue()==this.w_CIZOESPR)
      this.oPgFrm.Page2.oPag.oCIZOESPR_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIDSPCNT_2_16.value==this.w_CIDSPCNT)
      this.oPgFrm.Page2.oPag.oCIDSPCNT_2_16.value=this.w_CIDSPCNT
    endif
    if not(this.oPgFrm.Page2.oPag.oCIWARTYP_2_35.RadioValue()==this.w_CIWARTYP)
      this.oPgFrm.Page2.oPag.oCIWARTYP_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIZOOMMO_2_37.RadioValue()==this.w_CIZOOMMO)
      this.oPgFrm.Page2.oPag.oCIZOOMMO_2_37.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCIAGSFNM_6_60.RadioValue()==this.w_CIAGSFNM)
      this.oPgFrm.Page6.oPag.oCIAGSFNM_6_60.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCIAGSFSZ_6_62.value==this.w_CIAGSFSZ)
      this.oPgFrm.Page6.oPag.oCIAGSFSZ_6_62.value=this.w_CIAGSFSZ
    endif
    if not(this.oPgFrm.Page6.oPag.oCIAGSFIT_6_64.RadioValue()==this.w_CIAGSFIT)
      this.oPgFrm.Page6.oPag.oCIAGSFIT_6_64.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCIAGSFBO_6_65.RadioValue()==this.w_CIAGSFBO)
      this.oPgFrm.Page6.oPag.oCIAGSFBO_6_65.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCIAGIFNM_6_68.RadioValue()==this.w_CIAGIFNM)
      this.oPgFrm.Page6.oPag.oCIAGIFNM_6_68.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCIAGIFSZ_6_70.value==this.w_CIAGIFSZ)
      this.oPgFrm.Page6.oPag.oCIAGIFSZ_6_70.value=this.w_CIAGIFSZ
    endif
    if not(this.oPgFrm.Page6.oPag.oCIAGIFIT_6_72.RadioValue()==this.w_CIAGIFIT)
      this.oPgFrm.Page6.oPag.oCIAGIFIT_6_72.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCIAGIFBO_6_73.RadioValue()==this.w_CIAGIFBO)
      this.oPgFrm.Page6.oPag.oCIAGIFBO_6_73.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCIGIORHO_6_74.value==this.w_CIGIORHO)
      this.oPgFrm.Page6.oPag.oCIGIORHO_6_74.value=this.w_CIGIORHO
    endif
    if not(this.oPgFrm.Page2.oPag.oCIZOOMSC_2_39.RadioValue()==this.w_CIZOOMSC)
      this.oPgFrm.Page2.oPag.oCIZOOMSC_2_39.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIDIMFRM_2_42.RadioValue()==this.w_CIDIMFRM)
      this.oPgFrm.Page2.oPag.oCIDIMFRM_2_42.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIWIDFRM_2_44.value==this.w_CIWIDFRM)
      this.oPgFrm.Page2.oPag.oCIWIDFRM_2_44.value=this.w_CIWIDFRM
    endif
    if not(this.oPgFrm.Page2.oPag.oCIHEIFRM_2_45.value==this.w_CIHEIFRM)
      this.oPgFrm.Page2.oPag.oCIHEIFRM_2_45.value=this.w_CIHEIFRM
    endif
    if not(this.oPgFrm.Page1.oPag.oCISEARNT_1_45.RadioValue()==this.w_CISEARNT)
      this.oPgFrm.Page1.oPag.oCISEARNT_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCISEARNT_1_46.RadioValue()==this.w_CISEARNT)
      this.oPgFrm.Page1.oPag.oCISEARNT_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCISEAMNW_1_47.RadioValue()==this.w_CISEAMNW)
      this.oPgFrm.Page1.oPag.oCISEAMNW_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCISEADKM_1_48.RadioValue()==this.w_CISEADKM)
      this.oPgFrm.Page1.oPag.oCISEADKM_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCISEAAPB_1_49.RadioValue()==this.w_CISEAAPB)
      this.oPgFrm.Page1.oPag.oCISEAAPB_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCINUMREC_1_51.value==this.w_CINUMREC)
      this.oPgFrm.Page1.oPag.oCINUMREC_1_51.value=this.w_CINUMREC
    endif
    if not(this.oPgFrm.Page1.oPag.oCINRECEN_1_52.value==this.w_CINRECEN)
      this.oPgFrm.Page1.oPag.oCINRECEN_1_52.value=this.w_CINRECEN
    endif
    if not(this.oPgFrm.Page2.oPag.oCITYPCAL_2_49.RadioValue()==this.w_CITYPCAL)
      this.oPgFrm.Page2.oPag.oCITYPCAL_2_49.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCISAVFRM_2_51.RadioValue()==this.w_CISAVFRM)
      this.oPgFrm.Page2.oPag.oCISAVFRM_2_51.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB1_8_2.RadioValue()==this.w_XXB1)
      this.oPgFrm.Page8.oPag.oXXB1_8_2.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB2_8_3.RadioValue()==this.w_XXB2)
      this.oPgFrm.Page8.oPag.oXXB2_8_3.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB3_8_4.RadioValue()==this.w_XXB3)
      this.oPgFrm.Page8.oPag.oXXB3_8_4.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB4_8_5.RadioValue()==this.w_XXB4)
      this.oPgFrm.Page8.oPag.oXXB4_8_5.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB5_8_6.RadioValue()==this.w_XXB5)
      this.oPgFrm.Page8.oPag.oXXB5_8_6.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB17_8_7.RadioValue()==this.w_XXB17)
      this.oPgFrm.Page8.oPag.oXXB17_8_7.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB6_8_8.RadioValue()==this.w_XXB6)
      this.oPgFrm.Page8.oPag.oXXB6_8_8.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB7_8_9.RadioValue()==this.w_XXB7)
      this.oPgFrm.Page8.oPag.oXXB7_8_9.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB8_8_10.RadioValue()==this.w_XXB8)
      this.oPgFrm.Page8.oPag.oXXB8_8_10.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB9_8_11.RadioValue()==this.w_XXB9)
      this.oPgFrm.Page8.oPag.oXXB9_8_11.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB10_8_12.RadioValue()==this.w_XXB10)
      this.oPgFrm.Page8.oPag.oXXB10_8_12.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB11_8_13.RadioValue()==this.w_XXB11)
      this.oPgFrm.Page8.oPag.oXXB11_8_13.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB12_8_14.RadioValue()==this.w_XXB12)
      this.oPgFrm.Page8.oPag.oXXB12_8_14.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB13_8_15.RadioValue()==this.w_XXB13)
      this.oPgFrm.Page8.oPag.oXXB13_8_15.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB14_8_16.RadioValue()==this.w_XXB14)
      this.oPgFrm.Page8.oPag.oXXB14_8_16.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB15_8_17.RadioValue()==this.w_XXB15)
      this.oPgFrm.Page8.oPag.oXXB15_8_17.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oXXB16_8_18.RadioValue()==this.w_XXB16)
      this.oPgFrm.Page8.oPag.oXXB16_8_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIVTHEME_1_58.RadioValue()==this.w_CIVTHEME)
      this.oPgFrm.Page1.oPag.oCIVTHEME_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIGADGET_1_61.RadioValue()==this.w_CIGADGET)
      this.oPgFrm.Page1.oPag.oCIGADGET_1_61.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'CONF_INT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(!Empty(.w_CILBLFNM) and TestFont('L', .w_CILBLFNM, .w_CILBLFSZ)<>-1 And !Empty(.w_CILBLFSZ))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCILBLFSZ_4_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CITXTFNM) and TestFont('T', .w_CITXTFNM, .w_CITXTFSZ)<>-1 And !Empty(.w_CITXTFSZ))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCITXTFSZ_4_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CICBXFNM) and TestFont('C', .w_CICBXFNM, .w_CICBXFSZ)<>-1 And !Empty(.w_CICBXFSZ))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCICBXFSZ_4_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CIBTNFNM) and TestFont('B', .w_CIBTNFNM, .w_CIBTNFSZ)<>-1 And !Empty(.w_CIBTNFSZ))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCIBTNFSZ_4_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CIGRDFNM) and TestFont('G', .w_CIGRDFNM, .w_CIGRDFSZ)<>-1 And !Empty(.w_CIGRDFSZ))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCIGRDFSZ_4_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CIPAGFNM) and TestFont('P', .w_CIPAGFNM, .w_CIPAGFSZ)<>-1 And !Empty(.w_CIPAGFSZ))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCIPAGFSZ_4_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CIMNAFNM) and TestFont('L', .w_CIMNAFNM, .w_CIMNAFSZ)<>-1 And !Empty(.w_CIMNAFSZ))
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oCIMNAFSZ_7_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(!Empty(.w_CIWMAFNM) and TestFont('L', .w_CIWMAFNM, .w_CIWMAFSZ)<>-1 And !Empty(.w_CIWMAFSZ))
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oCIWMAFSZ_7_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione del font non supportata")
          case   not(.w_CIHEHEZO > 0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCIHEHEZO_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire l'altezza della testata dello zoom maggiore di zero")
          case   not(.w_CIHEROZO > 0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCIHEROZO_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire l'altezza della righe dello zoom maggiore di zero")
          case   not(.w_CIDSPCNT > 0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCIDSPCNT_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire numero righe maggiore di zero")
          case   not(.w_CIWIDFRM>0)  and (.w_CIDIMFRM='D')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCIWIDFRM_2_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dimensione non valida")
          case   not(.w_CIHEIFRM>0)  and (.w_CIDIMFRM='D')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCIHEIFRM_2_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CINUMREC>=0)  and not(isalt() OR .w_CISEARNT<>"S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCINUMREC_1_51.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CINRECEN>=0)  and not(.w_CISEARNT<>"S" or .w_CINUMREC=0 and not isalt())  and (.w_CISEARNT="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCINRECEN_1_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CISHWMEN = this.w_CISHWMEN
    this.o_XXSEARMN = this.w_XXSEARMN
    this.o_XXSEARM1 = this.w_XXSEARM1
    this.o_XXSEARM2 = this.w_XXSEARM2
    this.o_CIFRMBUT = this.w_CIFRMBUT
    this.o_CISYSFRM = this.w_CISYSFRM
    this.o_CISETCEN = this.w_CISETCEN
    this.o_CISETDAT = this.w_CISETDAT
    this.o_CISETPNT = this.w_CISETPNT
    this.o_CISETSEP = this.w_CISETSEP
    this.o_CISETMRK = this.w_CISETMRK
    this.o_SETDAT = this.w_SETDAT
    this.o_SETMRK = this.w_SETMRK
    this.o_SETCEN = this.w_SETCEN
    this.o_SETPNT = this.w_SETPNT
    this.o_SETSEP = this.w_SETSEP
    this.o_CIDSKMEN = this.w_CIDSKMEN
    this.o_CIMDIFRM = this.w_CIMDIFRM
    this.o_CICTRGRD = this.w_CICTRGRD
    this.o_CIHEHEZO = this.w_CIHEHEZO
    this.o_CIDIMFRM = this.w_CIDIMFRM
    this.o_CISEARNT = this.w_CISEARNT
    this.o_XXB1 = this.w_XXB1
    this.o_XXB2 = this.w_XXB2
    this.o_XXB3 = this.w_XXB3
    this.o_XXB4 = this.w_XXB4
    this.o_XXB5 = this.w_XXB5
    this.o_XXB17 = this.w_XXB17
    this.o_XXB6 = this.w_XXB6
    this.o_XXB7 = this.w_XXB7
    this.o_XXB8 = this.w_XXB8
    this.o_XXB9 = this.w_XXB9
    this.o_XXB10 = this.w_XXB10
    this.o_XXB11 = this.w_XXB11
    this.o_XXB12 = this.w_XXB12
    this.o_XXB13 = this.w_XXB13
    this.o_XXB14 = this.w_XXB14
    this.o_XXB15 = this.w_XXB15
    this.o_XXB16 = this.w_XXB16
    this.o_CIVTHEME = this.w_CIVTHEME
    this.o_CIGADGET = this.w_CIGADGET
    return

enddefine

* --- Define pages as container
define class tgsut_acuPag1 as StdContainer
  Width  = 709
  height = 511
  stdWidth  = 709
  stdheight = 511
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCICPTBAR_1_1 as StdCombo with uid="OJRJRQJXNJ",rtseq=1,rtrep=.f.,left=14,top=167,width=223,height=23;
    , ToolTipText = "Permette di decidere la posizione della barra degli strumenti o la disattivazione della stessa";
    , HelpContextID = 17513080;
    , cFormVar="w_CICPTBAR",RowSource=""+"Disattivata,"+"Ancorata in alto,"+"Ancorata a sinistra,"+"Ancorata a destra,"+"Ancorata in basso,"+"Mobile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCICPTBAR_1_1.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'L',;
    iif(this.value =4,'R',;
    iif(this.value =5,'D',;
    iif(this.value =6,'F',;
    space(1))))))))
  endfunc
  func oCICPTBAR_1_1.GetRadio()
    this.Parent.oContained.w_CICPTBAR = this.RadioValue()
    return .t.
  endfunc

  func oCICPTBAR_1_1.SetRadio()
    this.Parent.oContained.w_CICPTBAR=trim(this.Parent.oContained.w_CICPTBAR)
    this.value = ;
      iif(this.Parent.oContained.w_CICPTBAR=='N',1,;
      iif(this.Parent.oContained.w_CICPTBAR=='S',2,;
      iif(this.Parent.oContained.w_CICPTBAR=='L',3,;
      iif(this.Parent.oContained.w_CICPTBAR=='R',4,;
      iif(this.Parent.oContained.w_CICPTBAR=='D',5,;
      iif(this.Parent.oContained.w_CICPTBAR=='F',6,;
      0))))))
  endfunc


  add object oCIDSKBAR_1_2 as StdCombo with uid="ZXPARNXJCJ",rtseq=2,rtrep=.f.,left=14,top=214,width=223,height=23;
    , ToolTipText = "Se attivo visualizza la barra delle applicazioni";
    , HelpContextID = 8276600;
    , cFormVar="w_CIDSKBAR",RowSource=""+"Disattivata,"+"Ancorata in alto,"+"Ancorata a sinistra,"+"Ancorata a destra,"+"Ancorata in basso,"+"Mobile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCIDSKBAR_1_2.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'L',;
    iif(this.value =4,'R',;
    iif(this.value =5,'D',;
    iif(this.value =6,'F',;
    space(1))))))))
  endfunc
  func oCIDSKBAR_1_2.GetRadio()
    this.Parent.oContained.w_CIDSKBAR = this.RadioValue()
    return .t.
  endfunc

  func oCIDSKBAR_1_2.SetRadio()
    this.Parent.oContained.w_CIDSKBAR=trim(this.Parent.oContained.w_CIDSKBAR)
    this.value = ;
      iif(this.Parent.oContained.w_CIDSKBAR=='N',1,;
      iif(this.Parent.oContained.w_CIDSKBAR=='S',2,;
      iif(this.Parent.oContained.w_CIDSKBAR=='L',3,;
      iif(this.Parent.oContained.w_CIDSKBAR=='R',4,;
      iif(this.Parent.oContained.w_CIDSKBAR=='D',5,;
      iif(this.Parent.oContained.w_CIDSKBAR=='F',6,;
      0))))))
  endfunc

  add object oCITOOBDS_1_3 as StdCheck with uid="OXBHDMETUM",rtseq=3,rtrep=.f.,left=14, top=240, caption="Nascondi automaticamente la toolbar",;
    ToolTipText = "Nascondi automaticamente la toolbar",;
    HelpContextID = 12274297,;
    cFormVar="w_CITOOBDS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCITOOBDS_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCITOOBDS_1_3.GetRadio()
    this.Parent.oContained.w_CITOOBDS = this.RadioValue()
    return .t.
  endfunc

  func oCITOOBDS_1_3.SetRadio()
    this.Parent.oContained.w_CITOOBDS=trim(this.Parent.oContained.w_CITOOBDS)
    this.value = ;
      iif(this.Parent.oContained.w_CITOOBDS=='S',1,;
      0)
  endfunc


  add object oCITBSIZE_1_4 as StdCombo with uid="TKEQGLEPQU",rtseq=4,rtrep=.f.,left=14,top=290,width=76,height=21;
    , ToolTipText = "Dimensione icone toolbar";
    , HelpContextID = 135378325;
    , cFormVar="w_CITBSIZE",RowSource=""+"16,"+"24,"+"32,"+"48", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCITBSIZE_1_4.RadioValue()
    return(iif(this.value =1,16,;
    iif(this.value =2,24,;
    iif(this.value =3,32,;
    iif(this.value =4,48,;
    0)))))
  endfunc
  func oCITBSIZE_1_4.GetRadio()
    this.Parent.oContained.w_CITBSIZE = this.RadioValue()
    return .t.
  endfunc

  func oCITBSIZE_1_4.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CITBSIZE==16,1,;
      iif(this.Parent.oContained.w_CITBSIZE==24,2,;
      iif(this.Parent.oContained.w_CITBSIZE==32,3,;
      iif(this.Parent.oContained.w_CITBSIZE==48,4,;
      0))))
  endfunc

  add object oCISTABAR_1_5 as StdCheck with uid="ERRTOGTWXP",rtseq=5,rtrep=.f.,left=14, top=324, caption="Visualizza barra di stato",;
    ToolTipText = "Se attivo visualizza la barra di stato (in basso)",;
    HelpContextID = 266353272,;
    cFormVar="w_CISTABAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCISTABAR_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCISTABAR_1_5.GetRadio()
    this.Parent.oContained.w_CISTABAR = this.RadioValue()
    return .t.
  endfunc

  func oCISTABAR_1_5.SetRadio()
    this.Parent.oContained.w_CISTABAR=trim(this.Parent.oContained.w_CISTABAR)
    this.value = ;
      iif(this.Parent.oContained.w_CISTABAR=='S',1,;
      0)
  endfunc

  add object oCISHWMEN_1_6 as StdCheck with uid="XQTYKSGSQB",rtseq=6,rtrep=.f.,left=318, top=149, caption="Visualizza men� principale",;
    ToolTipText = "Se attiva visualizza il men� principale (ctrl+m)",;
    HelpContextID = 204749428,;
    cFormVar="w_CISHWMEN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCISHWMEN_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCISHWMEN_1_6.GetRadio()
    this.Parent.oContained.w_CISHWMEN = this.RadioValue()
    return .t.
  endfunc

  func oCISHWMEN_1_6.SetRadio()
    this.Parent.oContained.w_CISHWMEN=trim(this.Parent.oContained.w_CISHWMEN)
    this.value = ;
      iif(this.Parent.oContained.w_CISHWMEN=='S',1,;
      0)
  endfunc

  add object oCISTASCR_1_7 as StdCheck with uid="BLPSRDTKOT",rtseq=7,rtrep=.f.,left=318, top=173, caption="Start screen", enabled=.f.,;
    ToolTipText = "Se attivo abilita start screen",;
    HelpContextID = 14695032,;
    cFormVar="w_CISTASCR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCISTASCR_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCISTASCR_1_7.GetRadio()
    this.Parent.oContained.w_CISTASCR = this.RadioValue()
    return .t.
  endfunc

  func oCISTASCR_1_7.SetRadio()
    this.Parent.oContained.w_CISTASCR=trim(this.Parent.oContained.w_CISTASCR)
    this.value = ;
      iif(this.Parent.oContained.w_CISTASCR=='S',1,;
      0)
  endfunc

  func oCISTASCR_1_7.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oCIMENFIX_1_8 as StdCheck with uid="QKUNFLMZFJ",rtseq=8,rtrep=.f.,left=318, top=197, caption="Men� in posizione fissa",;
    ToolTipText = "Se attivo abilita la barra del men� in posizione fissa",;
    HelpContextID = 190784898,;
    cFormVar="w_CIMENFIX", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIMENFIX_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCIMENFIX_1_8.GetRadio()
    this.Parent.oContained.w_CIMENFIX = this.RadioValue()
    return .t.
  endfunc

  func oCIMENFIX_1_8.SetRadio()
    this.Parent.oContained.w_CIMENFIX=trim(this.Parent.oContained.w_CIMENFIX)
    this.value = ;
      iif(this.Parent.oContained.w_CIMENFIX=='S',1,;
      0)
  endfunc

  func oCIMENFIX_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIVTHEME<>-1)
    endwith
   endif
  endfunc

  add object oCIWNDMEN_1_9 as StdCheck with uid="KNWFNELKVE",rtseq=9,rtrep=.f.,left=318, top=222, caption="Abilita men� Window",;
    ToolTipText = "Se attivo abilita la voce di men� per la gestione delle finestre",;
    HelpContextID = 185236084,;
    cFormVar="w_CIWNDMEN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIWNDMEN_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIWNDMEN_1_9.GetRadio()
    this.Parent.oContained.w_CIWNDMEN = this.RadioValue()
    return .t.
  endfunc

  func oCIWNDMEN_1_9.SetRadio()
    this.Parent.oContained.w_CIWNDMEN=trim(this.Parent.oContained.w_CIWNDMEN)
    this.value = ;
      iif(this.Parent.oContained.w_CIWNDMEN=='S',1,;
      0)
  endfunc

  func oCIWNDMEN_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CISHWMEN = 'S')
    endwith
   endif
  endfunc

  add object oXXSEARMN_1_12 as StdCheck with uid="RSZUCJRNCP",rtseq=11,rtrep=.f.,left=318, top=247, caption="Abilita ricerca voci men�",;
    ToolTipText = "Se attivo visualizza il controllo per la ricerca di voci di men�",;
    HelpContextID = 3061052,;
    cFormVar="w_XXSEARMN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oXXSEARMN_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oXXSEARMN_1_12.GetRadio()
    this.Parent.oContained.w_XXSEARMN = this.RadioValue()
    return .t.
  endfunc

  func oXXSEARMN_1_12.SetRadio()
    this.Parent.oContained.w_XXSEARMN=trim(this.Parent.oContained.w_XXSEARMN)
    this.value = ;
      iif(this.Parent.oContained.w_XXSEARMN=='S',1,;
      0)
  endfunc

  func oXXSEARMN_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_CISHWMEN = 'S' OR .w_CIDSKMEN <> 'H') And .w_CIVTHEME<>-1)
    endwith
   endif
  endfunc

  add object oXXSEARM1_1_13 as StdCheck with uid="TWTMFOCEGE",rtseq=12,rtrep=.f.,left=335, top=267, caption="Abilita su men�",;
    ToolTipText = "Se attivo visualizza il controllo per la ricerca di voci di men� sul men�",;
    HelpContextID = 3061081,;
    cFormVar="w_XXSEARM1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oXXSEARM1_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oXXSEARM1_1_13.GetRadio()
    this.Parent.oContained.w_XXSEARM1 = this.RadioValue()
    return .t.
  endfunc

  func oXXSEARM1_1_13.SetRadio()
    this.Parent.oContained.w_XXSEARM1=trim(this.Parent.oContained.w_XXSEARM1)
    this.value = ;
      iif(this.Parent.oContained.w_XXSEARM1=='S',1,;
      0)
  endfunc

  func oXXSEARM1_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_XXSEARMN = 'S' AND .w_CISHWMEN = 'S' And .w_CIVTHEME<>-1)
    endwith
   endif
  endfunc

  add object oXXSEARM2_1_14 as StdCheck with uid="TWLFFDCWTM",rtseq=13,rtrep=.f.,left=335, top=288, caption="Abilita su desktop men�",;
    ToolTipText = "Se attivo visualizza il controllo per la ricerca di voci di men� sul desktop men�",;
    HelpContextID = 3061080,;
    cFormVar="w_XXSEARM2", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oXXSEARM2_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oXXSEARM2_1_14.GetRadio()
    this.Parent.oContained.w_XXSEARM2 = this.RadioValue()
    return .t.
  endfunc

  func oXXSEARM2_1_14.SetRadio()
    this.Parent.oContained.w_XXSEARM2=trim(this.Parent.oContained.w_XXSEARM2)
    this.value = ;
      iif(this.Parent.oContained.w_XXSEARM2=='S',1,;
      0)
  endfunc

  func oXXSEARM2_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_XXSEARMN = 'S' AND .w_CIDSKMEN <> 'H' And .w_CIVTHEME<>-1)
    endwith
   endif
  endfunc

  add object oCIFRMBUT_1_15 as StdCheck with uid="SADVYDVSIH",rtseq=14,rtrep=.f.,left=318, top=315, caption="Abilita men� funzionalit�",;
    ToolTipText = "Se attivo Permette di attivare il men� funzionalit� con il tasto destro del mouse",;
    HelpContextID = 10316410,;
    cFormVar="w_CIFRMBUT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIFRMBUT_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCIFRMBUT_1_15.GetRadio()
    this.Parent.oContained.w_CIFRMBUT = this.RadioValue()
    return .t.
  endfunc

  func oCIFRMBUT_1_15.SetRadio()
    this.Parent.oContained.w_CIFRMBUT=trim(this.Parent.oContained.w_CIFRMBUT)
    this.value = ;
      iif(this.Parent.oContained.w_CIFRMBUT=='S',1,;
      0)
  endfunc

  add object oCIFRMPAG_1_16 as StdCheck with uid="JHSJJLCTKI",rtseq=15,rtrep=.f.,left=318, top=340, caption="Visualizza funz. solo pagina attiva",;
    ToolTipText = "Se attivo permette di visualizzare solo le funzionalit� della pagina attiva",;
    HelpContextID = 245197421,;
    cFormVar="w_CIFRMPAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIFRMPAG_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCIFRMPAG_1_16.GetRadio()
    this.Parent.oContained.w_CIFRMPAG = this.RadioValue()
    return .t.
  endfunc

  func oCIFRMPAG_1_16.SetRadio()
    this.Parent.oContained.w_CIFRMPAG=trim(this.Parent.oContained.w_CIFRMPAG)
    this.value = ;
      iif(this.Parent.oContained.w_CIFRMPAG=='S',1,;
      0)
  endfunc

  func oCIFRMPAG_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIFRMBUT='S')
    endwith
   endif
  endfunc

  add object oCITOOLMN_1_17 as StdCheck with uid="RKRMTWUXJM",rtseq=16,rtrep=.f.,left=318, top=365, caption="Abilita tool men�",;
    ToolTipText = "Se attivo abilita il tool men� (visualizzabile tramite ctrl+t)",;
    HelpContextID = 88389004,;
    cFormVar="w_CITOOLMN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCITOOLMN_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCITOOLMN_1_17.GetRadio()
    this.Parent.oContained.w_CITOOLMN = this.RadioValue()
    return .t.
  endfunc

  func oCITOOLMN_1_17.SetRadio()
    this.Parent.oContained.w_CITOOLMN=trim(this.Parent.oContained.w_CITOOLMN)
    this.value = ;
      iif(this.Parent.oContained.w_CITOOLMN=='S',1,;
      0)
  endfunc

  add object oCIMENIMM_1_18 as StdCheck with uid="IHRIKEWKEQ",rtseq=17,rtrep=.f.,left=318, top=390, caption="Disabilita immagini nelle voci di men�",;
    ToolTipText = "Abilita/disabilita le immagini abbinate nelle voci di men�",;
    HelpContextID = 140453261,;
    cFormVar="w_CIMENIMM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIMENIMM_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIMENIMM_1_18.GetRadio()
    this.Parent.oContained.w_CIMENIMM = this.RadioValue()
    return .t.
  endfunc

  func oCIMENIMM_1_18.SetRadio()
    this.Parent.oContained.w_CIMENIMM=trim(this.Parent.oContained.w_CIMENIMM)
    this.value = ;
      iif(this.Parent.oContained.w_CIMENIMM=='S',1,;
      0)
  endfunc

  func oCIMENIMM_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIVTHEME<>-1)
    endwith
   endif
  endfunc

  add object oCINEWPRI_1_19 as StdCheck with uid="LBTGGCJCOU",rtseq=18,rtrep=.f.,left=14, top=421, caption="Abilita la nuova printsystem",;
    ToolTipText = "Abilita/disabilita la nuova printsystem",;
    HelpContextID = 254863983,;
    cFormVar="w_CINEWPRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCINEWPRI_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCINEWPRI_1_19.GetRadio()
    this.Parent.oContained.w_CINEWPRI = this.RadioValue()
    return .t.
  endfunc

  func oCINEWPRI_1_19.SetRadio()
    this.Parent.oContained.w_CINEWPRI=trim(this.Parent.oContained.w_CINEWPRI)
    this.value = ;
      iif(this.Parent.oContained.w_CINEWPRI=='S',1,;
      0)
  endfunc

  func oCINEWPRI_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CICODUTE=-1)
    endwith
   endif
  endfunc

  func oCINEWPRI_1_19.mHide()
    with this.Parent.oContained
      return (.w_CICODUTE<>-1)
    endwith
  endfunc

  add object oCIMONFRA_1_20 as StdCheck with uid="UTCDBRWROE",rtseq=19,rtrep=.f.,left=14, top=444, caption="Attivazione monitor framework",;
    ToolTipText = "Se attivo la procedura � sottoposta al controllo del monitor framework",;
    HelpContextID = 78305895,;
    cFormVar="w_CIMONFRA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIMONFRA_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIMONFRA_1_20.GetRadio()
    this.Parent.oContained.w_CIMONFRA = this.RadioValue()
    return .t.
  endfunc

  func oCIMONFRA_1_20.SetRadio()
    this.Parent.oContained.w_CIMONFRA=trim(this.Parent.oContained.w_CIMONFRA)
    this.value = ;
      iif(this.Parent.oContained.w_CIMONFRA=='S',1,;
      0)
  endfunc

  func oCIMONFRA_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CICODUTE=-1)
    endwith
   endif
  endfunc

  func oCIMONFRA_1_20.mHide()
    with this.Parent.oContained
      return (.w_CICODUTE<>-1)
    endwith
  endfunc

  add object oCI_ILIKE_1_21 as StdCheck with uid="HIECVDPFYF",rtseq=20,rtrep=.f.,left=14, top=467, caption="Usa ilike nelle ricerche (PostgreSQL)",;
    ToolTipText = "Se attivo usa l'operatore ilike (ricerca case-insensitive) al posto di like (solo database PostgreSQL)",;
    HelpContextID = 142214549,;
    cFormVar="w_CI_ILIKE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCI_ILIKE_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCI_ILIKE_1_21.GetRadio()
    this.Parent.oContained.w_CI_ILIKE = this.RadioValue()
    return .t.
  endfunc

  func oCI_ILIKE_1_21.SetRadio()
    this.Parent.oContained.w_CI_ILIKE=trim(this.Parent.oContained.w_CI_ILIKE)
    this.value = ;
      iif(this.Parent.oContained.w_CI_ILIKE=='S',1,;
      0)
  endfunc

  func oCI_ILIKE_1_21.mHide()
    with this.Parent.oContained
      return (.w_CICODUTE<>-1 or CP_DBTYPE<>'PostgreSQL')
    endwith
  endfunc

  add object oCIMINPST_1_22 as StdField with uid="NWGGEGGGEB",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CIMINPST", cQueryName = "CIMINPST",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Minuti intervallo Post-IN",;
    HelpContextID = 245684858,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=494, Top=43


  add object oCIMODPST_1_23 as StdCombo with uid="EKIWBPTEUQ",rtseq=22,rtrep=.f.,left=438,top=70,width=97,height=21;
    , ToolTipText = "Metodo verifica Post-IN";
    , HelpContextID = 235592314;
    , cFormVar="w_CIMODPST",RowSource=""+"Manuale,"+"Automatico", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCIMODPST_1_23.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    0)))
  endfunc
  func oCIMODPST_1_23.GetRadio()
    this.Parent.oContained.w_CIMODPST = this.RadioValue()
    return .t.
  endfunc

  func oCIMODPST_1_23.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CIMODPST==1,1,;
      iif(this.Parent.oContained.w_CIMODPST==2,2,;
      0))
  endfunc

  add object oCISEARNT_1_45 as StdCheck with uid="FXLOOQKYON",rtseq=150,rtrep=.f.,left=318, top=415, caption="Abilita ricerca men� recenti",;
    ToolTipText = "Se attivo visualizza il controllo per la ricerca dei recenti",;
    HelpContextID = 3065222,;
    cFormVar="w_CISEARNT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCISEARNT_1_45.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCISEARNT_1_45.GetRadio()
    this.Parent.oContained.w_CISEARNT = this.RadioValue()
    return .t.
  endfunc

  func oCISEARNT_1_45.SetRadio()
    this.Parent.oContained.w_CISEARNT=trim(this.Parent.oContained.w_CISEARNT)
    this.value = ;
      iif(this.Parent.oContained.w_CISEARNT=='S',1,;
      0)
  endfunc

  func oCISEARNT_1_45.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oCISEARNT_1_46 as StdCheck with uid="WQFCVWNHIW",rtseq=151,rtrep=.f.,left=318, top=415, caption="Abilita ricerca men� pratiche recenti",;
    ToolTipText = "Se attivo visualizza l'elenco delle pratiche consultate",;
    HelpContextID = 3065222,;
    cFormVar="w_CISEARNT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCISEARNT_1_46.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCISEARNT_1_46.GetRadio()
    this.Parent.oContained.w_CISEARNT = this.RadioValue()
    return .t.
  endfunc

  func oCISEARNT_1_46.SetRadio()
    this.Parent.oContained.w_CISEARNT=trim(this.Parent.oContained.w_CISEARNT)
    this.value = ;
      iif(this.Parent.oContained.w_CISEARNT=='S',1,;
      0)
  endfunc

  func oCISEARNT_1_46.mHide()
    with this.Parent.oContained
      return (!isalt())
    endwith
  endfunc

  add object oCISEAMNW_1_47 as StdCheck with uid="KSXPCJWFJU",rtseq=152,rtrep=.f.,left=335, top=435, caption="Abilita su men� windows",;
    ToolTipText = "Se attivo abilita l'elenco dei recenti sul men� windows",;
    HelpContextID = 86951299,;
    cFormVar="w_CISEAMNW", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCISEAMNW_1_47.RadioValue()
    return(iif(this.value =1,1,;
    0))
  endfunc
  func oCISEAMNW_1_47.GetRadio()
    this.Parent.oContained.w_CISEAMNW = this.RadioValue()
    return .t.
  endfunc

  func oCISEAMNW_1_47.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CISEAMNW==1,1,;
      0)
  endfunc

  func oCISEAMNW_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIWNDMEN = 'S')
    endwith
   endif
  endfunc

  func oCISEAMNW_1_47.mHide()
    with this.Parent.oContained
      return (.w_CISEARNT<>"S")
    endwith
  endfunc

  add object oCISEADKM_1_48 as StdCheck with uid="OSHZNCICEA",rtseq=153,rtrep=.f.,left=335, top=456, caption="Abilita su desktop men�",;
    ToolTipText = "Se attivo abilita l'elenco dei recenti sul desktop men�",;
    HelpContextID = 237946253,;
    cFormVar="w_CISEADKM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCISEADKM_1_48.RadioValue()
    return(iif(this.value =1,2,;
    0))
  endfunc
  func oCISEADKM_1_48.GetRadio()
    this.Parent.oContained.w_CISEADKM = this.RadioValue()
    return .t.
  endfunc

  func oCISEADKM_1_48.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CISEADKM==2,1,;
      0)
  endfunc

  func oCISEADKM_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CISEARNT = 'S')
    endwith
   endif
  endfunc

  func oCISEADKM_1_48.mHide()
    with this.Parent.oContained
      return (.w_CISEARNT<>"S")
    endwith
  endfunc

  add object oCISEAAPB_1_49 as StdCheck with uid="SZPXGHUDXX",rtseq=154,rtrep=.f.,left=335, top=477, caption="Abilita su application bar",;
    ToolTipText = "Se attivo abilita l'elenco dei recenti sull'application bar",;
    HelpContextID = 19842456,;
    cFormVar="w_CISEAAPB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCISEAAPB_1_49.RadioValue()
    return(iif(this.value =1,4,;
    0))
  endfunc
  func oCISEAAPB_1_49.GetRadio()
    this.Parent.oContained.w_CISEAAPB = this.RadioValue()
    return .t.
  endfunc

  func oCISEAAPB_1_49.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CISEAAPB==4,1,;
      0)
  endfunc

  func oCISEAAPB_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CISEARNT = 'S')
    endwith
   endif
  endfunc

  func oCISEAAPB_1_49.mHide()
    with this.Parent.oContained
      return (.w_CISEARNT<>"S")
    endwith
  endfunc

  add object oCINUMREC_1_51 as StdField with uid="YDCVXVAXLI",rtseq=156,rtrep=.f.,;
    cFormVar = "w_CINUMREC", cQueryName = "CINUMREC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero massimo voci di men� recenti (men� windows)",;
    HelpContextID = 10545769,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=670, Top=436, cSayPict='"99"', cGetPict='"99"'

  func oCINUMREC_1_51.mHide()
    with this.Parent.oContained
      return (isalt() OR .w_CISEARNT<>"S")
    endwith
  endfunc

  func oCINUMREC_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CINUMREC>=0)
    endwith
    return bRes
  endfunc

  add object oCINRECEN_1_52 as StdField with uid="WLOMSSCRRU",rtseq=157,rtrep=.f.,;
    cFormVar = "w_CINRECEN", cQueryName = "CINRECEN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di dati recenti memorizzati",;
    HelpContextID = 18737780,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=670, Top=478, cSayPict='"99"', cGetPict='"99"'

  func oCINRECEN_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CISEARNT="S")
    endwith
   endif
  endfunc

  func oCINRECEN_1_52.mHide()
    with this.Parent.oContained
      return (.w_CISEARNT<>"S" or .w_CINUMREC=0 and not isalt())
    endwith
  endfunc

  func oCINRECEN_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CINRECEN>=0)
    endwith
    return bRes
  endfunc


  add object oCIVTHEME_1_58 as StdThemesCombo with uid="KKYNAIXTLY",rtseq=178,rtrep=.f.,left=14,top=43,width=216,height=22;
    , ToolTipText = "Tema applicato al gestionale";
    , HelpContextID = 212833685;
    , cFormVar="w_CIVTHEME",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=0;
  , bGlobalFont=.t.



  add object oObj_1_59 as cp_runprogram with uid="ZQLRSXLQXV",left=11, top=550, width=246,height=19,;
    caption='GSUT_BCI(Default)',;
   bGlobalFont=.t.,;
    prg="GSUT_BCI('Style-Default')",;
    cEvent = "Blank",;
    nPag=1;
    , ToolTipText = "Setta i valori di default (da bottone di gsut_kci)";
    , HelpContextID = 222101043


  add object oObj_1_60 as cp_runprogram with uid="TYYRNFIJYQ",left=11, top=569, width=245,height=19,;
    caption='GSUT_BCI(SetByTheme)',;
   bGlobalFont=.t.,;
    prg="GSUT_BCI('SetByTheme')",;
    cEvent = "w_CIVTHEME Changed",;
    nPag=1;
    , HelpContextID = 7680537

  add object oCIGADGET_1_61 as StdCheck with uid="ZOHPMNDXTJ",rtseq=179,rtrep=.f.,left=14, top=68, caption="Abilita interfaccia gadget",;
    ToolTipText = "Se attivo abilita la visualizzazione dei gadget",;
    HelpContextID = 83655290,;
    cFormVar="w_CIGADGET", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIGADGET_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCIGADGET_1_61.GetRadio()
    this.Parent.oContained.w_CIGADGET = this.RadioValue()
    return .t.
  endfunc

  func oCIGADGET_1_61.SetRadio()
    this.Parent.oContained.w_CIGADGET=trim(this.Parent.oContained.w_CIGADGET)
    this.value = ;
      iif(this.Parent.oContained.w_CIGADGET=='S',1,;
      0)
  endfunc

  add object oStr_1_26 as StdString with uid="EGTKSXWAQH",Visible=.t., Left=14, Top=128,;
    Alignment=0, Width=156, Height=17,;
    Caption="Tool bar / status bar"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_29 as StdString with uid="ZPUXLISGUW",Visible=.t., Left=14, Top=21,;
    Alignment=0, Width=130, Height=17,;
    Caption="Tema"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="JOSOXDUYZH",Visible=.t., Left=14, Top=269,;
    Alignment=0, Width=227, Height=18,;
    Caption="Dimensione immagini toolbar"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="IFRGPEXQCQ",Visible=.t., Left=318, Top=129,;
    Alignment=0, Width=124, Height=17,;
    Caption="men�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="NRDXHYYCJE",Visible=.t., Left=306, Top=47,;
    Alignment=1, Width=185, Height=18,;
    Caption="Verifica dei Post-IN ogni minuti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="TJZHELJTNK",Visible=.t., Left=318, Top=21,;
    Alignment=0, Width=134, Height=17,;
    Caption="Verifica Post-IN"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="ZEAJJPBBBY",Visible=.t., Left=305, Top=70,;
    Alignment=1, Width=127, Height=18,;
    Caption="Apertura messaggi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="VUFKPVUXUV",Visible=.t., Left=14, Top=398,;
    Alignment=0, Width=131, Height=17,;
    Caption="Impostazioni generali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_39 as StdString with uid="RAHVSDSHAH",Visible=.t., Left=240, Top=329,;
    Alignment=0, Width=60, Height=17,;
    Caption="C.m. : 15%"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="KELQFCYQRK",Visible=.t., Left=240, Top=170,;
    Alignment=0, Width=53, Height=17,;
    Caption="C.m. : 1%"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="FCHEXHYEEF",Visible=.t., Left=240, Top=218,;
    Alignment=0, Width=53, Height=17,;
    Caption="C.m. : 1%"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="QJNHLNPVDG",Visible=.t., Left=555, Top=395,;
    Alignment=0, Width=60, Height=18,;
    Caption="C.m. : 13%"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="OUXBIRLKXF",Visible=.t., Left=477, Top=439,;
    Alignment=1, Width=190, Height=18,;
    Caption="Numero massimo di recenti:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (isalt() OR .w_CISEARNT<>"S")
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="YEPISLAJSE",Visible=.t., Left=527, Top=481,;
    Alignment=1, Width=140, Height=18,;
    Caption="Numero massimo di dati:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_CISEARNT<>"S" or .w_CINUMREC=0 and not isalt())
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="GZAPDXVXBJ",Visible=.t., Left=11, Top=149,;
    Alignment=0, Width=232, Height=18,;
    Caption="Impostazione barra degli strumenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="EFZCBVJEOP",Visible=.t., Left=11, Top=193,;
    Alignment=0, Width=204, Height=18,;
    Caption="Impostazione barra delle applicazioni"  ;
  , bGlobalFont=.t.

  add object oBox_1_27 as StdBox with uid="TLVIIGADYM",left=8, top=144, width=223,height=1

  add object oBox_1_32 as StdBox with uid="FXMQMUYWXW",left=299, top=144, width=240,height=1

  add object oBox_1_35 as StdBox with uid="TRYXWIBKUC",left=299, top=37, width=240,height=1

  add object oBox_1_38 as StdBox with uid="KENURBLINX",left=8, top=414, width=223,height=1

  add object oBox_1_42 as StdBox with uid="TKTZKPVEKN",left=8, top=37, width=223,height=1
enddefine
define class tgsut_acuPag2 as StdContainer
  Width  = 709
  height = 511
  stdWidth  = 709
  stdheight = 511
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCIMDIFRM_2_1 as StdCombo with uid="BQFRHJEJMG",rtseq=118,rtrep=.f.,left=19,top=51,width=166,height=21;
    , ToolTipText = "Modalit� di visualizzazione delle maschere";
    , HelpContextID = 72342131;
    , cFormVar="w_CIMDIFRM",RowSource=""+"Classica,"+"Integrata,"+"Classica / Integrata,"+"Integrata / Classica", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCIMDIFRM_2_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'I',;
    iif(this.value =3,'A',;
    iif(this.value =4,'M',;
    space(1))))))
  endfunc
  func oCIMDIFRM_2_1.GetRadio()
    this.Parent.oContained.w_CIMDIFRM = this.RadioValue()
    return .t.
  endfunc

  func oCIMDIFRM_2_1.SetRadio()
    this.Parent.oContained.w_CIMDIFRM=trim(this.Parent.oContained.w_CIMDIFRM)
    this.value = ;
      iif(this.Parent.oContained.w_CIMDIFRM=='S',1,;
      iif(this.Parent.oContained.w_CIMDIFRM=='I',2,;
      iif(this.Parent.oContained.w_CIMDIFRM=='A',3,;
      iif(this.Parent.oContained.w_CIMDIFRM=='M',4,;
      0))))
  endfunc


  add object oCITABMEN_2_2 as StdCombo with uid="UGMBXROSQM",rtseq=119,rtrep=.f.,left=19,top=95,width=166,height=21;
    , ToolTipText = "Modalit� di visualizzazione delle tab";
    , HelpContextID = 182274676;
    , cFormVar="w_CITABMEN",RowSource=""+"Standard,"+"A tema", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCITABMEN_2_2.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oCITABMEN_2_2.GetRadio()
    this.Parent.oContained.w_CITABMEN = this.RadioValue()
    return .t.
  endfunc

  func oCITABMEN_2_2.SetRadio()
    this.Parent.oContained.w_CITABMEN=trim(this.Parent.oContained.w_CITABMEN)
    this.value = ;
      iif(this.Parent.oContained.w_CITABMEN=='S',1,;
      iif(this.Parent.oContained.w_CITABMEN=='T',2,;
      0))
  endfunc

  func oCITABMEN_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIVTHEME <> -1)
    endwith
   endif
  endfunc


  add object oCIXPTHEM_2_3 as StdCombo with uid="AMVFHFREXI",value=1,rtseq=120,rtrep=.f.,left=19,top=140,width=108,height=21;
    , ToolTipText = "Tema XP attivo o meno";
    , HelpContextID = 118262387;
    , cFormVar="w_CIXPTHEM",RowSource=""+"Disattivo,"+"Attivo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCIXPTHEM_2_3.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    -1)))
  endfunc
  func oCIXPTHEM_2_3.GetRadio()
    this.Parent.oContained.w_CIXPTHEM = this.RadioValue()
    return .t.
  endfunc

  func oCIXPTHEM_2_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CIXPTHEM==0,1,;
      iif(this.Parent.oContained.w_CIXPTHEM==1,2,;
      0))
  endfunc


  add object oCIRICONT_2_4 as StdCombo with uid="VKGCVNWNED",rtseq=121,rtrep=.f.,left=19,top=184,width=238,height=21;
    , ToolTipText = "Modalit� di ricerca per contenuto: con digitazione o meno della %  sui campi codice e/o in interroga e/o nell'elenco delle anagrafiche.";
    , HelpContextID = 51041670;
    , cFormVar="w_CIRICONT",RowSource=""+"Digitazione %,"+"No digit. % sui campi codice,"+"No digit. % sui campi codice/in interroga,"+"No digit. % sui campi/in interroga/nell'elenco", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCIRICONT_2_4.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'L',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oCIRICONT_2_4.GetRadio()
    this.Parent.oContained.w_CIRICONT = this.RadioValue()
    return .t.
  endfunc

  func oCIRICONT_2_4.SetRadio()
    this.Parent.oContained.w_CIRICONT=trim(this.Parent.oContained.w_CIRICONT)
    this.value = ;
      iif(this.Parent.oContained.w_CIRICONT=='N',1,;
      iif(this.Parent.oContained.w_CIRICONT=='S',2,;
      iif(this.Parent.oContained.w_CIRICONT=='L',3,;
      iif(this.Parent.oContained.w_CIRICONT=='T',4,;
      0))))
  endfunc

  add object oCIBCKGRD_2_5 as StdCheck with uid="NNTWTSEYPQ",rtseq=122,rtrep=.f.,left=19, top=208, caption="Gradiente di sfondo",;
    ToolTipText = "Se attivo mostra un gradiente di sfondo",;
    HelpContextID = 91105898,;
    cFormVar="w_CIBCKGRD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCIBCKGRD_2_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIBCKGRD_2_5.GetRadio()
    this.Parent.oContained.w_CIBCKGRD = this.RadioValue()
    return .t.
  endfunc

  func oCIBCKGRD_2_5.SetRadio()
    this.Parent.oContained.w_CIBCKGRD=trim(this.Parent.oContained.w_CIBCKGRD)
    this.value = ;
      iif(this.Parent.oContained.w_CIBCKGRD=='S',1,;
      0)
  endfunc

  func oCIBCKGRD_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIVTHEME <> -1 And .w_CITABMEN <> 'S')
    endwith
   endif
  endfunc

  add object oCIPROBAR_2_6 as StdCheck with uid="XLTJSDGNEA",rtseq=123,rtrep=.f.,left=19, top=234, caption="Abilita progress bar",;
    ToolTipText = "Se attivo abilita la visualizzazione dello stato di avanzamento dell'elaborazione (progressbar)",;
    HelpContextID = 12454520,;
    cFormVar="w_CIPROBAR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCIPROBAR_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCIPROBAR_2_6.GetRadio()
    this.Parent.oContained.w_CIPROBAR = this.RadioValue()
    return .t.
  endfunc

  func oCIPROBAR_2_6.SetRadio()
    this.Parent.oContained.w_CIPROBAR=trim(this.Parent.oContained.w_CIPROBAR)
    this.value = ;
      iif(this.Parent.oContained.w_CIPROBAR=='S',1,;
      0)
  endfunc

  add object oCI_COLON_2_7 as StdCheck with uid="UHNLFTMQSY",rtseq=124,rtrep=.f.,left=19, top=260, caption="Visualizza ':' dopo nomi dei campi",;
    ToolTipText = "Se attivo visualizza il carattere ':' alla fine dei nomi dei campi",;
    HelpContextID = 89130380,;
    cFormVar="w_CI_COLON", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCI_COLON_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCI_COLON_2_7.GetRadio()
    this.Parent.oContained.w_CI_COLON = this.RadioValue()
    return .t.
  endfunc

  func oCI_COLON_2_7.SetRadio()
    this.Parent.oContained.w_CI_COLON=trim(this.Parent.oContained.w_CI_COLON)
    this.value = ;
      iif(this.Parent.oContained.w_CI_COLON=='S',1,;
      0)
  endfunc

  add object oCINOBTNI_2_8 as StdCheck with uid="NNXMPWJSPT",rtseq=125,rtrep=.f.,left=19, top=286, caption="Nascondi immagini bottoni",;
    ToolTipText = "Nascondi immagini bottoni",;
    HelpContextID = 236262801,;
    cFormVar="w_CINOBTNI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCINOBTNI_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCINOBTNI_2_8.GetRadio()
    this.Parent.oContained.w_CINOBTNI = this.RadioValue()
    return .t.
  endfunc

  func oCINOBTNI_2_8.SetRadio()
    this.Parent.oContained.w_CINOBTNI=trim(this.Parent.oContained.w_CINOBTNI)
    this.value = ;
      iif(this.Parent.oContained.w_CINOBTNI=='S',1,;
      0)
  endfunc

  add object ociwaitwd_2_9 as StdCheck with uid="VUZWGYUVMR",rtseq=126,rtrep=.f.,left=19, top=312, caption="Abilita tema wait window",;
    ToolTipText = "Wait window themed",;
    HelpContextID = 194012022,;
    cFormVar="w_ciwaitwd", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func ociwaitwd_2_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func ociwaitwd_2_9.GetRadio()
    this.Parent.oContained.w_ciwaitwd = this.RadioValue()
    return .t.
  endfunc

  func ociwaitwd_2_9.SetRadio()
    this.Parent.oContained.w_ciwaitwd=trim(this.Parent.oContained.w_ciwaitwd)
    this.value = ;
      iif(this.Parent.oContained.w_ciwaitwd=='S',1,;
      0)
  endfunc

  add object oCISHWBTN_2_10 as StdCheck with uid="FQMHCYRRPS",rtseq=127,rtrep=.f.,left=19, top=338, caption="Visualizza bottone contestuale",;
    ToolTipText = "Se attivo visualizza il bottone contestuale accanto ai campi con link",;
    HelpContextID = 20200052,;
    cFormVar="w_CISHWBTN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCISHWBTN_2_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCISHWBTN_2_10.GetRadio()
    this.Parent.oContained.w_CISHWBTN = this.RadioValue()
    return .t.
  endfunc

  func oCISHWBTN_2_10.SetRadio()
    this.Parent.oContained.w_CISHWBTN=trim(this.Parent.oContained.w_CISHWBTN)
    this.value = ;
      iif(this.Parent.oContained.w_CISHWBTN=='S',1,;
      0)
  endfunc

  add object oCIMRKGRD_2_11 as StdCheck with uid="FPPQZCEDUQ",rtseq=128,rtrep=.f.,left=396, top=42, caption="Puntatore del record",;
    ToolTipText = "Se attivo, visualizza il puntatore del record negli oggetti di tipo elenco e griglie di dati",;
    HelpContextID = 92133994,;
    cFormVar="w_CIMRKGRD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCIMRKGRD_2_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIMRKGRD_2_11.GetRadio()
    this.Parent.oContained.w_CIMRKGRD = this.RadioValue()
    return .t.
  endfunc

  func oCIMRKGRD_2_11.SetRadio()
    this.Parent.oContained.w_CIMRKGRD=trim(this.Parent.oContained.w_CIMRKGRD)
    this.value = ;
      iif(this.Parent.oContained.w_CIMRKGRD=='S',1,;
      0)
  endfunc

  add object oCICTRGRD_2_12 as StdCheck with uid="FKYNTLELKV",rtseq=129,rtrep=.f.,left=396, top=67, caption="Intestazione griglia avanzata",;
    ToolTipText = "Se attivo, visualizza il controllo avanzato nell'intestazione degli zoom",;
    HelpContextID = 99564138,;
    cFormVar="w_CICTRGRD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCICTRGRD_2_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCICTRGRD_2_12.GetRadio()
    this.Parent.oContained.w_CICTRGRD = this.RadioValue()
    return .t.
  endfunc

  func oCICTRGRD_2_12.SetRadio()
    this.Parent.oContained.w_CICTRGRD=trim(this.Parent.oContained.w_CICTRGRD)
    this.value = ;
      iif(this.Parent.oContained.w_CICTRGRD=='S',1,;
      0)
  endfunc

  func oCICTRGRD_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIVTHEME<>-1)
    endwith
   endif
  endfunc

  add object oCIHEHEZO_2_13 as StdField with uid="QDOCYNHTLQ",rtseq=130,rtrep=.f.,;
    cFormVar = "w_CIHEHEZO", cQueryName = "CIHEHEZO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire l'altezza della testata dello zoom maggiore di zero",;
    ToolTipText = "Altezza della testata degli zoom",;
    HelpContextID = 213874059,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=584, Top=99

  func oCIHEHEZO_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CIHEHEZO > 0)
    endwith
    return bRes
  endfunc

  add object oCIHEROZO_2_14 as StdField with uid="BMELVTMRQG",rtseq=131,rtrep=.f.,;
    cFormVar = "w_CIHEROZO", cQueryName = "CIHEROZO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire l'altezza della righe dello zoom maggiore di zero",;
    ToolTipText = "Altezza righe dello zoom",;
    HelpContextID = 35616139,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=584, Top=123

  func oCIHEROZO_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CIHEROZO > 0)
    endwith
    return bRes
  endfunc

  add object oCIZOESPR_2_15 as StdCheck with uid="UVIIICGFIG",rtseq=132,rtrep=.f.,left=396, top=150, caption="Espandi gestione parametri zoom",;
    ToolTipText = "Espandi gestione parametri negli zoom",;
    HelpContextID = 18590328,;
    cFormVar="w_CIZOESPR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCIZOESPR_2_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIZOESPR_2_15.GetRadio()
    this.Parent.oContained.w_CIZOESPR = this.RadioValue()
    return .t.
  endfunc

  func oCIZOESPR_2_15.SetRadio()
    this.Parent.oContained.w_CIZOESPR=trim(this.Parent.oContained.w_CIZOESPR)
    this.value = ;
      iif(this.Parent.oContained.w_CIZOESPR=='S',1,;
      0)
  endfunc

  add object oCIDSPCNT_2_16 as StdField with uid="ZELGZGYPLJ",rtseq=133,rtrep=.f.,;
    cFormVar = "w_CIDSPCNT", cQueryName = "CIDSPCNT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire numero righe maggiore di zero",;
    ToolTipText = "Numero di righe da visualizzare nelle combo box",;
    HelpContextID = 238138758,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=264, Top=376

  func oCIDSPCNT_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CIDSPCNT > 0)
    endwith
    return bRes
  endfunc


  add object oCIWARTYP_2_35 as StdCombo with uid="QGBRGDGRSH",rtseq=135,rtrep=.f.,left=396,top=251,width=166,height=21;
    , ToolTipText = "Tipo visualizzazione warning (Standard/Balloon)";
    , HelpContextID = 220366218;
    , cFormVar="w_CIWARTYP",RowSource=""+"Standard,"+"Balloon", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCIWARTYP_2_35.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'B',;
    space(1))))
  endfunc
  func oCIWARTYP_2_35.GetRadio()
    this.Parent.oContained.w_CIWARTYP = this.RadioValue()
    return .t.
  endfunc

  func oCIWARTYP_2_35.SetRadio()
    this.Parent.oContained.w_CIWARTYP=trim(this.Parent.oContained.w_CIWARTYP)
    this.value = ;
      iif(this.Parent.oContained.w_CIWARTYP=='S',1,;
      iif(this.Parent.oContained.w_CIWARTYP=='B',2,;
      0))
  endfunc


  add object oCIZOOMMO_2_37 as StdCombo with uid="HJWYMKQNND",rtseq=136,rtrep=.f.,left=396,top=296,width=166,height=21;
    , ToolTipText = "Tipo visualizzazione zoom (Standard / Contestuale)";
    , HelpContextID = 71587211;
    , cFormVar="w_CIZOOMMO",RowSource=""+"Standard,"+"Contestuale", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCIZOOMMO_2_37.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oCIZOOMMO_2_37.GetRadio()
    this.Parent.oContained.w_CIZOOMMO = this.RadioValue()
    return .t.
  endfunc

  func oCIZOOMMO_2_37.SetRadio()
    this.Parent.oContained.w_CIZOOMMO=trim(this.Parent.oContained.w_CIZOOMMO)
    this.value = ;
      iif(this.Parent.oContained.w_CIZOOMMO=='S',1,;
      iif(this.Parent.oContained.w_CIZOOMMO=='C',2,;
      0))
  endfunc

  add object oCIZOOMSC_2_39 as StdCheck with uid="AQITXGXESX",rtseq=146,rtrep=.f.,left=396, top=329, caption="Selezione record con singolo click",;
    ToolTipText = "Se attivo il record dello zoom verr� selezionato con un singolo click del mouse",;
    HelpContextID = 196848233,;
    cFormVar="w_CIZOOMSC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCIZOOMSC_2_39.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIZOOMSC_2_39.GetRadio()
    this.Parent.oContained.w_CIZOOMSC = this.RadioValue()
    return .t.
  endfunc

  func oCIZOOMSC_2_39.SetRadio()
    this.Parent.oContained.w_CIZOOMSC=trim(this.Parent.oContained.w_CIZOOMSC)
    this.value = ;
      iif(this.Parent.oContained.w_CIZOOMSC=='S',1,;
      0)
  endfunc


  add object oCIDIMFRM_2_42 as StdCombo with uid="FMCGXYBEQP",rtseq=147,rtrep=.f.,left=19,top=433,width=164,height=21;
    , ToolTipText = "Permette l'impostazione della dimensione di apertura delle maschere";
    , HelpContextID = 76827251;
    , cFormVar="w_CIDIMFRM",RowSource=""+"Standard,"+"Dimensione fissa", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCIDIMFRM_2_42.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oCIDIMFRM_2_42.GetRadio()
    this.Parent.oContained.w_CIDIMFRM = this.RadioValue()
    return .t.
  endfunc

  func oCIDIMFRM_2_42.SetRadio()
    this.Parent.oContained.w_CIDIMFRM=trim(this.Parent.oContained.w_CIDIMFRM)
    this.value = ;
      iif(this.Parent.oContained.w_CIDIMFRM=='S',1,;
      iif(this.Parent.oContained.w_CIDIMFRM=='D',2,;
      0))
  endfunc

  add object oCIWIDFRM_2_44 as StdField with uid="NBISKYWAZW",rtseq=148,rtrep=.f.,;
    cFormVar = "w_CIWIDFRM", cQueryName = "CIWIDFRM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione non valida",;
    ToolTipText = "Dimensione orizzontale maschere",;
    HelpContextID = 67467891,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=229, Top=433, cSayPict='"9999"', cGetPict='"9999"'

  func oCIWIDFRM_2_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIDIMFRM='D')
    endwith
   endif
  endfunc

  func oCIWIDFRM_2_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CIWIDFRM>0)
    endwith
    return bRes
  endfunc

  add object oCIHEIFRM_2_45 as StdField with uid="YYLQQTWQRO",rtseq=149,rtrep=.f.,;
    cFormVar = "w_CIHEIFRM", cQueryName = "CIHEIFRM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Altezza maschere",;
    HelpContextID = 72387187,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=307, Top=433, cSayPict='"9999"', cGetPict='"9999"'

  func oCIHEIFRM_2_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIDIMFRM='D')
    endwith
   endif
  endfunc

  func oCIHEIFRM_2_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CIHEIFRM>0)
    endwith
    return bRes
  endfunc


  add object oCITYPCAL_2_49 as StdCombo with uid="RQZKRHOAIS",rtseq=158,rtrep=.f.,left=396,top=376,width=166,height=21;
    , ToolTipText = "Permette di selezionare il tipo di calendario da visualizzare";
    , HelpContextID = 30755442;
    , cFormVar="w_CITYPCAL",RowSource=""+"Standard,"+"Girevole", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCITYPCAL_2_49.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oCITYPCAL_2_49.GetRadio()
    this.Parent.oContained.w_CITYPCAL = this.RadioValue()
    return .t.
  endfunc

  func oCITYPCAL_2_49.SetRadio()
    this.Parent.oContained.w_CITYPCAL=trim(this.Parent.oContained.w_CITYPCAL)
    this.value = ;
      iif(this.Parent.oContained.w_CITYPCAL=='S',1,;
      iif(this.Parent.oContained.w_CITYPCAL=='R',2,;
      0))
  endfunc


  add object oCISAVFRM_2_51 as StdCombo with uid="PJLUABZPJN",value=1,rtseq=159,rtrep=.f.,left=19,top=464,width=258,height=21;
    , ToolTipText = "Permette il salvataggio della posizione visualizzazione form";
    , HelpContextID = 85801587;
    , cFormVar="w_CISAVFRM",RowSource=""+"Salva sempre posizione,"+"Non salvare posizione ,"+"Disabilita gestione salvataggio", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCISAVFRM_2_51.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'N',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oCISAVFRM_2_51.GetRadio()
    this.Parent.oContained.w_CISAVFRM = this.RadioValue()
    return .t.
  endfunc

  func oCISAVFRM_2_51.SetRadio()
    this.Parent.oContained.w_CISAVFRM=trim(this.Parent.oContained.w_CISAVFRM)
    this.value = ;
      iif(this.Parent.oContained.w_CISAVFRM=='',1,;
      iif(this.Parent.oContained.w_CISAVFRM=='N',2,;
      iif(this.Parent.oContained.w_CISAVFRM=='D',3,;
      0)))
  endfunc

  func oCISAVFRM_2_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIMDIFRM='S')
    endwith
   endif
  endfunc

  add object oStr_2_17 as StdString with uid="LMQOAPKKXG",Visible=.t., Left=18, Top=121,;
    Alignment=0, Width=110, Height=18,;
    Caption="Tema XP"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="TQMNZARRNJ",Visible=.t., Left=16, Top=13,;
    Alignment=0, Width=99, Height=17,;
    Caption="Form / Control"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_20 as StdString with uid="KUXDXMXLAI",Visible=.t., Left=67, Top=378,;
    Alignment=1, Width=190, Height=18,;
    Caption="N� di righe nelle combo box:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="MDCYWKOLAP",Visible=.t., Left=19, Top=32,;
    Alignment=0, Width=165, Height=18,;
    Caption="Modalit� visualizzazione"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="BSYVOZDLMT",Visible=.t., Left=19, Top=76,;
    Alignment=0, Width=94, Height=18,;
    Caption="Tab"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="FCRVRKLLPW",Visible=.t., Left=19, Top=165,;
    Alignment=0, Width=227, Height=18,;
    Caption="Modalit� ricerca per contenuto"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="LOADUBBILQ",Visible=.t., Left=251, Top=311,;
    Alignment=0, Width=60, Height=18,;
    Caption="C.m. : 17%"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="ODPPWRTRBM",Visible=.t., Left=251, Top=140,;
    Alignment=0, Width=53, Height=18,;
    Caption="C.m. : 1%"  ;
  , bGlobalFont=.t.

  func oStr_2_25.mHide()
    with this.Parent.oContained
      return (.w_CIXPTHEM<>1)
    endwith
  endfunc

  add object oStr_2_26 as StdString with uid="RYTDSZDHCC",Visible=.t., Left=251, Top=48,;
    Alignment=0, Width=53, Height=18,;
    Caption="C.m. : 3%"  ;
  , bGlobalFont=.t.

  func oStr_2_26.mHide()
    with this.Parent.oContained
      return (.w_CIMDIFRM='M')
    endwith
  endfunc

  add object oStr_2_27 as StdString with uid="NARCCCKJDZ",Visible=.t., Left=251, Top=48,;
    Alignment=0, Width=63, Height=18,;
    Caption="C.m. : 0,5%"  ;
  , bGlobalFont=.t.

  func oStr_2_27.mHide()
    with this.Parent.oContained
      return (.w_CIMDIFRM<>'M')
    endwith
  endfunc

  add object oStr_2_28 as StdString with uid="TQKPQEVNYQ",Visible=.t., Left=251, Top=140,;
    Alignment=0, Width=53, Height=18,;
    Caption="C.m. : 2%"  ;
  , bGlobalFont=.t.

  func oStr_2_28.mHide()
    with this.Parent.oContained
      return (.w_CIXPTHEM<>0)
    endwith
  endfunc

  add object oStr_2_29 as StdString with uid="TJUWVTXVKS",Visible=.t., Left=396, Top=13,;
    Alignment=0, Width=99, Height=17,;
    Caption="Grid"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_31 as StdString with uid="VAAORWAKSA",Visible=.t., Left=386, Top=125,;
    Alignment=1, Width=190, Height=18,;
    Caption="Altezza righe zoom:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="FTZZWCWVAJ",Visible=.t., Left=386, Top=101,;
    Alignment=1, Width=190, Height=18,;
    Caption="Altezza testata zoom:"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="YYRVEOCSOS",Visible=.t., Left=396, Top=232,;
    Alignment=0, Width=295, Height=18,;
    Caption="Modalit� messaggi di avviso"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="ASDDVYGBBA",Visible=.t., Left=396, Top=276,;
    Alignment=0, Width=239, Height=18,;
    Caption="Modalit� visualizzazione zoom"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="QNYDJVMTDE",Visible=.t., Left=396, Top=207,;
    Alignment=0, Width=99, Height=18,;
    Caption="Zoom"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_43 as StdString with uid="DHPKEUYHDE",Visible=.t., Left=19, Top=414,;
    Alignment=0, Width=200, Height=18,;
    Caption="Dimensione maschere"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="OWFMMHFGNZ",Visible=.t., Left=215, Top=414,;
    Alignment=2, Width=77, Height=18,;
    Caption="Larghezza"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="CWJVJVUOPE",Visible=.t., Left=290, Top=414,;
    Alignment=2, Width=77, Height=18,;
    Caption="Altezza"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="SBZJTVVTGW",Visible=.t., Left=283, Top=435,;
    Alignment=2, Width=18, Height=18,;
    Caption="x"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="SYVKOTWCZM",Visible=.t., Left=396, Top=356,;
    Alignment=0, Width=239, Height=18,;
    Caption="Visualizzazione calendario"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="RLTUMDGLJK",Visible=.t., Left=251, Top=338,;
    Alignment=0, Width=53, Height=17,;
    Caption="C.m. : 1%"  ;
  , bGlobalFont=.t.

  add object oBox_2_19 as StdBox with uid="IEBPSKRUQN",left=10, top=29, width=240,height=1

  add object oBox_2_30 as StdBox with uid="ZNRMSEQJJI",left=390, top=29, width=240,height=1

  add object oBox_2_41 as StdBox with uid="ALEIFRLFYH",left=390, top=224, width=240,height=1
enddefine
define class tgsut_acuPag3 as StdContainer
  Width  = 709
  height = 511
  stdWidth  = 709
  stdheight = 511
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_3_7 as StdButton with uid="KJTASBKMYO",left=258, top=63, width=22,height=19,;
    caption="...", nPag=3;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_3_7.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CIOBLCOL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_8 as StdButton with uid="PEBTFBBJMK",left=258, top=92, width=22,height=19,;
    caption="...", nPag=3;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_3_8.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CISELECL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_9 as StdButton with uid="NXWXMXHDJW",left=258, top=121, width=22,height=19,;
    caption="...", nPag=3;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_3_9.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CIEDTCOL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_10 as StdButton with uid="FKETAYEIAG",left=258, top=150, width=22,height=19,;
    caption="...", nPag=3;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_3_10.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CIRICCOL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_3_11 as cp_setobjprop with uid="LYUZXMHOBI",left=798, top=437, width=184,height=24,;
    caption='Colore sfondo ricerca per contenuto',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORCTRI",;
    nPag=3;
    , ToolTipText = "Colore sfondo control ricerca per contenuto";
    , HelpContextID = 255825109


  add object oBtn_3_12 as StdButton with uid="UNCGHMEXBP",left=521, top=63, width=22,height=19,;
    caption="...", nPag=3;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_3_12.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CIDSBLBC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_13 as StdButton with uid="ZFYVKJJAHZ",left=521, top=92, width=22,height=19,;
    caption="...", nPag=3;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_3_13.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CIDSBLFC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_3_14 as cp_setobjprop with uid="HKZXMTAGRT",left=798, top=236, width=184,height=24,;
    caption='Campi obbligatori',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORCTOB",;
    nPag=3;
    , ToolTipText = "Campi obbligatori";
    , HelpContextID = 184286122

  add object oCOLORCTOB_3_16 as StdField with uid="XURQDVYPNZ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_COLORCTOB", cQueryName = "COLORCTOB",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per evidenziare i campi obbligatori",;
    HelpContextID = 32167061,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=168, Top=63, InputMask=replicate('X',10)


  add object oObj_3_18 as cp_setobjprop with uid="EQRJTNAASA",left=798, top=283, width=184,height=24,;
    caption='Sfondo Control editabili',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORCTED",;
    nPag=3;
    , ToolTipText = "Sfondo control editabili";
    , HelpContextID = 232882841


  add object oObj_3_19 as cp_setobjprop with uid="QXICVSSUMR",left=798, top=260, width=184,height=24,;
    caption='Sfondo Control Selezionato',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORCTSEL",;
    nPag=3;
    , ToolTipText = "Sfondo control selezionato";
    , HelpContextID = 123271523

  add object oCOLORCTED_3_20 as StdField with uid="NPBORGGMIE",rtseq=33,rtrep=.f.,;
    cFormVar = "w_COLORCTED", cQueryName = "COLORCTED",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo dei campi editabili",;
    HelpContextID = 32167083,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=168, Top=121, InputMask=replicate('X',10)

  add object oCOLORCTSEL_3_21 as StdField with uid="MOGOFUELKX",rtseq=34,rtrep=.f.,;
    cFormVar = "w_COLORCTSEL", cQueryName = "COLORCTSEL",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo dei campi selezionati",;
    HelpContextID = 32186569,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=168, Top=92, InputMask=replicate('X',10)


  add object oObj_3_24 as cp_setobjprop with uid="UEBWXYJNIO",left=798, top=414, width=184,height=24,;
    caption='Colore Font control disabilitato',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORFCTDSBL",;
    nPag=3;
    , ToolTipText = "Colore font control disabilitato";
    , HelpContextID = 209974304


  add object oObj_3_25 as cp_setobjprop with uid="EKKBIWECLA",left=798, top=391, width=184,height=24,;
    caption='Colore sfondo control disabilitato',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORBCTDSBL",;
    nPag=3;
    , ToolTipText = "Colore sfondo control disabilitato";
    , HelpContextID = 187768702

  add object oCOLORFCTDSBL_3_26 as StdField with uid="KEPBBFTUIZ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_COLORFCTDSBL", cQueryName = "COLORFCTDSBL",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per il carattere dei campi disabilitati",;
    HelpContextID = 87771066,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=431, Top=92, InputMask=replicate('X',10)

  add object oCOLORBCTDSBL_3_27 as StdField with uid="HAWELLGLKM",rtseq=36,rtrep=.f.,;
    cFormVar = "w_COLORBCTDSBL", cQueryName = "COLORBCTDSBL",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo dei campi disabilitati",;
    HelpContextID = 20662202,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=431, Top=63, InputMask=replicate('X',10)

  add object oCOLORCTRI_3_33 as StdField with uid="PINJIGRYUH",rtseq=37,rtrep=.f.,;
    cFormVar = "w_COLORCTRI", cQueryName = "COLORCTRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per evidenziare i campi sui quali si pu� effettuare una ricerca per contenuto",;
    HelpContextID = 32167176,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=168, Top=150, InputMask=replicate('X',10)


  add object oBtn_3_35 as StdButton with uid="FTMRQULGFT",left=258, top=234, width=22,height=19,;
    caption="...", nPag=3;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_3_35.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CIGRIDCL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_36 as StdButton with uid="RBEAVUZWHM",left=521, top=234, width=22,height=19,;
    caption="...", nPag=3;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_3_36.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CISCRCOL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCIEVIZOM_3_37 as StdCheck with uid="ORRAUIDZIS",rtseq=38,rtrep=.f.,left=100, top=285, caption="Evidenzia la riga selezionata",;
    ToolTipText = "Se attivo, la riga selezionata negli oggetti di tipo elenco verr� evidenziata con un colore",;
    HelpContextID = 140597875,;
    cFormVar="w_CIEVIZOM", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oCIEVIZOM_3_37.RadioValue()
    return(iif(this.value =1,2,;
    0))
  endfunc
  func oCIEVIZOM_3_37.GetRadio()
    this.Parent.oContained.w_CIEVIZOM = this.RadioValue()
    return .t.
  endfunc

  func oCIEVIZOM_3_37.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CIEVIZOM==2,1,;
      0)
  endfunc


  add object oBtn_3_38 as StdButton with uid="KIEABAIYOW",left=258, top=321, width=22,height=19,;
    caption="...", nPag=3;
    , ToolTipText = "Seleziona colore dello sfondo";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_3_38.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CICOLZOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CIEVIZOM = 2)
      endwith
    endif
  endfunc

  func oBtn_3_38.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CIEVIZOM <> 2)
     endwith
    endif
  endfunc

  add object oCOLORGRD_3_40 as StdField with uid="KXBYKVRCAE",rtseq=39,rtrep=.f.,;
    cFormVar = "w_COLORGRD", cQueryName = "COLORGRD",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per le orizzontali e verticali",;
    HelpContextID = 99274858,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=168, Top=234, InputMask=replicate('X',10)

  add object oCOLORSCREEN_3_43 as StdField with uid="QHTHYIIPFX",rtseq=40,rtrep=.f.,;
    cFormVar = "w_COLORSCREEN", cQueryName = "COLORSCREEN",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo del desktop",;
    HelpContextID = 32504264,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=431, Top=234, InputMask=replicate('X',10)


  add object oBtn_3_47 as StdButton with uid="FAQVHSXMZR",left=521, top=320, width=22,height=19,;
    caption="...", nPag=3;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_3_47.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CIDTLCLR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOLORROWDTL_3_49 as StdField with uid="ZTFNGYYBDO",rtseq=41,rtrep=.f.,;
    cFormVar = "w_COLORROWDTL", cQueryName = "COLORROWDTL",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo della riga selezionata in una griglia di dati (se impostato diverso da bianco non saranno applicati i colori di sfondo legati all'editabilit� sulla griglia di dati)",;
    HelpContextID = 15722685,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=431, Top=320, InputMask=replicate('X',10)


  add object oObj_3_51 as cp_setobjprop with uid="HTUAIRSZBU",left=798, top=467, width=184,height=24,;
    caption='Colore Screen',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORSCREEN",;
    nPag=3;
    , ToolTipText = "Colore screen";
    , HelpContextID = 223918249


  add object oObj_3_53 as cp_setobjprop with uid="HQPKMNSDWL",left=798, top=331, width=184,height=24,;
    caption='Colore Riga Selezionata Dtl',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORROWDTL",;
    nPag=3;
    , ToolTipText = "Colore screen";
    , HelpContextID = 84932755


  add object oObj_3_55 as cp_setobjprop with uid="SJXRMMHHHD",left=798, top=307, width=184,height=24,;
    caption='Colore linee griglie',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORGRD",;
    nPag=3;
    , ToolTipText = "Setta colore linee griglia";
    , HelpContextID = 1787909

  add object oCOLORZOM_3_58 as StdField with uid="RIGFTLDQIM",rtseq=45,rtrep=.f.,;
    cFormVar = "w_COLORZOM", cQueryName = "COLORZOM",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo della riga selezionata in un elenco",;
    HelpContextID = 149606515,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=168, Top=321, InputMask=replicate('X',10)

  func oCOLORZOM_3_58.mHide()
    with this.Parent.oContained
      return (.w_CIEVIZOM <> 2)
    endwith
  endfunc


  add object oObj_3_61 as cp_setobjprop with uid="RAKYNHALNR",left=798, top=355, width=184,height=24,;
    caption='Evidenza riga selezionata negli zoom',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORZOM",;
    nPag=3;
    , ToolTipText = "Evidenza riga selezionata negli zoom";
    , HelpContextID = 43956976


  add object oBtn_3_64 as StdButton with uid="CKJDADDETR",left=258, top=388, width=22,height=19,;
    caption="...", nPag=3;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_3_64.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CICOLPST")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOLORPOSTIN_3_65 as StdField with uid="UHNFDUVIRQ",rtseq=103,rtrep=.f.,;
    cFormVar = "w_COLORPOSTIN", cQueryName = "COLORPOSTIN",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo dei post-in",;
    HelpContextID = 17826119,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=168, Top=388, InputMask=replicate('X',10)


  add object oObj_3_67 as cp_setobjprop with uid="XUTPNKGNDI",left=798, top=491, width=184,height=24,;
    caption='Colore post-in',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORPOSTIN",;
    nPag=3;
    , ToolTipText = "Colore post-in";
    , HelpContextID = 86922618

  add object oStr_3_15 as StdString with uid="QZMKQOYXLQ",Visible=.t., Left=29, Top=63,;
    Alignment=1, Width=134, Height=18,;
    Caption="Obbligatori:"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="CEKXRNXBRY",Visible=.t., Left=60, Top=33,;
    Alignment=0, Width=177, Height=18,;
    Caption="Abilitati"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_22 as StdString with uid="DOZEKZBHRE",Visible=.t., Left=29, Top=121,;
    Alignment=1, Width=134, Height=18,;
    Caption="Editabile:"  ;
  , bGlobalFont=.t.

  add object oStr_3_23 as StdString with uid="SIJNQEVDJS",Visible=.t., Left=29, Top=92,;
    Alignment=1, Width=134, Height=18,;
    Caption="Selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_3_28 as StdString with uid="WBTKYRDXRM",Visible=.t., Left=295, Top=92,;
    Alignment=1, Width=134, Height=18,;
    Caption="Carattere:"  ;
  , bGlobalFont=.t.

  add object oStr_3_29 as StdString with uid="KZYMQJXXIO",Visible=.t., Left=295, Top=63,;
    Alignment=1, Width=134, Height=18,;
    Caption="Sfondo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_30 as StdString with uid="KOJIFVHWEB",Visible=.t., Left=306, Top=33,;
    Alignment=0, Width=177, Height=18,;
    Caption="Disabilitati"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_34 as StdString with uid="RSWFJADMQO",Visible=.t., Left=29, Top=150,;
    Alignment=1, Width=134, Height=18,;
    Caption="Ricerca su contenuto:"  ;
  , bGlobalFont=.t.

  add object oStr_3_39 as StdString with uid="AHEXUSEGBY",Visible=.t., Left=29, Top=234,;
    Alignment=1, Width=134, Height=18,;
    Caption="Linee griglie:"  ;
  , bGlobalFont=.t.

  add object oStr_3_41 as StdString with uid="OYNODWFTEC",Visible=.t., Left=60, Top=204,;
    Alignment=0, Width=177, Height=18,;
    Caption="Elenchi / zoom"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_44 as StdString with uid="WVSMJOVFHN",Visible=.t., Left=295, Top=234,;
    Alignment=1, Width=134, Height=18,;
    Caption="Sfondo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_45 as StdString with uid="MRONQBDVIX",Visible=.t., Left=306, Top=204,;
    Alignment=0, Width=177, Height=18,;
    Caption="Desktop"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_48 as StdString with uid="KAFSEBDEFM",Visible=.t., Left=295, Top=320,;
    Alignment=1, Width=134, Height=18,;
    Caption="Riga selezionata:"  ;
  , bGlobalFont=.t.

  add object oStr_3_56 as StdString with uid="TZEEBVPTMD",Visible=.t., Left=306, Top=290,;
    Alignment=0, Width=177, Height=18,;
    Caption="Griglie di dati"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_59 as StdString with uid="YGHAYYNNKF",Visible=.t., Left=21, Top=321,;
    Alignment=1, Width=142, Height=18,;
    Caption="Colore riga selezionata:"  ;
  , bGlobalFont=.t.

  func oStr_3_59.mHide()
    with this.Parent.oContained
      return (.w_CIEVIZOM <> 2)
    endwith
  endfunc

  add object oStr_3_62 as StdString with uid="PUKSRDLMNZ",Visible=.t., Left=60, Top=358,;
    Alignment=0, Width=177, Height=18,;
    Caption="Post-IN"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_66 as StdString with uid="LUSTJYCKJP",Visible=.t., Left=29, Top=388,;
    Alignment=1, Width=134, Height=18,;
    Caption="Sfondo:"  ;
  , bGlobalFont=.t.

  add object oBox_3_31 as StdBox with uid="UVDBTBFTNC",left=44, top=50, width=248,height=1

  add object oBox_3_32 as StdBox with uid="FMNXZMMLUY",left=302, top=50, width=248,height=1

  add object oBox_3_42 as StdBox with uid="WBEAFAEBLG",left=44, top=221, width=248,height=1

  add object oBox_3_46 as StdBox with uid="TPWVWCSGLL",left=302, top=221, width=248,height=1

  add object oBox_3_57 as StdBox with uid="GKWHLPOCVK",left=302, top=307, width=248,height=1

  add object oBox_3_63 as StdBox with uid="TPBPPSRTZO",left=44, top=375, width=248,height=1
enddefine
define class tgsut_acuPag4 as StdContainer
  Width  = 709
  height = 511
  stdWidth  = 709
  stdheight = 511
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCILBLFNM_4_1 as StdTableComboFont with uid="XEWFUYKPVZ",rtseq=47,rtrep=.f.,left=127,top=48,width=164,height=21;
    , ToolTipText = "Font da utilizzare per la etichette, checkbox, radiobox";
    , HelpContextID = 193082765;
    , cFormVar="w_CILBLFNM",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.



  add object oCITXTFNM_4_2 as StdTableComboFont with uid="AEBOTFGNNC",rtseq=48,rtrep=.f.,left=127,top=82,width=164,height=21;
    , ToolTipText = "Font da utilizzare per textbox, memo";
    , HelpContextID = 183219597;
    , cFormVar="w_CITXTFNM",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.



  add object oCICBXFNM_4_3 as StdTableComboFont with uid="NXNLBIHBGE",rtseq=49,rtrep=.f.,left=127,top=116,width=164,height=21;
    , ToolTipText = "Font da utilizzare per combobox";
    , HelpContextID = 180536717;
    , cFormVar="w_CICBXFNM",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.



  add object oCIBTNFNM_4_4 as StdTableComboFont with uid="FYKEPEUGQU",rtseq=50,rtrep=.f.,left=127,top=150,width=164,height=21;
    , ToolTipText = "Font da utilizzare per i bottoni";
    , HelpContextID = 189846925;
    , cFormVar="w_CIBTNFNM",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.



  add object oCIGRDFNM_4_5 as StdTableComboFont with uid="JJSHBWWONC",rtseq=51,rtrep=.f.,left=127,top=184,width=164,height=21;
    , ToolTipText = "Font da utilizzare per gli zoom";
    , HelpContextID = 200443277;
    , cFormVar="w_CIGRDFNM",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.



  add object oCIPAGFNM_4_6 as StdTableComboFont with uid="LCIYMQPBZY",rtseq=52,rtrep=.f.,left=127,top=218,width=164,height=21;
    , ToolTipText = "Font da utilizzare per le tab";
    , HelpContextID = 198374797;
    , cFormVar="w_CIPAGFNM",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  add object oCILBLFSZ_4_13 as StdField with uid="KTPMHIBSVZ",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CILBLFSZ", cQueryName = "CILBLFSZ",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font etichette",;
    HelpContextID = 75352704,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=407, Top=48, cSayPict='"999"', cGetPict='"999"'

  func oCILBLFSZ_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CILBLFNM) and TestFont('L', .w_CILBLFNM, .w_CILBLFSZ)<>-1 And !Empty(.w_CILBLFSZ))
      if bRes and !(TestFont('L', .w_CILBLFNM, .w_CILBLFSZ) = 1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oCITXTFSZ_4_15 as StdField with uid="MZYXYCYXKG",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CITXTFSZ", cQueryName = "CITXTFSZ",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font campi",;
    HelpContextID = 85215872,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=407, Top=82, cSayPict='"999"', cGetPict='"999"'

  func oCITXTFSZ_4_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CITXTFNM) and TestFont('T', .w_CITXTFNM, .w_CITXTFSZ)<>-1 And !Empty(.w_CITXTFSZ))
      if bRes and !(TestFont('T', .w_CITXTFNM, .w_CITXTFSZ) = 1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oCICBXFSZ_4_17 as StdField with uid="ELFXAWJCPG",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CICBXFSZ", cQueryName = "CICBXFSZ",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font combobox",;
    HelpContextID = 87898752,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=407, Top=116, cSayPict='"999"', cGetPict='"999"'

  func oCICBXFSZ_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CICBXFNM) and TestFont('C', .w_CICBXFNM, .w_CICBXFSZ)<>-1 And !Empty(.w_CICBXFSZ))
      if bRes and !(TestFont('C', .w_CICBXFNM, .w_CICBXFSZ) = 1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oCIBTNFSZ_4_19 as StdField with uid="CDYSADIXOB",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CIBTNFSZ", cQueryName = "CIBTNFSZ",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font bottoni",;
    HelpContextID = 78588544,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=407, Top=150, cSayPict='"999"', cGetPict='"999"'

  func oCIBTNFSZ_4_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CIBTNFNM) and TestFont('B', .w_CIBTNFNM, .w_CIBTNFSZ)<>-1 And !Empty(.w_CIBTNFSZ))
      if bRes and !(TestFont('B', .w_CIBTNFNM, .w_CIBTNFSZ)=1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oCIGRDFSZ_4_21 as StdField with uid="HAYREGIFUQ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CIGRDFSZ", cQueryName = "CIGRDFSZ",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font zoom",;
    HelpContextID = 67992192,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=407, Top=184, cSayPict='"999"', cGetPict='"999"'

  func oCIGRDFSZ_4_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CIGRDFNM) and TestFont('G', .w_CIGRDFNM, .w_CIGRDFSZ)<>-1 And !Empty(.w_CIGRDFSZ))
      if bRes and !(TestFont('G', .w_CIGRDFNM, .w_CIGRDFSZ)=1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oCIPAGFSZ_4_23 as StdField with uid="TZTVIGUUPA",rtseq=58,rtrep=.f.,;
    cFormVar = "w_CIPAGFSZ", cQueryName = "CIPAGFSZ",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font tab",;
    HelpContextID = 70060672,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=407, Top=218, cSayPict='"999"', cGetPict='"999"'

  func oCIPAGFSZ_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CIPAGFNM) and TestFont('P', .w_CIPAGFNM, .w_CIPAGFSZ)<>-1 And !Empty(.w_CIPAGFSZ))
      if bRes and !(TestFont('P', .w_CIPAGFNM, .w_CIPAGFSZ)=1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oCILBLFIT_4_25 as StdCheck with uid="ZVWDSPGSBR",rtseq=106,rtrep=.f.,left=499, top=48, caption="Italic",;
    HelpContextID = 193082758,;
    cFormVar="w_CILBLFIT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCILBLFIT_4_25.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCILBLFIT_4_25.GetRadio()
    this.Parent.oContained.w_CILBLFIT = this.RadioValue()
    return .t.
  endfunc

  func oCILBLFIT_4_25.SetRadio()
    this.Parent.oContained.w_CILBLFIT=trim(this.Parent.oContained.w_CILBLFIT)
    this.value = ;
      iif(this.Parent.oContained.w_CILBLFIT=='S',1,;
      0)
  endfunc

  add object oCITXTFIT_4_26 as StdCheck with uid="PCDBEISXPW",rtseq=107,rtrep=.f.,left=499, top=82, caption="Italic",;
    HelpContextID = 183219590,;
    cFormVar="w_CITXTFIT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCITXTFIT_4_26.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCITXTFIT_4_26.GetRadio()
    this.Parent.oContained.w_CITXTFIT = this.RadioValue()
    return .t.
  endfunc

  func oCITXTFIT_4_26.SetRadio()
    this.Parent.oContained.w_CITXTFIT=trim(this.Parent.oContained.w_CITXTFIT)
    this.value = ;
      iif(this.Parent.oContained.w_CITXTFIT=='S',1,;
      0)
  endfunc

  add object oCICBXFIT_4_27 as StdCheck with uid="PUGVECLQRN",rtseq=108,rtrep=.f.,left=499, top=116, caption="Italic",;
    HelpContextID = 180536710,;
    cFormVar="w_CICBXFIT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCICBXFIT_4_27.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCICBXFIT_4_27.GetRadio()
    this.Parent.oContained.w_CICBXFIT = this.RadioValue()
    return .t.
  endfunc

  func oCICBXFIT_4_27.SetRadio()
    this.Parent.oContained.w_CICBXFIT=trim(this.Parent.oContained.w_CICBXFIT)
    this.value = ;
      iif(this.Parent.oContained.w_CICBXFIT=='S',1,;
      0)
  endfunc

  add object oCIBTNFIT_4_28 as StdCheck with uid="RSYBGVOCWW",rtseq=109,rtrep=.f.,left=499, top=150, caption="Italic",;
    HelpContextID = 189846918,;
    cFormVar="w_CIBTNFIT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCIBTNFIT_4_28.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIBTNFIT_4_28.GetRadio()
    this.Parent.oContained.w_CIBTNFIT = this.RadioValue()
    return .t.
  endfunc

  func oCIBTNFIT_4_28.SetRadio()
    this.Parent.oContained.w_CIBTNFIT=trim(this.Parent.oContained.w_CIBTNFIT)
    this.value = ;
      iif(this.Parent.oContained.w_CIBTNFIT=='S',1,;
      0)
  endfunc

  add object oCIGRDFIT_4_29 as StdCheck with uid="XXQSRCESWM",rtseq=110,rtrep=.f.,left=499, top=184, caption="Italic",;
    HelpContextID = 200443270,;
    cFormVar="w_CIGRDFIT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCIGRDFIT_4_29.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIGRDFIT_4_29.GetRadio()
    this.Parent.oContained.w_CIGRDFIT = this.RadioValue()
    return .t.
  endfunc

  func oCIGRDFIT_4_29.SetRadio()
    this.Parent.oContained.w_CIGRDFIT=trim(this.Parent.oContained.w_CIGRDFIT)
    this.value = ;
      iif(this.Parent.oContained.w_CIGRDFIT=='S',1,;
      0)
  endfunc

  add object oCIPAGFIT_4_30 as StdCheck with uid="NWDWWQMMDF",rtseq=111,rtrep=.f.,left=499, top=218, caption="Italic",;
    HelpContextID = 198374790,;
    cFormVar="w_CIPAGFIT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCIPAGFIT_4_30.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIPAGFIT_4_30.GetRadio()
    this.Parent.oContained.w_CIPAGFIT = this.RadioValue()
    return .t.
  endfunc

  func oCIPAGFIT_4_30.SetRadio()
    this.Parent.oContained.w_CIPAGFIT=trim(this.Parent.oContained.w_CIPAGFIT)
    this.value = ;
      iif(this.Parent.oContained.w_CIPAGFIT=='S',1,;
      0)
  endfunc

  add object oCILBLFBO_4_31 as StdCheck with uid="YLOPRWXCNT",rtseq=112,rtrep=.f.,left=559, top=48, caption="Bold",;
    HelpContextID = 75352693,;
    cFormVar="w_CILBLFBO", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCILBLFBO_4_31.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCILBLFBO_4_31.GetRadio()
    this.Parent.oContained.w_CILBLFBO = this.RadioValue()
    return .t.
  endfunc

  func oCILBLFBO_4_31.SetRadio()
    this.Parent.oContained.w_CILBLFBO=trim(this.Parent.oContained.w_CILBLFBO)
    this.value = ;
      iif(this.Parent.oContained.w_CILBLFBO=='S',1,;
      0)
  endfunc

  add object oCITXTFBO_4_32 as StdCheck with uid="JCOBAWCLQV",rtseq=113,rtrep=.f.,left=559, top=82, caption="Bold",;
    HelpContextID = 85215861,;
    cFormVar="w_CITXTFBO", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCITXTFBO_4_32.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCITXTFBO_4_32.GetRadio()
    this.Parent.oContained.w_CITXTFBO = this.RadioValue()
    return .t.
  endfunc

  func oCITXTFBO_4_32.SetRadio()
    this.Parent.oContained.w_CITXTFBO=trim(this.Parent.oContained.w_CITXTFBO)
    this.value = ;
      iif(this.Parent.oContained.w_CITXTFBO=='S',1,;
      0)
  endfunc

  add object oCICBXFBO_4_33 as StdCheck with uid="CAVJHIUWRT",rtseq=114,rtrep=.f.,left=559, top=116, caption="Bold",;
    HelpContextID = 87898741,;
    cFormVar="w_CICBXFBO", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCICBXFBO_4_33.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCICBXFBO_4_33.GetRadio()
    this.Parent.oContained.w_CICBXFBO = this.RadioValue()
    return .t.
  endfunc

  func oCICBXFBO_4_33.SetRadio()
    this.Parent.oContained.w_CICBXFBO=trim(this.Parent.oContained.w_CICBXFBO)
    this.value = ;
      iif(this.Parent.oContained.w_CICBXFBO=='S',1,;
      0)
  endfunc

  add object oCIBTNFBO_4_34 as StdCheck with uid="JZCIWULNQJ",rtseq=115,rtrep=.f.,left=559, top=150, caption="Bold",;
    HelpContextID = 78588533,;
    cFormVar="w_CIBTNFBO", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCIBTNFBO_4_34.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIBTNFBO_4_34.GetRadio()
    this.Parent.oContained.w_CIBTNFBO = this.RadioValue()
    return .t.
  endfunc

  func oCIBTNFBO_4_34.SetRadio()
    this.Parent.oContained.w_CIBTNFBO=trim(this.Parent.oContained.w_CIBTNFBO)
    this.value = ;
      iif(this.Parent.oContained.w_CIBTNFBO=='S',1,;
      0)
  endfunc

  add object oCIGRDFBO_4_35 as StdCheck with uid="RIPYOINPFE",rtseq=116,rtrep=.f.,left=559, top=184, caption="Bold",;
    HelpContextID = 67992181,;
    cFormVar="w_CIGRDFBO", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCIGRDFBO_4_35.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIGRDFBO_4_35.GetRadio()
    this.Parent.oContained.w_CIGRDFBO = this.RadioValue()
    return .t.
  endfunc

  func oCIGRDFBO_4_35.SetRadio()
    this.Parent.oContained.w_CIGRDFBO=trim(this.Parent.oContained.w_CIGRDFBO)
    this.value = ;
      iif(this.Parent.oContained.w_CIGRDFBO=='S',1,;
      0)
  endfunc

  add object oCIPAGFBO_4_36 as StdCheck with uid="PCCERGZQZJ",rtseq=117,rtrep=.f.,left=559, top=218, caption="Bold",;
    HelpContextID = 70060661,;
    cFormVar="w_CIPAGFBO", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCIPAGFBO_4_36.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIPAGFBO_4_36.GetRadio()
    this.Parent.oContained.w_CIPAGFBO = this.RadioValue()
    return .t.
  endfunc

  func oCIPAGFBO_4_36.SetRadio()
    this.Parent.oContained.w_CIPAGFBO=trim(this.Parent.oContained.w_CIPAGFBO)
    this.value = ;
      iif(this.Parent.oContained.w_CIPAGFBO=='S',1,;
      0)
  endfunc

  add object oStr_4_7 as StdString with uid="ADMMUSPOPS",Visible=.t., Left=10, Top=48,;
    Alignment=1, Width=112, Height=18,;
    Caption="Font etichette:"  ;
  , bGlobalFont=.t.

  add object oStr_4_8 as StdString with uid="BNIFNBVKCO",Visible=.t., Left=10, Top=82,;
    Alignment=1, Width=112, Height=17,;
    Caption="Font campi:"  ;
  , bGlobalFont=.t.

  add object oStr_4_9 as StdString with uid="CQKRHDMKUH",Visible=.t., Left=10, Top=150,;
    Alignment=1, Width=112, Height=17,;
    Caption="Font bottoni:"  ;
  , bGlobalFont=.t.

  add object oStr_4_10 as StdString with uid="XKMJQKCKEI",Visible=.t., Left=10, Top=116,;
    Alignment=1, Width=112, Height=17,;
    Caption="Font combobox:"  ;
  , bGlobalFont=.t.

  add object oStr_4_11 as StdString with uid="UUIYYLBVDV",Visible=.t., Left=10, Top=184,;
    Alignment=1, Width=112, Height=17,;
    Caption="Font zoom:"  ;
  , bGlobalFont=.t.

  add object oStr_4_12 as StdString with uid="TNZYLLZWTX",Visible=.t., Left=10, Top=218,;
    Alignment=1, Width=112, Height=17,;
    Caption="Font tab:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="EYUYQDCQFF",Visible=.t., Left=299, Top=48,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="DWOOPVJKAO",Visible=.t., Left=299, Top=82,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_18 as StdString with uid="YPNFLIHBPX",Visible=.t., Left=299, Top=116,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_20 as StdString with uid="AVEECVEIJT",Visible=.t., Left=299, Top=150,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_22 as StdString with uid="TIKPZEESDU",Visible=.t., Left=299, Top=184,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_24 as StdString with uid="WLZLVADTVY",Visible=.t., Left=299, Top=218,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsut_acuPag5 as StdContainer
  Width  = 709
  height = 511
  stdWidth  = 709
  stdheight = 511
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCISYSFRM_5_1 as StdCombo with uid="BBRHWXCHHJ",rtseq=59,rtrep=.f.,left=124,top=42,width=147,height=21;
    , HelpContextID = 84228723;
    , cFormVar="w_CISYSFRM",RowSource=""+"Sistema operativo,"+"Ad hoc", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oCISYSFRM_5_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oCISYSFRM_5_1.GetRadio()
    this.Parent.oContained.w_CISYSFRM = this.RadioValue()
    return .t.
  endfunc

  func oCISYSFRM_5_1.SetRadio()
    this.Parent.oContained.w_CISYSFRM=trim(this.Parent.oContained.w_CISYSFRM)
    this.value = ;
      iif(this.Parent.oContained.w_CISYSFRM=='S',1,;
      iif(this.Parent.oContained.w_CISYSFRM=='N',2,;
      0))
  endfunc

  add object oPRVNUM_5_3 as StdField with uid="IHTOWOIWWP",rtseq=60,rtrep=.f.,;
    cFormVar = "w_PRVNUM", cQueryName = "PRVNUM",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 203060214,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=423, Top=242, InputMask=replicate('X',12)

  add object oPRVDATA_5_4 as StdField with uid="TZTAXEYKZO",rtseq=61,rtrep=.f.,;
    cFormVar = "w_PRVDATA", cQueryName = "PRVDATA",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 30438390,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=423, Top=114


  add object oSETDAT_5_18 as StdCombo with uid="ORKRVQKYSR",rtseq=67,rtrep=.f.,left=124,top=114,width=162,height=21;
    , ToolTipText = "Formato data";
    , HelpContextID = 30426918;
    , cFormVar="w_SETDAT",RowSource=""+"Italian dd-MM-yy (default),"+"American mm/dd/yy,"+"Ansi yy.mm.dd,"+"British/french dd/mm/yy,"+"German dd.MM.yy,"+"Japan yy/mm/dd,"+"Taiwan yy/mm/dd,"+"Usa MM-dd-yy,"+"Mdy mm/dd/yy,"+"Dmy dd/mm/yy,"+"Ymd yy/mm/dd,"+"Short", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oSETDAT_5_18.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    iif(this.value =3,'N',;
    iif(this.value =4,'B',;
    iif(this.value =5,'G',;
    iif(this.value =6,'J',;
    iif(this.value =7,'T',;
    iif(this.value =8,'U',;
    iif(this.value =9,'M',;
    iif(this.value =10,'D',;
    iif(this.value =11,'Y',;
    iif(this.value =12,'S',;
    space(1))))))))))))))
  endfunc
  func oSETDAT_5_18.GetRadio()
    this.Parent.oContained.w_SETDAT = this.RadioValue()
    return .t.
  endfunc

  func oSETDAT_5_18.SetRadio()
    this.Parent.oContained.w_SETDAT=trim(this.Parent.oContained.w_SETDAT)
    this.value = ;
      iif(this.Parent.oContained.w_SETDAT=='I',1,;
      iif(this.Parent.oContained.w_SETDAT=='A',2,;
      iif(this.Parent.oContained.w_SETDAT=='N',3,;
      iif(this.Parent.oContained.w_SETDAT=='B',4,;
      iif(this.Parent.oContained.w_SETDAT=='G',5,;
      iif(this.Parent.oContained.w_SETDAT=='J',6,;
      iif(this.Parent.oContained.w_SETDAT=='T',7,;
      iif(this.Parent.oContained.w_SETDAT=='U',8,;
      iif(this.Parent.oContained.w_SETDAT=='M',9,;
      iif(this.Parent.oContained.w_SETDAT=='D',10,;
      iif(this.Parent.oContained.w_SETDAT=='Y',11,;
      iif(this.Parent.oContained.w_SETDAT=='S',12,;
      0))))))))))))
  endfunc

  func oSETDAT_5_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CISYSFRM<>'S')
    endwith
   endif
  endfunc

  add object oSETMRK_5_19 as StdField with uid="LIWZDXINXG",rtseq=68,rtrep=.f.,;
    cFormVar = "w_SETMRK", cQueryName = "SETMRK",;
    bObbl = .f. , nPag = 5, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Separatore delle date",;
    HelpContextID = 166283046,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=124, Top=152, InputMask=replicate('X',1)

  func oSETMRK_5_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CISYSFRM<>'S' AND .w_CISETDAT<>'S')
    endwith
   endif
  endfunc

  add object oSETCEN_5_20 as StdCheck with uid="UDJKWTHFEB",rtseq=69,rtrep=.f.,left=190, top=151, caption="Anno (1998 vs 98)",;
    ToolTipText = "Anno espresso con 4 cifre (31/12/2005) o con 2 (31/12/05)",;
    HelpContextID = 202327846,;
    cFormVar="w_SETCEN", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oSETCEN_5_20.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSETCEN_5_20.GetRadio()
    this.Parent.oContained.w_SETCEN = this.RadioValue()
    return .t.
  endfunc

  func oSETCEN_5_20.SetRadio()
    this.Parent.oContained.w_SETCEN=trim(this.Parent.oContained.w_SETCEN)
    this.value = ;
      iif(this.Parent.oContained.w_SETCEN=='S',1,;
      0)
  endfunc

  func oSETCEN_5_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CISYSFRM<>'S' AND .w_CISETDAT<>'S')
    endwith
   endif
  endfunc


  add object oSETPNT_5_21 as StdCombo with uid="ZBYBMVNBBO",rtseq=70,rtrep=.f.,left=124,top=242,width=68,height=21;
    , ToolTipText = "Separatore delle decine";
    , HelpContextID = 44844838;
    , cFormVar="w_SETPNT",RowSource=""+"Virgola,"+"Punto", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oSETPNT_5_21.RadioValue()
    return(iif(this.value =1,",",;
    iif(this.value =2,".",;
    space(1))))
  endfunc
  func oSETPNT_5_21.GetRadio()
    this.Parent.oContained.w_SETPNT = this.RadioValue()
    return .t.
  endfunc

  func oSETPNT_5_21.SetRadio()
    this.Parent.oContained.w_SETPNT=trim(this.Parent.oContained.w_SETPNT)
    this.value = ;
      iif(this.Parent.oContained.w_SETPNT==",",1,;
      iif(this.Parent.oContained.w_SETPNT==".",2,;
      0))
  endfunc

  func oSETPNT_5_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CISYSFRM<>'S')
    endwith
   endif
  endfunc


  add object oSETSEP_5_22 as StdCombo with uid="QZZTVQAZAH",rtseq=71,rtrep=.f.,left=325,top=242,width=68,height=21;
    , ToolTipText = "Separatore delle migliaia";
    , HelpContextID = 236930854;
    , cFormVar="w_SETSEP",RowSource=""+"Punto,"+"Virgola", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oSETSEP_5_22.RadioValue()
    return(iif(this.value =1,".",;
    iif(this.value =2,",",;
    space(1))))
  endfunc
  func oSETSEP_5_22.GetRadio()
    this.Parent.oContained.w_SETSEP = this.RadioValue()
    return .t.
  endfunc

  func oSETSEP_5_22.SetRadio()
    this.Parent.oContained.w_SETSEP=trim(this.Parent.oContained.w_SETSEP)
    this.value = ;
      iif(this.Parent.oContained.w_SETSEP==".",1,;
      iif(this.Parent.oContained.w_SETSEP==",",2,;
      0))
  endfunc

  func oSETSEP_5_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CISYSFRM<>'S')
    endwith
   endif
  endfunc

  add object oStr_5_2 as StdString with uid="FNEHNEFHMI",Visible=.t., Left=6, Top=42,;
    Alignment=1, Width=114, Height=17,;
    Caption="Impostazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_5_7 as StdString with uid="PKGRYYTSRR",Visible=.t., Left=6, Top=114,;
    Alignment=1, Width=114, Height=17,;
    Caption="Formato data:"  ;
  , bGlobalFont=.t.

  add object oStr_5_9 as StdString with uid="OKQHUXFLHP",Visible=.t., Left=6, Top=244,;
    Alignment=1, Width=114, Height=18,;
    Caption="Sep. decine:"  ;
  , bGlobalFont=.t.

  add object oStr_5_11 as StdString with uid="ALHKNMMYPZ",Visible=.t., Left=200, Top=244,;
    Alignment=1, Width=124, Height=18,;
    Caption="Sep. migliaia:"  ;
  , bGlobalFont=.t.

  add object oStr_5_13 as StdString with uid="DFFXQROYGI",Visible=.t., Left=6, Top=152,;
    Alignment=1, Width=114, Height=18,;
    Caption="Sep. delle date:"  ;
  , bGlobalFont=.t.

  add object oStr_5_14 as StdString with uid="AOOSZZDTRG",Visible=.t., Left=39, Top=90,;
    Alignment=0, Width=39, Height=18,;
    Caption="Data"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_17 as StdString with uid="XOAEQCYIYT",Visible=.t., Left=39, Top=220,;
    Alignment=0, Width=64, Height=18,;
    Caption="Numerici"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_26 as StdString with uid="NBVRNSWVEK",Visible=.t., Left=289, Top=40,;
    Alignment=0, Width=53, Height=18,;
    Caption="C.m. : 3%"  ;
  , bGlobalFont=.t.

  func oStr_5_26.mHide()
    with this.Parent.oContained
      return (.w_CISYSFRM='S')
    endwith
  endfunc

  add object oStr_5_27 as StdString with uid="MJPBSGNIUI",Visible=.t., Left=289, Top=40,;
    Alignment=0, Width=63, Height=18,;
    Caption="C.m. : 0,1%"  ;
  , bGlobalFont=.t.

  func oStr_5_27.mHide()
    with this.Parent.oContained
      return (.w_CISYSFRM<>'S')
    endwith
  endfunc

  add object oBox_5_15 as StdBox with uid="IHTJCEOZKS",left=9, top=107, width=497,height=1

  add object oBox_5_16 as StdBox with uid="EVAUSTGVGY",left=9, top=237, width=497,height=1
enddefine
define class tgsut_acuPag6 as StdContainer
  Width  = 709
  height = 511
  stdWidth  = 709
  stdheight = 511
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_6_8 as StdButton with uid="YWIBXNTEOD",left=261, top=59, width=22,height=19,;
    caption="...", nPag=6;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_6_8.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CICOLPRM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_6_9 as StdButton with uid="TLZQBOJQNZ",left=261, top=83, width=22,height=19,;
    caption="...", nPag=6;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_6_9.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CICOLNNP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_6_10 as StdButton with uid="QAEAZDKSNM",left=261, top=107, width=22,height=19,;
    caption="...", nPag=6;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_6_10.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CICOLHDR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_6_11 as StdButton with uid="RINXDASKTU",left=261, top=131, width=22,height=19,;
    caption="...", nPag=6;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_6_11.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CICOLSEL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_6_12 as StdButton with uid="EQWTOINARO",left=262, top=172, width=22,height=19,;
    caption="...", nPag=6;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_6_12.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CIATUCOL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_6_13 as StdButton with uid="NCCGYEXIGE",left=262, top=196, width=22,height=19,;
    caption="...", nPag=6;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_6_13.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CIATTCOL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_6_14 as StdButton with uid="PZBSNDLYBI",left=262, top=220, width=22,height=19,;
    caption="...", nPag=6;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_6_14.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CIATCCOL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_6_15 as StdButton with uid="EPWFCGJPMZ",left=262, top=244, width=22,height=19,;
    caption="...", nPag=6;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 109457962;
  , bGlobalFont=.t.

    proc oBtn_6_15.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CICOLACF")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOLORATU_6_16 as StdField with uid="GJTMAFFCIS",rtseq=79,rtrep=.f.,;
    cFormVar = "w_COLORATU", cQueryName = "COLORATU",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per le attivit� con priorit� urgente",;
    HelpContextID = 267047035,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=172, Top=172, InputMask=replicate('X',10)


  add object oObj_6_18 as cp_setobjprop with uid="GBATVVLTGX",left=802, top=220, width=184,height=24,;
    caption='Colore attivit� con priorit� urgente',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORATU",;
    nPag=6;
    , ToolTipText = "Colore attivit� con priorit� urgente";
    , HelpContextID = 43365981

  add object oCOLORATT_6_19 as StdField with uid="RKUKWJAPJX",rtseq=80,rtrep=.f.,;
    cFormVar = "w_COLORATT", cQueryName = "COLORATT",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per le attivit� con priorit� scadenza termine",;
    HelpContextID = 267047034,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=172, Top=196, InputMask=replicate('X',10)

  add object oCOLORATC_6_21 as StdField with uid="NLIFKYFRAY",rtseq=81,rtrep=.f.,;
    cFormVar = "w_COLORATC", cQueryName = "COLORATC",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per le attivit� con stato evasa o completata",;
    HelpContextID = 267047017,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=172, Top=220, InputMask=replicate('X',10)


  add object oObj_6_23 as cp_setobjprop with uid="KWEBVUIFXR",left=802, top=244, width=184,height=24,;
    caption='Colore attivit� con priorit� scadenza termine',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORATT",;
    nPag=6;
    , ToolTipText = "Colore attivit� con priorit� scadenza termine";
    , HelpContextID = 41987636


  add object oObj_6_24 as cp_setobjprop with uid="KVSQHHCNON",left=802, top=268, width=184,height=24,;
    caption='Colore attivit� con stato completata',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORATC",;
    nPag=6;
    , ToolTipText = "Colore attivit� con stato completata";
    , HelpContextID = 69161248

  add object oCIAGEVIS_6_25 as StdRadio with uid="JNOJONGVPF",rtseq=82,rtrep=.f.,left=354, top=64, width=128,height=81;
    , ToolTipText = "Visualizzazione iniziale";
    , cFormVar="w_CIAGEVIS", ButtonCount=5, bObbl=.f., nPag=6;
  , bGlobalFont=.t.

    proc oCIAGEVIS_6_25.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Giornaliero"
      this.Buttons(1).HelpContextID = 200140167
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Lavorativo"
      this.Buttons(2).HelpContextID = 200140167
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="Settimanale"
      this.Buttons(3).HelpContextID = 200140167
      this.Buttons(3).Top=30
      this.Buttons(4).Caption="Mensile"
      this.Buttons(4).HelpContextID = 200140167
      this.Buttons(4).Top=45
      this.Buttons(5).Caption="Annuale"
      this.Buttons(5).HelpContextID = 200140167
      this.Buttons(5).Top=60
      this.SetAll("Width",126)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Visualizzazione iniziale")
      StdRadio::init()
    endproc

  func oCIAGEVIS_6_25.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,5,;
    iif(this.value =5,6,;
    0))))))
  endfunc
  func oCIAGEVIS_6_25.GetRadio()
    this.Parent.oContained.w_CIAGEVIS = this.RadioValue()
    return .t.
  endfunc

  func oCIAGEVIS_6_25.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CIAGEVIS==1,1,;
      iif(this.Parent.oContained.w_CIAGEVIS==2,2,;
      iif(this.Parent.oContained.w_CIAGEVIS==3,3,;
      iif(this.Parent.oContained.w_CIAGEVIS==5,4,;
      iif(this.Parent.oContained.w_CIAGEVIS==6,5,;
      0)))))
  endfunc

  add object oCOLORPRM_6_33 as StdField with uid="LKHBGYHSVJ",rtseq=83,rtrep=.f.,;
    cFormVar = "w_COLORPRM", cQueryName = "COLORPRM",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per lo sfondo (orario di lavoro per agenda)",;
    HelpContextID = 250269811,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=172, Top=59, InputMask=replicate('X',10)

  add object oCOLORNNP_6_34 as StdField with uid="TMWEGNXBLQ",rtseq=84,rtrep=.f.,;
    cFormVar = "w_COLORNNP", cQueryName = "COLORNNP",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per il bordo nella disponibililit� risorse e per l'orario di chiusura nell'agenda",;
    HelpContextID = 51720074,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=172, Top=83, InputMask=replicate('X',10)

  add object oCOLORHDR_6_35 as StdField with uid="FYKEGIIQAK",rtseq=85,rtrep=.f.,;
    cFormVar = "w_COLORHDR", cQueryName = "COLORHDR",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per l'intestazione",;
    HelpContextID = 116052088,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=172, Top=107, InputMask=replicate('X',10)

  add object oCOLORSEL_6_36 as StdField with uid="GMNDZGNPKJ",rtseq=86,rtrep=.f.,;
    cFormVar = "w_COLORSEL", cQueryName = "COLORSEL",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per la selezione nella disponibilit� risorse e nell'agenda",;
    HelpContextID = 32166002,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=172, Top=131, InputMask=replicate('X',10)


  add object oObj_6_37 as cp_setobjprop with uid="SACJFHEPVH",left=802, top=327, width=184,height=24,;
    caption='Colore orario di lavoro',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORPRM",;
    nPag=6;
    , ToolTipText = "Colore orario di lavoro";
    , HelpContextID = 153487586


  add object oObj_6_38 as cp_setobjprop with uid="DYWZQXRSMH",left=802, top=351, width=184,height=24,;
    caption='Colore orario di chiusura',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORNNP",;
    nPag=6;
    , ToolTipText = "Colore orario di chiusura";
    , HelpContextID = 137192608


  add object oObj_6_39 as cp_setobjprop with uid="DACMHQMRHR",left=802, top=375, width=184,height=24,;
    caption='Colore intestazioni',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORHDR",;
    nPag=6;
    , ToolTipText = "Colore intestazioni";
    , HelpContextID = 248365256


  add object oObj_6_40 as cp_setobjprop with uid="GKNZHTBAMG",left=802, top=399, width=184,height=24,;
    caption='Colore selezione',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORSEL",;
    nPag=6;
    , ToolTipText = "Colore selezione";
    , HelpContextID = 203273038


  add object oCIORAINI_6_44 as StdCombo with uid="XHMBUVFSSS",value=1,rtseq=87,rtrep=.f.,left=395,top=190,width=62,height=21;
    , ToolTipText = "Ora inizio giornata lavorativa";
    , HelpContextID = 153224593;
    , cFormVar="w_CIORAINI",RowSource=""+"00:00,"+"00:30,"+"01:00,"+"01:30,"+"02:00,"+"02:30,"+"03:00,"+"03:30,"+"04:00,"+"04:30,"+"05:00,"+"05:30,"+"06:00,"+"06:30,"+"07:00,"+"07:30,"+"08:00,"+"08:30,"+"09:00,"+"09:30,"+"10:00,"+"10:30,"+"11:00,"+"11:30,"+"12:00,"+"12:30,"+"13:00,"+"13:30,"+"14:00,"+"14:30,"+"15:00,"+"15:30,"+"16:00,"+"16:30,"+"17:00,"+"17:30,"+"18:00,"+"18:30,"+"19:00,"+"19:30,"+"20:00,"+"20:30,"+"21:00,"+"21:30,"+"22:00,"+"22:30,"+"23:00,"+"23:30", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oCIORAINI_6_44.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,0.5,;
    iif(this.value =3,1,;
    iif(this.value =4,1.5,;
    iif(this.value =5,2,;
    iif(this.value =6,2.5,;
    iif(this.value =7,3,;
    iif(this.value =8,3.5,;
    iif(this.value =9,4,;
    iif(this.value =10,4.5,;
    iif(this.value =11,5,;
    iif(this.value =12,5.5,;
    iif(this.value =13,6,;
    iif(this.value =14,6.5,;
    iif(this.value =15,7,;
    iif(this.value =16,7.5,;
    iif(this.value =17,8,;
    iif(this.value =18,8.5,;
    iif(this.value =19,9,;
    iif(this.value =20,9.5,;
    iif(this.value =21,10,;
    iif(this.value =22,10.5,;
    iif(this.value =23,11,;
    iif(this.value =24,11.5,;
    iif(this.value =25,12,;
    iif(this.value =26,12.5,;
    iif(this.value =27,13,;
    iif(this.value =28,13.5,;
    iif(this.value =29,14,;
    iif(this.value =30,14.5,;
    iif(this.value =31,15,;
    iif(this.value =32,15.5,;
    iif(this.value =33,16,;
    iif(this.value =34,16.5,;
    iif(this.value =35,17,;
    iif(this.value =36,17.5,;
    iif(this.value =37,18,;
    iif(this.value =38,18.5,;
    iif(this.value =39,19,;
    iif(this.value =40,19.5,;
    iif(this.value =41,20,;
    iif(this.value =42,20.5,;
    iif(this.value =43,21,;
    iif(this.value =44,21.5,;
    iif(this.value =45,22,;
    iif(this.value =46,22.5,;
    iif(this.value =47,23,;
    iif(this.value =48,23.5,;
    9)))))))))))))))))))))))))))))))))))))))))))))))))
  endfunc
  func oCIORAINI_6_44.GetRadio()
    this.Parent.oContained.w_CIORAINI = this.RadioValue()
    return .t.
  endfunc

  func oCIORAINI_6_44.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CIORAINI==0,1,;
      iif(this.Parent.oContained.w_CIORAINI==0.5,2,;
      iif(this.Parent.oContained.w_CIORAINI==1,3,;
      iif(this.Parent.oContained.w_CIORAINI==1.5,4,;
      iif(this.Parent.oContained.w_CIORAINI==2,5,;
      iif(this.Parent.oContained.w_CIORAINI==2.5,6,;
      iif(this.Parent.oContained.w_CIORAINI==3,7,;
      iif(this.Parent.oContained.w_CIORAINI==3.5,8,;
      iif(this.Parent.oContained.w_CIORAINI==4,9,;
      iif(this.Parent.oContained.w_CIORAINI==4.5,10,;
      iif(this.Parent.oContained.w_CIORAINI==5,11,;
      iif(this.Parent.oContained.w_CIORAINI==5.5,12,;
      iif(this.Parent.oContained.w_CIORAINI==6,13,;
      iif(this.Parent.oContained.w_CIORAINI==6.5,14,;
      iif(this.Parent.oContained.w_CIORAINI==7,15,;
      iif(this.Parent.oContained.w_CIORAINI==7.5,16,;
      iif(this.Parent.oContained.w_CIORAINI==8,17,;
      iif(this.Parent.oContained.w_CIORAINI==8.5,18,;
      iif(this.Parent.oContained.w_CIORAINI==9,19,;
      iif(this.Parent.oContained.w_CIORAINI==9.5,20,;
      iif(this.Parent.oContained.w_CIORAINI==10,21,;
      iif(this.Parent.oContained.w_CIORAINI==10.5,22,;
      iif(this.Parent.oContained.w_CIORAINI==11,23,;
      iif(this.Parent.oContained.w_CIORAINI==11.5,24,;
      iif(this.Parent.oContained.w_CIORAINI==12,25,;
      iif(this.Parent.oContained.w_CIORAINI==12.5,26,;
      iif(this.Parent.oContained.w_CIORAINI==13,27,;
      iif(this.Parent.oContained.w_CIORAINI==13.5,28,;
      iif(this.Parent.oContained.w_CIORAINI==14,29,;
      iif(this.Parent.oContained.w_CIORAINI==14.5,30,;
      iif(this.Parent.oContained.w_CIORAINI==15,31,;
      iif(this.Parent.oContained.w_CIORAINI==15.5,32,;
      iif(this.Parent.oContained.w_CIORAINI==16,33,;
      iif(this.Parent.oContained.w_CIORAINI==16.5,34,;
      iif(this.Parent.oContained.w_CIORAINI==17,35,;
      iif(this.Parent.oContained.w_CIORAINI==17.5,36,;
      iif(this.Parent.oContained.w_CIORAINI==18,37,;
      iif(this.Parent.oContained.w_CIORAINI==18.5,38,;
      iif(this.Parent.oContained.w_CIORAINI==19,39,;
      iif(this.Parent.oContained.w_CIORAINI==19.5,40,;
      iif(this.Parent.oContained.w_CIORAINI==20,41,;
      iif(this.Parent.oContained.w_CIORAINI==20.5,42,;
      iif(this.Parent.oContained.w_CIORAINI==21,43,;
      iif(this.Parent.oContained.w_CIORAINI==21.5,44,;
      iif(this.Parent.oContained.w_CIORAINI==22,45,;
      iif(this.Parent.oContained.w_CIORAINI==22.5,46,;
      iif(this.Parent.oContained.w_CIORAINI==23,47,;
      iif(this.Parent.oContained.w_CIORAINI==23.5,48,;
      0))))))))))))))))))))))))))))))))))))))))))))))))
  endfunc


  add object oCIORAFIN_6_45 as StdCombo with uid="WAHTKYIDYV",value=1,rtseq=88,rtrep=.f.,left=395,top=214,width=62,height=21;
    , ToolTipText = "Ora fine giornata lavorativa";
    , HelpContextID = 203556236;
    , cFormVar="w_CIORAFIN",RowSource=""+"00:00,"+"00:30,"+"01:00,"+"01:30,"+"02:00,"+"02:30,"+"03:00,"+"03:30,"+"04:00,"+"04:30,"+"05:00,"+"05:30,"+"06:00,"+"06:30,"+"07:00,"+"07:30,"+"08:00,"+"08:30,"+"09:00,"+"09:30,"+"10:00,"+"10:30,"+"11:00,"+"11:30,"+"12:00,"+"12:30,"+"13:00,"+"13:30,"+"14:00,"+"14:30,"+"15:00,"+"15:30,"+"16:00,"+"16:30,"+"17:00,"+"17:30,"+"18:00,"+"18:30,"+"19:00,"+"19:30,"+"20:00,"+"20:30,"+"21:00,"+"21:30,"+"22:00,"+"22:30,"+"23:00,"+"23:30", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oCIORAFIN_6_45.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,0.5,;
    iif(this.value =3,1,;
    iif(this.value =4,1.5,;
    iif(this.value =5,2,;
    iif(this.value =6,2.5,;
    iif(this.value =7,3,;
    iif(this.value =8,3.5,;
    iif(this.value =9,4,;
    iif(this.value =10,4.5,;
    iif(this.value =11,5,;
    iif(this.value =12,5.5,;
    iif(this.value =13,6,;
    iif(this.value =14,6.5,;
    iif(this.value =15,7,;
    iif(this.value =16,7.5,;
    iif(this.value =17,8,;
    iif(this.value =18,8.5,;
    iif(this.value =19,9,;
    iif(this.value =20,9.5,;
    iif(this.value =21,10,;
    iif(this.value =22,10.5,;
    iif(this.value =23,11,;
    iif(this.value =24,11.5,;
    iif(this.value =25,12,;
    iif(this.value =26,12.5,;
    iif(this.value =27,13,;
    iif(this.value =28,13.5,;
    iif(this.value =29,14,;
    iif(this.value =30,14.5,;
    iif(this.value =31,15,;
    iif(this.value =32,15.5,;
    iif(this.value =33,16,;
    iif(this.value =34,16.5,;
    iif(this.value =35,17,;
    iif(this.value =36,17.5,;
    iif(this.value =37,18,;
    iif(this.value =38,18.5,;
    iif(this.value =39,19,;
    iif(this.value =40,19.5,;
    iif(this.value =41,20,;
    iif(this.value =42,20.5,;
    iif(this.value =43,21,;
    iif(this.value =44,21.5,;
    iif(this.value =45,22,;
    iif(this.value =46,22.5,;
    iif(this.value =47,23,;
    iif(this.value =48,23.5,;
    18)))))))))))))))))))))))))))))))))))))))))))))))))
  endfunc
  func oCIORAFIN_6_45.GetRadio()
    this.Parent.oContained.w_CIORAFIN = this.RadioValue()
    return .t.
  endfunc

  func oCIORAFIN_6_45.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CIORAFIN==0,1,;
      iif(this.Parent.oContained.w_CIORAFIN==0.5,2,;
      iif(this.Parent.oContained.w_CIORAFIN==1,3,;
      iif(this.Parent.oContained.w_CIORAFIN==1.5,4,;
      iif(this.Parent.oContained.w_CIORAFIN==2,5,;
      iif(this.Parent.oContained.w_CIORAFIN==2.5,6,;
      iif(this.Parent.oContained.w_CIORAFIN==3,7,;
      iif(this.Parent.oContained.w_CIORAFIN==3.5,8,;
      iif(this.Parent.oContained.w_CIORAFIN==4,9,;
      iif(this.Parent.oContained.w_CIORAFIN==4.5,10,;
      iif(this.Parent.oContained.w_CIORAFIN==5,11,;
      iif(this.Parent.oContained.w_CIORAFIN==5.5,12,;
      iif(this.Parent.oContained.w_CIORAFIN==6,13,;
      iif(this.Parent.oContained.w_CIORAFIN==6.5,14,;
      iif(this.Parent.oContained.w_CIORAFIN==7,15,;
      iif(this.Parent.oContained.w_CIORAFIN==7.5,16,;
      iif(this.Parent.oContained.w_CIORAFIN==8,17,;
      iif(this.Parent.oContained.w_CIORAFIN==8.5,18,;
      iif(this.Parent.oContained.w_CIORAFIN==9,19,;
      iif(this.Parent.oContained.w_CIORAFIN==9.5,20,;
      iif(this.Parent.oContained.w_CIORAFIN==10,21,;
      iif(this.Parent.oContained.w_CIORAFIN==10.5,22,;
      iif(this.Parent.oContained.w_CIORAFIN==11,23,;
      iif(this.Parent.oContained.w_CIORAFIN==11.5,24,;
      iif(this.Parent.oContained.w_CIORAFIN==12,25,;
      iif(this.Parent.oContained.w_CIORAFIN==12.5,26,;
      iif(this.Parent.oContained.w_CIORAFIN==13,27,;
      iif(this.Parent.oContained.w_CIORAFIN==13.5,28,;
      iif(this.Parent.oContained.w_CIORAFIN==14,29,;
      iif(this.Parent.oContained.w_CIORAFIN==14.5,30,;
      iif(this.Parent.oContained.w_CIORAFIN==15,31,;
      iif(this.Parent.oContained.w_CIORAFIN==15.5,32,;
      iif(this.Parent.oContained.w_CIORAFIN==16,33,;
      iif(this.Parent.oContained.w_CIORAFIN==16.5,34,;
      iif(this.Parent.oContained.w_CIORAFIN==17,35,;
      iif(this.Parent.oContained.w_CIORAFIN==17.5,36,;
      iif(this.Parent.oContained.w_CIORAFIN==18,37,;
      iif(this.Parent.oContained.w_CIORAFIN==18.5,38,;
      iif(this.Parent.oContained.w_CIORAFIN==19,39,;
      iif(this.Parent.oContained.w_CIORAFIN==19.5,40,;
      iif(this.Parent.oContained.w_CIORAFIN==20,41,;
      iif(this.Parent.oContained.w_CIORAFIN==20.5,42,;
      iif(this.Parent.oContained.w_CIORAFIN==21,43,;
      iif(this.Parent.oContained.w_CIORAFIN==21.5,44,;
      iif(this.Parent.oContained.w_CIORAFIN==22,45,;
      iif(this.Parent.oContained.w_CIORAFIN==22.5,46,;
      iif(this.Parent.oContained.w_CIORAFIN==23,47,;
      iif(this.Parent.oContained.w_CIORAFIN==23.5,48,;
      0))))))))))))))))))))))))))))))))))))))))))))))))
  endfunc


  add object oCISTAINI_6_46 as StdCombo with uid="MFONWMBQKG",value=1,rtseq=89,rtrep=.f.,left=395,top=238,width=62,height=21;
    , ToolTipText = "Ora inizio giornata lavorativa in stampa";
    , HelpContextID = 153077137;
    , cFormVar="w_CISTAINI",RowSource=""+"00:00,"+"00:30,"+"01:00,"+"01:30,"+"02:00,"+"02:30,"+"03:00,"+"03:30,"+"04:00,"+"04:30,"+"05:00,"+"05:30,"+"06:00,"+"06:30,"+"07:00,"+"07:30,"+"08:00,"+"08:30,"+"09:00,"+"09:30,"+"10:00,"+"10:30,"+"11:00,"+"11:30,"+"12:00,"+"12:30,"+"13:00,"+"13:30,"+"14:00,"+"14:30,"+"15:00,"+"15:30,"+"16:00,"+"16:30,"+"17:00,"+"17:30,"+"18:00,"+"18:30,"+"19:00,"+"19:30,"+"20:00,"+"20:30,"+"21:00,"+"21:30,"+"22:00,"+"22:30,"+"23:00,"+"23:30", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oCISTAINI_6_46.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,0.5,;
    iif(this.value =3,1,;
    iif(this.value =4,1.5,;
    iif(this.value =5,2,;
    iif(this.value =6,2.5,;
    iif(this.value =7,3,;
    iif(this.value =8,3.5,;
    iif(this.value =9,4,;
    iif(this.value =10,4.5,;
    iif(this.value =11,5,;
    iif(this.value =12,5.5,;
    iif(this.value =13,6,;
    iif(this.value =14,6.5,;
    iif(this.value =15,7,;
    iif(this.value =16,7.5,;
    iif(this.value =17,8,;
    iif(this.value =18,8.5,;
    iif(this.value =19,9,;
    iif(this.value =20,9.5,;
    iif(this.value =21,10,;
    iif(this.value =22,10.5,;
    iif(this.value =23,11,;
    iif(this.value =24,11.5,;
    iif(this.value =25,12,;
    iif(this.value =26,12.5,;
    iif(this.value =27,13,;
    iif(this.value =28,13.5,;
    iif(this.value =29,14,;
    iif(this.value =30,14.5,;
    iif(this.value =31,15,;
    iif(this.value =32,15.5,;
    iif(this.value =33,16,;
    iif(this.value =34,16.5,;
    iif(this.value =35,17,;
    iif(this.value =36,17.5,;
    iif(this.value =37,18,;
    iif(this.value =38,18.5,;
    iif(this.value =39,19,;
    iif(this.value =40,19.5,;
    iif(this.value =41,20,;
    iif(this.value =42,20.5,;
    iif(this.value =43,21,;
    iif(this.value =44,21.5,;
    iif(this.value =45,22,;
    iif(this.value =46,22.5,;
    iif(this.value =47,23,;
    iif(this.value =48,23.5,;
    9)))))))))))))))))))))))))))))))))))))))))))))))))
  endfunc
  func oCISTAINI_6_46.GetRadio()
    this.Parent.oContained.w_CISTAINI = this.RadioValue()
    return .t.
  endfunc

  func oCISTAINI_6_46.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CISTAINI==0,1,;
      iif(this.Parent.oContained.w_CISTAINI==0.5,2,;
      iif(this.Parent.oContained.w_CISTAINI==1,3,;
      iif(this.Parent.oContained.w_CISTAINI==1.5,4,;
      iif(this.Parent.oContained.w_CISTAINI==2,5,;
      iif(this.Parent.oContained.w_CISTAINI==2.5,6,;
      iif(this.Parent.oContained.w_CISTAINI==3,7,;
      iif(this.Parent.oContained.w_CISTAINI==3.5,8,;
      iif(this.Parent.oContained.w_CISTAINI==4,9,;
      iif(this.Parent.oContained.w_CISTAINI==4.5,10,;
      iif(this.Parent.oContained.w_CISTAINI==5,11,;
      iif(this.Parent.oContained.w_CISTAINI==5.5,12,;
      iif(this.Parent.oContained.w_CISTAINI==6,13,;
      iif(this.Parent.oContained.w_CISTAINI==6.5,14,;
      iif(this.Parent.oContained.w_CISTAINI==7,15,;
      iif(this.Parent.oContained.w_CISTAINI==7.5,16,;
      iif(this.Parent.oContained.w_CISTAINI==8,17,;
      iif(this.Parent.oContained.w_CISTAINI==8.5,18,;
      iif(this.Parent.oContained.w_CISTAINI==9,19,;
      iif(this.Parent.oContained.w_CISTAINI==9.5,20,;
      iif(this.Parent.oContained.w_CISTAINI==10,21,;
      iif(this.Parent.oContained.w_CISTAINI==10.5,22,;
      iif(this.Parent.oContained.w_CISTAINI==11,23,;
      iif(this.Parent.oContained.w_CISTAINI==11.5,24,;
      iif(this.Parent.oContained.w_CISTAINI==12,25,;
      iif(this.Parent.oContained.w_CISTAINI==12.5,26,;
      iif(this.Parent.oContained.w_CISTAINI==13,27,;
      iif(this.Parent.oContained.w_CISTAINI==13.5,28,;
      iif(this.Parent.oContained.w_CISTAINI==14,29,;
      iif(this.Parent.oContained.w_CISTAINI==14.5,30,;
      iif(this.Parent.oContained.w_CISTAINI==15,31,;
      iif(this.Parent.oContained.w_CISTAINI==15.5,32,;
      iif(this.Parent.oContained.w_CISTAINI==16,33,;
      iif(this.Parent.oContained.w_CISTAINI==16.5,34,;
      iif(this.Parent.oContained.w_CISTAINI==17,35,;
      iif(this.Parent.oContained.w_CISTAINI==17.5,36,;
      iif(this.Parent.oContained.w_CISTAINI==18,37,;
      iif(this.Parent.oContained.w_CISTAINI==18.5,38,;
      iif(this.Parent.oContained.w_CISTAINI==19,39,;
      iif(this.Parent.oContained.w_CISTAINI==19.5,40,;
      iif(this.Parent.oContained.w_CISTAINI==20,41,;
      iif(this.Parent.oContained.w_CISTAINI==20.5,42,;
      iif(this.Parent.oContained.w_CISTAINI==21,43,;
      iif(this.Parent.oContained.w_CISTAINI==21.5,44,;
      iif(this.Parent.oContained.w_CISTAINI==22,45,;
      iif(this.Parent.oContained.w_CISTAINI==22.5,46,;
      iif(this.Parent.oContained.w_CISTAINI==23,47,;
      iif(this.Parent.oContained.w_CISTAINI==23.5,48,;
      0))))))))))))))))))))))))))))))))))))))))))))))))
  endfunc


  add object oCISTAFIN_6_47 as StdCombo with uid="WKWCJNHIAI",value=1,rtseq=90,rtrep=.f.,left=395,top=262,width=62,height=21;
    , ToolTipText = "Ora fine giornata lavorativa in stampa";
    , HelpContextID = 203408780;
    , cFormVar="w_CISTAFIN",RowSource=""+"00:00,"+"00:30,"+"01:00,"+"01:30,"+"02:00,"+"02:30,"+"03:00,"+"03:30,"+"04:00,"+"04:30,"+"05:00,"+"05:30,"+"06:00,"+"06:30,"+"07:00,"+"07:30,"+"08:00,"+"08:30,"+"09:00,"+"09:30,"+"10:00,"+"10:30,"+"11:00,"+"11:30,"+"12:00,"+"12:30,"+"13:00,"+"13:30,"+"14:00,"+"14:30,"+"15:00,"+"15:30,"+"16:00,"+"16:30,"+"17:00,"+"17:30,"+"18:00,"+"18:30,"+"19:00,"+"19:30,"+"20:00,"+"20:30,"+"21:00,"+"21:30,"+"22:00,"+"22:30,"+"23:00,"+"23:30", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oCISTAFIN_6_47.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,0.5,;
    iif(this.value =3,1,;
    iif(this.value =4,1.5,;
    iif(this.value =5,2,;
    iif(this.value =6,2.5,;
    iif(this.value =7,3,;
    iif(this.value =8,3.5,;
    iif(this.value =9,4,;
    iif(this.value =10,4.5,;
    iif(this.value =11,5,;
    iif(this.value =12,5.5,;
    iif(this.value =13,6,;
    iif(this.value =14,6.5,;
    iif(this.value =15,7,;
    iif(this.value =16,7.5,;
    iif(this.value =17,8,;
    iif(this.value =18,8.5,;
    iif(this.value =19,9,;
    iif(this.value =20,9.5,;
    iif(this.value =21,10,;
    iif(this.value =22,10.5,;
    iif(this.value =23,11,;
    iif(this.value =24,11.5,;
    iif(this.value =25,12,;
    iif(this.value =26,12.5,;
    iif(this.value =27,13,;
    iif(this.value =28,13.5,;
    iif(this.value =29,14,;
    iif(this.value =30,14.5,;
    iif(this.value =31,15,;
    iif(this.value =32,15.5,;
    iif(this.value =33,16,;
    iif(this.value =34,16.5,;
    iif(this.value =35,17,;
    iif(this.value =36,17.5,;
    iif(this.value =37,18,;
    iif(this.value =38,18.5,;
    iif(this.value =39,19,;
    iif(this.value =40,19.5,;
    iif(this.value =41,20,;
    iif(this.value =42,20.5,;
    iif(this.value =43,21,;
    iif(this.value =44,21.5,;
    iif(this.value =45,22,;
    iif(this.value =46,22.5,;
    iif(this.value =47,23,;
    iif(this.value =48,23.5,;
    18)))))))))))))))))))))))))))))))))))))))))))))))))
  endfunc
  func oCISTAFIN_6_47.GetRadio()
    this.Parent.oContained.w_CISTAFIN = this.RadioValue()
    return .t.
  endfunc

  func oCISTAFIN_6_47.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CISTAFIN==0,1,;
      iif(this.Parent.oContained.w_CISTAFIN==0.5,2,;
      iif(this.Parent.oContained.w_CISTAFIN==1,3,;
      iif(this.Parent.oContained.w_CISTAFIN==1.5,4,;
      iif(this.Parent.oContained.w_CISTAFIN==2,5,;
      iif(this.Parent.oContained.w_CISTAFIN==2.5,6,;
      iif(this.Parent.oContained.w_CISTAFIN==3,7,;
      iif(this.Parent.oContained.w_CISTAFIN==3.5,8,;
      iif(this.Parent.oContained.w_CISTAFIN==4,9,;
      iif(this.Parent.oContained.w_CISTAFIN==4.5,10,;
      iif(this.Parent.oContained.w_CISTAFIN==5,11,;
      iif(this.Parent.oContained.w_CISTAFIN==5.5,12,;
      iif(this.Parent.oContained.w_CISTAFIN==6,13,;
      iif(this.Parent.oContained.w_CISTAFIN==6.5,14,;
      iif(this.Parent.oContained.w_CISTAFIN==7,15,;
      iif(this.Parent.oContained.w_CISTAFIN==7.5,16,;
      iif(this.Parent.oContained.w_CISTAFIN==8,17,;
      iif(this.Parent.oContained.w_CISTAFIN==8.5,18,;
      iif(this.Parent.oContained.w_CISTAFIN==9,19,;
      iif(this.Parent.oContained.w_CISTAFIN==9.5,20,;
      iif(this.Parent.oContained.w_CISTAFIN==10,21,;
      iif(this.Parent.oContained.w_CISTAFIN==10.5,22,;
      iif(this.Parent.oContained.w_CISTAFIN==11,23,;
      iif(this.Parent.oContained.w_CISTAFIN==11.5,24,;
      iif(this.Parent.oContained.w_CISTAFIN==12,25,;
      iif(this.Parent.oContained.w_CISTAFIN==12.5,26,;
      iif(this.Parent.oContained.w_CISTAFIN==13,27,;
      iif(this.Parent.oContained.w_CISTAFIN==13.5,28,;
      iif(this.Parent.oContained.w_CISTAFIN==14,29,;
      iif(this.Parent.oContained.w_CISTAFIN==14.5,30,;
      iif(this.Parent.oContained.w_CISTAFIN==15,31,;
      iif(this.Parent.oContained.w_CISTAFIN==15.5,32,;
      iif(this.Parent.oContained.w_CISTAFIN==16,33,;
      iif(this.Parent.oContained.w_CISTAFIN==16.5,34,;
      iif(this.Parent.oContained.w_CISTAFIN==17,35,;
      iif(this.Parent.oContained.w_CISTAFIN==17.5,36,;
      iif(this.Parent.oContained.w_CISTAFIN==18,37,;
      iif(this.Parent.oContained.w_CISTAFIN==18.5,38,;
      iif(this.Parent.oContained.w_CISTAFIN==19,39,;
      iif(this.Parent.oContained.w_CISTAFIN==19.5,40,;
      iif(this.Parent.oContained.w_CISTAFIN==20,41,;
      iif(this.Parent.oContained.w_CISTAFIN==20.5,42,;
      iif(this.Parent.oContained.w_CISTAFIN==21,43,;
      iif(this.Parent.oContained.w_CISTAFIN==21.5,44,;
      iif(this.Parent.oContained.w_CISTAFIN==22,45,;
      iif(this.Parent.oContained.w_CISTAFIN==22.5,46,;
      iif(this.Parent.oContained.w_CISTAFIN==23,47,;
      iif(this.Parent.oContained.w_CISTAFIN==23.5,48,;
      0))))))))))))))))))))))))))))))))))))))))))))))))
  endfunc


  add object oCITYPBAL_6_48 as StdCombo with uid="EAHSMHYEMF",rtseq=91,rtrep=.f.,left=395,top=292,width=110,height=19;
    , ToolTipText = "Tipo visualizzazione warning (Standard/Balloon)";
    , HelpContextID = 13978226;
    , cFormVar="w_CITYPBAL",RowSource=""+"Standard,"+"Balloon", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oCITYPBAL_6_48.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'B',;
    space(1))))
  endfunc
  func oCITYPBAL_6_48.GetRadio()
    this.Parent.oContained.w_CITYPBAL = this.RadioValue()
    return .t.
  endfunc

  func oCITYPBAL_6_48.SetRadio()
    this.Parent.oContained.w_CITYPBAL=trim(this.Parent.oContained.w_CITYPBAL)
    this.value = ;
      iif(this.Parent.oContained.w_CITYPBAL=='S',1,;
      iif(this.Parent.oContained.w_CITYPBAL=='B',2,;
      0))
  endfunc

  add object oCOLORACF_6_53 as StdField with uid="BFBCKEOLPU",rtseq=92,rtrep=.f.,;
    cFormVar = "w_COLORACF", cQueryName = "COLORACF",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore da utilizzare per le attivit� in attesa conferma",;
    HelpContextID = 267047020,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=172, Top=244, InputMask=replicate('X',10)


  add object oObj_6_56 as cp_setobjprop with uid="KWSVCQEYAV",left=802, top=292, width=184,height=24,;
    caption='Colore attivit� attesa conferma',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORACF",;
    nPag=6;
    , ToolTipText = "Colore attivit� attesa conferma";
    , HelpContextID = 29193398

  add object oCIFLCOMP_6_59 as StdRadio with uid="LAAZNVIWGO",rtseq=104,rtrep=.f.,left=20, top=329, width=259,height=23;
    , ToolTipText = "Completamento o data scadenza in visualizzazione delle cose da fare";
    , cFormVar="w_CIFLCOMP", ButtonCount=2, bObbl=.f., nPag=6;
  , bGlobalFont=.t.

    proc oCIFLCOMP_6_59.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="% completamento"
      this.Buttons(1).HelpContextID = 50894218
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("% completamento","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Data scadenza"
      this.Buttons(2).HelpContextID = 50894218
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Data scadenza","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Completamento o data scadenza in visualizzazione delle cose da fare")
      StdRadio::init()
    endproc

  func oCIFLCOMP_6_59.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oCIFLCOMP_6_59.GetRadio()
    this.Parent.oContained.w_CIFLCOMP = this.RadioValue()
    return .t.
  endfunc

  func oCIFLCOMP_6_59.SetRadio()
    this.Parent.oContained.w_CIFLCOMP=trim(this.Parent.oContained.w_CIFLCOMP)
    this.value = ;
      iif(this.Parent.oContained.w_CIFLCOMP=='S',1,;
      iif(this.Parent.oContained.w_CIFLCOMP=='N',2,;
      0))
  endfunc


  add object oCIAGSFNM_6_60 as StdTableComboFont with uid="GWDVJHOFUY",rtseq=137,rtrep=.f.,left=172,top=382,width=164,height=21;
    , ToolTipText = "Font da utilizzare per i giorni del calendario senza impegni";
    , HelpContextID = 185460109;
    , cFormVar="w_CIAGSFNM",tablefilter="", bObbl = .f. , nPag = 6;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  add object oCIAGSFSZ_6_62 as StdField with uid="RKFYZYIQLX",rtseq=138,rtrep=.f.,;
    cFormVar = "w_CIAGSFSZ", cQueryName = "CIAGSFSZ",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione font per i giorni del calendario senza impegni",;
    HelpContextID = 82975360,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=450, Top=382, cSayPict='"999"', cGetPict='"999"'

  add object oCIAGSFIT_6_64 as StdCheck with uid="XSIMSBGWES",rtseq=139,rtrep=.f.,left=519, top=382, caption="Italic",;
    HelpContextID = 185460102,;
    cFormVar="w_CIAGSFIT", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oCIAGSFIT_6_64.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIAGSFIT_6_64.GetRadio()
    this.Parent.oContained.w_CIAGSFIT = this.RadioValue()
    return .t.
  endfunc

  func oCIAGSFIT_6_64.SetRadio()
    this.Parent.oContained.w_CIAGSFIT=trim(this.Parent.oContained.w_CIAGSFIT)
    this.value = ;
      iif(this.Parent.oContained.w_CIAGSFIT=='S',1,;
      0)
  endfunc

  add object oCIAGSFBO_6_65 as StdCheck with uid="WASFGKMQFK",rtseq=140,rtrep=.f.,left=602, top=382, caption="Bold",;
    HelpContextID = 82975349,;
    cFormVar="w_CIAGSFBO", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oCIAGSFBO_6_65.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIAGSFBO_6_65.GetRadio()
    this.Parent.oContained.w_CIAGSFBO = this.RadioValue()
    return .t.
  endfunc

  func oCIAGSFBO_6_65.SetRadio()
    this.Parent.oContained.w_CIAGSFBO=trim(this.Parent.oContained.w_CIAGSFBO)
    this.value = ;
      iif(this.Parent.oContained.w_CIAGSFBO=='S',1,;
      0)
  endfunc


  add object oCIAGIFNM_6_68 as StdTableComboFont with uid="UDIGVBZLMK",rtseq=141,rtrep=.f.,left=172,top=410,width=164,height=21;
    , ToolTipText = "Font da utilizzare per i giorni del calendario senza impegni";
    , HelpContextID = 195945869;
    , cFormVar="w_CIAGIFNM",tablefilter="", bObbl = .f. , nPag = 6;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  add object oCIAGIFSZ_6_70 as StdField with uid="DWMIZZSKDB",rtseq=142,rtrep=.f.,;
    cFormVar = "w_CIAGIFSZ", cQueryName = "CIAGIFSZ",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione font per i giorni del calendario senza impegni",;
    HelpContextID = 72489600,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=450, Top=410, cSayPict='"999"', cGetPict='"999"'

  add object oCIAGIFIT_6_72 as StdCheck with uid="FMACSBBWJP",rtseq=143,rtrep=.f.,left=519, top=410, caption="Italic",;
    HelpContextID = 195945862,;
    cFormVar="w_CIAGIFIT", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oCIAGIFIT_6_72.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIAGIFIT_6_72.GetRadio()
    this.Parent.oContained.w_CIAGIFIT = this.RadioValue()
    return .t.
  endfunc

  func oCIAGIFIT_6_72.SetRadio()
    this.Parent.oContained.w_CIAGIFIT=trim(this.Parent.oContained.w_CIAGIFIT)
    this.value = ;
      iif(this.Parent.oContained.w_CIAGIFIT=='S',1,;
      0)
  endfunc

  add object oCIAGIFBO_6_73 as StdCheck with uid="WTDKATBBSE",rtseq=144,rtrep=.f.,left=602, top=410, caption="Bold",;
    HelpContextID = 72489589,;
    cFormVar="w_CIAGIFBO", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oCIAGIFBO_6_73.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIAGIFBO_6_73.GetRadio()
    this.Parent.oContained.w_CIAGIFBO = this.RadioValue()
    return .t.
  endfunc

  func oCIAGIFBO_6_73.SetRadio()
    this.Parent.oContained.w_CIAGIFBO=trim(this.Parent.oContained.w_CIAGIFBO)
    this.value = ;
      iif(this.Parent.oContained.w_CIAGIFBO=='S',1,;
      0)
  endfunc

  add object oCIGIORHO_6_74 as StdField with uid="HFUCQKOHXP",rtseq=145,rtrep=.f.,;
    cFormVar = "w_CIGIORHO", cQueryName = "CIGIORHO",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Unit� incremento/diminuzione altezza righe calendari agenda",;
    HelpContextID = 256607627,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=450, Top=436, cSayPict='"999"', cGetPict='"999"'

  add object oStr_6_17 as StdString with uid="JLJFOBYYVF",Visible=.t., Left=43, Top=175,;
    Alignment=1, Width=121, Height=18,;
    Caption="Urgente:"  ;
  , bGlobalFont=.t.

  add object oStr_6_20 as StdString with uid="UOQWESOSSD",Visible=.t., Left=43, Top=200,;
    Alignment=1, Width=121, Height=18,;
    Caption="In scadenza termine:"  ;
  , bGlobalFont=.t.

  add object oStr_6_22 as StdString with uid="QGVBETDCJF",Visible=.t., Left=1, Top=225,;
    Alignment=1, Width=163, Height=18,;
    Caption="Evasa o completata:"  ;
  , bGlobalFont=.t.

  add object oStr_6_26 as StdString with uid="QKGDGIZSEF",Visible=.t., Left=293, Top=37,;
    Alignment=0, Width=236, Height=18,;
    Caption="Visualizzazione iniziale agenda"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_28 as StdString with uid="NKZTAFCJJQ",Visible=.t., Left=43, Top=61,;
    Alignment=1, Width=121, Height=18,;
    Caption="Sfondo:"  ;
  , bGlobalFont=.t.

  add object oStr_6_29 as StdString with uid="YNOHHIFVXQ",Visible=.t., Left=293, Top=167,;
    Alignment=0, Width=236, Height=18,;
    Caption="Orario lavorativo dell'agenda"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_30 as StdString with uid="RNYHKFULLF",Visible=.t., Left=43, Top=85,;
    Alignment=1, Width=121, Height=18,;
    Caption="Bordo:"  ;
  , bGlobalFont=.t.

  add object oStr_6_31 as StdString with uid="SKPFGKOPBL",Visible=.t., Left=43, Top=109,;
    Alignment=1, Width=121, Height=18,;
    Caption="Intestazione:"  ;
  , bGlobalFont=.t.

  add object oStr_6_32 as StdString with uid="BPIPKFWVMU",Visible=.t., Left=43, Top=133,;
    Alignment=1, Width=121, Height=18,;
    Caption="Selezione:"  ;
  , bGlobalFont=.t.

  add object oStr_6_41 as StdString with uid="LCVRMPMMKA",Visible=.t., Left=343, Top=192,;
    Alignment=1, Width=43, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_6_43 as StdString with uid="KKNNHNHOKD",Visible=.t., Left=20, Top=37,;
    Alignment=0, Width=236, Height=18,;
    Caption="Selezione colori"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_49 as StdString with uid="PHQRIJCRPQ",Visible=.t., Left=346, Top=216,;
    Alignment=1, Width=40, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oStr_6_50 as StdString with uid="MBVTQQGIMO",Visible=.t., Left=297, Top=240,;
    Alignment=1, Width=89, Height=18,;
    Caption="Inizio stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_6_51 as StdString with uid="CJYZLCWOOB",Visible=.t., Left=297, Top=264,;
    Alignment=1, Width=89, Height=18,;
    Caption="Fine stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_6_54 as StdString with uid="QTSITRBGDE",Visible=.t., Left=43, Top=250,;
    Alignment=1, Width=121, Height=18,;
    Caption="Provvisoria:"  ;
  , bGlobalFont=.t.

  add object oStr_6_58 as StdString with uid="UXTRSRCOBN",Visible=.t., Left=20, Top=305,;
    Alignment=0, Width=236, Height=18,;
    Caption="Visualizzazione in 'Cose da fare'"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_61 as StdString with uid="QVASNRFNJP",Visible=.t., Left=4, Top=382,;
    Alignment=1, Width=164, Height=18,;
    Caption="Giorni senza impegni:"  ;
  , bGlobalFont=.t.

  add object oStr_6_63 as StdString with uid="YBQIWBKRPL",Visible=.t., Left=342, Top=382,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_6_67 as StdString with uid="ZTLQOSDERJ",Visible=.t., Left=20, Top=356,;
    Alignment=0, Width=236, Height=18,;
    Caption="Font calendari agenda"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_69 as StdString with uid="XIIFFCKWLH",Visible=.t., Left=4, Top=410,;
    Alignment=1, Width=164, Height=18,;
    Caption="Giorni con impegni:"  ;
  , bGlobalFont=.t.

  add object oStr_6_71 as StdString with uid="GEFLNIILII",Visible=.t., Left=342, Top=410,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_6_75 as StdString with uid="UZIKRHTTIT",Visible=.t., Left=17, Top=440,;
    Alignment=1, Width=426, Height=18,;
    Caption="Unit� incremento/diminuzione altezza righe giorni agenda:"  ;
  , bGlobalFont=.t.

  add object oStr_6_76 as StdString with uid="MQPQRFXLEZ",Visible=.t., Left=287, Top=293,;
    Alignment=1, Width=99, Height=18,;
    Caption="Modalit� warning:"  ;
  , bGlobalFont=.t.

  add object oBox_6_27 as StdBox with uid="ITENCWZYBP",left=290, top=184, width=199,height=1

  add object oBox_6_42 as StdBox with uid="GHROKSGJSL",left=6, top=54, width=276,height=1

  add object oBox_6_52 as StdBox with uid="BXBLBFDLKX",left=290, top=54, width=199,height=1

  add object oBox_6_57 as StdBox with uid="AXKTEOWZEC",left=6, top=322, width=276,height=1

  add object oBox_6_66 as StdBox with uid="GWORPEOEZK",left=6, top=373, width=276,height=1
enddefine
define class tgsut_acuPag7 as StdContainer
  Width  = 709
  height = 511
  stdWidth  = 709
  stdheight = 511
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCIDSKMEN_7_1 as StdCombo with uid="SLYGQQBJHE",rtseq=94,rtrep=.f.,left=257,top=44,width=186,height=21;
    , ToolTipText = "Se attivo abilita il desktop men� (visualizzabile tramite ctrl+d)";
    , HelpContextID = 192825972;
    , cFormVar="w_CIDSKMEN",RowSource=""+"Abilita desktop men�,"+"Disabilita desktop men�,"+"Apri desktop men� all'ingresso", bObbl = .f. , nPag = 7;
  , bGlobalFont=.t.


  func oCIDSKMEN_7_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'H',;
    iif(this.value =3,'O',;
    space(1)))))
  endfunc
  func oCIDSKMEN_7_1.GetRadio()
    this.Parent.oContained.w_CIDSKMEN = this.RadioValue()
    return .t.
  endfunc

  func oCIDSKMEN_7_1.SetRadio()
    this.Parent.oContained.w_CIDSKMEN=trim(this.Parent.oContained.w_CIDSKMEN)
    this.value = ;
      iif(this.Parent.oContained.w_CIDSKMEN=='S',1,;
      iif(this.Parent.oContained.w_CIDSKMEN=='H',2,;
      iif(this.Parent.oContained.w_CIDSKMEN=='O',3,;
      0)))
  endfunc

  func oCIDSKMEN_7_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIVTHEME <> -1)
    endwith
   endif
  endfunc


  add object oCINAVSTA_7_2 as StdCombo with uid="QUKGWVIOIL",rtseq=95,rtrep=.f.,left=257,top=73,width=186,height=21;
    , ToolTipText = "Stato del Deskmen� all'avvio";
    , HelpContextID = 35449447;
    , cFormVar="w_CINAVSTA",RowSource=""+"Aperta,"+"Chiusa", bObbl = .f. , nPag = 7;
  , bGlobalFont=.t.


  func oCINAVSTA_7_2.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oCINAVSTA_7_2.GetRadio()
    this.Parent.oContained.w_CINAVSTA = this.RadioValue()
    return .t.
  endfunc

  func oCINAVSTA_7_2.SetRadio()
    this.Parent.oContained.w_CINAVSTA=trim(this.Parent.oContained.w_CINAVSTA)
    this.value = ;
      iif(this.Parent.oContained.w_CINAVSTA=='A',1,;
      iif(this.Parent.oContained.w_CINAVSTA=='C',2,;
      0))
  endfunc


  add object oCINAVNUB_7_3 as StdCombo with uid="AJAKYKZJWK",value=1,rtseq=96,rtrep=.f.,left=258,top=102,width=62,height=21;
    , ToolTipText = "Importa il numero massimo di pulsanti visualizzati dal desktop men�";
    , HelpContextID = 219998824;
    , cFormVar="w_CINAVNUB",RowSource=""+"0,"+"1,"+"2,"+"3,"+"4,"+"5,"+"6,"+"7,"+"8,"+"9,"+"10,"+"11,"+"12", bObbl = .f. , nPag = 7;
  , bGlobalFont=.t.


  func oCINAVNUB_7_3.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    iif(this.value =5,4,;
    iif(this.value =6,5,;
    iif(this.value =7,6,;
    iif(this.value =8,7,;
    iif(this.value =9,8,;
    iif(this.value =10,9,;
    iif(this.value =11,10,;
    iif(this.value =12,11,;
    iif(this.value =13,12,;
    0))))))))))))))
  endfunc
  func oCINAVNUB_7_3.GetRadio()
    this.Parent.oContained.w_CINAVNUB = this.RadioValue()
    return .t.
  endfunc

  func oCINAVNUB_7_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CINAVNUB==0,1,;
      iif(this.Parent.oContained.w_CINAVNUB==1,2,;
      iif(this.Parent.oContained.w_CINAVNUB==2,3,;
      iif(this.Parent.oContained.w_CINAVNUB==3,4,;
      iif(this.Parent.oContained.w_CINAVNUB==4,5,;
      iif(this.Parent.oContained.w_CINAVNUB==5,6,;
      iif(this.Parent.oContained.w_CINAVNUB==6,7,;
      iif(this.Parent.oContained.w_CINAVNUB==7,8,;
      iif(this.Parent.oContained.w_CINAVNUB==8,9,;
      iif(this.Parent.oContained.w_CINAVNUB==9,10,;
      iif(this.Parent.oContained.w_CINAVNUB==10,11,;
      iif(this.Parent.oContained.w_CINAVNUB==11,12,;
      iif(this.Parent.oContained.w_CINAVNUB==12,13,;
      0)))))))))))))
  endfunc

  add object oCINAVDIM_7_4 as StdField with uid="MUFZHXFLFE",rtseq=97,rtrep=.f.,;
    cFormVar = "w_CINAVDIM", cQueryName = "CINAVDIM",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione iniziale in pixel del desktop men�",;
    HelpContextID = 216208781,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=258, Top=132, cSayPict='"9999999999"', cGetPict='"9999999999"'


  add object oCIMNAFNM_7_5 as StdTableComboFont with uid="VDGVFJSWLZ",rtseq=98,rtrep=.f.,left=187,top=189,width=164,height=21;
    , ToolTipText = "Font da utilizzare per la sezione men� navigator";
    , HelpContextID = 203826573;
    , cFormVar="w_CIMNAFNM",tablefilter="", bObbl = .f. , nPag = 7;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  add object oCIMNAFSZ_7_6 as StdField with uid="BFGXYNJXAE",rtseq=99,rtrep=.f.,;
    cFormVar = "w_CIMNAFSZ", cQueryName = "CIMNAFSZ",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font per la sezione men� navigator",;
    HelpContextID = 64608896,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=463, Top=189, cSayPict='"999"', cGetPict='"999"'

  func oCIMNAFSZ_7_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CIMNAFNM) and TestFont('L', .w_CIMNAFNM, .w_CIMNAFSZ)<>-1 And !Empty(.w_CIMNAFSZ))
      if bRes and !(TestFont('L', .w_CIMNAFNM, .w_CIMNAFSZ) = 1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc


  add object oCIWMAFNM_7_7 as StdTableComboFont with uid="CEJELALOQK",rtseq=100,rtrep=.f.,left=187,top=223,width=164,height=21;
    , ToolTipText = "Font da utilizzare per la sezione Windows manager";
    , HelpContextID = 203851149;
    , cFormVar="w_CIWMAFNM",tablefilter="", bObbl = .f. , nPag = 7;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  add object oCIWMAFSZ_7_8 as StdField with uid="LNZPGHJJVM",rtseq=101,rtrep=.f.,;
    cFormVar = "w_CIWMAFSZ", cQueryName = "CIWMAFSZ",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Dimensione del font non supportata",;
    ToolTipText = "Dimensione font per la sezione Windows manager",;
    HelpContextID = 64584320,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=463, Top=223, cSayPict='"999"', cGetPict='"999"'

  func oCIWMAFSZ_7_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!Empty(.w_CIWMAFNM) and TestFont('L', .w_CIWMAFNM, .w_CIWMAFSZ)<>-1 And !Empty(.w_CIWMAFSZ))
      if bRes and !(TestFont('L', .w_CIWMAFNM, .w_CIWMAFSZ) = 1)
         bRes=(cp_WarningMsg(thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oCIOPNGST_7_17 as StdField with uid="AAVMDAJZLK",rtseq=102,rtrep=.f.,;
    cFormVar = "w_CIOPNGST", cQueryName = "CIOPNGST",;
    bObbl = .f. , nPag = 7, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Permette di specificare una o pi� gestioni (separate da ;) da aprire all'avvio",;
    HelpContextID = 95156858,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=187, Top=257, InputMask=replicate('X',50)

  add object oCIDSKRSS_7_19 as StdCheck with uid="SRGGROYERW",rtseq=105,rtrep=.f.,left=188, top=294, caption="Visualizza RSS Feeds",;
    ToolTipText = "Se attivo mostra il controllo RSS Viewer all'interno del deskmen�",;
    HelpContextID = 8276601,;
    cFormVar="w_CIDSKRSS", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oCIDSKRSS_7_19.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIDSKRSS_7_19.GetRadio()
    this.Parent.oContained.w_CIDSKRSS = this.RadioValue()
    return .t.
  endfunc

  func oCIDSKRSS_7_19.SetRadio()
    this.Parent.oContained.w_CIDSKRSS=trim(this.Parent.oContained.w_CIDSKRSS)
    this.value = ;
      iif(this.Parent.oContained.w_CIDSKRSS=='S',1,;
      0)
  endfunc

  add object oStr_7_9 as StdString with uid="RBCVPKTCZB",Visible=.t., Left=26, Top=43,;
    Alignment=1, Width=226, Height=18,;
    Caption="Desktop men�:"  ;
  , bGlobalFont=.t.

  add object oStr_7_10 as StdString with uid="JMOSAGWQOA",Visible=.t., Left=23, Top=189,;
    Alignment=1, Width=159, Height=18,;
    Caption="Font men� navigator:"  ;
  , bGlobalFont=.t.

  add object oStr_7_11 as StdString with uid="UBKQDBPFVW",Visible=.t., Left=23, Top=222,;
    Alignment=1, Width=159, Height=18,;
    Caption="Font windows manager:"  ;
  , bGlobalFont=.t.

  add object oStr_7_12 as StdString with uid="YVNYKNFGIQ",Visible=.t., Left=355, Top=189,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_7_13 as StdString with uid="ESWVGHVNDV",Visible=.t., Left=355, Top=223,;
    Alignment=1, Width=101, Height=18,;
    Caption="Dimensione:"  ;
  , bGlobalFont=.t.

  add object oStr_7_14 as StdString with uid="UZCLKWTABU",Visible=.t., Left=26, Top=105,;
    Alignment=1, Width=226, Height=18,;
    Caption="Numero di bottoni visualizzabili:"  ;
  , bGlobalFont=.t.

  add object oStr_7_15 as StdString with uid="VRGRVVMDTK",Visible=.t., Left=26, Top=74,;
    Alignment=1, Width=226, Height=18,;
    Caption="Stato Desktop men� all'avvio:"  ;
  , bGlobalFont=.t.

  add object oStr_7_16 as StdString with uid="HQOOUXWMFP",Visible=.t., Left=26, Top=134,;
    Alignment=1, Width=226, Height=18,;
    Caption="Larghezza iniziale Desktop men�:"  ;
  , bGlobalFont=.t.

  add object oStr_7_18 as StdString with uid="SVMGPHLNEJ",Visible=.t., Left=0, Top=259,;
    Alignment=1, Width=182, Height=18,;
    Caption="Gestioni da aprire all'avvio:"  ;
  , bGlobalFont=.t.

  add object oStr_7_20 as StdString with uid="IHCCWZTEKC",Visible=.t., Left=469, Top=43,;
    Alignment=0, Width=53, Height=18,;
    Caption="C.m. : 1%"  ;
  , bGlobalFont=.t.

  func oStr_7_20.mHide()
    with this.Parent.oContained
      return (.w_CIDSKMEN<>'S')
    endwith
  endfunc

  add object oStr_7_21 as StdString with uid="GQZHAOMVPC",Visible=.t., Left=469, Top=43,;
    Alignment=0, Width=53, Height=18,;
    Caption="C.m. : 6%"  ;
  , bGlobalFont=.t.

  func oStr_7_21.mHide()
    with this.Parent.oContained
      return (.w_CIDSKMEN<>'O')
    endwith
  endfunc

  add object oStr_7_22 as StdString with uid="MCAKOCBJLC",Visible=.t., Left=469, Top=43,;
    Alignment=0, Width=63, Height=18,;
    Caption="C.m. : 0.1%"  ;
  , bGlobalFont=.t.

  func oStr_7_22.mHide()
    with this.Parent.oContained
      return (.w_CIDSKMEN<>'H')
    endwith
  endfunc
enddefine
define class tgsut_acuPag8 as StdContainer
  Width  = 709
  height = 511
  stdWidth  = 709
  stdheight = 511
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oXXB1_8_2 as StdCheck with uid="RBIMBLFCWU",rtseq=160,rtrep=.f.,left=98, top=49, caption="Crea Post-IN",;
    ToolTipText = "Se attivo visualizza il bottone 'Crea Post-IN'",;
    HelpContextID = 106153354,;
    cFormVar="w_XXB1", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB1_8_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB1_8_2.GetRadio()
    this.Parent.oContained.w_XXB1 = this.RadioValue()
    return .t.
  endfunc

  func oXXB1_8_2.SetRadio()
    this.Parent.oContained.w_XXB1=trim(this.Parent.oContained.w_XXB1)
    this.value = ;
      iif(this.Parent.oContained.w_XXB1=='S',1,;
      0)
  endfunc

  func oXXB1_8_2.mHide()
    with this.Parent.oContained
      return (NOT (i_ab_btnpostin And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB2_8_3 as StdCheck with uid="WLUETDTCVL",rtseq=161,rtrep=.f.,left=98, top=75, caption="Cartella Post-IN",;
    ToolTipText = "Se attivo visualizza il bottone 'Cartella Post-IN'",;
    HelpContextID = 106087818,;
    cFormVar="w_XXB2", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB2_8_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB2_8_3.GetRadio()
    this.Parent.oContained.w_XXB2 = this.RadioValue()
    return .t.
  endfunc

  func oXXB2_8_3.SetRadio()
    this.Parent.oContained.w_XXB2=trim(this.Parent.oContained.w_XXB2)
    this.value = ;
      iif(this.Parent.oContained.w_XXB2=='S',1,;
      0)
  endfunc

  func oXXB2_8_3.mHide()
    with this.Parent.oContained
      return (NOT (i_ab_btnpostin And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB3_8_4 as StdCheck with uid="AGXDNYDXDF",rtseq=162,rtrep=.f.,left=98, top=101, caption="Verifica posta",;
    ToolTipText = "Se attivo visualizza il bottone 'Verifica posta'",;
    HelpContextID = 106022282,;
    cFormVar="w_XXB3", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB3_8_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB3_8_4.GetRadio()
    this.Parent.oContained.w_XXB3 = this.RadioValue()
    return .t.
  endfunc

  func oXXB3_8_4.SetRadio()
    this.Parent.oContained.w_XXB3=trim(this.Parent.oContained.w_XXB3)
    this.value = ;
      iif(this.Parent.oContained.w_XXB3=='S',1,;
      0)
  endfunc

  func oXXB3_8_4.mHide()
    with this.Parent.oContained
      return (NOT (Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB4_8_5 as StdCheck with uid="NOFCSLYFHU",rtseq=163,rtrep=.f.,left=98, top=127, caption="Utenti",;
    ToolTipText = "Se attivo visualizza il bottone 'Utenti'",;
    HelpContextID = 105956746,;
    cFormVar="w_XXB4", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB4_8_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB4_8_5.GetRadio()
    this.Parent.oContained.w_XXB4 = this.RadioValue()
    return .t.
  endfunc

  func oXXB4_8_5.SetRadio()
    this.Parent.oContained.w_XXB4=trim(this.Parent.oContained.w_XXB4)
    this.value = ;
      iif(this.Parent.oContained.w_XXB4=='S',1,;
      0)
  endfunc

  func oXXB4_8_5.mHide()
    with this.Parent.oContained
      return (NOT (i_ab_btnuser And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB5_8_6 as StdCheck with uid="ELWCJYRBTU",rtseq=164,rtrep=.f.,left=98, top=153, caption="Flussi e autorizzazioni",;
    ToolTipText = "Se attivo visualizza il bottone 'Flussi e autorizzazioni'",;
    HelpContextID = 105891210,;
    cFormVar="w_XXB5", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB5_8_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB5_8_6.GetRadio()
    this.Parent.oContained.w_XXB5 = this.RadioValue()
    return .t.
  endfunc

  func oXXB5_8_6.SetRadio()
    this.Parent.oContained.w_XXB5=trim(this.Parent.oContained.w_XXB5)
    this.value = ;
      iif(this.Parent.oContained.w_XXB5=='S',1,;
      0)
  endfunc

  func oXXB5_8_6.mHide()
    with this.Parent.oContained
      return (NOT (g_APPLICATION <> "ADHOC REVOLUTION" And g_WFLD='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB17_8_7 as StdCheck with uid="HKLCYKWOHL",rtseq=165,rtrep=.f.,left=98, top=179, caption="Infinity Zucchetti",;
    ToolTipText = "Se attivo visualizza il bottone 'Infinity Zucchetti'",;
    HelpContextID = 48481674,;
    cFormVar="w_XXB17", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB17_8_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB17_8_7.GetRadio()
    this.Parent.oContained.w_XXB17 = this.RadioValue()
    return .t.
  endfunc

  func oXXB17_8_7.SetRadio()
    this.Parent.oContained.w_XXB17=trim(this.Parent.oContained.w_XXB17)
    this.value = ;
      iif(this.Parent.oContained.w_XXB17=='S',1,;
      0)
  endfunc

  func oXXB17_8_7.mHide()
    with this.Parent.oContained
      return (NOT (g_REVI='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB6_8_8 as StdCheck with uid="KWTKJYMRCJ",rtseq=166,rtrep=.f.,left=98, top=205, caption="Ricerca documenti archiviati",;
    ToolTipText = "Se attivo visualizza il bottone 'Ricerca documenti archiviati'",;
    HelpContextID = 105825674,;
    cFormVar="w_XXB6", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB6_8_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB6_8_8.GetRadio()
    this.Parent.oContained.w_XXB6 = this.RadioValue()
    return .t.
  endfunc

  func oXXB6_8_8.SetRadio()
    this.Parent.oContained.w_XXB6=trim(this.Parent.oContained.w_XXB6)
    this.value = ;
      iif(this.Parent.oContained.w_XXB6=='S',1,;
      0)
  endfunc

  func oXXB6_8_8.mHide()
    with this.Parent.oContained
      return (NOT (g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB7_8_9 as StdCheck with uid="PYQJMBWWTO",rtseq=167,rtrep=.f.,left=98, top=231, caption="Ricerca documenti archiviati per pratica",;
    ToolTipText = "Se attivo visualizza il bottone 'Ricerca documenti archiviati per pratica'",;
    HelpContextID = 105760138,;
    cFormVar="w_XXB7", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB7_8_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB7_8_9.GetRadio()
    this.Parent.oContained.w_XXB7 = this.RadioValue()
    return .t.
  endfunc

  func oXXB7_8_9.SetRadio()
    this.Parent.oContained.w_XXB7=trim(this.Parent.oContained.w_XXB7)
    this.value = ;
      iif(this.Parent.oContained.w_XXB7=='S',1,;
      0)
  endfunc

  func oXXB7_8_9.mHide()
    with this.Parent.oContained
      return (NOT (isalt() and g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB8_8_10 as StdCheck with uid="RDXFOHIJSC",rtseq=168,rtrep=.f.,left=98, top=257, caption="Nuovo documento",;
    ToolTipText = "Se attivo visualizza il bottone 'Nuovo documento'",;
    HelpContextID = 105694602,;
    cFormVar="w_XXB8", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB8_8_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB8_8_10.GetRadio()
    this.Parent.oContained.w_XXB8 = this.RadioValue()
    return .t.
  endfunc

  func oXXB8_8_10.SetRadio()
    this.Parent.oContained.w_XXB8=trim(this.Parent.oContained.w_XXB8)
    this.value = ;
      iif(this.Parent.oContained.w_XXB8=='S',1,;
      0)
  endfunc

  func oXXB8_8_10.mHide()
    with this.Parent.oContained
      return (NOT (g_DMIP<>'S' and g_DOCM='S' and not ((g_JBSH='S' OR g_IZCP='S') AND g_UserScheduler)))
    endwith
  endfunc

  add object oXXB9_8_11 as StdCheck with uid="XBAASWQSYU",rtseq=169,rtrep=.f.,left=98, top=283, caption="Archiviazione da file di un documento",;
    ToolTipText = "Se attivo visualizza il bottone 'Archiviazione da file di un documento'",;
    HelpContextID = 105629066,;
    cFormVar="w_XXB9", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB9_8_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB9_8_11.GetRadio()
    this.Parent.oContained.w_XXB9 = this.RadioValue()
    return .t.
  endfunc

  func oXXB9_8_11.SetRadio()
    this.Parent.oContained.w_XXB9=trim(this.Parent.oContained.w_XXB9)
    this.value = ;
      iif(this.Parent.oContained.w_XXB9=='S',1,;
      0)
  endfunc

  func oXXB9_8_11.mHide()
    with this.Parent.oContained
      return (NOT (g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB10_8_12 as StdCheck with uid="QILEMMXVRJ",rtseq=170,rtrep=.f.,left=98, top=309, caption="Archiviazione da Infinity di un documento",;
    ToolTipText = "Se attivo visualizza il bottone 'Archiviazione da Infinity di un documento'",;
    HelpContextID = 55821706,;
    cFormVar="w_XXB10", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB10_8_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB10_8_12.GetRadio()
    this.Parent.oContained.w_XXB10 = this.RadioValue()
    return .t.
  endfunc

  func oXXB10_8_12.SetRadio()
    this.Parent.oContained.w_XXB10=trim(this.Parent.oContained.w_XXB10)
    this.value = ;
      iif(this.Parent.oContained.w_XXB10=='S',1,;
      0)
  endfunc

  func oXXB10_8_12.mHide()
    with this.Parent.oContained
      return (.t. or NOT (NOT isalt() and Not((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB11_8_13 as StdCheck with uid="IHQCUPUZBW",rtseq=171,rtrep=.f.,left=98, top=335, caption="Archiviazione da scanner di un documento",;
    ToolTipText = "Se attivo visualizza il bottone 'Archiviazione da scanner di un documento'",;
    HelpContextID = 54773130,;
    cFormVar="w_XXB11", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB11_8_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB11_8_13.GetRadio()
    this.Parent.oContained.w_XXB11 = this.RadioValue()
    return .t.
  endfunc

  func oXXB11_8_13.SetRadio()
    this.Parent.oContained.w_XXB11=trim(this.Parent.oContained.w_XXB11)
    this.value = ;
      iif(this.Parent.oContained.w_XXB11=='S',1,;
      0)
  endfunc

  func oXXB11_8_13.mHide()
    with this.Parent.oContained
      return (NOT (g_DOCM='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB12_8_14 as StdCheck with uid="GBQOMDSMUN",rtseq=172,rtrep=.f.,left=98, top=361, caption="Associazione attributi",;
    ToolTipText = "Se attivo visualizza il bottone 'Associazione attributi'",;
    HelpContextID = 53724554,;
    cFormVar="w_XXB12", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB12_8_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB12_8_14.GetRadio()
    this.Parent.oContained.w_XXB12 = this.RadioValue()
    return .t.
  endfunc

  func oXXB12_8_14.SetRadio()
    this.Parent.oContained.w_XXB12=trim(this.Parent.oContained.w_XXB12)
    this.value = ;
      iif(this.Parent.oContained.w_XXB12=='S',1,;
      0)
  endfunc

  func oXXB12_8_14.mHide()
    with this.Parent.oContained
      return (NOT (g_APPLICATION <> "ADHOC REVOLUTION"And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB13_8_15 as StdCheck with uid="ADSMDDRZQT",rtseq=173,rtrep=.f.,left=98, top=387, caption="InfoBusiness",;
    ToolTipText = "Se attivo visualizza il bottone 'InfoBusiness'",;
    HelpContextID = 52675978,;
    cFormVar="w_XXB13", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB13_8_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB13_8_15.GetRadio()
    this.Parent.oContained.w_XXB13 = this.RadioValue()
    return .t.
  endfunc

  func oXXB13_8_15.SetRadio()
    this.Parent.oContained.w_XXB13=trim(this.Parent.oContained.w_XXB13)
    this.value = ;
      iif(this.Parent.oContained.w_XXB13=='S',1,;
      0)
  endfunc

  func oXXB13_8_15.mHide()
    with this.Parent.oContained
      return (NOT (g_IRDR='S' And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler)))
    endwith
  endfunc

  add object oXXB14_8_16 as StdCheck with uid="MYOPJSJJZN",rtseq=174,rtrep=.f.,left=98, top=413, caption="Configurazioni",;
    ToolTipText = "Se attivo visualizza il bottone 'Configurazioni'",;
    HelpContextID = 51627402,;
    cFormVar="w_XXB14", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB14_8_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB14_8_16.GetRadio()
    this.Parent.oContained.w_XXB14 = this.RadioValue()
    return .t.
  endfunc

  func oXXB14_8_16.SetRadio()
    this.Parent.oContained.w_XXB14=trim(this.Parent.oContained.w_XXB14)
    this.value = ;
      iif(this.Parent.oContained.w_XXB14=='S',1,;
      0)
  endfunc

  func oXXB14_8_16.mHide()
    with this.Parent.oContained
      return (NOT ((Vartype(g_bDisableCfgGest)="U" Or !g_bDisableCfgGest) And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler) And Not Isalt()))
    endwith
  endfunc

  add object oXXB15_8_17 as StdCheck with uid="ISUPLYUHTG",rtseq=175,rtrep=.f.,left=98, top=439, caption="La mia agenda",;
    ToolTipText = "Se attivo visualizza il bottone 'La mia agenda'",;
    HelpContextID = 50578826,;
    cFormVar="w_XXB15", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB15_8_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB15_8_17.GetRadio()
    this.Parent.oContained.w_XXB15 = this.RadioValue()
    return .t.
  endfunc

  func oXXB15_8_17.SetRadio()
    this.Parent.oContained.w_XXB15=trim(this.Parent.oContained.w_XXB15)
    this.value = ;
      iif(this.Parent.oContained.w_XXB15=='S',1,;
      0)
  endfunc

  func oXXB15_8_17.mHide()
    with this.Parent.oContained
      return (NOT (g_AGEN="S" And (Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler))))
    endwith
  endfunc

  add object oXXB16_8_18 as StdCheck with uid="AOZDSDNGEL",rtseq=176,rtrep=.f.,left=98, top=465, caption="Timer",;
    ToolTipText = "Se attivo visualizza il bottone 'Timer'",;
    HelpContextID = 49530250,;
    cFormVar="w_XXB16", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oXXB16_8_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oXXB16_8_18.GetRadio()
    this.Parent.oContained.w_XXB16 = this.RadioValue()
    return .t.
  endfunc

  func oXXB16_8_18.SetRadio()
    this.Parent.oContained.w_XXB16=trim(this.Parent.oContained.w_XXB16)
    this.value = ;
      iif(this.Parent.oContained.w_XXB16=='S',1,;
      0)
  endfunc

  func oXXB16_8_18.mHide()
    with this.Parent.oContained
      return (NOT (g_AGEN="S" And Not ((g_JBSH='S' Or g_IZCP='S') And g_UserScheduler) And Isalt()))
    endwith
  endfunc


  add object XXB1IM as cp_showimage with uid="DXVYBGWJOA",left=56, top=49, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="postit.bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB4IM as cp_showimage with uid="IKRTAZQFLK",left=56, top=127, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="user.bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB3IM as cp_showimage with uid="RWDWOGPRGV",left=56, top=101, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="maildn.bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB2IM as cp_showimage with uid="SURTIJYNNZ",left=56, top=75, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="folder_postit.bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB5IM as cp_showimage with uid="PFRDOHLXQI",left=56, top=153, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="wfld.Bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB61IM as cp_showimage with uid="CSAIEDZPID",left=56, top=205, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="visualicat.bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB17IM as cp_showimage with uid="VPJLUXAJCN",left=56, top=179, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="fixinf.bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB62IM as cp_showimage with uid="ULFHCVDLJX",left=56, top=205, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="visualicat_infinityproject.bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB7IM as cp_showimage with uid="EBAXPPGCOY",left=56, top=231, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="VisualPra.ico",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB92IM as cp_showimage with uid="NFZDLTSGMP",left=56, top=283, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="zarchivia_infinityproject.bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB91IM as cp_showimage with uid="UZEASQFRSF",left=56, top=283, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="zarchivia.bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB82IM as cp_showimage with uid="AQAMVJJFDK",left=56, top=257, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="folder_edit_infinityproject.bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB81IM as cp_showimage with uid="FRETJONTZY",left=56, top=257, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="folder_edit.bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB101IM as cp_showimage with uid="KAKZPICNIA",left=56, top=309, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="barcode.Bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB102IM as cp_showimage with uid="BNSJDBSFIP",left=56, top=309, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="barcode_infinityproject.Bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB111IM as cp_showimage with uid="TLRKASPGEL",left=56, top=335, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="scanner.Bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB112IM as cp_showimage with uid="TYLLIJMZHR",left=56, top=335, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="scanner_infinityproject.Bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB12IM as cp_showimage with uid="WZOPPYINGB",left=56, top=361, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="tattrib.Bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB13IM as cp_showimage with uid="WFVNESHEZC",left=56, top=387, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="IReader.Bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB14IM as cp_showimage with uid="DYJHYICTTR",left=56, top=413, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="FOLDERCL.Bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB15IM as cp_showimage with uid="ACJSGCKREM",left=56, top=439, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="ScadT.Bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662


  add object XXB16IM as cp_showimage with uid="VDUSOUXLYS",left=56, top=465, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="timer.Bmp",stretch=1,;
    nPag=8;
    , HelpContextID = 68338662

  add object oStr_8_45 as StdString with uid="CLMOCOMNKX",Visible=.t., Left=98, Top=16,;
    Alignment=0, Width=512, Height=18,;
    Caption="Elenco dei bottoni da visualizzare nella barra delle applicazioni"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_acu','CONF_INT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CICODUTE=CONF_INT.CICODUTE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_acu
*--- Aggiorna i campi delle impostazioni
Proc Aggiorna_Impostazioni(pParent)
IF pParent.w_CISYSFRM='S'
  *--- Salvo le impostazioni
   cOLDSETSYS = SET('SYSFORMATS')
   cOLDSETMRK = '"'+SET('MARK')+'"'
   cOLDSETPNT = '"'+SET('POINT')+'"'
   cOLDSETSEP = '"'+SET('SEPARATOR')+'"'
   cOLDSETDAT = SET('DATE')
   cOLDSETCEN = SET('CENTURY')

   *--- Abilito le impostazioni S.O.
   SET SYSFORMATS ON
   DO CASE
     CASE SET('DATE')='AMERICAN'
        pParent.w_SETDAT='A'
     CASE SET('DATE')='ANSI'
        pParent.w_SETDAT='N'
     CASE SET('DATE')='BRITISH' OR SET('DATE')='FRENCH'
        pParent.w_SETDAT='B'
     CASE SET('DATE')='GERMAN'
        pParent.w_SETDAT='G'
     CASE SET('DATE')='ITALIAN'
        pParent.w_SETDAT='I'
     CASE SET('DATE')='JAPAN'
        pParent.w_SETDAT='J'
     CASE SET('DATE')='TAIWAN'
        pParent.w_SETDAT='T'
     CASE SET('DATE')='USA'
        pParent.w_SETDAT='U'
     CASE SET('DATE')='MDY'
        pParent.w_SETDAT='M'
     CASE SET('DATE')='DMY'
        pParent.w_SETDAT='D'
     CASE SET('DATE')='YMD'
        pParent.w_SETDAT='Y'
     CASE SET('DATE')='SHORT'
        pParent.w_SETDAT='S'
     CASE SET('DATE')='LONG'
        pParent.w_SETDAT='L'
   ENDCASE
   pParent.w_SETSEP=SET('SEPARATOR')
   pParent.w_SETPNT=SET('POINT')
   pParent.w_SETMRK=SET('MARK')
   pParent.w_SETCEN=IIF(SET('CENTURY')='ON','S', 'N')

   *--- Ripristino le impostazioni precedenti
   SET SYSFORMATS &cOLDSETSYS
   SET CENTURY &cOLDSETCEN
   SET MARK TO &cOLDSETMRK
   SET POINT TO &cOLDSETPNT
   SET SEPARATOR TO &cOLDSETSEP
   SET DATE &cOLDSETDAT
ENDIF
endproc
*--- Anteprima data/numero
proc Anteprima_Data_Num(pParent)
*--- Aggiorna data
   oCtrl=pParent.GetCtrl("w_PRVDATA")
   DO CASE
     CASE pParent.w_SETDAT='A'
        oCtrl.DateFormat=1
     CASE pParent.w_SETDAT='N'
        oCtrl.DateFormat=2
     CASE pParent.w_SETDAT='B'
        oCtrl.DateFormat=3
     CASE pParent.w_SETDAT='G'
        oCtrl.DateFormat=6
     CASE pParent.w_SETDAT='I'
        oCtrl.DateFormat=4
     CASE pParent.w_SETDAT='J'
        oCtrl.DateFormat=7
     CASE pParent.w_SETDAT='T'
        oCtrl.DateFormat=8
     CASE pParent.w_SETDAT='U'
        oCtrl.DateFormat=9
     CASE pParent.w_SETDAT='M'
        oCtrl.DateFormat=10
     CASE pParent.w_SETDAT='D'
        oCtrl.DateFormat=11
     CASE pParent.w_SETDAT='Y'
        oCtrl.DateFormat=12
     CASE pParent.w_SETDAT='S'
        oCtrl.DateFormat=13
     CASE pParent.w_SETDAT='L'
        oCtrl.DateFormat=14
   ENDCASE
   oCtrl.DateMark=pParent.w_SETMRK
   oCtrl.Century=IIF(pParent.w_SETCEN='S', 1, 0)
   oCtrl=.null.
   *--- Aggiorna campo numerico
   pParent.w_PRVNUM="1"+pParent.w_SETSEP+"234"+pParent.w_SETSEP+"567"+pParent.w_SETPNT+"12"
endproc

*--- Segnalo che saranno prese le impostazioni di sistema
procedure DataShortMessage(pParent)
   If pParent.w_SETDAT='S'
     Ah_ErrorMsg("Per il formato data saranno utilizzate le impostazioni del sistema operativo",'i')
   Endif
endproc

define class StdTableComboFont as StdTableCombo
  proc Init
    * --- Zucchetti Aulla - Inizio Interfaccia
  	IF VARTYPE(this.bNoBackColor)='U'
		  This.backcolor=i_nEBackColor
	  ENDIF
    * --- Zucchetti Aulla - Fine Interfaccia	
    this.ToolTipText=cp_Translate(this.ToolTipText)
    local l_numf, l_i
    AFONT(this.combovalues)
    This.nValues = ALEN(this.combovalues)
    l_i = 1
    do while l_i <= This.nValues
        this.AddItem(this.combovalues[l_i])
        l_i = l_i + 1
    enddo
    This.SetFont()
  endproc
enddefine

Define Class StdThemesCombo As StdtableCombo

  cPathFiles=""
  
  Proc Init()

		This.BackColor=Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
		This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
		This.ToolTipText=cp_Translate(This.ToolTipText)
    
		Dimension This.combovalues[1]
		This.AddItem('Standard')
    This.combovalues[1]=-1
    local i,j,k,fname,cPath,cFile, cString, cNumTheme, cThemePath, bApplication,bTrovato
    cThemePath = cPathVfcsim+'\themes\'
    ADIR(aDirectories, cThemePath+'*','D')
    *ASORT(aDirectories,1)
    
    *-- I numeri, quando sono di tipo carattere, non vengono ordinati correttamente
    *-- Creo un array di appoggio per ordinare i numeri, dopo faccio corrispondere questo array all'inserimento nella combovalues
    dimension aWellSortedDir[1]
    i=1
    j=1
    do while m.i<=ALEN(aDirectories,1)
      fname = aDirectories[m.i,1]
      bApplication = (IsAlt() And Not 'AHR'$Upper(fname) And Not 'AHE'$Upper(fname)) Or (IsAhe() And Not 'AHR'$Upper(fname) And Not 'AETOP'$Upper(fname)) or (IsAhr() And Not 'AHE'$Upper(fname) And Not 'AETOP'$Upper(fname))
      if bApplication And Isdigit(Left(fname, At('.', fname)-1))
        fname = aDirectories[m.i,1]
        dimension aWellSortedDir[j]
        aWellSortedDir[m.j] = Int(Val(Left(fname,At('.',fname)-1)))
        j = j+1
      endif
      i = m.i+1
    enddo
    ASORT(aWellSortedDir)

    k=1
    Do While m.k<=ALEN(aWellSortedDir)
      bTrovato = .f.
      i=1
    Do While m.i<=ALEN(aDirectories,1) And Not bTrovato
      fname = aDirectories[m.i,1]
      bApplication = (IsAlt() And Not 'AHR'$Upper(fname) And Not 'AHE'$Upper(fname)) Or (IsAhe() And Not 'AHR'$Upper(fname) And Not 'AETOP'$Upper(fname)) or (IsAhr() And Not 'AHE'$Upper(fname) And Not 'AETOP'$Upper(fname))
			If m.bApplication And m.fname<>'.' and m.fname<>'..' and 'D'$aDirectories[m.i,5] and Isdigit(Left(m.fname, At('.', m.fname)-1)) And Left(m.fname, At('.', m.fname)-1) == Alltrim(Str(aWellSortedDir[m.k]))
        cPath = m.cThemePath+aDirectories[m.i,1]
        cFile = Alltrim(Substr(m.fname, At('.', m.fname)+1))
        cNumTheme = Left(m.fname, At('.', m.fname)-1)
        if cp_fileexist(m.cPath+"\"+m.cFile+".xml")
          j=Alen(This.combovalues)+1
          Dimension This.combovalues[m.j]
          m.cString = StrTran(m.cFile,'_',' ')
          This.AddItem(cp_Translate(Proper(m.cString)))
          This.combovalues[m.j]=Int(Val(m.cNumTheme))
          This.cPathFiles = This.cPathFiles + m.cPath + "\"+ m.cFile + ".xml" + ","
          bTrovato = .t.
        Endif
      Endif
      i=m.i+1
    EndDo
    k=m.k+1
    EndDo
    This.nValues=alen(This.combovalues)
		If This.bSetFont
			This.SetFont()
		Endif
		This.ToolTipText=cp_Translate(This.ToolTipText)
    This.BackColor=Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
		This.SpecialEffect = i_nSpecialEffect
		This.BorderColor = i_nBorderColor
		Return
  Endproc
  
  Func GetPath()
  *-- Recupero il path del file relativo al valore della combo
     Local ini,fin
     If this.value>0 And This.combovalues[this.value]>0
       m.ini = iif(This.combovalues[this.value]=1 ,0, At(",",This.cPathFiles,This.combovalues[this.value]-1) )
       m.fin = At(",",This.cPathFiles,This.combovalues[this.value]) - m.ini
       Return SubStr(This.cPathFiles,m.ini+1,m.fin-1)
     Endif
     Return ""
  Endfunc
  
Enddefine
* --- Fine Area Manuale
