* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_mli                                                        *
*              Manutenzione listini                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_82]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-16                                                      *
* Last revis.: 2014-10-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsma_mli")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsma_mli")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsma_mli")
  return

* --- Class definition
define class tgsma_mli as StdPCForm
  Width  = 658
  Height = 229
  Top    = 128
  Left   = 16
  cComment = "Manutenzione listini"
  cPrg = "gsma_mli"
  HelpContextID=53838999
  add object cnt as tcgsma_mli
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsma_mli as PCContext
  w_TIPOPE = space(10)
  w_LICODART = space(20)
  w_LICODLIS = space(5)
  w_DTINVA = space(8)
  w_DTFINV = space(8)
  w_LIDATATT = space(8)
  w_LIDATDIS = space(8)
  w_DESLIS = space(40)
  w_VALLIS = space(3)
  w_SIMVAL = space(5)
  w_IVALIS = space(1)
  w_FLSCO = space(1)
  w_LIUNIMIS = space(3)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_CODART = space(20)
  w_FLSERG = space(1)
  proc Save(i_oFrom)
    this.w_TIPOPE = i_oFrom.w_TIPOPE
    this.w_LICODART = i_oFrom.w_LICODART
    this.w_LICODLIS = i_oFrom.w_LICODLIS
    this.w_DTINVA = i_oFrom.w_DTINVA
    this.w_DTFINV = i_oFrom.w_DTFINV
    this.w_LIDATATT = i_oFrom.w_LIDATATT
    this.w_LIDATDIS = i_oFrom.w_LIDATDIS
    this.w_DESLIS = i_oFrom.w_DESLIS
    this.w_VALLIS = i_oFrom.w_VALLIS
    this.w_SIMVAL = i_oFrom.w_SIMVAL
    this.w_IVALIS = i_oFrom.w_IVALIS
    this.w_FLSCO = i_oFrom.w_FLSCO
    this.w_LIUNIMIS = i_oFrom.w_LIUNIMIS
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_CODART = i_oFrom.w_CODART
    this.w_FLSERG = i_oFrom.w_FLSERG
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_TIPOPE = this.w_TIPOPE
    i_oTo.w_LICODART = this.w_LICODART
    i_oTo.w_LICODLIS = this.w_LICODLIS
    i_oTo.w_DTINVA = this.w_DTINVA
    i_oTo.w_DTFINV = this.w_DTFINV
    i_oTo.w_LIDATATT = this.w_LIDATATT
    i_oTo.w_LIDATDIS = this.w_LIDATDIS
    i_oTo.w_DESLIS = this.w_DESLIS
    i_oTo.w_VALLIS = this.w_VALLIS
    i_oTo.w_SIMVAL = this.w_SIMVAL
    i_oTo.w_IVALIS = this.w_IVALIS
    i_oTo.w_FLSCO = this.w_FLSCO
    i_oTo.w_LIUNIMIS = this.w_LIUNIMIS
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_CODART = this.w_CODART
    i_oTo.w_FLSERG = this.w_FLSERG
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsma_mli as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 658
  Height = 229
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-01"
  HelpContextID=53838999
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  LIS_TINI_IDX = 0
  LISTINI_IDX = 0
  VALUTE_IDX = 0
  LIS_SCAG_IDX = 0
  UNIMIS_IDX = 0
  ART_ICOL_IDX = 0
  cFile = "LIS_TINI"
  cKeySelect = "LICODART"
  cKeyWhere  = "LICODART=this.w_LICODART"
  cKeyDetail  = "LICODART=this.w_LICODART"
  cKeyWhereODBC = '"LICODART="+cp_ToStrODBC(this.w_LICODART)';

  cKeyDetailWhereODBC = '"LICODART="+cp_ToStrODBC(this.w_LICODART)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"LIS_TINI.LICODART="+cp_ToStrODBC(this.w_LICODART)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'LIS_TINI.LICODLIS,LIS_TINI. LIDATATT DESC,LIS_TINI. LIDATDIS'
  cPrg = "gsma_mli"
  cComment = "Manutenzione listini"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPOPE = space(10)
  w_LICODART = space(20)
  o_LICODART = space(20)
  w_LICODLIS = space(5)
  o_LICODLIS = space(5)
  w_DTINVA = ctod('  /  /  ')
  w_DTFINV = ctod('  /  /  ')
  w_LIDATATT = ctod('  /  /  ')
  w_LIDATDIS = ctod('  /  /  ')
  w_DESLIS = space(40)
  w_VALLIS = space(3)
  w_SIMVAL = space(5)
  w_IVALIS = space(1)
  w_FLSCO = space(1)
  w_LIUNIMIS = space(3)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_CODART = space(20)
  w_FLSERG = space(1)

  * --- Children pointers
  GSMA_MLG = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsma_mli
  bLoadRecFilter=.t.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSMA_MLG additive
    with this
      .Pages(1).addobject("oPag","tgsma_mliPag1","gsma_mli",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSMA_MLG
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='LIS_SCAG'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='LIS_TINI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.LIS_TINI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.LIS_TINI_IDX,3]
  return

  function CreateChildren()
    this.GSMA_MLG = CREATEOBJECT('stdDynamicChild',this,'GSMA_MLG',this.oPgFrm.Page1.oPag.oLinkPC_2_9)
    this.GSMA_MLG.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgsma_mli'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSMA_MLG)
      this.GSMA_MLG.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSMA_MLG.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSMA_MLG.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSMA_MLG)
      this.GSMA_MLG.DestroyChildrenChain()
      this.GSMA_MLG=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_9')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMA_MLG.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMA_MLG.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMA_MLG.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSMA_MLG.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_LICODART,"LICODART";
             ,.w_CPROWNUM,"LIROWNUM";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from LIS_TINI where LICODART=KeySet.LICODART
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.LIS_TINI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LIS_TINI_IDX,2],this.bLoadRecFilter,this.LIS_TINI_IDX,"gsma_mli")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('LIS_TINI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "LIS_TINI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' LIS_TINI '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LICODART',this.w_LICODART  )
      select * from (i_cTable) LIS_TINI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPOPE = this.oParentObject .w_TIPOPE
        .w_LICODART = NVL(LICODART,space(20))
        .w_UNMIS1 = this.oParentObject .w_ARUNMIS1
        .w_UNMIS2 = this.oParentObject .w_ARUNMIS2
        .w_CODART = this.oParentObject .w_ARCODART
        .w_FLSERG = IIF(this.oParentObject .w_ARTIPART<>'FM', ' ', this.oParentObject .w_ARFLSERG)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'LIS_TINI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DTINVA = ctod("  /  /  ")
          .w_DTFINV = ctod("  /  /  ")
          .w_DESLIS = space(40)
          .w_VALLIS = space(3)
          .w_SIMVAL = space(5)
          .w_IVALIS = space(1)
          .w_FLSCO = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_LICODLIS = NVL(LICODLIS,space(5))
          if link_2_1_joined
            this.w_LICODLIS = NVL(LSCODLIS201,NVL(this.w_LICODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS201,space(40))
            this.w_IVALIS = NVL(LSIVALIS201,space(1))
            this.w_VALLIS = NVL(LSVALLIS201,space(3))
            this.w_DTINVA = NVL(cp_ToDate(LSDTINVA201),ctod("  /  /  "))
            this.w_DTFINV = NVL(cp_ToDate(LSDTOBSO201),ctod("  /  /  "))
            this.w_FLSCO = NVL(LSFLSCON201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_LIDATATT = NVL(cp_ToDate(LIDATATT),ctod("  /  /  "))
          .w_LIDATDIS = NVL(cp_ToDate(LIDATDIS),ctod("  /  /  "))
          .link_2_7('Load')
          .w_LIUNIMIS = NVL(LIUNIMIS,space(3))
          * evitabile
          *.link_2_12('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_TIPOPE = this.oParentObject .w_TIPOPE
        .w_UNMIS1 = this.oParentObject .w_ARUNMIS1
        .w_UNMIS2 = this.oParentObject .w_ARUNMIS2
        .w_CODART = this.oParentObject .w_ARCODART
        .w_FLSERG = IIF(this.oParentObject .w_ARTIPART<>'FM', ' ', this.oParentObject .w_ARFLSERG)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
    this.Calculate_LZEYELHFDX()
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TIPOPE=space(10)
      .w_LICODART=space(20)
      .w_LICODLIS=space(5)
      .w_DTINVA=ctod("  /  /  ")
      .w_DTFINV=ctod("  /  /  ")
      .w_LIDATATT=ctod("  /  /  ")
      .w_LIDATDIS=ctod("  /  /  ")
      .w_DESLIS=space(40)
      .w_VALLIS=space(3)
      .w_SIMVAL=space(5)
      .w_IVALIS=space(1)
      .w_FLSCO=space(1)
      .w_LIUNIMIS=space(3)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_CODART=space(20)
      .w_FLSERG=space(1)
      if .cFunction<>"Filter"
        .w_TIPOPE = this.oParentObject .w_TIPOPE
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_LICODLIS))
         .link_2_1('Full')
        endif
        .DoRTCalc(4,5,.f.)
        .w_LIDATATT = .w_DTINVA
        .w_LIDATDIS = IIF(.w_DTFINV<.w_LIDATATT AND NOT EMPTY(.w_DTFINV), .w_LIDATATT, .w_DTFINV)
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_VALLIS))
         .link_2_7('Full')
        endif
        .DoRTCalc(10,12,.f.)
        .w_LIUNIMIS = .w_UNMIS1
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_LIUNIMIS))
         .link_2_12('Full')
        endif
        .w_UNMIS1 = this.oParentObject .w_ARUNMIS1
        .w_UNMIS2 = this.oParentObject .w_ARUNMIS2
        .w_CODART = this.oParentObject .w_ARCODART
        .w_FLSERG = IIF(this.oParentObject .w_ARTIPART<>'FM', ' ', this.oParentObject .w_ARFLSERG)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'LIS_TINI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsma_mli
    * Imposto il focus sul primo controllo, altrimenti viene impostato sul dettaglio scaglioni
    this.oPgFrm.Page1.oPag.oBody.oFirstControl.SetFocus()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oLIUNIMIS_2_12.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSMA_MLG.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'LIS_TINI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSMA_MLG.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.LIS_TINI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LICODART,"LICODART",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_LICODLIS C(5);
      ,t_LIDATATT D(8);
      ,t_LIDATDIS D(8);
      ,t_DESLIS C(40);
      ,t_SIMVAL C(5);
      ,t_IVALIS N(3);
      ,t_LIUNIMIS C(3);
      ,CPROWNUM N(10);
      ,t_DTINVA D(8);
      ,t_DTFINV D(8);
      ,t_VALLIS C(3);
      ,t_FLSCO C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsma_mlibodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLICODLIS_2_1.controlsource=this.cTrsName+'.t_LICODLIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLIDATATT_2_4.controlsource=this.cTrsName+'.t_LIDATATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLIDATDIS_2_5.controlsource=this.cTrsName+'.t_LIDATDIS'
    this.oPgFRm.Page1.oPag.oDESLIS_2_6.controlsource=this.cTrsName+'.t_DESLIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSIMVAL_2_8.controlsource=this.cTrsName+'.t_SIMVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIVALIS_2_10.controlsource=this.cTrsName+'.t_IVALIS'
    this.oPgFRm.Page1.oPag.oLIUNIMIS_2_12.controlsource=this.cTrsName+'.t_LIUNIMIS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(71)
    this.AddVLine(154)
    this.AddVLine(235)
    this.AddVLine(292)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLICODLIS_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.LIS_TINI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LIS_TINI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.LIS_TINI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LIS_TINI_IDX,2])
      *
      * insert into LIS_TINI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'LIS_TINI')
        i_extval=cp_InsertValODBCExtFlds(this,'LIS_TINI')
        i_cFldBody=" "+;
                  "(LICODART,LICODLIS,LIDATATT,LIDATDIS,LIUNIMIS,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_LICODART)+","+cp_ToStrODBCNull(this.w_LICODLIS)+","+cp_ToStrODBC(this.w_LIDATATT)+","+cp_ToStrODBC(this.w_LIDATDIS)+","+cp_ToStrODBCNull(this.w_LIUNIMIS)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'LIS_TINI')
        i_extval=cp_InsertValVFPExtFlds(this,'LIS_TINI')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'LICODART',this.w_LICODART)
        INSERT INTO (i_cTable) (;
                   LICODART;
                  ,LICODLIS;
                  ,LIDATATT;
                  ,LIDATDIS;
                  ,LIUNIMIS;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_LICODART;
                  ,this.w_LICODLIS;
                  ,this.w_LIDATATT;
                  ,this.w_LIDATDIS;
                  ,this.w_LIUNIMIS;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.LIS_TINI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LIS_TINI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_LICODLIS<>space(5) AND NOT EMPTY(t_LIDATDIS)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'LIS_TINI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'LIS_TINI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_LICODLIS<>space(5) AND NOT EMPTY(t_LIDATDIS)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSMA_MLG.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_LICODART,"LICODART";
                     ,this.w_CPROWNUM,"LIROWNUM";
                     )
              this.GSMA_MLG.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update LIS_TINI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'LIS_TINI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " LICODLIS="+cp_ToStrODBCNull(this.w_LICODLIS)+;
                     ",LIDATATT="+cp_ToStrODBC(this.w_LIDATATT)+;
                     ",LIDATDIS="+cp_ToStrODBC(this.w_LIDATDIS)+;
                     ",LIUNIMIS="+cp_ToStrODBCNull(this.w_LIUNIMIS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'LIS_TINI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      LICODLIS=this.w_LICODLIS;
                     ,LIDATATT=this.w_LIDATATT;
                     ,LIDATDIS=this.w_LIDATDIS;
                     ,LIUNIMIS=this.w_LIUNIMIS;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (t_LICODLIS<>space(5) AND NOT EMPTY(t_LIDATDIS))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSMA_MLG.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_LICODART,"LICODART";
               ,this.w_CPROWNUM,"LIROWNUM";
               )
          this.GSMA_MLG.mReplace()
          this.GSMA_MLG.bSaveContext=.f.
        endif
      endscan
     this.GSMA_MLG.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.LIS_TINI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LIS_TINI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_LICODLIS<>space(5) AND NOT EMPTY(t_LIDATDIS)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSMA_MLG : Deleting
        this.GSMA_MLG.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_LICODART,"LICODART";
               ,this.w_CPROWNUM,"LIROWNUM";
               )
        this.GSMA_MLG.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete LIS_TINI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_LICODLIS<>space(5) AND NOT EMPTY(t_LIDATDIS)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.LIS_TINI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LIS_TINI_IDX,2])
    if i_bUpd
      with this
          .w_TIPOPE = this.oParentObject .w_TIPOPE
        .DoRTCalc(2,2,.t.)
        if .o_LICODART<>.w_LICODART
          .link_2_1('Full')
        endif
        .DoRTCalc(4,5,.t.)
        if .o_LICODLIS<>.w_LICODLIS
          .w_LIDATATT = .w_DTINVA
        endif
        if .o_LICODLIS<>.w_LICODLIS
          .w_LIDATDIS = IIF(.w_DTFINV<.w_LIDATATT AND NOT EMPTY(.w_DTFINV), .w_LIDATATT, .w_DTFINV)
        endif
        .DoRTCalc(8,8,.t.)
          .link_2_7('Full')
        .DoRTCalc(10,13,.t.)
          .w_UNMIS1 = this.oParentObject .w_ARUNMIS1
          .w_UNMIS2 = this.oParentObject .w_ARUNMIS2
          .w_CODART = this.oParentObject .w_ARCODART
          .w_FLSERG = IIF(this.oParentObject .w_ARTIPART<>'FM', ' ', this.oParentObject .w_ARFLSERG)
        if .o_LICODLIS<>.w_LICODLIS
          .Calculate_XAZVQYUEQH()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DTINVA with this.w_DTINVA
      replace t_DTFINV with this.w_DTFINV
      replace t_VALLIS with this.w_VALLIS
      replace t_FLSCO with this.w_FLSCO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_XAZVQYUEQH()
    with this
          * --- ricalcola Um
          .w_LIUNIMIS = .w_UNMIS1
    endwith
  endproc
  proc Calculate_LZEYELHFDX()
    with this
          * --- Ricerca chiave in situazione di filtro
          gsma_blk(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLIDATATT_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLIDATATT_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLIDATDIS_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLIDATDIS_2_5.mCond()
    this.GSMA_MLG.enabled = this.oPgFrm.Page1.oPag.oLinkPC_2_9.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LICODLIS
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LICODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LICODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LICODLIS))
          select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LICODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_LICODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_LICODLIS)+"%");

            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_LICODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLICODLIS_2_1'),i_cWhere,'GSAR_ALI',"Listini",'GSMA1MVM.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LICODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LICODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LICODLIS)
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LICODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_DTINVA = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_DTFINV = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_FLSCO = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LICODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_IVALIS = space(1)
      this.w_VALLIS = space(3)
      this.w_DTINVA = ctod("  /  /  ")
      this.w_DTFINV = ctod("  /  /  ")
      this.w_FLSCO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LICODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.LSCODLIS as LSCODLIS201"+ ",link_2_1.LSDESLIS as LSDESLIS201"+ ",link_2_1.LSIVALIS as LSIVALIS201"+ ",link_2_1.LSVALLIS as LSVALLIS201"+ ",link_2_1.LSDTINVA as LSDTINVA201"+ ",link_2_1.LSDTOBSO as LSDTOBSO201"+ ",link_2_1.LSFLSCON as LSFLSCON201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on LIS_TINI.LICODLIS=link_2_1.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and LIS_TINI.LICODLIS=link_2_1.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALLIS
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALLIS)
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALLIS = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALLIS = space(3)
      endif
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LIUNIMIS
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LIUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_LIUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_LIUNIMIS))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LIUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LIUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oLIUNIMIS_2_12'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSMA1QUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LIUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_LIUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_LIUNIMIS)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LIUNIMIS = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_LIUNIMIS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUMLIS(.w_CODART, IIF(.w_FLSERG='S', '***', .w_LIUNIMIS), .w_UNMIS1, .w_UNMIS2) AND NOT EMPTY(.w_LIUNIMIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_LIUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LIUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESLIS_2_6.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_2_6.value=this.w_DESLIS
      replace t_DESLIS with this.oPgFrm.Page1.oPag.oDESLIS_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLIUNIMIS_2_12.value==this.w_LIUNIMIS)
      this.oPgFrm.Page1.oPag.oLIUNIMIS_2_12.value=this.w_LIUNIMIS
      replace t_LIUNIMIS with this.oPgFrm.Page1.oPag.oLIUNIMIS_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLICODLIS_2_1.value==this.w_LICODLIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLICODLIS_2_1.value=this.w_LICODLIS
      replace t_LICODLIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLICODLIS_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIDATATT_2_4.value==this.w_LIDATATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIDATATT_2_4.value=this.w_LIDATATT
      replace t_LIDATATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIDATATT_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIDATDIS_2_5.value==this.w_LIDATDIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIDATDIS_2_5.value=this.w_LIDATDIS
      replace t_LIDATDIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIDATDIS_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSIMVAL_2_8.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSIMVAL_2_8.value=this.w_SIMVAL
      replace t_SIMVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSIMVAL_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVALIS_2_10.RadioValue()==this.w_IVALIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVALIS_2_10.SetRadio()
      replace t_IVALIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVALIS_2_10.value
    endif
    cp_SetControlsValueExtFlds(this,'LIS_TINI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_LIDATATT) and (NOT EMPTY(.w_LICODLIS)) and (.w_LICODLIS<>space(5) AND NOT EMPTY(.w_LIDATDIS))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIDATATT_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   (empty(.w_LIDATDIS) or not(EMPTY(.w_LIDATDIS) OR .w_LIDATDIS>=.w_LIDATATT)) and (NOT EMPTY(.w_LICODLIS) AND NOT EMPTY(.w_LIDATATT)) and (.w_LICODLIS<>space(5) AND NOT EMPTY(.w_LIDATDIS))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIDATDIS_2_5
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Data di fine validit� non valida")
        case   (empty(.w_LIUNIMIS) or not(CHKUMLIS(.w_CODART, IIF(.w_FLSERG='S', '***', .w_LIUNIMIS), .w_UNMIS1, .w_UNMIS2) AND NOT EMPTY(.w_LIUNIMIS))) and (.w_LICODLIS<>space(5) AND NOT EMPTY(.w_LIDATDIS))
          .oNewFocus=.oPgFrm.Page1.oPag.oLIUNIMIS_2_12
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
      endcase
      i_bRes = i_bRes .and. .GSMA_MLG.CheckForm()
      if .w_LICODLIS<>space(5) AND NOT EMPTY(.w_LIDATDIS)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LICODART = this.w_LICODART
    this.o_LICODLIS = this.w_LICODLIS
    * --- GSMA_MLG : Depends On
    this.GSMA_MLG.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_LICODLIS<>space(5) AND NOT EMPTY(t_LIDATDIS))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_LICODLIS=space(5)
      .w_DTINVA=ctod("  /  /  ")
      .w_DTFINV=ctod("  /  /  ")
      .w_LIDATATT=ctod("  /  /  ")
      .w_LIDATDIS=ctod("  /  /  ")
      .w_DESLIS=space(40)
      .w_VALLIS=space(3)
      .w_SIMVAL=space(5)
      .w_IVALIS=space(1)
      .w_FLSCO=space(1)
      .w_LIUNIMIS=space(3)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_LICODLIS))
        .link_2_1('Full')
      endif
      .DoRTCalc(4,5,.f.)
        .w_LIDATATT = .w_DTINVA
        .w_LIDATDIS = IIF(.w_DTFINV<.w_LIDATATT AND NOT EMPTY(.w_DTFINV), .w_LIDATATT, .w_DTFINV)
      .DoRTCalc(8,9,.f.)
      if not(empty(.w_VALLIS))
        .link_2_7('Full')
      endif
      .DoRTCalc(10,12,.f.)
        .w_LIUNIMIS = .w_UNMIS1
      .DoRTCalc(13,13,.f.)
      if not(empty(.w_LIUNIMIS))
        .link_2_12('Full')
      endif
    endwith
    this.DoRTCalc(14,17,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_LICODLIS = t_LICODLIS
    this.w_DTINVA = t_DTINVA
    this.w_DTFINV = t_DTFINV
    this.w_LIDATATT = t_LIDATATT
    this.w_LIDATDIS = t_LIDATDIS
    this.w_DESLIS = t_DESLIS
    this.w_VALLIS = t_VALLIS
    this.w_SIMVAL = t_SIMVAL
    this.w_IVALIS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVALIS_2_10.RadioValue(.t.)
    this.w_FLSCO = t_FLSCO
    this.w_LIUNIMIS = t_LIUNIMIS
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_LICODLIS with this.w_LICODLIS
    replace t_DTINVA with this.w_DTINVA
    replace t_DTFINV with this.w_DTFINV
    replace t_LIDATATT with this.w_LIDATATT
    replace t_LIDATDIS with this.w_LIDATDIS
    replace t_DESLIS with this.w_DESLIS
    replace t_VALLIS with this.w_VALLIS
    replace t_SIMVAL with this.w_SIMVAL
    replace t_IVALIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIVALIS_2_10.ToRadio()
    replace t_FLSCO with this.w_FLSCO
    replace t_LIUNIMIS with this.w_LIUNIMIS
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsma_mliPag1 as StdContainer
  Width  = 654
  height = 229
  stdWidth  = 654
  stdheight = 229
  resizeXpos=630
  resizeYpos=124
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=11, top=0, width=367,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="LICODLIS",Label1="Listino",Field2="LIDATATT",Label2="Valido dal",Field3="LIDATDIS",Label3="Fino al",Field4="SIMVAL",Label4="Valuta",Field5="IVALIS",Label5="IVA",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 166941062

  add object oStr_1_3 as StdString with uid="EHMRBAQZCR",Visible=.t., Left=14, Top=175,;
    Alignment=1, Width=121, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="LOBUONYXWP",Visible=.t., Left=14, Top=201,;
    Alignment=1, Width=121, Height=18,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsma_mlg",lower(this.oContained.GSMA_MLG.class))=0
        this.oContained.GSMA_MLG.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=19,;
    width=363+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=20,width=362+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='LISTINI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESLIS_2_6.Refresh()
      this.Parent.oLIUNIMIS_2_12.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='LISTINI'
        oDropInto=this.oBodyCol.oRow.oLICODLIS_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESLIS_2_6 as StdTrsField with uid="KZZKOHCOFW",rtseq=8,rtrep=.t.,;
    cFormVar="w_DESLIS",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione del listino",;
    HelpContextID = 186056246,;
    cTotal="", bFixedPos=.t., cQueryName = "DESLIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=212, Left=138, Top=176, InputMask=replicate('X',40)

  add object oLinkPC_2_9 as stdDynamicChildContainer with uid="RWJMUQGOGN",bOnScreen=.t.,width=266,height=228,;
   left=385, top=0;


  func oLinkPC_2_9.mCond()
    with this.Parent.oContained
      return (not empty(.w_LICODLIS) and not empty(.w_LIDATDIS) and not empty(.w_LIDATATT))
    endwith
  endfunc

  add object oLIUNIMIS_2_12 as StdTrsField with uid="HVYCJMKYCD",rtseq=13,rtrep=.t.,;
    cFormVar="w_LIUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura a cui il listino si riferisce",;
    HelpContextID = 182902007,;
    cTotal="", bFixedPos=.t., cQueryName = "LIUNIMIS",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=138, Top=201, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_LIUNIMIS"

  func oLIUNIMIS_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oLIUNIMIS_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oLIUNIMIS_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oLIUNIMIS_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSMA1QUM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oLIUNIMIS_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_LIUNIMIS
    i_obj.ecpSave()
  endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsma_mliBodyRow as CPBodyRowCnt
  Width=353
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oLICODLIS_2_1 as StdTrsField with uid="DVNPISVNVM",rtseq=3,rtrep=.t.,;
    cFormVar="w_LICODLIS",value=space(5),;
    ToolTipText = "Codice listino",;
    HelpContextID = 204930295,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LICODLIS"

  func oLICODLIS_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oLICODLIS_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLICODLIS_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLICODLIS_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMA1MVM.LISTINI_VZM',this.parent.oContained
  endproc
  proc oLICODLIS_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_LICODLIS
    i_obj.ecpSave()
  endproc

  add object oLIDATATT_2_4 as StdTrsField with uid="TCWTPGKGPZ",rtseq=6,rtrep=.t.,;
    cFormVar="w_LIDATATT",value=ctod("  /  /  "),;
    ToolTipText = "Data di entrata in vigore",;
    HelpContextID = 105180406,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=60, Top=0

  func oLIDATATT_2_4.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_LICODLIS))
    endwith
  endfunc

  add object oLIDATDIS_2_5 as StdTrsField with uid="NQHJPHDYZZ",rtseq=7,rtrep=.t.,;
    cFormVar="w_LIDATDIS",value=ctod("  /  /  "),;
    ToolTipText = "Data di fine attivit� del listino",;
    HelpContextID = 54848759,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Data di fine validit� non valida",;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=143, Top=0

  func oLIDATDIS_2_5.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_LICODLIS) AND NOT EMPTY(.w_LIDATATT))
    endwith
  endfunc

  func oLIDATDIS_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_LIDATDIS) OR .w_LIDATDIS>=.w_LIDATATT)
    endwith
    return bRes
  endfunc

  add object oSIMVAL_2_8 as StdTrsField with uid="VAMNYTSOLH",rtseq=10,rtrep=.t.,;
    cFormVar="w_SIMVAL",value=space(5),enabled=.f.,;
    HelpContextID = 60859174,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=226, Top=0, InputMask=replicate('X',5)

  add object oIVALIS_2_10 as StdTrsCombo with uid="VSWCWLAQXC",rtrep=.t.,;
    cFormVar="w_IVALIS", RowSource=""+"Lordo,"+"Netto" , enabled=.f.,;
    ToolTipText = "Se attivo: listino comprensivo di IVA",;
    HelpContextID = 185986950,;
    Height=22, Width=67, Left=281, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oIVALIS_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IVALIS,&i_cF..t_IVALIS),this.value)
    return(iif(xVal =1,'L',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oIVALIS_2_10.GetRadio()
    this.Parent.oContained.w_IVALIS = this.RadioValue()
    return .t.
  endfunc

  func oIVALIS_2_10.ToRadio()
    this.Parent.oContained.w_IVALIS=trim(this.Parent.oContained.w_IVALIS)
    return(;
      iif(this.Parent.oContained.w_IVALIS=='L',1,;
      iif(this.Parent.oContained.w_IVALIS=='N',2,;
      0)))
  endfunc

  func oIVALIS_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oLICODLIS_2_1.When()
    return(.t.)
  proc oLICODLIS_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oLICODLIS_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_mli','LIS_TINI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LICODART=LIS_TINI.LICODART";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
