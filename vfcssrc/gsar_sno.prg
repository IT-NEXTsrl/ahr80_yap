* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_sno                                                        *
*              Stampa nominativi                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-15                                                      *
* Last revis.: 2010-06-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_sno",oParentObject))

* --- Class definition
define class tgsar_sno as StdForm
  Top    = 14
  Left   = 11

  * --- Standard Properties
  Width  = 596
  Height = 387+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-06-21"
  HelpContextID=93751447
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=54

  * --- Constant Properties
  _IDX = 0
  GRU_NOMI_IDX = 0
  ORI_NOMI_IDX = 0
  cpgroups_IDX = 0
  ZONE_IDX = 0
  AGENTI_IDX = 0
  CAT_ATTR_IDX = 0
  GRU_PRIO_IDX = 0
  NAZIONI_IDX = 0
  OFF_NOMI_IDX = 0
  RUO_CONT_IDX = 0
  cpusers_IDX = 0
  cPrg = "gsar_sno"
  cComment = "Stampa nominativi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SNCODIC1 = space(20)
  w_STDESNO1 = space(40)
  w_SNCODIC2 = space(20)
  w_STDESNO2 = space(40)
  w_SNTIPNOM = space(1)
  w_SNTIPNOM = space(1)
  w_DATASTAM = ctod('  /  /  ')
  o_DATASTAM = ctod('  /  /  ')
  w_SNPROVIN = space(2)
  w_SNPROVIN = space(2)
  w_SNNAZION = space(3)
  w_DESNAZ = space(35)
  w_SNCODPRI = space(6)
  w_DESPRIO = space(35)
  w_SNCODGRU = space(5)
  w_DESCGN = space(35)
  w_SNCODORI = space(5)
  w_DESORI = space(35)
  w_SNCODUTE = 0
  w_DESOPE = space(35)
  w_SNCODZON = space(3)
  w_DESZONE = space(35)
  w_SNCODAGE = space(5)
  w_DESAGE = space(35)
  w_SELCON = space(2)
  w_SELATT = space(2)
  w_OBSOLETI = space(1)
  w_SNCODAT0 = space(10)
  o_SNCODAT0 = space(10)
  w_DESATT0 = space(35)
  w_SNCODAT1 = space(10)
  o_SNCODAT1 = space(10)
  w_DESATT1 = space(35)
  w_SNCODAT2 = space(10)
  o_SNCODAT2 = space(10)
  w_DESATT2 = space(35)
  w_SNCODAT3 = space(10)
  o_SNCODAT3 = space(10)
  w_DESATT3 = space(35)
  w_SNCODAT4 = space(10)
  o_SNCODAT4 = space(10)
  w_DESATT4 = space(35)
  w_SNCODAT5 = space(10)
  o_SNCODAT5 = space(10)
  w_DESATT5 = space(35)
  w_SNCODAT6 = space(10)
  o_SNCODAT6 = space(10)
  w_DESATT6 = space(35)
  w_SNCODAT7 = space(10)
  o_SNCODAT7 = space(10)
  w_DESATT7 = space(35)
  w_SNCODAT8 = space(10)
  o_SNCODAT8 = space(10)
  w_DESATT8 = space(35)
  w_SNCODAT9 = space(10)
  w_DESATT9 = space(35)
  w_SNCDRUO = space(10)
  w_DESRUO = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_SNNUMINI = 0
  w_SNNUMFIN = 0
  w_SNTESTCO = space(20)
  w_SNTESTAT = space(20)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_snoPag1","gsar_sno",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Filtri e opzioni di stampa")
      .Pages(2).addobject("oPag","tgsar_snoPag2","gsar_sno",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Attributi e contatti")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSNCODIC1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='GRU_NOMI'
    this.cWorkTables[2]='ORI_NOMI'
    this.cWorkTables[3]='cpgroups'
    this.cWorkTables[4]='ZONE'
    this.cWorkTables[5]='AGENTI'
    this.cWorkTables[6]='CAT_ATTR'
    this.cWorkTables[7]='GRU_PRIO'
    this.cWorkTables[8]='NAZIONI'
    this.cWorkTables[9]='OFF_NOMI'
    this.cWorkTables[10]='RUO_CONT'
    this.cWorkTables[11]='cpusers'
    return(this.OpenAllTables(11))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsar_sno
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SNCODIC1=space(20)
      .w_STDESNO1=space(40)
      .w_SNCODIC2=space(20)
      .w_STDESNO2=space(40)
      .w_SNTIPNOM=space(1)
      .w_SNTIPNOM=space(1)
      .w_DATASTAM=ctod("  /  /  ")
      .w_SNPROVIN=space(2)
      .w_SNPROVIN=space(2)
      .w_SNNAZION=space(3)
      .w_DESNAZ=space(35)
      .w_SNCODPRI=space(6)
      .w_DESPRIO=space(35)
      .w_SNCODGRU=space(5)
      .w_DESCGN=space(35)
      .w_SNCODORI=space(5)
      .w_DESORI=space(35)
      .w_SNCODUTE=0
      .w_DESOPE=space(35)
      .w_SNCODZON=space(3)
      .w_DESZONE=space(35)
      .w_SNCODAGE=space(5)
      .w_DESAGE=space(35)
      .w_SELCON=space(2)
      .w_SELATT=space(2)
      .w_OBSOLETI=space(1)
      .w_SNCODAT0=space(10)
      .w_DESATT0=space(35)
      .w_SNCODAT1=space(10)
      .w_DESATT1=space(35)
      .w_SNCODAT2=space(10)
      .w_DESATT2=space(35)
      .w_SNCODAT3=space(10)
      .w_DESATT3=space(35)
      .w_SNCODAT4=space(10)
      .w_DESATT4=space(35)
      .w_SNCODAT5=space(10)
      .w_DESATT5=space(35)
      .w_SNCODAT6=space(10)
      .w_DESATT6=space(35)
      .w_SNCODAT7=space(10)
      .w_DESATT7=space(35)
      .w_SNCODAT8=space(10)
      .w_DESATT8=space(35)
      .w_SNCODAT9=space(10)
      .w_DESATT9=space(35)
      .w_SNCDRUO=space(10)
      .w_DESRUO=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_SNNUMINI=0
      .w_SNNUMFIN=0
      .w_SNTESTCO=space(20)
      .w_SNTESTAT=space(20)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_SNCODIC1))
          .link_1_2('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_SNCODIC2))
          .link_1_5('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_SNTIPNOM = 'X'
        .w_SNTIPNOM = 'X'
        .w_DATASTAM = i_datsys
        .DoRTCalc(8,10,.f.)
        if not(empty(.w_SNNAZION))
          .link_1_15('Full')
        endif
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_SNCODPRI))
          .link_1_18('Full')
        endif
        .DoRTCalc(13,14,.f.)
        if not(empty(.w_SNCODGRU))
          .link_1_21('Full')
        endif
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_SNCODORI))
          .link_1_24('Full')
        endif
          .DoRTCalc(17,17,.f.)
        .w_SNCODUTE = IIF(UPPER(g_APPLICATION)="AD HOC ENTERPRISE", 0 , IIF(IsAlt(), 0, i_CODUTE ))
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_SNCODUTE))
          .link_1_27('Full')
        endif
        .DoRTCalc(19,20,.f.)
        if not(empty(.w_SNCODZON))
          .link_1_30('Full')
        endif
        .DoRTCalc(21,22,.f.)
        if not(empty(.w_SNCODAGE))
          .link_1_33('Full')
        endif
          .DoRTCalc(23,23,.f.)
        .w_SELCON = 'S'
        .w_SELATT = 'S'
        .w_OBSOLETI = 'N'
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_SNCODAT0))
          .link_2_2('Full')
        endif
          .DoRTCalc(28,28,.f.)
        .w_SNCODAT1 = SPACE(10)
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_SNCODAT1))
          .link_2_4('Full')
        endif
          .DoRTCalc(30,30,.f.)
        .w_SNCODAT2 = SPACE(10)
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_SNCODAT2))
          .link_2_6('Full')
        endif
          .DoRTCalc(32,32,.f.)
        .w_SNCODAT3 = SPACE(10)
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_SNCODAT3))
          .link_2_8('Full')
        endif
          .DoRTCalc(34,34,.f.)
        .w_SNCODAT4 = SPACE(10)
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_SNCODAT4))
          .link_2_10('Full')
        endif
          .DoRTCalc(36,36,.f.)
        .w_SNCODAT5 = SPACE(10)
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_SNCODAT5))
          .link_2_12('Full')
        endif
          .DoRTCalc(38,38,.f.)
        .w_SNCODAT6 = SPACE(10)
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_SNCODAT6))
          .link_2_14('Full')
        endif
          .DoRTCalc(40,40,.f.)
        .w_SNCODAT7 = SPACE(10)
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_SNCODAT7))
          .link_2_16('Full')
        endif
          .DoRTCalc(42,42,.f.)
        .w_SNCODAT8 = SPACE(10)
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_SNCODAT8))
          .link_2_18('Full')
        endif
          .DoRTCalc(44,44,.f.)
        .w_SNCODAT9 = SPACE(10)
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_SNCODAT9))
          .link_2_20('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .DoRTCalc(46,47,.f.)
        if not(empty(.w_SNCDRUO))
          .link_2_23('Full')
        endif
          .DoRTCalc(48,48,.f.)
        .w_OBTEST = .w_DATASTAM
          .DoRTCalc(50,52,.f.)
        .w_SNTESTCO = IIF(.w_SELCON='S', SPACE(20),'xzxzxzxzxzxzxzx')
        .w_SNTESTAT = IIF(.w_SELATT='S', SPACE(20),'xzxzxzxzxzxzxzx')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,28,.t.)
        if .o_SNCODAT0<>.w_SNCODAT0
            .w_SNCODAT1 = SPACE(10)
          .link_2_4('Full')
        endif
        .DoRTCalc(30,30,.t.)
        if .o_SNCODAT1<>.w_SNCODAT1
            .w_SNCODAT2 = SPACE(10)
          .link_2_6('Full')
        endif
        .DoRTCalc(32,32,.t.)
        if .o_SNCODAT2<>.w_SNCODAT2
            .w_SNCODAT3 = SPACE(10)
          .link_2_8('Full')
        endif
        .DoRTCalc(34,34,.t.)
        if .o_SNCODAT3<>.w_SNCODAT3
            .w_SNCODAT4 = SPACE(10)
          .link_2_10('Full')
        endif
        .DoRTCalc(36,36,.t.)
        if .o_SNCODAT4<>.w_SNCODAT4
            .w_SNCODAT5 = SPACE(10)
          .link_2_12('Full')
        endif
        .DoRTCalc(38,38,.t.)
        if .o_SNCODAT5<>.w_SNCODAT5
            .w_SNCODAT6 = SPACE(10)
          .link_2_14('Full')
        endif
        .DoRTCalc(40,40,.t.)
        if .o_SNCODAT6<>.w_SNCODAT6
            .w_SNCODAT7 = SPACE(10)
          .link_2_16('Full')
        endif
        .DoRTCalc(42,42,.t.)
        if .o_SNCODAT7<>.w_SNCODAT7
            .w_SNCODAT8 = SPACE(10)
          .link_2_18('Full')
        endif
        .DoRTCalc(44,44,.t.)
        if .o_SNCODAT8<>.w_SNCODAT8
            .w_SNCODAT9 = SPACE(10)
          .link_2_20('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .DoRTCalc(46,48,.t.)
        if .o_DATASTAM<>.w_DATASTAM
            .w_OBTEST = .w_DATASTAM
        endif
        .DoRTCalc(50,52,.t.)
            .w_SNTESTCO = IIF(.w_SELCON='S', SPACE(20),'xzxzxzxzxzxzxzx')
            .w_SNTESTAT = IIF(.w_SELATT='S', SPACE(20),'xzxzxzxzxzxzxzx')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSNTIPNOM_1_8.enabled = this.oPgFrm.Page1.oPag.oSNTIPNOM_1_8.mCond()
    this.oPgFrm.Page1.oPag.oSNTIPNOM_1_9.enabled = this.oPgFrm.Page1.oPag.oSNTIPNOM_1_9.mCond()
    this.oPgFrm.Page1.oPag.oSNPROVIN_1_12.enabled = this.oPgFrm.Page1.oPag.oSNPROVIN_1_12.mCond()
    this.oPgFrm.Page1.oPag.oSNPROVIN_1_13.enabled = this.oPgFrm.Page1.oPag.oSNPROVIN_1_13.mCond()
    this.oPgFrm.Page2.oPag.oSNCODAT1_2_4.enabled = this.oPgFrm.Page2.oPag.oSNCODAT1_2_4.mCond()
    this.oPgFrm.Page2.oPag.oSNCODAT2_2_6.enabled = this.oPgFrm.Page2.oPag.oSNCODAT2_2_6.mCond()
    this.oPgFrm.Page2.oPag.oSNCODAT3_2_8.enabled = this.oPgFrm.Page2.oPag.oSNCODAT3_2_8.mCond()
    this.oPgFrm.Page2.oPag.oSNCODAT4_2_10.enabled = this.oPgFrm.Page2.oPag.oSNCODAT4_2_10.mCond()
    this.oPgFrm.Page2.oPag.oSNCODAT5_2_12.enabled = this.oPgFrm.Page2.oPag.oSNCODAT5_2_12.mCond()
    this.oPgFrm.Page2.oPag.oSNCODAT6_2_14.enabled = this.oPgFrm.Page2.oPag.oSNCODAT6_2_14.mCond()
    this.oPgFrm.Page2.oPag.oSNCODAT7_2_16.enabled = this.oPgFrm.Page2.oPag.oSNCODAT7_2_16.mCond()
    this.oPgFrm.Page2.oPag.oSNCODAT8_2_18.enabled = this.oPgFrm.Page2.oPag.oSNCODAT8_2_18.mCond()
    this.oPgFrm.Page2.oPag.oSNCODAT9_2_20.enabled = this.oPgFrm.Page2.oPag.oSNCODAT9_2_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(IsAlt())
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Attributi e contatti"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oSNTIPNOM_1_8.visible=!this.oPgFrm.Page1.oPag.oSNTIPNOM_1_8.mHide()
    this.oPgFrm.Page1.oPag.oSNTIPNOM_1_9.visible=!this.oPgFrm.Page1.oPag.oSNTIPNOM_1_9.mHide()
    this.oPgFrm.Page1.oPag.oSNPROVIN_1_12.visible=!this.oPgFrm.Page1.oPag.oSNPROVIN_1_12.mHide()
    this.oPgFrm.Page1.oPag.oSNPROVIN_1_13.visible=!this.oPgFrm.Page1.oPag.oSNPROVIN_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oSNCODZON_1_30.visible=!this.oPgFrm.Page1.oPag.oSNCODZON_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDESZONE_1_31.visible=!this.oPgFrm.Page1.oPag.oDESZONE_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oSNCODAGE_1_33.visible=!this.oPgFrm.Page1.oPag.oSNCODAGE_1_33.mHide()
    this.oPgFrm.Page1.oPag.oDESAGE_1_34.visible=!this.oPgFrm.Page1.oPag.oDESAGE_1_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SNCODIC1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODIC1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_SNCODIC1)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_SNCODIC1))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODIC1)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODIC1) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oSNCODIC1_1_2'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODIC1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_SNCODIC1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_SNCODIC1)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODIC1 = NVL(_Link_.NOCODICE,space(20))
      this.w_STDESNO1 = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODIC1 = space(20)
      endif
      this.w_STDESNO1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_SNCODIC2)) OR  (UPPER(.w_SNCODIC1)<= UPPER(.w_SNCODIC2))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice nominativo iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_SNCODIC1 = space(20)
        this.w_STDESNO1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODIC1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODIC2
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODIC2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_SNCODIC2)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_SNCODIC2))
          select NOCODICE,NODESCRI,NODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODIC2)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODIC2) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oSNCODIC2_1_5'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODIC2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_SNCODIC2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_SNCODIC2)
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODIC2 = NVL(_Link_.NOCODICE,space(20))
      this.w_STDESNO2 = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODIC2 = space(20)
      endif
      this.w_STDESNO2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((UPPER(.w_SNCODIC1) <= UPPER(.w_SNCODIC2)) or (empty(.w_SNCODIC2))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice nominativo iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_SNCODIC2 = space(20)
        this.w_STDESNO2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODIC2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNNAZION
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_SNNAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_SNNAZION))
          select NACODNAZ,NADESNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNNAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNNAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oSNNAZION_1_15'),i_cWhere,'',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_SNNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_SNNAZION)
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_DESNAZ = NVL(_Link_.NADESNAZ,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNNAZION = space(3)
      endif
      this.w_DESNAZ = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODPRI
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODPRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_SNCODPRI)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_SNCODPRI))
          select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODPRI)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODPRI) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oSNCODPRI_1_18'),i_cWhere,'',"Gruppi priorit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODPRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_SNCODPRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_SNCODPRI)
            select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODPRI = NVL(_Link_.GPCODICE,space(6))
      this.w_DESPRIO = NVL(_Link_.GPDESCRI,space(35))
      this.w_SNNUMINI = NVL(_Link_.GPNUMINI,0)
      this.w_SNNUMFIN = NVL(_Link_.GPNUMFIN,0)
    else
      if i_cCtrl<>'Load'
        this.w_SNCODPRI = space(6)
      endif
      this.w_DESPRIO = space(35)
      this.w_SNNUMINI = 0
      this.w_SNNUMFIN = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODPRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODGRU
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_NOMI_IDX,3]
    i_lTable = "GRU_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2], .t., this.GRU_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GNCODICE like "+cp_ToStrODBC(trim(this.w_SNCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GNCODICE',trim(this.w_SNCODGRU))
          select GNCODICE,GNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODGRU)==trim(_Link_.GNCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRU_NOMI','*','GNCODICE',cp_AbsName(oSource.parent,'oSNCODGRU_1_21'),i_cWhere,'',"Gruppi nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',oSource.xKey(1))
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(this.w_SNCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',this.w_SNCODGRU)
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODGRU = NVL(_Link_.GNCODICE,space(5))
      this.w_DESCGN = NVL(_Link_.GNDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODGRU = space(5)
      endif
      this.w_DESCGN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.GNCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODORI
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ORI_NOMI_IDX,3]
    i_lTable = "ORI_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2], .t., this.ORI_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ORI_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ONCODICE like "+cp_ToStrODBC(trim(this.w_SNCODORI)+"%");

          i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ONCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ONCODICE',trim(this.w_SNCODORI))
          select ONCODICE,ONDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ONCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODORI)==trim(_Link_.ONCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODORI) and !this.bDontReportError
            deferred_cp_zoom('ORI_NOMI','*','ONCODICE',cp_AbsName(oSource.parent,'oSNCODORI_1_24'),i_cWhere,'',"Codici origine nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',oSource.xKey(1))
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(this.w_SNCODORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',this.w_SNCODORI)
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODORI = NVL(_Link_.ONCODICE,space(5))
      this.w_DESORI = NVL(_Link_.ONDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODORI = space(5)
      endif
      this.w_DESORI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.ONCODICE,1)
      cp_ShowWarn(i_cKey,this.ORI_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODUTE
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_SNCODUTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_SNCODUTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_SNCODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oSNCODUTE_1_27'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_SNCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_SNCODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODUTE = NVL(_Link_.code,0)
      this.w_DESOPE = NVL(_Link_.name,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODUTE = 0
      endif
      this.w_DESOPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODZON
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_SNCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_SNCODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oSNCODZON_1_30'),i_cWhere,'',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_SNCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_SNCODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZONE = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODZON = space(3)
      endif
      this.w_DESZONE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODAGE
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_SNCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_SNCODAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_SNCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_SNCODAGE)+"%");

            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SNCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oSNCODAGE_1_33'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_SNCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_SNCODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODAT0
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODAT0) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SNCODAT0)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SNCODAT0))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODAT0)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODAT0) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSNCODAT0_2_2'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODAT0)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SNCODAT0);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SNCODAT0)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODAT0 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT0 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODAT0 = space(10)
      endif
      this.w_DESATT0 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODAT0 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODAT1
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODAT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SNCODAT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SNCODAT1))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODAT1)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODAT1) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSNCODAT1_2_4'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODAT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SNCODAT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SNCODAT1)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODAT1 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT1 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODAT1 = space(10)
      endif
      this.w_DESATT1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODAT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODAT2
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODAT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SNCODAT2)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SNCODAT2))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODAT2)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODAT2) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSNCODAT2_2_6'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODAT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SNCODAT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SNCODAT2)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODAT2 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT2 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODAT2 = space(10)
      endif
      this.w_DESATT2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODAT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODAT3
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODAT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SNCODAT3)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SNCODAT3))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODAT3)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODAT3) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSNCODAT3_2_8'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODAT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SNCODAT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SNCODAT3)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODAT3 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT3 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODAT3 = space(10)
      endif
      this.w_DESATT3 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODAT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODAT4
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODAT4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SNCODAT4)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SNCODAT4))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODAT4)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODAT4) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSNCODAT4_2_10'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODAT4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SNCODAT4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SNCODAT4)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODAT4 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT4 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODAT4 = space(10)
      endif
      this.w_DESATT4 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODAT4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODAT5
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODAT5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SNCODAT5)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SNCODAT5))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODAT5)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODAT5) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSNCODAT5_2_12'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODAT5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SNCODAT5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SNCODAT5)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODAT5 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT5 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODAT5 = space(10)
      endif
      this.w_DESATT5 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODAT5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODAT6
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODAT6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SNCODAT6)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SNCODAT6))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODAT6)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODAT6) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSNCODAT6_2_14'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODAT6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SNCODAT6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SNCODAT6)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODAT6 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT6 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODAT6 = space(10)
      endif
      this.w_DESATT6 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODAT6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODAT7
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODAT7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SNCODAT7)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SNCODAT7))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODAT7)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODAT7) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSNCODAT7_2_16'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODAT7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SNCODAT7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SNCODAT7)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODAT7 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT7 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODAT7 = space(10)
      endif
      this.w_DESATT7 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODAT7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODAT8
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODAT8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SNCODAT8)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SNCODAT8))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODAT8)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODAT8) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSNCODAT8_2_18'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODAT8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SNCODAT8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SNCODAT8)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODAT8 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT8 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODAT8 = space(10)
      endif
      this.w_DESATT8 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODAT8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODAT9
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODAT9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SNCODAT9)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SNCODAT9))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODAT9)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODAT9) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSNCODAT9_2_20'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODAT9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SNCODAT9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SNCODAT9)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODAT9 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT9 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODAT9 = space(10)
      endif
      this.w_DESATT9 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODAT9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCDRUO
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RUO_CONT_IDX,3]
    i_lTable = "RUO_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2], .t., this.RUO_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCDRUO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ARC',True,'RUO_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RCCODICE like "+cp_ToStrODBC(trim(this.w_SNCDRUO)+"%");

          i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RCCODICE',trim(this.w_SNCDRUO))
          select RCCODICE,RCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCDRUO)==trim(_Link_.RCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCDRUO) and !this.bDontReportError
            deferred_cp_zoom('RUO_CONT','*','RCCODICE',cp_AbsName(oSource.parent,'oSNCDRUO_2_23'),i_cWhere,'GSAR_ARC',"Ruolo contatti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',oSource.xKey(1))
            select RCCODICE,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCDRUO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(this.w_SNCDRUO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',this.w_SNCDRUO)
            select RCCODICE,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCDRUO = NVL(_Link_.RCCODICE,space(10))
      this.w_DESRUO = NVL(_Link_.RCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCDRUO = space(10)
      endif
      this.w_DESRUO = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2])+'\'+cp_ToStr(_Link_.RCCODICE,1)
      cp_ShowWarn(i_cKey,this.RUO_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCDRUO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSNCODIC1_1_2.value==this.w_SNCODIC1)
      this.oPgFrm.Page1.oPag.oSNCODIC1_1_2.value=this.w_SNCODIC1
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDESNO1_1_3.value==this.w_STDESNO1)
      this.oPgFrm.Page1.oPag.oSTDESNO1_1_3.value=this.w_STDESNO1
    endif
    if not(this.oPgFrm.Page1.oPag.oSNCODIC2_1_5.value==this.w_SNCODIC2)
      this.oPgFrm.Page1.oPag.oSNCODIC2_1_5.value=this.w_SNCODIC2
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDESNO2_1_6.value==this.w_STDESNO2)
      this.oPgFrm.Page1.oPag.oSTDESNO2_1_6.value=this.w_STDESNO2
    endif
    if not(this.oPgFrm.Page1.oPag.oSNTIPNOM_1_8.RadioValue()==this.w_SNTIPNOM)
      this.oPgFrm.Page1.oPag.oSNTIPNOM_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSNTIPNOM_1_9.RadioValue()==this.w_SNTIPNOM)
      this.oPgFrm.Page1.oPag.oSNTIPNOM_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATASTAM_1_10.value==this.w_DATASTAM)
      this.oPgFrm.Page1.oPag.oDATASTAM_1_10.value=this.w_DATASTAM
    endif
    if not(this.oPgFrm.Page1.oPag.oSNPROVIN_1_12.value==this.w_SNPROVIN)
      this.oPgFrm.Page1.oPag.oSNPROVIN_1_12.value=this.w_SNPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSNPROVIN_1_13.value==this.w_SNPROVIN)
      this.oPgFrm.Page1.oPag.oSNPROVIN_1_13.value=this.w_SNPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSNNAZION_1_15.value==this.w_SNNAZION)
      this.oPgFrm.Page1.oPag.oSNNAZION_1_15.value=this.w_SNNAZION
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNAZ_1_16.value==this.w_DESNAZ)
      this.oPgFrm.Page1.oPag.oDESNAZ_1_16.value=this.w_DESNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oSNCODPRI_1_18.value==this.w_SNCODPRI)
      this.oPgFrm.Page1.oPag.oSNCODPRI_1_18.value=this.w_SNCODPRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRIO_1_19.value==this.w_DESPRIO)
      this.oPgFrm.Page1.oPag.oDESPRIO_1_19.value=this.w_DESPRIO
    endif
    if not(this.oPgFrm.Page1.oPag.oSNCODGRU_1_21.value==this.w_SNCODGRU)
      this.oPgFrm.Page1.oPag.oSNCODGRU_1_21.value=this.w_SNCODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCGN_1_22.value==this.w_DESCGN)
      this.oPgFrm.Page1.oPag.oDESCGN_1_22.value=this.w_DESCGN
    endif
    if not(this.oPgFrm.Page1.oPag.oSNCODORI_1_24.value==this.w_SNCODORI)
      this.oPgFrm.Page1.oPag.oSNCODORI_1_24.value=this.w_SNCODORI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESORI_1_25.value==this.w_DESORI)
      this.oPgFrm.Page1.oPag.oDESORI_1_25.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page1.oPag.oSNCODUTE_1_27.value==this.w_SNCODUTE)
      this.oPgFrm.Page1.oPag.oSNCODUTE_1_27.value=this.w_SNCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESOPE_1_28.value==this.w_DESOPE)
      this.oPgFrm.Page1.oPag.oDESOPE_1_28.value=this.w_DESOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oSNCODZON_1_30.value==this.w_SNCODZON)
      this.oPgFrm.Page1.oPag.oSNCODZON_1_30.value=this.w_SNCODZON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESZONE_1_31.value==this.w_DESZONE)
      this.oPgFrm.Page1.oPag.oDESZONE_1_31.value=this.w_DESZONE
    endif
    if not(this.oPgFrm.Page1.oPag.oSNCODAGE_1_33.value==this.w_SNCODAGE)
      this.oPgFrm.Page1.oPag.oSNCODAGE_1_33.value=this.w_SNCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_34.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_34.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oSELCON_1_36.RadioValue()==this.w_SELCON)
      this.oPgFrm.Page1.oPag.oSELCON_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELATT_1_37.RadioValue()==this.w_SELATT)
      this.oPgFrm.Page1.oPag.oSELATT_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOBSOLETI_1_38.RadioValue()==this.w_OBSOLETI)
      this.oPgFrm.Page1.oPag.oOBSOLETI_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODAT0_2_2.value==this.w_SNCODAT0)
      this.oPgFrm.Page2.oPag.oSNCODAT0_2_2.value=this.w_SNCODAT0
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT0_2_3.value==this.w_DESATT0)
      this.oPgFrm.Page2.oPag.oDESATT0_2_3.value=this.w_DESATT0
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODAT1_2_4.value==this.w_SNCODAT1)
      this.oPgFrm.Page2.oPag.oSNCODAT1_2_4.value=this.w_SNCODAT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT1_2_5.value==this.w_DESATT1)
      this.oPgFrm.Page2.oPag.oDESATT1_2_5.value=this.w_DESATT1
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODAT2_2_6.value==this.w_SNCODAT2)
      this.oPgFrm.Page2.oPag.oSNCODAT2_2_6.value=this.w_SNCODAT2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT2_2_7.value==this.w_DESATT2)
      this.oPgFrm.Page2.oPag.oDESATT2_2_7.value=this.w_DESATT2
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODAT3_2_8.value==this.w_SNCODAT3)
      this.oPgFrm.Page2.oPag.oSNCODAT3_2_8.value=this.w_SNCODAT3
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT3_2_9.value==this.w_DESATT3)
      this.oPgFrm.Page2.oPag.oDESATT3_2_9.value=this.w_DESATT3
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODAT4_2_10.value==this.w_SNCODAT4)
      this.oPgFrm.Page2.oPag.oSNCODAT4_2_10.value=this.w_SNCODAT4
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT4_2_11.value==this.w_DESATT4)
      this.oPgFrm.Page2.oPag.oDESATT4_2_11.value=this.w_DESATT4
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODAT5_2_12.value==this.w_SNCODAT5)
      this.oPgFrm.Page2.oPag.oSNCODAT5_2_12.value=this.w_SNCODAT5
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT5_2_13.value==this.w_DESATT5)
      this.oPgFrm.Page2.oPag.oDESATT5_2_13.value=this.w_DESATT5
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODAT6_2_14.value==this.w_SNCODAT6)
      this.oPgFrm.Page2.oPag.oSNCODAT6_2_14.value=this.w_SNCODAT6
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT6_2_15.value==this.w_DESATT6)
      this.oPgFrm.Page2.oPag.oDESATT6_2_15.value=this.w_DESATT6
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODAT7_2_16.value==this.w_SNCODAT7)
      this.oPgFrm.Page2.oPag.oSNCODAT7_2_16.value=this.w_SNCODAT7
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT7_2_17.value==this.w_DESATT7)
      this.oPgFrm.Page2.oPag.oDESATT7_2_17.value=this.w_DESATT7
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODAT8_2_18.value==this.w_SNCODAT8)
      this.oPgFrm.Page2.oPag.oSNCODAT8_2_18.value=this.w_SNCODAT8
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT8_2_19.value==this.w_DESATT8)
      this.oPgFrm.Page2.oPag.oDESATT8_2_19.value=this.w_DESATT8
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODAT9_2_20.value==this.w_SNCODAT9)
      this.oPgFrm.Page2.oPag.oSNCODAT9_2_20.value=this.w_SNCODAT9
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT9_2_21.value==this.w_DESATT9)
      this.oPgFrm.Page2.oPag.oDESATT9_2_21.value=this.w_DESATT9
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCDRUO_2_23.value==this.w_SNCDRUO)
      this.oPgFrm.Page2.oPag.oSNCDRUO_2_23.value=this.w_SNCDRUO
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRUO_2_24.value==this.w_DESRUO)
      this.oPgFrm.Page2.oPag.oDESRUO_2_24.value=this.w_DESRUO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_SNCODIC2)) OR  (UPPER(.w_SNCODIC1)<= UPPER(.w_SNCODIC2))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_SNCODIC1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSNCODIC1_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice nominativo iniziale � maggiore di quello finale oppure obsoleto")
          case   not(((UPPER(.w_SNCODIC1) <= UPPER(.w_SNCODIC2)) or (empty(.w_SNCODIC2))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_SNCODIC2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSNCODIC2_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice nominativo iniziale � maggiore di quello finale oppure obsoleto")
          case   (empty(.w_DATASTAM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATASTAM_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DATASTAM)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATASTAM = this.w_DATASTAM
    this.o_SNCODAT0 = this.w_SNCODAT0
    this.o_SNCODAT1 = this.w_SNCODAT1
    this.o_SNCODAT2 = this.w_SNCODAT2
    this.o_SNCODAT3 = this.w_SNCODAT3
    this.o_SNCODAT4 = this.w_SNCODAT4
    this.o_SNCODAT5 = this.w_SNCODAT5
    this.o_SNCODAT6 = this.w_SNCODAT6
    this.o_SNCODAT7 = this.w_SNCODAT7
    this.o_SNCODAT8 = this.w_SNCODAT8
    return

enddefine

* --- Define pages as container
define class tgsar_snoPag1 as StdContainer
  Width  = 592
  height = 387
  stdWidth  = 592
  stdheight = 387
  resizeXpos=371
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSNCODIC1_1_2 as StdField with uid="FLQRTXUTOU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SNCODIC1", cQueryName = "SNCODIC1",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice nominativo iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Selezione dal codice nominativo",;
    HelpContextID = 215348137,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=121, Top=9, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_SNCODIC1"

  func oSNCODIC1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODIC1_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODIC1_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oSNCODIC1_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oSNCODIC1_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_SNCODIC1
     i_obj.ecpSave()
  endproc

  add object oSTDESNO1_1_3 as StdField with uid="RCFFKFXLNJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_STDESNO1", cQueryName = "STDESNO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 116383145,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=290, Top=9, InputMask=replicate('X',40)

  add object oSNCODIC2_1_5 as StdField with uid="VIKAEKGHKM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SNCODIC2", cQueryName = "SNCODIC2",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice nominativo iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Selezione al codice nominativo",;
    HelpContextID = 215348136,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=121, Top=32, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_SNCODIC2"

  func oSNCODIC2_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODIC2_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODIC2_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oSNCODIC2_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oSNCODIC2_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_SNCODIC2
     i_obj.ecpSave()
  endproc

  add object oSTDESNO2_1_6 as StdField with uid="TKYSFKDMDA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_STDESNO2", cQueryName = "STDESNO2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 116383144,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=290, Top=32, InputMask=replicate('X',40)


  add object oSNTIPNOM_1_8 as StdCombo with uid="GNJTDAXDDK",rtseq=5,rtrep=.f.,left=121,top=58,width=111,height=21;
    , ToolTipText = "Tipo nominativo";
    , HelpContextID = 119202701;
    , cFormVar="w_SNTIPNOM",RowSource=""+"Tutti,"+"Da valutare,"+"Potenziale,"+"Lead,"+"Prospect,"+"Cliente,"+"Congelato,"+"Non interessante", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSNTIPNOM_1_8.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'T',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'L',;
    iif(this.value =5,'P',;
    iif(this.value =6,'C',;
    iif(this.value =7,'G',;
    iif(this.value =8,'N',;
    space(1))))))))))
  endfunc
  func oSNTIPNOM_1_8.GetRadio()
    this.Parent.oContained.w_SNTIPNOM = this.RadioValue()
    return .t.
  endfunc

  func oSNTIPNOM_1_8.SetRadio()
    this.Parent.oContained.w_SNTIPNOM=trim(this.Parent.oContained.w_SNTIPNOM)
    this.value = ;
      iif(this.Parent.oContained.w_SNTIPNOM=='X',1,;
      iif(this.Parent.oContained.w_SNTIPNOM=='T',2,;
      iif(this.Parent.oContained.w_SNTIPNOM=='Z',3,;
      iif(this.Parent.oContained.w_SNTIPNOM=='L',4,;
      iif(this.Parent.oContained.w_SNTIPNOM=='P',5,;
      iif(this.Parent.oContained.w_SNTIPNOM=='C',6,;
      iif(this.Parent.oContained.w_SNTIPNOM=='G',7,;
      iif(this.Parent.oContained.w_SNTIPNOM=='N',8,;
      0))))))))
  endfunc

  func oSNTIPNOM_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
   endif
  endfunc

  func oSNTIPNOM_1_8.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oSNTIPNOM_1_9 as StdCombo with uid="YDJHUGWLQL",rtseq=6,rtrep=.f.,left=121,top=58,width=111,height=21;
    , ToolTipText = "Tipo nominativo";
    , HelpContextID = 119202701;
    , cFormVar="w_SNTIPNOM",RowSource=""+"Tutti,"+"Nominativo,"+"Cliente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSNTIPNOM_1_9.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'T',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oSNTIPNOM_1_9.GetRadio()
    this.Parent.oContained.w_SNTIPNOM = this.RadioValue()
    return .t.
  endfunc

  func oSNTIPNOM_1_9.SetRadio()
    this.Parent.oContained.w_SNTIPNOM=trim(this.Parent.oContained.w_SNTIPNOM)
    this.value = ;
      iif(this.Parent.oContained.w_SNTIPNOM=='X',1,;
      iif(this.Parent.oContained.w_SNTIPNOM=='T',2,;
      iif(this.Parent.oContained.w_SNTIPNOM=='C',3,;
      0)))
  endfunc

  func oSNTIPNOM_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oSNTIPNOM_1_9.mHide()
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
  endfunc

  add object oDATASTAM_1_10 as StdField with uid="YLKKPSCVPU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATASTAM", cQueryName = "DATASTAM",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 15921533,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=121, Top=283

  add object oSNPROVIN_1_12 as StdField with uid="UCFVECZVAP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SNPROVIN", cQueryName = "SNPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza",;
    HelpContextID = 14539892,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=121, Top=86, InputMask=replicate('X',2), bHasZoom = .t. 

  func oSNPROVIN_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)="AD HOC ENTERPRISE")
    endwith
   endif
  endfunc

  func oSNPROVIN_1_12.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE")
    endwith
  endfunc

  proc oSNPROVIN_1_12.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_SNPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSNPROVIN_1_13 as StdField with uid="HLDJQNHYQT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SNPROVIN", cQueryName = "SNPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza",;
    HelpContextID = 14539892,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=121, Top=86, InputMask=replicate('X',2)

  func oSNPROVIN_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)="ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oSNPROVIN_1_13.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oSNNAZION_1_15 as StdField with uid="DUUDSXWDKM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SNNAZION", cQueryName = "SNNAZION",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Nazione di appartenenza",;
    HelpContextID = 193151884,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=110, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", oKey_1_1="NACODNAZ", oKey_1_2="this.w_SNNAZION"

  func oSNNAZION_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNNAZION_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNNAZION_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oSNNAZION_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Nazioni",'',this.parent.oContained
  endproc

  add object oDESNAZ_1_16 as StdField with uid="QKMQLEIWNB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESNAZ", cQueryName = "DESNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione nazione",;
    HelpContextID = 201719242,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=187, Top=110, InputMask=replicate('X',35)

  add object oSNCODPRI_1_18 as StdField with uid="KUEIEJAHIM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SNCODPRI", cQueryName = "SNCODPRI",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Priorit� attribuita al nominativo",;
    HelpContextID = 97907601,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=160, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="GRU_PRIO", oKey_1_1="GPCODICE", oKey_1_2="this.w_SNCODPRI"

  func oSNCODPRI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODPRI_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODPRI_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oSNCODPRI_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi priorit�",'',this.parent.oContained
  endproc

  add object oDESPRIO_1_19 as StdField with uid="XTKEVJMOAS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESPRIO", cQueryName = "DESPRIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione priorit�",;
    HelpContextID = 200539594,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=187, Top=160, InputMask=replicate('X',35)

  add object oSNCODGRU_1_21 as StdField with uid="UNPJLMPEEC",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SNCODGRU", cQueryName = "SNCODGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo di appartenenza",;
    HelpContextID = 248902533,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=134, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_NOMI", oKey_1_1="GNCODICE", oKey_1_2="this.w_SNCODGRU"

  func oSNCODGRU_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODGRU_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODGRU_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_NOMI','*','GNCODICE',cp_AbsName(this.parent,'oSNCODGRU_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi nominativi",'',this.parent.oContained
  endproc

  add object oDESCGN_1_22 as StdField with uid="OXGVGSHZVB",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCGN", cQueryName = "DESCGN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo nominativo",;
    HelpContextID = 129039818,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=187, Top=134, InputMask=replicate('X',35)

  add object oSNCODORI_1_24 as StdField with uid="PNBSTVXJHA",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SNCODORI", cQueryName = "SNCODORI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Origine reperimento nominativo",;
    HelpContextID = 114684817,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=187, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ORI_NOMI", oKey_1_1="ONCODICE", oKey_1_2="this.w_SNCODORI"

  func oSNCODORI_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODORI_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODORI_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ORI_NOMI','*','ONCODICE',cp_AbsName(this.parent,'oSNCODORI_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici origine nominativi",'',this.parent.oContained
  endproc

  add object oDESORI_1_25 as StdField with uid="FBXJICCBRH",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione origine",;
    HelpContextID = 200605130,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=187, Top=187, InputMask=replicate('X',35)

  add object oSNCODUTE_1_27 as StdField with uid="WUQFLGQQWQ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_SNCODUTE", cQueryName = "SNCODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 14021525,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=211, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_SNCODUTE"

  func oSNCODUTE_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODUTE_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODUTE_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oSNCODUTE_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESOPE_1_28 as StdField with uid="UODXIIMSHN",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESOPE", cQueryName = "DESOPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice operatore",;
    HelpContextID = 1375690,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=187, Top=211, InputMask=replicate('X',35)

  add object oSNCODZON_1_30 as StdField with uid="JEIWNKIDFS",rtseq=20,rtrep=.f.,;
    cFormVar = "w_SNCODZON", cQueryName = "SNCODZON",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice zona di appartenenza",;
    HelpContextID = 198570892,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=235, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", oKey_1_1="ZOCODZON", oKey_1_2="this.w_SNCODZON"

  func oSNCODZON_1_30.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oSNCODZON_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODZON_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODZON_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oSNCODZON_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Zone",'',this.parent.oContained
  endproc

  add object oDESZONE_1_31 as StdField with uid="PJLDMIGCKS",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESZONE", cQueryName = "DESZONE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione zone",;
    HelpContextID = 149291574,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=187, Top=235, InputMask=replicate('X',35)

  func oDESZONE_1_31.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oSNCODAGE_1_33 as StdField with uid="UDANMVNSCD",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SNCODAGE", cQueryName = "SNCODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente",;
    HelpContextID = 187305067,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=259, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_SNCODAGE"

  func oSNCODAGE_1_33.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oSNCODAGE_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODAGE_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODAGE_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oSNCODAGE_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oDESAGE_1_34 as StdField with uid="NZBPQJVISD",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 11730378,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=187, Top=259, InputMask=replicate('X',35)

  func oDESAGE_1_34.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oSELCON_1_36 as StdCheck with uid="ZFAVEQINMK",rtseq=24,rtrep=.f.,left=451, top=130, caption="Dettaglio contatti",;
    ToolTipText = "Stampa i contatti associati ai nominativi",;
    HelpContextID = 120679642,;
    cFormVar="w_SELCON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELCON_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    ''))
  endfunc
  func oSELCON_1_36.GetRadio()
    this.Parent.oContained.w_SELCON = this.RadioValue()
    return .t.
  endfunc

  func oSELCON_1_36.SetRadio()
    this.Parent.oContained.w_SELCON=trim(this.Parent.oContained.w_SELCON)
    this.value = ;
      iif(this.Parent.oContained.w_SELCON=='S',1,;
      0)
  endfunc

  add object oSELATT_1_37 as StdCheck with uid="NQDBCKHBMM",rtseq=25,rtrep=.f.,left=451, top=149, caption="Dettaglio attributi",;
    ToolTipText = "Stampa gli attributi assegnati ai nominativi",;
    HelpContextID = 14904538,;
    cFormVar="w_SELATT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELATT_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    ''))
  endfunc
  func oSELATT_1_37.GetRadio()
    this.Parent.oContained.w_SELATT = this.RadioValue()
    return .t.
  endfunc

  func oSELATT_1_37.SetRadio()
    this.Parent.oContained.w_SELATT=trim(this.Parent.oContained.w_SELATT)
    this.value = ;
      iif(this.Parent.oContained.w_SELATT=='S',1,;
      0)
  endfunc

  add object oOBSOLETI_1_38 as StdCheck with uid="LLGAKQJVHH",rtseq=26,rtrep=.f.,left=451, top=168, caption="Solo obsoleti",;
    ToolTipText = "Stampa solo i nominativi obsoleti",;
    HelpContextID = 5570513,;
    cFormVar="w_OBSOLETI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOBSOLETI_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oOBSOLETI_1_38.GetRadio()
    this.Parent.oContained.w_OBSOLETI = this.RadioValue()
    return .t.
  endfunc

  func oOBSOLETI_1_38.SetRadio()
    this.Parent.oContained.w_OBSOLETI=trim(this.Parent.oContained.w_OBSOLETI)
    this.value = ;
      iif(this.Parent.oContained.w_OBSOLETI=='S',1,;
      0)
  endfunc


  add object oBtn_1_40 as StdButton with uid="SMDSNJPKFR",left=486, top=337, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 93772006;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_41 as cp_outputCombo with uid="ZLERXIVPWT",left=121, top=309, width=464,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 265121818


  add object oBtn_1_42 as StdButton with uid="YESNQIRUXG",left=537, top=337, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 120399098;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="VOXGBJFEJL",Visible=.t., Left=5, Top=9,;
    Alignment=1, Width=112, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="BRSOYYEROT",Visible=.t., Left=5, Top=32,;
    Alignment=1, Width=112, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="CFWPGFRFCB",Visible=.t., Left=5, Top=58,;
    Alignment=1, Width=112, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="SPZPYYFLGV",Visible=.t., Left=5, Top=86,;
    Alignment=1, Width=112, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="OXUFLKYTNO",Visible=.t., Left=5, Top=110,;
    Alignment=1, Width=112, Height=18,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="NJZGBOAVCW",Visible=.t., Left=5, Top=161,;
    Alignment=1, Width=112, Height=18,;
    Caption="Gr. priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="GYZPKGJJXH",Visible=.t., Left=5, Top=134,;
    Alignment=1, Width=112, Height=18,;
    Caption="Gr. nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="JINCGGAKVH",Visible=.t., Left=5, Top=187,;
    Alignment=1, Width=112, Height=18,;
    Caption="Origine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="FSDBXSLPSK",Visible=.t., Left=5, Top=211,;
    Alignment=1, Width=112, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="GGMQEJEICX",Visible=.t., Left=5, Top=235,;
    Alignment=1, Width=112, Height=18,;
    Caption="Codice zona:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="EDIGVBZFUW",Visible=.t., Left=5, Top=259,;
    Alignment=1, Width=112, Height=18,;
    Caption="Codice agente:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="XBXFWAOQQM",Visible=.t., Left=451, Top=109,;
    Alignment=0, Width=130, Height=15,;
    Caption="Opzioni di stampa"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="KHWVLZCVUG",Visible=.t., Left=5, Top=309,;
    Alignment=1, Width=112, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="WFISAKWOJQ",Visible=.t., Left=5, Top=284,;
    Alignment=1, Width=112, Height=18,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_43 as StdBox with uid="CUBDGORAXB",left=450, top=125, width=130,height=1
enddefine
define class tgsar_snoPag2 as StdContainer
  Width  = 592
  height = 387
  stdWidth  = 592
  stdheight = 387
  resizeXpos=434
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSNCODAT0_2_2 as StdField with uid="FXJGYGRWAP",rtseq=27,rtrep=.f.,;
    cFormVar = "w_SNCODAT0", cQueryName = "SNCODAT0",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 1",;
    HelpContextID = 81130410,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=147, Top=23, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SNCODAT0"

  func oSNCODAT0_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODAT0_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODAT0_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSNCODAT0_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT0_2_3 as StdField with uid="HMQDUTQTVT",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESATT0", cQueryName = "DESATT0",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 14876106,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=253, Top=23, InputMask=replicate('X',35)

  add object oSNCODAT1_2_4 as StdField with uid="DLOEXUGDHN",rtseq=29,rtrep=.f.,;
    cFormVar = "w_SNCODAT1", cQueryName = "SNCODAT1",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 2",;
    HelpContextID = 81130409,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=147, Top=47, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SNCODAT1"

  func oSNCODAT1_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_SNCODAT0))
    endwith
   endif
  endfunc

  func oSNCODAT1_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODAT1_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODAT1_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSNCODAT1_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT1_2_5 as StdField with uid="WDFDSCLFND",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESATT1", cQueryName = "DESATT1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 14876106,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=253, Top=47, InputMask=replicate('X',35)

  add object oSNCODAT2_2_6 as StdField with uid="HNBJFMRUWX",rtseq=31,rtrep=.f.,;
    cFormVar = "w_SNCODAT2", cQueryName = "SNCODAT2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 3",;
    HelpContextID = 81130408,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=147, Top=71, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SNCODAT2"

  func oSNCODAT2_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_SNCODAT1))
    endwith
   endif
  endfunc

  func oSNCODAT2_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODAT2_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODAT2_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSNCODAT2_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT2_2_7 as StdField with uid="MAJTQMWKHR",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESATT2", cQueryName = "DESATT2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 14876106,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=253, Top=71, InputMask=replicate('X',35)

  add object oSNCODAT3_2_8 as StdField with uid="BNQEYHQYDN",rtseq=33,rtrep=.f.,;
    cFormVar = "w_SNCODAT3", cQueryName = "SNCODAT3",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 4",;
    HelpContextID = 81130407,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=147, Top=95, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SNCODAT3"

  func oSNCODAT3_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_SNCODAT2))
    endwith
   endif
  endfunc

  func oSNCODAT3_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODAT3_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODAT3_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSNCODAT3_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT3_2_9 as StdField with uid="IELFGCXART",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESATT3", cQueryName = "DESATT3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 14876106,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=253, Top=95, InputMask=replicate('X',35)

  add object oSNCODAT4_2_10 as StdField with uid="STFSYPYSZH",rtseq=35,rtrep=.f.,;
    cFormVar = "w_SNCODAT4", cQueryName = "SNCODAT4",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 5",;
    HelpContextID = 81130406,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=147, Top=119, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SNCODAT4"

  func oSNCODAT4_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_SNCODAT3))
    endwith
   endif
  endfunc

  func oSNCODAT4_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODAT4_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODAT4_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSNCODAT4_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT4_2_11 as StdField with uid="TTVSRHBSED",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESATT4", cQueryName = "DESATT4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 14876106,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=253, Top=119, InputMask=replicate('X',35)

  add object oSNCODAT5_2_12 as StdField with uid="BHBXERTFYU",rtseq=37,rtrep=.f.,;
    cFormVar = "w_SNCODAT5", cQueryName = "SNCODAT5",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 6",;
    HelpContextID = 81130405,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=147, Top=143, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SNCODAT5"

  func oSNCODAT5_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_SNCODAT4))
    endwith
   endif
  endfunc

  func oSNCODAT5_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODAT5_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODAT5_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSNCODAT5_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT5_2_13 as StdField with uid="VHYSDADBOD",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESATT5", cQueryName = "DESATT5",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 253559350,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=253, Top=143, InputMask=replicate('X',35)

  add object oSNCODAT6_2_14 as StdField with uid="AIDJNARPHX",rtseq=39,rtrep=.f.,;
    cFormVar = "w_SNCODAT6", cQueryName = "SNCODAT6",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 7",;
    HelpContextID = 81130404,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=147, Top=167, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SNCODAT6"

  func oSNCODAT6_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_SNCODAT5))
    endwith
   endif
  endfunc

  func oSNCODAT6_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODAT6_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODAT6_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSNCODAT6_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT6_2_15 as StdField with uid="FOTTNHBWMW",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESATT6", cQueryName = "DESATT6",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 253559350,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=253, Top=167, InputMask=replicate('X',35)

  add object oSNCODAT7_2_16 as StdField with uid="MWEBONAXRD",rtseq=41,rtrep=.f.,;
    cFormVar = "w_SNCODAT7", cQueryName = "SNCODAT7",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 8",;
    HelpContextID = 81130403,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=147, Top=191, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SNCODAT7"

  func oSNCODAT7_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_SNCODAT6))
    endwith
   endif
  endfunc

  func oSNCODAT7_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODAT7_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODAT7_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSNCODAT7_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT7_2_17 as StdField with uid="KBCELBEALV",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESATT7", cQueryName = "DESATT7",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 253559350,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=253, Top=191, InputMask=replicate('X',35)

  add object oSNCODAT8_2_18 as StdField with uid="DTWRSHIQBR",rtseq=43,rtrep=.f.,;
    cFormVar = "w_SNCODAT8", cQueryName = "SNCODAT8",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 9",;
    HelpContextID = 81130402,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=147, Top=215, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SNCODAT8"

  func oSNCODAT8_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_SNCODAT7))
    endwith
   endif
  endfunc

  func oSNCODAT8_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODAT8_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODAT8_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSNCODAT8_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT8_2_19 as StdField with uid="UHXRCKAJZN",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESATT8", cQueryName = "DESATT8",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 253559350,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=253, Top=215, InputMask=replicate('X',35)

  add object oSNCODAT9_2_20 as StdField with uid="UQSEZFBHPS",rtseq=45,rtrep=.f.,;
    cFormVar = "w_SNCODAT9", cQueryName = "SNCODAT9",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 10",;
    HelpContextID = 81130401,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=147, Top=240, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SNCODAT9"

  func oSNCODAT9_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_SNCODAT8))
    endwith
   endif
  endfunc

  func oSNCODAT9_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODAT9_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODAT9_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSNCODAT9_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT9_2_21 as StdField with uid="CBRIXWXQMU",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESATT9", cQueryName = "DESATT9",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 253559350,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=253, Top=239, InputMask=replicate('X',35)

  add object oSNCDRUO_2_23 as StdField with uid="BTWAVAOLMZ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_SNCDRUO", cQueryName = "SNCDRUO",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Ruolo della persona contattata",;
    HelpContextID = 62426,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=147, Top=275, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="RUO_CONT", cZoomOnZoom="GSAR_ARC", oKey_1_1="RCCODICE", oKey_1_2="this.w_SNCDRUO"

  func oSNCDRUO_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCDRUO_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCDRUO_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'RUO_CONT','*','RCCODICE',cp_AbsName(this.parent,'oSNCDRUO_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ARC',"Ruolo contatti",'',this.parent.oContained
  endproc
  proc oSNCDRUO_2_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ARC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RCCODICE=this.parent.oContained.w_SNCDRUO
     i_obj.ecpSave()
  endproc

  add object oDESRUO_2_24 as StdField with uid="SQNMVIDPCM",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESRUO", cQueryName = "DESRUO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 96599498,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=253, Top=275, InputMask=replicate('X',35)

  add object oStr_2_1 as StdString with uid="VEAHWSHIUO",Visible=.t., Left=9, Top=23,;
    Alignment=1, Width=134, Height=18,;
    Caption="Categ. attributi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="EHKOJGSWYD",Visible=.t., Left=9, Top=275,;
    Alignment=1, Width=134, Height=18,;
    Caption="Ruolo contatto:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_sno','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
