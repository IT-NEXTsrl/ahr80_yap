* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_bg1                                                        *
*              Gestione maschera generazione ordini da web                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_44]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-12-28                                                      *
* Last revis.: 2001-12-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_bg1",oParentObject,m.pPARAM)
return(i_retval)

define class tgscp_bg1 as StdBatch
  * --- Local variables
  pPARAM = space(4)
  w_RGBLIVCON = 0
  w_OBJ = .NULL.
  w_OBJ = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione MSG BOX
    i_MsgTitle=IIF(type("i_MsgTitle")="C", i_MsgTitle, iif(g_typebutton=1,"adhoc Revolution", "adhoc Enterprise"))
    do case
      case this.pPARAM = "SELE"
        * --- Seleziona gli ordini
        UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET xChk=0 WHERE ORLIVCON >= 0
        if this.oParentObject.w_SELEZI="ALL"
          * --- Seleziona tutte le righe dello zoom
          UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET xChk=1 WHERE ORLIVCON >= 0
        else
          * --- Seleziona le righe dello zoom in base al livello
          UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET xChk=1 WHERE ORLIVCON = Val(this.oParentObject.w_SELEZI)
        endif
      case this.pPARAM = "DESELE"
        * --- Deseleziona gli ordini
        if this.oParentObject.w_DESELEZI="ALL"
          * --- Deseleziona tutte le righe dello zoom
          UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET xChk=0
        else
          * --- Deleziona le righe dello zoom in base al livello
          UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET xChk=0 WHERE ORLIVCON = Val(this.oParentObject.w_DESELEZI)
        endif
      case this.pPARAM = "LIV_CHANGE"
        * --- Modifica il livello di conformit� di un ordine
        this.w_RGBLIVCON = iif(this.oParentObject.w_LIVELLO=8,65280,iif(this.oParentObject.w_LIVELLO=6,65535,iif(this.oParentObject.w_LIVELLO=4,255,iif(this.oParentObject.w_LIVELLO=2,0,16777215)))) 
        UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET ORLIVCON= this.oParentObject.w_LIVELLO WHERE ORSERIAL = this.oParentObject.w_ORSERIAL
        UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET RGBLIVCON= this.w_RGBLIVCON WHERE ORSERIAL = this.oParentObject.w_ORSERIAL
        if this.oParentObject.w_LIVELLO < 0
          UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET xChk=0 WHERE ORSERIAL = this.oParentObject.w_ORSERIAL
        endif
      case this.pPARAM = "SHOW_ORDI"
        * --- Visualizza ordine selezionato
        * --- Chiave seriale dell'ordine
        * --- Oggetto gestione
        this.w_OBJ = GSCP_MOW()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if (this.w_OBJ.bSec1)
          * --- Inizializzo la chiave primaria
          this.w_OBJ.w_ORSERIAL = this.oParentObject.w_ORSERIAL
          * --- Creo il cursore delle solo chiavi
          this.w_OBJ.QueryKeySet("ORSERIAL='"+this.oParentObject.w_ORSERIAL+ "'","")     
          * --- Attiva modalit� di interrogazione
          this.w_OBJ.LoadRecWarn()     
        else
          ah_Msg("Permessi insufficienti per visualizzare il documento",.f.)
        endif
      case this.pPARAM = "INTERROGA"
        * --- Puntatore ad oggetto parent
        this.w_OBJ = this.oParentObject
        * --- Aggiorna zoom con elenco ordini
        this.w_OBJ.NotifyEvent("Interroga")     
        * --- Attiva la pagina 1 automaticamente
        this.w_OBJ.oPgFrm.ActivePage = 1
      case this.pPARAM = "AFTER_QUERY"
        * --- Evento w_ZoomSel AFTER QUERY
        UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET NOTECONT= space(1)
        UPDATE (this.oParentObject.w_ZoomSel.cCursor) SET ORMERISP= space(1)
        select (this.oParentObject.w_ZoomSel.cCursor)
        go top
      case this.pPARAM = "ROW_CHECKED"
        * --- Evento w_ZoomSel ROW CHECKED
        select (this.oParentObject.w_ZoomSel.cCursor)
        if ORLIVCON < 0
          ColCount=ALLTRIM(STR(this.oParentObject.w_ZoomSel.grd.ColumnCount))
          this.oParentObject.w_ZoomSel.grd.Column&ColCount..chk.Value = 0
          ah_ErrorMsg("Non � possibile marcare l'ordine prima di aver determinato il livello di conformit�","!")
        endif
    endcase
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
