* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_glt                                                        *
*              Gadget Live Tile                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-12-16                                                      *
* Last revis.: 2015-12-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_glt",oParentObject))

* --- Class definition
define class tgsut_glt as StdGadgetForm
  Top    = 2
  Left   = 1

  * --- Standard Properties
  Width  = 148
  Height = 94
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-03"
  HelpContextID=220808041
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_glt"
  cComment = "Gadget Live Tile"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_VALUE = space(10)
  w_TITLE = space(254)
  o_TITLE = space(254)
  w_QUERY = space(254)
  w_RET = .F.
  w_IMAGE = space(254)
  w_DATETIME = space(20)
  w_PROGRAM = space(250)
  w_VALUEFORMAT = space(50)
  w_PRGDESCRI = space(50)
  w_FONTCLR = 0
  w_IMAGETMP = space(254)
  w_APPLYTHEMEIMG = .F.
  w_IMAGETMP_1 = space(254)
  w_VALUELBL = .NULL.
  w_IMGOBJ = .NULL.
  w_oFooter = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_glt
  *--- ApplyTheme
  Proc ApplyTheme()
     DoDefault()
     Local l_oldErr, l_OnErrorMsg
     With This
       m.l_OnErrorMsg = ""
       m.l_oldErr = On("Error")
       On Error m.l_OnErrorMsg=ah_MsgFormat("%1%0%2",Iif(Empty(m.l_OnErrorMsg), "Aggiornamento gadget:",m.l_OnErrorMsg),Message())
       
       .w_FONTCLR =  Iif(.nFontColor<>-1, .nFontColor, Rgb(243,243,243))
       .w_IMAGETMP = Iif(.w_APPLYTHEMEIMG, cp_TmpColorizeImg(.w_IMAGETMP_1, .w_FONTCLR), .w_IMAGETMP_1)
       
       On Error &l_oldErr
       If !Empty(m.l_OnErrorMsg) && c'� stato un errore di programma
          .cAlertMsg = Iif(Empty(.cAlertMsg), m.l_OnErrorMsg, ah_MsgFormat("%1%0%2",.cAlertMsg,m.l_OnErrorMsg))
       Endif
       
       .mCalc(.t.)
       .SaveDependsOn()
     Endwith
  EndProc
  * --- Fine Area Manuale
  *--- Define Header
  add object oHeader as cp_headergadget with Width=this.Width, Left=0, Top=0, nNumPages=1 
  Height = This.Height + this.oHeader.Height

  * --- Define Page Frame
  add object oPgFrm as cp_oPgFrm with PageCount=1, Top=this.oHeader.Height, Width=this.Width, Height=this.Height-this.oHeader.Height
  proc oPgFrm.Init
    cp_oPgFrm::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_gltPag1","gsut_glt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_VALUELBL = this.oPgFrm.Pages(1).oPag.VALUELBL
    this.w_IMGOBJ = this.oPgFrm.Pages(1).oPag.IMGOBJ
    this.w_oFooter = this.oPgFrm.Pages(1).oPag.oFooter
    DoDefault()
    proc Destroy()
      this.w_VALUELBL = .NULL.
      this.w_IMGOBJ = .NULL.
      this.w_oFooter = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_glt
    This.w_VALUELBL.FontName ='Open Sans'
    This.w_VALUELBL.FontSize = 35
    This.w_VALUELBL.bGlobalFont = .f.
    This.w_VALUELBL.Alignment = 1
    
    This.w_IMGOBJ.Visible = .f.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VALUE=space(10)
      .w_TITLE=space(254)
      .w_QUERY=space(254)
      .w_RET=.f.
      .w_IMAGE=space(254)
      .w_DATETIME=space(20)
      .w_PROGRAM=space(250)
      .w_VALUEFORMAT=space(50)
      .w_PRGDESCRI=space(50)
      .w_FONTCLR=0
      .w_IMAGETMP=space(254)
      .w_APPLYTHEMEIMG=.f.
      .w_IMAGETMP_1=space(254)
      .oPgFrm.Page1.oPag.VALUELBL.Calculate(.w_VALUE,.w_FONTCLR)
          .DoRTCalc(1,4,.f.)
        .w_IMAGE = ''
      .oPgFrm.Page1.oPag.IMGOBJ.Calculate(.w_IMAGETMP)
      .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
          .DoRTCalc(6,7,.f.)
        .w_VALUEFORMAT = ''
          .DoRTCalc(9,9,.f.)
        .w_FONTCLR = Rgb(243,243,243)
          .DoRTCalc(11,11,.f.)
        .w_APPLYTHEMEIMG = .t.
    endwith
    this.DoRTCalc(13,13,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
      	*--- Assegnamento caption ad oggetto header
          If .o_TITLE <> .w_TITLE
			   .oHeader.Calculate(.oPgFrm.Page1.oPag.oContained.w_TITLE)
          EndIf
        .oPgFrm.Page1.oPag.VALUELBL.Calculate(.w_VALUE,.w_FONTCLR)
        .oPgFrm.Page1.oPag.IMGOBJ.Calculate(.w_IMAGETMP)
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.VALUELBL.Calculate(.w_VALUE,.w_FONTCLR)
        .oPgFrm.Page1.oPag.IMGOBJ.Calculate(.w_IMAGETMP)
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
    endwith
  return

  proc Calculate_OGRWDURRLK()
    with this
          * --- Refresh Label
          .w_RET = RefreshAll(This)
    endwith
  endproc
  proc Calculate_YCUUTVASEB()
    with this
          * --- Gadget Option
          .w_RET = This.GadgetOption()
          .w_RET = RefreshAll(This)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("GadgetOnDemand") or lower(cEvent)==lower("GadgetArranged") or lower(cEvent)==lower("GadgetOnTimer") or lower(cEvent)==lower("oheader RefreshOnDemand")
          .Calculate_OGRWDURRLK()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.VALUELBL.Event(cEvent)
        if lower(cEvent)==lower("GadgetOption")
          .Calculate_YCUUTVASEB()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.IMGOBJ.Event(cEvent)
      .oPgFrm.Page1.oPag.oFooter.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TITLE = this.w_TITLE
    return

enddefine

* --- Define pages as container
define class tgsut_gltPag1 as StdContainer
  Width  = 148
  height = 94
  stdWidth  = 148
  stdheight = 94
  resizeXpos=78
  resizeYpos=68
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object VALUELBL as cp_calclbl with uid="DJZRBPOYUI",left=72, top=3, width=65,height=59,;
    caption='xValue',;
   bGlobalFont=.t.,;
    caption="00",Alignment=1,fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,bGlobalFont=.t.,alignment=1,fontname="Arial",;
    nPag=1;
    , HelpContextID = 261696118


  add object IMGOBJ as cp_showimage with uid="DVGZJQZVIK",left=5, top=3, width=64,height=59,;
    caption='cImage',;
   bGlobalFont=.t.,;
    stretch=1,default="..\vfcsim\themes\fepa\bmp\gd_Document.png",;
    nPag=1;
    , HelpContextID = 246340646


  add object oFooter as cp_FooterGadget with uid="UMXGAILDJX",left=4, top=64, width=137,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 225625062
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_glt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_glt
*--- Refresh
Proc RefreshAll(pParent)
  Local cCursor,l_Value,l_Title,l_Image,bFoundAlias,l_Anchor,l_oldError,l_OnErrorMsg
  
  With m.pParent
    .cAlertMsg = ""
    If Not Empty(.w_QUERY) And cp_FileExist(ForceExt(.w_QUERY,'VQR'))
      
      *--- Eseguo query
      m.cCursor = "Gadget_GLT"+Sys(2015)
      Vq_Exec(.w_QUERY, m.pParent, m.cCursor)
      If Used(m.cCursor)
       
        m.l_oldError = On("Error")
        On Error m.bFoundAlias = .f.
         
        *--- xValue
        m.bFoundAlias = .t.
        m.l_Value = Trans(&cCursor..xValue, .w_VALUEFORMAT)
        If m.bFoundAlias
          .w_VALUE = Alltrim(m.l_Value)
        Else
          .cAlertMsg = ah_MsgFormat('La query %1 deve avere una colonna con alias "xValue"',Alltrim(.w_QUERY))
          .w_IMGOBJ.Visible = .f.
          On Error &l_oldError
          Return
        Endif
         
        *--- cTitle
        m.bFoundAlias = .t.
        m.l_Title = Alltrim(&cCursor..cTitle)
        If m.bFoundAlias
          .w_TITLE = m.l_Title
        Else
          .w_TITLE = Alltrim(.w_TITLE)
        Endif
         
        *--- cImage
        m.bFoundAlias = .t.
        m.l_Image = Alltrim(&cCursor..cImage)
        If m.bFoundAlias
          .w_IMAGETMP_1 = m.l_Image
        Else
          .w_IMAGETMP_1 = Alltrim(.w_IMAGE)
        Endif
         
        m.l_OnErrorMsg = ""
        On Error m.l_OnErrorMsg=ah_MsgFormat("%1%0%2",Iif(Empty(m.l_OnErrorMsg), "Aggiornamento gadget:",m.l_OnErrorMsg),Message())
       
        *--- Se l'immagine non � stata passata o non trovo il file nascondo il controllo
        .w_IMAGETMP = Iif(.w_APPLYTHEMEIMG, cp_TmpColorizeImg(.w_IMAGETMP_1, .w_FONTCLR), .w_IMAGETMP_1)
        If !Empty(.w_IMAGETMP) And cp_FileExist(.w_IMAGETMP)
          .w_IMGOBJ.Visible = .t.
          m.l_Anchor = .w_VALUELBL.Anchor
          .w_VALUELBL.Anchor = 0
          .w_VALUELBL.Move(.w_IMGOBJ.Left+.w_IMGOBJ.Width+5, .w_VALUELBL.Top, .Width-.w_IMGOBJ.Width-15)
          .w_VALUELBL.Anchor = m.l_Anchor
        Else
          .w_IMGOBJ.Visible = .f.
          m.l_Anchor = .w_VALUELBL.Anchor
          .w_VALUELBL.Anchor = 0
          .w_VALUELBL.Move(5, .w_VALUELBL.Top, .Width-10)
          .w_VALUELBL.Anchor = m.l_Anchor
        Endif
         
        *--- Chiusura cursore, altrimenti ho un errore nella mcalc
        Use In Select(m.cCursor)        
        *--- Aggiorno la data di ultimo aggiornamento
        .tLastUpdate = DateTime()
        .w_DATETIME = Alltrim(TTOC(.tLastUpdate))+' '
        
        On Error &l_oldError
      Endif
    Else
      .cAlertMsg = ah_MsgFormat('Query non definita o inesistente:%0"%1"', Alltrim(.w_QUERY))
    EndIf

    If !Empty(m.l_OnErrorMsg) && c'� stato un errore di programma
      .cAlertMsg = Iif(Empty(.cAlertMsg), m.l_OnErrorMsg, ah_MsgFormat("%1%0%2",.cAlertMsg,m.l_OnErrorMsg))
    Endif
    .mCalc(.T.)
    .SaveDependsOn()
  EndWith
EndProc
* --- Fine Area Manuale
