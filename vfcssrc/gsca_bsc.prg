* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bsc                                                        *
*              Stampe di controllo                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_172]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-22                                                      *
* Last revis.: 2009-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bsc",oParentObject)
return(i_retval)

define class tgsca_bsc as StdBatch
  * --- Local variables
  w_LIVE = 0
  w_RIPARTIZ = space(1)
  w_RIPA = space(1)
  w_SIMBOLO = space(5)
  w_CAMBIO = 0
  * --- WorkFile variables
  VALUTE_idx=0
  RIPATMP1_idx=0
  TMP_ANA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampe di Controllo (da GSCA_SSC)
    * --- PN - Prima Nota, AN Mov. Analitica, DO Documenti
    if empty( this.oParentObject.w_DATA1 ) OR empty( this.oParentObject.w_DATA2 )
      ah_ErrorMsg("Intervallo di date non valido",,"")
      i_retcode = 'stop'
      return
    endif
    do case
      case this.oParentObject.w_ONUME=1
        * --- Dettaglio Movimenti per Voci e C./C.R.
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_ONUME=2
        * --- Dettaglio Movimenti per C./C.R. e Voci
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_ONUME=3
        * --- Dettaglio Movimenti per Conto e C./C.R.
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_ONUME=4
        * --- Dettaglio Movimenti per Voci C./C.R. e Conto
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_ONUME=5
        * --- Dettaglio Movimenti per Commessa
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    if this.oParentObject.w_DATARAGG="S"
      * --- Raggruppamento per data registrazione
      do case
        case Between(this.oParentObject.w_ONUME,3,4)
          * --- Create temporary table RIPATMP1
          i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('query\gsca_10c',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.RIPATMP1_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        case this.oParentObject.w_ONUME=5
          * --- Create temporary table RIPATMP1
          i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('query\gsca_09c',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.RIPATMP1_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        otherwise
          * --- Create temporary table RIPATMP1
          i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('query\gsca_11c',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.RIPATMP1_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endcase
      * --- Create temporary table TMP_ANA
      i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gsca_12c',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_ANA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Esecuzione Query Output Utente (su Tabella Temporanea)
    vq_exec(alltrim(this.oParentObject.w_OQRY),this,"__tmp__")
    * --- Variabili utilizzate nel Report
    L_COMMES=this.oParentObject.w_COMMES
    L_TIPO=this.oParentObject.w_TIPO
    L_CENCER=this.oParentObject.w_CENCER
    L_PROVECCR=this.oParentObject.w_PROVECCR
    L_PROVECOM=this.oParentObject.w_PROVECOM
    L_PROVE=this.oParentObject.w_PROVE
    L_VOCE=this.oParentObject.w_VOCE
    L_LIVE1=this.oParentObject.w_LIVE1
    L_CONCON=this.oParentObject.w_CONCON
    L_LIVE2=this.oParentObject.w_LIVE2
    L_DATA1=this.oParentObject.w_DATA1
    L_SIMBOLO=this.w_SIMBOLO
    L_DATA2=this.oParentObject.w_DATA2
    L_CAMBIO=this.w_CAMBIO
    L_DATARAGG=this.oParentObject.w_DATARAGG
    L_SUCOMMES=this.oParentObject.w_SUCOMMES
    * --- Lancio Stampa Output Utente
    CP_CHPRN( ALLTRIM(this.oParentObject.w_OREP), " ", this )
    * --- Eliminazione Tabelle Temporanee
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    * --- Drop temporary table TMP_ANA
    i_nIdx=cp_GetTableDefIdx('TMP_ANA')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ANA')
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GSCA_SSC.VQR => Manuali Originari
    *     GSCA5SSC.VQR => Primanota Originari
    *     GSCA50SC.VQR => Documenti Originari
    *     GSCA10SC.VQR => Manuali Ripartiti
    *     GSCA20SC.VQR => Primanota Ripartiti
    *     GSCA30SC.VQR => Documenti Ripartiti
    * --- Creazione Tabella Temporanea
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca_ssc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Leggo il simbolo della valuta di conto attuale - la stampa � comunque nella valuta di conto
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMBOLO = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Calcolo gli importi del Rateo in base al periodo
    * --- Per calcolare il RATEO moltipiclo l'importo per il numero di giorni della competenza interni al periodo
    * --- il risultato lo divido l'importo per il numero di giorni della competenza
    * --- I cambi e gli arrotondamenti li calcolo nel Report
    * --- Create temporary table TMP_ANA
    i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca_1sc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_ANA_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_ANA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ANA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ANA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MRCODICE,MRCODVOC,TIPO,CHIAVE1,CHIAVE2,CHIAVE3"
      do vq_exec with 'QUERY\GSCA__SC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ANA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" from "+i_cTable+" TMP_ANA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA, "+i_cQueryTable+" _t2 set ";
      +"TMP_ANA.MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +Iif(Empty(i_ccchkf),"",",TMP_ANA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_ANA.MRCODICE = t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = t2.CHIAVE3";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA set (";
          +"MRRATEO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MRCODICE = "+i_cQueryTable+".MRCODICE";
              +" and "+i_cTable+".MRCODVOC = "+i_cQueryTable+".MRCODVOC";
              +" and "+i_cTable+".TIPO = "+i_cQueryTable+".TIPO";
              +" and "+i_cTable+".CHIAVE1 = "+i_cQueryTable+".CHIAVE1";
              +" and "+i_cTable+".CHIAVE2 = "+i_cQueryTable+".CHIAVE2";
              +" and "+i_cTable+".CHIAVE3 = "+i_cQueryTable+".CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GSCA1SSC.VQR => Manuali Originari
    *     GSCA6SSC.VQR => Primanota Originari
    *     GSCA60SC.VQR => Documenti Originari
    *     GSCA11SC.VQR => Manuali Ripartiti
    *     GSCA21SC.VQR => Primanota Ripartiti
    *     GSCA31SC.VQR => Documenti Ripartiti
    * --- Creazione Tabella Temporanea
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca1ssc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Leggo il simbolo della valuta di conto attuale - la stampa � comunque nella valuta di conto
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMBOLO = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Calcolo gli importi del Rateo in base al periodo
    * --- Per calcolare il RATEO moltipiclo l'importo per il numero di giorni della competenza interni al periodo
    * --- il risultato lo divido l'importo per il numero di giorni della competenza
    * --- I cambi e gli arrotondamenti li calcolo nel Report
    * --- Create temporary table TMP_ANA
    i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca_2sc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_ANA_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_ANA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ANA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ANA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MRCODICE,MRCODVOC,TIPO,CHIAVE1,CHIAVE2,CHIAVE3"
      do vq_exec with 'QUERY\GSCA__SC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ANA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" from "+i_cTable+" TMP_ANA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA, "+i_cQueryTable+" _t2 set ";
      +"TMP_ANA.MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +Iif(Empty(i_ccchkf),"",",TMP_ANA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_ANA.MRCODICE = t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = t2.CHIAVE3";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA set (";
          +"MRRATEO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MRCODICE = "+i_cQueryTable+".MRCODICE";
              +" and "+i_cTable+".MRCODVOC = "+i_cQueryTable+".MRCODVOC";
              +" and "+i_cTable+".TIPO = "+i_cQueryTable+".TIPO";
              +" and "+i_cTable+".CHIAVE1 = "+i_cQueryTable+".CHIAVE1";
              +" and "+i_cTable+".CHIAVE2 = "+i_cQueryTable+".CHIAVE2";
              +" and "+i_cTable+".CHIAVE3 = "+i_cQueryTable+".CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GSCA3SSC.VQR => Manuali Originari
    *     GSCA7SSC.VQR => Primanota Originari
    *     GSCA70SC.VQR => Documenti Originari
    *     GSCA12SC.VQR => Primanota Ripartiti
    *     GSCA22SC.VQR => Documenti Ripartiti
    *     GSCA32SC.VQR => Manuali Ripartiti
    * --- Creazione Tabella Temporanea
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca2ssc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- sistemazione del conto contabile (contropartita) per i documenti, combinando la categoria contabile articolo con la categoria contabile cliente/fornitore
    * --- Write into RIPATMP1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIPATMP1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CHIAVE1,CHIAVE2,CHIAVE3,TIPO,ORIGINE,MRCODICE"
      do vq_exec with 'gsca7sss',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP1_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="RIPATMP1.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"RIPATMP1.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"RIPATMP1.CHIAVE3 = _t2.CHIAVE3";
              +" and "+"RIPATMP1.TIPO = _t2.TIPO";
              +" and "+"RIPATMP1.ORIGINE = _t2.ORIGINE";
              +" and "+"RIPATMP1.MRCODICE = _t2.MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODICE = _t2.ANCODIC2";
          +",ANDESCRI = _t2.ANDESCR2";
          +i_ccchkf;
          +" from "+i_cTable+" RIPATMP1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="RIPATMP1.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"RIPATMP1.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"RIPATMP1.CHIAVE3 = _t2.CHIAVE3";
              +" and "+"RIPATMP1.TIPO = _t2.TIPO";
              +" and "+"RIPATMP1.ORIGINE = _t2.ORIGINE";
              +" and "+"RIPATMP1.MRCODICE = _t2.MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP1, "+i_cQueryTable+" _t2 set ";
          +"RIPATMP1.ANCODICE = _t2.ANCODIC2";
          +",RIPATMP1.ANDESCRI = _t2.ANDESCR2";
          +Iif(Empty(i_ccchkf),"",",RIPATMP1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="RIPATMP1.CHIAVE1 = t2.CHIAVE1";
              +" and "+"RIPATMP1.CHIAVE2 = t2.CHIAVE2";
              +" and "+"RIPATMP1.CHIAVE3 = t2.CHIAVE3";
              +" and "+"RIPATMP1.TIPO = t2.TIPO";
              +" and "+"RIPATMP1.ORIGINE = t2.ORIGINE";
              +" and "+"RIPATMP1.MRCODICE = t2.MRCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP1 set (";
          +"ANCODICE,";
          +"ANDESCRI";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ANCODIC2,";
          +"t2.ANDESCR2";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="RIPATMP1.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"RIPATMP1.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"RIPATMP1.CHIAVE3 = _t2.CHIAVE3";
              +" and "+"RIPATMP1.TIPO = _t2.TIPO";
              +" and "+"RIPATMP1.ORIGINE = _t2.ORIGINE";
              +" and "+"RIPATMP1.MRCODICE = _t2.MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP1 set ";
          +"ANCODICE = _t2.ANCODIC2";
          +",ANDESCRI = _t2.ANDESCR2";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CHIAVE1 = "+i_cQueryTable+".CHIAVE1";
              +" and "+i_cTable+".CHIAVE2 = "+i_cQueryTable+".CHIAVE2";
              +" and "+i_cTable+".CHIAVE3 = "+i_cQueryTable+".CHIAVE3";
              +" and "+i_cTable+".TIPO = "+i_cQueryTable+".TIPO";
              +" and "+i_cTable+".ORIGINE = "+i_cQueryTable+".ORIGINE";
              +" and "+i_cTable+".MRCODICE = "+i_cQueryTable+".MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODICE = (select ANCODIC2 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",ANDESCRI = (select ANDESCR2 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Filtro sul conto contabile (non pu� essere effettuato nelle query)
    if NOT EMPTY(this.oParentObject.w_CONCON)
      * --- Delete from RIPATMP1
      i_nConn=i_TableProp[this.RIPATMP1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"ANCODICE <> "+cp_ToStrODBC(this.oParentObject.w_CONCON);
               )
      else
        delete from (i_cTable) where;
              ANCODICE <> this.oParentObject.w_CONCON;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Leggo il simbolo della valuta di conto attuale - la stampa � comunque nella valuta di conto
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMBOLO = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Calcolo gli importi del Rateo in base al periodo
    * --- Per calcolare il RATEO moltipiclo l'importo per il numero di giorni della competenza interni al periodo
    * --- il risultato lo divido l'importo per il numero di giorni della competenza
    * --- I cambi e gli arrotondamenti li calcolo nel Report
    * --- Create temporary table TMP_ANA
    i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca_3sc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_ANA_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_ANA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ANA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ANA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MRCODICE,MRCODVOC,TIPO,CHIAVE1,CHIAVE2,CHIAVE3"
      do vq_exec with 'QUERY\GSCA__SC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ANA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" from "+i_cTable+" TMP_ANA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA, "+i_cQueryTable+" _t2 set ";
      +"TMP_ANA.MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +Iif(Empty(i_ccchkf),"",",TMP_ANA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_ANA.MRCODICE = t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = t2.CHIAVE3";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA set (";
          +"MRRATEO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MRCODICE = "+i_cQueryTable+".MRCODICE";
              +" and "+i_cTable+".MRCODVOC = "+i_cQueryTable+".MRCODVOC";
              +" and "+i_cTable+".TIPO = "+i_cQueryTable+".TIPO";
              +" and "+i_cTable+".CHIAVE1 = "+i_cQueryTable+".CHIAVE1";
              +" and "+i_cTable+".CHIAVE2 = "+i_cQueryTable+".CHIAVE2";
              +" and "+i_cTable+".CHIAVE3 = "+i_cQueryTable+".CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GSCA3SSC.VQR => Manuali Originari
    *     GSCA7SSC.VQR => Primanota Originari
    *     GSCA70SC.VQR => Documenti Originari
    *     GSCA12SC.VQR => Primanota Ripartiti
    *     GSCA22SC.VQR => Documenti Ripartiti
    *     GSCA32SC.VQR => Manuali Ripartiti
    * --- Creazione Tabella Temporanea
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca2ssc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- sistemazione del conto contabile (contropartita) per i documenti, combinando la categoria contabile articolo con la categoria contabile cliente/fornitore
    * --- Write into RIPATMP1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIPATMP1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CHIAVE1,CHIAVE2,CHIAVE3,TIPO,ORIGINE,MRCODICE"
      do vq_exec with 'gsca7sss',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP1_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="RIPATMP1.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"RIPATMP1.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"RIPATMP1.CHIAVE3 = _t2.CHIAVE3";
              +" and "+"RIPATMP1.TIPO = _t2.TIPO";
              +" and "+"RIPATMP1.ORIGINE = _t2.ORIGINE";
              +" and "+"RIPATMP1.MRCODICE = _t2.MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODICE = _t2.ANCODIC2";
          +",ANDESCRI = _t2.ANDESCR2";
          +i_ccchkf;
          +" from "+i_cTable+" RIPATMP1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="RIPATMP1.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"RIPATMP1.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"RIPATMP1.CHIAVE3 = _t2.CHIAVE3";
              +" and "+"RIPATMP1.TIPO = _t2.TIPO";
              +" and "+"RIPATMP1.ORIGINE = _t2.ORIGINE";
              +" and "+"RIPATMP1.MRCODICE = _t2.MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP1, "+i_cQueryTable+" _t2 set ";
          +"RIPATMP1.ANCODICE = _t2.ANCODIC2";
          +",RIPATMP1.ANDESCRI = _t2.ANDESCR2";
          +Iif(Empty(i_ccchkf),"",",RIPATMP1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="RIPATMP1.CHIAVE1 = t2.CHIAVE1";
              +" and "+"RIPATMP1.CHIAVE2 = t2.CHIAVE2";
              +" and "+"RIPATMP1.CHIAVE3 = t2.CHIAVE3";
              +" and "+"RIPATMP1.TIPO = t2.TIPO";
              +" and "+"RIPATMP1.ORIGINE = t2.ORIGINE";
              +" and "+"RIPATMP1.MRCODICE = t2.MRCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP1 set (";
          +"ANCODICE,";
          +"ANDESCRI";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ANCODIC2,";
          +"t2.ANDESCR2";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="RIPATMP1.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"RIPATMP1.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"RIPATMP1.CHIAVE3 = _t2.CHIAVE3";
              +" and "+"RIPATMP1.TIPO = _t2.TIPO";
              +" and "+"RIPATMP1.ORIGINE = _t2.ORIGINE";
              +" and "+"RIPATMP1.MRCODICE = _t2.MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP1 set ";
          +"ANCODICE = _t2.ANCODIC2";
          +",ANDESCRI = _t2.ANDESCR2";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CHIAVE1 = "+i_cQueryTable+".CHIAVE1";
              +" and "+i_cTable+".CHIAVE2 = "+i_cQueryTable+".CHIAVE2";
              +" and "+i_cTable+".CHIAVE3 = "+i_cQueryTable+".CHIAVE3";
              +" and "+i_cTable+".TIPO = "+i_cQueryTable+".TIPO";
              +" and "+i_cTable+".ORIGINE = "+i_cQueryTable+".ORIGINE";
              +" and "+i_cTable+".MRCODICE = "+i_cQueryTable+".MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODICE = (select ANCODIC2 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",ANDESCRI = (select ANDESCR2 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Filtro sul conto contabile (non pu� essere effettuato nelle query)
    if NOT EMPTY(this.oParentObject.w_CONCON)
      * --- Delete from RIPATMP1
      i_nConn=i_TableProp[this.RIPATMP1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"ANCODICE <> "+cp_ToStrODBC(this.oParentObject.w_CONCON);
               )
      else
        delete from (i_cTable) where;
              ANCODICE <> this.oParentObject.w_CONCON;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Leggo il simbolo della valuta di conto attuale - la stampa � comunque nella valuta di conto
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMBOLO = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Calcolo gli importi del Rateo in base al periodo
    * --- Per calcolare il RATEO moltipiclo l'importo per il numero di giorni della competenza interni al periodo
    * --- il risultato lo divido l'importo per il numero di giorni della competenza
    * --- I cambi e gli arrotondamenti li calcolo nel Report
    * --- Create temporary table TMP_ANA
    i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca_4sc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_ANA_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_ANA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ANA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ANA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MRCODICE,MRCODVOC,TIPO,CHIAVE1,CHIAVE2,CHIAVE3"
      do vq_exec with 'QUERY\GSCA__SC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ANA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" from "+i_cTable+" TMP_ANA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA, "+i_cQueryTable+" _t2 set ";
      +"TMP_ANA.MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +Iif(Empty(i_ccchkf),"",",TMP_ANA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_ANA.MRCODICE = t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = t2.CHIAVE3";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA set (";
          +"MRRATEO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MRCODICE = "+i_cQueryTable+".MRCODICE";
              +" and "+i_cTable+".MRCODVOC = "+i_cQueryTable+".MRCODVOC";
              +" and "+i_cTable+".TIPO = "+i_cQueryTable+".TIPO";
              +" and "+i_cTable+".CHIAVE1 = "+i_cQueryTable+".CHIAVE1";
              +" and "+i_cTable+".CHIAVE2 = "+i_cQueryTable+".CHIAVE2";
              +" and "+i_cTable+".CHIAVE3 = "+i_cQueryTable+".CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GSCA3SSC.VQR => Manuali Originari
    *     GSCA8SSC.VQR => Primanota Originari
    *     GSCA80SC.VQR => Documenti Originari
    *     GSCA13SC.VQR => Manuali Ripartiti
    *     GSCA14SC.VQR => Primanota Ripartiti
    *     GSCA15SC.VQR => Documenti Ripartiti
    * --- Creazione Tabella Temporanea
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca3ssc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Stampo sempre nella valuta di conto !
    * --- Calcolo il cambio con la valuta di conto - leggo anche la valuta
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAMBIO = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_SIMBOLO = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Calcolo gli importi del Rateo in base al periodo
    * --- Per calcolare il RATEO moltipiclo l'importo per il numero di giorni della competenza interni al periodo
    * --- il risultato lo divido l'importo per il numero di giorni della competenza
    * --- I cambi e gli arrotondamenti li calcolo nel Report
    * --- Create temporary table TMP_ANA
    i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca_5sc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_ANA_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_ANA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ANA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ANA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MRCODICE,MRCODVOC,TIPO,CHIAVE1,CHIAVE2,CHIAVE3"
      do vq_exec with 'QUERY\GSCA__SC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ANA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" from "+i_cTable+" TMP_ANA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA, "+i_cQueryTable+" _t2 set ";
      +"TMP_ANA.MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +Iif(Empty(i_ccchkf),"",",TMP_ANA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_ANA.MRCODICE = t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = t2.CHIAVE3";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA set (";
          +"MRRATEO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_ANA.MRCODICE = _t2.MRCODICE";
              +" and "+"TMP_ANA.MRCODVOC = _t2.MRCODVOC";
              +" and "+"TMP_ANA.TIPO = _t2.TIPO";
              +" and "+"TMP_ANA.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"TMP_ANA.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"TMP_ANA.CHIAVE3 = _t2.CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_ANA set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MRCODICE = "+i_cQueryTable+".MRCODICE";
              +" and "+i_cTable+".MRCODVOC = "+i_cQueryTable+".MRCODVOC";
              +" and "+i_cTable+".TIPO = "+i_cQueryTable+".TIPO";
              +" and "+i_cTable+".CHIAVE1 = "+i_cQueryTable+".CHIAVE1";
              +" and "+i_cTable+".CHIAVE2 = "+i_cQueryTable+".CHIAVE2";
              +" and "+i_cTable+".CHIAVE3 = "+i_cQueryTable+".CHIAVE3";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRRATEO ="+cp_NullLink(cp_ToStrODBC(0),'TMP_ANA','MRRATEO');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='*RIPATMP1'
    this.cWorkTables[3]='*TMP_ANA'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
