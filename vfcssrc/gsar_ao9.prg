* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ao9                                                        *
*              Parametri saldaconto                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_115]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2011-12-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_ao9"))

* --- Class definition
define class tgsar_ao9 as StdForm
  Top    = 93
  Left   = 22

  * --- Standard Properties
  Width  = 589
  Height = 202+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-12-20"
  HelpContextID=91654295
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Constant Properties
  CONTROPA_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  AZIENDA_IDX = 0
  cFile = "CONTROPA"
  cKeySelect = "COCODAZI"
  cKeyWhere  = "COCODAZI=this.w_COCODAZI"
  cKeyWhereODBC = '"COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cKeyWhereODBCqualified = '"CONTROPA.COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cPrg = "gsar_ao9"
  cComment = "Parametri saldaconto"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COCODAZI = space(5)
  w_RAGAZI = space(40)
  w_COTIPCON = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_COCONSAL = space(15)
  w_COCAUSAL = space(5)
  w_COCAUSAF = space(5)
  w_COCAUSAC = space(5)
  w_TIPREG = space(1)
  w_PARTSN = space(1)
  w_COPAGRIB = space(2)
  w_COPAGRIM = space(2)
  w_COPAGCAM = space(2)
  w_COPAGMAV = space(2)
  w_COPAGRID = space(2)
  w_COPAGBON = space(2)
  w_DESSAL = space(40)
  w_DESCAU = space(35)
  w_DESCAUF = space(35)
  w_DESCAUC = space(35)
  w_DATOBSO1 = ctod('  /  /  ')
  w_TIPREG1 = space(1)
  w_DATOBSO2 = ctod('  /  /  ')
  w_TIPREG2 = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsar_ao9
  * --- Disabilita il Caricamento e la Cancellazione sulla Toolbar
  proc SetCPToolBar()
          doDefault()
          oCpToolBar.b3.enabled=.f.
          oCpToolBar.b5.enabled=.f.
  endproc
  * ---- Disattiva i metodi Load e Delete (posso solo variare)
  proc ecpLoad()
      * ----
  endproc
  proc ecpDelete()
      * ----
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CONTROPA','gsar_ao9')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ao9Pag1","gsar_ao9",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Vendite")
      .Pages(1).HelpContextID = 260975786
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsar_ao9
      * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='CONTROPA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTROPA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTROPA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_COCODAZI = NVL(COCODAZI,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- gsar_ao9
    this.w_cocodazi=i_codazi
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONTROPA where COCODAZI=KeySet.COCODAZI
    *
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTROPA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTROPA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTROPA '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RAGAZI = space(40)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_TIPREG = space(1)
        .w_PARTSN = space(1)
        .w_DESSAL = space(40)
        .w_DESCAU = space(35)
        .w_DESCAUF = space(35)
        .w_DESCAUC = space(35)
        .w_DATOBSO1 = ctod("  /  /  ")
        .w_TIPREG1 = space(1)
        .w_DATOBSO2 = ctod("  /  /  ")
        .w_TIPREG2 = space(1)
        .w_COCODAZI = NVL(COCODAZI,space(5))
          if link_1_1_joined
            this.w_COCODAZI = NVL(AZCODAZI101,NVL(this.w_COCODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_COTIPCON = NVL(COTIPCON,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .w_COCONSAL = NVL(COCONSAL,space(15))
          if link_1_11_joined
            this.w_COCONSAL = NVL(ANCODICE111,NVL(this.w_COCONSAL,space(15)))
            this.w_DESSAL = NVL(ANDESCRI111,space(40))
            this.w_PARTSN = NVL(ANPARTSN111,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO111),ctod("  /  /  "))
          else
          .link_1_11('Load')
          endif
        .w_COCAUSAL = NVL(COCAUSAL,space(5))
          if link_1_12_joined
            this.w_COCAUSAL = NVL(CCCODICE112,NVL(this.w_COCAUSAL,space(5)))
            this.w_DESCAU = NVL(CCDESCRI112,space(35))
            this.w_TIPREG = NVL(CCTIPREG112,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO112),ctod("  /  /  "))
          else
          .link_1_12('Load')
          endif
        .w_COCAUSAF = NVL(COCAUSAF,space(5))
          if link_1_13_joined
            this.w_COCAUSAF = NVL(CCCODICE113,NVL(this.w_COCAUSAF,space(5)))
            this.w_DESCAUF = NVL(CCDESCRI113,space(35))
            this.w_TIPREG1 = NVL(CCTIPREG113,space(1))
            this.w_DATOBSO1 = NVL(cp_ToDate(CCDTOBSO113),ctod("  /  /  "))
          else
          .link_1_13('Load')
          endif
        .w_COCAUSAC = NVL(COCAUSAC,space(5))
          if link_1_14_joined
            this.w_COCAUSAC = NVL(CCCODICE114,NVL(this.w_COCAUSAC,space(5)))
            this.w_DESCAUC = NVL(CCDESCRI114,space(35))
            this.w_TIPREG2 = NVL(CCTIPREG114,space(1))
            this.w_DATOBSO2 = NVL(cp_ToDate(CCDTOBSO114),ctod("  /  /  "))
          else
          .link_1_14('Load')
          endif
        .w_COPAGRIB = NVL(COPAGRIB,space(2))
        .w_COPAGRIM = NVL(COPAGRIM,space(2))
        .w_COPAGCAM = NVL(COPAGCAM,space(2))
        .w_COPAGMAV = NVL(COPAGMAV,space(2))
        .w_COPAGRID = NVL(COPAGRID,space(2))
        .w_COPAGBON = NVL(COPAGBON,space(2))
        cp_LoadRecExtFlds(this,'CONTROPA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_ao9
    if this.w_cocodazi<>i_codazi and not empty(this.w_cocodazi)
     this.blankrec()
     this.w_cocodazi=""
     ah_ErrorMsg("Manutenzione consentita alla sola azienda corrente (%1)",,'',i_CODAZI)
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COCODAZI = space(5)
      .w_RAGAZI = space(40)
      .w_COTIPCON = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_COCONSAL = space(15)
      .w_COCAUSAL = space(5)
      .w_COCAUSAF = space(5)
      .w_COCAUSAC = space(5)
      .w_TIPREG = space(1)
      .w_PARTSN = space(1)
      .w_COPAGRIB = space(2)
      .w_COPAGRIM = space(2)
      .w_COPAGCAM = space(2)
      .w_COPAGMAV = space(2)
      .w_COPAGRID = space(2)
      .w_COPAGBON = space(2)
      .w_DESSAL = space(40)
      .w_DESCAU = space(35)
      .w_DESCAUF = space(35)
      .w_DESCAUC = space(35)
      .w_DATOBSO1 = ctod("  /  /  ")
      .w_TIPREG1 = space(1)
      .w_DATOBSO2 = ctod("  /  /  ")
      .w_TIPREG2 = space(1)
      if .cFunction<>"Filter"
        .w_COCODAZI = i_codazi
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_COCODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_COTIPCON = 'G'
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
          .DoRTCalc(4,7,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(9,10,.f.)
          if not(empty(.w_COCONSAL))
          .link_1_11('Full')
          endif
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_COCAUSAL))
          .link_1_12('Full')
          endif
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_COCAUSAF))
          .link_1_13('Full')
          endif
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_COCAUSAC))
          .link_1_14('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTROPA')
    this.DoRTCalc(14,29,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_ao9
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCOCONSAL_1_11.enabled = i_bVal
      .Page1.oPag.oCOCAUSAL_1_12.enabled = i_bVal
      .Page1.oPag.oCOCAUSAF_1_13.enabled = i_bVal
      .Page1.oPag.oCOCAUSAC_1_14.enabled = i_bVal
      .Page1.oPag.oCOPAGRIB_1_17.enabled = i_bVal
      .Page1.oPag.oCOPAGRIM_1_18.enabled = i_bVal
      .Page1.oPag.oCOPAGCAM_1_19.enabled = i_bVal
      .Page1.oPag.oCOPAGMAV_1_20.enabled = i_bVal
      .Page1.oPag.oCOPAGRID_1_21.enabled = i_bVal
      .Page1.oPag.oCOPAGBON_1_22.enabled = i_bVal
      .Page1.oPag.oBtn_1_23.enabled = i_bVal
      .Page1.oPag.oBtn_1_24.enabled = .Page1.oPag.oBtn_1_24.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'CONTROPA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODAZI,"COCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPCON,"COTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCONSAL,"COCONSAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAUSAL,"COCAUSAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAUSAF,"COCAUSAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAUSAC,"COCAUSAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPAGRIB,"COPAGRIB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPAGRIM,"COPAGRIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPAGCAM,"COPAGCAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPAGMAV,"COPAGMAV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPAGRID,"COPAGRID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPAGBON,"COPAGBON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    i_lTable = "CONTROPA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CONTROPA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONTROPA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONTROPA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTROPA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(COCODAZI,COTIPCON,UTCC,UTCV,UTDC"+;
                  ",UTDV,COCONSAL,COCAUSAL,COCAUSAF,COCAUSAC"+;
                  ",COPAGRIB,COPAGRIM,COPAGCAM,COPAGMAV,COPAGRID"+;
                  ",COPAGBON "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_COCODAZI)+;
                  ","+cp_ToStrODBC(this.w_COTIPCON)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBCNull(this.w_COCONSAL)+;
                  ","+cp_ToStrODBCNull(this.w_COCAUSAL)+;
                  ","+cp_ToStrODBCNull(this.w_COCAUSAF)+;
                  ","+cp_ToStrODBCNull(this.w_COCAUSAC)+;
                  ","+cp_ToStrODBC(this.w_COPAGRIB)+;
                  ","+cp_ToStrODBC(this.w_COPAGRIM)+;
                  ","+cp_ToStrODBC(this.w_COPAGCAM)+;
                  ","+cp_ToStrODBC(this.w_COPAGMAV)+;
                  ","+cp_ToStrODBC(this.w_COPAGRID)+;
                  ","+cp_ToStrODBC(this.w_COPAGBON)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTROPA')
        cp_CheckDeletedKey(i_cTable,0,'COCODAZI',this.w_COCODAZI)
        INSERT INTO (i_cTable);
              (COCODAZI,COTIPCON,UTCC,UTCV,UTDC,UTDV,COCONSAL,COCAUSAL,COCAUSAF,COCAUSAC,COPAGRIB,COPAGRIM,COPAGCAM,COPAGMAV,COPAGRID,COPAGBON  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_COCODAZI;
                  ,this.w_COTIPCON;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_COCONSAL;
                  ,this.w_COCAUSAL;
                  ,this.w_COCAUSAF;
                  ,this.w_COCAUSAC;
                  ,this.w_COPAGRIB;
                  ,this.w_COPAGRIM;
                  ,this.w_COPAGCAM;
                  ,this.w_COPAGMAV;
                  ,this.w_COPAGRID;
                  ,this.w_COPAGBON;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONTROPA_IDX,i_nConn)
      *
      * update CONTROPA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONTROPA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " COTIPCON="+cp_ToStrODBC(this.w_COTIPCON)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",COCONSAL="+cp_ToStrODBCNull(this.w_COCONSAL)+;
             ",COCAUSAL="+cp_ToStrODBCNull(this.w_COCAUSAL)+;
             ",COCAUSAF="+cp_ToStrODBCNull(this.w_COCAUSAF)+;
             ",COCAUSAC="+cp_ToStrODBCNull(this.w_COCAUSAC)+;
             ",COPAGRIB="+cp_ToStrODBC(this.w_COPAGRIB)+;
             ",COPAGRIM="+cp_ToStrODBC(this.w_COPAGRIM)+;
             ",COPAGCAM="+cp_ToStrODBC(this.w_COPAGCAM)+;
             ",COPAGMAV="+cp_ToStrODBC(this.w_COPAGMAV)+;
             ",COPAGRID="+cp_ToStrODBC(this.w_COPAGRID)+;
             ",COPAGBON="+cp_ToStrODBC(this.w_COPAGBON)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONTROPA')
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        UPDATE (i_cTable) SET;
              COTIPCON=this.w_COTIPCON;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,COCONSAL=this.w_COCONSAL;
             ,COCAUSAL=this.w_COCAUSAL;
             ,COCAUSAF=this.w_COCAUSAF;
             ,COCAUSAC=this.w_COCAUSAC;
             ,COPAGRIB=this.w_COPAGRIB;
             ,COPAGRIM=this.w_COPAGRIM;
             ,COPAGCAM=this.w_COPAGCAM;
             ,COPAGMAV=this.w_COPAGMAV;
             ,COPAGRID=this.w_COPAGRID;
             ,COPAGBON=this.w_COPAGBON;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONTROPA_IDX,i_nConn)
      *
      * delete CONTROPA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
            .w_COTIPCON = 'G'
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_COCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_COCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on CONTROPA.COCODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and CONTROPA.COCODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCONSAL
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCONSAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCONSAL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCONSAL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCONSAL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCONSAL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCONSAL_1_11'),i_cWhere,'GSAR_BZC',"Conti saldaconto",'CONCOEFA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCONSAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCONSAL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCONSAL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCONSAL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESSAL = NVL(_Link_.ANDESCRI,space(40))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCONSAL = space(15)
      endif
      this.w_DESSAL = space(40)
      this.w_PARTSN = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_PARTSN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COCONSAL = space(15)
        this.w_DESSAL = space(40)
        this.w_PARTSN = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCONSAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.ANCODICE as ANCODICE111"+ ",link_1_11.ANDESCRI as ANDESCRI111"+ ",link_1_11.ANPARTSN as ANPARTSN111"+ ",link_1_11.ANDTOBSO as ANDTOBSO111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on CONTROPA.COCONSAL=link_1_11.ANCODICE"+" and CONTROPA.COTIPCON=link_1_11.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and CONTROPA.COCONSAL=link_1_11.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_11.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAUSAL
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAUSAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAUSAL)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAUSAL))
          select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAUSAL)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCAUSAL) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAUSAL_1_12'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAUSAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAUSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAUSAL)
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAUSAL = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAUSAL = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_TIPREG = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPREG='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
        endif
        this.w_COCAUSAL = space(5)
        this.w_DESCAU = space(35)
        this.w_TIPREG = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAUSAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.CCCODICE as CCCODICE112"+ ",link_1_12.CCDESCRI as CCDESCRI112"+ ",link_1_12.CCTIPREG as CCTIPREG112"+ ",link_1_12.CCDTOBSO as CCDTOBSO112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on CONTROPA.COCAUSAL=link_1_12.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and CONTROPA.COCAUSAL=link_1_12.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAUSAF
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAUSAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAUSAF)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAUSAF))
          select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAUSAF)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCAUSAF) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAUSAF_1_13'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAUSAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAUSAF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAUSAF)
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAUSAF = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAUF = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREG1 = NVL(_Link_.CCTIPREG,space(1))
      this.w_DATOBSO1 = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAUSAF = space(5)
      endif
      this.w_DESCAUF = space(35)
      this.w_TIPREG1 = space(1)
      this.w_DATOBSO1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO1) OR .w_DATOBSO1>.w_OBTEST) AND .w_TIPREG1='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
        endif
        this.w_COCAUSAF = space(5)
        this.w_DESCAUF = space(35)
        this.w_TIPREG1 = space(1)
        this.w_DATOBSO1 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAUSAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.CCCODICE as CCCODICE113"+ ",link_1_13.CCDESCRI as CCDESCRI113"+ ",link_1_13.CCTIPREG as CCTIPREG113"+ ",link_1_13.CCDTOBSO as CCDTOBSO113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on CONTROPA.COCAUSAF=link_1_13.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and CONTROPA.COCAUSAF=link_1_13.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAUSAC
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAUSAC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAUSAC)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAUSAC))
          select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAUSAC)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCAUSAC) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAUSAC_1_14'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAUSAC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAUSAC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAUSAC)
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAUSAC = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAUC = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREG2 = NVL(_Link_.CCTIPREG,space(1))
      this.w_DATOBSO2 = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCAUSAC = space(5)
      endif
      this.w_DESCAUC = space(35)
      this.w_TIPREG2 = space(1)
      this.w_DATOBSO2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO2) OR .w_DATOBSO2>.w_OBTEST) AND .w_TIPREG2='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
        endif
        this.w_COCAUSAC = space(5)
        this.w_DESCAUC = space(35)
        this.w_TIPREG2 = space(1)
        this.w_DATOBSO2 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAUSAC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.CCCODICE as CCCODICE114"+ ",link_1_14.CCDESCRI as CCDESCRI114"+ ",link_1_14.CCTIPREG as CCTIPREG114"+ ",link_1_14.CCDTOBSO as CCDTOBSO114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on CONTROPA.COCAUSAC=link_1_14.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and CONTROPA.COCAUSAC=link_1_14.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOCONSAL_1_11.value==this.w_COCONSAL)
      this.oPgFrm.Page1.oPag.oCOCONSAL_1_11.value=this.w_COCONSAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCAUSAL_1_12.value==this.w_COCAUSAL)
      this.oPgFrm.Page1.oPag.oCOCAUSAL_1_12.value=this.w_COCAUSAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCAUSAF_1_13.value==this.w_COCAUSAF)
      this.oPgFrm.Page1.oPag.oCOCAUSAF_1_13.value=this.w_COCAUSAF
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCAUSAC_1_14.value==this.w_COCAUSAC)
      this.oPgFrm.Page1.oPag.oCOCAUSAC_1_14.value=this.w_COCAUSAC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPAGRIB_1_17.RadioValue()==this.w_COPAGRIB)
      this.oPgFrm.Page1.oPag.oCOPAGRIB_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPAGRIM_1_18.RadioValue()==this.w_COPAGRIM)
      this.oPgFrm.Page1.oPag.oCOPAGRIM_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPAGCAM_1_19.RadioValue()==this.w_COPAGCAM)
      this.oPgFrm.Page1.oPag.oCOPAGCAM_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPAGMAV_1_20.RadioValue()==this.w_COPAGMAV)
      this.oPgFrm.Page1.oPag.oCOPAGMAV_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPAGRID_1_21.RadioValue()==this.w_COPAGRID)
      this.oPgFrm.Page1.oPag.oCOPAGRID_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPAGBON_1_22.RadioValue()==this.w_COPAGBON)
      this.oPgFrm.Page1.oPag.oCOPAGBON_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSAL_1_25.value==this.w_DESSAL)
      this.oPgFrm.Page1.oPag.oDESSAL_1_25.value=this.w_DESSAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_27.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_27.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAUF_1_29.value==this.w_DESCAUF)
      this.oPgFrm.Page1.oPag.oDESCAUF_1_29.value=this.w_DESCAUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAUC_1_31.value==this.w_DESCAUC)
      this.oPgFrm.Page1.oPag.oDESCAUC_1_31.value=this.w_DESCAUC
    endif
    cp_SetControlsValueExtFlds(this,'CONTROPA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_PARTSN))  and not(empty(.w_COCONSAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCONSAL_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPREG='N')  and not(empty(.w_COCAUSAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCAUSAL_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
          case   not((EMPTY(.w_DATOBSO1) OR .w_DATOBSO1>.w_OBTEST) AND .w_TIPREG1='N')  and not(empty(.w_COCAUSAF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCAUSAF_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
          case   not((EMPTY(.w_DATOBSO2) OR .w_DATOBSO2>.w_OBTEST) AND .w_TIPREG2='N')  and not(empty(.w_COCAUSAC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCAUSAC_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_ao9Pag1 as StdContainer
  Width  = 585
  height = 202
  stdWidth  = 585
  stdheight = 202
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_8 as cp_runprogram with uid="WUGFWDJQQA",left=-4, top=217, width=246,height=24,;
    caption='GSAR_BAM(modifica)',;
   bGlobalFont=.t.,;
    prg='GSAR_BAM("modifica")',;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 179863462

  add object oCOCONSAL_1_11 as StdField with uid="KSBVZCKFCZ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_COCONSAL", cQueryName = "COCONSAL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Contropartita utilizzata nel saldaconto",;
    HelpContextID = 229248114,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=156, Top=17, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCONSAL"

  func oCOCONSAL_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCONSAL_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCONSAL_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCONSAL_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti saldaconto",'CONCOEFA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCOCONSAL_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCONSAL
     i_obj.ecpSave()
  endproc

  add object oCOCAUSAL_1_12 as StdField with uid="VMOHGYRRVO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_COCAUSAL", cQueryName = "COCAUSAL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente, obsoleta o di tipo IVA",;
    ToolTipText = "Causale contabile utilizzata per saldaconto per i clienti",;
    HelpContextID = 235670642,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=156, Top=41, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAUSAL"

  func oCOCAUSAL_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAUSAL_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAUSAL_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAUSAL_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAUSAL_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAUSAL
     i_obj.ecpSave()
  endproc

  add object oCOCAUSAF_1_13 as StdField with uid="DKQYUNKYKJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COCAUSAF", cQueryName = "COCAUSAF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente, obsoleta o di tipo IVA",;
    ToolTipText = "Causale contabile utilizzata per saldaconto per i fornitori",;
    HelpContextID = 235670636,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=156, Top=65, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAUSAF"

  func oCOCAUSAF_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAUSAF_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAUSAF_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAUSAF_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAUSAF_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAUSAF
     i_obj.ecpSave()
  endproc

  add object oCOCAUSAC_1_14 as StdField with uid="CPNKISGOXQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_COCAUSAC", cQueryName = "COCAUSAC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente, obsoleta o di tipo IVA",;
    ToolTipText = "Causale contabile utilizzata per saldaconto per i conti",;
    HelpContextID = 235670633,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=156, Top=89, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAUSAC"

  func oCOCAUSAC_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAUSAC_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAUSAC_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAUSAC_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAUSAC_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAUSAC
     i_obj.ecpSave()
  endproc

  add object oCOPAGRIB_1_17 as StdCheck with uid="ETDXTHVCPX",rtseq=16,rtrep=.f.,left=20, top=127, caption="Ri.Ba.",;
    ToolTipText = "Pagamento RiBa",;
    HelpContextID = 64168856,;
    cFormVar="w_COPAGRIB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOPAGRIB_1_17.RadioValue()
    return(iif(this.value =1,'RB',;
    'XX'))
  endfunc
  func oCOPAGRIB_1_17.GetRadio()
    this.Parent.oContained.w_COPAGRIB = this.RadioValue()
    return .t.
  endfunc

  func oCOPAGRIB_1_17.SetRadio()
    this.Parent.oContained.w_COPAGRIB=trim(this.Parent.oContained.w_COPAGRIB)
    this.value = ;
      iif(this.Parent.oContained.w_COPAGRIB=='RB',1,;
      0)
  endfunc

  add object oCOPAGRIM_1_18 as StdCheck with uid="GWCTDPLMXD",rtseq=17,rtrep=.f.,left=20, top=165, caption="Rimessa diretta",;
    ToolTipText = "Pagamento rimessa diretta",;
    HelpContextID = 64168845,;
    cFormVar="w_COPAGRIM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOPAGRIM_1_18.RadioValue()
    return(iif(this.value =1,'RD',;
    'XX'))
  endfunc
  func oCOPAGRIM_1_18.GetRadio()
    this.Parent.oContained.w_COPAGRIM = this.RadioValue()
    return .t.
  endfunc

  func oCOPAGRIM_1_18.SetRadio()
    this.Parent.oContained.w_COPAGRIM=trim(this.Parent.oContained.w_COPAGRIM)
    this.value = ;
      iif(this.Parent.oContained.w_COPAGRIM=='RD',1,;
      0)
  endfunc

  add object oCOPAGCAM_1_19 as StdCheck with uid="QBMRVNMLIL",rtseq=18,rtrep=.f.,left=180, top=127, caption="Cambiale",;
    ToolTipText = "Pagamento cambiale",;
    HelpContextID = 221043827,;
    cFormVar="w_COPAGCAM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOPAGCAM_1_19.RadioValue()
    return(iif(this.value =1,'CA',;
    'XX'))
  endfunc
  func oCOPAGCAM_1_19.GetRadio()
    this.Parent.oContained.w_COPAGCAM = this.RadioValue()
    return .t.
  endfunc

  func oCOPAGCAM_1_19.SetRadio()
    this.Parent.oContained.w_COPAGCAM=trim(this.Parent.oContained.w_COPAGCAM)
    this.value = ;
      iif(this.Parent.oContained.w_COPAGCAM=='CA',1,;
      0)
  endfunc

  add object oCOPAGMAV_1_20 as StdCheck with uid="GGMJKMRACG",rtseq=19,rtrep=.f.,left=180, top=165, caption="M.AV.",;
    ToolTipText = "Pagamento M.AV.",;
    HelpContextID = 120380540,;
    cFormVar="w_COPAGMAV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOPAGMAV_1_20.RadioValue()
    return(iif(this.value =1,'MA',;
    'XX'))
  endfunc
  func oCOPAGMAV_1_20.GetRadio()
    this.Parent.oContained.w_COPAGMAV = this.RadioValue()
    return .t.
  endfunc

  func oCOPAGMAV_1_20.SetRadio()
    this.Parent.oContained.w_COPAGMAV=trim(this.Parent.oContained.w_COPAGMAV)
    this.value = ;
      iif(this.Parent.oContained.w_COPAGMAV=='MA',1,;
      0)
  endfunc

  add object oCOPAGRID_1_21 as StdCheck with uid="GKNUTXFWZS",rtseq=20,rtrep=.f.,left=331, top=127, caption="R.I.D.",;
    ToolTipText = "Pagamento R.I.D.",;
    HelpContextID = 64168854,;
    cFormVar="w_COPAGRID", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOPAGRID_1_21.RadioValue()
    return(iif(this.value =1,'RI',;
    'XX'))
  endfunc
  func oCOPAGRID_1_21.GetRadio()
    this.Parent.oContained.w_COPAGRID = this.RadioValue()
    return .t.
  endfunc

  func oCOPAGRID_1_21.SetRadio()
    this.Parent.oContained.w_COPAGRID=trim(this.Parent.oContained.w_COPAGRID)
    this.value = ;
      iif(this.Parent.oContained.w_COPAGRID=='RI',1,;
      0)
  endfunc

  add object oCOPAGBON_1_22 as StdCheck with uid="FGBKZRDKOY",rtseq=21,rtrep=.f.,left=331, top=165, caption="Bonifico",;
    ToolTipText = "Pagamento bonifico",;
    HelpContextID = 204266612,;
    cFormVar="w_COPAGBON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOPAGBON_1_22.RadioValue()
    return(iif(this.value =1,'BO',;
    'XX'))
  endfunc
  func oCOPAGBON_1_22.GetRadio()
    this.Parent.oContained.w_COPAGBON = this.RadioValue()
    return .t.
  endfunc

  func oCOPAGBON_1_22.SetRadio()
    this.Parent.oContained.w_COPAGBON=trim(this.Parent.oContained.w_COPAGBON)
    this.value = ;
      iif(this.Parent.oContained.w_COPAGBON=='BO',1,;
      0)
  endfunc


  add object oBtn_1_23 as StdButton with uid="VAWYNICEAL",left=481, top=153, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 91683046;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_24 as StdButton with uid="QKFOLHCQMP",left=533, top=153, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per abbandonare";
    , HelpContextID = 98971718;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESSAL_1_25 as StdField with uid="YNDTULZADW",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESSAL", cQueryName = "DESSAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 98501174,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=292, Top=17, InputMask=replicate('X',40)

  add object oDESCAU_1_27 as StdField with uid="ZOWWJXAEZY",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 248447542,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=292, Top=41, InputMask=replicate('X',35)

  add object oDESCAUF_1_29 as StdField with uid="TNHVIWCVRT",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCAUF", cQueryName = "DESCAUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 19987914,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=292, Top=65, InputMask=replicate('X',35)

  add object oDESCAUC_1_31 as StdField with uid="VJFTRHHMQK",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCAUC", cQueryName = "DESCAUC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 19987914,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=292, Top=89, InputMask=replicate('X',35)

  add object oStr_1_26 as StdString with uid="LDTXXXVWQV",Visible=.t., Left=5, Top=16,;
    Alignment=1, Width=151, Height=18,;
    Caption="Contropartita saldaconto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="HEQCGXUCXE",Visible=.t., Left=5, Top=41,;
    Alignment=1, Width=151, Height=18,;
    Caption="Causale saldac. clienti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="FZMXDZCRGU",Visible=.t., Left=5, Top=66,;
    Alignment=1, Width=151, Height=18,;
    Caption="Causale saldac. fornitori:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="YYLNFOIMBU",Visible=.t., Left=5, Top=90,;
    Alignment=1, Width=151, Height=18,;
    Caption="Causale saldac. conti:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ao9','CONTROPA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COCODAZI=CONTROPA.COCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
