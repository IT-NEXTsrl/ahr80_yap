* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bri                                                        *
*              Stampa registri IVA                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_490]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-21                                                      *
* Last revis.: 2018-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bri",oParentObject)
return(i_retval)

define class tgscg_bri as StdBatch
  * --- Local variables
  w_NUMOPE = 0
  w_SCRIVILOG = .f.
  w_LOGDAT = space(1)
  w_IA__ANNO = space(4)
  w_IALIMCOM = ctod("  /  /  ")
  w_FINPE2 = ctod("  /  /  ")
  w_APPO = space(10)
  w_OLDSER = space(10)
  w_IMPSTA = 0
  w_APPO1 = space(10)
  w_SERIAL = space(10)
  w_IVASTA = 0
  w_APPO2 = space(10)
  w_TIPREC = space(1)
  w_IVISTA = 0
  w_INIPER = ctod("  /  /  ")
  w_ALFDOC = space(10)
  w_TOTMAC = 0
  w_DESCRI = space(40)
  w_IMPSEG = 0
  w_FINPER = ctod("  /  /  ")
  w_NUMDOC = 0
  w_IVASEG = 0
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_TIPSOT = space(1)
  w_FINPE2 = ctod("  /  /  ")
  w_OLDNUM = 0
  w_IVISEG = 0
  w_OKCAL = .f.
  w_DATDOC = ctod("  /  /  ")
  w_IMPIND = 0
  w_OK = .f.
  w_TROV = .f.
  w_OLDDAT = 0
  w_IVAIND = 0
  w_DATBLO = ctod("  /  /  ")
  w_OKNUM = 0
  w_IVIIND = 0
  w_DATBLO2 = ctod("  /  /  ")
  w_FLIVDF = space(1)
  w_IMPPRE = 0
  w_CONTA = 0
  w_ANNO2 = space(4)
  w_IVAPRE = 0
  w_PERVEN = 0
  w_IMPVEN = 0
  w_MESS = space(90)
  w_NUMPER2 = 0
  w_IVIPRE = 0
  w_IMPONI = 0
  w_COMPET = space(9)
  w_IMPFAD = 0
  w_IMPIVA = 0
  w_NOMREP = space(10)
  w_IVAFAD = 0
  w_VALNAZ = space(3)
  w_CONFER = space(1)
  w_IVIFAD = 0
  w_DATREG = ctod("  /  /  ")
  w_DATULT = ctod("  /  /  ")
  w_COMIVA = ctod("  /  /  ")
  w_CODIVA = space(5)
  w_OKSEQ = .f.
  w_MACSTA = 0
  w_MACPRE = 0
  w_MACSEG = 0
  w_MACFAD = 0
  w_MACIND = 0
  w_ELENCO = space(50)
  w_ATTIVI = space(5)
  w_DATAINIP = ctod("  /  /  ")
  w_DATAFINP = ctod("  /  /  ")
  w_DATAINIZ = ctod("  /  /  ")
  w_FLTRASP = .f.
  w_INDAZI = space(25)
  w_LOCAZI = space(35)
  w_CAPAZI = space(5)
  w_PROAZI = space(2)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_PAGINE = 0
  w_DATIN1 = ctod("  /  /  ")
  w_DATAIN1P = ctod("  /  /  ")
  w_ANNOT = 0
  w_FLPROR = space(1)
  w_PERPRO = 0
  w_DIFPERC = 0
  * --- WorkFile variables
  ATTIDETT_idx=0
  AZIENDA_idx=0
  CONTI_idx=0
  CREDMIVA_idx=0
  PNT_DETT_idx=0
  PNT_IVA_idx=0
  PNT_MAST_idx=0
  PRI_DETT_idx=0
  PRI_MAST_idx=0
  VALUTE_idx=0
  ATTIMAST_idx=0
  PRO_RATA_idx=0
  DAT_IVAN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Registri IVA (da GSCG_SRI)
    * --- Variabile utilizzata Per acconto Iva con metodo Operazioni effettuate.
    *     La variabile viene utilizzata nel batch GSVE_BV2 di ricostruzione saldi.
    *     Quindi viene definita anche qui per non causare errore.
    this.w_NUMOPE = 1
    this.w_FLTRASP = .F.
    * --- Eventuale Gestione IVA Trasportatori
    if this.oParentObject.w_TIPREG="V"
      this.w_DATAINIZ = this.oParentObject.w_DATINI
      do trim_prec with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- SE � trimestrale o del Mese 3,6,9,12 deve elaborare le registrazioni AUTOTRASPORTO
      this.w_FLTRASP = IIF(g_TIPDEN="T" OR MOD(this.oParentObject.w_NUMPER, 3)=0, .T., .F.)
      * --- Determino la data di inizio anno (relativo alle fatture autotrasportatori)
      this.w_ANNOT = YEAR(this.w_DATAINIP)
      this.w_DATAIN1P = cp_CharToDate("01-01-" + RIGHT("0000"+ALLTRIM(STR(this.w_ANNOT)), 4))
    endif
    * --- Read from ATTIMAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ATTIMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATPERPRO"+;
        " from "+i_cTable+" ATTIMAST where ";
            +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATPERPRO;
        from (i_cTable) where;
            ATCODATT = this.oParentObject.w_CODATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLPROR = NVL(cp_ToDate(_read_.ATPERPRO),cp_NullValue(_read_.ATPERPRO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Determino la data di inizio anno
    this.w_ANNOT = YEAR(this.oParentObject.w_DATINI)
    this.w_DATIN1 = cp_CharToDate("01-01-" + RIGHT("0000"+ALLTRIM(STR(this.w_ANNOT)), 4))
    * --- Verifiche Preliminari
    do case
      case EMPTY(this.oParentObject.w_CODATT)
        ah_ErrorMsg("Codice attivit� non definito",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_ANNO)
        ah_ErrorMsg("Anno di riferimento non definito",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_NUMPER)
        ah_ErrorMsg("Periodo di riferimento non definito",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_DATINI) OR EMPTY(this.oParentObject.w_DATFIN) OR EMPTY(this.oParentObject.w_DATFI2)
        ah_ErrorMsg("Date di inizio/fine selezione non definite",,"")
        i_retcode = 'stop'
        return
      case NOT ((this.oParentObject.w_NUMPER<13 AND g_TIPDEN="M") OR (this.oParentObject.w_NUMPER<5 AND g_TIPDEN<>"M"))
        ah_ErrorMsg("Periodo di riferimento incongruente",,"")
        i_retcode = 'stop'
        return
      case CALCESER(this.oParentObject.w_DATINI, "####")<>g_CODESE OR CALCESER(this.oParentObject.w_DATFIN, "####")<>g_CODESE
        ah_ErrorMsg("Il periodo selezionato non � di competenza dell'esercizio corrente",,"")
        i_retcode = 'stop'
        return
    endcase
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Crea il File delle Messaggistiche di Errore
    this.w_SCRIVILOG = .F.
    this.w_LOGDAT = " "
     
 CREATE CURSOR MessErr ; 
 (NUMDOC N(15,0), ALFDOC C(10), DATDOC D(8), TIPCLF C(1), CODCLF C(15), DATREG D(8), COMIVA D(8), FLPROV C(1), FLPNUM C(1), ; 
 FLPDAT C(1), LOGDAT C(1), PNNUMDOC N(15,0), PNALFDOC C(10), DESCLF C(40))
    this.w_CONFER = " "
    this.w_PERVEN = 0
    this.w_IMPVEN = 0
    this.w_PAGINE = 0
    * --- Calcola Limiti Periodo
    this.w_IA__ANNO = this.oParentObject.w_ANNO
    * --- Read from DAT_IVAN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IALIMCOM"+;
        " from "+i_cTable+" DAT_IVAN where ";
            +"IACODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and IA__ANNO = "+cp_ToStrODBC(this.w_IA__ANNO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IALIMCOM;
        from (i_cTable) where;
            IACODAZI = i_CODAZI;
            and IA__ANNO = this.w_IA__ANNO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_IALIMCOM = NVL(cp_ToDate(_read_.IALIMCOM),cp_NullValue(_read_.IALIMCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if (this.oParentObject.w_NUMPER<13 AND g_TIPDEN="M") OR (this.oParentObject.w_NUMPER<5 AND g_TIPDEN<>"M")
      this.w_APPO = IIF(g_TIPDEN="M", this.oParentObject.w_NUMPER, (this.oParentObject.w_NUMPER * 3) - 2)
      this.w_APPO2 = this.oParentObject.w_ANNO
      this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
      this.w_INIPER = cp_CharToDate(this.w_APPO1)
      this.w_APPO = IIF(g_TIPDEN="M", this.oParentObject.w_NUMPER+1, ((this.oParentObject.w_NUMPER+1) * 3) - 2)
      this.w_APPO2 = this.oParentObject.w_ANNO
      if this.w_APPO>12
        this.w_APPO = 1
        this.w_APPO2 = ALLTRIM(STR(VAL(this.oParentObject.w_ANNO)+1,4,0))
      endif
      this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
      this.w_FINPER = cp_CharToDate(this.w_APPO1)-1
    endif
    if EMPTY(this.w_INIPER) OR EMPTY(this.w_FINPER)
      ah_ErrorMsg("Date di inizio/fine periodo non definite",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Se Data di Fine selezione inferiore alla Data Fine Periodo non deve stampare i Totali
    this.oParentObject.w_FLSTOT = IIF(this.oParentObject.w_DATFIN<this.w_FINPER, " ", this.oParentObject.w_FLSTOT)
    * --- Controllo Data iniziale Successiva Ultima Stampa - oppure prima stampa deve essere uguale a inizio esercizio
    if (NOT EMPTY(this.oParentObject.w_ULTDAT) AND this.oParentObject.w_DATINI<>this.oParentObject.w_ULTDAT+1) OR (EMPTY(this.oParentObject.w_ULTDAT) AND this.oParentObject.w_DATINI<>this.w_INIPER)
      if this.oParentObject.w_FLDEFI="S"
        ah_ErrorMsg("Data iniziale non consecutiva all'ultima stampa%0Durante la stampa potrebbero essere ignorati alcuni documenti IVA%0%0Elaborazione interrotta",,"")
        i_retcode = 'stop'
        return
      else
        ah_ErrorMsg("Data iniziale non consecutiva all'ultima stampa%0Durante la stampa potrebbero essere ignorati alcuni documenti IVA",,"")
      endif
    endif
    * --- elimino per considerare anche quelle di competenza dell'esercizio successivo
    *     w_FINPE2 = MIN(w_DATFIN+15,g_FINESE)
    if g_TIPDEN="T"
      * --- trimestrale
      if this.oParentObject.w_NUMPER<>4 Or Empty(this.w_IALIMCOM)
        this.w_FINPE2 = this.oParentObject.w_DATFIN+15
      else
        this.w_FINPE2 = this.w_IALIMCOM
      endif
    else
      * --- Mensile
      if this.oParentObject.w_NUMPER<>12 Or Empty(this.w_IALIMCOM)
        this.w_FINPE2 = this.oParentObject.w_DATFIN+15
      else
        this.w_FINPE2 = this.w_IALIMCOM
      endif
    endif
    * --- Verifica che non esistano piu' di 1 Tipo e Num.Registri
    this.w_OK = .T.
    this.w_MESS = " "
    * --- Select from CONTAREG
    do vq_exec with 'CONTAREG',this,'_Curs_CONTAREG','',.f.,.t.
    if used('_Curs_CONTAREG')
      select _Curs_CONTAREG
      locate for 1=1
      do while not(eof())
      if NVL(_Curs_CONTAREG.ATNUMREG,0)=this.oParentObject.w_NUMREG AND NVL(_Curs_CONTAREG.ATTIPREG," ")=this.oParentObject.w_TIPREG
        if NVL(_Curs_CONTAREG.CONTA,0)>1
          this.w_OK = .F.
          this.w_APPO = NVL(_Curs_CONTAREG.ATTIPREG," ")
          do case
            case this.w_APPO="E"
              this.w_MESS = "Verificare registro IVA corrisp./ventilazione - num.: %1 presente in pi� attivit�"
            case this.w_APPO="C"
              this.w_MESS = "Verificare registro IVA corrisp./scorporo - num.: %1 presente in pi� attivit�"
            case this.w_APPO="A"
              this.w_MESS = "Verificare registro IVA acquisti - num.: %1 presente in pi� attivit�"
            otherwise
              this.w_MESS = "Verificare registro IVA vendite - num.: %1 presente in pi� attivit�"
          endcase
          ah_ErrorMsg(this.w_MESS,,"", ALLTR(STR(NVL(_Curs_CONTAREG.ATNUMREG,0))) )
        endif
      endif
        select _Curs_CONTAREG
        continue
      enddo
      use
    endif
    if this.w_OK=.F.
      i_retcode = 'stop'
      return
    endif
    * --- Blocco della Primanota e dei registri associati alla Attivita' se Stampa Definitiva
    if this.oParentObject.w_FLDEFI = "S"
      * --- Try
      local bErr_04A06C18
      bErr_04A06C18=bTrsErr
      this.Try_04A06C18()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg(i_errmsg,,"")
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_04A06C18
      * --- End
    endif
    this.w_OKCAL = .T.
    * --- Esegue Calcolo dei Saldi (Tranne se Provvisoria e no Stampa Totali)
    if this.oParentObject.w_FLDEFI="S" OR this.oParentObject.w_FLSTOT="S"
      this.w_OKCAL = .F.
      * --- Per Compatibilita' con elaborazione Liq.IVA
      this.w_ATTIVI = this.oParentObject.w_CODATT
      do GSCG_BV2 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_OKCAL=.T.
      * --- Inizia Elaborazione Vera e Propria (Legge le Registrazioni IVA dalla Primanota)
      do case
        case this.oParentObject.w_TIPREG="V"
          * --- Registro Vendite
          if this.w_FLTRASP
            * --- Se gestita IVA Trasportatori elabora le Fatture AUTOTRASPORTATORI del Trimestre Precedente
            VQ_EXEC("QUERY\GSCG0BRV.VQR",this,"REGIVA")
          else
            * --- Lettura senza Fatture TRASPORTATORI (standard)
            VQ_EXEC("QUERY\GSCG_BRV.VQR",this,"REGIVA")
          endif
        case this.oParentObject.w_TIPREG="A"
          * --- Registro Acquisti
          VQ_EXEC("QUERY\GSCG_BRA.VQR",this,"REGIVA")
        case this.oParentObject.w_TIPREG="C"
          * --- Registro Corrispettivi/Scorporo
          VQ_EXEC("QUERY\GSCG_BRC.VQR",this,"REGIVA")
        case this.oParentObject.w_TIPREG="E"
          * --- Registro Corrispettivi/Ventilazione
          VQ_EXEC("QUERY\GSCG_BRE.VQR",this,"REGIVA")
      endcase
      if USED("REGIVA")
        SELECT REGIVA
        this.w_OK = .T.
        this.w_OKSEQ = .T.
        * --- Verifica se nella Selezioni esistono Registrazioni non Confermate
        this.w_CONTA = 0
        COUNT FOR NVL(FLPROV,"N")="S" TO this.w_CONTA
        if this.w_CONTA>0
          if this.oParentObject.w_FLDEFI="S"
            ah_ErrorMsg("Per le selezioni impostate esistono registrazioni non confermate%0Confermare tali registrazioni o eseguire la stampa non definitiva",,"")
            this.w_OK = .F.
          else
            this.w_OK = ah_YesNo("Per le selezioni impostate esistono registrazioni non confermate%0Esse non verranno computate nei calcoli dei totali di stampa%0Proseguo comunque?")
          endif
        endif
        L_ARIMP=0
        L_ERIMP=0
        L_ARIVA=0
        L_ERIVA=0
        L_ARIVI=0
        L_ERIVI=0
        if this.w_OK=.T.
          * --- Try
          local bErr_04A2B760
          bErr_04A2B760=bTrsErr
          this.Try_04A2B760()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            this.w_OK = .F.
            if NOT EMPTY(i_errmsg)
              ah_ErrorMsg(i_errmsg,,"")
            else
              ah_ErrorMsg("Errore durante l'elaborazione; operazione abbandonata",,"")
            endif
          endif
          bTrsErr=bTrsErr or bErr_04A2B760
          * --- End
        endif
        if USED("MessErr")
          * --- Raggruppo il cursore degli errori per data registrazione e numero alfa e data documento
          *     in odo da non avere righe ripetute per il numero delle righe del castelletto IVA
          *     e ordino nello stesso modo del Registro
           
 SELECT NUMDOC , ALFDOC , DATDOC , MIN(TIPCLF) AS TIPCLF , MIN(CODCLF) AS CODCLF, DATREG, MIN(COMIVA) AS COMIVA, MIN(FLPROV) AS FLPROV, MIN(FLPNUM) AS FLPNUM, ; 
 MIN(FLPDAT) AS FLPDAT, MIN(LOGDAT) AS LOGDAT, MIN(PNNUMDOC) AS PNNUMDOC, MIN(PNALFDOC) AS PNALFDOC, MIN(DESCLF) AS DESCLF FROM MessErr ; 
 GROUP BY NUMDOC , ALFDOC , DATDOC, DATREG ; 
 ORDER BY DATREG, ALFDOC, NUMDOC, DATDOC INTO CURSOR MESSERR 
 
          SELECT MessErr
          if RECCOUNT()>0
            if ah_YesNo("Visualizzo la situazione delle registrazioni incongruenti?")
              L_TIPREG = this.oParentObject.w_TIPREG
              L_NUMREG = this.oParentObject.w_NUMREG
              if USED("__tmp__")
                SELECT __tmp__
                USE
              endif
              SELECT * FROM MessErr INTO CURSOR __TMP__
              CP_CHPRN("QUERY\GSCG_BRI.FRX", "", this)
            endif
          endif
        endif
      endif
    endif
    * --- Elimina i Cursori
    if USED("REGIVA")
      SELECT REGIVA
      USE
    endif
    if USED("__tmp__")
      SELECT __tmp__
      USE
    endif
    if USED("MessErr")
      SELECT MessErr
      USE
    endif
    * --- Toglie Blocchi per Primanota e Attivita' se Definitiva
    if this.oParentObject.w_FLDEFI = "S"
      * --- Try
      local bErr_04A1C910
      bErr_04A1C910=bTrsErr
      this.Try_04A1C910()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Impossibile rimuovere le date di blocco presenti in primanota e registri IVA%0Provvedere a rimuoverle manualmente dai dati azienda e attivit�",,"")
        this.w_OK = .F.
      endif
      bTrsErr=bTrsErr or bErr_04A1C910
      * --- End
    endif
  endproc
  proc Try_04A06C18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_DATBLO)
      * --- Verifica le Attivita'
      this.w_DATBLO2 = cp_CharToDate("  -  -  ")
      * --- Select from ATTIDETT
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIDETT ";
            +" where ATCODATT="+cp_ToStrODBC(this.oParentObject.w_CODATT)+"";
             ,"_Curs_ATTIDETT")
      else
        select * from (i_cTable);
         where ATCODATT=this.oParentObject.w_CODATT;
          into cursor _Curs_ATTIDETT
      endif
      if used('_Curs_ATTIDETT')
        select _Curs_ATTIDETT
        locate for 1=1
        do while not(eof())
        this.w_DATBLO2 = IIF(EMPTY(CP_TODATE(_Curs_ATTIDETT.ATDATBLO)), this.w_DATBLO2, CP_TODATE(_Curs_ATTIDETT.ATDATBLO))
          select _Curs_ATTIDETT
          continue
        enddo
        use
      endif
      if EMPTY(this.w_DATBLO2)
        * --- Inserisce <Blocco> per Primanota in caso di Stampa Definitiva
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFI2),'AZIENDA','AZDATBLO');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZDATBLO = this.oParentObject.w_DATFI2;
              &i_ccchkf. ;
           where;
              AZCODAZI = this.oParentObject.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Blocca tutti i registri dell'Attivita'
        * --- Write into ATTIDETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFI2),'ATTIDETT','ATDATBLO');
              +i_ccchkf ;
          +" where ";
              +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                 )
        else
          update (i_cTable) set;
              ATDATBLO = this.oParentObject.w_DATFI2;
              &i_ccchkf. ;
           where;
              ATCODATT = this.oParentObject.w_CODATT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        this.w_MESS = "Archivio attivit� bloccato - verificare semaforo bollati in attivita"
        * --- Raise
        i_Error=this.w_MESS
        return
      endif
    else
      this.w_MESS = "Prima nota bloccata - verificare semaforo bollati in dati azienda"
      * --- Raise
      i_Error=this.w_MESS
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04A2B760()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    do case
      case this.oParentObject.w_TIPREG="V"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPREG="A"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPREG="C"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPREG="E"
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    * --- commit
    cp_EndTrs(.t.)
    this.w_OK = .T.
    if this.w_OKSEQ=.F. AND this.oParentObject.w_FLDEFI="S"
      this.w_OK = ah_YesNo("Per le selezioni impostate esistono numerazioni fuori sequenza%0Proseguo comunque?")
    endif
    if this.w_OK=.T.
      * --- Esegue la Stampa
      SELECT REGIVA
      if RECCOUNT()>0
        if this.oParentObject.w_FLSTOT="S"
          * --- Verifica Progressivi Anno su piu Valute
          this.w_TROV = .F.
          SELECT REGIVA
          GO TOP
          SCAN FOR NVL(TIPREC, " ")="R" AND NOT EMPTY(NVL(CODIVA,"  ")) 
          this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
          this.w_IMPSTA = NVL(PROIMP, 0)
          this.w_IVASTA = NVL(PROIVA, 0)
          this.w_IVISTA = NVL(PROIVI, 0)
          * --- Se La valuta dell'esercizio della Riga e' diversa da quella dell' Esercizio Attuale, converte
          * --- Prevediamo solo LIRE o EURO
          if this.w_VALNAZ<>g_PERVAL AND GETCAM(g_PERVAL, I_DATSYS)<>0 
            this.w_TROV = .T.
            this.w_IMPSTA = VAL2MON(this.w_IMPSTA, GETCAM(this.w_VALNAZ, I_DATSYS),1, i_DATSYS, this.w_VALNAZ, GETVALUT(this.w_VALNAZ, "VADECTOT"))
            this.w_IVASTA = VAL2MON(this.w_IVASTA,GETCAM(this.w_VALNAZ, I_DATSYS),1, i_DATSYS, this.w_VALNAZ, GETVALUT(this.w_VALNAZ, "VADECTOT"))
            this.w_IVISTA = VAL2MON(this.w_IVISTA,GETCAM(this.w_VALNAZ, I_DATSYS),1, i_DATSYS, this.w_VALNAZ, GETVALUT(this.w_VALNAZ, "VADECTOT"))
            REPLACE VALNAZ WITH g_PERVAL, PROIMP WITH this.w_IMPSTA, PROIVA WITH this.w_IVASTA, PROIVI WITH this.w_IVISTA
          endif
          ENDSCAN
          if this.w_TROV=.T.
            SELECT REGIVA
            GO TOP
            SELECT CODIVA, SUM(PROIMP) AS PROIMP, SUM(PROIVA) AS PROIVA,SUM(PROIVI) AS PROIVI FROM REGIVA ;
            WHERE NVL(TIPREC, " ")="R" AND NOT EMPTY(NVL(CODIVA,"  ")) INTO CURSOR APPOREGI GROUP BY CODIVA
            SELECT APPOREGI
            if RECCOUNT()>0
              GO TOP
              SCAN FOR NOT EMPTY(CODIVA)
              this.w_CODIVA = CODIVA
              this.w_IMPSTA = NVL(PROIMP, 0)
              this.w_IVASTA = NVL(PROIVA, 0)
              this.w_IVISTA = NVL(PROIVI, 0)
              this.w_TROV = .F.
              * --- Scrive un solo Recor dello stesso Codice IVA ed elimina tutti gli altri
              SELECT REGIVA
              GO TOP
              SCAN FOR NVL(TIPREC, " ")="R" AND NVL(CODIVA,"  ")=this.w_CODIVA AND NOT DELETED()
              if this.w_TROV=.F.
                REPLACE PROIMP WITH this.w_IMPSTA, PROIVA WITH this.w_IVASTA, PROIVI WITH this.w_IVISTA
                this.w_TROV = .T.
              else
                DELETE
              endif
              ENDSCAN
              SELECT APPOREGI
              ENDSCAN
            endif
            if USED("APPOREGI")
              SELECT APPOREGI
              USE
            endif
          endif
        endif
        SELECT REGIVA
        GO TOP
        * --- Carica Cursore di Stampa Ordine su Tipo, Data Reg, Alfa Doc, Num.Doc, Data Doc.
        if USED("__tmp__")
          SELECT __tmp__
          USE
        endif
        * --- tolto 
        * --- Totali Progressivi di Riga
        SELECT REGIVA
        GO TOP
        SUM PROIMP TO L_ARIMP FOR NVL(TIPREC, " ")="R" AND NVL(PERIVA,0)<>0 
 SUM PROIMP TO L_ERIMP FOR NVL(TIPREC, " ")="R" AND NVL(PERIVA,0)=0 
 SUM PROIVA TO L_ARIVA FOR NVL(TIPREC, " ")="R" AND NVL(PERIVA,0)<>0 
 SUM PROIVA TO L_ERIVA FOR NVL(TIPREC, " ")="R" AND NVL(PERIVA,0)=0 
 SUM PROIVI TO L_ARIVI FOR NVL(TIPREC, " ")="R" AND NVL(PERIVA,0)<>0 
 SUM PROIVI TO L_ERIVI FOR NVL(TIPREC, " ")="R" AND NVL(PERIVA,0)=0
        if this.oParentObject.w_FLNOALI<>"S"
          SELECT REGIVA.*, " " AS POSIZ FROM REGIVA INTO CURSOR __TMP__ WHERE ORIGINE<>"C" ORDER BY 1,2,3,4,5
        else
          SELECT REGIVA.*, " " AS POSIZ FROM REGIVA INTO CURSOR __TMP__ WHERE ORIGINE<>"C" AND ((TIPREC<>"R") OR (TIPREC="R" AND (IMPSTA<>0 OR IMPPRE<>0 OR IMPSEG<>0 OR IMPFAD<>0 OR IMPIND<>0))) ORDER BY 1,2,3,4,5
        endif
        SELECT REGIVA
        USE
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        ah_ErrorMsg("Per il periodo impostato non ci sono dati da stampare",,"")
      endif
      * --- Stampa Definitiva: Aggiorna Ultima data di Stampa
      if this.oParentObject.w_FLDEFI="S" 
        this.w_CONFER = " "
        this.w_DATULT = this.oParentObject.w_DATFIN
        this.w_PAGINE = IIF(this.w_PAGINE=0,this.oParentObject.w_PRPARI,this.w_PAGINE)
        do GSCG_S2I with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_CONFER="S"
          this.oParentObject.w_PRPARI = this.w_PAGINE
          * --- Aggiorno il progressivo pagine solo se Attivo il check intestazione
          if this.oParentObject.w_INTLIG="S"
            * --- inserisco il progressivo pagina nelle Attivit�
            * --- Write into ATTIDETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ATTIDETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ATPRPARI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRPARI),'ATTIDETT','ATPRPARI');
                  +i_ccchkf ;
              +" where ";
                  +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                  +" and ATNUMREG = "+cp_ToStrODBC(this.oParentObject.w_NUMREG);
                  +" and ATTIPREG = "+cp_ToStrODBC(this.oParentObject.w_TIPREG);
                     )
            else
              update (i_cTable) set;
                  ATPRPARI = this.oParentObject.w_PRPARI;
                  &i_ccchkf. ;
               where;
                  ATCODATT = this.oParentObject.w_CODATT;
                  and ATNUMREG = this.oParentObject.w_NUMREG;
                  and ATTIPREG = this.oParentObject.w_TIPREG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- inserisco la data di stampa 
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATSTA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFIN),'ATTIDETT','ATDATSTA');
                +i_ccchkf ;
            +" where ";
                +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                +" and ATNUMREG = "+cp_ToStrODBC(this.oParentObject.w_NUMREG);
                +" and ATTIPREG = "+cp_ToStrODBC(this.oParentObject.w_TIPREG);
                   )
          else
            update (i_cTable) set;
                ATDATSTA = this.oParentObject.w_DATFIN;
                &i_ccchkf. ;
             where;
                ATCODATT = this.oParentObject.w_CODATT;
                and ATNUMREG = this.oParentObject.w_NUMREG;
                and ATTIPREG = this.oParentObject.w_TIPREG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Aggiorna Data Ultima Stampa
          this.oParentObject.w_ULTDAT = this.oParentObject.w_DATFIN
        endif
      endif
    endif
    return
  proc Try_04A1C910()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Blocca tutti i registri dell'Attivita'
    * --- Write into ATTIDETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ATTIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
          +i_ccchkf ;
      +" where ";
          +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
             )
    else
      update (i_cTable) set;
          ATDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          ATCODATT = this.oParentObject.w_CODATT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione Variabili di Lavoro
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Registri IVA Vendite/Acquisti
    * --- Controllo Progressivita' Documenti, Gestione eventuale altra valuta di conto, ecc.
    SELECT NVL(ALFDOC, "  ") AS ALFDOC FROM REGIVA ;
    WHERE NVL(TIPREC," ") = "F" ;
    GROUP BY ALFDOC ;
    INTO CURSOR SERDOC ORDER BY ALFDOC
    SELECT SERDOC
    if RECCOUNT()>0
      GO TOP
      SCAN
      this.w_ALFDOC = ALFDOC
      this.w_OLDNUM = -99988
      this.w_OKNUM = -99988
      this.w_OLDSER = " "
      this.w_DATDOC = cp_CharToDate("  -  -  ")
      this.w_OLDDAT = cp_CharToDate("  -  -  ")
      SELECT REGIVA
      GO TOP
      SCAN FOR( NVL(TIPREC, " ")="F" OR NVL(TIPREC, " ")="I" )AND NVL(ALFDOC,"  ")=this.w_ALFDOC
      this.w_SCRIVILOG = .F.
      this.w_NUMDOC = NVL(NUMDOC, 0)
      this.w_DATDOC = CP_TODATE(DATDOC)
      this.w_SERIAL = NVL(SERIAL, SPACE(10))
      * --- Flag Esig.Differita: F = Fattura Esig.Differita; I = Incasso/Pag.Es.Diff.; ' ' NO Esig.Diff.
      this.w_FLIVDF = IIF(NVL(FLPDIF," ")="S", "I", IIF(NVL(FLIVDF, " ")="S", "F", " "))
      if this.w_OLDNUM=-99988
        this.w_OLDNUM = this.w_NUMDOC
        this.w_OKNUM = this.w_NUMDOC
        this.w_OLDDAT = this.w_DATDOC
        this.w_OLDSER = this.w_SERIAL
      endif
      if this.w_FLIVDF<>"I" AND NOT (this.w_NUMDOC=this.w_OLDNUM+1 OR (this.w_NUMDOC=this.w_OLDNUM AND this.w_SERIAL=this.w_OLDSER))
        if this.w_NUMDOC<>this.w_OKNUM+1
          REPLACE FLPNUM WITH IIF(this.w_NUMDOC>this.w_OLDNUM, "B", "S")
          this.w_SCRIVILOG = .T.
          this.w_OKSEQ = .F.
        else
          this.w_OKNUM = this.w_NUMDOC
        endif
        this.w_OLDNUM = this.w_NUMDOC
      else
        if this.w_FLIVDF<>"I"
          * --- Per controllo progressivo Numero (tranne incasso a esig Differita)
          this.w_OLDNUM = this.w_NUMDOC
          this.w_OKNUM = this.w_NUMDOC
        endif
      endif
      if this.w_DATDOC<this.w_OLDDAT AND this.oParentObject.w_TIPREG="V" AND this.w_FLIVDF<>"I" and Nvl(TIPCLF," ")="C" 
        * --- Controlla la data solo se Registro Vendite e no Incasso a Esig.Differita
        *     e no vendite provenienti da INTRA o Reverse Charge
        REPLACE FLPDAT WITH "S"
        this.w_SCRIVILOG = .T.
        this.w_OKSEQ = .F.
      endif
      if this.w_FLIVDF<>"I"
        * --- Per controllo progressivo Data (tranne incasso a esig Differita)
        this.w_OLDDAT = this.w_DATDOC
        this.w_OLDSER = this.w_SERIAL
      endif
      this.w_DATREG = CP_TODATE(DATREG)
      this.w_COMIVA = IIF(EMPTY(CP_TODATE(COMIVA)), this.w_DATREG, CP_TODATE(COMIVA))
      * --- Inverte il Segno se Nota Credito
      this.w_IMPONI = NVL(IMPONI, 0) * IIF(NVL(TIPDOC,"  ") $ "NC-NE-NU", -1, 1)
      this.w_IMPIVA = NVL(IMPIVA, 0) * IIF(NVL(TIPDOC,"  ") $ "NC-NE-NU", -1, 1)
      * --- Se La valuta dell'esercizio della Riga e' diversa da quella dell' Esercizio Attuale, converte
      this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
      * --- Prevediamo solo LIRE o EURO
      if this.w_VALNAZ<>g_PERVAL AND GETCAM(g_PERVAL, i_DATSYS)<>0 
        this.w_IMPONI = VAL2MON(this.w_IMPONI,GETCAM(this.w_VALNAZ, I_DATSYS),1, i_DATSYS, this.w_VALNAZ, GETVALUT(this.w_VALNAZ, "VADECTOT"))
        this.w_IMPIVA = VAL2MON(this.w_IMPIVA,GETCAM(this.w_VALNAZ, I_DATSYS),1, i_DATSYS, this.w_VALNAZ, GETVALUT(this.w_VALNAZ, "VADECTOT"))
      endif
      * --- Se l'attivit� ha il flag sul prorata, leggo la percentuale
      if this.w_FLPROR="S"
        * --- Read from PRO_RATA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRO_RATA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRO_RATA_idx,2],.t.,this.PRO_RATA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AIPERPRO"+;
            " from "+i_cTable+" PRO_RATA where ";
                +"AICODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                +" and AI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AIPERPRO;
            from (i_cTable) where;
                AICODATT = this.oParentObject.w_CODATT;
                and AI__ANNO = this.oParentObject.w_ANNO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERPRO = NVL(cp_ToDate(_read_.AIPERPRO),cp_NullValue(_read_.AIPERPRO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows=0
          * --- Se non trova prorata relativo all'anno lo metto a 100 cos� rimane invariato.
          this.w_PERPRO = 100
        else
          this.w_PERPRO = this.w_PERPRO
        endif
      else
        this.w_PERPRO = 100
      endif
      * --- Calcola la Competenza
      this.w_COMPET = ALLTRIM(STR(this.oParentObject.w_NUMPER,2,0))+"-"+this.oParentObject.w_ANNO
      this.w_ANNO2 = ALLTRIM(STR(YEAR(this.w_COMIVA),4,0))
      this.w_NUMPER2 = IIF(g_TIPDEN="M", MONTH(this.w_COMIVA), INT((MONTH(this.w_COMIVA)+2)/3))
      do case
        case VAL(this.w_ANNO2)<VAL(this.oParentObject.w_ANNO) OR (this.w_NUMPER2<this.oParentObject.w_NUMPER AND this.w_ANNO2=this.oParentObject.w_ANNO)
          this.w_COMPET = ALLTRIM(STR(this.w_NUMPER2,2,0))+"-"+this.w_ANNO2
        case this.w_FLIVDF $ "I-F"
          this.w_COMPET = Nvl(COMPET,Space(9))
      endcase
      * --- Al termine riscrive gli Importi e il Flag Competenza
      REPLACE IMPONI WITH this.w_IMPONI
      REPLACE IMPIVA WITH this.w_IMPIVA
      REPLACE COMPET WITH this.w_COMPET
      this.w_LOGDAT = " "
      if NVL(TIPREC, " ")="F"
        if NVL(FLPROV, " ")="S"
          * --- Registrazione Provvisoria
          this.w_SCRIVILOG = .T.
        endif
        do case
          case CP_TODATE(DATREG)<CP_TODATE(COMIVA)
            * --- Se data Registrazione Minore della Data Competenza IVA segnala Errore
            this.w_SCRIVILOG = .T.
            this.w_LOGDAT = "A"
          case CP_TODATE(DATREG)>CP_TODATE(COMIVA)
            if this.oParentObject.w_TIPREG="A"
              * --- Se fatture passiva controllo che la competenza iva non sia < della data registrazione.
              this.w_SCRIVILOG = .T.
              this.w_LOGDAT = "C"
            else
              * --- Se data Registrazione Maggiore della Data Competenza IVA verifica che non ecceda il 15 del mese successivo di quest'ultima.
              this.w_APPO1 = MONTH(CP_TODATE(COMIVA)) + 1
              this.w_APPO2 = YEAR(CP_TODATE(COMIVA))
              if this.w_APPO1>12
                this.w_APPO1 = 1
                this.w_APPO2 = this.w_APPO2 + 1
              endif
              this.w_APPO1 = RIGHT("00" + ALLTRIM(STR(this.w_APPO1)), 2)
              this.w_APPO2 = RIGHT("0000" + ALLTRIM(STR(this.w_APPO2)), 4)
              this.w_APPO = cp_CharToDate("15-" + this.w_APPO1 + "-" + this.w_APPO2)
              if CP_TODATE(DATREG)>this.w_APPO
                this.w_SCRIVILOG = .T.
                this.w_LOGDAT = "B"
              endif
            endif
        endcase
      endif
      if USED("MessErr") AND this.w_SCRIVILOG=.T. AND REGIVA.ORIGINE<>"C"
        INSERT INTO MessErr ;
        (NUMDOC, ALFDOC, DATDOC, TIPCLF, CODCLF, DATREG, COMIVA, FLPROV, ;
        FLPNUM, FLPDAT, LOGDAT, PNNUMDOC, PNALFDOC, DESCLF) VALUES ;
        (NVL(REGIVA.NUMDOC, 0), NVL(REGIVA.ALFDOC, "  "), CP_TODATE(REGIVA.DATDOC), ;
        NVL(REGIVA.TIPCLF, " "), NVL(REGIVA.CODCLF, SPACE(15)), ;
        CP_TODATE(REGIVA.DATREG), CP_TODATE(REGIVA.COMIVA), ;
        NVL(REGIVA.FLPROV, " "), NVL(REGIVA.FLPNUM, " "), NVL(REGIVA.FLPDAT, " "), this.w_LOGDAT, ; 
        NVL(REGIVA.PNNUMDOC, 0), NVL(REGIVA.PNALFDOC, "  "), NVL(REGIVA.DESCLF, SPACE(40)))
        SELECT MessErr
        GO TOP
        SELECT REGIVA
      endif
      ENDSCAN
      SELECT SERDOC
      ENDSCAN
    endif
    if USED("SERDOC")
      SELECT SERDOC
      USE
    endif
    * --- Scrive i Saldi del Periodo
    SELECT REGIVA
    GO TOP
    SCAN FOR NVL(TIPREC, " ")="R" AND NOT EMPTY(NVL(CODIVA,"  "))
    if this.oParentObject.w_FLSTOT="S"
      this.w_CODIVA = CODIVA
      this.w_IMPSTA = 0
      this.w_IMPPRE = 0
      this.w_IVASTA = 0
      this.w_IVAPRE = 0
      this.w_IVISTA = 0
      this.w_IVIPRE = 0
      this.w_IMPSEG = 0
      this.w_IMPFAD = 0
      this.w_IVASEG = 0
      this.w_IVAFAD = 0
      this.w_IVISEG = 0
      this.w_IVIFAD = 0
      this.w_IMPIND = 0
      this.w_IVAIND = 0
      this.w_IVIIND = 0
      * --- Read from PRI_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRI_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRI_DETT_idx,2],.t.,this.PRI_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TRIMPSTA,TRIVASTA,TRIVISTA,TRIMPPRE,TRIVAPRE,TRIVIPRE,TRIMPSEG,TRIVASEG,TRIVISEG,TRIMPFAD,TRIVAFAD,TRIVIFAD,TRIMPIND,TRIVAIND,TRIVIIND"+;
          " from "+i_cTable+" PRI_DETT where ";
              +"TR__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
              +" and TRNUMPER = "+cp_ToStrODBC(this.oParentObject.w_NUMPER);
              +" and TRTIPREG = "+cp_ToStrODBC(this.oParentObject.w_TIPREG);
              +" and TRNUMREG = "+cp_ToStrODBC(this.oParentObject.w_NUMREG);
              +" and TRCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TRIMPSTA,TRIVASTA,TRIVISTA,TRIMPPRE,TRIVAPRE,TRIVIPRE,TRIMPSEG,TRIVASEG,TRIVISEG,TRIMPFAD,TRIVAFAD,TRIVIFAD,TRIMPIND,TRIVAIND,TRIVIIND;
          from (i_cTable) where;
              TR__ANNO = this.oParentObject.w_ANNO;
              and TRNUMPER = this.oParentObject.w_NUMPER;
              and TRTIPREG = this.oParentObject.w_TIPREG;
              and TRNUMREG = this.oParentObject.w_NUMREG;
              and TRCODIVA = this.w_CODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_IMPSTA = NVL(cp_ToDate(_read_.TRIMPSTA),cp_NullValue(_read_.TRIMPSTA))
        this.w_IVASTA = NVL(cp_ToDate(_read_.TRIVASTA),cp_NullValue(_read_.TRIVASTA))
        this.w_IVISTA = NVL(cp_ToDate(_read_.TRIVISTA),cp_NullValue(_read_.TRIVISTA))
        this.w_IMPPRE = NVL(cp_ToDate(_read_.TRIMPPRE),cp_NullValue(_read_.TRIMPPRE))
        this.w_IVAPRE = NVL(cp_ToDate(_read_.TRIVAPRE),cp_NullValue(_read_.TRIVAPRE))
        this.w_IVIPRE = NVL(cp_ToDate(_read_.TRIVIPRE),cp_NullValue(_read_.TRIVIPRE))
        this.w_IMPSEG = NVL(cp_ToDate(_read_.TRIMPSEG),cp_NullValue(_read_.TRIMPSEG))
        this.w_IVASEG = NVL(cp_ToDate(_read_.TRIVASEG),cp_NullValue(_read_.TRIVASEG))
        this.w_IVISEG = NVL(cp_ToDate(_read_.TRIVISEG),cp_NullValue(_read_.TRIVISEG))
        this.w_IMPFAD = NVL(cp_ToDate(_read_.TRIMPFAD),cp_NullValue(_read_.TRIMPFAD))
        this.w_IVAFAD = NVL(cp_ToDate(_read_.TRIVAFAD),cp_NullValue(_read_.TRIVAFAD))
        this.w_IVIFAD = NVL(cp_ToDate(_read_.TRIVIFAD),cp_NullValue(_read_.TRIVIFAD))
        this.w_IMPIND = NVL(cp_ToDate(_read_.TRIMPIND),cp_NullValue(_read_.TRIMPIND))
        this.w_IVAIND = NVL(cp_ToDate(_read_.TRIVAIND),cp_NullValue(_read_.TRIVAIND))
        this.w_IVIIND = NVL(cp_ToDate(_read_.TRIVIIND),cp_NullValue(_read_.TRIVIIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.oParentObject.w_TIPREG="A" AND FLVAFF <>"N" AND MACIVA="S"
        this.w_IVASTA = 0
        this.w_IVAPRE = 0
        this.w_IVISTA = 0
        this.w_IVIPRE = 0
        this.w_IVASEG = 0
        this.w_IVAFAD = 0
        this.w_IVISEG = 0
        this.w_IVIFAD = 0
        this.w_IVAIND = 0
        this.w_IVIIND = 0
      endif
      SELECT REGIVA
      * --- Aggiorna il Cursore dei Valori Letti
      REPLACE IMPSTA WITH this.w_IMPSTA, IVASTA WITH this.w_IVASTA, IVISTA WITH this.w_IVISTA
      REPLACE IMPPRE WITH this.w_IMPPRE, IVAPRE WITH this.w_IVAPRE, IVIPRE WITH this.w_IVIPRE
      REPLACE IMPSEG WITH this.w_IMPSEG, IVASEG WITH this.w_IVASEG, IVISEG WITH this.w_IVISEG
      REPLACE IMPFAD WITH this.w_IMPFAD, IVAFAD WITH this.w_IVAFAD, IVIFAD WITH this.w_IVIFAD
      REPLACE IMPIND WITH this.w_IMPIND, IVAIND WITH this.w_IVAIND, IVIIND WITH this.w_IVIIND
    else
      DELETE
    endif
    ENDSCAN
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Registri IVA
    this.w_NOMREP = " "
    if this.oParentObject.w_STAREG="O"
      * --- Stampa Orizzontale
      do case
        case this.oParentObject.w_TIPREG="V"
          this.w_NOMREP = IIF(this.oParentObject.w_FLTEST="S", "REGVEN_O.FXP", "GSCG_OSRV.FRX")
        case this.oParentObject.w_TIPREG="A"
          this.w_NOMREP = IIF(this.oParentObject.w_FLTEST="S", "REGACQ_O.FXP", "GSCG_OSRA.FRX")
        case this.oParentObject.w_TIPREG="C"
          this.w_NOMREP = IIF(this.oParentObject.w_FLTEST="S", "REGCOS_O.FXP", "GSCG_OSRC.FRX")
        case this.oParentObject.w_TIPREG="E"
          this.w_NOMREP = IIF(this.oParentObject.w_FLTEST="S", "REGCOV_O.FXP", "GSCG_OSRE.FRX")
      endcase
    else
      * --- Stampa Verticale
      do case
        case this.oParentObject.w_TIPREG="V"
          this.w_NOMREP = IIF(this.oParentObject.w_FLTEST="S", "REGVEN_V.FXP", "GSCG_SRV.FRX")
        case this.oParentObject.w_TIPREG="A"
          this.w_NOMREP = IIF(this.oParentObject.w_FLTEST="S", "REGACQ_V.FXP", "GSCG_SRA.FRX")
        case this.oParentObject.w_TIPREG="C"
          this.w_NOMREP = IIF(this.oParentObject.w_FLTEST="S", "REGCOS_V.FXP", "GSCG_SRC.FRX")
        case this.oParentObject.w_TIPREG="E"
          this.w_NOMREP = IIF(this.oParentObject.w_FLTEST="S", "REGCOV_V.FXP", "GSCG_SRE.FRX")
      endcase
    endif
    if this.oParentObject.w_TIPREG="C" and this.oParentObject.w_RAGCOR="S"
      * --- Raggruppamento per data registrazione
      Select tiprec, datreg, min(serial) as serial, descri, tipdoc, sum(imponi) as imponi, sum(impiva) as impiva, codiva, periva, desiva, ; 
 min(comiva) as comiva, valnaz, flprov, compet, cprown, sum(impsta) as impsta, sum(ivasta) as ivasta, sum(ivista) as ivista, ; 
 sum(imppre) as imppre, sum(ivapre) as ivapre, sum(ivipre) as ivipre, sum(impseg) as impseg, sum(ivaseg) as ivaseg, ; 
 sum(iviseg) as iviseg, sum(proimp) as proimp, sum(proiva) as proiva, sum(proivi) as proivi, origine, posiz ; 
 from __tmp__ group by tiprec, datreg, descri, tipdoc, codiva, valnaz, flprov, compet into cursor __tmp__
    endif
    * --- Determino totale Monte Acquisti per il calcol delle percentuali nel report
    this.w_TOTMAC = 0
    * --- Select from PRI_DETT
    i_nConn=i_TableProp[this.PRI_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRI_DETT_idx,2],.t.,this.PRI_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PRI_DETT ";
          +" where TR__ANNO= "+cp_ToStrODBC(this.oParentObject.w_ANNO)+" AND TRNUMREG = "+cp_ToStrODBC(this.oParentObject.w_NUMREG)+" AND TRTIPREG = "+cp_ToStrODBC(this.oParentObject.w_TIPREG)+" AND TRNUMPER = "+cp_ToStrODBC(this.oParentObject.w_NUMPER)+"";
           ,"_Curs_PRI_DETT")
    else
      select * from (i_cTable);
       where TR__ANNO= this.oParentObject.w_ANNO AND TRNUMREG = this.oParentObject.w_NUMREG AND TRTIPREG = this.oParentObject.w_TIPREG AND TRNUMPER = this.oParentObject.w_NUMPER;
        into cursor _Curs_PRI_DETT
    endif
    if used('_Curs_PRI_DETT')
      select _Curs_PRI_DETT
      locate for 1=1
      do while not(eof())
      if (_Curs_PRI_DETT.TRMACSTA + _Curs_PRI_DETT.TRMACSEG + _Curs_PRI_DETT.TRMACIND) - ( _Curs_PRI_DETT.TRMACPRE + _Curs_PRI_DETT.TRMACFAD)>=0
        this.w_TOTMAC = (this.w_TOTMAC + (_Curs_PRI_DETT.TRMACSTA + _Curs_PRI_DETT.TRMACSEG + _Curs_PRI_DETT.TRMACIND) - ( _Curs_PRI_DETT.TRMACPRE + _Curs_PRI_DETT.TRMACFAD))
      else
        this.w_TOTMAC = this.w_TOTMAC + 0
      endif
        select _Curs_PRI_DETT
        continue
      enddo
      use
    endif
    this.w_ELENCO = ""
    if this.oParentObject.w_TIPREG="C"
      * --- Se stampa Corrispettivi/Scorporo, Determino numeri documento delle FC del periodo
      * --- Select from PNT_MAST
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PNNUMDOC,PNALFDOC,PNDATREG  from "+i_cTable+" PNT_MAST ";
            +" where PNDATREG >= "+cp_ToStrODBC(this.oParentObject.w_DATINI)+" AND PNDATREG <= "+cp_ToStrODBC(this.oParentObject.w_DATFIN)+" AND PNTIPDOC ='FC' AND PNNUMREG = "+cp_ToStrODBC(this.oParentObject.w_NUMREG)+" AND PNTIPREG = "+cp_ToStrODBC(this.oParentObject.w_TIPREG)+" AND PNNUMDOC<>0";
            +" order by PNNUMDOC";
             ,"_Curs_PNT_MAST")
      else
        select PNNUMDOC,PNALFDOC,PNDATREG from (i_cTable);
         where PNDATREG >= this.oParentObject.w_DATINI AND PNDATREG <= this.oParentObject.w_DATFIN AND PNTIPDOC ="FC" AND PNNUMREG = this.oParentObject.w_NUMREG AND PNTIPREG = this.oParentObject.w_TIPREG AND PNNUMDOC<>0;
         order by PNNUMDOC;
          into cursor _Curs_PNT_MAST
      endif
      if used('_Curs_PNT_MAST')
        select _Curs_PNT_MAST
        locate for 1=1
        do while not(eof())
        if Alltrim(Str(Year(_Curs_PNT_MAST.PNDATREG))) = this.oParentObject.w_ANNO
          this.w_ELENCO = this.w_ELENCO + IIF(EMPTY(this.w_ELENCO), "", ", ") + ALLTRIM(STR(_Curs_PNT_MAST.PNNUMDOC,15))
          if NOT EMPTY(NVL(_Curs_PNT_MAST.PNALFDOC, ""))
            this.w_ELENCO = this.w_ELENCO + "/"+ALLTRIM(_Curs_PNT_MAST.PNALFDOC)
          endif
        endif
          select _Curs_PNT_MAST
          continue
        enddo
        use
      endif
    endif
    * --- Variabile per la Stampa Acquisti (percentuale Prorata)
    L_PERPRO=this.w_PERPRO
    L_FLPROR=this.w_FLPROR
    * --- Variabile per la Stampa Corrispettivi Ventilazione 
    L_TOTMAC = this.w_TOTMAC
    if this.oParentObject.w_TIPREG="E" 
       
 SELECT __TMP__.*,iif(L_totmac<>0 And ((MACSTA+MACSEG+MACIND)-(MACPRE+MACFAD))>=0,cp_Round(((MACSTA+MACSEG+MACIND)-(MACPRE+MACFAD))*(100/L_TOTMAC),6),MACPRE*0) as PERCEN FROM __TMP__ INTO CURSOR __TMP__ WHERE ORIGINE<>"C" ORDER BY 1,2,3,4,5
      if L_totmac<>0
         
 select MAX(CODIVA) as CODIVA ,TIPREC,Sum(PERCEN) as TOTPREC from __TMP__ Group by TIPREC where TIPREC ="R" into cursor TMP_PERC
        this.w_DIFPERC = 100-TMP_PERC.TOTPREC
        =WRCURSOR("__TMP__")
        if this.w_DIFPERC<>0
          Select __TMP__ 
 Go Top 
 Locate For TIPREC="R"
          * --- Metto sulla prima riga di tipo R l'eventuale resto
          if Found()
            Replace PERCEN with Nvl(PERCEN,0) + this.w_DIFPERC 
          endif
        endif
         
 Use in TMP_PERC
      endif
    endif
    * --- Variabile per la Stampa Corrispettivi Scorporo 
    L_ELENCO = this.w_ELENCO
    * --- Variabili da passare al Report
    L_FLDEFI = this.oParentObject.w_FLDEFI
    L_NUMREG = this.oParentObject.w_NUMREG
    L_NUMPER = this.oParentObject.w_NUMPER
    L_DATINI = this.oParentObject.w_DATINI
    L_DATFIN = this.oParentObject.w_DATFIN
    L_CREINI = 0
    * --- PER NUMERAZIONE PAGINE IN TESTATA
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
      this.w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
      this.w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
      this.w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
      this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    L_PAGINE=0
    L_PRPARI=this.oParentObject.w_PRPARI
    L_CODESE=this.oParentObject.w_CODESE
    L_INTLIG=this.oParentObject.w_INTLIG
    L_PREFIS=this.oParentObject.w_PREFIS
    L_INDAZI=this.w_INDAZI
    L_LOCAZI=this.w_LOCAZI
    L_CAPAZI=this.w_CAPAZI
    L_PROAZI=this.w_PROAZI
    L_COFAZI=this.w_COFAZI
    L_PIVAZI=this.w_PIVAZI
    if this.oParentObject.w_TIPREG="A"
      * --- Stampa anche il Credito IVA Anno Precedente
      this.w_APPO = 0
      this.w_APPO1 = g_PERVAL
      * --- Read from CREDMIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CREDMIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CREDMIVA_idx,2],.t.,this.CREDMIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CICREINI,CIVALINI"+;
          " from "+i_cTable+" CREDMIVA where ";
              +"CI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CICREINI,CIVALINI;
          from (i_cTable) where;
              CI__ANNO = this.oParentObject.w_ANNO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_APPO = NVL(cp_ToDate(_read_.CICREINI),cp_NullValue(_read_.CICREINI))
        this.w_APPO1 = NVL(cp_ToDate(_read_.CIVALINI),cp_NullValue(_read_.CIVALINI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_APPO<>0 AND this.w_APPO1<>g_PERVAL AND GETCAM(g_PERVAL, i_DATSYS)<>0 
        this.w_APPO = VAL2MON(this.w_APPO,GETCAM(this.w_APPO1, I_DATSYS),1, i_DATSYS, this.w_APPO1, GETVALUT(this.w_APPO1, "VADECTOT"))
      endif
      L_CREINI = this.w_APPO
    endif
    this.w_APPO = "  "
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADESVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADESVAL;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APPO = NVL(cp_ToDate(_read_.VADESVAL),cp_NullValue(_read_.VADESVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    L_DESVAL = this.w_APPO
    * --- Esegue la Stampa
    if NOT EMPTY(this.w_NOMREP)
      PUBLIC STPAG
      SELECT __TMP__
      if this.oParentObject.w_FLTEST="S"
        * --- Solo Testo
        if this.oParentObject.w_TIPREG $ "VA"
          =WRCURSOR("__TMP__")
          GO BOTTOM
          this.w_APPO = "####"
          do while NOT BOF()
            * --- Inserisce check per Righe Ripetute
            if TIPREC+DTOS(CP_TODATE(DATREG))+ALFDOC+STR(NUMDOC,15,0)+DTOS(CP_TODATE(DATDOC))=this.w_APPO
            else
              REPLACE POSIZ WITH "U"
              this.w_APPO = TIPREC+DTOS(CP_TODATE(DATREG))+ALFDOC+STR(NUMDOC,15,0)+DTOS(CP_TODATE(DATDOC))
            endif
            SKIP -1
          enddo
          GO TOP
          this.w_APPO = "####"
          do while NOT EOF()
            * --- Inserisce check per Righe Ripetute
            if TIPREC+DTOS(CP_TODATE(DATREG))+ALFDOC+STR(NUMDOC,15,0)+DTOS(CP_TODATE(DATDOC))=this.w_APPO
            else
              REPLACE POSIZ WITH IIF(POSIZ="U", "X", "P")
              this.w_APPO = TIPREC+DTOS(CP_TODATE(DATREG))+ALFDOC+STR(NUMDOC,15,0)+DTOS(CP_TODATE(DATDOC))
            endif
            SKIP
          enddo
        endif
        STPAG = this.oParentObject.w_PRPARI
        GO TOP
        CP_CHPRN(this.w_NOMREP, "S", this)
        this.w_PAGINE = STPAG
      else
        STPAG=0
        GO TOP
        CP_CHPRN("QUERY\" + this.w_NOMREP, " ", this)
        this.w_PAGINE = STPAG+this.oParentObject.w_PRPARI
      endif
      STPAG=0
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Registri IVA Corrispettivi/Scorporo
    * --- Controllo Gestione eventuale altra valuta di conto, ecc.
    SELECT REGIVA
    GO TOP
    SCAN FOR NVL(TIPREC, " ")="F"
    this.w_DATREG = CP_TODATE(DATREG)
    this.w_COMIVA = IIF(EMPTY(CP_TODATE(COMIVA)), this.w_DATREG, CP_TODATE(COMIVA))
    * --- Inverte il Segno se Nota Credito
    this.w_IMPONI = NVL(IMPONI, 0) * IIF(NVL(TIPDOC,"  ") $ "NC-NE-NU", -1, 1)
    this.w_IMPIVA = NVL(IMPIVA, 0) * IIF(NVL(TIPDOC,"  ") $ "NC-NE", -1, 1)
    this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
    * --- Ricavo Descrizione Prima Contropartita di Vendita da Conti
    this.w_SERIAL = SERIAL
    VQ_EXEC("QUERY\GSCG_DES.VQR",this,"TEMP")
    this.w_DESCRI = TEMP.DESCRI
    SELECT REGIVA
    * --- Se La valuta dell'esercizio della Riga e' diversa da quella dell' Esercizio Attuale, converte
    * --- Prevediamo solo LIRE o EURO
    if this.w_VALNAZ<>g_PERVAL AND GETCAM(g_PERVAL, I_DATSYS)<>0 
      this.w_IMPONI = VAL2MON(this.w_IMPONI,GETCAM(this.w_VALNAZ, I_DATSYS),1, i_DATSYS, this.w_VALNAZ, GETVALUT(this.w_VALNAZ, "VADECTOT"))
      this.w_IMPIVA = VAL2MON(this.w_IMPIVA,GETCAM(this.w_VALNAZ, I_DATSYS),1, i_DATSYS, this.w_VALNAZ, GETVALUT(this.w_VALNAZ, "VADECTOT"))
    endif
    * --- Calcola la Competenza
    this.w_ANNO2 = ALLTRIM(STR(YEAR(this.w_COMIVA),4,0))
    this.w_NUMPER2 = IIF(g_TIPDEN="M", MONTH(this.w_COMIVA), INT((MONTH(this.w_COMIVA)+2)/3))
    this.w_COMPET = ALLTRIM(STR(this.w_NUMPER2,2,0))+"-"+this.w_ANNO2
    REPLACE IMPONI WITH this.w_IMPONI
    REPLACE IMPIVA WITH this.w_IMPIVA
    REPLACE DESCRI WITH this.w_DESCRI
    REPLACE COMPET WITH this.w_COMPET
    ENDSCAN
    if USED("TEMP")
      USE IN TEMP
    endif
    * --- Scrive i Saldi del Periodo
    SELECT REGIVA
    GO TOP
    SCAN FOR NVL(TIPREC, " ")="R" AND NOT EMPTY(NVL(CODIVA,"  "))
    if this.oParentObject.w_FLSTOT="S"
      this.w_CODIVA = CODIVA
      this.w_IMPSTA = 0
      this.w_IVASTA = 0
      this.w_IVISTA = 0
      this.w_IMPSEG = 0
      this.w_IVASEG = 0
      this.w_IVISEG = 0
      this.w_IMPPRE = 0
      this.w_IVAPRE = 0
      this.w_IVIPRE = 0
      * --- Read from PRI_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRI_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRI_DETT_idx,2],.t.,this.PRI_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TRIMPSTA,TRIVASTA,TRIVISTA,TRIMPPRE,TRIVAPRE,TRIVIPRE,TRIMPSEG,TRIVASEG,TRIVISEG"+;
          " from "+i_cTable+" PRI_DETT where ";
              +"TR__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
              +" and TRNUMPER = "+cp_ToStrODBC(this.oParentObject.w_NUMPER);
              +" and TRTIPREG = "+cp_ToStrODBC(this.oParentObject.w_TIPREG);
              +" and TRNUMREG = "+cp_ToStrODBC(this.oParentObject.w_NUMREG);
              +" and TRCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TRIMPSTA,TRIVASTA,TRIVISTA,TRIMPPRE,TRIVAPRE,TRIVIPRE,TRIMPSEG,TRIVASEG,TRIVISEG;
          from (i_cTable) where;
              TR__ANNO = this.oParentObject.w_ANNO;
              and TRNUMPER = this.oParentObject.w_NUMPER;
              and TRTIPREG = this.oParentObject.w_TIPREG;
              and TRNUMREG = this.oParentObject.w_NUMREG;
              and TRCODIVA = this.w_CODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_IMPSTA = NVL(cp_ToDate(_read_.TRIMPSTA),cp_NullValue(_read_.TRIMPSTA))
        this.w_IVASTA = NVL(cp_ToDate(_read_.TRIVASTA),cp_NullValue(_read_.TRIVASTA))
        this.w_IVISTA = NVL(cp_ToDate(_read_.TRIVISTA),cp_NullValue(_read_.TRIVISTA))
        this.w_IMPPRE = NVL(cp_ToDate(_read_.TRIMPPRE),cp_NullValue(_read_.TRIMPPRE))
        this.w_IVAPRE = NVL(cp_ToDate(_read_.TRIVAPRE),cp_NullValue(_read_.TRIVAPRE))
        this.w_IVIPRE = NVL(cp_ToDate(_read_.TRIVIPRE),cp_NullValue(_read_.TRIVIPRE))
        this.w_IMPSEG = NVL(cp_ToDate(_read_.TRIMPSEG),cp_NullValue(_read_.TRIMPSEG))
        this.w_IVASEG = NVL(cp_ToDate(_read_.TRIVASEG),cp_NullValue(_read_.TRIVASEG))
        this.w_IVISEG = NVL(cp_ToDate(_read_.TRIVISEG),cp_NullValue(_read_.TRIVISEG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT REGIVA
      * --- Aggiorna il Cursore dei Valori Letti
      REPLACE IMPSTA WITH this.w_IMPSTA, IVASTA WITH this.w_IVASTA, IVISTA WITH this.w_IVISTA
      REPLACE IMPPRE WITH this.w_IMPPRE, IVAPRE WITH this.w_IVAPRE, IVIPRE WITH this.w_IVIPRE
      REPLACE IMPSEG WITH this.w_IMPSEG, IVASEG WITH this.w_IVASEG, IVISEG WITH this.w_IVISEG
    else
      DELETE
    endif
    ENDSCAN
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Registri IVA Corrispettivi/Ventilazione
    * --- Controllo Gestione eventuale altra valuta di conto, ecc.
    SELECT REGIVA
    GO TOP
    SCAN FOR NVL(TIPREC, " ")="F"
    this.w_DATREG = CP_TODATE(DATREG)
    this.w_COMIVA = IIF(EMPTY(CP_TODATE(COMIVA)), this.w_DATREG, CP_TODATE(COMIVA))
    this.w_IMPONI = NVL(IMPONI, 0)
    this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
    * --- Se La valuta dell'esercizio della Riga e' diversa da quella dell' Esercizio Attuale, converte
    * --- Prevediamo solo LIRE o EURO
    if this.w_VALNAZ<>g_PERVAL AND GETCAM(g_PERVAL, I_DATSYS)<>0 
      this.w_IMPONI = VAL2MON(this.w_IMPONI,GETCAM(this.w_VALNAZ, I_DATSYS),1, i_DATSYS, this.w_VALNAZ, GETVALUT(this.w_VALNAZ, "VADECTOT"))
      REPLACE IMPONI WITH this.w_IMPONI
    endif
    * --- Calcola la Competenza
    this.w_ANNO2 = ALLTRIM(STR(YEAR(this.w_COMIVA),4,0))
    this.w_NUMPER2 = IIF(g_TIPDEN="M", MONTH(this.w_COMIVA), INT((MONTH(this.w_COMIVA)+2)/3))
    this.w_COMPET = ALLTRIM(STR(this.w_NUMPER2,2,0))+"-"+this.w_ANNO2
    REPLACE COMPET WITH this.w_COMPET
    ENDSCAN
    * --- Scrive i Saldi del Periodo
    SELECT REGIVA
    GO TOP
    SCAN FOR NVL(TIPREC, " ")="R" AND NOT EMPTY(NVL(CODIVA,"  "))
    if this.oParentObject.w_FLSTOT="S"
      this.w_CODIVA = CODIVA
      this.w_IMPSTA = 0
      this.w_IVASTA = 0
      this.w_IVISTA = 0
      this.w_MACSTA = 0
      this.w_IMPPRE = 0
      this.w_IVAPRE = 0
      this.w_IVIPRE = 0
      this.w_MACPRE = 0
      this.w_IMPSEG = 0
      this.w_IVASEG = 0
      this.w_IVISEG = 0
      this.w_MACSEG = 0
      this.w_IMPFAD = 0
      this.w_IVAFAD = 0
      this.w_IVIFAD = 0
      this.w_MACFAD = 0
      this.w_IMPIND = 0
      this.w_IVAIND = 0
      this.w_IVIIND = 0
      this.w_MACIND = 0
      * --- Read from PRI_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRI_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRI_DETT_idx,2],.t.,this.PRI_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TRIMPSTA,TRIVASTA,TRIVISTA,TRIMPPRE,TRIVAPRE,TRIVIPRE,TRIMPSEG,TRIVASEG,TRIVISEG,TRIMPFAD,TRIVAFAD,TRIVIFAD,TRIMPIND,TRIVAIND,TRIVIIND,TRMACSTA,TRMACPRE,TRMACSEG,TRMACFAD,TRMACIND"+;
          " from "+i_cTable+" PRI_DETT where ";
              +"TR__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
              +" and TRNUMPER = "+cp_ToStrODBC(this.oParentObject.w_NUMPER);
              +" and TRTIPREG = "+cp_ToStrODBC(this.oParentObject.w_TIPREG);
              +" and TRNUMREG = "+cp_ToStrODBC(this.oParentObject.w_NUMREG);
              +" and TRCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TRIMPSTA,TRIVASTA,TRIVISTA,TRIMPPRE,TRIVAPRE,TRIVIPRE,TRIMPSEG,TRIVASEG,TRIVISEG,TRIMPFAD,TRIVAFAD,TRIVIFAD,TRIMPIND,TRIVAIND,TRIVIIND,TRMACSTA,TRMACPRE,TRMACSEG,TRMACFAD,TRMACIND;
          from (i_cTable) where;
              TR__ANNO = this.oParentObject.w_ANNO;
              and TRNUMPER = this.oParentObject.w_NUMPER;
              and TRTIPREG = this.oParentObject.w_TIPREG;
              and TRNUMREG = this.oParentObject.w_NUMREG;
              and TRCODIVA = this.w_CODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_IMPSTA = NVL(cp_ToDate(_read_.TRIMPSTA),cp_NullValue(_read_.TRIMPSTA))
        this.w_IVASTA = NVL(cp_ToDate(_read_.TRIVASTA),cp_NullValue(_read_.TRIVASTA))
        this.w_IVISTA = NVL(cp_ToDate(_read_.TRIVISTA),cp_NullValue(_read_.TRIVISTA))
        this.w_IMPPRE = NVL(cp_ToDate(_read_.TRIMPPRE),cp_NullValue(_read_.TRIMPPRE))
        this.w_IVAPRE = NVL(cp_ToDate(_read_.TRIVAPRE),cp_NullValue(_read_.TRIVAPRE))
        this.w_IVIPRE = NVL(cp_ToDate(_read_.TRIVIPRE),cp_NullValue(_read_.TRIVIPRE))
        this.w_IMPSEG = NVL(cp_ToDate(_read_.TRIMPSEG),cp_NullValue(_read_.TRIMPSEG))
        this.w_IVASEG = NVL(cp_ToDate(_read_.TRIVASEG),cp_NullValue(_read_.TRIVASEG))
        this.w_IVISEG = NVL(cp_ToDate(_read_.TRIVISEG),cp_NullValue(_read_.TRIVISEG))
        this.w_IMPFAD = NVL(cp_ToDate(_read_.TRIMPFAD),cp_NullValue(_read_.TRIMPFAD))
        this.w_IVAFAD = NVL(cp_ToDate(_read_.TRIVAFAD),cp_NullValue(_read_.TRIVAFAD))
        this.w_IVIFAD = NVL(cp_ToDate(_read_.TRIVIFAD),cp_NullValue(_read_.TRIVIFAD))
        this.w_IMPIND = NVL(cp_ToDate(_read_.TRIMPIND),cp_NullValue(_read_.TRIMPIND))
        this.w_IVAIND = NVL(cp_ToDate(_read_.TRIVAIND),cp_NullValue(_read_.TRIVAIND))
        this.w_IVIIND = NVL(cp_ToDate(_read_.TRIVIIND),cp_NullValue(_read_.TRIVIIND))
        this.w_MACSTA = NVL(cp_ToDate(_read_.TRMACSTA),cp_NullValue(_read_.TRMACSTA))
        this.w_MACPRE = NVL(cp_ToDate(_read_.TRMACPRE),cp_NullValue(_read_.TRMACPRE))
        this.w_MACSEG = NVL(cp_ToDate(_read_.TRMACSEG),cp_NullValue(_read_.TRMACSEG))
        this.w_MACFAD = NVL(cp_ToDate(_read_.TRMACFAD),cp_NullValue(_read_.TRMACFAD))
        this.w_MACIND = NVL(cp_ToDate(_read_.TRMACIND),cp_NullValue(_read_.TRMACIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT REGIVA
      * --- Aggiorna il Cursore dei Valori Letti
      REPLACE IMPSTA WITH this.w_IMPSTA, IVASTA WITH this.w_IVASTA, IVISTA WITH this.w_IVISTA, MACSTA WITH this.w_MACSTA
      REPLACE IMPPRE WITH this.w_IMPPRE, IVAPRE WITH this.w_IVAPRE, IVIPRE WITH this.w_IVIPRE, MACPRE WITH this.w_MACPRE
      REPLACE IMPSEG WITH this.w_IMPSEG, IVASEG WITH this.w_IVASEG, IVISEG WITH this.w_IVISEG, MACSEG WITH this.w_MACSEG
      REPLACE IMPFAD WITH this.w_IMPFAD, IVAFAD WITH this.w_IVAFAD, IVIFAD WITH this.w_IVIFAD, MACFAD WITH this.w_MACFAD
      REPLACE IMPIND WITH this.w_IMPIND, IVAIND WITH this.w_IVAIND, IVIIND WITH this.w_IVIIND, MACIND WITH this.w_MACIND
    else
      DELETE
    endif
    ENDSCAN
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='ATTIDETT'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='CREDMIVA'
    this.cWorkTables[5]='PNT_DETT'
    this.cWorkTables[6]='PNT_IVA'
    this.cWorkTables[7]='PNT_MAST'
    this.cWorkTables[8]='PRI_DETT'
    this.cWorkTables[9]='PRI_MAST'
    this.cWorkTables[10]='VALUTE'
    this.cWorkTables[11]='ATTIMAST'
    this.cWorkTables[12]='PRO_RATA'
    this.cWorkTables[13]='DAT_IVAN'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_CONTAREG')
      use in _Curs_CONTAREG
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_PRI_DETT')
      use in _Curs_PRI_DETT
    endif
    if used('_Curs_PNT_MAST')
      use in _Curs_PNT_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
