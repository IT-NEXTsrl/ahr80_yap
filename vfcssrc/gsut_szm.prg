* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_szm                                                        *
*              Generazione massiva xml descrittore dei documenti               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-11-26                                                      *
* Last revis.: 2016-05-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_szm",oParentObject))

* --- Class definition
define class tgsut_szm as StdForm
  Top    = 0
  Left   = 3

  * --- Standard Properties
  Width  = 795
  Height = 540+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-13"
  HelpContextID=26655895
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=55

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  CONTI_IDX = 0
  ESERCIZI_IDX = 0
  TIP_DOCU_IDX = 0
  PROMCLAS_IDX = 0
  cPrg = "gsut_szm"
  cComment = "Generazione massiva xml descrittore dei documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_FLVEAC = space(1)
  o_FLVEAC = space(1)
  w_TIPOIN = space(5)
  o_TIPOIN = space(5)
  w_DESDOC = space(35)
  w_TCATDOC = space(2)
  w_TFLVEAC = space(1)
  w_ORDINE = 0
  o_ORDINE = 0
  w_ORDERBY_GSUT_SZM = space(10)
  w_CLADOCDI = space(2)
  w_CLADOCOR = space(2)
  w_CLADOCDT = space(2)
  w_CLADOCFA = space(2)
  w_CLADOCNC = space(2)
  w_CLADOCOP = space(2)
  w_CLADOCRF = space(2)
  w_NORDERBY = 0
  w_SERIALE = space(10)
  w_PARAME = space(3)
  w_DESAPP = space(30)
  w_SERDOCU = space(10)
  w_OKDELFLT = .F.
  w_REPSEC = space(1)
  w_CLAMOD = space(1)
  w_CODUTE = 0
  w_ARCHIVIO = space(20)
  w_CLASSE = space(15)
  w_DESCRI = space(40)
  w_MODALLEG = space(1)
  w_CODESE = space(4)
  o_CODESE = space(4)
  w_data1 = ctod('  /  /  ')
  w_data2 = ctod('  /  /  ')
  w_datain = ctod('  /  /  ')
  w_numini = 0
  w_SERIE1 = space(10)
  w_datafi = ctod('  /  /  ')
  w_numfin = 0
  w_SERIE2 = space(10)
  w_DATINI = ctod('  /  /  ')
  w_PROTINI = 0
  w_SEPROT1 = space(10)
  w_DATFIN = ctod('  /  /  ')
  w_PROTFIN = 0
  w_SEPROT2 = space(10)
  w_MAGCLI = space(1)
  w_CLISEL = space(15)
  w_DESCLI = space(40)
  w_MAGFOR = space(1)
  w_FORSEL = space(15)
  w_DESFOR = space(40)
  w_MAGALT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CDTIPOBC = space(1)
  w_CDREPOBC = space(254)
  w_CDRIFTAB = space(20)
  w_ZoomDoc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_szmPag1","gsut_szm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsut_szmPag2","gsut_szm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLVEAC_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDoc = this.oPgFrm.Pages(1).oPag.ZoomDoc
    DoDefault()
    proc Destroy()
      this.w_ZoomDoc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='TIP_DOCU'
    this.cWorkTables[5]='PROMCLAS'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_FLVEAC=space(1)
      .w_TIPOIN=space(5)
      .w_DESDOC=space(35)
      .w_TCATDOC=space(2)
      .w_TFLVEAC=space(1)
      .w_ORDINE=0
      .w_ORDERBY_GSUT_SZM=space(10)
      .w_CLADOCDI=space(2)
      .w_CLADOCOR=space(2)
      .w_CLADOCDT=space(2)
      .w_CLADOCFA=space(2)
      .w_CLADOCNC=space(2)
      .w_CLADOCOP=space(2)
      .w_CLADOCRF=space(2)
      .w_NORDERBY=0
      .w_SERIALE=space(10)
      .w_PARAME=space(3)
      .w_DESAPP=space(30)
      .w_SERDOCU=space(10)
      .w_OKDELFLT=.f.
      .w_REPSEC=space(1)
      .w_CLAMOD=space(1)
      .w_CODUTE=0
      .w_ARCHIVIO=space(20)
      .w_CLASSE=space(15)
      .w_DESCRI=space(40)
      .w_MODALLEG=space(1)
      .w_CODESE=space(4)
      .w_data1=ctod("  /  /  ")
      .w_data2=ctod("  /  /  ")
      .w_datain=ctod("  /  /  ")
      .w_numini=0
      .w_SERIE1=space(10)
      .w_datafi=ctod("  /  /  ")
      .w_numfin=0
      .w_SERIE2=space(10)
      .w_DATINI=ctod("  /  /  ")
      .w_PROTINI=0
      .w_SEPROT1=space(10)
      .w_DATFIN=ctod("  /  /  ")
      .w_PROTFIN=0
      .w_SEPROT2=space(10)
      .w_MAGCLI=space(1)
      .w_CLISEL=space(15)
      .w_DESCLI=space(40)
      .w_MAGFOR=space(1)
      .w_FORSEL=space(15)
      .w_DESFOR=space(40)
      .w_MAGALT=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CDTIPOBC=space(1)
      .w_CDREPOBC=space(254)
      .w_CDRIFTAB=space(20)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_FLVEAC = 'A'
        .w_TIPOIN = SPACE(5)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_TIPOIN))
          .link_1_5('Full')
        endif
          .DoRTCalc(4,6,.f.)
        .w_ORDINE = iif(.w_FLVEAC="A", 1, 3)
        .w_ORDERBY_GSUT_SZM = icase(.w_ORDINE=1,"MVALFEST,7,MVNUMEST,MVDATDOC,MVALFDOC,MVNUMDOC", .w_ORDINE=2,"MVALFEST,7 DESC,MVNUMEST DESC,MVDATDOC DESC,MVALFDOC,MVNUMDOC DESC", .w_ORDINE=3,"MVALFDOC,MVDATDOC,MVNUMDOC,MVALFEST,7,MVNUMEST", .w_ORDINE=4,"MVALFDOC,MVDATDOC DESC,MVNUMDOC DESC,MVALFEST,7 DESC,MVNUMEST DESC", "")
        .w_CLADOCDI = 'XX'
        .w_CLADOCOR = 'XX'
        .w_CLADOCDT = 'XX'
        .w_CLADOCFA = 'XX'
        .w_CLADOCNC = 'XX'
        .w_CLADOCOP = 'XX'
        .w_CLADOCRF = 'XX'
      .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
          .DoRTCalc(16,16,.f.)
        .w_SERIALE = .w_ZoomDoc.getVar('MVSERIAL')
        .w_PARAME = .w_ZoomDoc.getVar('MVFLVEAC')+.w_ZoomDoc.getVar('MVCLADOC')
          .DoRTCalc(19,21,.f.)
        .w_REPSEC = 'C'
        .w_CLAMOD = 'S'
        .w_CODUTE = i_CODUTE
        .w_ARCHIVIO = 'DOC_MAST            '
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CLASSE))
          .link_1_40('Full')
        endif
        .DoRTCalc(27,29,.f.)
        if not(empty(.w_CODESE))
          .link_2_1('Full')
        endif
          .DoRTCalc(30,31,.f.)
        .w_datain = .w_data1
        .w_numini = 1
          .DoRTCalc(34,34,.f.)
        .w_datafi = .w_data2
        .w_numfin = IIF(IsAhe(), 9999999999, 999999999999999)
          .DoRTCalc(37,37,.f.)
        .w_DATINI = .w_data1
          .DoRTCalc(39,40,.f.)
        .w_DATFIN = .w_data2
          .DoRTCalc(42,43,.f.)
        .w_MAGCLI = 'C'
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_CLISEL))
          .link_2_35('Full')
        endif
          .DoRTCalc(46,46,.f.)
        .w_MAGFOR = 'F'
        .DoRTCalc(48,48,.f.)
        if not(empty(.w_FORSEL))
          .link_2_39('Full')
        endif
          .DoRTCalc(49,49,.f.)
        .w_MAGALT = 'N'
        .w_OBTEST = i_INIDAT
    endwith
    this.DoRTCalc(52,55,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_44.enabled = this.oPgFrm.Page2.oPag.oBtn_2_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
        if .o_FLVEAC<>.w_FLVEAC
            .w_TIPOIN = SPACE(5)
          .link_1_5('Full')
        endif
        if .o_TIPOIN<>.w_TIPOIN
          .Calculate_NZKZPBPPUC()
        endif
        .DoRTCalc(4,6,.t.)
        if .o_FLVEAC<>.w_FLVEAC
            .w_ORDINE = iif(.w_FLVEAC="A", 1, 3)
        endif
            .w_ORDERBY_GSUT_SZM = icase(.w_ORDINE=1,"MVALFEST,7,MVNUMEST,MVDATDOC,MVALFDOC,MVNUMDOC", .w_ORDINE=2,"MVALFEST,7 DESC,MVNUMEST DESC,MVDATDOC DESC,MVALFDOC,MVNUMDOC DESC", .w_ORDINE=3,"MVALFDOC,MVDATDOC,MVNUMDOC,MVALFEST,7,MVNUMEST", .w_ORDINE=4,"MVALFDOC,MVDATDOC DESC,MVNUMDOC DESC,MVALFEST,7 DESC,MVNUMEST DESC", "")
        if .o_ORDINE<>.w_ORDINE
          .Calculate_DMYIOWTQGY()
        endif
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .DoRTCalc(9,16,.t.)
            .w_SERIALE = .w_ZoomDoc.getVar('MVSERIAL')
            .w_PARAME = .w_ZoomDoc.getVar('MVFLVEAC')+.w_ZoomDoc.getVar('MVCLADOC')
        .DoRTCalc(19,31,.t.)
        if .o_CODESE<>.w_CODESE
            .w_datain = .w_data1
        endif
        .DoRTCalc(33,34,.t.)
        if .o_CODESE<>.w_CODESE
            .w_datafi = .w_data2
        endif
        .DoRTCalc(36,37,.t.)
        if .o_CODESE<>.w_CODESE
            .w_DATINI = .w_data1
        endif
        .DoRTCalc(39,40,.t.)
        if .o_CODESE<>.w_CODESE
            .w_DATFIN = .w_data2
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(42,55,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
    endwith
  return

  proc Calculate_NZKZPBPPUC()
    with this
          * --- Imposta flag categoria
          .w_CLADOCDI = iif(.w_TCATDOC='DI','DI', .w_CLADOCDI)
          .w_CLADOCOR = iif(.w_TCATDOC='OR','OR', .w_CLADOCOR)
          .w_CLADOCDT = iif(.w_TCATDOC='DT','DT', .w_CLADOCDT)
          .w_CLADOCFA = iif(.w_TCATDOC='FA','FA', .w_CLADOCFA)
          .w_CLADOCNC = iif(.w_TCATDOC='NC','NC', .w_CLADOCNC)
          .w_CLADOCOP = iif(.w_TCATDOC='OP','OP', .w_CLADOCOP)
          .w_CLADOCRF = iif(.w_TCATDOC='RF','RF', .w_CLADOCRF)
    endwith
  endproc
  proc Calculate_DMYIOWTQGY()
    with this
          * --- Ordinamento
          .w_ZOOMDOC.bQueryOnDblClick = .w_ORDINE>4
    endwith
  endproc
  proc Calculate_HLCZNDCVFC()
    with this
          * --- Esegui
          .w_NORDERBY = .w_ZOOMDOC.norderby
          .w_ZOOMDOC.norderby = IIF(.w_ORDINE<5 and .w_NORDERBY=1, 0, .w_ZOOMDOC.norderby)
          .w_ZOOMDOC.corderby = IIF(.w_ORDINE<5 and .w_NORDERBY=1, "", .w_ZOOMDOC.corderby)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oCLISEL_2_35.enabled = this.oPgFrm.Page2.oPag.oCLISEL_2_35.mCond()
    this.oPgFrm.Page2.oPag.oFORSEL_2_39.enabled = this.oPgFrm.Page2.oPag.oFORSEL_2_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCLADOCOP_1_20.visible=!this.oPgFrm.Page1.oPag.oCLADOCOP_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCLADOCRF_1_21.visible=!this.oPgFrm.Page1.oPag.oCLADOCRF_1_21.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Esegui")
          .Calculate_HLCZNDCVFC()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZoomDoc.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPOIN
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPOIN)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDTIPDOC,TDDESDOC,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_FLVEAC;
                     ,'TDTIPDOC',trim(this.w_TIPOIN))
          select TDFLVEAC,TDTIPDOC,TDDESDOC,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOIN)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStrODBC(trim(this.w_TIPOIN)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);

            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDTIPDOC,TDDESDOC,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStr(trim(this.w_TIPOIN)+"%");
                   +" and TDFLVEAC="+cp_ToStr(this.w_FLVEAC);

            select TDFLVEAC,TDTIPDOC,TDDESDOC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TIPOIN) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDTIPDOC',cp_AbsName(oSource.parent,'oTIPOIN_1_5'),i_cWhere,'GSVE_ATD',"Tipi documenti",'GSUT0SZM.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDTIPDOC,TDDESDOC,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDTIPDOC,TDDESDOC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente oppure inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDTIPDOC,TDDESDOC,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDTIPDOC',oSource.xKey(2))
            select TDFLVEAC,TDTIPDOC,TDDESDOC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDTIPDOC,TDDESDOC,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPOIN);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_FLVEAC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_FLVEAC;
                       ,'TDTIPDOC',this.w_TIPOIN)
            select TDFLVEAC,TDTIPDOC,TDDESDOC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOIN = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_TCATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_TFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOIN = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_TCATDOC = space(2)
      this.w_TFLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TFLVEAC=.w_FLVEAC
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente oppure inesistente")
        endif
        this.w_TIPOIN = space(5)
        this.w_DESDOC = space(35)
        this.w_TCATDOC = space(2)
        this.w_TFLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLASSE
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLASSE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_CLASSE)+"%");
                   +" and CDMODALL="+cp_ToStrODBC(this.w_CLAMOD);
                   +" and CDRIFTAB="+cp_ToStrODBC(this.w_ARCHIVIO);

          i_ret=cp_SQL(i_nConn,"select CDMODALL,CDRIFTAB,CDCODCLA,CDDESCLA,CDTIPOBC,CDREPOBC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDMODALL,CDRIFTAB,CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDMODALL',this.w_CLAMOD;
                     ,'CDRIFTAB',this.w_ARCHIVIO;
                     ,'CDCODCLA',trim(this.w_CLASSE))
          select CDMODALL,CDRIFTAB,CDCODCLA,CDDESCLA,CDTIPOBC,CDREPOBC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDMODALL,CDRIFTAB,CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLASSE)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CDDESCLA like "+cp_ToStrODBC(trim(this.w_CLASSE)+"%");
                   +" and CDMODALL="+cp_ToStrODBC(this.w_CLAMOD);
                   +" and CDRIFTAB="+cp_ToStrODBC(this.w_ARCHIVIO);

            i_ret=cp_SQL(i_nConn,"select CDMODALL,CDRIFTAB,CDCODCLA,CDDESCLA,CDTIPOBC,CDREPOBC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CDDESCLA like "+cp_ToStr(trim(this.w_CLASSE)+"%");
                   +" and CDMODALL="+cp_ToStr(this.w_CLAMOD);
                   +" and CDRIFTAB="+cp_ToStr(this.w_ARCHIVIO);

            select CDMODALL,CDRIFTAB,CDCODCLA,CDDESCLA,CDTIPOBC,CDREPOBC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLASSE) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDMODALL,CDRIFTAB,CDCODCLA',cp_AbsName(oSource.parent,'oCLASSE_1_40'),i_cWhere,'GSUT_MCD',"Classi documentali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CLAMOD<>oSource.xKey(1);
           .or. this.w_ARCHIVIO<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDMODALL,CDRIFTAB,CDCODCLA,CDDESCLA,CDTIPOBC,CDREPOBC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CDMODALL,CDRIFTAB,CDCODCLA,CDDESCLA,CDTIPOBC,CDREPOBC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per archiviare documenti nella classe selezionata oppure archivazione con scanner in modalit� collegamento")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDMODALL,CDRIFTAB,CDCODCLA,CDDESCLA,CDTIPOBC,CDREPOBC";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(3));
                     +" and CDMODALL="+cp_ToStrODBC(this.w_CLAMOD);
                     +" and CDRIFTAB="+cp_ToStrODBC(this.w_ARCHIVIO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDMODALL',oSource.xKey(1);
                       ,'CDRIFTAB',oSource.xKey(2);
                       ,'CDCODCLA',oSource.xKey(3))
            select CDMODALL,CDRIFTAB,CDCODCLA,CDDESCLA,CDTIPOBC,CDREPOBC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLASSE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDMODALL,CDRIFTAB,CDCODCLA,CDDESCLA,CDTIPOBC,CDREPOBC";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_CLASSE);
                   +" and CDMODALL="+cp_ToStrODBC(this.w_CLAMOD);
                   +" and CDRIFTAB="+cp_ToStrODBC(this.w_ARCHIVIO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDMODALL',this.w_CLAMOD;
                       ,'CDRIFTAB',this.w_ARCHIVIO;
                       ,'CDCODCLA',this.w_CLASSE)
            select CDMODALL,CDRIFTAB,CDCODCLA,CDDESCLA,CDTIPOBC,CDREPOBC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLASSE = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCRI = NVL(_Link_.CDDESCLA,space(40))
      this.w_MODALLEG = NVL(_Link_.CDMODALL,space(1))
      this.w_CDTIPOBC = NVL(_Link_.CDTIPOBC,space(1))
      this.w_CDREPOBC = NVL(_Link_.CDREPOBC,space(254))
      this.w_CDRIFTAB = NVL(_Link_.CDRIFTAB,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CLASSE = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_MODALLEG = space(1)
      this.w_CDTIPOBC = space(1)
      this.w_CDREPOBC = space(254)
      this.w_CDRIFTAB = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MODALLEG=.w_CLAMOD AND SUBSTR( CHKPECLA( .w_CLASSE, .w_CODUTE, .w_ARCHIVIO), 2, 1 ) = 'S' AND UPPER(.w_CDRIFTAB)="DOC_MAST"
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per archiviare documenti nella classe selezionata oppure archivazione con scanner in modalit� collegamento")
        endif
        this.w_CLASSE = space(15)
        this.w_DESCRI = space(40)
        this.w_MODALLEG = space(1)
        this.w_CDTIPOBC = space(1)
        this.w_CDREPOBC = space(254)
        this.w_CDRIFTAB = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDMODALL,1)+'\'+cp_ToStr(_Link_.CDRIFTAB,1)+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLASSE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_2_1'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_data1 = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_data2 = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_data1 = ctod("  /  /  ")
      this.w_data2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLISEL
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLISEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACL',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGCLI);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MAGCLI;
                     ,'ANCODICE',trim(this.w_CLISEL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLISEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGCLI);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MAGCLI);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLISEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLISEL_2_35'),i_cWhere,'GSAR_ACL',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MAGCLI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLISEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLISEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MAGCLI;
                       ,'ANCODICE',this.w_CLISEL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLISEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLISEL = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        endif
        this.w_CLISEL = space(15)
        this.w_DESCLI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLISEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORSEL
  func Link_2_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MAGFOR;
                     ,'ANCODICE',trim(this.w_FORSEL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORSEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGFOR);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MAGFOR);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORSEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORSEL_2_39'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MAGFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORSEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MAGFOR;
                       ,'ANCODICE',this.w_FORSEL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORSEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORSEL = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        endif
        this.w_FORSEL = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLVEAC_1_3.RadioValue()==this.w_FLVEAC)
      this.oPgFrm.Page1.oPag.oFLVEAC_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOIN_1_5.value==this.w_TIPOIN)
      this.oPgFrm.Page1.oPag.oTIPOIN_1_5.value=this.w_TIPOIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_6.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_6.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oORDINE_1_10.RadioValue()==this.w_ORDINE)
      this.oPgFrm.Page1.oPag.oORDINE_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOCDI_1_15.RadioValue()==this.w_CLADOCDI)
      this.oPgFrm.Page1.oPag.oCLADOCDI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOCOR_1_16.RadioValue()==this.w_CLADOCOR)
      this.oPgFrm.Page1.oPag.oCLADOCOR_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOCDT_1_17.RadioValue()==this.w_CLADOCDT)
      this.oPgFrm.Page1.oPag.oCLADOCDT_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOCFA_1_18.RadioValue()==this.w_CLADOCFA)
      this.oPgFrm.Page1.oPag.oCLADOCFA_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOCNC_1_19.RadioValue()==this.w_CLADOCNC)
      this.oPgFrm.Page1.oPag.oCLADOCNC_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOCOP_1_20.RadioValue()==this.w_CLADOCOP)
      this.oPgFrm.Page1.oPag.oCLADOCOP_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOCRF_1_21.RadioValue()==this.w_CLADOCRF)
      this.oPgFrm.Page1.oPag.oCLADOCRF_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASSE_1_40.value==this.w_CLASSE)
      this.oPgFrm.Page1.oPag.oCLASSE_1_40.value=this.w_CLASSE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_42.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_42.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODESE_2_1.value==this.w_CODESE)
      this.oPgFrm.Page2.oPag.oCODESE_2_1.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page2.oPag.odatain_2_8.value==this.w_datain)
      this.oPgFrm.Page2.oPag.odatain_2_8.value=this.w_datain
    endif
    if not(this.oPgFrm.Page2.oPag.onumini_2_10.value==this.w_numini)
      this.oPgFrm.Page2.oPag.onumini_2_10.value=this.w_numini
    endif
    if not(this.oPgFrm.Page2.oPag.oSERIE1_2_12.value==this.w_SERIE1)
      this.oPgFrm.Page2.oPag.oSERIE1_2_12.value=this.w_SERIE1
    endif
    if not(this.oPgFrm.Page2.oPag.odatafi_2_14.value==this.w_datafi)
      this.oPgFrm.Page2.oPag.odatafi_2_14.value=this.w_datafi
    endif
    if not(this.oPgFrm.Page2.oPag.onumfin_2_16.value==this.w_numfin)
      this.oPgFrm.Page2.oPag.onumfin_2_16.value=this.w_numfin
    endif
    if not(this.oPgFrm.Page2.oPag.oSERIE2_2_18.value==this.w_SERIE2)
      this.oPgFrm.Page2.oPag.oSERIE2_2_18.value=this.w_SERIE2
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINI_2_21.value==this.w_DATINI)
      this.oPgFrm.Page2.oPag.oDATINI_2_21.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPROTINI_2_22.value==this.w_PROTINI)
      this.oPgFrm.Page2.oPag.oPROTINI_2_22.value=this.w_PROTINI
    endif
    if not(this.oPgFrm.Page2.oPag.oSEPROT1_2_23.value==this.w_SEPROT1)
      this.oPgFrm.Page2.oPag.oSEPROT1_2_23.value=this.w_SEPROT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFIN_2_27.value==this.w_DATFIN)
      this.oPgFrm.Page2.oPag.oDATFIN_2_27.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPROTFIN_2_28.value==this.w_PROTFIN)
      this.oPgFrm.Page2.oPag.oPROTFIN_2_28.value=this.w_PROTFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oSEPROT2_2_29.value==this.w_SEPROT2)
      this.oPgFrm.Page2.oPag.oSEPROT2_2_29.value=this.w_SEPROT2
    endif
    if not(this.oPgFrm.Page2.oPag.oMAGCLI_2_33.RadioValue()==this.w_MAGCLI)
      this.oPgFrm.Page2.oPag.oMAGCLI_2_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCLISEL_2_35.value==this.w_CLISEL)
      this.oPgFrm.Page2.oPag.oCLISEL_2_35.value=this.w_CLISEL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLI_2_36.value==this.w_DESCLI)
      this.oPgFrm.Page2.oPag.oDESCLI_2_36.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oMAGFOR_2_37.RadioValue()==this.w_MAGFOR)
      this.oPgFrm.Page2.oPag.oMAGFOR_2_37.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFORSEL_2_39.value==this.w_FORSEL)
      this.oPgFrm.Page2.oPag.oFORSEL_2_39.value=this.w_FORSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFOR_2_40.value==this.w_DESFOR)
      this.oPgFrm.Page2.oPag.oDESFOR_2_40.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page2.oPag.oMAGALT_2_41.RadioValue()==this.w_MAGALT)
      this.oPgFrm.Page2.oPag.oMAGALT_2_41.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TFLVEAC=.w_FLVEAC)  and not(empty(.w_TIPOIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente oppure inesistente")
          case   ((empty(.w_CLASSE)) or not(.w_MODALLEG=.w_CLAMOD AND SUBSTR( CHKPECLA( .w_CLASSE, .w_CODUTE, .w_ARCHIVIO), 2, 1 ) = 'S' AND UPPER(.w_CDRIFTAB)="DOC_MAST"))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLASSE_1_40.SetFocus()
            i_bnoObbl = !empty(.w_CLASSE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per archiviare documenti nella classe selezionata oppure archivazione con scanner in modalit� collegamento")
          case   not((empty(.w_DATAFI)) OR  (.w_DATAIN<=.w_DATAFI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.odatain_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.onumini_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSERIE1_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not((empty(.w_DATAin)) OR  (.w_DATAIN<=.w_DATAFI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.odatafi_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.onumfin_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((.w_SERIE2>=.w_SERIE1) or (empty(.w_SERIE1)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSERIE2_2_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATINI_2_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_PROTINI<=.w_PROTFIN or .w_PROTFIN=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPROTINI_2_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((empty(.w_SEPROT2)) OR  (.w_SEPROT1<=.w_SEPROT2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSEPROT1_2_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not(.w_DATINI<=.w_DATFIN)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATFIN_2_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_PROTINI<=.w_PROTFIN)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPROTFIN_2_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((.w_SEPROT2>=.w_SEPROT1) or (empty(.w_SEPROT1)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSEPROT2_2_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_MAGCLI='C')  and not(empty(.w_CLISEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLISEL_2_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_MAGFOR='F')  and not(empty(.w_FORSEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFORSEL_2_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLVEAC = this.w_FLVEAC
    this.o_TIPOIN = this.w_TIPOIN
    this.o_ORDINE = this.w_ORDINE
    this.o_CODESE = this.w_CODESE
    return

enddefine

* --- Define pages as container
define class tgsut_szmPag1 as StdContainer
  Width  = 791
  height = 540
  stdWidth  = 791
  stdheight = 540
  resizeXpos=718
  resizeYpos=348
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLVEAC_1_3 as StdCombo with uid="ZQSVNGVRJV",rtseq=2,rtrep=.f.,left=100,top=6,width=132,height=22;
    , ToolTipText = "Tipo gestione interessata";
    , HelpContextID = 118395562;
    , cFormVar="w_FLVEAC",RowSource=""+"Vendite,"+"Acquisti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLVEAC_1_3.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oFLVEAC_1_3.GetRadio()
    this.Parent.oContained.w_FLVEAC = this.RadioValue()
    return .t.
  endfunc

  func oFLVEAC_1_3.SetRadio()
    this.Parent.oContained.w_FLVEAC=trim(this.Parent.oContained.w_FLVEAC)
    this.value = ;
      iif(this.Parent.oContained.w_FLVEAC=='V',1,;
      iif(this.Parent.oContained.w_FLVEAC=='A',2,;
      0))
  endfunc

  func oFLVEAC_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_TIPOIN)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oTIPOIN_1_5 as StdField with uid="JAINMPNBRJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TIPOIN", cQueryName = "TIPOIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente oppure inesistente",;
    ToolTipText = "Codice tipo documento di selezione",;
    HelpContextID = 193262794,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=269, Top=6, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_FLVEAC", oKey_2_1="TDTIPDOC", oKey_2_2="this.w_TIPOIN"

  func oTIPOIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPOIN_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOIN_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_FLVEAC)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDTIPDOC',cp_AbsName(this.parent,'oTIPOIN_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Tipi documenti",'GSUT0SZM.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPOIN_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_FLVEAC
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPOIN
     i_obj.ecpSave()
  endproc

  add object oDESDOC_1_6 as StdField with uid="GLKHKTUIJU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 103795146,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=333, Top=6, InputMask=replicate('X',35)


  add object oORDINE_1_10 as StdCombo with uid="XXKAGNNGKA",rtseq=7,rtrep=.f.,left=616,top=6,width=104,height=22;
    , ToolTipText = "Imposta l'ordinamento dell'elenco; se Utente, l'ordinamento � impostato col doppio click sulle colonne";
    , HelpContextID = 71019546;
    , cFormVar="w_ORDINE",RowSource=""+"Num. protocollo,"+"Num. prot. decr.,"+"Num. documento,"+"Num. doc. decr.,"+"Utente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oORDINE_1_10.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    0))))))
  endfunc
  func oORDINE_1_10.GetRadio()
    this.Parent.oContained.w_ORDINE = this.RadioValue()
    return .t.
  endfunc

  func oORDINE_1_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ORDINE==1,1,;
      iif(this.Parent.oContained.w_ORDINE==2,2,;
      iif(this.Parent.oContained.w_ORDINE==3,3,;
      iif(this.Parent.oContained.w_ORDINE==4,4,;
      iif(this.Parent.oContained.w_ORDINE==5,5,;
      0)))))
  endfunc

  add object oCLADOCDI_1_15 as StdCheck with uid="DVOJYWQWFC",rtseq=9,rtrep=.f.,left=13, top=40, caption="Doc. interni",;
    ToolTipText = "Se attivo: seleziona documenti riferiti alla categoria documenti interni",;
    HelpContextID = 103867025,;
    cFormVar="w_CLADOCDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLADOCDI_1_15.RadioValue()
    return(iif(this.value =1,'DI',;
    'XX'))
  endfunc
  func oCLADOCDI_1_15.GetRadio()
    this.Parent.oContained.w_CLADOCDI = this.RadioValue()
    return .t.
  endfunc

  func oCLADOCDI_1_15.SetRadio()
    this.Parent.oContained.w_CLADOCDI=trim(this.Parent.oContained.w_CLADOCDI)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOCDI=='DI',1,;
      0)
  endfunc

  add object oCLADOCOR_1_16 as StdCheck with uid="ZMHRWVMGUF",rtseq=10,rtrep=.f.,left=110, top=40, caption="Ordini",;
    ToolTipText = "Se attivo: seleziona documenti riferiti alla categoria ordini",;
    HelpContextID = 103867016,;
    cFormVar="w_CLADOCOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLADOCOR_1_16.RadioValue()
    return(iif(this.value =1,'OR',;
    'XX'))
  endfunc
  func oCLADOCOR_1_16.GetRadio()
    this.Parent.oContained.w_CLADOCOR = this.RadioValue()
    return .t.
  endfunc

  func oCLADOCOR_1_16.SetRadio()
    this.Parent.oContained.w_CLADOCOR=trim(this.Parent.oContained.w_CLADOCOR)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOCOR=='OR',1,;
      0)
  endfunc

  add object oCLADOCDT_1_17 as StdCheck with uid="SYMTQWDIJK",rtseq=11,rtrep=.f.,left=181, top=40, caption="Doc. di trasporto",;
    ToolTipText = "Se attivo: seleziona documenti riferiti alla categoria documento di trasporto",;
    HelpContextID = 103867014,;
    cFormVar="w_CLADOCDT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLADOCDT_1_17.RadioValue()
    return(iif(this.value =1,'DT',;
    'XX'))
  endfunc
  func oCLADOCDT_1_17.GetRadio()
    this.Parent.oContained.w_CLADOCDT = this.RadioValue()
    return .t.
  endfunc

  func oCLADOCDT_1_17.SetRadio()
    this.Parent.oContained.w_CLADOCDT=trim(this.Parent.oContained.w_CLADOCDT)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOCDT=='DT',1,;
      0)
  endfunc

  add object oCLADOCFA_1_18 as StdCheck with uid="NWIBLYCNGL",rtseq=12,rtrep=.f.,left=303, top=40, caption="Fatture",;
    ToolTipText = "Se attivo: seleziona documenti riferiti alla categoria fatture",;
    HelpContextID = 103867033,;
    cFormVar="w_CLADOCFA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLADOCFA_1_18.RadioValue()
    return(iif(this.value =1,'FA',;
    'XX'))
  endfunc
  func oCLADOCFA_1_18.GetRadio()
    this.Parent.oContained.w_CLADOCFA = this.RadioValue()
    return .t.
  endfunc

  func oCLADOCFA_1_18.SetRadio()
    this.Parent.oContained.w_CLADOCFA=trim(this.Parent.oContained.w_CLADOCFA)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOCFA=='FA',1,;
      0)
  endfunc

  add object oCLADOCNC_1_19 as StdCheck with uid="BXBHUCTNDW",rtseq=13,rtrep=.f.,left=378, top=40, caption="Note di credito",;
    ToolTipText = "Se attivo: seleziona documenti riferiti alla categoria note di credito",;
    HelpContextID = 164568425,;
    cFormVar="w_CLADOCNC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLADOCNC_1_19.RadioValue()
    return(iif(this.value =1,'NC',;
    'XX'))
  endfunc
  func oCLADOCNC_1_19.GetRadio()
    this.Parent.oContained.w_CLADOCNC = this.RadioValue()
    return .t.
  endfunc

  func oCLADOCNC_1_19.SetRadio()
    this.Parent.oContained.w_CLADOCNC=trim(this.Parent.oContained.w_CLADOCNC)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOCNC=='NC',1,;
      0)
  endfunc

  add object oCLADOCOP_1_20 as StdCheck with uid="OYAQVEVXZZ",rtseq=14,rtrep=.f.,left=490, top=40, caption="Ordini previsionali",;
    ToolTipText = "Se attivo: seleziona documenti riferiti alla categoria ordini previsionali",;
    HelpContextID = 103867018,;
    cFormVar="w_CLADOCOP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLADOCOP_1_20.RadioValue()
    return(iif(this.value =1,'OP',;
    'XX'))
  endfunc
  func oCLADOCOP_1_20.GetRadio()
    this.Parent.oContained.w_CLADOCOP = this.RadioValue()
    return .t.
  endfunc

  func oCLADOCOP_1_20.SetRadio()
    this.Parent.oContained.w_CLADOCOP=trim(this.Parent.oContained.w_CLADOCOP)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOCOP=='OP',1,;
      0)
  endfunc

  func oCLADOCOP_1_20.mHide()
    with this.Parent.oContained
      return (!isahe())
    endwith
  endfunc

  add object oCLADOCRF_1_21 as StdCheck with uid="UVYZNONNFL",rtseq=15,rtrep=.f.,left=490, top=40, caption="Corrispettivi",;
    ToolTipText = "Se attivo: seleziona documenti riferiti alla categoria corrispettivi",;
    HelpContextID = 103867028,;
    cFormVar="w_CLADOCRF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLADOCRF_1_21.RadioValue()
    return(iif(this.value =1,'RF',;
    'XX'))
  endfunc
  func oCLADOCRF_1_21.GetRadio()
    this.Parent.oContained.w_CLADOCRF = this.RadioValue()
    return .t.
  endfunc

  func oCLADOCRF_1_21.SetRadio()
    this.Parent.oContained.w_CLADOCRF=trim(this.Parent.oContained.w_CLADOCRF)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOCRF=='RF',1,;
      0)
  endfunc

  func oCLADOCRF_1_21.mHide()
    with this.Parent.oContained
      return (!isahr())
    endwith
  endfunc


  add object oBtn_1_22 as StdButton with uid="QYBPKEREZO",left=740, top=7, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 229342138;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        .NotifyEvent("Esegui")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomDoc as cp_szoombox with uid="FMGYZDFNXS",left=3, top=65, width=786,height=426,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_DETT",bRetriveallrows=.t.,cZoomFile="GSUT_SZM",bOptions=.f.,bQueryOnLoad=.f.,bAdvOptions=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=FALSE,bRetriveAllRows=.t.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 63781914


  add object oBtn_1_33 as StdButton with uid="VVCSESRAJP",left=6, top=494, width=48,height=45,;
    CpPicture="bmp\Check.ico", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutte le righe";
    , HelpContextID = 161670950;
    ,  Caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        .w_ZoomDoc.CheckAll()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_34 as StdButton with uid="DSBYBNBLSB",left=56, top=494, width=48,height=45,;
    CpPicture="bmp\unCheck.ico", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutte le righe";
    , HelpContextID = 147019318;
    ,  Caption='\<Deselez';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        .w_ZoomDoc.UnCheckAll()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_35 as StdButton with uid="SVOKPXVALD",left=106, top=494, width=48,height=45,;
    CpPicture="bmp\invcheck.ico", caption="", nPag=1;
    , ToolTipText = "Premere per invertile la selezione effettuata";
    , HelpContextID = 194932858;
    ,  Caption='\<Inv. Sel';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      with this.Parent.oContained
        .w_ZoomDoc.InvertSelection()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_36 as StdButton with uid="IVUYXSPMRW",left=162, top=494, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato alla riga selezionata";
    , HelpContextID = 110182458;
    , caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_SERIALE, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  add object oCLASSE_1_40 as StdField with uid="ZKGFHWQBEA",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CLASSE", cQueryName = "CLASSE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida o non si possiedono le autorizzazioni per archiviare documenti nella classe selezionata oppure archivazione con scanner in modalit� collegamento",;
    ToolTipText = "Classe documentale (solo modalit� Infinity D.M.S. stand alone)",;
    HelpContextID = 65135322,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=342, Top=494, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDMODALL", oKey_1_2="this.w_CLAMOD", oKey_2_1="CDRIFTAB", oKey_2_2="this.w_ARCHIVIO", oKey_3_1="CDCODCLA", oKey_3_2="this.w_CLASSE"

  func oCLASSE_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLASSE_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLASSE_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.PROMCLAS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CDMODALL="+cp_ToStrODBC(this.Parent.oContained.w_CLAMOD)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CDRIFTAB="+cp_ToStrODBC(this.Parent.oContained.w_ARCHIVIO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CDMODALL="+cp_ToStr(this.Parent.oContained.w_CLAMOD)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CDRIFTAB="+cp_ToStr(this.Parent.oContained.w_ARCHIVIO)
    endif
    do cp_zoom with 'PROMCLAS','*','CDMODALL,CDRIFTAB,CDCODCLA',cp_AbsName(this.parent,'oCLASSE_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MCD',"Classi documentali",'',this.parent.oContained
  endproc
  proc oCLASSE_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CDMODALL=w_CLAMOD
    i_obj.CDRIFTAB=w_ARCHIVIO
     i_obj.w_CDCODCLA=this.parent.oContained.w_CLASSE
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_42 as StdField with uid="BCQHKBKQOX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione classe",;
    HelpContextID = 51658,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=342, Top=517, InputMask=replicate('X',40)


  add object oBtn_1_47 as StdButton with uid="ADVRKLPKKG",left=636, top=494, width=48,height=45,;
    CpPicture="fxInf.bmp", caption="", nPag=1;
    , ToolTipText = "Genera l'allegato tramite il report principale ed il relativo file xml-descrittore";
    , HelpContextID = 26961398;
    , caption='\<Infinity';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      with this.Parent.oContained
        GSUT_BZM(this.Parent.oContained,"PDF")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CLASSE))
      endwith
    endif
  endfunc


  add object oBtn_1_48 as StdButton with uid="WGITVJQEJG",left=688, top=494, width=48,height=45,;
    CpPicture="bmp\matricole.ico", caption="", nPag=1;
    , ToolTipText = "Stampa i barcode di associazione e genera il relativo file xml-descrittore";
    , HelpContextID = 26988406;
    , caption='\<Barcode';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_48.Click()
      with this.Parent.oContained
        GSUT_BZM(this.Parent.oContained,"XML")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_48.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CLASSE))
      endwith
    endif
  endfunc


  add object oBtn_1_49 as StdButton with uid="PXBGFOADEM",left=740, top=494, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 150833642;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_49.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="NYIFVFJARA",Visible=.t., Left=57, Top=10,;
    Alignment=1, Width=40, Height=18,;
    Caption="Ciclo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="PEGPUESRJN",Visible=.t., Left=235, Top=10,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="BYRFQZCKUU",Visible=.t., Left=553, Top=10,;
    Alignment=1, Width=61, Height=18,;
    Caption="Ordina per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="KUTBRHJKUV",Visible=.t., Left=9, Top=22,;
    Alignment=1, Width=57, Height=18,;
    Caption="Categoria"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="JBWSDNOZTJ",Visible=.t., Left=214, Top=494,;
    Alignment=1, Width=126, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="NYMUTOMUHM",Visible=.t., Left=272, Top=521,;
    Alignment=1, Width=68, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oBox_1_14 as StdBox with uid="YREDSJDNZR",left=11, top=39, width=722,height=1
enddefine
define class tgsut_szmPag2 as StdContainer
  Width  = 791
  height = 540
  stdWidth  = 791
  stdheight = 540
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODESE_2_1 as StdField with uid="TDNJQIZCMZ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio selezionato",;
    HelpContextID = 66039770,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=104, Top=19, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object odatain_2_8 as StdField with uid="TLSMGPEYWG",rtseq=32,rtrep=.f.,;
    cFormVar = "w_datain", cQueryName = "datain",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento iniziale selezionata",;
    HelpContextID = 158374858,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=104, Top=83

  func odatain_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_DATAFI)) OR  (.w_DATAIN<=.w_DATAFI))
    endwith
    return bRes
  endfunc

  add object onumini_2_10 as StdField with uid="CGTAYVCLKJ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_numini", cQueryName = "numini",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento iniziale selezionato",;
    HelpContextID = 236517162,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=295, Top=83, cSayPict='IIF(IsAhe(), "9999999999","999999999999999")', cGetPict='IIF(IsAhe(), "9999999999","999999999999999")', nMaxValue = 999999999999999

  func onumini_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oSERIE1_2_12 as StdField with uid="IKCHOCFECU",rtseq=34,rtrep=.f.,;
    cFormVar = "w_SERIE1", cQueryName = "SERIE1",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Serie documento iniziale selezionato",;
    HelpContextID = 147511514,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=438, Top=83, cSayPict='IIF(!IsAhe(), "!!!!!!!!!!", "!!")', cGetPict='IIF(!IsAhe(), "!!!!!!!!!!", "!!")', InputMask=replicate('X',10)

  func oSERIE1_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object odatafi_2_14 as StdField with uid="SJPNDJOWBM",rtseq=35,rtrep=.f.,;
    cFormVar = "w_datafi", cQueryName = "datafi",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento finale selezionata",;
    HelpContextID = 245406666,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=104, Top=110

  func odatafi_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_DATAin)) OR  (.w_DATAIN<=.w_DATAFI))
    endwith
    return bRes
  endfunc

  add object onumfin_2_16 as StdField with uid="PPVPLBFEDU",rtseq=36,rtrep=.f.,;
    cFormVar = "w_numfin", cQueryName = "numfin",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento finale selezionato",;
    HelpContextID = 158070570,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=295, Top=110, cSayPict='IIF(IsAhe(), "9999999999","999999999999999")', cGetPict='IIF(IsAhe(), "9999999999","999999999999999")', nMaxValue = 999999999999999

  func onumfin_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oSERIE2_2_18 as StdField with uid="SNGKDXOYPG",rtseq=37,rtrep=.f.,;
    cFormVar = "w_SERIE2", cQueryName = "SERIE2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Serie documento finale selezionato",;
    HelpContextID = 130734298,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=438, Top=110, cSayPict='IIF(!IsAhe(), "!!!!!!!!!!", "!!")', cGetPict='IIF(!IsAhe(), "!!!!!!!!!!", "!!")', InputMask=replicate('X',10)

  func oSERIE2_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_SERIE2>=.w_SERIE1) or (empty(.w_SERIE1)))
    endwith
    return bRes
  endfunc

  add object oDATINI_2_21 as StdField with uid="VQIOCNGNZD",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data protocollo di inizio selezione",;
    HelpContextID = 3849674,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=176

  func oDATINI_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oPROTINI_2_22 as StdField with uid="XNSNUVBOCK",rtseq=39,rtrep=.f.,;
    cFormVar = "w_PROTINI", cQueryName = "PROTINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero protocollo iniziale selezionato",;
    HelpContextID = 75498486,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=295, Top=178, cSayPict='IIF(IsAhe(), "9999999999","999999999999999")', cGetPict='IIF(IsAhe(), "9999999999","999999999999999")', nMaxValue = 999999999999999

  func oPROTINI_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PROTINI<=.w_PROTFIN or .w_PROTFIN=0)
    endwith
    return bRes
  endfunc

  add object oSEPROT1_2_23 as StdField with uid="ELXOTABQPZ",rtseq=40,rtrep=.f.,;
    cFormVar = "w_SEPROT1", cQueryName = "SEPROT1",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Serie protocollo iniziale selezionato",;
    HelpContextID = 86112474,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=438, Top=178, cSayPict='IIF(!IsAhe(), "!!!!!!!!!!", "!!")', cGetPict='IIF(!IsAhe(), "!!!!!!!!!!", "!!")', InputMask=replicate('X',10)

  func oSEPROT1_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SEPROT2)) OR  (.w_SEPROT1<=.w_SEPROT2))
    endwith
    return bRes
  endfunc

  add object oDATFIN_2_27 as StdField with uid="WTJZXNNZRA",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data protocollo di fine selezione",;
    HelpContextID = 193838538,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=200

  func oDATFIN_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oPROTFIN_2_28 as StdField with uid="IWNHAWBITK",rtseq=42,rtrep=.f.,;
    cFormVar = "w_PROTFIN", cQueryName = "PROTFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero protocollo finale selezionato",;
    HelpContextID = 256902134,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=295, Top=205, cSayPict='IIF(IsAhe(), "9999999999","999999999999999")', cGetPict='IIF(IsAhe(), "9999999999","999999999999999")', nMaxValue = 999999999999999

  func oPROTFIN_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PROTINI<=.w_PROTFIN)
    endwith
    return bRes
  endfunc

  add object oSEPROT2_2_29 as StdField with uid="NIWCXFKHYJ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_SEPROT2", cQueryName = "SEPROT2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Serie protocollo finale selezionato",;
    HelpContextID = 86112474,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=438, Top=205, cSayPict='IIF(!IsAhe(), "!!!!!!!!!!", "!!")', cGetPict='IIF(!IsAhe(), "!!!!!!!!!!", "!!")', InputMask=replicate('X',10)

  func oSEPROT2_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_SEPROT2>=.w_SEPROT1) or (empty(.w_SEPROT1)))
    endwith
    return bRes
  endfunc

  add object oMAGCLI_2_33 as StdCheck with uid="TVBXUUIWAS",rtseq=44,rtrep=.f.,left=90, top=266, caption="Clienti",;
    ToolTipText = "Se attivo: seleziona documenti riferiti a clienti",;
    HelpContextID = 6393146,;
    cFormVar="w_MAGCLI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMAGCLI_2_33.RadioValue()
    return(iif(this.value =1,'C',;
    'X'))
  endfunc
  func oMAGCLI_2_33.GetRadio()
    this.Parent.oContained.w_MAGCLI = this.RadioValue()
    return .t.
  endfunc

  func oMAGCLI_2_33.SetRadio()
    this.Parent.oContained.w_MAGCLI=trim(this.Parent.oContained.w_MAGCLI)
    this.value = ;
      iif(this.Parent.oContained.w_MAGCLI=='C',1,;
      0)
  endfunc

  func oMAGCLI_2_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CLISEL)
        bRes2=.link_2_35('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCLISEL_2_35 as StdField with uid="KEIYERAFZE",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CLISEL", cQueryName = "CLISEL",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice cliente inesistente oppure obsoleto",;
    ToolTipText = "Cliente di selezione scadenze",;
    HelpContextID = 230777562,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=295, Top=266, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_ACL", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MAGCLI", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLISEL"

  func oCLISEL_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MAGCLI='C')
    endwith
   endif
  endfunc

  func oCLISEL_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLISEL_2_35.ecpDrop(oSource)
    this.Parent.oContained.link_2_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLISEL_2_35.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MAGCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MAGCLI)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLISEL_2_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACL',"Clienti",'',this.parent.oContained
  endproc
  proc oCLISEL_2_35.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MAGCLI
     i_obj.w_ANCODICE=this.parent.oContained.w_CLISEL
     i_obj.ecpSave()
  endproc

  add object oDESCLI_2_36 as StdField with uid="CBMXEDVNXE",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 6343114,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=438, Top=266, InputMask=replicate('X',40)

  add object oMAGFOR_2_37 as StdCheck with uid="HDWWNGKAVN",rtseq=47,rtrep=.f.,left=90, top=294, caption="Fornitori",;
    ToolTipText = "Se attivo: seleziona documenti riferiti a fornitori",;
    HelpContextID = 120491322,;
    cFormVar="w_MAGFOR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMAGFOR_2_37.RadioValue()
    return(iif(this.value =1,'F',;
    'X'))
  endfunc
  func oMAGFOR_2_37.GetRadio()
    this.Parent.oContained.w_MAGFOR = this.RadioValue()
    return .t.
  endfunc

  func oMAGFOR_2_37.SetRadio()
    this.Parent.oContained.w_MAGFOR=trim(this.Parent.oContained.w_MAGFOR)
    this.value = ;
      iif(this.Parent.oContained.w_MAGFOR=='F',1,;
      0)
  endfunc

  func oMAGFOR_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_FORSEL)
        bRes2=.link_2_39('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oFORSEL_2_39 as StdField with uid="NWEEGBCARU",rtseq=48,rtrep=.f.,;
    cFormVar = "w_FORSEL", cQueryName = "FORSEL",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente oppure obsoleto",;
    ToolTipText = "Fornitore di selezione scadenze",;
    HelpContextID = 230739882,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=295, Top=294, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MAGFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORSEL"

  func oFORSEL_2_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MAGFOR='F')
    endwith
   endif
  endfunc

  func oFORSEL_2_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORSEL_2_39.ecpDrop(oSource)
    this.Parent.oContained.link_2_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORSEL_2_39.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MAGFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MAGFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORSEL_2_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oFORSEL_2_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MAGFOR
     i_obj.w_ANCODICE=this.parent.oContained.w_FORSEL
     i_obj.ecpSave()
  endproc

  add object oDESFOR_2_40 as StdField with uid="RKAWVHAKME",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 120441290,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=438, Top=294, InputMask=replicate('X',40)

  add object oMAGALT_2_41 as StdCheck with uid="MREYRVIZRR",rtseq=50,rtrep=.f.,left=90, top=322, caption="Altri movimenti",;
    ToolTipText = "Se attivo: seleziona documenti non riferiti a clienti o fornitori",;
    HelpContextID = 90410298,;
    cFormVar="w_MAGALT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMAGALT_2_41.RadioValue()
    return(iif(this.value =1,'N',;
    'X'))
  endfunc
  func oMAGALT_2_41.GetRadio()
    this.Parent.oContained.w_MAGALT = this.RadioValue()
    return .t.
  endfunc

  func oMAGALT_2_41.SetRadio()
    this.Parent.oContained.w_MAGALT=trim(this.Parent.oContained.w_MAGALT)
    this.value = ;
      iif(this.Parent.oContained.w_MAGALT=='N',1,;
      0)
  endfunc


  add object oBtn_2_44 as StdButton with uid="GQEEWIYYRM",left=740, top=493, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 150833642;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_44.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_45 as StdButton with uid="LHRVJUTWDZ",left=740, top=7, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Visualizza la pagina dell'elenco documenti";
    , HelpContextID = 34038326;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_45.Click()
      with this.Parent.oContained
        PosPgUp(this)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_4 as StdString with uid="WEXYCECXGJ",Visible=.t., Left=16, Top=19,;
    Alignment=1, Width=82, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="WKWTZFUARM",Visible=.t., Left=50, Top=59,;
    Alignment=0, Width=64, Height=18,;
    Caption="Documento"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="RWHNHUCUAO",Visible=.t., Left=39, Top=83,;
    Alignment=1, Width=59, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="JIPRXJTPCJ",Visible=.t., Left=208, Top=83,;
    Alignment=1, Width=85, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="RGJNSAKFNJ",Visible=.t., Left=419, Top=83,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="GFGSYQKGMB",Visible=.t., Left=39, Top=110,;
    Alignment=1, Width=59, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="TVMPXBRQTI",Visible=.t., Left=208, Top=110,;
    Alignment=1, Width=85, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="TQXDLWGEQQ",Visible=.t., Left=419, Top=110,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="RCQNQMCGGA",Visible=.t., Left=50, Top=155,;
    Alignment=0, Width=55, Height=18,;
    Caption="Protocollo"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="KCXNCIRHVM",Visible=.t., Left=29, Top=177,;
    Alignment=1, Width=69, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="JBRPJMJPPX",Visible=.t., Left=223, Top=178,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="MDRFPHTNUP",Visible=.t., Left=419, Top=178,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="JYYRYYQBHK",Visible=.t., Left=29, Top=201,;
    Alignment=1, Width=69, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="AAQPHIOGGS",Visible=.t., Left=223, Top=205,;
    Alignment=1, Width=70, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="EEHWAJWMWS",Visible=.t., Left=419, Top=205,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="PJYVZHYENM",Visible=.t., Left=233, Top=266,;
    Alignment=1, Width=60, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="EDLRSPJZSE",Visible=.t., Left=232, Top=294,;
    Alignment=1, Width=61, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oBox_2_5 as StdBox with uid="JUAZYMXUEI",left=44, top=77, width=485,height=1

  add object oBox_2_20 as StdBox with uid="UJGGHIMZBJ",left=44, top=173, width=485,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_szm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_szm
Proc PosPgUp(oObj)
=cp_StandardFunction(oObj,"PgUp")
oObj.Parent.oContained.NotifyEvent('Esegui')
endproc
* --- Fine Area Manuale
