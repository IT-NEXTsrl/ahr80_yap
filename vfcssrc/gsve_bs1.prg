* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bs1                                                        *
*              Eventi da generazione fatture                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_43]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2011-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEvent
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bs1",oParentObject,m.pEvent)
return(i_retval)

define class tgsve_bs1 as StdBatch
  * --- Local variables
  pEvent = space(3)
  w_CHKMAST = 0
  w_SERDETT = space(10)
  w_MESS = space(10)
  w_RIGVAL = 0
  w_OK = .f.
  w_POSIZ = 0
  w_PADRE = .NULL.
  w_POSIZ = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue eventi da Selezione/Spostamento Riga Master (da GSVE_KSF)
    * --- Evento: 
    *     'MRC' - Eseguito il Check sul Master 
    *     'MRU' - Eseguito l' Uncheck sul Master
    *     'DRC' - Eseguito il Check sul Dettaglio
    *     'DBQ' - Evento Before Query sul Dettaglio
    *     'DAQ' - Evento After Query sul Dettaglio
    *     'SCH' - Evento Seleziona/Deseleziona Tutti sul Dettaglio
    *     'SC1' - Evento Seleziona/Deseleziona Tutti sul Master
    this.w_PADRE = this.oParentObject
    this.w_CHKMAST = -1
    ND = this.oParentObject.w_ZoomDett.cCursor
    NM = this.oParentObject.w_ZoomMast.cCursor
    do case
      case this.pEvent = "MRC"
        * --- Eseguito il Check sul Master
        * --- Inserisce il Flag Selezionato sulle righe del Detail
        do case
          case this.oParentObject.w_FLVALO="N"
            * --- Conto le righe non descrittive con MVPREZZO =0
            Select ( this.oParentObject.w_ZoomDett.cCursor ) 
 Go Top 
 SUM (IIF(NVL(MVPREZZO,0)=0 AND MVTIPRIG<>"D" ,1,0)) TO this.w_RIGVAL 
 Go top
            * --- Nel caso non esistano righe non valorizzate seleziono tutto
            if this.w_RIGVAL=0
              UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 1 WHERE 1=1
            endif
          case this.oParentObject.w_FLVALO="R"
            * --- Righe valorizzate: seleziono solo righe con MVPREZZO <>0 o Descrittive
            UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 1 WHERE NVL(MVPREZZO,0)<>0 Or MVTIPRIG="D"
          case this.oParentObject.w_FLVALO="T"
            * --- Seleziono tutto
            UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 1
        endcase
        this.w_CHKMAST = 1
      case this.pEvent = "MRU"
        * --- Eseguito l' Uncheck sul Master
        * --- Toglie il Flag Selezionato sulle righe del Detail
        UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 0
        this.w_CHKMAST = 0
      case this.pEvent = "DRC"
        * --- Eseguito il Check sul Dettaglio
        * --- Inserisce il Flag Selezionato sulla riga del Detail
        this.w_OK = .T.
        if this.oParentObject.w_ZoomMast.GetVar("XCHK") = 1
          * --- Selezionato il Documento Master
          if this.oParentObject.w_FLVALO="N"
            * --- Conto le righe non descrittive con MVPREZZO =0
            Select ( this.oParentObject.w_ZoomDett.cCursor )
            this.w_POSIZ = Recno()
            Go Top 
 SUM (IIF(NVL(MVPREZZO,0)=0 AND MVTIPRIG<>"D" ,1,0)) TO this.w_RIGVAL 
 Go this.w_POSIZ
            if this.w_RIGVAL>0
              this.w_OK = .F.
              this.w_MESS = "Impossibile selezionare: esistono righe non valorizzate"
            endif
          endif
          if &ND..MVPREZZO=0 And this.oParentObject.w_FLVALO="R" And &ND..MVTIPRIG<>"D"
            * --- Se Attiva combo Righe Valorizzate non posso selezionare righe con MVPREZZO=0 se non Descrittive
            this.w_OK = .F.
            this.w_MESS = "Impossibile selezionare una riga non valorizzata"
          endif
          if Not( this.w_OK)
            * --- Errore
            this.oParentObject.w_ZoomDett.grd.Columns[ this.w_PADRE.w_ZoomDett.grd.ColumnCount ].chk.Value = 0
            ah_Msg(this.w_MESS)
          endif
        else
          this.oParentObject.w_ZoomDett.grd.Columns[ this.w_PADRE.w_ZoomDett.grd.ColumnCount ].chk.Value = 0
          this.w_MESS = "Documento non selezionato"
          ah_Msg(this.w_MESS)
        endif
      case this.pEvent = "DBQ"
        * --- Evento Before Query sul Dettaglio
        * --- Salva in RigheDoc i Documenti Deselezionati sul Dettaglio
        if USED( this.oParentObject.w_ZoomDett.cCursor ) AND USED("GeneApp")
          this.w_SERDETT = this.oParentObject.w_ZoomDett.GetVar("MVSERIAL")
          SELECT * FROM GeneApp WHERE MVSERIAL <> this.w_SERDETT UNION ;
          SELECT MVSERIAL, CPROWNUM FROM (ND) WHERE XCHK = 0 INTO CURSOR GeneApp
          SELECT ( this.oParentObject.w_ZoomDett.cCursor )
        endif
      case this.pEvent = "DAQ"
        * --- Evento After Query sul Dettaglio
        * --- Riporta sul Dettaglio le Variazioni precedentemente definite e salvate in GeneApp
        if USED( this.oParentObject.w_ZoomDett.cCursor )
          this.w_CHKMAST = this.oParentObject.w_ZoomMast.GetVar("XCHK")
          if this.w_CHKMAST=1
            do case
              case this.oParentObject.w_FLVALO="N"
                * --- Conto le righe non descrittive con MVPREZZO =0
                Select ( this.oParentObject.w_ZoomDett.cCursor ) 
 Go Top 
 SUM (IIF(NVL(MVPREZZO,0)=0 AND MVTIPRIG<>"D" ,1,0)) TO this.w_RIGVAL 
 Go top
                * --- Nel caso non esistano righe non valorizzate seleziono tutto
                if this.w_RIGVAL=0
                  UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 1 WHERE 1=1
                endif
              case this.oParentObject.w_FLVALO="R"
                * --- Righe valorizzate: seleziono solo righe con MVPREZZO <>0 o Descrittive
                UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 1 WHERE NVL(MVPREZZO,0)<>0 Or MVTIPRIG="D"
              case this.oParentObject.w_FLVALO="T"
                * --- Seleziono tutto
                UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 1
            endcase
            * --- Toglie il Flag alle Eventuali Righe da non trasferire
            if USED("GeneApp")
              SELECT ( this.oParentObject.w_ZoomDett.cCursor )
              SCAN FOR NOT EMPTY(NVL(MVSERIAL,""))
              SELECT * FROM GeneApp WHERE GeneApp.MVSERIAL = &ND..MVSERIAL ;
              AND GeneApp.CPROWNUM = &ND..CPROWNUM INTO CURSOR APPCUR
              SELECT ( this.oParentObject.w_ZoomDett.cCursor )
              if RECCOUNT("APPCUR")<>0
                REPLACE XCHK WITH 0
              endif
              ENDSCAN
            endif
          else
            if this.oParentObject.w_FLVALO="N"
              UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 0 WHERE NVL(MVPREZZO,0)<>0
            else
              UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 0
            endif
          endif
        endif
      case this.pEvent = "SCH"
        * --- Evento w_SELEZI Changed
        * --- Seleziona/Deselezione la Righe Dettaglio (se consentito)
        if USED( this.oParentObject.w_ZoomDett.cCursor )
          if this.oParentObject.w_ZoomMast.GetVar("XCHK") = 1
            * --- Selezionato il Documento Master
            if this.oParentObject.w_SELEZI = "S"
              do case
                case this.oParentObject.w_FLVALO="N"
                  * --- Conto le righe non descrittive con MVPREZZO =0
                  Select ( this.oParentObject.w_ZoomDett.cCursor ) 
 Go Top 
 SUM (IIF(NVL(MVPREZZO,0)=0 AND MVTIPRIG<>"D" ,1,0)) TO this.w_RIGVAL 
 Go top
                  * --- Nel caso non esistano righe non valorizzate seleziono tutto
                  if this.w_RIGVAL=0
                    UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 1 WHERE 1=1
                  else
                    this.w_MESS = "Impossibile selezionare: presenti righe non valorizzate"
                    ah_Msg(this.w_MESS)
                  endif
                case this.oParentObject.w_FLVALO="R"
                  * --- Righe valorizzate: seleziono solo righe con MVPREZZO <>0 o Descrittive
                  UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 1 WHERE NVL(MVPREZZO,0)<>0 Or MVTIPRIG="D"
                case this.oParentObject.w_FLVALO="T"
                  * --- Seleziono tutto
                  UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 1
              endcase
            else
              * --- deseleziona Tutto
              UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK = 0
            endif
          else
            this.w_MESS = "Documento da generare non selezionato"
            ah_Msg(this.w_MESS)
            this.oParentObject.w_SELEZI = IIF(this.oParentObject.w_SELEZI = "S", "D", "S")
          endif
        endif
      case this.pEvent="SC1" Or this.pEvent="SFA" Or this.pEvent="SF1"
        ND = this.oParentObject.w_ZoomDett.cCursor
        NM = this.oParentObject.w_ZoomMast.cCursor
        if this.pEvent = "SC1"
          Filtro = " 1 = 1"
        else
          Select (NM)
          this.w_POSIZ = Recno()
          Filtro = " Recno() <= " + STR(this.w_POSIZ)
        endif
        * --- Evento w_SELEZ1 Changed
        * --- Seleziona/Deselezione la Testata del Documento (se consentito)
        if USED( this.oParentObject.w_ZoomMast.cCursor )
          * --- Selezionato il Documento Master
          if (this.oParentObject.w_SELEZ1 = "S" And this.pEvent= "SC1") Or (this.pEvent = "SFA")
            * --- Seleziona Tutto
            SELECT (NM)
            if Ah_YesNo("La selezione automatica di %1 documenti potrebbe durare molto tempo. Proseguire comunque?","", IIF(this.pEvent="SC1",ALLTRIM(STR(reccount())), Alltrim(Str(this.w_POSIZ))) )
               
 UPDATE (NM) SET XCHK=1 Where &Filtro 
 SELECT (NM) 
 GO TOP 
 SCAN FOR &Filtro
              this.oParentObject.w_SERIAL = MVSERIAL
              this.w_PADRE.NotifyEvent("CalcRig")     
              UPDATE (ND) SET XCHK=1
               
 SELECT (NM) 
 Endscan
            else
              i_retcode = 'stop'
              return
            endif
          else
            * --- deseleziona Tutto
            * --- Seleziona Tutto
            SELECT (NM)
            if Ah_YesNo("La deselezione automatica di %1 documenti potrebbe durare molto tempo. Proseguire comunque?","", IIF(this.pEvent="SC1",ALLTRIM(STR(reccount())), Alltrim(Str(this.w_POSIZ))) )
               
 UPDATE (NM) SET XCHK=0 Where &Filtro 
 SELECT (NM) 
 GO TOP 
 SCAN For &Filtro
              this.oParentObject.w_SERIAL = MVSERIAL
              this.w_PADRE.NotifyEvent("CalcRig")     
              UPDATE (ND) SET XCHK=0
               
 SELECT (NM) 
 Endscan
            else
              i_retcode = 'stop'
              return
            endif
          endif
           
 SELECT (NM) 
 Go Top
          this.oParentObject.w_SERIAL = MVSERIAL
          this.w_PADRE.NotifyEvent("CalcRig")     
        else
          this.w_MESS = "Documento da generare non selezionato"
          ah_Msg(this.w_MESS)
          this.oParentObject.w_SELEZ1 = IIF(this.oParentObject.w_SELEZ1 = "S", "D", "S")
        endif
    endcase
    * --- Setta Proprieta' Campi del Cursore
    if this.w_CHKMAST <> -1
      SELECT ( this.oParentObject.w_ZoomDett.cCursor )
      FOR I=1 TO this.oParentObject.w_ZoomDett.grd.ColumnCount
      NC = ALLTRIM(STR(I))
      if UPPER(this.oParentObject.w_ZoomDett.grd.Column&NC..ControlSource)<>"XCHK"
        this.oParentObject.w_ZoomDett.grd.Column&NC..DynamicForeColor = ;
        "IIF(XCHK=1, RGB(255,0,0), RGB(0,0,0))"
      else
        this.oParentObject.w_ZoomDett.grd.Column&NC..Enabled = IIF(this.w_CHKMAST=1, .T., .F.)
      endif
      ENDFOR
    endif
    * --- !!!ATTENZIONE: Eliminare l'esecuzione della mCalc dalla Maschera
    * --- perche per alcuni eventi, richiamerebbe questo batch, entrando in Loop...
    this.bUpdateParentObject = .F.
  endproc


  proc Init(oParentObject,pEvent)
    this.pEvent=pEvent
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEvent"
endproc
