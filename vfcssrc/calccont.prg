* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: calccont                                                        *
*              Gestione cambi bilanci                                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_2]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-11                                                      *
* Last revis.: 2006-02-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tcalccont",oParentObject)
return(i_retval)

define class tcalccont as StdBatch
  * --- Local variables
  pre_div = space(3)
  w_DATESE = ctod("  /  /  ")
  w_AZI = space(5)
  * --- WorkFile variables
  ESERCIZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo Batch calcola i cambi per i bilanci
    * --- Riceve come variabili esterne w_MOLTI E w_DIVIDE che sono il moltiplicatore e divisore tra le due valute
    * --- pi� l'esercizio di conto w_ESE, w_INIESE data inizio esercizio, w_VALESE valuta esercizio
    * --- IL BATCH CALCOLA ANCHE L'ESERCIZIO PRECEDENTE
    * --- Nei casi sopra infatti il cambio � sempre tra valute europee e viene applicato dopo g_DATEUR
    this.oParentObject.w_MOLTI = 0
    this.oParentObject.w_DIVIDE = 0
    * --- trovo l'esercizio precedente a quello di stampa
    this.w_datese = this.oParentObject.w_iniese-1
    this.w_AZI = i_codazi
    vq_exec("query\ESEPREC",this,"eserci")
    if reccount()>0
      this.pre_div = NVL(ESERCI.ESVALNAZ, SPACE(3))
      this.oParentObject.pre_ese = NVL(ESERCI.ESCODESE, SPACE(4))
      if this.pre_div<>this.oParentObject.w_VALESE
        * --- l'esercizio precedente ha un'altra valuta rispetto a quello attuale - se l'hanno uguale non devo fare niente
        * --- cerco il cambio tra la moneta di conto dell'esercizio attuale e quello precedente all'esercizio di stampa
        this.oParentObject.w_MOLTI = IIF(this.oParentObject.w_INIESE<g_DATEUR,1, GETCAM(this.oParentObject.w_VALESE,this.oParentObject.w_INIESE,1) )
        this.oParentObject.w_DIVIDE = GETCAM(this.pre_div,this.oParentObject.w_INIESE,1)
      else
        * --- Se hanno la solita valuta non applico cambi
        this.oParentObject.w_DIVIDE = 1
        this.oParentObject.w_MOLTI = 1
      endif
    else
      * --- Se non ho esercizio precedente non applico cambi
      this.oParentObject.w_DIVIDE = 1
      this.oParentObject.w_MOLTI = 1
    endif
    if used("ESERCI")
      select eserci
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ESERCIZI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
