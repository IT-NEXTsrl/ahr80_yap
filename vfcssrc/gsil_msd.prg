* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsil_msd                                                        *
*              Strutture dati                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_59]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-07                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsil_msd"))

* --- Class definition
define class tgsil_msd as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 554
  Height = 270+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=97111913
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  STRUTABE_IDX = 0
  XDC_TABLE_IDX = 0
  XDC_FIELDS_IDX = 0
  cFile = "STRUTABE"
  cKeySelect = "STNOMTAB"
  cKeyWhere  = "STNOMTAB=this.w_STNOMTAB"
  cKeyDetail  = "STNOMTAB=this.w_STNOMTAB and STNOMCAM=this.w_STNOMCAM"
  cKeyWhereODBC = '"STNOMTAB="+cp_ToStrODBC(this.w_STNOMTAB)';

  cKeyDetailWhereODBC = '"STNOMTAB="+cp_ToStrODBC(this.w_STNOMTAB)';
      +'+" and STNOMCAM="+cp_ToStrODBC(this.w_STNOMCAM)';

  cKeyWhereODBCqualified = '"STRUTABE.STNOMTAB="+cp_ToStrODBC(this.w_STNOMTAB)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsil_msd"
  cComment = "Strutture dati"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_STNOMTAB = space(8)
  o_STNOMTAB = space(8)
  w_NOMTAB = space(15)
  w_STDESTAB = space(35)
  w_STNOMCAM = space(8)
  w_STDESCAM = space(30)
  w_ST__TIPO = space(1)
  w_ST__LUNG = 0
  w_ST__NDEC = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'STRUTABE','gsil_msd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsil_msdPag1","gsil_msd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Strutture dati")
      .Pages(1).HelpContextID = 157667096
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTNOMTAB_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='XDC_TABLE'
    this.cWorkTables[2]='XDC_FIELDS'
    this.cWorkTables[3]='STRUTABE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.STRUTABE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.STRUTABE_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_STNOMTAB = NVL(STNOMTAB,space(8))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from STRUTABE where STNOMTAB=KeySet.STNOMTAB
    *                            and STNOMCAM=KeySet.STNOMCAM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.STRUTABE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2],this.bLoadRecFilter,this.STRUTABE_IDX,"gsil_msd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('STRUTABE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "STRUTABE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' STRUTABE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'STNOMTAB',this.w_STNOMTAB  )
      select * from (i_cTable) STRUTABE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_STNOMTAB = NVL(STNOMTAB,space(8))
          * evitabile
          *.link_1_3('Load')
        .w_NOMTAB = LEFT(.w_STNOMTAB+REPLICATE(' ',15),15)
        .w_STDESTAB = NVL(STDESTAB,space(35))
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'STRUTABE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_STNOMCAM = NVL(STNOMCAM,space(8))
          * evitabile
          *.link_2_1('Load')
          .w_STDESCAM = NVL(STDESCAM,space(30))
          .w_ST__TIPO = NVL(ST__TIPO,space(1))
          .w_ST__LUNG = NVL(ST__LUNG,0)
          .w_ST__NDEC = NVL(ST__NDEC,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace STNOMCAM with .w_STNOMCAM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_NOMTAB = LEFT(.w_STNOMTAB+REPLICATE(' ',15),15)
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_STNOMTAB=space(8)
      .w_NOMTAB=space(15)
      .w_STDESTAB=space(35)
      .w_STNOMCAM=space(8)
      .w_STDESCAM=space(30)
      .w_ST__TIPO=space(1)
      .w_ST__LUNG=0
      .w_ST__NDEC=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_STNOMTAB))
         .link_1_3('Full')
        endif
        .w_NOMTAB = LEFT(.w_STNOMTAB+REPLICATE(' ',15),15)
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_STNOMCAM))
         .link_2_1('Full')
        endif
        .DoRTCalc(5,5,.f.)
        .w_ST__TIPO = 'C'
        .w_ST__LUNG = 10
        .w_ST__NDEC = 0
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'STRUTABE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSTNOMTAB_1_3.enabled = i_bVal
      .Page1.oPag.oSTDESTAB_1_5.enabled = i_bVal
      .Page1.oPag.oBtn_1_7.enabled = i_bVal
      .Page1.oPag.oObj_1_6.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSTNOMTAB_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSTNOMTAB_1_3.enabled = .t.
        .Page1.oPag.oSTDESTAB_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'STRUTABE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.STRUTABE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STNOMTAB,"STNOMTAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STDESTAB,"STDESTAB",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STRUTABE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])
    i_lTable = "STRUTABE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.STRUTABE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_STNOMCAM C(8);
      ,t_STDESCAM C(30);
      ,t_ST__TIPO N(3);
      ,t_ST__LUNG N(3);
      ,t_ST__NDEC N(1);
      ,STNOMCAM C(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsil_msdbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTNOMCAM_2_1.controlsource=this.cTrsName+'.t_STNOMCAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTDESCAM_2_2.controlsource=this.cTrsName+'.t_STDESCAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oST__TIPO_2_3.controlsource=this.cTrsName+'.t_ST__TIPO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oST__LUNG_2_4.controlsource=this.cTrsName+'.t_ST__LUNG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oST__NDEC_2_5.controlsource=this.cTrsName+'.t_ST__NDEC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(102)
    this.AddVLine(328)
    this.AddVLine(407)
    this.AddVLine(449)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTNOMCAM_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STRUTABE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STRUTABE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])
      *
      * insert into STRUTABE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'STRUTABE')
        i_extval=cp_InsertValODBCExtFlds(this,'STRUTABE')
        i_cFldBody=" "+;
                  "(STNOMTAB,STDESTAB,STNOMCAM,STDESCAM,ST__TIPO"+;
                  ",ST__LUNG,ST__NDEC,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_STNOMTAB)+","+cp_ToStrODBC(this.w_STDESTAB)+","+cp_ToStrODBCNull(this.w_STNOMCAM)+","+cp_ToStrODBC(this.w_STDESCAM)+","+cp_ToStrODBC(this.w_ST__TIPO)+;
             ","+cp_ToStrODBC(this.w_ST__LUNG)+","+cp_ToStrODBC(this.w_ST__NDEC)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'STRUTABE')
        i_extval=cp_InsertValVFPExtFlds(this,'STRUTABE')
        cp_CheckDeletedKey(i_cTable,0,'STNOMTAB',this.w_STNOMTAB,'STNOMCAM',this.w_STNOMCAM)
        INSERT INTO (i_cTable) (;
                   STNOMTAB;
                  ,STDESTAB;
                  ,STNOMCAM;
                  ,STDESCAM;
                  ,ST__TIPO;
                  ,ST__LUNG;
                  ,ST__NDEC;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_STNOMTAB;
                  ,this.w_STDESTAB;
                  ,this.w_STNOMCAM;
                  ,this.w_STDESCAM;
                  ,this.w_ST__TIPO;
                  ,this.w_ST__LUNG;
                  ,this.w_ST__NDEC;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.STRUTABE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_STNOMCAM) AND NOT EMPTY(t_ST__TIPO)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'STRUTABE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " STDESTAB="+cp_ToStrODBC(this.w_STDESTAB)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and STNOMCAM="+cp_ToStrODBC(&i_TN.->STNOMCAM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'STRUTABE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  STDESTAB=this.w_STDESTAB;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and STNOMCAM=&i_TN.->STNOMCAM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_STNOMCAM) AND NOT EMPTY(t_ST__TIPO)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and STNOMCAM="+cp_ToStrODBC(&i_TN.->STNOMCAM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and STNOMCAM=&i_TN.->STNOMCAM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace STNOMCAM with this.w_STNOMCAM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update STRUTABE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'STRUTABE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " STDESTAB="+cp_ToStrODBC(this.w_STDESTAB)+;
                     ",STDESCAM="+cp_ToStrODBC(this.w_STDESCAM)+;
                     ",ST__TIPO="+cp_ToStrODBC(this.w_ST__TIPO)+;
                     ",ST__LUNG="+cp_ToStrODBC(this.w_ST__LUNG)+;
                     ",ST__NDEC="+cp_ToStrODBC(this.w_ST__NDEC)+;
                     ",STNOMCAM="+cp_ToStrODBC(this.w_STNOMCAM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and STNOMCAM="+cp_ToStrODBC(STNOMCAM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'STRUTABE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      STDESTAB=this.w_STDESTAB;
                     ,STDESCAM=this.w_STDESCAM;
                     ,ST__TIPO=this.w_ST__TIPO;
                     ,ST__LUNG=this.w_ST__LUNG;
                     ,ST__NDEC=this.w_ST__NDEC;
                     ,STNOMCAM=this.w_STNOMCAM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and STNOMCAM=&i_TN.->STNOMCAM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.STRUTABE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_STNOMCAM) AND NOT EMPTY(t_ST__TIPO)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete STRUTABE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and STNOMCAM="+cp_ToStrODBC(&i_TN.->STNOMCAM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and STNOMCAM=&i_TN.->STNOMCAM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_STNOMCAM) AND NOT EMPTY(t_ST__TIPO)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.STRUTABE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_STNOMTAB<>.w_STNOMTAB
          .w_NOMTAB = LEFT(.w_STNOMTAB+REPLICATE(' ',15),15)
        endif
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=STNOMTAB
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STNOMTAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_KCD',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_STNOMTAB)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_STNOMTAB))
          select TBNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STNOMTAB)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STNOMTAB) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oSTNOMTAB_1_3'),i_cWhere,'GSUT_KCD',"Nome tabella",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STNOMTAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_STNOMTAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_STNOMTAB)
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STNOMTAB = NVL(_Link_.TBNAME,space(8))
      this.w_STDESTAB = NVL(_Link_.TBCOMMENT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_STNOMTAB = space(8)
      endif
      this.w_STDESTAB = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STNOMTAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STNOMCAM
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STNOMCAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_STNOMCAM)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_NOMTAB);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT,FLDECIMA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_NOMTAB;
                     ,'FLNAME',trim(this.w_STNOMCAM))
          select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT,FLDECIMA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STNOMCAM)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STNOMCAM) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oSTNOMCAM_2_1'),i_cWhere,'',"Nome campo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_NOMTAB<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT,FLDECIMA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT,FLDECIMA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Manca il nome campo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT,FLDECIMA";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_NOMTAB);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT,FLDECIMA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STNOMCAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT,FLDECIMA";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_STNOMCAM);
                   +" and TBNAME="+cp_ToStrODBC(this.w_NOMTAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_NOMTAB;
                       ,'FLNAME',this.w_STNOMCAM)
            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT,FLDECIMA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STNOMCAM = NVL(_Link_.FLNAME,space(8))
      this.w_STDESCAM = NVL(_Link_.FLCOMMEN,space(30))
      this.w_ST__TIPO = NVL(_Link_.FLTYPE,space(1))
      this.w_ST__LUNG = NVL(_Link_.FLLENGHT,0)
      this.w_ST__NDEC = NVL(_Link_.FLDECIMA,0)
    else
      if i_cCtrl<>'Load'
        this.w_STNOMCAM = space(8)
      endif
      this.w_STDESCAM = space(30)
      this.w_ST__TIPO = space(1)
      this.w_ST__LUNG = 0
      this.w_ST__NDEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STNOMCAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSTNOMTAB_1_3.value==this.w_STNOMTAB)
      this.oPgFrm.Page1.oPag.oSTNOMTAB_1_3.value=this.w_STNOMTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDESTAB_1_5.value==this.w_STDESTAB)
      this.oPgFrm.Page1.oPag.oSTDESTAB_1_5.value=this.w_STDESTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTNOMCAM_2_1.value==this.w_STNOMCAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTNOMCAM_2_1.value=this.w_STNOMCAM
      replace t_STNOMCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTNOMCAM_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTDESCAM_2_2.value==this.w_STDESCAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTDESCAM_2_2.value=this.w_STDESCAM
      replace t_STDESCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTDESCAM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__TIPO_2_3.RadioValue()==this.w_ST__TIPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__TIPO_2_3.SetRadio()
      replace t_ST__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__TIPO_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__LUNG_2_4.value==this.w_ST__LUNG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__LUNG_2_4.value=this.w_ST__LUNG
      replace t_ST__LUNG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__LUNG_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__NDEC_2_5.value==this.w_ST__NDEC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__NDEC_2_5.value=this.w_ST__NDEC
      replace t_ST__NDEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__NDEC_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'STRUTABE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsil_msd
      this.w_STNOMTAB=ALLTRIM(this.w_STNOMTAB)
      this.w_STDESTAB=ALLTRIM(this.w_STDESTAB)
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (NOT EMPTY(t_STNOMCAM) AND NOT EMPTY(t_ST__TIPO));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if NOT EMPTY(.w_STNOMCAM) AND NOT EMPTY(.w_ST__TIPO)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_STNOMTAB = this.w_STNOMTAB
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_STNOMCAM) AND NOT EMPTY(t_ST__TIPO))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_STNOMCAM=space(8)
      .w_STDESCAM=space(30)
      .w_ST__TIPO=space(1)
      .w_ST__LUNG=0
      .w_ST__NDEC=0
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_STNOMCAM))
        .link_2_1('Full')
      endif
      .DoRTCalc(5,5,.f.)
        .w_ST__TIPO = 'C'
        .w_ST__LUNG = 10
        .w_ST__NDEC = 0
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_STNOMCAM = t_STNOMCAM
    this.w_STDESCAM = t_STDESCAM
    this.w_ST__TIPO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__TIPO_2_3.RadioValue(.t.)
    this.w_ST__LUNG = t_ST__LUNG
    this.w_ST__NDEC = t_ST__NDEC
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_STNOMCAM with this.w_STNOMCAM
    replace t_STDESCAM with this.w_STDESCAM
    replace t_ST__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__TIPO_2_3.ToRadio()
    replace t_ST__LUNG with this.w_ST__LUNG
    replace t_ST__NDEC with this.w_ST__NDEC
    if i_srv='A'
      replace STNOMCAM with this.w_STNOMCAM
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsil_msdPag1 as StdContainer
  Width  = 550
  height = 270
  stdWidth  = 550
  stdheight = 270
  resizeXpos=265
  resizeYpos=201
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTNOMTAB_1_3 as StdField with uid="CCVMEEHHYR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_STNOMTAB", cQueryName = "STNOMTAB",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    sErrorMsg = "Manca il nome tabella",;
    ToolTipText = "Nome della tabella",;
    HelpContextID = 56257128,;
   bGlobalFont=.t.,;
    Height=21, Width=99, Left=90, Top=11, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="XDC_TABLE", cZoomOnZoom="GSUT_KCD", oKey_1_1="TBNAME", oKey_1_2="this.w_STNOMTAB"

  func oSTNOMTAB_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTNOMTAB_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTNOMTAB_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oSTNOMTAB_1_3.readonly and this.parent.oSTNOMTAB_1_3.isprimarykey)
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oSTNOMTAB_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_KCD',"Nome tabella",'',this.parent.oContained
   endif
  endproc
  proc oSTNOMTAB_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSUT_KCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TBNAME=this.parent.oContained.w_STNOMTAB
    i_obj.ecpSave()
  endproc

  add object oSTDESTAB_1_5 as StdField with uid="CRWMSVFVDR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_STDESTAB", cQueryName = "STDESTAB",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 61852264,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=288, Top=11, InputMask=replicate('X',35)


  add object oObj_1_6 as cp_runprogram with uid="KZUWHPJNFT",left=1, top=284, width=213,height=19,;
    caption='GSIL_BTL',;
   bGlobalFont=.t.,;
    prg="GSIL_BTL",;
    cEvent = "w_ST__TIPO Changed",;
    nPag=1;
    , HelpContextID = 41359538


  add object oBtn_1_7 as StdButton with uid="EWGMREZXIT",left=498, top=48, width=48,height=45,;
    CpPicture="BMP\APPLICA.BMP", caption="", nPag=1;
    , ToolTipText = "Aggiorna la struttura dati";
    , HelpContextID = 97111818;
    , caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        do GSIL_BCC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    with this.Parent.oContained
      return (not empty(.w_STNOMTAB))
    endwith
  endfunc

  func oBtn_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION<>'ad hoc ENTERPRISE')
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=13, top=52, width=481,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="STNOMCAM",Label1="Campo",Field2="STDESCAM",Label2="Descrizione",Field3="ST__TIPO",Label3="Tipo",Field4="ST__LUNG",Label4="Lungh.",Field5="ST__NDEC",Label5="Dec.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218978938

  add object oStr_1_1 as StdString with uid="BERVIKNOQT",Visible=.t., Left=4, Top=13,;
    Alignment=1, Width=85, Height=18,;
    Caption="Nome tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="WWFYWCOVET",Visible=.t., Left=202, Top=13,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=3,top=71,;
    width=477+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=4,top=72,width=476+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='XDC_FIELDS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='XDC_FIELDS'
        oDropInto=this.oBodyCol.oRow.oSTNOMCAM_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsil_msdBodyRow as CPBodyRowCnt
  Width=467
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oSTNOMCAM_2_1 as StdTrsField with uid="CAEGZZYKES",rtseq=4,rtrep=.t.,;
    cFormVar="w_STNOMCAM",value=space(8),isprimarykey=.t.,;
    HelpContextID = 39479923,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Manca il nome campo",;
   bGlobalFont=.t.,;
    Height=17, Width=88, Left=-2, Top=0, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_NOMTAB", oKey_2_1="FLNAME", oKey_2_2="this.w_STNOMCAM"

  func oSTNOMCAM_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTNOMCAM_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSTNOMCAM_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oSTNOMCAM_2_1.readonly and this.parent.oSTNOMCAM_2_1.isprimarykey)
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_NOMTAB)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_NOMTAB)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oSTNOMCAM_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Nome campo",'',this.parent.oContained
   endif
  endproc

  add object oSTDESCAM_2_2 as StdTrsField with uid="HIJHGVHJHD",rtseq=5,rtrep=.t.,;
    cFormVar="w_STDESCAM",value=space(30),;
    HelpContextID = 45075059,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=88, Top=0, InputMask=replicate('X',30)

  add object oST__TIPO_2_3 as StdTrsCombo with uid="EMLPDFREYR",rtrep=.t.,;
    cFormVar="w_ST__TIPO", RowSource=""+"Char,"+"Numeric,"+"Date,"+"Logic,"+"Memo" , enabled=.f.,;
    HelpContextID = 148601461,;
    Height=21, Width=76, Left=315, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oST__TIPO_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ST__TIPO,&i_cF..t_ST__TIPO),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'N',;
    iif(xVal =3,'D',;
    iif(xVal =4,'L',;
    iif(xVal =5,'M',;
    'C'))))))
  endfunc
  func oST__TIPO_2_3.GetRadio()
    this.Parent.oContained.w_ST__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oST__TIPO_2_3.ToRadio()
    this.Parent.oContained.w_ST__TIPO=trim(this.Parent.oContained.w_ST__TIPO)
    return(;
      iif(this.Parent.oContained.w_ST__TIPO=='C',1,;
      iif(this.Parent.oContained.w_ST__TIPO=='N',2,;
      iif(this.Parent.oContained.w_ST__TIPO=='D',3,;
      iif(this.Parent.oContained.w_ST__TIPO=='L',4,;
      iif(this.Parent.oContained.w_ST__TIPO=='M',5,;
      0))))))
  endfunc

  func oST__TIPO_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oST__LUNG_2_4 as StdTrsField with uid="HJDDEPBJSB",rtseq=7,rtrep=.t.,;
    cFormVar="w_ST__LUNG",value=0,enabled=.f.,;
    HelpContextID = 195331475,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "La lunghezza del campo deve essere maggiore di 0 (uguale a 8 se di tipo date) (uguale a 1 se di tipo logic)",;
   bGlobalFont=.t.,;
    Height=17, Width=38, Left=394, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oST__NDEC_2_5 as StdTrsField with uid="ZVNWNIFYOG",rtseq=8,rtrep=.t.,;
    cFormVar="w_ST__NDEC",value=0,enabled=.f.,;
    HelpContextID = 58423913,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=25, Left=437, Top=0, cSayPict=["9"], cGetPict=["9"]
  add object oLast as LastKeyMover
  * ---
  func oSTNOMCAM_2_1.When()
    return(.t.)
  proc oSTNOMCAM_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oSTNOMCAM_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsil_msd','STRUTABE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".STNOMTAB=STRUTABE.STNOMTAB";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
