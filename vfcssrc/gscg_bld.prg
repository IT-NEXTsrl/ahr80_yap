* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bld                                                        *
*              Visualizzazione dichiarazioni                                   *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-02                                                      *
* Last revis.: 2012-03-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPROV
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bld",oParentObject,m.pPROV)
return(i_retval)

define class tgscg_bld as StdBatch
  * --- Local variables
  pPROV = space(3)
  w_PUNPAD = .NULL.
  * --- WorkFile variables
  AZIENDA_idx=0
  SALDIART_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia la Ricerca ,  cancella Rif Dichiarazione Intento --da GSCG_KDT
    do case
      case this.pPROV="RIC"
        * --- Lancio la Ricerca
        if upper(this.oParentObject.class)="TGSCG_KAD"
          This.OparentObject.w_ZOOMDOC.cCpQueryName = "QUERY\GSCG1KAD"
        endif
        This.OparentObject.NotifyEvent("Esegui")
      case this.pPROV="DEL"
        * --- Rimuove il riferimento alla dichiarazione di intento
        this.oParentObject.w_RIFDIC = SPACE(10)
        this.oParentObject.w_ANNDIC = Space(4)
        this.oParentObject.w_DATDIC = cp_CharToDate("  /  /    ")
        this.oParentObject.w_NUMDIC = 0
      case this.pPROV="INT"
        * --- La visualizzazione pu� essere lanciata da men� oppure 
        *     dalla Manutenzione Dichiarazioni
        this.w_PUNPAD = this.oParentObject.oParentObject
        if Type("this.w_PUNPAD")="O"
          this.oParentObject.w_RIFDIC = this.w_PUNPAD.w_DISERIAL
          if !EMPTY(this.oParentObject.w_RIFDIC)
            this.oParentObject.w_NUMDIC = this.w_PUNPAD.w_DINUMDOC
            this.oParentObject.w_ANNDIC = this.w_PUNPAD.w_DI__ANNO
            this.oParentObject.w_DATDIC = this.w_PUNPAD.w_DIDATDIC
            this.oParentObject.w_TIPCON = this.w_PUNPAD.w_DITIPCON
            this.oParentObject.w_FLVEAC = IIF(this.oParentObject.w_TIPCON="C","V","A")
            this.oParentObject.w_CODCON = this.w_PUNPAD.w_DICODICE
            if this.oParentObject.w_TIPCON="C"
              this.oParentObject.w_CLISEL = this.oParentObject.w_CODCON
            else
              this.oParentObject.w_FORSEL = this.oParentObject.w_CODCON
            endif
            this.oParentObject.o_CODCON = IIF(this.oParentObject.w_TIPCON = "F", this.oParentObject.w_FORSEL, this.oParentObject.w_CLISEL)
            this.oParentObject.w_CODCON = IIF(this.oParentObject.w_TIPCON = "F", this.oParentObject.w_FORSEL, this.oParentObject.w_CLISEL)
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANDESCRI"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANDESCRI;
                from (i_cTable) where;
                    ANTIPCON = this.oParentObject.w_TIPCON;
                    and ANCODICE = this.oParentObject.w_CODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_DESCON = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pPROV)
    this.pPROV=pPROV
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='SALDIART'
    this.cWorkTables[3]='CONTI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPROV"
endproc
