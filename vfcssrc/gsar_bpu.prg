* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bpu                                                        *
*              Pubblicazione clienti/fornitori/agenti                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-11                                                      *
* Last revis.: 2005-12-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SCELTA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bpu",oParentObject,m.w_SCELTA)
return(i_retval)

define class tgsar_bpu as StdBatch
  * --- Local variables
  w_SCELTA = space(3)
  CURSORE = space(10)
  w_MODIFICATO = .f.
  w_CODICE = space(20)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CONTI_idx=0
  AGENTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine richiamata dalla maschera gsar_kpu
    * --- Variabili Caller
    * --- Variabili locali
    * --- --
    do case
      case this.w_SCELTA = "OPT"
        * --- Selezione o deselezione di tutti i programmi visualizzati nella maschera.
        this.CURSORE = this.oParentObject.w_ZOOMPRO.cCursor
        if this.oParentObject.w_OPZIONI = "SEL"
          UPDATE (this.CURSORE) SET XCHK = 1
        else
          UPDATE (this.CURSORE) SET XCHK = 0
        endif
        SELECT(this.oParentObject.w_ZOOMPRO.cCursor)
        GO TOP
        this.oParentObject.w_ZOOMPRO.refresh()
      case this.w_SCELTA = "MOD"
        * --- Aggiornamento prodotti pubblicati
        this.w_MODIFICATO = .F.
        this.CURSORE = this.oParentObject.w_ZOOMPRO.cCursor
        Select (this.CURSORE)
        go top
        SCAN FOR XCHK=1
        this.w_MODIFICATO = .T.
        if this.oParentObject.w_TIPANA="A"
          this.w_CODICE = LEFT(ANCODICE,5)
          * --- Write into AGENTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.AGENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.AGENTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AGFLGCPZ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PUBBLICA),'AGENTI','AGFLGCPZ');
                +i_ccchkf ;
            +" where ";
                +"AGCODAGE = "+cp_ToStrODBC(this.w_CODICE);
                   )
          else
            update (i_cTable) set;
                AGFLGCPZ = this.oParentObject.w_PUBBLICA;
                &i_ccchkf. ;
             where;
                AGCODAGE = this.w_CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          this.w_CODICE = LEFT(ANCODICE,15)
          * --- Write into CONTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANFLGCPZ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PUBBLICA),'CONTI','ANFLGCPZ');
                +i_ccchkf ;
            +" where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPANA);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
                   )
          else
            update (i_cTable) set;
                ANFLGCPZ = this.oParentObject.w_PUBBLICA;
                &i_ccchkf. ;
             where;
                ANTIPCON = this.oParentObject.w_TIPANA;
                and ANCODICE = this.w_CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        ENDSCAN
        * --- Fine ....
        if this.w_MODIFICATO = .F.
          ah_ErrorMsg("� necessario selezionare almeno un'anagrafica",48,"")
        else
          this.oParentObject.notifyevent("Interroga")
          do case
            case this.oParentObject.w_PUBBLICA="S"
              ah_ErrorMsg("Per le anagrafiche selezionate � stato abilitato il check <pubblica su web>",64,"")
            case this.oParentObject.w_PUBBLICA="N"
              ah_ErrorMsg("Per le anagrafiche selezionate � stato disabilitato il check <pubblica su web>",64,"")
          endcase
        endif
    endcase
  endproc


  proc Init(oParentObject,w_SCELTA)
    this.w_SCELTA=w_SCELTA
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AGENTI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SCELTA"
endproc
