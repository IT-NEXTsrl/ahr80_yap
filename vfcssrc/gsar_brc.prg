* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_brc                                                        *
*              Controlli su tabella ripartiz.                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_35]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2000-01-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_brc",oParentObject,m.pTipo)
return(i_retval)

define class tgsar_brc as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_MESS = space(10)
  w_CURSOR = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dalla Movimentazione GSAR_API (Conti)
    * --- Questo Batch controlla che siano stati inseriti sia il Centro di Costo che la Voce.
    this.w_MESS = " "
    do case
      case this.pTipo="C"
        SELECT (this.oParentObject.GSAR_MRC.cTrsName)
      case this.pTipo="S"
        SELECT (this.oParentObject.NotifyEvent("Save"))
    endcase
    GO TOP
    SCAN FOR (EMPTY(NVL(t_MRCODVOC,"")) AND NOT(EMPTY(NVL(t_MRCODCDC,"")))) ;
    OR (EMPTY(NVL(t_MRCODCDC,"")) AND NOT(EMPTY(NVL(t_MRCODVOC,""))))
    do case
      case EMPTY(NVL(t_MRCODVOC,""))
        this.w_MESS = ah_Msgformat("Inserire la voce di costo/ricavo")
      case EMPTY(NVL(t_MRCODCDC,""))
        this.w_MESS = ah_Msgformat("Inserire il centro di costo/ricavo")
    endcase
    ENDSCAN
    if NOT EMPTY(this.w_MESS)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
