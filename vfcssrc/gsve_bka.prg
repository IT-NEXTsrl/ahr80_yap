* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bka                                                        *
*              Gestione cruscotto analisi scostamento prezzo                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-06-12                                                      *
* Last revis.: 2013-04-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bka",oParentObject,m.pOPER)
return(i_retval)

define class tgsve_bka as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_OLDFORNI = space(20)
  w_PRZCHK = 0
  w_DLCODART = space(20)
  w_DLCODVAR = space(20)
  w_DLUNIMIS = space(3)
  w_DLCODICE = space(5)
  w_DLPREZZO = 0
  w_FLPRZUND = space(1)
  w_PREZ_UCA = 0
  w_DATE_UCA = ctod("  /  /  ")
  w_LIST_CHK = space(5)
  w_PRCODFOR = space(15)
  w_KEYLISTF = space(10)
  w_GUIDCLF = space(14)
  w_MVFLSCOR = space(1)
  w_GUIDART = space(14)
  w_CONDVA = space(1)
  w_MVSERCHK = space(10)
  w_MVROWCHK = 0
  w_PADRE = .NULL.
  w_DATADELE = ctod("  /  /  ")
  w_RESULT = space(1)
  w_OBJGEST = .NULL.
  w_oGSVE_KAS = .NULL.
  w_oGSVE_MDV = .NULL.
  w_MVDATDOC = ctod("  /  /  ")
  w_NUMREC = 0
  * --- WorkFile variables
  PAR_RIOR_idx=0
  CONTI_idx=0
  TMPVISDO_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = This.oParentObject
    do case
      case this.pOPER=="RICERCA"
        if this.oParentObject.w_FLFRSTRU
          if i_ADVANCEDHEADERZOOM
            * --- Check if the fields of discounts exist and are in sequence
            this.Page_2(this.oParentObject.w_ZOOMDETT,"Sconti/maggiorazioni","MVSCONT1","MVSCONT2","MVSCONT3","MVSCONT4","","1","2","3","4","")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Check if the fields of business units exist and are in sequence
            this.Page_2(this.oParentObject.w_ZOOMDETT,"Business unit","MVCODBUN","BUDESCRI","","","","Codice","Descrizione","","","")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Check if the fields of mark-up classes exist and are in sequence
            this.Page_2(this.oParentObject.w_ZOOMDETT,"Classe di ricarico","ARCODRIC","CRDESCRI","CRPERCEN","","","Codice","Descrizione","%","","")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          * --- Delete from TMPVISDO
          i_nConn=i_TableProp[this.TMPVISDO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVISDO_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"ID__GEST = "+cp_ToStrODBC(this.oParentObject.w_ID__GEST);
                   )
          else
            delete from (i_cTable) where;
                  ID__GEST = this.oParentObject.w_ID__GEST;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        this.oParentObject.w_FLFRSTRU = .F.
        * --- Creation of temporary table with documents to be processed
        * --- Insert into TMPVISDO
        i_nConn=i_TableProp[this.TMPVISDO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVISDO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSVE0KAS",this.TMPVISDO_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Rows analysis of selected documents to calculate the values of final cost and it deviations
        this.w_OLDFORNI = "##@@ZZ_AU@@##"
        * --- Select from TMPVISDO
        i_nConn=i_TableProp[this.TMPVISDO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVISDO_idx,2],.t.,this.TMPVISDO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPVISDO ";
               ,"_Curs_TMPVISDO")
        else
          select * from (i_cTable);
            into cursor _Curs_TMPVISDO
        endif
        if used('_Curs_TMPVISDO')
          select _Curs_TMPVISDO
          locate for 1=1
          do while not(eof())
          this.w_PRZCHK = (_Curs_TMPVISDO.MVIMPNAZ / _Curs_TMPVISDO.MVQTAUM1) - IIF( EMPTY( _Curs_TMPVISDO.MVPERPRO ) , _Curs_TMPVISDO.MVIMPPRO , _Curs_TMPVISDO.MVPREZZO * _Curs_TMPVISDO.MVPERPRO / 100 )
          this.w_DLCODART = _Curs_TMPVISDO.MVCODART
          this.w_DLCODVAR = _Curs_TMPVISDO.MVCODVAR
          this.w_DLUNIMIS = _Curs_TMPVISDO.MVUNIMIS
          this.w_GUIDART = _Curs_TMPVISDO.GUIDART
          this.w_CONDVA = _Curs_TMPVISDO.ARPREZUM
          * --- Storing key to update the temporary table at the end of price check
          this.w_MVSERCHK = _Curs_TMPVISDO.MVSERIAL
          this.w_MVROWCHK = _Curs_TMPVISDO.CPROWNUM
          * --- Reset calculated variables
          this.w_LIST_CHK = SPACE(5)
          this.w_DATE_UCA = cp_CharToDate("  /  /    ")
          this.w_PREZ_UCA = 0
          do case
            case _Curs_TMPVISDO.ARCHKUCA="U"
              * --- Elaboration of last purchase cost 
              DECLARE ARRCALC (4,1)
              * --- Type('ArrCalc') must have type different from 'L'
              ARRCALC(1)=0
              this.w_RESULT = CALCULT("I", _Curs_TMPVISDO.MVKEYSAL, _Curs_TMPVISDO.MVCODMAG,"-",g_PERVAL,g_CAOVAL,g_PERPUL," ", " "," ", @ARRCALC, ICASE(this.oParentObject.w_FLTYPEAN="D", _Curs_TMPVISDO.MVDATDOC, this.oParentObject.w_FLTYPEAN="O", this.oParentObject.w_FLDATRIF, cp_ChartoDate("  /  /    ") ) )
              this.w_PREZ_UCA = ARRCALC(3)
              this.w_DATE_UCA = ARRCALC(4)
              release ARRCALC
            case _Curs_TMPVISDO.ARCHKUCA="S"
              * --- Elaboration of statistical list price
              this.w_DLCODICE = NVL(_Curs_TMPVISDO.ARLISCHK, SPACE(5) )
              this.w_MVDATDOC = IIF(this.oParentObject.w_FLTYPEAN="O", this.oParentObject.w_FLDATRIF, _Curs_TMPVISDO.MVDATDOC )
              if !EMPTY(this.w_DLCODICE)
                * --- Select from gsve_biu
                do vq_exec with 'gsve_biu',this,'_Curs_gsve_biu','',.f.,.t.
                if used('_Curs_gsve_biu')
                  select _Curs_gsve_biu
                  locate for 1=1
                  do while not(eof())
                  this.w_DLPREZZO = _Curs_gsve_biu.DLPREZZO
                  exit
                    select _Curs_gsve_biu
                    continue
                  enddo
                  use
                endif
              endif
              this.w_PREZ_UCA = this.w_DLPREZZO
              this.w_DATE_UCA = this.w_MVDATDOC
              this.w_LIST_CHK = this.w_DLCODICE
            case _Curs_TMPVISDO.ARCHKUCA="F"
              this.w_PRCODFOR = ""
              if EMPTY(NVL(this.w_DLCODVAR, " "))
                * --- Read from PAR_RIOR
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PRCODFOR"+;
                    " from "+i_cTable+" PAR_RIOR where ";
                        +"PRCODART = "+cp_ToStrODBC(this.w_DLCODART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PRCODFOR;
                    from (i_cTable) where;
                        PRCODART = this.w_DLCODART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_PRCODFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              else
                * --- Read from PAR_RIOR
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PRCODFOR"+;
                    " from "+i_cTable+" PAR_RIOR where ";
                        +"PRCODART = "+cp_ToStrODBC(this.w_DLCODART);
                        +" and PRCODVAR = "+cp_ToStrODBC(this.w_DLCODVAR);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PRCODFOR;
                    from (i_cTable) where;
                        PRCODART = this.w_DLCODART;
                        and PRCODVAR = this.w_DLCODVAR;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_PRCODFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              if !EMPTY(NVL(this.w_PRCODFOR, " "))
                if this.w_PRCODFOR<>this.w_OLDFORNI
                  * --- Read from CONTI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.CONTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "GESTGUID,ANSCORPO"+;
                      " from "+i_cTable+" CONTI where ";
                          +"ANTIPCON = "+cp_ToStrODBC("F");
                          +" and ANCODICE = "+cp_ToStrODBC(this.w_PRCODFOR);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      GESTGUID,ANSCORPO;
                      from (i_cTable) where;
                          ANTIPCON = "F";
                          and ANCODICE = this.w_PRCODFOR;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_GUIDCLF = NVL(cp_ToDate(_read_.GESTGUID),cp_NullValue(_read_.GESTGUID))
                    this.w_MVFLSCOR = NVL(cp_ToDate(_read_.ANSCORPO),cp_NullValue(_read_.ANSCORPO))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.w_KEYLISTF = SYS(2015)
                  DECLARE ARRCALC (4,1)
                  this.w_RESULT = CALCLIST("CREA",@ARRCALC,"A","N",_Curs_TMPVISDO.MVCODVAL,"F",this.w_PRCODFOR, IIF(this.oParentObject.w_FLTYPEAN<>"O", _Curs_TMPVISDO.MVDATDOC, this.oParentObject.w_FLDATRIF ),"","",this.w_KEYLISTF,-20,this.w_MVFLSCOR," "," ")
                  release ARRCALC
                  this.w_OLDFORNI = this.w_PRCODFOR
                endif
                DECLARE ARRCALC (17,1)
                this.w_RESULT = CalPrzli(this.w_KEYLISTF," "," " ," "," ", this.w_DLCODART, this.w_DLCODVAR, _Curs_TMPVISDO.MVQTAUM1," ", _Curs_TMPVISDO.MVCODVAL, _Curs_TMPVISDO.MVCAOVAL, IIF(this.oParentObject.w_FLTYPEAN<>"O", _Curs_TMPVISDO.MVDATDOC, this.oParentObject.w_FLDATRIF ), @ARRCALC, this.w_DLUNIMIS, this.w_CONDVA, "N", " ", "AF"+this.w_PRCODFOR,this.w_GUIDART, this.w_GUIDCLF, "R", "N", .T., _Curs_TMPVISDO.MVQTAUM1, " ")
                this.w_DLPREZZO = 0
                this.w_DLCODICE = ""
                if ARRCALC(7)=1 
                  this.w_DLPREZZO = ARRCALC(5)
                  this.w_DLCODICE = ARRCALC(10)
                endif
                release ARRCALC
                this.w_PREZ_UCA = this.w_DLPREZZO
                this.w_LIST_CHK = this.w_DLCODICE
              endif
          endcase
          if this.oParentObject.w_FLTYPROW = "S"
            if cp_Round( IIF(this.w_PREZ_UCA=0, 0, (this.w_PRZCHK - this.w_PREZ_UCA) * 100 / this.w_PREZ_UCA), 2) >= this.oParentObject.w_FLPERMAR
              this.w_FLPRZUND = "S"
            else
              * --- Price isn't lower than last purchase cost
              this.w_FLPRZUND = "N"
            endif
          else
            if cp_Round( IIF(this.w_PREZ_UCA=0, 0, (this.w_PRZCHK - this.w_PREZ_UCA) * 100 / this.w_PREZ_UCA), 2) <= this.oParentObject.w_FLPERMAR * IIF( this.oParentObject.w_FLTYPROW = "I", -1, 1 )
              this.w_FLPRZUND = "S"
            else
              * --- Price isn't lower than last purchase cost
              this.w_FLPRZUND = "N"
            endif
          endif
          if this.oParentObject.w_FLTYPROW<>"T" AND this.w_FLPRZUND="N"
            * --- Only rows with price lower than control price must be displayed. Delete row with regular price
            * --- Delete from TMPVISDO
            i_nConn=i_TableProp[this.TMPVISDO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVISDO_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"ID__GEST = "+cp_ToStrODBC(this.oParentObject.w_ID__GEST);
                    +" and MVSERIAL = "+cp_ToStrODBC(this.w_MVSERCHK);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWCHK);
                     )
            else
              delete from (i_cTable) where;
                    ID__GEST = this.oParentObject.w_ID__GEST;
                    and MVSERIAL = this.w_MVSERCHK;
                    and CPROWNUM = this.w_MVROWCHK;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          else
            * --- All rows must be displayed. Update row data
            * --- Write into TMPVISDO
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVISDO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVISDO_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVISDO_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PREZ_UCA ="+cp_NullLink(cp_ToStrODBC(this.w_PREZ_UCA),'TMPVISDO','PREZ_UCA');
              +",DATE_UCA ="+cp_NullLink(cp_ToStrODBC(this.w_DATE_UCA),'TMPVISDO','DATE_UCA');
              +",LIST_CHK ="+cp_NullLink(cp_ToStrODBC(this.w_LIST_CHK),'TMPVISDO','LIST_CHK');
              +",FLPRZUND ="+cp_NullLink(cp_ToStrODBC(this.w_FLPRZUND),'TMPVISDO','FLPRZUND');
              +",PRZCHECK ="+cp_NullLink(cp_ToStrODBC(this.w_PRZCHK),'TMPVISDO','PRZCHECK');
              +",LIMRGUCA ="+cp_NullLink(cp_ToStrODBC(this.w_PRZCHK - this.w_PREZ_UCA),'TMPVISDO','LIMRGUCA');
                  +i_ccchkf ;
              +" where ";
                  +"ID__GEST = "+cp_ToStrODBC(this.oParentObject.w_ID__GEST);
                  +" and MVSERIAL = "+cp_ToStrODBC(this.w_MVSERCHK);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWCHK);
                     )
            else
              update (i_cTable) set;
                  PREZ_UCA = this.w_PREZ_UCA;
                  ,DATE_UCA = this.w_DATE_UCA;
                  ,LIST_CHK = this.w_LIST_CHK;
                  ,FLPRZUND = this.w_FLPRZUND;
                  ,PRZCHECK = this.w_PRZCHK;
                  ,LIMRGUCA = this.w_PRZCHK - this.w_PREZ_UCA;
                  &i_ccchkf. ;
               where;
                  ID__GEST = this.oParentObject.w_ID__GEST;
                  and MVSERIAL = this.w_MVSERCHK;
                  and CPROWNUM = this.w_MVROWCHK;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
            select _Curs_TMPVISDO
            continue
          enddo
          use
        endif
        this.w_PADRE.NotifyEvent("AggZoom")     
        this.w_PADRE.oPgfrm.ActivePage = 2
      case this.pOPER=="CHIUSURA"
        * --- Delete temporary data of current elaboration
        * --- Delete from TMPVISDO
        i_nConn=i_TableProp[this.TMPVISDO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVISDO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"ID__GEST = "+cp_ToStrODBC(this.oParentObject.w_ID__GEST);
                 )
        else
          delete from (i_cTable) where;
                ID__GEST = this.oParentObject.w_ID__GEST;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete data older than thirty days to prevent unwanted filling of the table
        *     when users closing application incorrectly
        this.w_DATADELE = i_DatSys - 30
        * --- Delete from TMPVISDO
        i_nConn=i_TableProp[this.TMPVISDO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVISDO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"DATAELAB <= "+cp_ToStrODBC(this.w_DATADELE);
                 )
        else
          delete from (i_cTable) where;
                DATAELAB <= this.w_DATADELE;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.pOPER=="APRIDADOC"
        if lower(this.w_PADRE.class)= "im_contextmenu" and (left(lower(this.w_PADRE.oContained.class), 9) == "tgsve_mdv" OR vartype(this.w_PADRE.w_MVSERIAL)="C" )
          this.w_oGSVE_KAS = GSVE_KAS()
          if this.w_oGSVE_KAS.bSec1
            do case
              case lower(this.w_PADRE.class)= "im_contextmenu" and left(lower(this.w_PADRE.oContained.class), 9) == "tgsve_mdv"
                this.w_OBJGEST = this.w_PADRE.oContained.Parent.Parent.Parent
                this.w_oGSVE_KAS.w_MVFLVEAC = this.w_OBJGEST.w_MVFLVEAC
                this.w_oGSVE_KAS.w_MVCLADOC = this.w_OBJGEST.w_MVCLADOC
                this.w_oGSVE_KAS.w_MVTIPDOC = this.w_OBJGEST.w_MVTIPDOC
                this.w_oGSVE_KAS.w_MVCONINI = this.w_OBJGEST.w_MVCODCON
                this.w_oGSVE_KAS.w_MVCONFIN = this.w_OBJGEST.w_MVCODCON
                this.w_oGSVE_KAS.w_MVNREINI = this.w_OBJGEST.w_MVNUMREG
                this.w_oGSVE_KAS.w_MVNREFIN = this.w_OBJGEST.w_MVNUMREG
                this.w_oGSVE_KAS.w_MVREGINI = this.w_OBJGEST.w_MVDATREG
                this.w_oGSVE_KAS.w_MVREGFIN = this.w_OBJGEST.w_MVDATREG
                this.w_oGSVE_KAS.w_MVCODVAL = this.w_OBJGEST.w_MVCODVAL
                this.w_oGSVE_KAS.w_MVFLPROV = this.w_OBJGEST.w_MVFLPROV
              case vartype(this.w_PADRE.w_MVSERIAL)="C"
                * --- Read from DOC_MAST
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "MVFLVEAC,MVCLADOC,MVTIPDOC,MVCODCON,MVNUMREG,MVDATREG,MVCODVAL,MVFLPROV"+;
                    " from "+i_cTable+" DOC_MAST where ";
                        +"MVSERIAL = "+cp_ToStrODBC(this.w_PADRE.w_MVSERIAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    MVFLVEAC,MVCLADOC,MVTIPDOC,MVCODCON,MVNUMREG,MVDATREG,MVCODVAL,MVFLPROV;
                    from (i_cTable) where;
                        MVSERIAL = this.w_PADRE.w_MVSERIAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_oGSVE_KAS.w_MVFLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
                  this.w_oGSVE_KAS.w_MVCLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
                  this.w_oGSVE_KAS.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
                  this.w_oGSVE_KAS.w_MVCONINI = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
                  this.w_oGSVE_KAS.w_MVNREINI = NVL(cp_ToDate(_read_.MVNUMREG),cp_NullValue(_read_.MVNUMREG))
                  this.w_oGSVE_KAS.w_MVREGINI = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
                  this.w_oGSVE_KAS.w_MVCODVAL = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
                  this.w_oGSVE_KAS.w_MVFLPROV = NVL(cp_ToDate(_read_.MVFLPROV),cp_NullValue(_read_.MVFLPROV))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_oGSVE_KAS.w_MVCONFIN = this.w_oGSVE_KAS.w_MVCONINI
                this.w_oGSVE_KAS.w_MVNREFIN = this.w_oGSVE_KAS.w_MVNREINI
                this.w_oGSVE_KAS.w_MVREGFIN = this.w_oGSVE_KAS.w_MVREGINI
            endcase
            this.w_oGSVE_KAS.NotifyEvent("Ricerca")     
          endif
        else
          ah_ErrorMsg("La gestione chiamante non dispone delle informazioni necessarie per l'apertura del cruscotto difformità prezzi. Impossibile proseguire")
        endif
        This.bUpdateParentObject = .F.
      case this.pOPER=="APRIMAST" OR this.pOPER=="APRIRIGA"
        this.w_oGSVE_MDV = GSVE_MDV( ALLTRIM(this.oParentObject.w_ATFLVEAC)+ALLTRIM(this.oParentObject.w_ATCLADOC) )
        if this.w_oGSVE_MDV.bSec1
          this.w_oGSVE_MDV.ecpFilter()     
          this.w_oGSVE_MDV.w_MVSERIAL = this.oParentObject.w_MVSERATT
          this.w_oGSVE_MDV.ecpSave()     
          if this.w_oGSVE_MDV.bLoaded
            if this.pOPER=="APRIRIGA"
              this.w_NUMREC = this.w_oGSVE_MDV.Search( "CPROWNUM = " + ALLTRIM(STR(this.oParentObject.w_MVROWATT)) )
              if this.w_NUMREC <> -1
                this.w_oGSVE_MDV.SetRow(this.w_NUMREC)     
              else
                ah_ErrorMsg("Riga non trovata")
              endif
            endif
          else
            ah_ErrorMsg("Documento non trovato")
            this.w_oGSVE_MDV.ecpQuit()     
          endif
          this.w_oGSVE_MDV = .NULL.
        endif
    endcase
  endproc


  procedure Page_2
    param pObjZoom,pMergeCaption,pField1Name,pField2Name,pField3Name,pField4Name,pField5Name,pField1NewCaption,pField2NewCaption,pField3NewCaption,pField4NewCaption,pField5NewCaption
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    private objManyHeader
    m.objManyHeader=createobject("timer")
    private w_oColumn
    m.w_oColumn=createobject("timer")
    private w_Field1Pos
    m.w_Field1Pos = 0
    private w_Field2Pos
    m.w_Field2Pos = 0
    private w_Field3Pos
    m.w_Field3Pos = 0
    private w_Field4Pos
    m.w_Field4Pos = 0
    private w_Field5Pos
    m.w_Field5Pos = 0
    private w_FieldIdx
    m.w_FieldIdx = 0
    * --- Definition of an object variable without class will generate an error during object creation.
    *     Timer class is used only to prevent error
    Local L_MACRO
    m.w_Field1Pos = IIF( EMPTY( m.pField1Name ) , -1, m.pObjZoom.nColOrd[ ASCAN( m.pObjZoom.cFields , m.pField1Name, 1, -1, 1, 15)] )
    m.w_Field2Pos = IIF( EMPTY( m.pField2Name ) , -1, m.pObjZoom.nColOrd[ ASCAN( m.pObjZoom.cFields , m.pField2Name, 1, -1, 1, 15)] )
    m.w_Field3Pos = IIF( EMPTY( m.pField3Name ) , -1, m.pObjZoom.nColOrd[ ASCAN( m.pObjZoom.cFields , m.pField3Name, 1, -1, 1, 15)] )
    m.w_Field4Pos = IIF( EMPTY( m.pField4Name ) , -1, m.pObjZoom.nColOrd[ ASCAN( m.pObjZoom.cFields , m.pField4Name, 1, -1, 1, 15)] )
    m.w_Field5Pos = IIF( EMPTY( m.pField5Name ) , -1, m.pObjZoom.nColOrd[ ASCAN( m.pObjZoom.cFields , m.pField5Name, 1, -1, 1, 15)] )
    if m.w_Field1Pos + 1 = m.w_Field2Pos AND ( m.w_Field3Pos < 0 OR m.w_Field2Pos + 1 = m.w_Field3Pos ) AND ( m.w_Field4Pos < 0 OR m.w_Field3Pos + 1 = m.w_Field4Pos) AND ( m.w_Field5Pos < 0 OR m.w_Field4Pos + 1 = m.w_Field5Pos)
      m.pObjZoom.grd.HeaderHeight = 40
      m.objManyHeader = m.pObjZoom.oRefHeaderObject
      m.objManyHeader._FontName = i_cGRDFONTNAME
      m.objManyHeader._FontSize = i_nGRDFONTSIZE
      m.objManyHeader.lMaster = .T.
      m.objManyHeader.lSplitBar = .T.
      * --- Columns merge and common description valorization
      m.objManyHeader.MergeHeader(m.w_Field1Pos , MAX(m.w_Field2Pos, m.w_Field3Pos, m.w_Field4Pos, m.w_Field5Pos ) , ah_MsgFormat(m.pMergeCaption))
      m.w_FieldIdx = 1
      do while m.w_FieldIdx <= 5
        L_MACRO = "IIF( EMPTY( pField" + ALLTRIM(STR(m.w_FieldIdx)) + "Name ) , -1, pObjZoom.nColOrd[ASCAN( pObjZoom.cFields , pField" + ALLTRIM(STR(m.w_FieldIdx)) + "Name, 1, -1, 1, 15)] )"
        if &L_MACRO > 0
          m.w_oColumn = .NULL.
          m.w_oColumn = m.objManyHeader.GetColumn( &L_MACRO )
          if VARTYPE( m.w_oColumn ) = "O"
            L_MACRO = "m.pField" + ALLTRIM(STR(m.w_FieldIdx)) + "NewCaption"
            m.w_oColumn.HDR.Caption = ah_MsgFormat( &L_MACRO )
          endif
        endif
        m.w_FieldIdx = m.w_FieldIdx + 1
      enddo
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PAR_RIOR'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='TMPVISDO'
    this.cWorkTables[4]='DOC_MAST'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_TMPVISDO')
      use in _Curs_TMPVISDO
    endif
    if used('_Curs_gsve_biu')
      use in _Curs_gsve_biu
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
