* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_baz                                                        *
*              Cambio azienda                                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-17                                                      *
* Last revis.: 2015-06-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODAZI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_baz",oParentObject,m.pCODAZI)
return(i_retval)

define class tgsut_baz as StdBatch
  * --- Local variables
  pCODAZI = space(5)
  w_MOBIMODE = .f.
  w_AZI = space(5)
  w_CODAZI = space(5)
  w_CODESE = space(4)
  w_INIESE = ctod("  /  /  ")
  w_FINESE = ctod("  /  /  ")
  w_FLBUNI = space(1)
  w_RAGAZI = space(40)
  w_CODBUN = space(3)
  w_DESBUN = space(40)
  w_UTE = 0
  w_DATSYS = ctod("  /  /  ")
  w_DESUTE = space(20)
  w_OUTE = 0
  w_FLAG = .f.
  * --- WorkFile variables
  CPUSERS_idx=0
  AZIENDA_idx=0
  BUSIUNIT_idx=0
  AHEUSRCO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- -----------Variabili per il cambio azienda Inizio------------------
    this.w_CODESE = g_CODESE
    this.w_UTE = i_CODUTE
    this.w_OUTE = i_CODUTE
    this.w_DATSYS = i_DATSYS
    this.w_FLAG = .f.
    * --- -----------Variabili per il cambio azienda Fine------------------
    * --- Read from CPUSERS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CPUSERS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPUSERS_idx,2],.t.,this.CPUSERS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "NAME"+;
        " from "+i_cTable+" CPUSERS where ";
            +"CODE = "+cp_ToStrODBC(i_CODUTE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        NAME;
        from (i_cTable) where;
            CODE = i_CODUTE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESUTE = NVL(cp_ToDate(_read_.NAME),cp_NullValue(_read_.NAME))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CODAZI = this.pCODAZI
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZFLBUNI,AZRAGAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZFLBUNI,AZRAGAZI;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLBUNI = NVL(cp_ToDate(_read_.AZFLBUNI),cp_NullValue(_read_.AZFLBUNI))
      this.w_RAGAZI = NVL(cp_ToDate(_read_.AZRAGAZI),cp_NullValue(_read_.AZRAGAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_AZI = this.w_CODAZI
    if isAhr() or isAlt()
      do GS___BCK with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      if isAhe() or isAhpa()
        GS___BCK(this,"A")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Delete from AHEUSRCO
    i_nConn=i_TableProp[this.AHEUSRCO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AHEUSRCO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"UCCODUTE = "+cp_ToStrODBC(i_CODUTE);
             )
    else
      delete from (i_cTable) where;
            UCCODUTE = i_CODUTE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Cambio azienda
    do GS___BAZ with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,pCODAZI)
    this.pCODAZI=pCODAZI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='BUSIUNIT'
    this.cWorkTables[4]='AHEUSRCO'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODAZI"
endproc
