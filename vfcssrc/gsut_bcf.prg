* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bcf                                                        *
*              Lancia anagrafica frasi modello                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-17                                                      *
* Last revis.: 2007-12-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bcf",oParentObject)
return(i_retval)

define class tgsut_bcf as StdBatch
  * --- Local variables
  w_OBJECT = .NULL.
  w_OBJ = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                      Lancia anagrafica Frasi Modello da tasto destro cui campi memo
    * --- Istanzio l'anagrafica
    this.w_OBJECT = GSUT_AFM()
    * --- Controllo se ha passato il test di accesso
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- Vado in caricamento
    this.w_OBJECT.ECPLOAD()     
    this.w_OBJECT.w_FMGSTRIF = iif(pemstatus(this.oParentObject, "getSecurityCode", 5), this.oParentObject.getSecurityCode(), this.oParentObject.cprg)
    this.w_OBJECT.SetControlsValue()     
    this.w_OBJ = this.w_OBJECT.GetcTRL("w_FM__MEMO")
    if vartype(this.w_OBJ)="O"
      * --- Viene posizionato il cursore sul campo FM__MEMO
      this.w_OBJ.Setfocus()     
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
