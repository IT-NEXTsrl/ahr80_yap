* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bsf                                                        *
*              Elabora dettaglio differite                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-10-13                                                      *
* Last revis.: 2013-06-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bsf",oParentObject)
return(i_retval)

define class tgsve_bsf as StdBatch
  * --- Local variables
  w_ZOOM = space(10)
  w_OREC = 0
  w_SERIAL = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Dettagli Fatture Differite (da GSVE_KSF)
    this.w_ZOOM = this.oParentObject.w_ZoomMast
    * --- Lancia la Query
    ah_msg("Ricerca documenti da generare...")
    this.oParentObject.NotifyEvent("Calcola")
    WAIT CLEAR
    NC = this.w_Zoom.cCursor
    UPDATE (NC) SET XCHK = 1
    GO TOP
    * --- Setta Proprieta' Campi del Cursore
    FOR I=1 TO This.w_Zoom.grd.ColumnCount
    NC = ALLTRIM(STR(I))
    if UPPER(This.w_Zoom.grd.Column&NC..ControlSource)<>"XCHK"
      This.w_Zoom.grd.Column&NC..DynamicForeColor = ;
      "IIF(XCHK=0, RGB(0,0,0), RGB(255,0,0))"
    endif
    ENDFOR
    * --- Crea il Cursore di Appoggio Dettagli (Conterra' i Riferimenti alle Righe/Documenti DA NON GENERARE)
    CREATE CURSOR GeneApp (MVSERIAL C(10), CPROWNUM N(4,0))
    if g_APPLICATION = "ad hoc ENTERPRISE"
      VQ_EXEC("QUERY\GSVEDKFD1.VQR",this,"CursSelezione")
      if USED("CursSelezione")
        if this.oParentObject.w_FLVALO="T"
          * --- Tutte le righe da Generare
          SELECT MVSERIAL,CPROWNUM FROM CursSelezione INTO CURSOR GeneApp
        else
          * --- Escludo i documenti con sole righe descrittive
          SELECT MVSERIAL INTO CURSOR Seldoc FROM CursSelezione ; 
 GROUP BY MVSERIAL HAVING (MIN(MVTIPRIG)<>"D" OR MAX(MVTIPRIG)<>"D")
          SELECT MVSERIAL,CPROWNUM FROM CursSelezione ; 
 WHERE MVSERIAL in (SELECT MVSERIAL FROM Seldoc ) INTO CURSOR GeneApp
          Select Seldoc 
 Use
        endif
        Select CursSelezione 
 Use
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
