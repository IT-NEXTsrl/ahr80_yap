* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg2bcf                                                        *
*              Messaggi in contabilizzazione fittizia                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_272]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-07-28                                                      *
* Last revis.: 2010-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg2bcf",oParentObject,m.pParam)
return(i_retval)

define class tgscg2bcf as StdBatch
  * --- Local variables
  pParam = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messaggi in contabilizzazione fittizia di Anticipazioni, Acconti e Fatture (da GSCG1KCF)
    if (this.pParam="A" AND this.oParentObject.w_ACCONTI="S" AND this.oParentObject.w_ESCACC<>"S") OR (this.pParam="F" AND this.oParentObject.w_FATTURE="S" AND this.oParentObject.w_ESCFAT<>"S")
      ah_ErrorMsg("Disattivando questo check nelle stampe di analisi dei crediti, ripartizione dell'incassato e studi di settore le fatture risulteranno comunque incassate totalmente")
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
