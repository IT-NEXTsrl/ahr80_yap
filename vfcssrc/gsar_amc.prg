* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_amc                                                        *
*              Mastri                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_40]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2015-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_amc"))

* --- Class definition
define class tgsar_amc as StdForm
  Top    = 17
  Left   = 16

  * --- Standard Properties
  Width  = 566
  Height = 221+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-20"
  HelpContextID=210335593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  MASTRI_IDX = 0
  MASTRI_IDX = 0
  STUMPIAC_IDX = 0
  AZIENDA_IDX = 0
  NUMAUT_M_IDX = 0
  cFile = "MASTRI"
  cKeySelect = "MCCODICE"
  cKeyWhere  = "MCCODICE=this.w_MCCODICE"
  cKeyWhereODBC = '"MCCODICE="+cp_ToStrODBC(this.w_MCCODICE)';

  cKeyWhereODBCqualified = '"MASTRI.MCCODICE="+cp_ToStrODBC(this.w_MCCODICE)';

  cPrg = "gsar_amc"
  cComment = "Mastri"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0AMC'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AUTOAZI = space(5)
  w_AUTO = space(1)
  o_AUTO = space(1)
  w_MCCODICE = space(15)
  o_MCCODICE = space(15)
  w_MCDESCRI = space(40)
  w_CODPIA = space(4)
  w_MCNUMLIV = 0
  o_MCNUMLIV = 0
  w_MCFLDETT = space(1)
  w_MCSEQSTA = 0
  w_MCPROSTU = space(8)
  w_MCCONSUP = space(15)
  o_MCCONSUP = space(15)
  w_DESSUP = space(40)
  w_LIVSUP = 0
  w_MCSEZBIL = space(1)
  w_MCTIPMAS = space(1)
  w_MCCODSTU = space(10)
  o_MCCODSTU = space(10)
  w_DESCON = space(30)
  w_SEZBIL = space(1)
  w_SEZBILA = space(1)
  w_CONSMAS = space(15)
  w_LIVMAS = 0
  w_AZCOACOG = space(1)
  w_READAZI = space(5)
  w_AZFLGEAC = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MASTRI','gsar_amc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_amcPag1","gsar_amc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Mastro")
      .Pages(1).HelpContextID = 168959686
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMCCODICE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='MASTRI'
    this.cWorkTables[2]='STUMPIAC'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='NUMAUT_M'
    this.cWorkTables[5]='MASTRI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MASTRI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MASTRI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MCCODICE = NVL(MCCODICE,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_11_joined
    link_1_11_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MASTRI where MCCODICE=KeySet.MCCODICE
    *
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MASTRI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MASTRI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MASTRI '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MCCODICE',this.w_MCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AUTOAZI = i_CODAZI
        .w_AUTO = space(1)
        .w_CODPIA = space(4)
        .w_DESSUP = space(40)
        .w_LIVSUP = 0
        .w_DESCON = space(30)
        .w_SEZBIL = space(1)
        .w_SEZBILA = space(1)
        .w_CONSMAS = space(15)
        .w_LIVMAS = 0
        .w_AZCOACOG = space(1)
        .w_READAZI = i_CODAZI
        .w_AZFLGEAC = space(1)
          .link_1_1('Load')
        .w_MCCODICE = NVL(MCCODICE,space(15))
        .w_MCDESCRI = NVL(MCDESCRI,space(40))
        .w_MCNUMLIV = NVL(MCNUMLIV,0)
        .w_MCFLDETT = NVL(MCFLDETT,space(1))
        .w_MCSEQSTA = NVL(MCSEQSTA,0)
        .w_MCPROSTU = NVL(MCPROSTU,space(8))
          if link_1_10_joined
            this.w_MCPROSTU = NVL(LMPROGRE110,NVL(this.w_MCPROSTU,space(8)))
            this.w_DESCON = NVL(LMDESCON110,space(30))
          else
          .link_1_10('Load')
          endif
        .w_MCCONSUP = NVL(MCCONSUP,space(15))
          if link_1_11_joined
            this.w_MCCONSUP = NVL(MCCODICE111,NVL(this.w_MCCONSUP,space(15)))
            this.w_DESSUP = NVL(MCDESCRI111,space(40))
            this.w_LIVSUP = NVL(MCNUMLIV111,0)
            this.w_SEZBIL = NVL(MCSEZBIL111,space(1))
          else
          .link_1_11('Load')
          endif
        .w_MCSEZBIL = NVL(MCSEZBIL,space(1))
        .w_MCTIPMAS = NVL(MCTIPMAS,space(1))
        .w_MCCODSTU = NVL(MCCODSTU,space(10))
          .link_1_25('Load')
          .link_1_33('Load')
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        cp_LoadRecExtFlds(this,'MASTRI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_amc
    this.w_Livmas=this.w_Mcnumliv
    this.w_Consmas=this.w_Mcconsup
    this.w_Sezbila=this.w_Mcsezbil
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AUTOAZI = space(5)
      .w_AUTO = space(1)
      .w_MCCODICE = space(15)
      .w_MCDESCRI = space(40)
      .w_CODPIA = space(4)
      .w_MCNUMLIV = 0
      .w_MCFLDETT = space(1)
      .w_MCSEQSTA = 0
      .w_MCPROSTU = space(8)
      .w_MCCONSUP = space(15)
      .w_DESSUP = space(40)
      .w_LIVSUP = 0
      .w_MCSEZBIL = space(1)
      .w_MCTIPMAS = space(1)
      .w_MCCODSTU = space(10)
      .w_DESCON = space(30)
      .w_SEZBIL = space(1)
      .w_SEZBILA = space(1)
      .w_CONSMAS = space(15)
      .w_LIVMAS = 0
      .w_AZCOACOG = space(1)
      .w_READAZI = space(5)
      .w_AZFLGEAC = space(1)
      if .cFunction<>"Filter"
        .w_AUTOAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AUTOAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_MCCODICE = iif(.cFunction='Load' AND .w_AUTO='S', 'AUTO',.w_MCCODICE)
          .DoRTCalc(4,5,.f.)
        .w_MCNUMLIV = 1
        .w_MCFLDETT = 'N'
        .w_MCSEQSTA = 1
        .w_MCPROSTU = IIF( g_TRAEXP='A' Or  g_TRAEXP='S' , '@@@@@@@@' , .w_MCPROSTU)
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_MCPROSTU))
          .link_1_10('Full')
          endif
        .w_MCCONSUP = space(15)
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_MCCONSUP))
          .link_1_11('Full')
          endif
          .DoRTCalc(11,12,.f.)
        .w_MCSEZBIL = .w_SEZBIL
        .w_MCTIPMAS = 'G'
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_MCCODSTU))
          .link_1_25('Full')
          endif
          .DoRTCalc(16,21,.f.)
        .w_READAZI = i_CODAZI
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_READAZI))
          .link_1_33('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MASTRI')
    this.DoRTCalc(23,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMCCODICE_1_3.enabled = i_bVal
      .Page1.oPag.oMCDESCRI_1_4.enabled = i_bVal
      .Page1.oPag.oMCNUMLIV_1_6.enabled = i_bVal
      .Page1.oPag.oMCFLDETT_1_7.enabled = i_bVal
      .Page1.oPag.oMCSEQSTA_1_8.enabled = i_bVal
      .Page1.oPag.oMCCONSUP_1_11.enabled = i_bVal
      .Page1.oPag.oMCSEZBIL_1_14.enabled = i_bVal
      .Page1.oPag.oMCTIPMAS_1_21.enabled = i_bVal
      .Page1.oPag.oBtn_1_9.enabled = i_bVal
      .Page1.oPag.oObj_1_35.enabled = i_bVal
      .Page1.oPag.oObj_1_36.enabled = i_bVal
      .Page1.oPag.oObj_1_37.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMCCODICE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMCCODICE_1_3.enabled = .t.
        .Page1.oPag.oMCDESCRI_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MASTRI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODICE,"MCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCDESCRI,"MCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCNUMLIV,"MCNUMLIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCFLDETT,"MCFLDETT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCSEQSTA,"MCSEQSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCPROSTU,"MCPROSTU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCONSUP,"MCCONSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCSEZBIL,"MCSEZBIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCTIPMAS,"MCTIPMAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODSTU,"MCCODSTU",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    i_lTable = "MASTRI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MASTRI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SMA with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MASTRI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MASTRI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MASTRI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MASTRI')
        i_extval=cp_InsertValODBCExtFlds(this,'MASTRI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MCCODICE,MCDESCRI,MCNUMLIV,MCFLDETT,MCSEQSTA"+;
                  ",MCPROSTU,MCCONSUP,MCSEZBIL,MCTIPMAS,MCCODSTU "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MCCODICE)+;
                  ","+cp_ToStrODBC(this.w_MCDESCRI)+;
                  ","+cp_ToStrODBC(this.w_MCNUMLIV)+;
                  ","+cp_ToStrODBC(this.w_MCFLDETT)+;
                  ","+cp_ToStrODBC(this.w_MCSEQSTA)+;
                  ","+cp_ToStrODBCNull(this.w_MCPROSTU)+;
                  ","+cp_ToStrODBCNull(this.w_MCCONSUP)+;
                  ","+cp_ToStrODBC(this.w_MCSEZBIL)+;
                  ","+cp_ToStrODBC(this.w_MCTIPMAS)+;
                  ","+cp_ToStrODBCNull(this.w_MCCODSTU)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MASTRI')
        i_extval=cp_InsertValVFPExtFlds(this,'MASTRI')
        cp_CheckDeletedKey(i_cTable,0,'MCCODICE',this.w_MCCODICE)
        INSERT INTO (i_cTable);
              (MCCODICE,MCDESCRI,MCNUMLIV,MCFLDETT,MCSEQSTA,MCPROSTU,MCCONSUP,MCSEZBIL,MCTIPMAS,MCCODSTU  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MCCODICE;
                  ,this.w_MCDESCRI;
                  ,this.w_MCNUMLIV;
                  ,this.w_MCFLDETT;
                  ,this.w_MCSEQSTA;
                  ,this.w_MCPROSTU;
                  ,this.w_MCCONSUP;
                  ,this.w_MCSEZBIL;
                  ,this.w_MCTIPMAS;
                  ,this.w_MCCODSTU;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MASTRI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MASTRI_IDX,i_nConn)
      *
      * update MASTRI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MASTRI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MCDESCRI="+cp_ToStrODBC(this.w_MCDESCRI)+;
             ",MCNUMLIV="+cp_ToStrODBC(this.w_MCNUMLIV)+;
             ",MCFLDETT="+cp_ToStrODBC(this.w_MCFLDETT)+;
             ",MCSEQSTA="+cp_ToStrODBC(this.w_MCSEQSTA)+;
             ",MCPROSTU="+cp_ToStrODBCNull(this.w_MCPROSTU)+;
             ",MCCONSUP="+cp_ToStrODBCNull(this.w_MCCONSUP)+;
             ",MCSEZBIL="+cp_ToStrODBC(this.w_MCSEZBIL)+;
             ",MCTIPMAS="+cp_ToStrODBC(this.w_MCTIPMAS)+;
             ",MCCODSTU="+cp_ToStrODBCNull(this.w_MCCODSTU)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MASTRI')
        i_cWhere = cp_PKFox(i_cTable  ,'MCCODICE',this.w_MCCODICE  )
        UPDATE (i_cTable) SET;
              MCDESCRI=this.w_MCDESCRI;
             ,MCNUMLIV=this.w_MCNUMLIV;
             ,MCFLDETT=this.w_MCFLDETT;
             ,MCSEQSTA=this.w_MCSEQSTA;
             ,MCPROSTU=this.w_MCPROSTU;
             ,MCCONSUP=this.w_MCCONSUP;
             ,MCSEZBIL=this.w_MCSEZBIL;
             ,MCTIPMAS=this.w_MCTIPMAS;
             ,MCCODSTU=this.w_MCCODSTU;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MASTRI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MASTRI_IDX,i_nConn)
      *
      * delete MASTRI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MCCODICE',this.w_MCCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
        if .o_AUTO<>.w_AUTO
            .w_MCCODICE = iif(.cFunction='Load' AND .w_AUTO='S', 'AUTO',.w_MCCODICE)
        endif
        .DoRTCalc(4,6,.t.)
        if .o_MCNUMLIV<>.w_MCNUMLIV
            .w_MCFLDETT = 'N'
        endif
        .DoRTCalc(8,8,.t.)
        if .o_MCCODSTU<>.w_MCCODSTU
            .w_MCPROSTU = IIF( g_TRAEXP='A' Or  g_TRAEXP='S' , '@@@@@@@@' , .w_MCPROSTU)
          .link_1_10('Full')
        endif
        if .o_MCNUMLIV<>.w_MCNUMLIV
            .w_MCCONSUP = space(15)
          .link_1_11('Full')
        endif
        .DoRTCalc(11,12,.t.)
        if .o_MCCONSUP<>.w_MCCONSUP
            .w_MCSEZBIL = .w_SEZBIL
        endif
        if .o_MCNUMLIV<>.w_MCNUMLIV
            .w_MCTIPMAS = 'G'
        endif
        if .o_MCCODICE<>.w_MCCODICE
          .Calculate_JYMPFCUFHW()
        endif
          .link_1_25('Full')
        .DoRTCalc(16,21,.t.)
          .link_1_33('Full')
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
    endwith
  return

  proc Calculate_JYMPFCUFHW()
    with this
          * --- Calcolo valore variabile w_codpia
          .w_CODPIA = iif(g_LEMC='S',Looktab('STU_PARA','LMCODPDC','LMCODAZI',i_CODAZI),space(4))
    endwith
  endproc
  proc Calculate_OLYGVFQXTR()
    with this
          * --- Calcola Auto
          GSAR_BSS(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMCCODICE_1_3.enabled = this.oPgFrm.Page1.oPag.oMCCODICE_1_3.mCond()
    this.oPgFrm.Page1.oPag.oMCCONSUP_1_11.enabled = this.oPgFrm.Page1.oPag.oMCCONSUP_1_11.mCond()
    this.oPgFrm.Page1.oPag.oMCSEZBIL_1_14.enabled = this.oPgFrm.Page1.oPag.oMCSEZBIL_1_14.mCond()
    this.oPgFrm.Page1.oPag.oMCTIPMAS_1_21.enabled = this.oPgFrm.Page1.oPag.oMCTIPMAS_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMCFLDETT_1_7.visible=!this.oPgFrm.Page1.oPag.oMCFLDETT_1_7.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oMCCODSTU_1_25.visible=!this.oPgFrm.Page1.oPag.oMCCODSTU_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_27.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_27.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Edit Started") or lower(cEvent)==lower("Init")
          .Calculate_JYMPFCUFHW()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
        if lower(cEvent)==lower("Insert start")
          .Calculate_OLYGVFQXTR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AUTOAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
    i_lTable = "NUMAUT_M"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2], .t., this.NUMAUT_M_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AUTOAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AUTOAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NATIPGES,NACODAZI,NAACTIVE";
                   +" from "+i_cTable+" "+i_lTable+" where NACODAZI="+cp_ToStrODBC(this.w_AUTOAZI);
                   +" and NATIPGES="+cp_ToStrODBC('MG');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NATIPGES','MG';
                       ,'NACODAZI',this.w_AUTOAZI)
            select NATIPGES,NACODAZI,NAACTIVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AUTOAZI = NVL(_Link_.NACODAZI,space(5))
      this.w_AUTO = NVL(_Link_.NAACTIVE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AUTOAZI = space(5)
      endif
      this.w_AUTO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])+'\'+cp_ToStr(_Link_.NATIPGES,1)+'\'+cp_ToStr(_Link_.NACODAZI,1)
      cp_ShowWarn(i_cKey,this.NUMAUT_M_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AUTOAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCPROSTU
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STUMPIAC_IDX,3]
    i_lTable = "STUMPIAC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2], .t., this.STUMPIAC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCPROSTU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCPROSTU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODCON,LMPROGRE,LMDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where LMPROGRE="+cp_ToStrODBC(this.w_MCPROSTU);
                   +" and LMCODCON="+cp_ToStrODBC(this.w_MCCODSTU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODCON',this.w_MCCODSTU;
                       ,'LMPROGRE',this.w_MCPROSTU)
            select LMCODCON,LMPROGRE,LMDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCPROSTU = NVL(_Link_.LMPROGRE,space(8))
      this.w_DESCON = NVL(_Link_.LMDESCON,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MCPROSTU = space(8)
      endif
      this.w_DESCON = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2])+'\'+cp_ToStr(_Link_.LMCODCON,1)+'\'+cp_ToStr(_Link_.LMPROGRE,1)
      cp_ShowWarn(i_cKey,this.STUMPIAC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCPROSTU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.STUMPIAC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.LMPROGRE as LMPROGRE110"+ ",link_1_10.LMDESCON as LMDESCON110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on MASTRI.MCPROSTU=link_1_10.LMPROGRE"+" and MASTRI.MCCODSTU=link_1_10.LMCODCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and MASTRI.MCPROSTU=link_1_10.LMPROGRE(+)"'+'+" and MASTRI.MCCODSTU=link_1_10.LMCODCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MCCONSUP
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_MCCONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCSEZBIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_MCCONSUP))
          select MCCODICE,MCDESCRI,MCNUMLIV,MCSEZBIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_MCCONSUP)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCSEZBIL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_MCCONSUP)+"%");

            select MCCODICE,MCDESCRI,MCNUMLIV,MCSEZBIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MCCONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oMCCONSUP_1_11'),i_cWhere,'',"Mastri di raggruppamento",'GSAR_AMC.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCSEZBIL";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV,MCSEZBIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCSEZBIL";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_MCCONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_MCCONSUP)
            select MCCODICE,MCDESCRI,MCNUMLIV,MCSEZBIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_DESSUP = NVL(_Link_.MCDESCRI,space(40))
      this.w_LIVSUP = NVL(_Link_.MCNUMLIV,0)
      this.w_SEZBIL = NVL(_Link_.MCSEZBIL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MCCONSUP = space(15)
      endif
      this.w_DESSUP = space(40)
      this.w_LIVSUP = 0
      this.w_SEZBIL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LIVSUP>.w_MCNUMLIV AND .w_MCCONSUP<>.w_MCCODICE
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice mastro incongruente o di livello non superiore")
        endif
        this.w_MCCONSUP = space(15)
        this.w_DESSUP = space(40)
        this.w_LIVSUP = 0
        this.w_SEZBIL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.MCCODICE as MCCODICE111"+ ",link_1_11.MCDESCRI as MCDESCRI111"+ ",link_1_11.MCNUMLIV as MCNUMLIV111"+ ",link_1_11.MCSEZBIL as MCSEZBIL111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on MASTRI.MCCONSUP=link_1_11.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and MASTRI.MCCONSUP=link_1_11.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MCCODSTU
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STUMPIAC_IDX,3]
    i_lTable = "STUMPIAC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2], .t., this.STUMPIAC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODSTU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODSTU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODCON,LMDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODCON="+cp_ToStrODBC(this.w_MCCODSTU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODCON',this.w_MCCODSTU)
            select LMCODCON,LMDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODSTU = NVL(_Link_.LMCODCON,space(10))
      this.w_DESCON = NVL(_Link_.LMDESCON,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MCCODSTU = space(10)
      endif
      this.w_DESCON = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2])+'\'+cp_ToStr(_Link_.LMCODCON,1)
      cp_ShowWarn(i_cKey,this.STUMPIAC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODSTU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READAZI
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCOACOG,AZFLGEAC";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_READAZI)
            select AZCODAZI,AZCOACOG,AZFLGEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZCOACOG = NVL(_Link_.AZCOACOG,space(1))
      this.w_AZFLGEAC = NVL(_Link_.AZFLGEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_AZCOACOG = space(1)
      this.w_AZFLGEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMCCODICE_1_3.value==this.w_MCCODICE)
      this.oPgFrm.Page1.oPag.oMCCODICE_1_3.value=this.w_MCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMCDESCRI_1_4.value==this.w_MCDESCRI)
      this.oPgFrm.Page1.oPag.oMCDESCRI_1_4.value=this.w_MCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMCNUMLIV_1_6.RadioValue()==this.w_MCNUMLIV)
      this.oPgFrm.Page1.oPag.oMCNUMLIV_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMCFLDETT_1_7.RadioValue()==this.w_MCFLDETT)
      this.oPgFrm.Page1.oPag.oMCFLDETT_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMCSEQSTA_1_8.value==this.w_MCSEQSTA)
      this.oPgFrm.Page1.oPag.oMCSEQSTA_1_8.value=this.w_MCSEQSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCONSUP_1_11.value==this.w_MCCONSUP)
      this.oPgFrm.Page1.oPag.oMCCONSUP_1_11.value=this.w_MCCONSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSUP_1_12.value==this.w_DESSUP)
      this.oPgFrm.Page1.oPag.oDESSUP_1_12.value=this.w_DESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oLIVSUP_1_13.value==this.w_LIVSUP)
      this.oPgFrm.Page1.oPag.oLIVSUP_1_13.value=this.w_LIVSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oMCSEZBIL_1_14.RadioValue()==this.w_MCSEZBIL)
      this.oPgFrm.Page1.oPag.oMCSEZBIL_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMCTIPMAS_1_21.RadioValue()==this.w_MCTIPMAS)
      this.oPgFrm.Page1.oPag.oMCTIPMAS_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCODSTU_1_25.value==this.w_MCCODSTU)
      this.oPgFrm.Page1.oPag.oMCCODSTU_1_25.value=this.w_MCCODSTU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_27.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_27.value=this.w_DESCON
    endif
    cp_SetControlsValueExtFlds(this,'MASTRI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MCCODICE))  and (.w_AUTO<>'S' AND .cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCCODICE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_MCCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MCDESCRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCDESCRI_1_4.SetFocus()
            i_bnoObbl = !empty(.w_MCDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MCNUMLIV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCNUMLIV_1_6.SetFocus()
            i_bnoObbl = !empty(.w_MCNUMLIV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MCSEQSTA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCSEQSTA_1_8.SetFocus()
            i_bnoObbl = !empty(.w_MCSEQSTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MCCONSUP)) or not(.w_LIVSUP>.w_MCNUMLIV AND .w_MCCONSUP<>.w_MCCODICE))  and (.w_MCNUMLIV<g_MAXLIV)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCCONSUP_1_11.SetFocus()
            i_bnoObbl = !empty(.w_MCCONSUP)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice mastro incongruente o di livello non superiore")
          case   (empty(.w_MCSEZBIL))  and (empty(.w_MCCONSUP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCSEZBIL_1_14.SetFocus()
            i_bnoObbl = !empty(.w_MCSEZBIL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AUTO = this.w_AUTO
    this.o_MCCODICE = this.w_MCCODICE
    this.o_MCNUMLIV = this.w_MCNUMLIV
    this.o_MCCONSUP = this.w_MCCONSUP
    this.o_MCCODSTU = this.w_MCCODSTU
    return

enddefine

* --- Define pages as container
define class tgsar_amcPag1 as StdContainer
  Width  = 562
  height = 221
  stdWidth  = 562
  stdheight = 221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMCCODICE_1_3 as StdField with uid="ALMRMBZWGU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MCCODICE", cQueryName = "MCCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice mastro",;
    HelpContextID = 17432843,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=80, Top=9, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15)

  func oMCCODICE_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AUTO<>'S' AND .cFunction='Load')
    endwith
   endif
  endfunc

  add object oMCDESCRI_1_4 as StdField with uid="ZIEKJHXQQP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MCDESCRI", cQueryName = "MCDESCRI",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 200282383,;
   bGlobalFont=.t.,;
    Height=21, Width=312, Left=221, Top=9, InputMask=replicate('X',40)


  add object oMCNUMLIV_1_6 as StdCombo with uid="YPZSZYLBGI",rtseq=6,rtrep=.f.,left=80,top=38,width=74,height=21;
    , ToolTipText = "Numero di livello del mastro";
    , HelpContextID = 190795492;
    , cFormVar="w_MCNUMLIV",RowSource=""+"Uno,"+"Due,"+"Tre,"+"Quattro", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oMCNUMLIV_1_6.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    0)))))
  endfunc
  func oMCNUMLIV_1_6.GetRadio()
    this.Parent.oContained.w_MCNUMLIV = this.RadioValue()
    return .t.
  endfunc

  func oMCNUMLIV_1_6.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_MCNUMLIV==1,1,;
      iif(this.Parent.oContained.w_MCNUMLIV==2,2,;
      iif(this.Parent.oContained.w_MCNUMLIV==3,3,;
      iif(this.Parent.oContained.w_MCNUMLIV==4,4,;
      0))))
  endfunc

  add object oMCFLDETT_1_7 as StdCheck with uid="RUADBEBJAX",rtseq=7,rtrep=.f.,left=165, top=36, caption="Totalizza conti associati",;
    ToolTipText = "Se attivo, riporta nel bilancio, i totali dei conti appartenenti al mastro",;
    HelpContextID = 218575130,;
    cFormVar="w_MCFLDETT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMCFLDETT_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMCFLDETT_1_7.GetRadio()
    this.Parent.oContained.w_MCFLDETT = this.RadioValue()
    return .t.
  endfunc

  func oMCFLDETT_1_7.SetRadio()
    this.Parent.oContained.w_MCFLDETT=trim(this.Parent.oContained.w_MCFLDETT)
    this.value = ;
      iif(this.Parent.oContained.w_MCFLDETT=='S',1,;
      0)
  endfunc

  func oMCFLDETT_1_7.mHide()
    with this.Parent.oContained
      return (.w_MCNUMLIV<>1)
    endwith
  endfunc

  add object oMCSEQSTA_1_8 as StdField with uid="NOVBIDAKLO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MCSEQSTA", cQueryName = "MCSEQSTA",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Priorit� nella stampa bilancio (maggiore il livello prima verr� stampata)",;
    HelpContextID = 198246663,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=522, Top=37, cSayPict='"999"', cGetPict='"999"'


  add object oBtn_1_9 as StdButton with uid="PLBRIOJAXM",left=404, top=65, width=20,height=21,;
    caption="...", nPag=1;
    , ToolTipText = "Selezione conto da piano dei conti studio";
    , HelpContextID = 210134570;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        do GSLM_BCS with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MCNUMLIV=1 AND g_LEMC='S')
      endwith
    endif
  endfunc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_MCNUMLIV<>1 OR g_LEMC<>'S')
     endwith
    endif
  endfunc

  add object oMCCONSUP_1_11 as StdField with uid="CRJRICSBOG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MCCONSUP", cQueryName = "MCCONSUP",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice mastro incongruente o di livello non superiore",;
    ToolTipText = "Mastro di raggruppamento di livello superiore al mastro impostato",;
    HelpContextID = 195690774,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=23, Top=144, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", oKey_1_1="MCCODICE", oKey_1_2="this.w_MCCONSUP"

  func oMCCONSUP_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MCNUMLIV<g_MAXLIV)
    endwith
   endif
  endfunc

  func oMCCONSUP_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCONSUP_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCCONSUP_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oMCCONSUP_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Mastri di raggruppamento",'GSAR_AMC.MASTRI_VZM',this.parent.oContained
  endproc

  add object oDESSUP_1_12 as StdField with uid="OXWPRLFIMR",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESSUP", cQueryName = "DESSUP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 115408330,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=166, Top=144, InputMask=replicate('X',40)

  add object oLIVSUP_1_13 as StdField with uid="KQICRTQFQR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_LIVSUP", cQueryName = "LIVSUP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 115394890,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=494, Top=144


  add object oMCSEZBIL_1_14 as StdCombo with uid="XIHWVNEHSH",rtseq=13,rtrep=.f.,left=13,top=197,width=136,height=21;
    , ToolTipText = "Indicare la sezione di bilancio";
    , HelpContextID = 190906642;
    , cFormVar="w_MCSEZBIL",RowSource=""+"Attivit�,"+"Passivit�,"+"Ordine,"+"Costi,"+"Ricavi,"+"Transitori", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oMCSEZBIL_1_14.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'P',;
    iif(this.value =3,'O',;
    iif(this.value =4,'C',;
    iif(this.value =5,'R',;
    iif(this.value =6,'T',;
    space(1))))))))
  endfunc
  func oMCSEZBIL_1_14.GetRadio()
    this.Parent.oContained.w_MCSEZBIL = this.RadioValue()
    return .t.
  endfunc

  func oMCSEZBIL_1_14.SetRadio()
    this.Parent.oContained.w_MCSEZBIL=trim(this.Parent.oContained.w_MCSEZBIL)
    this.value = ;
      iif(this.Parent.oContained.w_MCSEZBIL=='A',1,;
      iif(this.Parent.oContained.w_MCSEZBIL=='P',2,;
      iif(this.Parent.oContained.w_MCSEZBIL=='O',3,;
      iif(this.Parent.oContained.w_MCSEZBIL=='C',4,;
      iif(this.Parent.oContained.w_MCSEZBIL=='R',5,;
      iif(this.Parent.oContained.w_MCSEZBIL=='T',6,;
      0))))))
  endfunc

  func oMCSEZBIL_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_MCCONSUP))
    endwith
   endif
  endfunc


  add object oMCTIPMAS_1_21 as StdCombo with uid="OFQZMMXWCX",rtseq=14,rtrep=.f.,left=166,top=197,width=136,height=21;
    , ToolTipText = "Tipo mastro: (clienti, fornitori o generico)";
    , HelpContextID = 96801049;
    , cFormVar="w_MCTIPMAS",RowSource=""+"Clienti,"+"Fornitori,"+"Generico", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMCTIPMAS_1_21.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oMCTIPMAS_1_21.GetRadio()
    this.Parent.oContained.w_MCTIPMAS = this.RadioValue()
    return .t.
  endfunc

  func oMCTIPMAS_1_21.SetRadio()
    this.Parent.oContained.w_MCTIPMAS=trim(this.Parent.oContained.w_MCTIPMAS)
    this.value = ;
      iif(this.Parent.oContained.w_MCTIPMAS=='C',1,;
      iif(this.Parent.oContained.w_MCTIPMAS=='F',2,;
      iif(this.Parent.oContained.w_MCTIPMAS=='G',3,;
      0)))
  endfunc

  func oMCTIPMAS_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MCNUMLIV=1)
    endwith
   endif
  endfunc

  add object oMCCODSTU_1_25 as StdField with uid="TXYZTBPQHD",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MCCODSTU", cQueryName = "MCCODSTU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Associazione con il codice conto dello studio",;
    HelpContextID = 185205019,;
   bGlobalFont=.t.,;
    Height=21, Width=102, Left=80, Top=65, InputMask=replicate('X',10), cLinkFile="STUMPIAC", cZoomOnZoom="GSAR_AMC", oKey_1_1="LMCODCON", oKey_1_2="this.w_MCCODSTU"

  func oMCCODSTU_1_25.mHide()
    with this.Parent.oContained
      return (.w_MCNUMLIV<>1 OR g_LEMC<>'S')
    endwith
  endfunc

  func oMCCODSTU_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_MCPROSTU)
        bRes2=.link_1_10('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDESCON_1_27 as StdField with uid="HLKFYXIJYA",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Associazione con la descrizione conto dello studio",;
    HelpContextID = 156302794,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=186, Top=65, InputMask=replicate('X',30)

  func oDESCON_1_27.mHide()
    with this.Parent.oContained
      return (.w_MCNUMLIV<>1 OR g_LEMC<>'S')
    endwith
  endfunc


  add object oObj_1_35 as cp_runprogram with uid="OIKHFDWNLI",left=212, top=258, width=227,height=21,;
    caption='GSAR_BMC(C)',;
   bGlobalFont=.t.,;
    prg="GSAR_BMC('C')",;
    cEvent = "Delete start",;
    nPag=1;
    , ToolTipText = "Avvisa se cancello mastro senza padre";
    , HelpContextID = 71317975


  add object oObj_1_36 as cp_runprogram with uid="MJDZCTNNVA",left=212, top=235, width=227,height=21,;
    caption='GSAR_BMC(V)',;
   bGlobalFont=.t.,;
    prg="GSAR_BMC('V')",;
    cEvent = "Insert end,Update end",;
    nPag=1;
    , HelpContextID = 71313111


  add object oObj_1_37 as cp_runprogram with uid="MLANBFILZN",left=212, top=282, width=227,height=21,;
    caption='GSAR_BMC(D)',;
   bGlobalFont=.t.,;
    prg="GSAR_BMC('D')",;
    cEvent = "w_MCNUMLIV Changed",;
    nPag=1;
    , HelpContextID = 71317719

  add object oStr_1_15 as StdString with uid="TAYSNRRKIG",Visible=.t., Left=20, Top=9,;
    Alignment=1, Width=57, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="QKMYUNOHMM",Visible=.t., Left=13, Top=178,;
    Alignment=1, Width=136, Height=15,;
    Caption="Sez. bilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="LLJBLCZFDG",Visible=.t., Left=13, Top=112,;
    Alignment=0, Width=256, Height=15,;
    Caption="Mastro di raggruppamento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="QULFKXOHRX",Visible=.t., Left=20, Top=38,;
    Alignment=1, Width=57, Height=18,;
    Caption="Livello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="AHLPCCLLQI",Visible=.t., Left=458, Top=144,;
    Alignment=1, Width=33, Height=15,;
    Caption="Liv.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="HOAGGLOTIM",Visible=.t., Left=166, Top=178,;
    Alignment=1, Width=136, Height=15,;
    Caption="Tipo mastro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="VWFWQCRTPG",Visible=.t., Left=379, Top=38,;
    Alignment=1, Width=139, Height=18,;
    Caption="Priorit� di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="WYUKUWNNBR",Visible=.t., Left=2, Top=65,;
    Alignment=1, Width=75, Height=18,;
    Caption="Conto studio:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (.w_MCNUMLIV<>1 OR g_LEMC<>'S')
    endwith
  endfunc

  add object oBox_1_17 as StdBox with uid="JJIPRLQPEB",left=13, top=133, width=515,height=42
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_amc','MASTRI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MCCODICE=MASTRI.MCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
