* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bci                                                        *
*              Verifica inv precedente                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_29]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-08                                                      *
* Last revis.: 2015-09-10                                                      *
*                                                                              *
* Implementa il check su INNUMPRE in GSMA_AIN                                  *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bci",oParentObject)
return(i_retval)

define class tgsma_bci as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Implementa il check su INNUMPRE in GSMA_AIN
    * --- Risultato da restituire all'esterno
    * --- Incongruenze gestite:
    *     Categoria omogenea
    *     Gruppo merceologico
    *     Famiglia
    *     Magazzino
    *     inventario con chiave diversa da quella corrente
    *     Stato storicizzato o confermato
    *     Quantit� totale
    *     Flag LIFO\FIFO
    *     Data inventario
    this.oParentObject.w_CHECK = (NVL(this.oParentObject.w_INCATOMO,SPACE(5))=NVL(this.oParentObject.w_PRCATOMO,SPACE(5)))
    this.oParentObject.w_CHECK = this.oParentObject.w_CHECK and (NVL(this.oParentObject.w_INGRUMER,SPACE(5))=NVL(this.oParentObject.w_PRGRUMER,SPACE(5)))
    this.oParentObject.w_CHECK = this.oParentObject.w_CHECK and (NVL(this.oParentObject.w_INCODFAM,SPACE(5))=NVL(this.oParentObject.w_PRCODFAM,SPACE(5)))
    this.oParentObject.w_CHECK = this.oParentObject.w_CHECK and (NVL(this.oParentObject.w_INCODMAG,SPACE(5))=NVL(this.oParentObject.w_PRCODMAG,SPACE(5)))
    this.oParentObject.w_CHECK = this.oParentObject.w_CHECK and (NVL(this.oParentObject.w_INMAGCOL,SPACE(1))=NVL(this.oParentObject.w_PRMAGCOL,SPACE(1)))
    this.oParentObject.w_CHECK = this.oParentObject.w_CHECK and (NVL(this.oParentObject.w_INFLGFCO,SPACE(1))=NVL(this.oParentObject.w_PRFLGFCO,SPACE(1)))
    this.oParentObject.w_CHECK = this.oParentObject.w_CHECK and (NVL(this.oParentObject.w_INFLGLCO,SPACE(1))=NVL(this.oParentObject.w_PRFLGLCO,SPACE(1)))
    this.oParentObject.w_CHECK = this.oParentObject.w_CHECK and (NVL(this.oParentObject.w_INFLGLSC,SPACE(1))=NVL(this.oParentObject.w_PRFLGLSC,SPACE(1)))
    this.oParentObject.w_CHECK = this.oParentObject.w_CHECK and (this.oParentObject.w_INDATINV > this.oParentObject.w_PRDATINV)
    * --- Check gi� esistente prima dell'introduzione del batch
    this.oParentObject.w_CHECK = Empty(this.oParentObject.w_INNUMPRE) Or ((this.oParentObject.w_INNUMPRE<>this.oParentObject.w_INNUMINV Or this.oParentObject.w_INCODESE<>this.oParentObject.w_INESEPRE) And this.oParentObject.w_PRSTAINV $ "SD" and this.oParentObject.w_CHECK)
    if NOT this.oParentObject.w_CHECK
      this.oParentObject.w_PRDATINV = cp_CharToDate("  -  -  ")
      this.oParentObject.w_INNUMPRE = SPACE(6)
      ah_ErrorMsg("L'inventario preso come riferimento � incongruente con l'inventario che si vuole elaborare.%0Utilizzare lo zoom (F9) per una lista di inventari validi.","!")
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
