* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_bvm                                                        *
*              Visualizzazione ordini                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_244]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2014-07-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsor_bvm",oParentObject,m.pParam)
return(i_retval)

define class tgsor_bvm as StdBatch
  * --- Local variables
  pParam = space(4)
  w_ZOOM = space(10)
  w_LTOTSCA = 0
  w_LTOTCAR = 0
  * --- WorkFile variables
  TMPVISUOR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LANCIA AL NOTIFYEVENT (da GSOR_SZM)
    do case
      case this.pParam="Init"
        this.TMPVISUOR_idx=cp_gettabledefidx("TMPVISUOR")
        * --- Create temporary table TMPVISUOR
        i_nIdx=cp_AddTableDef('TMPVISUOR') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPVISUOR_proto';
              )
        this.TMPVISUOR_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      case this.pParam="O"
        this.oParentObject.w_VISIBLE = .T.
        * --- Create temporary table TMPVISUOR
        i_nIdx=cp_AddTableDef('TMPVISUOR') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSOR_SZM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVISUOR_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        This.oParentObject.NotifyEvent("Ricerca")
        this.w_ZOOM = this.oParentObject.w_ZoomDoc
        NC = this.w_ZOOM.cCursor
        this.oParentObject.w_TOTCAR = 0
        this.oParentObject.w_TOTSCA = 0
        * --- Legge Totali Carichi, Scarichi
        SELECT (NC)
        GO TOP
        SUM QTACAR, QTASCA TO this.w_LTOTCAR, this.w_LTOTSCA
        SELECT (NC)
        GO TOP
        this.oParentObject.w_TOTCAR = this.w_LTOTCAR
        this.oParentObject.w_TOTSCA = this.w_LTOTSCA
        if NOT EMPTY(this.oParentObject.w_CODART)
          * --- Eseguo questa operazione nel batch poich� ci sono problemi sul calcolo delle variabili
          *     TOTCAR e TOTSCA nel caso di zoom parziale
          this.oParentObject.w_VISIBLE = .F.
          this.oParentObject.mHideControls()
        endif
        * --- Refresh della griglia
        this.w_zoom.refresh()
      case this.pParam="Done"
        * --- Drop temporary table TMPVISUOR
        i_nIdx=cp_GetTableDefIdx('TMPVISUOR')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVISUOR')
        endif
      case this.pParam="Stampa"
        L_CATEGO= this.oParentObject.w_FLVEAC1
        L_CODART = this.oParentObject.w_CODART
        L_CODMAG = this.oParentObject.w_CODMAG
        L_TIPDOC = this.oParentObject.w_TIPDOC
        L_DOCINI = this.oParentObject.w_DOCINI
        L_DOCFIN = this.oParentObject.w_DOCFIN
        L_CLISEL = this.oParentObject.w_CLISEL
        L_CAUSEL = this.oParentObject.w_CAUSEL
        L_AGESEL = this.oParentObject.w_AGESEL
        L_MVRIFCLI = this.oParentObject.w_MVRIFCLI
        L_COMMESSA = this.oParentObject.w_COMMESSA
        L_CODATT = this.oParentObject.w_CODATT
        L_TIPEVA = this.oParentObject.w_TIPEVA
        vx_exec(alltrim(this.oParentObject.w_OQRY)+", "+alltrim(this.oParentObject.w_OREP),this)
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMPVISUOR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
