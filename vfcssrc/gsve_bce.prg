* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bce                                                        *
*              Certificazione ENASARCO                                         *
*                                                                              *
*      Author: Davide Bertoloni                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_28]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-01-21                                                      *
* Last revis.: 2012-04-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bce",oParentObject)
return(i_retval)

define class tgsve_bce as StdBatch
  * --- Local variables
  w_DECTOT = 0
  w_VTCODVAL = space(3)
  w_VASIMVAL = space(3)
  w_VFCONFIR = 0
  w_VTCONPRE = 0
  w_VTIMPENA = 0
  w_VTCONPR2 = 0
  w_VTCONASS = 0
  w_VTCONASG = 0
  w_PERAZI = 0
  w_PERAGE = 0
  w_TOTPER = 0
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Certificazioni Enasarco (da GSVE_SCE)
    * --- Batch reso necessario per il calcolo della somma dei versamenti
    * --- Seleziona i versamenti trimestrali + versamenti FIRR (gsve2sce in union)
    vq_exec("QUERY\GSVE1SCE.VQR",this,"TEMP1")
    * --- Verifico la valuta di stampa
    this.oParentObject.w_PCODVAL = IIF(this.oParentObject.w_PCODVAL=g_codeur,g_codeur,g_codlir)
    * --- Effettuo una Read sulla tabella Valute per avere i decimali.
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_PCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_PCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Scansione sul Cursore per calcolo dei Versamenti Enasarco di Competenza dell'Azienda/Agente
    SELECT TEMP1
    GO TOP
    SCAN FOR NOT EMPTY(NVL(AGCODAGE,"")) AND NOT EMPTY(NVL(VTCODVAL,""))
    this.w_PERAZI = NVL(VTPERPRE, 0)
    this.w_PERAGE = NVL(VTPERPR2, 0)
    this.w_VTCODVAL = VTCODVAL
    this.w_VFCONFIR = NVL(VFCONFIR, 0)
    this.w_VTCONPRE = NVL(VTCONPRE, 0)
    this.w_VTIMPENA = NVL(VTIMPENA, 0)
    this.w_VTCONASS = NVL(VTCONASS, 0)
    this.w_VTCONASG = NVL(VTCONASG, 0)
    this.w_VASIMVAL = VASIMVAL
    * --- Se Altra Valuta, converte
    if this.oParentObject.w_PCODVAL<>this.w_VTCODVAL
      this.w_VFCONFIR = cp_ROUND(VALCAM(this.w_VFCONFIR, this.w_VTCODVAL, this.oParentObject.w_PCODVAL, this.oParentObject.w_PDATINIZ), this.w_DECTOT)
      this.w_VTCONPRE = cp_ROUND(VALCAM(this.w_VTCONPRE, this.w_VTCODVAL, this.oParentObject.w_PCODVAL, this.oParentObject.w_PDATINIZ), this.w_DECTOT)
      this.w_VTCONASS = cp_ROUND(VALCAM(this.w_VTCONASS, this.w_VTCODVAL, this.oParentObject.w_PCODVAL, this.oParentObject.w_PDATINIZ), this.w_DECTOT)
      this.w_VTCONASG = cp_ROUND(VALCAM(this.w_VTCONASG, this.w_VTCODVAL, this.oParentObject.w_PCODVAL, this.oParentObject.w_PDATINIZ), this.w_DECTOT)
      this.w_VTIMPENA = cp_ROUND(VALCAM(this.w_VTIMPENA, this.w_VTCODVAL, this.oParentObject.w_PCODVAL, this.oParentObject.w_PDATINIZ), this.w_DECTOT)
    endif
    * --- Calcolo dei Versamenti Enasarco di Competenza dell'Azienda/Agente
    this.w_VTCONPR2 = 0
    if this.w_PERAGE<>0 AND this.w_VTCONPRE<>0
      this.w_VTCONPR2 = cp_ROUND((this.w_VTIMPENA * this.w_PERAGE) / 100, this.w_DECTOT)
      this.w_VTCONPRE = this.w_VTCONPRE - this.w_VTCONPR2
    endif
    Replace VFCONFIR with this.w_VFCONFIR
    Replace VTCONPRE with this.w_VTCONPRE
    Replace VTCONPR2 with this.w_VTCONPR2
    Replace VTCONASS with this.w_VTCONASS
    Replace VTCONASG with this.w_VTCONASG
    SELECT TEMP1
    ENDSCAN
    SELECT TEMP1
    GO TOP
    Select Agcodage,Sum(Vtconpre) as Vtconpre,Sum(Vtconpr2) as Vtconpr2,Sum(Vtconass) as Vtconass,Sum(Vtconasg) as Vtconasg, Sum(Vfconfir) as Vfconfir, ;
    Min(Vtcodval) as Vtcodval,Min(Agdesage) as Agdesage,Min(Agindage) as Agindage,Min(Agcapage) as Agcapage, ;
    Min(Agcitage) as Agcitage,Min(Agproage) as Agproage,Min(Agfisage) as Agfisage,Min(Agcodena) as Agcodena ;
    from TEMP1 group by Agcodage order by agcodage into cursor __TMP__
    * --- Variabili per il report
    l_PCODVAL = this.oParentObject.w_PCODVAL
    l_PSIMVAL = this.w_VASIMVAL
    l_PANNO = this.oParentObject.w_PANNO
    l_PDATINIZ = this.oParentObject.w_PDATINIZ
    l_dectot=this.w_dectot
    * --- Lancio il report
    CP_CHPRN("QUERY\GSVE_SCE.FRX", " ", this)
    * --- Chiudo i cursori
    if used("TEMP1")
      select TEMP1
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
