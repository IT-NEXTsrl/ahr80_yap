* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_agb                                                        *
*              Gestione bilancio IVA                                           *
*                                                                              *
*      Author: TAM SOFTWARE & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-22                                                      *
* Last revis.: 2010-05-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_agb"))

* --- Class definition
define class tgscg_agb as StdForm
  Top    = 50
  Left   = 21

  * --- Standard Properties
  Width  = 772
  Height = 316+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-05-28"
  HelpContextID=42607977
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=43

  * --- Constant Properties
  GESTBIVA_IDX = 0
  ESERCIZI_IDX = 0
  BUSIUNIT_IDX = 0
  AZIENDA_IDX = 0
  SB_MAST_IDX = 0
  VALUTE_IDX = 0
  STR_TOTA_IDX = 0
  ATTIMAST_IDX = 0
  cFile = "GESTBIVA"
  cKeySelect = "GBNUMBIL,GBBILESE"
  cKeyWhere  = "GBNUMBIL=this.w_GBNUMBIL and GBBILESE=this.w_GBBILESE"
  cKeyWhereODBC = '"GBNUMBIL="+cp_ToStrODBC(this.w_GBNUMBIL)';
      +'+" and GBBILESE="+cp_ToStrODBC(this.w_GBBILESE)';

  cKeyWhereODBCqualified = '"GESTBIVA.GBNUMBIL="+cp_ToStrODBC(this.w_GBNUMBIL)';
      +'+" and GESTBIVA.GBBILESE="+cp_ToStrODBC(this.w_GBBILESE)';

  cPrg = "gscg_agb"
  cComment = "Gestione bilancio IVA"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(10)
  w_UTENTE = 0
  w_GESTBU = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_GBNUMBIL = 0
  w_GBBILESE = space(4)
  o_GBBILESE = space(4)
  w_CGCODESE = space(10)
  w_GBDATBIL = ctod('  /  /  ')
  w_GBCONFER = space(1)
  w_GB__ANNO = 0
  w_GBDATINI = ctod('  /  /  ')
  w_GBDATFIN = ctod('  /  /  ')
  w_GBPERIOD = 0
  w_GBCODATT = space(5)
  w_GBSTRUCT = space(15)
  w_GBDESCRI = space(35)
  w_GBTIPSTA = space(1)
  w_GBTIPSTA = space(1)
  w_GBRIGHES = space(1)
  w_GBTIPSTA = space(1)
  w_GBTIPSTA = space(1)
  w_GBARROTO = space(1)
  w_GBCODBUN = space(3)
  w_GBGRUBUN = space(15)
  w_VALNAZ = space(3)
  o_VALNAZ = space(3)
  w_DECIMI = 0
  w_DESCRI = space(40)
  w_ABILITA = .F.
  w_SALVA = .F.
  w_CALCPICT = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_EXTRAEUR = 0
  w_SIMVAL = space(5)
  w_GBCOMMES = space(15)
  w_GBFLGCON = space(1)
  w_ARROTOND = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_DESATT = space(35)
  w_FLMAN = .F.
  w_OKCAR = .F.

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_GBBILESE = this.W_GBBILESE
  op_GBNUMBIL = this.W_GBNUMBIL

  * --- Children pointers
  gscg_mdm = .NULL.
  GSCG_MDB = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GESTBIVA','gscg_agb')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_agbPag1","gscg_agb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Bilancio")
      .Pages(1).HelpContextID = 138540923
      .Pages(2).addobject("oPag","tgscg_agbPag2","gscg_agb",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Manuali")
      .Pages(2).HelpContextID = 268295994
      .Pages(3).addobject("oPag","tgscg_agbPag3","gscg_agb",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dettaglio")
      .Pages(3).HelpContextID = 83784817
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGBNUMBIL_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='BUSIUNIT'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='SB_MAST'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='STR_TOTA'
    this.cWorkTables[7]='ATTIMAST'
    this.cWorkTables[8]='GESTBIVA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GESTBIVA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GESTBIVA_IDX,3]
  return

  function CreateChildren()
    this.gscg_mdm = CREATEOBJECT('stdDynamicChild',this,'gscg_mdm',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.GSCG_MDB = CREATEOBJECT('stdDynamicChild',this,'GSCG_MDB',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.gscg_mdm)
      this.gscg_mdm.DestroyChildrenChain()
      this.gscg_mdm=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    if !ISNULL(this.GSCG_MDB)
      this.GSCG_MDB.DestroyChildrenChain()
      this.GSCG_MDB=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gscg_mdm.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MDB.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gscg_mdm.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MDB.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gscg_mdm.NewDocument()
    this.GSCG_MDB.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.gscg_mdm.SetKey(;
            .w_GBNUMBIL,"MRNUMBIL";
            ,.w_GBBILESE,"MRCODESE";
            )
      this.GSCG_MDB.SetKey(;
            .w_GBNUMBIL,"MRNUMBIL";
            ,.w_GBBILESE,"MRCODESE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .gscg_mdm.ChangeRow(this.cRowID+'      1',1;
             ,.w_GBNUMBIL,"MRNUMBIL";
             ,.w_GBBILESE,"MRCODESE";
             )
      .GSCG_MDB.ChangeRow(this.cRowID+'      1',1;
             ,.w_GBNUMBIL,"MRNUMBIL";
             ,.w_GBBILESE,"MRCODESE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.gscg_mdm)
        i_f=.gscg_mdm.BuildFilter()
        if !(i_f==.gscg_mdm.cQueryFilter)
          i_fnidx=.gscg_mdm.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.gscg_mdm.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.gscg_mdm.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.gscg_mdm.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.gscg_mdm.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_MDB)
        i_f=.GSCG_MDB.BuildFilter()
        if !(i_f==.GSCG_MDB.cQueryFilter)
          i_fnidx=.GSCG_MDB.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MDB.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MDB.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MDB.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MDB.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_GBNUMBIL = NVL(GBNUMBIL,0)
      .w_GBBILESE = NVL(GBBILESE,space(4))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GESTBIVA where GBNUMBIL=KeySet.GBNUMBIL
    *                            and GBBILESE=KeySet.GBBILESE
    *
    i_nConn = i_TableProp[this.GESTBIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GESTBIVA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GESTBIVA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GESTBIVA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GESTBIVA '
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GBNUMBIL',this.w_GBNUMBIL  ,'GBBILESE',this.w_GBBILESE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_UTENTE = g_Codute
        .w_GESTBU = space(1)
        .w_DTOBSO = i_datsys
        .w_VALNAZ = space(3)
        .w_DECIMI = 0
        .w_DESCRI = space(40)
        .w_SALVA = .T.
        .w_EXTRAEUR = 0
        .w_SIMVAL = space(5)
        .w_ARROTOND = space(10)
        .w_OBTEST = i_datsys
        .w_DESATT = space(35)
        .w_FLMAN = .f.
        .w_OKCAR = .f.
          .link_1_1('Load')
        .w_GBNUMBIL = NVL(GBNUMBIL,0)
        .op_GBNUMBIL = .w_GBNUMBIL
        .w_GBBILESE = NVL(GBBILESE,space(4))
        .op_GBBILESE = .w_GBBILESE
          .link_1_6('Load')
        .w_CGCODESE = .w_GBBILESE
        .w_GBDATBIL = NVL(cp_ToDate(GBDATBIL),ctod("  /  /  "))
        .w_GBCONFER = NVL(GBCONFER,space(1))
        .w_GB__ANNO = NVL(GB__ANNO,0)
        .w_GBDATINI = NVL(cp_ToDate(GBDATINI),ctod("  /  /  "))
        .w_GBDATFIN = NVL(cp_ToDate(GBDATFIN),ctod("  /  /  "))
        .w_GBPERIOD = NVL(GBPERIOD,0)
        .w_GBCODATT = NVL(GBCODATT,space(5))
          if link_1_14_joined
            this.w_GBCODATT = NVL(ATCODATT114,NVL(this.w_GBCODATT,space(5)))
            this.w_DESATT = NVL(ATDESATT114,space(35))
          else
          .link_1_14('Load')
          endif
        .w_GBSTRUCT = NVL(GBSTRUCT,space(15))
          if link_1_15_joined
            this.w_GBSTRUCT = NVL(TRCODICE115,NVL(this.w_GBSTRUCT,space(15)))
            this.w_DESCRI = NVL(TRDESCRI115,space(40))
            this.w_DTOBSO = NVL(cp_ToDate(TRDTOBSO115),ctod("  /  /  "))
          else
          .link_1_15('Load')
          endif
        .w_GBDESCRI = NVL(GBDESCRI,space(35))
        .w_GBTIPSTA = NVL(GBTIPSTA,space(1))
        .w_GBTIPSTA = NVL(GBTIPSTA,space(1))
        .w_GBRIGHES = NVL(GBRIGHES,space(1))
        .w_GBTIPSTA = NVL(GBTIPSTA,space(1))
        .w_GBTIPSTA = NVL(GBTIPSTA,space(1))
        .w_GBARROTO = NVL(GBARROTO,space(1))
        .w_GBCODBUN = NVL(GBCODBUN,space(3))
          * evitabile
          *.link_1_27('Load')
        .w_GBGRUBUN = NVL(GBGRUBUN,space(15))
          * evitabile
          *.link_1_28('Load')
          .link_1_29('Load')
        .w_ABILITA = iif(.cFunction='Load',.T.,.F.)
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .w_CALCPICT = DEFPIC(.w_DECIMI)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_GBCOMMES = NVL(GBCOMMES,space(15))
        .w_GBFLGCON = NVL(GBFLGCON,space(1))
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'GESTBIVA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(10)
      .w_UTENTE = 0
      .w_GESTBU = space(1)
      .w_DTOBSO = ctod("  /  /  ")
      .w_GBNUMBIL = 0
      .w_GBBILESE = space(4)
      .w_CGCODESE = space(10)
      .w_GBDATBIL = ctod("  /  /  ")
      .w_GBCONFER = space(1)
      .w_GB__ANNO = 0
      .w_GBDATINI = ctod("  /  /  ")
      .w_GBDATFIN = ctod("  /  /  ")
      .w_GBPERIOD = 0
      .w_GBCODATT = space(5)
      .w_GBSTRUCT = space(15)
      .w_GBDESCRI = space(35)
      .w_GBTIPSTA = space(1)
      .w_GBTIPSTA = space(1)
      .w_GBRIGHES = space(1)
      .w_GBTIPSTA = space(1)
      .w_GBTIPSTA = space(1)
      .w_GBARROTO = space(1)
      .w_GBCODBUN = space(3)
      .w_GBGRUBUN = space(15)
      .w_VALNAZ = space(3)
      .w_DECIMI = 0
      .w_DESCRI = space(40)
      .w_ABILITA = .f.
      .w_SALVA = .f.
      .w_CALCPICT = 0
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_EXTRAEUR = 0
      .w_SIMVAL = space(5)
      .w_GBCOMMES = space(15)
      .w_GBFLGCON = space(1)
      .w_ARROTOND = space(10)
      .w_OBTEST = ctod("  /  /  ")
      .w_DESATT = space(35)
      .w_FLMAN = .f.
      .w_OKCAR = .f.
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
        .w_UTENTE = g_Codute
          .DoRTCalc(3,3,.f.)
        .w_DTOBSO = i_datsys
          .DoRTCalc(5,5,.f.)
        .w_GBBILESE = g_CODESE
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_GBBILESE))
          .link_1_6('Full')
          endif
        .w_CGCODESE = .w_GBBILESE
        .w_GBDATBIL = i_DATSYS
        .w_GBCONFER = 'N'
        .DoRTCalc(10,14,.f.)
          if not(empty(.w_GBCODATT))
          .link_1_14('Full')
          endif
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_GBSTRUCT))
          .link_1_15('Full')
          endif
          .DoRTCalc(16,16,.f.)
        .w_GBTIPSTA = 'S'
        .w_GBTIPSTA = 'S'
        .w_GBRIGHES = 'N'
        .w_GBTIPSTA = IIF(islron(),'D','S')
        .w_GBTIPSTA = IIF(islron(),'D','S')
        .w_GBARROTO = 'N'
        .DoRTCalc(23,23,.f.)
          if not(empty(.w_GBCODBUN))
          .link_1_27('Full')
          endif
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_GBGRUBUN))
          .link_1_28('Full')
          endif
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_VALNAZ))
          .link_1_29('Full')
          endif
          .DoRTCalc(26,27,.f.)
        .w_ABILITA = iif(.cFunction='Load',.T.,.F.)
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .w_SALVA = .T.
        .w_CALCPICT = DEFPIC(.w_DECIMI)
        .w_UTCC = iif(.cFunction='Load',i_codute,.w_UTCC)
        .w_UTCV = i_codute
        .w_UTDC = iif(.cFunction='Load',i_datsys,.w_UTDC)
        .w_UTDV = i_datsys
          .DoRTCalc(35,36,.f.)
        .w_GBCOMMES = space(15)
        .w_GBFLGCON = 'S'
          .DoRTCalc(39,39,.f.)
        .w_OBTEST = i_datsys
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'GESTBIVA')
    this.DoRTCalc(41,43,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GESTBIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GESTBIVA_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"BINUM","i_CODAZI,w_GBBILESE,w_GBNUMBIL")
      .op_CODAZI = .w_CODAZI
      .op_GBBILESE = .w_GBBILESE
      .op_GBNUMBIL = .w_GBNUMBIL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGBNUMBIL_1_5.enabled = !i_bVal
      .Page1.oPag.oGBBILESE_1_6.enabled = i_bVal
      .Page1.oPag.oGBDATBIL_1_8.enabled = i_bVal
      .Page1.oPag.oGB__ANNO_1_10.enabled = i_bVal
      .Page1.oPag.oGBDATINI_1_11.enabled = i_bVal
      .Page1.oPag.oGBDATFIN_1_12.enabled = i_bVal
      .Page1.oPag.oGBPERIOD_1_13.enabled = i_bVal
      .Page1.oPag.oGBCODATT_1_14.enabled = i_bVal
      .Page1.oPag.oGBSTRUCT_1_15.enabled = i_bVal
      .Page1.oPag.oGBDESCRI_1_16.enabled = i_bVal
      .Page1.oPag.oGBTIPSTA_1_17.enabled = i_bVal
      .Page1.oPag.oGBTIPSTA_1_18.enabled = i_bVal
      .Page1.oPag.oGBRIGHES_1_19.enabled = i_bVal
      .Page1.oPag.oGBTIPSTA_1_20.enabled = i_bVal
      .Page1.oPag.oGBTIPSTA_1_21.enabled = i_bVal
      .Page1.oPag.oGBARROTO_1_22.enabled = i_bVal
      .Page1.oPag.oBtn_1_23.enabled = .Page1.oPag.oBtn_1_23.mCond()
      .Page1.oPag.oBtn_1_24.enabled = i_bVal
      .Page2.oPag.oBtn_2_2.enabled = i_bVal
      .Page1.oPag.oObj_1_38.enabled = i_bVal
      .Page2.oPag.oObj_2_3.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oGBBILESE_1_6.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oGBBILESE_1_6.enabled = .t.
      endif
    endwith
    this.gscg_mdm.SetStatus(i_cOp)
    this.GSCG_MDB.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'GESTBIVA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gscg_mdm.SetChildrenStatus(i_cOp)
  *  this.GSCG_MDB.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GESTBIVA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBNUMBIL,"GBNUMBIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBBILESE,"GBBILESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBDATBIL,"GBDATBIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBCONFER,"GBCONFER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GB__ANNO,"GB__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBDATINI,"GBDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBDATFIN,"GBDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBPERIOD,"GBPERIOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBCODATT,"GBCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBSTRUCT,"GBSTRUCT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBDESCRI,"GBDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBTIPSTA,"GBTIPSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBTIPSTA,"GBTIPSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBRIGHES,"GBRIGHES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBTIPSTA,"GBTIPSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBTIPSTA,"GBTIPSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBARROTO,"GBARROTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBCODBUN,"GBCODBUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBGRUBUN,"GBGRUBUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBCOMMES,"GBCOMMES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GBFLGCON,"GBFLGCON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GESTBIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GESTBIVA_IDX,2])
    i_lTable = "GESTBIVA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GESTBIVA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        GSCG_BRB(this,2)
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GESTBIVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GESTBIVA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GESTBIVA_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"BINUM","i_CODAZI,w_GBBILESE,w_GBNUMBIL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GESTBIVA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GESTBIVA')
        i_extval=cp_InsertValODBCExtFlds(this,'GESTBIVA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(GBNUMBIL,GBBILESE,GBDATBIL,GBCONFER,GB__ANNO"+;
                  ",GBDATINI,GBDATFIN,GBPERIOD,GBCODATT,GBSTRUCT"+;
                  ",GBDESCRI,GBTIPSTA,GBRIGHES,GBARROTO,GBCODBUN"+;
                  ",GBGRUBUN,UTCC,UTCV,UTDC,UTDV"+;
                  ",GBCOMMES,GBFLGCON "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_GBNUMBIL)+;
                  ","+cp_ToStrODBCNull(this.w_GBBILESE)+;
                  ","+cp_ToStrODBC(this.w_GBDATBIL)+;
                  ","+cp_ToStrODBC(this.w_GBCONFER)+;
                  ","+cp_ToStrODBC(this.w_GB__ANNO)+;
                  ","+cp_ToStrODBC(this.w_GBDATINI)+;
                  ","+cp_ToStrODBC(this.w_GBDATFIN)+;
                  ","+cp_ToStrODBC(this.w_GBPERIOD)+;
                  ","+cp_ToStrODBCNull(this.w_GBCODATT)+;
                  ","+cp_ToStrODBCNull(this.w_GBSTRUCT)+;
                  ","+cp_ToStrODBC(this.w_GBDESCRI)+;
                  ","+cp_ToStrODBC(this.w_GBTIPSTA)+;
                  ","+cp_ToStrODBC(this.w_GBRIGHES)+;
                  ","+cp_ToStrODBC(this.w_GBARROTO)+;
                  ","+cp_ToStrODBCNull(this.w_GBCODBUN)+;
                  ","+cp_ToStrODBCNull(this.w_GBGRUBUN)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_GBCOMMES)+;
                  ","+cp_ToStrODBC(this.w_GBFLGCON)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GESTBIVA')
        i_extval=cp_InsertValVFPExtFlds(this,'GESTBIVA')
        cp_CheckDeletedKey(i_cTable,0,'GBNUMBIL',this.w_GBNUMBIL,'GBBILESE',this.w_GBBILESE)
        INSERT INTO (i_cTable);
              (GBNUMBIL,GBBILESE,GBDATBIL,GBCONFER,GB__ANNO,GBDATINI,GBDATFIN,GBPERIOD,GBCODATT,GBSTRUCT,GBDESCRI,GBTIPSTA,GBRIGHES,GBARROTO,GBCODBUN,GBGRUBUN,UTCC,UTCV,UTDC,UTDV,GBCOMMES,GBFLGCON  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_GBNUMBIL;
                  ,this.w_GBBILESE;
                  ,this.w_GBDATBIL;
                  ,this.w_GBCONFER;
                  ,this.w_GB__ANNO;
                  ,this.w_GBDATINI;
                  ,this.w_GBDATFIN;
                  ,this.w_GBPERIOD;
                  ,this.w_GBCODATT;
                  ,this.w_GBSTRUCT;
                  ,this.w_GBDESCRI;
                  ,this.w_GBTIPSTA;
                  ,this.w_GBRIGHES;
                  ,this.w_GBARROTO;
                  ,this.w_GBCODBUN;
                  ,this.w_GBGRUBUN;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_GBCOMMES;
                  ,this.w_GBFLGCON;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GESTBIVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GESTBIVA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GESTBIVA_IDX,i_nConn)
      *
      * update GESTBIVA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GESTBIVA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GBDATBIL="+cp_ToStrODBC(this.w_GBDATBIL)+;
             ",GBCONFER="+cp_ToStrODBC(this.w_GBCONFER)+;
             ",GB__ANNO="+cp_ToStrODBC(this.w_GB__ANNO)+;
             ",GBDATINI="+cp_ToStrODBC(this.w_GBDATINI)+;
             ",GBDATFIN="+cp_ToStrODBC(this.w_GBDATFIN)+;
             ",GBPERIOD="+cp_ToStrODBC(this.w_GBPERIOD)+;
             ",GBCODATT="+cp_ToStrODBCNull(this.w_GBCODATT)+;
             ",GBSTRUCT="+cp_ToStrODBCNull(this.w_GBSTRUCT)+;
             ",GBDESCRI="+cp_ToStrODBC(this.w_GBDESCRI)+;
             ",GBTIPSTA="+cp_ToStrODBC(this.w_GBTIPSTA)+;
             ",GBRIGHES="+cp_ToStrODBC(this.w_GBRIGHES)+;
             ",GBARROTO="+cp_ToStrODBC(this.w_GBARROTO)+;
             ",GBCODBUN="+cp_ToStrODBCNull(this.w_GBCODBUN)+;
             ",GBGRUBUN="+cp_ToStrODBCNull(this.w_GBGRUBUN)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",GBCOMMES="+cp_ToStrODBC(this.w_GBCOMMES)+;
             ",GBFLGCON="+cp_ToStrODBC(this.w_GBFLGCON)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GESTBIVA')
        i_cWhere = cp_PKFox(i_cTable  ,'GBNUMBIL',this.w_GBNUMBIL  ,'GBBILESE',this.w_GBBILESE  )
        UPDATE (i_cTable) SET;
              GBDATBIL=this.w_GBDATBIL;
             ,GBCONFER=this.w_GBCONFER;
             ,GB__ANNO=this.w_GB__ANNO;
             ,GBDATINI=this.w_GBDATINI;
             ,GBDATFIN=this.w_GBDATFIN;
             ,GBPERIOD=this.w_GBPERIOD;
             ,GBCODATT=this.w_GBCODATT;
             ,GBSTRUCT=this.w_GBSTRUCT;
             ,GBDESCRI=this.w_GBDESCRI;
             ,GBTIPSTA=this.w_GBTIPSTA;
             ,GBRIGHES=this.w_GBRIGHES;
             ,GBARROTO=this.w_GBARROTO;
             ,GBCODBUN=this.w_GBCODBUN;
             ,GBGRUBUN=this.w_GBGRUBUN;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,GBCOMMES=this.w_GBCOMMES;
             ,GBFLGCON=this.w_GBFLGCON;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- gscg_mdm : Saving
      this.gscg_mdm.ChangeRow(this.cRowID+'      1',0;
             ,this.w_GBNUMBIL,"MRNUMBIL";
             ,this.w_GBBILESE,"MRCODESE";
             )
      this.gscg_mdm.mReplace()
      * --- GSCG_MDB : Saving
      this.GSCG_MDB.ChangeRow(this.cRowID+'      1',0;
             ,this.w_GBNUMBIL,"MRNUMBIL";
             ,this.w_GBBILESE,"MRCODESE";
             )
      this.GSCG_MDB.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- gscg_mdm : Deleting
    this.gscg_mdm.ChangeRow(this.cRowID+'      1',0;
           ,this.w_GBNUMBIL,"MRNUMBIL";
           ,this.w_GBBILESE,"MRCODESE";
           )
    this.gscg_mdm.mDelete()
    * --- GSCG_MDB : Deleting
    this.GSCG_MDB.ChangeRow(this.cRowID+'      1',0;
           ,this.w_GBNUMBIL,"MRNUMBIL";
           ,this.w_GBBILESE,"MRCODESE";
           )
    this.GSCG_MDB.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GESTBIVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GESTBIVA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GESTBIVA_IDX,i_nConn)
      *
      * delete GESTBIVA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'GBNUMBIL',this.w_GBNUMBIL  ,'GBBILESE',this.w_GBBILESE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GESTBIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GESTBIVA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,6,.t.)
            .w_CGCODESE = .w_GBBILESE
        .DoRTCalc(8,22,.t.)
          .link_1_27('Full')
          .link_1_28('Full')
        if .o_GBBILESE<>.w_GBBILESE
          .link_1_29('Full')
        endif
        .DoRTCalc(26,27,.t.)
            .w_ABILITA = iif(.cFunction='Load',.T.,.F.)
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .DoRTCalc(29,29,.t.)
            .w_CALCPICT = DEFPIC(.w_DECIMI)
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI .or. .op_GBBILESE<>.w_GBBILESE
           cp_AskTableProg(this,i_nConn,"BINUM","i_CODAZI,w_GBBILESE,w_GBNUMBIL")
          .op_GBNUMBIL = .w_GBNUMBIL
        endif
        .op_CODAZI = .w_CODAZI
        .op_GBBILESE = .w_GBBILESE
      endwith
      this.DoRTCalc(31,43,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oGBBILESE_1_6.enabled = this.oPgFrm.Page1.oPag.oGBBILESE_1_6.mCond()
    this.oPgFrm.Page1.oPag.oGBSTRUCT_1_15.enabled = this.oPgFrm.Page1.oPag.oGBSTRUCT_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oGBCODATT_1_14.visible=!this.oPgFrm.Page1.oPag.oGBCODATT_1_14.mHide()
    this.oPgFrm.Page1.oPag.oGBTIPSTA_1_17.visible=!this.oPgFrm.Page1.oPag.oGBTIPSTA_1_17.mHide()
    this.oPgFrm.Page1.oPag.oGBTIPSTA_1_18.visible=!this.oPgFrm.Page1.oPag.oGBTIPSTA_1_18.mHide()
    this.oPgFrm.Page1.oPag.oGBTIPSTA_1_20.visible=!this.oPgFrm.Page1.oPag.oGBTIPSTA_1_20.mHide()
    this.oPgFrm.Page1.oPag.oGBTIPSTA_1_21.visible=!this.oPgFrm.Page1.oPag.oGBTIPSTA_1_21.mHide()
    this.oPgFrm.Page1.oPag.oDESATT_1_53.visible=!this.oPgFrm.Page1.oPag.oDESATT_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gscg_agb
            * instanzio il figlio della seconda pagina immediatamente
            * viene richiamato per i calcoli
            IF Upper(CEVENT)='INIT'
             if Upper(this.GSCG_MDM.class)='STDDYNAMICCHILD'
               This.oPgFrm.Pages[2].opag.uienable(.T.)
               This.oPgFrm.ActivePage=1
             Endif
            Endif
            IF Upper(CEVENT)='INIT'
             if Upper(this.GSCG_MDB.class)='STDDYNAMICCHILD'
               This.oPgFrm.Pages[3].opag.uienable(.T.)
               This.oPgFrm.ActivePage=1
             Endif
            Endif
    
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_3.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(10))
      this.w_GESTBU = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(10)
      endif
      this.w_GESTBU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GBBILESE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GBBILESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_GBBILESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_GBBILESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GBBILESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GBBILESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oGBBILESE_1_6'),i_cWhere,'GSAR_KES',"ESERCIZI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GBBILESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_GBBILESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_GBBILESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GBBILESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_GBBILESE = space(4)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GBBILESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GBCODATT
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GBCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MAT',True,'ATTIMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_GBCODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_GBCODATT))
          select ATCODATT,ATDESATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GBCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GBCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIMAST','*','ATCODATT',cp_AbsName(oSource.parent,'oGBCODATT_1_14'),i_cWhere,'GSAR_MAT',"ATTIVITA'",'attnoriep.ATTIMAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GBCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_GBCODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_GBCODATT)
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GBCODATT = NVL(_Link_.ATCODATT,space(5))
      this.w_DESATT = NVL(_Link_.ATDESATT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GBCODATT = space(5)
      endif
      this.w_DESATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GBCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ATTIMAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.ATCODATT as ATCODATT114"+ ",link_1_14.ATDESATT as ATDESATT114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on GESTBIVA.GBCODATT=link_1_14.ATCODATT"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and GESTBIVA.GBCODATT=link_1_14.ATCODATT(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GBSTRUCT
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_TOTA_IDX,3]
    i_lTable = "STR_TOTA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_TOTA_IDX,2], .t., this.STR_TOTA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_TOTA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GBSTRUCT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MSF',True,'STR_TOTA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_GBSTRUCT)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI,TRDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_GBSTRUCT))
          select TRCODICE,TRDESCRI,TRDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GBSTRUCT)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GBSTRUCT) and !this.bDontReportError
            deferred_cp_zoom('STR_TOTA','*','TRCODICE',cp_AbsName(oSource.parent,'oGBSTRUCT_1_15'),i_cWhere,'GSCG_MSF',"STRUTTURE DI BILANCIO",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI,TRDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRDESCRI,TRDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GBSTRUCT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI,TRDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_GBSTRUCT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_GBSTRUCT)
            select TRCODICE,TRDESCRI,TRDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GBSTRUCT = NVL(_Link_.TRCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.TRDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.TRDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GBSTRUCT = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSO>.w_GBDATBIL OR EMPTY(.w_DTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice struttura inesistente o obsoleto")
        endif
        this.w_GBSTRUCT = space(15)
        this.w_DESCRI = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_TOTA_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_TOTA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GBSTRUCT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.STR_TOTA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.STR_TOTA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.TRCODICE as TRCODICE115"+ ",link_1_15.TRDESCRI as TRDESCRI115"+ ",link_1_15.TRDTOBSO as TRDTOBSO115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on GESTBIVA.GBSTRUCT=link_1_15.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and GESTBIVA.GBSTRUCT=link_1_15.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GBCODBUN
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GBCODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GBCODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_GBCODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_CODAZI;
                       ,'BUCODICE',this.w_GBCODBUN)
            select BUCODAZI,BUCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GBCODBUN = NVL(_Link_.BUCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_GBCODBUN = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GBCODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GBGRUBUN
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SB_MAST_IDX,3]
    i_lTable = "SB_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2], .t., this.SB_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GBGRUBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GBGRUBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SBCODICE="+cp_ToStrODBC(this.w_GBGRUBUN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SBCODICE',this.w_GBGRUBUN)
            select SBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GBGRUBUN = NVL(_Link_.SBCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_GBGRUBUN = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])+'\'+cp_ToStr(_Link_.SBCODICE,1)
      cp_ShowWarn(i_cKey,this.SB_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GBGRUBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_DECIMI = NVL(_Link_.VADECTOT,0)
      this.w_EXTRAEUR = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
      this.w_DECIMI = 0
      this.w_EXTRAEUR = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGBNUMBIL_1_5.value==this.w_GBNUMBIL)
      this.oPgFrm.Page1.oPag.oGBNUMBIL_1_5.value=this.w_GBNUMBIL
    endif
    if not(this.oPgFrm.Page1.oPag.oGBBILESE_1_6.value==this.w_GBBILESE)
      this.oPgFrm.Page1.oPag.oGBBILESE_1_6.value=this.w_GBBILESE
    endif
    if not(this.oPgFrm.Page1.oPag.oGBDATBIL_1_8.value==this.w_GBDATBIL)
      this.oPgFrm.Page1.oPag.oGBDATBIL_1_8.value=this.w_GBDATBIL
    endif
    if not(this.oPgFrm.Page1.oPag.oGB__ANNO_1_10.value==this.w_GB__ANNO)
      this.oPgFrm.Page1.oPag.oGB__ANNO_1_10.value=this.w_GB__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oGBDATINI_1_11.value==this.w_GBDATINI)
      this.oPgFrm.Page1.oPag.oGBDATINI_1_11.value=this.w_GBDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGBDATFIN_1_12.value==this.w_GBDATFIN)
      this.oPgFrm.Page1.oPag.oGBDATFIN_1_12.value=this.w_GBDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGBPERIOD_1_13.value==this.w_GBPERIOD)
      this.oPgFrm.Page1.oPag.oGBPERIOD_1_13.value=this.w_GBPERIOD
    endif
    if not(this.oPgFrm.Page1.oPag.oGBCODATT_1_14.value==this.w_GBCODATT)
      this.oPgFrm.Page1.oPag.oGBCODATT_1_14.value=this.w_GBCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oGBSTRUCT_1_15.value==this.w_GBSTRUCT)
      this.oPgFrm.Page1.oPag.oGBSTRUCT_1_15.value=this.w_GBSTRUCT
    endif
    if not(this.oPgFrm.Page1.oPag.oGBDESCRI_1_16.value==this.w_GBDESCRI)
      this.oPgFrm.Page1.oPag.oGBDESCRI_1_16.value=this.w_GBDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oGBTIPSTA_1_17.RadioValue()==this.w_GBTIPSTA)
      this.oPgFrm.Page1.oPag.oGBTIPSTA_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGBTIPSTA_1_18.RadioValue()==this.w_GBTIPSTA)
      this.oPgFrm.Page1.oPag.oGBTIPSTA_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGBRIGHES_1_19.RadioValue()==this.w_GBRIGHES)
      this.oPgFrm.Page1.oPag.oGBRIGHES_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGBTIPSTA_1_20.RadioValue()==this.w_GBTIPSTA)
      this.oPgFrm.Page1.oPag.oGBTIPSTA_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGBTIPSTA_1_21.RadioValue()==this.w_GBTIPSTA)
      this.oPgFrm.Page1.oPag.oGBTIPSTA_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGBARROTO_1_22.RadioValue()==this.w_GBARROTO)
      this.oPgFrm.Page1.oPag.oGBARROTO_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_36.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_36.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_53.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_53.value=this.w_DESATT
    endif
    cp_SetControlsValueExtFlds(this,'GESTBIVA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_GBBILESE))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGBBILESE_1_6.SetFocus()
            i_bnoObbl = !empty(.w_GBBILESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GB__ANNO>1900 or .w_GB__ANNO=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGB__ANNO_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GBDATINI<=.w_GBDATFIN or empty(.w_GBDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGBDATINI_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore di quella finale")
          case   not(.w_GBDATINI<=.w_GBDATFIN or empty(.w_GBDATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGBDATFIN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore di quella finale")
          case   ((empty(.w_GBSTRUCT)) or not(.w_DTOBSO>.w_GBDATBIL OR EMPTY(.w_DTOBSO)))  and (.cFunction = 'Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGBSTRUCT_1_15.SetFocus()
            i_bnoObbl = !empty(.w_GBSTRUCT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice struttura inesistente o obsoleto")
        endcase
      endif
      *i_bRes = i_bRes .and. .gscg_mdm.CheckForm()
      if i_bres
        i_bres=  .gscg_mdm.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_MDB.CheckForm()
      if i_bres
        i_bres=  .GSCG_MDB.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GBBILESE = this.w_GBBILESE
    this.o_VALNAZ = this.w_VALNAZ
    * --- gscg_mdm : Depends On
    this.gscg_mdm.SaveDependsOn()
    * --- GSCG_MDB : Depends On
    this.GSCG_MDB.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_agbPag1 as StdContainer
  Width  = 768
  height = 320
  stdWidth  = 768
  stdheight = 320
  resizeYpos=262
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGBNUMBIL_1_5 as StdField with uid="CDVLOGTDYB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_GBNUMBIL", cQueryName = "GBNUMBIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero bilancio",;
    HelpContextID = 77595058,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=149, Top=15, cSayPict='"999999"', cGetPict='"999999"'

  proc oGBNUMBIL_1_5.mBefore
    with this.Parent.oContained
      this.cQueryName = "GBNUMBIL,GBBILESE"
    endwith
  endproc

  add object oGBBILESE_1_6 as StdField with uid="ODGBSNGEVY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_GBBILESE", cQueryName = "GBNUMBIL,GBBILESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza del bilancio",;
    HelpContextID = 126042539,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=61, Left=285, Top=15, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_GBBILESE"

  func oGBBILESE_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oGBBILESE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oGBBILESE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGBBILESE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oGBBILESE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"ESERCIZI",'',this.parent.oContained
  endproc
  proc oGBBILESE_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_CODAZI
     i_obj.w_ESCODESE=this.parent.oContained.w_GBBILESE
     i_obj.ecpSave()
  endproc

  add object oGBDATBIL_1_8 as StdField with uid="UFLTKDGEVW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_GBDATBIL", cQueryName = "GBDATBIL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di elaborazione bilancio",;
    HelpContextID = 83583410,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=434, Top=15

  add object oGB__ANNO_1_10 as StdField with uid="QACAAVYLZB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_GB__ANNO", cQueryName = "GB__ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Filtro su anno solare",;
    HelpContextID = 1371723,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=149, Top=53, cSayPict="'9999'", cGetPict="'9999'"

  func oGB__ANNO_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GB__ANNO>1900 or .w_GB__ANNO=0)
    endwith
    return bRes
  endfunc

  add object oGBDATINI_1_11 as StdField with uid="FQUELCWYRS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_GBDATINI", cQueryName = "GBDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore di quella finale",;
    ToolTipText = "Filtro su data competenza IVA iniziale",;
    HelpContextID = 67411537,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=285, Top=53

  func oGBDATINI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GBDATINI<=.w_GBDATFIN or empty(.w_GBDATFIN))
    endwith
    return bRes
  endfunc

  add object oGBDATFIN_1_12 as StdField with uid="TUXCIDXXHU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_GBDATFIN", cQueryName = "GBDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore di quella finale",;
    ToolTipText = "Filtro su data competenza IVA finale",;
    HelpContextID = 150692276,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=434, Top=53

  func oGBDATFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GBDATINI<=.w_GBDATFIN or empty(.w_GBDATINI))
    endwith
    return bRes
  endfunc

  add object oGBPERIOD_1_13 as StdField with uid="AOKSODPAAS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_GBPERIOD", cQueryName = "GBPERIOD",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Periodo di selezione",;
    HelpContextID = 69197398,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=642, Top=53, cSayPict="'99'", cGetPict="'99'"

  add object oGBCODATT_1_14 as StdField with uid="UXMOOFVFUV",rtseq=14,rtrep=.f.,;
    cFormVar = "w_GBCODATT", cQueryName = "GBCODATT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� IVA di selezione",;
    HelpContextID = 50942394,;
   bGlobalFont=.t.,;
    Height=21, Width=87, Left=149, Top=89, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ATTIMAST", cZoomOnZoom="GSAR_MAT", oKey_1_1="ATCODATT", oKey_1_2="this.w_GBCODATT"

  func oGBCODATT_1_14.mHide()
    with this.Parent.oContained
      return (g_ATTIVI <>'S')
    endwith
  endfunc

  func oGBCODATT_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oGBCODATT_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGBCODATT_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ATTIMAST','*','ATCODATT',cp_AbsName(this.parent,'oGBCODATT_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MAT',"ATTIVITA'",'attnoriep.ATTIMAST_VZM',this.parent.oContained
  endproc
  proc oGBCODATT_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MAT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ATCODATT=this.parent.oContained.w_GBCODATT
     i_obj.ecpSave()
  endproc

  add object oGBSTRUCT_1_15 as StdField with uid="OMNDVHPVYB",rtseq=15,rtrep=.f.,;
    cFormVar = "w_GBSTRUCT", cQueryName = "GBSTRUCT",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice struttura inesistente o obsoleto",;
    ToolTipText = "Struttura di bilancio selezionata",;
    HelpContextID = 133124538,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=149, Top=126, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_TOTA", cZoomOnZoom="GSCG_MSF", oKey_1_1="TRCODICE", oKey_1_2="this.w_GBSTRUCT"

  func oGBSTRUCT_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction = 'Load')
    endwith
   endif
  endfunc

  func oGBSTRUCT_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oGBSTRUCT_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGBSTRUCT_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_TOTA','*','TRCODICE',cp_AbsName(this.parent,'oGBSTRUCT_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MSF',"STRUTTURE DI BILANCIO",'',this.parent.oContained
  endproc
  proc oGBSTRUCT_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MSF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_GBSTRUCT
     i_obj.ecpSave()
  endproc

  add object oGBDESCRI_1_16 as StdField with uid="OWCJJNPVXS",rtseq=16,rtrep=.f.,;
    cFormVar = "w_GBDESCRI", cQueryName = "GBDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 168861265,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=149, Top=162, InputMask=replicate('X',35)


  add object oGBTIPSTA_1_17 as StdCombo with uid="TSMEYHZZOA",rtseq=17,rtrep=.f.,left=629,top=166,width=129,height=21;
    , ToolTipText = "Tipo stampa";
    , HelpContextID = 96756135;
    , cFormVar="w_GBTIPSTA",RowSource=""+"Standard,"+"Excel", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGBTIPSTA_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oGBTIPSTA_1_17.GetRadio()
    this.Parent.oContained.w_GBTIPSTA = this.RadioValue()
    return .t.
  endfunc

  func oGBTIPSTA_1_17.SetRadio()
    this.Parent.oContained.w_GBTIPSTA=trim(this.Parent.oContained.w_GBTIPSTA)
    this.value = ;
      iif(this.Parent.oContained.w_GBTIPSTA=='S',1,;
      iif(this.Parent.oContained.w_GBTIPSTA=='E',2,;
      0))
  endfunc

  func oGBTIPSTA_1_17.mHide()
    with this.Parent.oContained
      return (g_office<>'M' or Islron())
    endwith
  endfunc


  add object oGBTIPSTA_1_18 as StdCombo with uid="TMSCGIMGGI",rtseq=18,rtrep=.f.,left=629,top=166,width=129,height=21;
    , ToolTipText = "Tipo stampa";
    , HelpContextID = 96756135;
    , cFormVar="w_GBTIPSTA",RowSource=""+"Standard,"+"Calc", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGBTIPSTA_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oGBTIPSTA_1_18.GetRadio()
    this.Parent.oContained.w_GBTIPSTA = this.RadioValue()
    return .t.
  endfunc

  func oGBTIPSTA_1_18.SetRadio()
    this.Parent.oContained.w_GBTIPSTA=trim(this.Parent.oContained.w_GBTIPSTA)
    this.value = ;
      iif(this.Parent.oContained.w_GBTIPSTA=='S',1,;
      iif(this.Parent.oContained.w_GBTIPSTA=='E',2,;
      0))
  endfunc

  func oGBTIPSTA_1_18.mHide()
    with this.Parent.oContained
      return (g_office='M' or Islron())
    endwith
  endfunc

  add object oGBRIGHES_1_19 as StdCheck with uid="LEJUOMXWEM",rtseq=19,rtrep=.f.,left=149, top=198, caption="Righe significative",;
    ToolTipText = "Stampa solo righe valorizzate",;
    HelpContextID = 171196857,;
    cFormVar="w_GBRIGHES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGBRIGHES_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGBRIGHES_1_19.GetRadio()
    this.Parent.oContained.w_GBRIGHES = this.RadioValue()
    return .t.
  endfunc

  func oGBRIGHES_1_19.SetRadio()
    this.Parent.oContained.w_GBRIGHES=trim(this.Parent.oContained.w_GBRIGHES)
    this.value = ;
      iif(this.Parent.oContained.w_GBRIGHES=='S',1,;
      0)
  endfunc


  add object oGBTIPSTA_1_20 as StdCombo with uid="HBYNFAYXJB",rtseq=20,rtrep=.f.,left=629,top=166,width=129,height=21;
    , ToolTipText = "Tipo stampa";
    , HelpContextID = 96756135;
    , cFormVar="w_GBTIPSTA",RowSource=""+"Dettagliata,"+"Calc", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGBTIPSTA_1_20.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oGBTIPSTA_1_20.GetRadio()
    this.Parent.oContained.w_GBTIPSTA = this.RadioValue()
    return .t.
  endfunc

  func oGBTIPSTA_1_20.SetRadio()
    this.Parent.oContained.w_GBTIPSTA=trim(this.Parent.oContained.w_GBTIPSTA)
    this.value = ;
      iif(this.Parent.oContained.w_GBTIPSTA=='D',1,;
      iif(this.Parent.oContained.w_GBTIPSTA=='E',2,;
      0))
  endfunc

  func oGBTIPSTA_1_20.mHide()
    with this.Parent.oContained
      return (g_office='M' or !Islron())
    endwith
  endfunc


  add object oGBTIPSTA_1_21 as StdCombo with uid="BVUVOYTKKP",rtseq=21,rtrep=.f.,left=629,top=166,width=129,height=21;
    , ToolTipText = "Tipo stampa";
    , HelpContextID = 96756135;
    , cFormVar="w_GBTIPSTA",RowSource=""+"Dettagliata,"+"Excel", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGBTIPSTA_1_21.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oGBTIPSTA_1_21.GetRadio()
    this.Parent.oContained.w_GBTIPSTA = this.RadioValue()
    return .t.
  endfunc

  func oGBTIPSTA_1_21.SetRadio()
    this.Parent.oContained.w_GBTIPSTA=trim(this.Parent.oContained.w_GBTIPSTA)
    this.value = ;
      iif(this.Parent.oContained.w_GBTIPSTA=='D',1,;
      iif(this.Parent.oContained.w_GBTIPSTA=='E',2,;
      0))
  endfunc

  func oGBTIPSTA_1_21.mHide()
    with this.Parent.oContained
      return (g_office<>'M' or !Islron())
    endwith
  endfunc

  add object oGBARROTO_1_22 as StdCheck with uid="DAZEVVYSKK",rtseq=22,rtrep=.f.,left=149, top=235, caption="Flag importi con decimali",;
    ToolTipText = "Se attivo, visualizza importi con decimali",;
    HelpContextID = 32256437,;
    cFormVar="w_GBARROTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGBARROTO_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGBARROTO_1_22.GetRadio()
    this.Parent.oContained.w_GBARROTO = this.RadioValue()
    return .t.
  endfunc

  func oGBARROTO_1_22.SetRadio()
    this.Parent.oContained.w_GBARROTO=trim(this.Parent.oContained.w_GBARROTO)
    this.value = ;
      iif(this.Parent.oContained.w_GBARROTO=='S',1,;
      0)
  endfunc


  add object oBtn_1_23 as StdButton with uid="VFZYZTLWLB",left=654, top=269, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la stampa";
    , HelpContextID = 35443162;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        GSCG_BRB(this.Parent.oContained,2)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( .cFunction='Query' and not empty(.w_GBNUMBIL) and not .w_GBNUMBIL=0)
      endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="QFTLKQVYFR",left=705, top=269, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per memorizzare i dati inseriti";
    , HelpContextID = 42578714;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSCG_BRB(this.Parent.oContained,1)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( not empty(.w_GBSTRUCT))
      endwith
    endif
  endfunc

  add object oDESCRI_1_36 as StdField with uid="NHBICMTFNP",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della struttura di bilancio selezionata",;
    HelpContextID = 69315530,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=289, Top=126, InputMask=replicate('X',40)


  add object oObj_1_38 as cp_runprogram with uid="ANBQYSBFOI",left=-5, top=330, width=164,height=19,;
    caption='GSCG_BRB(1)',;
   bGlobalFont=.t.,;
    prg="GSCG_BRB(1)",;
    cEvent = "Save",;
    nPag=1;
    , HelpContextID = 172743128

  add object oDESATT_1_53 as StdField with uid="FWHKPXWHJN",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 151235530,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=239, Top=89, InputMask=replicate('X',35)

  func oDESATT_1_53.mHide()
    with this.Parent.oContained
      return (g_ATTIVI <>'S')
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="IXMREBHNZQ",Visible=.t., Left=5, Top=17,;
    Alignment=1, Width=135, Height=15,;
    Caption="Numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="EFDMECTCSJ",Visible=.t., Left=389, Top=17,;
    Alignment=1, Width=41, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="YBVABMJXVT",Visible=.t., Left=211, Top=17,;
    Alignment=1, Width=71, Height=15,;
    Caption="Esercizio:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="YHWFCRTSIU",Visible=.t., Left=5, Top=128,;
    Alignment=1, Width=135, Height=15,;
    Caption="Struttura di bilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="SGJKVOPZCJ",Visible=.t., Left=528, Top=55,;
    Alignment=1, Width=112, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="APHBLFQSKQ",Visible=.t., Left=368, Top=55,;
    Alignment=1, Width=62, Height=15,;
    Caption="a data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="ZZMCOPBDSE",Visible=.t., Left=5, Top=164,;
    Alignment=1, Width=135, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="WQYQPITWGQ",Visible=.t., Left=210, Top=55,;
    Alignment=1, Width=72, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="JGTENUWSCZ",Visible=.t., Left=26, Top=55,;
    Alignment=1, Width=114, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="SIUNGMFKSR",Visible=.t., Left=23, Top=92,;
    Alignment=1, Width=117, Height=18,;
    Caption="Attivit� IVA:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (g_ATTIVI <>'S')
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="MXWIGEQXFN",Visible=.t., Left=542, Top=166,;
    Alignment=1, Width=83, Height=18,;
    Caption="Tipo stampa:"  ;
  , bGlobalFont=.t.
enddefine
define class tgscg_agbPag2 as StdContainer
  Width  = 768
  height = 320
  stdWidth  = 768
  stdheight = 320
  resizeXpos=343
  resizeYpos=153
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="CYZXEGDMTR",left=3, top=8, width=760, height=312, bOnScreen=.t.;



  add object oBtn_2_2 as StdButton with uid="JHHZMBUWGB",left=711, top=270, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per memorizzare i dati inseriti";
    , HelpContextID = 42578714;
    , tabstop=.f.,Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      with this.Parent.oContained
        GSCG_BRB(this.Parent.oContained,1)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( not empty(.w_GBSTRUCT))
      endwith
    endif
  endfunc


  add object oObj_2_3 as cp_runprogram with uid="UQDHCQFZKO",left=5, top=345, width=224,height=19,;
    caption='GSCG_BRB(1)',;
   bGlobalFont=.t.,;
    prg="GSCG_BRB(1)",;
    cEvent = "Save",;
    nPag=2;
    , HelpContextID = 172743128
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_mdm",lower(this.oContained.gscg_mdm.class))=0
        this.oContained.gscg_mdm.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgscg_agbPag3 as StdContainer
  Width  = 768
  height = 320
  stdWidth  = 768
  stdheight = 320
  resizeXpos=517
  resizeYpos=192
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="UNEEUXZLWP",left=8, top=9, width=747, height=306, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_mdb",lower(this.oContained.GSCG_MDB.class))=0
        this.oContained.GSCG_MDB.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_agb','GESTBIVA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GBNUMBIL=GESTBIVA.GBNUMBIL";
  +" and "+i_cAliasName2+".GBBILESE=GESTBIVA.GBBILESE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
