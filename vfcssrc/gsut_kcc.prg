* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kcc                                                        *
*              Cifratura CNF                                                   *
*                                                                              *
*      Author: Fabrizio Pollina                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_144]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-25                                                      *
* Last revis.: 2008-09-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_kcc
* Attivabile solo da un utente amministratore
if !cp_IsAdministrator(.t.)
 Ah_ErrorMsg("Funzionalit� abilitata solo per utenti amministratori")
 return
EndIf
* --- Fine Area Manuale
return(createobject("tgsut_kcc",oParentObject))

* --- Class definition
define class tgsut_kcc as StdForm
  Top    = 18
  Left   = 12

  * --- Standard Properties
  Width  = 674
  Height = 362
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-02"
  HelpContextID=99173225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kcc"
  cComment = "Cifratura CNF"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CNFIn = space(254)
  o_CNFIn = space(254)
  w_TipCryptAR = space(1)
  w_TipCryptAR = space(1)
  w_CONN = space(250)
  w_CIFRCONN = space(250)
  w_AppRole = space(250)
  w_PASS = space(250)
  w_CIFRAppRole = space(250)
  w_CIFRPASS = space(250)
  w_CNFOut = space(254)
  w_TipoDB = space(25)
  w_PosDBType = 0
  w_PosCrypt = 0
  w_PosAppRole = 0
  w_PosAppPW = 0
  w_PosConn = 0
  w_FLGEN = .F.
  w_MESSAGGIO = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kccPag1","gsut_kcc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCNFIn_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        gsut_bcc(this,"S")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CNFIn=space(254)
      .w_TipCryptAR=space(1)
      .w_TipCryptAR=space(1)
      .w_CONN=space(250)
      .w_CIFRCONN=space(250)
      .w_AppRole=space(250)
      .w_PASS=space(250)
      .w_CIFRAppRole=space(250)
      .w_CIFRPASS=space(250)
      .w_CNFOut=space(254)
      .w_TipoDB=space(25)
      .w_PosDBType=0
      .w_PosCrypt=0
      .w_PosAppRole=0
      .w_PosAppPW=0
      .w_PosConn=0
      .w_FLGEN=.f.
      .w_MESSAGGIO=space(0)
          .DoRTCalc(1,1,.f.)
        .w_TipCryptAR = 'N'
        .w_TipCryptAR = 'N'
        .w_CONN = iif( g_CRYPT='N', CP_ODBCCONN , "" )
          .DoRTCalc(5,5,.f.)
        .w_AppRole = this.oParentObject .w_ROLE
          .DoRTCalc(7,9,.f.)
        .w_CNFOut = iif(EMPTY(.w_CNFOut),.w_CNFIn,.w_CNFOut)
        .w_TipoDB = CP_DBTYPE
        .w_PosDBType = 0
        .w_PosCrypt = 0
        .w_PosAppRole = 0
        .w_PosAppPW = 0
        .w_PosConn = 0
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .w_FLGEN = .F.
        .w_MESSAGGIO = MessaggioDiIstruzione()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_CNFIn<>.w_CNFIn
            .w_CNFOut = iif(EMPTY(.w_CNFOut),.w_CNFIn,.w_CNFOut)
        endif
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTipCryptAR_1_5.visible=!this.oPgFrm.Page1.oPag.oTipCryptAR_1_5.mHide()
    this.oPgFrm.Page1.oPag.oTipCryptAR_1_6.visible=!this.oPgFrm.Page1.oPag.oTipCryptAR_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCONN_1_8.visible=!this.oPgFrm.Page1.oPag.oCONN_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCIFRCONN_1_12.visible=!this.oPgFrm.Page1.oPag.oCIFRCONN_1_12.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_13.visible=!this.oPgFrm.Page1.oPag.oBtn_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oAppRole_1_15.visible=!this.oPgFrm.Page1.oPag.oAppRole_1_15.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_16.visible=!this.oPgFrm.Page1.oPag.oBtn_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oPASS_1_18.visible=!this.oPgFrm.Page1.oPag.oPASS_1_18.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_19.visible=!this.oPgFrm.Page1.oPag.oBtn_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCIFRAppRole_1_21.visible=!this.oPgFrm.Page1.oPag.oCIFRAppRole_1_21.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oCIFRPASS_1_24.visible=!this.oPgFrm.Page1.oPag.oCIFRPASS_1_24.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_25.visible=!this.oPgFrm.Page1.oPag.oBtn_1_25.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCNFIn_1_2.value==this.w_CNFIn)
      this.oPgFrm.Page1.oPag.oCNFIn_1_2.value=this.w_CNFIn
    endif
    if not(this.oPgFrm.Page1.oPag.oTipCryptAR_1_5.RadioValue()==this.w_TipCryptAR)
      this.oPgFrm.Page1.oPag.oTipCryptAR_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTipCryptAR_1_6.RadioValue()==this.w_TipCryptAR)
      this.oPgFrm.Page1.oPag.oTipCryptAR_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONN_1_8.value==this.w_CONN)
      this.oPgFrm.Page1.oPag.oCONN_1_8.value=this.w_CONN
    endif
    if not(this.oPgFrm.Page1.oPag.oCIFRCONN_1_12.value==this.w_CIFRCONN)
      this.oPgFrm.Page1.oPag.oCIFRCONN_1_12.value=this.w_CIFRCONN
    endif
    if not(this.oPgFrm.Page1.oPag.oAppRole_1_15.RadioValue()==this.w_AppRole)
      this.oPgFrm.Page1.oPag.oAppRole_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPASS_1_18.value==this.w_PASS)
      this.oPgFrm.Page1.oPag.oPASS_1_18.value=this.w_PASS
    endif
    if not(this.oPgFrm.Page1.oPag.oCIFRAppRole_1_21.value==this.w_CIFRAppRole)
      this.oPgFrm.Page1.oPag.oCIFRAppRole_1_21.value=this.w_CIFRAppRole
    endif
    if not(this.oPgFrm.Page1.oPag.oCIFRPASS_1_24.value==this.w_CIFRPASS)
      this.oPgFrm.Page1.oPag.oCIFRPASS_1_24.value=this.w_CIFRPASS
    endif
    if not(this.oPgFrm.Page1.oPag.oCNFOut_1_27.value==this.w_CNFOut)
      this.oPgFrm.Page1.oPag.oCNFOut_1_27.value=this.w_CNFOut
    endif
    if not(this.oPgFrm.Page1.oPag.oMESSAGGIO_1_40.value==this.w_MESSAGGIO)
      this.oPgFrm.Page1.oPag.oMESSAGGIO_1_40.value=this.w_MESSAGGIO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CNFIn = this.w_CNFIn
    return

enddefine

* --- Define pages as container
define class tgsut_kccPag1 as StdContainer
  Width  = 670
  height = 362
  stdWidth  = 670
  stdheight = 362
  resizeXpos=482
  resizeYpos=55
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCNFIn_1_2 as StdField with uid="IAMWPSWCRE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CNFIn", cQueryName = "CNFIn",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso e nome del file CNF da cui caricare le impostazioni",;
    HelpContextID = 247173338,;
   bGlobalFont=.t.,;
    Height=21, Width=564, Left=15, Top=112, InputMask=replicate('X',254)


  add object oBtn_1_3 as StdButton with uid="IXNSCRKOFV",left=580, top=113, width=20,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Seleziona file da aprire";
    , HelpContextID = 98972202;
  , bGlobalFont=.t.

    proc oBtn_1_3.Click()
      with this.Parent.oContained
        gsut_bcc(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oTipCryptAR_1_5 as StdCombo with uid="OTRLCAAMDG",rtseq=2,rtrep=.f.,left=119,top=142,width=123,height=21;
    , ToolTipText = "Tipo di protezione da applicare";
    , HelpContextID = 176259514;
    , cFormVar="w_TipCryptAR",RowSource=""+"Cifra stringa,"+"Nessuna cifratura", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTipCryptAR_1_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oTipCryptAR_1_5.GetRadio()
    this.Parent.oContained.w_TipCryptAR = this.RadioValue()
    return .t.
  endfunc

  func oTipCryptAR_1_5.SetRadio()
    this.Parent.oContained.w_TipCryptAR=trim(this.Parent.oContained.w_TipCryptAR)
    this.value = ;
      iif(this.Parent.oContained.w_TipCryptAR=='C',1,;
      iif(this.Parent.oContained.w_TipCryptAR=='N',2,;
      0))
  endfunc

  func oTipCryptAR_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPODB='SQLServer')
    endwith
  endfunc


  add object oTipCryptAR_1_6 as StdCombo with uid="GOSGENOUZR",rtseq=3,rtrep=.f.,left=119,top=142,width=123,height=21;
    , ToolTipText = "Tipo di protezione da applicare";
    , HelpContextID = 176259514;
    , cFormVar="w_TipCryptAR",RowSource=""+"Application Role,"+"Cifra stringa,"+"Nessuna cifratura", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTipCryptAR_1_6.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oTipCryptAR_1_6.GetRadio()
    this.Parent.oContained.w_TipCryptAR = this.RadioValue()
    return .t.
  endfunc

  func oTipCryptAR_1_6.SetRadio()
    this.Parent.oContained.w_TipCryptAR=trim(this.Parent.oContained.w_TipCryptAR)
    this.value = ;
      iif(this.Parent.oContained.w_TipCryptAR=='A',1,;
      iif(this.Parent.oContained.w_TipCryptAR=='C',2,;
      iif(this.Parent.oContained.w_TipCryptAR=='N',3,;
      0)))
  endfunc

  func oTipCryptAR_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPODB<>'SQLServer')
    endwith
  endfunc

  add object oCONN_1_8 as StdField with uid="XZWNHEJMPK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CONN", cQueryName = "CONN",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di connessione da cifrare",;
    HelpContextID = 93720538,;
   bGlobalFont=.t.,;
    Height=21, Width=564, Left=13, Top=192, InputMask=replicate('X',250)

  func oCONN_1_8.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'C' AND .w_TipCryptAR='A')
    endwith
  endfunc


  add object oBtn_1_10 as StdButton with uid="ZMIZZNTUDJ",left=610, top=179, width=48,height=45,;
    CpPicture="BMP\CIFRA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per cifrare la stringa di connessione";
    , HelpContextID = 257949835;
    , caption='C\<ifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        .w_CIFRCONN=CifraCnf( ALLTRIM(.w_CONN) , 'C' )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipCryptAR<>'C')
     endwith
    endif
  endfunc

  add object oCIFRCONN_1_12 as StdField with uid="OBYZWKYTZJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CIFRCONN", cQueryName = "CIFRCONN",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di connessione cifrata",;
    HelpContextID = 40015244,;
   bGlobalFont=.t.,;
    Height=21, Width=562, Left=14, Top=238, InputMask=replicate('X',250), readonly = .t.

  func oCIFRCONN_1_12.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'C')
    endwith
  endfunc


  add object oBtn_1_13 as StdButton with uid="QVIVCARPHQ",left=610, top=226, width=48,height=45,;
    CpPicture="BMP\DECIFRA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per decifrare la stringa di connessione";
    , HelpContextID = 56339334;
    , caption='Deci\<fra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        .w_CONN=CifraCnf( ALLTRIM(.w_CIFRCONN) , 'D' )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipCryptAR<>'C')
     endwith
    endif
  endfunc


  add object oAppRole_1_15 as StdTableCombo with uid="QEJYBBSZIM",rtseq=6,rtrep=.f.,left=12,top=193,width=250,height=21;
    , ToolTipText = "Application Role da cifrare";
    , HelpContextID = 224407814;
    , cFormVar="w_AppRole",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='QUERY\GSUT_KAR.VQR',cKey='ROLENAME',cValue='ROLENAME',cOrderBy='',xDefault=space(250);
  , bGlobalFont=.t.


  func oAppRole_1_15.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'A')
    endwith
  endfunc


  add object oBtn_1_16 as StdButton with uid="XBAOGRHWXF",left=286, top=179, width=48,height=45,;
    CpPicture="BMP\CIFRA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per cifrare l'Application Role";
    , HelpContextID = 257977962;
    , caption='\<Cifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        .w_CIFRAPPROLE=CifraCnf( ALLTRIM(.w_APPROLE) , 'C' )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipCryptAR<>'A')
     endwith
    endif
  endfunc

  add object oPASS_1_18 as StdField with uid="XFNGLZYQWG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PASS", cQueryName = "PASS",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Password da cifrare",;
    HelpContextID = 93375754,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=340, Top=192, InputMask=replicate('X',250)

  func oPASS_1_18.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'A')
    endwith
  endfunc


  add object oBtn_1_19 as StdButton with uid="FTSUODACDM",left=610, top=179, width=48,height=45,;
    CpPicture="BMP\CIFRA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per cifrare la password";
    , HelpContextID = 257948489;
    , caption='C\<ifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        .w_CIFRPASS=CifraCnf( ALLTRIM(.w_PASS) , 'C' )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipCryptAR<>'A')
     endwith
    endif
  endfunc

  add object oCIFRAppRole_1_21 as StdField with uid="BGOIZNZTLS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CIFRAppRole", cQueryName = "CIFRAppRole",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Application Role cifrato",;
    HelpContextID = 243543400,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=12, Top=238, InputMask=replicate('X',250)

  func oCIFRAppRole_1_21.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'A')
    endwith
  endfunc


  add object oBtn_1_22 as StdButton with uid="FFOLHJLTHT",left=286, top=226, width=48,height=45,;
    CpPicture="BMP\DECIFRA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per decifrare l'Application Role";
    , HelpContextID = 49138790;
    , caption='\<Decifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        .w_APPROLE=CifraCnf( ALLTRIM(.w_CIFRAPPROLE) , 'D' )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipCryptAR<>'A')
     endwith
    endif
  endfunc

  add object oCIFRPASS_1_24 as StdField with uid="DJFVDUOGHL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CIFRPASS", cQueryName = "CIFRPASS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Password cifrata",;
    HelpContextID = 7170681,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=339, Top=238, InputMask=replicate('X',250)

  func oCIFRPASS_1_24.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'A')
    endwith
  endfunc


  add object oBtn_1_25 as StdButton with uid="YINHZSPDYQ",left=610, top=226, width=48,height=45,;
    CpPicture="BMP\DECIFRA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per decifrare la password";
    , HelpContextID = 56684118;
    , caption='Deci\<fra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        .w_PASS=CifraCnf( ALLTRIM(.w_CIFRPASS) , 'D' )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipCryptAR<>'A')
     endwith
    endif
  endfunc

  add object oCNFOut_1_27 as StdField with uid="IRJOVUJQIE",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CNFOut", cQueryName = "CNFOut",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso e nome del file CNF in cui salvare le modifiche",;
    HelpContextID = 96104230,;
   bGlobalFont=.t.,;
    Height=21, Width=564, Left=13, Top=285, InputMask=replicate('X',254)


  add object oBtn_1_28 as StdButton with uid="ZLTCUQWHCF",left=578, top=286, width=20,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Seleziona file da sovrascrivere";
    , HelpContextID = 98972202;
  , bGlobalFont=.t.

    proc oBtn_1_28.Click()
      with this.Parent.oContained
        .w_CNFOUT=GETFILE('CNF','CNF selezionato','Seleziona',0,'Seleziona CNF')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="EYHKPEEBMU",left=553, top=312, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Salva CNF";
    , HelpContextID = 257694938;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        gsut_bcc(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_30 as StdButton with uid="JWIBHYOYEX",left=611, top=312, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 91855802;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_37 as cp_runprogram with uid="RDIKQIKLRV",left=1, top=378, width=202,height=19,;
    caption='GSUT_BCC',;
   bGlobalFont=.t.,;
    prg="gsut_bcc('M')",;
    cEvent = "w_CNFIn Changed",;
    nPag=1;
    , HelpContextID = 39871657

  add object oMESSAGGIO_1_40 as StdMemo with uid="FTDNCVPQBS",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MESSAGGIO", cQueryName = "MESSAGGIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 92224511,;
   bGlobalFont=.t.,;
    Height=78, Width=639, Left=14, Top=3

  add object oStr_1_1 as StdString with uid="SRYNMCLUKK",Visible=.t., Left=17, Top=96,;
    Alignment=0, Width=124, Height=18,;
    Caption="CNF da modificare"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="QWTBHVFFJB",Visible=.t., Left=6, Top=143,;
    Alignment=1, Width=106, Height=18,;
    Caption="Tipo di protezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="SEATOFCXUK",Visible=.t., Left=15, Top=173,;
    Alignment=0, Width=215, Height=19,;
    Caption="Stringa da cifrare"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'C')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="NPXUWPCIVJ",Visible=.t., Left=15, Top=173,;
    Alignment=0, Width=215, Height=19,;
    Caption="Stringa di connessione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'N')
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="SIFKZLYFJF",Visible=.t., Left=12, Top=218,;
    Alignment=0, Width=192, Height=19,;
    Caption="Stringa cifrata"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'C')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="XPAMFYNFWQ",Visible=.t., Left=15, Top=173,;
    Alignment=0, Width=215, Height=19,;
    Caption="Application Role da cifrare"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'A')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="JGSBCWPXDF",Visible=.t., Left=340, Top=173,;
    Alignment=0, Width=181, Height=19,;
    Caption="Password da cifrare"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'A')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="AHRMFDOXRS",Visible=.t., Left=12, Top=218,;
    Alignment=0, Width=192, Height=19,;
    Caption="Application Role cifrato"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'A')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="LITBTCYDRJ",Visible=.t., Left=339, Top=218,;
    Alignment=0, Width=170, Height=19,;
    Caption="Password cifrata"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_TipCryptAR<>'A')
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="TUJXHORQGJ",Visible=.t., Left=17, Top=268,;
    Alignment=0, Width=123, Height=18,;
    Caption="CNF da scrivere"  ;
  , bGlobalFont=.t.

  add object oBox_1_38 as StdBox with uid="EABYCGLVAO",left=13, top=84, width=645,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kcc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kcc
Function MessaggioDiIstruzione()
local l_ReturnedMsg
l_ReturnedMsg = "Questa funzione permette di aprire un CNF esistente, modificarne le impostazioni di crittografia%0e salvare le modifiche effettuate sovrascrivendo il file o creando un nuovo CNF"
l_ReturnedMsg = l_ReturnedMsg + ".%0Se il database impostato nel CNF � di tipo SQL Server sar� possibile utilizzare la crittografia dell'application role selezionando il tipo di protezione"
l_ReturnedMsg = AH_MSGFORMAT( l_ReturnedMsg )
return l_ReturnedMsg
* --- Fine Area Manuale
