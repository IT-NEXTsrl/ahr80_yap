* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bvv                                                        *
*              Lettura tariffa concordata                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-01                                                      *
* Last revis.: 2014-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bvv",oParentObject,m.pOper)
return(i_retval)

define class tgsve_bvv as StdBatch
  * --- Local variables
  pOper = space(5)
  w_CALCONC = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciata da GSVE_MDV
    * --- Legge tariffa concordata sulla pratica
    do case
      case this.pOper == "LEGGETARCONC" AND ISALT()
        * --- Se sulla pratica � definita Tariffa concordata e se siamo in presenza di una tariffa a tempo
        if this.oParentObject.w_CNTARTEM="C" AND this.oParentObject.w_PRESTA="P"
          * --- Dichiarazione array tariffe a tempo concordate
          DECLARE ARRTARCON (3,1)
          * --- Azzero l'array che verr� riempito dalla funzione Caltarcon()
          ARRTARCON(1)=0 
 ARRTARCON(2)= " " 
 ARRTARCON(3)=0
          this.w_CALCONC = CALTARCON(this.oParentObject.w_MVCODCOM, this.oParentObject.w_MVCODRES, this.oParentObject.w_MVCODICE, @ARRTARCON)
          this.oParentObject.w_CNTARCON = ARRTARCON(1)
          this.oParentObject.w_SAV_CNTARCON = ARRTARCON(1)
          this.oParentObject.w_MVQTAMOV = ARRTARCON(3)
          this.oParentObject.w_SAV_MVQTAMOV = ARRTARCON(3)
        else
          this.oParentObject.w_SAV_CNTARCON = 0
          this.oParentObject.w_SAV_MVQTAMOV = 0
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
