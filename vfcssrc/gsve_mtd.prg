* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_mtd                                                        *
*              Report documenti                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_36]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-06                                                      *
* Last revis.: 2013-04-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsve_mtd")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsve_mtd")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsve_mtd")
  return

* --- Class definition
define class tgsve_mtd as StdPCForm
  Width  = 713
  Height = 291
  Top    = 45
  Left   = 39
  cComment = "Report documenti"
  cPrg = "gsve_mtd"
  HelpContextID=80360041
  add object cnt as tcgsve_mtd
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsve_mtd as PCContext
  w_LGCODICE = space(5)
  w_PRGSTASEC = space(30)
  w_PRGSTAPRI = space(30)
  w_PRGSTAPRO = space(30)
  w_CPROWORD = 0
  w_LGTIPREP = space(1)
  w_LGCODLIN = space(3)
  w_LGGRPDEF = space(5)
  w_LGCODUTE = 0
  w_LGCODGRP = 0
  w_PRGSTA = space(30)
  w_LGPRGSTA = 0
  w_LGFLGOPT = space(1)
  w_LGDESCOD = space(35)
  w_DESLIN = space(30)
  w_DESOUT = space(100)
  w_DESGRPDEF = space(40)
  w_DESCODUTE = space(20)
  w_DESCODGRP = space(20)
  w_LG_SPLIT = space(254)
  w_LG__LINK = space(254)
  w_LGSTMPOR = space(2)
  w_LGNO_RST = space(1)
  w_LGFLPRSY = space(1)
  proc Save(i_oFrom)
    this.w_LGCODICE = i_oFrom.w_LGCODICE
    this.w_PRGSTASEC = i_oFrom.w_PRGSTASEC
    this.w_PRGSTAPRI = i_oFrom.w_PRGSTAPRI
    this.w_PRGSTAPRO = i_oFrom.w_PRGSTAPRO
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_LGTIPREP = i_oFrom.w_LGTIPREP
    this.w_LGCODLIN = i_oFrom.w_LGCODLIN
    this.w_LGGRPDEF = i_oFrom.w_LGGRPDEF
    this.w_LGCODUTE = i_oFrom.w_LGCODUTE
    this.w_LGCODGRP = i_oFrom.w_LGCODGRP
    this.w_PRGSTA = i_oFrom.w_PRGSTA
    this.w_LGPRGSTA = i_oFrom.w_LGPRGSTA
    this.w_LGFLGOPT = i_oFrom.w_LGFLGOPT
    this.w_LGDESCOD = i_oFrom.w_LGDESCOD
    this.w_DESLIN = i_oFrom.w_DESLIN
    this.w_DESOUT = i_oFrom.w_DESOUT
    this.w_DESGRPDEF = i_oFrom.w_DESGRPDEF
    this.w_DESCODUTE = i_oFrom.w_DESCODUTE
    this.w_DESCODGRP = i_oFrom.w_DESCODGRP
    this.w_LG_SPLIT = i_oFrom.w_LG_SPLIT
    this.w_LG__LINK = i_oFrom.w_LG__LINK
    this.w_LGSTMPOR = i_oFrom.w_LGSTMPOR
    this.w_LGNO_RST = i_oFrom.w_LGNO_RST
    this.w_LGFLPRSY = i_oFrom.w_LGFLPRSY
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_LGCODICE = this.w_LGCODICE
    i_oTo.w_PRGSTASEC = this.w_PRGSTASEC
    i_oTo.w_PRGSTAPRI = this.w_PRGSTAPRI
    i_oTo.w_PRGSTAPRO = this.w_PRGSTAPRO
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_LGTIPREP = this.w_LGTIPREP
    i_oTo.w_LGCODLIN = this.w_LGCODLIN
    i_oTo.w_LGGRPDEF = this.w_LGGRPDEF
    i_oTo.w_LGCODUTE = this.w_LGCODUTE
    i_oTo.w_LGCODGRP = this.w_LGCODGRP
    i_oTo.w_PRGSTA = this.w_PRGSTA
    i_oTo.w_LGPRGSTA = this.w_LGPRGSTA
    i_oTo.w_LGFLGOPT = this.w_LGFLGOPT
    i_oTo.w_LGDESCOD = this.w_LGDESCOD
    i_oTo.w_DESLIN = this.w_DESLIN
    i_oTo.w_DESOUT = this.w_DESOUT
    i_oTo.w_DESGRPDEF = this.w_DESGRPDEF
    i_oTo.w_DESCODUTE = this.w_DESCODUTE
    i_oTo.w_DESCODGRP = this.w_DESCODGRP
    i_oTo.w_LG_SPLIT = this.w_LG_SPLIT
    i_oTo.w_LG__LINK = this.w_LG__LINK
    i_oTo.w_LGSTMPOR = this.w_LGSTMPOR
    i_oTo.w_LGNO_RST = this.w_LGNO_RST
    i_oTo.w_LGFLPRSY = this.w_LGFLPRSY
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsve_mtd as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 713
  Height = 291
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-04-17"
  HelpContextID=80360041
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  REPO_DOC_IDX = 0
  LINGUE_IDX = 0
  OUT_PUTS_IDX = 0
  GRP_DEFA_IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  cFile = "REPO_DOC"
  cKeySelect = "LGCODICE"
  cKeyWhere  = "LGCODICE=this.w_LGCODICE"
  cKeyDetail  = "LGCODICE=this.w_LGCODICE"
  cKeyWhereODBC = '"LGCODICE="+cp_ToStrODBC(this.w_LGCODICE)';

  cKeyDetailWhereODBC = '"LGCODICE="+cp_ToStrODBC(this.w_LGCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"REPO_DOC.LGCODICE="+cp_ToStrODBC(this.w_LGCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'REPO_DOC.CPROWORD '
  cPrg = "gsve_mtd"
  cComment = "Report documenti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LGCODICE = space(5)
  w_PRGSTASEC = space(30)
  w_PRGSTAPRI = space(30)
  w_PRGSTAPRO = space(30)
  w_CPROWORD = 0
  w_LGTIPREP = space(1)
  o_LGTIPREP = space(1)
  w_LGCODLIN = space(3)
  w_LGGRPDEF = space(5)
  w_LGCODUTE = 0
  w_LGCODGRP = 0
  w_PRGSTA = space(30)
  w_LGPRGSTA = 0
  o_LGPRGSTA = 0
  w_LGFLGOPT = space(1)
  w_LGDESCOD = space(35)
  w_DESLIN = space(30)
  w_DESOUT = space(100)
  w_DESGRPDEF = space(40)
  w_DESCODUTE = space(20)
  w_DESCODGRP = space(20)
  w_LG_SPLIT = space(254)
  w_LG__LINK = space(254)
  w_LGSTMPOR = space(2)
  w_LGNO_RST = space(1)
  w_LGFLPRSY = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_mtdPag1","gsve_mtd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLGSTMPOR_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='LINGUE'
    this.cWorkTables[2]='OUT_PUTS'
    this.cWorkTables[3]='GRP_DEFA'
    this.cWorkTables[4]='CPUSERS'
    this.cWorkTables[5]='CPGROUPS'
    this.cWorkTables[6]='REPO_DOC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.REPO_DOC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.REPO_DOC_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsve_mtd'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from REPO_DOC where LGCODICE=KeySet.LGCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.REPO_DOC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.REPO_DOC_IDX,2],this.bLoadRecFilter,this.REPO_DOC_IDX,"gsve_mtd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('REPO_DOC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "REPO_DOC.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' REPO_DOC '
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LGCODICE',this.w_LGCODICE  )
      select * from (i_cTable) REPO_DOC where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LGCODICE = NVL(LGCODICE,space(5))
        .w_PRGSTASEC = LEFT(this.oParentObject .w_PRGALT + SPACE(30),30)
        .w_PRGSTAPRI = LEFT(this.oParentObject .w_PRGSTA + SPACE(30),30)
        .w_PRGSTAPRO = LEFT('GSDS_SDP'+SPACE(30),30)
        .w_LGSTMPOR = NVL(LGSTMPOR,space(2))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'REPO_DOC')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESLIN = space(30)
          .w_DESOUT = space(100)
          .w_DESGRPDEF = space(40)
          .w_DESCODUTE = space(20)
          .w_DESCODGRP = space(20)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_LGTIPREP = NVL(LGTIPREP,space(1))
          .w_LGCODLIN = NVL(LGCODLIN,space(3))
          if link_2_3_joined
            this.w_LGCODLIN = NVL(LUCODICE203,NVL(this.w_LGCODLIN,space(3)))
            this.w_DESLIN = NVL(LUDESCRI203,space(30))
          else
          .link_2_3('Load')
          endif
          .w_LGGRPDEF = NVL(LGGRPDEF,space(5))
          if link_2_4_joined
            this.w_LGGRPDEF = NVL(GDCODICE204,NVL(this.w_LGGRPDEF,space(5)))
            this.w_DESGRPDEF = NVL(GDDESCRI204,space(40))
          else
          .link_2_4('Load')
          endif
          .w_LGCODUTE = NVL(LGCODUTE,0)
          .link_2_5('Load')
          .w_LGCODGRP = NVL(LGCODGRP,0)
          .link_2_6('Load')
        .w_PRGSTA = IIF(.w_LGTIPREP='M', .w_PRGSTAPRI, IIF(.w_LGTIPREP='S', .w_PRGSTASEC, .w_PRGSTAPRO))
          .w_LGPRGSTA = NVL(LGPRGSTA,0)
          .link_2_8('Load')
          .w_LGFLGOPT = NVL(LGFLGOPT,space(1))
          .w_LGDESCOD = NVL(LGDESCOD,space(35))
          .w_LG_SPLIT = NVL(LG_SPLIT,space(254))
          .w_LG__LINK = NVL(LG__LINK,space(254))
          .w_LGNO_RST = NVL(LGNO_RST,space(1))
          .w_LGFLPRSY = NVL(LGFLPRSY,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_PRGSTASEC = LEFT(this.oParentObject .w_PRGALT + SPACE(30),30)
        .w_PRGSTAPRI = LEFT(this.oParentObject .w_PRGSTA + SPACE(30),30)
        .w_PRGSTAPRO = LEFT('GSDS_SDP'+SPACE(30),30)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_19.enabled = .oPgFrm.Page1.oPag.oBtn_2_19.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_LGCODICE=space(5)
      .w_PRGSTASEC=space(30)
      .w_PRGSTAPRI=space(30)
      .w_PRGSTAPRO=space(30)
      .w_CPROWORD=10
      .w_LGTIPREP=space(1)
      .w_LGCODLIN=space(3)
      .w_LGGRPDEF=space(5)
      .w_LGCODUTE=0
      .w_LGCODGRP=0
      .w_PRGSTA=space(30)
      .w_LGPRGSTA=0
      .w_LGFLGOPT=space(1)
      .w_LGDESCOD=space(35)
      .w_DESLIN=space(30)
      .w_DESOUT=space(100)
      .w_DESGRPDEF=space(40)
      .w_DESCODUTE=space(20)
      .w_DESCODGRP=space(20)
      .w_LG_SPLIT=space(254)
      .w_LG__LINK=space(254)
      .w_LGSTMPOR=space(2)
      .w_LGNO_RST=space(1)
      .w_LGFLPRSY=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_PRGSTASEC = LEFT(this.oParentObject .w_PRGALT + SPACE(30),30)
        .w_PRGSTAPRI = LEFT(this.oParentObject .w_PRGSTA + SPACE(30),30)
        .w_PRGSTAPRO = LEFT('GSDS_SDP'+SPACE(30),30)
        .DoRTCalc(5,5,.f.)
        .w_LGTIPREP = 'M'
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_LGCODLIN))
         .link_2_3('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_LGGRPDEF))
         .link_2_4('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_LGCODUTE))
         .link_2_5('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_LGCODGRP))
         .link_2_6('Full')
        endif
        .w_PRGSTA = IIF(.w_LGTIPREP='M', .w_PRGSTAPRI, IIF(.w_LGTIPREP='S', .w_PRGSTASEC, .w_PRGSTAPRO))
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_LGPRGSTA))
         .link_2_8('Full')
        endif
        .w_LGFLGOPT = IIF(.w_LGTIPREP<>'M' AND .w_LGTIPREP<>'R', .w_LGFLGOPT, 'N')
        .DoRTCalc(14,23,.f.)
        .w_LGFLPRSY = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'REPO_DOC')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_19.enabled = this.oPgFrm.Page1.oPag.oBtn_2_19.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oLGDESCOD_2_10.enabled = i_bVal
      .Page1.oPag.oLGSTMPOR_1_10.enabled = i_bVal
      .Page1.oPag.oBtn_2_19.enabled = .Page1.oPag.oBtn_2_19.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'REPO_DOC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.REPO_DOC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LGCODICE,"LGCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LGSTMPOR,"LGSTMPOR",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_LGTIPREP N(3);
      ,t_LGCODLIN C(3);
      ,t_LGGRPDEF C(5);
      ,t_LGCODUTE N(4);
      ,t_LGCODGRP N(4);
      ,t_LGPRGSTA N(3);
      ,t_LGFLGOPT N(3);
      ,t_LGDESCOD C(35);
      ,t_DESLIN C(30);
      ,t_DESOUT C(100);
      ,t_DESGRPDEF C(40);
      ,t_DESCODUTE C(20);
      ,t_DESCODGRP C(20);
      ,CPROWNUM N(10);
      ,t_PRGSTA C(30);
      ,t_LG_SPLIT C(254);
      ,t_LG__LINK C(254);
      ,t_LGNO_RST C(1);
      ,t_LGFLPRSY C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsve_mtdbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLGTIPREP_2_2.controlsource=this.cTrsName+'.t_LGTIPREP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLGCODLIN_2_3.controlsource=this.cTrsName+'.t_LGCODLIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLGGRPDEF_2_4.controlsource=this.cTrsName+'.t_LGGRPDEF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLGCODUTE_2_5.controlsource=this.cTrsName+'.t_LGCODUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLGCODGRP_2_6.controlsource=this.cTrsName+'.t_LGCODGRP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLGPRGSTA_2_8.controlsource=this.cTrsName+'.t_LGPRGSTA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLGFLGOPT_2_9.controlsource=this.cTrsName+'.t_LGFLGOPT'
    this.oPgFRm.Page1.oPag.oLGDESCOD_2_10.controlsource=this.cTrsName+'.t_LGDESCOD'
    this.oPgFRm.Page1.oPag.oDESLIN_2_11.controlsource=this.cTrsName+'.t_DESLIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESOUT_2_12.controlsource=this.cTrsName+'.t_DESOUT'
    this.oPgFRm.Page1.oPag.oDESGRPDEF_2_13.controlsource=this.cTrsName+'.t_DESGRPDEF'
    this.oPgFRm.Page1.oPag.oDESCODUTE_2_14.controlsource=this.cTrsName+'.t_DESCODUTE'
    this.oPgFRm.Page1.oPag.oDESCODGRP_2_15.controlsource=this.cTrsName+'.t_DESCODGRP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(46)
    this.AddVLine(161)
    this.AddVLine(225)
    this.AddVLine(307)
    this.AddVLine(357)
    this.AddVLine(413)
    this.AddVLine(458)
    this.AddVLine(668)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.REPO_DOC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.REPO_DOC_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.REPO_DOC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.REPO_DOC_IDX,2])
      *
      * insert into REPO_DOC
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'REPO_DOC')
        i_extval=cp_InsertValODBCExtFlds(this,'REPO_DOC')
        i_cFldBody=" "+;
                  "(LGCODICE,CPROWORD,LGTIPREP,LGCODLIN,LGGRPDEF"+;
                  ",LGCODUTE,LGCODGRP,LGPRGSTA,LGFLGOPT,LGDESCOD"+;
                  ",LG_SPLIT,LG__LINK,LGSTMPOR,LGNO_RST,LGFLPRSY,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_LGCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_LGTIPREP)+","+cp_ToStrODBCNull(this.w_LGCODLIN)+","+cp_ToStrODBCNull(this.w_LGGRPDEF)+;
             ","+cp_ToStrODBCNull(this.w_LGCODUTE)+","+cp_ToStrODBCNull(this.w_LGCODGRP)+","+cp_ToStrODBCNull(this.w_LGPRGSTA)+","+cp_ToStrODBC(this.w_LGFLGOPT)+","+cp_ToStrODBC(this.w_LGDESCOD)+;
             ","+cp_ToStrODBC(this.w_LG_SPLIT)+","+cp_ToStrODBC(this.w_LG__LINK)+","+cp_ToStrODBC(this.w_LGSTMPOR)+","+cp_ToStrODBC(this.w_LGNO_RST)+","+cp_ToStrODBC(this.w_LGFLPRSY)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'REPO_DOC')
        i_extval=cp_InsertValVFPExtFlds(this,'REPO_DOC')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'LGCODICE',this.w_LGCODICE)
        INSERT INTO (i_cTable) (;
                   LGCODICE;
                  ,CPROWORD;
                  ,LGTIPREP;
                  ,LGCODLIN;
                  ,LGGRPDEF;
                  ,LGCODUTE;
                  ,LGCODGRP;
                  ,LGPRGSTA;
                  ,LGFLGOPT;
                  ,LGDESCOD;
                  ,LG_SPLIT;
                  ,LG__LINK;
                  ,LGSTMPOR;
                  ,LGNO_RST;
                  ,LGFLPRSY;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_LGCODICE;
                  ,this.w_CPROWORD;
                  ,this.w_LGTIPREP;
                  ,this.w_LGCODLIN;
                  ,this.w_LGGRPDEF;
                  ,this.w_LGCODUTE;
                  ,this.w_LGCODGRP;
                  ,this.w_LGPRGSTA;
                  ,this.w_LGFLGOPT;
                  ,this.w_LGDESCOD;
                  ,this.w_LG_SPLIT;
                  ,this.w_LG__LINK;
                  ,this.w_LGSTMPOR;
                  ,this.w_LGNO_RST;
                  ,this.w_LGFLPRSY;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.REPO_DOC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.REPO_DOC_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_LGPRGSTA)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'REPO_DOC')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " LGSTMPOR="+cp_ToStrODBC(this.w_LGSTMPOR)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'REPO_DOC')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  LGSTMPOR=this.w_LGSTMPOR;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_LGPRGSTA)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update REPO_DOC
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'REPO_DOC')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",LGTIPREP="+cp_ToStrODBC(this.w_LGTIPREP)+;
                     ",LGCODLIN="+cp_ToStrODBCNull(this.w_LGCODLIN)+;
                     ",LGGRPDEF="+cp_ToStrODBCNull(this.w_LGGRPDEF)+;
                     ",LGCODUTE="+cp_ToStrODBCNull(this.w_LGCODUTE)+;
                     ",LGCODGRP="+cp_ToStrODBCNull(this.w_LGCODGRP)+;
                     ",LGPRGSTA="+cp_ToStrODBCNull(this.w_LGPRGSTA)+;
                     ",LGFLGOPT="+cp_ToStrODBC(this.w_LGFLGOPT)+;
                     ",LGDESCOD="+cp_ToStrODBC(this.w_LGDESCOD)+;
                     ",LG_SPLIT="+cp_ToStrODBC(this.w_LG_SPLIT)+;
                     ",LG__LINK="+cp_ToStrODBC(this.w_LG__LINK)+;
                     ",LGSTMPOR="+cp_ToStrODBC(this.w_LGSTMPOR)+;
                     ",LGNO_RST="+cp_ToStrODBC(this.w_LGNO_RST)+;
                     ",LGFLPRSY="+cp_ToStrODBC(this.w_LGFLPRSY)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'REPO_DOC')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,LGTIPREP=this.w_LGTIPREP;
                     ,LGCODLIN=this.w_LGCODLIN;
                     ,LGGRPDEF=this.w_LGGRPDEF;
                     ,LGCODUTE=this.w_LGCODUTE;
                     ,LGCODGRP=this.w_LGCODGRP;
                     ,LGPRGSTA=this.w_LGPRGSTA;
                     ,LGFLGOPT=this.w_LGFLGOPT;
                     ,LGDESCOD=this.w_LGDESCOD;
                     ,LG_SPLIT=this.w_LG_SPLIT;
                     ,LG__LINK=this.w_LG__LINK;
                     ,LGSTMPOR=this.w_LGSTMPOR;
                     ,LGNO_RST=this.w_LGNO_RST;
                     ,LGFLPRSY=this.w_LGFLPRSY;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.REPO_DOC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.REPO_DOC_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_LGPRGSTA)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete REPO_DOC
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_LGPRGSTA)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.REPO_DOC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.REPO_DOC_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .w_PRGSTASEC = LEFT(this.oParentObject .w_PRGALT + SPACE(30),30)
          .w_PRGSTAPRI = LEFT(this.oParentObject .w_PRGSTA + SPACE(30),30)
          .w_PRGSTAPRO = LEFT('GSDS_SDP'+SPACE(30),30)
        .DoRTCalc(5,10,.t.)
          .w_PRGSTA = IIF(.w_LGTIPREP='M', .w_PRGSTAPRI, IIF(.w_LGTIPREP='S', .w_PRGSTASEC, .w_PRGSTAPRO))
        .DoRTCalc(12,12,.t.)
        if .o_LGTIPREP<>.w_LGTIPREP
          .w_LGFLGOPT = IIF(.w_LGTIPREP<>'M' AND .w_LGTIPREP<>'R', .w_LGFLGOPT, 'N')
        endif
        if .o_LGTIPREP<>.w_LGTIPREP
          .Calculate_VVCNUQIHQF()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(14,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PRGSTA with this.w_PRGSTA
      replace t_LG_SPLIT with this.w_LG_SPLIT
      replace t_LG__LINK with this.w_LG__LINK
      replace t_LGNO_RST with this.w_LGNO_RST
      replace t_LGFLPRSY with this.w_LGFLPRSY
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_VVCNUQIHQF()
    with this
          * --- Sbianco num. report, chiave rottura e link
          .w_LG_SPLIT = ""
          .w_LG__LINK = ""
          .w_LGPRGSTA = 0
          .w_DESOUT = ""
    endwith
  endproc
  proc Calculate_ZAYITSCMFD()
    with this
          * --- Valorizzo rottura e link
          .w_LG_SPLIT = IIF(Empty(.w_LG_SPLIT) And .w_LGTIPREP$'M', "MVSERIAL", .w_LG_SPLIT)
          .w_LG__LINK = IIF(Empty(.w_LG__LINK) And .w_LGTIPREP$'S', "MVSERIAL", .w_LG__LINK)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLGFLGOPT_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLGFLGOPT_2_9.mCond()
    this.oPgFrm.Page1.oPag.oLGDESCOD_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oLGDESCOD_2_10.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_LGPRGSTA Changed")
          .Calculate_ZAYITSCMFD()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LGCODLIN
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LGCODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_LGCODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_LGCODLIN))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LGCODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LGCODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oLGCODLIN_2_3'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LGCODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_LGCODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_LGCODLIN)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LGCODLIN = NVL(_Link_.LUCODICE,space(3))
      this.w_DESLIN = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_LGCODLIN = space(3)
      endif
      this.w_DESLIN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LGCODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LINGUE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.LUCODICE as LUCODICE203"+ ",link_2_3.LUDESCRI as LUDESCRI203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on REPO_DOC.LGCODLIN=link_2_3.LUCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and REPO_DOC.LGCODLIN=link_2_3.LUCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LGGRPDEF
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRP_DEFA_IDX,3]
    i_lTable = "GRP_DEFA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRP_DEFA_IDX,2], .t., this.GRP_DEFA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRP_DEFA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LGGRPDEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGD',True,'GRP_DEFA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GDCODICE like "+cp_ToStrODBC(trim(this.w_LGGRPDEF)+"%");

          i_ret=cp_SQL(i_nConn,"select GDCODICE,GDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GDCODICE',trim(this.w_LGGRPDEF))
          select GDCODICE,GDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LGGRPDEF)==trim(_Link_.GDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LGGRPDEF) and !this.bDontReportError
            deferred_cp_zoom('GRP_DEFA','*','GDCODICE',cp_AbsName(oSource.parent,'oLGGRPDEF_2_4'),i_cWhere,'GSAR_AGD',"Gruppi output",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GDCODICE,GDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GDCODICE',oSource.xKey(1))
            select GDCODICE,GDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LGGRPDEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GDCODICE,GDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GDCODICE="+cp_ToStrODBC(this.w_LGGRPDEF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GDCODICE',this.w_LGGRPDEF)
            select GDCODICE,GDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LGGRPDEF = NVL(_Link_.GDCODICE,space(5))
      this.w_DESGRPDEF = NVL(_Link_.GDDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LGGRPDEF = space(5)
      endif
      this.w_DESGRPDEF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRP_DEFA_IDX,2])+'\'+cp_ToStr(_Link_.GDCODICE,1)
      cp_ShowWarn(i_cKey,this.GRP_DEFA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LGGRPDEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRP_DEFA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRP_DEFA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.GDCODICE as GDCODICE204"+ ",link_2_4.GDDESCRI as GDDESCRI204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on REPO_DOC.LGGRPDEF=link_2_4.GDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and REPO_DOC.LGGRPDEF=link_2_4.GDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LGCODUTE
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LGCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_LGCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_LGCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_LGCODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oLGCODUTE_2_5'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LGCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_LGCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_LGCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LGCODUTE = NVL(_Link_.CODE,0)
      this.w_DESCODUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LGCODUTE = 0
      endif
      this.w_DESCODUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LGCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LGCODGRP
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LGCODGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_LGCODGRP);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_LGCODGRP)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_LGCODGRP) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oLGCODGRP_2_6'),i_cWhere,'',"Gruppi utente",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LGCODGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_LGCODGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_LGCODGRP)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LGCODGRP = NVL(_Link_.CODE,0)
      this.w_DESCODGRP = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LGCODGRP = 0
      endif
      this.w_DESCODGRP = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LGCODGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LGPRGSTA
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
    i_lTable = "OUT_PUTS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2], .t., this.OUT_PUTS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LGPRGSTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MOU',True,'OUT_PUTS')
        if i_nConn<>0
          i_cWhere = " OUROWNUM="+cp_ToStrODBC(this.w_LGPRGSTA);
                   +" and OUNOMPRG="+cp_ToStrODBC(this.w_PRGSTA);

          i_ret=cp_SQL(i_nConn,"select OUNOMPRG,OUROWNUM,OUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OUNOMPRG',this.w_PRGSTA;
                     ,'OUROWNUM',this.w_LGPRGSTA)
          select OUNOMPRG,OUROWNUM,OUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_LGPRGSTA) and !this.bDontReportError
            deferred_cp_zoom('OUT_PUTS','*','OUNOMPRG,OUROWNUM',cp_AbsName(oSource.parent,'oLGPRGSTA_2_8'),i_cWhere,'GSUT_MOU',"Documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PRGSTA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMPRG,OUROWNUM,OUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select OUNOMPRG,OUROWNUM,OUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMPRG,OUROWNUM,OUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where OUROWNUM="+cp_ToStrODBC(oSource.xKey(2));
                     +" and OUNOMPRG="+cp_ToStrODBC(this.w_PRGSTA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUNOMPRG',oSource.xKey(1);
                       ,'OUROWNUM',oSource.xKey(2))
            select OUNOMPRG,OUROWNUM,OUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LGPRGSTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMPRG,OUROWNUM,OUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where OUROWNUM="+cp_ToStrODBC(this.w_LGPRGSTA);
                   +" and OUNOMPRG="+cp_ToStrODBC(this.w_PRGSTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUNOMPRG',this.w_PRGSTA;
                       ,'OUROWNUM',this.w_LGPRGSTA)
            select OUNOMPRG,OUROWNUM,OUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LGPRGSTA = NVL(_Link_.OUROWNUM,0)
      this.w_DESOUT = NVL(_Link_.OUDESCRI,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_LGPRGSTA = 0
      endif
      this.w_DESOUT = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])+'\'+cp_ToStr(_Link_.OUNOMPRG,1)+'\'+cp_ToStr(_Link_.OUROWNUM,1)
      cp_ShowWarn(i_cKey,this.OUT_PUTS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LGPRGSTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oLGDESCOD_2_10.value==this.w_LGDESCOD)
      this.oPgFrm.Page1.oPag.oLGDESCOD_2_10.value=this.w_LGDESCOD
      replace t_LGDESCOD with this.oPgFrm.Page1.oPag.oLGDESCOD_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIN_2_11.value==this.w_DESLIN)
      this.oPgFrm.Page1.oPag.oDESLIN_2_11.value=this.w_DESLIN
      replace t_DESLIN with this.oPgFrm.Page1.oPag.oDESLIN_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRPDEF_2_13.value==this.w_DESGRPDEF)
      this.oPgFrm.Page1.oPag.oDESGRPDEF_2_13.value=this.w_DESGRPDEF
      replace t_DESGRPDEF with this.oPgFrm.Page1.oPag.oDESGRPDEF_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCODUTE_2_14.value==this.w_DESCODUTE)
      this.oPgFrm.Page1.oPag.oDESCODUTE_2_14.value=this.w_DESCODUTE
      replace t_DESCODUTE with this.oPgFrm.Page1.oPag.oDESCODUTE_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCODGRP_2_15.value==this.w_DESCODGRP)
      this.oPgFrm.Page1.oPag.oDESCODGRP_2_15.value=this.w_DESCODGRP
      replace t_DESCODGRP with this.oPgFrm.Page1.oPag.oDESCODGRP_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLGSTMPOR_1_10.value==this.w_LGSTMPOR)
      this.oPgFrm.Page1.oPag.oLGSTMPOR_1_10.value=this.w_LGSTMPOR
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGTIPREP_2_2.RadioValue()==this.w_LGTIPREP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGTIPREP_2_2.SetRadio()
      replace t_LGTIPREP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGTIPREP_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGCODLIN_2_3.value==this.w_LGCODLIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGCODLIN_2_3.value=this.w_LGCODLIN
      replace t_LGCODLIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGCODLIN_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGGRPDEF_2_4.value==this.w_LGGRPDEF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGGRPDEF_2_4.value=this.w_LGGRPDEF
      replace t_LGGRPDEF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGGRPDEF_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGCODUTE_2_5.value==this.w_LGCODUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGCODUTE_2_5.value=this.w_LGCODUTE
      replace t_LGCODUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGCODUTE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGCODGRP_2_6.value==this.w_LGCODGRP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGCODGRP_2_6.value=this.w_LGCODGRP
      replace t_LGCODGRP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGCODGRP_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGPRGSTA_2_8.value==this.w_LGPRGSTA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGPRGSTA_2_8.value=this.w_LGPRGSTA
      replace t_LGPRGSTA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGPRGSTA_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGFLGOPT_2_9.RadioValue()==this.w_LGFLGOPT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGFLGOPT_2_9.SetRadio()
      replace t_LGFLGOPT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGFLGOPT_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESOUT_2_12.value==this.w_DESOUT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESOUT_2_12.value=this.w_DESOUT
      replace t_DESOUT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESOUT_2_12.value
    endif
    cp_SetControlsValueExtFlds(this,'REPO_DOC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if NOT EMPTY(.w_LGPRGSTA)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LGTIPREP = this.w_LGTIPREP
    this.o_LGPRGSTA = this.w_LGPRGSTA
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_LGPRGSTA))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_LGTIPREP=space(1)
      .w_LGCODLIN=space(3)
      .w_LGGRPDEF=space(5)
      .w_LGCODUTE=0
      .w_LGCODGRP=0
      .w_PRGSTA=space(30)
      .w_LGPRGSTA=0
      .w_LGFLGOPT=space(1)
      .w_LGDESCOD=space(35)
      .w_DESLIN=space(30)
      .w_DESOUT=space(100)
      .w_DESGRPDEF=space(40)
      .w_DESCODUTE=space(20)
      .w_DESCODGRP=space(20)
      .w_LG_SPLIT=space(254)
      .w_LG__LINK=space(254)
      .w_LGNO_RST=space(1)
      .w_LGFLPRSY=space(1)
      .DoRTCalc(1,5,.f.)
        .w_LGTIPREP = 'M'
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_LGCODLIN))
        .link_2_3('Full')
      endif
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_LGGRPDEF))
        .link_2_4('Full')
      endif
      .DoRTCalc(9,9,.f.)
      if not(empty(.w_LGCODUTE))
        .link_2_5('Full')
      endif
      .DoRTCalc(10,10,.f.)
      if not(empty(.w_LGCODGRP))
        .link_2_6('Full')
      endif
        .w_PRGSTA = IIF(.w_LGTIPREP='M', .w_PRGSTAPRI, IIF(.w_LGTIPREP='S', .w_PRGSTASEC, .w_PRGSTAPRO))
      .DoRTCalc(12,12,.f.)
      if not(empty(.w_LGPRGSTA))
        .link_2_8('Full')
      endif
        .w_LGFLGOPT = IIF(.w_LGTIPREP<>'M' AND .w_LGTIPREP<>'R', .w_LGFLGOPT, 'N')
      .DoRTCalc(14,23,.f.)
        .w_LGFLPRSY = 'N'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_LGTIPREP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGTIPREP_2_2.RadioValue(.t.)
    this.w_LGCODLIN = t_LGCODLIN
    this.w_LGGRPDEF = t_LGGRPDEF
    this.w_LGCODUTE = t_LGCODUTE
    this.w_LGCODGRP = t_LGCODGRP
    this.w_PRGSTA = t_PRGSTA
    this.w_LGPRGSTA = t_LGPRGSTA
    this.w_LGFLGOPT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGFLGOPT_2_9.RadioValue(.t.)
    this.w_LGDESCOD = t_LGDESCOD
    this.w_DESLIN = t_DESLIN
    this.w_DESOUT = t_DESOUT
    this.w_DESGRPDEF = t_DESGRPDEF
    this.w_DESCODUTE = t_DESCODUTE
    this.w_DESCODGRP = t_DESCODGRP
    this.w_LG_SPLIT = t_LG_SPLIT
    this.w_LG__LINK = t_LG__LINK
    this.w_LGNO_RST = t_LGNO_RST
    this.w_LGFLPRSY = t_LGFLPRSY
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_LGTIPREP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGTIPREP_2_2.ToRadio()
    replace t_LGCODLIN with this.w_LGCODLIN
    replace t_LGGRPDEF with this.w_LGGRPDEF
    replace t_LGCODUTE with this.w_LGCODUTE
    replace t_LGCODGRP with this.w_LGCODGRP
    replace t_PRGSTA with this.w_PRGSTA
    replace t_LGPRGSTA with this.w_LGPRGSTA
    replace t_LGFLGOPT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLGFLGOPT_2_9.ToRadio()
    replace t_LGDESCOD with this.w_LGDESCOD
    replace t_DESLIN with this.w_DESLIN
    replace t_DESOUT with this.w_DESOUT
    replace t_DESGRPDEF with this.w_DESGRPDEF
    replace t_DESCODUTE with this.w_DESCODUTE
    replace t_DESCODGRP with this.w_DESCODGRP
    replace t_LG_SPLIT with this.w_LG_SPLIT
    replace t_LG__LINK with this.w_LG__LINK
    replace t_LGNO_RST with this.w_LGNO_RST
    replace t_LGFLPRSY with this.w_LGFLPRSY
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsve_mtdPag1 as StdContainer
  Width  = 709
  height = 291
  stdWidth  = 709
  stdheight = 291
  resizeXpos=400
  resizeYpos=95
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLGSTMPOR_1_10 as StdField with uid="JTMLIYLNAA",rtseq=22,rtrep=.f.,;
    cFormVar = "w_LGSTMPOR", cQueryName = "LGSTMPOR",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso per ordinare le causali al momento della ristampa",;
    HelpContextID = 6244872,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=510, Top=240, InputMask=replicate('X',2)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=0, width=693,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="CPROWORD",Label1="Riga",Field2="LGTIPREP",Label2="Tipo report",Field3="LGCODLIN",Label3="Lingua",Field4="LGGRPDEF",Label4="Gr. output",Field5="LGCODUTE",Label5="Utente",Field6="LGCODGRP",Label6="Gruppo",Field7="LGPRGSTA",Label7="N.",Field8="DESOUT",Label8="Report di stampa",Field9="LGFLGOPT",Label9="Opz.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235730810

  add object oStr_1_5 as StdString with uid="FYHNLJJTNT",Visible=.t., Left=4, Top=194,;
    Alignment=1, Width=152, Height=18,;
    Caption="Descrizione lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="REXTWFHXES",Visible=.t., Left=11, Top=161,;
    Alignment=1, Width=145, Height=18,;
    Caption="Descrizione del report:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="IPUBEHJXCO",Visible=.t., Left=8, Top=217,;
    Alignment=1, Width=148, Height=18,;
    Caption="Gruppo output:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="ABAXECOJAV",Visible=.t., Left=22, Top=240,;
    Alignment=1, Width=134, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="KSHABCGLNH",Visible=.t., Left=22, Top=263,;
    Alignment=1, Width=134, Height=18,;
    Caption="Gruppo utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="HQSBCDUMHK",Visible=.t., Left=350, Top=240,;
    Alignment=1, Width=159, Height=18,;
    Caption="Ordine di ristampa:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=19,;
    width=689+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=20,width=688+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='LINGUE|GRP_DEFA|CPUSERS|CPGROUPS|OUT_PUTS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oLGDESCOD_2_10.Refresh()
      this.Parent.oDESLIN_2_11.Refresh()
      this.Parent.oDESGRPDEF_2_13.Refresh()
      this.Parent.oDESCODUTE_2_14.Refresh()
      this.Parent.oDESCODGRP_2_15.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='LINGUE'
        oDropInto=this.oBodyCol.oRow.oLGCODLIN_2_3
      case cFile='GRP_DEFA'
        oDropInto=this.oBodyCol.oRow.oLGGRPDEF_2_4
      case cFile='CPUSERS'
        oDropInto=this.oBodyCol.oRow.oLGCODUTE_2_5
      case cFile='CPGROUPS'
        oDropInto=this.oBodyCol.oRow.oLGCODGRP_2_6
      case cFile='OUT_PUTS'
        oDropInto=this.oBodyCol.oRow.oLGPRGSTA_2_8
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLGDESCOD_2_10 as StdTrsField with uid="YKRKYERQLP",rtseq=14,rtrep=.t.,;
    cFormVar="w_LGDESCOD",value=space(35),;
    ToolTipText = "Traduzione in lingua",;
    HelpContextID = 206611974,;
    cTotal="", bFixedPos=.t., cQueryName = "LGDESCOD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=158, Top=161, InputMask=replicate('X',35)

  func oLGDESCOD_2_10.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_LGCODLIN))
    endwith
  endfunc

  add object oDESLIN_2_11 as StdTrsField with uid="IEVNMGGGWS",rtseq=15,rtrep=.t.,;
    cFormVar="w_DESLIN",value=space(30),enabled=.f.,;
    ToolTipText = "Descrizione della lingua",;
    HelpContextID = 236406582,;
    cTotal="", bFixedPos=.t., cQueryName = "DESLIN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=158, Top=194, InputMask=replicate('X',30)

  add object oDESGRPDEF_2_13 as StdTrsField with uid="KMMKZYPFGY",rtseq=17,rtrep=.t.,;
    cFormVar="w_DESGRPDEF",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione gruppo di default intestatario",;
    HelpContextID = 10636251,;
    cTotal="", bFixedPos=.t., cQueryName = "DESGRPDEF",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=158, Top=217, InputMask=replicate('X',40)

  add object oDESCODUTE_2_14 as StdTrsField with uid="SMPTZINYGB",rtseq=18,rtrep=.t.,;
    cFormVar="w_DESCODUTE",value=space(20),enabled=.f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 74337242,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCODUTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=158, Top=240, InputMask=replicate('X',20)

  add object oDESCODGRP_2_15 as StdTrsField with uid="TMNCGYVMPG",rtseq=19,rtrep=.t.,;
    cFormVar="w_DESCODGRP",value=space(20),enabled=.f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 74337416,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCODGRP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=158, Top=263, InputMask=replicate('X',20)

  add object oBtn_2_19 as StdButton with uid="TFPOBKTNKS",width=48,height=45,;
   left=634, top=158,;
    CpPicture="BMP\AVANZATE.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alle impostazioni avanzate";
    , HelpContextID = 71981163;
    , Caption='\<Avanzate';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_19.Click()
      do GSAR_KLG with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsve_mtdBodyRow as CPBodyRowCnt
  Width=679
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="PPRILCWEUC",rtseq=5,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 268059242,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=36, Left=-2, Top=0

  add object oLGTIPREP_2_2 as StdTableComboah with uid="PNPZPHUDYE",rtrep=.t.,;
    cFormVar="w_LGTIPREP", RowSource=""+"Valori reali definiti in area manuale,"+"Valori reali definiti in area manuale,"+"Valori reali definiti in area manuale,"+"Valori reali definiti in area manuale" , ;
    HelpContextID = 42228230,;
    Height=21, Width=111, Left=37, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oLGTIPREP_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LGTIPREP,&i_cF..t_LGTIPREP),this.value)
    return(iif(xVal =1,'M',;
    iif(xVal =2,'S',;
    iif(xVal =3,'P',;
    iif(xVal =4,'R',;
    space(1))))))
  endfunc
  func oLGTIPREP_2_2.GetRadio()
    this.Parent.oContained.w_LGTIPREP = this.RadioValue()
    return .t.
  endfunc

  func oLGTIPREP_2_2.ToRadio()
    this.Parent.oContained.w_LGTIPREP=trim(this.Parent.oContained.w_LGTIPREP)
    return(;
      iif(this.Parent.oContained.w_LGTIPREP=='M',1,;
      iif(this.Parent.oContained.w_LGTIPREP=='S',2,;
      iif(this.Parent.oContained.w_LGTIPREP=='P',3,;
      iif(this.Parent.oContained.w_LGTIPREP=='R',4,;
      0)))))
  endfunc

  func oLGTIPREP_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oLGCODLIN_2_3 as StdTrsField with uid="CLQQCBEDEJ",rtseq=7,rtrep=.t.,;
    cFormVar="w_LGCODLIN",value=space(3),;
    ToolTipText = "Codice lingua",;
    HelpContextID = 70694396,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=61, Left=152, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_LGCODLIN"

  func oLGCODLIN_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oLGCODLIN_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLGCODLIN_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oLGCODLIN_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oLGCODLIN_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_LGCODLIN
    i_obj.ecpSave()
  endproc

  add object oLGGRPDEF_2_4 as StdTrsField with uid="SHOXHPGXIZ",rtseq=8,rtrep=.t.,;
    cFormVar="w_LGGRPDEF",value=space(5),;
    ToolTipText = "Gruppo output intestatari",;
    HelpContextID = 76319228,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=216, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRP_DEFA", cZoomOnZoom="GSAR_AGD", oKey_1_1="GDCODICE", oKey_1_2="this.w_LGGRPDEF"

  func oLGGRPDEF_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oLGGRPDEF_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLGGRPDEF_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRP_DEFA','*','GDCODICE',cp_AbsName(this.parent,'oLGGRPDEF_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGD',"Gruppi output",'',this.parent.oContained
  endproc
  proc oLGGRPDEF_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GDCODICE=this.parent.oContained.w_LGGRPDEF
    i_obj.ecpSave()
  endproc

  add object oLGCODUTE_2_5 as StdTrsField with uid="MBCKEVBUDL",rtseq=9,rtrep=.t.,;
    cFormVar="w_LGCODUTE",value=0,;
    HelpContextID = 80300539,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=47, Left=298, Top=0, bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_LGCODUTE"

  func oLGCODUTE_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oLGCODUTE_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLGCODUTE_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oLGCODUTE_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oLGCODGRP_2_6 as StdTrsField with uid="QDWHDKZCNY",rtseq=10,rtrep=.t.,;
    cFormVar="w_LGCODGRP",value=0,;
    HelpContextID = 113854982,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=348, Top=0, bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_LGCODGRP"

  func oLGCODGRP_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oLGCODGRP_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLGCODGRP_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oLGCODGRP_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi utente",'',this.parent.oContained
  endproc

  add object oLGPRGSTA_2_8 as StdTrsField with uid="JUBZIOBBDY",rtseq=12,rtrep=.t.,;
    cFormVar="w_LGPRGSTA",value=0,;
    ToolTipText = "Programma di stampa associato al documento",;
    HelpContextID = 50141687,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=42, Left=404, Top=0, cSayPict=["999"], cGetPict=["999"], bHasZoom = .t. , cLinkFile="OUT_PUTS", cZoomOnZoom="GSUT_MOU", oKey_1_1="OUNOMPRG", oKey_1_2="this.w_PRGSTA", oKey_2_1="OUROWNUM", oKey_2_2="this.w_LGPRGSTA"

  func oLGPRGSTA_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oLGPRGSTA_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLGPRGSTA_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.OUT_PUTS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"OUNOMPRG="+cp_ToStrODBC(this.Parent.oContained.w_PRGSTA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"OUNOMPRG="+cp_ToStr(this.Parent.oContained.w_PRGSTA)
    endif
    do cp_zoom with 'OUT_PUTS','*','OUNOMPRG,OUROWNUM',cp_AbsName(this.parent,'oLGPRGSTA_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MOU',"Documenti",'',this.parent.oContained
  endproc
  proc oLGPRGSTA_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MOU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.OUNOMPRG=w_PRGSTA
     i_obj.w_OUROWNUM=this.parent.oContained.w_LGPRGSTA
    i_obj.ecpSave()
  endproc

  add object oLGFLGOPT_2_9 as StdTrsCheck with uid="OYFNFXLXLO",rtrep=.t.,;
    cFormVar="w_LGFLGOPT",  caption="",;
    ToolTipText = "Report opzionale",;
    HelpContextID = 251034122,;
    Left=659, Top=0, Width=15,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , autosize=.f.;
   , bGlobalFont=.t.


  func oLGFLGOPT_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LGFLGOPT,&i_cF..t_LGFLGOPT),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oLGFLGOPT_2_9.GetRadio()
    this.Parent.oContained.w_LGFLGOPT = this.RadioValue()
    return .t.
  endfunc

  func oLGFLGOPT_2_9.ToRadio()
    this.Parent.oContained.w_LGFLGOPT=trim(this.Parent.oContained.w_LGFLGOPT)
    return(;
      iif(this.Parent.oContained.w_LGFLGOPT=='S',1,;
      0))
  endfunc

  func oLGFLGOPT_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oLGFLGOPT_2_9.mCond()
    with this.Parent.oContained
      return (.w_LGTIPREP<>'M' AND .w_LGTIPREP<>'R')
    endwith
  endfunc

  add object oDESOUT_2_12 as StdTrsField with uid="QPIKSKPJZG",rtseq=16,rtrep=.t.,;
    cFormVar="w_DESOUT",value=space(100),enabled=.f.,;
    HelpContextID = 81413942,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=206, Left=449, Top=0, InputMask=replicate('X',100)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_mtd','REPO_DOC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LGCODICE=REPO_DOC.LGCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsve_mtd
Define Class StdTableComboah As StdTableCombo
	* Ridefinita classe per impostare i valori in funzione del prodotto
	Proc Init
		* --- Zucchetti Aulla - Inizio Interfaccia
		this.specialeffect = i_nSpecialEffect
		this.BorderColor = i_nBorderColor
		If Vartype(This.bNoBackColor)='U'
			This.BackColor=i_EBackColor
		Endif
		* --- Zucchetti Aulla - Fine Interfaccia
		This.ToolTipText=cp_Translate(This.ToolTipText)
		this.StatusBarText=Padr(this.ToolTipText,100)
		Dimension This.combovalues(IIF(IsAlt(),2,4))
	  This.combovalues(1)= 'M'
		This.AddItem(ah_msgformat('Principale'))
    This.combovalues(2)= 'S'
		This.AddItem(ah_msgformat('Secondario'))
    If isAhr()
      This.combovalues(3)= 'P'
	  	This.AddItem(ah_msgformat('Produzione'))
      This.combovalues(4)= 'R'
		  This.AddItem(ah_msgformat('Produzione principale'))
    EndIf
  	This.nValues = IIF(VARTYPE(This.combovalues)='L', 0, Alen(This.combovalues))
		This.SetFont()
	Endproc
Enddefine
* --- Fine Area Manuale
