* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_a10                                                        *
*              R.F. per causale documento                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_48]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-29                                                      *
* Last revis.: 2008-10-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_a10"))

* --- Class definition
define class tgsar_a10 as StdForm
  Top    = 72
  Left   = 34

  * --- Standard Properties
  Width  = 575
  Height = 256+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-09"
  HelpContextID=125208727
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  RIFI_CADO_IDX = 0
  CONTI_IDX = 0
  CACOCLFO_IDX = 0
  PAG_AMEN_IDX = 0
  CAU_CONT_IDX = 0
  TIP_DOCU_IDX = 0
  cFile = "RIFI_CADO"
  cKeySelect = "RFCAUDOC"
  cKeyWhere  = "RFCAUDOC=this.w_RFCAUDOC"
  cKeyWhereODBC = '"RFCAUDOC="+cp_ToStrODBC(this.w_RFCAUDOC)';

  cKeyWhereODBCqualified = '"RIFI_CADO.RFCAUDOC="+cp_ToStrODBC(this.w_RFCAUDOC)';

  cPrg = "gsar_a10"
  cComment = "R.F. per causale documento"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RFCAUDOC = space(5)
  w_RFCONCLI = space(15)
  w_RFCONCAF = space(15)
  w_RFCONABF = space(15)
  w_RFCONIVF = space(15)
  w_RFCATCLG = space(5)
  w_RFPAGRIC = space(5)
  w_CATDOC = space(2)
  w_DESCVE = space(40)
  w_DESCCAF = space(40)
  w_DESCABF = space(40)
  w_DESCIVF = space(40)
  w_DESCAT = space(35)
  w_DESPAG = space(30)
  w_RAGAZI = space(40)
  w_RFTIPCON = space(1)
  w_PERIVB = 0
  w_TIPART = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESCCAU = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RIFI_CADO','gsar_a10')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_a10Pag1","gsar_a10",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("R.F. per causale documento")
      .Pages(1).HelpContextID = 149987685
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRFCAUDOC_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CACOCLFO'
    this.cWorkTables[3]='PAG_AMEN'
    this.cWorkTables[4]='CAU_CONT'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='RIFI_CADO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RIFI_CADO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RIFI_CADO_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RFCAUDOC = NVL(RFCAUDOC,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_3_joined
    link_1_3_joined=.f.
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_5_joined
    link_1_5_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_1_7_joined
    link_1_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from RIFI_CADO where RFCAUDOC=KeySet.RFCAUDOC
    *
    i_nConn = i_TableProp[this.RIFI_CADO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIFI_CADO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RIFI_CADO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RIFI_CADO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RIFI_CADO '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RFCAUDOC',this.w_RFCAUDOC  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CATDOC = 'RF'
        .w_DESCVE = space(40)
        .w_DESCCAF = space(40)
        .w_DESCABF = space(40)
        .w_DESCIVF = space(40)
        .w_DESCAT = space(35)
        .w_DESPAG = space(30)
        .w_RAGAZI = space(40)
        .w_PERIVB = 0
        .w_TIPART = space(1)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESCCAU = space(40)
        .w_RFCAUDOC = NVL(RFCAUDOC,space(5))
          .link_1_1('Load')
        .w_RFCONCLI = NVL(RFCONCLI,space(15))
          if link_1_2_joined
            this.w_RFCONCLI = NVL(ANCODICE102,NVL(this.w_RFCONCLI,space(15)))
            this.w_DESCVE = NVL(ANDESCRI102,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO102),ctod("  /  /  "))
          else
          .link_1_2('Load')
          endif
        .w_RFCONCAF = NVL(RFCONCAF,space(15))
          if link_1_3_joined
            this.w_RFCONCAF = NVL(ANCODICE103,NVL(this.w_RFCONCAF,space(15)))
            this.w_DESCCAF = NVL(ANDESCRI103,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO103),ctod("  /  /  "))
          else
          .link_1_3('Load')
          endif
        .w_RFCONABF = NVL(RFCONABF,space(15))
          if link_1_4_joined
            this.w_RFCONABF = NVL(ANCODICE104,NVL(this.w_RFCONABF,space(15)))
            this.w_DESCABF = NVL(ANDESCRI104,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO104),ctod("  /  /  "))
          else
          .link_1_4('Load')
          endif
        .w_RFCONIVF = NVL(RFCONIVF,space(15))
          if link_1_5_joined
            this.w_RFCONIVF = NVL(ANCODICE105,NVL(this.w_RFCONIVF,space(15)))
            this.w_DESCIVF = NVL(ANDESCRI105,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO105),ctod("  /  /  "))
          else
          .link_1_5('Load')
          endif
        .w_RFCATCLG = NVL(RFCATCLG,space(5))
          if link_1_6_joined
            this.w_RFCATCLG = NVL(C2CODICE106,NVL(this.w_RFCATCLG,space(5)))
            this.w_DESCAT = NVL(C2DESCRI106,space(35))
          else
          .link_1_6('Load')
          endif
        .w_RFPAGRIC = NVL(RFPAGRIC,space(5))
          if link_1_7_joined
            this.w_RFPAGRIC = NVL(PACODICE107,NVL(this.w_RFPAGRIC,space(5)))
            this.w_DESPAG = NVL(PADESCRI107,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(PADTOBSO107),ctod("  /  /  "))
          else
          .link_1_7('Load')
          endif
        .w_RFTIPCON = NVL(RFTIPCON,space(1))
        cp_LoadRecExtFlds(this,'RIFI_CADO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RFCAUDOC = space(5)
      .w_RFCONCLI = space(15)
      .w_RFCONCAF = space(15)
      .w_RFCONABF = space(15)
      .w_RFCONIVF = space(15)
      .w_RFCATCLG = space(5)
      .w_RFPAGRIC = space(5)
      .w_CATDOC = space(2)
      .w_DESCVE = space(40)
      .w_DESCCAF = space(40)
      .w_DESCABF = space(40)
      .w_DESCIVF = space(40)
      .w_DESCAT = space(35)
      .w_DESPAG = space(30)
      .w_RAGAZI = space(40)
      .w_RFTIPCON = space(1)
      .w_PERIVB = 0
      .w_TIPART = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DESCCAU = space(40)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_RFCAUDOC))
          .link_1_1('Full')
          endif
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_RFCONCLI))
          .link_1_2('Full')
          endif
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_RFCONCAF))
          .link_1_3('Full')
          endif
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_RFCONABF))
          .link_1_4('Full')
          endif
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_RFCONIVF))
          .link_1_5('Full')
          endif
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_RFCATCLG))
          .link_1_6('Full')
          endif
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_RFPAGRIC))
          .link_1_7('Full')
          endif
        .w_CATDOC = 'RF'
          .DoRTCalc(9,15,.f.)
        .w_RFTIPCON = 'G'
          .DoRTCalc(17,18,.f.)
        .w_OBTEST = i_datsys
      endif
    endwith
    cp_BlankRecExtFlds(this,'RIFI_CADO')
    this.DoRTCalc(20,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRFCAUDOC_1_1.enabled = i_bVal
      .Page1.oPag.oRFCONCLI_1_2.enabled = i_bVal
      .Page1.oPag.oRFCONCAF_1_3.enabled = i_bVal
      .Page1.oPag.oRFCONABF_1_4.enabled = i_bVal
      .Page1.oPag.oRFCONIVF_1_5.enabled = i_bVal
      .Page1.oPag.oRFCATCLG_1_6.enabled = i_bVal
      .Page1.oPag.oRFPAGRIC_1_7.enabled = i_bVal
      .Page1.oPag.oBtn_1_29.enabled = i_bVal
      .Page1.oPag.oBtn_1_30.enabled = .Page1.oPag.oBtn_1_30.mCond()
      if i_cOp = "Edit"
        .Page1.oPag.oRFCAUDOC_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRFCAUDOC_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'RIFI_CADO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RIFI_CADO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFCAUDOC,"RFCAUDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFCONCLI,"RFCONCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFCONCAF,"RFCONCAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFCONABF,"RFCONABF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFCONIVF,"RFCONIVF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFCATCLG,"RFCATCLG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFPAGRIC,"RFPAGRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RFTIPCON,"RFTIPCON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RIFI_CADO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIFI_CADO_IDX,2])
    i_lTable = "RIFI_CADO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RIFI_CADO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RIFI_CADO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIFI_CADO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.RIFI_CADO_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RIFI_CADO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RIFI_CADO')
        i_extval=cp_InsertValODBCExtFlds(this,'RIFI_CADO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RFCAUDOC,RFCONCLI,RFCONCAF,RFCONABF,RFCONIVF"+;
                  ",RFCATCLG,RFPAGRIC,RFTIPCON "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_RFCAUDOC)+;
                  ","+cp_ToStrODBCNull(this.w_RFCONCLI)+;
                  ","+cp_ToStrODBCNull(this.w_RFCONCAF)+;
                  ","+cp_ToStrODBCNull(this.w_RFCONABF)+;
                  ","+cp_ToStrODBCNull(this.w_RFCONIVF)+;
                  ","+cp_ToStrODBCNull(this.w_RFCATCLG)+;
                  ","+cp_ToStrODBCNull(this.w_RFPAGRIC)+;
                  ","+cp_ToStrODBC(this.w_RFTIPCON)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RIFI_CADO')
        i_extval=cp_InsertValVFPExtFlds(this,'RIFI_CADO')
        cp_CheckDeletedKey(i_cTable,0,'RFCAUDOC',this.w_RFCAUDOC)
        INSERT INTO (i_cTable);
              (RFCAUDOC,RFCONCLI,RFCONCAF,RFCONABF,RFCONIVF,RFCATCLG,RFPAGRIC,RFTIPCON  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RFCAUDOC;
                  ,this.w_RFCONCLI;
                  ,this.w_RFCONCAF;
                  ,this.w_RFCONABF;
                  ,this.w_RFCONIVF;
                  ,this.w_RFCATCLG;
                  ,this.w_RFPAGRIC;
                  ,this.w_RFTIPCON;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.RIFI_CADO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIFI_CADO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.RIFI_CADO_IDX,i_nConn)
      *
      * update RIFI_CADO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'RIFI_CADO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RFCONCLI="+cp_ToStrODBCNull(this.w_RFCONCLI)+;
             ",RFCONCAF="+cp_ToStrODBCNull(this.w_RFCONCAF)+;
             ",RFCONABF="+cp_ToStrODBCNull(this.w_RFCONABF)+;
             ",RFCONIVF="+cp_ToStrODBCNull(this.w_RFCONIVF)+;
             ",RFCATCLG="+cp_ToStrODBCNull(this.w_RFCATCLG)+;
             ",RFPAGRIC="+cp_ToStrODBCNull(this.w_RFPAGRIC)+;
             ",RFTIPCON="+cp_ToStrODBC(this.w_RFTIPCON)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'RIFI_CADO')
        i_cWhere = cp_PKFox(i_cTable  ,'RFCAUDOC',this.w_RFCAUDOC  )
        UPDATE (i_cTable) SET;
              RFCONCLI=this.w_RFCONCLI;
             ,RFCONCAF=this.w_RFCONCAF;
             ,RFCONABF=this.w_RFCONABF;
             ,RFCONIVF=this.w_RFCONIVF;
             ,RFCATCLG=this.w_RFCATCLG;
             ,RFPAGRIC=this.w_RFPAGRIC;
             ,RFTIPCON=this.w_RFTIPCON;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RIFI_CADO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIFI_CADO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.RIFI_CADO_IDX,i_nConn)
      *
      * delete RIFI_CADO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RFCAUDOC',this.w_RFCAUDOC  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RIFI_CADO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIFI_CADO_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,15,.t.)
            .w_RFTIPCON = 'G'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RFCAUDOC
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RFCAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_RFCAUDOC)+"%");
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_CATDOC);

          i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDCATDOC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDCATDOC',this.w_CATDOC;
                     ,'TDTIPDOC',trim(this.w_RFCAUDOC))
          select TDCATDOC,TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDCATDOC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RFCAUDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RFCAUDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDCATDOC,TDTIPDOC',cp_AbsName(oSource.parent,'oRFCAUDOC_1_1'),i_cWhere,'GSVE_ATD',"Causali documenti di vendita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CATDOC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TDCATDOC="+cp_ToStrODBC(this.w_CATDOC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDCATDOC',oSource.xKey(1);
                       ,'TDTIPDOC',oSource.xKey(2))
            select TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RFCAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_RFCAUDOC);
                   +" and TDCATDOC="+cp_ToStrODBC(this.w_CATDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDCATDOC',this.w_CATDOC;
                       ,'TDTIPDOC',this.w_RFCAUDOC)
            select TDCATDOC,TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RFCAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCCAU = NVL(_Link_.TDDESDOC,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RFCAUDOC = space(5)
      endif
      this.w_DESCCAU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDCATDOC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RFCAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RFCONCLI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RFCONCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_RFCONCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_RFTIPCON;
                     ,'ANCODICE',trim(this.w_RFCONCLI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RFCONCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_RFCONCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_RFCONCLI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_RFTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RFCONCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oRFCONCLI_1_2'),i_cWhere,'GSAR_BZC',"Conti contropartita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_RFTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RFCONCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_RFCONCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_RFTIPCON;
                       ,'ANCODICE',this.w_RFCONCLI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RFCONCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCVE = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RFCONCLI = space(15)
      endif
      this.w_DESCVE = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_RFCONCLI = space(15)
        this.w_DESCVE = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RFCONCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.ANCODICE as ANCODICE102"+ ",link_1_2.ANDESCRI as ANDESCRI102"+ ",link_1_2.ANDTOBSO as ANDTOBSO102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on RIFI_CADO.RFCONCLI=link_1_2.ANCODICE"+" and RIFI_CADO.RFTIPCON=link_1_2.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and RIFI_CADO.RFCONCLI=link_1_2.ANCODICE(+)"'+'+" and RIFI_CADO.RFTIPCON=link_1_2.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RFCONCAF
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RFCONCAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_RFCONCAF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_RFTIPCON;
                     ,'ANCODICE',trim(this.w_RFCONCAF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RFCONCAF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_RFCONCAF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_RFCONCAF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_RFTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RFCONCAF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oRFCONCAF_1_3'),i_cWhere,'GSAR_BZC',"Conti contropartita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_RFTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RFCONCAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_RFCONCAF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_RFTIPCON;
                       ,'ANCODICE',this.w_RFCONCAF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RFCONCAF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCCAF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RFCONCAF = space(15)
      endif
      this.w_DESCCAF = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_RFCONCAF = space(15)
        this.w_DESCCAF = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RFCONCAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.ANCODICE as ANCODICE103"+ ",link_1_3.ANDESCRI as ANDESCRI103"+ ",link_1_3.ANDTOBSO as ANDTOBSO103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on RIFI_CADO.RFCONCAF=link_1_3.ANCODICE"+" and RIFI_CADO.RFTIPCON=link_1_3.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and RIFI_CADO.RFCONCAF=link_1_3.ANCODICE(+)"'+'+" and RIFI_CADO.RFTIPCON=link_1_3.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RFCONABF
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RFCONABF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_RFCONABF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_RFTIPCON;
                     ,'ANCODICE',trim(this.w_RFCONABF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RFCONABF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_RFCONABF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_RFCONABF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_RFTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RFCONABF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oRFCONABF_1_4'),i_cWhere,'GSAR_BZC',"Conti contropartita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_RFTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RFCONABF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_RFCONABF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_RFTIPCON;
                       ,'ANCODICE',this.w_RFCONABF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RFCONABF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCABF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RFCONABF = space(15)
      endif
      this.w_DESCABF = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_RFCONABF = space(15)
        this.w_DESCABF = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RFCONABF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.ANCODICE as ANCODICE104"+ ",link_1_4.ANDESCRI as ANDESCRI104"+ ",link_1_4.ANDTOBSO as ANDTOBSO104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on RIFI_CADO.RFCONABF=link_1_4.ANCODICE"+" and RIFI_CADO.RFTIPCON=link_1_4.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and RIFI_CADO.RFCONABF=link_1_4.ANCODICE(+)"'+'+" and RIFI_CADO.RFTIPCON=link_1_4.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RFCONIVF
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RFCONIVF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_RFCONIVF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_RFTIPCON;
                     ,'ANCODICE',trim(this.w_RFCONIVF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RFCONIVF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_RFCONIVF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_RFCONIVF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_RFTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RFCONIVF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oRFCONIVF_1_5'),i_cWhere,'GSAR_BZC',"Conti contropartita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_RFTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RFCONIVF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_RFCONIVF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RFTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_RFTIPCON;
                       ,'ANCODICE',this.w_RFCONIVF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RFCONIVF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCIVF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RFCONIVF = space(15)
      endif
      this.w_DESCIVF = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_RFCONIVF = space(15)
        this.w_DESCIVF = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RFCONIVF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.ANCODICE as ANCODICE105"+ ",link_1_5.ANDESCRI as ANDESCRI105"+ ",link_1_5.ANDTOBSO as ANDTOBSO105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on RIFI_CADO.RFCONIVF=link_1_5.ANCODICE"+" and RIFI_CADO.RFTIPCON=link_1_5.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and RIFI_CADO.RFCONIVF=link_1_5.ANCODICE(+)"'+'+" and RIFI_CADO.RFTIPCON=link_1_5.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RFCATCLG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RFCATCLG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_RFCATCLG)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_RFCATCLG))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RFCATCLG)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RFCATCLG) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oRFCATCLG_1_6'),i_cWhere,'GSAR_AC2',"Categorie contabili clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RFCATCLG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_RFCATCLG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_RFCATCLG)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RFCATCLG = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCAT = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_RFCATCLG = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RFCATCLG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOCLFO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.C2CODICE as C2CODICE106"+ ",link_1_6.C2DESCRI as C2DESCRI106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on RIFI_CADO.RFCATCLG=link_1_6.C2CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and RIFI_CADO.RFCATCLG=link_1_6.C2CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RFPAGRIC
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RFPAGRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_RFPAGRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_RFPAGRIC))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RFPAGRIC)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_RFPAGRIC)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_RFPAGRIC))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_RFPAGRIC)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RFPAGRIC) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oRFPAGRIC_1_7'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RFPAGRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_RFPAGRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_RFPAGRIC)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RFPAGRIC = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RFPAGRIC = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_RFPAGRIC) OR LEFT(CHTIPPAG(.w_RFPAGRIC, 'RD'),2)='RD') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo pagamento inesistente, non rimessa diretta oppure obsoleto")
        endif
        this.w_RFPAGRIC = space(5)
        this.w_DESPAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RFPAGRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.PACODICE as PACODICE107"+ ","+cp_TransLinkFldName('link_1_7.PADESCRI')+" as PADESCRI107"+ ",link_1_7.PADTOBSO as PADTOBSO107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on RIFI_CADO.RFPAGRIC=link_1_7.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and RIFI_CADO.RFPAGRIC=link_1_7.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRFCAUDOC_1_1.value==this.w_RFCAUDOC)
      this.oPgFrm.Page1.oPag.oRFCAUDOC_1_1.value=this.w_RFCAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oRFCONCLI_1_2.value==this.w_RFCONCLI)
      this.oPgFrm.Page1.oPag.oRFCONCLI_1_2.value=this.w_RFCONCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oRFCONCAF_1_3.value==this.w_RFCONCAF)
      this.oPgFrm.Page1.oPag.oRFCONCAF_1_3.value=this.w_RFCONCAF
    endif
    if not(this.oPgFrm.Page1.oPag.oRFCONABF_1_4.value==this.w_RFCONABF)
      this.oPgFrm.Page1.oPag.oRFCONABF_1_4.value=this.w_RFCONABF
    endif
    if not(this.oPgFrm.Page1.oPag.oRFCONIVF_1_5.value==this.w_RFCONIVF)
      this.oPgFrm.Page1.oPag.oRFCONIVF_1_5.value=this.w_RFCONIVF
    endif
    if not(this.oPgFrm.Page1.oPag.oRFCATCLG_1_6.value==this.w_RFCATCLG)
      this.oPgFrm.Page1.oPag.oRFCATCLG_1_6.value=this.w_RFCATCLG
    endif
    if not(this.oPgFrm.Page1.oPag.oRFPAGRIC_1_7.value==this.w_RFPAGRIC)
      this.oPgFrm.Page1.oPag.oRFPAGRIC_1_7.value=this.w_RFPAGRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCVE_1_9.value==this.w_DESCVE)
      this.oPgFrm.Page1.oPag.oDESCVE_1_9.value=this.w_DESCVE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCAF_1_10.value==this.w_DESCCAF)
      this.oPgFrm.Page1.oPag.oDESCCAF_1_10.value=this.w_DESCCAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCABF_1_11.value==this.w_DESCABF)
      this.oPgFrm.Page1.oPag.oDESCABF_1_11.value=this.w_DESCABF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCIVF_1_12.value==this.w_DESCIVF)
      this.oPgFrm.Page1.oPag.oDESCIVF_1_12.value=this.w_DESCIVF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_13.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_13.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_14.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_14.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCAU_1_28.value==this.w_DESCCAU)
      this.oPgFrm.Page1.oPag.oDESCCAU_1_28.value=this.w_DESCCAU
    endif
    cp_SetControlsValueExtFlds(this,'RIFI_CADO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_RFCAUDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRFCAUDOC_1_1.SetFocus()
            i_bnoObbl = !empty(.w_RFCAUDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_RFCONCLI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRFCONCLI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_RFCONCAF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRFCONCAF_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_RFCONABF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRFCONABF_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_RFCONIVF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRFCONIVF_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not((EMPTY(.w_RFPAGRIC) OR LEFT(CHTIPPAG(.w_RFPAGRIC, 'RD'),2)='RD') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_RFPAGRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRFPAGRIC_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo pagamento inesistente, non rimessa diretta oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_a10Pag1 as StdContainer
  Width  = 571
  height = 256
  stdWidth  = 571
  stdheight = 256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRFCAUDOC_1_1 as StdField with uid="UJSCLODTWT",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RFCAUDOC", cQueryName = "RFCAUDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Cod.causale del documento",;
    HelpContextID = 17564761,;
   bGlobalFont=.t.,;
    Height=21, Width=133, Left=150, Top=11, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDCATDOC", oKey_1_2="this.w_CATDOC", oKey_2_1="TDTIPDOC", oKey_2_2="this.w_RFCAUDOC"

  func oRFCAUDOC_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oRFCAUDOC_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRFCAUDOC_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStrODBC(this.Parent.oContained.w_CATDOC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDCATDOC="+cp_ToStr(this.Parent.oContained.w_CATDOC)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDCATDOC,TDTIPDOC',cp_AbsName(this.parent,'oRFCAUDOC_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti di vendita",'',this.parent.oContained
  endproc
  proc oRFCAUDOC_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDCATDOC=w_CATDOC
     i_obj.w_TDTIPDOC=this.parent.oContained.w_RFCAUDOC
     i_obj.ecpSave()
  endproc

  add object oRFCONCLI_1_2 as StdField with uid="QRBRBFCQCU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RFCONCLI", cQueryName = "RFCONCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto su cui imputare i crediti v/clienti per R.F. non incassate",;
    HelpContextID = 262800479,;
   bGlobalFont=.t.,;
    Height=21, Width=133, Left=150, Top=45, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_RFTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_RFCONCLI"

  func oRFCONCLI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oRFCONCLI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRFCONCLI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_RFTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_RFTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oRFCONCLI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'',this.parent.oContained
  endproc
  proc oRFCONCLI_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_RFTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_RFCONCLI
     i_obj.ecpSave()
  endproc

  add object oRFCONCAF_1_3 as StdField with uid="MHIKSKGTLF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RFCONCAF", cQueryName = "RFCONCAF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto cassa o banca per contabilizzare gli incassi",;
    HelpContextID = 5634980,;
   bGlobalFont=.t.,;
    Height=21, Width=133, Left=150, Top=73, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_RFTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_RFCONCAF"

  func oRFCONCAF_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oRFCONCAF_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRFCONCAF_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_RFTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_RFTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oRFCONCAF_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'',this.parent.oContained
  endproc
  proc oRFCONCAF_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_RFTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_RFCONCAF
     i_obj.ecpSave()
  endproc

  add object oRFCONABF_1_4 as StdField with uid="VLEMRWPDHK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RFCONABF", cQueryName = "RFCONABF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto di contabilizzazione degli abbuoni derivanti da corrispettivi incassati",;
    HelpContextID = 39189412,;
   bGlobalFont=.t.,;
    Height=21, Width=133, Left=150, Top=101, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_RFTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_RFCONABF"

  func oRFCONABF_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oRFCONABF_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRFCONABF_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_RFTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_RFTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oRFCONABF_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'',this.parent.oContained
  endproc
  proc oRFCONABF_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_RFTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_RFCONABF
     i_obj.ecpSave()
  endproc

  add object oRFCONIVF_1_5 as StdField with uid="AFJSLFBBWE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RFCONIVF", cQueryName = "RFCONIVF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto di contabilizzazione dell'IVA sospesa per R.F. di servizi non incassati",;
    HelpContextID = 173407140,;
   bGlobalFont=.t.,;
    Height=21, Width=133, Left=150, Top=129, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_RFTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_RFCONIVF"

  func oRFCONIVF_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oRFCONIVF_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRFCONIVF_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_RFTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_RFTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oRFCONIVF_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'',this.parent.oContained
  endproc
  proc oRFCONIVF_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_RFTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_RFCONIVF
     i_obj.ecpSave()
  endproc

  add object oRFCATCLG_1_6 as StdField with uid="FIBPWVACDW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_RFCATCLG", cQueryName = "RFCATCLG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria contabile relativa al conto crediti v. clienti",;
    HelpContextID = 268174429,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=150, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_RFCATCLG"

  func oRFCATCLG_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oRFCATCLG_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRFCATCLG_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oRFCATCLG_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili clienti",'',this.parent.oContained
  endproc
  proc oRFCATCLG_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_RFCATCLG
     i_obj.ecpSave()
  endproc

  add object oRFPAGRIC_1_7 as StdField with uid="KVQLYKRTKI",rtseq=7,rtrep=.f.,;
    cFormVar = "w_RFPAGRIC", cQueryName = "RFPAGRIC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo pagamento inesistente, non rimessa diretta oppure obsoleto",;
    ToolTipText = "Codice di pagamento di default per R.F. non incassata",;
    HelpContextID = 237818969,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=150, Top=185, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_RFPAGRIC"

  func oRFPAGRIC_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oRFPAGRIC_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRFPAGRIC_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oRFPAGRIC_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oRFPAGRIC_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_RFPAGRIC
     i_obj.ecpSave()
  endproc

  add object oDESCVE_1_9 as StdField with uid="UDNVRJCMOA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCVE", cQueryName = "DESCVE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 232848842,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=285, Top=45, InputMask=replicate('X',40)

  add object oDESCCAF_1_10 as StdField with uid="YLKJPWABUI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCCAF", cQueryName = "DESCCAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 51445194,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=285, Top=73, InputMask=replicate('X',40)

  add object oDESCABF_1_11 as StdField with uid="FJKQAIOYOO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCABF", cQueryName = "DESCABF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 36765130,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=285, Top=101, InputMask=replicate('X',40)

  add object oDESCIVF_1_12 as StdField with uid="DUPKYZIKCF",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCIVF", cQueryName = "DESCIVF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 229703114,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=285, Top=129, InputMask=replicate('X',40)

  add object oDESCAT_1_13 as StdField with uid="PIJSEWCNAI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 3210698,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=285, Top=157, InputMask=replicate('X',35)

  add object oDESPAG_1_14 as StdField with uid="VHEWVUUFXX",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 220462538,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=285, Top=185, InputMask=replicate('X',30)

  add object oDESCCAU_1_28 as StdField with uid="OANKGEOEUM",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCCAU", cQueryName = "DESCCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 51445194,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=285, Top=11, InputMask=replicate('X',40)


  add object oBtn_1_29 as StdButton with uid="JQRWVWKJKA",left=466, top=211, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 125237478;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_30 as StdButton with uid="NTQGCTDVWT",left=518, top=211, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per abbandonare";
    , HelpContextID = 132526150;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_15 as StdString with uid="VCBKXDRQEO",Visible=.t., Left=5, Top=45,;
    Alignment=1, Width=144, Height=15,;
    Caption="Crediti v/clienti"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="QNDKVXONGE",Visible=.t., Left=5, Top=185,;
    Alignment=1, Width=144, Height=15,;
    Caption="Pagam.ric.fiscali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="NXEQEUVLIB",Visible=.t., Left=5, Top=73,;
    Alignment=1, Width=144, Height=15,;
    Caption="Incasso corrispettivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="LKMMUMJLRL",Visible=.t., Left=5, Top=101,;
    Alignment=1, Width=144, Height=15,;
    Caption="Abbuoni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="YFPOOEOAJC",Visible=.t., Left=5, Top=129,;
    Alignment=1, Width=144, Height=15,;
    Caption="IVA sospesa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="BCXYYVTXLK",Visible=.t., Left=5, Top=157,;
    Alignment=1, Width=144, Height=15,;
    Caption="Cat.contabile clienti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="BADEFWKBRI",Visible=.t., Left=5, Top=13,;
    Alignment=1, Width=144, Height=18,;
    Caption="Cod.causale doc.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_a10','RIFI_CADO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RFCAUDOC=RIFI_CADO.RFCAUDOC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
