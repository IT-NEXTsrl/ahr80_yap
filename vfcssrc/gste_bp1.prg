* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bp1                                                        *
*              Zoom su partite aperte da scadenze diverse                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_69]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-06                                                      *
* Last revis.: 2013-10-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bp1",oParentObject,m.pEXEC)
return(i_retval)

define class tgste_bp1 as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_PTFLSOSP = space(1)
  w_DATSCA = ctod("  /  /  ")
  w_NUMPAR = space(31)
  w_TOTIMP = 0
  w_MODPAG = space(10)
  w_BANAPP = space(10)
  w_BANNOS = space(15)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODVAL = space(5)
  w_CAOVAL = 0
  w_CAOAPE = 0
  w_DATAPE = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_SEGNO = space(1)
  w_DESRIG = space(50)
  w_SERRIF = space(10)
  w_ORDRIF = 0
  w_NUMRIF = 0
  w_LDATSCA = ctod("  /  /  ")
  w_LNUMPAR = space(31)
  w_LTIPCON = space(1)
  w_LCODCON = space(15)
  w_LMODPAG = space(5)
  w_LCODVAL = space(5)
  w_LTOTIMP = 0
  w_LBANNOS = space(15)
  w_LBANAPP = space(10)
  w_LNUMDOC = 0
  w_LDATDOC = ctod("  /  /  ")
  w_LDESRIG = space(50)
  w_LIMPDOC = 0
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_LSEGNO = space(1)
  w_ISALT = .f.
  w_O_PTMODPAG = space(10)
  w_O_PTBANAPP = space(10)
  w_O_PTBANNOS = space(15)
  w_LFLSOSP = space(1)
  w_LCAOVAL = space(1)
  w_PADRE = .NULL.
  w_SERDOC = space(10)
  * --- WorkFile variables
  TMP_PART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue lo Zoom sulle Partite Aperte (da GSTE_MPA)
    * --- S salda da bottone salda
    *     A salda in automatica da bottone effetti
    * --- Esegue lo zoom solo in modalit� 'Saldo', quando cio� w_SCTIPCLF � uguale a 'Cliente' e w_SCTIPSCA � uguale a 'Passiva' 
    *     o quando w_SCTIPCLF � uguale a 'Fornitore' e w_SCTIPSCA � uguale a 'Attiva'
    this.w_ISALT = Isalt()
    WITH this.oParentObject.oParentObject
    if (Not this.w_Isalt and g_COGE<>"S") OR (.w_SCTIPCLF="G" AND .w_PFLCRSA="C") OR (.w_SCTIPCLF="C" AND .w_PFLCRSA="C") OR (.w_SCTIPCLF="F" AND .w_PFLCRSA="C")
      Ah_ErrorMsg("Funzionalit� non consentita!",48,"")
      i_retcode = 'stop'
      return
    endif
    ENDWITH
    this.w_NUMPAR = SPACE(31)
    this.oParentObject.w_PTFLCRSA = "S"
    WITH this.oParentObject.oParentObject
    if EMPTY(.w_SCCODCLF) OR EMPTY(.w_SCCODVAL)
      ah_ErrorMsg("Inserire il codice cliente e il codice valuta",,"")
      i_retcode = 'stop'
      return
    endif
    this.w_SERDOC = .w_SERDOC
    this.oParentObject.w_PTCODCON = .w_SCCODCLF
    this.oParentObject.w_PTTIPCON = .w_SCTIPCLF
    this.oParentObject.w_PTCODVAL = .w_SCCODVAL
    if .w_ORIGINE<>"A" 
      this.oParentObject.w_PTCODAGE = .w_SCCODAGE
      this.oParentObject.w_PTBANAPP = .w_SCCODBAN
      this.oParentObject.w_PTBANNOS = .w_SCBANNOS
    endif
    ENDWITH
    * --- Creo il temporaneo con l'elenco delle partite non saldate
    GSTE_BPA (this,this.oParentObject.w_PTDATSCA , this.oParentObject.w_PTDATSCA , this.oParentObject.w_PTTIPCON , this.oParentObject.w_PTCODCON,"", this.oParentObject.w_PTCODVAL , this.oParentObject.w_PTNUMPAR, "RB","BO","RD","RI","MA","CA", "","N", cp_CharToDate("  -  -  ") )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    vq_exec("query\GSTE_BP3.VQR",this,"SELEPART")
    * --- Drop temporary table TMP_PART_APE
    i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART_APE')
    endif
    this.w_O_PTMODPAG = this.oParentObject.w_PTMODPAG
    this.w_O_PTBANAPP = this.oParentObject.w_PTBANAPP
    this.w_O_PTBANNOS = this.oParentObject.w_PTBANNOS
    this.oParentObject.w_PTMODPAG = ""
    this.oParentObject.w_PTBANAPP = ""
    this.oParentObject.w_PTBANNOS = ""
    if USED("SELEPART")
      this.oParentObject.w_PTMODPAG = this.w_O_PTMODPAG
      this.oParentObject.w_PTBANAPP = this.w_O_PTBANAPP
      this.oParentObject.w_PTBANNOS = this.w_O_PTBANNOS
      GSTE_BCP(this,"C","SELEPART"," ")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Creo una tabella temporanea per inserirvi le scadenze aperte. Utilizzo
      *     questa tecnica per visualizzare i dati in uno standard zoom, evitando
      *     cos� di costruire una maschera.
      *     Questo motivato anche dal fatto che le partite aperte recuperate in questi
      *     casi non sono mai un insieme numericamente rilevante.
      *     
      * --- Create temporary table TMP_PART
      i_nIdx=cp_AddTableDef('TMP_PART') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSTE_BP2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_PART_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      SELECT SELEPART
      GO TOP
      Scan For (( Empty( this.oParentObject.w_PTBANNOS ) Or Nvl(BANNOS,SPACE(15))= this.oParentObject.w_PTBANNOS ) And ( Empty( this.oParentObject.w_PTBANAPP ) Or Nvl(BANAPP,SPACE(15))= this.oParentObject.w_PTBANAPP ) or this.w_Isalt) ; 
 And ( Empty( this.oParentObject.w_PTMODPAG ) Or Nvl(MODPAG,SPACE(10))= this.oParentObject.w_PTMODPAG ) and (Empty(this.w_SERDOC) or (Not Empty(this.w_SERDOC) and this.w_SERDOC=PTSERIAL))
      this.w_LDATSCA = SELEPART.DATSCA
      this.w_LNUMPAR = SELEPART.NUMPAR
      this.w_LTIPCON = SELEPART.TIPCON
      this.w_LCODCON = SELEPART.CODCON
      this.w_LMODPAG = NVL(SELEPART.MODPAG,SPACE(5))
      this.w_LCODVAL = NVL(SELEPART.CODVAL,SPACE(5))
      this.w_LTOTIMP = NVL(SELEPART.TOTIMP,0)
      this.w_LBANNOS = NVL(SELEPART.BANNOS,SPACE(15))
      this.w_LBANAPP = NVL(SELEPART.BANAPP,SPACE(10))
      this.w_LNUMDOC = NVL(SELEPART.NUMDOC,0)
      this.w_LDATDOC = SELEPART.DATDOC
      this.w_LDESRIG = SELEPART.DESRIG
      this.w_LIMPDOC = NVL(SELEPART.IMPDOC,0)
      this.w_PTSERIAL = SELEPART.PTSERIAL
      this.w_PTROWORD = SELEPART.PTROWORD
      this.w_CPROWNUM = SELEPART.CPROWNUM
      this.w_LSEGNO = NVL(SELEPART.SEGNO, " ")
      this.w_LCAOVAL = NVL(SELEPART.CAOVAL, " ")
      this.w_LFLSOSP = NVL(SELEPART.PTFLSOSP, " ")
      * --- Valorizzo Tabella Temporanea
      * --- Insert into TMP_PART
      i_nConn=i_TableProp[this.TMP_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_PART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",DATSCA"+",NUMPAR"+",TIPCON"+",CODCON"+",MODPAG"+",CODVAL"+",CAOVAL"+",TOTIMP"+",BANNOS"+",BANAPP"+",NUMDOC"+",DATDOC"+",DESRIG"+",IMPDOC"+",SEGNO"+",PTFLSOSP"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'TMP_PART','PTSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'TMP_PART','PTROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'TMP_PART','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LDATSCA),'TMP_PART','DATSCA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LNUMPAR),'TMP_PART','NUMPAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LTIPCON),'TMP_PART','TIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LCODCON),'TMP_PART','CODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LMODPAG),'TMP_PART','MODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LCODVAL),'TMP_PART','CODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LCAOVAL),'TMP_PART','CAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LTOTIMP),'TMP_PART','TOTIMP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LBANNOS),'TMP_PART','BANNOS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LBANAPP),'TMP_PART','BANAPP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LNUMDOC),'TMP_PART','NUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LDATDOC),'TMP_PART','DATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LDESRIG),'TMP_PART','DESRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LIMPDOC),'TMP_PART','IMPDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LSEGNO),'TMP_PART','SEGNO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LFLSOSP),'TMP_PART','PTFLSOSP');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PTSERIAL,'PTROWORD',this.w_PTROWORD,'CPROWNUM',this.w_CPROWNUM,'DATSCA',this.w_LDATSCA,'NUMPAR',this.w_LNUMPAR,'TIPCON',this.w_LTIPCON,'CODCON',this.w_LCODCON,'MODPAG',this.w_LMODPAG,'CODVAL',this.w_LCODVAL,'CAOVAL',this.w_LCAOVAL,'TOTIMP',this.w_LTOTIMP,'BANNOS',this.w_LBANNOS)
        insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,DATSCA,NUMPAR,TIPCON,CODCON,MODPAG,CODVAL,CAOVAL,TOTIMP,BANNOS,BANAPP,NUMDOC,DATDOC,DESRIG,IMPDOC,SEGNO,PTFLSOSP &i_ccchkf. );
           values (;
             this.w_PTSERIAL;
             ,this.w_PTROWORD;
             ,this.w_CPROWNUM;
             ,this.w_LDATSCA;
             ,this.w_LNUMPAR;
             ,this.w_LTIPCON;
             ,this.w_LCODCON;
             ,this.w_LMODPAG;
             ,this.w_LCODVAL;
             ,this.w_LCAOVAL;
             ,this.w_LTOTIMP;
             ,this.w_LBANNOS;
             ,this.w_LBANAPP;
             ,this.w_LNUMDOC;
             ,this.w_LDATDOC;
             ,this.w_LDESRIG;
             ,this.w_LIMPDOC;
             ,this.w_LSEGNO;
             ,this.w_LFLSOSP;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ENDSCAN
      if this.Pexec="S"
        vx_exec("QUERY\GSTE_BP2.VZM",this)
      endif
    endif
    this.w_PADRE = this.oParentObject
    if this.pEXEC="S"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_PADRE.Markpos()     
      * --- Select from TMP_PART
      i_nConn=i_TableProp[this.TMP_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2],.t.,this.TMP_PART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_PART ";
             ,"_Curs_TMP_PART")
      else
        select * from (i_cTable);
          into cursor _Curs_TMP_PART
      endif
      if used('_Curs_TMP_PART')
        select _Curs_TMP_PART
        locate for 1=1
        do while not(eof())
        this.w_PADRE.AddRow()     
        this.w_DATSCA = _Curs_TMP_PART.DATSCA
        this.w_NUMPAR = _Curs_TMP_PART.NUMPAR
        this.w_CODVAL = _Curs_TMP_PART.CODVAL
        this.w_MODPAG = _Curs_TMP_PART.MODPAG
        this.w_BANAPP = Nvl(_Curs_TMP_PART.BANAPP," ")
        this.w_BANNOS = Nvl(_Curs_TMP_PART.BANNOS," ")
        this.w_TIPCON = _Curs_TMP_PART.TIPCON
        this.w_CODCON = _Curs_TMP_PART.CODCON
        this.w_TOTIMP = _Curs_TMP_PART.TOTIMP
        this.w_PTSERIAL = _Curs_TMP_PART.PTSERIAL
        this.w_PTROWORD = _Curs_TMP_PART.PTROWORD
        this.w_CPROWNUM = _Curs_TMP_PART.CPROWNUM
        this.w_CAOVAL = Nvl(_Curs_TMP_PART.CAOVAL,0)
        this.w_NUMDOC = Nvl(_Curs_TMP_PART.NUMDOC,0)
        this.w_ALFDOC = Nvl(_Curs_TMP_PART.ALFDOC,"")
        this.w_DATDOC = _Curs_TMP_PART.DATDOC
        this.w_DESRIG = Nvl(_Curs_TMP_PART.DESRIG," ")
        this.w_SEGNO = _Curs_TMP_PART.SEGNO
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_PADRE.SaveRow()     
          select _Curs_TMP_PART
          continue
        enddo
        use
      endif
      this.w_PADRE.Repos()     
    endif
    * --- Drop temporary table TMP_PART
    i_nIdx=cp_GetTableDefIdx('TMP_PART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART')
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if NOT EMPTY(this.w_NUMPAR)
      WITH this.oParentObject.oParentObject
      this.oParentObject.w_PTFLCRSA = IIF(.w_PFLCRSA="S", "S", "C")
      ENDWITH
      this.oParentObject.w_PTDATSCA = this.w_DATSCA
      * --- Per non Ricalcolare la Descrizione
      this.oParentObject.o_PTDATSCA = this.w_DATSCA
      this.oParentObject.w_PTNUMPAR = this.w_NUMPAR
      this.oParentObject.w_PTCODVAL = this.w_CODVAL
      this.oParentObject.w_PTMODPAG = this.w_MODPAG
      this.oParentObject.w_PTBANAPP = this.w_BANAPP
      this.oParentObject.w_PTBANNOS = this.w_BANNOS
      this.oParentObject.w_PTTIPCON = this.w_TIPCON
      this.oParentObject.w_PTCODCON = this.w_CODCON
      this.oParentObject.w_PTTOTIMP = this.w_TOTIMP
      this.oParentObject.w_PTSERRIF = this.w_PTSERIAL
      this.oParentObject.w_PTORDRIF = this.w_PTROWORD
      this.oParentObject.w_PTNUMRIF = this.w_CPROWNUM
      WITH this.oParentObject.oParentObject
      .w_SCIMPSCA=.w_SCIMPSCA+this.w_TOTIMP
      this.oParentObject.w_PTDATAPE = IIF(EMPTY(.w_SCDATDOC), .w_SCDATREG, .w_SCDATDOC)
      this.oParentObject.w_PTCAOAPE = .w_SCCAOVAL
      ENDWITH
      this.oParentObject.w_PTCAOVAL = this.w_CAOVAL
      this.oParentObject.w_PTNUMDOC = this.w_NUMDOC
      this.oParentObject.w_PTALFDOC = this.w_ALFDOC
      this.oParentObject.w_PTDATDOC = this.w_DATDOC
      this.oParentObject.w_PTDESRIG = this.w_DESRIG
      this.oParentObject.w_PTFLCRSA = "S"
      this.oParentObject.w_PT_SEGNO = IIF(this.w_SEGNO="D", "A","D")
      this.oParentObject.w_PTTOTIMP = ABS(this.oParentObject.w_PTTOTIMP)
      this.oParentObject.w_IMPPAR = this.oParentObject.w_PTTOTIMP*IIF(this.oParentObject.w_PT_SEGNO="D", 1,-1)*IIF(this.oParentObject.w_PTTIPCON="C", 1,-1)*IIF( this.oParentObject.oParentObject.w_PFLCRSA="S",-1,1)
      this.oParentObject.w_TOTPAR = this.oParentObject.w_TOTPAR + this.oParentObject.w_IMPPAR
    endif
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMP_PART'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_TMP_PART')
      use in _Curs_TMP_PART
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
