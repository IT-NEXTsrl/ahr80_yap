* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bmc                                                        *
*              Aggiorna modifica cambio                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-22                                                      *
* Last revis.: 2013-04-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bmc",oParentObject)
return(i_retval)

define class tgsve_bmc as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna le Righe Documento al Variare del Cambio di Testata  (da GSVE_MDV, GSOR_MDV, GSAC_MDV)
    this.w_PADRE = this.oParentObject
    this.w_PADRE.MarkPos()     
    this.w_PADRE.FirstRow()     
    * --- Scorre il dettaglio e ricalcola valore fiscale ed ultimo valore...
    do while Not this.w_PADRE.Eof_Trs()
      this.w_PADRE.SetRow()     
      if this.w_PADRE.FullRow()
        this.oParentObject.w_MVIMPNAZ = CAIMPNAZ(this.oParentObject.w_MVFLVEAC, this.oParentObject.w_MVVALMAG, this.oParentObject.w_MVCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ, this.oParentObject.w_MVCODVAL, this.oParentObject.w_MVCODIVE, this.oParentObject.w_PERIVE, this.oParentObject.w_INDIVE, NVL(t_PERIVA,0), NVL(t_INDIVA,0) )
        this.oParentObject.w_MVIMPCOM = CAIMPCOM( IIF(Empty(this.oParentObject.w_MVCODCOM),"S", iif(this.oParentObject.w_MVFLEVAS="S" And Not Empty( this.oParentObject.w_MVFLORCO ),"S","N") ), this.oParentObject.w_MVVALNAZ, this.oParentObject.w_COCODVAL, this.oParentObject.w_MVIMPNAZ, this.oParentObject.w_CAONAZ, IIF(EMPTY(this.oParentObject.w_MVDATDOC), this.oParentObject.w_MVDATREG, this.oParentObject.w_MVDATDOC), this.oParentObject.w_DECCOM )
        this.oParentObject.w_MVVALULT = IIF(this.oParentObject.w_MVQTAUM1=0, 0, cp_ROUND(this.oParentObject.w_MVIMPNAZ/this.oParentObject.w_MVQTAUM1, this.oParentObject.w_DECTOU))
        this.oParentObject.w_VISNAZ = cp_ROUND(this.oParentObject.w_MVIMPNAZ, this.oParentObject.w_DECTOP)
        this.w_PADRE.Set("w_MVIMPNAZ" , this.oParentObject.w_MVIMPNAZ)     
        this.w_PADRE.Set("w_MVIMPCOM" , this.oParentObject.w_MVIMPCOM)     
        this.w_PADRE.Set("w_MVVALULT" , this.oParentObject.w_MVVALULT)     
        this.w_PADRE.Set("w_VISNAZ" , this.oParentObject.w_VISNAZ)     
        this.w_PADRE.SetUpdateRow()     
      endif
      this.w_PADRE.NextRow()     
    enddo
    this.w_PADRE.RePos()     
    * --- Setta per non eseguire mCalc all'uscita
    this.bUpdateParentObject=.f.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
