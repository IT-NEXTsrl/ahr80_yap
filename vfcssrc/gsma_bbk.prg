* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bbk                                                        *
*              Controlli barcode                                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-07                                                      *
* Last revis.: 2010-11-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODICE,pTIPBAR,pTIPORI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bbk",oParentObject,m.pCODICE,m.pTIPBAR,m.pTIPORI)
return(i_retval)

define class tgsma_bbk as StdBatch
  * --- Local variables
  pCODICE = space(20)
  pTIPBAR = space(1)
  pTIPORI = space(1)
  w_CFUNC = space(10)
  w_MESS = space(10)
  w_CODICE = space(41)
  w_MESS = space(100)
  w_TIPBARL = space(1)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue controllo CheckSum lanciato da GSMA_AAR,GSMA_ACA
    * --- Codice Barcode
    * --- Tipo Barcode
    * --- Origine da cui il batch � lanciato
    this.w_CFUNC = This.oParentObject.cFunction
    this.w_CODICE = IIF(this.pTIPORI="A",this.oParentObject.w_ARCODART,this.oParentObject.w_CACODICE)
    this.w_TIPBARL = IIF(VARTYPE(this.oParentObject.w_TIPBAR)="C",this.oParentObject.w_TIPBAR,this.oParentObject.w_CATIPBAR)
    * --- Se fallisce il controllo sulla chksum() eseguo allineamento
    if  this.pTIPBAR$ "12C"
      * --- Controllo CheckSum()
      if NOT CALCBAR(this.w_CODICE, this.pTIPBAR, 2) 
        if this.w_CFUNC="Load"
          if AH_YESNO("Attenzione: carattere checksum errato, si desidera correggerlo automaticamente?")
            if this.pTIPORI="A"
              this.oParentObject.w_ARCODART = CALCBAR(this.w_CODICE, this.pTIPBAR, 1)
            else
              this.oParentObject.w_CACODICE = CALCBAR(this.w_CODICE, this.pTIPBAR, 1)
            endif
          else
            if this.pTIPORI="A"
              ah_ErrorMsg("Attenzione: carattere checksum errato, impossibile generare barcode di tipo %1", 48, "" , iif(this.w_TIPBARL="2"," Ean13"," Ean8")) 
              this.oParentObject.w_ARTIPBAR = "0"
            else
              ah_ErrorMsg("Attenzione: carattere checksum errato, impossibile generare barcode di tipo %1", 48, "" , iif(this.w_TIPBARL="2"," Ean13"," Ean8")) 
              this.oParentObject.w_CATIPBAR = "0"
            endif
          endif
        else
          if this.pTIPORI="B"
            * --- Controllo in Modifica codice di ricerca
            ah_ErrorMsg("Attenzione: carattere checksum errato, impossibile generare barcode di tipo %1", 48, "" , iif(this.w_TIPBARL="2"," Ean13"," Ean8")) 
            this.oParentObject.w_CATIPBAR = "0"
          endif
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pCODICE,pTIPBAR,pTIPORI)
    this.pCODICE=pCODICE
    this.pTIPBAR=pTIPBAR
    this.pTIPORI=pTIPORI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='KEY_ARTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODICE,pTIPBAR,pTIPORI"
endproc
