* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mpg                                                        *
*              Caricamento rapido                                              *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_634]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-05-07                                                      *
* Last revis.: 2016-09-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsar_mpg
* Passo come parametro la gestione sulla quale
* occorre riempire il transitorio
Parameters Obj_Gest

* --- Fine Area Manuale
return(createobject("tgsar_mpg"))

* --- Class definition
define class tgsar_mpg as StdTrsForm
  Top    = 4
  Left   = 12

  * --- Standard Properties
  Width  = 819
  Height = 478+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-09-21"
  HelpContextID=147421033
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=245

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  GESPENNA_IDX = 0
  KEY_ARTI_IDX = 0
  UNIMIS_IDX = 0
  ART_ICOL_IDX = 0
  LOTTIART_IDX = 0
  UBICAZIO_IDX = 0
  MAGAZZIN_IDX = 0
  CAM_AGAZ_IDX = 0
  CENCOST_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  MATRICOL_IDX = 0
  TIP_DOCU_IDX = 0
  UNIT_LOG_IDX = 0
  TIPICONF_IDX = 0
  TIP_COLL_IDX = 0
  cFile = "GESPENNA"
  cKeySelect = "GPSERIAL"
  cKeyWhere  = "GPSERIAL=this.w_GPSERIAL"
  cKeyDetail  = "GPSERIAL=this.w_GPSERIAL"
  cKeyWhereODBC = '"GPSERIAL="+cp_ToStrODBC(this.w_GPSERIAL)';

  cKeyDetailWhereODBC = '"GPSERIAL="+cp_ToStrODBC(this.w_GPSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"GESPENNA.GPSERIAL="+cp_ToStrODBC(this.w_GPSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'GESPENNA.CPROWORD '
  cPrg = "gsar_mpg"
  cComment = "Caricamento rapido"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GPSERIAL = space(10)
  w_CPROWORD = 0
  w_GPCODICE = space(20)
  o_GPCODICE = space(20)
  w_GPCODMAT = space(40)
  o_GPCODMAT = space(40)
  w_CODART = space(20)
  w_GESMAT = space(1)
  w_GPUNIMIS = space(3)
  o_GPUNIMIS = space(3)
  w_GPQTAMOV = 0
  o_GPQTAMOV = 0
  w_GPPREZZO = 0
  w_QTAORI = 0
  w_GP_ESCLU = space(1)
  w_GPEVASIO = space(1)
  w_OLDQTA = 0
  w_MAGPRE = space(5)
  w_FLUBIC = space(1)
  w_F2UBIC = space(1)
  w_GPCODMAG = space(5)
  o_GPCODMAG = space(5)
  w_GPCODMA2 = space(5)
  w_GPCODUBI = space(20)
  w_GPCODUB2 = space(20)
  w_GPCODLOT = space(20)
  o_GPCODLOT = space(20)
  w_GPQTAUM1 = 0
  w_GPCODCEN = space(15)
  w_MTMAGCAR = space(5)
  o_MTMAGCAR = space(5)
  w_GPCODCOM = space(15)
  o_GPCODCOM = space(15)
  w_MTFLCARI = space(1)
  w_TIPATT = space(1)
  w_MTMAGSCA = space(5)
  o_MTMAGSCA = space(5)
  w_GPCODATT = space(15)
  w_ESIRIS = space(1)
  w_DESART = space(40)
  w_EVTIPDOC = space(5)
  w_MTKEYSAL = space(20)
  w_EVNUMDOC = 0
  w_UNMIS1 = space(3)
  w_EVALFDOC = space(10)
  w_FLSERG = space(1)
  w_NOMSGCHECK = space(1)
  w_EVDATDOC = ctod('  /  /  ')
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_FLSTAT = space(1)
  w_OBJ_GEST = space(10)
  w_CODCON = space(15)
  w_DATREG = ctod('  /  /  ')
  w_FLLOTT = space(1)
  w_MOLTI3 = 0
  w_FLCASC = space(1)
  o_FLCASC = space(1)
  w_FLRISE = space(1)
  w_MxFLLOTT = space(1)
  w_F2CASC = space(1)
  w_F2RISE = space(1)
  w_MxF2LOTT = space(1)
  w_STATOROW = space(1)
  w_FLPRG = space(1)
  w_LOTZOOM = .F.
  w_UBIZOOM = .F.
  w_OBTEST = ctod('  /  /  ')
  w_TEST = .F.
  w_CAPTION = space(50)
  w_TEXTCOLOR = 0
  w_TIPOOBJ_GEST = space(1)
  w_Msg = space(0)
  w_CODMAGTES = space(5)
  w_DESMAGTES = space(30)
  w_FLUBICTES = space(1)
  w_UBITES = space(20)
  w_CODMA2TES = space(5)
  w_UB2TES = space(20)
  w_CODCENTES = space(15)
  w_CODCOMTES = space(15)
  w_CODATTTES = space(15)
  w_UBIDESTES = space(40)
  w_ARTZOOM = space(1)
  w_FLMGPR = space(1)
  w_COMAG = space(5)
  w_MAGTER = space(5)
  w_OMAG = space(5)
  w_OMAT = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_DATOBS2 = ctod('  /  /  ')
  w_DESPIA = space(40)
  w_FLANAL = space(1)
  w_OCEN = space(15)
  w_FLGCOM = space(1)
  w_OCOM = space(15)
  w_DESCAN = space(40)
  w_OATT = space(15)
  w_ATDES = space(30)
  w_ESEGUITOIMPORT = .F.
  w_TIPRIG = space(1)
  w_FLVEAC = space(1)
  w_FLFRAZ = space(1)
  w_TIPORN = space(1)
  w_CODORN = space(15)
  w_TIPCON = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_FLSCOR = space(1)
  w_MVFLSCOR = space(1)
  w_TIPDOC = space(5)
  w_FILART = space(20)
  w_EVAINI = ctod('  /  /  ')
  w_EVAFIN = ctod('  /  /  ')
  w_ESCL1 = space(3)
  w_ESCL2 = space(3)
  w_ESCL3 = space(3)
  w_ESCL4 = space(3)
  w_ESCL5 = space(3)
  w_FILARM = space(20)
  w_FILCLI = space(15)
  w_FILCAU = space(5)
  w_SERIAL = space(10)
  w_FILMAG = space(5)
  w_FILCOMM = space(15)
  w_FILATTI = space(15)
  w_FILCEN = space(15)
  w_FILVOC = space(15)
  w_CODICE = space(20)
  w_UNIMIS = space(3)
  w_PREZZO = 0
  w_QTAMOV = 0
  w_CODLOT = space(20)
  w_CODUBI = space(20)
  w_CODMAT = space(40)
  w_CODMAG = space(5)
  w_GPSERRIF = space(10)
  w_GPROWRIF = 0
  w_DESAR1 = space(40)
  w_CODFOR = space(15)
  w_MTFLSCAR = space(1)
  w_MAGUBI = space(5)
  w_MOVMAT = .F.
  w_ULTPROG = 0
  w_SECODUBI = space(20)
  w_SECODLOT = space(20)
  w_MT__FLAG = space(1)
  w_OLDPRE = 0
  w_ROWAGG = space(1)
  w_NODOCU = space(1)
  w_MT_SALDO = 0
  w_NOMSG = .F.
  w_DATSCA = ctod('  /  /  ')
  w_MTSERRIF = space(10)
  w_MTROWRIF = 0
  w_MTRIFNUM = 0
  w_DOCCOLL = space(5)
  w_EVROWORD = 0
  w_DOQTAEV1 = 0
  w_DOIMPEVA = 0
  w_FLRRIF = space(1)
  w_PARAME = space(3)
  w_BACKCOLOR = 0
  w_FLAGRIS = space(1)
  w_FLVSRI = space(1)
  w_FLRIDE = space(1)
  w_FLNSRI = space(1)
  w_CODESC = space(5)
  w_LENSCF = 0
  w_TPRDES = space(3)
  w_TPVSRI = space(3)
  w_TPNSRI = space(3)
  w_MTRIFSTO = 0
  w_MTRIFSTO = 0
  w_FLGROR = space(1)
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_CAUMAG = space(5)
  w_CAUCOL = space(5)
  w_DISLOT = space(1)
  w_GPFLDOCU = space(1)
  w_GPFLINTE = space(1)
  w_GPFLACCO = space(1)
  w_CODUB2 = space(20)
  w_CODMA2 = space(5)
  w_COMAT = space(5)
  w_DESMA2TES = space(30)
  w_UB2DESTES = space(40)
  w_FLUBI2TES = space(1)
  w_CODCEN_OR = space(15)
  w_CODCOM_OR = space(15)
  w_CODATT_OR = space(15)
  w_FLUSEP = space(1)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTIP = 0
  w_QTAUM1 = 0
  w_GPUNILOG = space(18)
  w_UNILOG = space(18)
  w_TIPCO1 = space(5)
  w_TIPCO2 = space(5)
  w_TIPCO3 = space(3)
  w_TPCON1 = space(3)
  w_TPCON2 = space(3)
  w_TPCON3 = space(3)
  w_FLPACK = space(1)
  w_FLESUL = space(1)
  w_CODCONF = space(3)
  w_FLCOVA = space(1)
  w_GPCODCOL = space(5)
  w_GPNUMCOL = 0
  w_MVCODART = space(20)
  w_MVCODLOT = space(20)
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(1)
  w_FLFOM = space(1)
  w_DOQTAEVA = 0
  w_F2ORDI = space(1)
  w_F2IMPE = space(1)
  w_FLMTPR = space(1)
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_FLRICIVA = .F.
  o_FLRICIVA = .F.
  w_RIIVDTIN = ctod('  /  /  ')
  o_RIIVDTIN = ctod('  /  /  ')
  w_RIIVDTFI = ctod('  /  /  ')
  w_RIIVLSIV = space(0)
  w_ERRORCOD = space(200)
  w_MVAGG_01 = space(15)
  w_MVAGG_02 = space(15)
  w_MVAGG_03 = space(15)
  w_MVAGG_04 = space(15)
  w_MVAGG_05 = ctod('  /  /  ')
  w_MVAGG_06 = ctod('  /  /  ')
  w_TDFLIA01 = space(1)
  w_TDFLIA02 = space(1)
  w_TDFLIA03 = space(1)
  w_TDFLIA04 = space(1)
  w_TDFLIA05 = space(1)
  w_TDFLIA06 = space(1)
  w_TDFLRA01 = space(1)
  w_TDFLRA02 = space(1)
  w_TDFLRA03 = space(1)
  w_TDFLRA04 = space(1)
  w_TDFLRA05 = space(1)
  w_TDFLRA06 = space(1)
  w_DACAM_01 = space(30)
  w_DACAM_02 = space(30)
  w_DACAM_03 = space(30)
  w_DACAM_04 = space(30)
  w_DACAM_05 = space(30)
  w_DACAM_06 = space(30)
  w_COMCAR = space(15)
  w_COMSCA = space(15)
  * --- Area Manuale = Declare Variables
  * --- gsar_mpg
  * Nome Cursore (riempito in GSAR_BPG(L)
  w_CURSORE=Space(10)
  
  * Movimentazione d'appoggio al salvataggio
  * non scrive mai sul database ma sulla gestione
  * sottostante (NotifyEvent) ed in seguito
  * si chiude...
  Proc EcpSave()
   this.NotifyEvent('Salvaechiudi')
   * Per evitare msg "Abbandoni Modifiche.."
   This.cFunction='Query'
   This.EcpQuit()
  EndProc
  
  * redichiaro in toto la ECPQUIT
  * per chiudere la maschera a seguito del messaggio
  * Abbandoni le modifiche
  * --- Esc
    proc ecpQuit()
      local i_oldfunc
      * ===========================
      * unica modifica da
      * i_oldfunc=this.cFunction
      * a
      i_oldfunc='Query'
      * ===========================
      if inlist(this.cFunction,'Edit','Load')
        if this.IsAChildUpdated()
          if !cp_YesNo(MSG_DISCARD_CHANGES_QP)
            return
          endif
        endif
        this.bUpdated=.f.
        this.NotifyEvent('Edit Aborted')
      endif
      *--- Activity Logger traccio ESC
      If i_nACTIVATEPROFILER>0
      	cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UES", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
      Endif
      *--- Activity Logger traccio ESC
      * --- si sposta senza attivare i controlli
      this.cFunction='Filter'
      local i_err
      i_err=on('error')
      on error =.t.
      this.activecontrol.parent.oContained.bDontReportError=.t.
      on error &i_err
      this.__dummy__.enabled=.t.
      this.__dummy__.Setfocus()
      * ---
      do case
        case i_oldfunc="Query"
          this.NotifyEvent("Done")
          this.Hide()
          this.Release()
        case i_oldfunc="Load"
          *this.QueryKeySet(this.cLastWhere,this.cLastOrderBy)
          *this.ecpQuery()
          *this.nLoadTime=0
          This.cFunction="Query"
          This.BlankRec()
          This.bLoaded=.F.
          This.SetStatus()
          This.SetFocusOnFirstControl()
        case i_oldfunc="Edit"
          this.ecpQuery()
          this.nLoadTime=0
        otherwise
          this.ecpQuery()
      endcase
      return
  EndProc
  
    * Determina valore w_STATOROW
    Proc State_Row
    * Gestione w_STATOROW
    Local L_CtrlLotto,L_CtrlUbi, L_CtrlMag,L_CtrlCodCen,L_CtrlCodCom,L_CtrlCodAtt,L_CtrlCodMat
    with this
    L_CtrLotto=.GetCtrl('w_GPCODLOT')
    L_CtrUbi=.GetCtrl('w_GPCODUBI')
    L_CtrlMag=.GetCtrl('w_GPCODMAG')
    L_CtrlCodCen=.GetCtrl('w_GPCODCEN')
    L_CtrlCodCom=.GetCtrl('w_GPCODCOM')
    L_CtrlCodAtt=.GetCtrl('w_GPCODATT')
    L_CtrlCodMat=.GetBodyCtrl('w_GPCODMAT')
    L_CtrUb2=.GetCtrl('w_GPCODUB2')
    L_CtrlMa2=.GetCtrl('w_GPCODMA2')
    L_CtrlQta1=.GetCtrl('w_GPQTAUM1')
    L_CtrUb2=.GetCtrl('w_GPCODUB2')
    L_CtrlMa2=.GetCtrl('w_GPCODMA2')
    L_CtrlQta1=.GetCtrl('w_GPQTAUM1')
    L_CtrlUniLog=.GetCtrl('w_GPUNILOG')
  
    .w_STATOROW='S'
    .w_CAPTION=''
    * Se tutto Ok label colorata come la form
    .w_TEXTCOLOR=.backcolor
    .w_BACKCOLOR=.backcolor
  
    Do Case
     Case Empty( .w_GPCODICE )
    	* Manca Codice Articolo	
    	 .w_STATOROW='A'
    	 .w_CAPTION=ah_Msgformat("Articolo non presente")
    	* Sfondo maschera scritta rossa
    	 .w_TEXTCOLOR=Rgb(255,0,0)
       .w_BACKCOLOR=Rgb(255,255,255)
     Case Empty(  .w_GPUNIMIS )
    	* Manca Unit� di Misura	
    	 .w_STATOROW='M'
    	 .w_CAPTION=ah_Msgformat("Unit� di misura non presente")
    	* Sfondo maschera scritta gialla
    	 .w_TEXTCOLOR=Rgb(255,255,0)
       .w_BACKCOLOR=.backcolor
    Case  .w_GPQTAMOV=0 And .w_TIPOOBJ_GEST<>'R'
    	* Qt� a Zero (accetto solo se carico da rettifiche inventariali)
    	 .w_STATOROW='Q'	
    	 .w_CAPTION=ah_Msgformat("Quantit� movimentata a 0")
    	* Sfondo maschera scritta blu
    	 .w_TEXTCOLOR=Rgb(0,0,255)
       .w_BACKCOLOR=Rgb(255,255,255)
     Case L_CtrlMag.mCond() And Empty(  .w_GPCODMAG )
    	* Manca Magazzino	
    	 .w_STATOROW='D'		
    	 .w_CAPTION=ah_Msgformat("Magazzino non presente")
    	* Sfondo bianco scritta rossa
    	 .w_TEXTCOLOR=Rgb(255,0,0)
       .w_BACKCOLOR=Rgb(255,255,255)
     Case L_CtrLotto.mCond() And Empty( .w_GPCODLOT)AND g_PERLOT='S'
    	* Manca Lotto (se necessario)	
    	 .w_STATOROW='L'		
    	 .w_CAPTION=ah_Msgformat("Lotto non presente")
       * Sfondo bianco scritta Blu
    	 .w_TEXTCOLOR=Rgb(0,0,255)
       .w_BACKCOLOR=Rgb(255,255,255)
     Case L_CtrUbi.mCond() And Empty( .w_GPCODUBI)AND g_PERUBI='S'
    	* Manca Ubicazione (se necessario)	
    	 .w_STATOROW='U'			
    	 .w_CAPTION=ah_Msgformat("Ubicazione non presente")
       * Sfondo bianco scritta Blu
    	 .w_TEXTCOLOR=Rgb(0,0,255)
       .w_BACKCOLOR=Rgb(255,255,255)
      Case L_CtrlCodCen.mCond() And Empty(.w_GPCODCEN ) And .w_TIPOOBJ_GEST<>'O' AND(g_PERCAN='S' AND .w_FLANAL='S')
      	* Manca Centro di costo se non Ordini
    	 .w_STATOROW='C'			
    	 .w_CAPTION=ah_Msgformat("Centro di costo/ricavo non valorizzato")
       * Sfondo maschera scritta marrone
    	 .w_TEXTCOLOR=Rgb(128,0,0)
       .w_BACKCOLOR=Rgb(255,255,255)
      Case L_CtrlCodCom.mCond() And Empty(.w_GPCODCOM ) And .w_TIPOOBJ_GEST<>'O' AND g_COMM='S' AND .w_FLGCOM='S'
      	* Manca Commessa se non Ordini
    	 .w_STATOROW='P'			
    	 .w_CAPTION=ah_Msgformat("Commessa non valorizzata")
       * Sfondo bianco scritta marrone
    	 .w_TEXTCOLOR=Rgb(128,0,0)
       .w_BACKCOLOR=Rgb(255,255,255)
      Case L_CtrlCodAtt.mCond() And Empty(.w_GPCODATT ) And .w_TIPOOBJ_GEST<>'O'AND  g_COMM='S' AND .w_FLGCOM='S'
      	* Manca Attivit� di Commessa se non Ordini
    	 .w_STATOROW='G'			
    	 .w_CAPTION=ah_Msgformat("Attivit� di commessa non valorizzata")
       * Sfondo bianco scritta marrone
    	 .w_TEXTCOLOR=Rgb(128,0,0)
       .w_BACKCOLOR=Rgb(255,255,255)
      Case L_CtrlCodMat.mCond() And Empty(.w_GPCODMAT )AND .w_GESMAT='S'AND g_MATR='S' And .w_TIPOOBJ_GEST<>'P'
       * Manca Matricola
    	 .w_STATOROW='T'			
    	 .w_CAPTION=ah_Msgformat("Matricola mancante")
       * Sfondo bianco scritta marrone
    	 .w_TEXTCOLOR=Rgb(128,0,0)
       .w_BACKCOLOR=Rgb(255,255,255)
      Case .w_FLAGRIS='R'AND EMPTY(.w_GPSERRIF) AND .w_GESMAT='S'AND g_MATR='S' And .w_TIPOOBJ_GEST<>'P'
    	 .w_STATOROW='E'			
    	 .w_CAPTION=ah_Msgformat("Riga da evadere con riservato")
       * Sfondo bianco scritta blu
    	 .w_TEXTCOLOR=Rgb(0,0,255)
       .w_BACKCOLOR=Rgb(255,255,255)
      Case L_CtrlMa2.mCond() And Empty(  .w_GPCODMA2 )
    	* Manca Magazzino	
    	 .w_STATOROW='F'		
    	 .w_CAPTION=ah_Msgformat("Magazzino collegato non presente")
    	* Sfondo bianco scritta rossa
    	 .w_TEXTCOLOR=Rgb(255,0,0)
       .w_BACKCOLOR=Rgb(255,255,255)
      Case L_CtrUb2.mCond() And Empty( .w_GPCODUB2)AND g_PERUBI='S'
    	* Manca Ubicazione (se necessario)	
    	 .w_STATOROW='B'			
    	 .w_CAPTION=ah_Msgformat("Ubicazione collegata non presente")
       * Sfondo bianco scritta Blu
    	 .w_TEXTCOLOR=Rgb(0,0,255)
       .w_BACKCOLOR=Rgb(255,255,255)
      Case L_CtrlQta1.mCond() And Empty(  .w_GPQTAUM1 )
    	* Manca Quantit� nell'unit� di misura principale
    	 .w_STATOROW='1'		
    	 .w_CAPTION=ah_Msgformat("Quantit� nell'unit� di misura principale non presente o non frazionabile")
    	* Sfondo bianco scritta rossa
    	 .w_TEXTCOLOR=Rgb(255,0,0)
       .w_BACKCOLOR=Rgb(255,255,255)
      Case L_CtrlUniLog.mCond() and empty( .w_GPUNILOG )
      * Manca unit� logistica
    	 .w_STATOROW='N'		
    	 .w_CAPTION=ah_Msgformat("Unit� logistica non presente")
       * Sfondo bianco scritta Blu
    	 .w_TEXTCOLOR=Rgb(0,0,255)
       .w_BACKCOLOR=Rgb(255,255,255)
      EndCase
    EndWith
  
    EndProc
  
   Proc Shift_Column
     *Ridefinisce Struttura Griglia
     Local L_CtrlCodMat,L_ctrl,L_CtrlDesar1,L_CtrlCodice
     with this
     L_CtrlCodMat=.GetBodyCtrl('w_GPCODMAT')
     L_CtrlCodice=.GetBodyCtrl('w_GPCODICE')
     L_Ctrldesar1=.GetBodyCtrl('w_DESAR1')
  
     *Modifico Posizione delle righe
     if g_MATR='S'AND g_UNIMAT='M'AND Upper(Obj_Gest.CLASS)<>'TGSPS_MVD'
      L_CtrlCodice.tabstop=.F.
      L_Ctrldesar1.left=L_Ctrldesar1.left + thisform.width +10
      *Barra fra Articolo e Descrizione
      L_ctrl=.GetCtrl("XOVSCPOJVX")
      L_ctrl.left=L_ctrl.left+ thisform.width +10
      *Dicitura Descrizione
      L_ctrl=.GetCtrl("VWBDSEQRMI")
      L_ctrl.left= L_ctrl.left+ thisform.width +10
     else
      L_width=L_CtrlCodMat.width
      L_CtrlCodice.left=43
      *Dicitura Articolo
      L_ctrl=.GetCtrl("XHEGGIKTKX")
      L_ctrl.left=43
      *Barra verticale fra Matricola e Articolo
      L_ctrl=.GetCtrl("OTXFRRRBOC")
      L_ctrl.left=L_ctrl.left+ thisform.width +10
      if g_UNIMAT $'A-C' AND g_MATR='S'AND Upper(Obj_Gest.CLASS)<>'TGSPS_MVD'
       L_CtrlCodMat.left=197
       *Dicitura Matricola
       L_ctrl=.GetCtrl("VIBFBASHHS")
       L_ctrl.left=197
       *Descrizione Articolo
       L_Ctrldesar1.left=L_ctrl.left+ thisform.width +10
       *Dicitura Descrizione Articolo
       L_ctrl=.GetCtrl("VWBDSEQRMI")
       L_ctrl.left=L_ctrl.left+ thisform.width +10
      else
       L_CtrlCodMat.left=L_ctrl.left+ thisform.width +10
       *Dicitura Matricola
       L_ctrl=.GetCtrl("VIBFBASHHS")
       L_ctrl.left=L_ctrl.left+ thisform.width +10
      endif
    endif
    EndWith
   EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GESPENNA','gsar_mpg')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mpgPag1","gsar_mpg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati")
      .Pages(1).HelpContextID = 140038602
      .Pages(2).addobject("oPag","tgsar_mpgPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati generali")
      .Pages(2).HelpContextID = 139872524
      .Pages(3).addobject("oPag","tgsar_mpgPag3")
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Messaggi")
      .Pages(3).HelpContextID = 188669137
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[16]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='UNIMIS'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='LOTTIART'
    this.cWorkTables[5]='UBICAZIO'
    this.cWorkTables[6]='MAGAZZIN'
    this.cWorkTables[7]='CAM_AGAZ'
    this.cWorkTables[8]='CENCOST'
    this.cWorkTables[9]='CAN_TIER'
    this.cWorkTables[10]='ATTIVITA'
    this.cWorkTables[11]='MATRICOL'
    this.cWorkTables[12]='TIP_DOCU'
    this.cWorkTables[13]='UNIT_LOG'
    this.cWorkTables[14]='TIPICONF'
    this.cWorkTables[15]='TIP_COLL'
    this.cWorkTables[16]='GESPENNA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(16))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GESPENNA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GESPENNA_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_GPSERIAL = NVL(GPSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    local link_2_18_joined
    link_2_18_joined=.f.
    local link_2_23_joined
    link_2_23_joined=.f.
    local link_2_25_joined
    link_2_25_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from GESPENNA where GPSERIAL=KeySet.GPSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.GESPENNA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GESPENNA_IDX,2],this.bLoadRecFilter,this.GESPENNA_IDX,"gsar_mpg")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GESPENNA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GESPENNA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GESPENNA '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_18_joined=this.AddJoinedLink_2_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_23_joined=this.AddJoinedLink_2_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_25_joined=this.AddJoinedLink_2_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GPSERIAL',this.w_GPSERIAL  )
      select * from (i_cTable) GESPENNA where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_NOMSGCHECK = 'N'
        .w_OBJ_GEST = Obj_Gest
        .w_CODCON = space(15)
        .w_DATREG = ctod("  /  /  ")
        .w_FLCASC = space(1)
        .w_FLRISE = space(1)
        .w_F2CASC = space(1)
        .w_F2RISE = space(1)
        .w_FLPRG = 'P'
        .w_TIPOOBJ_GEST = IIF(Lower ( .w_OBJ_GEST.class ) = 'tgsps_mvd','P',IIF(Lower ( .w_OBJ_GEST.class ) = 'tgsve_mdv','V',IIF(Lower ( .w_OBJ_GEST.class ) = 'tgsac_mdv','A',IIF(Lower ( .w_OBJ_GEST.class ) = 'tgsor_mdv','O',IIF(Lower ( .w_OBJ_GEST.class ) = 'tgsma_mvm','M','R')))))
        .w_Msg = space(0)
        .w_CODMAGTES = space(5)
        .w_DESMAGTES = space(30)
        .w_FLUBICTES = space(1)
        .w_UBITES = space(20)
        .w_CODMA2TES = space(5)
        .w_UB2TES = space(20)
        .w_CODCENTES = space(15)
        .w_CODCOMTES = space(15)
        .w_CODATTTES = space(15)
        .w_UBIDESTES = space(40)
        .w_ARTZOOM = IIF(.w_TIPOOBJ_GEST='P','S',IIF(.w_TIPOOBJ_GEST$'M-R','M','N'))
        .w_FLMGPR = space(1)
        .w_COMAG = space(5)
        .w_MAGTER = space(5)
        .w_DATOBS2 = ctod("  /  /  ")
        .w_DESPIA = space(40)
        .w_FLANAL = space(1)
        .w_FLGCOM = space(1)
        .w_DESCAN = space(40)
        .w_ATDES = space(30)
        .w_ESEGUITOIMPORT = .F.
        .w_FLVEAC = space(1)
        .w_TIPORN = space(1)
        .w_CODORN = space(15)
        .w_TIPCON = space(1)
        .w_DATINI = ctod("  /  /  ")
        .w_DATFIN = ctod("  /  /  ")
        .w_FLSCOR = space(1)
        .w_MVFLSCOR = space(1)
        .w_TIPDOC = space(5)
        .w_FILART = space(20)
        .w_EVAINI = ctod("  /  /  ")
        .w_EVAFIN = ctod("  /  /  ")
        .w_ESCL1 = space(3)
        .w_ESCL2 = space(3)
        .w_ESCL3 = space(3)
        .w_ESCL4 = space(3)
        .w_ESCL5 = space(3)
        .w_FILARM = space(20)
        .w_FILCLI = space(15)
        .w_FILCAU = space(5)
        .w_SERIAL = space(10)
        .w_FILMAG = space(5)
        .w_FILCOMM = space(15)
        .w_FILATTI = space(15)
        .w_FILCEN = space(15)
        .w_FILVOC = space(15)
        .w_MOVMAT = .t.
        .w_ULTPROG = 0
        .w_SECODUBI = space(20)
        .w_SECODLOT = space(20)
        .w_BACKCOLOR = .backcolor
        .w_FLVSRI = space(1)
        .w_FLRIDE = space(1)
        .w_FLNSRI = space(1)
        .w_CODESC = space(5)
        .w_TPRDES = space(3)
        .w_TPVSRI = space(3)
        .w_TPNSRI = space(3)
        .w_CAUMAG = space(5)
        .w_CAUCOL = space(5)
        .w_COMAT = space(5)
        .w_DESMA2TES = space(30)
        .w_UB2DESTES = space(40)
        .w_FLUBI2TES = space(1)
        .w_FLPACK = space(1)
        .w_FLFOM = space(1)
        .w_F2ORDI = space(1)
        .w_F2IMPE = space(1)
        .w_FLMTPR = space(1)
        .w_FLORDI = space(1)
        .w_FLIMPE = space(1)
        .w_FLRICIVA = iif(Isalt(),.t.,.f.)
        .w_RIIVDTIN = ctod("  /  /  ")
        .w_RIIVDTFI = ctod("  /  /  ")
        .w_RIIVLSIV = space(0)
        .w_MVAGG_01 = space(15)
        .w_MVAGG_02 = space(15)
        .w_MVAGG_03 = space(15)
        .w_MVAGG_04 = space(15)
        .w_MVAGG_05 = ctod("  /  /  ")
        .w_MVAGG_06 = ctod("  /  /  ")
        .w_TDFLIA01 = space(1)
        .w_TDFLIA02 = space(1)
        .w_TDFLIA03 = space(1)
        .w_TDFLIA04 = space(1)
        .w_TDFLIA05 = space(1)
        .w_TDFLIA06 = space(1)
        .w_TDFLRA01 = space(1)
        .w_TDFLRA02 = space(1)
        .w_TDFLRA03 = space(1)
        .w_TDFLRA04 = space(1)
        .w_TDFLRA05 = space(1)
        .w_TDFLRA06 = space(1)
        .w_DACAM_01 = space(30)
        .w_DACAM_02 = space(30)
        .w_DACAM_03 = space(30)
        .w_DACAM_04 = space(30)
        .w_DACAM_05 = space(30)
        .w_DACAM_06 = space(30)
        .w_GPSERIAL = NVL(GPSERIAL,space(10))
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .w_OBTEST = .w_DATREG
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
          .link_4_2('Load')
          .link_4_6('Load')
          .link_4_8('Load')
          .link_4_9('Load')
          .link_4_11('Load')
          .link_4_12('Load')
          .link_4_13('Load')
        .w_OCEN = IIF(EMPTY(.w_GPCODCEN), .w_OCEN, .w_GPCODCEN)
        .w_OCOM = IIF(EMPTY(.w_GPCODCOM), .w_OCEN, .w_GPCODCOM)
        .w_OATT = IIF(EMPTY(.w_GPCODATT), .w_OATT, .w_GPCODATT)
        .w_CODFOR = iif(.w_CODMAG=.w_MTMAGCAR And .w_TIPCON='F',.w_CODCON,SPACE(15))
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .w_DOCCOLL = READCOLL(.w_TIPDOC)
        .w_ERRORCOD = iif(.w_TIPOOBJ_GEST$ 'A-O-V',  'Codice incongruente o inesistente o obsoleto', 'Articolo inesistente')
        .w_COMCAR = .w_GPCODCOM
        .w_COMSCA = .w_GPCODCOM
        cp_LoadRecExtFlds(this,'GESPENNA')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CODART = space(20)
          .w_GESMAT = space(1)
          .w_QTAORI = 0
          .w_FLUBIC = space(1)
          .w_F2UBIC = space(1)
        .w_TIPATT = 'A'
          .w_DESART = space(40)
          .w_EVTIPDOC = space(5)
          .w_EVNUMDOC = 0
          .w_UNMIS1 = space(3)
          .w_EVALFDOC = space(10)
          .w_FLSERG = space(1)
          .w_EVDATDOC = ctod("  /  /  ")
          .w_UNMIS2 = space(3)
          .w_UNMIS3 = space(3)
          .w_FLSTAT = space(1)
          .w_FLLOTT = space(1)
          .w_MOLTI3 = 0
        .w_STATOROW = 'A'
        .w_UBIZOOM = .T.
        .w_TEST = .T.
          .w_CAPTION = space(50)
          .w_TEXTCOLOR = 0
          .w_DATOBSO = ctod("  /  /  ")
          .w_TIPRIG = space(1)
          .w_FLFRAZ = space(1)
          .w_CODICE = space(20)
          .w_UNIMIS = space(3)
          .w_PREZZO = 0
          .w_QTAMOV = 0
          .w_CODLOT = space(20)
          .w_CODUBI = space(20)
          .w_CODMAT = space(40)
          .w_CODMAG = space(5)
          .w_DESAR1 = space(40)
          .w_MT__FLAG = space(1)
          .w_OLDPRE = 0
          .w_MT_SALDO = 0
          .w_DATSCA = ctod("  /  /  ")
          .w_MTSERRIF = space(10)
          .w_MTROWRIF = 0
          .w_MTRIFNUM = 0
          .w_EVROWORD = 0
          .w_DOQTAEV1 = 0
          .w_DOIMPEVA = 0
          .w_FLRRIF = space(1)
          .w_PARAME = space(3)
          .w_FLAGRIS = space(1)
          .w_LENSCF = 0
          .w_MTRIFSTO = 0
          .w_MTRIFSTO = 0
          .w_FLGROR = space(1)
          .w_SCONT1 = 0
          .w_SCONT2 = 0
          .w_SCONT3 = 0
          .w_SCONT4 = 0
          .w_DISLOT = space(1)
          .w_GPFLDOCU = space(1)
          .w_GPFLINTE = space(1)
          .w_GPFLACCO = space(1)
          .w_CODUB2 = space(20)
          .w_CODMA2 = space(5)
          .w_CODCEN_OR = space(15)
          .w_CODCOM_OR = space(15)
          .w_CODATT_OR = space(15)
          .w_FLUSEP = space(1)
          .w_OPERAT = space(1)
          .w_OPERA3 = space(1)
          .w_MOLTIP = 0
          .w_QTAUM1 = 0
          .w_UNILOG = space(18)
          .w_TIPCO1 = space(5)
          .w_TIPCO2 = space(5)
          .w_TIPCO3 = space(3)
          .w_TPCON1 = space(3)
          .w_TPCON2 = space(3)
          .w_TPCON3 = space(3)
          .w_FLESUL = space(1)
          .w_FLCOVA = space(1)
          .w_FLFRAZ1 = space(1)
          .w_MODUM2 = space(1)
          .w_DOQTAEVA = 0
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_GPCODICE = NVL(GPCODICE,space(20))
          if link_2_2_joined
            this.w_GPCODICE = NVL(CACODICE202,NVL(this.w_GPCODICE,space(20)))
            this.w_DESART = NVL(CADESART202,space(40))
            this.w_CODART = NVL(CACODART202,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS202,space(3))
            this.w_MOLTI3 = NVL(CAMOLTIP202,0)
            this.w_TIPRIG = NVL(CA__TIPO202,space(1))
            this.w_DESAR1 = NVL(CADESART202,space(40))
            this.w_LENSCF = NVL(CALENSCF202,0)
            this.w_OPERA3 = NVL(CAOPERAT202,space(1))
            this.w_TIPCO3 = NVL(CATIPCO3202,space(3))
            this.w_TPCON3 = NVL(CATPCON3202,space(3))
          else
          .link_2_2('Load')
          endif
          .w_GPCODMAT = NVL(GPCODMAT,space(40))
          .link_2_4('Load')
          .w_GPUNIMIS = NVL(GPUNIMIS,space(3))
          if link_2_6_joined
            this.w_GPUNIMIS = NVL(UMCODICE206,NVL(this.w_GPUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ206,space(1))
          else
          .link_2_6('Load')
          endif
          .w_GPQTAMOV = NVL(GPQTAMOV,0)
          .w_GPPREZZO = NVL(GPPREZZO,0)
          .w_GP_ESCLU = NVL(GP_ESCLU,space(1))
          .w_GPEVASIO = NVL(GPEVASIO,space(1))
        .w_OLDQTA = .w_GPQTAMOV
        .w_MAGPRE = iif(.w_GESMAT='S',SPACE(5),.w_MAGPRE)
          .w_GPCODMAG = NVL(GPCODMAG,space(5))
          if link_2_17_joined
            this.w_GPCODMAG = NVL(MGCODMAG217,NVL(this.w_GPCODMAG,space(5)))
            this.w_FLUBIC = NVL(MGFLUBIC217,space(1))
          else
          .link_2_17('Load')
          endif
          .w_GPCODMA2 = NVL(GPCODMA2,space(5))
          if link_2_18_joined
            this.w_GPCODMA2 = NVL(MGCODMAG218,NVL(this.w_GPCODMA2,space(5)))
            this.w_F2UBIC = NVL(MGFLUBIC218,space(1))
          else
          .link_2_18('Load')
          endif
          .w_GPCODUBI = NVL(GPCODUBI,space(20))
          * evitabile
          *.link_2_19('Load')
          .w_GPCODUB2 = NVL(GPCODUB2,space(20))
          * evitabile
          *.link_2_20('Load')
          .w_GPCODLOT = NVL(GPCODLOT,space(20))
          .link_2_21('Load')
          .w_GPQTAUM1 = NVL(GPQTAUM1,0)
          .w_GPCODCEN = NVL(GPCODCEN,space(15))
          if link_2_23_joined
            this.w_GPCODCEN = NVL(CC_CONTO223,NVL(this.w_GPCODCEN,space(15)))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO223),ctod("  /  /  "))
          else
          .link_2_23('Load')
          endif
        .w_MTMAGCAR = IIF(.w_FLCASC='+' Or .w_FLRISE='-' Or .w_F2CASC='+' Or .w_F2RISE='-',.w_GPCODMAG,SPACE(5))
          .w_GPCODCOM = NVL(GPCODCOM,space(15))
          if link_2_25_joined
            this.w_GPCODCOM = NVL(CNCODCAN225,NVL(this.w_GPCODCOM,space(15)))
            this.w_DATOBSO = NVL(cp_ToDate(CNDTOBSO225),ctod("  /  /  "))
          else
          .link_2_25('Load')
          endif
        .w_MTFLCARI = IIF(NOT EMPTY(.w_MTMAGCAR),IIF(.w_FLCASC='+' OR .w_F2CASC='+','E','R'),' ')
        .w_MTMAGSCA = IIF(.w_FLCASC='-' Or .w_FLRISE='+',.w_GPCODMAG,SPACE(5))
          .w_GPCODATT = NVL(GPCODATT,space(15))
          * evitabile
          *.link_2_29('Load')
        .w_ESIRIS =  IIF( Not Empty( .w_MTFLSCAR) , .w_MTFLSCAR , .w_MTFLCARI )
          .link_2_32('Load')
        .w_MTKEYSAL = .w_CODART
          .link_2_35('Load')
        .w_MxFLLOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_FLUBIC='S'), LEFT(ALLTRIM(.w_FLCASC)+IIF(.w_FLRISE='+', '-', IIF(.w_FLRISE='-', '+', ' ')), 1), ' ')
        .w_MxF2LOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_F2UBIC='S'), LEFT(ALLTRIM(.w_F2CASC)+IIF(.w_F2RISE='+', '-', IIF(.w_F2RISE='-', '+', ' ')), 1), ' ')
        .w_LOTZOOM = .F.
        .oPgFrm.Page1.oPag.oObj_2_50.Calculate(.w_CAPTION,.w_TEXTCOLOR,.w_BACKCOLOR)
        .w_OMAG = IIF(EMPTY(.w_GPCODMAG), IIF(EMPTY(.w_OMAG),IIF(EMPTY(.w_COMAG), g_MAGAZI, .w_COMAG),.w_OMAG), .w_GPCODMAG)
        .w_OMAT = IIF(EMPTY(.w_GPCODMAT), IIF(EMPTY(.w_OMAT),IIF(EMPTY(.w_COMAT), g_MAGAZI, .w_COMAT),.w_OMAT), .w_GPCODMAT)
          .w_GPSERRIF = NVL(GPSERRIF,space(10))
          .w_GPROWRIF = NVL(GPROWRIF,0)
        .w_MTFLSCAR = IIF(NOT EMPTY(.w_MTMAGSCA),IIF(.w_FLCASC='-' ,'E','R'),' ')
        .w_MAGUBI = IIF(Not Empty( .w_MTMAGCAR),IIF( .w_MTMAGCAR = .w_GPCODMAT , .w_GPCODMAT , .w_GPCODMAG ),IIF( .w_MTMAGSCA =.w_GPCODMAT , .w_GPCODMAT , .w_GPCODMAG ))
        .w_ROWAGG = 'A'
        .w_NODOCU = 'S'
        .oPgFrm.Page1.oPag.oObj_2_89.Calculate(IIF(EMPTY(.w_GPSERRIF), '','Doc Evaso:'))
        .oPgFrm.Page1.oPag.oObj_2_90.Calculate(IIF(EMPTY(.w_GPSERRIF),'', 'n.:'))
        .oPgFrm.Page1.oPag.oObj_2_91.Calculate(IIF(EMPTY(.w_GPSERRIF),'', '/'))
        .oPgFrm.Page1.oPag.oObj_2_92.Calculate(IIF(EMPTY(.w_GPSERRIF),'', 'del'))
        .w_NOMSG = .T.
        .oPgFrm.Page1.oPag.oObj_2_101.Calculate(IIF(.w_GP_ESCLU='S', '<Riga Esclusa>',IIF(.w_GPEVASIO='S','<Riga a Saldo>','')))
          .w_GPUNILOG = NVL(GPUNILOG,space(18))
          * evitabile
          *.link_2_130('Load')
        .w_CODCONF = IIF(.w_GPUNIMIS=.w_UNMIS3,.w_TPCON3,IIF(.w_GPUNIMIS=.w_UNMIS2,.w_TPCON2,.w_TPCON1))
          .link_2_139('Load')
          .w_GPCODCOL = NVL(GPCODCOL,space(5))
          * evitabile
          *.link_2_141('Load')
          .w_GPNUMCOL = NVL(GPNUMCOL,0)
        .w_MVCODART = .w_CODART
        .w_MVCODLOT = .w_GPCODLOT
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .w_OBTEST = .w_DATREG
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .w_OCEN = IIF(EMPTY(.w_GPCODCEN), .w_OCEN, .w_GPCODCEN)
        .w_OCOM = IIF(EMPTY(.w_GPCODCOM), .w_OCEN, .w_GPCODCOM)
        .w_OATT = IIF(EMPTY(.w_GPCODATT), .w_OATT, .w_GPCODATT)
        .w_CODFOR = iif(.w_CODMAG=.w_MTMAGCAR And .w_TIPCON='F',.w_CODCON,SPACE(15))
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .w_NOMSG = .T.
        .w_DOCCOLL = READCOLL(.w_TIPDOC)
        .w_ERRORCOD = iif(.w_TIPOOBJ_GEST$ 'A-O-V',  'Codice incongruente o inesistente o obsoleto', 'Articolo inesistente')
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsar_mpg
    * Per evitare lo sbiancamento dei messaggi
    * a seguito della chiamata di Driver
    Local L_OldMsg
    L_OldMsg=This.w_MSG
    
    this.Shift_Column()
    
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_GPSERIAL=space(10)
      .w_CPROWORD=10
      .w_GPCODICE=space(20)
      .w_GPCODMAT=space(40)
      .w_CODART=space(20)
      .w_GESMAT=space(1)
      .w_GPUNIMIS=space(3)
      .w_GPQTAMOV=0
      .w_GPPREZZO=0
      .w_QTAORI=0
      .w_GP_ESCLU=space(1)
      .w_GPEVASIO=space(1)
      .w_OLDQTA=0
      .w_MAGPRE=space(5)
      .w_FLUBIC=space(1)
      .w_F2UBIC=space(1)
      .w_GPCODMAG=space(5)
      .w_GPCODMA2=space(5)
      .w_GPCODUBI=space(20)
      .w_GPCODUB2=space(20)
      .w_GPCODLOT=space(20)
      .w_GPQTAUM1=0
      .w_GPCODCEN=space(15)
      .w_MTMAGCAR=space(5)
      .w_GPCODCOM=space(15)
      .w_MTFLCARI=space(1)
      .w_TIPATT=space(1)
      .w_MTMAGSCA=space(5)
      .w_GPCODATT=space(15)
      .w_ESIRIS=space(1)
      .w_DESART=space(40)
      .w_EVTIPDOC=space(5)
      .w_MTKEYSAL=space(20)
      .w_EVNUMDOC=0
      .w_UNMIS1=space(3)
      .w_EVALFDOC=space(10)
      .w_FLSERG=space(1)
      .w_NOMSGCHECK=space(1)
      .w_EVDATDOC=ctod("  /  /  ")
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_FLSTAT=space(1)
      .w_OBJ_GEST=space(10)
      .w_CODCON=space(15)
      .w_DATREG=ctod("  /  /  ")
      .w_FLLOTT=space(1)
      .w_MOLTI3=0
      .w_FLCASC=space(1)
      .w_FLRISE=space(1)
      .w_MxFLLOTT=space(1)
      .w_F2CASC=space(1)
      .w_F2RISE=space(1)
      .w_MxF2LOTT=space(1)
      .w_STATOROW=space(1)
      .w_FLPRG=space(1)
      .w_LOTZOOM=.f.
      .w_UBIZOOM=.f.
      .w_OBTEST=ctod("  /  /  ")
      .w_TEST=.f.
      .w_CAPTION=space(50)
      .w_TEXTCOLOR=0
      .w_TIPOOBJ_GEST=space(1)
      .w_Msg=space(0)
      .w_CODMAGTES=space(5)
      .w_DESMAGTES=space(30)
      .w_FLUBICTES=space(1)
      .w_UBITES=space(20)
      .w_CODMA2TES=space(5)
      .w_UB2TES=space(20)
      .w_CODCENTES=space(15)
      .w_CODCOMTES=space(15)
      .w_CODATTTES=space(15)
      .w_UBIDESTES=space(40)
      .w_ARTZOOM=space(1)
      .w_FLMGPR=space(1)
      .w_COMAG=space(5)
      .w_MAGTER=space(5)
      .w_OMAG=space(5)
      .w_OMAT=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DATOBS2=ctod("  /  /  ")
      .w_DESPIA=space(40)
      .w_FLANAL=space(1)
      .w_OCEN=space(15)
      .w_FLGCOM=space(1)
      .w_OCOM=space(15)
      .w_DESCAN=space(40)
      .w_OATT=space(15)
      .w_ATDES=space(30)
      .w_ESEGUITOIMPORT=.f.
      .w_TIPRIG=space(1)
      .w_FLVEAC=space(1)
      .w_FLFRAZ=space(1)
      .w_TIPORN=space(1)
      .w_CODORN=space(15)
      .w_TIPCON=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_FLSCOR=space(1)
      .w_MVFLSCOR=space(1)
      .w_TIPDOC=space(5)
      .w_FILART=space(20)
      .w_EVAINI=ctod("  /  /  ")
      .w_EVAFIN=ctod("  /  /  ")
      .w_ESCL1=space(3)
      .w_ESCL2=space(3)
      .w_ESCL3=space(3)
      .w_ESCL4=space(3)
      .w_ESCL5=space(3)
      .w_FILARM=space(20)
      .w_FILCLI=space(15)
      .w_FILCAU=space(5)
      .w_SERIAL=space(10)
      .w_FILMAG=space(5)
      .w_FILCOMM=space(15)
      .w_FILATTI=space(15)
      .w_FILCEN=space(15)
      .w_FILVOC=space(15)
      .w_CODICE=space(20)
      .w_UNIMIS=space(3)
      .w_PREZZO=0
      .w_QTAMOV=0
      .w_CODLOT=space(20)
      .w_CODUBI=space(20)
      .w_CODMAT=space(40)
      .w_CODMAG=space(5)
      .w_GPSERRIF=space(10)
      .w_GPROWRIF=0
      .w_DESAR1=space(40)
      .w_CODFOR=space(15)
      .w_MTFLSCAR=space(1)
      .w_MAGUBI=space(5)
      .w_MOVMAT=.f.
      .w_ULTPROG=0
      .w_SECODUBI=space(20)
      .w_SECODLOT=space(20)
      .w_MT__FLAG=space(1)
      .w_OLDPRE=0
      .w_ROWAGG=space(1)
      .w_NODOCU=space(1)
      .w_MT_SALDO=0
      .w_NOMSG=.f.
      .w_DATSCA=ctod("  /  /  ")
      .w_MTSERRIF=space(10)
      .w_MTROWRIF=0
      .w_MTRIFNUM=0
      .w_DOCCOLL=space(5)
      .w_EVROWORD=0
      .w_DOQTAEV1=0
      .w_DOIMPEVA=0
      .w_FLRRIF=space(1)
      .w_PARAME=space(3)
      .w_BACKCOLOR=0
      .w_FLAGRIS=space(1)
      .w_FLVSRI=space(1)
      .w_FLRIDE=space(1)
      .w_FLNSRI=space(1)
      .w_CODESC=space(5)
      .w_LENSCF=0
      .w_TPRDES=space(3)
      .w_TPVSRI=space(3)
      .w_TPNSRI=space(3)
      .w_MTRIFSTO=0
      .w_MTRIFSTO=0
      .w_FLGROR=space(1)
      .w_SCONT1=0
      .w_SCONT2=0
      .w_SCONT3=0
      .w_SCONT4=0
      .w_CAUMAG=space(5)
      .w_CAUCOL=space(5)
      .w_DISLOT=space(1)
      .w_GPFLDOCU=space(1)
      .w_GPFLINTE=space(1)
      .w_GPFLACCO=space(1)
      .w_CODUB2=space(20)
      .w_CODMA2=space(5)
      .w_COMAT=space(5)
      .w_DESMA2TES=space(30)
      .w_UB2DESTES=space(40)
      .w_FLUBI2TES=space(1)
      .w_CODCEN_OR=space(15)
      .w_CODCOM_OR=space(15)
      .w_CODATT_OR=space(15)
      .w_FLUSEP=space(1)
      .w_OPERAT=space(1)
      .w_OPERA3=space(1)
      .w_MOLTIP=0
      .w_QTAUM1=0
      .w_GPUNILOG=space(18)
      .w_UNILOG=space(18)
      .w_TIPCO1=space(5)
      .w_TIPCO2=space(5)
      .w_TIPCO3=space(3)
      .w_TPCON1=space(3)
      .w_TPCON2=space(3)
      .w_TPCON3=space(3)
      .w_FLPACK=space(1)
      .w_FLESUL=space(1)
      .w_CODCONF=space(3)
      .w_FLCOVA=space(1)
      .w_GPCODCOL=space(5)
      .w_GPNUMCOL=0
      .w_MVCODART=space(20)
      .w_MVCODLOT=space(20)
      .w_FLFRAZ1=space(1)
      .w_MODUM2=space(1)
      .w_FLFOM=space(1)
      .w_DOQTAEVA=0
      .w_F2ORDI=space(1)
      .w_F2IMPE=space(1)
      .w_FLMTPR=space(1)
      .w_FLORDI=space(1)
      .w_FLIMPE=space(1)
      .w_FLRICIVA=.f.
      .w_RIIVDTIN=ctod("  /  /  ")
      .w_RIIVDTFI=ctod("  /  /  ")
      .w_RIIVLSIV=space(0)
      .w_ERRORCOD=space(200)
      .w_MVAGG_01=space(15)
      .w_MVAGG_02=space(15)
      .w_MVAGG_03=space(15)
      .w_MVAGG_04=space(15)
      .w_MVAGG_05=ctod("  /  /  ")
      .w_MVAGG_06=ctod("  /  /  ")
      .w_TDFLIA01=space(1)
      .w_TDFLIA02=space(1)
      .w_TDFLIA03=space(1)
      .w_TDFLIA04=space(1)
      .w_TDFLIA05=space(1)
      .w_TDFLIA06=space(1)
      .w_TDFLRA01=space(1)
      .w_TDFLRA02=space(1)
      .w_TDFLRA03=space(1)
      .w_TDFLRA04=space(1)
      .w_TDFLRA05=space(1)
      .w_TDFLRA06=space(1)
      .w_DACAM_01=space(30)
      .w_DACAM_02=space(30)
      .w_DACAM_03=space(30)
      .w_DACAM_04=space(30)
      .w_DACAM_05=space(30)
      .w_DACAM_06=space(30)
      .w_COMCAR=space(15)
      .w_COMSCA=space(15)
      if .cFunction<>"Filter"
        .w_GPSERIAL = '000000001'
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_GPCODICE))
         .link_2_2('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_CODART))
         .link_2_4('Full')
        endif
        .DoRTCalc(6,6,.f.)
        .w_GPUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_GPUNIMIS))
         .link_2_6('Full')
        endif
        .w_GPQTAMOV = IIF(.w_TIPRIG='F', 1, IIF(.w_TIPRIG='D', 0, .w_GPQTAMOV))
        .DoRTCalc(9,11,.f.)
        .w_GPEVASIO = IIF(.w_GPQTAMOV>=.w_QTAORI AND NOT EMPTY(.w_GPSERRIF) ,'S',' ')
        .w_OLDQTA = .w_GPQTAMOV
        .w_MAGPRE = iif(.w_GESMAT='S',SPACE(5),.w_MAGPRE)
        .DoRTCalc(15,16,.f.)
        .w_GPCODMAG = IIF(Not .w_TIPOOBJ_GEST$'PR'  , CALCMAG(1, .w_FLMGPR, '     ', .w_COMAG, .w_OMAG, .w_MAGPRE, .w_MAGTER) ,Space(5))
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_GPCODMAG))
         .link_2_17('Full')
        endif
        .w_GPCODMA2 = IIF(.w_TIPRIG='R' AND NOT EMPTY(.w_F2CASC+.w_F2RISE+.w_F2ORDI+.w_F2IMPE), CALCMAG(1, .w_FLMTPR, '     ', .w_COMAT, .w_OMAT, .w_MAGPRE, .w_MAGTER), SPACE(5))
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_GPCODMA2))
         .link_2_18('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_GPCODUBI))
         .link_2_19('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_GPCODUB2))
         .link_2_20('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_GPCODLOT))
         .link_2_21('Full')
        endif
        .w_GPQTAUM1 = IIF(.w_TIPRIG='F', 1, CALQTAADV(.w_GPQTAMOV,.w_GPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,this, "GPQTAMOV"))
        .w_GPCODCEN = .w_OCEN
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_GPCODCEN))
         .link_2_23('Full')
        endif
        .w_MTMAGCAR = IIF(.w_FLCASC='+' Or .w_FLRISE='-' Or .w_F2CASC='+' Or .w_F2RISE='-',.w_GPCODMAG,SPACE(5))
        .w_GPCODCOM = .w_OCOM
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_GPCODCOM))
         .link_2_25('Full')
        endif
        .w_MTFLCARI = IIF(NOT EMPTY(.w_MTMAGCAR),IIF(.w_FLCASC='+' OR .w_F2CASC='+','E','R'),' ')
        .w_TIPATT = 'A'
        .w_MTMAGSCA = IIF(.w_FLCASC='-' Or .w_FLRISE='+',.w_GPCODMAG,SPACE(5))
        .w_GPCODATT = .w_OATT
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_GPCODATT))
         .link_2_29('Full')
        endif
        .w_ESIRIS =  IIF( Not Empty( .w_MTFLSCAR) , .w_MTFLSCAR , .w_MTFLCARI )
        .DoRTCalc(31,32,.f.)
        if not(empty(.w_EVTIPDOC))
         .link_2_32('Full')
        endif
        .w_MTKEYSAL = .w_CODART
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .DoRTCalc(34,35,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_35('Full')
        endif
        .DoRTCalc(36,37,.f.)
        .w_NOMSGCHECK = 'N'
        .DoRTCalc(39,42,.f.)
        .w_OBJ_GEST = Obj_Gest
        .DoRTCalc(44,49,.f.)
        .w_MxFLLOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_FLUBIC='S'), LEFT(ALLTRIM(.w_FLCASC)+IIF(.w_FLRISE='+', '-', IIF(.w_FLRISE='-', '+', ' ')), 1), ' ')
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .DoRTCalc(51,52,.f.)
        .w_MxF2LOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_F2UBIC='S'), LEFT(ALLTRIM(.w_F2CASC)+IIF(.w_F2RISE='+', '-', IIF(.w_F2RISE='-', '+', ' ')), 1), ' ')
        .w_STATOROW = 'A'
        .w_FLPRG = 'P'
        .w_LOTZOOM = .F.
        .w_UBIZOOM = .T.
        .w_OBTEST = .w_DATREG
        .w_TEST = .T.
        .oPgFrm.Page1.oPag.oObj_2_50.Calculate(.w_CAPTION,.w_TEXTCOLOR,.w_BACKCOLOR)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .DoRTCalc(60,61,.f.)
        .w_TIPOOBJ_GEST = IIF(Lower ( .w_OBJ_GEST.class ) = 'tgsps_mvd','P',IIF(Lower ( .w_OBJ_GEST.class ) = 'tgsve_mdv','V',IIF(Lower ( .w_OBJ_GEST.class ) = 'tgsac_mdv','A',IIF(Lower ( .w_OBJ_GEST.class ) = 'tgsor_mdv','O',IIF(Lower ( .w_OBJ_GEST.class ) = 'tgsma_mvm','M','R')))))
        .DoRTCalc(63,64,.f.)
        if not(empty(.w_CODMAGTES))
         .link_4_2('Full')
        endif
        .DoRTCalc(65,67,.f.)
        if not(empty(.w_UBITES))
         .link_4_6('Full')
        endif
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_CODMA2TES))
         .link_4_8('Full')
        endif
        .DoRTCalc(69,69,.f.)
        if not(empty(.w_UB2TES))
         .link_4_9('Full')
        endif
        .DoRTCalc(70,70,.f.)
        if not(empty(.w_CODCENTES))
         .link_4_11('Full')
        endif
        .DoRTCalc(71,71,.f.)
        if not(empty(.w_CODCOMTES))
         .link_4_12('Full')
        endif
        .DoRTCalc(72,72,.f.)
        if not(empty(.w_CODATTTES))
         .link_4_13('Full')
        endif
        .DoRTCalc(73,73,.f.)
        .w_ARTZOOM = IIF(.w_TIPOOBJ_GEST='P','S',IIF(.w_TIPOOBJ_GEST$'M-R','M','N'))
        .DoRTCalc(75,77,.f.)
        .w_OMAG = IIF(EMPTY(.w_GPCODMAG), IIF(EMPTY(.w_OMAG),IIF(EMPTY(.w_COMAG), g_MAGAZI, .w_COMAG),.w_OMAG), .w_GPCODMAG)
        .w_OMAT = IIF(EMPTY(.w_GPCODMAT), IIF(EMPTY(.w_OMAT),IIF(EMPTY(.w_COMAT), g_MAGAZI, .w_COMAT),.w_OMAT), .w_GPCODMAT)
        .DoRTCalc(80,83,.f.)
        .w_OCEN = IIF(EMPTY(.w_GPCODCEN), .w_OCEN, .w_GPCODCEN)
        .DoRTCalc(85,85,.f.)
        .w_OCOM = IIF(EMPTY(.w_GPCODCOM), .w_OCEN, .w_GPCODCOM)
        .DoRTCalc(87,87,.f.)
        .w_OATT = IIF(EMPTY(.w_GPCODATT), .w_OATT, .w_GPCODATT)
        .DoRTCalc(89,89,.f.)
        .w_ESEGUITOIMPORT = .F.
        .DoRTCalc(91,129,.f.)
        .w_CODFOR = iif(.w_CODMAG=.w_MTMAGCAR And .w_TIPCON='F',.w_CODCON,SPACE(15))
        .w_MTFLSCAR = IIF(NOT EMPTY(.w_MTMAGSCA),IIF(.w_FLCASC='-' ,'E','R'),' ')
        .w_MAGUBI = IIF(Not Empty( .w_MTMAGCAR),IIF( .w_MTMAGCAR = .w_GPCODMAT , .w_GPCODMAT , .w_GPCODMAG ),IIF( .w_MTMAGSCA =.w_GPCODMAT , .w_GPCODMAT , .w_GPCODMAG ))
        .w_MOVMAT = .t.
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .DoRTCalc(134,138,.f.)
        .w_ROWAGG = 'A'
        .w_NODOCU = 'S'
        .oPgFrm.Page1.oPag.oObj_2_89.Calculate(IIF(EMPTY(.w_GPSERRIF), '','Doc Evaso:'))
        .oPgFrm.Page1.oPag.oObj_2_90.Calculate(IIF(EMPTY(.w_GPSERRIF),'', 'n.:'))
        .oPgFrm.Page1.oPag.oObj_2_91.Calculate(IIF(EMPTY(.w_GPSERRIF),'', '/'))
        .oPgFrm.Page1.oPag.oObj_2_92.Calculate(IIF(EMPTY(.w_GPSERRIF),'', 'del'))
        .DoRTCalc(141,141,.f.)
        .w_NOMSG = .T.
        .DoRTCalc(143,146,.f.)
        .w_DOCCOLL = READCOLL(.w_TIPDOC)
        .oPgFrm.Page1.oPag.oObj_2_101.Calculate(IIF(.w_GP_ESCLU='S', '<Riga Esclusa>',IIF(.w_GPEVASIO='S','<Riga a Saldo>','')))
        .DoRTCalc(148,152,.f.)
        .w_BACKCOLOR = .backcolor
        .DoRTCalc(154,190,.f.)
        if not(empty(.w_GPUNILOG))
         .link_2_130('Full')
        endif
        .DoRTCalc(191,199,.f.)
        .w_CODCONF = IIF(.w_GPUNIMIS=.w_UNMIS3,.w_TPCON3,IIF(.w_GPUNIMIS=.w_UNMIS2,.w_TPCON2,.w_TPCON1))
        .DoRTCalc(200,200,.f.)
        if not(empty(.w_CODCONF))
         .link_2_139('Full')
        endif
        .DoRTCalc(201,201,.f.)
        .w_GPCODCOL = IIF(.w_GPUNIMIS=.w_UNMIS3,.w_TIPCO3,IIF(.w_GPUNIMIS=.w_UNMIS2,.w_TIPCO2,.w_TIPCO1))
        .DoRTCalc(202,202,.f.)
        if not(empty(.w_GPCODCOL))
         .link_2_141('Full')
        endif
        .DoRTCalc(203,203,.f.)
        .w_MVCODART = .w_CODART
        .w_MVCODLOT = .w_GPCODLOT
        .DoRTCalc(206,214,.f.)
        .w_FLRICIVA = iif(Isalt(),.t.,.f.)
        .DoRTCalc(216,218,.f.)
        .w_ERRORCOD = iif(.w_TIPOOBJ_GEST$ 'A-O-V',  'Codice incongruente o inesistente o obsoleto', 'Articolo inesistente')
        .DoRTCalc(220,243,.f.)
        .w_COMCAR = .w_GPCODCOM
        .w_COMSCA = .w_GPCODCOM
      endif
    endwith
    cp_BlankRecExtFlds(this,'GESPENNA')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_mpg
    * Per evitare lo sbiancamento dei messaggi
    * a seguito della chiamata di Driver
    This.w_MSG=L_OldMsg
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGPCODMAG_2_17.enabled = i_bVal
      .Page1.oPag.oGPCODMA2_2_18.enabled = i_bVal
      .Page1.oPag.oGPCODUBI_2_19.enabled = i_bVal
      .Page1.oPag.oGPCODUB2_2_20.enabled = i_bVal
      .Page1.oPag.oGPCODLOT_2_21.enabled = i_bVal
      .Page1.oPag.oGPQTAUM1_2_22.enabled = i_bVal
      .Page1.oPag.oGPCODCEN_2_23.enabled = i_bVal
      .Page1.oPag.oGPCODCOM_2_25.enabled = i_bVal
      .Page1.oPag.oGPCODATT_2_29.enabled = i_bVal
      .Page3.oPag.oMsg_5_1.enabled = i_bVal
      .Page2.oPag.oCODMAGTES_4_2.enabled = i_bVal
      .Page2.oPag.oUBITES_4_6.enabled = i_bVal
      .Page2.oPag.oCODMA2TES_4_8.enabled = i_bVal
      .Page2.oPag.oUB2TES_4_9.enabled = i_bVal
      .Page2.oPag.oCODCENTES_4_11.enabled = i_bVal
      .Page2.oPag.oCODCOMTES_4_12.enabled = i_bVal
      .Page2.oPag.oCODATTTES_4_13.enabled = i_bVal
      .Page1.oPag.oESEGUITOIMPORT_3_1.enabled = i_bVal
      .Page2.oPag.oFLVSRI_4_24.enabled = i_bVal
      .Page2.oPag.oFLRIDE_4_25.enabled = i_bVal
      .Page2.oPag.oFLNSRI_4_26.enabled = i_bVal
      .Page1.oPag.oGPUNILOG_2_130.enabled = i_bVal
      .Page1.oPag.oBtn_2_54.enabled = i_bVal
      .Page2.oPag.oBtn_4_7.enabled = i_bVal
      .Page2.oPag.oBtn_4_10.enabled = i_bVal
      .Page2.oPag.oBtn_4_15.enabled = i_bVal
      .Page3.oPag.oBtn_5_2.enabled = i_bVal
      .Page1.oPag.oBtn_3_2.enabled = i_bVal
      .Page1.oPag.oBtn_3_3.enabled = i_bVal
      .Page1.oPag.oBtn_3_4.enabled = i_bVal
      .Page1.oPag.oBtn_2_73.enabled = i_bVal
      .Page1.oPag.oBtn_2_98.enabled = i_bVal
      .Page1.oPag.oBtn_2_105.enabled = i_bVal
      .Page1.oPag.oObj_1_7.enabled = i_bVal
      .Page1.oPag.oObj_1_16.enabled = i_bVal
      .Page1.oPag.oObj_1_19.enabled = i_bVal
      .Page1.oPag.oObj_1_66.enabled = i_bVal
      .Page1.oPag.oObj_1_67.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'GESPENNA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsar_mpg
    * disabilito il tab elenco
    this.oPgFrm.Pages(this.oPgFrm.PageCount).enabled=.f.
    this.oPgFrm.Pages(this.oPgFrm.PageCount).Caption=""
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GESPENNA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPSERIAL,"GPSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GESPENNA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GESPENNA_IDX,2])
    i_lTable = "GESPENNA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GESPENNA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_GPCODICE C(20);
      ,t_GPCODMAT C(40);
      ,t_GPUNIMIS C(3);
      ,t_GPQTAMOV N(12,3);
      ,t_GPPREZZO N(18,5);
      ,t_GP_ESCLU N(3);
      ,t_GPEVASIO N(3);
      ,t_GPCODMAG C(5);
      ,t_GPCODMA2 C(5);
      ,t_GPCODUBI C(20);
      ,t_GPCODUB2 C(20);
      ,t_GPCODLOT C(20);
      ,t_GPQTAUM1 N(12,3);
      ,t_GPCODCEN C(15);
      ,t_GPCODCOM C(15);
      ,t_GPCODATT C(15);
      ,t_DESART C(40);
      ,t_EVTIPDOC C(5);
      ,t_EVNUMDOC N(15);
      ,t_UNMIS1 C(3);
      ,t_EVALFDOC C(10);
      ,t_EVDATDOC D(8);
      ,t_DESAR1 C(40);
      ,t_EVROWORD N(5);
      ,t_GPUNILOG C(18);
      ,CPROWNUM N(10);
      ,t_CODART C(20);
      ,t_GESMAT C(1);
      ,t_QTAORI N(12,3);
      ,t_OLDQTA N(12,3);
      ,t_MAGPRE C(5);
      ,t_FLUBIC C(1);
      ,t_F2UBIC C(1);
      ,t_MTMAGCAR C(5);
      ,t_MTFLCARI C(1);
      ,t_TIPATT C(1);
      ,t_MTMAGSCA C(5);
      ,t_ESIRIS C(1);
      ,t_MTKEYSAL C(20);
      ,t_FLSERG C(1);
      ,t_UNMIS2 C(3);
      ,t_UNMIS3 C(3);
      ,t_FLSTAT C(1);
      ,t_FLLOTT C(1);
      ,t_MOLTI3 N(10,4);
      ,t_MxFLLOTT C(1);
      ,t_MxF2LOTT C(1);
      ,t_STATOROW C(1);
      ,t_LOTZOOM L(1);
      ,t_UBIZOOM L(1);
      ,t_TEST L(1);
      ,t_CAPTION C(50);
      ,t_TEXTCOLOR N(10);
      ,t_OMAG C(5);
      ,t_OMAT C(5);
      ,t_DATOBSO D(8);
      ,t_TIPRIG C(1);
      ,t_FLFRAZ C(1);
      ,t_CODICE C(20);
      ,t_UNIMIS C(3);
      ,t_PREZZO N(18,5);
      ,t_QTAMOV N(12,3);
      ,t_CODLOT C(20);
      ,t_CODUBI C(20);
      ,t_CODMAT C(40);
      ,t_CODMAG C(5);
      ,t_GPSERRIF C(10);
      ,t_GPROWRIF N(4);
      ,t_MTFLSCAR C(1);
      ,t_MAGUBI C(5);
      ,t_MT__FLAG C(1);
      ,t_OLDPRE N(18,5);
      ,t_ROWAGG C(1);
      ,t_NODOCU C(1);
      ,t_MT_SALDO N(1);
      ,t_NOMSG L(1);
      ,t_DATSCA D(8);
      ,t_MTSERRIF C(10);
      ,t_MTROWRIF N(4);
      ,t_MTRIFNUM N(4);
      ,t_DOQTAEV1 N(12,3);
      ,t_DOIMPEVA N(18,4);
      ,t_FLRRIF C(1);
      ,t_PARAME C(3);
      ,t_FLAGRIS C(1);
      ,t_LENSCF N(1);
      ,t_MTRIFSTO N(4);
      ,t_FLGROR C(1);
      ,t_SCONT1 N(6,2);
      ,t_SCONT2 N(6,2);
      ,t_SCONT3 N(6,2);
      ,t_SCONT4 N(6,2);
      ,t_DISLOT C(1);
      ,t_GPFLDOCU C(1);
      ,t_GPFLINTE C(1);
      ,t_GPFLACCO C(1);
      ,t_CODUB2 C(20);
      ,t_CODMA2 C(5);
      ,t_CODCEN_OR C(15);
      ,t_CODCOM_OR C(15);
      ,t_CODATT_OR C(15);
      ,t_FLUSEP C(1);
      ,t_OPERAT C(1);
      ,t_OPERA3 C(1);
      ,t_MOLTIP N(10,4);
      ,t_QTAUM1 N(12,3);
      ,t_UNILOG C(18);
      ,t_TIPCO1 C(5);
      ,t_TIPCO2 C(5);
      ,t_TIPCO3 C(3);
      ,t_TPCON1 C(3);
      ,t_TPCON2 C(3);
      ,t_TPCON3 C(3);
      ,t_FLESUL C(1);
      ,t_CODCONF C(3);
      ,t_FLCOVA C(1);
      ,t_GPCODCOL C(5);
      ,t_GPNUMCOL N(5);
      ,t_MVCODART C(20);
      ,t_MVCODLOT C(20);
      ,t_FLFRAZ1 C(1);
      ,t_MODUM2 C(1);
      ,t_DOQTAEVA N(12,3);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mpgbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGPCODICE_2_2.controlsource=this.cTrsName+'.t_GPCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGPCODMAT_2_3.controlsource=this.cTrsName+'.t_GPCODMAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGPUNIMIS_2_6.controlsource=this.cTrsName+'.t_GPUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGPQTAMOV_2_7.controlsource=this.cTrsName+'.t_GPQTAMOV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGPPREZZO_2_8.controlsource=this.cTrsName+'.t_GPPREZZO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGP_ESCLU_2_10.controlsource=this.cTrsName+'.t_GP_ESCLU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGPEVASIO_2_12.controlsource=this.cTrsName+'.t_GPEVASIO'
    this.oPgFRm.Page1.oPag.oGPCODMAG_2_17.controlsource=this.cTrsName+'.t_GPCODMAG'
    this.oPgFRm.Page1.oPag.oGPCODMA2_2_18.controlsource=this.cTrsName+'.t_GPCODMA2'
    this.oPgFRm.Page1.oPag.oGPCODUBI_2_19.controlsource=this.cTrsName+'.t_GPCODUBI'
    this.oPgFRm.Page1.oPag.oGPCODUB2_2_20.controlsource=this.cTrsName+'.t_GPCODUB2'
    this.oPgFRm.Page1.oPag.oGPCODLOT_2_21.controlsource=this.cTrsName+'.t_GPCODLOT'
    this.oPgFRm.Page1.oPag.oGPQTAUM1_2_22.controlsource=this.cTrsName+'.t_GPQTAUM1'
    this.oPgFRm.Page1.oPag.oGPCODCEN_2_23.controlsource=this.cTrsName+'.t_GPCODCEN'
    this.oPgFRm.Page1.oPag.oGPCODCOM_2_25.controlsource=this.cTrsName+'.t_GPCODCOM'
    this.oPgFRm.Page1.oPag.oGPCODATT_2_29.controlsource=this.cTrsName+'.t_GPCODATT'
    this.oPgFRm.Page1.oPag.oDESART_2_31.controlsource=this.cTrsName+'.t_DESART'
    this.oPgFRm.Page1.oPag.oEVTIPDOC_2_32.controlsource=this.cTrsName+'.t_EVTIPDOC'
    this.oPgFRm.Page1.oPag.oEVNUMDOC_2_34.controlsource=this.cTrsName+'.t_EVNUMDOC'
    this.oPgFRm.Page1.oPag.oUNMIS1_2_35.controlsource=this.cTrsName+'.t_UNMIS1'
    this.oPgFRm.Page1.oPag.oEVALFDOC_2_36.controlsource=this.cTrsName+'.t_EVALFDOC'
    this.oPgFRm.Page1.oPag.oEVDATDOC_2_38.controlsource=this.cTrsName+'.t_EVDATDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESAR1_2_79.controlsource=this.cTrsName+'.t_DESAR1'
    this.oPgFRm.Page1.oPag.oEVROWORD_2_100.controlsource=this.cTrsName+'.t_EVROWORD'
    this.oPgFRm.Page1.oPag.oGPUNILOG_2_130.controlsource=this.cTrsName+'.t_GPUNILOG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GESPENNA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GESPENNA_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GESPENNA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GESPENNA_IDX,2])
      *
      * insert into GESPENNA
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GESPENNA')
        i_extval=cp_InsertValODBCExtFlds(this,'GESPENNA')
        i_cFldBody=" "+;
                  "(GPSERIAL,CPROWORD,GPCODICE,GPCODMAT,GPUNIMIS"+;
                  ",GPQTAMOV,GPPREZZO,GP_ESCLU,GPEVASIO,GPCODMAG"+;
                  ",GPCODMA2,GPCODUBI,GPCODUB2,GPCODLOT,GPQTAUM1"+;
                  ",GPCODCEN,GPCODCOM,GPCODATT,GPSERRIF,GPROWRIF"+;
                  ",GPUNILOG,GPCODCOL,GPNUMCOL,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_GPSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_GPCODICE)+","+cp_ToStrODBC(this.w_GPCODMAT)+","+cp_ToStrODBCNull(this.w_GPUNIMIS)+;
             ","+cp_ToStrODBC(this.w_GPQTAMOV)+","+cp_ToStrODBC(this.w_GPPREZZO)+","+cp_ToStrODBC(this.w_GP_ESCLU)+","+cp_ToStrODBC(this.w_GPEVASIO)+","+cp_ToStrODBCNull(this.w_GPCODMAG)+;
             ","+cp_ToStrODBCNull(this.w_GPCODMA2)+","+cp_ToStrODBCNull(this.w_GPCODUBI)+","+cp_ToStrODBCNull(this.w_GPCODUB2)+","+cp_ToStrODBCNull(this.w_GPCODLOT)+","+cp_ToStrODBC(this.w_GPQTAUM1)+;
             ","+cp_ToStrODBCNull(this.w_GPCODCEN)+","+cp_ToStrODBCNull(this.w_GPCODCOM)+","+cp_ToStrODBCNull(this.w_GPCODATT)+","+cp_ToStrODBC(this.w_GPSERRIF)+","+cp_ToStrODBC(this.w_GPROWRIF)+;
             ","+cp_ToStrODBCNull(this.w_GPUNILOG)+","+cp_ToStrODBCNull(this.w_GPCODCOL)+","+cp_ToStrODBC(this.w_GPNUMCOL)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GESPENNA')
        i_extval=cp_InsertValVFPExtFlds(this,'GESPENNA')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'GPSERIAL',this.w_GPSERIAL)
        INSERT INTO (i_cTable) (;
                   GPSERIAL;
                  ,CPROWORD;
                  ,GPCODICE;
                  ,GPCODMAT;
                  ,GPUNIMIS;
                  ,GPQTAMOV;
                  ,GPPREZZO;
                  ,GP_ESCLU;
                  ,GPEVASIO;
                  ,GPCODMAG;
                  ,GPCODMA2;
                  ,GPCODUBI;
                  ,GPCODUB2;
                  ,GPCODLOT;
                  ,GPQTAUM1;
                  ,GPCODCEN;
                  ,GPCODCOM;
                  ,GPCODATT;
                  ,GPSERRIF;
                  ,GPROWRIF;
                  ,GPUNILOG;
                  ,GPCODCOL;
                  ,GPNUMCOL;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_GPSERIAL;
                  ,this.w_CPROWORD;
                  ,this.w_GPCODICE;
                  ,this.w_GPCODMAT;
                  ,this.w_GPUNIMIS;
                  ,this.w_GPQTAMOV;
                  ,this.w_GPPREZZO;
                  ,this.w_GP_ESCLU;
                  ,this.w_GPEVASIO;
                  ,this.w_GPCODMAG;
                  ,this.w_GPCODMA2;
                  ,this.w_GPCODUBI;
                  ,this.w_GPCODUB2;
                  ,this.w_GPCODLOT;
                  ,this.w_GPQTAUM1;
                  ,this.w_GPCODCEN;
                  ,this.w_GPCODCOM;
                  ,this.w_GPCODATT;
                  ,this.w_GPSERRIF;
                  ,this.w_GPROWRIF;
                  ,this.w_GPUNILOG;
                  ,this.w_GPCODCOL;
                  ,this.w_GPNUMCOL;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- gsar_mpg
        *Elusione check riga piena evito scrittura sul database
        Update (this.cTrsName) Set t_TEST=.f.
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.GESPENNA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GESPENNA_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_TEST AND NOT EMPTY(t_GPCODICE)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'GESPENNA')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'GESPENNA')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_TEST AND NOT EMPTY(t_GPCODICE)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update GESPENNA
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'GESPENNA')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",GPCODICE="+cp_ToStrODBCNull(this.w_GPCODICE)+;
                     ",GPCODMAT="+cp_ToStrODBC(this.w_GPCODMAT)+;
                     ",GPUNIMIS="+cp_ToStrODBCNull(this.w_GPUNIMIS)+;
                     ",GPQTAMOV="+cp_ToStrODBC(this.w_GPQTAMOV)+;
                     ",GPPREZZO="+cp_ToStrODBC(this.w_GPPREZZO)+;
                     ",GP_ESCLU="+cp_ToStrODBC(this.w_GP_ESCLU)+;
                     ",GPEVASIO="+cp_ToStrODBC(this.w_GPEVASIO)+;
                     ",GPCODMAG="+cp_ToStrODBCNull(this.w_GPCODMAG)+;
                     ",GPCODMA2="+cp_ToStrODBCNull(this.w_GPCODMA2)+;
                     ",GPCODUBI="+cp_ToStrODBCNull(this.w_GPCODUBI)+;
                     ",GPCODUB2="+cp_ToStrODBCNull(this.w_GPCODUB2)+;
                     ",GPCODLOT="+cp_ToStrODBCNull(this.w_GPCODLOT)+;
                     ",GPQTAUM1="+cp_ToStrODBC(this.w_GPQTAUM1)+;
                     ",GPCODCEN="+cp_ToStrODBCNull(this.w_GPCODCEN)+;
                     ",GPCODCOM="+cp_ToStrODBCNull(this.w_GPCODCOM)+;
                     ",GPCODATT="+cp_ToStrODBCNull(this.w_GPCODATT)+;
                     ",GPSERRIF="+cp_ToStrODBC(this.w_GPSERRIF)+;
                     ",GPROWRIF="+cp_ToStrODBC(this.w_GPROWRIF)+;
                     ",GPUNILOG="+cp_ToStrODBCNull(this.w_GPUNILOG)+;
                     ",GPCODCOL="+cp_ToStrODBCNull(this.w_GPCODCOL)+;
                     ",GPNUMCOL="+cp_ToStrODBC(this.w_GPNUMCOL)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'GESPENNA')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,GPCODICE=this.w_GPCODICE;
                     ,GPCODMAT=this.w_GPCODMAT;
                     ,GPUNIMIS=this.w_GPUNIMIS;
                     ,GPQTAMOV=this.w_GPQTAMOV;
                     ,GPPREZZO=this.w_GPPREZZO;
                     ,GP_ESCLU=this.w_GP_ESCLU;
                     ,GPEVASIO=this.w_GPEVASIO;
                     ,GPCODMAG=this.w_GPCODMAG;
                     ,GPCODMA2=this.w_GPCODMA2;
                     ,GPCODUBI=this.w_GPCODUBI;
                     ,GPCODUB2=this.w_GPCODUB2;
                     ,GPCODLOT=this.w_GPCODLOT;
                     ,GPQTAUM1=this.w_GPQTAUM1;
                     ,GPCODCEN=this.w_GPCODCEN;
                     ,GPCODCOM=this.w_GPCODCOM;
                     ,GPCODATT=this.w_GPCODATT;
                     ,GPSERRIF=this.w_GPSERRIF;
                     ,GPROWRIF=this.w_GPROWRIF;
                     ,GPUNILOG=this.w_GPUNILOG;
                     ,GPCODCOL=this.w_GPCODCOL;
                     ,GPNUMCOL=this.w_GPNUMCOL;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsar_mpg
        *Elusione check riga piena evito scrittura sul database
        Update (this.cTrsName) Set t_TEST=.t.
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GESPENNA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GESPENNA_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_TEST AND NOT EMPTY(t_GPCODICE)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete GESPENNA
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_TEST AND NOT EMPTY(t_GPCODICE)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GESPENNA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GESPENNA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_GPCODMAT<>.w_GPCODMAT
          .link_2_2('Full')
        endif
        .DoRTCalc(4,4,.t.)
          .link_2_4('Full')
        .DoRTCalc(6,6,.t.)
        if .o_GPCODICE<>.w_GPCODICE
          .w_GPUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
          .link_2_6('Full')
        endif
        if .o_GPCODICE<>.w_GPCODICE
          .w_GPQTAMOV = IIF(.w_TIPRIG='F', 1, IIF(.w_TIPRIG='D', 0, .w_GPQTAMOV))
        endif
        .DoRTCalc(9,11,.t.)
        if .o_GPQTAMOV<>.w_GPQTAMOV
          .w_GPEVASIO = IIF(.w_GPQTAMOV>=.w_QTAORI AND NOT EMPTY(.w_GPSERRIF) ,'S',' ')
        endif
        if .o_GPQTAMOV<>.w_GPQTAMOV
          .w_OLDQTA = .w_GPQTAMOV
        endif
          .w_MAGPRE = iif(.w_GESMAT='S',SPACE(5),.w_MAGPRE)
        .DoRTCalc(15,16,.t.)
        if .o_GPCODICE<>.w_GPCODICE
          .w_GPCODMAG = IIF(Not .w_TIPOOBJ_GEST$'PR'  , CALCMAG(1, .w_FLMGPR, '     ', .w_COMAG, .w_OMAG, .w_MAGPRE, .w_MAGTER) ,Space(5))
          .link_2_17('Full')
        endif
        if .o_GPCODICE<>.w_GPCODICE
          .w_GPCODMA2 = IIF(.w_TIPRIG='R' AND NOT EMPTY(.w_F2CASC+.w_F2RISE+.w_F2ORDI+.w_F2IMPE), CALCMAG(1, .w_FLMTPR, '     ', .w_COMAT, .w_OMAT, .w_MAGPRE, .w_MAGTER), SPACE(5))
          .link_2_18('Full')
        endif
        if .o_GPCODMAG<>.w_GPCODMAG
          .link_2_19('Full')
        endif
        if .o_GPCODMAG<>.w_GPCODMAG
          .link_2_20('Full')
        endif
        .DoRTCalc(21,21,.t.)
        if .o_GPQTAMOV<>.w_GPQTAMOV.or. .o_GPUNIMIS<>.w_GPUNIMIS
          .w_GPQTAUM1 = IIF(.w_TIPRIG='F', 1, CALQTAADV(.w_GPQTAMOV,.w_GPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,this, "GPQTAMOV"))
        endif
        if .o_GPCODICE<>.w_GPCODICE
          .w_GPCODCEN = .w_OCEN
          .link_2_23('Full')
        endif
        if .o_GPCODMAG<>.w_GPCODMAG.or. .o_GPCODMAT<>.w_GPCODMAT.or. .o_GPCODICE<>.w_GPCODICE
          .w_MTMAGCAR = IIF(.w_FLCASC='+' Or .w_FLRISE='-' Or .w_F2CASC='+' Or .w_F2RISE='-',.w_GPCODMAG,SPACE(5))
        endif
        if .o_GPCODICE<>.w_GPCODICE
          .w_GPCODCOM = .w_OCOM
          .link_2_25('Full')
        endif
        if .o_FLCASC<>.w_FLCASC.or. .o_MTMAGCAR<>.w_MTMAGCAR
          .w_MTFLCARI = IIF(NOT EMPTY(.w_MTMAGCAR),IIF(.w_FLCASC='+' OR .w_F2CASC='+','E','R'),' ')
        endif
        .DoRTCalc(27,27,.t.)
        if .o_GPCODMAT<>.w_GPCODMAT.or. .o_GPCODMAG<>.w_GPCODMAG.or. .o_GPCODICE<>.w_GPCODICE
          .w_MTMAGSCA = IIF(.w_FLCASC='-' Or .w_FLRISE='+',.w_GPCODMAG,SPACE(5))
        endif
        if .o_GPCODCOM<>.w_GPCODCOM
          .link_2_29('Full')
        endif
        if .o_GPCODICE<>.w_GPCODICE.or. .o_GPCODMAT<>.w_GPCODMAT.or. .o_MTMAGCAR<>.w_MTMAGCAR.or. .o_MTMAGSCA<>.w_MTMAGSCA
          .w_ESIRIS =  IIF( Not Empty( .w_MTFLSCAR) , .w_MTFLSCAR , .w_MTFLCARI )
        endif
        .DoRTCalc(31,31,.t.)
          .link_2_32('Full')
        if .o_GPCODICE<>.w_GPCODICE
          .w_MTKEYSAL = .w_CODART
        endif
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .DoRTCalc(34,34,.t.)
          .link_2_35('Full')
        .DoRTCalc(36,49,.t.)
          .w_MxFLLOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_FLUBIC='S'), LEFT(ALLTRIM(.w_FLCASC)+IIF(.w_FLRISE='+', '-', IIF(.w_FLRISE='-', '+', ' ')), 1), ' ')
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .DoRTCalc(51,52,.t.)
          .w_MxF2LOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_F2UBIC='S'), LEFT(ALLTRIM(.w_F2CASC)+IIF(.w_F2RISE='+', '-', IIF(.w_F2RISE='-', '+', ' ')), 1), ' ')
        .DoRTCalc(54,55,.t.)
        if .o_GPCODLOT<>.w_GPCODLOT
          .w_LOTZOOM = .F.
        endif
        .DoRTCalc(57,57,.t.)
          .w_OBTEST = .w_DATREG
        .oPgFrm.Page1.oPag.oObj_2_50.Calculate(.w_CAPTION,.w_TEXTCOLOR,.w_BACKCOLOR)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .DoRTCalc(59,77,.t.)
        if .o_GPCODICE<>.w_GPCODICE.or. .o_GPCODMAG<>.w_GPCODMAG
          .w_OMAG = IIF(EMPTY(.w_GPCODMAG), IIF(EMPTY(.w_OMAG),IIF(EMPTY(.w_COMAG), g_MAGAZI, .w_COMAG),.w_OMAG), .w_GPCODMAG)
        endif
        if .o_GPCODICE<>.w_GPCODICE.or. .o_GPCODMAG<>.w_GPCODMAG
          .w_OMAT = IIF(EMPTY(.w_GPCODMAT), IIF(EMPTY(.w_OMAT),IIF(EMPTY(.w_COMAT), g_MAGAZI, .w_COMAT),.w_OMAT), .w_GPCODMAT)
        endif
        .DoRTCalc(80,83,.t.)
          .w_OCEN = IIF(EMPTY(.w_GPCODCEN), .w_OCEN, .w_GPCODCEN)
        .DoRTCalc(85,85,.t.)
          .w_OCOM = IIF(EMPTY(.w_GPCODCOM), .w_OCEN, .w_GPCODCOM)
        .DoRTCalc(87,87,.t.)
          .w_OATT = IIF(EMPTY(.w_GPCODATT), .w_OATT, .w_GPCODATT)
        .DoRTCalc(89,129,.t.)
          .w_CODFOR = iif(.w_CODMAG=.w_MTMAGCAR And .w_TIPCON='F',.w_CODCON,SPACE(15))
          .w_MTFLSCAR = IIF(NOT EMPTY(.w_MTMAGSCA),IIF(.w_FLCASC='-' ,'E','R'),' ')
        if .o_GPCODMAT<>.w_GPCODMAT.or. .o_GPCODMAG<>.w_GPCODMAG
          .w_MAGUBI = IIF(Not Empty( .w_MTMAGCAR),IIF( .w_MTMAGCAR = .w_GPCODMAT , .w_GPCODMAT , .w_GPCODMAG ),IIF( .w_MTMAGSCA =.w_GPCODMAT , .w_GPCODMAT , .w_GPCODMAG ))
        endif
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .DoRTCalc(133,138,.t.)
          .w_ROWAGG = 'A'
          .w_NODOCU = 'S'
        .oPgFrm.Page1.oPag.oObj_2_89.Calculate(IIF(EMPTY(.w_GPSERRIF), '','Doc Evaso:'))
        .oPgFrm.Page1.oPag.oObj_2_90.Calculate(IIF(EMPTY(.w_GPSERRIF),'', 'n.:'))
        .oPgFrm.Page1.oPag.oObj_2_91.Calculate(IIF(EMPTY(.w_GPSERRIF),'', '/'))
        .oPgFrm.Page1.oPag.oObj_2_92.Calculate(IIF(EMPTY(.w_GPSERRIF),'', 'del'))
        .DoRTCalc(141,141,.t.)
          .w_NOMSG = .T.
        .DoRTCalc(143,146,.t.)
          .w_DOCCOLL = READCOLL(.w_TIPDOC)
        .oPgFrm.Page1.oPag.oObj_2_101.Calculate(IIF(.w_GP_ESCLU='S', '<Riga Esclusa>',IIF(.w_GPEVASIO='S','<Riga a Saldo>','')))
        .DoRTCalc(148,199,.t.)
          .w_CODCONF = IIF(.w_GPUNIMIS=.w_UNMIS3,.w_TPCON3,IIF(.w_GPUNIMIS=.w_UNMIS2,.w_TPCON2,.w_TPCON1))
          .link_2_139('Full')
        .DoRTCalc(201,201,.t.)
        if .o_GPCODICE<>.w_GPCODICE.or. .o_GPUNIMIS<>.w_GPUNIMIS
          .w_GPCODCOL = IIF(.w_GPUNIMIS=.w_UNMIS3,.w_TIPCO3,IIF(.w_GPUNIMIS=.w_UNMIS2,.w_TIPCO2,.w_TIPCO1))
          .link_2_141('Full')
        endif
        .DoRTCalc(203,203,.t.)
          .w_MVCODART = .w_CODART
          .w_MVCODLOT = .w_GPCODLOT
        .DoRTCalc(206,218,.t.)
          .w_ERRORCOD = iif(.w_TIPOOBJ_GEST$ 'A-O-V',  'Codice incongruente o inesistente o obsoleto', 'Articolo inesistente')
        .DoRTCalc(220,243,.t.)
        if .o_GPCODICE<>.w_GPCODICE.or. .o_GPCODCOM<>.w_GPCODCOM
          .w_COMCAR = .w_GPCODCOM
        endif
        if .o_GPCODICE<>.w_GPCODICE.or. .o_GPCODCOM<>.w_GPCODCOM
          .w_COMSCA = .w_GPCODCOM
        endif
        * --- Area Manuale = Calculate
        * --- gsar_mpg
        * Determina lo stato della riga
        this.State_Row()
        
        * Aggiorno le Label di conseguenza
        *Lancio mCalc per aggiornare Label msg righe non corrette
        This.mCalcRowObjs()
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CODART with this.w_CODART
      replace t_GESMAT with this.w_GESMAT
      replace t_QTAORI with this.w_QTAORI
      replace t_OLDQTA with this.w_OLDQTA
      replace t_MAGPRE with this.w_MAGPRE
      replace t_FLUBIC with this.w_FLUBIC
      replace t_F2UBIC with this.w_F2UBIC
      replace t_MTMAGCAR with this.w_MTMAGCAR
      replace t_MTFLCARI with this.w_MTFLCARI
      replace t_TIPATT with this.w_TIPATT
      replace t_MTMAGSCA with this.w_MTMAGSCA
      replace t_ESIRIS with this.w_ESIRIS
      replace t_MTKEYSAL with this.w_MTKEYSAL
      replace t_FLSERG with this.w_FLSERG
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_FLSTAT with this.w_FLSTAT
      replace t_FLLOTT with this.w_FLLOTT
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_MxFLLOTT with this.w_MxFLLOTT
      replace t_MxF2LOTT with this.w_MxF2LOTT
      replace t_STATOROW with this.w_STATOROW
      replace t_LOTZOOM with this.w_LOTZOOM
      replace t_UBIZOOM with this.w_UBIZOOM
      replace t_TEST with this.w_TEST
      replace t_CAPTION with this.w_CAPTION
      replace t_TEXTCOLOR with this.w_TEXTCOLOR
      replace t_OMAG with this.w_OMAG
      replace t_OMAT with this.w_OMAT
      replace t_DATOBSO with this.w_DATOBSO
      replace t_TIPRIG with this.w_TIPRIG
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_CODICE with this.w_CODICE
      replace t_UNIMIS with this.w_UNIMIS
      replace t_PREZZO with this.w_PREZZO
      replace t_QTAMOV with this.w_QTAMOV
      replace t_CODLOT with this.w_CODLOT
      replace t_CODUBI with this.w_CODUBI
      replace t_CODMAT with this.w_CODMAT
      replace t_CODMAG with this.w_CODMAG
      replace t_GPSERRIF with this.w_GPSERRIF
      replace t_GPROWRIF with this.w_GPROWRIF
      replace t_MTFLSCAR with this.w_MTFLSCAR
      replace t_MAGUBI with this.w_MAGUBI
      replace t_MT__FLAG with this.w_MT__FLAG
      replace t_OLDPRE with this.w_OLDPRE
      replace t_ROWAGG with this.w_ROWAGG
      replace t_NODOCU with this.w_NODOCU
      replace t_MT_SALDO with this.w_MT_SALDO
      replace t_NOMSG with this.w_NOMSG
      replace t_DATSCA with this.w_DATSCA
      replace t_MTSERRIF with this.w_MTSERRIF
      replace t_MTROWRIF with this.w_MTROWRIF
      replace t_MTRIFNUM with this.w_MTRIFNUM
      replace t_DOQTAEV1 with this.w_DOQTAEV1
      replace t_DOIMPEVA with this.w_DOIMPEVA
      replace t_FLRRIF with this.w_FLRRIF
      replace t_PARAME with this.w_PARAME
      replace t_FLAGRIS with this.w_FLAGRIS
      replace t_LENSCF with this.w_LENSCF
      replace t_MTRIFSTO with this.w_MTRIFSTO
      replace t_MTRIFSTO with this.w_MTRIFSTO
      replace t_FLGROR with this.w_FLGROR
      replace t_SCONT1 with this.w_SCONT1
      replace t_SCONT2 with this.w_SCONT2
      replace t_SCONT3 with this.w_SCONT3
      replace t_SCONT4 with this.w_SCONT4
      replace t_DISLOT with this.w_DISLOT
      replace t_GPFLDOCU with this.w_GPFLDOCU
      replace t_GPFLINTE with this.w_GPFLINTE
      replace t_GPFLACCO with this.w_GPFLACCO
      replace t_CODUB2 with this.w_CODUB2
      replace t_CODMA2 with this.w_CODMA2
      replace t_CODCEN_OR with this.w_CODCEN_OR
      replace t_CODCOM_OR with this.w_CODCOM_OR
      replace t_CODATT_OR with this.w_CODATT_OR
      replace t_FLUSEP with this.w_FLUSEP
      replace t_OPERAT with this.w_OPERAT
      replace t_OPERA3 with this.w_OPERA3
      replace t_MOLTIP with this.w_MOLTIP
      replace t_QTAUM1 with this.w_QTAUM1
      replace t_UNILOG with this.w_UNILOG
      replace t_TIPCO1 with this.w_TIPCO1
      replace t_TIPCO2 with this.w_TIPCO2
      replace t_TIPCO3 with this.w_TIPCO3
      replace t_TPCON1 with this.w_TPCON1
      replace t_TPCON2 with this.w_TPCON2
      replace t_TPCON3 with this.w_TPCON3
      replace t_FLESUL with this.w_FLESUL
      replace t_CODCONF with this.w_CODCONF
      replace t_FLCOVA with this.w_FLCOVA
      replace t_GPCODCOL with this.w_GPCODCOL
      replace t_GPNUMCOL with this.w_GPNUMCOL
      replace t_MVCODART with this.w_MVCODART
      replace t_MVCODLOT with this.w_MVCODLOT
      replace t_FLFRAZ1 with this.w_FLFRAZ1
      replace t_MODUM2 with this.w_MODUM2
      replace t_DOQTAEVA with this.w_DOQTAEVA
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_50.Calculate(.w_CAPTION,.w_TEXTCOLOR,.w_BACKCOLOR)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_89.Calculate(IIF(EMPTY(.w_GPSERRIF), '','Doc Evaso:'))
        .oPgFrm.Page1.oPag.oObj_2_90.Calculate(IIF(EMPTY(.w_GPSERRIF),'', 'n.:'))
        .oPgFrm.Page1.oPag.oObj_2_91.Calculate(IIF(EMPTY(.w_GPSERRIF),'', '/'))
        .oPgFrm.Page1.oPag.oObj_2_92.Calculate(IIF(EMPTY(.w_GPSERRIF),'', 'del'))
        .oPgFrm.Page1.oPag.oObj_2_101.Calculate(IIF(.w_GP_ESCLU='S', '<Riga Esclusa>',IIF(.w_GPEVASIO='S','<Riga a Saldo>','')))
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_50.Calculate(.w_CAPTION,.w_TEXTCOLOR,.w_BACKCOLOR)
        .oPgFrm.Page1.oPag.oObj_2_89.Calculate(IIF(EMPTY(.w_GPSERRIF), '','Doc Evaso:'))
        .oPgFrm.Page1.oPag.oObj_2_90.Calculate(IIF(EMPTY(.w_GPSERRIF),'', 'n.:'))
        .oPgFrm.Page1.oPag.oObj_2_91.Calculate(IIF(EMPTY(.w_GPSERRIF),'', '/'))
        .oPgFrm.Page1.oPag.oObj_2_92.Calculate(IIF(EMPTY(.w_GPSERRIF),'', 'del'))
        .oPgFrm.Page1.oPag.oObj_2_101.Calculate(IIF(.w_GP_ESCLU='S', '<Riga Esclusa>',IIF(.w_GPEVASIO='S','<Riga a Saldo>','')))
    endwith
  return
  proc Calculate_DZZONLTNIW()
    with this
          * --- Sbianco gpserrif/gprowrif se riga esclusa
          .w_GPSERRIF = IIF( .w_GP_ESCLU='S' , Space(10) , .w_GPSERRIF )
          .w_GPROWRIF = IIF( .w_GP_ESCLU='S' , 0 , .w_GPROWRIF )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oCODMAGTES_4_2.enabled = this.oPgFrm.Page2.oPag.oCODMAGTES_4_2.mCond()
    this.oPgFrm.Page2.oPag.oUBITES_4_6.enabled = this.oPgFrm.Page2.oPag.oUBITES_4_6.mCond()
    this.oPgFrm.Page2.oPag.oCODMA2TES_4_8.enabled = this.oPgFrm.Page2.oPag.oCODMA2TES_4_8.mCond()
    this.oPgFrm.Page2.oPag.oUB2TES_4_9.enabled = this.oPgFrm.Page2.oPag.oUB2TES_4_9.mCond()
    this.oPgFrm.Page2.oPag.oCODCENTES_4_11.enabled = this.oPgFrm.Page2.oPag.oCODCENTES_4_11.mCond()
    this.oPgFrm.Page2.oPag.oCODCOMTES_4_12.enabled = this.oPgFrm.Page2.oPag.oCODCOMTES_4_12.mCond()
    this.oPgFrm.Page2.oPag.oCODATTTES_4_13.enabled = this.oPgFrm.Page2.oPag.oCODATTTES_4_13.mCond()
    this.oPgFrm.Page2.oPag.oFLVSRI_4_24.enabled = this.oPgFrm.Page2.oPag.oFLVSRI_4_24.mCond()
    this.oPgFrm.Page2.oPag.oFLRIDE_4_25.enabled = this.oPgFrm.Page2.oPag.oFLRIDE_4_25.mCond()
    this.oPgFrm.Page2.oPag.oFLNSRI_4_26.enabled = this.oPgFrm.Page2.oPag.oFLNSRI_4_26.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_7.enabled = this.oPgFrm.Page2.oPag.oBtn_4_7.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_10.enabled = this.oPgFrm.Page2.oPag.oBtn_4_10.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_15.enabled = this.oPgFrm.Page2.oPag.oBtn_4_15.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGPCODICE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGPCODICE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGPCODMAT_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGPCODMAT_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGPUNIMIS_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGPUNIMIS_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGPQTAMOV_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGPQTAMOV_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGPPREZZO_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGPPREZZO_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGPEVASIO_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oGPEVASIO_2_12.mCond()
    this.oPgFrm.Page1.oPag.oGPCODMAG_2_17.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oGPCODMAG_2_17.mCond()
    this.oPgFrm.Page1.oPag.oGPCODMA2_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oGPCODMA2_2_18.mCond()
    this.oPgFrm.Page1.oPag.oGPCODUBI_2_19.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oGPCODUBI_2_19.mCond()
    this.oPgFrm.Page1.oPag.oGPCODUB2_2_20.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oGPCODUB2_2_20.mCond()
    this.oPgFrm.Page1.oPag.oGPCODLOT_2_21.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oGPCODLOT_2_21.mCond()
    this.oPgFrm.Page1.oPag.oGPQTAUM1_2_22.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oGPQTAUM1_2_22.mCond()
    this.oPgFrm.Page1.oPag.oGPCODCEN_2_23.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oGPCODCEN_2_23.mCond()
    this.oPgFrm.Page1.oPag.oGPCODCOM_2_25.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oGPCODCOM_2_25.mCond()
    this.oPgFrm.Page1.oPag.oGPCODATT_2_29.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oGPCODATT_2_29.mCond()
    this.oPgFrm.Page1.oPag.oGPUNILOG_2_130.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oGPUNILOG_2_130.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oStr_4_5.visible=!this.oPgFrm.Page2.oPag.oStr_4_5.mHide()
    this.oPgFrm.Page2.oPag.oUBITES_4_6.visible=!this.oPgFrm.Page2.oPag.oUBITES_4_6.mHide()
    this.oPgFrm.Page2.oPag.oUB2TES_4_9.visible=!this.oPgFrm.Page2.oPag.oUB2TES_4_9.mHide()
    this.oPgFrm.Page2.oPag.oCODCENTES_4_11.visible=!this.oPgFrm.Page2.oPag.oCODCENTES_4_11.mHide()
    this.oPgFrm.Page2.oPag.oCODCOMTES_4_12.visible=!this.oPgFrm.Page2.oPag.oCODCOMTES_4_12.mHide()
    this.oPgFrm.Page2.oPag.oCODATTTES_4_13.visible=!this.oPgFrm.Page2.oPag.oCODATTTES_4_13.mHide()
    this.oPgFrm.Page2.oPag.oUBIDESTES_4_14.visible=!this.oPgFrm.Page2.oPag.oUBIDESTES_4_14.mHide()
    this.oPgFrm.Page2.oPag.oBtn_4_15.visible=!this.oPgFrm.Page2.oPag.oBtn_4_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_16.visible=!this.oPgFrm.Page2.oPag.oStr_4_16.mHide()
    this.oPgFrm.Page2.oPag.oDESPIA_4_18.visible=!this.oPgFrm.Page2.oPag.oDESPIA_4_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_19.visible=!this.oPgFrm.Page2.oPag.oStr_4_19.mHide()
    this.oPgFrm.Page2.oPag.oDESCAN_4_20.visible=!this.oPgFrm.Page2.oPag.oDESCAN_4_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_21.visible=!this.oPgFrm.Page2.oPag.oStr_4_21.mHide()
    this.oPgFrm.Page2.oPag.oATDES_4_22.visible=!this.oPgFrm.Page2.oPag.oATDES_4_22.mHide()
    this.oPgFrm.Page1.oPag.oBtn_3_2.visible=!this.oPgFrm.Page1.oPag.oBtn_3_2.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_23.visible=!this.oPgFrm.Page2.oPag.oStr_4_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page2.oPag.oFLVSRI_4_24.visible=!this.oPgFrm.Page2.oPag.oFLVSRI_4_24.mHide()
    this.oPgFrm.Page2.oPag.oFLRIDE_4_25.visible=!this.oPgFrm.Page2.oPag.oFLRIDE_4_25.mHide()
    this.oPgFrm.Page2.oPag.oFLNSRI_4_26.visible=!this.oPgFrm.Page2.oPag.oFLNSRI_4_26.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_27.visible=!this.oPgFrm.Page2.oPag.oStr_4_27.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_34.visible=!this.oPgFrm.Page2.oPag.oStr_4_34.mHide()
    this.oPgFrm.Page2.oPag.oUB2DESTES_4_35.visible=!this.oPgFrm.Page2.oPag.oUB2DESTES_4_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_78.visible=!this.oPgFrm.Page1.oPag.oStr_1_78.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_81.visible=!this.oPgFrm.Page1.oPag.oStr_1_81.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_82.visible=!this.oPgFrm.Page1.oPag.oStr_1_82.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_83.visible=!this.oPgFrm.Page1.oPag.oStr_1_83.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_84.visible=!this.oPgFrm.Page1.oPag.oStr_1_84.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGP_ESCLU_2_10.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGP_ESCLU_2_10.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPEVASIO_2_12.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPEVASIO_2_12.mHide()
    this.oPgFrm.Page1.oPag.oGPCODUBI_2_19.visible=!this.oPgFrm.Page1.oPag.oGPCODUBI_2_19.mHide()
    this.oPgFrm.Page1.oPag.oGPCODUB2_2_20.visible=!this.oPgFrm.Page1.oPag.oGPCODUB2_2_20.mHide()
    this.oPgFrm.Page1.oPag.oGPCODLOT_2_21.visible=!this.oPgFrm.Page1.oPag.oGPCODLOT_2_21.mHide()
    this.oPgFrm.Page1.oPag.oGPQTAUM1_2_22.visible=!this.oPgFrm.Page1.oPag.oGPQTAUM1_2_22.mHide()
    this.oPgFrm.Page1.oPag.oGPCODCEN_2_23.visible=!this.oPgFrm.Page1.oPag.oGPCODCEN_2_23.mHide()
    this.oPgFrm.Page1.oPag.oGPCODCOM_2_25.visible=!this.oPgFrm.Page1.oPag.oGPCODCOM_2_25.mHide()
    this.oPgFrm.Page1.oPag.oGPCODATT_2_29.visible=!this.oPgFrm.Page1.oPag.oGPCODATT_2_29.mHide()
    this.oPgFrm.Page1.oPag.oDESART_2_31.visible=!this.oPgFrm.Page1.oPag.oDESART_2_31.mHide()
    this.oPgFrm.Page1.oPag.oEVTIPDOC_2_32.visible=!this.oPgFrm.Page1.oPag.oEVTIPDOC_2_32.mHide()
    this.oPgFrm.Page1.oPag.oEVNUMDOC_2_34.visible=!this.oPgFrm.Page1.oPag.oEVNUMDOC_2_34.mHide()
    this.oPgFrm.Page1.oPag.oUNMIS1_2_35.visible=!this.oPgFrm.Page1.oPag.oUNMIS1_2_35.mHide()
    this.oPgFrm.Page1.oPag.oEVALFDOC_2_36.visible=!this.oPgFrm.Page1.oPag.oEVALFDOC_2_36.mHide()
    this.oPgFrm.Page1.oPag.oEVDATDOC_2_38.visible=!this.oPgFrm.Page1.oPag.oEVDATDOC_2_38.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_73.visible=!this.oPgFrm.Page1.oPag.oBtn_2_73.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESAR1_2_79.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESAR1_2_79.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_98.visible=!this.oPgFrm.Page1.oPag.oBtn_2_98.mHide()
    this.oPgFrm.Page1.oPag.oEVROWORD_2_100.visible=!this.oPgFrm.Page1.oPag.oEVROWORD_2_100.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_105.visible=!this.oPgFrm.Page1.oPag.oBtn_2_105.mHide()
    this.oPgFrm.Page1.oPag.oGPUNILOG_2_130.visible=!this.oPgFrm.Page1.oPag.oGPUNILOG_2_130.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_66.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_89.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_90.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_91.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_92.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_101.Event(cEvent)
        if lower(cEvent)==lower("w_GP_ESCLU Changed")
          .Calculate_DZZONLTNIW()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsar_mpg
    * A seguito dell'apertura della maschera mi metto in
    * caricamento
    If cEvent=='Init'
       * Carico i dati dal Driver
       *--- Riposiziono la maschera nella posizione iniziale
       *--- altrimenti non la vedo perch� fuori schermo
        This.Left = This.nOldLeft   
        this.NotifyEvent('LanciaDriver')
        this.EcpLoad()
    Endif
    
    * Se cambio l'articolo il lotto deve essere sbiancato
    If cEvent='w_GPCODICE Changed'
     This.w_GPCODLOT = SPACE(20)
     This.w_GPUNILOG = SPACE(20)
    Endif
    
    * Al Cambio di lotto e ubicazione da zoom
    * non scatta la mCalc qui aggiorno lo stato della
    * riga
    If cEvent='w_GPCODLOT Changed'
       This.State_Row()
       * RICALCOLA CONFEZIONI
       If g_MADV='S' And inlist(this.cfunction,"Edit","Load") and not empty(this.w_GPCODCOL) and this.w_FLPACK='S'
         do GSAR_BCO WITH THIS, "R"
       endif
    Endif
    
    If cEvent='w_GPCODUBI Changed'
      This.State_Row()
    Endif
    
    If cEvent='w_GPCODMAT Changed'
      This.State_Row()
    Endif
    
    
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GPCODICE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_GPCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAMOLTIP,CA__TIPO,CALENSCF,CAOPERAT,CATIPCO3,CATPCON3";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_GPCODICE))
          select CACODICE,CADESART,CACODART,CAUNIMIS,CAMOLTIP,CA__TIPO,CALENSCF,CAOPERAT,CATIPCO3,CATPCON3;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_GPCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAMOLTIP,CA__TIPO,CALENSCF,CAOPERAT,CATIPCO3,CATPCON3";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_GPCODICE)+"%");

            select CACODICE,CADESART,CACODART,CAUNIMIS,CAMOLTIP,CA__TIPO,CALENSCF,CAOPERAT,CATIPCO3,CATPCON3;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GPCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oGPCODICE_2_2'),i_cWhere,'GSMA_ACA',"Elenco codici articolo",'GSVE_MDV.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAMOLTIP,CA__TIPO,CALENSCF,CAOPERAT,CATIPCO3,CATPCON3";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAMOLTIP,CA__TIPO,CALENSCF,CAOPERAT,CATIPCO3,CATPCON3;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAMOLTIP,CA__TIPO,CALENSCF,CAOPERAT,CATIPCO3,CATPCON3";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_GPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_GPCODICE)
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAMOLTIP,CA__TIPO,CALENSCF,CAOPERAT,CATIPCO3,CATPCON3;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_DESART = NVL(_Link_.CADESART,space(40))
      this.w_CODART = NVL(_Link_.CACODART,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_TIPRIG = NVL(_Link_.CA__TIPO,space(1))
      this.w_DESAR1 = NVL(_Link_.CADESART,space(40))
      this.w_LENSCF = NVL(_Link_.CALENSCF,0)
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_TIPCO3 = NVL(_Link_.CATIPCO3,space(3))
      this.w_TPCON3 = NVL(_Link_.CATPCON3,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODICE = space(20)
      endif
      this.w_DESART = space(40)
      this.w_CODART = space(20)
      this.w_UNMIS3 = space(3)
      this.w_MOLTI3 = 0
      this.w_TIPRIG = space(1)
      this.w_DESAR1 = space(40)
      this.w_LENSCF = 0
      this.w_OPERA3 = space(1)
      this.w_TIPCO3 = space(3)
      this.w_TPCON3 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPOOBJ_GEST$ 'A-O-V'  Or ( g_GPOS='S' And .w_TIPOOBJ_GEST= 'P' And PSCHKART(.w_CODART, .w_GPCODICE,  .w_DATREG, .w_CODESC, .w_CODCON, THIS) ) Or (.w_TIPRIG='R' And .w_TIPOOBJ_GEST$'MR')) And (g_FLCESC <> 'S' Or .w_LENSCF=0 Or (Right(Alltrim(.w_GPCODICE), .w_LENSCF) = Alltrim(.w_CODESC)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("%w_ERRORCOD%")
        endif
        this.w_GPCODICE = space(20)
        this.w_DESART = space(40)
        this.w_CODART = space(20)
        this.w_UNMIS3 = space(3)
        this.w_MOLTI3 = 0
        this.w_TIPRIG = space(1)
        this.w_DESAR1 = space(40)
        this.w_LENSCF = 0
        this.w_OPERA3 = space(1)
        this.w_TIPCO3 = space(3)
        this.w_TPCON3 = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 11 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+11<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CACODICE as CACODICE202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CACODART as CACODART202"+ ",link_2_2.CAUNIMIS as CAUNIMIS202"+ ",link_2_2.CAMOLTIP as CAMOLTIP202"+ ",link_2_2.CA__TIPO as CA__TIPO202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CALENSCF as CALENSCF202"+ ",link_2_2.CAOPERAT as CAOPERAT202"+ ",link_2_2.CATIPCO3 as CATIPCO3202"+ ",link_2_2.CATPCON3 as CATPCON3202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on GESPENNA.GPCODICE=link_2_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+11
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and GESPENNA.GPCODICE=link_2_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+11
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARFLLOTT,ARMAGPRE,ARGESMAT,ARDISLOT,ARFLUSEP,AROPERAT,ARMOLTIP,ARTIPCO1,ARTIPCO2,ARFLESUL,ARTPCONF,ARTPCON2";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARFLLOTT,ARMAGPRE,ARGESMAT,ARDISLOT,ARFLUSEP,AROPERAT,ARMOLTIP,ARTIPCO1,ARTIPCO2,ARFLESUL,ARTPCONF,ARTPCON2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_FLLOTT = NVL(_Link_.ARFLLOTT,space(1))
      this.w_MAGPRE = NVL(_Link_.ARMAGPRE,space(5))
      this.w_GESMAT = NVL(_Link_.ARGESMAT,space(1))
      this.w_DISLOT = NVL(_Link_.ARDISLOT,space(1))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_TIPCO1 = NVL(_Link_.ARTIPCO1,space(5))
      this.w_TIPCO2 = NVL(_Link_.ARTIPCO2,space(5))
      this.w_FLESUL = NVL(_Link_.ARFLESUL,space(1))
      this.w_TPCON1 = NVL(_Link_.ARTPCONF,space(3))
      this.w_TPCON2 = NVL(_Link_.ARTPCON2,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_FLSERG = space(1)
      this.w_FLLOTT = space(1)
      this.w_MAGPRE = space(5)
      this.w_GESMAT = space(1)
      this.w_DISLOT = space(1)
      this.w_FLUSEP = space(1)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_TIPCO1 = space(5)
      this.w_TIPCO2 = space(5)
      this.w_FLESUL = space(1)
      this.w_TPCON1 = space(3)
      this.w_TPCON2 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPUNIMIS
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_GPUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_GPUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oGPUNIMIS_2_6'),i_cWhere,'GSAR_AUM',"Elenco unit� di misura",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_GPUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_GPUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GPUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLSERG='S' Or CHKUNIMI(.w_GPUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3,.w_NOMSGCHECK) AND NOT EMPTY(.w_GPUNIMIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_GPUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.UMCODICE as UMCODICE206"+ ",link_2_6.UMFLFRAZ as UMFLFRAZ206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on GESPENNA.GPUNIMIS=link_2_6.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and GESPENNA.GPUNIMIS=link_2_6.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GPCODMAG
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_GPCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_GPCODMAG))
          select MGCODMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oGPCODMAG_2_17'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_GPCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_GPCODMAG)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODMAG = space(5)
      endif
      this.w_FLUBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.MGCODMAG as MGCODMAG217"+ ",link_2_17.MGFLUBIC as MGFLUBIC217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on GESPENNA.GPCODMAG=link_2_17.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and GESPENNA.GPCODMAG=link_2_17.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GPCODMA2
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODMA2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_GPCODMA2)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_GPCODMA2))
          select MGCODMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODMA2)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODMA2) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oGPCODMA2_2_18'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODMA2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_GPCODMA2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_GPCODMA2)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODMA2 = NVL(_Link_.MGCODMAG,space(5))
      this.w_F2UBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODMA2 = space(5)
      endif
      this.w_F2UBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODMA2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_18.MGCODMAG as MGCODMAG218"+ ",link_2_18.MGFLUBIC as MGFLUBIC218"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_18 on GESPENNA.GPCODMA2=link_2_18.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_18"
          i_cKey=i_cKey+'+" and GESPENNA.GPCODMA2=link_2_18.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GPCODUBI
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_GPCODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_GPCODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_GPCODMAG;
                     ,'UBCODICE',trim(this.w_GPCODUBI))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oGPCODUBI_2_19'),i_cWhere,'GSMD_MUB',"Ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_GPCODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_GPCODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_GPCODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_GPCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_GPCODMAG;
                       ,'UBCODICE',this.w_GPCODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MxFLLOTT='+') OR CHKLOTUB(.w_GPCODUBI,'',0,.w_GPCODMAG,.w_CODART,.w_CODCON,.w_MxFLLOTT,.w_FLSTAT,.T.,.w_DATREG,' ','U','     ','     ',.T.,.w_DISLOT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        endif
        this.w_GPCODUBI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCODUB2
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODUB2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_GPCODUB2)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_GPCODMA2);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_GPCODMA2;
                     ,'UBCODICE',trim(this.w_GPCODUB2))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODUB2)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODUB2) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oGPCODUB2_2_20'),i_cWhere,'GSMD_MUB',"Ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_GPCODMA2<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_GPCODMA2);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODUB2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_GPCODUB2);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_GPCODMA2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_GPCODMA2;
                       ,'UBCODICE',this.w_GPCODUB2)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODUB2 = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODUB2 = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MxF2LOTT='+') OR CHKLOTUB(.w_GPCODUB2,'',0,.w_GPCODMA2,.w_CODART,.w_CODCON,.w_MxF2LOTT,.w_FLSTAT,.T.,.w_DATREG,' ','U','     ','     ',.T.,.w_DISLOT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        endif
        this.w_GPCODUB2 = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODUB2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCODLOT
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_GPCODLOT)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT,LODATSCA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODART;
                     ,'LOCODICE',trim(this.w_GPCODLOT))
          select LOCODART,LOCODICE,LOFLSTAT,LODATSCA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODLOT)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODLOT) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oGPCODLOT_2_21'),i_cWhere,'GSMD_ALO',"Lotti articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT,LODATSCA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LOFLSTAT,LODATSCA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente, scaduto o sospeso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT,LODATSCA";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LOFLSTAT,LODATSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT,LODATSCA";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_GPCODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODART;
                       ,'LOCODICE',this.w_GPCODLOT)
            select LOCODART,LOCODICE,LOFLSTAT,LODATSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODLOT = NVL(_Link_.LOCODICE,space(20))
      this.w_FLSTAT = NVL(_Link_.LOFLSTAT,space(1))
      this.w_DATSCA = NVL(cp_ToDate(_Link_.LODATSCA),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODLOT = space(20)
      endif
      this.w_FLSTAT = space(1)
      this.w_DATSCA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_FLSTAT<>'S' AND (Empty(.w_DATSCA) OR .w_DATSCA >.w_DATREG)) OR CHKLOTUB(.w_GPCODLOT,Space(10),0,.w_GPCODMAG,.w_CODART,.w_CODCON,.w_MxFLLOTT,.w_FLSTAT,.w_LOTZOOM,.w_DATREG,.w_FLRRIF,'L',.w_CAUMAG,.w_CAUCOL,.F.,.w_DISLOT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente, scaduto o sospeso")
        endif
        this.w_GPCODLOT = space(20)
        this.w_FLSTAT = space(1)
        this.w_DATSCA = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCODCEN
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_GPCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_GPCODCEN))
          select CC_CONTO,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oGPCODCEN_2_23'),i_cWhere,'GSCA_ACC',"Elenco centri di costo e ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_GPCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_GPCODCEN)
            select CC_CONTO,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODCEN = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Centro di Costo obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di costo incongruente o obsoleto")
        endif
        this.w_GPCODCEN = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_23.CC_CONTO as CC_CONTO223"+ ",link_2_23.CCDTOBSO as CCDTOBSO223"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_23 on GESPENNA.GPCODCEN=link_2_23.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_23"
          i_cKey=i_cKey+'+" and GESPENNA.GPCODCEN=link_2_23.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GPCODCOM
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_GPCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_GPCODCOM))
          select CNCODCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oGPCODCOM_2_25'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_GPCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_GPCODCOM)
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODCOM = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_GPCODCOM = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_25.CNCODCAN as CNCODCAN225"+ ",link_2_25.CNDTOBSO as CNDTOBSO225"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_25 on GESPENNA.GPCODCOM=link_2_25.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_25"
          i_cKey=i_cKey+'+" and GESPENNA.GPCODCOM=link_2_25.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GPCODATT
  func Link_2_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_GPCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_GPCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_GPCODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_GPCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oGPCODATT_2_29'),i_cWhere,'GSPC_BZZ',"Elenco attivit� di commessa",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_GPCODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_GPCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_GPCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_GPCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_GPCODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_GPCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODATT = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODATT = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EVTIPDOC
  func Link_2_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_EVTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_EVTIPDOC)
            select TDTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_EVTIPDOC = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ1 = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_FLFRAZ1 = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAGTES
  func Link_4_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAGTES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAGTES)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAGTES))
          select MGCODMAG,MGDESMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAGTES)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAGTES) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAGTES_4_2'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAGTES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAGTES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAGTES)
            select MGCODMAG,MGDESMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAGTES = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGTES = NVL(_Link_.MGDESMAG,space(30))
      this.w_FLUBICTES = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAGTES = space(5)
      endif
      this.w_DESMAGTES = space(30)
      this.w_FLUBICTES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAGTES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UBITES
  func Link_4_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UBITES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_UBITES)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAGTES);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_CODMAGTES;
                     ,'UBCODICE',trim(this.w_UBITES))
          select UBCODMAG,UBCODICE,UBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UBITES)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_UBITES) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oUBITES_4_6'),i_cWhere,'GSMD_MUB',"Elenco ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODMAGTES<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAGTES);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UBITES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_UBITES);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAGTES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_CODMAGTES;
                       ,'UBCODICE',this.w_UBITES)
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UBITES = NVL(_Link_.UBCODICE,space(20))
      this.w_UBIDESTES = NVL(_Link_.UBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_UBITES = space(20)
      endif
      this.w_UBIDESTES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UBITES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMA2TES
  func Link_4_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMA2TES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMA2TES)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMA2TES))
          select MGCODMAG,MGDESMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMA2TES)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMA2TES) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMA2TES_4_8'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMA2TES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMA2TES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMA2TES)
            select MGCODMAG,MGDESMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMA2TES = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMA2TES = NVL(_Link_.MGDESMAG,space(30))
      this.w_FLUBI2TES = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODMA2TES = space(5)
      endif
      this.w_DESMA2TES = space(30)
      this.w_FLUBI2TES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMA2TES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UB2TES
  func Link_4_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UB2TES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_UB2TES)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMA2TES);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_CODMA2TES;
                     ,'UBCODICE',trim(this.w_UB2TES))
          select UBCODMAG,UBCODICE,UBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UB2TES)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_UB2TES) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oUB2TES_4_9'),i_cWhere,'GSMD_MUB',"Elenco ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODMA2TES<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMA2TES);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UB2TES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_UB2TES);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMA2TES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_CODMA2TES;
                       ,'UBCODICE',this.w_UB2TES)
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UB2TES = NVL(_Link_.UBCODICE,space(20))
      this.w_UB2DESTES = NVL(_Link_.UBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_UB2TES = space(20)
      endif
      this.w_UB2DESTES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UB2TES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCENTES
  func Link_4_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCENTES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CODCENTES)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDTOBSO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CODCENTES))
          select CC_CONTO,CCDTOBSO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCENTES)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDTOBSO like "+cp_ToStrODBC(trim(this.w_CODCENTES)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDTOBSO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDTOBSO like "+cp_ToStr(trim(this.w_CODCENTES)+"%");

            select CC_CONTO,CCDTOBSO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCENTES) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCODCENTES_4_11'),i_cWhere,'GSCA_ACC',"Elenco centri costo e ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDTOBSO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDTOBSO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCENTES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDTOBSO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CODCENTES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CODCENTES)
            select CC_CONTO,CCDTOBSO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCENTES = NVL(_Link_.CC_CONTO,space(15))
      this.w_DATOBS2 = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_DESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCENTES = space(15)
      endif
      this.w_DATOBS2 = ctod("  /  /  ")
      this.w_DESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBS2,.w_OBTEST,"Centro di Costo obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di costo incongruente o obsoleto")
        endif
        this.w_CODCENTES = space(15)
        this.w_DATOBS2 = ctod("  /  /  ")
        this.w_DESPIA = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCENTES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOMTES
  func Link_4_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOMTES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOMTES)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOMTES))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOMTES)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOMTES)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOMTES)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOMTES) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOMTES_4_12'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOMTES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOMTES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOMTES)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOMTES = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(40))
      this.w_DATOBS2 = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOMTES = space(15)
      endif
      this.w_DESCAN = space(40)
      this.w_DATOBS2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_CODCOMTES = space(15)
        this.w_DESCAN = space(40)
        this.w_DATOBS2 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOMTES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATTTES
  func Link_4_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATTTES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATTTES)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMTES);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOMTES;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATTTES))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATTTES)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATTTES)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMTES);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATTTES)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOMTES);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATTTES) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATTTES_4_13'),i_cWhere,'GSPC_BZZ',"Elenco attivit� di commessa",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOMTES<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMTES);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATTTES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATTTES);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMTES);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOMTES;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATTTES)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATTTES = NVL(_Link_.ATCODATT,space(15))
      this.w_ATDES = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATTTES = space(15)
      endif
      this.w_ATDES = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATTTES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPUNILOG
  func Link_2_130(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_lTable = "UNIT_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2], .t., this.UNIT_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPUNILOG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_AUL',True,'UNIT_LOG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UL__SSCC like "+cp_ToStrODBC(trim(this.w_GPUNILOG)+"%");

          i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UL__SSCC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UL__SSCC',trim(this.w_GPUNILOG))
          select UL__SSCC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UL__SSCC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPUNILOG)==trim(_Link_.UL__SSCC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPUNILOG) and !this.bDontReportError
            deferred_cp_zoom('UNIT_LOG','*','UL__SSCC',cp_AbsName(oSource.parent,'oGPUNILOG_2_130'),i_cWhere,'GSMD_AUL',"Unit� logistiche",'GSMD_KDA.UNIT_LOG_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                     +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',oSource.xKey(1))
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPUNILOG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                   +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(this.w_GPUNILOG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',this.w_GPUNILOG)
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPUNILOG = NVL(_Link_.UL__SSCC,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_GPUNILOG = space(18)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=checksscc('L',.w_GPUNILOG,.t.,.t.,.w_CODART,.w_GPCODLOT, .w_GPCODCOL)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GPUNILOG = space(18)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])+'\'+cp_ToStr(_Link_.UL__SSCC,1)
      cp_ShowWarn(i_cKey,this.UNIT_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPUNILOG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCONF
  func Link_2_139(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPICONF_IDX,3]
    i_lTable = "TIPICONF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2], .t., this.TIPICONF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCONF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCONF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCFLCOVA";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CODCONF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CODCONF)
            select TCCODICE,TCFLCOVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCONF = NVL(_Link_.TCCODICE,space(3))
      this.w_FLCOVA = NVL(_Link_.TCFLCOVA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCONF = space(3)
      endif
      this.w_FLCOVA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPICONF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCONF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCODCOL
  func Link_2_141(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_GPCODCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_GPCODCOL)
            select TCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODCOL = NVL(_Link_.TCCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODCOL = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oGPCODMAG_2_17.value==this.w_GPCODMAG)
      this.oPgFrm.Page1.oPag.oGPCODMAG_2_17.value=this.w_GPCODMAG
      replace t_GPCODMAG with this.oPgFrm.Page1.oPag.oGPCODMAG_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODMA2_2_18.value==this.w_GPCODMA2)
      this.oPgFrm.Page1.oPag.oGPCODMA2_2_18.value=this.w_GPCODMA2
      replace t_GPCODMA2 with this.oPgFrm.Page1.oPag.oGPCODMA2_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODUBI_2_19.value==this.w_GPCODUBI)
      this.oPgFrm.Page1.oPag.oGPCODUBI_2_19.value=this.w_GPCODUBI
      replace t_GPCODUBI with this.oPgFrm.Page1.oPag.oGPCODUBI_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODUB2_2_20.value==this.w_GPCODUB2)
      this.oPgFrm.Page1.oPag.oGPCODUB2_2_20.value=this.w_GPCODUB2
      replace t_GPCODUB2 with this.oPgFrm.Page1.oPag.oGPCODUB2_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODLOT_2_21.value==this.w_GPCODLOT)
      this.oPgFrm.Page1.oPag.oGPCODLOT_2_21.value=this.w_GPCODLOT
      replace t_GPCODLOT with this.oPgFrm.Page1.oPag.oGPCODLOT_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oGPQTAUM1_2_22.value==this.w_GPQTAUM1)
      this.oPgFrm.Page1.oPag.oGPQTAUM1_2_22.value=this.w_GPQTAUM1
      replace t_GPQTAUM1 with this.oPgFrm.Page1.oPag.oGPQTAUM1_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODCEN_2_23.value==this.w_GPCODCEN)
      this.oPgFrm.Page1.oPag.oGPCODCEN_2_23.value=this.w_GPCODCEN
      replace t_GPCODCEN with this.oPgFrm.Page1.oPag.oGPCODCEN_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODCOM_2_25.value==this.w_GPCODCOM)
      this.oPgFrm.Page1.oPag.oGPCODCOM_2_25.value=this.w_GPCODCOM
      replace t_GPCODCOM with this.oPgFrm.Page1.oPag.oGPCODCOM_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODATT_2_29.value==this.w_GPCODATT)
      this.oPgFrm.Page1.oPag.oGPCODATT_2_29.value=this.w_GPCODATT
      replace t_GPCODATT with this.oPgFrm.Page1.oPag.oGPCODATT_2_29.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_2_31.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_2_31.value=this.w_DESART
      replace t_DESART with this.oPgFrm.Page1.oPag.oDESART_2_31.value
    endif
    if not(this.oPgFrm.Page1.oPag.oEVTIPDOC_2_32.value==this.w_EVTIPDOC)
      this.oPgFrm.Page1.oPag.oEVTIPDOC_2_32.value=this.w_EVTIPDOC
      replace t_EVTIPDOC with this.oPgFrm.Page1.oPag.oEVTIPDOC_2_32.value
    endif
    if not(this.oPgFrm.Page1.oPag.oEVNUMDOC_2_34.value==this.w_EVNUMDOC)
      this.oPgFrm.Page1.oPag.oEVNUMDOC_2_34.value=this.w_EVNUMDOC
      replace t_EVNUMDOC with this.oPgFrm.Page1.oPag.oEVNUMDOC_2_34.value
    endif
    if not(this.oPgFrm.Page1.oPag.oUNMIS1_2_35.value==this.w_UNMIS1)
      this.oPgFrm.Page1.oPag.oUNMIS1_2_35.value=this.w_UNMIS1
      replace t_UNMIS1 with this.oPgFrm.Page1.oPag.oUNMIS1_2_35.value
    endif
    if not(this.oPgFrm.Page1.oPag.oEVALFDOC_2_36.value==this.w_EVALFDOC)
      this.oPgFrm.Page1.oPag.oEVALFDOC_2_36.value=this.w_EVALFDOC
      replace t_EVALFDOC with this.oPgFrm.Page1.oPag.oEVALFDOC_2_36.value
    endif
    if not(this.oPgFrm.Page1.oPag.oEVDATDOC_2_38.value==this.w_EVDATDOC)
      this.oPgFrm.Page1.oPag.oEVDATDOC_2_38.value=this.w_EVDATDOC
      replace t_EVDATDOC with this.oPgFrm.Page1.oPag.oEVDATDOC_2_38.value
    endif
    if not(this.oPgFrm.Page3.oPag.oMsg_5_1.value==this.w_Msg)
      this.oPgFrm.Page3.oPag.oMsg_5_1.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMAGTES_4_2.value==this.w_CODMAGTES)
      this.oPgFrm.Page2.oPag.oCODMAGTES_4_2.value=this.w_CODMAGTES
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAGTES_4_3.value==this.w_DESMAGTES)
      this.oPgFrm.Page2.oPag.oDESMAGTES_4_3.value=this.w_DESMAGTES
    endif
    if not(this.oPgFrm.Page2.oPag.oUBITES_4_6.value==this.w_UBITES)
      this.oPgFrm.Page2.oPag.oUBITES_4_6.value=this.w_UBITES
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMA2TES_4_8.value==this.w_CODMA2TES)
      this.oPgFrm.Page2.oPag.oCODMA2TES_4_8.value=this.w_CODMA2TES
    endif
    if not(this.oPgFrm.Page2.oPag.oUB2TES_4_9.value==this.w_UB2TES)
      this.oPgFrm.Page2.oPag.oUB2TES_4_9.value=this.w_UB2TES
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCENTES_4_11.value==this.w_CODCENTES)
      this.oPgFrm.Page2.oPag.oCODCENTES_4_11.value=this.w_CODCENTES
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCOMTES_4_12.value==this.w_CODCOMTES)
      this.oPgFrm.Page2.oPag.oCODCOMTES_4_12.value=this.w_CODCOMTES
    endif
    if not(this.oPgFrm.Page2.oPag.oCODATTTES_4_13.value==this.w_CODATTTES)
      this.oPgFrm.Page2.oPag.oCODATTTES_4_13.value=this.w_CODATTTES
    endif
    if not(this.oPgFrm.Page2.oPag.oUBIDESTES_4_14.value==this.w_UBIDESTES)
      this.oPgFrm.Page2.oPag.oUBIDESTES_4_14.value=this.w_UBIDESTES
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPIA_4_18.value==this.w_DESPIA)
      this.oPgFrm.Page2.oPag.oDESPIA_4_18.value=this.w_DESPIA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAN_4_20.value==this.w_DESCAN)
      this.oPgFrm.Page2.oPag.oDESCAN_4_20.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page2.oPag.oATDES_4_22.value==this.w_ATDES)
      this.oPgFrm.Page2.oPag.oATDES_4_22.value=this.w_ATDES
    endif
    if not(this.oPgFrm.Page1.oPag.oESEGUITOIMPORT_3_1.value==this.w_ESEGUITOIMPORT)
      this.oPgFrm.Page1.oPag.oESEGUITOIMPORT_3_1.value=this.w_ESEGUITOIMPORT
    endif
    if not(this.oPgFrm.Page1.oPag.oEVROWORD_2_100.value==this.w_EVROWORD)
      this.oPgFrm.Page1.oPag.oEVROWORD_2_100.value=this.w_EVROWORD
      replace t_EVROWORD with this.oPgFrm.Page1.oPag.oEVROWORD_2_100.value
    endif
    if not(this.oPgFrm.Page2.oPag.oFLVSRI_4_24.RadioValue()==this.w_FLVSRI)
      this.oPgFrm.Page2.oPag.oFLVSRI_4_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLRIDE_4_25.RadioValue()==this.w_FLRIDE)
      this.oPgFrm.Page2.oPag.oFLRIDE_4_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLNSRI_4_26.RadioValue()==this.w_FLNSRI)
      this.oPgFrm.Page2.oPag.oFLNSRI_4_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMA2TES_4_33.value==this.w_DESMA2TES)
      this.oPgFrm.Page2.oPag.oDESMA2TES_4_33.value=this.w_DESMA2TES
    endif
    if not(this.oPgFrm.Page2.oPag.oUB2DESTES_4_35.value==this.w_UB2DESTES)
      this.oPgFrm.Page2.oPag.oUB2DESTES_4_35.value=this.w_UB2DESTES
    endif
    if not(this.oPgFrm.Page1.oPag.oGPUNILOG_2_130.value==this.w_GPUNILOG)
      this.oPgFrm.Page1.oPag.oGPUNILOG_2_130.value=this.w_GPUNILOG
      replace t_GPUNILOG with this.oPgFrm.Page1.oPag.oGPUNILOG_2_130.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPCODICE_2_2.value==this.w_GPCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPCODICE_2_2.value=this.w_GPCODICE
      replace t_GPCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPCODMAT_2_3.value==this.w_GPCODMAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPCODMAT_2_3.value=this.w_GPCODMAT
      replace t_GPCODMAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPCODMAT_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPUNIMIS_2_6.value==this.w_GPUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPUNIMIS_2_6.value=this.w_GPUNIMIS
      replace t_GPUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPUNIMIS_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPQTAMOV_2_7.value==this.w_GPQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPQTAMOV_2_7.value=this.w_GPQTAMOV
      replace t_GPQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPQTAMOV_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPPREZZO_2_8.value==this.w_GPPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPPREZZO_2_8.value=this.w_GPPREZZO
      replace t_GPPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPPREZZO_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGP_ESCLU_2_10.RadioValue()==this.w_GP_ESCLU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGP_ESCLU_2_10.SetRadio()
      replace t_GP_ESCLU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGP_ESCLU_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPEVASIO_2_12.RadioValue()==this.w_GPEVASIO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPEVASIO_2_12.SetRadio()
      replace t_GPEVASIO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPEVASIO_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESAR1_2_79.value==this.w_DESAR1)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESAR1_2_79.value=this.w_DESAR1
      replace t_DESAR1 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESAR1_2_79.value
    endif
    cp_SetControlsValueExtFlds(this,'GESPENNA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(CHKDTOBS(.w_DATOBS2,.w_OBTEST,"Centro di Costo obsoleto!",.F.))  and not(NOT (g_PERCCR='S' AND .w_FLANAL='S'))  and (g_PERCCR='S' AND .w_FLANAL='S' And  Not .w_TIPOOBJ_GEST$ 'P-M-R')  and not(empty(.w_CODCENTES))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCODCENTES_4_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di costo incongruente o obsoleto")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.))  and not(NOT(((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))))  and (((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S')) And Not .w_TIPOOBJ_GEST$ 'P-M-R')  and not(empty(.w_CODCOMTES))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCODCOMTES_4_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (t_TEST AND NOT EMPTY(t_GPCODICE));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not((.w_TIPOOBJ_GEST$ 'A-O-V'  Or ( g_GPOS='S' And .w_TIPOOBJ_GEST= 'P' And PSCHKART(.w_CODART, .w_GPCODICE,  .w_DATREG, .w_CODESC, .w_CODCON, THIS) ) Or (.w_TIPRIG='R' And .w_TIPOOBJ_GEST$'MR')) And (g_FLCESC <> 'S' Or .w_LENSCF=0 Or (Right(Alltrim(.w_GPCODICE), .w_LENSCF) = Alltrim(.w_CODESC)))) and (EMPTY(.w_GPSERRIF) AND EMPTY(.w_GPCODMAT) AND EMPTY(.w_GPSERRIF)) and not(empty(.w_GPCODICE)) and (.w_TEST AND NOT EMPTY(.w_GPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPCODICE_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("%w_ERRORCOD%")
        case   not(.w_FLSERG='S' Or CHKUNIMI(.w_GPUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3,.w_NOMSGCHECK) AND NOT EMPTY(.w_GPUNIMIS)) and (.w_TIPRIG $ 'RM' AND Not Empty( .w_GPCODICE )  AND EMPTY(.w_GPSERRIF)  AND ( EMPTY(.w_GPCODMAT)  OR (.w_FLCASC='+' OR .w_FLRISE='-'))) and not(empty(.w_GPUNIMIS)) and (.w_TEST AND NOT EMPTY(.w_GPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPUNIMIS_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
        case   not(.w_GPQTAMOV=0  Or  .w_FLFRAZ<>'S' Or .w_GPQTAMOV=INT(.w_GPQTAMOV) OR NOT .w_TIPRIG $ 'RM') and (.w_TIPRIG $ 'RMA' AND NOT EMPTY(.w_GPCODICE) AND EMPTY(.w_GPCODMAT) AND EMPTY(.w_GPSERRIF)) and (.w_TEST AND NOT EMPTY(.w_GPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPQTAMOV_2_7
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
        case   not((.w_MxFLLOTT='+') OR CHKLOTUB(.w_GPCODUBI,'',0,.w_GPCODMAG,.w_CODART,.w_CODCON,.w_MxFLLOTT,.w_FLSTAT,.T.,.w_DATREG,' ','U','     ','     ',.T.,.w_DISLOT)) and (.w_TIPOOBJ_GEST<>'P' And g_PERUBI='S' AND NOT EMPTY(.w_CODART) AND NOT EMPTY(.w_GPCODMAG) AND .w_FLUBIC='S' AND .w_MxFLLOTT $ '+-' AND (.w_GPQTAMOV<>0 OR UPPER(.w_OBJ_GEST.CLASS)="TGSMA_KCR")  AND (EMPTY(.w_GPCODMAT) OR NOT EMPTY(.w_MTFLCARI))  AND NOT(NOT EMPTY(.w_GPSERRIF) AND  .w_FLRRIF$'+-')) and not(empty(.w_GPCODUBI)) and (.w_TEST AND NOT EMPTY(.w_GPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oGPCODUBI_2_19
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        case   not((.w_MxF2LOTT='+') OR CHKLOTUB(.w_GPCODUB2,'',0,.w_GPCODMA2,.w_CODART,.w_CODCON,.w_MxF2LOTT,.w_FLSTAT,.T.,.w_DATREG,' ','U','     ','     ',.T.,.w_DISLOT)) and (.w_TIPOOBJ_GEST<>'P' And g_PERUBI='S' AND NOT EMPTY(.w_CODART) AND NOT EMPTY(.w_GPCODMA2) AND .w_F2UBIC='S' AND .w_MxF2LOTT $ '+-' AND (.w_GPQTAMOV<>0 OR UPPER(.w_OBJ_GEST.CLASS)="TGSMA_KCR")  AND (EMPTY(.w_GPCODMAT) OR NOT EMPTY(.w_MTFLSCAR))  AND NOT(NOT EMPTY(.w_GPSERRIF) AND  .w_FLRRIF$'+-')) and not(empty(.w_GPCODUB2)) and (.w_TEST AND NOT EMPTY(.w_GPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oGPCODUB2_2_20
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        case   not((.w_FLSTAT<>'S' AND (Empty(.w_DATSCA) OR .w_DATSCA >.w_DATREG)) OR CHKLOTUB(.w_GPCODLOT,Space(10),0,.w_GPCODMAG,.w_CODART,.w_CODCON,.w_MxFLLOTT,.w_FLSTAT,.w_LOTZOOM,.w_DATREG,.w_FLRRIF,'L',.w_CAUMAG,.w_CAUCOL,.F.,.w_DISLOT)) and (.w_TIPOOBJ_GEST<>'P' And g_PERLOT='S' AND NOT EMPTY(.w_CODART) AND .w_FLLOTT$ 'SC' AND (.w_MxFLLOTT $ '+-' ) AND (.w_GPQTAMOV<>0 OR UPPER(.w_OBJ_GEST.CLASS)="TGSMA_KCR") AND (EMPTY(.w_GPCODMAT) OR NOT EMPTY(.w_MTFLCARI)) AND NOT(NOT EMPTY(.w_GPSERRIF) AND  .w_FLRRIF$'+-')) and not(empty(.w_GPCODLOT)) and (.w_TEST AND NOT EMPTY(.w_GPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oGPCODLOT_2_21
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice lotto inesistente, incongruente, scaduto o sospeso")
        case   not(.w_TIPRIG $ 'RMA' AND Not Empty( .w_GPCODICE )  AND EMPTY(.w_GPSERRIF) AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S')) and (.w_FLUSEP$'SQ' AND .w_GPUNIMIS<>.w_UNMIS1 AND .w_TIPRIG $ 'RMA' AND Not Empty( .w_GPCODICE )  AND EMPTY(.w_GPSERRIF) AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S')) and (.w_TEST AND NOT EMPTY(.w_GPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oGPQTAUM1_2_22
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Centro di Costo obsoleto!",.F.)) and (g_PERCCR='S' AND .w_FLANAL='S' And Not .w_TIPOOBJ_GEST$ 'P-M-R') and not(empty(.w_GPCODCEN)) and (.w_TEST AND NOT EMPTY(.w_GPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oGPCODCEN_2_23
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Centro di costo incongruente o obsoleto")
        case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.)) and (((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))  And Not .w_TIPOOBJ_GEST$ 'P-M-R') and not(empty(.w_GPCODCOM)) and (.w_TEST AND NOT EMPTY(.w_GPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oGPCODCOM_2_25
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
        case   not(checksscc('L',.w_GPUNILOG,.t.,.t.,.w_CODART,.w_GPCODLOT, .w_GPCODCOL)) and (not empty(.w_GPCODLOT) and not empty(.w_GPCODCOL) and .w_GPNUMCOL>0 and .w_FLPACK='S' AND (g_MADV<>'S' AND .w_FLESUL<>'S')) and not(empty(.w_GPUNILOG)) and (.w_TEST AND NOT EMPTY(.w_GPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oGPUNILOG_2_130
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if .w_TEST AND NOT EMPTY(.w_GPCODICE)
        * --- Area Manuale = Check Row
        * --- gsar_mpg
        * Controllo che la matricola inserita non sia gia presente
        * nel dettaglio
        Local L_Posizione,L_Area,L_CodMat,L_Trovato,L_oldcodmat
        
        L_Area = Select()
        
        Select (this.cTrsName)
        L_Posizione=Recno(this.cTrsName)
        L_CodMat= t_GPCODMAT
        L_CodArt= t_CODART
        
        if L_Posizione<>0 And Not Empty(L_CodMat)
        	
        	Go Top
         if g_MATR='S'
          *se Univocita non per matricola cerco anche per articolo
          if g_UNIMAT='M'
        	LOCATE FOR t_GPCODMAT = L_CodMat And Not Deleted()
          else
          LOCATE FOR t_GPCODMAT = L_CodMat And t_CODART = L_CodArt And Not Deleted()
          endif
         endif
        	
        	L_Trovato=Found()
        	
        	if L_Trovato And Recno()<>L_Posizione
        		i_bRes = .f.
        		i_bnoChk = .f.
        		i_cErrorMsg = Ah_MsgFormat("Matricola gi� utilizzata nel dettaglio")
        	ELse
        		if L_Trovato
        			Continue
        			if Found() And Recno()<>L_Posizione
        				i_bRes = .f.
        				i_bnoChk = .f.
        				i_cErrorMsg = Ah_MsgFormat("Matricola gi� utilizzata nel dettaglio")
        			endif
        		Endif
        	Endif
        
        	* mi riposiziono nella riga di partenza
        	Select (this.cTrsName)
        	Go L_Posizione
        endif
        			
        * mi rimetto nella vecchia area
        Select (L_Area)
        
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GPCODICE = this.w_GPCODICE
    this.o_GPCODMAT = this.w_GPCODMAT
    this.o_GPUNIMIS = this.w_GPUNIMIS
    this.o_GPQTAMOV = this.w_GPQTAMOV
    this.o_GPCODMAG = this.w_GPCODMAG
    this.o_GPCODLOT = this.w_GPCODLOT
    this.o_MTMAGCAR = this.w_MTMAGCAR
    this.o_GPCODCOM = this.w_GPCODCOM
    this.o_MTMAGSCA = this.w_MTMAGSCA
    this.o_FLCASC = this.w_FLCASC
    this.o_FLRICIVA = this.w_FLRICIVA
    this.o_RIIVDTIN = this.w_RIIVDTIN
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_TEST AND NOT EMPTY(t_GPCODICE))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_GPCODICE=space(20)
      .w_GPCODMAT=space(40)
      .w_CODART=space(20)
      .w_GESMAT=space(1)
      .w_GPUNIMIS=space(3)
      .w_GPQTAMOV=0
      .w_GPPREZZO=0
      .w_QTAORI=0
      .w_GP_ESCLU=space(1)
      .w_GPEVASIO=space(1)
      .w_OLDQTA=0
      .w_MAGPRE=space(5)
      .w_FLUBIC=space(1)
      .w_F2UBIC=space(1)
      .w_GPCODMAG=space(5)
      .w_GPCODMA2=space(5)
      .w_GPCODUBI=space(20)
      .w_GPCODUB2=space(20)
      .w_GPCODLOT=space(20)
      .w_GPQTAUM1=0
      .w_GPCODCEN=space(15)
      .w_MTMAGCAR=space(5)
      .w_GPCODCOM=space(15)
      .w_MTFLCARI=space(1)
      .w_TIPATT=space(1)
      .w_MTMAGSCA=space(5)
      .w_GPCODATT=space(15)
      .w_ESIRIS=space(1)
      .w_DESART=space(40)
      .w_EVTIPDOC=space(5)
      .w_MTKEYSAL=space(20)
      .w_EVNUMDOC=0
      .w_UNMIS1=space(3)
      .w_EVALFDOC=space(10)
      .w_FLSERG=space(1)
      .w_EVDATDOC=ctod("  /  /  ")
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_FLSTAT=space(1)
      .w_FLLOTT=space(1)
      .w_MOLTI3=0
      .w_MxFLLOTT=space(1)
      .w_MxF2LOTT=space(1)
      .w_STATOROW=space(1)
      .w_LOTZOOM=.f.
      .w_UBIZOOM=.f.
      .w_TEST=.f.
      .w_CAPTION=space(50)
      .w_TEXTCOLOR=0
      .w_OMAG=space(5)
      .w_OMAT=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPRIG=space(1)
      .w_FLFRAZ=space(1)
      .w_CODICE=space(20)
      .w_UNIMIS=space(3)
      .w_PREZZO=0
      .w_QTAMOV=0
      .w_CODLOT=space(20)
      .w_CODUBI=space(20)
      .w_CODMAT=space(40)
      .w_CODMAG=space(5)
      .w_GPSERRIF=space(10)
      .w_GPROWRIF=0
      .w_DESAR1=space(40)
      .w_MTFLSCAR=space(1)
      .w_MAGUBI=space(5)
      .w_MT__FLAG=space(1)
      .w_OLDPRE=0
      .w_ROWAGG=space(1)
      .w_NODOCU=space(1)
      .w_MT_SALDO=0
      .w_NOMSG=.f.
      .w_DATSCA=ctod("  /  /  ")
      .w_MTSERRIF=space(10)
      .w_MTROWRIF=0
      .w_MTRIFNUM=0
      .w_EVROWORD=0
      .w_DOQTAEV1=0
      .w_DOIMPEVA=0
      .w_FLRRIF=space(1)
      .w_PARAME=space(3)
      .w_FLAGRIS=space(1)
      .w_LENSCF=0
      .w_MTRIFSTO=0
      .w_MTRIFSTO=0
      .w_FLGROR=space(1)
      .w_SCONT1=0
      .w_SCONT2=0
      .w_SCONT3=0
      .w_SCONT4=0
      .w_DISLOT=space(1)
      .w_GPFLDOCU=space(1)
      .w_GPFLINTE=space(1)
      .w_GPFLACCO=space(1)
      .w_CODUB2=space(20)
      .w_CODMA2=space(5)
      .w_CODCEN_OR=space(15)
      .w_CODCOM_OR=space(15)
      .w_CODATT_OR=space(15)
      .w_FLUSEP=space(1)
      .w_OPERAT=space(1)
      .w_OPERA3=space(1)
      .w_MOLTIP=0
      .w_QTAUM1=0
      .w_GPUNILOG=space(18)
      .w_UNILOG=space(18)
      .w_TIPCO1=space(5)
      .w_TIPCO2=space(5)
      .w_TIPCO3=space(3)
      .w_TPCON1=space(3)
      .w_TPCON2=space(3)
      .w_TPCON3=space(3)
      .w_FLESUL=space(1)
      .w_CODCONF=space(3)
      .w_FLCOVA=space(1)
      .w_GPCODCOL=space(5)
      .w_GPNUMCOL=0
      .w_MVCODART=space(20)
      .w_MVCODLOT=space(20)
      .w_FLFRAZ1=space(1)
      .w_MODUM2=space(1)
      .w_DOQTAEVA=0
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_GPCODICE))
        .link_2_2('Full')
      endif
      .DoRTCalc(4,5,.f.)
      if not(empty(.w_CODART))
        .link_2_4('Full')
      endif
      .DoRTCalc(6,6,.f.)
        .w_GPUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_GPUNIMIS))
        .link_2_6('Full')
      endif
        .w_GPQTAMOV = IIF(.w_TIPRIG='F', 1, IIF(.w_TIPRIG='D', 0, .w_GPQTAMOV))
      .DoRTCalc(9,11,.f.)
        .w_GPEVASIO = IIF(.w_GPQTAMOV>=.w_QTAORI AND NOT EMPTY(.w_GPSERRIF) ,'S',' ')
        .w_OLDQTA = .w_GPQTAMOV
        .w_MAGPRE = iif(.w_GESMAT='S',SPACE(5),.w_MAGPRE)
      .DoRTCalc(15,16,.f.)
        .w_GPCODMAG = IIF(Not .w_TIPOOBJ_GEST$'PR'  , CALCMAG(1, .w_FLMGPR, '     ', .w_COMAG, .w_OMAG, .w_MAGPRE, .w_MAGTER) ,Space(5))
      .DoRTCalc(17,17,.f.)
      if not(empty(.w_GPCODMAG))
        .link_2_17('Full')
      endif
        .w_GPCODMA2 = IIF(.w_TIPRIG='R' AND NOT EMPTY(.w_F2CASC+.w_F2RISE+.w_F2ORDI+.w_F2IMPE), CALCMAG(1, .w_FLMTPR, '     ', .w_COMAT, .w_OMAT, .w_MAGPRE, .w_MAGTER), SPACE(5))
      .DoRTCalc(18,18,.f.)
      if not(empty(.w_GPCODMA2))
        .link_2_18('Full')
      endif
      .DoRTCalc(19,19,.f.)
      if not(empty(.w_GPCODUBI))
        .link_2_19('Full')
      endif
      .DoRTCalc(20,20,.f.)
      if not(empty(.w_GPCODUB2))
        .link_2_20('Full')
      endif
      .DoRTCalc(21,21,.f.)
      if not(empty(.w_GPCODLOT))
        .link_2_21('Full')
      endif
        .w_GPQTAUM1 = IIF(.w_TIPRIG='F', 1, CALQTAADV(.w_GPQTAMOV,.w_GPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,this, "GPQTAMOV"))
        .w_GPCODCEN = .w_OCEN
      .DoRTCalc(23,23,.f.)
      if not(empty(.w_GPCODCEN))
        .link_2_23('Full')
      endif
        .w_MTMAGCAR = IIF(.w_FLCASC='+' Or .w_FLRISE='-' Or .w_F2CASC='+' Or .w_F2RISE='-',.w_GPCODMAG,SPACE(5))
        .w_GPCODCOM = .w_OCOM
      .DoRTCalc(25,25,.f.)
      if not(empty(.w_GPCODCOM))
        .link_2_25('Full')
      endif
        .w_MTFLCARI = IIF(NOT EMPTY(.w_MTMAGCAR),IIF(.w_FLCASC='+' OR .w_F2CASC='+','E','R'),' ')
        .w_TIPATT = 'A'
        .w_MTMAGSCA = IIF(.w_FLCASC='-' Or .w_FLRISE='+',.w_GPCODMAG,SPACE(5))
        .w_GPCODATT = .w_OATT
      .DoRTCalc(29,29,.f.)
      if not(empty(.w_GPCODATT))
        .link_2_29('Full')
      endif
        .w_ESIRIS =  IIF( Not Empty( .w_MTFLSCAR) , .w_MTFLSCAR , .w_MTFLCARI )
      .DoRTCalc(31,32,.f.)
      if not(empty(.w_EVTIPDOC))
        .link_2_32('Full')
      endif
        .w_MTKEYSAL = .w_CODART
      .DoRTCalc(34,35,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_35('Full')
      endif
      .DoRTCalc(36,49,.f.)
        .w_MxFLLOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_FLUBIC='S'), LEFT(ALLTRIM(.w_FLCASC)+IIF(.w_FLRISE='+', '-', IIF(.w_FLRISE='-', '+', ' ')), 1), ' ')
      .DoRTCalc(51,52,.f.)
        .w_MxF2LOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_F2UBIC='S'), LEFT(ALLTRIM(.w_F2CASC)+IIF(.w_F2RISE='+', '-', IIF(.w_F2RISE='-', '+', ' ')), 1), ' ')
        .w_STATOROW = 'A'
      .DoRTCalc(55,55,.f.)
        .w_LOTZOOM = .F.
        .w_UBIZOOM = .T.
      .DoRTCalc(58,58,.f.)
        .w_TEST = .T.
        .oPgFrm.Page1.oPag.oObj_2_50.Calculate(.w_CAPTION,.w_TEXTCOLOR,.w_BACKCOLOR)
      .DoRTCalc(60,77,.f.)
        .w_OMAG = IIF(EMPTY(.w_GPCODMAG), IIF(EMPTY(.w_OMAG),IIF(EMPTY(.w_COMAG), g_MAGAZI, .w_COMAG),.w_OMAG), .w_GPCODMAG)
        .w_OMAT = IIF(EMPTY(.w_GPCODMAT), IIF(EMPTY(.w_OMAT),IIF(EMPTY(.w_COMAT), g_MAGAZI, .w_COMAT),.w_OMAT), .w_GPCODMAT)
      .DoRTCalc(80,130,.f.)
        .w_MTFLSCAR = IIF(NOT EMPTY(.w_MTMAGSCA),IIF(.w_FLCASC='-' ,'E','R'),' ')
        .w_MAGUBI = IIF(Not Empty( .w_MTMAGCAR),IIF( .w_MTMAGCAR = .w_GPCODMAT , .w_GPCODMAT , .w_GPCODMAG ),IIF( .w_MTMAGSCA =.w_GPCODMAT , .w_GPCODMAT , .w_GPCODMAG ))
      .DoRTCalc(133,138,.f.)
        .w_ROWAGG = 'A'
        .w_NODOCU = 'S'
        .oPgFrm.Page1.oPag.oObj_2_89.Calculate(IIF(EMPTY(.w_GPSERRIF), '','Doc Evaso:'))
        .oPgFrm.Page1.oPag.oObj_2_90.Calculate(IIF(EMPTY(.w_GPSERRIF),'', 'n.:'))
        .oPgFrm.Page1.oPag.oObj_2_91.Calculate(IIF(EMPTY(.w_GPSERRIF),'', '/'))
        .oPgFrm.Page1.oPag.oObj_2_92.Calculate(IIF(EMPTY(.w_GPSERRIF),'', 'del'))
      .DoRTCalc(141,141,.f.)
        .w_NOMSG = .T.
        .oPgFrm.Page1.oPag.oObj_2_101.Calculate(IIF(.w_GP_ESCLU='S', '<Riga Esclusa>',IIF(.w_GPEVASIO='S','<Riga a Saldo>','')))
      .DoRTCalc(143,190,.f.)
      if not(empty(.w_GPUNILOG))
        .link_2_130('Full')
      endif
      .DoRTCalc(191,199,.f.)
        .w_CODCONF = IIF(.w_GPUNIMIS=.w_UNMIS3,.w_TPCON3,IIF(.w_GPUNIMIS=.w_UNMIS2,.w_TPCON2,.w_TPCON1))
      .DoRTCalc(200,200,.f.)
      if not(empty(.w_CODCONF))
        .link_2_139('Full')
      endif
      .DoRTCalc(201,201,.f.)
        .w_GPCODCOL = IIF(.w_GPUNIMIS=.w_UNMIS3,.w_TIPCO3,IIF(.w_GPUNIMIS=.w_UNMIS2,.w_TIPCO2,.w_TIPCO1))
      .DoRTCalc(202,202,.f.)
      if not(empty(.w_GPCODCOL))
        .link_2_141('Full')
      endif
      .DoRTCalc(203,203,.f.)
        .w_MVCODART = .w_CODART
        .w_MVCODLOT = .w_GPCODLOT
    endwith
    this.DoRTCalc(206,245,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_GPCODICE = t_GPCODICE
    this.w_GPCODMAT = t_GPCODMAT
    this.w_CODART = t_CODART
    this.w_GESMAT = t_GESMAT
    this.w_GPUNIMIS = t_GPUNIMIS
    this.w_GPQTAMOV = t_GPQTAMOV
    this.w_GPPREZZO = t_GPPREZZO
    this.w_QTAORI = t_QTAORI
    this.w_GP_ESCLU = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGP_ESCLU_2_10.RadioValue(.t.)
    this.w_GPEVASIO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPEVASIO_2_12.RadioValue(.t.)
    this.w_OLDQTA = t_OLDQTA
    this.w_MAGPRE = t_MAGPRE
    this.w_FLUBIC = t_FLUBIC
    this.w_F2UBIC = t_F2UBIC
    this.w_GPCODMAG = t_GPCODMAG
    this.w_GPCODMA2 = t_GPCODMA2
    this.w_GPCODUBI = t_GPCODUBI
    this.w_GPCODUB2 = t_GPCODUB2
    this.w_GPCODLOT = t_GPCODLOT
    this.w_GPQTAUM1 = t_GPQTAUM1
    this.w_GPCODCEN = t_GPCODCEN
    this.w_MTMAGCAR = t_MTMAGCAR
    this.w_GPCODCOM = t_GPCODCOM
    this.w_MTFLCARI = t_MTFLCARI
    this.w_TIPATT = t_TIPATT
    this.w_MTMAGSCA = t_MTMAGSCA
    this.w_GPCODATT = t_GPCODATT
    this.w_ESIRIS = t_ESIRIS
    this.w_DESART = t_DESART
    this.w_EVTIPDOC = t_EVTIPDOC
    this.w_MTKEYSAL = t_MTKEYSAL
    this.w_EVNUMDOC = t_EVNUMDOC
    this.w_UNMIS1 = t_UNMIS1
    this.w_EVALFDOC = t_EVALFDOC
    this.w_FLSERG = t_FLSERG
    this.w_EVDATDOC = t_EVDATDOC
    this.w_UNMIS2 = t_UNMIS2
    this.w_UNMIS3 = t_UNMIS3
    this.w_FLSTAT = t_FLSTAT
    this.w_FLLOTT = t_FLLOTT
    this.w_MOLTI3 = t_MOLTI3
    this.w_MxFLLOTT = t_MxFLLOTT
    this.w_MxF2LOTT = t_MxF2LOTT
    this.w_STATOROW = t_STATOROW
    this.w_LOTZOOM = t_LOTZOOM
    this.w_UBIZOOM = t_UBIZOOM
    this.w_TEST = t_TEST
    this.w_CAPTION = t_CAPTION
    this.w_TEXTCOLOR = t_TEXTCOLOR
    this.w_OMAG = t_OMAG
    this.w_OMAT = t_OMAT
    this.w_DATOBSO = t_DATOBSO
    this.w_TIPRIG = t_TIPRIG
    this.w_FLFRAZ = t_FLFRAZ
    this.w_CODICE = t_CODICE
    this.w_UNIMIS = t_UNIMIS
    this.w_PREZZO = t_PREZZO
    this.w_QTAMOV = t_QTAMOV
    this.w_CODLOT = t_CODLOT
    this.w_CODUBI = t_CODUBI
    this.w_CODMAT = t_CODMAT
    this.w_CODMAG = t_CODMAG
    this.w_GPSERRIF = t_GPSERRIF
    this.w_GPROWRIF = t_GPROWRIF
    this.w_DESAR1 = t_DESAR1
    this.w_MTFLSCAR = t_MTFLSCAR
    this.w_MAGUBI = t_MAGUBI
    this.w_MT__FLAG = t_MT__FLAG
    this.w_OLDPRE = t_OLDPRE
    this.w_ROWAGG = t_ROWAGG
    this.w_NODOCU = t_NODOCU
    this.w_MT_SALDO = t_MT_SALDO
    this.w_NOMSG = t_NOMSG
    this.w_DATSCA = t_DATSCA
    this.w_MTSERRIF = t_MTSERRIF
    this.w_MTROWRIF = t_MTROWRIF
    this.w_MTRIFNUM = t_MTRIFNUM
    this.w_EVROWORD = t_EVROWORD
    this.w_DOQTAEV1 = t_DOQTAEV1
    this.w_DOIMPEVA = t_DOIMPEVA
    this.w_FLRRIF = t_FLRRIF
    this.w_PARAME = t_PARAME
    this.w_FLAGRIS = t_FLAGRIS
    this.w_LENSCF = t_LENSCF
    this.w_MTRIFSTO = t_MTRIFSTO
    this.w_MTRIFSTO = t_MTRIFSTO
    this.w_FLGROR = t_FLGROR
    this.w_SCONT1 = t_SCONT1
    this.w_SCONT2 = t_SCONT2
    this.w_SCONT3 = t_SCONT3
    this.w_SCONT4 = t_SCONT4
    this.w_DISLOT = t_DISLOT
    this.w_GPFLDOCU = t_GPFLDOCU
    this.w_GPFLINTE = t_GPFLINTE
    this.w_GPFLACCO = t_GPFLACCO
    this.w_CODUB2 = t_CODUB2
    this.w_CODMA2 = t_CODMA2
    this.w_CODCEN_OR = t_CODCEN_OR
    this.w_CODCOM_OR = t_CODCOM_OR
    this.w_CODATT_OR = t_CODATT_OR
    this.w_FLUSEP = t_FLUSEP
    this.w_OPERAT = t_OPERAT
    this.w_OPERA3 = t_OPERA3
    this.w_MOLTIP = t_MOLTIP
    this.w_QTAUM1 = t_QTAUM1
    this.w_GPUNILOG = t_GPUNILOG
    this.w_UNILOG = t_UNILOG
    this.w_TIPCO1 = t_TIPCO1
    this.w_TIPCO2 = t_TIPCO2
    this.w_TIPCO3 = t_TIPCO3
    this.w_TPCON1 = t_TPCON1
    this.w_TPCON2 = t_TPCON2
    this.w_TPCON3 = t_TPCON3
    this.w_FLESUL = t_FLESUL
    this.w_CODCONF = t_CODCONF
    this.w_FLCOVA = t_FLCOVA
    this.w_GPCODCOL = t_GPCODCOL
    this.w_GPNUMCOL = t_GPNUMCOL
    this.w_MVCODART = t_MVCODART
    this.w_MVCODLOT = t_MVCODLOT
    this.w_FLFRAZ1 = t_FLFRAZ1
    this.w_MODUM2 = t_MODUM2
    this.w_DOQTAEVA = t_DOQTAEVA
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_GPCODICE with this.w_GPCODICE
    replace t_GPCODMAT with this.w_GPCODMAT
    replace t_CODART with this.w_CODART
    replace t_GESMAT with this.w_GESMAT
    replace t_GPUNIMIS with this.w_GPUNIMIS
    replace t_GPQTAMOV with this.w_GPQTAMOV
    replace t_GPPREZZO with this.w_GPPREZZO
    replace t_QTAORI with this.w_QTAORI
    replace t_GP_ESCLU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGP_ESCLU_2_10.ToRadio()
    replace t_GPEVASIO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGPEVASIO_2_12.ToRadio()
    replace t_OLDQTA with this.w_OLDQTA
    replace t_MAGPRE with this.w_MAGPRE
    replace t_FLUBIC with this.w_FLUBIC
    replace t_F2UBIC with this.w_F2UBIC
    replace t_GPCODMAG with this.w_GPCODMAG
    replace t_GPCODMA2 with this.w_GPCODMA2
    replace t_GPCODUBI with this.w_GPCODUBI
    replace t_GPCODUB2 with this.w_GPCODUB2
    replace t_GPCODLOT with this.w_GPCODLOT
    replace t_GPQTAUM1 with this.w_GPQTAUM1
    replace t_GPCODCEN with this.w_GPCODCEN
    replace t_MTMAGCAR with this.w_MTMAGCAR
    replace t_GPCODCOM with this.w_GPCODCOM
    replace t_MTFLCARI with this.w_MTFLCARI
    replace t_TIPATT with this.w_TIPATT
    replace t_MTMAGSCA with this.w_MTMAGSCA
    replace t_GPCODATT with this.w_GPCODATT
    replace t_ESIRIS with this.w_ESIRIS
    replace t_DESART with this.w_DESART
    replace t_EVTIPDOC with this.w_EVTIPDOC
    replace t_MTKEYSAL with this.w_MTKEYSAL
    replace t_EVNUMDOC with this.w_EVNUMDOC
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_EVALFDOC with this.w_EVALFDOC
    replace t_FLSERG with this.w_FLSERG
    replace t_EVDATDOC with this.w_EVDATDOC
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_FLSTAT with this.w_FLSTAT
    replace t_FLLOTT with this.w_FLLOTT
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_MxFLLOTT with this.w_MxFLLOTT
    replace t_MxF2LOTT with this.w_MxF2LOTT
    replace t_STATOROW with this.w_STATOROW
    replace t_LOTZOOM with this.w_LOTZOOM
    replace t_UBIZOOM with this.w_UBIZOOM
    replace t_TEST with this.w_TEST
    replace t_CAPTION with this.w_CAPTION
    replace t_TEXTCOLOR with this.w_TEXTCOLOR
    replace t_OMAG with this.w_OMAG
    replace t_OMAT with this.w_OMAT
    replace t_DATOBSO with this.w_DATOBSO
    replace t_TIPRIG with this.w_TIPRIG
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_CODICE with this.w_CODICE
    replace t_UNIMIS with this.w_UNIMIS
    replace t_PREZZO with this.w_PREZZO
    replace t_QTAMOV with this.w_QTAMOV
    replace t_CODLOT with this.w_CODLOT
    replace t_CODUBI with this.w_CODUBI
    replace t_CODMAT with this.w_CODMAT
    replace t_CODMAG with this.w_CODMAG
    replace t_GPSERRIF with this.w_GPSERRIF
    replace t_GPROWRIF with this.w_GPROWRIF
    replace t_DESAR1 with this.w_DESAR1
    replace t_MTFLSCAR with this.w_MTFLSCAR
    replace t_MAGUBI with this.w_MAGUBI
    replace t_MT__FLAG with this.w_MT__FLAG
    replace t_OLDPRE with this.w_OLDPRE
    replace t_ROWAGG with this.w_ROWAGG
    replace t_NODOCU with this.w_NODOCU
    replace t_MT_SALDO with this.w_MT_SALDO
    replace t_NOMSG with this.w_NOMSG
    replace t_DATSCA with this.w_DATSCA
    replace t_MTSERRIF with this.w_MTSERRIF
    replace t_MTROWRIF with this.w_MTROWRIF
    replace t_MTRIFNUM with this.w_MTRIFNUM
    replace t_EVROWORD with this.w_EVROWORD
    replace t_DOQTAEV1 with this.w_DOQTAEV1
    replace t_DOIMPEVA with this.w_DOIMPEVA
    replace t_FLRRIF with this.w_FLRRIF
    replace t_PARAME with this.w_PARAME
    replace t_FLAGRIS with this.w_FLAGRIS
    replace t_LENSCF with this.w_LENSCF
    replace t_MTRIFSTO with this.w_MTRIFSTO
    replace t_MTRIFSTO with this.w_MTRIFSTO
    replace t_FLGROR with this.w_FLGROR
    replace t_SCONT1 with this.w_SCONT1
    replace t_SCONT2 with this.w_SCONT2
    replace t_SCONT3 with this.w_SCONT3
    replace t_SCONT4 with this.w_SCONT4
    replace t_DISLOT with this.w_DISLOT
    replace t_GPFLDOCU with this.w_GPFLDOCU
    replace t_GPFLINTE with this.w_GPFLINTE
    replace t_GPFLACCO with this.w_GPFLACCO
    replace t_CODUB2 with this.w_CODUB2
    replace t_CODMA2 with this.w_CODMA2
    replace t_CODCEN_OR with this.w_CODCEN_OR
    replace t_CODCOM_OR with this.w_CODCOM_OR
    replace t_CODATT_OR with this.w_CODATT_OR
    replace t_FLUSEP with this.w_FLUSEP
    replace t_OPERAT with this.w_OPERAT
    replace t_OPERA3 with this.w_OPERA3
    replace t_MOLTIP with this.w_MOLTIP
    replace t_QTAUM1 with this.w_QTAUM1
    replace t_GPUNILOG with this.w_GPUNILOG
    replace t_UNILOG with this.w_UNILOG
    replace t_TIPCO1 with this.w_TIPCO1
    replace t_TIPCO2 with this.w_TIPCO2
    replace t_TIPCO3 with this.w_TIPCO3
    replace t_TPCON1 with this.w_TPCON1
    replace t_TPCON2 with this.w_TPCON2
    replace t_TPCON3 with this.w_TPCON3
    replace t_FLESUL with this.w_FLESUL
    replace t_CODCONF with this.w_CODCONF
    replace t_FLCOVA with this.w_FLCOVA
    replace t_GPCODCOL with this.w_GPCODCOL
    replace t_GPNUMCOL with this.w_GPNUMCOL
    replace t_MVCODART with this.w_MVCODART
    replace t_MVCODLOT with this.w_MVCODLOT
    replace t_FLFRAZ1 with this.w_FLFRAZ1
    replace t_MODUM2 with this.w_MODUM2
    replace t_DOQTAEVA with this.w_DOQTAEVA
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mpgPag1 as StdContainer
  Width  = 815
  height = 478
  stdWidth  = 815
  stdheight = 478
  resizeXpos=333
  resizeYpos=224
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_7 as cp_runprogram with uid="NJALRRIZZM",left=175, top=555, width=177,height=23,;
    caption='GSAR_BPG(C)',;
   bGlobalFont=.t.,;
    prg="GSAR_BPG('C')",;
    cEvent = "New record",;
    nPag=1;
    , ToolTipText = "Carica il transitorio";
    , HelpContextID = 260032045


  add object oObj_1_16 as cp_runprogram with uid="BLXTLIRTNC",left=354, top=555, width=177,height=23,;
    caption='GSAR_BPG(S)',;
   bGlobalFont=.t.,;
    prg="GSAR_BPG('S')",;
    cEvent = "Salvaechiudi",;
    nPag=1;
    , ToolTipText = "Chiude la maschera e carica il transitorio della gestione sottostante";
    , HelpContextID = 260036141


  add object oObj_1_19 as cp_runprogram with uid="POKDSRQCDX",left=-4, top=555, width=177,height=23,;
    caption='GSAR_BPG(L)',;
   bGlobalFont=.t.,;
    prg="GSAR_BPG('L')",;
    cEvent = "LanciaDriver",;
    nPag=1;
    , ToolTipText = "Crea il cursore e lancia il driver per riempirlo...";
    , HelpContextID = 260034349


  add object oObj_1_66 as cp_runprogram with uid="ZRZPFWLUDD",left=533, top=555, width=177,height=23,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('CHANGED')",;
    cEvent = "w_GPCODMAT Changed",;
    nPag=1;
    , ToolTipText = "Al cambio della matricola effettua tutti i controlli pi� eventuali letture";
    , HelpContextID = 9457485


  add object oObj_1_67 as cp_runprogram with uid="QNFIELAANX",left=712, top=555, width=158,height=23,;
    caption='GSAR_BPG(Z)',;
   bGlobalFont=.t.,;
    prg="GSAR_BPG('Z')",;
    cEvent = "ImportaDoc",;
    nPag=1;
    , HelpContextID = 260037933

  add object oStr_1_2 as StdString with uid="XMAAHAHMJF",Visible=.t., Left=5, Top=10,;
    Alignment=2, Width=41, Height=18,;
    Caption="Riga"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="XHEGGIKTKX",Visible=.t., Left=349, Top=10,;
    Alignment=2, Width=146, Height=19,;
    Caption="Articolo"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="VVMYEMESSB",Visible=.t., Left=499, Top=10,;
    Alignment=2, Width=34, Height=18,;
    Caption="U.M."  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="BPVACTQPPQ",Visible=.t., Left=535, Top=10,;
    Alignment=2, Width=83, Height=18,;
    Caption="Quantit�"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="GAPCUOPWED",Visible=.t., Left=622, Top=10,;
    Alignment=2, Width=123, Height=18,;
    Caption="Prezzo unitario"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="HJPZDOOZXB",Visible=.t., Left=29, Top=339,;
    Alignment=1, Width=103, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="HNAKDRTYML",Visible=.t., Left=303, Top=385,;
    Alignment=1, Width=85, Height=18,;
    Caption="C/Ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' AND .w_FLANAL='S') Or .w_FLVEAC='A')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="NRORDMLTVI",Visible=.t., Left=303, Top=408,;
    Alignment=1, Width=85, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (NOT(((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))))
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="STODULZOXW",Visible=.t., Left=303, Top=431,;
    Alignment=1, Width=85, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (NOT(g_COMM='S' AND .w_FLGCOM='S' ))
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="LFKFBKAEUP",Visible=.t., Left=303, Top=385,;
    Alignment=1, Width=85, Height=18,;
    Caption="C/Costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' AND .w_FLANAL='S') Or .w_FLVEAC='V')
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="SPVGYUXSVQ",Visible=.t., Left=29, Top=312,;
    Alignment=1, Width=103, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (g_MATR<>'S' OR .w_TIPOOBJ_GEST='P')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="VIBFBASHHS",Visible=.t., Left=48, Top=10,;
    Alignment=2, Width=296, Height=18,;
    Caption="Matricola"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="VWBDSEQRMI",Visible=.t., Left=274, Top=10,;
    Alignment=2, Width=146, Height=18,;
    Caption="Descrizione"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="BURSSKFCFY",Visible=.t., Left=774, Top=10,;
    Alignment=2, Width=24, Height=18,;
    Caption="Sal."  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="BUDPJYNBWS",Visible=.t., Left=750, Top=10,;
    Alignment=2, Width=21, Height=18,;
    Caption="Esc"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="TOGNYPZJUR",Visible=.t., Left=290, Top=361,;
    Alignment=1, Width=98, Height=18,;
    Caption="Qta nella 1^UM:"  ;
  , bGlobalFont=.t.

  func oStr_1_78.mHide()
    with this.Parent.oContained
      return (.w_GPUNIMIS=.w_UNMIS1 OR EMPTY(.w_GPCODICE) OR (.w_GPUNIMIS<>.w_UNMIS1 And .w_GPUNIMIS<>.w_UNMIS2 AND .w_GPUNIMIS<>.w_UNMIS3))
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="RDISXNXVUU",Visible=.t., Left=3, Top=363,;
    Alignment=1, Width=129, Height=18,;
    Caption="Magazzino collegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="UPEIZFINLA",Visible=.t., Left=29, Top=386,;
    Alignment=1, Width=103, Height=18,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_81.mHide()
    with this.Parent.oContained
      return (g_PERUBI<>'S' OR .w_TIPOOBJ_GEST='P')
    endwith
  endfunc

  add object oStr_1_82 as StdString with uid="SWGJVUTAXP",Visible=.t., Left=3, Top=409,;
    Alignment=1, Width=129, Height=18,;
    Caption="Ubicazione collegata:"  ;
  , bGlobalFont=.t.

  func oStr_1_82.mHide()
    with this.Parent.oContained
      return (g_PERUBI<>'S' OR .w_TIPOOBJ_GEST='P')
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="TKLKTBWCIE",Visible=.t., Left=29, Top=433,;
    Alignment=1, Width=103, Height=18,;
    Caption="Lotto:"  ;
  , bGlobalFont=.t.

  func oStr_1_83.mHide()
    with this.Parent.oContained
      return (g_PERLOT<>'S'  OR .w_TIPOOBJ_GEST='P')
    endwith
  endfunc

  add object oStr_1_84 as StdString with uid="EJZURWBVYC",Visible=.t., Left=29, Top=456,;
    Alignment=1, Width=103, Height=18,;
    Caption="Unit� logistica:"  ;
  , bGlobalFont=.t.

  func oStr_1_84.mHide()
    with this.Parent.oContained
      return ((.w_FLPACK<>'S' and EMPTY(.w_GPUNILOG)) or g_MADV<>'S' or .w_FLESUL='S')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=31,;
    width=805+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=32,width=804+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oGPCODMAG_2_17.Refresh()
      this.Parent.oGPCODMA2_2_18.Refresh()
      this.Parent.oGPCODUBI_2_19.Refresh()
      this.Parent.oGPCODUB2_2_20.Refresh()
      this.Parent.oGPCODLOT_2_21.Refresh()
      this.Parent.oGPQTAUM1_2_22.Refresh()
      this.Parent.oGPCODCEN_2_23.Refresh()
      this.Parent.oGPCODCOM_2_25.Refresh()
      this.Parent.oGPCODATT_2_29.Refresh()
      this.Parent.oDESART_2_31.Refresh()
      this.Parent.oEVTIPDOC_2_32.Refresh()
      this.Parent.oEVNUMDOC_2_34.Refresh()
      this.Parent.oUNMIS1_2_35.Refresh()
      this.Parent.oEVALFDOC_2_36.Refresh()
      this.Parent.oEVDATDOC_2_38.Refresh()
      this.Parent.oEVROWORD_2_100.Refresh()
      this.Parent.oGPUNILOG_2_130.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oGPCODICE_2_2
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oGPUNIMIS_2_6
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oGPCODMAG_2_17 as StdTrsField with uid="UHRTVCWXVK",rtseq=17,rtrep=.t.,;
    cFormVar="w_GPCODMAG",value=space(5),;
    HelpContextID = 147459501,;
    cTotal="", bFixedPos=.t., cQueryName = "GPCODMAG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=135, Top=336, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_GPCODMAG"

  func oGPCODMAG_2_17.mCond()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST<>'P' AND EMPTY(.w_GPSERRIF)  AND .w_TIPRIG='R' AND NOT EMPTY(ALLTRIM(.w_FLCASC+.w_FLRISE+.w_FLIMPE+.w_FLORDI)) AND NOT EMPTY(.w_GPCODICE) AND (EMPTY(.w_GPCODMAT) OR .w_FLCASC='+' OR .w_FLRISE='-' OR .w_F2CASC='+' OR .w_F2RISE='-' ))
    endwith
  endfunc

  func oGPCODMAG_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
      if .not. empty(.w_GPCODUBI)
        bRes2=.link_2_19('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oGPCODMAG_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oGPCODMAG_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oGPCODMAG_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oGPCODMAG_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_GPCODMAG
    i_obj.ecpSave()
  endproc

  add object oGPCODMA2_2_18 as StdTrsField with uid="QYJBYHFHRQ",rtseq=18,rtrep=.t.,;
    cFormVar="w_GPCODMA2",value=space(5),;
    ToolTipText = "Magazzino collegato",;
    HelpContextID = 147459480,;
    cTotal="", bFixedPos=.t., cQueryName = "GPCODMA2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=135, Top=360, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_GPCODMA2"

  func oGPCODMA2_2_18.mCond()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST<>'P' AND EMPTY(.w_GPSERRIF)  AND .w_TIPRIG='R' AND NOT EMPTY(ALLTRIM(.w_F2CASC+.w_F2RISE)) AND NOT EMPTY(.w_GPCODICE))
    endwith
  endfunc

  func oGPCODMA2_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
      if .not. empty(.w_GPCODUB2)
        bRes2=.link_2_20('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oGPCODMA2_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oGPCODMA2_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oGPCODMA2_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oGPCODMA2_2_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_GPCODMA2
    i_obj.ecpSave()
  endproc

  add object oGPCODUBI_2_19 as StdTrsField with uid="AHBUSGXJNT",rtseq=19,rtrep=.t.,;
    cFormVar="w_GPCODUBI",value=space(20),;
    ToolTipText = "Codice ubicazione",;
    HelpContextID = 13241775,;
    cTotal="", bFixedPos=.t., cQueryName = "GPCODUBI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice ubicazione inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=135, Top=384, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_GPCODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_GPCODUBI"

  func oGPCODUBI_2_19.mCond()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST<>'P' And g_PERUBI='S' AND NOT EMPTY(.w_CODART) AND NOT EMPTY(.w_GPCODMAG) AND .w_FLUBIC='S' AND .w_MxFLLOTT $ '+-' AND (.w_GPQTAMOV<>0 OR UPPER(.w_OBJ_GEST.CLASS)="TGSMA_KCR")  AND (EMPTY(.w_GPCODMAT) OR NOT EMPTY(.w_MTFLCARI))  AND NOT(NOT EMPTY(.w_GPSERRIF) AND  .w_FLRRIF$'+-'))
    endwith
  endfunc

  func oGPCODUBI_2_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI<>'S' OR .w_TIPOOBJ_GEST='P')
    endwith
    endif
  endfunc

  func oGPCODUBI_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODUBI_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oGPCODUBI_2_19.mZoom
      with this.Parent.oContained
        GSMD_BZU(this.Parent.oContained,"A")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oGPCODUBI_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_GPCODMAG
     i_obj.w_UBCODICE=this.parent.oContained.w_GPCODUBI
    i_obj.ecpSave()
  endproc

  add object oGPCODUB2_2_20 as StdTrsField with uid="BWMXUDSTYA",rtseq=20,rtrep=.t.,;
    cFormVar="w_GPCODUB2",value=space(20),;
    ToolTipText = "Ubicazione magazzino collegato",;
    HelpContextID = 13241752,;
    cTotal="", bFixedPos=.t., cQueryName = "GPCODUB2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice ubicazione inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=135, Top=408, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_GPCODMA2", oKey_2_1="UBCODICE", oKey_2_2="this.w_GPCODUB2"

  func oGPCODUB2_2_20.mCond()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST<>'P' And g_PERUBI='S' AND NOT EMPTY(.w_CODART) AND NOT EMPTY(.w_GPCODMA2) AND .w_F2UBIC='S' AND .w_MxF2LOTT $ '+-' AND (.w_GPQTAMOV<>0 OR UPPER(.w_OBJ_GEST.CLASS)="TGSMA_KCR")  AND (EMPTY(.w_GPCODMAT) OR NOT EMPTY(.w_MTFLSCAR))  AND NOT(NOT EMPTY(.w_GPSERRIF) AND  .w_FLRRIF$'+-'))
    endwith
  endfunc

  func oGPCODUB2_2_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI<>'S' OR .w_TIPOOBJ_GEST='P')
    endwith
    endif
  endfunc

  func oGPCODUB2_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODUB2_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oGPCODUB2_2_20.mZoom
      with this.Parent.oContained
        GSMD_BZU(this.Parent.oContained,"B")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oGPCODUB2_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_GPCODMA2
     i_obj.w_UBCODICE=this.parent.oContained.w_GPCODUB2
    i_obj.ecpSave()
  endproc

  add object oGPCODLOT_2_21 as StdTrsField with uid="FMNIEQQAZE",rtseq=21,rtrep=.t.,;
    cFormVar="w_GPCODLOT",value=space(20),;
    HelpContextID = 130682298,;
    cTotal="", bFixedPos=.t., cQueryName = "GPCODLOT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto inesistente, incongruente, scaduto o sospeso",;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=135, Top=432, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_CODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_GPCODLOT"

  func oGPCODLOT_2_21.mCond()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST<>'P' And g_PERLOT='S' AND NOT EMPTY(.w_CODART) AND .w_FLLOTT$ 'SC' AND (.w_MxFLLOTT $ '+-' ) AND (.w_GPQTAMOV<>0 OR UPPER(.w_OBJ_GEST.CLASS)="TGSMA_KCR") AND (EMPTY(.w_GPCODMAT) OR NOT EMPTY(.w_MTFLCARI)) AND NOT(NOT EMPTY(.w_GPSERRIF) AND  .w_FLRRIF$'+-'))
    endwith
  endfunc

  func oGPCODLOT_2_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERLOT<>'S' OR .w_TIPOOBJ_GEST='P')
    endwith
    endif
  endfunc

  func oGPCODLOT_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODLOT_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oGPCODLOT_2_21.mZoom
      with this.Parent.oContained
        do GSMD_BZL with this.Parent.oContained
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oGPCODLOT_2_21.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_CODART
     i_obj.w_LOCODICE=this.parent.oContained.w_GPCODLOT
    i_obj.ecpSave()
  endproc

  add object oGPQTAUM1_2_22 as StdTrsField with uid="BRFISZZUMG",rtseq=22,rtrep=.t.,;
    cFormVar="w_GPQTAUM1",value=0,;
    ToolTipText = "Quantit� movimentata riferita alla 1^UM",;
    HelpContextID = 10481047,;
    cTotal="", bFixedPos=.t., cQueryName = "GPQTAUM1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=430, Top=358, cSayPict=['@Z '+v_PQ(11)], cGetPict=['@Z '+v_GQ(11)], BackStyle=0

  func oGPQTAUM1_2_22.mCond()
    with this.Parent.oContained
      return (.w_FLUSEP$'SQ' AND .w_GPUNIMIS<>.w_UNMIS1 AND .w_TIPRIG $ 'RMA' AND Not Empty( .w_GPCODICE )  AND EMPTY(.w_GPSERRIF) AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S'))
    endwith
  endfunc

  func oGPQTAUM1_2_22.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPUNIMIS=.w_UNMIS1 OR EMPTY(.w_GPCODICE) OR (.w_GPUNIMIS<>.w_UNMIS1 And .w_GPUNIMIS<>.w_UNMIS2 AND .w_GPUNIMIS<>.w_UNMIS3))
    endwith
    endif
  endfunc

  func oGPQTAUM1_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TIPRIG $ 'RMA' AND Not Empty( .w_GPCODICE )  AND EMPTY(.w_GPSERRIF) AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S'))
    endwith
    return bRes
  endfunc

  add object oGPCODCEN_2_23 as StdTrsField with uid="NWUGDIDWQK",rtseq=23,rtrep=.t.,;
    cFormVar="w_GPCODCEN",value=space(15),;
    ToolTipText = "Centro di costo/ricavo",;
    HelpContextID = 248122804,;
    cTotal="", bFixedPos=.t., cQueryName = "GPCODCEN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Centro di costo incongruente o obsoleto",;
   bGlobalFont=.t.,;
    Height=21, Width=143, Left=391, Top=382, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_GPCODCEN"

  func oGPCODCEN_2_23.mCond()
    with this.Parent.oContained
      return (g_PERCCR='S' AND .w_FLANAL='S' And Not .w_TIPOOBJ_GEST$ 'P-M-R')
    endwith
  endfunc

  func oGPCODCEN_2_23.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' AND .w_FLANAL='S'))
    endwith
    endif
  endfunc

  func oGPCODCEN_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODCEN_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oGPCODCEN_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oGPCODCEN_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Elenco centri di costo e ricavo",'',this.parent.oContained
  endproc
  proc oGPCODCEN_2_23.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_GPCODCEN
    i_obj.ecpSave()
  endproc

  add object oGPCODCOM_2_25 as StdTrsField with uid="VGYVWFULBB",rtseq=25,rtrep=.t.,;
    cFormVar="w_GPCODCOM",value=space(15),;
    ToolTipText = "Commessa",;
    HelpContextID = 248122803,;
    cTotal="", bFixedPos=.t., cQueryName = "GPCODCOM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
   bGlobalFont=.t.,;
    Height=21, Width=143, Left=391, Top=406, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_GPCODCOM"

  func oGPCODCOM_2_25.mCond()
    with this.Parent.oContained
      return (((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))  And Not .w_TIPOOBJ_GEST$ 'P-M-R')
    endwith
  endfunc

  func oGPCODCOM_2_25.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))))
    endwith
    endif
  endfunc

  func oGPCODCOM_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
      if .not. empty(.w_GPCODATT)
        bRes2=.link_2_29('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oGPCODCOM_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oGPCODCOM_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oGPCODCOM_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oGPCODCOM_2_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_GPCODCOM
    i_obj.ecpSave()
  endproc

  add object oGPCODATT_2_29 as StdTrsField with uid="KHKBZLOUSY",rtseq=29,rtrep=.t.,;
    cFormVar="w_GPCODATT",value=space(15),;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 214568378,;
    cTotal="", bFixedPos=.t., cQueryName = "GPCODATT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=143, Left=391, Top=430, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_GPCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_GPCODATT"

  func oGPCODATT_2_29.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_GPCODCOM) AND g_COMM='S' AND .w_FLGCOM='S')
    endwith
  endfunc

  func oGPCODATT_2_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(g_COMM='S' AND .w_FLGCOM='S' ))
    endwith
    endif
  endfunc

  func oGPCODATT_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODATT_2_29.ecpDrop(oSource)
    this.Parent.oContained.link_2_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oGPCODATT_2_29.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_GPCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_GPCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oGPCODATT_2_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit� di commessa",'',this.parent.oContained
  endproc
  proc oGPCODATT_2_29.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_GPCODCOM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_GPCODATT
    i_obj.ecpSave()
  endproc

  add object oDESART_2_31 as StdTrsField with uid="TEFQGDLLOQ",rtseq=31,rtrep=.t.,;
    cFormVar="w_DESART",value=space(40),enabled=.f.,;
    HelpContextID = 10289718,;
    cTotal="", bFixedPos=.t., cQueryName = "DESART",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=384, Left=134, Top=312, InputMask=replicate('X',40)

  func oDESART_2_31.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MATR<>'S' OR  .w_TIPOOBJ_GEST='P')
    endwith
    endif
  endfunc

  add object oEVTIPDOC_2_32 as StdTrsField with uid="SOHWEZIBKN",rtseq=32,rtrep=.t.,;
    cFormVar="w_EVTIPDOC",value=space(5),enabled=.f.,;
    HelpContextID = 8725385,;
    cTotal="", bFixedPos=.t., cQueryName = "EVTIPDOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=603, Top=335, InputMask=replicate('X',5), cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_EVTIPDOC"

  func oEVTIPDOC_2_32.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_GPSERRIF))
    endwith
    endif
  endfunc

  func oEVTIPDOC_2_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oEVNUMDOC_2_34 as StdTrsField with uid="RZMTVZDSHM",rtseq=34,rtrep=.t.,;
    cFormVar="w_EVNUMDOC",value=0,enabled=.f.,;
    HelpContextID = 6341513,;
    cTotal="", bFixedPos=.t., cQueryName = "EVNUMDOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=603, Top=357

  func oEVNUMDOC_2_34.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_GPSERRIF))
    endwith
    endif
  endfunc

  add object oUNMIS1_2_35 as StdTrsField with uid="XPSXLIJTFQ",rtseq=35,rtrep=.t.,;
    cFormVar="w_UNMIS1",value=space(3),enabled=.f.,;
    HelpContextID = 229944390,;
    cTotal="", bFixedPos=.t., cQueryName = "UNMIS1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=391, Top=358, InputMask=replicate('X',3), cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_UNMIS1"

  func oUNMIS1_2_35.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPUNIMIS=.w_UNMIS1 OR EMPTY(.w_GPCODICE) OR (.w_GPUNIMIS<>.w_UNMIS1 And .w_GPUNIMIS<>.w_UNMIS2 AND .w_GPUNIMIS<>.w_UNMIS3))
    endwith
    endif
  endfunc

  func oUNMIS1_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oEVALFDOC_2_36 as StdTrsField with uid="IJRCIFJONO",rtseq=36,rtrep=.t.,;
    cFormVar="w_EVALFDOC",value=space(10),enabled=.f.,;
    HelpContextID = 266793865,;
    cTotal="", bFixedPos=.t., cQueryName = "EVALFDOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=729, Top=357, InputMask=replicate('X',10)

  func oEVALFDOC_2_36.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_GPSERRIF))
    endwith
    endif
  endfunc

  add object oEVDATDOC_2_38 as StdTrsField with uid="BRYBNZWWAL",rtseq=39,rtrep=.t.,;
    cFormVar="w_EVDATDOC",value=ctod("  /  /  "),enabled=.f.,;
    HelpContextID = 12329865,;
    cTotal="", bFixedPos=.t., cQueryName = "EVDATDOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=736, Top=335

  func oEVDATDOC_2_38.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_GPSERRIF))
    endwith
    endif
  endfunc

  add object oObj_2_50 as cp_calclbl with uid="CNZZSNXNRN",width=222,height=18,;
   left=481, top=266,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=2;
    , HelpContextID = 30576614

  add object oBtn_2_54 as StdButton with uid="INDUHDNEMW",width=48,height=45,;
   left=6, top=263,;
    CpPicture="BMP\SCARICA.BMP", caption="", nPag=2;
    , ToolTipText = "Dati rilevati dal lettore ottico";
    , HelpContextID = 257805642;
    , TabStop=.f.,Caption='L\<ettore';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_54.Click()
      do gsar_kle with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBtn_2_73 as StdButton with uid="OCNHODCHWV",width=48,height=45,;
   left=111, top=263,;
    CpPicture="BMP\CARLOTTI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per creare un nuovo lotto con i parametri impostati";
    , HelpContextID = 22922058;
    , TabStop=.f.,Caption='\<Lotto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_73.Click()
      with this.Parent.oContained
        do GSMA_BKL with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_73.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_perlot<>'S' OR .w_FLCASC <>'+' OR EMPTY(.w_CODART) OR .w_FLLOTT = 'N' OR EMPTY(.w_FLLOTT) OR .cFunction = 'Query')
    endwith
   endif
  endfunc

  add object oObj_2_89 as cp_calclbl with uid="ZUOMZYVRWU",width=69,height=18,;
   left=533, top=337,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=2;
    , HelpContextID = 30576614

  add object oObj_2_90 as cp_calclbl with uid="ELGHSUELJP",width=23,height=18,;
   left=579, top=359,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=2;
    , HelpContextID = 30576614

  add object oObj_2_91 as cp_calclbl with uid="LWFZOUPXMV",width=19,height=18,;
   left=718, top=359,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=2;
    , HelpContextID = 30576614

  add object oObj_2_92 as cp_calclbl with uid="NLLQAPGNEM",width=23,height=18,;
   left=712, top=337,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=2;
    , HelpContextID = 30576614

  add object oBtn_2_98 as StdButton with uid="PPLYHNSIWX",width=48,height=45,;
   left=58, top=263,;
    CpPicture="bmp\GENERA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per generare nuove matricole";
    , HelpContextID = 20985218;
    , TabStop=.f.,Caption='\<Matricole';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_98.Click()
      do gsmd_kcm with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_98.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MATR<>'S' OR .w_GESMAT<>'S' OR .cFunction = 'Query'  OR .w_TIPOOBJ_GEST='P')
    endwith
   endif
  endfunc

  add object oEVROWORD_2_100 as StdTrsField with uid="AGOLODEDKE",rtseq=148,rtrep=.t.,;
    cFormVar="w_EVROWORD",value=0,enabled=.f.,;
    ToolTipText = "Numero di riga evasione",;
    HelpContextID = 200999818,;
    cTotal="", bFixedPos=.t., cQueryName = "EVROWORD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=660, Top=335, cSayPict=["99999"], cGetPict=["99999"]

  func oEVROWORD_2_100.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_GPSERRIF))
    endwith
    endif
  endfunc

  add object oObj_2_101 as cp_calclbl with uid="DMTOBXUTQJ",width=96,height=18,;
   left=712, top=264,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=2;
    , HelpContextID = 30576614

  add object oBtn_2_105 as StdButton with uid="IKLWSKNLGG",width=48,height=45,;
   left=735, top=382,;
    CpPicture="BMP\DOC.BMP", caption="", nPag=2;
    , ToolTipText = "Visualizza il documento evaso dalla riga selezionata";
    , HelpContextID = 63664154;
    , TabStop=.f.,Caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_105.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_GPSERRIF, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_105.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_GPSERRIF))
    endwith
   endif
  endfunc

  add object oGPUNILOG_2_130 as StdTrsField with uid="REDRYNTMJZ",rtseq=190,rtrep=.t.,;
    cFormVar="w_GPUNILOG",value=space(18),;
    HelpContextID = 135933357,;
    cTotal="", bFixedPos=.t., cQueryName = "GPUNILOG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=135, Top=456, InputMask=replicate('X',18), bHasZoom = .t. , cLinkFile="UNIT_LOG", cZoomOnZoom="GSMD_AUL", oKey_1_1="UL__SSCC", oKey_1_2="this.w_GPUNILOG"

  func oGPUNILOG_2_130.mCond()
    with this.Parent.oContained
      return (not empty(.w_GPCODLOT) and not empty(.w_GPCODCOL) and .w_GPNUMCOL>0 and .w_FLPACK='S' AND (g_MADV<>'S' AND .w_FLESUL<>'S'))
    endwith
  endfunc

  func oGPUNILOG_2_130.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLPACK<>'S' and EMPTY(.w_GPUNILOG)) or g_MADV<>'S' or .w_FLESUL='S')
    endwith
    endif
  endfunc

  func oGPUNILOG_2_130.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_130('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPUNILOG_2_130.ecpDrop(oSource)
    this.Parent.oContained.link_2_130('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oGPUNILOG_2_130.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIT_LOG','*','UL__SSCC',cp_AbsName(this.parent,'oGPUNILOG_2_130'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_AUL',"Unit� logistiche",'GSMD_KDA.UNIT_LOG_VZM',this.parent.oContained
  endproc
  proc oGPUNILOG_2_130.mZoomOnZoom
    local i_obj
    i_obj=GSMD_AUL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UL__SSCC=this.parent.oContained.w_GPUNILOG
    i_obj.ecpSave()
  endproc

  add object oStr_2_55 as StdString with uid="AVMYSILAEK",Visible=.t., Left=405, Top=267,;
    Alignment=0, Width=70, Height=18,;
    Caption="Errore riga"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_11 as StdBox with uid="OCDNQYRADL",left=748, top=8, width=2,height=253

  add object oBox_2_53 as StdBox with uid="OVTSKFLFBC",left=478, top=264, width=228,height=23

  add object oBox_2_56 as StdBox with uid="MHYMBEDBMX",left=0, top=7, width=817,height=25

  add object oBox_2_57 as StdBox with uid="XOVSCPOJVX",left=196, top=8, width=2,height=253

  add object oBox_2_58 as StdBox with uid="ABBCSLUEST",left=46, top=8, width=2,height=253

  add object oBox_2_59 as StdBox with uid="OTXFRRRBOC",left=345, top=8, width=2,height=253

  add object oBox_2_60 as StdBox with uid="HRKARZYZOK",left=496, top=8, width=2,height=253

  add object oBox_2_61 as StdBox with uid="XBBMNNFVZX",left=533, top=8, width=2,height=253

  add object oBox_2_62 as StdBox with uid="TPANIGDCXR",left=619, top=8, width=2,height=253

  add object oBox_2_99 as StdBox with uid="BOWPLIRXNN",left=772, top=8, width=2,height=253

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oESEGUITOIMPORT_3_1 as StdField with uid="PXBLXZQXFB",rtseq=90,rtrep=.f.,;
    cFormVar="w_ESEGUITOIMPORT",value=.f.,;
    ToolTipText = "MESSO A.T. DA GSVE_KIM IN CASO DI CONFERMA OPERAZIONE - USATO NEL BOTTONE IMPORTA",;
    HelpContextID = 12156379,;
    cQueryName = "ESEGUITOIMPORT",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=131, Left=751, Top=531

  add object oBtn_3_2 as StdButton with uid="ZSJURYLGAP",width=48,height=45,;
   left=164, top=263,;
    CpPicture="BMP\EVASIONE.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per selezionare i documenti da evadere con le righe selezionate";
    , HelpContextID = 46987910;
    , TabStop=.f.,Caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_2.Click()
      do GSVE_KIM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DOCCOLL) OR .w_ESEGUITOIMPORT)
    endwith
   endif
  endfunc

  add object oBtn_3_3 as StdButton with uid="HZPJQRFJAB",width=48,height=45,;
   left=680, top=431,;
    CpPicture="BMP\OK.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per inserire i dati nella gestione sottostante";
    , HelpContextID = 248974886;
    , Caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_3.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBtn_3_4 as StdButton with uid="YKTSCIFCSO",width=48,height=45,;
   left=735, top=431,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per uscire dalla maschera";
    , HelpContextID = 140103610;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

  define class tgsar_mpgPag2 as StdContainer
    Width  = 815
    height = 478
    stdWidth  = 815
    stdheight = 478
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODMAGTES_4_2 as StdField with uid="YWWCDKFCZP",rtseq=64,rtrep=.f.,;
    cFormVar = "w_CODMAGTES", cQueryName = "CODMAGTES",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 43524507,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=134, Top=18, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAGTES"

  func oCODMAGTES_4_2.mCond()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST<>'P')
    endwith
  endfunc

  func oCODMAGTES_4_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_2('Part',this)
      if .not. empty(.w_UBITES)
        bRes2=.link_4_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODMAGTES_4_2.ecpDrop(oSource)
    this.Parent.oContained.link_4_2('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAGTES_4_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAGTES_4_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oCODMAGTES_4_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAGTES
    i_obj.ecpSave()
  endproc

  add object oDESMAGTES_4_3 as StdField with uid="AJLJDCITLM",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESMAGTES", cQueryName = "DESMAGTES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 43583403,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=290, Top=18, InputMask=replicate('X',30)

  add object oUBITES_4_6 as StdField with uid="ZBMAMKDOHV",rtseq=67,rtrep=.f.,;
    cFormVar = "w_UBITES", cQueryName = "UBITES",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 249520198,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=134, Top=45, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_CODMAGTES", oKey_2_1="UBCODICE", oKey_2_2="this.w_UBITES"

  func oUBITES_4_6.mCond()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST<>'P' And g_PERUBI='S' AND NOT EMPTY(.w_CODMAGTES) AND .w_FLUBICTES='S' AND LEFT(ALLTRIM(.w_FLCASC)+IIF(.w_FLRISE='+', '-', IIF(.w_FLRISE='-', '+', ' ')), 1) $ '+-')
    endwith
  endfunc

  func oUBITES_4_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
    endif
  endfunc

  func oUBITES_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oUBITES_4_6.ecpDrop(oSource)
    this.Parent.oContained.link_4_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUBITES_4_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_CODMAGTES)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_CODMAGTES)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oUBITES_4_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_MUB',"Elenco ubicazioni",'',this.parent.oContained
  endproc
  proc oUBITES_4_6.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_CODMAGTES
     i_obj.w_UBCODICE=this.parent.oContained.w_UBITES
    i_obj.ecpSave()
  endproc


  add object oBtn_4_7 as StdButton with uid="PSBFVFELTZ",left=580, top=22, width=48,height=45,;
    CpPicture="BMP\APPLICA.BMP", caption="", nPag=4;
    , ToolTipText = "Applica le informazioni magazzino / ubicazione a tutte le righe";
    , HelpContextID = 20577542;
    , Caption='\<Applica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_7.Click()
      with this.Parent.oContained
        GSAR_BPG(this.Parent.oContained,"G-MAG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_7.mCond()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST<>'P')
    endwith
  endfunc

  add object oCODMA2TES_4_8 as StdField with uid="QKVYAAVYLU",rtseq=68,rtrep=.f.,;
    cFormVar = "w_CODMA2TES", cQueryName = "CODMA2TES",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 228073883,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=134, Top=86, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMA2TES"

  func oCODMA2TES_4_8.mCond()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST<>'P')
    endwith
  endfunc

  func oCODMA2TES_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_8('Part',this)
      if .not. empty(.w_UB2TES)
        bRes2=.link_4_9('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODMA2TES_4_8.ecpDrop(oSource)
    this.Parent.oContained.link_4_8('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMA2TES_4_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMA2TES_4_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oCODMA2TES_4_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMA2TES
    i_obj.ecpSave()
  endproc

  add object oUB2TES_4_9 as StdField with uid="LNZTZWHRHM",rtseq=69,rtrep=.f.,;
    cFormVar = "w_UB2TES", cQueryName = "UB2TES",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 249425990,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=134, Top=113, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_CODMA2TES", oKey_2_1="UBCODICE", oKey_2_2="this.w_UB2TES"

  func oUB2TES_4_9.mCond()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST<>'P' And g_PERUBI='S' AND NOT EMPTY(.w_CODMAGTES) AND .w_FLUBICTES='S' AND LEFT(ALLTRIM(.w_FLCASC)+IIF(.w_FLRISE='+', '-', IIF(.w_FLRISE='-', '+', ' ')), 1) $ '+-')
    endwith
  endfunc

  func oUB2TES_4_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
    endif
  endfunc

  func oUB2TES_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oUB2TES_4_9.ecpDrop(oSource)
    this.Parent.oContained.link_4_9('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUB2TES_4_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_CODMA2TES)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_CODMA2TES)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oUB2TES_4_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_MUB',"Elenco ubicazioni",'',this.parent.oContained
  endproc
  proc oUB2TES_4_9.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_CODMA2TES
     i_obj.w_UBCODICE=this.parent.oContained.w_UB2TES
    i_obj.ecpSave()
  endproc


  add object oBtn_4_10 as StdButton with uid="LWBHDZQVCC",left=580, top=90, width=48,height=45,;
    CpPicture="BMP\APPLICA.BMP", caption="", nPag=4;
    , ToolTipText = "Applica le informazioni magazzino / ubicazione a tutte le righe";
    , HelpContextID = 20577542;
    , Caption='A\<pplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_10.Click()
      with this.Parent.oContained
        GSAR_BPG(this.Parent.oContained,"G-MA2")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_10.mCond()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST<>'P')
    endwith
  endfunc

  add object oCODCENTES_4_11 as StdField with uid="JZNKQPJKBT",rtseq=70,rtrep=.f.,;
    cFormVar = "w_CODCENTES", cQueryName = "CODCENTES",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di costo incongruente o obsoleto",;
    ToolTipText = "Centro di costo/ricavo",;
    HelpContextID = 164503963,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=134, Top=153, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CODCENTES"

  func oCODCENTES_4_11.mCond()
    with this.Parent.oContained
      return (g_PERCCR='S' AND .w_FLANAL='S' And  Not .w_TIPOOBJ_GEST$ 'P-M-R')
    endwith
  endfunc

  func oCODCENTES_4_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' AND .w_FLANAL='S'))
    endwith
    endif
  endfunc

  func oCODCENTES_4_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCENTES_4_11.ecpDrop(oSource)
    this.Parent.oContained.link_4_11('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCENTES_4_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCODCENTES_4_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Elenco centri costo e ricavo",'',this.parent.oContained
  endproc
  proc oCODCENTES_4_11.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CODCENTES
    i_obj.ecpSave()
  endproc

  add object oCODCOMTES_4_12 as StdField with uid="ABWBOEZNIR",rtseq=71,rtrep=.f.,;
    cFormVar = "w_CODCOMTES", cQueryName = "CODCOMTES",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
    ToolTipText = "Codice commessa",;
    HelpContextID = 158212507,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=134, Top=179, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOMTES"

  func oCODCOMTES_4_12.mCond()
    with this.Parent.oContained
      return (((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S')) And Not .w_TIPOOBJ_GEST$ 'P-M-R')
    endwith
  endfunc

  func oCODCOMTES_4_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))))
    endwith
    endif
  endfunc

  func oCODCOMTES_4_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_12('Part',this)
      if .not. empty(.w_CODATTTES)
        bRes2=.link_4_13('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOMTES_4_12.ecpDrop(oSource)
    this.Parent.oContained.link_4_12('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOMTES_4_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOMTES_4_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oCODCOMTES_4_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODCOMTES
    i_obj.ecpSave()
  endproc

  add object oCODATTTES_4_13 as StdField with uid="IZCFGERWDI",rtseq=72,rtrep=.f.,;
    cFormVar = "w_CODATTTES", cQueryName = "CODATTTES",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 256106085,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=134, Top=205, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOMTES", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATTTES"

  func oCODATTTES_4_13.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODCOMTES) AND g_COMM='S' AND .w_FLGCOM='S')
    endwith
  endfunc

  func oCODATTTES_4_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(g_COMM='S' AND .w_FLGCOM='S'))
    endwith
    endif
  endfunc

  func oCODATTTES_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATTTES_4_13.ecpDrop(oSource)
    this.Parent.oContained.link_4_13('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATTTES_4_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOMTES)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOMTES)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATTTES_4_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit� di commessa",'',this.parent.oContained
  endproc
  proc oCODATTTES_4_13.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOMTES
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_CODATTTES
    i_obj.ecpSave()
  endproc

  add object oUBIDESTES_4_14 as StdField with uid="PAKXZRNZHZ",rtseq=73,rtrep=.f.,;
    cFormVar = "w_UBIDESTES", cQueryName = "UBIDESTES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 248473019,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=290, Top=45, InputMask=replicate('X',40)

  func oUBIDESTES_4_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
    endif
  endfunc


  add object oBtn_4_15 as StdButton with uid="BTGHEPOPZV",left=580, top=182, width=48,height=45,;
    CpPicture="BMP\APPLICA.BMP", caption="", nPag=4;
    , ToolTipText = "Applica le informazioni analitiche a tutte le righe";
    , HelpContextID = 20577542;
    , Caption='App\<lica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_15.Click()
      with this.Parent.oContained
        GSAR_BPG(this.Parent.oContained,"G-ANA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_15.mCond()
    with this.Parent.oContained
      return (Not .w_TIPOOBJ_GEST$ 'P-M-R')
    endwith
  endfunc

  func oBtn_4_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' AND .w_FLANAL='S') And NOT(((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))) And NOT(g_COMM='S' AND .w_FLGCOM='S'))
    endwith
   endif
  endfunc

  add object oDESPIA_4_18 as StdField with uid="TMRVQTDNVO",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DESPIA", cQueryName = "DESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 219939382,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=290, Top=153, InputMask=replicate('X',40)

  func oDESPIA_4_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' AND .w_FLANAL='S'))
    endwith
    endif
  endfunc

  add object oDESCAN_4_20 as StdField with uid="AGEPRCGPSG",rtseq=87,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 160367158,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=290, Top=179, InputMask=replicate('X',40)

  func oDESCAN_4_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))))
    endwith
    endif
  endfunc

  add object oATDES_4_22 as StdField with uid="UYUYEDCEZO",rtseq=89,rtrep=.f.,;
    cFormVar = "w_ATDES", cQueryName = "ATDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 55566074,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=290, Top=205, InputMask=replicate('X',30)

  func oATDES_4_22.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(g_COMM='S' AND .w_FLGCOM='S' ))
    endwith
    endif
  endfunc

  add object oFLVSRI_4_24 as StdCheck with uid="JTVUITDNDV",rtseq=155,rtrep=.f.,left=663, top=63, caption="Vostro riferimento",;
    ToolTipText = "Se attivo: aggiunge una riga descrittiva di vostro riferimento ad ogni nuovo doc",;
    HelpContextID = 95369558,;
    cFormVar="w_FLVSRI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLVSRI_4_24.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLVSRI,&i_cF..t_FLVSRI),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oFLVSRI_4_24.GetRadio()
    this.Parent.oContained.w_FLVSRI = this.RadioValue()
    return .t.
  endfunc

  func oFLVSRI_4_24.ToRadio()
    this.Parent.oContained.w_FLVSRI=trim(this.Parent.oContained.w_FLVSRI)
    return(;
      iif(this.Parent.oContained.w_FLVSRI=='S',1,;
      0))
  endfunc

  func oFLVSRI_4_24.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oFLVSRI_4_24.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(g_ARTDES))
    endwith
  endfunc

  func oFLVSRI_4_24.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DOCCOLL))
    endwith
    endif
  endfunc

  add object oFLRIDE_4_25 as StdCheck with uid="ONSACLVYYL",rtseq=156,rtrep=.f.,left=663, top=84, caption="Rif. descrittivo",;
    ToolTipText = "Se attivo: aggiunge una riga descrittiva di rif. descrittivo ad ogni nuovo doc",;
    HelpContextID = 12908886,;
    cFormVar="w_FLRIDE", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLRIDE_4_25.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLRIDE,&i_cF..t_FLRIDE),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oFLRIDE_4_25.GetRadio()
    this.Parent.oContained.w_FLRIDE = this.RadioValue()
    return .t.
  endfunc

  func oFLRIDE_4_25.ToRadio()
    this.Parent.oContained.w_FLRIDE=trim(this.Parent.oContained.w_FLRIDE)
    return(;
      iif(this.Parent.oContained.w_FLRIDE=='S',1,;
      0))
  endfunc

  func oFLRIDE_4_25.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oFLRIDE_4_25.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(g_ARTDES))
    endwith
  endfunc

  func oFLRIDE_4_25.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DOCCOLL))
    endwith
    endif
  endfunc

  add object oFLNSRI_4_26 as StdCheck with uid="QNJYIHULOC",rtseq=157,rtrep=.f.,left=663, top=42, caption="Nostro riferimento",;
    ToolTipText = "Se attivo: aggiunge una riga descrittiva di nostro riferimento ad ogni nuovo doc",;
    HelpContextID = 95336790,;
    cFormVar="w_FLNSRI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLNSRI_4_26.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLNSRI,&i_cF..t_FLNSRI),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oFLNSRI_4_26.GetRadio()
    this.Parent.oContained.w_FLNSRI = this.RadioValue()
    return .t.
  endfunc

  func oFLNSRI_4_26.ToRadio()
    this.Parent.oContained.w_FLNSRI=trim(this.Parent.oContained.w_FLNSRI)
    return(;
      iif(this.Parent.oContained.w_FLNSRI=='S',1,;
      0))
  endfunc

  func oFLNSRI_4_26.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oFLNSRI_4_26.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(g_ARTDES))
    endwith
  endfunc

  func oFLNSRI_4_26.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DOCCOLL))
    endwith
    endif
  endfunc

  add object oDESMA2TES_4_33 as StdField with uid="JNRWHMRQYT",rtseq=179,rtrep=.f.,;
    cFormVar = "w_DESMA2TES", cQueryName = "DESMA2TES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 228132779,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=290, Top=86, InputMask=replicate('X',30)

  add object oUB2DESTES_4_35 as StdField with uid="NQZWKKWRBK",rtseq=180,rtrep=.f.,;
    cFormVar = "w_UB2DESTES", cQueryName = "UB2DESTES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 248378811,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=290, Top=113, InputMask=replicate('X',40)

  func oUB2DESTES_4_35.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
    endif
  endfunc

  add object oStr_4_1 as StdString with uid="KMEJAWDYRM",Visible=.t., Left=63, Top=20,;
    Alignment=1, Width=69, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_4_5 as StdString with uid="ODASSGEANO",Visible=.t., Left=68, Top=47,;
    Alignment=1, Width=64, Height=18,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  func oStr_4_5.mHide()
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
  endfunc

  add object oStr_4_16 as StdString with uid="KWLJKMHVUY",Visible=.t., Left=77, Top=154,;
    Alignment=1, Width=55, Height=18,;
    Caption="C/Costo:"  ;
  , bGlobalFont=.t.

  func oStr_4_16.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' AND .w_FLANAL='S') Or .w_FLVEAC='V')
    endwith
  endfunc

  add object oStr_4_19 as StdString with uid="BJHLVZECQF",Visible=.t., Left=63, Top=180,;
    Alignment=1, Width=69, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_4_19.mHide()
    with this.Parent.oContained
      return (NOT(((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))))
    endwith
  endfunc

  add object oStr_4_21 as StdString with uid="RRNEPXTUBP",Visible=.t., Left=79, Top=206,;
    Alignment=1, Width=53, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_4_21.mHide()
    with this.Parent.oContained
      return (NOT(g_COMM='S' AND .w_FLGCOM='S' ))
    endwith
  endfunc

  add object oStr_4_23 as StdString with uid="MDSIHNJQHM",Visible=.t., Left=73, Top=154,;
    Alignment=1, Width=59, Height=18,;
    Caption="C/Ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_4_23.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' AND .w_FLANAL='S') Or .w_FLVEAC='A')
    endwith
  endfunc

  add object oStr_4_27 as StdString with uid="EUAPEHJLKN",Visible=.t., Left=663, Top=16,;
    Alignment=0, Width=122, Height=18,;
    Caption="Riferimenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_4_27.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_DOCCOLL))
    endwith
  endfunc

  add object oStr_4_32 as StdString with uid="ZKEMIBSVDL",Visible=.t., Left=2, Top=88,;
    Alignment=1, Width=130, Height=18,;
    Caption="Magazzino collegato:"  ;
  , bGlobalFont=.t.

  add object oStr_4_34 as StdString with uid="HIMJLQPIRK",Visible=.t., Left=15, Top=115,;
    Alignment=1, Width=117, Height=18,;
    Caption="Ubicazione collegata:"  ;
  , bGlobalFont=.t.

  func oStr_4_34.mHide()
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
  endfunc

  add object oBox_4_28 as StdBox with uid="ZNVCIOHLRH",left=653, top=34, width=157,height=2
enddefine

  define class tgsar_mpgPag3 as StdContainer
    Width  = 815
    height = 478
    stdWidth  = 815
    stdheight = 478
  resizeXpos=462
  resizeYpos=422
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMsg_5_1 as StdMemo with uid="QYBPPIIQMH",rtseq=63,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 5, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 146968378,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=443, Width=715, Left=5, Top=13, tabstop = .f., readonly = .t.


  add object oBtn_5_2 as StdButton with uid="NDUWUQNEUK",left=723, top=411, width=48,height=45,;
    CpPicture="BMP\verifica.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per verificare lo stato delle righe caricate";
    , HelpContextID = 150531145;
    , Caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_2.Click()
      with this.Parent.oContained
        GSAR_BPG(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

* --- Defining Body row
define class tgsar_mpgBodyRow as CPBodyRowCnt
  Width=795
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="HMKMSDMREN",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 200998250,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], BackStyle=0

  add object oGPCODICE_2_2 as StdTrsField with uid="ACWAYIUMJC",rtseq=3,rtrep=.t.,;
    cFormVar="w_GPCODICE",value=space(20),;
    HelpContextID = 80350635,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "%w_ERRORCOD%",;
   bGlobalFont=.t.,;
    Height=17, Width=146, Left=341, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_GPCODICE"

  func oGPCODICE_2_2.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_GPSERRIF) AND EMPTY(.w_GPCODMAT) AND EMPTY(.w_GPSERRIF))
    endwith
  endfunc

  proc oGPCODICE_2_2.mAfter
      with this.Parent.oContained
        GSAR_BGP(this.Parent.oContained,this.value)
      endwith
  endproc

  func oGPCODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODICE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oGPCODICE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oGPCODICE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Elenco codici articolo",'GSVE_MDV.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oGPCODICE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_GPCODICE
    i_obj.ecpSave()
  endproc

  add object oGPCODMAT_2_3 as StdTrsField with uid="COZXPVYISQ",rtseq=4,rtrep=.t.,;
    cFormVar="w_GPCODMAT",value=space(40),;
    ToolTipText = "Codice matricola",;
    HelpContextID = 147459514,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=292, Left=43, Top=0, InputMask=replicate('X',40), bHasZoom = .t. , BackStyle=0

  func oGPCODMAT_2_3.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_GPSERRIF) AND (EMPTY(.w_GPCODICE) OR (NOT EMPTY(.w_GPCODICE) AND .w_GESMAT='S')) AND EMPTY(.w_GPSERRIF) AND (.w_FLCASC $ '+-' Or .w_F2CASC $ '+-' Or .w_FLRISE $ '+-' Or .w_F2RISE $ '+-' ))
    endwith
  endfunc

  proc oGPCODMAT_2_3.mZoom
      with this.Parent.oContained
        GSMD_BGM(this.Parent.oContained,"ZOOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oGPUNIMIS_2_6 as StdTrsField with uid="LAOASFBSVX",rtseq=7,rtrep=.t.,;
    cFormVar="w_GPUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura (se non specificata l'unit� di misura principale assegnata all'articolo)",;
    HelpContextID = 115724871,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=492, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , BackStyle=0, cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_GPUNIMIS"

  func oGPUNIMIS_2_6.mCond()
    with this.Parent.oContained
      return (.w_TIPRIG $ 'RM' AND Not Empty( .w_GPCODICE )  AND EMPTY(.w_GPSERRIF)  AND ( EMPTY(.w_GPCODMAT)  OR (.w_FLCASC='+' OR .w_FLRISE='-')))
    endwith
  endfunc

  func oGPUNIMIS_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPUNIMIS_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oGPUNIMIS_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oGPUNIMIS_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Elenco unit� di misura",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oGPUNIMIS_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_GPUNIMIS
    i_obj.ecpSave()
  endproc

  add object oGPQTAMOV_2_7 as StdTrsField with uid="FYJFXPCZCD",rtseq=8,rtrep=.t.,;
    cFormVar="w_GPQTAMOV",value=0,;
    ToolTipText = "Quantit� movimentata nell'unit� di misura selezionata",;
    HelpContextID = 144698812,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=528, Top=0, cSayPict=['@Z '+v_PQ(11)], cGetPict=['@Z '+v_GQ(11)], BackStyle=0

  func oGPQTAMOV_2_7.mCond()
    with this.Parent.oContained
      return (.w_TIPRIG $ 'RMA' AND NOT EMPTY(.w_GPCODICE) AND EMPTY(.w_GPCODMAT) AND EMPTY(.w_GPSERRIF))
    endwith
  endfunc

  func oGPQTAMOV_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GPQTAMOV=0  Or  .w_FLFRAZ<>'S' Or .w_GPQTAMOV=INT(.w_GPQTAMOV) OR NOT .w_TIPRIG $ 'RM')
    endwith
    return bRes
  endfunc

  add object oGPPREZZO_2_8 as StdTrsField with uid="OQIZOHNBCL",rtseq=9,rtrep=.t.,;
    cFormVar="w_GPPREZZO",value=0,;
    ToolTipText = "Prezzo (si intende nella valuta del documento sul quale sar� importata)",;
    HelpContextID = 170009163,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=615, Top=0, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)], BackStyle=0

  func oGPPREZZO_2_8.mCond()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST<>'R')
    endwith
  endfunc

  add object oGP_ESCLU_2_10 as StdTrsCheck with uid="UOCDLZIDMG",rtrep=.t.,;
    cFormVar="w_GP_ESCLU",  caption="",;
    ToolTipText = "Flag esclusione righe da evasione",;
    HelpContextID = 5124677,;
    Left=746, Top=0, Width=15,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oGP_ESCLU_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..GP_ESCLU,&i_cF..t_GP_ESCLU),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oGP_ESCLU_2_10.GetRadio()
    this.Parent.oContained.w_GP_ESCLU = this.RadioValue()
    return .t.
  endfunc

  func oGP_ESCLU_2_10.ToRadio()
    this.Parent.oContained.w_GP_ESCLU=trim(this.Parent.oContained.w_GP_ESCLU)
    return(;
      iif(this.Parent.oContained.w_GP_ESCLU=='S',1,;
      0))
  endfunc

  func oGP_ESCLU_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oGP_ESCLU_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DOCCOLL))
    endwith
    endif
  endfunc

  add object oGPEVASIO_2_12 as StdTrsCheck with uid="CKAANZSRBY",rtrep=.t.,;
    cFormVar="w_GPEVASIO",  caption="",;
    ToolTipText = "Evasione",;
    HelpContextID = 22991435,;
    Left=775, Top=0, Width=15,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oGPEVASIO_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..GPEVASIO,&i_cF..t_GPEVASIO),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oGPEVASIO_2_12.GetRadio()
    this.Parent.oContained.w_GPEVASIO = this.RadioValue()
    return .t.
  endfunc

  func oGPEVASIO_2_12.ToRadio()
    this.Parent.oContained.w_GPEVASIO=trim(this.Parent.oContained.w_GPEVASIO)
    return(;
      iif(this.Parent.oContained.w_GPEVASIO=='S',1,;
      0))
  endfunc

  func oGPEVASIO_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oGPEVASIO_2_12.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_GPSERRIF) AND .w_GPQTAMOV<.w_QTAORI)
    endwith
  endfunc

  func oGPEVASIO_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DOCCOLL))
    endwith
    endif
  endfunc

  add object oDESAR1_2_79 as StdTrsField with uid="PDHNHRQKWX",rtseq=129,rtrep=.t.,;
    cFormVar="w_DESAR1",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione alternativa",;
    HelpContextID = 228393526,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=292, Left=198, Top=0, InputMask=replicate('X',40), BackStyle=0

  func oDESAR1_2_79.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=0)
    endwith
    endif
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mpg','GESPENNA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GPSERIAL=GESPENNA.GPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
