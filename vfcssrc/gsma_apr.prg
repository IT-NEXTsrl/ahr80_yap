* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_apr                                                        *
*              Dati articolo / magazzini                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_69]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-15                                                      *
* Last revis.: 2018-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsma_apr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsma_apr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsma_apr")
  return

* --- Class definition
define class tgsma_apr as StdPCForm
  Width  = 726
  Height = 396+35
  Top    = 61
  Left   = 12
  cComment = "Dati articolo / magazzini"
  cPrg = "gsma_apr"
  HelpContextID=160070505
  add object cnt as tcgsma_apr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsma_apr as PCContext
  w_PRCODART = space(20)
  w_TIPOPE = space(4)
  w_PROVEN = space(1)
  w_CODDIS = space(20)
  w_TIPGES = space(1)
  w_TIPART = space(1)
  w_PRCOSSTA = 0
  w_PRQTAMIN = 0
  w_PRQTAMAX = 0
  w_PRLOTRIO = 0
  w_PRPUNRIO = 0
  w_PRGIOINV = 0
  w_PRLEAMPS = 0
  w_PRLOWLEV = 0
  w_PROGGMPS = space(1)
  w_PRGIOCOP = 0
  w_PRSCOMIN = 0
  w_PRCOERSS = 0
  w_PRSCOMAX = 0
  w_PRDISMIN = 0
  w_PRGIOAPP = 0
  w_PRCOEFLT = 0
  w_PRLOTMED = 0
  w_UNIMIS = space(3)
  w_PRORDMPS = space(1)
  w_DATOBSO = space(8)
  w_PRTIPCON = space(1)
  w_PRCODPRO = space(15)
  w_PRCODFOR = space(15)
  w_UNIMIS = space(3)
  w_UNIMIS = space(3)
  w_UNIMIS = space(3)
  w_UNIMIS = space(3)
  w_UNIMIS = space(3)
  w_UNIMIS = space(3)
  w_OBTEST = space(8)
  w_DESPRO = space(40)
  w_DESFOR = space(40)
  w_FLCOMM = space(1)
  w_PRLISCOS = space(5)
  w_DESLIS = space(40)
  w_PRESECOS = space(4)
  w_PRINVCOS = space(6)
  w_PUUTEELA = 0
  w_UTEDES = space(40)
  w_UNIMIS = space(3)
  w_PRSAFELT = 0
  w_PRPERSCA = 0
  w_PRORDPIA = space(1)
  w_PREXPLPH = space(1)
  w_PRPIAPUN = space(1)
  w_PRPERPIA = space(1)
  w_PRTIPDTF = space(1)
  w_PRDOMMED = 0
  w_PRINTRIO = 0
  w_PRDATPAR = space(8)
  w_PRDATAUL = space(8)
  w_PRCSLAVO = 0
  w_PRCSMATE = 0
  w_PRCSSPGE = 0
  w_PRCULAVO = 0
  w_PRCUMATE = 0
  w_PRCUSPGE = 0
  w_PRCMLAVO = 0
  w_PRCMMATE = 0
  w_PRCMSPGE = 0
  w_PRCLLAVO = 0
  w_PRCLMATE = 0
  w_PRCLSPGE = 0
  w_PRCSLAVE = 0
  w_PRCSLAVC = 0
  w_PRCSMATC = 0
  w_PRCSSPGC = 0
  w_PRCULAVC = 0
  w_PRCUMATC = 0
  w_PRCUSPGC = 0
  w_PRCMLAVC = 0
  w_PRCMMATC = 0
  w_PRCMSPGC = 0
  w_PRCLLAVC = 0
  w_PRCLMATC = 0
  w_PRCLSPGC = 0
  w_PRCSLVCE = 0
  w_PRCULVCE = 0
  w_PRCMLVCE = 0
  w_PRCLLVCE = 0
  w_PRCULAVE = 0
  w_PRCMLAVE = 0
  w_PRCLLAVE = 0
  w_TOTSTAND = 0
  w_TOTULTIM = 0
  w_TOTMEDIO = 0
  w_TOTLISTI = 0
  w_PRDATCOS = space(8)
  w_PRLOTSTD = 0
  w_PRDATULT = space(8)
  w_PRLOTULT = 0
  w_PRDATMED = space(8)
  w_PRLOTMED = 0
  w_PRDATLIS = space(8)
  w_PRLOTLIS = 0
  w_PRLISCOS = space(5)
  w_DESLIS = space(40)
  w_PRESECOS = space(4)
  w_PRINVCOS = space(6)
  w_PUUTEELA = 0
  w_UTEDES = space(40)
  w_TOTSTAND = 0
  w_TOTULTIM = 0
  w_TOTMEDIO = 0
  w_TOTLISTI = 0
  w_PRDATCOS = space(8)
  w_PRLOTSTD = 0
  w_PRDATULT = space(8)
  w_PRLOTULT = 0
  w_PRDATMED = space(8)
  w_PRLOTMED = 0
  w_PRDATLIS = space(8)
  w_PRLOTLIS = 0
  w_TOTCSLAVO = 0
  w_TOTCULAVO = 0
  w_TOTCMLAVO = 0
  w_TOTCLLAVO = 0
  w_TOTCSLAVE = 0
  w_TOTCULAVE = 0
  w_TOTCMLAVE = 0
  w_TOTCLLAVE = 0
  w_TOTCSMATE = 0
  w_TOTCUMATE = 0
  w_TOTCMMATE = 0
  w_TOTCLMATE = 0
  w_TOTCSSPGE = 0
  w_TOTCUSPGE = 0
  w_TOTCMSPGE = 0
  w_TOTCLSPGE = 0
  w_PRGIODTF = 0
  proc Save(oFrom)
    this.w_PRCODART = oFrom.w_PRCODART
    this.w_TIPOPE = oFrom.w_TIPOPE
    this.w_PROVEN = oFrom.w_PROVEN
    this.w_CODDIS = oFrom.w_CODDIS
    this.w_TIPGES = oFrom.w_TIPGES
    this.w_TIPART = oFrom.w_TIPART
    this.w_PRCOSSTA = oFrom.w_PRCOSSTA
    this.w_PRQTAMIN = oFrom.w_PRQTAMIN
    this.w_PRQTAMAX = oFrom.w_PRQTAMAX
    this.w_PRLOTRIO = oFrom.w_PRLOTRIO
    this.w_PRPUNRIO = oFrom.w_PRPUNRIO
    this.w_PRGIOINV = oFrom.w_PRGIOINV
    this.w_PRLEAMPS = oFrom.w_PRLEAMPS
    this.w_PRLOWLEV = oFrom.w_PRLOWLEV
    this.w_PROGGMPS = oFrom.w_PROGGMPS
    this.w_PRGIOCOP = oFrom.w_PRGIOCOP
    this.w_PRSCOMIN = oFrom.w_PRSCOMIN
    this.w_PRCOERSS = oFrom.w_PRCOERSS
    this.w_PRSCOMAX = oFrom.w_PRSCOMAX
    this.w_PRDISMIN = oFrom.w_PRDISMIN
    this.w_PRGIOAPP = oFrom.w_PRGIOAPP
    this.w_PRCOEFLT = oFrom.w_PRCOEFLT
    this.w_PRLOTMED = oFrom.w_PRLOTMED
    this.w_UNIMIS = oFrom.w_UNIMIS
    this.w_PRORDMPS = oFrom.w_PRORDMPS
    this.w_DATOBSO = oFrom.w_DATOBSO
    this.w_PRTIPCON = oFrom.w_PRTIPCON
    this.w_PRCODPRO = oFrom.w_PRCODPRO
    this.w_PRCODFOR = oFrom.w_PRCODFOR
    this.w_UNIMIS = oFrom.w_UNIMIS
    this.w_UNIMIS = oFrom.w_UNIMIS
    this.w_UNIMIS = oFrom.w_UNIMIS
    this.w_UNIMIS = oFrom.w_UNIMIS
    this.w_UNIMIS = oFrom.w_UNIMIS
    this.w_UNIMIS = oFrom.w_UNIMIS
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_DESPRO = oFrom.w_DESPRO
    this.w_DESFOR = oFrom.w_DESFOR
    this.w_FLCOMM = oFrom.w_FLCOMM
    this.w_PRLISCOS = oFrom.w_PRLISCOS
    this.w_DESLIS = oFrom.w_DESLIS
    this.w_PRESECOS = oFrom.w_PRESECOS
    this.w_PRINVCOS = oFrom.w_PRINVCOS
    this.w_PUUTEELA = oFrom.w_PUUTEELA
    this.w_UTEDES = oFrom.w_UTEDES
    this.w_UNIMIS = oFrom.w_UNIMIS
    this.w_PRSAFELT = oFrom.w_PRSAFELT
    this.w_PRPERSCA = oFrom.w_PRPERSCA
    this.w_PRORDPIA = oFrom.w_PRORDPIA
    this.w_PREXPLPH = oFrom.w_PREXPLPH
    this.w_PRPIAPUN = oFrom.w_PRPIAPUN
    this.w_PRPERPIA = oFrom.w_PRPERPIA
    this.w_PRTIPDTF = oFrom.w_PRTIPDTF
    this.w_PRDOMMED = oFrom.w_PRDOMMED
    this.w_PRINTRIO = oFrom.w_PRINTRIO
    this.w_PRDATPAR = oFrom.w_PRDATPAR
    this.w_PRDATAUL = oFrom.w_PRDATAUL
    this.w_PRCSLAVO = oFrom.w_PRCSLAVO
    this.w_PRCSMATE = oFrom.w_PRCSMATE
    this.w_PRCSSPGE = oFrom.w_PRCSSPGE
    this.w_PRCULAVO = oFrom.w_PRCULAVO
    this.w_PRCUMATE = oFrom.w_PRCUMATE
    this.w_PRCUSPGE = oFrom.w_PRCUSPGE
    this.w_PRCMLAVO = oFrom.w_PRCMLAVO
    this.w_PRCMMATE = oFrom.w_PRCMMATE
    this.w_PRCMSPGE = oFrom.w_PRCMSPGE
    this.w_PRCLLAVO = oFrom.w_PRCLLAVO
    this.w_PRCLMATE = oFrom.w_PRCLMATE
    this.w_PRCLSPGE = oFrom.w_PRCLSPGE
    this.w_PRCSLAVE = oFrom.w_PRCSLAVE
    this.w_PRCSLAVC = oFrom.w_PRCSLAVC
    this.w_PRCSMATC = oFrom.w_PRCSMATC
    this.w_PRCSSPGC = oFrom.w_PRCSSPGC
    this.w_PRCULAVC = oFrom.w_PRCULAVC
    this.w_PRCUMATC = oFrom.w_PRCUMATC
    this.w_PRCUSPGC = oFrom.w_PRCUSPGC
    this.w_PRCMLAVC = oFrom.w_PRCMLAVC
    this.w_PRCMMATC = oFrom.w_PRCMMATC
    this.w_PRCMSPGC = oFrom.w_PRCMSPGC
    this.w_PRCLLAVC = oFrom.w_PRCLLAVC
    this.w_PRCLMATC = oFrom.w_PRCLMATC
    this.w_PRCLSPGC = oFrom.w_PRCLSPGC
    this.w_PRCSLVCE = oFrom.w_PRCSLVCE
    this.w_PRCULVCE = oFrom.w_PRCULVCE
    this.w_PRCMLVCE = oFrom.w_PRCMLVCE
    this.w_PRCLLVCE = oFrom.w_PRCLLVCE
    this.w_PRCULAVE = oFrom.w_PRCULAVE
    this.w_PRCMLAVE = oFrom.w_PRCMLAVE
    this.w_PRCLLAVE = oFrom.w_PRCLLAVE
    this.w_TOTSTAND = oFrom.w_TOTSTAND
    this.w_TOTULTIM = oFrom.w_TOTULTIM
    this.w_TOTMEDIO = oFrom.w_TOTMEDIO
    this.w_TOTLISTI = oFrom.w_TOTLISTI
    this.w_PRDATCOS = oFrom.w_PRDATCOS
    this.w_PRLOTSTD = oFrom.w_PRLOTSTD
    this.w_PRDATULT = oFrom.w_PRDATULT
    this.w_PRLOTULT = oFrom.w_PRLOTULT
    this.w_PRDATMED = oFrom.w_PRDATMED
    this.w_PRLOTMED = oFrom.w_PRLOTMED
    this.w_PRDATLIS = oFrom.w_PRDATLIS
    this.w_PRLOTLIS = oFrom.w_PRLOTLIS
    this.w_PRLISCOS = oFrom.w_PRLISCOS
    this.w_DESLIS = oFrom.w_DESLIS
    this.w_PRESECOS = oFrom.w_PRESECOS
    this.w_PRINVCOS = oFrom.w_PRINVCOS
    this.w_PUUTEELA = oFrom.w_PUUTEELA
    this.w_UTEDES = oFrom.w_UTEDES
    this.w_TOTSTAND = oFrom.w_TOTSTAND
    this.w_TOTULTIM = oFrom.w_TOTULTIM
    this.w_TOTMEDIO = oFrom.w_TOTMEDIO
    this.w_TOTLISTI = oFrom.w_TOTLISTI
    this.w_PRDATCOS = oFrom.w_PRDATCOS
    this.w_PRLOTSTD = oFrom.w_PRLOTSTD
    this.w_PRDATULT = oFrom.w_PRDATULT
    this.w_PRLOTULT = oFrom.w_PRLOTULT
    this.w_PRDATMED = oFrom.w_PRDATMED
    this.w_PRLOTMED = oFrom.w_PRLOTMED
    this.w_PRDATLIS = oFrom.w_PRDATLIS
    this.w_PRLOTLIS = oFrom.w_PRLOTLIS
    this.w_TOTCSLAVO = oFrom.w_TOTCSLAVO
    this.w_TOTCULAVO = oFrom.w_TOTCULAVO
    this.w_TOTCMLAVO = oFrom.w_TOTCMLAVO
    this.w_TOTCLLAVO = oFrom.w_TOTCLLAVO
    this.w_TOTCSLAVE = oFrom.w_TOTCSLAVE
    this.w_TOTCULAVE = oFrom.w_TOTCULAVE
    this.w_TOTCMLAVE = oFrom.w_TOTCMLAVE
    this.w_TOTCLLAVE = oFrom.w_TOTCLLAVE
    this.w_TOTCSMATE = oFrom.w_TOTCSMATE
    this.w_TOTCUMATE = oFrom.w_TOTCUMATE
    this.w_TOTCMMATE = oFrom.w_TOTCMMATE
    this.w_TOTCLMATE = oFrom.w_TOTCLMATE
    this.w_TOTCSSPGE = oFrom.w_TOTCSSPGE
    this.w_TOTCUSPGE = oFrom.w_TOTCUSPGE
    this.w_TOTCMSPGE = oFrom.w_TOTCMSPGE
    this.w_TOTCLSPGE = oFrom.w_TOTCLSPGE
    this.w_PRGIODTF = oFrom.w_PRGIODTF
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_PRCODART = this.w_PRCODART
    oTo.w_TIPOPE = this.w_TIPOPE
    oTo.w_PROVEN = this.w_PROVEN
    oTo.w_CODDIS = this.w_CODDIS
    oTo.w_TIPGES = this.w_TIPGES
    oTo.w_TIPART = this.w_TIPART
    oTo.w_PRCOSSTA = this.w_PRCOSSTA
    oTo.w_PRQTAMIN = this.w_PRQTAMIN
    oTo.w_PRQTAMAX = this.w_PRQTAMAX
    oTo.w_PRLOTRIO = this.w_PRLOTRIO
    oTo.w_PRPUNRIO = this.w_PRPUNRIO
    oTo.w_PRGIOINV = this.w_PRGIOINV
    oTo.w_PRLEAMPS = this.w_PRLEAMPS
    oTo.w_PRLOWLEV = this.w_PRLOWLEV
    oTo.w_PROGGMPS = this.w_PROGGMPS
    oTo.w_PRGIOCOP = this.w_PRGIOCOP
    oTo.w_PRSCOMIN = this.w_PRSCOMIN
    oTo.w_PRCOERSS = this.w_PRCOERSS
    oTo.w_PRSCOMAX = this.w_PRSCOMAX
    oTo.w_PRDISMIN = this.w_PRDISMIN
    oTo.w_PRGIOAPP = this.w_PRGIOAPP
    oTo.w_PRCOEFLT = this.w_PRCOEFLT
    oTo.w_PRLOTMED = this.w_PRLOTMED
    oTo.w_UNIMIS = this.w_UNIMIS
    oTo.w_PRORDMPS = this.w_PRORDMPS
    oTo.w_DATOBSO = this.w_DATOBSO
    oTo.w_PRTIPCON = this.w_PRTIPCON
    oTo.w_PRCODPRO = this.w_PRCODPRO
    oTo.w_PRCODFOR = this.w_PRCODFOR
    oTo.w_UNIMIS = this.w_UNIMIS
    oTo.w_UNIMIS = this.w_UNIMIS
    oTo.w_UNIMIS = this.w_UNIMIS
    oTo.w_UNIMIS = this.w_UNIMIS
    oTo.w_UNIMIS = this.w_UNIMIS
    oTo.w_UNIMIS = this.w_UNIMIS
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_DESPRO = this.w_DESPRO
    oTo.w_DESFOR = this.w_DESFOR
    oTo.w_FLCOMM = this.w_FLCOMM
    oTo.w_PRLISCOS = this.w_PRLISCOS
    oTo.w_DESLIS = this.w_DESLIS
    oTo.w_PRESECOS = this.w_PRESECOS
    oTo.w_PRINVCOS = this.w_PRINVCOS
    oTo.w_PUUTEELA = this.w_PUUTEELA
    oTo.w_UTEDES = this.w_UTEDES
    oTo.w_UNIMIS = this.w_UNIMIS
    oTo.w_PRSAFELT = this.w_PRSAFELT
    oTo.w_PRPERSCA = this.w_PRPERSCA
    oTo.w_PRORDPIA = this.w_PRORDPIA
    oTo.w_PREXPLPH = this.w_PREXPLPH
    oTo.w_PRPIAPUN = this.w_PRPIAPUN
    oTo.w_PRPERPIA = this.w_PRPERPIA
    oTo.w_PRTIPDTF = this.w_PRTIPDTF
    oTo.w_PRDOMMED = this.w_PRDOMMED
    oTo.w_PRINTRIO = this.w_PRINTRIO
    oTo.w_PRDATPAR = this.w_PRDATPAR
    oTo.w_PRDATAUL = this.w_PRDATAUL
    oTo.w_PRCSLAVO = this.w_PRCSLAVO
    oTo.w_PRCSMATE = this.w_PRCSMATE
    oTo.w_PRCSSPGE = this.w_PRCSSPGE
    oTo.w_PRCULAVO = this.w_PRCULAVO
    oTo.w_PRCUMATE = this.w_PRCUMATE
    oTo.w_PRCUSPGE = this.w_PRCUSPGE
    oTo.w_PRCMLAVO = this.w_PRCMLAVO
    oTo.w_PRCMMATE = this.w_PRCMMATE
    oTo.w_PRCMSPGE = this.w_PRCMSPGE
    oTo.w_PRCLLAVO = this.w_PRCLLAVO
    oTo.w_PRCLMATE = this.w_PRCLMATE
    oTo.w_PRCLSPGE = this.w_PRCLSPGE
    oTo.w_PRCSLAVE = this.w_PRCSLAVE
    oTo.w_PRCSLAVC = this.w_PRCSLAVC
    oTo.w_PRCSMATC = this.w_PRCSMATC
    oTo.w_PRCSSPGC = this.w_PRCSSPGC
    oTo.w_PRCULAVC = this.w_PRCULAVC
    oTo.w_PRCUMATC = this.w_PRCUMATC
    oTo.w_PRCUSPGC = this.w_PRCUSPGC
    oTo.w_PRCMLAVC = this.w_PRCMLAVC
    oTo.w_PRCMMATC = this.w_PRCMMATC
    oTo.w_PRCMSPGC = this.w_PRCMSPGC
    oTo.w_PRCLLAVC = this.w_PRCLLAVC
    oTo.w_PRCLMATC = this.w_PRCLMATC
    oTo.w_PRCLSPGC = this.w_PRCLSPGC
    oTo.w_PRCSLVCE = this.w_PRCSLVCE
    oTo.w_PRCULVCE = this.w_PRCULVCE
    oTo.w_PRCMLVCE = this.w_PRCMLVCE
    oTo.w_PRCLLVCE = this.w_PRCLLVCE
    oTo.w_PRCULAVE = this.w_PRCULAVE
    oTo.w_PRCMLAVE = this.w_PRCMLAVE
    oTo.w_PRCLLAVE = this.w_PRCLLAVE
    oTo.w_TOTSTAND = this.w_TOTSTAND
    oTo.w_TOTULTIM = this.w_TOTULTIM
    oTo.w_TOTMEDIO = this.w_TOTMEDIO
    oTo.w_TOTLISTI = this.w_TOTLISTI
    oTo.w_PRDATCOS = this.w_PRDATCOS
    oTo.w_PRLOTSTD = this.w_PRLOTSTD
    oTo.w_PRDATULT = this.w_PRDATULT
    oTo.w_PRLOTULT = this.w_PRLOTULT
    oTo.w_PRDATMED = this.w_PRDATMED
    oTo.w_PRLOTMED = this.w_PRLOTMED
    oTo.w_PRDATLIS = this.w_PRDATLIS
    oTo.w_PRLOTLIS = this.w_PRLOTLIS
    oTo.w_PRLISCOS = this.w_PRLISCOS
    oTo.w_DESLIS = this.w_DESLIS
    oTo.w_PRESECOS = this.w_PRESECOS
    oTo.w_PRINVCOS = this.w_PRINVCOS
    oTo.w_PUUTEELA = this.w_PUUTEELA
    oTo.w_UTEDES = this.w_UTEDES
    oTo.w_TOTSTAND = this.w_TOTSTAND
    oTo.w_TOTULTIM = this.w_TOTULTIM
    oTo.w_TOTMEDIO = this.w_TOTMEDIO
    oTo.w_TOTLISTI = this.w_TOTLISTI
    oTo.w_PRDATCOS = this.w_PRDATCOS
    oTo.w_PRLOTSTD = this.w_PRLOTSTD
    oTo.w_PRDATULT = this.w_PRDATULT
    oTo.w_PRLOTULT = this.w_PRLOTULT
    oTo.w_PRDATMED = this.w_PRDATMED
    oTo.w_PRLOTMED = this.w_PRLOTMED
    oTo.w_PRDATLIS = this.w_PRDATLIS
    oTo.w_PRLOTLIS = this.w_PRLOTLIS
    oTo.w_TOTCSLAVO = this.w_TOTCSLAVO
    oTo.w_TOTCULAVO = this.w_TOTCULAVO
    oTo.w_TOTCMLAVO = this.w_TOTCMLAVO
    oTo.w_TOTCLLAVO = this.w_TOTCLLAVO
    oTo.w_TOTCSLAVE = this.w_TOTCSLAVE
    oTo.w_TOTCULAVE = this.w_TOTCULAVE
    oTo.w_TOTCMLAVE = this.w_TOTCMLAVE
    oTo.w_TOTCLLAVE = this.w_TOTCLLAVE
    oTo.w_TOTCSMATE = this.w_TOTCSMATE
    oTo.w_TOTCUMATE = this.w_TOTCUMATE
    oTo.w_TOTCMMATE = this.w_TOTCMMATE
    oTo.w_TOTCLMATE = this.w_TOTCLMATE
    oTo.w_TOTCSSPGE = this.w_TOTCSSPGE
    oTo.w_TOTCUSPGE = this.w_TOTCUSPGE
    oTo.w_TOTCMSPGE = this.w_TOTCMSPGE
    oTo.w_TOTCLSPGE = this.w_TOTCLSPGE
    oTo.w_PRGIODTF = this.w_PRGIODTF
    PCContext::Load(oTo)
enddefine

define class tcgsma_apr as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 726
  Height = 396+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-10-15"
  HelpContextID=160070505
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=136

  * --- Constant Properties
  PAR_RIOR_IDX = 0
  CONTI_IDX = 0
  PAR_RIMA_IDX = 0
  LISTINI_IDX = 0
  cpusers_IDX = 0
  cFile = "PAR_RIOR"
  cKeySelect = "PRCODART"
  cKeyWhere  = "PRCODART=this.w_PRCODART"
  cKeyWhereODBC = '"PRCODART="+cp_ToStrODBC(this.w_PRCODART)';

  cKeyWhereODBCqualified = '"PAR_RIOR.PRCODART="+cp_ToStrODBC(this.w_PRCODART)';

  cPrg = "gsma_apr"
  cComment = "Dati articolo / magazzini"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PRCODART = space(20)
  w_TIPOPE = space(4)
  w_PROVEN = space(1)
  w_CODDIS = space(20)
  w_TIPGES = space(1)
  w_TIPART = space(1)
  o_TIPART = space(1)
  w_PRCOSSTA = 0
  w_PRQTAMIN = 0
  w_PRQTAMAX = 0
  w_PRLOTRIO = 0
  w_PRPUNRIO = 0
  w_PRGIOINV = 0
  w_PRLEAMPS = 0
  w_PRLOWLEV = 0
  w_PROGGMPS = space(1)
  o_PROGGMPS = space(1)
  w_PRGIOCOP = 0
  w_PRSCOMIN = 0
  w_PRCOERSS = 0
  w_PRSCOMAX = 0
  w_PRDISMIN = 0
  w_PRGIOAPP = 0
  w_PRCOEFLT = 0
  w_PRLOTMED = 0
  w_UNIMIS = space(3)
  w_PRORDMPS = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_PRTIPCON = space(1)
  w_PRCODPRO = space(15)
  w_PRCODFOR = space(15)
  w_UNIMIS = space(3)
  w_UNIMIS = space(3)
  w_UNIMIS = space(3)
  w_UNIMIS = space(3)
  w_UNIMIS = space(3)
  w_UNIMIS = space(3)
  w_OBTEST = ctod('  /  /  ')
  w_DESPRO = space(40)
  w_DESFOR = space(40)
  w_FLCOMM = space(1)
  o_FLCOMM = space(1)
  w_PRLISCOS = space(5)
  w_DESLIS = space(40)
  w_PRESECOS = space(4)
  w_PRINVCOS = space(6)
  w_PUUTEELA = 0
  w_UTEDES = space(40)
  w_UNIMIS = space(3)
  w_PRSAFELT = 0
  w_PRPERSCA = 0
  w_PRORDPIA = space(1)
  w_PREXPLPH = space(1)
  o_PREXPLPH = space(1)
  w_PRPIAPUN = space(1)
  w_PRPERPIA = space(1)
  w_PRTIPDTF = space(1)
  w_PRDOMMED = 0
  w_PRINTRIO = 0
  o_PRINTRIO = 0
  w_PRDATPAR = ctod('  /  /  ')
  o_PRDATPAR = ctod('  /  /  ')
  w_PRDATAUL = ctod('  /  /  ')
  w_PRCSLAVO = 0
  w_PRCSMATE = 0
  w_PRCSSPGE = 0
  w_PRCULAVO = 0
  w_PRCUMATE = 0
  w_PRCUSPGE = 0
  w_PRCMLAVO = 0
  w_PRCMMATE = 0
  w_PRCMSPGE = 0
  w_PRCLLAVO = 0
  w_PRCLMATE = 0
  w_PRCLSPGE = 0
  w_PRCSLAVE = 0
  w_PRCSLAVC = 0
  w_PRCSMATC = 0
  w_PRCSSPGC = 0
  w_PRCULAVC = 0
  w_PRCUMATC = 0
  w_PRCUSPGC = 0
  w_PRCMLAVC = 0
  w_PRCMMATC = 0
  w_PRCMSPGC = 0
  w_PRCLLAVC = 0
  w_PRCLMATC = 0
  w_PRCLSPGC = 0
  w_PRCSLVCE = 0
  w_PRCULVCE = 0
  w_PRCMLVCE = 0
  w_PRCLLVCE = 0
  w_PRCULAVE = 0
  w_PRCMLAVE = 0
  w_PRCLLAVE = 0
  w_TOTSTAND = 0
  w_TOTULTIM = 0
  w_TOTMEDIO = 0
  w_TOTLISTI = 0
  w_PRDATCOS = ctod('  /  /  ')
  w_PRLOTSTD = 0
  w_PRDATULT = ctod('  /  /  ')
  w_PRLOTULT = 0
  w_PRDATMED = ctod('  /  /  ')
  w_PRLOTMED = 0
  w_PRDATLIS = ctod('  /  /  ')
  w_PRLOTLIS = 0
  w_PRLISCOS = space(5)
  w_DESLIS = space(40)
  w_PRESECOS = space(4)
  w_PRINVCOS = space(6)
  w_PUUTEELA = 0
  w_UTEDES = space(40)
  w_TOTSTAND = 0
  w_TOTULTIM = 0
  w_TOTMEDIO = 0
  w_TOTLISTI = 0
  w_PRDATCOS = ctod('  /  /  ')
  w_PRLOTSTD = 0
  w_PRDATULT = ctod('  /  /  ')
  w_PRLOTULT = 0
  w_PRDATMED = ctod('  /  /  ')
  w_PRLOTMED = 0
  w_PRDATLIS = ctod('  /  /  ')
  w_PRLOTLIS = 0
  w_TOTCSLAVO = 0
  w_TOTCULAVO = 0
  w_TOTCMLAVO = 0
  w_TOTCLLAVO = 0
  w_TOTCSLAVE = 0
  w_TOTCULAVE = 0
  w_TOTCMLAVE = 0
  w_TOTCLLAVE = 0
  w_TOTCSMATE = 0
  w_TOTCUMATE = 0
  w_TOTCMMATE = 0
  w_TOTCLMATE = 0
  w_TOTCSSPGE = 0
  w_TOTCUSPGE = 0
  w_TOTCMSPGE = 0
  w_TOTCLSPGE = 0
  w_PRGIODTF = 0

  * --- Children pointers
  GSMA_MPR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_aprPag1","gsma_apr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati globali")
      .Pages(1).HelpContextID = 262778427
      .Pages(2).addobject("oPag","tgsma_aprPag2","gsma_apr",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettaglio magazzini")
      .Pages(2).HelpContextID = 69195111
      .Pages(3).addobject("oPag","tgsma_aprPag3","gsma_apr",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Costi prodotto")
      .Pages(3).HelpContextID = 71074696
      .Pages(4).addobject("oPag","tgsma_aprPag4","gsma_apr",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Dettaglio costi prodotto")
      .Pages(4).HelpContextID = 83759330
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPRCOSSTA_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='PAR_RIMA'
    this.cWorkTables[3]='LISTINI'
    this.cWorkTables[4]='cpusers'
    this.cWorkTables[5]='PAR_RIOR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_RIOR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_RIOR_IDX,3]
  return

  function CreateChildren()
    this.GSMA_MPR = CREATEOBJECT('stdDynamicChild',this,'GSMA_MPR',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.GSMA_MPR.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgsma_apr'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSMA_MPR)
      this.GSMA_MPR.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSMA_MPR.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSMA_MPR.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSMA_MPR)
      this.GSMA_MPR.DestroyChildrenChain()
      this.GSMA_MPR=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMA_MPR.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMA_MPR.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMA_MPR.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSMA_MPR.SetKey(;
            .w_PRCODART,"PRCODART";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSMA_MPR.ChangeRow(this.cRowID+'      1',1;
             ,.w_PRCODART,"PRCODART";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSMA_MPR)
        i_f=.GSMA_MPR.BuildFilter()
        if !(i_f==.GSMA_MPR.cQueryFilter)
          i_fnidx=.GSMA_MPR.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSMA_MPR.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSMA_MPR.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSMA_MPR.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSMA_MPR.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_30_joined
    link_1_30_joined=.f.
    local link_1_32_joined
    link_1_32_joined=.f.
    local link_3_7_joined
    link_3_7_joined=.f.
    local link_4_63_joined
    link_4_63_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_RIOR where PRCODART=KeySet.PRCODART
    *
    i_nConn = i_TableProp[this.PAR_RIOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_RIOR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_RIOR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_RIOR '
      link_1_30_joined=this.AddJoinedLink_1_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_32_joined=this.AddJoinedLink_1_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_7_joined=this.AddJoinedLink_3_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_63_joined=this.AddJoinedLink_4_63(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PRCODART',this.w_PRCODART  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DATOBSO = ctod("  /  /  ")
        .w_OBTEST = i_datsys
        .w_DESPRO = space(40)
        .w_DESFOR = space(40)
        .w_DESLIS = space(40)
        .w_UTEDES = space(40)
        .w_DESLIS = space(40)
        .w_UTEDES = space(40)
        .w_PRCODART = NVL(PRCODART,space(20))
        .w_TIPOPE = this.cFunction
        .w_PROVEN = this.oParentObject .w_ARPROPRE
        .w_CODDIS = this.oParentObject .w_ARCODDIS
        .w_TIPGES = this.oParentObject .w_ARTIPGES
        .w_TIPART = this.oParentObject .w_ARTIPART
        .w_PRCOSSTA = NVL(PRCOSSTA,0)
        .w_PRQTAMIN = NVL(PRQTAMIN,0)
        .w_PRQTAMAX = NVL(PRQTAMAX,0)
        .w_PRLOTRIO = NVL(PRLOTRIO,0)
        .w_PRPUNRIO = NVL(PRPUNRIO,0)
        .w_PRGIOINV = NVL(PRGIOINV,0)
        .w_PRLEAMPS = NVL(PRLEAMPS,0)
        .w_PRLOWLEV = NVL(PRLOWLEV,0)
        .w_PROGGMPS = NVL(PROGGMPS,space(1))
        .w_PRGIOCOP = NVL(PRGIOCOP,0)
        .w_PRSCOMIN = NVL(PRSCOMIN,0)
        .w_PRCOERSS = NVL(PRCOERSS,0)
        .w_PRSCOMAX = NVL(PRSCOMAX,0)
        .w_PRDISMIN = NVL(PRDISMIN,0)
        .w_PRGIOAPP = NVL(PRGIOAPP,0)
        .w_PRCOEFLT = NVL(PRCOEFLT,0)
        .w_PRLOTMED = NVL(PRLOTMED,0)
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_PRORDMPS = NVL(PRORDMPS,space(1))
        .w_PRTIPCON = NVL(PRTIPCON,space(1))
        .w_PRCODPRO = NVL(PRCODPRO,space(15))
          if link_1_30_joined
            this.w_PRCODPRO = NVL(ANCODICE130,NVL(this.w_PRCODPRO,space(15)))
            this.w_DESPRO = NVL(ANDESCRI130,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO130),ctod("  /  /  "))
          else
          .link_1_30('Load')
          endif
        .w_PRCODFOR = NVL(PRCODFOR,space(15))
          if link_1_32_joined
            this.w_PRCODFOR = NVL(ANCODICE132,NVL(this.w_PRCODFOR,space(15)))
            this.w_DESFOR = NVL(ANDESCRI132,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO132),ctod("  /  /  "))
          else
          .link_1_32('Load')
          endif
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_FLCOMM = this.oParentObject .w_ARFLCOMM
        .w_PRLISCOS = NVL(PRLISCOS,space(5))
          if link_3_7_joined
            this.w_PRLISCOS = NVL(LSCODLIS307,NVL(this.w_PRLISCOS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS307,space(40))
          else
          .link_3_7('Load')
          endif
        .w_PRESECOS = NVL(PRESECOS,space(4))
        .w_PRINVCOS = NVL(PRINVCOS,space(6))
        .w_PUUTEELA = NVL(PUUTEELA,0)
          .link_3_14('Load')
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_PRSAFELT = NVL(PRSAFELT,0)
        .w_PRPERSCA = NVL(PRPERSCA,0)
        .w_PRORDPIA = NVL(PRORDPIA,space(1))
        .w_PREXPLPH = NVL(PREXPLPH,space(1))
        .w_PRPIAPUN = NVL(PRPIAPUN,space(1))
        .w_PRPERPIA = NVL(PRPERPIA,space(1))
        .w_PRTIPDTF = NVL(PRTIPDTF,space(1))
        .w_PRDOMMED = NVL(PRDOMMED,0)
        .w_PRINTRIO = NVL(PRINTRIO,0)
        .w_PRDATPAR = NVL(cp_ToDate(PRDATPAR),ctod("  /  /  "))
        .w_PRDATAUL = NVL(cp_ToDate(PRDATAUL),ctod("  /  /  "))
        .w_PRCSLAVO = NVL(PRCSLAVO,0)
        .w_PRCSMATE = NVL(PRCSMATE,0)
        .w_PRCSSPGE = NVL(PRCSSPGE,0)
        .w_PRCULAVO = NVL(PRCULAVO,0)
        .w_PRCUMATE = NVL(PRCUMATE,0)
        .w_PRCUSPGE = NVL(PRCUSPGE,0)
        .w_PRCMLAVO = NVL(PRCMLAVO,0)
        .w_PRCMMATE = NVL(PRCMMATE,0)
        .w_PRCMSPGE = NVL(PRCMSPGE,0)
        .w_PRCLLAVO = NVL(PRCLLAVO,0)
        .w_PRCLMATE = NVL(PRCLMATE,0)
        .w_PRCLSPGE = NVL(PRCLSPGE,0)
        .w_PRCSLAVE = NVL(PRCSLAVE,0)
        .w_PRCSLAVC = NVL(PRCSLAVC,0)
        .w_PRCSMATC = NVL(PRCSMATC,0)
        .w_PRCSSPGC = NVL(PRCSSPGC,0)
        .w_PRCULAVC = NVL(PRCULAVC,0)
        .w_PRCUMATC = NVL(PRCUMATC,0)
        .w_PRCUSPGC = NVL(PRCUSPGC,0)
        .w_PRCMLAVC = NVL(PRCMLAVC,0)
        .w_PRCMMATC = NVL(PRCMMATC,0)
        .w_PRCMSPGC = NVL(PRCMSPGC,0)
        .w_PRCLLAVC = NVL(PRCLLAVC,0)
        .w_PRCLMATC = NVL(PRCLMATC,0)
        .w_PRCLSPGC = NVL(PRCLSPGC,0)
        .w_PRCSLVCE = NVL(PRCSLVCE,0)
        .w_PRCULVCE = NVL(PRCULVCE,0)
        .w_PRCMLVCE = NVL(PRCMLVCE,0)
        .w_PRCLLVCE = NVL(PRCLLVCE,0)
        .w_PRCULAVE = NVL(PRCULAVE,0)
        .w_PRCMLAVE = NVL(PRCMLAVE,0)
        .w_PRCLLAVE = NVL(PRCLLAVE,0)
        .w_TOTSTAND = .w_PRCSLAVO+.w_PRCSLAVE+.w_PRCSLAVC+.w_PRCSLVCE+.w_PRCSMATE+.w_PRCSMATC+.w_PRCSSPGE+.w_PRCSSPGC
        .w_TOTULTIM = .w_PRCULAVO+.w_PRCULAVE+.w_PRCULAVC+.w_PRCULVCE+.w_PRCUMATE+.w_PRCUMATC+.w_PRCUSPGE+.w_PRCUSPGC
        .w_TOTMEDIO = .w_PRCMLAVO+.w_PRCMLAVE+.w_PRCMLAVC+.w_PRCMLVCE+.w_PRCMMATE+.w_PRCMMATC+.w_PRCMSPGE+.w_PRCMSPGC
        .w_TOTLISTI = .w_PRCLLAVO+.w_PRCLLAVE+.w_PRCLLAVC+.w_PRCLLVCE+.w_PRCLMATE+.w_PRCLMATC+.w_PRCLSPGE+.w_PRCLSPGC
        .w_PRDATCOS = NVL(cp_ToDate(PRDATCOS),ctod("  /  /  "))
        .w_PRLOTSTD = NVL(PRLOTSTD,0)
        .w_PRDATULT = NVL(cp_ToDate(PRDATULT),ctod("  /  /  "))
        .w_PRLOTULT = NVL(PRLOTULT,0)
        .w_PRDATMED = NVL(cp_ToDate(PRDATMED),ctod("  /  /  "))
        .w_PRLOTMED = NVL(PRLOTMED,0)
        .w_PRDATLIS = NVL(cp_ToDate(PRDATLIS),ctod("  /  /  "))
        .w_PRLOTLIS = NVL(PRLOTLIS,0)
        .w_PRLISCOS = NVL(PRLISCOS,space(5))
          if link_4_63_joined
            this.w_PRLISCOS = NVL(LSCODLIS463,NVL(this.w_PRLISCOS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS463,space(40))
          else
          .link_4_63('Load')
          endif
        .w_PRESECOS = NVL(PRESECOS,space(4))
        .w_PRINVCOS = NVL(PRINVCOS,space(6))
        .w_PUUTEELA = NVL(PUUTEELA,0)
          .link_4_70('Load')
        .w_TOTSTAND = .w_PRCSLAVO+.w_PRCSLAVE+.w_PRCSLAVC+.w_PRCSLVCE+.w_PRCSMATE+.w_PRCSMATC+.w_PRCSSPGE+.w_PRCSSPGC
        .w_TOTULTIM = .w_PRCULAVO+.w_PRCULAVE+.w_PRCULAVC+.w_PRCULVCE+.w_PRCUMATE+.w_PRCUMATC+.w_PRCUSPGE+.w_PRCUSPGC
        .w_TOTMEDIO = .w_PRCMLAVO+.w_PRCMLAVE+.w_PRCMLAVC+.w_PRCMLVCE+.w_PRCMMATE+.w_PRCMMATC+.w_PRCMSPGE+.w_PRCMSPGC
        .w_TOTLISTI = .w_PRCLLAVO+.w_PRCLLAVE+.w_PRCLLAVC+.w_PRCLLVCE+.w_PRCLMATE+.w_PRCLMATC+.w_PRCLSPGE+.w_PRCLSPGC
        .w_PRDATCOS = NVL(cp_ToDate(PRDATCOS),ctod("  /  /  "))
        .w_PRLOTSTD = NVL(PRLOTSTD,0)
        .w_PRDATULT = NVL(cp_ToDate(PRDATULT),ctod("  /  /  "))
        .w_PRLOTULT = NVL(PRLOTULT,0)
        .w_PRDATMED = NVL(cp_ToDate(PRDATMED),ctod("  /  /  "))
        .w_PRLOTMED = NVL(PRLOTMED,0)
        .w_PRDATLIS = NVL(cp_ToDate(PRDATLIS),ctod("  /  /  "))
        .w_PRLOTLIS = NVL(PRLOTLIS,0)
        .w_TOTCSLAVO = .w_PRCSLAVO+.w_PRCSLAVC
        .w_TOTCULAVO = .w_PRCULAVO+.w_PRCULAVC
        .w_TOTCMLAVO = .w_PRCMLAVO+.w_PRCMLAVC
        .w_TOTCLLAVO = .w_PRCLLAVO+.w_PRCLLAVC
        .w_TOTCSLAVE = .w_PRCSLAVE+.w_PRCSLVCE
        .w_TOTCULAVE = .w_PRCULAVE+.w_PRCULVCE
        .w_TOTCMLAVE = .w_PRCMLAVE+.w_PRCMLVCE
        .w_TOTCLLAVE = .w_PRCLLAVE+.w_PRCLLVCE
        .w_TOTCSMATE = .w_PRCSMATE+.w_PRCSMATC
        .w_TOTCUMATE = .w_PRCUMATE+.w_PRCUMATC
        .w_TOTCMMATE = .w_PRCMMATE+.w_PRCMMATC
        .w_TOTCLMATE = .w_PRCLMATE+.w_PRCLMATC
        .w_TOTCSSPGE = .w_PRCSSPGE+.w_PRCSSPGC
        .w_TOTCUSPGE = .w_PRCUSPGE+.w_PRCUSPGC
        .w_TOTCMSPGE = .w_PRCMSPGE+.w_PRCMSPGC
        .w_TOTCLSPGE = .w_PRCLSPGE+.w_PRCLSPGC
        .w_PRGIODTF = NVL(PRGIODTF,0)
        cp_LoadRecExtFlds(this,'PAR_RIOR')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsma_apr
    * Forza sempre l'esecuzione della Check Form
    this.bUpdated=.t.
    
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_PRCODART = space(20)
      .w_TIPOPE = space(4)
      .w_PROVEN = space(1)
      .w_CODDIS = space(20)
      .w_TIPGES = space(1)
      .w_TIPART = space(1)
      .w_PRCOSSTA = 0
      .w_PRQTAMIN = 0
      .w_PRQTAMAX = 0
      .w_PRLOTRIO = 0
      .w_PRPUNRIO = 0
      .w_PRGIOINV = 0
      .w_PRLEAMPS = 0
      .w_PRLOWLEV = 0
      .w_PROGGMPS = space(1)
      .w_PRGIOCOP = 0
      .w_PRSCOMIN = 0
      .w_PRCOERSS = 0
      .w_PRSCOMAX = 0
      .w_PRDISMIN = 0
      .w_PRGIOAPP = 0
      .w_PRCOEFLT = 0
      .w_PRLOTMED = 0
      .w_UNIMIS = space(3)
      .w_PRORDMPS = space(1)
      .w_DATOBSO = ctod("  /  /  ")
      .w_PRTIPCON = space(1)
      .w_PRCODPRO = space(15)
      .w_PRCODFOR = space(15)
      .w_UNIMIS = space(3)
      .w_UNIMIS = space(3)
      .w_UNIMIS = space(3)
      .w_UNIMIS = space(3)
      .w_UNIMIS = space(3)
      .w_UNIMIS = space(3)
      .w_OBTEST = ctod("  /  /  ")
      .w_DESPRO = space(40)
      .w_DESFOR = space(40)
      .w_FLCOMM = space(1)
      .w_PRLISCOS = space(5)
      .w_DESLIS = space(40)
      .w_PRESECOS = space(4)
      .w_PRINVCOS = space(6)
      .w_PUUTEELA = 0
      .w_UTEDES = space(40)
      .w_UNIMIS = space(3)
      .w_PRSAFELT = 0
      .w_PRPERSCA = 0
      .w_PRORDPIA = space(1)
      .w_PREXPLPH = space(1)
      .w_PRPIAPUN = space(1)
      .w_PRPERPIA = space(1)
      .w_PRTIPDTF = space(1)
      .w_PRDOMMED = 0
      .w_PRINTRIO = 0
      .w_PRDATPAR = ctod("  /  /  ")
      .w_PRDATAUL = ctod("  /  /  ")
      .w_PRCSLAVO = 0
      .w_PRCSMATE = 0
      .w_PRCSSPGE = 0
      .w_PRCULAVO = 0
      .w_PRCUMATE = 0
      .w_PRCUSPGE = 0
      .w_PRCMLAVO = 0
      .w_PRCMMATE = 0
      .w_PRCMSPGE = 0
      .w_PRCLLAVO = 0
      .w_PRCLMATE = 0
      .w_PRCLSPGE = 0
      .w_PRCSLAVE = 0
      .w_PRCSLAVC = 0
      .w_PRCSMATC = 0
      .w_PRCSSPGC = 0
      .w_PRCULAVC = 0
      .w_PRCUMATC = 0
      .w_PRCUSPGC = 0
      .w_PRCMLAVC = 0
      .w_PRCMMATC = 0
      .w_PRCMSPGC = 0
      .w_PRCLLAVC = 0
      .w_PRCLMATC = 0
      .w_PRCLSPGC = 0
      .w_PRCSLVCE = 0
      .w_PRCULVCE = 0
      .w_PRCMLVCE = 0
      .w_PRCLLVCE = 0
      .w_PRCULAVE = 0
      .w_PRCMLAVE = 0
      .w_PRCLLAVE = 0
      .w_TOTSTAND = 0
      .w_TOTULTIM = 0
      .w_TOTMEDIO = 0
      .w_TOTLISTI = 0
      .w_PRDATCOS = ctod("  /  /  ")
      .w_PRLOTSTD = 0
      .w_PRDATULT = ctod("  /  /  ")
      .w_PRLOTULT = 0
      .w_PRDATMED = ctod("  /  /  ")
      .w_PRLOTMED = 0
      .w_PRDATLIS = ctod("  /  /  ")
      .w_PRLOTLIS = 0
      .w_PRLISCOS = space(5)
      .w_DESLIS = space(40)
      .w_PRESECOS = space(4)
      .w_PRINVCOS = space(6)
      .w_PUUTEELA = 0
      .w_UTEDES = space(40)
      .w_TOTSTAND = 0
      .w_TOTULTIM = 0
      .w_TOTMEDIO = 0
      .w_TOTLISTI = 0
      .w_PRDATCOS = ctod("  /  /  ")
      .w_PRLOTSTD = 0
      .w_PRDATULT = ctod("  /  /  ")
      .w_PRLOTULT = 0
      .w_PRDATMED = ctod("  /  /  ")
      .w_PRLOTMED = 0
      .w_PRDATLIS = ctod("  /  /  ")
      .w_PRLOTLIS = 0
      .w_TOTCSLAVO = 0
      .w_TOTCULAVO = 0
      .w_TOTCMLAVO = 0
      .w_TOTCLLAVO = 0
      .w_TOTCSLAVE = 0
      .w_TOTCULAVE = 0
      .w_TOTCMLAVE = 0
      .w_TOTCLLAVE = 0
      .w_TOTCSMATE = 0
      .w_TOTCUMATE = 0
      .w_TOTCMMATE = 0
      .w_TOTCLMATE = 0
      .w_TOTCSSPGE = 0
      .w_TOTCUSPGE = 0
      .w_TOTCMSPGE = 0
      .w_TOTCLSPGE = 0
      .w_PRGIODTF = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_TIPOPE = this.cFunction
        .w_PROVEN = this.oParentObject .w_ARPROPRE
        .w_CODDIS = this.oParentObject .w_ARCODDIS
        .w_TIPGES = this.oParentObject .w_ARTIPGES
        .w_TIPART = this.oParentObject .w_ARTIPART
          .DoRTCalc(7,7,.f.)
        .w_PRQTAMIN = 0
        .w_PRQTAMAX = 0
        .w_PRLOTRIO = 0
        .w_PRPUNRIO = 0
          .DoRTCalc(12,14,.f.)
        .w_PROGGMPS = "N"
          .DoRTCalc(16,16,.f.)
        .w_PRSCOMIN = 0
          .DoRTCalc(18,23,.f.)
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_PRORDMPS = 'S'
          .DoRTCalc(26,26,.f.)
        .w_PRTIPCON = 'F'
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_PRCODPRO))
          .link_1_30('Full')
          endif
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_PRCODFOR))
          .link_1_32('Full')
          endif
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .w_OBTEST = i_datsys
          .DoRTCalc(37,38,.f.)
        .w_FLCOMM = this.oParentObject .w_ARFLCOMM
        .DoRTCalc(40,40,.f.)
          if not(empty(.w_PRLISCOS))
          .link_3_7('Full')
          endif
        .DoRTCalc(41,44,.f.)
          if not(empty(.w_PUUTEELA))
          .link_3_14('Full')
          endif
          .DoRTCalc(45,45,.f.)
        .w_UNIMIS = this.oParentObject .w_ARUNMIS1
          .DoRTCalc(47,48,.f.)
        .w_PRORDPIA = 'N'
        .w_PREXPLPH = 'S'
        .w_PRPIAPUN = 'N'
        .w_PRPERPIA = 'D'
        .w_PRTIPDTF = 'E'
          .DoRTCalc(54,56,.f.)
        .w_PRDATAUL = .w_PRDATPAR+.w_PRINTRIO
          .DoRTCalc(58,89,.f.)
        .w_TOTSTAND = .w_PRCSLAVO+.w_PRCSLAVE+.w_PRCSLAVC+.w_PRCSLVCE+.w_PRCSMATE+.w_PRCSMATC+.w_PRCSSPGE+.w_PRCSSPGC
        .w_TOTULTIM = .w_PRCULAVO+.w_PRCULAVE+.w_PRCULAVC+.w_PRCULVCE+.w_PRCUMATE+.w_PRCUMATC+.w_PRCUSPGE+.w_PRCUSPGC
        .w_TOTMEDIO = .w_PRCMLAVO+.w_PRCMLAVE+.w_PRCMLAVC+.w_PRCMLVCE+.w_PRCMMATE+.w_PRCMMATC+.w_PRCMSPGE+.w_PRCMSPGC
        .w_TOTLISTI = .w_PRCLLAVO+.w_PRCLLAVE+.w_PRCLLAVC+.w_PRCLLVCE+.w_PRCLMATE+.w_PRCLMATC+.w_PRCLSPGE+.w_PRCLSPGC
        .DoRTCalc(94,102,.f.)
          if not(empty(.w_PRLISCOS))
          .link_4_63('Full')
          endif
        .DoRTCalc(103,106,.f.)
          if not(empty(.w_PUUTEELA))
          .link_4_70('Full')
          endif
          .DoRTCalc(107,107,.f.)
        .w_TOTSTAND = .w_PRCSLAVO+.w_PRCSLAVE+.w_PRCSLAVC+.w_PRCSLVCE+.w_PRCSMATE+.w_PRCSMATC+.w_PRCSSPGE+.w_PRCSSPGC
        .w_TOTULTIM = .w_PRCULAVO+.w_PRCULAVE+.w_PRCULAVC+.w_PRCULVCE+.w_PRCUMATE+.w_PRCUMATC+.w_PRCUSPGE+.w_PRCUSPGC
        .w_TOTMEDIO = .w_PRCMLAVO+.w_PRCMLAVE+.w_PRCMLAVC+.w_PRCMLVCE+.w_PRCMMATE+.w_PRCMMATC+.w_PRCMSPGE+.w_PRCMSPGC
        .w_TOTLISTI = .w_PRCLLAVO+.w_PRCLLAVE+.w_PRCLLAVC+.w_PRCLLVCE+.w_PRCLMATE+.w_PRCLMATC+.w_PRCLSPGE+.w_PRCLSPGC
          .DoRTCalc(112,119,.f.)
        .w_TOTCSLAVO = .w_PRCSLAVO+.w_PRCSLAVC
        .w_TOTCULAVO = .w_PRCULAVO+.w_PRCULAVC
        .w_TOTCMLAVO = .w_PRCMLAVO+.w_PRCMLAVC
        .w_TOTCLLAVO = .w_PRCLLAVO+.w_PRCLLAVC
        .w_TOTCSLAVE = .w_PRCSLAVE+.w_PRCSLVCE
        .w_TOTCULAVE = .w_PRCULAVE+.w_PRCULVCE
        .w_TOTCMLAVE = .w_PRCMLAVE+.w_PRCMLVCE
        .w_TOTCLLAVE = .w_PRCLLAVE+.w_PRCLLVCE
        .w_TOTCSMATE = .w_PRCSMATE+.w_PRCSMATC
        .w_TOTCUMATE = .w_PRCUMATE+.w_PRCUMATC
        .w_TOTCMMATE = .w_PRCMMATE+.w_PRCMMATC
        .w_TOTCLMATE = .w_PRCLMATE+.w_PRCLMATC
        .w_TOTCSSPGE = .w_PRCSSPGE+.w_PRCSSPGC
        .w_TOTCUSPGE = .w_PRCUSPGE+.w_PRCUSPGC
        .w_TOTCMSPGE = .w_PRCMSPGE+.w_PRCMSPGC
        .w_TOTCLSPGE = .w_PRCLSPGE+.w_PRCLSPGC
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_RIOR')
    this.DoRTCalc(136,136,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsma_apr
    * Forza sempre l'esecuzione della Check Form
    this.bUpdated=.t.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPRCOSSTA_1_7.enabled = i_bVal
      .Page1.oPag.oPRQTAMIN_1_8.enabled = i_bVal
      .Page1.oPag.oPRQTAMAX_1_9.enabled = i_bVal
      .Page1.oPag.oPRLOTRIO_1_10.enabled = i_bVal
      .Page1.oPag.oPRPUNRIO_1_11.enabled = i_bVal
      .Page1.oPag.oPRGIOINV_1_12.enabled = i_bVal
      .Page1.oPag.oPRLEAMPS_1_13.enabled = i_bVal
      .Page1.oPag.oPROGGMPS_1_15.enabled_(i_bVal)
      .Page1.oPag.oPRGIOCOP_1_16.enabled = i_bVal
      .Page1.oPag.oPRSCOMIN_1_17.enabled = i_bVal
      .Page1.oPag.oPRCOERSS_1_18.enabled = i_bVal
      .Page1.oPag.oPRSCOMAX_1_19.enabled = i_bVal
      .Page1.oPag.oPRDISMIN_1_20.enabled = i_bVal
      .Page1.oPag.oPRGIOAPP_1_21.enabled = i_bVal
      .Page1.oPag.oPRCOEFLT_1_22.enabled = i_bVal
      .Page1.oPag.oPRLOTMED_1_23.enabled = i_bVal
      .Page1.oPag.oPRORDMPS_1_25.enabled = i_bVal
      .Page1.oPag.oPRCODPRO_1_30.enabled = i_bVal
      .Page1.oPag.oPRCODFOR_1_32.enabled = i_bVal
      .Page1.oPag.oPRSAFELT_1_64.enabled = i_bVal
      .Page1.oPag.oPRPERSCA_1_66.enabled = i_bVal
      .Page1.oPag.oPRORDPIA_1_68.enabled = i_bVal
      .Page1.oPag.oPREXPLPH_1_69.enabled = i_bVal
      .Page1.oPag.oPRPIAPUN_1_70.enabled = i_bVal
      .Page1.oPag.oPRPERPIA_1_71.enabled = i_bVal
      .Page1.oPag.oPRTIPDTF_1_74.enabled = i_bVal
      .Page1.oPag.oPRDOMMED_1_75.enabled = i_bVal
      .Page1.oPag.oPRGIODTF_1_82.enabled = i_bVal
    endwith
    this.GSMA_MPR.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PAR_RIOR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSMA_MPR.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_RIOR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODART,"PRCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCOSSTA,"PRCOSSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRQTAMIN,"PRQTAMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRQTAMAX,"PRQTAMAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLOTRIO,"PRLOTRIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPUNRIO,"PRPUNRIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGIOINV,"PRGIOINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLEAMPS,"PRLEAMPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLOWLEV,"PRLOWLEV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PROGGMPS,"PROGGMPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGIOCOP,"PRGIOCOP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRSCOMIN,"PRSCOMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCOERSS,"PRCOERSS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRSCOMAX,"PRSCOMAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDISMIN,"PRDISMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGIOAPP,"PRGIOAPP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCOEFLT,"PRCOEFLT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLOTMED,"PRLOTMED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRORDMPS,"PRORDMPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPCON,"PRTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODPRO,"PRCODPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODFOR,"PRCODFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLISCOS,"PRLISCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRESECOS,"PRESECOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRINVCOS,"PRINVCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUUTEELA,"PUUTEELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRSAFELT,"PRSAFELT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPERSCA,"PRPERSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRORDPIA,"PRORDPIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PREXPLPH,"PREXPLPH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPIAPUN,"PRPIAPUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPERPIA,"PRPERPIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPDTF,"PRTIPDTF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDOMMED,"PRDOMMED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRINTRIO,"PRINTRIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATPAR,"PRDATPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATAUL,"PRDATAUL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCSLAVO,"PRCSLAVO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCSMATE,"PRCSMATE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCSSPGE,"PRCSSPGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCULAVO,"PRCULAVO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCUMATE,"PRCUMATE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCUSPGE,"PRCUSPGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCMLAVO,"PRCMLAVO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCMMATE,"PRCMMATE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCMSPGE,"PRCMSPGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCLLAVO,"PRCLLAVO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCLMATE,"PRCLMATE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCLSPGE,"PRCLSPGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCSLAVE,"PRCSLAVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCSLAVC,"PRCSLAVC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCSMATC,"PRCSMATC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCSSPGC,"PRCSSPGC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCULAVC,"PRCULAVC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCUMATC,"PRCUMATC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCUSPGC,"PRCUSPGC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCMLAVC,"PRCMLAVC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCMMATC,"PRCMMATC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCMSPGC,"PRCMSPGC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCLLAVC,"PRCLLAVC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCLMATC,"PRCLMATC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCLSPGC,"PRCLSPGC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCSLVCE,"PRCSLVCE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCULVCE,"PRCULVCE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCMLVCE,"PRCMLVCE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCLLVCE,"PRCLLVCE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCULAVE,"PRCULAVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCMLAVE,"PRCMLAVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCLLAVE,"PRCLLAVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATCOS,"PRDATCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLOTSTD,"PRLOTSTD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATULT,"PRDATULT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLOTULT,"PRLOTULT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATMED,"PRDATMED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLOTMED,"PRLOTMED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATLIS,"PRDATLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLOTLIS,"PRLOTLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLISCOS,"PRLISCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRESECOS,"PRESECOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRINVCOS,"PRINVCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUUTEELA,"PUUTEELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATCOS,"PRDATCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLOTSTD,"PRLOTSTD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATULT,"PRDATULT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLOTULT,"PRLOTULT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATMED,"PRDATMED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLOTMED,"PRLOTMED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATLIS,"PRDATLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLOTLIS,"PRLOTLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGIODTF,"PRGIODTF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_RIOR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_RIOR
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_RIOR')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_RIOR')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PRCODART,PRCOSSTA,PRQTAMIN,PRQTAMAX,PRLOTRIO"+;
                  ",PRPUNRIO,PRGIOINV,PRLEAMPS,PRLOWLEV,PROGGMPS"+;
                  ",PRGIOCOP,PRSCOMIN,PRCOERSS,PRSCOMAX,PRDISMIN"+;
                  ",PRGIOAPP,PRCOEFLT,PRLOTMED,PRORDMPS,PRTIPCON"+;
                  ",PRCODPRO,PRCODFOR,PRLISCOS,PRESECOS,PRINVCOS"+;
                  ",PUUTEELA,PRSAFELT,PRPERSCA,PRORDPIA,PREXPLPH"+;
                  ",PRPIAPUN,PRPERPIA,PRTIPDTF,PRDOMMED,PRINTRIO"+;
                  ",PRDATPAR,PRDATAUL,PRCSLAVO,PRCSMATE,PRCSSPGE"+;
                  ",PRCULAVO,PRCUMATE,PRCUSPGE,PRCMLAVO,PRCMMATE"+;
                  ",PRCMSPGE,PRCLLAVO,PRCLMATE,PRCLSPGE,PRCSLAVE"+;
                  ",PRCSLAVC,PRCSMATC,PRCSSPGC,PRCULAVC,PRCUMATC"+;
                  ",PRCUSPGC,PRCMLAVC,PRCMMATC,PRCMSPGC,PRCLLAVC"+;
                  ",PRCLMATC,PRCLSPGC,PRCSLVCE,PRCULVCE,PRCMLVCE"+;
                  ",PRCLLVCE,PRCULAVE,PRCMLAVE,PRCLLAVE,PRDATCOS"+;
                  ",PRLOTSTD,PRDATULT,PRLOTULT,PRDATMED,PRDATLIS"+;
                  ",PRLOTLIS,PRGIODTF "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PRCODART)+;
                  ","+cp_ToStrODBC(this.w_PRCOSSTA)+;
                  ","+cp_ToStrODBC(this.w_PRQTAMIN)+;
                  ","+cp_ToStrODBC(this.w_PRQTAMAX)+;
                  ","+cp_ToStrODBC(this.w_PRLOTRIO)+;
                  ","+cp_ToStrODBC(this.w_PRPUNRIO)+;
                  ","+cp_ToStrODBC(this.w_PRGIOINV)+;
                  ","+cp_ToStrODBC(this.w_PRLEAMPS)+;
                  ","+cp_ToStrODBC(this.w_PRLOWLEV)+;
                  ","+cp_ToStrODBC(this.w_PROGGMPS)+;
                  ","+cp_ToStrODBC(this.w_PRGIOCOP)+;
                  ","+cp_ToStrODBC(this.w_PRSCOMIN)+;
                  ","+cp_ToStrODBC(this.w_PRCOERSS)+;
                  ","+cp_ToStrODBC(this.w_PRSCOMAX)+;
                  ","+cp_ToStrODBC(this.w_PRDISMIN)+;
                  ","+cp_ToStrODBC(this.w_PRGIOAPP)+;
                  ","+cp_ToStrODBC(this.w_PRCOEFLT)+;
                  ","+cp_ToStrODBC(this.w_PRLOTMED)+;
                  ","+cp_ToStrODBC(this.w_PRORDMPS)+;
                  ","+cp_ToStrODBC(this.w_PRTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_PRCODPRO)+;
                  ","+cp_ToStrODBCNull(this.w_PRCODFOR)+;
                  ","+cp_ToStrODBCNull(this.w_PRLISCOS)+;
                  ","+cp_ToStrODBC(this.w_PRESECOS)+;
                  ","+cp_ToStrODBC(this.w_PRINVCOS)+;
                  ","+cp_ToStrODBCNull(this.w_PUUTEELA)+;
                  ","+cp_ToStrODBC(this.w_PRSAFELT)+;
                  ","+cp_ToStrODBC(this.w_PRPERSCA)+;
                  ","+cp_ToStrODBC(this.w_PRORDPIA)+;
                  ","+cp_ToStrODBC(this.w_PREXPLPH)+;
                  ","+cp_ToStrODBC(this.w_PRPIAPUN)+;
                  ","+cp_ToStrODBC(this.w_PRPERPIA)+;
                  ","+cp_ToStrODBC(this.w_PRTIPDTF)+;
                  ","+cp_ToStrODBC(this.w_PRDOMMED)+;
                  ","+cp_ToStrODBC(this.w_PRINTRIO)+;
                  ","+cp_ToStrODBC(this.w_PRDATPAR)+;
                  ","+cp_ToStrODBC(this.w_PRDATAUL)+;
                  ","+cp_ToStrODBC(this.w_PRCSLAVO)+;
                  ","+cp_ToStrODBC(this.w_PRCSMATE)+;
                  ","+cp_ToStrODBC(this.w_PRCSSPGE)+;
                  ","+cp_ToStrODBC(this.w_PRCULAVO)+;
                  ","+cp_ToStrODBC(this.w_PRCUMATE)+;
                  ","+cp_ToStrODBC(this.w_PRCUSPGE)+;
                  ","+cp_ToStrODBC(this.w_PRCMLAVO)+;
                  ","+cp_ToStrODBC(this.w_PRCMMATE)+;
                  ","+cp_ToStrODBC(this.w_PRCMSPGE)+;
                  ","+cp_ToStrODBC(this.w_PRCLLAVO)+;
                  ","+cp_ToStrODBC(this.w_PRCLMATE)+;
                  ","+cp_ToStrODBC(this.w_PRCLSPGE)+;
                  ","+cp_ToStrODBC(this.w_PRCSLAVE)+;
                  ","+cp_ToStrODBC(this.w_PRCSLAVC)+;
                  ","+cp_ToStrODBC(this.w_PRCSMATC)+;
                  ","+cp_ToStrODBC(this.w_PRCSSPGC)+;
                  ","+cp_ToStrODBC(this.w_PRCULAVC)+;
                  ","+cp_ToStrODBC(this.w_PRCUMATC)+;
                  ","+cp_ToStrODBC(this.w_PRCUSPGC)+;
                  ","+cp_ToStrODBC(this.w_PRCMLAVC)+;
                  ","+cp_ToStrODBC(this.w_PRCMMATC)+;
                  ","+cp_ToStrODBC(this.w_PRCMSPGC)+;
                  ","+cp_ToStrODBC(this.w_PRCLLAVC)+;
                  ","+cp_ToStrODBC(this.w_PRCLMATC)+;
                  ","+cp_ToStrODBC(this.w_PRCLSPGC)+;
                  ","+cp_ToStrODBC(this.w_PRCSLVCE)+;
                  ","+cp_ToStrODBC(this.w_PRCULVCE)+;
                  ","+cp_ToStrODBC(this.w_PRCMLVCE)+;
                  ","+cp_ToStrODBC(this.w_PRCLLVCE)+;
                  ","+cp_ToStrODBC(this.w_PRCULAVE)+;
                  ","+cp_ToStrODBC(this.w_PRCMLAVE)+;
                  ","+cp_ToStrODBC(this.w_PRCLLAVE)+;
                  ","+cp_ToStrODBC(this.w_PRDATCOS)+;
                  ","+cp_ToStrODBC(this.w_PRLOTSTD)+;
                  ","+cp_ToStrODBC(this.w_PRDATULT)+;
                  ","+cp_ToStrODBC(this.w_PRLOTULT)+;
                  ","+cp_ToStrODBC(this.w_PRDATMED)+;
                  ","+cp_ToStrODBC(this.w_PRDATLIS)+;
                  ","+cp_ToStrODBC(this.w_PRLOTLIS)+;
                  ","+cp_ToStrODBC(this.w_PRGIODTF)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_RIOR')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_RIOR')
        cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_PRCODART)
        INSERT INTO (i_cTable);
              (PRCODART,PRCOSSTA,PRQTAMIN,PRQTAMAX,PRLOTRIO,PRPUNRIO,PRGIOINV,PRLEAMPS,PRLOWLEV,PROGGMPS,PRGIOCOP,PRSCOMIN,PRCOERSS,PRSCOMAX,PRDISMIN,PRGIOAPP,PRCOEFLT,PRLOTMED,PRORDMPS,PRTIPCON,PRCODPRO,PRCODFOR,PRLISCOS,PRESECOS,PRINVCOS,PUUTEELA,PRSAFELT,PRPERSCA,PRORDPIA,PREXPLPH,PRPIAPUN,PRPERPIA,PRTIPDTF,PRDOMMED,PRINTRIO,PRDATPAR,PRDATAUL,PRCSLAVO,PRCSMATE,PRCSSPGE,PRCULAVO,PRCUMATE,PRCUSPGE,PRCMLAVO,PRCMMATE,PRCMSPGE,PRCLLAVO,PRCLMATE,PRCLSPGE,PRCSLAVE,PRCSLAVC,PRCSMATC,PRCSSPGC,PRCULAVC,PRCUMATC,PRCUSPGC,PRCMLAVC,PRCMMATC,PRCMSPGC,PRCLLAVC,PRCLMATC,PRCLSPGC,PRCSLVCE,PRCULVCE,PRCMLVCE,PRCLLVCE,PRCULAVE,PRCMLAVE,PRCLLAVE,PRDATCOS,PRLOTSTD,PRDATULT,PRLOTULT,PRDATMED,PRDATLIS,PRLOTLIS,PRGIODTF  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PRCODART;
                  ,this.w_PRCOSSTA;
                  ,this.w_PRQTAMIN;
                  ,this.w_PRQTAMAX;
                  ,this.w_PRLOTRIO;
                  ,this.w_PRPUNRIO;
                  ,this.w_PRGIOINV;
                  ,this.w_PRLEAMPS;
                  ,this.w_PRLOWLEV;
                  ,this.w_PROGGMPS;
                  ,this.w_PRGIOCOP;
                  ,this.w_PRSCOMIN;
                  ,this.w_PRCOERSS;
                  ,this.w_PRSCOMAX;
                  ,this.w_PRDISMIN;
                  ,this.w_PRGIOAPP;
                  ,this.w_PRCOEFLT;
                  ,this.w_PRLOTMED;
                  ,this.w_PRORDMPS;
                  ,this.w_PRTIPCON;
                  ,this.w_PRCODPRO;
                  ,this.w_PRCODFOR;
                  ,this.w_PRLISCOS;
                  ,this.w_PRESECOS;
                  ,this.w_PRINVCOS;
                  ,this.w_PUUTEELA;
                  ,this.w_PRSAFELT;
                  ,this.w_PRPERSCA;
                  ,this.w_PRORDPIA;
                  ,this.w_PREXPLPH;
                  ,this.w_PRPIAPUN;
                  ,this.w_PRPERPIA;
                  ,this.w_PRTIPDTF;
                  ,this.w_PRDOMMED;
                  ,this.w_PRINTRIO;
                  ,this.w_PRDATPAR;
                  ,this.w_PRDATAUL;
                  ,this.w_PRCSLAVO;
                  ,this.w_PRCSMATE;
                  ,this.w_PRCSSPGE;
                  ,this.w_PRCULAVO;
                  ,this.w_PRCUMATE;
                  ,this.w_PRCUSPGE;
                  ,this.w_PRCMLAVO;
                  ,this.w_PRCMMATE;
                  ,this.w_PRCMSPGE;
                  ,this.w_PRCLLAVO;
                  ,this.w_PRCLMATE;
                  ,this.w_PRCLSPGE;
                  ,this.w_PRCSLAVE;
                  ,this.w_PRCSLAVC;
                  ,this.w_PRCSMATC;
                  ,this.w_PRCSSPGC;
                  ,this.w_PRCULAVC;
                  ,this.w_PRCUMATC;
                  ,this.w_PRCUSPGC;
                  ,this.w_PRCMLAVC;
                  ,this.w_PRCMMATC;
                  ,this.w_PRCMSPGC;
                  ,this.w_PRCLLAVC;
                  ,this.w_PRCLMATC;
                  ,this.w_PRCLSPGC;
                  ,this.w_PRCSLVCE;
                  ,this.w_PRCULVCE;
                  ,this.w_PRCMLVCE;
                  ,this.w_PRCLLVCE;
                  ,this.w_PRCULAVE;
                  ,this.w_PRCMLAVE;
                  ,this.w_PRCLLAVE;
                  ,this.w_PRDATCOS;
                  ,this.w_PRLOTSTD;
                  ,this.w_PRDATULT;
                  ,this.w_PRLOTULT;
                  ,this.w_PRDATMED;
                  ,this.w_PRDATLIS;
                  ,this.w_PRLOTLIS;
                  ,this.w_PRGIODTF;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_RIOR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_RIOR_IDX,i_nConn)
      *
      * update PAR_RIOR
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_RIOR')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PRCOSSTA="+cp_ToStrODBC(this.w_PRCOSSTA)+;
             ",PRQTAMIN="+cp_ToStrODBC(this.w_PRQTAMIN)+;
             ",PRQTAMAX="+cp_ToStrODBC(this.w_PRQTAMAX)+;
             ",PRLOTRIO="+cp_ToStrODBC(this.w_PRLOTRIO)+;
             ",PRPUNRIO="+cp_ToStrODBC(this.w_PRPUNRIO)+;
             ",PRGIOINV="+cp_ToStrODBC(this.w_PRGIOINV)+;
             ",PRLEAMPS="+cp_ToStrODBC(this.w_PRLEAMPS)+;
             ",PRLOWLEV="+cp_ToStrODBC(this.w_PRLOWLEV)+;
             ",PROGGMPS="+cp_ToStrODBC(this.w_PROGGMPS)+;
             ",PRGIOCOP="+cp_ToStrODBC(this.w_PRGIOCOP)+;
             ",PRSCOMIN="+cp_ToStrODBC(this.w_PRSCOMIN)+;
             ",PRCOERSS="+cp_ToStrODBC(this.w_PRCOERSS)+;
             ",PRSCOMAX="+cp_ToStrODBC(this.w_PRSCOMAX)+;
             ",PRDISMIN="+cp_ToStrODBC(this.w_PRDISMIN)+;
             ",PRGIOAPP="+cp_ToStrODBC(this.w_PRGIOAPP)+;
             ",PRCOEFLT="+cp_ToStrODBC(this.w_PRCOEFLT)+;
             ",PRLOTMED="+cp_ToStrODBC(this.w_PRLOTMED)+;
             ",PRORDMPS="+cp_ToStrODBC(this.w_PRORDMPS)+;
             ",PRTIPCON="+cp_ToStrODBC(this.w_PRTIPCON)+;
             ",PRCODPRO="+cp_ToStrODBCNull(this.w_PRCODPRO)+;
             ",PRCODFOR="+cp_ToStrODBCNull(this.w_PRCODFOR)+;
             ",PRLISCOS="+cp_ToStrODBCNull(this.w_PRLISCOS)+;
             ",PRESECOS="+cp_ToStrODBC(this.w_PRESECOS)+;
             ",PRINVCOS="+cp_ToStrODBC(this.w_PRINVCOS)+;
             ",PUUTEELA="+cp_ToStrODBCNull(this.w_PUUTEELA)+;
             ",PRSAFELT="+cp_ToStrODBC(this.w_PRSAFELT)+;
             ",PRPERSCA="+cp_ToStrODBC(this.w_PRPERSCA)+;
             ",PRORDPIA="+cp_ToStrODBC(this.w_PRORDPIA)+;
             ",PREXPLPH="+cp_ToStrODBC(this.w_PREXPLPH)+;
             ",PRPIAPUN="+cp_ToStrODBC(this.w_PRPIAPUN)+;
             ",PRPERPIA="+cp_ToStrODBC(this.w_PRPERPIA)+;
             ",PRTIPDTF="+cp_ToStrODBC(this.w_PRTIPDTF)+;
             ",PRDOMMED="+cp_ToStrODBC(this.w_PRDOMMED)+;
             ",PRINTRIO="+cp_ToStrODBC(this.w_PRINTRIO)+;
             ",PRDATPAR="+cp_ToStrODBC(this.w_PRDATPAR)+;
             ",PRDATAUL="+cp_ToStrODBC(this.w_PRDATAUL)+;
             ",PRCSLAVO="+cp_ToStrODBC(this.w_PRCSLAVO)+;
             ",PRCSMATE="+cp_ToStrODBC(this.w_PRCSMATE)+;
             ",PRCSSPGE="+cp_ToStrODBC(this.w_PRCSSPGE)+;
             ",PRCULAVO="+cp_ToStrODBC(this.w_PRCULAVO)+;
             ",PRCUMATE="+cp_ToStrODBC(this.w_PRCUMATE)+;
             ",PRCUSPGE="+cp_ToStrODBC(this.w_PRCUSPGE)+;
             ",PRCMLAVO="+cp_ToStrODBC(this.w_PRCMLAVO)+;
             ",PRCMMATE="+cp_ToStrODBC(this.w_PRCMMATE)+;
             ",PRCMSPGE="+cp_ToStrODBC(this.w_PRCMSPGE)+;
             ",PRCLLAVO="+cp_ToStrODBC(this.w_PRCLLAVO)+;
             ",PRCLMATE="+cp_ToStrODBC(this.w_PRCLMATE)+;
             ",PRCLSPGE="+cp_ToStrODBC(this.w_PRCLSPGE)+;
             ",PRCSLAVE="+cp_ToStrODBC(this.w_PRCSLAVE)+;
             ",PRCSLAVC="+cp_ToStrODBC(this.w_PRCSLAVC)+;
             ",PRCSMATC="+cp_ToStrODBC(this.w_PRCSMATC)+;
             ",PRCSSPGC="+cp_ToStrODBC(this.w_PRCSSPGC)+;
             ",PRCULAVC="+cp_ToStrODBC(this.w_PRCULAVC)+;
             ",PRCUMATC="+cp_ToStrODBC(this.w_PRCUMATC)+;
             ",PRCUSPGC="+cp_ToStrODBC(this.w_PRCUSPGC)+;
             ",PRCMLAVC="+cp_ToStrODBC(this.w_PRCMLAVC)+;
             ",PRCMMATC="+cp_ToStrODBC(this.w_PRCMMATC)+;
             ",PRCMSPGC="+cp_ToStrODBC(this.w_PRCMSPGC)+;
             ",PRCLLAVC="+cp_ToStrODBC(this.w_PRCLLAVC)+;
             ",PRCLMATC="+cp_ToStrODBC(this.w_PRCLMATC)+;
             ",PRCLSPGC="+cp_ToStrODBC(this.w_PRCLSPGC)+;
             ",PRCSLVCE="+cp_ToStrODBC(this.w_PRCSLVCE)+;
             ",PRCULVCE="+cp_ToStrODBC(this.w_PRCULVCE)+;
             ",PRCMLVCE="+cp_ToStrODBC(this.w_PRCMLVCE)+;
             ",PRCLLVCE="+cp_ToStrODBC(this.w_PRCLLVCE)+;
             ",PRCULAVE="+cp_ToStrODBC(this.w_PRCULAVE)+;
             ",PRCMLAVE="+cp_ToStrODBC(this.w_PRCMLAVE)+;
             ",PRCLLAVE="+cp_ToStrODBC(this.w_PRCLLAVE)+;
             ",PRDATCOS="+cp_ToStrODBC(this.w_PRDATCOS)+;
             ",PRLOTSTD="+cp_ToStrODBC(this.w_PRLOTSTD)+;
             ",PRDATULT="+cp_ToStrODBC(this.w_PRDATULT)+;
             ",PRLOTULT="+cp_ToStrODBC(this.w_PRLOTULT)+;
             ",PRDATMED="+cp_ToStrODBC(this.w_PRDATMED)+;
             ",PRDATLIS="+cp_ToStrODBC(this.w_PRDATLIS)+;
             ",PRLOTLIS="+cp_ToStrODBC(this.w_PRLOTLIS)+;
             ",PRGIODTF="+cp_ToStrODBC(this.w_PRGIODTF)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_RIOR')
        i_cWhere = cp_PKFox(i_cTable  ,'PRCODART',this.w_PRCODART  )
        UPDATE (i_cTable) SET;
              PRCOSSTA=this.w_PRCOSSTA;
             ,PRQTAMIN=this.w_PRQTAMIN;
             ,PRQTAMAX=this.w_PRQTAMAX;
             ,PRLOTRIO=this.w_PRLOTRIO;
             ,PRPUNRIO=this.w_PRPUNRIO;
             ,PRGIOINV=this.w_PRGIOINV;
             ,PRLEAMPS=this.w_PRLEAMPS;
             ,PRLOWLEV=this.w_PRLOWLEV;
             ,PROGGMPS=this.w_PROGGMPS;
             ,PRGIOCOP=this.w_PRGIOCOP;
             ,PRSCOMIN=this.w_PRSCOMIN;
             ,PRCOERSS=this.w_PRCOERSS;
             ,PRSCOMAX=this.w_PRSCOMAX;
             ,PRDISMIN=this.w_PRDISMIN;
             ,PRGIOAPP=this.w_PRGIOAPP;
             ,PRCOEFLT=this.w_PRCOEFLT;
             ,PRLOTMED=this.w_PRLOTMED;
             ,PRORDMPS=this.w_PRORDMPS;
             ,PRTIPCON=this.w_PRTIPCON;
             ,PRCODPRO=this.w_PRCODPRO;
             ,PRCODFOR=this.w_PRCODFOR;
             ,PRLISCOS=this.w_PRLISCOS;
             ,PRESECOS=this.w_PRESECOS;
             ,PRINVCOS=this.w_PRINVCOS;
             ,PUUTEELA=this.w_PUUTEELA;
             ,PRSAFELT=this.w_PRSAFELT;
             ,PRPERSCA=this.w_PRPERSCA;
             ,PRORDPIA=this.w_PRORDPIA;
             ,PREXPLPH=this.w_PREXPLPH;
             ,PRPIAPUN=this.w_PRPIAPUN;
             ,PRPERPIA=this.w_PRPERPIA;
             ,PRTIPDTF=this.w_PRTIPDTF;
             ,PRDOMMED=this.w_PRDOMMED;
             ,PRINTRIO=this.w_PRINTRIO;
             ,PRDATPAR=this.w_PRDATPAR;
             ,PRDATAUL=this.w_PRDATAUL;
             ,PRCSLAVO=this.w_PRCSLAVO;
             ,PRCSMATE=this.w_PRCSMATE;
             ,PRCSSPGE=this.w_PRCSSPGE;
             ,PRCULAVO=this.w_PRCULAVO;
             ,PRCUMATE=this.w_PRCUMATE;
             ,PRCUSPGE=this.w_PRCUSPGE;
             ,PRCMLAVO=this.w_PRCMLAVO;
             ,PRCMMATE=this.w_PRCMMATE;
             ,PRCMSPGE=this.w_PRCMSPGE;
             ,PRCLLAVO=this.w_PRCLLAVO;
             ,PRCLMATE=this.w_PRCLMATE;
             ,PRCLSPGE=this.w_PRCLSPGE;
             ,PRCSLAVE=this.w_PRCSLAVE;
             ,PRCSLAVC=this.w_PRCSLAVC;
             ,PRCSMATC=this.w_PRCSMATC;
             ,PRCSSPGC=this.w_PRCSSPGC;
             ,PRCULAVC=this.w_PRCULAVC;
             ,PRCUMATC=this.w_PRCUMATC;
             ,PRCUSPGC=this.w_PRCUSPGC;
             ,PRCMLAVC=this.w_PRCMLAVC;
             ,PRCMMATC=this.w_PRCMMATC;
             ,PRCMSPGC=this.w_PRCMSPGC;
             ,PRCLLAVC=this.w_PRCLLAVC;
             ,PRCLMATC=this.w_PRCLMATC;
             ,PRCLSPGC=this.w_PRCLSPGC;
             ,PRCSLVCE=this.w_PRCSLVCE;
             ,PRCULVCE=this.w_PRCULVCE;
             ,PRCMLVCE=this.w_PRCMLVCE;
             ,PRCLLVCE=this.w_PRCLLVCE;
             ,PRCULAVE=this.w_PRCULAVE;
             ,PRCMLAVE=this.w_PRCMLAVE;
             ,PRCLLAVE=this.w_PRCLLAVE;
             ,PRDATCOS=this.w_PRDATCOS;
             ,PRLOTSTD=this.w_PRLOTSTD;
             ,PRDATULT=this.w_PRDATULT;
             ,PRLOTULT=this.w_PRLOTULT;
             ,PRDATMED=this.w_PRDATMED;
             ,PRDATLIS=this.w_PRDATLIS;
             ,PRLOTLIS=this.w_PRLOTLIS;
             ,PRGIODTF=this.w_PRGIODTF;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSMA_MPR : Saving
      this.GSMA_MPR.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PRCODART,"PRCODART";
             )
      this.GSMA_MPR.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- GSMA_MPR : Deleting
    this.GSMA_MPR.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PRCODART,"PRCODART";
           )
    this.GSMA_MPR.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_RIOR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_RIOR_IDX,i_nConn)
      *
      * delete PAR_RIOR
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PRCODART',this.w_PRCODART  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_RIOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_TIPOPE = this.cFunction
            .w_PROVEN = this.oParentObject .w_ARPROPRE
            .w_CODDIS = this.oParentObject .w_ARCODDIS
            .w_TIPGES = this.oParentObject .w_ARTIPGES
            .w_TIPART = this.oParentObject .w_ARTIPART
        .DoRTCalc(7,7,.t.)
        if .o_FLCOMM<>.w_FLCOMM
            .w_PRQTAMIN = 0
        endif
        if .o_FLCOMM<>.w_FLCOMM
            .w_PRQTAMAX = 0
        endif
        if .o_FLCOMM<>.w_FLCOMM
            .w_PRLOTRIO = 0
        endif
        if .o_FLCOMM<>.w_FLCOMM
            .w_PRPUNRIO = 0
        endif
        .DoRTCalc(12,16,.t.)
        if .o_FLCOMM<>.w_FLCOMM
            .w_PRSCOMIN = 0
        endif
        .DoRTCalc(18,23,.t.)
            .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .DoRTCalc(25,26,.t.)
            .w_PRTIPCON = 'F'
        .DoRTCalc(28,29,.t.)
            .w_UNIMIS = this.oParentObject .w_ARUNMIS1
            .w_UNIMIS = this.oParentObject .w_ARUNMIS1
            .w_UNIMIS = this.oParentObject .w_ARUNMIS1
            .w_UNIMIS = this.oParentObject .w_ARUNMIS1
            .w_UNIMIS = this.oParentObject .w_ARUNMIS1
            .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .DoRTCalc(36,38,.t.)
            .w_FLCOMM = this.oParentObject .w_ARFLCOMM
          .link_3_7('Full')
        .DoRTCalc(41,43,.t.)
          .link_3_14('Full')
        .DoRTCalc(45,45,.t.)
            .w_UNIMIS = this.oParentObject .w_ARUNMIS1
        .DoRTCalc(47,49,.t.)
        if .o_TIPART<>.w_TIPART.or. .o_PROGGMPS<>.w_PROGGMPS
            .w_PREXPLPH = 'S'
        endif
        if .o_PROGGMPS<>.w_PROGGMPS.or. .o_PREXPLPH<>.w_PREXPLPH
            .w_PRPIAPUN = 'N'
        endif
        if .o_PROGGMPS<>.w_PROGGMPS
            .w_PRPERPIA = 'D'
        endif
        .DoRTCalc(53,56,.t.)
        if .o_PRINTRIO<>.w_PRINTRIO.or. .o_PRDATPAR<>.w_PRDATPAR
            .w_PRDATAUL = .w_PRDATPAR+.w_PRINTRIO
        endif
        .DoRTCalc(58,89,.t.)
            .w_TOTSTAND = .w_PRCSLAVO+.w_PRCSLAVE+.w_PRCSLAVC+.w_PRCSLVCE+.w_PRCSMATE+.w_PRCSMATC+.w_PRCSSPGE+.w_PRCSSPGC
            .w_TOTULTIM = .w_PRCULAVO+.w_PRCULAVE+.w_PRCULAVC+.w_PRCULVCE+.w_PRCUMATE+.w_PRCUMATC+.w_PRCUSPGE+.w_PRCUSPGC
            .w_TOTMEDIO = .w_PRCMLAVO+.w_PRCMLAVE+.w_PRCMLAVC+.w_PRCMLVCE+.w_PRCMMATE+.w_PRCMMATC+.w_PRCMSPGE+.w_PRCMSPGC
            .w_TOTLISTI = .w_PRCLLAVO+.w_PRCLLAVE+.w_PRCLLAVC+.w_PRCLLVCE+.w_PRCLMATE+.w_PRCLMATC+.w_PRCLSPGE+.w_PRCLSPGC
        .DoRTCalc(94,101,.t.)
          .link_4_63('Full')
        .DoRTCalc(103,105,.t.)
          .link_4_70('Full')
        .DoRTCalc(107,107,.t.)
            .w_TOTSTAND = .w_PRCSLAVO+.w_PRCSLAVE+.w_PRCSLAVC+.w_PRCSLVCE+.w_PRCSMATE+.w_PRCSMATC+.w_PRCSSPGE+.w_PRCSSPGC
            .w_TOTULTIM = .w_PRCULAVO+.w_PRCULAVE+.w_PRCULAVC+.w_PRCULVCE+.w_PRCUMATE+.w_PRCUMATC+.w_PRCUSPGE+.w_PRCUSPGC
            .w_TOTMEDIO = .w_PRCMLAVO+.w_PRCMLAVE+.w_PRCMLAVC+.w_PRCMLVCE+.w_PRCMMATE+.w_PRCMMATC+.w_PRCMSPGE+.w_PRCMSPGC
            .w_TOTLISTI = .w_PRCLLAVO+.w_PRCLLAVE+.w_PRCLLAVC+.w_PRCLLVCE+.w_PRCLMATE+.w_PRCLMATC+.w_PRCLSPGE+.w_PRCLSPGC
        .DoRTCalc(112,119,.t.)
            .w_TOTCSLAVO = .w_PRCSLAVO+.w_PRCSLAVC
            .w_TOTCULAVO = .w_PRCULAVO+.w_PRCULAVC
            .w_TOTCMLAVO = .w_PRCMLAVO+.w_PRCMLAVC
            .w_TOTCLLAVO = .w_PRCLLAVO+.w_PRCLLAVC
            .w_TOTCSLAVE = .w_PRCSLAVE+.w_PRCSLVCE
            .w_TOTCULAVE = .w_PRCULAVE+.w_PRCULVCE
            .w_TOTCMLAVE = .w_PRCMLAVE+.w_PRCMLVCE
            .w_TOTCLLAVE = .w_PRCLLAVE+.w_PRCLLVCE
            .w_TOTCSMATE = .w_PRCSMATE+.w_PRCSMATC
            .w_TOTCUMATE = .w_PRCUMATE+.w_PRCUMATC
            .w_TOTCMMATE = .w_PRCMMATE+.w_PRCMMATC
            .w_TOTCLMATE = .w_PRCLMATE+.w_PRCLMATC
            .w_TOTCSSPGE = .w_PRCSSPGE+.w_PRCSSPGC
            .w_TOTCUSPGE = .w_PRCUSPGE+.w_PRCUSPGC
            .w_TOTCMSPGE = .w_PRCMSPGE+.w_PRCMSPGC
            .w_TOTCLSPGE = .w_PRCLSPGE+.w_PRCLSPGC
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(136,136,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPRQTAMIN_1_8.enabled = this.oPgFrm.Page1.oPag.oPRQTAMIN_1_8.mCond()
    this.oPgFrm.Page1.oPag.oPRQTAMAX_1_9.enabled = this.oPgFrm.Page1.oPag.oPRQTAMAX_1_9.mCond()
    this.oPgFrm.Page1.oPag.oPRLOTRIO_1_10.enabled = this.oPgFrm.Page1.oPag.oPRLOTRIO_1_10.mCond()
    this.oPgFrm.Page1.oPag.oPRPUNRIO_1_11.enabled = this.oPgFrm.Page1.oPag.oPRPUNRIO_1_11.mCond()
    this.oPgFrm.Page1.oPag.oPRLEAMPS_1_13.enabled = this.oPgFrm.Page1.oPag.oPRLEAMPS_1_13.mCond()
    this.oPgFrm.Page1.oPag.oPRGIOCOP_1_16.enabled = this.oPgFrm.Page1.oPag.oPRGIOCOP_1_16.mCond()
    this.oPgFrm.Page1.oPag.oPRCOEFLT_1_22.enabled = this.oPgFrm.Page1.oPag.oPRCOEFLT_1_22.mCond()
    this.oPgFrm.Page1.oPag.oPRORDMPS_1_25.enabled = this.oPgFrm.Page1.oPag.oPRORDMPS_1_25.mCond()
    this.oPgFrm.Page1.oPag.oPRSAFELT_1_64.enabled = this.oPgFrm.Page1.oPag.oPRSAFELT_1_64.mCond()
    this.oPgFrm.Page1.oPag.oPRPERSCA_1_66.enabled = this.oPgFrm.Page1.oPag.oPRPERSCA_1_66.mCond()
    this.oPgFrm.Page1.oPag.oPRORDPIA_1_68.enabled = this.oPgFrm.Page1.oPag.oPRORDPIA_1_68.mCond()
    this.oPgFrm.Page1.oPag.oPREXPLPH_1_69.enabled = this.oPgFrm.Page1.oPag.oPREXPLPH_1_69.mCond()
    this.oPgFrm.Page1.oPag.oPRPIAPUN_1_70.enabled = this.oPgFrm.Page1.oPag.oPRPIAPUN_1_70.mCond()
    this.oPgFrm.Page1.oPag.oPRPERPIA_1_71.enabled = this.oPgFrm.Page1.oPag.oPRPERPIA_1_71.mCond()
    this.oPgFrm.Page1.oPag.oPRTIPDTF_1_74.enabled = this.oPgFrm.Page1.oPag.oPRTIPDTF_1_74.mCond()
    this.oPgFrm.Page1.oPag.oPRGIODTF_1_82.enabled = this.oPgFrm.Page1.oPag.oPRGIODTF_1_82.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(4).enabled=not(g_PRFA<>'S')
    this.oPgFrm.Page1.oPag.oPRCOERSS_1_18.visible=!this.oPgFrm.Page1.oPag.oPRCOERSS_1_18.mHide()
    this.oPgFrm.Page1.oPag.oPRCOEFLT_1_22.visible=!this.oPgFrm.Page1.oPag.oPRCOEFLT_1_22.mHide()
    this.oPgFrm.Page1.oPag.oPRORDMPS_1_25.visible=!this.oPgFrm.Page1.oPag.oPRORDMPS_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oPRORDPIA_1_68.visible=!this.oPgFrm.Page1.oPag.oPRORDPIA_1_68.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PRCODPRO
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PRCODPRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PRTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PRTIPCON;
                     ,'ANCODICE',trim(this.w_PRCODPRO))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODPRO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PRCODPRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PRTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PRCODPRO)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PRTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PRCODPRO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPRCODPRO_1_30'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PRTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Produttore inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PRTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PRCODPRO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PRTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PRTIPCON;
                       ,'ANCODICE',this.w_PRCODPRO)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODPRO = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPRO = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODPRO = space(15)
      endif
      this.w_DESPRO = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Produttore inesistente o obsoleto")
        endif
        this.w_PRCODPRO = space(15)
        this.w_DESPRO = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_30.ANCODICE as ANCODICE130"+ ",link_1_30.ANDESCRI as ANDESCRI130"+ ",link_1_30.ANDTOBSO as ANDTOBSO130"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_30 on PAR_RIOR.PRCODPRO=link_1_30.ANCODICE"+" and PAR_RIOR.PRTIPCON=link_1_30.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_30"
          i_cKey=i_cKey+'+" and PAR_RIOR.PRCODPRO=link_1_30.ANCODICE(+)"'+'+" and PAR_RIOR.PRTIPCON=link_1_30.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCODFOR
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PRCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PRTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PRTIPCON;
                     ,'ANCODICE',trim(this.w_PRCODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PRCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PRTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PRCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PRTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PRCODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPRCODFOR_1_32'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PRTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PRTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PRCODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PRTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PRTIPCON;
                       ,'ANCODICE',this.w_PRCODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODFOR = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        endif
        this.w_PRCODFOR = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_32.ANCODICE as ANCODICE132"+ ",link_1_32.ANDESCRI as ANDESCRI132"+ ",link_1_32.ANDTOBSO as ANDTOBSO132"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_32 on PAR_RIOR.PRCODFOR=link_1_32.ANCODICE"+" and PAR_RIOR.PRTIPCON=link_1_32.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_32"
          i_cKey=i_cKey+'+" and PAR_RIOR.PRCODFOR=link_1_32.ANCODICE(+)"'+'+" and PAR_RIOR.PRTIPCON=link_1_32.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRLISCOS
  func Link_3_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRLISCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRLISCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_PRLISCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_PRLISCOS)
            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRLISCOS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PRLISCOS = space(5)
      endif
      this.w_DESLIS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRLISCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_7.LSCODLIS as LSCODLIS307"+ ",link_3_7.LSDESLIS as LSDESLIS307"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_7 on PAR_RIOR.PRLISCOS=link_3_7.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_7"
          i_cKey=i_cKey+'+" and PAR_RIOR.PRLISCOS=link_3_7.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUUTEELA
  func Link_3_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUUTEELA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUUTEELA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PUUTEELA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PUUTEELA)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUUTEELA = NVL(_Link_.CODE,0)
      this.w_UTEDES = NVL(_Link_.NAME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PUUTEELA = 0
      endif
      this.w_UTEDES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUUTEELA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRLISCOS
  func Link_4_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRLISCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRLISCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_PRLISCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_PRLISCOS)
            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRLISCOS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PRLISCOS = space(5)
      endif
      this.w_DESLIS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRLISCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_63(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_63.LSCODLIS as LSCODLIS463"+ ",link_4_63.LSDESLIS as LSDESLIS463"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_63 on PAR_RIOR.PRLISCOS=link_4_63.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_63"
          i_cKey=i_cKey+'+" and PAR_RIOR.PRLISCOS=link_4_63.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUUTEELA
  func Link_4_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUUTEELA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUUTEELA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PUUTEELA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PUUTEELA)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUUTEELA = NVL(_Link_.CODE,0)
      this.w_UTEDES = NVL(_Link_.NAME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PUUTEELA = 0
      endif
      this.w_UTEDES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUUTEELA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPRCOSSTA_1_7.value==this.w_PRCOSSTA)
      this.oPgFrm.Page1.oPag.oPRCOSSTA_1_7.value=this.w_PRCOSSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oPRQTAMIN_1_8.value==this.w_PRQTAMIN)
      this.oPgFrm.Page1.oPag.oPRQTAMIN_1_8.value=this.w_PRQTAMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPRQTAMAX_1_9.value==this.w_PRQTAMAX)
      this.oPgFrm.Page1.oPag.oPRQTAMAX_1_9.value=this.w_PRQTAMAX
    endif
    if not(this.oPgFrm.Page1.oPag.oPRLOTRIO_1_10.value==this.w_PRLOTRIO)
      this.oPgFrm.Page1.oPag.oPRLOTRIO_1_10.value=this.w_PRLOTRIO
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPUNRIO_1_11.value==this.w_PRPUNRIO)
      this.oPgFrm.Page1.oPag.oPRPUNRIO_1_11.value=this.w_PRPUNRIO
    endif
    if not(this.oPgFrm.Page1.oPag.oPRGIOINV_1_12.value==this.w_PRGIOINV)
      this.oPgFrm.Page1.oPag.oPRGIOINV_1_12.value=this.w_PRGIOINV
    endif
    if not(this.oPgFrm.Page1.oPag.oPRLEAMPS_1_13.value==this.w_PRLEAMPS)
      this.oPgFrm.Page1.oPag.oPRLEAMPS_1_13.value=this.w_PRLEAMPS
    endif
    if not(this.oPgFrm.Page1.oPag.oPRLOWLEV_1_14.value==this.w_PRLOWLEV)
      this.oPgFrm.Page1.oPag.oPRLOWLEV_1_14.value=this.w_PRLOWLEV
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGGMPS_1_15.RadioValue()==this.w_PROGGMPS)
      this.oPgFrm.Page1.oPag.oPROGGMPS_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRGIOCOP_1_16.value==this.w_PRGIOCOP)
      this.oPgFrm.Page1.oPag.oPRGIOCOP_1_16.value=this.w_PRGIOCOP
    endif
    if not(this.oPgFrm.Page1.oPag.oPRSCOMIN_1_17.value==this.w_PRSCOMIN)
      this.oPgFrm.Page1.oPag.oPRSCOMIN_1_17.value=this.w_PRSCOMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCOERSS_1_18.value==this.w_PRCOERSS)
      this.oPgFrm.Page1.oPag.oPRCOERSS_1_18.value=this.w_PRCOERSS
    endif
    if not(this.oPgFrm.Page1.oPag.oPRSCOMAX_1_19.value==this.w_PRSCOMAX)
      this.oPgFrm.Page1.oPag.oPRSCOMAX_1_19.value=this.w_PRSCOMAX
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDISMIN_1_20.value==this.w_PRDISMIN)
      this.oPgFrm.Page1.oPag.oPRDISMIN_1_20.value=this.w_PRDISMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPRGIOAPP_1_21.value==this.w_PRGIOAPP)
      this.oPgFrm.Page1.oPag.oPRGIOAPP_1_21.value=this.w_PRGIOAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCOEFLT_1_22.value==this.w_PRCOEFLT)
      this.oPgFrm.Page1.oPag.oPRCOEFLT_1_22.value=this.w_PRCOEFLT
    endif
    if not(this.oPgFrm.Page1.oPag.oPRLOTMED_1_23.value==this.w_PRLOTMED)
      this.oPgFrm.Page1.oPag.oPRLOTMED_1_23.value=this.w_PRLOTMED
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_24.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_24.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPRORDMPS_1_25.RadioValue()==this.w_PRORDMPS)
      this.oPgFrm.Page1.oPag.oPRORDMPS_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODPRO_1_30.value==this.w_PRCODPRO)
      this.oPgFrm.Page1.oPag.oPRCODPRO_1_30.value=this.w_PRCODPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODFOR_1_32.value==this.w_PRCODFOR)
      this.oPgFrm.Page1.oPag.oPRCODFOR_1_32.value=this.w_PRCODFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_39.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_39.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_40.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_40.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_41.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_41.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_42.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_42.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_43.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_43.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_44.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_44.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRO_1_46.value==this.w_DESPRO)
      this.oPgFrm.Page1.oPag.oDESPRO_1_46.value=this.w_DESPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_47.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_47.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oPRLISCOS_3_7.value==this.w_PRLISCOS)
      this.oPgFrm.Page3.oPag.oPRLISCOS_3_7.value=this.w_PRLISCOS
    endif
    if not(this.oPgFrm.Page3.oPag.oDESLIS_3_8.value==this.w_DESLIS)
      this.oPgFrm.Page3.oPag.oDESLIS_3_8.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page3.oPag.oPRESECOS_3_10.value==this.w_PRESECOS)
      this.oPgFrm.Page3.oPag.oPRESECOS_3_10.value=this.w_PRESECOS
    endif
    if not(this.oPgFrm.Page3.oPag.oPRINVCOS_3_11.value==this.w_PRINVCOS)
      this.oPgFrm.Page3.oPag.oPRINVCOS_3_11.value=this.w_PRINVCOS
    endif
    if not(this.oPgFrm.Page3.oPag.oPUUTEELA_3_14.value==this.w_PUUTEELA)
      this.oPgFrm.Page3.oPag.oPUUTEELA_3_14.value=this.w_PUUTEELA
    endif
    if not(this.oPgFrm.Page3.oPag.oUTEDES_3_15.value==this.w_UTEDES)
      this.oPgFrm.Page3.oPag.oUTEDES_3_15.value=this.w_UTEDES
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_61.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_61.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPRSAFELT_1_64.value==this.w_PRSAFELT)
      this.oPgFrm.Page1.oPag.oPRSAFELT_1_64.value=this.w_PRSAFELT
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPERSCA_1_66.value==this.w_PRPERSCA)
      this.oPgFrm.Page1.oPag.oPRPERSCA_1_66.value=this.w_PRPERSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oPRORDPIA_1_68.RadioValue()==this.w_PRORDPIA)
      this.oPgFrm.Page1.oPag.oPRORDPIA_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPREXPLPH_1_69.RadioValue()==this.w_PREXPLPH)
      this.oPgFrm.Page1.oPag.oPREXPLPH_1_69.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPIAPUN_1_70.RadioValue()==this.w_PRPIAPUN)
      this.oPgFrm.Page1.oPag.oPRPIAPUN_1_70.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPERPIA_1_71.RadioValue()==this.w_PRPERPIA)
      this.oPgFrm.Page1.oPag.oPRPERPIA_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRTIPDTF_1_74.RadioValue()==this.w_PRTIPDTF)
      this.oPgFrm.Page1.oPag.oPRTIPDTF_1_74.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDOMMED_1_75.value==this.w_PRDOMMED)
      this.oPgFrm.Page1.oPag.oPRDOMMED_1_75.value=this.w_PRDOMMED
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCSLAVO_4_1.value==this.w_PRCSLAVO)
      this.oPgFrm.Page4.oPag.oPRCSLAVO_4_1.value=this.w_PRCSLAVO
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCSMATE_4_2.value==this.w_PRCSMATE)
      this.oPgFrm.Page4.oPag.oPRCSMATE_4_2.value=this.w_PRCSMATE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCSSPGE_4_3.value==this.w_PRCSSPGE)
      this.oPgFrm.Page4.oPag.oPRCSSPGE_4_3.value=this.w_PRCSSPGE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCULAVO_4_4.value==this.w_PRCULAVO)
      this.oPgFrm.Page4.oPag.oPRCULAVO_4_4.value=this.w_PRCULAVO
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCUMATE_4_5.value==this.w_PRCUMATE)
      this.oPgFrm.Page4.oPag.oPRCUMATE_4_5.value=this.w_PRCUMATE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCUSPGE_4_6.value==this.w_PRCUSPGE)
      this.oPgFrm.Page4.oPag.oPRCUSPGE_4_6.value=this.w_PRCUSPGE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCMLAVO_4_7.value==this.w_PRCMLAVO)
      this.oPgFrm.Page4.oPag.oPRCMLAVO_4_7.value=this.w_PRCMLAVO
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCMMATE_4_8.value==this.w_PRCMMATE)
      this.oPgFrm.Page4.oPag.oPRCMMATE_4_8.value=this.w_PRCMMATE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCMSPGE_4_9.value==this.w_PRCMSPGE)
      this.oPgFrm.Page4.oPag.oPRCMSPGE_4_9.value=this.w_PRCMSPGE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCLLAVO_4_10.value==this.w_PRCLLAVO)
      this.oPgFrm.Page4.oPag.oPRCLLAVO_4_10.value=this.w_PRCLLAVO
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCLMATE_4_11.value==this.w_PRCLMATE)
      this.oPgFrm.Page4.oPag.oPRCLMATE_4_11.value=this.w_PRCLMATE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCLSPGE_4_12.value==this.w_PRCLSPGE)
      this.oPgFrm.Page4.oPag.oPRCLSPGE_4_12.value=this.w_PRCLSPGE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCSLAVE_4_16.value==this.w_PRCSLAVE)
      this.oPgFrm.Page4.oPag.oPRCSLAVE_4_16.value=this.w_PRCSLAVE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCSLAVC_4_19.value==this.w_PRCSLAVC)
      this.oPgFrm.Page4.oPag.oPRCSLAVC_4_19.value=this.w_PRCSLAVC
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCSMATC_4_20.value==this.w_PRCSMATC)
      this.oPgFrm.Page4.oPag.oPRCSMATC_4_20.value=this.w_PRCSMATC
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCSSPGC_4_21.value==this.w_PRCSSPGC)
      this.oPgFrm.Page4.oPag.oPRCSSPGC_4_21.value=this.w_PRCSSPGC
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCULAVC_4_22.value==this.w_PRCULAVC)
      this.oPgFrm.Page4.oPag.oPRCULAVC_4_22.value=this.w_PRCULAVC
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCUMATC_4_23.value==this.w_PRCUMATC)
      this.oPgFrm.Page4.oPag.oPRCUMATC_4_23.value=this.w_PRCUMATC
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCUSPGC_4_24.value==this.w_PRCUSPGC)
      this.oPgFrm.Page4.oPag.oPRCUSPGC_4_24.value=this.w_PRCUSPGC
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCMLAVC_4_25.value==this.w_PRCMLAVC)
      this.oPgFrm.Page4.oPag.oPRCMLAVC_4_25.value=this.w_PRCMLAVC
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCMMATC_4_26.value==this.w_PRCMMATC)
      this.oPgFrm.Page4.oPag.oPRCMMATC_4_26.value=this.w_PRCMMATC
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCMSPGC_4_27.value==this.w_PRCMSPGC)
      this.oPgFrm.Page4.oPag.oPRCMSPGC_4_27.value=this.w_PRCMSPGC
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCLLAVC_4_28.value==this.w_PRCLLAVC)
      this.oPgFrm.Page4.oPag.oPRCLLAVC_4_28.value=this.w_PRCLLAVC
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCLMATC_4_29.value==this.w_PRCLMATC)
      this.oPgFrm.Page4.oPag.oPRCLMATC_4_29.value=this.w_PRCLMATC
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCLSPGC_4_30.value==this.w_PRCLSPGC)
      this.oPgFrm.Page4.oPag.oPRCLSPGC_4_30.value=this.w_PRCLSPGC
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCSLVCE_4_34.value==this.w_PRCSLVCE)
      this.oPgFrm.Page4.oPag.oPRCSLVCE_4_34.value=this.w_PRCSLVCE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCULVCE_4_35.value==this.w_PRCULVCE)
      this.oPgFrm.Page4.oPag.oPRCULVCE_4_35.value=this.w_PRCULVCE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCMLVCE_4_36.value==this.w_PRCMLVCE)
      this.oPgFrm.Page4.oPag.oPRCMLVCE_4_36.value=this.w_PRCMLVCE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCLLVCE_4_37.value==this.w_PRCLLVCE)
      this.oPgFrm.Page4.oPag.oPRCLLVCE_4_37.value=this.w_PRCLLVCE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCULAVE_4_45.value==this.w_PRCULAVE)
      this.oPgFrm.Page4.oPag.oPRCULAVE_4_45.value=this.w_PRCULAVE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCMLAVE_4_46.value==this.w_PRCMLAVE)
      this.oPgFrm.Page4.oPag.oPRCMLAVE_4_46.value=this.w_PRCMLAVE
    endif
    if not(this.oPgFrm.Page4.oPag.oPRCLLAVE_4_47.value==this.w_PRCLLAVE)
      this.oPgFrm.Page4.oPag.oPRCLLAVE_4_47.value=this.w_PRCLLAVE
    endif
    if not(this.oPgFrm.Page4.oPag.oTOTSTAND_4_49.value==this.w_TOTSTAND)
      this.oPgFrm.Page4.oPag.oTOTSTAND_4_49.value=this.w_TOTSTAND
    endif
    if not(this.oPgFrm.Page4.oPag.oTOTULTIM_4_50.value==this.w_TOTULTIM)
      this.oPgFrm.Page4.oPag.oTOTULTIM_4_50.value=this.w_TOTULTIM
    endif
    if not(this.oPgFrm.Page4.oPag.oTOTMEDIO_4_51.value==this.w_TOTMEDIO)
      this.oPgFrm.Page4.oPag.oTOTMEDIO_4_51.value=this.w_TOTMEDIO
    endif
    if not(this.oPgFrm.Page4.oPag.oTOTLISTI_4_52.value==this.w_TOTLISTI)
      this.oPgFrm.Page4.oPag.oTOTLISTI_4_52.value=this.w_TOTLISTI
    endif
    if not(this.oPgFrm.Page4.oPag.oPRDATCOS_4_54.value==this.w_PRDATCOS)
      this.oPgFrm.Page4.oPag.oPRDATCOS_4_54.value=this.w_PRDATCOS
    endif
    if not(this.oPgFrm.Page4.oPag.oPRLOTSTD_4_55.value==this.w_PRLOTSTD)
      this.oPgFrm.Page4.oPag.oPRLOTSTD_4_55.value=this.w_PRLOTSTD
    endif
    if not(this.oPgFrm.Page4.oPag.oPRDATULT_4_56.value==this.w_PRDATULT)
      this.oPgFrm.Page4.oPag.oPRDATULT_4_56.value=this.w_PRDATULT
    endif
    if not(this.oPgFrm.Page4.oPag.oPRLOTULT_4_57.value==this.w_PRLOTULT)
      this.oPgFrm.Page4.oPag.oPRLOTULT_4_57.value=this.w_PRLOTULT
    endif
    if not(this.oPgFrm.Page4.oPag.oPRDATMED_4_58.value==this.w_PRDATMED)
      this.oPgFrm.Page4.oPag.oPRDATMED_4_58.value=this.w_PRDATMED
    endif
    if not(this.oPgFrm.Page4.oPag.oPRLOTMED_4_59.value==this.w_PRLOTMED)
      this.oPgFrm.Page4.oPag.oPRLOTMED_4_59.value=this.w_PRLOTMED
    endif
    if not(this.oPgFrm.Page4.oPag.oPRDATLIS_4_60.value==this.w_PRDATLIS)
      this.oPgFrm.Page4.oPag.oPRDATLIS_4_60.value=this.w_PRDATLIS
    endif
    if not(this.oPgFrm.Page4.oPag.oPRLOTLIS_4_61.value==this.w_PRLOTLIS)
      this.oPgFrm.Page4.oPag.oPRLOTLIS_4_61.value=this.w_PRLOTLIS
    endif
    if not(this.oPgFrm.Page4.oPag.oPRLISCOS_4_63.value==this.w_PRLISCOS)
      this.oPgFrm.Page4.oPag.oPRLISCOS_4_63.value=this.w_PRLISCOS
    endif
    if not(this.oPgFrm.Page4.oPag.oDESLIS_4_64.value==this.w_DESLIS)
      this.oPgFrm.Page4.oPag.oDESLIS_4_64.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page4.oPag.oPRESECOS_4_66.value==this.w_PRESECOS)
      this.oPgFrm.Page4.oPag.oPRESECOS_4_66.value=this.w_PRESECOS
    endif
    if not(this.oPgFrm.Page4.oPag.oPRINVCOS_4_67.value==this.w_PRINVCOS)
      this.oPgFrm.Page4.oPag.oPRINVCOS_4_67.value=this.w_PRINVCOS
    endif
    if not(this.oPgFrm.Page4.oPag.oPUUTEELA_4_70.value==this.w_PUUTEELA)
      this.oPgFrm.Page4.oPag.oPUUTEELA_4_70.value=this.w_PUUTEELA
    endif
    if not(this.oPgFrm.Page4.oPag.oUTEDES_4_71.value==this.w_UTEDES)
      this.oPgFrm.Page4.oPag.oUTEDES_4_71.value=this.w_UTEDES
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTSTAND_3_17.value==this.w_TOTSTAND)
      this.oPgFrm.Page3.oPag.oTOTSTAND_3_17.value=this.w_TOTSTAND
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTULTIM_3_18.value==this.w_TOTULTIM)
      this.oPgFrm.Page3.oPag.oTOTULTIM_3_18.value=this.w_TOTULTIM
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTMEDIO_3_19.value==this.w_TOTMEDIO)
      this.oPgFrm.Page3.oPag.oTOTMEDIO_3_19.value=this.w_TOTMEDIO
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTLISTI_3_20.value==this.w_TOTLISTI)
      this.oPgFrm.Page3.oPag.oTOTLISTI_3_20.value=this.w_TOTLISTI
    endif
    if not(this.oPgFrm.Page3.oPag.oPRDATCOS_3_22.value==this.w_PRDATCOS)
      this.oPgFrm.Page3.oPag.oPRDATCOS_3_22.value=this.w_PRDATCOS
    endif
    if not(this.oPgFrm.Page3.oPag.oPRLOTSTD_3_23.value==this.w_PRLOTSTD)
      this.oPgFrm.Page3.oPag.oPRLOTSTD_3_23.value=this.w_PRLOTSTD
    endif
    if not(this.oPgFrm.Page3.oPag.oPRDATULT_3_24.value==this.w_PRDATULT)
      this.oPgFrm.Page3.oPag.oPRDATULT_3_24.value=this.w_PRDATULT
    endif
    if not(this.oPgFrm.Page3.oPag.oPRLOTULT_3_25.value==this.w_PRLOTULT)
      this.oPgFrm.Page3.oPag.oPRLOTULT_3_25.value=this.w_PRLOTULT
    endif
    if not(this.oPgFrm.Page3.oPag.oPRDATMED_3_26.value==this.w_PRDATMED)
      this.oPgFrm.Page3.oPag.oPRDATMED_3_26.value=this.w_PRDATMED
    endif
    if not(this.oPgFrm.Page3.oPag.oPRLOTMED_3_27.value==this.w_PRLOTMED)
      this.oPgFrm.Page3.oPag.oPRLOTMED_3_27.value=this.w_PRLOTMED
    endif
    if not(this.oPgFrm.Page3.oPag.oPRDATLIS_3_28.value==this.w_PRDATLIS)
      this.oPgFrm.Page3.oPag.oPRDATLIS_3_28.value=this.w_PRDATLIS
    endif
    if not(this.oPgFrm.Page3.oPag.oPRLOTLIS_3_29.value==this.w_PRLOTLIS)
      this.oPgFrm.Page3.oPag.oPRLOTLIS_3_29.value=this.w_PRLOTLIS
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCSLAVO_3_31.value==this.w_TOTCSLAVO)
      this.oPgFrm.Page3.oPag.oTOTCSLAVO_3_31.value=this.w_TOTCSLAVO
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCULAVO_3_32.value==this.w_TOTCULAVO)
      this.oPgFrm.Page3.oPag.oTOTCULAVO_3_32.value=this.w_TOTCULAVO
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCMLAVO_3_33.value==this.w_TOTCMLAVO)
      this.oPgFrm.Page3.oPag.oTOTCMLAVO_3_33.value=this.w_TOTCMLAVO
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCLLAVO_3_34.value==this.w_TOTCLLAVO)
      this.oPgFrm.Page3.oPag.oTOTCLLAVO_3_34.value=this.w_TOTCLLAVO
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCSLAVE_3_35.value==this.w_TOTCSLAVE)
      this.oPgFrm.Page3.oPag.oTOTCSLAVE_3_35.value=this.w_TOTCSLAVE
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCULAVE_3_36.value==this.w_TOTCULAVE)
      this.oPgFrm.Page3.oPag.oTOTCULAVE_3_36.value=this.w_TOTCULAVE
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCMLAVE_3_37.value==this.w_TOTCMLAVE)
      this.oPgFrm.Page3.oPag.oTOTCMLAVE_3_37.value=this.w_TOTCMLAVE
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCLLAVE_3_38.value==this.w_TOTCLLAVE)
      this.oPgFrm.Page3.oPag.oTOTCLLAVE_3_38.value=this.w_TOTCLLAVE
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCSMATE_3_39.value==this.w_TOTCSMATE)
      this.oPgFrm.Page3.oPag.oTOTCSMATE_3_39.value=this.w_TOTCSMATE
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCUMATE_3_40.value==this.w_TOTCUMATE)
      this.oPgFrm.Page3.oPag.oTOTCUMATE_3_40.value=this.w_TOTCUMATE
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCMMATE_3_41.value==this.w_TOTCMMATE)
      this.oPgFrm.Page3.oPag.oTOTCMMATE_3_41.value=this.w_TOTCMMATE
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCLMATE_3_42.value==this.w_TOTCLMATE)
      this.oPgFrm.Page3.oPag.oTOTCLMATE_3_42.value=this.w_TOTCLMATE
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCSSPGE_3_43.value==this.w_TOTCSSPGE)
      this.oPgFrm.Page3.oPag.oTOTCSSPGE_3_43.value=this.w_TOTCSSPGE
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCUSPGE_3_44.value==this.w_TOTCUSPGE)
      this.oPgFrm.Page3.oPag.oTOTCUSPGE_3_44.value=this.w_TOTCUSPGE
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCMSPGE_3_45.value==this.w_TOTCMSPGE)
      this.oPgFrm.Page3.oPag.oTOTCMSPGE_3_45.value=this.w_TOTCMSPGE
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTCLSPGE_3_46.value==this.w_TOTCLSPGE)
      this.oPgFrm.Page3.oPag.oTOTCLSPGE_3_46.value=this.w_TOTCLSPGE
    endif
    if not(this.oPgFrm.Page1.oPag.oPRGIODTF_1_82.value==this.w_PRGIODTF)
      this.oPgFrm.Page1.oPag.oPRGIODTF_1_82.value=this.w_PRGIODTF
    endif
    cp_SetControlsValueExtFlds(this,'PAR_RIOR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(g_PROD<>'S' OR .w_TIPGES<>'S' OR .w_PRLOTRIO>0)  and (.w_FLCOMM<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRLOTRIO_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Indicare il lotto di riordino per gli articoli gestiti a scorta")
          case   not(g_PROD<>'S' OR .w_TIPGES<>'S' OR .w_PRPUNRIO>0)  and (.w_TIPGES $ 'CS' AND .w_FLCOMM<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRPUNRIO_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Indicare il punto di riordino per gli articoli gestiti a scorta")
          case   not(.w_PRGIOCOP>=0)  and (.w_TIPGES = 'F' and .w_FLCOMM<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRGIOCOP_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PRSCOMIN<=.w_PRSCOMAX OR .w_PRSCOMAX=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRSCOMIN_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PRCOERSS>=0 and .w_PRCOERSS<=1)  and not(.w_TIPGES <> 'F' or ! .w_PRSCOMIN>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRCOERSS_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore compreso tra zero e uno")
          case   not(.w_PRSCOMIN<=.w_PRSCOMAX OR .w_PRSCOMAX=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRSCOMAX_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PRCOEFLT>=0 and .w_PRCOEFLT<=1)  and not(NOT (.w_PROVEN='I' AND NOT EMPTY(.w_CODDIS)))  and (.w_PRGIOAPP>0 AND .w_PROVEN='I' AND NOT EMPTY(.w_CODDIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRCOEFLT_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Indicare un valore tra 0 e 1")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PRCODPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRCODPRO_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Produttore inesistente o obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PRCODFOR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRCODFOR_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Fornitore inesistente o obsoleto")
          case   not(.w_PRSAFELT>=0)  and (!.w_TIPGES$"SI" and .w_PROGGMPS='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRSAFELT_1_64.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSMA_MPR.CheckForm()
      if i_bres
        i_bres=  .GSMA_MPR.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPART = this.w_TIPART
    this.o_PROGGMPS = this.w_PROGGMPS
    this.o_FLCOMM = this.w_FLCOMM
    this.o_PREXPLPH = this.w_PREXPLPH
    this.o_PRINTRIO = this.w_PRINTRIO
    this.o_PRDATPAR = this.w_PRDATPAR
    * --- GSMA_MPR : Depends On
    this.GSMA_MPR.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsma_aprPag1 as StdContainer
  Width  = 725
  height = 396
  stdWidth  = 725
  stdheight = 396
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPRCOSSTA_1_7 as StdField with uid="HZVINCHBOG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PRCOSSTA", cQueryName = "PRCOSSTA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo standard (espresso nella moneta di conto)",;
    HelpContextID = 251202615,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=134, Top=32, cSayPict="v_PU(18)", cGetPict="v_GU(18)"

  add object oPRQTAMIN_1_8 as StdField with uid="QRCNDXMTLH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PRQTAMIN", cQueryName = "PRQTAMIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� minima di riordino dell'articolo",;
    HelpContextID = 132049988,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=134, Top=56, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  func oPRQTAMIN_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES $ 'CF' AND .w_FLCOMM<>'S')
    endwith
   endif
  endfunc

  add object oPRQTAMAX_1_9 as StdField with uid="ZPOPGJLZHL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PRQTAMAX", cQueryName = "PRQTAMAX",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� massima di riordino dell'articolo",;
    HelpContextID = 136385458,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=134, Top=80, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  func oPRQTAMAX_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCOMM<>'S')
    endwith
   endif
  endfunc

  add object oPRLOTRIO_1_10 as StdField with uid="TYIDYHWYSW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PRLOTRIO", cQueryName = "PRLOTRIO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Indicare il lotto di riordino per gli articoli gestiti a scorta",;
    ToolTipText = "Lotto di riordino dell'articolo",;
    HelpContextID = 235510853,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=134, Top=104, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  func oPRLOTRIO_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCOMM<>'S')
    endwith
   endif
  endfunc

  func oPRLOTRIO_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (g_PROD<>'S' OR .w_TIPGES<>'S' OR .w_PRLOTRIO>0)
    endwith
    return bRes
  endfunc

  add object oPRPUNRIO_1_11 as StdField with uid="GHFTGNEMDQ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PRPUNRIO", cQueryName = "PRPUNRIO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Indicare il punto di riordino per gli articoli gestiti a scorta",;
    ToolTipText = "Punto di riordino dell'articolo",;
    HelpContextID = 229628997,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=134, Top=128, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  func oPRPUNRIO_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES $ 'CS' AND .w_FLCOMM<>'S')
    endwith
   endif
  endfunc

  func oPRPUNRIO_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (g_PROD<>'S' OR .w_TIPGES<>'S' OR .w_PRPUNRIO>0)
    endwith
    return bRes
  endfunc

  add object oPRGIOINV_1_12 as StdField with uid="NTKYLOGNXG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PRGIOINV", cQueryName = "PRGIOINV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero massimo di giorni dopo i quali l'articolo compare nella stampa invenduto",;
    HelpContextID = 189576116,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=134, Top=152, cSayPict='"999"', cGetPict='"999"'

  add object oPRLEAMPS_1_13 as StdField with uid="AGXYVQHTEJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PRLEAMPS", cQueryName = "PRLEAMPS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lead-time globale in giorni",;
    HelpContextID = 137388983,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=312, Top=152

  func oPRLEAMPS_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODDIS))
    endwith
   endif
  endfunc

  add object oPRLOWLEV_1_14 as StdField with uid="SCYQXUHXMR",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PRLOWLEV", cQueryName = "PRLOWLEV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Low level code",;
    HelpContextID = 137993292,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=134, Top=176

  add object oPROGGMPS_1_15 as StdRadio with uid="EJYOKLZONH",rtseq=15,rtrep=.f.,left=134, top=201, width=95,height=51;
    , tabstop=.f.;
    , ToolTipText = "Sistema d'ordine";
    , cFormVar="w_PROGGMPS", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPROGGMPS_1_15.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Oggetto MPS"
      this.Buttons(1).HelpContextID = 130954167
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Ricambio"
      this.Buttons(2).HelpContextID = 130954167
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Nessuno"
      this.Buttons(3).HelpContextID = 130954167
      this.Buttons(3).Top=32
      this.SetAll("Width",93)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Sistema d'ordine")
      StdRadio::init()
    endproc

  func oPROGGMPS_1_15.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"R",;
    iif(this.value =3,"N",;
    space(1)))))
  endfunc
  func oPROGGMPS_1_15.GetRadio()
    this.Parent.oContained.w_PROGGMPS = this.RadioValue()
    return .t.
  endfunc

  func oPROGGMPS_1_15.SetRadio()
    this.Parent.oContained.w_PROGGMPS=trim(this.Parent.oContained.w_PROGGMPS)
    this.value = ;
      iif(this.Parent.oContained.w_PROGGMPS=="S",1,;
      iif(this.Parent.oContained.w_PROGGMPS=="R",2,;
      iif(this.Parent.oContained.w_PROGGMPS=="N",3,;
      0)))
  endfunc

  add object oPRGIOCOP_1_16 as StdField with uid="LHXBMMTHPB",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PRGIOCOP", cQueryName = "PRGIOCOP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni di copertura",;
    HelpContextID = 21803962,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=312, Top=176, cSayPict='"999"', cGetPict='"999"'

  func oPRGIOCOP_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES = 'F' and .w_FLCOMM<>'S')
    endwith
   endif
  endfunc

  func oPRGIOCOP_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRGIOCOP>=0)
    endwith
    return bRes
  endfunc

  add object oPRSCOMIN_1_17 as StdField with uid="GDKXGEEBTB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PRSCOMIN", cQueryName = "PRSCOMIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Scorta minima dell'articolo",;
    HelpContextID = 145624132,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=499, Top=32, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  func oPRSCOMIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRSCOMIN<=.w_PRSCOMAX OR .w_PRSCOMAX=0)
    endwith
    return bRes
  endfunc

  add object oPRCOERSS_1_18 as StdField with uid="QYMTXMPAOD",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PRCOERSS", cQueryName = "PRCOERSS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore compreso tra zero e uno",;
    ToolTipText = "Coeff. per la determinazione della data di consegna per il reintegro della SS",;
    HelpContextID = 219745353,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=566, Top=56, cSayPict='"9.99"', cGetPict='"9.99"'

  func oPRCOERSS_1_18.mHide()
    with this.Parent.oContained
      return (.w_TIPGES <> 'F' or ! .w_PRSCOMIN>0)
    endwith
  endfunc

  func oPRCOERSS_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRCOERSS>=0 and .w_PRCOERSS<=1)
    endwith
    return bRes
  endfunc

  add object oPRSCOMAX_1_19 as StdField with uid="MLUVBUTNPG",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PRSCOMAX", cQueryName = "PRSCOMAX",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Scorta massima dell'articolo",;
    HelpContextID = 122811314,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=499, Top=80, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  func oPRSCOMAX_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRSCOMIN<=.w_PRSCOMAX OR .w_PRSCOMAX=0)
    endwith
    return bRes
  endfunc

  add object oPRDISMIN_1_20 as StdField with uid="QVDUMLARJQ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PRDISMIN", cQueryName = "PRDISMIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Disponibilit� minima dell'articolo",;
    HelpContextID = 150150212,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=499, Top=104, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oPRGIOAPP_1_21 as StdField with uid="QKQKEJIXYU",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PRGIOAPP", cQueryName = "PRGIOAPP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di giorni necessario all'approvvigionamento dell'articolo",;
    HelpContextID = 55358394,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=499, Top=128

  add object oPRCOEFLT_1_22 as StdField with uid="VTNWXROFNH",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PRCOEFLT", cQueryName = "PRCOEFLT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Indicare un valore tra 0 e 1",;
    ToolTipText = "Coefficiente per calcolo del lead-time variabile",;
    HelpContextID = 250016694,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=499, Top=152, cSayPict='"9.9999"', cGetPict='"9.9999"'

  func oPRCOEFLT_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRGIOAPP>0 AND .w_PROVEN='I' AND NOT EMPTY(.w_CODDIS))
    endwith
   endif
  endfunc

  func oPRCOEFLT_1_22.mHide()
    with this.Parent.oContained
      return (NOT (.w_PROVEN='I' AND NOT EMPTY(.w_CODDIS)))
    endwith
  endfunc

  func oPRCOEFLT_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRCOEFLT>=0 and .w_PRCOEFLT<=1)
    endwith
    return bRes
  endfunc

  add object oPRLOTMED_1_23 as StdField with uid="MCKGDFLNHV",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PRLOTMED", cQueryName = "PRLOTMED",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lotto medio di produzione utilizzato per il calcolo del lead time di produzione",;
    HelpContextID = 151624762,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=499, Top=176, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oUNIMIS_1_24 as StdField with uid="TFBQRTPEFY",rtseq=24,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 27826106,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=618, Top=176, InputMask=replicate('X',3)


  add object oPRORDMPS_1_25 as StdCombo with uid="IWGXFLDAAX",rtseq=25,rtrep=.f.,left=499,top=201,width=120,height=21;
    , ToolTipText = "Stato dell'ordine generato dall'MPS";
    , HelpContextID = 133378999;
    , cFormVar="w_PRORDMPS",RowSource=""+"Suggerito,"+"Confermato,"+"Da pianificare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRORDMPS_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    iif(this.value =3,'P',;
    space(1)))))
  endfunc
  func oPRORDMPS_1_25.GetRadio()
    this.Parent.oContained.w_PRORDMPS = this.RadioValue()
    return .t.
  endfunc

  func oPRORDMPS_1_25.SetRadio()
    this.Parent.oContained.w_PRORDMPS=trim(this.Parent.oContained.w_PRORDMPS)
    this.value = ;
      iif(this.Parent.oContained.w_PRORDMPS=='S',1,;
      iif(this.Parent.oContained.w_PRORDMPS=='C',2,;
      iif(this.Parent.oContained.w_PRORDMPS=='P',3,;
      0)))
  endfunc

  func oPRORDMPS_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PROGGMPS<>'N' and .w_TIPGES ="F")
    endwith
   endif
  endfunc

  func oPRORDMPS_1_25.mHide()
    with this.Parent.oContained
      return (.w_PROGGMPS='N')
    endwith
  endfunc

  add object oPRCODPRO_1_30 as StdField with uid="BWEOSBDQZJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PRCODPRO", cQueryName = "PRCODPRO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Produttore inesistente o obsoleto",;
    ToolTipText = "Codice del produttore abituale",;
    HelpContextID = 185142341,;
   bGlobalFont=.t.,;
    Height=21, Width=138, Left=134, Top=306, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PRTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PRCODPRO"

  func oPRCODPRO_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODPRO_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCODPRO_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PRTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PRTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPRCODPRO_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oPRCODPRO_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PRTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PRCODPRO
     i_obj.ecpSave()
  endproc

  add object oPRCODFOR_1_32 as StdField with uid="YKPCIKLAKU",rtseq=29,rtrep=.f.,;
    cFormVar = "w_PRCODFOR", cQueryName = "PRCODFOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Fornitore inesistente o obsoleto",;
    ToolTipText = "Codice del fornitore abituale",;
    HelpContextID = 251065272,;
   bGlobalFont=.t.,;
    Height=21, Width=138, Left=134, Top=330, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PRTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PRCODFOR"

  func oPRCODFOR_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODFOR_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCODFOR_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PRTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PRTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPRCODFOR_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oPRCODFOR_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PRTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PRCODFOR
     i_obj.ecpSave()
  endproc

  add object oUNIMIS_1_39 as StdField with uid="ACHWFUJJKV",rtseq=30,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 27826106,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=618, Top=32, InputMask=replicate('X',3)

  add object oUNIMIS_1_40 as StdField with uid="SPVSWAIBZA",rtseq=31,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 27826106,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=618, Top=80, InputMask=replicate('X',3)

  add object oUNIMIS_1_41 as StdField with uid="VIEUZDIJJU",rtseq=32,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 27826106,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=618, Top=104, InputMask=replicate('X',3)

  add object oUNIMIS_1_42 as StdField with uid="BCCJJEIOHL",rtseq=33,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 27826106,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=253, Top=56, InputMask=replicate('X',3)

  add object oUNIMIS_1_43 as StdField with uid="TZSYFKMJAS",rtseq=34,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 27826106,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=253, Top=104, InputMask=replicate('X',3)

  add object oUNIMIS_1_44 as StdField with uid="LQUWKRGGUA",rtseq=35,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 27826106,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=253, Top=128, InputMask=replicate('X',3)

  add object oDESPRO_1_46 as StdField with uid="CAFBGIAFSU",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESPRO", cQueryName = "DESPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 85262794,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=277, Top=306, InputMask=replicate('X',40)

  add object oDESFOR_1_47 as StdField with uid="NWNPNKWAHA",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 38732234,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=277, Top=330, InputMask=replicate('X',40)

  add object oUNIMIS_1_61 as StdField with uid="ZKWQJGJMQJ",rtseq=46,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 27826106,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=253, Top=80, InputMask=replicate('X',3)

  add object oPRSAFELT_1_64 as StdField with uid="HQCHLJXLVV",rtseq=47,rtrep=.f.,;
    cFormVar = "w_PRSAFELT", cQueryName = "PRSAFELT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lead-time sicurezza",;
    HelpContextID = 266597302,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=499, Top=226, cSayPict='"999.99"', cGetPict='"999.99"'

  func oPRSAFELT_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_TIPGES$"SI" and .w_PROGGMPS='N')
    endwith
   endif
  endfunc

  func oPRSAFELT_1_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRSAFELT>=0)
    endwith
    return bRes
  endfunc

  add object oPRPERSCA_1_66 as StdField with uid="JRLLCJAQII",rtseq=48,rtrep=.f.,;
    cFormVar = "w_PRPERSCA", cQueryName = "PRPERSCA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "% di scarto di prodotto",;
    HelpContextID = 249551927,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=657, Top=226, cSayPict='"999.99"', cGetPict='"999.99"'

  func oPRPERSCA_1_66.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES$"F-C" and .w_FLCOMM<>'S')
    endwith
   endif
  endfunc


  add object oPRORDPIA_1_68 as StdCombo with uid="YKJRFHCUCZ",rtseq=49,rtrep=.f.,left=499,top=201,width=120,height=21;
    , ToolTipText = "Stato dell'ordine generato dall'MRP";
    , HelpContextID = 185388087;
    , cFormVar="w_PRORDPIA",RowSource=""+"Pianificato,"+"Suggerito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRORDPIA_1_68.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oPRORDPIA_1_68.GetRadio()
    this.Parent.oContained.w_PRORDPIA = this.RadioValue()
    return .t.
  endfunc

  func oPRORDPIA_1_68.SetRadio()
    this.Parent.oContained.w_PRORDPIA=trim(this.Parent.oContained.w_PRORDPIA)
    this.value = ;
      iif(this.Parent.oContained.w_PRORDPIA=='S',1,;
      iif(this.Parent.oContained.w_PRORDPIA=='N',2,;
      0))
  endfunc

  func oPRORDPIA_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MMRP='S' and .w_TIPGES ="F" and .w_PROVEN $ 'I-E-L'  and  .w_PROGGMPS='N')
    endwith
   endif
  endfunc

  func oPRORDPIA_1_68.mHide()
    with this.Parent.oContained
      return (.w_PROGGMPS<>'N')
    endwith
  endfunc


  add object oPREXPLPH_1_69 as StdCombo with uid="TNVABFKXLN",rtseq=50,rtrep=.f.,left=499,top=252,width=149,height=22;
    , ToolTipText = "Modalit� di pianificazione dell'articolo di tipologia phantom (MRP)";
    , HelpContextID = 137221058;
    , cFormVar="w_PREXPLPH",RowSource=""+"Standard,"+"Gestisci nei documenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPREXPLPH_1_69.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'I',;
    space(1))))
  endfunc
  func oPREXPLPH_1_69.GetRadio()
    this.Parent.oContained.w_PREXPLPH = this.RadioValue()
    return .t.
  endfunc

  func oPREXPLPH_1_69.SetRadio()
    this.Parent.oContained.w_PREXPLPH=trim(this.Parent.oContained.w_PREXPLPH)
    this.value = ;
      iif(this.Parent.oContained.w_PREXPLPH=='S',1,;
      iif(this.Parent.oContained.w_PREXPLPH=='I',2,;
      0))
  endfunc

  func oPREXPLPH_1_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PROGGMPS='N' and .w_TIPART='PH')
    endwith
   endif
  endfunc

  add object oPRPIAPUN_1_70 as StdCheck with uid="OHEAKCAMVP",rtseq=51,rtrep=.f.,left=134, top=277, caption="Pianificazione puntuale",;
    ToolTipText = "Se attivo emette ordine per ogni riga di impegno (MRP)",;
    HelpContextID = 181656644,;
    cFormVar="w_PRPIAPUN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPRPIAPUN_1_70.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPRPIAPUN_1_70.GetRadio()
    this.Parent.oContained.w_PRPIAPUN = this.RadioValue()
    return .t.
  endfunc

  func oPRPIAPUN_1_70.SetRadio()
    this.Parent.oContained.w_PRPIAPUN=trim(this.Parent.oContained.w_PRPIAPUN)
    this.value = ;
      iif(this.Parent.oContained.w_PRPIAPUN=='S',1,;
      0)
  endfunc

  func oPRPIAPUN_1_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PROGGMPS='N' and (.w_TIPART<>'PH' or .w_TIPART='PH' and .w_PREXPLPH<>'S'))
    endwith
   endif
  endfunc


  add object oPRPERPIA_1_71 as StdCombo with uid="MIIQBJSFGW",rtseq=52,rtrep=.f.,left=134,top=252,width=138,height=22;
    , ToolTipText = "Indica il periodo di raggruppamento degli ordini (MRP)";
    , HelpContextID = 199220279;
    , cFormVar="w_PRPERPIA",RowSource=""+"Giorno,"+"Settimana,"+"Mese,"+"Trimestre,"+"Quadrimestre,"+"Semestre", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRPERPIA_1_71.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'W',;
    iif(this.value =3,'M',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'F',;
    iif(this.value =6,'S',;
    space(1))))))))
  endfunc
  func oPRPERPIA_1_71.GetRadio()
    this.Parent.oContained.w_PRPERPIA = this.RadioValue()
    return .t.
  endfunc

  func oPRPERPIA_1_71.SetRadio()
    this.Parent.oContained.w_PRPERPIA=trim(this.Parent.oContained.w_PRPERPIA)
    this.value = ;
      iif(this.Parent.oContained.w_PRPERPIA=='D',1,;
      iif(this.Parent.oContained.w_PRPERPIA=='W',2,;
      iif(this.Parent.oContained.w_PRPERPIA=='M',3,;
      iif(this.Parent.oContained.w_PRPERPIA=='Q',4,;
      iif(this.Parent.oContained.w_PRPERPIA=='F',5,;
      iif(this.Parent.oContained.w_PRPERPIA=='S',6,;
      0))))))
  endfunc

  func oPRPERPIA_1_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PROGGMPS='N')
    endwith
   endif
  endfunc


  add object oPRTIPDTF_1_74 as StdCombo with uid="CAWZZMJXMN",rtseq=53,rtrep=.f.,left=499,top=279,width=97,height=22;
    , ToolTipText = "Indica la modalit� di gestione del DTF (demand time fence) dando la possibilit� di gestire il parametro per articolo oppure generico per elaborazione";
    , HelpContextID = 264510524;
    , cFormVar="w_PRTIPDTF",RowSource=""+"Da elab. MPS,"+"Forza DTF", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRTIPDTF_1_74.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oPRTIPDTF_1_74.GetRadio()
    this.Parent.oContained.w_PRTIPDTF = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPDTF_1_74.SetRadio()
    this.Parent.oContained.w_PRTIPDTF=trim(this.Parent.oContained.w_PRTIPDTF)
    this.value = ;
      iif(this.Parent.oContained.w_PRTIPDTF=='E',1,;
      iif(this.Parent.oContained.w_PRTIPDTF=='F',2,;
      0))
  endfunc

  func oPRTIPDTF_1_74.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PROGGMPS<>'N' and g_MMPS='S')
    endwith
   endif
  endfunc

  add object oPRDOMMED_1_75 as StdField with uid="ZKRUJGJGQC",rtseq=54,rtrep=.f.,;
    cFormVar = "w_PRDOMMED", cQueryName = "PRDOMMED",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Domanda media",;
    HelpContextID = 144251962,;
   bGlobalFont=.t.,;
    Height=21, Width=138, Left=134, Top=354, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oPRGIODTF_1_82 as StdField with uid="YTLHQXGZLW",rtseq=136,rtrep=.f.,;
    cFormVar = "w_PRGIODTF", cQueryName = "PRGIODTF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Demand time fence (non considera le previsioni nei prossimi N giorni)",;
    HelpContextID = 263408700,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=667, Top=279

  func oPRGIODTF_1_82.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PROGGMPS<>'N' and g_MMPS='S')
    endwith
   endif
  endfunc

  add object oStr_1_26 as StdString with uid="NLDKXNFDUE",Visible=.t., Left=330, Top=34,;
    Alignment=1, Width=166, Height=15,;
    Caption="Scorta minima:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="GBPOBEYUJI",Visible=.t., Left=330, Top=81,;
    Alignment=1, Width=166, Height=15,;
    Caption="Scorta massima:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="MBHWQYSBNN",Visible=.t., Left=330, Top=107,;
    Alignment=1, Width=166, Height=15,;
    Caption="Disponibilit� minima:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="EHNPXQDONP",Visible=.t., Left=22, Top=107,;
    Alignment=1, Width=109, Height=15,;
    Caption="Lotto di riordino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="OGLXGKVVPI",Visible=.t., Left=22, Top=156,;
    Alignment=1, Width=109, Height=15,;
    Caption="Max.gg.invenduto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="EMFRMXWAAP",Visible=.t., Left=-8, Top=131,;
    Alignment=1, Width=140, Height=15,;
    Caption="Punto di riordino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="ZRLQCCLXTK",Visible=.t., Left=22, Top=34,;
    Alignment=1, Width=109, Height=15,;
    Caption="Costo standard:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="YQHXXVOTYH",Visible=.t., Left=9, Top=7,;
    Alignment=0, Width=306, Height=15,;
    Caption="Dati validi per tutti i magazzini"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_48 as StdString with uid="WFDLKFSFGQ",Visible=.t., Left=22, Top=306,;
    Alignment=1, Width=109, Height=15,;
    Caption="Produttore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="RXUZXHFKYS",Visible=.t., Left=22, Top=330,;
    Alignment=1, Width=109, Height=15,;
    Caption="Fornitore abituale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="DNWPAKECTO",Visible=.t., Left=16, Top=58,;
    Alignment=1, Width=115, Height=15,;
    Caption="Q.t� minima riordino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="CSRJPWNVUX",Visible=.t., Left=302, Top=131,;
    Alignment=1, Width=194, Height=15,;
    Caption="N.giorni per approvvigionamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (.w_PROVEN='I' AND NOT EMPTY(.w_CODDIS))
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="XECROAAQUQ",Visible=.t., Left=201, Top=156,;
    Alignment=1, Width=109, Height=15,;
    Caption="Lead-time globale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="YVRCAYUATH",Visible=.t., Left=383, Top=131,;
    Alignment=1, Width=113, Height=15,;
    Caption="Lead-time fisso:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (NOT (.w_PROVEN='I' AND NOT EMPTY(.w_CODDIS)))
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="UZYPZKPDGR",Visible=.t., Left=399, Top=180,;
    Alignment=1, Width=97, Height=15,;
    Caption="Lotto medio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="CNTYFRZDXN",Visible=.t., Left=374, Top=156,;
    Alignment=1, Width=122, Height=15,;
    Caption="Coeff. x LT variabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (NOT (.w_PROVEN='I' AND NOT EMPTY(.w_CODDIS)))
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="MJITOFBNJQ",Visible=.t., Left=22, Top=180,;
    Alignment=1, Width=109, Height=15,;
    Caption="Low level code:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="VMPIHOBGLN",Visible=.t., Left=203, Top=180,;
    Alignment=1, Width=101, Height=15,;
    Caption="Giorni copertura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="KSWQHGIGXR",Visible=.t., Left=402, Top=59,;
    Alignment=1, Width=160, Height=15,;
    Caption="Coefficiente ripristino SS:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (.w_TIPGES <> 'F' or ! .w_PRSCOMIN>0)
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="YTGCYHOIWF",Visible=.t., Left=5, Top=81,;
    Alignment=1, Width=126, Height=15,;
    Caption="Q.t� massima riordino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="ACKDXVZWPC",Visible=.t., Left=12, Top=201,;
    Alignment=1, Width=119, Height=15,;
    Caption="Sistema d'ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="WHJEPTTKBP",Visible=.t., Left=405, Top=203,;
    Alignment=1, Width=91, Height=18,;
    Caption="Stato ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="EOOWBKNHBS",Visible=.t., Left=374, Top=228,;
    Alignment=1, Width=121, Height=15,;
    Caption="Lead-time sicurezza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="KDTOKHMWFZ",Visible=.t., Left=589, Top=228,;
    Alignment=1, Width=65, Height=15,;
    Caption="% Scarto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="QMMGFJBILZ",Visible=.t., Left=-5, Top=254,;
    Alignment=1, Width=136, Height=18,;
    Caption="Pianificazione periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="KDFBPOCRRZ",Visible=.t., Left=301, Top=255,;
    Alignment=1, Width=194, Height=18,;
    Caption="Pianificazione articolo fantasma:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="KVQSBZMVVW",Visible=.t., Left=3, Top=354,;
    Alignment=1, Width=128, Height=18,;
    Caption="Domanda media:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="LSCCHYBDBU",Visible=.t., Left=353, Top=282,;
    Alignment=1, Width=143, Height=18,;
    Caption="Tipo di gestione DTF:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="VXOPVPCUQD",Visible=.t., Left=599, Top=283,;
    Alignment=1, Width=68, Height=18,;
    Caption="Giorni DTF:"  ;
  , bGlobalFont=.t.

  add object oBox_1_38 as StdBox with uid="MMKYLKUIMB",left=4, top=26, width=653,height=2
enddefine
define class tgsma_aprPag2 as StdContainer
  Width  = 725
  height = 396
  stdWidth  = 725
  stdheight = 396
  resizeXpos=663
  resizeYpos=147
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="MRCOEOZEPK",left=-3, top=4, width=728, height=392, bOnScreen=.t.;

enddefine
define class tgsma_aprPag3 as StdContainer
  Width  = 725
  height = 396
  stdWidth  = 725
  stdheight = 396
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPRLISCOS_3_7 as StdField with uid="WKQOUOFNRO",rtseq=40,rtrep=.f.,;
    cFormVar = "w_PRLISCOS", cQueryName = "PRLISCOS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 17589175,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=317, Top=36, InputMask=replicate('X',5), cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_PRLISCOS"

  func oPRLISCOS_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESLIS_3_8 as StdField with uid="JBPTFFPKEP",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 27853258,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=368, Top=36, InputMask=replicate('X',40)

  add object oPRESECOS_3_10 as StdField with uid="BSFLOXLSGS",rtseq=42,rtrep=.f.,;
    cFormVar = "w_PRESECOS", cQueryName = "PRESECOS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 31642551,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=150, Top=12, InputMask=replicate('X',4)

  add object oPRINVCOS_3_11 as StdField with uid="XMIRIFGNKP",rtseq=43,rtrep=.f.,;
    cFormVar = "w_PRINVCOS", cQueryName = "PRINVCOS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(6), bMultilanguage =  .f.,;
    HelpContextID = 14128055,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=150, Top=35, InputMask=replicate('X',6)

  add object oPUUTEELA_3_14 as StdField with uid="DXCHTROYHI",rtseq=44,rtrep=.f.,;
    cFormVar = "w_PUUTEELA", cQueryName = "PUUTEELA",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 266391753,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=317, Top=12, cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_PUUTEELA"

  func oPUUTEELA_3_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oUTEDES_3_15 as StdField with uid="TWBUPKBEWI",rtseq=45,rtrep=.f.,;
    cFormVar = "w_UTEDES", cQueryName = "UTEDES",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 32625082,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=368, Top=12, InputMask=replicate('X',40)

  add object oTOTSTAND_3_17 as StdField with uid="NWAWHAZXIO",rtseq=108,rtrep=.f.,;
    cFormVar = "w_TOTSTAND", cQueryName = "TOTSTAND",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 49407622,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=159, Top=211, cSayPict="v_PU(140)"

  add object oTOTULTIM_3_18 as StdField with uid="PCLZVQVQXL",rtseq=109,rtrep=.f.,;
    cFormVar = "w_TOTULTIM", cQueryName = "TOTULTIM",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 261101955,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=295, Top=211, cSayPict="v_PU(140)"

  add object oTOTMEDIO_3_19 as StdField with uid="ZLTTOGBVRD",rtseq=110,rtrep=.f.,;
    cFormVar = "w_TOTMEDIO", cQueryName = "TOTMEDIO",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 253237637,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=431, Top=211, cSayPict="v_PU(140)"

  add object oTOTLISTI_3_20 as StdField with uid="GKTBIFMMPA",rtseq=111,rtrep=.f.,;
    cFormVar = "w_TOTLISTI", cQueryName = "TOTLISTI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 240589183,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=567, Top=211, cSayPict="v_PU(140)"

  add object oPRDATCOS_3_22 as StdField with uid="OUJZJQMEJR",rtseq=112,rtrep=.f.,;
    cFormVar = "w_PRDATCOS", cQueryName = "PRDATCOS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 17097655,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=72, Left=219, Top=240

  add object oPRLOTSTD_3_23 as StdField with uid="CQKSWUXOBD",rtseq=113,rtrep=.f.,;
    cFormVar = "w_PRLOTSTD", cQueryName = "PRLOTSTD",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 252288058,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=60, Left=159, Top=240, cSayPict='"99,999,999"'

  add object oPRDATULT_3_24 as StdField with uid="HMGNICQBVY",rtseq=114,rtrep=.f.,;
    cFormVar = "w_PRDATULT", cQueryName = "PRDATULT",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 251978678,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=72, Left=355, Top=240

  add object oPRLOTULT_3_25 as StdField with uid="QIIMQNBVWL",rtseq=115,rtrep=.f.,;
    cFormVar = "w_PRLOTULT", cQueryName = "PRLOTULT",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 251028406,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=60, Left=295, Top=240, cSayPict='"99,999,999"'

  add object oPRDATMED_3_26 as StdField with uid="HSDCIAJBWA",rtseq=116,rtrep=.f.,;
    cFormVar = "w_PRDATMED", cQueryName = "PRDATMED",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 150674490,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=72, Left=491, Top=240

  add object oPRLOTMED_3_27 as StdField with uid="TMGKGTSWQX",rtseq=117,rtrep=.f.,;
    cFormVar = "w_PRLOTMED", cQueryName = "PRLOTMED",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 151624762,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=60, Left=431, Top=240, cSayPict='"99,999,999"'

  add object oPRDATLIS_3_28 as StdField with uid="VSFAEZHGST",rtseq=118,rtrep=.f.,;
    cFormVar = "w_PRDATLIS", cQueryName = "PRDATLIS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 133897289,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=72, Left=627, Top=240

  add object oPRLOTLIS_3_29 as StdField with uid="VMLUDLOTKI",rtseq=119,rtrep=.f.,;
    cFormVar = "w_PRLOTLIS", cQueryName = "PRLOTLIS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 134847561,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=60, Left=567, Top=240, cSayPict='"99,999,999"'

  add object oTOTCSLAVO_3_31 as StdField with uid="YBBLPKYRHL",rtseq=120,rtrep=.f.,;
    cFormVar = "w_TOTCSLAVO", cQueryName = "TOTCSLAVO",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 135389572,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=159, Top=98, cSayPict="v_PU(140)"

  add object oTOTCULAVO_3_32 as StdField with uid="MORJDEWVMM",rtseq=121,rtrep=.f.,;
    cFormVar = "w_TOTCULAVO", cQueryName = "TOTCULAVO",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 133292420,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=295, Top=98, cSayPict="v_PU(140)"

  add object oTOTCMLAVO_3_33 as StdField with uid="WNUTQYQKZL",rtseq=122,rtrep=.f.,;
    cFormVar = "w_TOTCMLAVO", cQueryName = "TOTCMLAVO",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 141681028,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=431, Top=98, cSayPict="v_PU(140)"

  add object oTOTCLLAVO_3_34 as StdField with uid="LHJIMLOEJN",rtseq=123,rtrep=.f.,;
    cFormVar = "w_TOTCLLAVO", cQueryName = "TOTCLLAVO",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 142729604,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=567, Top=98, cSayPict="v_PU(140)"

  add object oTOTCSLAVE_3_35 as StdField with uid="GEUVXGXJFL",rtseq=124,rtrep=.f.,;
    cFormVar = "w_TOTCSLAVE", cQueryName = "TOTCSLAVE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 135389732,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=159, Top=121, cSayPict="v_PU(140)"

  add object oTOTCULAVE_3_36 as StdField with uid="XGQHVVUUJE",rtseq=125,rtrep=.f.,;
    cFormVar = "w_TOTCULAVE", cQueryName = "TOTCULAVE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 133292580,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=295, Top=121, cSayPict="v_PU(140)"

  add object oTOTCMLAVE_3_37 as StdField with uid="LTZEPMWQMY",rtseq=126,rtrep=.f.,;
    cFormVar = "w_TOTCMLAVE", cQueryName = "TOTCMLAVE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 141681188,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=431, Top=121, cSayPict="v_PU(140)"

  add object oTOTCLLAVE_3_38 as StdField with uid="ZSAQILCBCS",rtseq=127,rtrep=.f.,;
    cFormVar = "w_TOTCLLAVE", cQueryName = "TOTCLLAVE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 142729764,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=567, Top=121, cSayPict="v_PU(140)"

  add object oTOTCSMATE_3_39 as StdField with uid="LFIHLLTAYT",rtseq=128,rtrep=.f.,;
    cFormVar = "w_TOTCSMATE", cQueryName = "TOTCSMATE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 118612518,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=159, Top=144, cSayPict="v_PU(140)"

  add object oTOTCUMATE_3_40 as StdField with uid="UWHJSNDVYT",rtseq=129,rtrep=.f.,;
    cFormVar = "w_TOTCUMATE", cQueryName = "TOTCUMATE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 116515366,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=295, Top=144, cSayPict="v_PU(140)"

  add object oTOTCMMATE_3_41 as StdField with uid="LBLQZSZPDE",rtseq=130,rtrep=.f.,;
    cFormVar = "w_TOTCMMATE", cQueryName = "TOTCMMATE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 124903974,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=431, Top=144, cSayPict="v_PU(140)"

  add object oTOTCLMATE_3_42 as StdField with uid="SYXIXSOZMC",rtseq=131,rtrep=.f.,;
    cFormVar = "w_TOTCLMATE", cQueryName = "TOTCLMATE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 125952550,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=567, Top=144, cSayPict="v_PU(140)"

  add object oTOTCSSPGE_3_43 as StdField with uid="GNRCCLFPLS",rtseq=132,rtrep=.f.,;
    cFormVar = "w_TOTCSSPGE", cQueryName = "TOTCSSPGE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 17949235,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=159, Top=167, cSayPict="v_PU(140)"

  add object oTOTCUSPGE_3_44 as StdField with uid="AIRUQKGDCP",rtseq=133,rtrep=.f.,;
    cFormVar = "w_TOTCUSPGE", cQueryName = "TOTCUSPGE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 15852083,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=295, Top=167, cSayPict="v_PU(140)"

  add object oTOTCMSPGE_3_45 as StdField with uid="YGNVMQFVMW",rtseq=134,rtrep=.f.,;
    cFormVar = "w_TOTCMSPGE", cQueryName = "TOTCMSPGE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 24240691,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=431, Top=167, cSayPict="v_PU(140)"

  add object oTOTCLSPGE_3_46 as StdField with uid="KHBLYHZHSI",rtseq=135,rtrep=.f.,;
    cFormVar = "w_TOTCLSPGE", cQueryName = "TOTCLSPGE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25289267,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=567, Top=167, cSayPict="v_PU(140)"

  add object oStr_3_2 as StdString with uid="FMFZFUYPKS",Visible=.t., Left=14, Top=72,;
    Alignment=0, Width=153, Height=18,;
    Caption="Costi prodotto"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_3 as StdString with uid="DXIDPZZCGF",Visible=.t., Left=158, Top=72,;
    Alignment=0, Width=50, Height=18,;
    Caption="Standard"  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="DSTDKKQMAJ",Visible=.t., Left=302, Top=72,;
    Alignment=0, Width=76, Height=18,;
    Caption="Ultimo costo"  ;
  , bGlobalFont=.t.

  add object oStr_3_5 as StdString with uid="IPETFLAZIF",Visible=.t., Left=445, Top=72,;
    Alignment=0, Width=108, Height=18,;
    Caption="Medio ponderato"  ;
  , bGlobalFont=.t.

  add object oStr_3_6 as StdString with uid="PKOVNDSSKQ",Visible=.t., Left=588, Top=72,;
    Alignment=0, Width=54, Height=18,;
    Caption="Listino"  ;
  , bGlobalFont=.t.

  add object oStr_3_9 as StdString with uid="IDEQGHXGLI",Visible=.t., Left=257, Top=38,;
    Alignment=1, Width=57, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="YIHOPIUVQH",Visible=.t., Left=94, Top=14,;
    Alignment=1, Width=53, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="HWDMMYHMDV",Visible=.t., Left=30, Top=37,;
    Alignment=1, Width=117, Height=18,;
    Caption="Numero inventario:"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="FSWFPXPLKA",Visible=.t., Left=257, Top=14,;
    Alignment=1, Width=57, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_3_21 as StdString with uid="YYGWXCFLZK",Visible=.t., Left=35, Top=241,;
    Alignment=1, Width=120, Height=17,;
    Caption="Lotto medio / elab.:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_30 as StdString with uid="SGIUKRBAEI",Visible=.t., Left=35, Top=214,;
    Alignment=1, Width=120, Height=18,;
    Caption="Totale costi prodotto:"  ;
  , bGlobalFont=.t.

  add object oStr_3_47 as StdString with uid="TRHBVSWTHF",Visible=.t., Left=35, Top=101,;
    Alignment=1, Width=120, Height=18,;
    Caption="Lavorazione interna:"  ;
  , bGlobalFont=.t.

  add object oStr_3_48 as StdString with uid="HHSIAOGELO",Visible=.t., Left=35, Top=147,;
    Alignment=1, Width=120, Height=18,;
    Caption="Materiali:"  ;
  , bGlobalFont=.t.

  add object oStr_3_49 as StdString with uid="JPZOOEUGQH",Visible=.t., Left=3, Top=170,;
    Alignment=1, Width=152, Height=18,;
    Caption="Spese generali:"  ;
  , bGlobalFont=.t.

  add object oStr_3_50 as StdString with uid="LIANFJXCWG",Visible=.t., Left=35, Top=124,;
    Alignment=1, Width=120, Height=18,;
    Caption="Lavorazione esterna:"  ;
  , bGlobalFont=.t.

  add object oBox_3_1 as StdBox with uid="AQBZEJXDPK",left=15, top=89, width=696,height=2
enddefine
define class tgsma_aprPag4 as StdContainer
  Width  = 725
  height = 396
  stdWidth  = 725
  stdheight = 396
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPRCSLAVO_4_1 as StdField with uid="UGZDSNPATT",rtseq=58,rtrep=.f.,;
    cFormVar = "w_PRCSLAVO", cQueryName = "PRCSLAVO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210570309,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=160, Top=98, cSayPict="v_PU(140)"

  add object oPRCSMATE_4_2 as StdField with uid="IVJIYFLBWN",rtseq=59,rtrep=.f.,;
    cFormVar = "w_PRCSMATE", cQueryName = "PRCSMATE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211618875,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=160, Top=144, cSayPict="v_PU(140)"

  add object oPRCSSPGE_4_3 as StdField with uid="YLEVFZBHMM",rtseq=60,rtrep=.f.,;
    cFormVar = "w_PRCSSPGE", cQueryName = "PRCSSPGE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201133115,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=160, Top=167, cSayPict="v_PU(140)"

  add object oPRCULAVO_4_4 as StdField with uid="TVOPWMAJTS",rtseq=61,rtrep=.f.,;
    cFormVar = "w_PRCULAVO", cQueryName = "PRCULAVO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210701381,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=296, Top=98, cSayPict="v_PU(140)"

  add object oPRCUMATE_4_5 as StdField with uid="QMCOCQGHOC",rtseq=62,rtrep=.f.,;
    cFormVar = "w_PRCUMATE", cQueryName = "PRCUMATE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211749947,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=296, Top=144, cSayPict="v_PU(140)"

  add object oPRCUSPGE_4_6 as StdField with uid="LRWPHHPXDW",rtseq=63,rtrep=.f.,;
    cFormVar = "w_PRCUSPGE", cQueryName = "PRCUSPGE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201264187,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=296, Top=167, cSayPict="v_PU(140)"

  add object oPRCMLAVO_4_7 as StdField with uid="PUOUHRMEHE",rtseq=64,rtrep=.f.,;
    cFormVar = "w_PRCMLAVO", cQueryName = "PRCMLAVO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210177093,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=98, cSayPict="v_PU(140)"

  add object oPRCMMATE_4_8 as StdField with uid="XAVQFDTGKS",rtseq=65,rtrep=.f.,;
    cFormVar = "w_PRCMMATE", cQueryName = "PRCMMATE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211225659,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=144, cSayPict="v_PU(140)"

  add object oPRCMSPGE_4_9 as StdField with uid="SPYFISHLCN",rtseq=66,rtrep=.f.,;
    cFormVar = "w_PRCMSPGE", cQueryName = "PRCMSPGE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 200739899,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=167, cSayPict="v_PU(140)"

  add object oPRCLLAVO_4_10 as StdField with uid="WQPZEVIOOW",rtseq=67,rtrep=.f.,;
    cFormVar = "w_PRCLLAVO", cQueryName = "PRCLLAVO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210111557,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=568, Top=98, cSayPict="v_PU(140)"

  add object oPRCLMATE_4_11 as StdField with uid="OTNOZVDPIC",rtseq=68,rtrep=.f.,;
    cFormVar = "w_PRCLMATE", cQueryName = "PRCLMATE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211160123,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=568, Top=144, cSayPict="v_PU(140)"

  add object oPRCLSPGE_4_12 as StdField with uid="KWNRJAXTOL",rtseq=69,rtrep=.f.,;
    cFormVar = "w_PRCLSPGE", cQueryName = "PRCLSPGE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 200674363,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=568, Top=167, cSayPict="v_PU(140)"

  add object oPRCSLAVE_4_16 as StdField with uid="HQPKIOGTFC",rtseq=70,rtrep=.f.,;
    cFormVar = "w_PRCSLAVE", cQueryName = "PRCSLAVE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210570299,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=160, Top=121, cSayPict="v_PU(140)"

  add object oPRCSLAVC_4_19 as StdField with uid="SZVVPOLFSV",rtseq=71,rtrep=.f.,;
    cFormVar = "w_PRCSLAVC", cQueryName = "PRCSLAVC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210570297,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=160, Top=213, cSayPict="v_PU(140)"

  add object oPRCSMATC_4_20 as StdField with uid="LIOQVQNFOV",rtseq=72,rtrep=.f.,;
    cFormVar = "w_PRCSMATC", cQueryName = "PRCSMATC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211618873,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=160, Top=259, cSayPict="v_PU(140)"

  add object oPRCSSPGC_4_21 as StdField with uid="JPAREHNDFU",rtseq=73,rtrep=.f.,;
    cFormVar = "w_PRCSSPGC", cQueryName = "PRCSSPGC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201133113,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=160, Top=282, cSayPict="v_PU(140)"

  add object oPRCULAVC_4_22 as StdField with uid="SPWFCDWTCF",rtseq=74,rtrep=.f.,;
    cFormVar = "w_PRCULAVC", cQueryName = "PRCULAVC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210701369,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=296, Top=213, cSayPict="v_PU(140)"

  add object oPRCUMATC_4_23 as StdField with uid="DJDEYHTORY",rtseq=75,rtrep=.f.,;
    cFormVar = "w_PRCUMATC", cQueryName = "PRCUMATC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211749945,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=296, Top=259, cSayPict="v_PU(140)"

  add object oPRCUSPGC_4_24 as StdField with uid="TRJRPPVKGE",rtseq=76,rtrep=.f.,;
    cFormVar = "w_PRCUSPGC", cQueryName = "PRCUSPGC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201264185,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=296, Top=282, cSayPict="v_PU(140)"

  add object oPRCMLAVC_4_25 as StdField with uid="NYWIQDOQXL",rtseq=77,rtrep=.f.,;
    cFormVar = "w_PRCMLAVC", cQueryName = "PRCMLAVC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210177081,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=213, cSayPict="v_PU(140)"

  add object oPRCMMATC_4_26 as StdField with uid="SAFDYUIYZH",rtseq=78,rtrep=.f.,;
    cFormVar = "w_PRCMMATC", cQueryName = "PRCMMATC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211225657,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=259, cSayPict="v_PU(140)"

  add object oPRCMSPGC_4_27 as StdField with uid="XQYSMFFIES",rtseq=79,rtrep=.f.,;
    cFormVar = "w_PRCMSPGC", cQueryName = "PRCMSPGC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 200739897,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=282, cSayPict="v_PU(140)"

  add object oPRCLLAVC_4_28 as StdField with uid="HFQWWHZKVD",rtseq=80,rtrep=.f.,;
    cFormVar = "w_PRCLLAVC", cQueryName = "PRCLLAVC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210111545,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=568, Top=213, cSayPict="v_PU(140)"

  add object oPRCLMATC_4_29 as StdField with uid="LTAEWAGYNE",rtseq=81,rtrep=.f.,;
    cFormVar = "w_PRCLMATC", cQueryName = "PRCLMATC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211160121,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=568, Top=259, cSayPict="v_PU(140)"

  add object oPRCLSPGC_4_30 as StdField with uid="TUIFAGJIUA",rtseq=82,rtrep=.f.,;
    cFormVar = "w_PRCLSPGC", cQueryName = "PRCLSPGC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 200674361,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=568, Top=282, cSayPict="v_PU(140)"

  add object oPRCSLVCE_4_34 as StdField with uid="YRRAZEKTFJ",rtseq=83,rtrep=.f.,;
    cFormVar = "w_PRCSLVCE", cQueryName = "PRCSLVCE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 26020923,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=160, Top=236, cSayPict="v_PU(140)"

  add object oPRCULVCE_4_35 as StdField with uid="LQPKCVYUBA",rtseq=84,rtrep=.f.,;
    cFormVar = "w_PRCULVCE", cQueryName = "PRCULVCE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 26151995,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=296, Top=236, cSayPict="v_PU(140)"

  add object oPRCMLVCE_4_36 as StdField with uid="FRNBGQWQVY",rtseq=85,rtrep=.f.,;
    cFormVar = "w_PRCMLVCE", cQueryName = "PRCMLVCE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25627707,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=236, cSayPict="v_PU(140)"

  add object oPRCLLVCE_4_37 as StdField with uid="FXNAQPXGXM",rtseq=86,rtrep=.f.,;
    cFormVar = "w_PRCLLVCE", cQueryName = "PRCLLVCE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25562171,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=568, Top=236, cSayPict="v_PU(140)"

  add object oPRCULAVE_4_45 as StdField with uid="DPZNTARIAF",rtseq=87,rtrep=.f.,;
    cFormVar = "w_PRCULAVE", cQueryName = "PRCULAVE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210701371,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=296, Top=121, cSayPict="v_PU(140)"

  add object oPRCMLAVE_4_46 as StdField with uid="WQZWMKHIIP",rtseq=88,rtrep=.f.,;
    cFormVar = "w_PRCMLAVE", cQueryName = "PRCMLAVE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210177083,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=121, cSayPict="v_PU(140)"

  add object oPRCLLAVE_4_47 as StdField with uid="WKWBOTYWYZ",rtseq=89,rtrep=.f.,;
    cFormVar = "w_PRCLLAVE", cQueryName = "PRCLLAVE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210111547,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=568, Top=121, cSayPict="v_PU(140)"

  add object oTOTSTAND_4_49 as StdField with uid="MRXKAWOOCF",rtseq=90,rtrep=.f.,;
    cFormVar = "w_TOTSTAND", cQueryName = "TOTSTAND",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 49407622,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=160, Top=332, cSayPict="v_PU(140)"

  add object oTOTULTIM_4_50 as StdField with uid="OKMBYSBRON",rtseq=91,rtrep=.f.,;
    cFormVar = "w_TOTULTIM", cQueryName = "TOTULTIM",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 261101955,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=296, Top=332, cSayPict="v_PU(140)"

  add object oTOTMEDIO_4_51 as StdField with uid="FSQPSELISF",rtseq=92,rtrep=.f.,;
    cFormVar = "w_TOTMEDIO", cQueryName = "TOTMEDIO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 253237637,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=332, cSayPict="v_PU(140)"

  add object oTOTLISTI_4_52 as StdField with uid="YMJKVXVRUA",rtseq=93,rtrep=.f.,;
    cFormVar = "w_TOTLISTI", cQueryName = "TOTLISTI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 240589183,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=568, Top=332, cSayPict="v_PU(140)"

  add object oPRDATCOS_4_54 as StdField with uid="IUZHVJDHTG",rtseq=94,rtrep=.f.,;
    cFormVar = "w_PRDATCOS", cQueryName = "PRDATCOS",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 17097655,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=72, Left=220, Top=361

  add object oPRLOTSTD_4_55 as StdField with uid="KDEOQKYONI",rtseq=95,rtrep=.f.,;
    cFormVar = "w_PRLOTSTD", cQueryName = "PRLOTSTD",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 252288058,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=60, Left=160, Top=361, cSayPict='"99,999,999"'

  add object oPRDATULT_4_56 as StdField with uid="KAVBKCBLFO",rtseq=96,rtrep=.f.,;
    cFormVar = "w_PRDATULT", cQueryName = "PRDATULT",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 251978678,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=72, Left=356, Top=361

  add object oPRLOTULT_4_57 as StdField with uid="LIPXPQAPMY",rtseq=97,rtrep=.f.,;
    cFormVar = "w_PRLOTULT", cQueryName = "PRLOTULT",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 251028406,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=60, Left=296, Top=361, cSayPict='"99,999,999"'

  add object oPRDATMED_4_58 as StdField with uid="YTTCOGDOPK",rtseq=98,rtrep=.f.,;
    cFormVar = "w_PRDATMED", cQueryName = "PRDATMED",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 150674490,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=72, Left=492, Top=361

  add object oPRLOTMED_4_59 as StdField with uid="PTCUSNXCZB",rtseq=99,rtrep=.f.,;
    cFormVar = "w_PRLOTMED", cQueryName = "PRLOTMED",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 151624762,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=60, Left=432, Top=361, cSayPict='"99,999,999"'

  add object oPRDATLIS_4_60 as StdField with uid="DSOOLIPZLT",rtseq=100,rtrep=.f.,;
    cFormVar = "w_PRDATLIS", cQueryName = "PRDATLIS",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 133897289,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=72, Left=628, Top=361

  add object oPRLOTLIS_4_61 as StdField with uid="XWQLXYQHRP",rtseq=101,rtrep=.f.,;
    cFormVar = "w_PRLOTLIS", cQueryName = "PRLOTLIS",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 134847561,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=60, Left=568, Top=361, cSayPict='"99,999,999"'

  add object oPRLISCOS_4_63 as StdField with uid="VNKCHQGEYR",rtseq=102,rtrep=.f.,;
    cFormVar = "w_PRLISCOS", cQueryName = "PRLISCOS",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 17589175,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=317, Top=36, InputMask=replicate('X',5), cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_PRLISCOS"

  func oPRLISCOS_4_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESLIS_4_64 as StdField with uid="OAMBHQLRGT",rtseq=103,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 27853258,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=368, Top=36, InputMask=replicate('X',40)

  add object oPRESECOS_4_66 as StdField with uid="NNXZCKZICX",rtseq=104,rtrep=.f.,;
    cFormVar = "w_PRESECOS", cQueryName = "PRESECOS",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 31642551,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=150, Top=12, InputMask=replicate('X',4)

  add object oPRINVCOS_4_67 as StdField with uid="THCMSSJTCO",rtseq=105,rtrep=.f.,;
    cFormVar = "w_PRINVCOS", cQueryName = "PRINVCOS",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(6), bMultilanguage =  .f.,;
    HelpContextID = 14128055,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=150, Top=35, InputMask=replicate('X',6)

  add object oPUUTEELA_4_70 as StdField with uid="ROIEFOGSSL",rtseq=106,rtrep=.f.,;
    cFormVar = "w_PUUTEELA", cQueryName = "PUUTEELA",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 266391753,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=317, Top=12, cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_PUUTEELA"

  func oPUUTEELA_4_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oUTEDES_4_71 as StdField with uid="RXADMSPNRI",rtseq=107,rtrep=.f.,;
    cFormVar = "w_UTEDES", cQueryName = "UTEDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 32625082,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=368, Top=12, InputMask=replicate('X',40)

  add object oStr_4_13 as StdString with uid="ZLJIBXSPCP",Visible=.t., Left=36, Top=100,;
    Alignment=1, Width=120, Height=18,;
    Caption="Lavorazione interna:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="LPOZUEPQEO",Visible=.t., Left=36, Top=148,;
    Alignment=1, Width=120, Height=18,;
    Caption="Materiali:"  ;
  , bGlobalFont=.t.

  add object oStr_4_15 as StdString with uid="JYKHDIWSRX",Visible=.t., Left=4, Top=170,;
    Alignment=1, Width=152, Height=18,;
    Caption="Spese generali:"  ;
  , bGlobalFont=.t.

  add object oStr_4_18 as StdString with uid="ZBRNTTIADI",Visible=.t., Left=11, Top=194,;
    Alignment=0, Width=153, Height=16,;
    Caption="Analisi costi componenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_31 as StdString with uid="WRSSELMTIF",Visible=.t., Left=36, Top=215,;
    Alignment=1, Width=120, Height=18,;
    Caption="Lavorazione interna:"  ;
  , bGlobalFont=.t.

  add object oStr_4_32 as StdString with uid="VHZQORPPDZ",Visible=.t., Left=36, Top=263,;
    Alignment=1, Width=120, Height=18,;
    Caption="Materiali:"  ;
  , bGlobalFont=.t.

  add object oStr_4_33 as StdString with uid="SULQDZWOAD",Visible=.t., Left=4, Top=284,;
    Alignment=1, Width=152, Height=18,;
    Caption="Spese generali:"  ;
  , bGlobalFont=.t.

  add object oStr_4_38 as StdString with uid="GQWQCTHSJK",Visible=.t., Left=36, Top=239,;
    Alignment=1, Width=120, Height=18,;
    Caption="Lavorazione esterna:"  ;
  , bGlobalFont=.t.

  add object oStr_4_39 as StdString with uid="SJZAUUVLEW",Visible=.t., Left=160, Top=72,;
    Alignment=0, Width=50, Height=18,;
    Caption="Standard"  ;
  , bGlobalFont=.t.

  add object oStr_4_40 as StdString with uid="UYUGZXHHGD",Visible=.t., Left=302, Top=72,;
    Alignment=0, Width=76, Height=18,;
    Caption="Ultimo costo"  ;
  , bGlobalFont=.t.

  add object oStr_4_41 as StdString with uid="WIMLWORPMN",Visible=.t., Left=445, Top=72,;
    Alignment=0, Width=108, Height=18,;
    Caption="Medio ponderato"  ;
  , bGlobalFont=.t.

  add object oStr_4_42 as StdString with uid="BUBGMOYTJC",Visible=.t., Left=588, Top=72,;
    Alignment=0, Width=54, Height=18,;
    Caption="Listino"  ;
  , bGlobalFont=.t.

  add object oStr_4_44 as StdString with uid="MXRELBPBWW",Visible=.t., Left=11, Top=72,;
    Alignment=0, Width=144, Height=18,;
    Caption="Analisi costi prodotto"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_48 as StdString with uid="LOAKFRKYXI",Visible=.t., Left=36, Top=124,;
    Alignment=1, Width=120, Height=18,;
    Caption="Lavorazione esterna:"  ;
  , bGlobalFont=.t.

  add object oStr_4_53 as StdString with uid="QSIXTRPCEF",Visible=.t., Left=36, Top=362,;
    Alignment=1, Width=120, Height=17,;
    Caption="Lotto medio / elab.:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_62 as StdString with uid="WIVSHWORIN",Visible=.t., Left=36, Top=336,;
    Alignment=1, Width=120, Height=18,;
    Caption="Totale costi prodotto:"  ;
  , bGlobalFont=.t.

  add object oStr_4_65 as StdString with uid="TJOKEREQQV",Visible=.t., Left=257, Top=38,;
    Alignment=1, Width=57, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_4_68 as StdString with uid="XMRWTENLOU",Visible=.t., Left=94, Top=14,;
    Alignment=1, Width=53, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_4_69 as StdString with uid="OVGQOBBOEL",Visible=.t., Left=30, Top=37,;
    Alignment=1, Width=117, Height=18,;
    Caption="Numero inventario:"  ;
  , bGlobalFont=.t.

  add object oStr_4_72 as StdString with uid="GCKQKBTLTX",Visible=.t., Left=257, Top=14,;
    Alignment=1, Width=57, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oBox_4_17 as StdBox with uid="IRXWMCNDOF",left=9, top=209, width=708,height=2

  add object oBox_4_43 as StdBox with uid="HCNIMHDTUG",left=9, top=89, width=708,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_apr','PAR_RIOR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PRCODART=PAR_RIOR.PRCODART";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
