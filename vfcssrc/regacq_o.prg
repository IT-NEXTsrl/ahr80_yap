* ---------------------------------------------------------------------------- *
* #%&%#Build:0000
*                                                                              *
*   Procedure: REGACQ_O                                                        *
*              Registro acquisti Orizz.                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 10/4/01                                                         *
* Last revis.: 11/9/14                                                         *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
private i_formh13,i_formh14
private i_brk,i_quit,i_row,i_pag,i_oldarea,i_oldrows
private i_rdvars,i_rdkvars,i_rdkey
private w_s,i_rec,i_wait,i_modal
private i_usr_brk          && .T. se l'utente ha interrotto la stampa
private i_frm_rpr          && .T. se � stata stampata la testata del report
private Sm                 && Variabile di appoggio per formattazione memo
private i_form_ph,i_form_phh,i_form_phg,i_form_pf,i_saveh13,i_exec

* --- Variabili per configurazione stampante
private w_t_stdevi,w_t_stmsin,w_t_stnrig
private w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
private w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
private w_t_stpica,w_t_stelit
private w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
private w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
private w_stdesc
store " " to w_t_stdevi
store " " to w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
store " " to w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
store " " to w_t_stpica,w_t_stelit, i_rdkey
store " " to w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
store " " to w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
store ""  to Sm
store 0 to i_formh13,i_formh14,i_form_phh,i_form_phg,i_saveh13
store space(20) to w_stdesc
i_form_ph = 12
i_form_pf = 13
i_wait = 1
i_modal = .t.
i_oldrows = 0
store .F. to i_usr_brk, i_frm_rpr, w_s
i_exec = ""

dimension i_rdvars[41,2],i_rdkvars[6,2],i_zoompars[3]
store 0 to i_rdvars[1,1],i_rdkvars[1,1]
private w_SCompr,w_TIPREG,w_TIPREC,w_NUMALFDOC,w_PNNUMALF
private w_DESSET,w_TIPO,w_SCompr,w_ALIQUO,w_SCompr
private w_SCompr,w_IMPTOT,w_SCompr,w_COMPET,w_SCompr
private w_NOTE,w_RIEIMP,w_RIEIVA,w_RIEIVI,w_RIEIMP1
private w_RIEIMP2,w_RIEIVA1,w_RIEIVA2,w_ARIMP1,w_ARIMP2
private w_ARIVA1,w_ARIVA2,w_APIVI1,w_APIVI2,w_ARIVI1
private w_ARIVI2,w_SPAZIO,w_TOTDOC,w_APIMP,w_APIVA
private w_APIVI,w_ARIMP,w_ARIVA,w_ARIVI,text01
private TOT_IVADET,TOT_IVAPRO,w_EPIMP,w_EPIVA,w_EPIVI
private w_ERIMP,w_ERIVA,w_ERIVI,w_APIMPT,w_APIVAT
private w_APIVIT,w_ARIMPT,w_ARIVAT,w_ARIVIT,w_SETTA
private PREFIS,NUMPAG,p_RAGAZI,p_PARTO,w_DATIAZ
private p_CODO,w_Set,w_INTESTA,P_DATINI,P_DATFIN
private P_VALSIM,P_DESVAL,L_STAMPATO,P_CREINI,w_SETTA
private PREFIS,NUMPAG,p_RAGAZI,p_PARTO,w_DATIAZ
private p_CODO,w_INTESTA,P_DATINI,P_DATFIN,P_VALSIM
private P_DESVAL,P_CREINI
w_SCompr = space(1)
w_TIPREG = space(10)
w_TIPREC = space(10)
w_NUMALFDOC = space(26)
w_PNNUMALF = space(26)
w_DESSET = space(31)
w_TIPO = space(2)
w_SCompr = space(1)
w_ALIQUO = space(5)
w_SCompr = space(1)
w_SCompr = space(1)
w_IMPTOT = 0
w_SCompr = space(1)
w_COMPET = space(9)
w_SCompr = space(1)
w_NOTE = space(5)
w_RIEIMP = 0
w_RIEIVA = 0
w_RIEIVI = 0
w_RIEIMP1 = 0
w_RIEIMP2 = 0
w_RIEIVA1 = 0
w_RIEIVA2 = 0
w_ARIMP1 = 0
w_ARIMP2 = 0
w_ARIVA1 = 0
w_ARIVA2 = 0
w_APIVI1 = 0
w_APIVI2 = 0
w_ARIVI1 = 0
w_ARIVI2 = 0
w_SPAZIO = space(10)
w_TOTDOC = 0
w_APIMP = 0
w_APIVA = 0
w_APIVI = 0
w_ARIMP = 0
w_ARIVA = 0
w_ARIVI = 0
text01 = space(10)
TOT_IVADET = 0
TOT_IVAPRO = 0
w_EPIMP = 0
w_EPIVA = 0
w_EPIVI = 0
w_ERIMP = 0
w_ERIVA = 0
w_ERIVI = 0
w_APIMPT = 0
w_APIVAT = 0
w_APIVIT = 0
w_ARIMPT = 0
w_ARIVAT = 0
w_ARIVIT = 0
w_SETTA = space(1)
PREFIS = space(20)
NUMPAG = 0
p_RAGAZI = space(99)
p_PARTO = space(16)
w_DATIAZ = space(99)
p_CODO = space(16)
w_Set = space(10)
w_INTESTA = space(35)
P_DATINI = ctod("  /  /  ")
P_DATFIN = ctod("  /  /  ")
P_VALSIM = space(3)
P_DESVAL = space(35)
L_STAMPATO = .f.
P_CREINI = space(20)
w_SETTA = space(1)
PREFIS = space(20)
NUMPAG = 0
p_RAGAZI = space(99)
p_PARTO = space(16)
w_DATIAZ = space(99)
p_CODO = space(16)
w_INTESTA = space(35)
P_DATINI = ctod("  /  /  ")
P_DATFIN = ctod("  /  /  ")
P_VALSIM = space(3)
P_DESVAL = space(35)
P_CREINI = space(20)

i_oldarea = select()
select __tmp__
go top

w_t_stnrig = 65
w_t_stmsin = 0
  
  
* --- Inizializza Variabili per configurazione stampante da CP_CHPRN
w_t_stdevi = cFileStampa+'.prn'
w_t_stlung = ts_ForPag
w_t_stnrig = ts_RowOk 
w_t_strese = ts_Reset+ts_Inizia
w_t_st10 = ts_10Cpi
w_t_st12 = ts_12Cpi
w_t_st15 = ts_15Cpi
w_t_stcomp = ts_Comp
w_t_stnorm = ts_RtComp
w_t_stbold = ts_StBold
w_t_stwide = ts_StDoub
w_t_stital = ts_StItal
w_t_stunde = ts_StUnde
w_t_stbol_ = ts_FiBold
w_t_stwid_ = ts_FiDoub
w_t_stita_ = ts_FiItal
w_t_stund_ = ts_FiUnde
* --- non definiti
*w_t_stmsin
*w_t_stnlq
*w_t_stdraf
*w_t_stpica
*w_t_stelit

i_row = 0
i_pag = 1
*---------------------------------------
wait wind "Generazione file appoggio..." nowait
*----------------------------------------
activate screen
* --- Settaggio stampante
set printer to &w_t_stdevi
set device to printer
set margin to w_t_stmsin
if len(trim(w_t_strese))>0
  @ 0,0 say &w_t_strese
endif
if len(trim(w_t_stlung))>0
  @ 0,0 say &w_t_stlung
endif
* --- Inizio stampa
do REGA4Q_O with 1, 0
if i_frm_rpr .and. .not. i_usr_brk
  * stampa il piede del report
  do REGA4Q_O with 14, 0
endif
if i_row<>0 
  @ prow(),pcol() say chr(12)
endif
set device to screen
set printer off
set printer to
if .not. i_frm_rpr
  do cplu_erm with "Non ci sono dati da stampare"
endif
* --- Fine
if .not.(empty(wontop()))
  activate  window (wontop())
endif
i_warea = alltrim(str(i_oldarea))
select (i_oldarea)
return


procedure REGA4Q_O
* === Procedure REGA4Q_O
parameters i_form_id, i_height

private i_currec, i_prevrec, i_formh
private i_frm_brk    && flag che indica il verificarsi di un break interform
                     && anche se la specifica condizione non � soddisfatta
private i_break, i_cond1, i_cond2

do case
  case i_form_id=1
    select __tmp__
    i_warea = '__tmp__'
    * --- inizializza le condizioni dei break interform
    i_cond1 = (TIPREC)
    i_cond2 = (alfdoc+str(numdoc))
    i_frm_brk = .T.
    do while .not. eof() 
     wait wind "Elabora riga:"+str(recno()) +"/"+str(reccount()) nowait
      if .not. i_frm_rpr
        * --- stampa l'intestazione del report
        do REGA4Q_O with 11, 0
        i_frm_rpr = .T.
      endif
      if i_cond1<>(TIPREC) .or. i_frm_brk
        i_cond1 = (TIPREC)
        i_frm_brk = .T.
        do REGA5Q_O with 1.00, 0
      endif
      if i_cond2<>(alfdoc+str(numdoc)) .or. i_frm_brk
        i_cond2 = (alfdoc+str(numdoc))
        i_frm_brk = .T.
        do REGA5Q_O with 1.01, -1
      endif
      i_frm_brk = .F.
      * stampa del dettaglio
      do REGA5Q_O with 1.02, 1
      if TIPREC$'FI'
        do REGA5Q_O with 1.03, 1
      endif
      if (NVL(FLPROV,'N')='S' AND TIPREC$ 'FI') or (L_FLDEFI<>'S' AND (NOT EMPTY(FLPNUM) OR NOT EMPTY(FLPDAT))) AND POSIZ$"XP"
        do REGA5Q_O with 1.04, 1
      endif
      if TIPREC='R'
        do REGA5Q_O with 1.05, 1
      endif
      if TIPREC='R'
        do REGA5Q_O with 1.06, 1
      endif
      if (IMPPRE+IVAPRE+IVIPRE)<>0
        do REGA5Q_O with 1.07, 1
      endif
      if (IMPSEG+IVASEG+IVISEG)<>0
        do REGA5Q_O with 1.08, 1
      endif
      if (IMPFAD+IVAFAD+IVIFAD)<>0
        do REGA5Q_O with 1.09, 1
      endif
      if (IMPIND+IVAIND+IVIIND)<>0
        do REGA5Q_O with 1.10, 1
      endif
      if TIPREC='R'
        do REGA5Q_O with 1.11, 2
      endif
      if TIPREC$ 'FI' AND VALNAZ<>CODVAL AND TOTDOC<>0 AND POSIZ$"XU"
        do REGA5Q_O with 1.12, 1
      endif
      if EndOfGroup() .and. TIPREC<>'R'
        do REGA5Q_O with 1.13, 1
      endif
      if i_usr_brk
        exit
      endif
      * --- passa al record successivo
      i_prevrec = recno()
      if .not. eof()
        skip
      endif
      i_currec = iif(eof(), -1, recno())
      if eof()  .or. i_cond2<>(alfdoc+str(numdoc)) .or. i_cond1<>(TIPREC)      
        go i_prevrec
          do REGA5Q_O with 1.14, 0
        if TIPREC="R"
          do REGA5Q_O with 1.15, 1
        endif
        if TIPREC="R"
          do REGA5Q_O with 1.16, 1
        endif
        if TIPREC="R"
          do REGA5Q_O with 1.17, 1
        endif
        if TIPREC="R"
          do REGA5Q_O with 1.18, 1
        endif
        if TIPREC="R"
          do REGA5Q_O with 1.19, 2
        endif
        if TIPREC="R" .and.  i_Row<>ts_RowOk
          do REGA5Q_O with 1.20, 1
        endif
        do cplu_go with i_currec
      endif
      if eof()  .or. i_cond1<>(TIPREC)      
        go i_prevrec
        do REGA5Q_O with 1.21, 0
        do cplu_go with i_currec
      endif
    enddo
    * --- lancio del form successivo
    if .not. i_usr_brk
      do REGA4Q_O with 2, 0
    endif
  case i_form_id=11
    do REGA5Q_O with 11.00, 9
    if TIPREC$'FI' and L_numper=1
      do REGA5Q_O with 11.01, 1
    endif
    if .t.
      do REGA5Q_O with 11.02, 3
    endif
  case i_form_id=12
    do REGA5Q_O with 12.00, 9
    if TIPREC$'FI' and L_numper=1
      do REGA5Q_O with 12.01, 1
    endif
    if .t.
      do REGA5Q_O with 12.02, 3
    endif
  case i_form_id=99
    * --- controllo per il salto pagina
    if inkey()=27
      i_usr_brk = .T.
    else
      if i_row+i_height+i_formh13>w_t_stnrig
        * --- stampa il piede di pagina
        i_row = w_t_stnrig-i_formh13
        if i_form_pf=13
          do REGA4Q_O with 13, 0
        else
          do REGA5Q_O with -i_form_pf,i_formh13
        endif
        i_row = 0
        i_pag = i_pag+1
        @ prow(),pcol() say chr(12)+chr(13)
        w_s = 0
        * --- stampa l'intestazione di pagina
        if i_form_ph=12
          do REGA4Q_O with 12, 0
        else
          do REGA5Q_O with -i_form_ph,i_form_phh
        endif
      endif
    endif
  case i_form_id=98
    i_form_pf = 0.00
    i_saveh13 = i_formh13
    i_formh13 = 0
endcase
return

procedure REGA5Q_O
* === Procedure REGA5Q_O
parameters i_form_id, i_form_h

* --- controllo per il salto pagina
if i_form_id<11 .and. i_form_id>0
  do REGA4Q_O with 99, i_form_h
  if i_usr_brk
    return
  endif
endif
if i_form_id<0
  i_form_id = -i_form_id
endif
do case
  * --- 1� form
  case i_form_id=1.0
    do frm1_0
  case i_form_id=1.01
    do frm1_01
  case i_form_id=1.02
    do frm1_02
  case i_form_id=1.03
    do frm1_03
  case i_form_id=1.04
    do frm1_04
  case i_form_id=1.05
    do frm1_05
  case i_form_id=1.06
    do frm1_06
  case i_form_id=1.07
    do frm1_07
  case i_form_id=1.08
    do frm1_08
  case i_form_id=1.09
    do frm1_09
  case i_form_id=1.10
    do frm1_10
  case i_form_id=1.11
    do frm1_11
  case i_form_id=1.12
    do frm1_12
  case i_form_id=1.13
    do frm1_13
  case i_form_id=1.14
    do frm1_14
  case i_form_id=1.15
    do frm1_15
  case i_form_id=1.16
    do frm1_16
  case i_form_id=1.17
    do frm1_17
  case i_form_id=1.18
    do frm1_18
  case i_form_id=1.19
    do frm1_19
  case i_form_id=1.20
    do frm1_20
  case i_form_id=1.21
    do frm1_21
  * --- 11� form
  case i_form_id=11.0
    do frm11_0
  case i_form_id=11.01
    do frm11_01
  case i_form_id=11.02
    do frm11_02
  * --- 12� form
  case i_form_id=12.0
    do frm12_0
  case i_form_id=12.01
    do frm12_01
  case i_form_id=12.02
    do frm12_02
endcase
i_row = i_row+i_form_h
return


* --- 1� form
procedure frm1_0
return

* --- frm1_01
procedure frm1_01
return

* --- frm1_02
procedure frm1_02
return

* --- frm1_03
procedure frm1_03
  w_SCompr = &w_t_stcomp
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),0,at_x(3),transform(w_SCompr,""),i_fn
  endif
  w_TIPREG = iif(TIPREC$'FI',CP_TODATE(datreg),' ')
  if POSIZ$'XP'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),1,at_x(11),transform(w_TIPREG,""),i_fn
  endif
  w_TIPREC = iif(TIPREC$'FI',CP_TODATE(datdoc),' ')
  if POSIZ$'XP'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),12,at_x(97),transform(w_TIPREC,""),i_fn
  endif
  w_NUMALFDOC = iif(TIPREC$'FI',alltrim(str(numdoc,15))+alltrim(iif(not empty(nvl(alfdoc,' ')),'/', ' '))+ ALLTRIM(ALFDOC),' ')
  if POSIZ$'XP'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),23,at_x(184),transform(w_NUMALFDOC,""),i_fn
  endif
  w_PNNUMALF = iif(TIPREC$'FI',alltrim(str(pnnumdoc,15))+alltrim(iif(not empty(nvl(pnalfdoc,' ')),'/', ' '))+ ALLTRIM(PNALFDOC),' ')
  if POSIZ$'XP'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),50,at_x(403),transform(w_PNNUMALF,""),i_fn
  endif
  w_DESSET = DESCLF
  if TIPREC$ 'FI' and POSIZ$'XP'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),76,at_x(614),transform(w_DESSET,Repl('X',31)),i_fn
  endif
  w_TIPO = tipdoc
  if POSIZ$"XP"
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),108,at_x(865),transform(w_TIPO,""),i_fn
  endif
  w_SCompr = &w_t_stnorm
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),110,at_x(882),transform(w_SCompr,""),i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),111,at_x(890),transform(IMPONI,V_PV[14]),i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),125,at_x(1006),transform(IMPIVA,V_PV[14]),i_fn
  endif
  w_ALIQUO = ALLTR(TRAN(PERIVA, '999.9'))
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),140,at_x(1122),transform(w_ALIQUO,""),i_fn
  endif
  w_SCompr = &w_t_stcomp
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),145,at_x(1165),transform(w_SCompr,""),i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),146,at_x(1173),transform(DESIVA,Repl('X',16)),i_fn
  endif
  w_SCompr = &w_t_stnorm
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),163,at_x(1304),transform(w_SCompr,""),i_fn
  endif
  w_IMPTOT = IMPONI+IMPIVA
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),164,at_x(1313),transform(w_IMPTOT,V_PV[14]),i_fn
  endif
  w_SCompr = &w_t_stcomp
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),178,at_x(1429),transform(w_SCompr,""),i_fn
  endif
  w_COMPET = IIF(VAL(COMPET)>9.or.VAL(COMPET)=0, "", " ")+COMPET
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),179,at_x(1437),transform(w_COMPET,Repl('X',9)),i_fn
  w_SCompr = &w_t_stnorm
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(57-56),189,at_x(1514),transform(w_SCompr,""),i_fn
  endif
return

* --- frm1_04
procedure frm1_04
  w_NOTE = '*'+iif(NVL(FLPROV,'N')='S' AND TIPREC$ 'FI','R*','')+IIF(NOT EMPTY(FLPNUM),'N*',IIF(NOT EMPTY(FLPDAT),'D*',''))
  if (NVL(FLPROV,'N')='S' AND TIPREC$ 'FI') or (L_FLDEFI<>'S' AND (NOT EMPTY(FLPNUM) OR NOT EMPTY(FLPDAT))) AND POSIZ$"XP"
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(95-94),22,at_x(178),transform(w_NOTE,""),i_fn
  endif
return

* --- frm1_05
procedure frm1_05
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(133-132),3,at_x(24),transform(CODIVA,""),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(133-132),10,at_x(80),transform(PERIVA, '999.9'),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(133-132),18,at_x(146),transform(DESIVA,""),i_fn
return

* --- frm1_06
procedure frm1_06
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(171-170),8,at_x(64),"Documenti Registrati nel Periodo:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(171-170),42,at_x(336),transform(IMPSTA,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(171-170),56,at_x(450),transform(IVASTA,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(171-170),71,at_x(568),transform(IVISTA,V_PV[14]),i_fn
return

* --- frm1_07
procedure frm1_07
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),0,at_x(0),"- Documenti Competenza del Periodo Prec.:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),42,at_x(336),transform(IMPPRE,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),56,at_x(450),transform(IVAPRE,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(209-208),71,at_x(568),transform(IVIPRE,V_PV[14]),i_fn
return

* --- frm1_08
procedure frm1_08
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(247-246),1,at_x(8),"+ Documenti Registrati nel Periodo Seg.:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(247-246),42,at_x(336),transform(IMPSEG,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(247-246),56,at_x(450),transform(IVASEG,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(247-246),71,at_x(568),transform(IVISEG,V_PV[14]),i_fn
return

* --- frm1_09
procedure frm1_09
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(285-284),5,at_x(40),"- Fatture ad Esigibilita' Differita:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(285-284),42,at_x(336),transform(IMPFAD,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(285-284),56,at_x(450),transform(IVAFAD,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(285-284),71,at_x(568),transform(IVIFAD,V_PV[14]),i_fn
return

* --- frm1_10
procedure frm1_10
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(323-322),3,at_x(24),"+ Pagamenti ad Esigibilita' Differita:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(323-322),42,at_x(336),transform(IMPIND,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(323-322),56,at_x(450),transform(IVAIND,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(323-322),71,at_x(568),transform(IVIIND,V_PV[14]),i_fn
return

* --- frm1_11
procedure frm1_11
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),11,at_x(88),"= Doc. Competenza del Periodo:",i_fn
  w_RIEIMP = (IMPSTA+IMPSEG+IMPIND)-(IMPPRE+IMPFAD)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),42,at_x(336),transform(w_RIEIMP,V_PV[14]),i_fn
  w_RIEIVA = (IVASTA+IVASEG+IVAIND)-(IVAPRE+IVAFAD)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),56,at_x(448),transform(w_RIEIVA,V_PV[14]),i_fn
  w_RIEIVI = (IVISTA+IVISEG+IVIIND)-(IVIPRE+IVIFAD)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),71,at_x(568),transform(w_RIEIVI,V_PV[14]),i_fn
  w_RIEIMP1 = IIF(NVL(PERIVA,0)=0,0,w_RIEIMP)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),87,at_x(696),transform(w_RIEIMP1,V_PV[20]),i_fn
   endif
   w_APIMP = w_APIMP+w_RIEIMP1
  w_RIEIMP2 = IIF(NVL(PERIVA,0)=0,w_RIEIMP,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),90,at_x(724),transform(w_RIEIMP2,V_PV[20]),i_fn
   endif
   w_EPIMP = w_EPIMP+w_RIEIMP2
  w_RIEIVA1 = IIF(NVL(PERIVA,0)=0,0,w_RIEIVA)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),94,at_x(752),transform(w_RIEIVA1,V_PV[20]),i_fn
   endif
   w_APIVA = w_APIVA+w_RIEIVA1
  w_RIEIVA2 = IIF(NVL(PERIVA,0)=0,w_RIEIVA,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),97,at_x(780),transform(w_RIEIVA2,V_PV[20]),i_fn
   endif
   w_EPIVA = w_EPIVA+w_RIEIVA2
  w_ARIMP1 = IIF(NVL(PERIVA,0)=0,0,PROIMP)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),101,at_x(808),transform(w_ARIMP1,V_PV[20]),i_fn
   endif
  w_ARIMP2 = IIF(NVL(PERIVA,0)=0,PROIMP,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),104,at_x(836),transform(w_ARIMP2,V_PV[20]),i_fn
   endif
  w_ARIVA1 = IIF(NVL(PERIVA,0)=0,0,PROIVA)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),108,at_x(864),transform(w_ARIVA1,V_PV[20]),i_fn
   endif
  w_ARIVA2 = IIF(NVL(PERIVA,0)=0,PROIVA,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),111,at_x(893),transform(w_ARIVA2,V_PV[20]),i_fn
   endif
  w_APIVI1 = IIF(NVL(PERIVA,0)=0,0,w_RIEIVI)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),115,at_x(920),transform(w_APIVI1,V_PV[20]),i_fn
   endif
   w_APIVI = w_APIVI+w_APIVI1
  w_APIVI2 = IIF(NVL(PERIVA,0)=0,w_RIEIVI,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),118,at_x(949),transform(w_APIVI2,V_PV[20]),i_fn
   endif
   w_EPIVI = w_EPIVI+w_APIVI2
  w_ARIVI1 = IIF(NVL(PERIVA,0)=0,0,PROIVI)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),122,at_x(976),transform(w_ARIVI1,V_PV[20]),i_fn
   endif
  w_ARIVI2 = IIF(NVL(PERIVA,0)=0,PROIVI,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(361-360),126,at_x(1008),transform(w_ARIVI2,V_PV[20]),i_fn
   endif
  w_SPAZIO = Space(10)
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(380-360),42,at_x(336),transform(w_SPAZIO,""),i_fn
return

* --- frm1_12
procedure frm1_12
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(418-417),41,at_x(328),"Totale Documento:",i_fn
  w_TOTDOC = Right(Space(18)+ALLTRIM(TRAN(TOTDOC, V_PV[40+(18*NVL(DECTOT,0))])),18)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(418-417),59,at_x(473),transform(w_TOTDOC,""),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(418-417),78,at_x(629),transform(SIMVAL,Repl('X',5)),i_fn
return

* --- frm1_13
procedure frm1_13
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(456-455),17,at_x(136),"Lo spazio sottostante di questa pagina non e' stato utilizzato ed e' da",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(456-455),90,at_x(720),"considerarsi annullato",i_fn
return

* --- frm1_14
procedure frm1_14
return

* --- frm1_15
procedure frm1_15
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(513-512),83,at_x(664),"Imponibile Progr.IVA Detr.Progr.IVA Indet.Progr.",i_fn
return

* --- frm1_16
procedure frm1_16
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(551-550),24,at_x(192),"Totale a Credito:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(551-550),42,at_x(336),transform(w_APIMP,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(551-550),56,at_x(448),transform(w_APIVA,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(551-550),71,at_x(568),transform(w_APIVI,V_PV[14]),i_fn
  w_ARIMP = L_ARIMP
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(551-550),86,at_x(688),transform(w_ARIMP,V_PV[14]),i_fn
  w_ARIVA = L_ARIVA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(551-550),101,at_x(808),transform(w_ARIVA,V_PV[14]),i_fn
  w_ARIVI = L_ARIVI
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(551-550),115,at_x(920),transform(w_ARIVI,V_PV[14]),i_fn
return

* --- frm1_17
procedure frm1_17
  if L_FLPROR='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(589-588),13,at_x(104),"Totali al Netto di Prorata :",i_fn
  endif
  text01 = alltrim(str(L_PERPRO)) + '%'
  if L_FLPROR='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(589-588),42,at_x(336),transform(text01,""),i_fn
  endif
  TOT_IVADET = (w_APIVA*L_PERPRO)/100
  if L_FLPROR='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(589-588),56,at_x(448),transform(TOT_IVADET,V_PV[14]),i_fn
  endif
  TOT_IVAPRO = (L_ARIVA*L_PERPRO)/100
  if L_FLPROR='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(589-588),101,at_x(808),transform(TOT_IVAPRO,V_PV[14]),i_fn
  endif
return

* --- frm1_18
procedure frm1_18
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(627-626),19,at_x(152),"Totale altre Aliquote:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(627-626),42,at_x(336),transform(w_EPIMP,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(627-626),56,at_x(448),transform(w_EPIVA,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(627-626),71,at_x(568),transform(w_EPIVI,V_PV[14]),i_fn
  w_ERIMP = L_ERIMP
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(627-626),86,at_x(688),transform(w_ERIMP,V_PV[14]),i_fn
  w_ERIVA = L_ERIVA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(627-626),101,at_x(808),transform(w_ERIVA,V_PV[14]),i_fn
  w_ERIVI = L_ERIVI
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(627-626),115,at_x(920),transform(w_ERIVI,V_PV[14]),i_fn
return

* --- frm1_19
procedure frm1_19
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(665-664),22,at_x(176),"Totale Complessivo:",i_fn
  w_APIMPT = w_APIMP+w_EPIMP
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(665-664),42,at_x(336),transform(w_APIMPT,V_PV[14]),i_fn
  w_APIVAT = w_APIVA+w_EPIVA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(665-664),56,at_x(448),transform(w_APIVAT,V_PV[14]),i_fn
  w_APIVIT = w_APIVI+w_EPIVI
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(665-664),71,at_x(568),transform(w_APIVIT,V_PV[14]),i_fn
  w_ARIMPT = L_ARIMP+L_ERIMP
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(665-664),86,at_x(688),transform(w_ARIMPT,V_PV[14]),i_fn
  w_ARIVAT = L_ARIVA+L_ERIVA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(665-664),101,at_x(808),transform(w_ARIVAT,V_PV[14]),i_fn
  w_ARIVIT = L_ARIVI+L_ERIVI
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(665-664),115,at_x(920),transform(w_ARIVIT,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(684-664),20,at_x(160),"��������������������������������������������������������������������������������",i_fn
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(684-664),100,at_x(800),"�����������������������������",i_fn
return

* --- frm1_20
procedure frm1_20
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(722-721),18,at_x(144),"Lo spazio sottostante di questa pagina non e' stato utilizzato ed e' da",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(722-721),90,at_x(720),"considerarsi annullato",i_fn
return

* --- frm1_21
procedure frm1_21
  i_row = w_t_stnrig-i_formh13
return

* --- 11� form
procedure frm11_0
  w_SETTA = " "
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),0,at_x(2),transform(w_SETTA,"X"),i_fn
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),0,at_x(2),"REGISTRO IVA ACQUISTI",i_fn
  endif
  PREFIS = RIGHT(SPACE(20)+ALLTRIM(L_PREFIS),20)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),88,at_x(704),transform(PREFIS,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),111,at_x(890),"Pag.",i_fn
  endif
  NUMPAG = L_PRPARI+I_PAG
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),125,at_x(1000),transform(NUMPAG,"9999999"),i_fn
   STPAG=NUMPAG
  endif
  p_RAGAZI = g_RAGAZI
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),0,at_x(2),transform(p_RAGAZI,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),103,at_x(826),"Partita IVA:",i_fn
  endif
  p_PARTO = RIGHT(SPACE(16) + ALLTRIM(L_PIVAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),116,at_x(928),transform(p_PARTO,""),i_fn
  endif
  w_DATIAZ = trim(L_INDAZI)+' - '+L_CAPAZI+' - '+TRIM(L_LOCAZI)+IIF(EMPTY(L_PROAZI),'',' ( '+L_PROAZI+' )')
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),0,at_x(2),transform(w_DATIAZ,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),100,at_x(802),"Codice fiscale:",i_fn
  endif
  p_CODO = RIGHT(SPACE(16) + ALLTRIM(L_COFAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),116,at_x(928),transform(p_CODO,""),i_fn
  endif
  if L_FLDEFI<>'S'  AND TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(116-0),41,at_x(335),"*N*=Numero fuori sequenza *D*=Data fuori sequenza *R*=Registraz. non confermata",i_fn
  endif
  if .f.
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(138-0),0,at_x(2),transform(w_Set,""),i_fn
   endif
  w_INTESTA = CompString()
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(138-0),2,at_x(18),transform(w_INTESTA,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),1,at_x(8),"Dal:",i_fn
  P_DATINI = L_DATINI
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),5,at_x(42),transform(P_DATINI,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),17,at_x(142),"Al:",i_fn
  P_DATFIN = L_DATFIN
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),20,at_x(167),transform(P_DATFIN,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),40,at_x(320),"Importi espressi in:",i_fn
  P_VALSIM = G_VALSIM
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),60,at_x(487),transform(P_VALSIM,""),i_fn
  P_DESVAL = L_DESVAL
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),65,at_x(520),transform(P_DESVAL,""),i_fn
  L_STAMPATO = .t.
  if .f.
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),101,at_x(813),transform(L_STAMPATO,""),i_fn
   endif
return

* --- frm11_01
procedure frm11_01
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(206-205),1,at_x(8),"Credito IVA Anno Precedente:",i_fn
  P_CREINI = ALLTRIM(TRAN(L_CREINI, V_PV(20)))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(206-205),29,at_x(235),transform(P_CREINI,""),i_fn
return

* --- frm11_02
procedure frm11_02
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(246-243),0,at_x(2),"D.Reg. D.Doc. N.Prot.      N.Doc.           Den. o Rag.Soc.  Tipo",i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(246-243),68,at_x(551),"Imponibile      Imposta Aliquota IVA    Importo Totale",i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(246-243),123,at_x(991),"Comp.",i_fn
  endif
  if TIPREC='R'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(270-243),1,at_x(8)," C.I.     %      Descrizione",i_fn
  endif
  if TIPREC='R'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(270-243),37,at_x(297),"Imponibile Periodo IVA Detr.Periodo IVA Indet.Periodo",i_fn
  endif
return

* --- 12� form
procedure frm12_0
  w_SETTA = " "
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),0,at_x(2),transform(w_SETTA,"X"),i_fn
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),0,at_x(2),"REGISTRO IVA ACQUISTI",i_fn
  endif
  PREFIS = RIGHT(SPACE(20)+ALLTRIM(L_PREFIS),20)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),88,at_x(704),transform(PREFIS,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),111,at_x(890),"Pag.",i_fn
  endif
  NUMPAG = L_PRPARI+I_PAG
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),125,at_x(1000),transform(NUMPAG,"9999999"),i_fn
   STPAG=NUMPAG
  endif
  p_RAGAZI = g_RAGAZI
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),0,at_x(2),transform(p_RAGAZI,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),103,at_x(826),"Partita IVA:",i_fn
  endif
  p_PARTO = RIGHT(SPACE(16) + ALLTRIM(L_PIVAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),116,at_x(928),transform(p_PARTO,""),i_fn
  endif
  w_DATIAZ = trim(L_INDAZI)+' - '+L_CAPAZI+' - '+TRIM(L_LOCAZI)+IIF(EMPTY(L_PROAZI),'',' ( '+L_PROAZI+' )')
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),0,at_x(2),transform(w_DATIAZ,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),100,at_x(802),"Codice fiscale:",i_fn
  endif
  p_CODO = RIGHT(SPACE(16) + ALLTRIM(L_COFAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),116,at_x(928),transform(p_CODO,""),i_fn
  endif
  if L_FLDEFI<>'S'  AND TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(112-0),41,at_x(335),"*N*=Numero fuori sequenza *D*=Data fuori sequenza *R*=Registraz. non confermata",i_fn
  endif
  w_INTESTA = CompString()
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(138-0),2,at_x(18),transform(w_INTESTA,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),1,at_x(8),"Dal:",i_fn
  P_DATINI = L_DATINI
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),5,at_x(42),transform(P_DATINI,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),17,at_x(142),"Al:",i_fn
  P_DATFIN = L_DATFIN
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),20,at_x(167),transform(P_DATFIN,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),40,at_x(320),"Importi espressi in:",i_fn
  P_VALSIM = G_VALSIM
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),60,at_x(487),transform(P_VALSIM,""),i_fn
  P_DESVAL = L_DESVAL
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),65,at_x(520),transform(P_DESVAL,""),i_fn
return

* --- frm12_01
procedure frm12_01
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(206-205),1,at_x(8),"Credito IVA Anno Precedente:",i_fn
  P_CREINI = ALLTRIM(TRAN(L_CREINI, V_PV(20)))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(206-205),29,at_x(235),transform(P_CREINI,""),i_fn
return

* --- frm12_02
procedure frm12_02
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(246-243),0,at_x(2),"D.Reg. D.Doc. N.Prot.      N.Doc.           Den. o Rag.Soc.  Tipo",i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(246-243),68,at_x(551),"Imponibile      Imposta Aliquota IVA    Importo Totale",i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(246-243),123,at_x(991),"Comp.",i_fn
  endif
  if TIPREC='R'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(270-243),1,at_x(8)," C.I.     %      Descrizione",i_fn
  endif
  if TIPREC='R'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(270-243),37,at_x(297),"Imponibile Periodo IVA Detr.Periodo IVA Indet.Periodo",i_fn
  endif
return

function at_x
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/6
  return i_pos

function at_y
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/13
  return i_pos

procedure F_Say
  parameter y,py,x,px,s,f
  * --- Questa funzione corregge un errore del driver "Generica solo testo"
  *     di Windows che aggiunge spazi oltre la 89 colonna
  *     Inoltre in Windows sostituisce il carattere 196 con un '-'
  if y=-1
    y = prow()
    x = pcol()
  endif
  @ y,x say s
  return

PROCEDURE CPLU_GO
parameter i_recpos

if i_recpos<=0 .or. i_recpos>reccount()
  if reccount()<>0
    goto bottom
    if .not. eof()
      skip
    endif
  endif  
else
  goto i_recpos
endif
return

* --- Area Manuale = Functions & Procedures 
* --- REGACQ_O

FUNCTION CompString
* Composizione stringa per intestazione pagina
private w_r_Stringa
w_r_Stringa=Space(35)

if TIPREC$'F'
   w_r_Stringa='Registro IVA acquisti num. '+Alltrim(str(L_NUMREG,6,0))+ '  anno: ' + alltrim(str(year(l_datini)))
else
     if TIPREC$'R' 
        w_r_Stringa='Riepilogo registro IVA acquisti num. '+Alltrim(str(L_NUMREG,6,0))+ '  anno: ' + alltrim(str(year(l_datini)))
     else
         if tiprec='I'
           w_r_Stringa='Pagamenti ad Esigibilit� Differita.'
         endif
     endif
endif

return (w_r_Stringa)

FUNCTION EndOfGroup()
*Verifica fine gruppo e fine pagina per stampa
*dicitura di annullamento spazio sottostante

Private w_Ret, w_CtrlREC
w_Ret=.f.
w_CtrlREC=TIPREC
skip 
If w_CtrlREC<>TIPREC .and. i_Row<>ts_RowOk .and. (!empty(TIPREC).or.eof()) .and. empty(TOTDOC)
      w_Ret=.t.
endif
skip -1

Return (w_Ret)

* --- Fine Area Manuale 
