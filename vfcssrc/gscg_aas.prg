* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_aas                                                        *
*              Generazione scritture di assestamento                           *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [116] [VRS_199]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-03                                                      *
* Last revis.: 2012-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_aas"))

* --- Class definition
define class tgscg_aas as StdForm
  Top    = 15
  Left   = 51

  * --- Standard Properties
  Width  = 575
  Height = 344+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-04"
  HelpContextID=143271273
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=70

  * --- Constant Properties
  ASSESTAM_IDX = 0
  CAU_CONT_IDX = 0
  INVENTAR_IDX = 0
  CONTI_IDX = 0
  ESERCIZI_IDX = 0
  CONTROPA_IDX = 0
  AZIENDA_IDX = 0
  cFile = "ASSESTAM"
  cKeySelect = "ASSERIAL"
  cKeyWhere  = "ASSERIAL=this.w_ASSERIAL"
  cKeyWhereODBC = '"ASSERIAL="+cp_ToStrODBC(this.w_ASSERIAL)';

  cKeyWhereODBCqualified = '"ASSESTAM.ASSERIAL="+cp_ToStrODBC(this.w_ASSERIAL)';

  cPrg = "gscg_aas"
  cComment = "Generazione scritture di assestamento"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ASSERIAL = space(10)
  w_ASDATREG = ctod('  /  /  ')
  o_ASDATREG = ctod('  /  /  ')
  w_ASDATFIN = ctod('  /  /  ')
  o_ASDATFIN = ctod('  /  /  ')
  w_ASNUMDOC = 0
  w_ASALFDOC = space(10)
  w_ASDATDOC = ctod('  /  /  ')
  w_ASDESCRI = space(50)
  w_ASFLDOCE = space(1)
  w_ASFLDOCR = space(1)
  w_ASRAGSCR = space(1)
  w_ASFLRATE = space(1)
  o_ASFLRATE = space(1)
  w_ASRRPROV = space(1)
  w_ASFLCESP = space(1)
  w_ASFLRIMA = space(1)
  w_NUMINV = space(6)
  w_TIPVAL = space(1)
  w_ASFLSTOR = space(1)
  o_ASFLSTOR = space(1)
  w_ASFLSTOR = space(1)
  w_ASDTRGST = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CCOBSO = ctod('  /  /  ')
  w_AZIENDA = space(5)
  w_FLASSE = space(1)
  w_CODESE = space(4)
  o_CODESE = space(4)
  w_CONCON = ctod('  /  /  ')
  w_STALIG = ctod('  /  /  ')
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_ASDATINI = ctod('  /  /  ')
  w_COCODAZI = space(5)
  w_COFATEME = space(15)
  w_COIVADEB = space(15)
  w_COCAUFDE = space(5)
  w_COCASFDE = space(5)
  w_COFATRIC = space(15)
  w_COIVACRE = space(15)
  w_COCAUFDR = space(5)
  w_COCASFDR = space(5)
  w_COCAURIM = space(5)
  w_COCASRIM = space(5)
  w_CORATATT = space(15)
  w_CORATPAS = space(15)
  w_CORISATT = space(15)
  w_CORISPAS = space(15)
  w_COCAURER = space(5)
  w_COCASRER = space(5)
  w_COCAUCES = space(5)
  w_COCASCES = space(5)
  w_COCAFRER = space(5)
  w_COCONINC = space(15)
  w_COCONIMB = space(15)
  w_COCONTRA = space(15)
  w_COCONBOL = space(15)
  w_COCONDIC = space(15)
  w_COCONARR = space(15)
  w_COTIPCON = space(1)
  w_RAGGSCRI = space(1)
  o_RAGGSCRI = space(1)
  w_ACQU = space(1)
  w_VEND = space(1)
  w_MSG = space(50)
  w_CHIUSO = space(1)
  w_COACQINC = space(15)
  w_COACQIMB = space(15)
  w_COACQTRA = space(15)
  w_COACQBOL = space(15)
  w_COINDIVA = space(1)
  w_FLESER = .F.
  o_FLESER = .F.
  w_PANUMREG = space(6)
  w_PACODESE = space(4)
  w_PADATREG = ctod('  /  /  ')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_ASSERIAL = this.W_ASSERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ASSESTAM','gscg_aas')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_aasPag1","gscg_aas",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(1).HelpContextID = 119229975
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oASSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='INVENTAR'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='CONTROPA'
    this.cWorkTables[6]='AZIENDA'
    this.cWorkTables[7]='ASSESTAM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ASSESTAM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ASSESTAM_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ASSERIAL = NVL(ASSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ASSESTAM where ASSERIAL=KeySet.ASSERIAL
    *
    i_nConn = i_TableProp[this.ASSESTAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASSESTAM_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ASSESTAM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ASSESTAM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ASSESTAM '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ASSERIAL',this.w_ASSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_NUMINV = space(6)
        .w_TIPVAL = '1'
        .w_CCOBSO = ctod("  /  /  ")
        .w_AZIENDA = I_CODAZI
        .w_FLASSE = space(1)
        .w_CODESE = g_CODESE
        .w_CONCON = ctod("  /  /  ")
        .w_STALIG = ctod("  /  /  ")
        .w_INIESE = g_INIESE
        .w_FINESE = g_FINESE
        .w_COCODAZI = i_CODAZI
        .w_COFATEME = space(15)
        .w_COIVADEB = space(15)
        .w_COCAUFDE = space(5)
        .w_COCASFDE = space(5)
        .w_COFATRIC = space(15)
        .w_COIVACRE = space(15)
        .w_COCAUFDR = space(5)
        .w_COCASFDR = space(5)
        .w_COCAURIM = space(5)
        .w_COCASRIM = space(5)
        .w_CORATATT = space(15)
        .w_CORATPAS = space(15)
        .w_CORISATT = space(15)
        .w_CORISPAS = space(15)
        .w_COCAURER = space(5)
        .w_COCASRER = space(5)
        .w_COCAUCES = space(5)
        .w_COCASCES = space(5)
        .w_COCAFRER = space(5)
        .w_COCONINC = space(15)
        .w_COCONIMB = space(15)
        .w_COCONTRA = space(15)
        .w_COCONBOL = space(15)
        .w_COCONDIC = space(15)
        .w_COCONARR = space(15)
        .w_COTIPCON = space(1)
        .w_RAGGSCRI = IIF(.w_ASFLDOCE<>'S' AND .w_ASFLDOCR<>'S',' ',.w_RAGGSCRI)
        .w_VEND = g_VEND
        .w_MSG = space(50)
        .w_CHIUSO = space(1)
        .w_COACQINC = space(15)
        .w_COACQIMB = space(15)
        .w_COACQTRA = space(15)
        .w_COACQBOL = space(15)
        .w_COINDIVA = space(1)
        .w_FLESER = .T.
        .w_PANUMREG = space(6)
        .w_PACODESE = space(4)
        .w_PADATREG = ctod("  /  /  ")
        .w_ASSERIAL = NVL(ASSERIAL,space(10))
        .op_ASSERIAL = .w_ASSERIAL
        .w_ASDATREG = NVL(cp_ToDate(ASDATREG),ctod("  /  /  "))
        .w_ASDATFIN = NVL(cp_ToDate(ASDATFIN),ctod("  /  /  "))
        .w_ASNUMDOC = NVL(ASNUMDOC,0)
        .w_ASALFDOC = NVL(ASALFDOC,space(10))
        .w_ASDATDOC = NVL(cp_ToDate(ASDATDOC),ctod("  /  /  "))
        .w_ASDESCRI = NVL(ASDESCRI,space(50))
        .w_ASFLDOCE = NVL(ASFLDOCE,space(1))
        .w_ASFLDOCR = NVL(ASFLDOCR,space(1))
        .w_ASRAGSCR = NVL(ASRAGSCR,space(1))
        .w_ASFLRATE = NVL(ASFLRATE,space(1))
        .w_ASRRPROV = NVL(ASRRPROV,space(1))
        .w_ASFLCESP = NVL(ASFLCESP,space(1))
        .w_ASFLRIMA = NVL(ASFLRIMA,space(1))
          .link_1_16('Load')
        .w_ASFLSTOR = NVL(ASFLSTOR,space(1))
        .w_ASFLSTOR = NVL(ASFLSTOR,space(1))
        .w_ASDTRGST = NVL(cp_ToDate(ASDTRGST),ctod("  /  /  "))
        .w_OBTEST = .w_ASDATREG
          .link_1_24('Load')
          .link_1_26('Load')
        .w_ASDATINI = NVL(cp_ToDate(ASDATINI),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
          .link_1_42('Load')
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .w_ACQU = g_ACQU
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ASSESTAM')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_79.enabled = this.oPgFrm.Page1.oPag.oBtn_1_79.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ASSERIAL = space(10)
      .w_ASDATREG = ctod("  /  /  ")
      .w_ASDATFIN = ctod("  /  /  ")
      .w_ASNUMDOC = 0
      .w_ASALFDOC = space(10)
      .w_ASDATDOC = ctod("  /  /  ")
      .w_ASDESCRI = space(50)
      .w_ASFLDOCE = space(1)
      .w_ASFLDOCR = space(1)
      .w_ASRAGSCR = space(1)
      .w_ASFLRATE = space(1)
      .w_ASRRPROV = space(1)
      .w_ASFLCESP = space(1)
      .w_ASFLRIMA = space(1)
      .w_NUMINV = space(6)
      .w_TIPVAL = space(1)
      .w_ASFLSTOR = space(1)
      .w_ASFLSTOR = space(1)
      .w_ASDTRGST = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_CCOBSO = ctod("  /  /  ")
      .w_AZIENDA = space(5)
      .w_FLASSE = space(1)
      .w_CODESE = space(4)
      .w_CONCON = ctod("  /  /  ")
      .w_STALIG = ctod("  /  /  ")
      .w_INIESE = ctod("  /  /  ")
      .w_FINESE = ctod("  /  /  ")
      .w_ASDATINI = ctod("  /  /  ")
      .w_COCODAZI = space(5)
      .w_COFATEME = space(15)
      .w_COIVADEB = space(15)
      .w_COCAUFDE = space(5)
      .w_COCASFDE = space(5)
      .w_COFATRIC = space(15)
      .w_COIVACRE = space(15)
      .w_COCAUFDR = space(5)
      .w_COCASFDR = space(5)
      .w_COCAURIM = space(5)
      .w_COCASRIM = space(5)
      .w_CORATATT = space(15)
      .w_CORATPAS = space(15)
      .w_CORISATT = space(15)
      .w_CORISPAS = space(15)
      .w_COCAURER = space(5)
      .w_COCASRER = space(5)
      .w_COCAUCES = space(5)
      .w_COCASCES = space(5)
      .w_COCAFRER = space(5)
      .w_COCONINC = space(15)
      .w_COCONIMB = space(15)
      .w_COCONTRA = space(15)
      .w_COCONBOL = space(15)
      .w_COCONDIC = space(15)
      .w_COCONARR = space(15)
      .w_COTIPCON = space(1)
      .w_RAGGSCRI = space(1)
      .w_ACQU = space(1)
      .w_VEND = space(1)
      .w_MSG = space(50)
      .w_CHIUSO = space(1)
      .w_COACQINC = space(15)
      .w_COACQIMB = space(15)
      .w_COACQTRA = space(15)
      .w_COACQBOL = space(15)
      .w_COINDIVA = space(1)
      .w_FLESER = .f.
      .w_PANUMREG = space(6)
      .w_PACODESE = space(4)
      .w_PADATREG = ctod("  /  /  ")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_ASDATREG = i_DATSYS
          .DoRTCalc(3,9,.f.)
        .w_ASRAGSCR = IIF(.cFunction='Load',.w_RAGGSCRI,.w_ASRAGSCR)
          .DoRTCalc(11,11,.f.)
        .w_ASRRPROV = 'N'
        .DoRTCalc(13,15,.f.)
          if not(empty(.w_NUMINV))
          .link_1_16('Full')
          endif
        .w_TIPVAL = '1'
          .DoRTCalc(17,18,.f.)
        .w_ASDTRGST = IIF(.w_ASFLSTOR<>'S' OR !.w_FLESER,cp_CharToDate('  -  -    '),IIF(.w_ASDATFIN<>g_FINESE OR .w_ASDATREG>g_FINESE,.w_ASDATREG+1,g_FINESE+1))
        .w_OBTEST = .w_ASDATREG
          .DoRTCalc(21,21,.f.)
        .w_AZIENDA = I_CODAZI
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_AZIENDA))
          .link_1_24('Full')
          endif
          .DoRTCalc(23,23,.f.)
        .w_CODESE = g_CODESE
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_CODESE))
          .link_1_26('Full')
          endif
          .DoRTCalc(25,26,.f.)
        .w_INIESE = g_INIESE
        .w_FINESE = g_FINESE
        .w_ASDATINI = IIF(.cFunction='Load',.w_INIESE,.w_ASDATINI)
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .w_COCODAZI = i_CODAZI
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_COCODAZI))
          .link_1_42('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
          .DoRTCalc(31,56,.f.)
        .w_RAGGSCRI = IIF(.w_ASFLDOCE<>'S' AND .w_ASFLDOCR<>'S',' ',.w_RAGGSCRI)
        .w_ACQU = g_ACQU
        .w_VEND = g_VEND
          .DoRTCalc(60,66,.f.)
        .w_FLESER = .T.
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ASSESTAM')
    this.DoRTCalc(68,70,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_79.enabled = this.oPgFrm.Page1.oPag.oBtn_1_79.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_aas
    * ---- Marco come modificata la maschera
    * ---- x impedire il lancio del batch senza aver immesso
    * ---- tutti i dati necessari
    this.bupdated=.t.
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ASSESTAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASSESTAM_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEASS","i_codazi,w_ASSERIAL")
      .op_codazi = .w_codazi
      .op_ASSERIAL = .w_ASSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oASSERIAL_1_1.enabled = !i_bVal
      .Page1.oPag.oASDATREG_1_2.enabled = i_bVal
      .Page1.oPag.oASDATFIN_1_3.enabled = i_bVal
      .Page1.oPag.oASNUMDOC_1_4.enabled = i_bVal
      .Page1.oPag.oASALFDOC_1_5.enabled = i_bVal
      .Page1.oPag.oASDATDOC_1_6.enabled = i_bVal
      .Page1.oPag.oASDESCRI_1_7.enabled = i_bVal
      .Page1.oPag.oASFLDOCE_1_8.enabled = i_bVal
      .Page1.oPag.oASFLDOCR_1_9.enabled = i_bVal
      .Page1.oPag.oASFLRATE_1_11.enabled = i_bVal
      .Page1.oPag.oASRRPROV_1_12.enabled = i_bVal
      .Page1.oPag.oASFLCESP_1_13.enabled = i_bVal
      .Page1.oPag.oASFLRIMA_1_15.enabled = i_bVal
      .Page1.oPag.oNUMINV_1_16.enabled = i_bVal
      .Page1.oPag.oTIPVAL_1_17.enabled = i_bVal
      .Page1.oPag.oASFLSTOR_1_18.enabled = i_bVal
      .Page1.oPag.oASFLSTOR_1_19.enabled = i_bVal
      .Page1.oPag.oASDTRGST_1_20.enabled = i_bVal
      .Page1.oPag.oRAGGSCRI_1_74.enabled = i_bVal
      .Page1.oPag.oBtn_1_14.enabled = .Page1.oPag.oBtn_1_14.mCond()
      .Page1.oPag.oBtn_1_21.enabled = i_bVal
      .Page1.oPag.oBtn_1_79.enabled = .Page1.oPag.oBtn_1_79.mCond()
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      .Page1.oPag.oObj_1_72.enabled = i_bVal
      .Page1.oPag.oObj_1_86.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ASSESTAM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ASSESTAM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASSERIAL,"ASSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASDATREG,"ASDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASDATFIN,"ASDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASNUMDOC,"ASNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASALFDOC,"ASALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASDATDOC,"ASDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASDESCRI,"ASDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASFLDOCE,"ASFLDOCE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASFLDOCR,"ASFLDOCR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASRAGSCR,"ASRAGSCR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASFLRATE,"ASFLRATE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASRRPROV,"ASRRPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASFLCESP,"ASFLCESP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASFLRIMA,"ASFLRIMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASFLSTOR,"ASFLSTOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASFLSTOR,"ASFLSTOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASDTRGST,"ASDTRGST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASDATINI,"ASDATINI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ASSESTAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASSESTAM_IDX,2])
    i_lTable = "ASSESTAM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ASSESTAM_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ASSESTAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ASSESTAM_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ASSESTAM_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEASS","i_codazi,w_ASSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ASSESTAM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ASSESTAM')
        i_extval=cp_InsertValODBCExtFlds(this,'ASSESTAM')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ASSERIAL,ASDATREG,ASDATFIN,ASNUMDOC,ASALFDOC"+;
                  ",ASDATDOC,ASDESCRI,ASFLDOCE,ASFLDOCR,ASRAGSCR"+;
                  ",ASFLRATE,ASRRPROV,ASFLCESP,ASFLRIMA,ASFLSTOR"+;
                  ",ASDTRGST,ASDATINI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ASSERIAL)+;
                  ","+cp_ToStrODBC(this.w_ASDATREG)+;
                  ","+cp_ToStrODBC(this.w_ASDATFIN)+;
                  ","+cp_ToStrODBC(this.w_ASNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_ASALFDOC)+;
                  ","+cp_ToStrODBC(this.w_ASDATDOC)+;
                  ","+cp_ToStrODBC(this.w_ASDESCRI)+;
                  ","+cp_ToStrODBC(this.w_ASFLDOCE)+;
                  ","+cp_ToStrODBC(this.w_ASFLDOCR)+;
                  ","+cp_ToStrODBC(this.w_ASRAGSCR)+;
                  ","+cp_ToStrODBC(this.w_ASFLRATE)+;
                  ","+cp_ToStrODBC(this.w_ASRRPROV)+;
                  ","+cp_ToStrODBC(this.w_ASFLCESP)+;
                  ","+cp_ToStrODBC(this.w_ASFLRIMA)+;
                  ","+cp_ToStrODBC(this.w_ASFLSTOR)+;
                  ","+cp_ToStrODBC(this.w_ASDTRGST)+;
                  ","+cp_ToStrODBC(this.w_ASDATINI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ASSESTAM')
        i_extval=cp_InsertValVFPExtFlds(this,'ASSESTAM')
        cp_CheckDeletedKey(i_cTable,0,'ASSERIAL',this.w_ASSERIAL)
        INSERT INTO (i_cTable);
              (ASSERIAL,ASDATREG,ASDATFIN,ASNUMDOC,ASALFDOC,ASDATDOC,ASDESCRI,ASFLDOCE,ASFLDOCR,ASRAGSCR,ASFLRATE,ASRRPROV,ASFLCESP,ASFLRIMA,ASFLSTOR,ASDTRGST,ASDATINI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ASSERIAL;
                  ,this.w_ASDATREG;
                  ,this.w_ASDATFIN;
                  ,this.w_ASNUMDOC;
                  ,this.w_ASALFDOC;
                  ,this.w_ASDATDOC;
                  ,this.w_ASDESCRI;
                  ,this.w_ASFLDOCE;
                  ,this.w_ASFLDOCR;
                  ,this.w_ASRAGSCR;
                  ,this.w_ASFLRATE;
                  ,this.w_ASRRPROV;
                  ,this.w_ASFLCESP;
                  ,this.w_ASFLRIMA;
                  ,this.w_ASFLSTOR;
                  ,this.w_ASDTRGST;
                  ,this.w_ASDATINI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ASSESTAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ASSESTAM_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ASSESTAM_IDX,i_nConn)
      *
      * update ASSESTAM
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ASSESTAM')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ASDATREG="+cp_ToStrODBC(this.w_ASDATREG)+;
             ",ASDATFIN="+cp_ToStrODBC(this.w_ASDATFIN)+;
             ",ASNUMDOC="+cp_ToStrODBC(this.w_ASNUMDOC)+;
             ",ASALFDOC="+cp_ToStrODBC(this.w_ASALFDOC)+;
             ",ASDATDOC="+cp_ToStrODBC(this.w_ASDATDOC)+;
             ",ASDESCRI="+cp_ToStrODBC(this.w_ASDESCRI)+;
             ",ASFLDOCE="+cp_ToStrODBC(this.w_ASFLDOCE)+;
             ",ASFLDOCR="+cp_ToStrODBC(this.w_ASFLDOCR)+;
             ",ASRAGSCR="+cp_ToStrODBC(this.w_ASRAGSCR)+;
             ",ASFLRATE="+cp_ToStrODBC(this.w_ASFLRATE)+;
             ",ASRRPROV="+cp_ToStrODBC(this.w_ASRRPROV)+;
             ",ASFLCESP="+cp_ToStrODBC(this.w_ASFLCESP)+;
             ",ASFLRIMA="+cp_ToStrODBC(this.w_ASFLRIMA)+;
             ",ASFLSTOR="+cp_ToStrODBC(this.w_ASFLSTOR)+;
             ",ASDTRGST="+cp_ToStrODBC(this.w_ASDTRGST)+;
             ",ASDATINI="+cp_ToStrODBC(this.w_ASDATINI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ASSESTAM')
        i_cWhere = cp_PKFox(i_cTable  ,'ASSERIAL',this.w_ASSERIAL  )
        UPDATE (i_cTable) SET;
              ASDATREG=this.w_ASDATREG;
             ,ASDATFIN=this.w_ASDATFIN;
             ,ASNUMDOC=this.w_ASNUMDOC;
             ,ASALFDOC=this.w_ASALFDOC;
             ,ASDATDOC=this.w_ASDATDOC;
             ,ASDESCRI=this.w_ASDESCRI;
             ,ASFLDOCE=this.w_ASFLDOCE;
             ,ASFLDOCR=this.w_ASFLDOCR;
             ,ASRAGSCR=this.w_ASRAGSCR;
             ,ASFLRATE=this.w_ASFLRATE;
             ,ASRRPROV=this.w_ASRRPROV;
             ,ASFLCESP=this.w_ASFLCESP;
             ,ASFLRIMA=this.w_ASFLRIMA;
             ,ASFLSTOR=this.w_ASFLSTOR;
             ,ASDTRGST=this.w_ASDTRGST;
             ,ASDATINI=this.w_ASDATINI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ASSESTAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ASSESTAM_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ASSESTAM_IDX,i_nConn)
      *
      * delete ASSESTAM
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ASSERIAL',this.w_ASSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ASSESTAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASSESTAM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_RAGGSCRI<>.w_RAGGSCRI
            .w_ASRAGSCR = IIF(.cFunction='Load',.w_RAGGSCRI,.w_ASRAGSCR)
        endif
        .DoRTCalc(11,18,.t.)
        if .o_ASFLSTOR<>.w_ASFLSTOR.or. .o_ASDATFIN<>.w_ASDATFIN.or. .o_ASDATREG<>.w_ASDATREG.or. .o_FLESER<>.w_FLESER
            .w_ASDTRGST = IIF(.w_ASFLSTOR<>'S' OR !.w_FLESER,cp_CharToDate('  -  -    '),IIF(.w_ASDATFIN<>g_FINESE OR .w_ASDATREG>g_FINESE,.w_ASDATREG+1,g_FINESE+1))
        endif
        if .o_ASDATREG<>.w_ASDATREG
            .w_OBTEST = .w_ASDATREG
        endif
        .DoRTCalc(21,21,.t.)
          .link_1_24('Full')
        .DoRTCalc(23,23,.t.)
        if .o_CODESE<>.w_CODESE
          .link_1_26('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .DoRTCalc(25,29,.t.)
          .link_1_42('Full')
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .DoRTCalc(31,57,.t.)
            .w_ACQU = g_ACQU
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEASS","i_codazi,w_ASSERIAL")
          .op_ASSERIAL = .w_ASSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(59,70,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
    endwith
  return

  proc Calculate_ERMUEJWTRR()
    with this
          * --- Sbianco riferimenti piano ammortamento al cambio check cespiti
          .w_PANUMREG = Space(6)
          .w_PADATREG = cp_CharToDate('  -  -    ')
          .w_PACODESE = Space(4)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oASDATREG_1_2.enabled = this.oPgFrm.Page1.oPag.oASDATREG_1_2.mCond()
    this.oPgFrm.Page1.oPag.oASDATFIN_1_3.enabled = this.oPgFrm.Page1.oPag.oASDATFIN_1_3.mCond()
    this.oPgFrm.Page1.oPag.oASNUMDOC_1_4.enabled = this.oPgFrm.Page1.oPag.oASNUMDOC_1_4.mCond()
    this.oPgFrm.Page1.oPag.oASALFDOC_1_5.enabled = this.oPgFrm.Page1.oPag.oASALFDOC_1_5.mCond()
    this.oPgFrm.Page1.oPag.oASDATDOC_1_6.enabled = this.oPgFrm.Page1.oPag.oASDATDOC_1_6.mCond()
    this.oPgFrm.Page1.oPag.oASFLDOCE_1_8.enabled = this.oPgFrm.Page1.oPag.oASFLDOCE_1_8.mCond()
    this.oPgFrm.Page1.oPag.oASFLDOCR_1_9.enabled = this.oPgFrm.Page1.oPag.oASFLDOCR_1_9.mCond()
    this.oPgFrm.Page1.oPag.oASFLRATE_1_11.enabled = this.oPgFrm.Page1.oPag.oASFLRATE_1_11.mCond()
    this.oPgFrm.Page1.oPag.oASRRPROV_1_12.enabled = this.oPgFrm.Page1.oPag.oASRRPROV_1_12.mCond()
    this.oPgFrm.Page1.oPag.oASFLCESP_1_13.enabled = this.oPgFrm.Page1.oPag.oASFLCESP_1_13.mCond()
    this.oPgFrm.Page1.oPag.oASFLRIMA_1_15.enabled = this.oPgFrm.Page1.oPag.oASFLRIMA_1_15.mCond()
    this.oPgFrm.Page1.oPag.oASFLSTOR_1_18.enabled = this.oPgFrm.Page1.oPag.oASFLSTOR_1_18.mCond()
    this.oPgFrm.Page1.oPag.oASFLSTOR_1_19.enabled = this.oPgFrm.Page1.oPag.oASFLSTOR_1_19.mCond()
    this.oPgFrm.Page1.oPag.oASDTRGST_1_20.enabled = this.oPgFrm.Page1.oPag.oASDTRGST_1_20.mCond()
    this.oPgFrm.Page1.oPag.oRAGGSCRI_1_74.enabled = this.oPgFrm.Page1.oPag.oRAGGSCRI_1_74.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oASFLDOCR_1_9.visible=!this.oPgFrm.Page1.oPag.oASFLDOCR_1_9.mHide()
    this.oPgFrm.Page1.oPag.oASRAGSCR_1_10.visible=!this.oPgFrm.Page1.oPag.oASRAGSCR_1_10.mHide()
    this.oPgFrm.Page1.oPag.oASRRPROV_1_12.visible=!this.oPgFrm.Page1.oPag.oASRRPROV_1_12.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_14.visible=!this.oPgFrm.Page1.oPag.oBtn_1_14.mHide()
    this.oPgFrm.Page1.oPag.oNUMINV_1_16.visible=!this.oPgFrm.Page1.oPag.oNUMINV_1_16.mHide()
    this.oPgFrm.Page1.oPag.oTIPVAL_1_17.visible=!this.oPgFrm.Page1.oPag.oTIPVAL_1_17.mHide()
    this.oPgFrm.Page1.oPag.oASFLSTOR_1_18.visible=!this.oPgFrm.Page1.oPag.oASFLSTOR_1_18.mHide()
    this.oPgFrm.Page1.oPag.oASFLSTOR_1_19.visible=!this.oPgFrm.Page1.oPag.oASFLSTOR_1_19.mHide()
    this.oPgFrm.Page1.oPag.oASDTRGST_1_20.visible=!this.oPgFrm.Page1.oPag.oASDTRGST_1_20.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_21.visible=!this.oPgFrm.Page1.oPag.oBtn_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_73.visible=!this.oPgFrm.Page1.oPag.oStr_1_73.mHide()
    this.oPgFrm.Page1.oPag.oRAGGSCRI_1_74.visible=!this.oPgFrm.Page1.oPag.oRAGGSCRI_1_74.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_79.visible=!this.oPgFrm.Page1.oPag.oBtn_1_79.mHide()
    this.oPgFrm.Page1.oPag.oPANUMREG_1_87.visible=!this.oPgFrm.Page1.oPag.oPANUMREG_1_87.mHide()
    this.oPgFrm.Page1.oPag.oPACODESE_1_88.visible=!this.oPgFrm.Page1.oPag.oPACODESE_1_88.mHide()
    this.oPgFrm.Page1.oPag.oPADATREG_1_89.visible=!this.oPgFrm.Page1.oPag.oPADATREG_1_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_90.visible=!this.oPgFrm.Page1.oPag.oStr_1_90.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_72.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_86.Event(cEvent)
        if lower(cEvent)==lower("w_ASFLCESP Changed")
          .Calculate_ERMUEJWTRR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NUMINV
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_NUMINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_CODESE);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_CODESE;
                     ,'INNUMINV',trim(this.w_NUMINV))
          select INCODESE,INNUMINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oNUMINV_1_16'),i_cWhere,'',"Inventari",'gsbi_inv.INVENTAR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_CODESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_NUMINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_CODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_CODESE;
                       ,'INNUMINV',this.w_NUMINV)
            select INCODESE,INNUMINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMINV = NVL(_Link_.INNUMINV,space(6))
    else
      if i_cCtrl<>'Load'
        this.w_NUMINV = space(6)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZIENDA
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZSTALIG";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZSTALIG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_STALIG = NVL(cp_ToDate(_Link_.AZSTALIG),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_STALIG = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESCONCON";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESCONCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_CONCON = NVL(cp_ToDate(_Link_.ESCONCON),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_CONCON = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODAZI
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_COCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_COCODAZI)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_COFATEME = NVL(_Link_.COFATEME,space(15))
      this.w_COFATRIC = NVL(_Link_.COFATRIC,space(15))
      this.w_CORATATT = NVL(_Link_.CORATATT,space(15))
      this.w_CORATPAS = NVL(_Link_.CORATPAS,space(15))
      this.w_CORISATT = NVL(_Link_.CORISATT,space(15))
      this.w_COIVADEB = NVL(_Link_.COIVADEB,space(15))
      this.w_COIVACRE = NVL(_Link_.COIVACRE,space(15))
      this.w_COCAUFDE = NVL(_Link_.COCAUFDE,space(5))
      this.w_COCASFDE = NVL(_Link_.COCASFDE,space(5))
      this.w_COCAUFDR = NVL(_Link_.COCAUFDR,space(5))
      this.w_COCASFDR = NVL(_Link_.COCASFDR,space(5))
      this.w_COCAURER = NVL(_Link_.COCAURER,space(5))
      this.w_COCASRER = NVL(_Link_.COCASRER,space(5))
      this.w_COCAUCES = NVL(_Link_.COCAUCES,space(5))
      this.w_COCASCES = NVL(_Link_.COCASCES,space(5))
      this.w_COCAURIM = NVL(_Link_.COCAURIM,space(5))
      this.w_COCASRIM = NVL(_Link_.COCASRIM,space(5))
      this.w_COTIPCON = NVL(_Link_.COTIPCON,space(1))
      this.w_CORISPAS = NVL(_Link_.CORISPAS,space(15))
      this.w_COCONDIC = NVL(_Link_.CODIFCON,space(15))
      this.w_COCONARR = NVL(_Link_.COCONARR,space(15))
      this.w_COCAFRER = NVL(_Link_.COCAFRER,space(5))
      this.w_COCONBOL = NVL(_Link_.COCONBOL,space(15))
      this.w_COCONTRA = NVL(_Link_.COCONTRA,space(15))
      this.w_COCONINC = NVL(_Link_.COCONINC,space(15))
      this.w_COCONIMB = NVL(_Link_.COCONIMB,space(15))
      this.w_COINDIVA = NVL(_Link_.COINDIVA,space(1))
      this.w_COACQINC = NVL(_Link_.COACQINC,space(15))
      this.w_COACQIMB = NVL(_Link_.COACQIMB,space(15))
      this.w_COACQTRA = NVL(_Link_.COACQTRA,space(15))
      this.w_COACQBOL = NVL(_Link_.COACQBOL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_COCODAZI = space(5)
      endif
      this.w_COFATEME = space(15)
      this.w_COFATRIC = space(15)
      this.w_CORATATT = space(15)
      this.w_CORATPAS = space(15)
      this.w_CORISATT = space(15)
      this.w_COIVADEB = space(15)
      this.w_COIVACRE = space(15)
      this.w_COCAUFDE = space(5)
      this.w_COCASFDE = space(5)
      this.w_COCAUFDR = space(5)
      this.w_COCASFDR = space(5)
      this.w_COCAURER = space(5)
      this.w_COCASRER = space(5)
      this.w_COCAUCES = space(5)
      this.w_COCASCES = space(5)
      this.w_COCAURIM = space(5)
      this.w_COCASRIM = space(5)
      this.w_COTIPCON = space(1)
      this.w_CORISPAS = space(15)
      this.w_COCONDIC = space(15)
      this.w_COCONARR = space(15)
      this.w_COCAFRER = space(5)
      this.w_COCONBOL = space(15)
      this.w_COCONTRA = space(15)
      this.w_COCONINC = space(15)
      this.w_COCONIMB = space(15)
      this.w_COINDIVA = space(1)
      this.w_COACQINC = space(15)
      this.w_COACQIMB = space(15)
      this.w_COACQTRA = space(15)
      this.w_COACQBOL = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oASSERIAL_1_1.value==this.w_ASSERIAL)
      this.oPgFrm.Page1.oPag.oASSERIAL_1_1.value=this.w_ASSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oASDATREG_1_2.value==this.w_ASDATREG)
      this.oPgFrm.Page1.oPag.oASDATREG_1_2.value=this.w_ASDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oASDATFIN_1_3.value==this.w_ASDATFIN)
      this.oPgFrm.Page1.oPag.oASDATFIN_1_3.value=this.w_ASDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oASNUMDOC_1_4.value==this.w_ASNUMDOC)
      this.oPgFrm.Page1.oPag.oASNUMDOC_1_4.value=this.w_ASNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oASALFDOC_1_5.value==this.w_ASALFDOC)
      this.oPgFrm.Page1.oPag.oASALFDOC_1_5.value=this.w_ASALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oASDATDOC_1_6.value==this.w_ASDATDOC)
      this.oPgFrm.Page1.oPag.oASDATDOC_1_6.value=this.w_ASDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oASDESCRI_1_7.value==this.w_ASDESCRI)
      this.oPgFrm.Page1.oPag.oASDESCRI_1_7.value=this.w_ASDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oASFLDOCE_1_8.RadioValue()==this.w_ASFLDOCE)
      this.oPgFrm.Page1.oPag.oASFLDOCE_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASFLDOCR_1_9.RadioValue()==this.w_ASFLDOCR)
      this.oPgFrm.Page1.oPag.oASFLDOCR_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASRAGSCR_1_10.RadioValue()==this.w_ASRAGSCR)
      this.oPgFrm.Page1.oPag.oASRAGSCR_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASFLRATE_1_11.RadioValue()==this.w_ASFLRATE)
      this.oPgFrm.Page1.oPag.oASFLRATE_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASRRPROV_1_12.RadioValue()==this.w_ASRRPROV)
      this.oPgFrm.Page1.oPag.oASRRPROV_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASFLCESP_1_13.RadioValue()==this.w_ASFLCESP)
      this.oPgFrm.Page1.oPag.oASFLCESP_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASFLRIMA_1_15.RadioValue()==this.w_ASFLRIMA)
      this.oPgFrm.Page1.oPag.oASFLRIMA_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINV_1_16.value==this.w_NUMINV)
      this.oPgFrm.Page1.oPag.oNUMINV_1_16.value=this.w_NUMINV
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPVAL_1_17.RadioValue()==this.w_TIPVAL)
      this.oPgFrm.Page1.oPag.oTIPVAL_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASFLSTOR_1_18.RadioValue()==this.w_ASFLSTOR)
      this.oPgFrm.Page1.oPag.oASFLSTOR_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASFLSTOR_1_19.RadioValue()==this.w_ASFLSTOR)
      this.oPgFrm.Page1.oPag.oASFLSTOR_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASDTRGST_1_20.value==this.w_ASDTRGST)
      this.oPgFrm.Page1.oPag.oASDTRGST_1_20.value=this.w_ASDTRGST
    endif
    if not(this.oPgFrm.Page1.oPag.oASDATINI_1_31.value==this.w_ASDATINI)
      this.oPgFrm.Page1.oPag.oASDATINI_1_31.value=this.w_ASDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGGSCRI_1_74.RadioValue()==this.w_RAGGSCRI)
      this.oPgFrm.Page1.oPag.oRAGGSCRI_1_74.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPANUMREG_1_87.value==this.w_PANUMREG)
      this.oPgFrm.Page1.oPag.oPANUMREG_1_87.value=this.w_PANUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPACODESE_1_88.value==this.w_PACODESE)
      this.oPgFrm.Page1.oPag.oPACODESE_1_88.value=this.w_PACODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oPADATREG_1_89.value==this.w_PADATREG)
      this.oPgFrm.Page1.oPag.oPADATREG_1_89.value=this.w_PADATREG
    endif
    cp_SetControlsValueExtFlds(this,'ASSESTAM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_ASFLCESP<>'S' Or Not Empty( .w_PANUMREG ))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un piano di ammortamento")
          case   ((empty(.w_ASDATFIN)) or not(.w_ASDATFIN>=.w_ASDATINI AND .w_ASDATFIN<=.w_FINESE))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oASDATFIN_1_3.SetFocus()
            i_bnoObbl = !empty(.w_ASDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data fine elaborazione < data inizio o non compresa nell'esercizio")
          case   not((.w_ASDATREG<.w_ASDTRGST OR EMPTY(.w_ASDTRGST)))  and not(.w_ASFLSTOR<>'S')  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oASDTRGST_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data inferiore della data registrazione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_aas
      IF EMPTY(.w_NUMINV) AND .w_ASFLRIMA='S' AND .cFunction='Load'
              i_bRes = .f.
              i_bnoChk = .f.
              i_cErrorMsg = ah_MsgFormat("Codice inventario obbligatorio")
      ENDIF
      IF EMPTY(.w_ASDTRGST) AND .w_ASFLSTOR='S' AND .cFunction='Load'
         i_bRes = .f.
         i_bnoChk = .f.
         i_cErrorMsg = ah_MsgFormat("Data registrazione storno o apertura ratei/risconti obbligatoria")
      ENDIF
      IF ((.w_ASDATREG<=.w_Concon) OR (.w_ASDATREG<=.w_STALIG)) AND i_bRes AND .cFunction='Load'
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg =ah_MsgFormat("Data registrazione inferiore o uguale alla data di consolidamento o alla data di stampa del libro giornale")
      ENDIF
      
      * Su ad hoc ENTERPRISE leggo con una looktab se l'esercizio � chiuso.
      * Campo non presente su REVOLUTION
      IF g_APPLICATION <> "ADHOC REVOLUTION"
         .w_CHIUSO=looktab('ESERCIZI','ESCHIUSO','ESCODAZI',.w_AZIENDA,'ESCODESE', .w_CODESE )
      Else
         .w_CHIUSO='N'
      Endif
      
      IF .w_CHIUSO='S' AND i_bRes AND .cFunction='Load'
            i_bRes=ah_YesNo("Attenzione l'esercizio di competenza risulta chiuso%0Si vuole confermare il movimento?")
            if !i_bRes
                i_cErrorMsg = ah_MsgFormat("Transazione abbandonata")
                i_bNoChk = .f.
            endif
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ASDATREG = this.w_ASDATREG
    this.o_ASDATFIN = this.w_ASDATFIN
    this.o_ASFLRATE = this.w_ASFLRATE
    this.o_ASFLSTOR = this.w_ASFLSTOR
    this.o_CODESE = this.w_CODESE
    this.o_RAGGSCRI = this.w_RAGGSCRI
    this.o_FLESER = this.w_FLESER
    return

enddefine

* --- Define pages as container
define class tgscg_aasPag1 as StdContainer
  Width  = 571
  height = 344
  stdWidth  = 571
  stdheight = 344
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oASSERIAL_1_1 as StdField with uid="RPMYSRVYOJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ASSERIAL", cQueryName = "ASSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Seriale elaborazione",;
    HelpContextID = 169844142,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=129, Top=10, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oASDATREG_1_2 as StdField with uid="XPKIJZNRQL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ASDATREG", cQueryName = "ASDATREG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione",;
    HelpContextID = 251359821,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=129, Top=37

  func oASDATREG_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oASDATFIN_1_3 as StdField with uid="ZENSEQQEBY",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ASDATFIN", cQueryName = "ASDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data fine elaborazione < data inizio o non compresa nell'esercizio",;
    ToolTipText = "Data fine elaborazione",;
    HelpContextID = 50033236,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=444, Top=37

  func oASDATFIN_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oASDATFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ASDATFIN>=.w_ASDATINI AND .w_ASDATFIN<=.w_FINESE)
    endwith
    return bRes
  endfunc

  add object oASNUMDOC_1_4 as StdField with uid="OQBRXKBOPA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ASNUMDOC", cQueryName = "ASNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento registrazione",;
    HelpContextID = 257945015,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=129, Top=64, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oASNUMDOC_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oASALFDOC_1_5 as StdField with uid="URSAEEVOOZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ASALFDOC", cQueryName = "ASALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 265928119,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=258, Top=64, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oASALFDOC_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oASDATDOC_1_6 as StdField with uid="PFYURWWFTW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ASDATDOC", cQueryName = "ASDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento",;
    HelpContextID = 251956663,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=444, Top=64

  func oASDATDOC_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oASDESCRI_1_7 as StdField with uid="VHJKLGNIEU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ASDESCRI", cQueryName = "ASDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione movimenti generati se non specificata compilata in automatico con riferimenti documento",;
    HelpContextID = 1084849,;
   bGlobalFont=.t.,;
    Height=21, Width=394, Left=129, Top=91, InputMask=replicate('X',50)

  add object oASFLDOCE_1_8 as StdCheck with uid="MTSTOLSGMH",rtseq=8,rtrep=.f.,left=23, top=148, caption="Fatture da emettere",;
    ToolTipText = "Generazione scritture da fatture da emettere",;
    HelpContextID = 184980043,;
    cFormVar="w_ASFLDOCE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASFLDOCE_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oASFLDOCE_1_8.GetRadio()
    this.Parent.oContained.w_ASFLDOCE = this.RadioValue()
    return .t.
  endfunc

  func oASFLDOCE_1_8.SetRadio()
    this.Parent.oContained.w_ASFLDOCE=trim(this.Parent.oContained.w_ASFLDOCE)
    this.value = ;
      iif(this.Parent.oContained.w_ASFLDOCE=='S',1,;
      0)
  endfunc

  func oASFLDOCE_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' AND g_MAGA='S' AND .w_VEND='S')
    endwith
   endif
  endfunc

  add object oASFLDOCR_1_9 as StdCheck with uid="NHCWDGQWFS",rtseq=9,rtrep=.f.,left=193, top=148, caption="Fatture da ricevere",;
    ToolTipText = "Generazione scritture da fatture da ricevere",;
    HelpContextID = 184980056,;
    cFormVar="w_ASFLDOCR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASFLDOCR_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oASFLDOCR_1_9.GetRadio()
    this.Parent.oContained.w_ASFLDOCR = this.RadioValue()
    return .t.
  endfunc

  func oASFLDOCR_1_9.SetRadio()
    this.Parent.oContained.w_ASFLDOCR=trim(this.Parent.oContained.w_ASFLDOCR)
    this.value = ;
      iif(this.Parent.oContained.w_ASFLDOCR=='S',1,;
      0)
  endfunc

  func oASFLDOCR_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' AND g_MAGA='S' AND .w_ACQU='S' AND NOT IsAlt())
    endwith
   endif
  endfunc

  func oASFLDOCR_1_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oASRAGSCR_1_10 as StdCheck with uid="GEDVHWBBHF",rtseq=10,rtrep=.f.,left=366, top=148, caption="Raggruppa scritture", enabled=.f.,;
    ToolTipText = "Raggruppa scritture in una unica di primanota",;
    HelpContextID = 254562904,;
    cFormVar="w_ASRAGSCR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASRAGSCR_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oASRAGSCR_1_10.GetRadio()
    this.Parent.oContained.w_ASRAGSCR = this.RadioValue()
    return .t.
  endfunc

  func oASRAGSCR_1_10.SetRadio()
    this.Parent.oContained.w_ASRAGSCR=trim(this.Parent.oContained.w_ASRAGSCR)
    this.value = ;
      iif(this.Parent.oContained.w_ASRAGSCR=='S',1,;
      0)
  endfunc

  func oASRAGSCR_1_10.mHide()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  add object oASFLRATE_1_11 as StdCheck with uid="VWLSPIRNTK",rtseq=11,rtrep=.f.,left=23, top=179, caption="Ratei / risconti",;
    ToolTipText = "Generazione scritture da ratei e risconti",;
    HelpContextID = 233214539,;
    cFormVar="w_ASFLRATE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASFLRATE_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oASFLRATE_1_11.GetRadio()
    this.Parent.oContained.w_ASFLRATE = this.RadioValue()
    return .t.
  endfunc

  func oASFLRATE_1_11.SetRadio()
    this.Parent.oContained.w_ASFLRATE=trim(this.Parent.oContained.w_ASFLRATE)
    this.value = ;
      iif(this.Parent.oContained.w_ASFLRATE=='S',1,;
      0)
  endfunc

  func oASFLRATE_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oASRRPROV_1_12 as StdCheck with uid="KFQDJTKJVP",rtseq=12,rtrep=.f.,left=366, top=179, caption="Movimenti provvisori",;
    ToolTipText = "Se attivo il flag vengono considerati i movimenti provvisori per la generazione ratei e risconti",;
    HelpContextID = 20098468,;
    cFormVar="w_ASRRPROV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASRRPROV_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oASRRPROV_1_12.GetRadio()
    this.Parent.oContained.w_ASRRPROV = this.RadioValue()
    return .t.
  endfunc

  func oASRRPROV_1_12.SetRadio()
    this.Parent.oContained.w_ASRRPROV=trim(this.Parent.oContained.w_ASRRPROV)
    this.value = ;
      iif(this.Parent.oContained.w_ASRRPROV=='S',1,;
      0)
  endfunc

  func oASRRPROV_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' and .w_ASFLRATE='S')
    endwith
   endif
  endfunc

  func oASRRPROV_1_12.mHide()
    with this.Parent.oContained
      return (.w_ASFLRATE<>'S')
    endwith
  endfunc

  add object oASFLCESP_1_13 as StdCheck with uid="JIFSMWDQHZ",rtseq=13,rtrep=.f.,left=23, top=215, caption="Cespiti",;
    ToolTipText = "Generazione scritture da movimenti cespiti",;
    HelpContextID = 16159318,;
    cFormVar="w_ASFLCESP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASFLCESP_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oASFLCESP_1_13.GetRadio()
    this.Parent.oContained.w_ASFLCESP = this.RadioValue()
    return .t.
  endfunc

  func oASFLCESP_1_13.SetRadio()
    this.Parent.oContained.w_ASFLCESP=trim(this.Parent.oContained.w_ASFLCESP)
    this.value = ;
      iif(this.Parent.oContained.w_ASFLCESP=='S',1,;
      0)
  endfunc

  func oASFLCESP_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' AND g_CESP='S')
    endwith
   endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="LFNELGBYBK",left=299, top=215, width=20,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Zoom di selezione";
    , HelpContextID = 143070250;
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      vx_exec("QUERY\GSAR_KPM.VZM",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ASFLCESP<>'S')
     endwith
    endif
  endfunc

  add object oASFLRIMA_1_15 as StdCheck with uid="GVSWHZMQMY",rtseq=14,rtrep=.f.,left=23, top=258, caption="Rimanenze",;
    ToolTipText = "Generazione scritture da rimanenze di magazzino",;
    HelpContextID = 169438649,;
    cFormVar="w_ASFLRIMA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASFLRIMA_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oASFLRIMA_1_15.GetRadio()
    this.Parent.oContained.w_ASFLRIMA = this.RadioValue()
    return .t.
  endfunc

  func oASFLRIMA_1_15.SetRadio()
    this.Parent.oContained.w_ASFLRIMA=trim(this.Parent.oContained.w_ASFLRIMA)
    this.value = ;
      iif(this.Parent.oContained.w_ASFLRIMA=='S',1,;
      0)
  endfunc

  func oASFLRIMA_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' AND g_MAGA='S')
    endwith
   endif
  endfunc

  add object oNUMINV_1_16 as StdField with uid="OPWSFXSFCJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_NUMINV", cQueryName = "NUMINV",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    HelpContextID = 224131882,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=193, Top=258, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", oKey_1_1="INCODESE", oKey_1_2="this.w_CODESE", oKey_2_1="INNUMINV", oKey_2_2="this.w_NUMINV"

  func oNUMINV_1_16.mHide()
    with this.Parent.oContained
      return (.w_ASFLRIMA<>'S' OR .cFunction<>'Load')
    endwith
  endfunc

  func oNUMINV_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMINV_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMINV_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_CODESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_CODESE)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oNUMINV_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Inventari",'gsbi_inv.INVENTAR_VZM',this.parent.oContained
  endproc


  add object oTIPVAL_1_17 as StdCombo with uid="KLKRQOPNIY",rtseq=16,rtrep=.f.,left=376,top=258,width=145,height=21;
    , HelpContextID = 136238794;
    , cFormVar="w_TIPVAL",RowSource=""+"Medio pond.esercizio,"+"Medio pond.periodo,"+"Ultimo,"+"Ultimo standard,"+"FIFO continuo,"+"LIFO continuo,"+"LIFO scatti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPVAL_1_17.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    space(1)))))))))
  endfunc
  func oTIPVAL_1_17.GetRadio()
    this.Parent.oContained.w_TIPVAL = this.RadioValue()
    return .t.
  endfunc

  func oTIPVAL_1_17.SetRadio()
    this.Parent.oContained.w_TIPVAL=trim(this.Parent.oContained.w_TIPVAL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPVAL=='1',1,;
      iif(this.Parent.oContained.w_TIPVAL=='2',2,;
      iif(this.Parent.oContained.w_TIPVAL=='3',3,;
      iif(this.Parent.oContained.w_TIPVAL=='4',4,;
      iif(this.Parent.oContained.w_TIPVAL=='5',5,;
      iif(this.Parent.oContained.w_TIPVAL=='6',6,;
      iif(this.Parent.oContained.w_TIPVAL=='7',7,;
      0)))))))
  endfunc

  func oTIPVAL_1_17.mHide()
    with this.Parent.oContained
      return (.w_ASFLRIMA<>'S' OR .cFunction<>'Load')
    endwith
  endfunc

  add object oASFLSTOR_1_18 as StdCheck with uid="PANSABWZVV",rtseq=17,rtrep=.f.,left=23, top=317, caption="Scritture di storno",;
    ToolTipText = "Genera scritture di storno",;
    HelpContextID = 252276136,;
    cFormVar="w_ASFLSTOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASFLSTOR_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oASFLSTOR_1_18.GetRadio()
    this.Parent.oContained.w_ASFLSTOR = this.RadioValue()
    return .t.
  endfunc

  func oASFLSTOR_1_18.SetRadio()
    this.Parent.oContained.w_ASFLSTOR=trim(this.Parent.oContained.w_ASFLSTOR)
    this.value = ;
      iif(this.Parent.oContained.w_ASFLSTOR=='S',1,;
      0)
  endfunc

  func oASFLSTOR_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oASFLSTOR_1_18.mHide()
    with this.Parent.oContained
      return (.w_ASDATFIN=g_FINESE)
    endwith
  endfunc

  add object oASFLSTOR_1_19 as StdCheck with uid="VWKKBUDEHZ",rtseq=18,rtrep=.f.,left=23, top=317, caption="Apertura risconti",;
    ToolTipText = "Genera scritture di riapertura risconti",;
    HelpContextID = 252276136,;
    cFormVar="w_ASFLSTOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASFLSTOR_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oASFLSTOR_1_19.GetRadio()
    this.Parent.oContained.w_ASFLSTOR = this.RadioValue()
    return .t.
  endfunc

  func oASFLSTOR_1_19.SetRadio()
    this.Parent.oContained.w_ASFLSTOR=trim(this.Parent.oContained.w_ASFLSTOR)
    this.value = ;
      iif(this.Parent.oContained.w_ASFLSTOR=='S',1,;
      0)
  endfunc

  func oASFLSTOR_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oASFLSTOR_1_19.mHide()
    with this.Parent.oContained
      return (.w_ASDATFIN<>g_FINESE OR .w_ASFLRATE<>'S')
    endwith
  endfunc

  add object oASDTRGST_1_20 as StdField with uid="WENOUNPACK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ASDTRGST", cQueryName = "ASDTRGST",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data inferiore della data registrazione",;
    ToolTipText = "Data registrazione scrittura di storno o apertura esercizio",;
    HelpContextID = 65958490,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=428, Top=317

  func oASDTRGST_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oASDTRGST_1_20.mHide()
    with this.Parent.oContained
      return (.w_ASFLSTOR<>'S')
    endwith
  endfunc

  func oASDTRGST_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ASDATREG<.w_ASDTRGST OR EMPTY(.w_ASDTRGST)))
    endwith
    return bRes
  endfunc


  add object oBtn_1_21 as StdButton with uid="ZMIZZNTUDJ",left=519, top=297, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare le scritture";
    , HelpContextID = 88116202;
    , Caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction<>'Load')
     endwith
    endif
  endfunc

  add object oASDATINI_1_31 as StdField with uid="IDFIAURVGE",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ASDATINI", cQueryName = "ASDATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio elaborazione",;
    HelpContextID = 168070577,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=444, Top=10


  add object oObj_1_41 as cp_runprogram with uid="FGIKLJPCYV",left=-3, top=498, width=155,height=19,;
    caption='GSCG_BAT',;
   bGlobalFont=.t.,;
    prg="GSCG_BAT",;
    cEvent = "Insert end",;
    nPag=1;
    , HelpContextID = 5152070


  add object oObj_1_72 as cp_runprogram with uid="ZMKXUJHUQI",left=159, top=499, width=235,height=19,;
    caption='GSCG_BAD(D)',;
   bGlobalFont=.t.,;
    prg="GSCG_BAD('D')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 4966102

  add object oRAGGSCRI_1_74 as StdCheck with uid="YMPJQNYSFF",rtseq=57,rtrep=.f.,left=366, top=148, caption="Raggruppa scritture",;
    ToolTipText = "Raggruppa scritture in una unica di primanota",;
    HelpContextID = 945825,;
    cFormVar="w_RAGGSCRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRAGGSCRI_1_74.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oRAGGSCRI_1_74.GetRadio()
    this.Parent.oContained.w_RAGGSCRI = this.RadioValue()
    return .t.
  endfunc

  func oRAGGSCRI_1_74.SetRadio()
    this.Parent.oContained.w_RAGGSCRI=trim(this.Parent.oContained.w_RAGGSCRI)
    this.value = ;
      iif(this.Parent.oContained.w_RAGGSCRI=='S',1,;
      0)
  endfunc

  func oRAGGSCRI_1_74.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ASFLDOCE='S' OR .w_ASFLDOCR='S' OR .w_ASFLRATE='S')
    endwith
   endif
  endfunc

  func oRAGGSCRI_1_74.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc


  add object oBtn_1_79 as StdButton with uid="XEJUNYJJMS",left=519, top=297, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Dettaglio righe di primanota generate";
    , HelpContextID = 184449889;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_79.Click()
      do GSCG_KAA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_79.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load'  Or Empty(  .w_ASSERIAL ))
     endwith
    endif
  endfunc


  add object oObj_1_86 as cp_runprogram with uid="UMOUDDHFMG",left=159, top=519, width=366,height=19,;
    caption='GSCG_BAD(A)',;
   bGlobalFont=.t.,;
    prg="GSCG_BAD('A')",;
    cEvent = "w_ASFLSTOR Changed, w_ASDTRGST Changed",;
    nPag=1;
    , HelpContextID = 4966870

  add object oPANUMREG_1_87 as StdField with uid="DNXGCAKZWL",rtseq=68,rtrep=.f.,;
    cFormVar = "w_PANUMREG", cQueryName = "PANUMREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Piano di ammortamento",;
    HelpContextID = 245367101,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=246, Top=215, cSayPict="'999999'", cGetPict="'999999'", InputMask=replicate('X',6)

  func oPANUMREG_1_87.mHide()
    with this.Parent.oContained
      return (.w_ASFLCESP<>'S')
    endwith
  endfunc

  add object oPACODESE_1_88 as StdField with uid="IKZOVUQVRY",rtseq=69,rtrep=.f.,;
    cFormVar = "w_PACODESE", cQueryName = "PACODESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 17387835,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=394, Top=215, InputMask=replicate('X',4)

  func oPACODESE_1_88.mHide()
    with this.Parent.oContained
      return (.w_ASFLCESP<>'S')
    endwith
  endfunc

  add object oPADATREG_1_89 as StdField with uid="YJZMTKPAUC",rtseq=70,rtrep=.f.,;
    cFormVar = "w_PADATREG", cQueryName = "PADATREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 251355453,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=489, Top=215

  func oPADATREG_1_89.mHide()
    with this.Parent.oContained
      return (.w_ASFLCESP<>'S')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="JKIGFAPXNF",Visible=.t., Left=3, Top=10,;
    Alignment=1, Width=122, Height=15,;
    Caption="Numero elab.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="UXXEYZISMG",Visible=.t., Left=3, Top=37,;
    Alignment=1, Width=122, Height=15,;
    Caption="Data registr.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="LXVTWMUJHD",Visible=.t., Left=314, Top=10,;
    Alignment=1, Width=127, Height=15,;
    Caption="Periodo - da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="FZVGVCHDKB",Visible=.t., Left=379, Top=37,;
    Alignment=1, Width=62, Height=15,;
    Caption="a data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="IQFBDHNWZT",Visible=.t., Left=3, Top=64,;
    Alignment=1, Width=122, Height=15,;
    Caption="Documento n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="PEVTHLDIOH",Visible=.t., Left=248, Top=64,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="FUXVYJVBWN",Visible=.t., Left=394, Top=64,;
    Alignment=1, Width=47, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="BSOTWMBHVP",Visible=.t., Left=9, Top=121,;
    Alignment=0, Width=206, Height=18,;
    Caption="Tipologie scritture da generare"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="LBWNOPTWXE",Visible=.t., Left=3, Top=91,;
    Alignment=1, Width=122, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="YGHASYCYBE",Visible=.t., Left=130, Top=259,;
    Alignment=1, Width=58, Height=15,;
    Caption="Inventario:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (.w_ASFLRIMA<>'S' OR .cFunction<>'Load')
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="OLCPQRTLGH",Visible=.t., Left=255, Top=259,;
    Alignment=1, Width=120, Height=15,;
    Caption="Tipo valorizzazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (.w_ASFLRIMA<>'S' OR .cFunction<>'Load')
    endwith
  endfunc

  add object oStr_1_73 as StdString with uid="IZZBFMMUMQ",Visible=.t., Left=305, Top=317,;
    Alignment=1, Width=122, Height=15,;
    Caption="Data registr.:"  ;
  , bGlobalFont=.t.

  func oStr_1_73.mHide()
    with this.Parent.oContained
      return (.w_ASFLSTOR<>'S')
    endwith
  endfunc

  add object oStr_1_90 as StdString with uid="EDECACDWGV",Visible=.t., Left=129, Top=215,;
    Alignment=1, Width=112, Height=18,;
    Caption="Piano di ammort.:"  ;
  , bGlobalFont=.t.

  func oStr_1_90.mHide()
    with this.Parent.oContained
      return (.w_ASFLCESP<>'S')
    endwith
  endfunc

  add object oStr_1_91 as StdString with uid="TLQPZNBKGD",Visible=.t., Left=319, Top=215,;
    Alignment=1, Width=74, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (.w_ASFLCESP<>'S')
    endwith
  endfunc

  add object oStr_1_92 as StdString with uid="MWZMOKSBGC",Visible=.t., Left=443, Top=215,;
    Alignment=1, Width=42, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return (.w_ASFLCESP<>'S')
    endwith
  endfunc

  add object oBox_1_39 as StdBox with uid="FULZWXKUMK",left=9, top=139, width=559,height=151
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_aas','ASSESTAM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ASSERIAL=ASSESTAM.ASSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
