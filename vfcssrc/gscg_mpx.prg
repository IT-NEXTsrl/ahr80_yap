* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mpx                                                        *
*              Parametri elenchi clienti / fornitori mastri da escludere       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-21                                                      *
* Last revis.: 2011-02-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mpx")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mpx")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mpx")
  return

* --- Class definition
define class tgscg_mpx as StdPCForm
  Width  = 638
  Height = 239
  Top    = 8
  Left   = 8
  cComment = "Parametri elenchi clienti / fornitori mastri da escludere"
  cPrg = "gscg_mpx"
  HelpContextID=147465577
  add object cnt as tcgscg_mpx
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mpx as PCContext
  w_PE__ANNO = space(4)
  w_PETIPCOM = space(1)
  w_PEMASTRO = space(15)
  w_MCDESCRI = space(40)
  w_TIPMAS = space(1)
  w_NULIV = 0
  w_PECATCON = space(5)
  w_PECATCOM = space(3)
  w_DESCON = space(35)
  w_DESCAC = space(35)
  proc Save(i_oFrom)
    this.w_PE__ANNO = i_oFrom.w_PE__ANNO
    this.w_PETIPCOM = i_oFrom.w_PETIPCOM
    this.w_PEMASTRO = i_oFrom.w_PEMASTRO
    this.w_MCDESCRI = i_oFrom.w_MCDESCRI
    this.w_TIPMAS = i_oFrom.w_TIPMAS
    this.w_NULIV = i_oFrom.w_NULIV
    this.w_PECATCON = i_oFrom.w_PECATCON
    this.w_PECATCOM = i_oFrom.w_PECATCOM
    this.w_DESCON = i_oFrom.w_DESCON
    this.w_DESCAC = i_oFrom.w_DESCAC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PE__ANNO = this.w_PE__ANNO
    i_oTo.w_PETIPCOM = this.w_PETIPCOM
    i_oTo.w_PEMASTRO = this.w_PEMASTRO
    i_oTo.w_MCDESCRI = this.w_MCDESCRI
    i_oTo.w_TIPMAS = this.w_TIPMAS
    i_oTo.w_NULIV = this.w_NULIV
    i_oTo.w_PECATCON = this.w_PECATCON
    i_oTo.w_PECATCOM = this.w_PECATCOM
    i_oTo.w_DESCON = this.w_DESCON
    i_oTo.w_DESCAC = this.w_DESCAC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mpx as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 638
  Height = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-28"
  HelpContextID=147465577
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PAELCLFM_IDX = 0
  MASTRI_IDX = 0
  CACOCLFO_IDX = 0
  CATECOMM_IDX = 0
  cFile = "PAELCLFM"
  cKeySelect = "PE__ANNO,PETIPCOM"
  cKeyWhere  = "PE__ANNO=this.w_PE__ANNO and PETIPCOM=this.w_PETIPCOM"
  cKeyDetail  = "PE__ANNO=this.w_PE__ANNO and PETIPCOM=this.w_PETIPCOM and PEMASTRO=this.w_PEMASTRO and PECATCON=this.w_PECATCON and PECATCOM=this.w_PECATCOM"
  cKeyWhereODBC = '"PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';
      +'+" and PETIPCOM="+cp_ToStrODBC(this.w_PETIPCOM)';

  cKeyDetailWhereODBC = '"PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';
      +'+" and PETIPCOM="+cp_ToStrODBC(this.w_PETIPCOM)';
      +'+" and PEMASTRO="+cp_ToStrODBC(this.w_PEMASTRO)';
      +'+" and PECATCON="+cp_ToStrODBC(this.w_PECATCON)';
      +'+" and PECATCOM="+cp_ToStrODBC(this.w_PECATCOM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PAELCLFM.PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';
      +'+" and PAELCLFM.PETIPCOM="+cp_ToStrODBC(this.w_PETIPCOM)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PAELCLFM.PEMASTRO,PAELCLFM.PECATCON,PAELCLFM.PECATCOM'
  cPrg = "gscg_mpx"
  cComment = "Parametri elenchi clienti / fornitori mastri da escludere"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PE__ANNO = space(4)
  w_PETIPCOM = space(1)
  w_PEMASTRO = space(15)
  w_MCDESCRI = space(40)
  w_TIPMAS = space(1)
  w_NULIV = 0
  w_PECATCON = space(5)
  w_PECATCOM = space(3)
  w_DESCON = space(35)
  w_DESCAC = space(35)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mpxPag1","gscg_mpx",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='MASTRI'
    this.cWorkTables[2]='CACOCLFO'
    this.cWorkTables[3]='CATECOMM'
    this.cWorkTables[4]='PAELCLFM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAELCLFM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAELCLFM_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mpx'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PAELCLFM where PE__ANNO=KeySet.PE__ANNO
    *                            and PETIPCOM=KeySet.PETIPCOM
    *                            and PEMASTRO=KeySet.PEMASTRO
    *                            and PECATCON=KeySet.PECATCON
    *                            and PECATCOM=KeySet.PECATCOM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PAELCLFM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAELCLFM_IDX,2],this.bLoadRecFilter,this.PAELCLFM_IDX,"gscg_mpx")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAELCLFM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAELCLFM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAELCLFM '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PE__ANNO',this.w_PE__ANNO  ,'PETIPCOM',this.w_PETIPCOM  )
      select * from (i_cTable) PAELCLFM where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PE__ANNO = NVL(PE__ANNO,space(4))
        .w_PETIPCOM = NVL(PETIPCOM,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PAELCLFM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_MCDESCRI = space(40)
          .w_TIPMAS = space(1)
          .w_NULIV = 0
          .w_DESCON = space(35)
          .w_DESCAC = space(35)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_PEMASTRO = NVL(PEMASTRO,space(15))
          if link_2_1_joined
            this.w_PEMASTRO = NVL(MCCODICE201,NVL(this.w_PEMASTRO,space(15)))
            this.w_MCDESCRI = NVL(MCDESCRI201,space(40))
            this.w_NULIV = NVL(MCNUMLIV201,0)
            this.w_TIPMAS = NVL(MCTIPMAS201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_PECATCON = NVL(PECATCON,space(5))
          if link_2_5_joined
            this.w_PECATCON = NVL(C2CODICE205,NVL(this.w_PECATCON,space(5)))
            this.w_DESCON = NVL(C2DESCRI205,space(35))
          else
          .link_2_5('Load')
          endif
          .w_PECATCOM = NVL(PECATCOM,space(3))
          if link_2_6_joined
            this.w_PECATCOM = NVL(CTCODICE206,NVL(this.w_PECATCOM,space(3)))
            this.w_DESCAC = NVL(CTDESCRI206,space(35))
          else
          .link_2_6('Load')
          endif
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PE__ANNO=space(4)
      .w_PETIPCOM=space(1)
      .w_PEMASTRO=space(15)
      .w_MCDESCRI=space(40)
      .w_TIPMAS=space(1)
      .w_NULIV=0
      .w_PECATCON=space(5)
      .w_PECATCOM=space(3)
      .w_DESCON=space(35)
      .w_DESCAC=space(35)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_PEMASTRO))
         .link_2_1('Full')
        endif
        .DoRTCalc(4,7,.f.)
        if not(empty(.w_PECATCON))
         .link_2_5('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_PECATCOM))
         .link_2_6('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAELCLFM')
    this.DoRTCalc(9,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PAELCLFM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAELCLFM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PE__ANNO,"PE__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PETIPCOM,"PETIPCOM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PEMASTRO C(15);
      ,t_MCDESCRI C(40);
      ,t_PECATCON C(5);
      ,t_PECATCOM C(3);
      ,t_DESCON C(35);
      ,t_DESCAC C(35);
      ,CPROWNUM N(10);
      ,t_TIPMAS C(1);
      ,t_NULIV N(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mpxbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPEMASTRO_2_1.controlsource=this.cTrsName+'.t_PEMASTRO'
    this.oPgFRm.Page1.oPag.oMCDESCRI_2_2.controlsource=this.cTrsName+'.t_MCDESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECATCON_2_5.controlsource=this.cTrsName+'.t_PECATCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPECATCOM_2_6.controlsource=this.cTrsName+'.t_PECATCOM'
    this.oPgFRm.Page1.oPag.oDESCON_2_7.controlsource=this.cTrsName+'.t_DESCON'
    this.oPgFRm.Page1.oPag.oDESCAC_2_8.controlsource=this.cTrsName+'.t_DESCAC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(160)
    this.AddVLine(227)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEMASTRO_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAELCLFM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAELCLFM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAELCLFM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAELCLFM_IDX,2])
      *
      * insert into PAELCLFM
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAELCLFM')
        i_extval=cp_InsertValODBCExtFlds(this,'PAELCLFM')
        i_cFldBody=" "+;
                  "(PE__ANNO,PETIPCOM,PEMASTRO,PECATCON,PECATCOM,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PE__ANNO)+","+cp_ToStrODBC(this.w_PETIPCOM)+","+cp_ToStrODBCNull(this.w_PEMASTRO)+","+cp_ToStrODBCNull(this.w_PECATCON)+","+cp_ToStrODBCNull(this.w_PECATCOM)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAELCLFM')
        i_extval=cp_InsertValVFPExtFlds(this,'PAELCLFM')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PE__ANNO',this.w_PE__ANNO,'PETIPCOM',this.w_PETIPCOM,'PEMASTRO',this.w_PEMASTRO,'PECATCON',this.w_PECATCON,'PECATCOM',this.w_PECATCOM)
        INSERT INTO (i_cTable) (;
                   PE__ANNO;
                  ,PETIPCOM;
                  ,PEMASTRO;
                  ,PECATCON;
                  ,PECATCOM;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PE__ANNO;
                  ,this.w_PETIPCOM;
                  ,this.w_PEMASTRO;
                  ,this.w_PECATCON;
                  ,this.w_PECATCOM;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PAELCLFM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAELCLFM_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_PEMASTRO)) or not(Empty(t_PECATCON)) or not(Empty(t_PECATCOM))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PAELCLFM')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PAELCLFM')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_PEMASTRO)) or not(Empty(t_PECATCON)) or not(Empty(t_PECATCOM))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PAELCLFM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PAELCLFM')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PAELCLFM')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAELCLFM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAELCLFM_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_PEMASTRO)) or not(Empty(t_PECATCON)) or not(Empty(t_PECATCOM))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PAELCLFM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_PEMASTRO)) or not(Empty(t_PECATCON)) or not(Empty(t_PECATCOM))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAELCLFM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAELCLFM_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TIPMAS with this.w_TIPMAS
      replace t_NULIV with this.w_NULIV
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECATCON_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECATCON_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECATCOM_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPECATCOM_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PEMASTRO
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PEMASTRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_PEMASTRO)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_PEMASTRO))
          select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PEMASTRO)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PEMASTRO) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPEMASTRO_2_1'),i_cWhere,'GSAR_AMC',"",'GSCG_MPX.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PEMASTRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_PEMASTRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_PEMASTRO)
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PEMASTRO = NVL(_Link_.MCCODICE,space(15))
      this.w_MCDESCRI = NVL(_Link_.MCDESCRI,space(40))
      this.w_NULIV = NVL(_Link_.MCNUMLIV,0)
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PEMASTRO = space(15)
      endif
      this.w_MCDESCRI = space(40)
      this.w_NULIV = 0
      this.w_TIPMAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NULIV=1 AND (.w_TIPMAS='C' or .w_TIPMAS='F')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PEMASTRO = space(15)
        this.w_MCDESCRI = space(40)
        this.w_NULIV = 0
        this.w_TIPMAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PEMASTRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.MCCODICE as MCCODICE201"+ ",link_2_1.MCDESCRI as MCDESCRI201"+ ",link_2_1.MCNUMLIV as MCNUMLIV201"+ ",link_2_1.MCTIPMAS as MCTIPMAS201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on PAELCLFM.PEMASTRO=link_2_1.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and PAELCLFM.PEMASTRO=link_2_1.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PECATCON
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PECATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_PECATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_PECATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PECATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PECATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oPECATCON_2_5'),i_cWhere,'GSAR_AC2',"Categorie contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PECATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_PECATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_PECATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PECATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCON = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PECATCON = space(5)
      endif
      this.w_DESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PECATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOCLFO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.C2CODICE as C2CODICE205"+ ",link_2_5.C2DESCRI as C2DESCRI205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on PAELCLFM.PECATCON=link_2_5.C2CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and PAELCLFM.PECATCON=link_2_5.C2CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PECATCOM
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PECATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_PECATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_PECATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PECATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PECATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oPECATCOM_2_6'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PECATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_PECATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_PECATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PECATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCAC = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PECATCOM = space(3)
      endif
      this.w_DESCAC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PECATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATECOMM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.CTCODICE as CTCODICE206"+ ",link_2_6.CTDESCRI as CTDESCRI206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on PAELCLFM.PECATCOM=link_2_6.CTCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and PAELCLFM.PECATCOM=link_2_6.CTCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMCDESCRI_2_2.value==this.w_MCDESCRI)
      this.oPgFrm.Page1.oPag.oMCDESCRI_2_2.value=this.w_MCDESCRI
      replace t_MCDESCRI with this.oPgFrm.Page1.oPag.oMCDESCRI_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_2_7.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_2_7.value=this.w_DESCON
      replace t_DESCON with this.oPgFrm.Page1.oPag.oDESCON_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAC_2_8.value==this.w_DESCAC)
      this.oPgFrm.Page1.oPag.oDESCAC_2_8.value=this.w_DESCAC
      replace t_DESCAC with this.oPgFrm.Page1.oPag.oDESCAC_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEMASTRO_2_1.value==this.w_PEMASTRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEMASTRO_2_1.value=this.w_PEMASTRO
      replace t_PEMASTRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEMASTRO_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECATCON_2_5.value==this.w_PECATCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECATCON_2_5.value=this.w_PECATCON
      replace t_PECATCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECATCON_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECATCOM_2_6.value==this.w_PECATCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECATCOM_2_6.value=this.w_PECATCOM
      replace t_PECATCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPECATCOM_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'PAELCLFM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_NULIV=1 AND (.w_TIPMAS='C' or .w_TIPMAS='F')) and not(empty(.w_PEMASTRO)) and (not(Empty(.w_PEMASTRO)) or not(Empty(.w_PECATCON)) or not(Empty(.w_PECATCOM)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPEMASTRO_2_1
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_PEMASTRO)) or not(Empty(.w_PECATCON)) or not(Empty(.w_PECATCOM))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_PEMASTRO)) or not(Empty(t_PECATCON)) or not(Empty(t_PECATCOM)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PEMASTRO=space(15)
      .w_MCDESCRI=space(40)
      .w_TIPMAS=space(1)
      .w_NULIV=0
      .w_PECATCON=space(5)
      .w_PECATCOM=space(3)
      .w_DESCON=space(35)
      .w_DESCAC=space(35)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_PEMASTRO))
        .link_2_1('Full')
      endif
      .DoRTCalc(4,7,.f.)
      if not(empty(.w_PECATCON))
        .link_2_5('Full')
      endif
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_PECATCOM))
        .link_2_6('Full')
      endif
    endwith
    this.DoRTCalc(9,10,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PEMASTRO = t_PEMASTRO
    this.w_MCDESCRI = t_MCDESCRI
    this.w_TIPMAS = t_TIPMAS
    this.w_NULIV = t_NULIV
    this.w_PECATCON = t_PECATCON
    this.w_PECATCOM = t_PECATCOM
    this.w_DESCON = t_DESCON
    this.w_DESCAC = t_DESCAC
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PEMASTRO with this.w_PEMASTRO
    replace t_MCDESCRI with this.w_MCDESCRI
    replace t_TIPMAS with this.w_TIPMAS
    replace t_NULIV with this.w_NULIV
    replace t_PECATCON with this.w_PECATCON
    replace t_PECATCOM with this.w_PECATCOM
    replace t_DESCON with this.w_DESCON
    replace t_DESCAC with this.w_DESCAC
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mpxPag1 as StdContainer
  Width  = 634
  height = 239
  stdWidth  = 634
  stdheight = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=24, width=291,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="PEMASTRO",Label1="Mastri",Field2="PECATCON",Label2="Cat.cont.",Field3="PECATCOM",Label3="Cat.comm.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 99810182

  add object oStr_1_3 as StdString with uid="JPTDFDFWPB",Visible=.t., Left=13, Top=3,;
    Alignment=0, Width=277, Height=18,;
    Caption="Soggetti da escludere"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="WSMDLKYXAY",Visible=.t., Left=305, Top=54,;
    Alignment=0, Width=321, Height=18,;
    Caption="Descrizione mastri"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="NCZCAQMHDZ",Visible=.t., Left=305, Top=101,;
    Alignment=0, Width=321, Height=18,;
    Caption="Descrizione categoria contabile"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="DVLWAXIDOE",Visible=.t., Left=305, Top=149,;
    Alignment=0, Width=321, Height=18,;
    Caption="Descrizione categoria commerciale"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=43,;
    width=287+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=44,width=286+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='MASTRI|CACOCLFO|CATECOMM|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oMCDESCRI_2_2.Refresh()
      this.Parent.oDESCON_2_7.Refresh()
      this.Parent.oDESCAC_2_8.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='MASTRI'
        oDropInto=this.oBodyCol.oRow.oPEMASTRO_2_1
      case cFile='CACOCLFO'
        oDropInto=this.oBodyCol.oRow.oPECATCON_2_5
      case cFile='CATECOMM'
        oDropInto=this.oBodyCol.oRow.oPECATCOM_2_6
    endcase
    return(oDropInto)
  EndFunc


  add object oMCDESCRI_2_2 as StdTrsField with uid="WXEDSIRJYE",rtseq=4,rtrep=.t.,;
    cFormVar="w_MCDESCRI",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione mastro",;
    HelpContextID = 263152399,;
    cTotal="", bFixedPos=.t., cQueryName = "MCDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=305, Top=71, InputMask=replicate('X',40)

  add object oDESCON_2_7 as StdTrsField with uid="DBFPKMMEAL",rtseq=9,rtrep=.t.,;
    cFormVar="w_DESCON",value=space(35),enabled=.f.,;
    ToolTipText = "Descrizione categoria contabile",;
    HelpContextID = 175002678,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCON",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=305, Top=118, InputMask=replicate('X',35)

  add object oDESCAC_2_8 as StdTrsField with uid="DZGKWJCFVO",rtseq=10,rtrep=.t.,;
    cFormVar="w_DESCAC",value=space(35),enabled=.f.,;
    ToolTipText = "Descrizione categoria commerciale",;
    HelpContextID = 244208694,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCAC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=305, Top=166, InputMask=replicate('X',35)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mpxBodyRow as CPBodyRowCnt
  Width=277
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPEMASTRO_2_1 as StdTrsField with uid="OZQPBQPPWU",rtseq=3,rtrep=.t.,;
    cFormVar="w_PEMASTRO",value=space(15),isprimarykey=.t.,;
    ToolTipText = "Mastro da escludere nell'estrazione",;
    HelpContextID = 11269445,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=-2, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_PEMASTRO"

  func oPEMASTRO_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPEMASTRO_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPEMASTRO_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPEMASTRO_2_1.readonly and this.parent.oPEMASTRO_2_1.isprimarykey)
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPEMASTRO_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"",'GSCG_MPX.MASTRI_VZM',this.parent.oContained
   endif
  endproc
  proc oPEMASTRO_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_PEMASTRO
    i_obj.ecpSave()
  endproc

  add object oPECATCON_2_5 as StdTrsField with uid="PYHLCGKKYU",rtseq=7,rtrep=.t.,;
    cFormVar="w_PECATCON",value=space(5),isprimarykey=.t.,;
    ToolTipText = "Categoria contabile da escludere nell'estrazione",;
    HelpContextID = 263935300,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=155, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_PECATCON"

  func oPECATCON_2_5.mCond()
    with this.Parent.oContained
      return (type("I_SRV")='C' AND I_SRV="A")
    endwith
  endfunc

  func oPECATCON_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPECATCON_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPECATCON_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPECATCON_2_5.readonly and this.parent.oPECATCON_2_5.isprimarykey)
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oPECATCON_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili",'',this.parent.oContained
   endif
  endproc
  proc oPECATCON_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_PECATCON
    i_obj.ecpSave()
  endproc

  add object oPECATCOM_2_6 as StdTrsField with uid="JXCIXEKAVI",rtseq=8,rtrep=.t.,;
    cFormVar="w_PECATCOM",value=space(3),isprimarykey=.t.,;
    ToolTipText = "Categegoria commerciale da escludere nell'estrazione",;
    HelpContextID = 263935299,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=224, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_PECATCOM"

  func oPECATCOM_2_6.mCond()
    with this.Parent.oContained
      return (type("I_SRV")='C' AND I_SRV="A")
    endwith
  endfunc

  func oPECATCOM_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPECATCOM_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPECATCOM_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPECATCOM_2_6.readonly and this.parent.oPECATCOM_2_6.isprimarykey)
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oPECATCOM_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
   endif
  endproc
  proc oPECATCOM_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_PECATCOM
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover
  * ---
  func oPEMASTRO_2_1.When()
    return(.t.)
  proc oPEMASTRO_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPEMASTRO_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mpx','PAELCLFM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PE__ANNO=PAELCLFM.PE__ANNO";
  +" and "+i_cAliasName2+".PETIPCOM=PAELCLFM.PETIPCOM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
