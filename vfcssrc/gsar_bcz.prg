* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bcz                                                        *
*              Lancia i contratti                                              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_5]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-11-11                                                      *
* Last revis.: 2008-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bcz",oParentObject)
return(i_retval)

define class tgsar_bcz as StdBatch
  * --- Local variables
  w_TIPCON = space(1)
  w_PROG = space(8)
  w_CONTI = space(1)
  w_TIPO = space(1)
  w_DXBTN = .f.
  w_CODICE = space(15)
  * --- WorkFile variables
  CON_TRAM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia i Contratti
    * --- Se la variabile che riferisce all'ultimo form aperto � vuota esco
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      if g_oMenu.nKeyCount > 1
        * --- Ho le due chiavi (tipo e codice)
        this.w_TIPCON = Nvl(g_oMenu.oKey(1,3),"")
        this.w_CODICE = Nvl(g_oMenu.oKey(2,3),"") 
      else
        * --- Se ho solo il codice, cerco il tipo sul db
        this.w_CODICE = Nvl(g_oMenu.oKey(1,3),"")
        * --- Read from CON_TRAM
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CON_TRAM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2],.t.,this.CON_TRAM_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "COTIPCLF"+;
            " from "+i_cTable+" CON_TRAM where ";
                +"CONUMERO = "+cp_ToStrODBC(this.w_CODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            COTIPCLF;
            from (i_cTable) where;
                CONUMERO = this.w_CODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPCON = NVL(cp_ToDate(_read_.COTIPCLF),cp_NullValue(_read_.COTIPCLF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    else
      if i_curform=.NULL.
        i_retcode = 'stop'
        return
      endif
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      * --- Tipo di Conto
      this.w_TIPCON = &cCurs..COTIPCLF
    endif
    if EMPTY(this.w_TIPCON) OR NOT this.w_TIPCON $ "CF" And Not this.w_DXBTN
      * --- Se non � stato riconosciuto in precedenza
      * --- legge il tipo di conto partendo dalla espressione di Where della select..
      cInit=i_curform.z1.o_initsel
      if TYPE ("cInit") ="C"
        cInit=UPPER(cInit)
        this.w_CONTI = AT("COTIPCLF=", UPPER(cInit))
        if this.w_CONTI<>0
          this.w_CONTI = this.w_CONTI+10
          if LEN(ALLTRIM(cInit))>=this.w_CONTI
            this.w_TIPO = SUBSTR(cInit, this.w_CONTI,1)
            this.w_TIPCON = IIF(this.w_TIPO $ "CF", this.w_TIPO, this.w_TIPCON)
          endif
        endif
      endif
    endif
    do case
      case this.w_TIPCON="C"
        * --- Lancia Contratti di Vendita
        this.w_PROG = "GSVE_MCO"
      case this.w_TIPCON="F"
        * --- Lancia Contratti di Acquisto
        this.w_PROG = "GSAC_MCO"
    endcase
    OpenGest(iif(this.w_DXBTN,g_oMenu.cBatchType,"S"),this.w_PROG,"CONUMERO",this.w_CODICE)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CON_TRAM'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
