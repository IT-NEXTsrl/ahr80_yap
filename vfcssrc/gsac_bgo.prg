* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bgo                                                        *
*              Generazione ordini da PDA                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_514]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-21                                                      *
* Last revis.: 2016-12-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bgo",oParentObject)
return(i_retval)

define class tgsac_bgo as StdBatch
  * --- Local variables
  w_PDDATORD = ctod("  /  /  ")
  w_SERIAL = space(10)
  w_APPO = space(10)
  w_MVCLADOC = space(2)
  w_ROWNUM = 0
  w_NUDOC = 0
  w_CODART = space(20)
  w_OKDOC = 0
  w_CODICE1 = space(20)
  w_DESART = space(40)
  w_DESSUP = space(0)
  w_CODCON = space(15)
  Padre = .NULL.
  w_TIPCON = space(1)
  NC = space(10)
  w_CODLIN = space(3)
  w_DESART1 = space(40)
  w_DESSUP1 = space(0)
  w_CALPRZ = 0
  w_AGGSAL = space(10)
  w_ARDATINT = space(1)
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_COMMAPPO = space(15)
  w_SEDESPRE = space(1)
  w_SEDESPRE = space(1)
  w_CODSED = space(5)
  w_MVTFLCOM = space(1)
  w_DBRK = space(10)
  w_DTOBSO = ctod("  /  /  ")
  w_MVTIPOPE = space(10)
  w_NAZDES = space(3)
  w_OK_LET = .f.
  w_NUMCOR = space(25)
  w_ANTIPOPE = space(10)
  w_ARTIPOPE = space(10)
  w_PREZUM = space(1)
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_CLUNIMIS = space(3)
  w_QTALIS = 0
  w_LISCON = 0
  w_DECCOM = 0
  w_CPROWORD = 0
  w_MVCONTRA = space(15)
  w_MVSERRIF = space(10)
  w_MVIVAINC = space(5)
  w_MVSPEINC = 0
  w_MVNUMRIF = 0
  w_MVCONIND = space(15)
  w_MVROWRIF = 0
  w_MVFLRINC = space(1)
  w_MVFLSALD = space(1)
  w_CPROWNUM = 0
  w_MVTIPRIG = space(1)
  w_MVCODLIS = space(5)
  w_MVSPEIMB = 0
  w_MVFLORDI = space(1)
  w_MVCODICE = space(20)
  w_MVQTAMOV = 0
  w_MVTINCOM = ctod("  /  /  ")
  w_MVIVAIMB = space(5)
  w_MVFLIMPE = space(1)
  w_MVCODART = space(20)
  w_MVQTAUM1 = 0
  w_MVACCONT = 0
  w_MVFLRIMB = space(1)
  w_MVCODMAG = space(5)
  w_MVPREZZO = 0
  w_MVSCOCL1 = 0
  w_MVSPETRA = 0
  w_MVVOCCEN = space(15)
  w_MVDESART = space(40)
  w_MVSCONT1 = 0
  w_MVSCOCL2 = 0
  w_MVIVATRA = space(5)
  w_MVCODCEN = space(15)
  w_MVDESSUP = space(10)
  w_MV__NOTE = space(10)
  w_MVSCONT2 = 0
  w_MVSCOPAG = 0
  w_MVFLRTRA = space(1)
  w_MVCODCOM = space(15)
  w_MVUNIMIS = space(3)
  w_MVSCONT3 = 0
  w_MVCAUMAG = space(5)
  w_MVSPEBOL = 0
  w_MVCATCON = space(5)
  w_MVSCONT4 = 0
  w_MVMOLSUP = 0
  w_MVIVABOL = space(5)
  w_MVFLSCOR = space(1)
  w_MVCODCLA = space(3)
  w_MVFLOMAG = space(1)
  w_MVSCONTI = 0
  w_MVDATDIV = ctod("  /  /  ")
  w_MVCODCON = space(15)
  w_MVCODIVA = space(5)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVCODIVE = space(5)
  w_MVCODPAG = space(5)
  w_MVKEYSAL = space(20)
  w_MVVALNAZ = space(3)
  w_MFLAVAL = space(1)
  w_MVCODAGE = space(5)
  w_MVCODBAN = space(10)
  w_MVCODBA2 = space(10)
  w_MVCODPOR = space(1)
  w_MVCAOVAL = 0
  w_MVTCONTR = space(15)
  w_MVCODVAL = space(3)
  w_MVCODVET = space(5)
  w_MVVALRIG = 0
  w_MVFLELGM = space(1)
  w_MVPESNET = 0
  w_MVCODSPE = space(3)
  w_MVIMPSCO = 0
  w_MVRIFDIC = space(10)
  w_MVFLTRAS = space(1)
  w_MVTIPATT = space(1)
  w_MVVALMAG = 0
  w_MVPESNET = 0
  w_MVTCOLIS = space(5)
  w_MVNOMENC = space(8)
  w_MVCODATT = space(15)
  w_MVIMPNAZ = 0
  w_MVACIVA1 = space(5)
  w_MVAIMPN1 = 0
  w_MVAFLOM1 = space(1)
  w_MVAIMPS1 = 0
  w_MVUMSUPP = space(3)
  w_MVACIVA2 = space(5)
  w_MVAIMPN2 = 0
  w_MVAFLOM2 = space(1)
  w_MVAIMPS2 = 0
  w_MVFLVEAC = space(1)
  w_MVACIVA3 = space(5)
  w_MVAIMPN3 = 0
  w_MVAFLOM3 = space(1)
  w_MVAIMPS3 = 0
  w_MVCODDES = space(5)
  w_MVACIVA4 = space(5)
  w_MVAIMPN4 = 0
  w_CAOCON = 0
  w_VALCON = space(3)
  w_MVAFLOM4 = space(1)
  w_MVAIMPS4 = 0
  w_MVIMPARR = 0
  w_MVACIVA5 = space(5)
  w_MVAIMPN5 = 0
  w_MVAFLOM5 = space(1)
  w_MVAIMPS5 = 0
  w_MVDATEVA = ctod("  /  /  ")
  w_MVACIVA6 = space(5)
  w_MVAIMPN6 = 0
  w_MVAFLOM6 = space(1)
  w_MVAIMPS6 = 0
  w_PDSERIAL = space(10)
  w_MVACCPRE = 0
  w_MVTOTRIT = 0
  w_MVTOTENA = 0
  w_MVIMPACC = 0
  w_PDROWNUM = 0
  w_MVVOCCOS = space(15)
  w_MVRITPRE = 0
  w_CAOVAL = 0
  w_BOLARR = 0
  w_PERIVA = 0
  w_PEIINC = 0
  w_MFLCASC = space(1)
  w_IMPARR = 0
  w_BOLMIN = 0
  w_BOLIVA = space(1)
  w_BOLINC = space(1)
  w_MFLORDI = space(1)
  w_MFLORDI1 = space(1)
  w_RSIMPRAT = 0
  w_TOTMERCE = 0
  w_MESE1 = 0
  w_PEIIMB = 0
  w_MFLIMPE = space(1)
  w_DECTOT = 0
  w_TOTALE = 0
  w_MESE2 = 0
  w_BOLIMB = space(1)
  w_MFLRISE = space(1)
  w_BOLESE = 0
  w_TOTIMPON = 0
  w_GIORN1 = 0
  w_PEITRA = 0
  w_MFLELGM = space(1)
  w_BOLSUP = 0
  w_TOTIMPOS = 0
  w_GIORN2 = 0
  w_BOLTRA = space(1)
  w_MFLCOMM = space(1)
  w_BOLCAM = 0
  w_TOTFATTU = 0
  w_GIOFIS = 0
  w_BOLBOL = space(1)
  w_PERIVE = 0
  w_CAONAZ = 0
  w_CLBOLFAT = space(1)
  w_BOLIVE = space(1)
  w_RSMODPAG = space(10)
  w_RSNUMRAT = 0
  w_MVDATEVA = ctod("  /  /  ")
  w_RSDATRAT = ctod("  /  /  ")
  w_CODESE = space(4)
  w_ACCPRE = 0
  w_ORDNUM = 0
  w_CODNAZ = space(3)
  w_APPO1 = 0
  w_DETNUM = 0
  w_ACQINT = space(1)
  w_CATCOM = space(3)
  w_IVACON = space(1)
  w_FLFOBO = space(1)
  w_UNMIS1 = space(3)
  w_LIPREZZO = 0
  w_IVALIS = space(1)
  w_UNMIS2 = space(3)
  w_CODART = space(20)
  w_CATSCC = space(5)
  w_QUALIS = space(1)
  w_UNMIS3 = space(3)
  w_CATSCM = space(5)
  w_DECUNI = 0
  w_MOLTIP = 0
  w_DATREG = ctod("  /  /  ")
  w_GRUMER = space(5)
  w_QUAN = 0
  w_MOLTI3 = 0
  w_CODVAL = space(3)
  w_ROWNUM = 0
  w_QTAUM1 = 0
  w_OPERAT = space(1)
  w_CODLIS = space(5)
  w_OPERA3 = space(1)
  w_CODCON = space(15)
  w_OK = .f.
  w_CATCLI = space(5)
  w_PZ0 = 0
  w_PZ1 = 0
  w_PZ2 = 0
  w_OK0 = .f.
  w_CATART = space(5)
  w_S10 = 0
  w_S11 = 0
  w_S12 = 0
  w_OK1 = .f.
  w_ARRSUP = 0
  w_S20 = 0
  w_S21 = 0
  w_S22 = 0
  w_OK2 = .f.
  w_SCOCON = .f.
  w_S30 = 0
  w_S31 = 0
  w_S32 = 0
  w_DT0 = ctod("  /  /  ")
  w_CODGRU = space(5)
  w_S40 = 0
  w_S41 = 0
  w_S42 = 0
  w_DT1 = ctod("  /  /  ")
  w_OK_ANALI = .f.
  w_RN0 = 0
  w_RN1 = 0
  w_RN2 = 0
  w_OK_COMM = .f.
  w_PAGA = space(1)
  w_SCOLIS = space(1)
  w_FLSERA = space(1)
  w_INDIVE = 0
  w_INDIVA = 0
  w_MVFLVABD = space(1)
  w_MVPROORD = space(2)
  w_MVNAZPRO = space(3)
  w_MVPAEFOR = space(3)
  w_MVCONCON = space(1)
  DR = space(10)
  w_LOOP = 0
  w_MVFLSCOM = space(1)
  w_DATCOM = ctod("  /  /  ")
  w_ANFLAPCA = space(1)
  w_TDFLAPCA = space(1)
  w_ARFLAPCA = space(1)
  w_MVFLORCO = space(1)
  w_MVIMPCOM = 0
  w_COCODVAL = space(3)
  w_MVCODCOS = space(5)
  w_CODCOS = space(5)
  w_WARN_COMM = .f.
  w_MVRIFCAC = 0
  w_MVCACONT = space(5)
  w_SEARCHROW = 0
  w_ACTPOS = 0
  w_WARN_OBSO = .f.
  w_ARDTOBSO = ctod("  /  /  ")
  w_MESSAGG = space(20)
  w_RSDESRIG = space(50)
  w_RSBANNOS = space(15)
  w_RSBANAPP = space(10)
  w_RSFLSOSP = space(1)
  w_RSCONCOR = space(25)
  w_RSFLPROV = space(1)
  w_COFRAZ = space(1)
  w_UNIMIS = space(3)
  w_KCODICE = space(20)
  w_KUNIMIS = space(3)
  w_KQTAMOV = 0
  w_KPREZZO = 0
  w_KCODIVA = space(3)
  w_CONTRIB = space(5)
  w_RIFCACESP = 0
  w_CATDOC = space(2)
  w_POSAPP = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAM_AGAZ_idx=0
  CONTI_idx=0
  CON_COSC_idx=0
  CON_TRAM_idx=0
  DES_DIVE_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  KEY_ARTI_idx=0
  LISTINI_idx=0
  LIS_SCAG_idx=0
  OUT_PUTS_idx=0
  PAG_2AME_idx=0
  PAG_AMEN_idx=0
  PDA_DETT_idx=0
  SALDIART_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  TRADARTI_idx=0
  CAN_TIER_idx=0
  VOC_COST_idx=0
  MA_COSTI_idx=0
  TIP_DOCU_idx=0
  UNIMIS_idx=0
  AGENTI_idx=0
  SALDICOM_idx=0
  AZIENDA_idx=0
  SEDIAZIE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametro Esegue: I = Non Spunta Documenti; D = Spunta Documenti
    * --- Generazione Automatica Ordini da PDA (da GSAC_KGO)
    this.w_SEDESPRE = "S"
    this.w_CODSED = SPACE(5)
    this.w_MVDATDIV = this.oParentObject.w_DATDIV
    this.w_MVVALNAZ = g_PERVAL
    this.w_CAONAZ = GETCAM(this.w_MVVALNAZ, this.w_MVDATDOC, 0)
    this.w_MVFLVEAC = "A"
    this.w_CODESE = this.oParentObject.w_MVCODESE
    this.w_FLFOBO = " "
    this.w_ACQINT = "N"
    this.w_MVCLADOC = "OR"
    this.Padre = this.oParentObject
    this.NC = this.Padre.w_ZoomSel.cCursor
    * --- Legge la sede preferita di consegna merce
    * --- Read from SEDIAZIE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SECODDES"+;
        " from "+i_cTable+" SEDIAZIE where ";
            +"SECODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and SEDESPRE = "+cp_ToStrODBC(this.w_SEDESPRE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SECODDES;
        from (i_cTable) where;
            SECODAZI = i_CODAZI;
            and SEDESPRE = this.w_SEDESPRE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODSED = NVL(cp_ToDate(_read_.SECODDES),cp_NullValue(_read_.SECODDES))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Legge Informazioni Commessa da Causale
    if g_COMM="S"
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDFLCOMM,TDFLAPCA,TDCATDOC"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDFLCOMM,TDFLAPCA,TDCATDOC;
          from (i_cTable) where;
              TDTIPDOC = this.oParentObject.w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVTFLCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
        this.w_TDFLAPCA = NVL(cp_ToDate(_read_.TDFLAPCA),cp_NullValue(_read_.TDFLAPCA))
        this.w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_MVTFLCOM = SPACE(1)
    endif
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    * --- Legge Informazioni di Riga di Default
    this.w_MVCAUMAG = this.oParentObject.w_MVTCAMAG
    this.w_MFLCASC = " "
    this.w_MFLORDI = " "
    this.w_MFLIMPE = " "
    this.w_MFLRISE = " "
    this.w_MFLELGM = " "
    this.w_MFLAVAL = " "
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVTCAMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM;
        from (i_cTable) where;
            CMCODICE = this.oParentObject.w_MVTCAMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
      this.w_MFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_MFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.w_MFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      this.w_MFLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
      this.w_MFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
      this.w_MFLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MFLORDI1 = this.w_MFLORDI
    this.w_MFLCASC = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLCASC)
    this.w_MFLORDI = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLORDI)
    this.w_MFLIMPE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLIMPE)
    this.w_MFLRISE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLRISE)
    this.w_AGGSAL = ALLTRIM(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE)
    * --- Blocco
    do case
      case NOT USED(this.NC)
        ah_ErrorMsg("Non esistono dati da generare")
        i_retcode = 'stop'
        return
      case EMPTY(this.w_MFLORDI1) 
        ah_ErrorMsg("La causale magazzino non movimenta l'ordinato; non utilizzabile")
        i_retcode = 'stop'
        return
      case Not Empty(CHKCONS(this.w_MVFLVEAC+IIF(NOT EMPTY(this.w_AGGSAL),"M",""),this.oParentObject.w_DATDOC,"B","S"))
        * --- Eseguo controllo su data consolidamento impostata nei dati azienda
        i_retcode = 'stop'
        return
    endcase
    * --- Carica i Documenti da Generare
    ah_Msg("Ricerca proposte di acquisto da elaborare...")
    SELECT * FROM (this.NC) WHERE XCHK=1 and not empty(nvl(PDCODCON," ")) and not empty(nvl(PDCODMAG," ")) ;
    INTO CURSOR GENEORDI ORDER BY PDCODCON,PDDATORD
    * --- Rende il Cursore Editabile
    CreaCur=WRCURSOR("GENEORDI")
    if USED("GENEORDI")
      * --- Inizio Aggiornamento
      this.w_OKDOC = 0
      this.w_NUDOC = 0
      if RECCOUNT("GENEORDI") > 0
        ah_Msg("Inizio fase di generazione...",.T.)
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_APPO = "Operazione completata%0N.%1 ordini generati%0su %2 ordini da generare"
        ah_ErrorMsg(this.w_APPO,,"", ALLTRIM(STR(this.w_OKDOC)), ALLTRIM(STR(this.w_NUDOC)) )
        this.Padre.NotifyEvent("Interroga")     
      else
        this.w_APPO = "Per l'intervallo selezionato non esistono documenti da generare"
        ah_ErrorMsg(this.w_APPO,,"")
      endif
      * --- Chiude i Cursori lasciati Aperti
      use in GENEORDI
    endif
    if USED("GeneApp")
      * --- Elimina Cursore di Appoggio se esiste ancora
      use in GeneApp
    endif
    if USED("PAGAM")
      * --- Elimina Cursore di Appoggio se esiste ancora
      use in PAGAM
    endif
    if USED("pda_err")
      * --- Stampa errori
      SELECT * from pda_err into cursor __tmp__
      cp_chprn ("query\gsac_sep", " ", this)
      use in pda_err
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera i Documenti di Ordine da PDA
    * --- Testa il Cambio di Fornitore
    this.w_DBRK = "##zz##"
    SELECT GENEORDI
    GO TOP
    SCAN FOR NOT EMPTY(NVL(PDSERIAL," "))
    * --- Testa Cambio Fornitore
    if this.w_DBRK<>PDCODCON+IIF(this.oParentObject.w_FLDATA="S",DTOS(CP_TODATE(PDDATORD)), "")
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT GENEORDI
      * --- Inizializza i dati di Testata del Nuovo Documento
      this.w_PDDATORD = CP_TODATE(PDDATORD)
      this.w_MVCODCON = PDCODCON
      this.w_MVCODIVE = NVL(CODIVE, SPACE(5))
      this.w_MVCODPAG = NVL(CODPAG, SPACE(5))
      this.w_MVCODBAN = NVL(CODBAN, SPACE(10))
      this.w_MVCODBA2 = ANCODBA2
      this.w_MVCODVAL = NVL(CODVAL, SPACE(5))
      this.w_MVSCOCL1 = NVL(SCOCL1,0)
      this.w_MVSCOCL2 = NVL(SCOCL2,0)
      this.w_MVSCOPAG = NVL(SCOPAG,0)
      this.w_MVFLSCOR = NVL(FLSCOR, "N")
      this.w_MVTCOLIS = NVL(MVTCOLIS, SPACE(5))
      this.w_MVTCONTR = SPACE(15)
      this.w_MVCODVAL = IIF(EMPTY(this.w_MVCODVAL), this.w_MVVALNAZ, this.w_MVCODVAL)
      this.w_MVCAOVAL = this.w_CAONAZ
      this.oParentObject.w_DATDOC = IIF(this.oParentObject.w_FLDATA="S",this.w_PDDATORD,this.oParentObject.w_DATDOC)
      this.w_MVDATDOC = this.oParentObject.w_DATDOC
      this.oParentObject.w_MVDATCIV = this.oParentObject.w_DATDOC
      this.oParentObject.w_MVDATREG = this.oParentObject.w_DATDOC
      this.w_MV__NOTE = space(10)
      if this.w_MVCODVAL<>this.w_MVVALNAZ
        this.w_MVCAOVAL = GETCAM(this.w_MVCODVAL, this.w_MVDATDOC, 0)
      endif
      this.w_MVCAOVAL = IIF(this.w_MVCAOVAL=0, g_CAOVAL, this.w_MVCAOVAL)
      * --- Legge Dati Associati alla Valuta
      this.w_DECTOT = 0
      this.w_DECUNI = 0
      this.w_BOLESE = 0
      this.w_BOLSUP = 0
      this.w_BOLCAM = 0
      this.w_BOLARR = 0
      this.w_BOLMIN = 0
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI;
          from (i_cTable) where;
              VACODVAL = this.w_MVCODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        this.w_BOLESE = NVL(cp_ToDate(_read_.VABOLESE),cp_NullValue(_read_.VABOLESE))
        this.w_BOLSUP = NVL(cp_ToDate(_read_.VABOLSUP),cp_NullValue(_read_.VABOLSUP))
        this.w_BOLCAM = NVL(cp_ToDate(_read_.VABOLCAM),cp_NullValue(_read_.VABOLCAM))
        this.w_BOLARR = NVL(cp_ToDate(_read_.VABOLARR),cp_NullValue(_read_.VABOLARR))
        this.w_BOLMIN = NVL(cp_ToDate(_read_.VABOLMIM),cp_NullValue(_read_.VABOLMIM))
        this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT GENEORDI
      * --- calcola le Spese di Incasso
      this.w_MVSPEINC = 0
      this.w_MVSPEIMB = 0
      this.w_MVSPETRA = 0
      this.w_MVSPEBOL = 0
      this.w_MVIMPARR = 0
      this.w_MVACCPRE = 0
      this.w_MVIVAINC = SPACE(5)
      this.w_MVIVAIMB = SPACE(5)
      this.w_MVIVATRA = SPACE(5)
      this.w_MVIVABOL = SPACE(5)
      this.w_MVCODVET = SPACE(5)
      this.w_MVCODSPE = SPACE(3)
      this.w_MVCODPOR = " "
      this.w_GIORN1 = NVL(ANGIOSC1, 0)
      this.w_GIORN2 = NVL(ANGIOSC2, 0)
      this.w_MESE1 = NVL(AN1MESCL, 0)
      this.w_MESE2 = NVL(AN2MESCL, 0)
      this.w_GIOFIS = NVL(ANGIOFIS, 0)
      this.w_CLBOLFAT = NVL(ANBOLFAT, " ")
      this.w_CODNAZ = NVL(ANNAZION, g_CODNAZ)
      this.w_CATSCC = NVL(ANCATSCM, SPACE(5))
      this.w_CAOCON = this.w_MVCAOVAL
      this.w_VALCON = this.w_MVCODVAL
      this.w_BOLINC = " "
      this.w_BOLIMB = " "
      this.w_BOLTRA = " "
      this.w_BOLBOL = " "
      this.w_BOLIVE = " "
      this.w_PEIINC = 0
      this.w_PEIIMB = 0
      this.w_PEITRA = 0
      this.w_PERIVE = 0
      this.w_IVACON = " "
      this.w_IVALIS = " "
      this.w_QUALIS = " "
      this.w_CODLIN = SPACE(3)
      this.w_DESART1 = SPACE(40)
      this.w_DESSUP1 = SPACE(10)
      this.w_MVTIPOPE = Space(10)
      * --- Propone dati Accompagnatori (Riferimento: 'CO' = Consegna) Predefinita
      * --- Select from DES_DIVE
      i_nConn=i_TableProp[this.DES_DIVE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" DES_DIVE ";
            +" where DDTIPCON="+cp_ToStrODBC(this.oParentObject.w_MVTIPCON)+" AND DDCODICE="+cp_ToStrODBC(this.w_MVCODCON)+" AND DDTIPRIF='CO' AND DDPREDEF='S'";
             ,"_Curs_DES_DIVE")
      else
        select * from (i_cTable);
         where DDTIPCON=this.oParentObject.w_MVTIPCON AND DDCODICE=this.w_MVCODCON AND DDTIPRIF="CO" AND DDPREDEF="S";
          into cursor _Curs_DES_DIVE
      endif
      if used('_Curs_DES_DIVE')
        select _Curs_DES_DIVE
        locate for 1=1
        do while not(eof())
        this.w_DTOBSO = Cp_ToDate(_Curs_DES_DIVE.DDDTOBSO)
        if this.w_DTOBSO>this.w_MVDATDOC Or Empty(this.w_DTOBSO)
          this.w_MVCODDES = _Curs_DES_DIVE.DDCODDES
          this.w_MVCODVET = Nvl(_Curs_DES_DIVE.DDCODVET,Space(5))
          this.w_MVCODPOR = Nvl(_Curs_DES_DIVE.DDCODPOR," ")
          this.w_MVCODSPE = Nvl(_Curs_DES_DIVE.DDCODSPE,Space(3))
          this.w_MVCODAGE = Nvl(_Curs_DES_DIVE.DDCODAGE,Space(5))
          this.w_MVTIPOPE = Nvl( _Curs_DES_DIVE.DDTIPOPE , Space(10) )
          this.w_NAZDES = Nvl( _Curs_DES_DIVE.DDCODNAZ , Space(3) )
          if Not Empty(this.w_MVCODAGE)
            * --- Lettura Capoarea
            * --- Read from AGENTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.AGENTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AGCZOAGE"+;
                " from "+i_cTable+" AGENTI where ";
                    +"AGCODAGE = "+cp_ToStrODBC(this.w_MVCODAGE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AGCZOAGE;
                from (i_cTable) where;
                    AGCODAGE = this.w_MVCODAGE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              w_MVCODAG2 = NVL(cp_ToDate(_read_.AGCZOAGE),cp_NullValue(_read_.AGCZOAGE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          EXIT
        endif
          select _Curs_DES_DIVE
          continue
        enddo
        use
      endif
      * --- Lettura lettera di intento valida
      DECLARE ARRDIC (14,1)
      * --- Svuoto il riferimento alla Dichiarazione di Intento
      this.w_MVRIFDIC = Space(15)
      * --- Azzero l'Array che verr� riempito dalla Funzione
      ARRDIC(1)=0
      this.w_OK_LET = CAL_LETT(this.w_MVDATDOC,this.oParentObject.w_MVTIPCON,this.w_MVCODCON, @ArrDic, space(10),this.w_MVCODDES)
      if this.w_OK_LET
        * --- Parametri
        *     pDatRif : Data di Riferimento per filtro su Lettere di intento
        *     pTipCon : Tipo Conto : 'C' Clienti, 'F' : Fornitori
        *     pCodCon : Codice Cliente/Fornitore
        *     pArrDic : Array passato per riferimento: conterr� anche i dati letti dalla lettera di intento
        *     
        *     pArrDic[ 1 ]   = Numero Dichiarazione
        *     pArrDic[ 2 ]   = Tipo Operazione
        *     pArrDic[ 3 ]   = Anno Dichiarazione
        *     pArrDic[ 4 ]   = Importo Dichiarazione
        *     pArrDic[ 5 ]   = Data Dichiarazione
        *     pArrDic[ 6 ]   = Importo Utilizzato
        *     pArrDic[ 7 ]   = Tipo conto: Cliente/Fornitore
        *     pArrDic[ 8 ]   = Codice Iva Agevolata
        *     pArrDic[ 9 ]   = Tipo Iva (Agevolata o senza)
        *     pArrDic[ 10 ] = Data Inizio Validit�
        *     pArrDic[ 11 ] = Codice Dichiarazione (Se a clienti = codice Cliente, Se Fornitori= codice progressivo)
        *     pArrDic[ 12 ] = Data Obsolescenza
        this.w_MVCODIVE = ArrDic(8)
        this.w_MVRIFDIC = ArrDic(11)
      endif
      if NOT EMPTY(this.w_MVTCOLIS)
        * --- Read from LISTINI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LISTINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LSIVALIS,LSQUANTI,LSFLSCON"+;
            " from "+i_cTable+" LISTINI where ";
                +"LSCODLIS = "+cp_ToStrODBC(this.w_MVTCOLIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LSIVALIS,LSQUANTI,LSFLSCON;
            from (i_cTable) where;
                LSCODLIS = this.w_MVTCOLIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_IVALIS = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
          this.w_QUALIS = NVL(cp_ToDate(_read_.LSQUANTI),cp_NullValue(_read_.LSQUANTI))
          this.w_SCOLIS = NVL(cp_ToDate(_read_.LSFLSCON),cp_NullValue(_read_.LSFLSCON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT GENEORDI
      endif
      if NOT EMPTY(this.w_MVCODIVE)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVBOLIVA,IVPERIVA,IVPERIND"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVBOLIVA,IVPERIVA,IVPERIND;
            from (i_cTable) where;
                IVCODIVA = this.w_MVCODIVE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_BOLIVE = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
          this.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_INDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT GENEORDI
      endif
      this.w_DBRK = this.w_MVCODCON+IIF(this.oParentObject.w_FLDATA="S", DTOS(this.w_PDDATORD), "")
    endif
    this.w_MVFLVABD = " "
    * --- Leggo anche il conto corrente.
    *     ANNUMCOR viene sempre valorizzato con il conto corrente di default della tabella BAN_CONTI
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCODLIN,ANFLACBD,ANCATSCM,ANNUMCOR,AFFLINTR,ANFLAPCA,ANTIPOPE"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCODLIN,ANFLACBD,ANCATSCM,ANNUMCOR,AFFLINTR,ANFLAPCA,ANTIPOPE;
        from (i_cTable) where;
            ANTIPCON = this.oParentObject.w_MVTIPCON;
            and ANCODICE = this.w_MVCODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODLIN = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
      this.w_MVFLVABD = NVL(cp_ToDate(_read_.ANFLACBD),cp_NullValue(_read_.ANFLACBD))
      this.w_CATCLI = NVL(cp_ToDate(_read_.ANCATSCM),cp_NullValue(_read_.ANCATSCM))
      this.w_NUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
      this.w_ACQINT = NVL(cp_ToDate(_read_.AFFLINTR),cp_NullValue(_read_.AFFLINTR))
      this.w_ANFLAPCA = NVL(cp_ToDate(_read_.ANFLAPCA),cp_NullValue(_read_.ANFLAPCA))
      this.w_ANTIPOPE = NVL(cp_ToDate(_read_.ANTIPOPE),cp_NullValue(_read_.ANTIPOPE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Empty(this.w_MVCODBAN)
      * --- Il numero di conto corrente devo riportarlo solo se c'� la banca 
      this.w_NUMCOR = Space(25)
    endif
    SELECT GENEORDI
    * --- Scrive nuova Riga sul Temporaneo di Appoggio
    this.w_MVTIPRIG = "R"
    this.w_MVCODICE = NVL(PDCODICE, SPACE(20))
    this.w_MVCODART = NVL(PDCODART, SPACE(20))
    this.w_MVUNIMIS = NVL(PDUMORDI, SPACE(3))
    this.w_MVCATCON = NVL(ARCATCON, SPACE(5))
    this.w_MVCODCLA = NVL(ARCODCLA, SPACE(3))
    this.w_MVCONTRA = SPACE(15)
    * --- Read from TRADARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TRADARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2],.t.,this.TRADARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LGDESCOD,LGDESSUP"+;
        " from "+i_cTable+" TRADARTI where ";
            +"LGCODICE = "+cp_ToStrODBC(this.w_MVCODICE);
            +" and LGCODLIN = "+cp_ToStrODBC(this.w_CODLIN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LGDESCOD,LGDESSUP;
        from (i_cTable) where;
            LGCODICE = this.w_MVCODICE;
            and LGCODLIN = this.w_CODLIN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESART1 = NVL(cp_ToDate(_read_.LGDESCOD),cp_NullValue(_read_.LGDESCOD))
      this.w_DESSUP1 = NVL(cp_ToDate(_read_.LGDESSUP),cp_NullValue(_read_.LGDESSUP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT GENEORDI
    this.w_MVDESART = IIF(NOT EMPTY(NVL(this.w_DESART1,SPACE(40))),this.w_DESART1,NVL(CADESART, SPACE(40)))
    this.w_MVDESSUP = IIF(NOT EMPTY(NVL(this.w_DESSUP1,SPACE(10))),this.w_DESSUP1,NVL(CADESSUP, SPACE(10)))
    this.w_GRUMER = NVL(ARGRUMER, SPACE(5))
    this.w_CATSCM = NVL(ARCATSCM, SPACE(5))
    this.w_MVCODLIS = this.w_MVTCOLIS
    this.w_MVQTAMOV = NVL(PDQTAORD, 0)
    this.w_MVQTAUM1 = NVL(PDQTAUM1, 0)
    this.w_MVDATEVA = CP_TODATE(PDDATEVA)
    this.w_MVPREZZO = 0
    this.w_MVSCONT1 = 0
    this.w_MVSCONT2 = 0
    this.w_MVSCONT3 = 0
    this.w_MVSCONT4 = 0
    this.w_MVFLOMAG = "X"
    this.w_MVCODIVA = NVL(ARCODIVA, SPACE(5))
    this.w_ARTIPOPE = SPACE(10)
    if this.w_MVTIPRIG="R" AND this.w_MVQTAMOV>0 
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARCATSCM,ARDTOBSO,ARFLAPCA,ARPREZUM,ARTIPOPE"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARCATSCM,ARDTOBSO,ARFLAPCA,ARPREZUM,ARTIPOPE;
          from (i_cTable) where;
              ARCODART = this.w_MVCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CATART = NVL(cp_ToDate(_read_.ARCATSCM),cp_NullValue(_read_.ARCATSCM))
        this.w_ARDTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
        this.w_ARFLAPCA = NVL(cp_ToDate(_read_.ARFLAPCA),cp_NullValue(_read_.ARFLAPCA))
        this.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
        this.w_ARTIPOPE = NVL(cp_ToDate(_read_.ARTIPOPE),cp_NullValue(_read_.ARTIPOPE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_MVCODIVA = CALCODIV(this.w_MVCODART, this.oParentObject.w_MVTIPCON, this.w_MVCODCON , this.w_MVTIPOPE, this.w_MVDATDOC, this.w_ANTIPOPE, this.w_MVCODIVA, this.w_ARTIPOPE)
    * --- Rileggo i dati realtivi all'IVA..
    * --- Read from VOCIIVA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VOCIIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IVPERIND,IVPERIVA,IVBOLIVA"+;
        " from "+i_cTable+" VOCIIVA where ";
            +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IVPERIND,IVPERIVA,IVBOLIVA;
        from (i_cTable) where;
            IVCODIVA = this.w_MVCODIVA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INDIVA = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
      this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
      this.w_BOLIVA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
      this.w_INDIVA = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MVVALRIG = 0
    this.w_MVIMPACC = 0
    this.w_MVIMPSCO = 0
    this.w_MVVALMAG = 0
    this.w_MVIMPNAZ = 0
    this.w_LIPREZZO = 0
    this.w_UNMIS1 = NVL(ARUNMIS1, SPACE(3))
    this.w_UNMIS2 = NVL(ARUNMIS2, SPACE(3))
    this.w_UNMIS3 = NVL(CAUNIMIS, SPACE(3))
    this.w_MOLTIP = NVL(ARMOLTIP, 0)
    this.w_MOLTI3 = NVL(CAMOLTIP, 0)
    this.w_OPERAT = NVL(AROPERAT, " ")
    this.w_OPERA3 = NVL(CAOPERAT, " ")
    this.w_FLSERA = NVL(ARTIPSER,SPACE(1))
    this.w_MVCODMAG = NVL(PDCODMAG, SPACE(5))
    this.w_MVCONTRA = NVL(PDCONTRA, SPACE(15))
    * --- Qui calcolare il Prezzo
    if this.w_MVTIPRIG="R" AND this.w_MVQTAMOV>0 
      * --- Calcola Prezzo Da Listini/Contratti
      DECLARE ARRCALC (16,1)
      * --- Filtro su Cat.Commerciale non esiste
      this.w_CATCOM = "XXXX"
      * --- Azzero l'Array che verr� riempito dalla Funzione
      ARRCALC(1)=0
      this.w_QTAUM3 = CALQTA(this.w_MVQTAUM1,this.w_UNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
      this.w_QTAUM2 = CALQTA(this.w_MVQTAUM1,this.w_UNMIS2, this.w_UNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
      DIMENSION pArrUm[9]
      pArrUm [1] = this.w_PREZUM 
 pArrUm [2] = this.w_MVUNIMIS 
 pArrUm [3] = this.w_MVQTAMOV 
 pArrUm [4] = this.w_UNMIS1 
 pArrUm [5] = this.w_MVQTAUM1 
 pArrUm [6] = this.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
      * --- Lancio la funzione per il calcolo dei prezzi da listino contratto e ricerca sconti.
      *     Il secondo passaggio del parametro MVCODVAL sostituisce il passaggio del codice
      *     della valuta dell'ultimo costodi acquisto.
      *     Nel caso di contratti di acquisto non � gestito. Quindi ripasso il codice valuta.
      *     Lo stesso per penultimo parametro (valore ultimo costo di acquisto: passo 0)
      *     Non eseguir� nessuna operazione in merito
      this.w_CALPRZ = CalPrzli( this.w_MVCONTRA , this.oParentObject.w_MVTIPCON , this.w_MVCODLIS , this.w_MVCODART , this.w_GRUMER , this.w_MVQTAUM1 , this.w_MVCODVAL , this.w_MVCAOVAL , this.oParentObject.w_MVDATREG , this.w_CATCLI , this.w_CATART, this.w_MVCODVAL, this.w_MVCODCON, this.w_CATCOM, this.w_MVFLSCOR, this.w_SCOLIS,0,"A",@ARRCALC, this.oParentObject.w_PRZVAC, this.w_MVFLVEAC,"N", @pArrUm )
      this.w_CLUNIMIS = ARRCALC(16)
      this.w_LIPREZZO = ARRCALC(5)
      if this.w_MVUNIMIS<>this.w_CLUNIMIS AND NOT EMPTY(this.w_CLUNIMIS)
        this.w_LISCON = ARRCALC(7)
        this.w_QTALIS = IIF(this.w_CLUNIMIS=this.w_UNMIS1,this.w_MVQTAUM1, IIF(this.w_CLUNIMIS=this.w_UNMIS2, this.w_QTAUM2, this.w_QTAUM3))
        this.w_LIPREZZO = cp_Round(CALMMPZ(this.w_LIPREZZO, this.w_MVQTAMOV, this.w_QTALIS, IIF(this.w_LISCON=2, this.w_IVALIS, "N"), IIF(this.w_MVFLSCOR="S", 0, this.w_PERIVA), this.w_DECUNI),this.w_DECUNI)
      endif
      this.w_MVSCONT1 = ARRCALC(1)
      this.w_MVSCONT2 = ARRCALC(2)
      this.w_MVSCONT3 = ARRCALC(3)
      this.w_MVSCONT4 = ARRCALC(4)
      this.w_MVCONTRA = ARRCALC(9)
      this.w_IVACON = ARRCALC(12)
      SELECT GENEORDI
    endif
    this.w_APPO = this.w_UNMIS1+this.w_UNMIS2+this.w_UNMIS3+this.w_MVUNIMIS+this.w_OPERAT+this.w_OPERA3+this.w_IVALIS+"P"+ALLTRIM(STR(this.w_DECUNI))
    if EMPTY(this.w_CLUNIMIS)
      this.w_MVPREZZO = CALMMLIS(this.w_LIPREZZO,this.w_APPO,this.w_MOLTIP,this.w_MOLTI3, IIF(this.w_MVFLSCOR="S", 0, this.w_PERIVA))
    else
      if this.w_MVUNIMIS=this.w_CLUNIMIS
        * --- mi serve solo calcolare l'iva pertanto inganno il ricalcolo delle quantit� mettendo i moltiplicatori a 1
        this.w_MVPREZZO = CALMMLIS(this.w_LIPREZZO,this.w_APPO,1,1, IIF(this.w_MVFLSCOR="S", 0, this.w_PERIVA))
      else
        this.w_MVPREZZO = this.w_LIPREZZO
      endif
    endif
    this.w_MVVALRIG = CAVALRIG(this.w_MVPREZZO,this.w_MVQTAMOV, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT)
    this.w_MVVALMAG = CAVALMAG(this.w_MVFLSCOR, this.w_MVVALRIG, this.w_MVIMPSCO, 0, this.w_PERIVA, this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE )
    this.w_MVIMPNAZ = CAIMPNAZ(this.w_MVFLVEAC, this.w_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_MVCODIVE, this.w_PERIVE, this.w_INDIVE, this.w_PERIVA, this.w_INDIVA )
    * --- cond consegna
    this.w_MVCONCON = NVL(ANCONCON," ")
    * --- dati intra
    this.w_MVPESNET = NVL(ARPESNET, 0)
    this.w_MVNOMENC = NVL(ARNOMENC, SPACE(8))
    this.w_MVPROORD = NVL(MGPROMAG, SPACE(2))
    this.w_MVPROORD = iif(empty(this.w_MVPROORD), g_PROAZI, this.w_MVPROORD)
    if not empty(this.w_NAZDES)
      this.w_MVNAZPRO = this.w_NAZDES
    else
      this.w_MVNAZPRO = this.w_CODNAZ
    endif
    this.w_MVPAEFOR = this.w_CODNAZ
    this.w_MVFLTRAS = IIF(NVL(ARDATINT,"F")="S","Z",IIF( NVL(ARDATINT,"F")="F"," ",IIF( NVL(ARDATINT,"F")="I","I","S")))
    this.w_MVUMSUPP = NVL(ARUMSUPP, SPACE(3))
    this.w_MVMOLSUP = NVL(ARMOLSUP, 0)
    this.w_MVCODCEN = NVL(PDCODCEN, SPACE(15))
    this.w_MVVOCCOS = NVL(PDVOCCOS, SPACE(15))
    this.w_MVCODCOM = NVL(PDCODCOM, SPACE(15))
    this.w_MVTIPATT = NVL(PDTIPATT,"A")
    this.w_MVCODATT = NVL(PDCODATT, SPACE(15))
    this.w_OK_COMM = g_COMM="S" And this.w_MFLCOMM<>"N" And not empty(this.w_MVCODCOM) and not empty(this.w_MVCODATT)
    this.w_MVFLORCO = IIF(g_COMM="S" ,iif(this.w_MFLCOMM="I","+",IIF(this.w_MFLCOMM="D","-"," "))," ")
    * --- Costi di Commessa Commessa
    if this.w_OK_COMM and nvl(this.w_MVTFLCOM, " ") = "S"
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNCODVAL"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.w_MVCODCOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNCODVAL;
          from (i_cTable) where;
              CNCODCAN = this.w_MVCODCOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COCODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_COCODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT;
          from (i_cTable) where;
              VACODVAL = this.w_COCODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVIMPCOM = CAIMPCOM( IIF(Empty(this.w_MVCODCOM),"S", " " ), this.w_MVCODVAL, this.w_COCODVAL, this.w_MVIMPNAZ, this.w_MVCAOVAL, this.w_MVDATDOC, this.w_DECCOM )
      if NOT EMPTY(this.w_MVVOCCOS)
        * --- Read from VOC_COST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOC_COST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2],.t.,this.VOC_COST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VCTIPCOS"+;
            " from "+i_cTable+" VOC_COST where ";
                +"VCCODICE = "+cp_ToStrODBC(this.w_MVVOCCOS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VCTIPCOS;
            from (i_cTable) where;
                VCCODICE = this.w_MVVOCCOS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODCOS = NVL(cp_ToDate(_read_.VCTIPCOS),cp_NullValue(_read_.VCTIPCOS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVCODCOS = NVL(this.w_CODCOS, SPACE(5))
      endif
    endif
    * --- Totalizzatori
    this.w_TOTALE = this.w_TOTALE + this.w_MVVALRIG
    this.w_TOTMERCE = this.w_TOTMERCE + IIF(this.w_MVFLOMAG="X", this.w_MVVALRIG, 0)
    this.w_PDSERIAL = PDSERIAL
    this.w_PDROWNUM = NVL(PDROWNUM, 0)
    INSERT INTO GeneApp ;
    (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ;
    t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ;
    t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ;
    t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ;
    t_MVIMPACC, t_MVIMPSCO, t_MVVALMAG, t_MVIMPNAZ, ;
    t_MVFLORCO, t_MVIMPCOM, t_MVCODCOS, ;
    t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ;
    t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, ;
    t_MVPESNET, t_MVNOMENC, t_MVUMSUPP, t_MVMOLSUP, ;
    t_MVCODMAG, t_MVCODCEN, t_MVVOCCOS, ;
    t_MVCODCOM, t_MVTIPATT, t_MVCODATT, t_MVDATEVA, ;
    t_PDSERIAL, t_PDROWNUM, t_FLSERA, t_MVIMPAC2, t_INDIVA, t_ARFLAPCA,t_MVFLTRAS, t_MVPROORD) ;
    VALUES (this.w_MVTIPRIG, this.w_MVCODICE, this.w_MVCODART, ;
    this.w_MVDESART, this.w_MVDESSUP, this.w_MVUNIMIS, this.w_MVCATCON, ;
    this.w_MVCODCLA, this.w_MVCONTRA, this.w_MVCODLIS, ;
    this.w_MVQTAMOV, this.w_MVQTAUM1, this.w_MVPREZZO, ;
    this.w_MVIMPACC, this.w_MVIMPSCO, this.w_MVVALMAG, this.w_MVIMPNAZ, ;
    this.w_MVFLORCO, this.w_MVIMPCOM, this.w_MVCODCOS, ;
    this.w_MVSCONT1, this.w_MVSCONT2, this.w_MVSCONT3, this.w_MVSCONT4, ;
    this.w_MVFLOMAG, this.w_MVCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, ;
    this.w_MVPESNET, this.w_MVNOMENC, this.w_MVUMSUPP, this.w_MVMOLSUP, ;
    this.w_MVCODMAG, this.w_MVCODCEN, this.w_MVVOCCOS, ;
    this.w_MVCODCOM, this.w_MVTIPATT, this.w_MVCODATT, this.w_MVDATEVA, ;
    this.w_PDSERIAL, this.w_PDROWNUM, this.w_FLSERA, 0, this.w_INDIVA , this.w_ARFLAPCA,this.w_MVFLTRAS, this.w_MVPROORD)
    SELECT GENEORDI
    ENDSCAN
    * --- Testa l'Ultima Uscita
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Locali
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se non e' il Primo Ingresso, Scrive il Nuovo Documento di Destinazione
    if this.w_DBRK <> "##zz##" AND RECCOUNT("GeneApp") > 0
      * --- Rate Scadenze
      DIMENSION DR[1000, 9]
      this.w_LOOP = 1
      do while this.w_LOOP <= 1000
        DR[ this.w_LOOP , 1] = cp_CharToDate("  -  -  ")
        DR[ this.w_LOOP , 2] = 0
        DR[ this.w_LOOP , 3] = "  "
        DR[ this.w_LOOP , 4] = "  "
        DR[ this.w_LOOP , 5] = "  "
        DR[ this.w_LOOP , 6] = "  "
        DR[ this.w_LOOP , 7] = " "
        DR[ this.w_LOOP , 8] = " "
        DR[ this.w_LOOP , 9] = " "
        this.w_LOOP = this.w_LOOP + 1
      enddo
      * --- Calcola Sconti su Omaggio
      this.w_MVFLSCOM = IIF(g_FLSCOM="S" AND this.w_MVDATDOC<g_DTSCOM AND !empty(nvl(g_DTSCOM,cp_CharToDate("  /  /    ")))," ",g_FLSCOM)
      * --- Aggiorna le Spese Accessorie e gli Sconti Finali
      this.w_MVSCONTI = Calsco(this.w_TOTMERCE, this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT)
      this.w_MVFLSALD = " "
      * --- Ripartisce Sconti
      this.w_DATCOM = IIF(Empty(this.w_MVDATDOC),this.oParentObject.w_MVDATREG, this.w_MVDATDOC)
      * --- Il 5� e l'ultimo parametro sono le spese accessorie; in questo caso non esistono e quindi non devono essere ripartite
      GSAR_BRS(this,"B", "GeneApp", this.w_MVFLVEAC, this.w_MVFLSCOR, 0, this.w_MVSCONTI, this.w_TOTMERCE, this.w_MVCODIVE, this.w_MVCAOVAL, this.w_DATCOM, this.w_CAONAZ, this.w_MVVALNAZ, this.w_MVCODVAL, 0)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if g_COAC="S" And this.w_ANFLAPCA="S" And this.w_TDFLAPCA="S" 
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Calcoli Finali
      GSAR_BFA(this,"D")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Calcola MVSERIAL, MVNUMREG, MVNUMDOC
      this.oParentObject.w_MVSERIAL = SPACE(10)
      this.oParentObject.w_MVNUMDOC = 0
      this.oParentObject.w_MVNUMEST = 0
      this.oParentObject.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_MVNUMRIF = -20
      this.w_NUDOC = this.w_NUDOC + 1
      * --- Controllo se il pagamento associato al fornitore prevede data diversa
      vq_exec("query\GSOR_PDA.VQR",this,"PAGAM")
      if RECCOUNT("PAGAM")=0 
        * --- Se non prevede data diversa 
        this.w_PAGA = "N"
      else
        * --- Se prevede data diversa 
        this.w_PAGA = "S"
      endif
      * --- Try
      local bErr_048AD758
      bErr_048AD758=bTrsErr
      this.Try_048AD758()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_048AD758
      * --- End
    endif
    * --- Azzera il Temporaneo di Appoggio
    CREATE CURSOR GeneApp ;
    (t_MVTIPRIG C(1), t_MVCODICE C(20), t_MVCODART C(20), ;
    t_MVDESART C(40), t_MVDESSUP M(10), t_MVUNIMIS C(3), t_MVCATCON C(5), ;
    t_MVCODCLA C(3), t_MVCONTRA C(15), t_MVCODLIS C(5), ;
    t_MVQTAMOV N(12,3), t_MVQTAUM1 N(12,3), t_MVPREZZO N(18,5), ;
    t_MVSCONT1 N(6,2), t_MVSCONT2 N(6,2), t_MVSCONT3 N(6,2), t_MVSCONT4 N(6,2), ;
    t_MVFLOMAG C(1), t_MVCODIVA C(5), t_PERIVA N(5,2), t_BOLIVA C(1), t_MVVALRIG N(18,4), ;
    t_MVPESNET N(9,3), t_MVNOMENC C(8), t_MVUMSUPP C(3), t_MVMOLSUP N(8,3), ;
    t_MVIMPACC N(18,4), t_MVIMPSCO N(18,4), t_MVVALMAG N(18,4), t_MVIMPNAZ N(18,4), ;
    t_MVFLORCO C(1), t_MVIMPCOM N(18,4), t_MVCODCOS C(5), ;
    t_MVCODMAG C(5), t_MVCODCEN C(15), t_MVVOCCOS C(15), ;
    t_MVCODCOM C(15), t_MVTIPATT C(1), t_MVCODATT C(15), t_MVDATEVA D(8), ;
    t_PDSERIAL C(10), t_PDROWNUM N(8,0), t_FLSERA C(1), t_MVIMPAC2 N(18,4), t_INDIVA N(5,2), ;
    t_ARFLAPCA C(1), t_MVCACONT C(5), t_CPROWNUM N(4), t_CPROWORD N(5),t_MVFLTRAS C(1), t_MVPROORD C(2) )
    * --- Reinizializza le Variabili di Lavoro
    this.w_TOTMERCE = 0
    this.w_MVIVAIMB = SPACE(5)
    this.w_MVACIVA1 = SPACE(5)
    this.w_MVAIMPS1 = 0
    this.w_TOTALE = 0
    this.w_MVIVAINC = SPACE(5)
    this.w_MVACIVA2 = SPACE(5)
    this.w_MVAIMPS2 = 0
    this.w_MVSPEINC = 0
    this.w_MVIVATRA = SPACE(5)
    this.w_MVACIVA3 = SPACE(5)
    this.w_MVAIMPS3 = 0
    this.w_MVSPEIMB = 0
    this.w_MVIVABOL = SPACE(5)
    this.w_MVACIVA4 = SPACE(5)
    this.w_MVAIMPS4 = 0
    this.w_MVSPETRA = 0
    this.w_MVFLRINC = " "
    this.w_MVACIVA5 = SPACE(5)
    this.w_MVAIMPS5 = 0
    this.w_MVSPEBOL = 0
    this.w_MVFLRIMB = " "
    this.w_MVACIVA6 = SPACE(5)
    this.w_MVAIMPS6 = 0
    this.w_MVACCONT = 0
    this.w_MVFLRTRA = " "
    this.w_MVAFLOM1 = SPACE(1)
    this.w_MVAIMPN1 = 0
    this.w_TOTIMPON = 0
    this.w_MVAFLOM2 = SPACE(1)
    this.w_MVAIMPN2 = 0
    this.w_TOTIMPOS = 0
    this.w_MVAFLOM3 = SPACE(1)
    this.w_MVAIMPN3 = 0
    this.w_TOTFATTU = 0
    this.w_MVAFLOM4 = SPACE(1)
    this.w_MVAIMPN4 = 0
    this.w_MVIMPARR = 0
    this.w_MVAFLOM5 = SPACE(1)
    this.w_MVAIMPN5 = 0
    this.w_MVACCPRE = 0
    this.w_MVAFLOM6 = SPACE(1)
    this.w_MVAIMPN6 = 0
    this.w_MVTOTRIT = 0
    this.w_MVTOTENA = 0
    this.w_MVTOTRIT = 0
  endproc
  proc Try_048AD758()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_NextTableProg(this.oParentObject, i_Conn, "SEDOC", "i_codazi,w_MVSERIAL")
    cp_NextTableProg(this.oParentObject, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
    this.oParentObject.w_MVNUMREG = this.oParentObject.w_MVNUMDOC
    * --- Scrive la Testata
    * --- Insert into DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVALFDOC"+",MVANNDOC"+",MVCAUCON"+",MVCLADOC"+",MVCODAGE"+",MVCODCON"+",MVCODDES"+",MVCODESE"+",MVCODPOR"+",MVCODSPE"+",MVCODUTE"+",MVCODVET"+",MVCONCON"+",MVDATCIV"+",MVDATDOC"+",MVDATREG"+",MVFLACCO"+",MVFLCONT"+",MVFLGIOM"+",MVFLINTE"+",MVFLPROV"+",MVFLSCOR"+",MVFLVEAC"+",MVEMERIC"+",MVGENEFF"+",MVGENPRO"+",MVNUMDOC"+",MVNUMREG"+",MVPRD"+",MVRIFDIC"+",MVSERIAL"+",MVTCAMAG"+",MVTCOLIS"+",MVTCONTR"+",MVTFRAGG"+",MVTIPCON"+",MVTIPDOC"+",MVTIPORN"+",MVFLSALD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVALFDOC),'DOC_MAST','MVALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVANNDOC),'DOC_MAST','MVANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCAUCON),'DOC_MAST','MVCAUCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCLADOC),'DOC_MAST','MVCLADOC');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_MAST','MVCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCON),'DOC_MAST','MVCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODDES),'DOC_MAST','MVCODDES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODESE),'DOC_MAST','MVCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODPOR),'DOC_MAST','MVCODPOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODSPE),'DOC_MAST','MVCODSPE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODUTE),'DOC_MAST','MVCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODVET),'DOC_MAST','MVCODVET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONCON),'DOC_MAST','MVCONCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATCIV),'DOC_MAST','MVDATCIV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DOC_MAST','MVDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATREG),'DOC_MAST','MVDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLACCO),'DOC_MAST','MVFLACCO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLGIOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLINTE),'DOC_MAST','MVFLINTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLPROV),'DOC_MAST','MVFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOR),'DOC_MAST','MVFLSCOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVFLVEAC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVEMERIC');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENEFF');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMREG),'DOC_MAST','MVNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVPRD),'DOC_MAST','MVPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVRIFDIC),'DOC_MAST','MVRIFDIC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DOC_MAST','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTCAMAG),'DOC_MAST','MVTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCOLIS),'DOC_MAST','MVTCOLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCONTR),'DOC_MAST','MVTCONTR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTFRAGG),'DOC_MAST','MVTFRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPCON),'DOC_MAST','MVTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPDOC),'DOC_MAST','MVTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPCON),'DOC_MAST','MVTIPORN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLSALD),'DOC_MAST','MVFLSALD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVALFDOC',this.oParentObject.w_MVALFDOC,'MVANNDOC',this.oParentObject.w_MVANNDOC,'MVCAUCON',this.oParentObject.w_MVCAUCON,'MVCLADOC',this.w_MVCLADOC,'MVCODAGE',SPACE(5),'MVCODCON',this.w_MVCODCON,'MVCODDES',this.w_MVCODDES,'MVCODESE',this.oParentObject.w_MVCODESE,'MVCODPOR',this.w_MVCODPOR,'MVCODSPE',this.w_MVCODSPE,'MVCODUTE',this.oParentObject.w_MVCODUTE,'MVCODVET',this.w_MVCODVET)
      insert into (i_cTable) (MVALFDOC,MVANNDOC,MVCAUCON,MVCLADOC,MVCODAGE,MVCODCON,MVCODDES,MVCODESE,MVCODPOR,MVCODSPE,MVCODUTE,MVCODVET,MVCONCON,MVDATCIV,MVDATDOC,MVDATREG,MVFLACCO,MVFLCONT,MVFLGIOM,MVFLINTE,MVFLPROV,MVFLSCOR,MVFLVEAC,MVEMERIC,MVGENEFF,MVGENPRO,MVNUMDOC,MVNUMREG,MVPRD,MVRIFDIC,MVSERIAL,MVTCAMAG,MVTCOLIS,MVTCONTR,MVTFRAGG,MVTIPCON,MVTIPDOC,MVTIPORN,MVFLSALD &i_ccchkf. );
         values (;
           this.oParentObject.w_MVALFDOC;
           ,this.oParentObject.w_MVANNDOC;
           ,this.oParentObject.w_MVCAUCON;
           ,this.w_MVCLADOC;
           ,SPACE(5);
           ,this.w_MVCODCON;
           ,this.w_MVCODDES;
           ,this.oParentObject.w_MVCODESE;
           ,this.w_MVCODPOR;
           ,this.w_MVCODSPE;
           ,this.oParentObject.w_MVCODUTE;
           ,this.w_MVCODVET;
           ,this.w_MVCONCON;
           ,this.oParentObject.w_MVDATCIV;
           ,this.w_MVDATDOC;
           ,this.oParentObject.w_MVDATREG;
           ,this.oParentObject.w_MVFLACCO;
           ," ";
           ," ";
           ,this.oParentObject.w_MVFLINTE;
           ,this.oParentObject.w_MVFLPROV;
           ,this.w_MVFLSCOR;
           ,this.w_MVFLVEAC;
           ,this.w_MVFLVEAC;
           ," ";
           ," ";
           ,this.oParentObject.w_MVNUMDOC;
           ,this.oParentObject.w_MVNUMREG;
           ,this.oParentObject.w_MVPRD;
           ,this.w_MVRIFDIC;
           ,this.oParentObject.w_MVSERIAL;
           ,this.oParentObject.w_MVTCAMAG;
           ,this.w_MVTCOLIS;
           ,this.w_MVTCONTR;
           ,this.oParentObject.w_MVTFRAGG;
           ,this.oParentObject.w_MVTIPCON;
           ,this.oParentObject.w_MVTIPDOC;
           ,this.oParentObject.w_MVTIPCON;
           ,this.w_MVFLSALD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Occorrono 3 frasi per problema lunghezza stringa INSERT
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODPAG),'DOC_MAST','MVCODPAG');
      +",MVCODIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVE),'DOC_MAST','MVCODIVE');
      +",MVSCONTI ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONTI),'DOC_MAST','MVSCONTI');
      +",MVSPEINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEINC),'DOC_MAST','MVSPEINC');
      +",MVIVAINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAINC),'DOC_MAST','MVIVAINC');
      +",MVFLRINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRINC),'DOC_MAST','MVFLRINC');
      +",MVSPETRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPETRA),'DOC_MAST','MVSPETRA');
      +",MVIVATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVATRA),'DOC_MAST','MVIVATRA');
      +",MVFLRTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRTRA),'DOC_MAST','MVFLRTRA');
      +",MVSPEIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEIMB),'DOC_MAST','MVSPEIMB');
      +",MVIVAIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAIMB),'DOC_MAST','MVIVAIMB');
      +",MVFLRIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRIMB),'DOC_MAST','MVFLRIMB');
      +",MVSPEBOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEBOL),'DOC_MAST','MVSPEBOL');
      +",MVCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBAN),'DOC_MAST','MVCODBAN');
      +",MVVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALNAZ),'DOC_MAST','MVVALNAZ');
      +",MVCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODVAL),'DOC_MAST','MVCODVAL');
      +",MVCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAOVAL),'DOC_MAST','MVCAOVAL');
      +",MVSCOCL1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL1),'DOC_MAST','MVSCOCL1');
      +",MVSCOCL2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL2),'DOC_MAST','MVSCOCL2');
      +",MVSCOPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOPAG),'DOC_MAST','MVSCOPAG');
      +",MVALFEST ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVALFEST),'DOC_MAST','MVALFEST');
      +",MVPRP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVPRP),'DOC_MAST','MVPRP');
      +",MVANNPRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVANNPRO),'DOC_MAST','MVANNPRO');
      +",MVNUMEST ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMEST),'DOC_MAST','MVNUMEST');
      +",MVFLFOSC ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLFOSC');
      +",MVNOTAGG ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVNOTAGG');
      +",MVCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBA2),'DOC_MAST','MVCODBA2');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVCODPAG = this.w_MVCODPAG;
          ,MVCODIVE = this.w_MVCODIVE;
          ,MVSCONTI = this.w_MVSCONTI;
          ,MVSPEINC = this.w_MVSPEINC;
          ,MVIVAINC = this.w_MVIVAINC;
          ,MVFLRINC = this.w_MVFLRINC;
          ,MVSPETRA = this.w_MVSPETRA;
          ,MVIVATRA = this.w_MVIVATRA;
          ,MVFLRTRA = this.w_MVFLRTRA;
          ,MVSPEIMB = this.w_MVSPEIMB;
          ,MVIVAIMB = this.w_MVIVAIMB;
          ,MVFLRIMB = this.w_MVFLRIMB;
          ,MVSPEBOL = this.w_MVSPEBOL;
          ,MVCODBAN = this.w_MVCODBAN;
          ,MVVALNAZ = this.w_MVVALNAZ;
          ,MVCODVAL = this.w_MVCODVAL;
          ,MVCAOVAL = this.w_MVCAOVAL;
          ,MVSCOCL1 = this.w_MVSCOCL1;
          ,MVSCOCL2 = this.w_MVSCOCL2;
          ,MVSCOPAG = this.w_MVSCOPAG;
          ,MVALFEST = this.oParentObject.w_MVALFEST;
          ,MVPRP = this.oParentObject.w_MVPRP;
          ,MVANNPRO = this.oParentObject.w_MVANNPRO;
          ,MVNUMEST = this.oParentObject.w_MVNUMEST;
          ,MVFLFOSC = " ";
          ,MVNOTAGG = " ";
          ,MVCODBA2 = this.w_MVCODBA2;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.oParentObject.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVIMPARR ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPARR),'DOC_MAST','MVIMPARR');
      +",MVIVABOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVABOL),'DOC_MAST','MVIVABOL');
      +",MVACCONT ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCONT),'DOC_MAST','MVACCONT');
      +",UTCC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DOC_MAST','UTCC');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(0),'DOC_MAST','UTCV');
      +",UTDC ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'DOC_MAST','UTDC');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'DOC_MAST','UTDV');
      +",MVACIVA1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA1),'DOC_MAST','MVACIVA1');
      +",MVACIVA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA2),'DOC_MAST','MVACIVA2');
      +",MVACIVA3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA3),'DOC_MAST','MVACIVA3');
      +",MVACIVA4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA4),'DOC_MAST','MVACIVA4');
      +",MVACIVA5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA5),'DOC_MAST','MVACIVA5');
      +",MVACIVA6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA6),'DOC_MAST','MVACIVA6');
      +",MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN1),'DOC_MAST','MVAIMPN1');
      +",MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN2),'DOC_MAST','MVAIMPN2');
      +",MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN3),'DOC_MAST','MVAIMPN3');
      +",MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN4),'DOC_MAST','MVAIMPN4');
      +",MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN5),'DOC_MAST','MVAIMPN5');
      +",MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN6),'DOC_MAST','MVAIMPN6');
      +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS1),'DOC_MAST','MVAIMPS1');
      +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS2),'DOC_MAST','MVAIMPS2');
      +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS3),'DOC_MAST','MVAIMPS3');
      +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS4),'DOC_MAST','MVAIMPS4');
      +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS5),'DOC_MAST','MVAIMPS5');
      +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS6),'DOC_MAST','MVAIMPS6');
      +",MVAFLOM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM1),'DOC_MAST','MVAFLOM1');
      +",MVAFLOM2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM2),'DOC_MAST','MVAFLOM2');
      +",MVAFLOM3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM3),'DOC_MAST','MVAFLOM3');
      +",MVAFLOM4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM4),'DOC_MAST','MVAFLOM4');
      +",MVAFLOM5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM5),'DOC_MAST','MVAFLOM5');
      +",MVAFLOM6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM6),'DOC_MAST','MVAFLOM6');
      +",MVACCPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCPRE),'DOC_MAST','MVACCPRE');
      +",MVFLVABD ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLVABD),'DOC_MAST','MVFLVABD');
      +",MVTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPOPE),'DOC_MAST','MVTIPOPE');
      +",MVCATOPE ="+cp_NullLink(cp_ToStrODBC("OP"),'DOC_MAST','MVCATOPE');
      +",MVFLSCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOM),'DOC_MAST','MVFLSCOM');
      +",MVNUMCOR ="+cp_NullLink(cp_ToStrODBC(this.w_NUMCOR),'DOC_MAST','MVNUMCOR');
      +",MV__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_MV__NOTE),'DOC_MAST','MV__NOTE');
      +",MVCODSED ="+cp_NullLink(cp_ToStrODBC(this.w_CODSED),'DOC_MAST','MVCODSED');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVIMPARR = this.w_MVIMPARR;
          ,MVIVABOL = this.w_MVIVABOL;
          ,MVACCONT = this.w_MVACCONT;
          ,UTCC = i_CODUTE;
          ,UTCV = 0;
          ,UTDC = SetInfoDate( g_CALUTD );
          ,UTDV = cp_CharToDate("  -  -    ");
          ,MVACIVA1 = this.w_MVACIVA1;
          ,MVACIVA2 = this.w_MVACIVA2;
          ,MVACIVA3 = this.w_MVACIVA3;
          ,MVACIVA4 = this.w_MVACIVA4;
          ,MVACIVA5 = this.w_MVACIVA5;
          ,MVACIVA6 = this.w_MVACIVA6;
          ,MVAIMPN1 = this.w_MVAIMPN1;
          ,MVAIMPN2 = this.w_MVAIMPN2;
          ,MVAIMPN3 = this.w_MVAIMPN3;
          ,MVAIMPN4 = this.w_MVAIMPN4;
          ,MVAIMPN5 = this.w_MVAIMPN5;
          ,MVAIMPN6 = this.w_MVAIMPN6;
          ,MVAIMPS1 = this.w_MVAIMPS1;
          ,MVAIMPS2 = this.w_MVAIMPS2;
          ,MVAIMPS3 = this.w_MVAIMPS3;
          ,MVAIMPS4 = this.w_MVAIMPS4;
          ,MVAIMPS5 = this.w_MVAIMPS5;
          ,MVAIMPS6 = this.w_MVAIMPS6;
          ,MVAFLOM1 = this.w_MVAFLOM1;
          ,MVAFLOM2 = this.w_MVAFLOM2;
          ,MVAFLOM3 = this.w_MVAFLOM3;
          ,MVAFLOM4 = this.w_MVAFLOM4;
          ,MVAFLOM5 = this.w_MVAFLOM5;
          ,MVAFLOM6 = this.w_MVAFLOM6;
          ,MVACCPRE = this.w_MVACCPRE;
          ,MVFLVABD = this.w_MVFLVABD;
          ,MVTIPOPE = this.w_MVTIPOPE;
          ,MVCATOPE = "OP";
          ,MVFLSCOM = this.w_MVFLSCOM;
          ,MVNUMCOR = this.w_NUMCOR;
          ,MV__NOTE = this.w_MV__NOTE;
          ,MVCODSED = this.w_CODSED;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.oParentObject.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Se il pagamento legato al fornitore prevede data diversa nell'ordine verr� 
    *     aggiornato il campo Data diversa con la data inserita nella maschera di generazione ordini
    if this.w_PAGA="S"
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVDATDIV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATDIV),'DOC_MAST','MVDATDIV');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
               )
      else
        update (i_cTable) set;
            MVDATDIV = this.oParentObject.w_DATDIV;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.oParentObject.w_MVSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Cicla sulle Righe Documento
    ah_Msg("Generazione documento: %1",.T.,.F.,.F., ALLTRIM(STR(this.oParentObject.w_MVNUMDOC,15)) )
    SELECT GeneApp
    GO TOP
    SCAN FOR t_MVTIPRIG<>" " AND NOT EMPTY(t_MVCODICE)
    * --- Scrive il Dettaglio
    this.w_MVTIPRIG = t_MVTIPRIG
    this.w_MVCODICE = t_MVCODICE
    this.w_MVCODART = t_MVCODART
    this.w_MVDESART = t_MVDESART
    this.w_MVDESSUP = t_MVDESSUP
    this.w_MVUNIMIS = t_MVUNIMIS
    this.w_MVCATCON = t_MVCATCON
    this.w_MVCODCLA = t_MVCODCLA
    this.w_MVCONTRA = t_MVCONTRA
    this.w_MVCODLIS = t_MVCODLIS
    this.w_MVQTAMOV = t_MVQTAMOV
    this.w_MVQTAUM1 = t_MVQTAUM1
    this.w_MVPREZZO = t_MVPREZZO
    this.w_MVIMPACC = t_MVIMPACC
    this.w_MVIMPSCO = t_MVIMPSCO
    this.w_MVVALMAG = t_MVVALMAG
    this.w_MVIMPNAZ = t_MVIMPNAZ
    this.w_MVSCONT1 = t_MVSCONT1
    this.w_MVSCONT2 = t_MVSCONT2
    this.w_MVSCONT3 = t_MVSCONT3
    this.w_MVSCONT4 = t_MVSCONT4
    this.w_MVFLOMAG = t_MVFLOMAG
    this.w_MVCODIVA = t_MVCODIVA
    this.w_MVVALRIG = t_MVVALRIG
    this.w_MVPESNET = t_MVPESNET
    this.w_MVNOMENC = t_MVNOMENC
    this.w_MVFLTRAS = t_MVFLTRAS
    this.w_MVUMSUPP = t_MVUMSUPP
    this.w_MVMOLSUP = t_MVMOLSUP
    this.w_MVCODMAG = t_MVCODMAG
    this.w_MVCODCEN = t_MVCODCEN
    this.w_MVVOCCOS = t_MVVOCCOS
    this.w_MVCODCOM = t_MVCODCOM
    this.w_MVTIPATT = t_MVTIPATT
    this.w_MVCODATT = t_MVCODATT
    this.w_MVDATEVA = t_MVDATEVA
    this.w_MVCAUMAG = this.oParentObject.w_MVTCAMAG
    this.w_MVFLORDI = this.w_MFLORDI
    this.w_MVFLIMPE = this.w_MFLIMPE
    this.w_MVFLELGM = this.w_MFLELGM
    this.w_PDSERIAL = t_PDSERIAL
    this.w_PDROWNUM = t_PDROWNUM
    this.w_MVKEYSAL = IIF(this.w_MVTIPRIG="R" AND this.w_MFLAVAL<>"N", this.w_MVCODART, SPACE(20))
    this.w_MVCODMAG = IIF(EMPTY(this.w_MVKEYSAL), SPACE(5), this.w_MVCODMAG)
    this.w_MVPROORD = t_MVPROORD
    * --- Gestione Analitica
    this.w_MVVOCCEN = SPACE(15)
    this.w_OK_ANALI = .T.
    * --- Gestione Commessa
    this.w_MVIMPCOM = 0
    this.w_MVFLORCO = t_MVFLORCO
    this.w_MVCODCOS = SPACE(5)
    this.w_OK_COMM = .F.
    if g_PERCCR="S" 
      * --- Voce di Costo definita sulla PDA
      this.w_MVVOCCEN = this.w_MVVOCCOS
      if NOT EMPTY(this.w_MVVOCCEN) OR this.w_MVTIPRIG $ "MF"
        this.w_MVCODCEN = t_MVCODCEN
      endif
      this.w_OK_ANALI = NOT EMPTY(this.w_MVCODCEN) AND NOT EMPTY(this.w_MVVOCCEN)
    endif
    if g_COMM="S"
      this.w_MVCODCOM = NVL(t_MVCODCOM, SPACE(15))
      this.w_MVCODATT = NVL(t_MVCODATT, SPACE(15))
      this.w_OK_COMM = this.w_MFLCOMM<>"N" And not empty(this.w_MVCODCOM) and not empty(this.w_MVCODATT)
    else
      this.w_OK_COMM = .F.
    endif
    if this.w_OK_COMM and nvl(this.w_MVTFLCOM, " ") = "S"
      this.w_MVIMPCOM = t_MVIMPCOM
      this.w_MVFLORCO = t_MVFLORCO
      this.w_MVCODCOS = t_MVCODCOS
    endif
    * --- Warning per manacanza dati Gestione progetti (COMM)
    *     Se modulo attivo, causale di magazzino movimenta commessa e non
    *     ho inserito o commessa o attivit� lo segnalo.
    this.w_WARN_COMM = g_COMM="S" And this.w_MFLCOMM<>"N" And ( empty(this.w_MVCODCOM) Or empty(this.w_MVCODATT) )
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    if g_COAC="S"
      * --- Marco sul trransitorio il reale CPROWNUM della riga se non riferimento accessorio..
      if Empty( Nvl(geneapp.t_MVCACONT, Space(5)) )
         
 Select "GeneApp" 
 Replace t_CPROWNUM With this.w_CPROWNUM
        this.w_MVRIFCAC = 0
      else
        * --- Se t_CPROWNUM valorizzato mi sposto alla riga e leggo il CPROWNUM
        *     effetivamente salvato sul documento..
        this.w_SEARCHROW = Nvl( geneApp.t_CPROWNUM, 0)
        if this.w_SEARCHROW>0
          if not Eof( "GeneApp" )
            this.w_ACTPOS = Recno("GeneApp")
          else
            this.w_ACTPOS = Recno("GeneApp") -1
          endif
           
 Select GeneApp 
 Go ( this.w_SEARCHROW )
          this.w_MVRIFCAC = Nvl(GeneApp.t_CPROWNUM,0)
           
 Select GeneApp 
 Go ( this.w_ACTPOS )
          * --- Rileggo dal documento il numero di riga dell'articolo al quale il contributo � legato..
          * --- Read from DOC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CPROWORD"+;
              " from "+i_cTable+" DOC_DETT where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVRIFCAC);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CPROWORD;
              from (i_cTable) where;
                  MVSERIAL = this.oParentObject.w_MVSERIAL;
                  and CPROWNUM = this.w_MVRIFCAC;
                  and MVNUMRIF = this.w_MVNUMRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CPROWORD = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Al CPROWORD del padre aggiungo 1 pi� il progressivo di ogni contributo
          *     (il primo 0) calcolata nella pagina di esplosione).
          this.w_CPROWORD = this.w_CPROWORD + 1 + Nvl( GeneApp.t_CPROWORD , 0 )
        else
          this.w_MVRIFCAC = 0
        endif
      endif
      this.w_MVCACONT = Nvl(geneapp.t_MVCACONT, Space(5)) 
    else
      this.w_MVRIFCAC = 0
      this.w_MVCACONT = Space(5)
    endif
    * --- Insert into DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+",CPROWORD"+",MVTIPRIG"+",MVCODICE"+",MVCODART"+",MVDESART"+",MVDESSUP"+",MVUNIMIS"+",MVCATCON"+",MVCONTRO"+",MVCAUMAG"+",MVCODCLA"+",MVCONTRA"+",MVCODLIS"+",MVQTAMOV"+",MVQTAUM1"+",MVPREZZO"+",MVSCONT1"+",MVSCONT2"+",MVSCONT3"+",MVSCONT4"+",MVFLOMAG"+",MVCODIVA"+",MVCONIND"+",MVVALRIG"+",MVIMPACC"+",MVIMPSCO"+",MVVALMAG"+",MVIMPNAZ"+",MVPERPRO"+",MVIMPPRO"+",MVPESNET"+",MVNOMENC"+",MVMOLSUP"+",MVNUMCOL"+",MVFLTRAS"+",MVFLRAGG"+",MVSERRIF"+",MVROWRIF"+",MVFLARIF"+",MVQTASAL"+",MVFLLOTT"+",MVF2LOTT"+",MVPROCAP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DOC_DETT','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DOC_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'DOC_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCONTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONIND),'DOC_DETT','MVCONIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPERPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVNUMCOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTFRAGG),'DOC_DETT','MVFLRAGG');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_DETT','MVSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVROWRIF');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLLOTT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2LOTT');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPROCAP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.oParentObject.w_MVSERIAL,'CPROWNUM',this.w_CPROWNUM,'MVNUMRIF',this.w_MVNUMRIF,'CPROWORD',this.w_CPROWORD,'MVTIPRIG',this.w_MVTIPRIG,'MVCODICE',this.w_MVCODICE,'MVCODART',this.w_MVCODART,'MVDESART',this.w_MVDESART,'MVDESSUP',this.w_MVDESSUP,'MVUNIMIS',this.w_MVUNIMIS,'MVCATCON',this.w_MVCATCON,'MVCONTRO',SPACE(15))
      insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF,CPROWORD,MVTIPRIG,MVCODICE,MVCODART,MVDESART,MVDESSUP,MVUNIMIS,MVCATCON,MVCONTRO,MVCAUMAG,MVCODCLA,MVCONTRA,MVCODLIS,MVQTAMOV,MVQTAUM1,MVPREZZO,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVFLOMAG,MVCODIVA,MVCONIND,MVVALRIG,MVIMPACC,MVIMPSCO,MVVALMAG,MVIMPNAZ,MVPERPRO,MVIMPPRO,MVPESNET,MVNOMENC,MVMOLSUP,MVNUMCOL,MVFLTRAS,MVFLRAGG,MVSERRIF,MVROWRIF,MVFLARIF,MVQTASAL,MVFLLOTT,MVF2LOTT,MVPROCAP &i_ccchkf. );
         values (;
           this.oParentObject.w_MVSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_MVNUMRIF;
           ,this.w_CPROWORD;
           ,this.w_MVTIPRIG;
           ,this.w_MVCODICE;
           ,this.w_MVCODART;
           ,this.w_MVDESART;
           ,this.w_MVDESSUP;
           ,this.w_MVUNIMIS;
           ,this.w_MVCATCON;
           ,SPACE(15);
           ,this.w_MVCAUMAG;
           ,this.w_MVCODCLA;
           ,this.w_MVCONTRA;
           ,this.w_MVCODLIS;
           ,this.w_MVQTAMOV;
           ,this.w_MVQTAUM1;
           ,this.w_MVPREZZO;
           ,this.w_MVSCONT1;
           ,this.w_MVSCONT2;
           ,this.w_MVSCONT3;
           ,this.w_MVSCONT4;
           ,this.w_MVFLOMAG;
           ,this.w_MVCODIVA;
           ,this.w_MVCONIND;
           ,this.w_MVVALRIG;
           ,this.w_MVIMPACC;
           ,this.w_MVIMPSCO;
           ,this.w_MVVALMAG;
           ,this.w_MVIMPNAZ;
           ,0;
           ,0;
           ,this.w_MVPESNET;
           ,this.w_MVNOMENC;
           ,this.w_MVMOLSUP;
           ,0;
           ,this.w_MVFLTRAS;
           ,this.oParentObject.w_MVTFRAGG;
           ,SPACE(10);
           ,0;
           ," ";
           ,this.w_MVQTAUM1;
           ," ";
           ," ";
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
      +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPATT),'DOC_DETT','MVTIPATT');
      +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
      +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
      +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'DOC_DETT','MVKEYSAL');
      +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
      +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLRISE),'DOC_DETT','MVFLRISE');
      +",MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
      +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MFLORDI),'DOC_DETT','MVFLORDI');
      +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLIMPE),'DOC_DETT','MVFLIMPE');
      +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
      +",MVFLERIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLERIF');
      +",MVFLELGM ="+cp_NullLink(cp_ToStrODBC(this.w_MFLELGM),'DOC_DETT','MVFLELGM');
      +",MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MFLCASC),'DOC_DETT','MVFLCASC');
      +",MVFLARIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
      +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2RISE');
      +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2ORDI');
      +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2IMPE');
      +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2CASC');
      +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATEVA),'DOC_DETT','MVDATEVA');
      +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAT');
      +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'DOC_DETT','MVCODMAG');
      +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
      +",MVCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'DOC_DETT','MVCODCEN');
      +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'DOC_DETT','MVCODATT');
      +",MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
      +",MVCAUCOL ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCAUCOL');
      +",MVCODLOT ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVCODLOT');
      +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVCODUBI');
      +",MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVCODUB2');
      +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIMP');
      +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIM1');
      +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
      +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCOM),'DOC_DETT','MVIMPCOM');
      +",MVFLORCO ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLORCO),'DOC_DETT','MVFLORCO');
      +",MVCODCOS ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOS),'DOC_DETT','MVCODCOS');
      +",MVRIFCAC ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIFCAC),'DOC_DETT','MVRIFCAC');
      +",MVCACONT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCACONT),'DOC_DETT','MVCACONT');
      +",MVPROORD ="+cp_NullLink(cp_ToStrODBC(this.w_MVPROORD),'DOC_DETT','MVPROORD');
      +",MVNAZPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVNAZPRO),'DOC_DETT','MVNAZPRO');
      +",MVPAEFOR ="+cp_NullLink(cp_ToStrODBC(this.w_MVPAEFOR),'DOC_DETT','MVPAEFOR');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
          +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
             )
    else
      update (i_cTable) set;
          MVVOCCEN = this.w_MVVOCCEN;
          ,MVTIPATT = this.w_MVTIPATT;
          ,MVQTAEVA = 0;
          ,MVQTAEV1 = 0;
          ,MVKEYSAL = this.w_MVKEYSAL;
          ,MVIMPEVA = 0;
          ,MVFLRISE = this.w_MFLRISE;
          ,MVFLRIPA = " ";
          ,MVFLORDI = this.w_MFLORDI;
          ,MVFLIMPE = this.w_MFLIMPE;
          ,MVFLEVAS = " ";
          ,MVFLERIF = " ";
          ,MVFLELGM = this.w_MFLELGM;
          ,MVFLCASC = this.w_MFLCASC;
          ,MVFLARIF = " ";
          ,MVF2RISE = " ";
          ,MVF2ORDI = " ";
          ,MVF2IMPE = " ";
          ,MVF2CASC = " ";
          ,MVDATEVA = this.w_MVDATEVA;
          ,MVCODMAT = SPACE(5);
          ,MVCODMAG = this.w_MVCODMAG;
          ,MVCODCOM = this.w_MVCODCOM;
          ,MVCODCEN = this.w_MVCODCEN;
          ,MVCODATT = this.w_MVCODATT;
          ,MVCAUMAG = this.w_MVCAUMAG;
          ,MVCAUCOL = SPACE(5);
          ,MVCODLOT = SPACE(20);
          ,MVCODUBI = SPACE(20);
          ,MVCODUB2 = SPACE(20);
          ,MVQTAIMP = 0;
          ,MVQTAIM1 = 0;
          ,MV_SEGNO = this.oParentObject.w_MV_SEGNO;
          ,MVIMPCOM = this.w_MVIMPCOM;
          ,MVFLORCO = this.w_MVFLORCO;
          ,MVCODCOS = this.w_MVCODCOS;
          ,MVRIFCAC = this.w_MVRIFCAC;
          ,MVCACONT = this.w_MVCACONT;
          ,MVPROORD = this.w_MVPROORD;
          ,MVNAZPRO = this.w_MVNAZPRO;
          ,MVPAEFOR = this.w_MVPAEFOR;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.oParentObject.w_MVSERIAL;
          and CPROWNUM = this.w_CPROWNUM;
          and MVNUMRIF = this.w_MVNUMRIF;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna Saldi Commessa
    if this.w_OK_COMM
      * --- Try
      local bErr_03017798
      bErr_03017798=bTrsErr
      this.Try_03017798()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03017798
      * --- End
      * --- Write into MA_COSTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_MVFLORCO,'CSORDIN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CSORDIN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSORDIN');
            +i_ccchkf ;
        +" where ";
            +"CSCODCOM = "+cp_ToStrODBC(this.w_MVCODCOM);
            +" and CSCODMAT = "+cp_ToStrODBC(this.w_MVCODATT);
            +" and CSCODCOS = "+cp_ToStrODBC(this.w_MVCODCOS);
               )
      else
        update (i_cTable) set;
            CSORDIN = &i_cOp1.;
            &i_ccchkf. ;
         where;
            CSCODCOM = this.w_MVCODCOM;
            and CSCODMAT = this.w_MVCODATT;
            and CSCODCOS = this.w_MVCODCOS;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Aggiorna Saldi
    if this.w_MVQTAUM1<>0 AND NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_MVCODMAG)
      if NOT EMPTY(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE)
        * --- Try
        local bErr_030197A8
        bErr_030197A8=bTrsErr
        this.Try_030197A8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_030197A8
        * --- End
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SLQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SLQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SLQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SLQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTAPER = &i_cOp1.;
              ,SLQTRPER = &i_cOp2.;
              ,SLQTOPER = &i_cOp3.;
              ,SLQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_MVKEYSAL;
              and SLCODMAG = this.w_MVCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SALCOM="S"
          if empty(nvl(this.w_MVCODCOM,""))
            this.w_COMMAPPO = this.w_COMMDEFA
          else
            this.w_COMMAPPO = this.w_MVCODCOM
          endif
          * --- Try
          local bErr_03018B78
          bErr_03018B78=bTrsErr
          this.Try_03018B78()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03018B78
          * --- End
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SCQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SCQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SCQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SCQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                   )
          else
            update (i_cTable) set;
                SCQTAPER = &i_cOp1.;
                ,SCQTRPER = &i_cOp2.;
                ,SCQTOPER = &i_cOp3.;
                ,SCQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_MVKEYSAL;
                and SCCODMAG = this.w_MVCODMAG;
                and SCCODCAN = this.w_COMMAPPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    * --- Setta lo stato della Proposta di Acquisto a 'O' (Ordinata)
    if not Empty( this.w_PDSERIAL ) And this.w_PDROWNUM>0
      * --- Write into PDA_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PDA_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PDA_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PDA_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PDSTATUS ="+cp_NullLink(cp_ToStrODBC("O"),'PDA_DETT','PDSTATUS');
        +",PDSERRIF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'PDA_DETT','PDSERRIF');
            +i_ccchkf ;
        +" where ";
            +"PDSERIAL = "+cp_ToStrODBC(this.w_PDSERIAL);
            +" and PDROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
               )
      else
        update (i_cTable) set;
            PDSTATUS = "O";
            ,PDSERRIF = this.oParentObject.w_MVSERIAL;
            &i_ccchkf. ;
         where;
            PDSERIAL = this.w_PDSERIAL;
            and PDROWNUM = this.w_PDROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_WARN_OBSO = not (this.w_ARDTOBSO > this.w_MVDATEVA or empty(this.w_ARDTOBSO))
    if not this.w_OK_ANALI Or this.w_WARN_COMM Or this.w_WARN_OBSO
      if not used("pda_err")
        * --- Crea cursore di stampa degli errori
        create cursor pda_err (pdserial c(10), pdrownum n(8,0), pdcodart c(20), pdcodcen c(15), pdvoccos c(15), ; 
 pdcodcom c(15), pdcodatt c(15), pddateva d(8), Messagg c(80))
      endif
    endif
    if not this.w_OK_ANALI Or this.w_WARN_COMM
      this.w_MESSAGG = ""
      SELECT pda_err
      INSERT INTO pda_err (pdserial, pdrownum, pdcodart, pdcodcen, pdvoccos, pdcodcom, pdcodatt, pddateva, messagg) ;
      VALUES (this.w_PDSERIAL, this.w_PDROWNUM, this.w_MVCODART, this.w_MVCODCEN, this.w_MVVOCCEN, ; 
 this.w_MVCODCOM, this.w_MVCODATT, this.w_MVDATEVA, this.w_MESSAGG)
    endif
    if this.w_WARN_OBSO
      this.w_MESSAGG = ah_Msgformat("PDA per articolo obsoleto (al %1)", dtoc(this.w_ARDTOBSO) )
      SELECT pda_err
      INSERT INTO pda_err (pdserial, pdrownum, pdcodart, pdcodcen, pdvoccos, pdcodcom, pdcodatt, pddateva, messagg) ;
      VALUES (this.w_PDSERIAL, this.w_PDROWNUM, this.w_MVCODART, this.w_MVCODCEN, this.w_MVVOCCEN, this.w_MVCODCOM, ;
      this.w_MVCODATT, this.w_MVDATEVA, this.w_MESSAGG)
    endif
    SELECT GeneApp
    ENDSCAN
    if this.w_CPROWNUM=0
      * --- NESSUNA RIGA NEL DETTAGLIO
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    else
      * --- Cicla Sul Vettore delle Rate (Calcolato in GSAR_BFA)
      FOR l_i=1 TO 999
      if NOT EMPTY(DR[l_i, 1]) AND NOT EMPTY(DR[l_i, 2])
        this.w_RSNUMRAT = l_i
        this.w_RSDATRAT = DR[l_i, 1]
        this.w_RSIMPRAT = DR[l_i, 2]
        this.w_RSMODPAG = DR[l_i, 3]
        this.w_RSDESRIG = DR[l_i, 4]
        this.w_RSBANNOS = DR[l_i, 5]
        this.w_RSBANAPP = DR[l_i, 6]
        this.w_RSFLSOSP = DR[l_i, 7]
        this.w_RSCONCOR = DR[l_i, 8]
        this.w_RSFLPROV = DR[l_i, 9]
        * --- Scrive Doc_Rate
        * --- Insert into DOC_RATE
        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLSOSP"+",RSDESRIG"+",RSBANNOS"+",RSBANAPP"+",RSCONCOR"+",RSFLPROV"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DOC_RATE','RSSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSNUMRAT),'DOC_RATE','RSNUMRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSDATRAT),'DOC_RATE','RSDATRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSIMPRAT),'DOC_RATE','RSIMPRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSMODPAG),'DOC_RATE','RSMODPAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSFLSOSP),'DOC_RATE','RSFLSOSP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSDESRIG),'DOC_RATE','RSDESRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSBANNOS),'DOC_RATE','RSBANNOS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSBANAPP),'DOC_RATE','RSBANAPP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSCONCOR),'DOC_RATE','RSCONCOR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSFLPROV),'DOC_RATE','RSFLPROV');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.oParentObject.w_MVSERIAL,'RSNUMRAT',this.w_RSNUMRAT,'RSDATRAT',this.w_RSDATRAT,'RSIMPRAT',this.w_RSIMPRAT,'RSMODPAG',this.w_RSMODPAG,'RSFLSOSP',this.w_RSFLSOSP,'RSDESRIG',this.w_RSDESRIG,'RSBANNOS',this.w_RSBANNOS,'RSBANAPP',this.w_RSBANAPP,'RSCONCOR',this.w_RSCONCOR,'RSFLPROV',this.w_RSFLPROV)
          insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLSOSP,RSDESRIG,RSBANNOS,RSBANAPP,RSCONCOR,RSFLPROV &i_ccchkf. );
             values (;
               this.oParentObject.w_MVSERIAL;
               ,this.w_RSNUMRAT;
               ,this.w_RSDATRAT;
               ,this.w_RSIMPRAT;
               ,this.w_RSMODPAG;
               ,this.w_RSFLSOSP;
               ,this.w_RSDESRIG;
               ,this.w_RSBANNOS;
               ,this.w_RSBANAPP;
               ,this.w_RSCONCOR;
               ,this.w_RSFLPROV;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      NEXT
      * --- commit
      cp_EndTrs(.t.)
      this.w_OKDOC = this.w_OKDOC + 1
    endif
    * --- NUMDOC azzerato per evitare che i documenti succerssivi vengano 'Forzati'
    this.oParentObject.w_MVNUMDOC = 0
    return
  proc Try_03017798()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODCOM"+",CSTIPSTR"+",CSCODMAT"+",CSCODCOS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'MA_COSTI','CSCODCOM');
      +","+cp_NullLink(cp_ToStrODBC("A"),'MA_COSTI','CSTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'MA_COSTI','CSCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOS),'MA_COSTI','CSCODCOS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',this.w_MVCODCOM,'CSTIPSTR',"A",'CSCODMAT',this.w_MVCODATT,'CSCODCOS',this.w_MVCODCOS)
      insert into (i_cTable) (CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS &i_ccchkf. );
         values (;
           this.w_MVCODCOM;
           ,"A";
           ,this.w_MVCODATT;
           ,this.w_MVCODCOS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_030197A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MVKEYSAL,'SLCODMAG',this.w_MVCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03018B78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_MVKEYSAL,'SCCODMAG',this.w_MVCODMAG,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.w_MVCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAG;
           ,this.w_COMMAPPO;
           ,this.w_MVCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dato il cursore del dettaglio esplode riga per riga i contributi di riga e
    *     sul cursore determina il dettaglio per l'esplosione a peso
    Select GeneApp 
 Go Top
    do while Not Eof( "GeneApp" )
      * --- L'ultimo carattere contiene il check contributi accessori dell'articolo
      if GeneApp.t_ARFLAPCA="S"
        this.w_RIFCACESP = Recno( "GeneApp" )
        * --- Come riferimento passo il numero di riga dell'articolo che provoca
        *     l'esplosione
        this.w_UNIMIS = GeneApp.t_MVUNIMIS
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMFLFRAZ"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.w_UNIMIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMFLFRAZ;
            from (i_cTable) where;
                UMCODICE = this.w_UNIMIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        GSAR_BEG(this,GeneApp.t_MVCODART, this.oParentObject.w_MVTIPCON, this.w_MVCODCON , this.w_MVDATDOC, GeneApp.t_MVCODIVA,GeneApp.t_MVUNIMIS ,GeneApp.t_MVQTAMOV, GeneApp.t_MVPREZZO, this.w_MVCODVAL, IIF(this.w_MVFLSCOR="S" ,GeneApp.t_PERIVA ,0), this.w_COFRAZ, "ACC", "CURESPL")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Used( "CURESPL")
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      Select GeneApp 
 Skip
    enddo
    if this.w_CATDOC="FA" Or this.w_CATDOC="NC"
      * --- Esplosione a peso, solo per fatture / note di credito
      *     (da offerte ad oggi non possibile...)
      this.w_RIFCACESP = -1
      * --- Alla routine devo passare un cursore che contiene il dettaglio
      *     del documento appena caricato
      GSAR_BGA(this,"GEN", This , 0 , 0 , 10 , "I")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Used("CurEspl")
      Select CurEspl 
 Go top
      this.w_POSAPP = RecNo("GeneApp")
      this.w_CPROWORD = 0
      do while (.not. eof("CurEspl"))
        * --- RIcostruisco tutti i dati di riga da quanto restituito dal cursore di esplosione..
        this.w_KCODICE = CURESPL.CODICE
        this.w_KUNIMIS = CURESPL.UNIMIS
        this.w_KQTAMOV = CURESPL.QTAMOV
        this.w_KPREZZO = CURESPL.PREZZO
        this.w_KCODIVA = CURESPL.CODIVA
        this.w_CONTRIB = CURESPL.CATCON
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CA__TIPO,CACODART,CADESSUP,CADESART"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_KCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CA__TIPO,CACODART,CADESSUP,CADESART;
            from (i_cTable) where;
                CACODICE = this.w_KCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
          this.w_MVCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          this.w_MVDESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
          this.w_MVDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARNOMENC,ARUMSUPP,ARMOLSUP,ARPESNET,ARCATCON,ARCODCLA,ARVOCRIC,ARDATINT"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARNOMENC,ARUMSUPP,ARMOLSUP,ARPESNET,ARCATCON,ARCODCLA,ARVOCRIC,ARDATINT;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVNOMENC = NVL(cp_ToDate(_read_.ARNOMENC),cp_NullValue(_read_.ARNOMENC))
          this.w_MVUMSUPP = NVL(cp_ToDate(_read_.ARUMSUPP),cp_NullValue(_read_.ARUMSUPP))
          this.w_MVMOLSUP = NVL(cp_ToDate(_read_.ARMOLSUP),cp_NullValue(_read_.ARMOLSUP))
          this.w_MVPESNET = NVL(cp_ToDate(_read_.ARPESNET),cp_NullValue(_read_.ARPESNET))
          this.w_MVCATCON = NVL(cp_ToDate(_read_.ARCATCON),cp_NullValue(_read_.ARCATCON))
          this.w_MVCODCLA = NVL(cp_ToDate(_read_.ARCODCLA),cp_NullValue(_read_.ARCODCLA))
          this.w_MVVOCCOS = NVL(cp_ToDate(_read_.ARVOCRIC),cp_NullValue(_read_.ARVOCRIC))
          this.w_ARDATINT = NVL(cp_ToDate(_read_.ARDATINT),cp_NullValue(_read_.ARDATINT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIND,IVPERIVA,IVBOLIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_KCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIND,IVPERIVA,IVBOLIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_KCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIVA = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_BOLIVA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVVALRIG = CAVALRIG(this.w_KPREZZO,this.w_KQTAMOV, 0,0,0,0,this.w_DECTOT)
        this.w_MVVALMAG = CAVALMAG(this.w_MVFLSCOR, this.w_MVVALRIG, 0, 0, this.w_PERIVA, this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE )
        this.w_MVIMPNAZ = CAIMPNAZ(this.w_MVFLVEAC, this.w_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_MVCODIVE, this.w_PERIVE, this.w_INDIVE, this.w_PERIVA, this.w_INDIVA )
        this.w_MVFLTRAS = IIF( this.w_ARDATINT="S","Z",IIF( this.w_ARDATINT="F"," ",IIF( this.w_ARDATINT="I","I","S")))
        this.w_MVCODCEN = Space(15)
        this.w_MVVOCCEN = Space(15)
        this.w_MVCODCOM = Space(15)
        this.w_MVIMPCOM = CAIMPCOM( "S", this.w_MVVALNAZ, Space(3), this.w_MVIMPNAZ, this.w_CAONAZ, IIF(EMPTY(this.w_MVDATDOC), this.oParentObject.w_MVDATREG, this.w_MVDATDOC), 0 )
        this.w_CPROWORD = this.w_CPROWORD + 1
         
 INSERT INTO GeneApp ; 
 (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ; 
 t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ; 
 t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ; 
 t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ; 
 t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ; 
 t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG,; 
 t_MVPESNET, t_MVNOMENC, t_MVUMSUPP, t_MVMOLSUP, ; 
 t_MVIMPACC, t_MVIMPSCO , t_MVVALMAG , t_MVIMPNAZ, ; 
 t_MVFLORCO, t_MVIMPCOM, t_MVCODCOS, ; 
 t_MVCODMAG, t_MVCODCEN, t_MVVOCCOS, ; 
 t_MVCODCOM, t_MVTIPATT, t_MVCODATT, t_MVDATEVA, ; 
 t_FLSERA,t_MVIMPAC2, t_INDIVA, t_ARFLAPCA, t_MVCACONT , t_CPROWNUM, t_CPROWORD,t_MVFLTRAS); 
 VALUES ; 
 ( this.w_MVTIPRIG, this.w_KCODICE, this.w_MVCODART, ; 
 this.w_MVDESART, this.w_MVDESSUP, this.w_KUNIMIS, this.w_MVCATCON, ; 
 this.w_MVCODCLA, Space(15), Space(5), ; 
 this.w_KQTAMOV, this.w_KQTAMOV , this.w_KPREZZO, ; 
 0, 0, 0, 0, ; 
 "X", this.w_KCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, ; 
 this.w_MVPESNET, this.w_MVNOMENC , this.w_MVUMSUPP, this.w_MVMOLSUP, ; 
 0,0, this.w_MVVALMAG, this.w_MVIMPNAZ,; 
 Space(1), this.w_MVIMPCOM, Space(5), ; 
 this.w_MVCODMAG , this.w_MVCODCEN, this.w_MVVOCCOS, ; 
 this.w_MVCODCOM, "A" , Space(15), this.w_MVDATDOC, ; 
 this.w_FLSERA, 0 , this.w_INDIVA,this.w_ARFLAPCA , this.w_CONTRIB , this.w_RIFCACESP, this.w_CPROWORD,this.w_MVFLTRAS )
        Select CurEspl
        if Not Eof("CurEspl")
          Skip
        endif
      enddo
      Select CurEspl 
 Use
      Select "GeneApp" 
 Go ( this.w_POSAPP )
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,29)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='CON_COSC'
    this.cWorkTables[5]='CON_TRAM'
    this.cWorkTables[6]='DES_DIVE'
    this.cWorkTables[7]='DOC_DETT'
    this.cWorkTables[8]='DOC_MAST'
    this.cWorkTables[9]='DOC_RATE'
    this.cWorkTables[10]='KEY_ARTI'
    this.cWorkTables[11]='LISTINI'
    this.cWorkTables[12]='LIS_SCAG'
    this.cWorkTables[13]='OUT_PUTS'
    this.cWorkTables[14]='PAG_2AME'
    this.cWorkTables[15]='PAG_AMEN'
    this.cWorkTables[16]='PDA_DETT'
    this.cWorkTables[17]='SALDIART'
    this.cWorkTables[18]='VALUTE'
    this.cWorkTables[19]='VOCIIVA'
    this.cWorkTables[20]='TRADARTI'
    this.cWorkTables[21]='CAN_TIER'
    this.cWorkTables[22]='VOC_COST'
    this.cWorkTables[23]='MA_COSTI'
    this.cWorkTables[24]='TIP_DOCU'
    this.cWorkTables[25]='UNIMIS'
    this.cWorkTables[26]='AGENTI'
    this.cWorkTables[27]='SALDICOM'
    this.cWorkTables[28]='AZIENDA'
    this.cWorkTables[29]='SEDIAZIE'
    return(this.OpenAllTables(29))

  proc CloseCursors()
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
