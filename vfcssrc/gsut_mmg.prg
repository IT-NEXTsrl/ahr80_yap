* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mmg                                                        *
*              Configurazione My Gadget                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-01-16                                                      *
* Last revis.: 2015-09-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_mmg"))

* --- Class definition
define class tgsut_mmg as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 552
  Height = 386+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-09-28"
  HelpContextID=197739369
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  MYG_MAST_IDX = 0
  MYG_DETT_IDX = 0
  GAD_MAST_IDX = 0
  AZIENDA_IDX = 0
  GRP_GADG_IDX = 0
  cFile = "MYG_MAST"
  cFileDetail = "MYG_DETT"
  cKeySelect = "MGCODUTE"
  cKeyWhere  = "MGCODUTE=this.w_MGCODUTE"
  cKeyDetail  = "MGCODUTE=this.w_MGCODUTE and MGCODGAD=this.w_MGCODGAD and MGCODAZI=this.w_MGCODAZI"
  cKeyWhereODBC = '"MGCODUTE="+cp_ToStrODBC(this.w_MGCODUTE)';

  cKeyDetailWhereODBC = '"MGCODUTE="+cp_ToStrODBC(this.w_MGCODUTE)';
      +'+" and MGCODGAD="+cp_ToStrODBC(this.w_MGCODGAD)';
      +'+" and MGCODAZI="+cp_ToStrODBC(this.w_MGCODAZI)';

  cKeyWhereODBCqualified = '"MYG_DETT.MGCODUTE="+cp_ToStrODBC(this.w_MGCODUTE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MYG_DETT.MGCODGAD'
  cPrg = "gsut_mmg"
  cComment = "Configurazione My Gadget"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MGCODUTE = 0
  w_MG_WIDTH = 0
  w_MGCHKCOL = space(1)
  w_MGEFTRAN = space(1)
  w_MGDTHEME = space(5)
  w_BCKCOLOR = space(1)
  w_MG_START = space(1)
  w_MGPIANOV = space(1)
  w_MG_TIMER = space(1)
  w_MGHIDREF = space(1)
  w_MG_PINMG = space(1)
  w_MGSALCNF = space(1)
  w_MGDISRAP = space(1)
  w_MGBCKCOL = 0
  w_MGCODGAD = space(10)
  w_GDDESCRI = space(50)
  w_MGCODAZI = space(5)
  w_MGCODGRP = space(5)
  w_GAFORAZI = space(1)
  w_MGORDGRP = space(100)
  w_MGFLGEXP = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MYG_MAST','gsut_mmg')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_mmgPag1","gsut_mmg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("MyGadget")
      .Pages(1).HelpContextID = 237196486
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMGCODUTE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='GAD_MAST'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='GRP_GADG'
    this.cWorkTables[4]='MYG_MAST'
    this.cWorkTables[5]='MYG_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MYG_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MYG_MAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MGCODUTE = NVL(MGCODUTE,0)
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from MYG_MAST where MGCODUTE=KeySet.MGCODUTE
    *
    i_nConn = i_TableProp[this.MYG_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MYG_MAST_IDX,2],this.bLoadRecFilter,this.MYG_MAST_IDX,"gsut_mmg")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MYG_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MYG_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"MYG_DETT.","MYG_MAST.")
      i_cTable = i_cTable+' MYG_MAST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MGCODUTE',this.w_MGCODUTE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_BCKCOLOR = ''
        .w_MGCODUTE = NVL(MGCODUTE,0)
        .w_MG_WIDTH = NVL(MG_WIDTH,0)
        .w_MGCHKCOL = NVL(MGCHKCOL,space(1))
        .w_MGEFTRAN = NVL(MGEFTRAN,space(1))
        .w_MGDTHEME = NVL(MGDTHEME,space(5))
        .w_MG_START = NVL(MG_START,space(1))
        .w_MGPIANOV = NVL(MGPIANOV,space(1))
        .w_MG_TIMER = NVL(MG_TIMER,space(1))
        .w_MGHIDREF = NVL(MGHIDREF,space(1))
        .w_MG_PINMG = NVL(MG_PINMG,space(1))
        .w_MGSALCNF = NVL(MGSALCNF,space(1))
        .w_MGDISRAP = NVL(MGDISRAP,space(1))
        .w_MGBCKCOL = NVL(MGBCKCOL,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_MGORDGRP = NVL(MGORDGRP,space(100))
        cp_LoadRecExtFlds(this,'MYG_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from MYG_DETT where MGCODUTE=KeySet.MGCODUTE
      *                            and MGCODGAD=KeySet.MGCODGAD
      *                            and MGCODAZI=KeySet.MGCODAZI
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.MYG_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MYG_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('MYG_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "MYG_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" MYG_DETT"
        link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'MGCODUTE',this.w_MGCODUTE  )
        select * from (i_cTable) MYG_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_GDDESCRI = space(50)
          .w_GAFORAZI = space(1)
          .w_MGCODGAD = NVL(MGCODGAD,space(10))
          if link_2_1_joined
            this.w_MGCODGAD = NVL(GACODICE201,NVL(this.w_MGCODGAD,space(10)))
            this.w_GDDESCRI = NVL(GA__NOME201,space(50))
            this.w_GAFORAZI = NVL(GAFORAZI201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_MGCODAZI = NVL(MGCODAZI,space(5))
          * evitabile
          *.link_2_3('Load')
          .w_MGCODGRP = NVL(MGCODGRP,space(5))
          * evitabile
          *.link_2_4('Load')
          .w_MGFLGEXP = NVL(MGFLGEXP,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace MGCODGAD with .w_MGCODGAD
          replace MGCODAZI with .w_MGCODAZI
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_MGCODUTE=0
      .w_MG_WIDTH=0
      .w_MGCHKCOL=space(1)
      .w_MGEFTRAN=space(1)
      .w_MGDTHEME=space(5)
      .w_BCKCOLOR=space(1)
      .w_MG_START=space(1)
      .w_MGPIANOV=space(1)
      .w_MG_TIMER=space(1)
      .w_MGHIDREF=space(1)
      .w_MG_PINMG=space(1)
      .w_MGSALCNF=space(1)
      .w_MGDISRAP=space(1)
      .w_MGBCKCOL=0
      .w_MGCODGAD=space(10)
      .w_GDDESCRI=space(50)
      .w_MGCODAZI=space(5)
      .w_MGCODGRP=space(5)
      .w_GAFORAZI=space(1)
      .w_MGORDGRP=space(100)
      .w_MGFLGEXP=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        .w_MGDTHEME = ''
        .w_BCKCOLOR = ''
        .DoRTCalc(7,15,.f.)
        if not(empty(.w_MGCODGAD))
         .link_2_1('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(16,16,.f.)
        .w_MGCODAZI = 'xxx'
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_MGCODAZI))
         .link_2_3('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_MGCODGRP))
         .link_2_4('Full')
        endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'MYG_MAST')
    this.DoRTCalc(19,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMGCODUTE_1_1.enabled = i_bVal
      .Page1.oPag.oMG_WIDTH_1_2.enabled = i_bVal
      .Page1.oPag.oMGCHKCOL_1_3.enabled = i_bVal
      .Page1.oPag.oMGEFTRAN_1_4.enabled = i_bVal
      .Page1.oPag.oMGDTHEME_1_5.enabled = i_bVal
      .Page1.oPag.oMG_START_1_9.enabled = i_bVal
      .Page1.oPag.oMGPIANOV_1_10.enabled = i_bVal
      .Page1.oPag.oMG_TIMER_1_11.enabled = i_bVal
      .Page1.oPag.oMGHIDREF_1_12.enabled = i_bVal
      .Page1.oPag.oMG_PINMG_1_13.enabled = i_bVal
      .Page1.oPag.oMGSALCNF_1_14.enabled = i_bVal
      .Page1.oPag.oMGDISRAP_1_15.enabled = i_bVal
      .Page1.oPag.oBtn_1_7.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMGCODUTE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMGCODUTE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MYG_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MYG_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGCODUTE,"MGCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MG_WIDTH,"MG_WIDTH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGCHKCOL,"MGCHKCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGEFTRAN,"MGEFTRAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGDTHEME,"MGDTHEME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MG_START,"MG_START",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGPIANOV,"MGPIANOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MG_TIMER,"MG_TIMER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGHIDREF,"MGHIDREF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MG_PINMG,"MG_PINMG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGSALCNF,"MGSALCNF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGDISRAP,"MGDISRAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGBCKCOL,"MGBCKCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGORDGRP,"MGORDGRP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MYG_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MYG_MAST_IDX,2])
    i_lTable = "MYG_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MYG_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MGCODGAD C(10);
      ,t_GDDESCRI C(50);
      ,t_MGCODAZI N(3);
      ,t_MGCODGRP N(3);
      ,MGCODGAD C(10);
      ,MGCODAZI C(5);
      ,t_GAFORAZI C(1);
      ,t_MGFLGEXP C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_mmgbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODGAD_2_1.controlsource=this.cTrsName+'.t_MGCODGAD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGDDESCRI_2_2.controlsource=this.cTrsName+'.t_GDDESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODAZI_2_3.controlsource=this.cTrsName+'.t_MGCODAZI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODGRP_2_4.controlsource=this.cTrsName+'.t_MGCODGRP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(111)
    this.AddVLine(322)
    this.AddVLine(402)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODGAD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MYG_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MYG_MAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MYG_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MYG_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'MYG_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(MGCODUTE,MG_WIDTH,MGCHKCOL,MGEFTRAN,MGDTHEME"+;
                  ",MG_START,MGPIANOV,MG_TIMER,MGHIDREF,MG_PINMG"+;
                  ",MGSALCNF,MGDISRAP,MGBCKCOL,MGORDGRP"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_MGCODUTE)+;
                    ","+cp_ToStrODBC(this.w_MG_WIDTH)+;
                    ","+cp_ToStrODBC(this.w_MGCHKCOL)+;
                    ","+cp_ToStrODBC(this.w_MGEFTRAN)+;
                    ","+cp_ToStrODBC(this.w_MGDTHEME)+;
                    ","+cp_ToStrODBC(this.w_MG_START)+;
                    ","+cp_ToStrODBC(this.w_MGPIANOV)+;
                    ","+cp_ToStrODBC(this.w_MG_TIMER)+;
                    ","+cp_ToStrODBC(this.w_MGHIDREF)+;
                    ","+cp_ToStrODBC(this.w_MG_PINMG)+;
                    ","+cp_ToStrODBC(this.w_MGSALCNF)+;
                    ","+cp_ToStrODBC(this.w_MGDISRAP)+;
                    ","+cp_ToStrODBC(this.w_MGBCKCOL)+;
                    ","+cp_ToStrODBC(this.w_MGORDGRP)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MYG_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'MYG_MAST')
        cp_CheckDeletedKey(i_cTable,0,'MGCODUTE',this.w_MGCODUTE)
        INSERT INTO (i_cTable);
              (MGCODUTE,MG_WIDTH,MGCHKCOL,MGEFTRAN,MGDTHEME,MG_START,MGPIANOV,MG_TIMER,MGHIDREF,MG_PINMG,MGSALCNF,MGDISRAP,MGBCKCOL,MGORDGRP &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_MGCODUTE;
                  ,this.w_MG_WIDTH;
                  ,this.w_MGCHKCOL;
                  ,this.w_MGEFTRAN;
                  ,this.w_MGDTHEME;
                  ,this.w_MG_START;
                  ,this.w_MGPIANOV;
                  ,this.w_MG_TIMER;
                  ,this.w_MGHIDREF;
                  ,this.w_MG_PINMG;
                  ,this.w_MGSALCNF;
                  ,this.w_MGDISRAP;
                  ,this.w_MGBCKCOL;
                  ,this.w_MGORDGRP;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MYG_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MYG_DETT_IDX,2])
      *
      * insert into MYG_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(MGCODUTE,MGCODGAD,MGCODAZI,MGCODGRP,MGFLGEXP,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MGCODUTE)+","+cp_ToStrODBCNull(this.w_MGCODGAD)+","+cp_ToStrODBCNull(this.w_MGCODAZI)+","+cp_ToStrODBCNull(this.w_MGCODGRP)+","+cp_ToStrODBC(this.w_MGFLGEXP)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'MGCODUTE',this.w_MGCODUTE,'MGCODGAD',this.w_MGCODGAD,'MGCODAZI',this.w_MGCODAZI)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_MGCODUTE,this.w_MGCODGAD,this.w_MGCODAZI,this.w_MGCODGRP,this.w_MGFLGEXP,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MYG_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MYG_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update MYG_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'MYG_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " MG_WIDTH="+cp_ToStrODBC(this.w_MG_WIDTH)+;
             ",MGCHKCOL="+cp_ToStrODBC(this.w_MGCHKCOL)+;
             ",MGEFTRAN="+cp_ToStrODBC(this.w_MGEFTRAN)+;
             ",MGDTHEME="+cp_ToStrODBC(this.w_MGDTHEME)+;
             ",MG_START="+cp_ToStrODBC(this.w_MG_START)+;
             ",MGPIANOV="+cp_ToStrODBC(this.w_MGPIANOV)+;
             ",MG_TIMER="+cp_ToStrODBC(this.w_MG_TIMER)+;
             ",MGHIDREF="+cp_ToStrODBC(this.w_MGHIDREF)+;
             ",MG_PINMG="+cp_ToStrODBC(this.w_MG_PINMG)+;
             ",MGSALCNF="+cp_ToStrODBC(this.w_MGSALCNF)+;
             ",MGDISRAP="+cp_ToStrODBC(this.w_MGDISRAP)+;
             ",MGBCKCOL="+cp_ToStrODBC(this.w_MGBCKCOL)+;
             ",MGORDGRP="+cp_ToStrODBC(this.w_MGORDGRP)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'MYG_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'MGCODUTE',this.w_MGCODUTE  )
          UPDATE (i_cTable) SET;
              MG_WIDTH=this.w_MG_WIDTH;
             ,MGCHKCOL=this.w_MGCHKCOL;
             ,MGEFTRAN=this.w_MGEFTRAN;
             ,MGDTHEME=this.w_MGDTHEME;
             ,MG_START=this.w_MG_START;
             ,MGPIANOV=this.w_MGPIANOV;
             ,MG_TIMER=this.w_MG_TIMER;
             ,MGHIDREF=this.w_MGHIDREF;
             ,MG_PINMG=this.w_MG_PINMG;
             ,MGSALCNF=this.w_MGSALCNF;
             ,MGDISRAP=this.w_MGDISRAP;
             ,MGBCKCOL=this.w_MGBCKCOL;
             ,MGORDGRP=this.w_MGORDGRP;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_MGFLGEXP<>t_MGFLGEXP
            i_bUpdAll = .t.
          endif
        endif
        scan for (not(Empty(t_MGCODGAD)) And not(Empty(t_MGCODAZI))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.MYG_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.MYG_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from MYG_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and MGCODGAD="+cp_ToStrODBC(&i_TN.->MGCODGAD)+;
                            " and MGCODAZI="+cp_ToStrODBC(&i_TN.->MGCODAZI)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and MGCODGAD=&i_TN.->MGCODGAD;
                            and MGCODAZI=&i_TN.->MGCODAZI;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace MGCODGAD with this.w_MGCODGAD
              replace MGCODAZI with this.w_MGCODAZI
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MYG_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MGCODGRP="+cp_ToStrODBCNull(this.w_MGCODGRP)+;
                     ",MGFLGEXP="+cp_ToStrODBC(this.w_MGFLGEXP)+;
                     " ,MGCODGAD="+cp_ToStrODBC(this.w_MGCODGAD)+;
                     " ,MGCODAZI="+cp_ToStrODBC(this.w_MGCODAZI)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and MGCODGAD="+cp_ToStrODBC(MGCODGAD)+;
                             " and MGCODAZI="+cp_ToStrODBC(MGCODAZI)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MGCODGRP=this.w_MGCODGRP;
                     ,MGFLGEXP=this.w_MGFLGEXP;
                     ,MGCODGAD=this.w_MGCODGAD;
                     ,MGCODAZI=this.w_MGCODAZI;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and MGCODGAD=&i_TN.->MGCODGAD;
                                      and MGCODAZI=&i_TN.->MGCODAZI;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_MGCODGAD)) And not(Empty(t_MGCODAZI))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.MYG_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MYG_DETT_IDX,2])
        *
        * delete MYG_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and MGCODGAD="+cp_ToStrODBC(&i_TN.->MGCODGAD)+;
                            " and MGCODAZI="+cp_ToStrODBC(&i_TN.->MGCODAZI)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and MGCODGAD=&i_TN.->MGCODGAD;
                              and MGCODAZI=&i_TN.->MGCODAZI;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.MYG_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MYG_MAST_IDX,2])
        *
        * delete MYG_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_MGCODGAD)) And not(Empty(t_MGCODAZI))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MYG_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MYG_MAST_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,17,.t.)
          .link_2_4('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_GAFORAZI with this.w_GAFORAZI
      replace t_MGFLGEXP with this.w_MGFLGEXP
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_UKYNHMFVCO()
    with this
          * --- Cambia il colore di sfondo
          .w_MGBCKCOL = getColor()
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMGCODAZI_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMGCODAZI_2_3.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ChangeColor")
          .Calculate_UKYNHMFVCO()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_mmg
    IF cEvent=='ChangeColor'
       local l_obj
       l_obj = This.GetCtrl("w_BCKCOLOR")
       l_obj.DisabledBackcolor = This.w_MGBCKCOL
       l_obj=.null.
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MGCODGAD
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GAD_MAST_IDX,3]
    i_lTable = "GAD_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2], .t., this.GAD_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MGCODGAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MGA',True,'GAD_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GACODICE like "+cp_ToStrODBC(trim(this.w_MGCODGAD)+"%");

          i_ret=cp_SQL(i_nConn,"select GACODICE,GA__NOME,GAFORAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GACODICE',trim(this.w_MGCODGAD))
          select GACODICE,GA__NOME,GAFORAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MGCODGAD)==trim(_Link_.GACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MGCODGAD) and !this.bDontReportError
            deferred_cp_zoom('GAD_MAST','*','GACODICE',cp_AbsName(oSource.parent,'oMGCODGAD_2_1'),i_cWhere,'GSUT_MGA',"Gadget library",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GA__NOME,GAFORAZI";
                     +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',oSource.xKey(1))
            select GACODICE,GA__NOME,GAFORAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MGCODGAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GA__NOME,GAFORAZI";
                   +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(this.w_MGCODGAD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',this.w_MGCODGAD)
            select GACODICE,GA__NOME,GAFORAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MGCODGAD = NVL(_Link_.GACODICE,space(10))
      this.w_GDDESCRI = NVL(_Link_.GA__NOME,space(50))
      this.w_GAFORAZI = NVL(_Link_.GAFORAZI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MGCODGAD = space(10)
      endif
      this.w_GDDESCRI = space(50)
      this.w_GAFORAZI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])+'\'+cp_ToStr(_Link_.GACODICE,1)
      cp_ShowWarn(i_cKey,this.GAD_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MGCODGAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GAD_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.GACODICE as GACODICE201"+ ",link_2_1.GA__NOME as GA__NOME201"+ ",link_2_1.GAFORAZI as GAFORAZI201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on MYG_DETT.MGCODGAD=link_2_1.GACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and MYG_DETT.MGCODGAD=link_2_1.GACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MGCODAZI
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MGCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AZIENDA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_MGCODAZI)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_MGCODAZI))
          select AZCODAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MGCODAZI)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MGCODAZI) and !this.bDontReportError
            deferred_cp_zoom('AZIENDA','*','AZCODAZI',cp_AbsName(oSource.parent,'oMGCODAZI_2_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MGCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_MGCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_MGCODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_MGCODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MGCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MGCODGRP
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRP_GADG_IDX,3]
    i_lTable = "GRP_GADG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRP_GADG_IDX,2], .t., this.GRP_GADG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRP_GADG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MGCODGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MGCODGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GGCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where GGCODICE="+cp_ToStrODBC(this.w_MGCODGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GGCODICE',this.w_MGCODGRP)
            select GGCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MGCODGRP = NVL(_Link_.GGCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MGCODGRP = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRP_GADG_IDX,2])+'\'+cp_ToStr(_Link_.GGCODICE,1)
      cp_ShowWarn(i_cKey,this.GRP_GADG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MGCODGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMGCODUTE_1_1.value==this.w_MGCODUTE)
      this.oPgFrm.Page1.oPag.oMGCODUTE_1_1.value=this.w_MGCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oMG_WIDTH_1_2.value==this.w_MG_WIDTH)
      this.oPgFrm.Page1.oPag.oMG_WIDTH_1_2.value=this.w_MG_WIDTH
    endif
    if not(this.oPgFrm.Page1.oPag.oMGCHKCOL_1_3.RadioValue()==this.w_MGCHKCOL)
      this.oPgFrm.Page1.oPag.oMGCHKCOL_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGEFTRAN_1_4.RadioValue()==this.w_MGEFTRAN)
      this.oPgFrm.Page1.oPag.oMGEFTRAN_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDTHEME_1_5.RadioValue()==this.w_MGDTHEME)
      this.oPgFrm.Page1.oPag.oMGDTHEME_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBCKCOLOR_1_8.value==this.w_BCKCOLOR)
      this.oPgFrm.Page1.oPag.oBCKCOLOR_1_8.value=this.w_BCKCOLOR
    endif
    if not(this.oPgFrm.Page1.oPag.oMG_START_1_9.RadioValue()==this.w_MG_START)
      this.oPgFrm.Page1.oPag.oMG_START_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGPIANOV_1_10.RadioValue()==this.w_MGPIANOV)
      this.oPgFrm.Page1.oPag.oMGPIANOV_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMG_TIMER_1_11.RadioValue()==this.w_MG_TIMER)
      this.oPgFrm.Page1.oPag.oMG_TIMER_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGHIDREF_1_12.RadioValue()==this.w_MGHIDREF)
      this.oPgFrm.Page1.oPag.oMGHIDREF_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMG_PINMG_1_13.RadioValue()==this.w_MG_PINMG)
      this.oPgFrm.Page1.oPag.oMG_PINMG_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGSALCNF_1_14.RadioValue()==this.w_MGSALCNF)
      this.oPgFrm.Page1.oPag.oMGSALCNF_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDISRAP_1_15.RadioValue()==this.w_MGDISRAP)
      this.oPgFrm.Page1.oPag.oMGDISRAP_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODGAD_2_1.value==this.w_MGCODGAD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODGAD_2_1.value=this.w_MGCODGAD
      replace t_MGCODGAD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODGAD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGDDESCRI_2_2.value==this.w_GDDESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGDDESCRI_2_2.value=this.w_GDDESCRI
      replace t_GDDESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGDDESCRI_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODAZI_2_3.RadioValue()==this.w_MGCODAZI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODAZI_2_3.SetRadio()
      replace t_MGCODAZI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODAZI_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODGRP_2_4.RadioValue()==this.w_MGCODGRP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODGRP_2_4.SetRadio()
      replace t_MGCODGRP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODGRP_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'MYG_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_MGCODGAD)) And not(Empty(.w_MGCODAZI))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_MGCODGAD)) And not(Empty(t_MGCODAZI)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MGCODGAD=space(10)
      .w_GDDESCRI=space(50)
      .w_MGCODAZI=space(5)
      .w_MGCODGRP=space(5)
      .w_GAFORAZI=space(1)
      .w_MGFLGEXP=space(1)
      .DoRTCalc(1,15,.f.)
      if not(empty(.w_MGCODGAD))
        .link_2_1('Full')
      endif
      .DoRTCalc(16,16,.f.)
        .w_MGCODAZI = 'xxx'
      .DoRTCalc(17,17,.f.)
      if not(empty(.w_MGCODAZI))
        .link_2_3('Full')
      endif
      .DoRTCalc(18,18,.f.)
      if not(empty(.w_MGCODGRP))
        .link_2_4('Full')
      endif
    endwith
    this.DoRTCalc(19,21,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MGCODGAD = t_MGCODGAD
    this.w_GDDESCRI = t_GDDESCRI
    this.w_MGCODAZI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODAZI_2_3.RadioValue(.t.)
    this.w_MGCODGRP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODGRP_2_4.RadioValue(.t.)
    this.w_GAFORAZI = t_GAFORAZI
    this.w_MGFLGEXP = t_MGFLGEXP
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MGCODGAD with this.w_MGCODGAD
    replace t_GDDESCRI with this.w_GDDESCRI
    replace t_MGCODAZI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODAZI_2_3.ToRadio()
    replace t_MGCODGRP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMGCODGRP_2_4.ToRadio()
    replace t_GAFORAZI with this.w_GAFORAZI
    replace t_MGFLGEXP with this.w_MGFLGEXP
    if i_srv='A'
      replace MGCODGAD with this.w_MGCODGAD
      replace MGCODAZI with this.w_MGCODAZI
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_mmgPag1 as StdContainer
  Width  = 548
  height = 386
  stdWidth  = 548
  stdheight = 386
  resizeXpos=352
  resizeYpos=358
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMGCODUTE_1_1 as StdField with uid="MRVEQAGOCD",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MGCODUTE", cQueryName = "MGCODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231356683,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=113, Top=16, cSayPict='"9999"', cGetPict='"9999"'

  add object oMG_WIDTH_1_2 as StdField with uid="YZGQAUSJSN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MG_WIDTH", cQueryName = "MG_WIDTH",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 220461326,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=113, Top=40, cSayPict='"9999"', cGetPict='"9999"'


  add object oMGCHKCOL_1_3 as StdCombo with uid="XNBRIIMRHF",rtseq=3,rtrep=.f.,left=113,top=64,width=164,height=22;
    , ToolTipText = "Se selezionato, all'apertura MyGadget sar� visualizzato a schermo intero";
    , HelpContextID = 204683538;
    , cFormVar="w_MGCHKCOL",RowSource=""+"Schermo intero,"+"Schermo parziale,"+"Nascosto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMGCHKCOL_1_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MGCHKCOL,&i_cF..t_MGCHKCOL),this.value)
    return(iif(xVal =1,"N",;
    iif(xVal =2,"S",;
    iif(xVal =3,"X",;
    space(1)))))
  endfunc
  func oMGCHKCOL_1_3.GetRadio()
    this.Parent.oContained.w_MGCHKCOL = this.RadioValue()
    return .t.
  endfunc

  func oMGCHKCOL_1_3.ToRadio()
    this.Parent.oContained.w_MGCHKCOL=trim(this.Parent.oContained.w_MGCHKCOL)
    return(;
      iif(this.Parent.oContained.w_MGCHKCOL=="N",1,;
      iif(this.Parent.oContained.w_MGCHKCOL=="S",2,;
      iif(this.Parent.oContained.w_MGCHKCOL=="X",3,;
      0))))
  endfunc

  func oMGCHKCOL_1_3.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oMGEFTRAN_1_4 as StdCombo with uid="CXLNPZHGFE",value=1,rtseq=4,rtrep=.f.,left=113,top=88,width=164,height=20;
    , HelpContextID = 197220628;
    , cFormVar="w_MGEFTRAN",RowSource=""+"Nessuno,"+"Linear,"+"BounceEaseOut,"+"SineEaseOut,"+"ElasticEaseOut,"+"QuarticEaseInOut,"+"ExponentialEaseInOut", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMGEFTRAN_1_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MGEFTRAN,&i_cF..t_MGEFTRAN),this.value)
    return(iif(xVal =1,' ',;
    iif(xVal =2,'L',;
    iif(xVal =3,'B',;
    iif(xVal =4,'S',;
    iif(xVal =5,'E',;
    iif(xVal =6,'Q',;
    iif(xVal =7,'X',;
    'X'))))))))
  endfunc
  func oMGEFTRAN_1_4.GetRadio()
    this.Parent.oContained.w_MGEFTRAN = this.RadioValue()
    return .t.
  endfunc

  func oMGEFTRAN_1_4.ToRadio()
    this.Parent.oContained.w_MGEFTRAN=trim(this.Parent.oContained.w_MGEFTRAN)
    return(;
      iif(this.Parent.oContained.w_MGEFTRAN=='',1,;
      iif(this.Parent.oContained.w_MGEFTRAN=='L',2,;
      iif(this.Parent.oContained.w_MGEFTRAN=='B',3,;
      iif(this.Parent.oContained.w_MGEFTRAN=='S',4,;
      iif(this.Parent.oContained.w_MGEFTRAN=='E',5,;
      iif(this.Parent.oContained.w_MGEFTRAN=='Q',6,;
      iif(this.Parent.oContained.w_MGEFTRAN=='X',7,;
      0))))))))
  endfunc

  func oMGEFTRAN_1_4.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oMGDTHEME_1_5 as StdTableCombo with uid="UWPZZHLKEV",rtseq=5,rtrep=.f.,left=113,top=110,width=164,height=21;
    , cDescEmptyElement = "Nessuno";
    , ToolTipText = "Tema che verr� applicato di default ai gadget aggiunti da library";
    , HelpContextID = 32552693;
    , cFormVar="w_MGDTHEME",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='query/gsut_mgc.vqr',cKey='GCCODICE',cValue='GCDESCRI',cOrderBy='',xDefault=space(5);
  , bGlobalFont=.t.



  add object oBtn_1_7 as StdButton with uid="MZCUOVDROD",left=201, top=132, width=22,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Seleziona il colore di sfondo";
    , HelpContextID = 197538346;
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      this.parent.oContained.NotifyEvent("ChangeColor")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBCKCOLOR_1_8 as StdField with uid="DMWPHOMGLE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_BCKCOLOR", cQueryName = "BCKCOLOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 91141224,;
   bGlobalFont=.t.,;
    Height=20, Width=86, Left=113, Top=133, InputMask=replicate('X',1), bNoBackcolor=.T.

  add object oMG_START_1_9 as StdCheck with uid="ZIGRKZDRMR",rtseq=7,rtrep=.f.,left=323, top=9, caption="Apri all'avvio",;
    HelpContextID = 181401882,;
    cFormVar="w_MG_START", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMG_START_1_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MG_START,&i_cF..t_MG_START),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oMG_START_1_9.GetRadio()
    this.Parent.oContained.w_MG_START = this.RadioValue()
    return .t.
  endfunc

  func oMG_START_1_9.ToRadio()
    this.Parent.oContained.w_MG_START=trim(this.Parent.oContained.w_MG_START)
    return(;
      iif(this.Parent.oContained.w_MG_START=='S',1,;
      0))
  endfunc

  func oMG_START_1_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMGPIANOV_1_10 as StdCheck with uid="WVGUKCTVDF",rtseq=8,rtrep=.f.,left=323, top=32, caption="In primo piano",;
    ToolTipText = "Determina il piano di visualizzazione di MyGadget (background o foreground)",;
    HelpContextID = 110430492,;
    cFormVar="w_MGPIANOV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGPIANOV_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MGPIANOV,&i_cF..t_MGPIANOV),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oMGPIANOV_1_10.GetRadio()
    this.Parent.oContained.w_MGPIANOV = this.RadioValue()
    return .t.
  endfunc

  func oMGPIANOV_1_10.ToRadio()
    this.Parent.oContained.w_MGPIANOV=trim(this.Parent.oContained.w_MGPIANOV)
    return(;
      iif(this.Parent.oContained.w_MGPIANOV=='S',1,;
      0))
  endfunc

  func oMGPIANOV_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMG_TIMER_1_11 as StdCheck with uid="YWHAUOCBLK",rtseq=9,rtrep=.f.,left=323, top=55, caption="Aggiornamento automatico",;
    HelpContextID = 165611240,;
    cFormVar="w_MG_TIMER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMG_TIMER_1_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MG_TIMER,&i_cF..t_MG_TIMER),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oMG_TIMER_1_11.GetRadio()
    this.Parent.oContained.w_MG_TIMER = this.RadioValue()
    return .t.
  endfunc

  func oMG_TIMER_1_11.ToRadio()
    this.Parent.oContained.w_MG_TIMER=trim(this.Parent.oContained.w_MG_TIMER)
    return(;
      iif(this.Parent.oContained.w_MG_TIMER=='S',1,;
      0))
  endfunc

  func oMG_TIMER_1_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMGHIDREF_1_12 as StdCheck with uid="DYRPBHFVKD",rtseq=10,rtrep=.f.,left=323, top=78, caption="Aggiorna gadget nascosti",;
    ToolTipText = "Se selezionato, nel caso in cui un Gadget venga tolto da MyGadget, le configurazioni dell'utente relative a quel Gadget verranno mantenute",;
    HelpContextID = 87783156,;
    cFormVar="w_MGHIDREF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGHIDREF_1_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MGHIDREF,&i_cF..t_MGHIDREF),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oMGHIDREF_1_12.GetRadio()
    this.Parent.oContained.w_MGHIDREF = this.RadioValue()
    return .t.
  endfunc

  func oMGHIDREF_1_12.ToRadio()
    this.Parent.oContained.w_MGHIDREF=trim(this.Parent.oContained.w_MGHIDREF)
    return(;
      iif(this.Parent.oContained.w_MGHIDREF=='S',1,;
      0))
  endfunc

  func oMGHIDREF_1_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMG_PINMG_1_13 as StdCheck with uid="MWVTQQJYBK",rtseq=11,rtrep=.f.,left=323, top=101, caption="Blocca My Gadget bar",;
    HelpContextID = 119339277,;
    cFormVar="w_MG_PINMG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMG_PINMG_1_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MG_PINMG,&i_cF..t_MG_PINMG),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oMG_PINMG_1_13.GetRadio()
    this.Parent.oContained.w_MG_PINMG = this.RadioValue()
    return .t.
  endfunc

  func oMG_PINMG_1_13.ToRadio()
    this.Parent.oContained.w_MG_PINMG=trim(this.Parent.oContained.w_MG_PINMG)
    return(;
      iif(this.Parent.oContained.w_MG_PINMG=='S',1,;
      0))
  endfunc

  func oMG_PINMG_1_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMGSALCNF_1_14 as StdCheck with uid="EJZGGMRJHO",rtseq=12,rtrep=.f.,left=323, top=124, caption="Conserva attributi utente",;
    ToolTipText = "Se selezionato, nel caso in cui un Gadget venga tolto da MyGadget, le configurazioni dell'utente relative a quel Gadget verranno mantenute",;
    HelpContextID = 205338892,;
    cFormVar="w_MGSALCNF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGSALCNF_1_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MGSALCNF,&i_cF..t_MGSALCNF),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oMGSALCNF_1_14.GetRadio()
    this.Parent.oContained.w_MGSALCNF = this.RadioValue()
    return .t.
  endfunc

  func oMGSALCNF_1_14.ToRadio()
    this.Parent.oContained.w_MGSALCNF=trim(this.Parent.oContained.w_MGSALCNF)
    return(;
      iif(this.Parent.oContained.w_MGSALCNF=='S',1,;
      0))
  endfunc

  func oMGSALCNF_1_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMGDISRAP_1_15 as StdCheck with uid="MHTQMFYHOB",rtseq=13,rtrep=.f.,left=323, top=147, caption="Disposizione rapida",;
    HelpContextID = 196364566,;
    cFormVar="w_MGDISRAP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGDISRAP_1_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MGDISRAP,&i_cF..t_MGDISRAP),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oMGDISRAP_1_15.GetRadio()
    this.Parent.oContained.w_MGDISRAP = this.RadioValue()
    return .t.
  endfunc

  func oMGDISRAP_1_15.ToRadio()
    this.Parent.oContained.w_MGDISRAP=trim(this.Parent.oContained.w_MGDISRAP)
    return(;
      iif(this.Parent.oContained.w_MGDISRAP=='S',1,;
      0))
  endfunc

  func oMGDISRAP_1_15.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=175, width=535,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="MGCODGAD",Label1="Codice",Field2="GDDESCRI",Label2="Nome Gadget",Field3="MGCODAZI",Label3="Azienda",Field4="MGCODGRP",Label4="Area",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118351482

  add object oStr_1_17 as StdString with uid="ANMWAYBMFY",Visible=.t., Left=63, Top=17,;
    Alignment=1, Width=48, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="CHHELQIMIN",Visible=.t., Left=45, Top=41,;
    Alignment=1, Width=66, Height=18,;
    Caption="Larghezza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="VJFECTXVSA",Visible=.t., Left=11, Top=135,;
    Alignment=1, Width=100, Height=18,;
    Caption="Colore di sfondo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="IEQVGCXXUU",Visible=.t., Left=7, Top=89,;
    Alignment=1, Width=104, Height=18,;
    Caption="Animazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="TOQPIAZLIO",Visible=.t., Left=54, Top=65,;
    Alignment=1, Width=57, Height=18,;
    Caption="Visualizza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="WFWQPZCQTZ",Visible=.t., Left=16, Top=112,;
    Alignment=1, Width=95, Height=18,;
    Caption="Tema di default:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=194,;
    width=531+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=195,width=530+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='GAD_MAST|AZIENDA|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='GAD_MAST'
        oDropInto=this.oBodyCol.oRow.oMGCODGAD_2_1
      case cFile='AZIENDA'
        oDropInto=this.oBodyCol.oRow.oMGCODAZI_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsut_mmgBodyRow as CPBodyRowCnt
  Width=521
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMGCODGAD_2_1 as StdTrsField with uid="ATICBTABLU",rtseq=15,rtrep=.t.,;
    cFormVar="w_MGCODGAD",value=space(10),nZero=10,isprimarykey=.t.,;
    ToolTipText = "Codice del Gadget",;
    HelpContextID = 264911114,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=103, Left=-2, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="GAD_MAST", cZoomOnZoom="GSUT_MGA", oKey_1_1="GACODICE", oKey_1_2="this.w_MGCODGAD"

  func oMGCODGAD_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMGCODGAD_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMGCODGAD_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oMGCODGAD_2_1.readonly and this.parent.oMGCODGAD_2_1.isprimarykey)
    do cp_zoom with 'GAD_MAST','*','GACODICE',cp_AbsName(this.parent,'oMGCODGAD_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MGA',"Gadget library",'',this.parent.oContained
   endif
  endproc
  proc oMGCODGAD_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MGA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GACODICE=this.parent.oContained.w_MGCODGAD
    i_obj.ecpSave()
  endproc

  add object oGDDESCRI_2_2 as StdTrsField with uid="BCDGPHUWEF",rtseq=16,rtrep=.t.,;
    cFormVar="w_GDDESCRI",value=space(50),enabled=.f.,;
    ToolTipText = "Nome del Gadget",;
    HelpContextID = 212878767,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=208, Left=105, Top=0, InputMask=replicate('X',50)

  add object oMGCODAZI_2_3 as stdTrsTableCombo with uid="VKYVUFJBSJ",rtrep=.t.,;
    cFormVar="w_MGCODAZI", tablefilter="" , ;
    HelpContextID = 104187633,;
    Height=22, Width=77, Left=316, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, cLinkFile="AZIENDA"  , xEmptyKey = 'xxx', cDescEmptyElement = "Nessuna";
, bIsInHeader=.f.;
    , cTable='AZIENDA',cKey='AZCODAZI',cValue='AZCODAZI',cOrderBy='AZCODAZI',xDefault=space(5);
  , bGlobalFont=.t.



  func oMGCODAZI_2_3.mCond()
    with this.Parent.oContained
      return (.w_GAFORAZI='S')
    endwith
  endfunc

  func oMGCODAZI_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMGCODAZI_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  add object oMGCODGRP_2_4 as stdTrsTableCombo with uid="QHTWOJQXKP",rtrep=.t.,;
    cFormVar="w_MGCODGRP", tablefilter="" , enabled=.f.,;
    ToolTipText = "Area gadget",;
    HelpContextID = 264911126,;
    Height=22, Width=121, Left=395, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, cLinkFile="GRP_GADG"  , cDescEmptyElement = "Nessuna";
, bIsInHeader=.f.;
    , cTable='GRP_GADG',cKey='GGCODICE',cValue='GGDESCRI',cOrderBy='GGCODICE',xDefault=space(5);
  , bGlobalFont=.t.



  func oMGCODGRP_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oMGCODGAD_2_1.When()
    return(.t.)
  proc oMGCODGAD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMGCODGAD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mmg','MYG_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MGCODUTE=MYG_MAST.MGCODUTE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_mmg
*--- Se fallisce il link sulla tabella AZIENDA ritorno sempre "xxx"
Func MGCODAZI_XXX(pMGCODAZI)
   Return "xxx"
EndFunc
* --- Fine Area Manuale
