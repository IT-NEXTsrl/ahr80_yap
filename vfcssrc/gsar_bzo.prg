* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bzo                                                        *
*              Documenti da primanota                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_80]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-21                                                      *
* Last revis.: 2000-04-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bzo",oParentObject)
return(i_retval)

define class tgsar_bzo as StdBatch
  * --- Local variables
  w_CHIAVE = space(10)
  w_SERIAL = space(10)
  w_FLVEAC = space(1)
  w_CLADOC = space(2)
  w_DOC = .NULL.
  * --- WorkFile variables
  CON_INDI_idx=0
  DES_DIVE_idx=0
  DIS_TINT_idx=0
  DOC_MAST_idx=0
  INC_CORR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lancia I DOCUMENTI DI VENDITA / ACQUISTI dalla visualizzazione della Primanota e visualizza da Indiretta Effetti, Distinte Effetti, Corrispettivi,Manutenzione Contenzioso,Movimenti Cespiti
    * --- ALFA NUMERO E DATA DEL DOCUMENTO
    * --- CERCO IL DOCUMENTO
    * --- Questo oggetto sar� definito come documento
    this.w_FLVEAC = " "
    this.w_CLADOC = "  "
    this.w_SERIAL = SPACE(10)
    do case
      case NOT EMPTY(NVL(this.oParentObject.w_RIFDOC,SPACE(10)))
        if NVL(this.oParentObject.w_RIFDOC,SPACE(10)) ="CORRISP"
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVFLVEAC,MVCLADOC,MVSERIAL"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVRIFCON = "+cp_ToStrODBC(this.oParentObject.w_SERIALP);
                  +" and MVFLCONT = "+cp_ToStrODBC("S");
                  +" and MVCLADOC = "+cp_ToStrODBC("RF");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVFLVEAC,MVCLADOC,MVSERIAL;
              from (i_cTable) where;
                  MVRIFCON = this.oParentObject.w_SERIALP;
                  and MVFLCONT = "S";
                  and MVCLADOC = "RF";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
            this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
            this.w_SERIAL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.w_CHIAVE = this.oParentObject.w_RIFDOC
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVFLVEAC,MVCLADOC,MVSERIAL"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_RIFDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVFLVEAC,MVCLADOC,MVSERIAL;
              from (i_cTable) where;
                  MVSERIAL = this.oParentObject.w_RIFDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
            this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
            this.w_SERIAL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      case NOT EMPTY(NVL(this.oParentObject.w_RIFDIS,SPACE(10)))
        if VAL(this.oParentObject.w_RIFDIS)<0
          this.w_CHIAVE = "0"+RIGHT(this.oParentObject.w_RIFDIS,9)
          * --- Read from CON_INDI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CON_INDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CON_INDI_idx,2],.t.,this.CON_INDI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CISERIAL"+;
              " from "+i_cTable+" CON_INDI where ";
                  +"CISERIAL = "+cp_ToStrODBC(this.w_CHIAVE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CISERIAL;
              from (i_cTable) where;
                  CISERIAL = this.w_CHIAVE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERIAL = NVL(cp_ToDate(_read_.CISERIAL),cp_NullValue(_read_.CISERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(this.w_SERIAL)
            this.w_DOC = GSCG_ACI()
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_DOC.bSec1)
              i_retcode = 'stop'
              return
            endif
            * --- creo il cursore delle solo chiavi
            this.w_DOC.QueryKeySet("CISERIAL='"+this.w_SERIAL+ "'","")     
            * --- carico il record
            this.w_DOC.LoadRecwarn()     
            i_retcode = 'stop'
            return
          endif
        else
          this.w_CHIAVE = "9999999999"
          * --- Read from DIS_TINT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIS_TINT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DINUMDIS"+;
              " from "+i_cTable+" DIS_TINT where ";
                  +"DINUMDIS = "+cp_ToStrODBC(this.oParentObject.w_RIFDIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DINUMDIS;
              from (i_cTable) where;
                  DINUMDIS = this.oParentObject.w_RIFDIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERIAL = NVL(cp_ToDate(_read_.DINUMDIS),cp_NullValue(_read_.DINUMDIS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(this.w_SERIAL)
            this.w_DOC = GSTE_ADI()
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_DOC.bSec1)
              i_retcode = 'stop'
              return
            endif
            * --- creo il cursore delle solo chiavi
            this.w_DOC.QueryKeySet("DINUMDIS='"+this.w_SERIAL+ "'","")     
            * --- carico il record
            this.w_DOC.LoadRecwarn()     
            i_retcode = 'stop'
            return
          endif
        endif
      case NOT EMPTY(NVL(this.oParentObject.w_RIFINC,SPACE(10)))
        * --- Read from INC_CORR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.INC_CORR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INC_CORR_idx,2],.t.,this.INC_CORR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "INSERIAL"+;
            " from "+i_cTable+" INC_CORR where ";
                +"INSERIAL = "+cp_ToStrODBC(this.oParentObject.w_RIFINC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            INSERIAL;
            from (i_cTable) where;
                INSERIAL = this.oParentObject.w_RIFINC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERIAL = NVL(cp_ToDate(_read_.INSERIAL),cp_NullValue(_read_.INSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.w_SERIAL)
          this.w_DOC = GSVE_MIC()
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_DOC.bSec1)
            i_retcode = 'stop'
            return
          endif
          * --- creo il cursore delle solo chiavi
          this.w_DOC.QueryKeySet("INSERIAL='"+this.w_SERIAL+ "'","")     
          * --- carico il record
          this.w_DOC.LoadRecwarn()     
          i_retcode = 'stop'
          return
        endif
      case NOT EMPTY(NVL(this.oParentObject.w_RIFCES,SPACE(1)))
        do case
          case NVL(this.oParentObject.w_RIFCES,SPACE(1))="C" AND "SOLL" $ UPPER(i_cModules)
            vq_exec("..\SOLL\EXE\QUERY\GSSO_BZO.vqr",this,"TEMP")
            this.w_SERIAL = TEMP.SERIAL
            if used("TEMP")
              select TEMP
              use
            endif
            if NOT EMPTY(this.w_SERIAL)
              this.w_DOC = GSSO_MCO()
              * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
              if !(this.w_DOC.bSec1)
                i_retcode = 'stop'
                return
              endif
              * --- creo il cursore delle solo chiavi
              this.w_DOC.QueryKeySet("COSERIAL='"+this.w_SERIAL+ "'","")     
              * --- carico il record
              this.w_DOC.LoadRecwarn()     
              i_retcode = 'stop'
              return
            endif
          case NVL(this.oParentObject.w_RIFCES,SPACE(1))="S" AND "CESP" $ UPPER(i_cModules)
            vq_exec("..\CESP\EXE\QUERY\GSCE_BZO.vqr",this,"TEMP")
            this.w_SERIAL = TEMP.SERIAL
            if used("TEMP")
              select TEMP
              use
            endif
            if NOT EMPTY(this.w_SERIAL)
              this.w_DOC = GSCE_AMC()
              * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
              if !(this.w_DOC.bSec1)
                i_retcode = 'stop'
                return
              endif
              * --- creo il cursore delle solo chiavi
              this.w_DOC.QueryKeySet("MCSERIAL='"+this.w_SERIAL+ "'","")     
              * --- carico il record
              this.w_DOC.LoadRecwarn()     
              i_retcode = 'stop'
              return
            endif
        endcase
    endcase
    if EMPTY(this.w_SERIAL)
      this.w_FLVEAC = "V"
      this.w_CLADOC = "  "
      * --- Select from DOC_MAST
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MVSERIAL,MVFLVEAC,MVCLADOC  from "+i_cTable+" DOC_MAST ";
            +" where MVALFDOC= "+cp_ToStrODBC(this.oParentObject.w_ALFA)+" and MVNUMDOC= "+cp_ToStrODBC(this.oParentObject.w_NUMERO)+" and MVDATDOC="+cp_ToStrODBC(this.oParentObject.w_DATA)+" and MVRIFCON = "+cp_ToStrODBC(this.oParentObject.w_SERIALP)+"";
             ,"_Curs_DOC_MAST")
      else
        select MVSERIAL,MVFLVEAC,MVCLADOC from (i_cTable);
         where MVALFDOC= this.oParentObject.w_ALFA and MVNUMDOC= this.oParentObject.w_NUMERO and MVDATDOC=this.oParentObject.w_DATA and MVRIFCON = this.oParentObject.w_SERIALP;
          into cursor _Curs_DOC_MAST
      endif
      if used('_Curs_DOC_MAST')
        select _Curs_DOC_MAST
        locate for 1=1
        do while not(eof())
        this.w_SERIAL = _Curs_DOC_MAST.MVSERIAL
        this.w_FLVEAC = _Curs_DOC_MAST.MVFLVEAC
        this.w_CLADOC = _Curs_DOC_MAST.MVCLADOC
          select _Curs_DOC_MAST
          continue
        enddo
        use
      endif
    endif
    * --- Il documento potrebbe non esistere
    if EMPTY(NVL(this.w_SERIAL," "))
      ah_ErrorMsg("Documento non esistente",,"")
      i_retcode = 'stop'
      return
    endif
    * --- definisco l'oggetto come appartenente alla classe dei documenti
    if this.w_CLADOC="OR"
      this.w_DOC = GSOR_MDV(this.w_FLVEAC)
    else
      if this.w_FLVEAC="V"
        this.w_DOC = GSVE_MDV(this.w_CLADOC)
      else
        this.w_DOC = GSAC_MDV(this.w_CLADOC)
      endif
    endif
    * --- inizializzo la chiave dei documenti
    this.w_DOC.w_MVSERIAL = this.w_SERIAL
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_DOC.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- creo il cursore delle solo chiavi
    this.w_DOC.QueryKeySet("MVSERIAL='"+this.w_SERIAL+ "'","")     
    * --- carico il record
    this.w_DOC.LoadRecwarn()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CON_INDI'
    this.cWorkTables[2]='DES_DIVE'
    this.cWorkTables[3]='DIS_TINT'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='INC_CORR'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
