* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bf2                                                        *
*              Aggiorna status documenti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_122]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2012-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOperaz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bf2",oParentObject,m.pOperaz)
return(i_retval)

define class tgsve_bf2 as StdBatch
  * --- Local variables
  pOperaz = space(1)
  w_SERIAL = space(10)
  w_RSNUMRAT = 0
  w_MVAIMPN1 = 0
  w_MVAIMPS1 = 0
  w_ND = 0
  w_RSDATRAT = ctod("  /  /  ")
  w_MVAIMPN2 = 0
  w_MVAIMPS2 = 0
  w_AD = space(10)
  w_RSIMPRAT = 0
  w_MVAIMPN3 = 0
  w_MVAIMPS3 = 0
  w_DD = ctod("  /  /  ")
  w_RSMODPAG = space(10)
  w_MVAIMPN4 = 0
  w_MVAIMPS4 = 0
  w_MESS = space(10)
  w_MVFLSCOR = space(1)
  w_MVAIMPN5 = 0
  w_MVAIMPS5 = 0
  w_SERRIF = space(10)
  w_MVSPEINC = 0
  w_MVAIMPN6 = 0
  w_MVAIMPS6 = 0
  w_ROWRIF = 0
  w_MVSPEIMB = 0
  w_MVSPEBOL = 0
  w_MVFLRINC = space(1)
  w_MVANNDOC = space(4)
  w_MVSPETRA = 0
  w_MVACCONT = 0
  w_MVFLRIMB = space(1)
  w_MVPRD = space(2)
  w_GIORN1 = 0
  w_MVACCPRE = 0
  w_MVFLRTRA = space(1)
  w_MVALFDOC = space(10)
  w_GIORN2 = 0
  w_MVTIPCON = space(1)
  w_MVNUMDOC = 0
  w_MESE1 = 0
  w_MVCODCON = space(15)
  w_NUMRIF = 0
  w_MESE2 = 0
  w_MVCODPAG = space(5)
  w_NURATE = 0
  w_GIOFIS = 0
  w_MVCODVAL = space(5)
  w_APPO = 0
  w_ESCL1 = space(4)
  w_DECTOT = 0
  w_APPO1 = 0
  w_ESCL2 = space(4)
  w_APPO2 = 0
  w_MVDATDOC = ctod("  /  /  ")
  w_NUMINI = 0
  w_NUMFIN = 0
  w_TOTNUM = 0
  w_TOTRIN = 0
  w_NUMFAT = 0
  w_DATFAT = ctod("  /  /  ")
  w_OK = .f.
  w_INSTOT = .f.
  w_OKINS = .f.
  w_PRODOC = 0
  w_RSFLPROV = space(1)
  w_MVAFLOM1 = space(1)
  w_MVAFLOM2 = space(1)
  w_MVAFLOM3 = space(1)
  w_MVAFLOM4 = space(1)
  w_MVAFLOM5 = space(1)
  w_MVAFLOM6 = space(1)
  RA = space(10)
  * --- WorkFile variables
  DOC_MAST_idx=0
  DET_DIFF_idx=0
  DOC_DETT_idx=0
  DOC_RATE_idx=0
  CONTI_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna lo Status dei Documenti associati al Fatturazione Differita (da GSVE_KFD)
    if this.pOperaz="I" 
      if UPPER(this.oParentobject.oParentobject.cFunction)<>"EDIT"
        this.w_MESS = "Per confermare i documenti � necessario entrare in modifica del piano di fatturazione"
        ah_ErrorMsg(this.w_MESS)
      endif
      i_retcode = 'stop'
      return
    endif
    if this.pOperaz="U"
      * --- Aggiorna Dati sull'Anagrafica
      WITH this.oParentObject.oParentObject
      .w_PSSTATUS = this.oParentObject.w_PSSTATUS
      .w_PSNUMDOC = this.oParentObject.w_PSNUMDOC
      .w_PSALFDOC = this.oParentObject.w_PSALFDOC
      .w_PSDATDOC = this.oParentObject.w_PSDATDOC
      .w_PSDATDIV = this.oParentObject.w_PSDATDIV
      .w_PSDATCIV = this.oParentObject.w_PSDATCIV
      ENDWITH
      * --- Aggiorna Anagrafica
      this.oParentObject.oParentObject.bUpdated=.T.
      do cp_DoAction with "ecpSave"
    else
      * --- Rate Scadenze
      DIMENSION RA[1000, 6]
      this.oParentObject.w_PSSTATUS = "N"
      this.w_MVNUMDOC = this.oParentObject.w_PSNUMDOC
      this.w_MVDATDOC = this.oParentObject.w_PSDATDOC
      this.w_MVALFDOC = this.oParentObject.w_PSALFDOC
      this.w_MVANNDOC = STR(YEAR(this.oParentObject.w_PSDATDOC), 4, 0)
      this.w_MVPRD = this.oParentObject.w_OPRD
      this.w_PRODOC = 0
      * --- Legge l'ultimo progressivo
      i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
      cp_AskTableProg(this, i_Conn, "PRDOC", "i_CODAZI,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_PRODOC")
      this.w_OK = .T.
      this.w_INSTOT = .T.
      this.w_OKINS = .T.
      if NOT EMPTY(this.oParentObject.w_PSSERIAL)
        * --- Elenco dei Numeri e Date Documenti Inseriti
        vq_exec("QUERY\GSVE_BF2.VQR",this,"DOCINS")
        * --- Elenco dei Numeri e Date Documenti da Inserire
        vq_exec("QUERY\GSVE_BF3.VQR",this,"FATTUR")
        this.w_TOTNUM = 0
        this.w_TOTRIN = 0
        if USED("FATTUR")
          SELECT FATTUR
          this.w_TOTNUM = RECCOUNT()
        endif
        this.w_NUMINI = this.w_MVNUMDOC
        this.w_NUMFIN = this.w_NUMINI + (this.w_TOTNUM - 1)
        if this.w_TOTNUM=0
          this.w_MESS = "Non ci sono documenti da rinumerare"
          ah_ErrorMsg(this.w_MESS)
          this.w_OK = .F.
        else
          * --- Verifica se Inseribile il Primo Numero
          this.w_NUMFAT = this.w_NUMINI
          this.w_DATFAT = this.w_MVDATDOC
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_OKINS=.F.
            this.w_MESS = "Numerazione documenti fuori sequenza%0Riallineare la data fattura o modificare la serie di numerazione"
            ah_ErrorMsg(this.w_MESS,,"")
            this.w_OK = .F.
          else
            * --- Verifica se Inseribile l'Ultimo Numero
            this.w_NUMFAT = this.w_NUMFIN
            this.w_DATFAT = this.w_MVDATDOC
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_OKINS=.F.
              this.w_INSTOT = .F.
              this.w_MESS = "Numero documenti da assegnare (%1)%0maggiore del numero massimo inseribili%0Rinumero i soli documenti inseribili?%0(Se <s�> i documenti non rinumerabili verranno mantenuti provvisori)"
              this.w_OK = ah_YesNo(this.w_MESS,"", ALLTRIM(STR(this.w_TOTNUM,6,0)) )
            endif
          endif
        endif
        * --- Verifica i Limiti di Inseribilita' dei Numeri Documento
        this.w_OKINS = .T.
        if this.w_OK
          SELECT FATTUR
          GO TOP
          SCAN FOR NOT EMPTY(NVL(DPSERDOC,""))
          this.w_SERIAL = DPSERDOC
          this.w_ND = NVL(DPNUMDOC, 0)
          this.w_AD = NVL(DPALFDOC,"")
          this.w_DD = CP_TODATE(DPDATDOC)
          if NOT EMPTY(this.w_SERIAL)
            this.w_MESS = "Aggiorno status%0Doc. n.: %1 del %2"
            ah_Msg(this.w_MESS,.T.,.F.,.F.,STR(this.w_ND,15,0) + IIF(EMPTY(this.w_AD),"","/"+Alltrim(this.w_AD)), DTOC(this.w_DD) )
            this.w_MVALFDOC = this.oParentObject.w_PSALFDOC
            this.w_MVANNDOC = STR(YEAR(this.oParentObject.w_PSDATDOC), 4, 0)
            this.w_MVPRD = this.oParentObject.w_OPRD
            this.w_MVNUMDOC = this.w_NUMINI
            * --- Verifica inseribilita' Documento
            this.w_NUMFAT = this.w_MVNUMDOC
            this.w_DATFAT = this.w_MVDATDOC
            if this.w_OKINS
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              SELECT FATTUR
              if this.w_OKINS
                * --- Try
                local bErr_03ADCBA8
                bErr_03ADCBA8=bTrsErr
                this.Try_03ADCBA8()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- rollback
                  bTrsErr=.t.
                  cp_EndTrs(.t.)
                endif
                bTrsErr=bTrsErr or bErr_03ADCBA8
                * --- End
              endif
            endif
            * --- Incremente il Successivo Numero Documento 
            this.w_NUMINI = this.w_NUMINI + 1
          endif
          SELECT FATTUR
          ENDSCAN
          if this.w_TOTNUM=this.w_TOTRIN
            if this.w_TOTNUM=0
              ah_ErrorMsg("Non esistono documenti da rinumerare")
            else
              ah_ErrorMsg("Rinumerazione documenti completata")
            endif
          else
            if this.w_TOTRIN=0
              ah_ErrorMsg("Impossibile rinumerare i documenti")
            else
              this.w_MESS = "Rinumerazione documenti parzialmente completata%0Documenti rinumerati: %1 - su %2 documenti da rinumerare"
              ah_ErrorMsg(this.w_MESS,,"", ALLTRIM(STR(this.w_TOTRIN)), ALLTRIM(STR(this.w_TOTNUM)) )
              this.oParentObject.w_PSSTATUS = "S"
              * --- Se Resta Provvisoria
              WITH this.oParentObject.oParentObject
              this.oParentObject.w_PSNUMDOC = .w_PSNUMDOC
              this.oParentObject.w_PSALFDOC = .w_PSALFDOC
              this.oParentObject.w_PSDATDOC = .w_PSDATDOC
              this.oParentObject.w_PSDATDIV = .w_PSDATDIV
              this.oParentObject.w_PSDATCIV = .w_PSDATCIV
              ENDWITH
            endif
          endif
        endif
        this.oParentObject.w_OKAGG = this.w_OK
        WAIT CLEAR
        if USED("DOCINS")
          SELECT DOCINS
          USE
        endif
        if USED("FATTUR")
          SELECT FATTUR
          USE
        endif
      endif
    endif
  endproc
  proc Try_03ADCBA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLPROV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSSTATUS),'DOC_MAST','MVFLPROV');
      +",MVDATREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSDATDOC),'DOC_MAST','MVDATREG');
      +",MVDATDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSDATDOC),'DOC_MAST','MVDATDOC');
      +",MVDATCIV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSDATCIV),'DOC_MAST','MVDATCIV');
      +",MVDATDIV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSDATDIV),'DOC_MAST','MVDATDIV');
      +",MVNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
      +",MVALFDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'DOC_MAST','MVALFDOC');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          MVFLPROV = this.oParentObject.w_PSSTATUS;
          ,MVDATREG = this.oParentObject.w_PSDATDOC;
          ,MVDATDOC = this.oParentObject.w_PSDATDOC;
          ,MVDATCIV = this.oParentObject.w_PSDATCIV;
          ,MVDATDIV = this.oParentObject.w_PSDATDIV;
          ,MVNUMDOC = this.w_MVNUMDOC;
          ,MVALFDOC = this.w_MVALFDOC;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Se Cambiata Data Documento o Data Diverso Pagamento
    if this.oParentObject.w_ODATDIV<>this.oParentObject.w_PSDATDIV OR this.oParentObject.w_ODATDOC<>this.oParentObject.w_PSDATDOC
      * --- Riscadenzia Le Rate sul Documento
      * --- Delete from DOC_RATE
      i_nConn=i_TableProp[this.DOC_RATE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"RSSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        delete from (i_cTable) where;
              RSSERIAL = this.w_SERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      this.w_MVFLSCOR = " "
      this.w_MVAIMPN1 = 0
      this.w_MVAIMPS1 = 0
      this.w_GIORN1 = 0
      this.w_APPO2 = 0
      this.w_MVAIMPN2 = 0
      this.w_MVAIMPS2 = 0
      this.w_GIORN2 = 0
      this.w_MVSPEINC = 0
      this.w_MVAIMPN3 = 0
      this.w_MVAIMPS3 = 0
      this.w_MESE1 = 0
      this.w_MVSPEIMB = 0
      this.w_MVAIMPN4 = 0
      this.w_MVAIMPS4 = 0
      this.w_MESE2 = 0
      this.w_MVSPETRA = 0
      this.w_MVAIMPN5 = 0
      this.w_MVAIMPS5 = 0
      this.w_GIOFIS = 0
      this.w_MVFLRINC = " "
      this.w_MVAIMPN6 = 0
      this.w_MVAIMPS6 = 0
      this.w_MVFLRIMB = " "
      this.w_MVSPEBOL = 0
      this.w_DECTOT = g_PERPVL
      this.w_MVFLRTRA = " "
      this.w_MVACCONT = 0
      this.w_APPO = 0
      this.w_MVACCPRE = 0
      this.w_MVTIPCON = " "
      this.w_MVCODPAG = " "
      this.w_MVCODCON = " "
      this.w_MVCODVAL = " "
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              MVSERIAL = this.w_SERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVFLSCOR = NVL(cp_ToDate(_read_.MVFLSCOR),cp_NullValue(_read_.MVFLSCOR))
        this.w_MVSPEINC = NVL(cp_ToDate(_read_.MVSPEINC),cp_NullValue(_read_.MVSPEINC))
        this.w_MVSPEIMB = NVL(cp_ToDate(_read_.MVSPEIMB),cp_NullValue(_read_.MVSPEIMB))
        this.w_MVSPETRA = NVL(cp_ToDate(_read_.MVSPETRA),cp_NullValue(_read_.MVSPETRA))
        this.w_MVFLRINC = NVL(cp_ToDate(_read_.MVFLRINC),cp_NullValue(_read_.MVFLRINC))
        this.w_MVFLRIMB = NVL(cp_ToDate(_read_.MVFLRIMB),cp_NullValue(_read_.MVFLRIMB))
        this.w_MVFLRTRA = NVL(cp_ToDate(_read_.MVFLRTRA),cp_NullValue(_read_.MVFLRTRA))
        this.w_MVAIMPN1 = NVL(cp_ToDate(_read_.MVAIMPN1),cp_NullValue(_read_.MVAIMPN1))
        this.w_MVAIMPN2 = NVL(cp_ToDate(_read_.MVAIMPN2),cp_NullValue(_read_.MVAIMPN2))
        this.w_MVAIMPN3 = NVL(cp_ToDate(_read_.MVAIMPN3),cp_NullValue(_read_.MVAIMPN3))
        this.w_MVAIMPN4 = NVL(cp_ToDate(_read_.MVAIMPN4),cp_NullValue(_read_.MVAIMPN4))
        this.w_MVAIMPN5 = NVL(cp_ToDate(_read_.MVAIMPN5),cp_NullValue(_read_.MVAIMPN5))
        this.w_MVAIMPN6 = NVL(cp_ToDate(_read_.MVAIMPN6),cp_NullValue(_read_.MVAIMPN6))
        this.w_MVAIMPS1 = NVL(cp_ToDate(_read_.MVAIMPS1),cp_NullValue(_read_.MVAIMPS1))
        this.w_MVAIMPS2 = NVL(cp_ToDate(_read_.MVAIMPS2),cp_NullValue(_read_.MVAIMPS2))
        this.w_MVAIMPS3 = NVL(cp_ToDate(_read_.MVAIMPS3),cp_NullValue(_read_.MVAIMPS3))
        this.w_MVAIMPS4 = NVL(cp_ToDate(_read_.MVAIMPS4),cp_NullValue(_read_.MVAIMPS4))
        this.w_MVAIMPS5 = NVL(cp_ToDate(_read_.MVAIMPS5),cp_NullValue(_read_.MVAIMPS5))
        this.w_MVAIMPS6 = NVL(cp_ToDate(_read_.MVAIMPS6),cp_NullValue(_read_.MVAIMPS6))
        this.w_MVSPEBOL = NVL(cp_ToDate(_read_.MVSPEBOL),cp_NullValue(_read_.MVSPEBOL))
        this.w_MVACCONT = NVL(cp_ToDate(_read_.MVACCONT),cp_NullValue(_read_.MVACCONT))
        this.w_MVACCPRE = NVL(cp_ToDate(_read_.MVACCPRE),cp_NullValue(_read_.MVACCPRE))
        this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
        this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
        this.w_MVCODVAL = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
        this.w_MVCODPAG = NVL(cp_ToDate(_read_.MVCODPAG),cp_NullValue(_read_.MVCODPAG))
        this.w_MVAFLOM1 = NVL(cp_ToDate(_read_.MVAFLOM1),cp_NullValue(_read_.MVAFLOM1))
        this.w_MVAFLOM2 = NVL(cp_ToDate(_read_.MVAFLOM2),cp_NullValue(_read_.MVAFLOM2))
        this.w_MVAFLOM3 = NVL(cp_ToDate(_read_.MVAFLOM3),cp_NullValue(_read_.MVAFLOM3))
        this.w_MVAFLOM4 = NVL(cp_ToDate(_read_.MVAFLOM4),cp_NullValue(_read_.MVAFLOM4))
        this.w_MVAFLOM5 = NVL(cp_ToDate(_read_.MVAFLOM5),cp_NullValue(_read_.MVAFLOM5))
        this.w_MVAFLOM6 = NVL(cp_ToDate(_read_.MVAFLOM6),cp_NullValue(_read_.MVAFLOM6))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANGIOSC1,ANGIOSC2,AN1MESCL,AN2MESCL,ANGIOFIS"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANGIOSC1,ANGIOSC2,AN1MESCL,AN2MESCL,ANGIOFIS;
          from (i_cTable) where;
              ANTIPCON = this.w_MVTIPCON;
              and ANCODICE = this.w_MVCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_GIORN1 = NVL(cp_ToDate(_read_.ANGIOSC1),cp_NullValue(_read_.ANGIOSC1))
        this.w_GIORN2 = NVL(cp_ToDate(_read_.ANGIOSC2),cp_NullValue(_read_.ANGIOSC2))
        this.w_MESE1 = NVL(cp_ToDate(_read_.AN1MESCL),cp_NullValue(_read_.AN1MESCL))
        this.w_MESE2 = NVL(cp_ToDate(_read_.AN2MESCL),cp_NullValue(_read_.AN2MESCL))
        this.w_GIOFIS = NVL(cp_ToDate(_read_.ANGIOFIS),cp_NullValue(_read_.ANGIOFIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_MVCODVAL<>g_PERVAL
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_MVCODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Calcola Eventuali Rate
      if this.w_MVFLSCOR<>"S"
        * --- Spese (Solo quelle da Ripartire)
        this.w_APPO2 = IIF(this.w_MVFLRINC="S", 0, this.w_MVSPEINC) + IIF(this.w_MVFLRIMB="S", 0, this.w_MVSPEIMB) + IIF(this.w_MVFLRTRA="S", 0, this.w_MVSPETRA)
      endif
      * --- Netto Rata: Tot.Imponibile+Spese Bolli-Acconti-Spese non Rip.
      this.w_APPO = IIF(this.w_MVAFLOM1="X", this.w_MVAIMPN1,0)
      this.w_APPO = this.w_APPO + IIF(this.w_MVAFLOM2="X", this.w_MVAIMPN2,0)
      this.w_APPO = this.w_APPO + IIF(this.w_MVAFLOM3="X", this.w_MVAIMPN3,0)
      this.w_APPO = this.w_APPO + IIF(this.w_MVAFLOM4="X", this.w_MVAIMPN4,0)
      this.w_APPO = this.w_APPO + IIF(this.w_MVAFLOM5="X", this.w_MVAIMPN5,0)
      this.w_APPO = this.w_APPO + IIF(this.w_MVAFLOM6="X", this.w_MVAIMPN6,0)
      this.w_APPO = (this.w_APPO)-(this.w_APPO2+this.w_MVACCONT+this.w_MVACCPRE)
      this.w_APPO1 = IIF(this.w_MVAFLOM1$"XI", this.w_MVAIMPS1,0)
      this.w_APPO1 = this.w_APPO1 + IIF(this.w_MVAFLOM2$"XI", this.w_MVAIMPS2,0)
      this.w_APPO1 = this.w_APPO1 + IIF(this.w_MVAFLOM3$"XI", this.w_MVAIMPS3,0)
      this.w_APPO1 = this.w_APPO1 + IIF(this.w_MVAFLOM4$"XI", this.w_MVAIMPS4,0)
      this.w_APPO1 = this.w_APPO1 + IIF(this.w_MVAFLOM5$"XI", this.w_MVAIMPS5,0)
      this.w_APPO1 = this.w_APPO1 + IIF(this.w_MVAFLOM6$"XI", this.w_MVAIMPS6,0)
      this.w_ESCL1 = STR(this.w_MESE1, 2, 0) + STR(this.w_GIORN1, 2, 0) + STR(this.w_GIOFIS, 2, 0)
      this.w_ESCL2 = STR(this.w_MESE2, 2, 0) + STR(this.w_GIORN2, 2, 0)
      this.w_NURATE = SCADENZE("RA",this.w_MVCODPAG,this.oParentObject.w_PSDATDOC,this.w_APPO,this.w_APPO1,this.w_APPO2,this.w_ESCL1,this.w_ESCL2,this.w_DECTOT,this.oParentObject.w_PSDATDIV)
      FOR L_i = 1 TO this.w_NURATE
      if NOT EMPTY(RA[L_i, 1]) AND (NOT EMPTY(RA[L_i, 2]) Or NOT EMPTY(RA[L_i, 3]) Or NOT EMPTY(RA[L_i, 4]) )
        this.w_RSNUMRAT = L_i
        this.w_RSDATRAT = RA[L_i, 1]
        this.w_RSIMPRAT = RA[L_i, 2] + RA[L_i, 3] + RA[L_i, 4]
        this.w_RSMODPAG = RA[L_i, 5]
        this.w_RSFLPROV = RA[L_i, 6]
        * --- Scrive Doc_Rate
        * --- Insert into DOC_RATE
        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLSOSP"+",RSFLPROV"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'DOC_RATE','RSSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSNUMRAT),'DOC_RATE','RSNUMRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSDATRAT),'DOC_RATE','RSDATRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSIMPRAT),'DOC_RATE','RSIMPRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSMODPAG),'DOC_RATE','RSMODPAG');
          +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_RATE','RSFLSOSP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSFLPROV),'DOC_RATE','RSFLPROV');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.w_SERIAL,'RSNUMRAT',this.w_RSNUMRAT,'RSDATRAT',this.w_RSDATRAT,'RSIMPRAT',this.w_RSIMPRAT,'RSMODPAG',this.w_RSMODPAG,'RSFLSOSP'," ",'RSFLPROV',this.w_RSFLPROV)
          insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLSOSP,RSFLPROV &i_ccchkf. );
             values (;
               this.w_SERIAL;
               ,this.w_RSNUMRAT;
               ,this.w_RSDATRAT;
               ,this.w_RSIMPRAT;
               ,this.w_RSMODPAG;
               ," ";
               ,this.w_RSFLPROV;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      ENDFOR
    endif
    if this.oParentObject.w_ODATDOC<>this.oParentObject.w_PSDATDOC
      * --- Aggiorna il Riferimento data Generazione FD dai Documenti che l' hanno Generata
      * --- Select from DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MVSERRIF,MVROWRIF  from "+i_cTable+" DOC_DETT ";
            +" where MVSERIAL="+cp_ToStrODBC(this.w_SERIAL)+"";
             ,"_Curs_DOC_DETT")
      else
        select MVSERRIF,MVROWRIF from (i_cTable);
         where MVSERIAL=this.w_SERIAL;
          into cursor _Curs_DOC_DETT
      endif
      if used('_Curs_DOC_DETT')
        select _Curs_DOC_DETT
        locate for 1=1
        do while not(eof())
        this.w_SERRIF = NVL(_Curs_DOC_DETT.MVSERRIF," ")
        this.w_ROWRIF = NVL(_Curs_DOC_DETT.MVROWRIF, 0)
        this.w_NUMRIF = -20
        if NOT EMPTY(this.w_SERRIF) AND this.w_ROWRIF>0
          * --- Try
          local bErr_03CC7420
          bErr_03CC7420=bTrsErr
          this.Try_03CC7420()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03CC7420
          * --- End
        endif
          select _Curs_DOC_DETT
          continue
        enddo
        use
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_TOTRIN = this.w_TOTRIN + 1
    return
  proc Try_03CC7420()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSDATDOC),'DOC_DETT','MVDATGEN');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
          +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
             )
    else
      update (i_cTable) set;
          MVDATGEN = this.oParentObject.w_PSDATDOC;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_SERRIF;
          and CPROWNUM = this.w_ROWRIF;
          and MVNUMRIF = this.w_NUMRIF;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se il Numero Documento e' Inseribile
    SELECT DOCINS
    GO TOP
    LOCATE FOR NVL(NUMDOC,0) = this.w_NUMFAT
    if FOUND()
      * --- Trovato un documento con lo stesso numero ; non inseribile!
      this.w_OKINS = .F.
    else
      * --- Cerca se esiste un Documento con Numero Maggiore
      GO TOP
      LOCATE FOR NVL(NUMDOC,0) > this.w_NUMFAT
      if FOUND()
        * --- Trovato un documento con numero maggiore ; Verifica se ci sono "buchi" disponibili
        if CP_TODATE(DATDOC) < this.w_DATFAT
          * --- La Data del documento inserito con Numero Maggiore e' minore della Data doc. da Inserire ; Non Inseribile!
          this.w_OKINS = .F.
        else
          * --- Data Documento da Inserire Minore o Uguale al Doc.Successivo
          * --- Se uguale o non ci sono documenti minori; OK
          if CP_TODATE(DATDOC) > this.w_DATFAT AND NOT BOF()
            * --- Altrimenti verifico la Data del Doc. Precedente
            SKIP -1
            if CP_TODATE(DATDOC) > this.w_DATFAT
              * --- La Data Documento Precedente (con Numero minore) e' Maggiore della Data doc. da Inserire; Non Inseribile!
              this.w_OKINS = .F.
            endif
          endif
        endif
      else
        * --- Il numero che si vuole inserire e' maggiore di tutti ; Verifica che la Data sia Congruente
        GO BOTTOM
        if CP_TODATE(DATDOC) > this.w_DATFAT
          * --- La Data dell' ultimo documento Inserito e' maggiore della Data doc. da Inserire ; Non Inseribile!
          this.w_OKINS = .F.
        else
          * --- Ultimo Numero Aggiorna il Progressivo Massimo!
          this.w_MVNUMDOC = IIF(this.w_MVNUMDOC<=this.w_PRODOC, this.w_MVNUMDOC, 0)
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pOperaz)
    this.pOperaz=pOperaz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DET_DIFF'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='DOC_RATE'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='VALUTE'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOperaz"
endproc
