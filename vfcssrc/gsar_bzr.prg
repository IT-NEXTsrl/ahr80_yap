* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bzr                                                        *
*              Lancia aree di destinazione                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-07-14                                                      *
* Last revis.: 2008-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bzr",oParentObject)
return(i_retval)

define class tgsar_bzr as StdBatch
  * --- Local variables
  w_PROG = .NULL.
  w_CODICE = space(5)
  w_DXBTN = .f.
  w_OKAREA = .f.
  * --- WorkFile variables
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia l'anagrafica Risorse umane da modulo BUAN
    this.w_OKAREA = .F.
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      this.w_CODICE = Nvl(g_oMenu.oKey(1,3),"")
      if g_oMenu.oKey(1,1)="RCCODICE"
        this.w_OKAREA = .T.
      endif
    else
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      * --- Tipo di Conto
      if TYPE("&cCurs..RCCODICE")<>"U"
        this.w_CODICE = &cCurs..RCCODICE
      else
        * --- Lancia aree
        this.w_PROG = GSCA_MRC(.T.)
        if TYPE("&cCurs..RCCODICE")<>"U"
          this.w_OKAREA = .T.
        endif
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_PROG = GSCA_MRC(.T.)
    if this.w_DXBTN
      * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
      *     presente nella variabile
      if g_oMenu.cBatchType$"AM"
        * --- Apri o Modifica
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_RCCODICE = this.w_CODICE
        this.w_PROG.ecpSave()     
        if g_oMenu.cBatchType="M"
          * --- Modifica
          this.w_PROG.ecpEdit()     
        endif
      endif
      if g_oMenu.cBatchType="L"
        if type ("this.w_PROG")="L"
          * --- Lancia Aree
          this.w_PROG = GSCA_MRC(.T.)
        endif
        * --- Carica
        this.w_PROG.ecpLoad()     
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
