* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bgi                                                        *
*              GENERAZIONE ELEMENTO STREAM                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-11                                                      *
* Last revis.: 2014-12-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCLASSE,pARCHIVIO,pSERIAL,pCREAIND
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bgi",oParentObject,m.pCLASSE,m.pARCHIVIO,m.pSERIAL,m.pCREAIND)
return(i_retval)

define class tgsar_bgi as StdBatch
  * --- Local variables
  pCLASSE = space(15)
  pARCHIVIO = space(30)
  pSERIAL = space(10)
  pCREAIND = .f.
  w_TROV = .f.
  w_PATHFILE = space(200)
  w_MultiRep = .NULL.
  w_TDTIPDOC = space(5)
  w_MVCODCON = space(15)
  w_MVTIPCON = space(1)
  w_TIPOIN = space(5)
  w_CODESE = space(4)
  w_NUMINI = 0
  w_NUMFIN = 0
  w_DATAIN = ctod("  /  /  ")
  w_DATAFI = ctod("  /  /  ")
  w_CLIFOR = space(15)
  w_MVRIFFAD = space(10)
  w_MVSERIAL = space(10)
  w_GRPDEF = space(5)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod("  /  /  ")
  w_TDCATDOC = space(2)
  w_NAZION = space(3)
  w_CODGRU = space(5)
  w_MVCODAGE = space(5)
  w_NOMEFILE = space(10)
  w_LINGUA = space(3)
  w_DATAIN = ctod("  /  /  ")
  w_DATAFI = ctod("  /  /  ")
  w_CODAGE = space(5)
  w_CODCOM = space(15)
  w_CODATT1 = space(15)
  w_CODATT2 = space(15)
  w_SERIE1 = space(2)
  w_SERIE2 = space(2)
  w_CATEGO = space(2)
  w_CODVET = space(5)
  w_CODZON = space(3)
  w_CATCOM = space(3)
  w_CODDES = space(5)
  w_CODPAG = space(5)
  w_NOSBAN = space(15)
  w_CDPUBWEB = space(1)
  w_INARCHIVIO = space(1)
  w_CDMODALL = space(1)
  w_READAZI = space(5)
  w_ALLCBI = space(1)
  w_OKDIR = .f.
  w_StmpOrderListStr = space(254)
  w_nCount = 0
  w_NUMREP = 0
  w_FILE = space(10)
  w_ONOMEFILE = space(10)
  w_PATALL = space(254)
  w_NOMFIL = space(220)
  w_AGG_ORI = space(220)
  w_IDLORIFIL = space(250)
  w_LORIFIL = space(250)
  * --- WorkFile variables
  PROMCLAS_idx=0
  DOC_MAST_idx=0
  CONTI_idx=0
  OUTPUTMP_idx=0
  AZIENDA_idx=0
  PRODINDI_idx=0
  PROMINDI_idx=0
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguita in funzione CREAELEEDI()
    *     Crea indice se non trovato e restituisce codifica 64 bit dello stream
    * --- Classe documentale da elemento
    *     dalla quale devo leggere il path dve creare il file
    * --- Archivo di riferimento
    * --- Crea indice se non trovato
    this.w_INARCHIVIO = "X"
    private L_SerIndex
    L_SerIndex=""
    this.w_NOMEFILE = " "
    this.w_MVSERIAL = this.pSERIAL
    if Not Empty(this.pSERIAL)
      this.w_MultiRep=createobject("Multireport")
      * --- Read from PROMCLAS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PROMCLAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CDPATSTD,CDPUBWEB,CDMODALL"+;
          " from "+i_cTable+" PROMCLAS where ";
              +"CDCODCLA = "+cp_ToStrODBC(this.pCLASSE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CDPATSTD,CDPUBWEB,CDMODALL;
          from (i_cTable) where;
              CDCODCLA = this.pCLASSE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PATHFILE = NVL(cp_ToDate(_read_.CDPATSTD),cp_NullValue(_read_.CDPATSTD))
        this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
        this.w_CDMODALL = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVTIPDOC,MVCODCON,MVTIPCON,MVNUMDOC,MVALFDOC,MVDATDOC,MVCLADOC,MVCODAGE"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.pSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVTIPDOC,MVCODCON,MVTIPCON,MVNUMDOC,MVALFDOC,MVDATDOC,MVCLADOC,MVCODAGE;
          from (i_cTable) where;
              MVSERIAL = this.pSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TDTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
        this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
        this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
        this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
        this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
        this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
        this.w_TDCATDOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
        this.w_MVCODAGE = NVL(cp_ToDate(_read_.MVCODAGE),cp_NullValue(_read_.MVCODAGE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_READAZI = i_CODAZI
      * --- Read from CONTROPA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COALLCBI"+;
          " from "+i_cTable+" CONTROPA where ";
              +"COCODAZI = "+cp_ToStrODBC(this.w_READAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COALLCBI;
          from (i_cTable) where;
              COCODAZI = this.w_READAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ALLCBI = NVL(cp_ToDate(_read_.COALLCBI),cp_NullValue(_read_.COALLCBI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Try
      local bErr_00E868F8
      bErr_00E868F8=bTrsErr
      this.Try_00E868F8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_errormsg("Errore nella generazione del file allegati!")
      endif
      bTrsErr=bTrsErr or bErr_00E868F8
      * --- End
      if Not empty(this.w_MVCODCON)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODLIN,ANGRPDEF,ANNAZION,ANCODGRU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODLIN,ANGRPDEF,ANNAZION,ANCODGRU;
            from (i_cTable) where;
                ANTIPCON = this.w_MVTIPCON;
                and ANCODICE = this.w_MVCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LINGUA = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
          this.w_GRPDEF = NVL(cp_ToDate(_read_.ANGRPDEF),cp_NullValue(_read_.ANGRPDEF))
          this.w_NAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
          this.w_CODGRU = NVL(cp_ToDate(_read_.ANCODGRU),cp_NullValue(_read_.ANCODGRU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Array che il report si aspetta di ricevere
       
 dimension iva(6,2) 
 dimension doc_rate(999,3) 
 dimension doc_imba(20,7) 
 dimension doc_rite(6,5) 
 dimension sedefat(12) 
 dimension contocli (60,6) 
 iva="" 
 doc_rate="" 
 doc_rite="" 
 doc_imba="" 
 sedefat="" 
 contocli=""
      dimension aCONT_ACC(6,3)
      aCONT_ACC=""
      * --- Cambio della Valuta in LIRE utilizzato in talune stampe
      l_cambio = GETCAM(g_PERVAL, I_DATSYS)
      * --- Passo al report Flag per incasso Corrispettivi
      L_FLINCA=" "
      * --- SETTO VARIABILI PER FAX E EMAIL
      * --- Cerco indice 
      *     ====================
      * --- Popolo oggetto multireport
      * --- Create temporary table OUTPUTMP
      i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.AZIENDA_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            +" where 1=0";
            )
      this.OUTPUTMP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      GSAR_BSD(this,this.w_MultiRep, This, this.w_TDTIPDOC, this.w_LINGUA, this.w_GRPDEF, "D")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Drop temporary table OUTPUTMP
      i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('OUTPUTMP')
      endif
      * --- Archiviazione standard singola ...
      this.w_NUMREP = this.w_MultiRep.GetNumReport()
      do while this.w_NUMREP>0
        this.w_NUMREP = this.w_NUMREP-1
        if this.w_NUMREP>0
          this.w_MultiRep.SetPrnSys(.f., this.w_NUMREP)     
        endif
      enddo
      * --- ----------  Multireport -----------
      if ! this.w_MultiRep.IsEmpty()
        CP_CHPRN(this.w_MultiRep, , this,10)
      endif
      PRIVATE w_CURSORE
      this.w_StmpOrderListStr = this.w_MultiRep.GetStmpOrderList()
      this.w_nCount = this.w_MultiRep.GetMainReport(this.w_StmpOrderListStr,this.w_nCount)
      if this.w_nCount>0
        w_CURSORE =this.w_MultiRep.GetCursorName(this.w_nCount)
      endif
      this.w_FILE = Searchindex(this.pCLASSE,this.pARCHIVIO,this.pSERIAL,"3")
      if Empty(this.w_FILE) 
        * --- Creo un indice nuovo
        if this.pCREAIND AND USED((w_CURSORE))
          Select (w_CURSORE) 
 Go top
          * --- Gestione ARCHIVIAZIONE DOCUMENTALE ===========================================================
          Private L_ArrParam
          dimension L_ArrParam(30)
          L_ArrParam(1)="AR"
          L_ArrParam(2)=this.w_NOMEFILE
          L_ArrParam(3)=this.pCLASSE
          L_ArrParam(4)=i_CODUTE
          L_ArrParam(5)=w_CURSORE
          L_ArrParam(6)=this.pARCHIVIO
          L_ArrParam(7)=this.w_MVSERIAL
          L_ArrParam(8)="Allegato CBI"
          L_ArrParam(9)=""
          L_ArrParam(10)=.T.
          L_ArrParam(11)=.T.
          L_ArrParam(12)=" "
          L_ArrParam(13)="S"
          L_ArrParam(14)=.null.
          L_ArrParam(15)=" "
          L_ArrParam(16)=" "
          L_ArrParam(17)=""
          L_ArrParam(18)=" "
          L_ArrParam(19)=""
          L_ArrParam(21)=.F.
          L_ArrParam(22)=" "
          L_ArrParam(23)=( NVL(this.w_CDPUBWEB, "N")="S" )
          L_ArrParam(24)=.F.
          GSUT_BBA(this,@L_ArrParam, @l_SerIndex)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        this.w_NOMEFILE = this.w_FILE
      endif
      this.w_MultiRep = .null.
    endif
    this.w_ONOMEFILE = this.w_NOMEFILE
    * --- Read from PROMINDI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IDPATALL,IDNOMFIL,IDORIGIN,IDORIFIL"+;
        " from "+i_cTable+" PROMINDI where ";
            +"IDSERIAL = "+cp_ToStrODBC(L_SerIndex);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IDPATALL,IDNOMFIL,IDORIGIN,IDORIFIL;
        from (i_cTable) where;
            IDSERIAL = L_SerIndex;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PATALL = NVL(cp_ToDate(_read_.IDPATALL),cp_NullValue(_read_.IDPATALL))
      this.w_NOMFIL = NVL(cp_ToDate(_read_.IDNOMFIL),cp_NullValue(_read_.IDNOMFIL))
      this.w_AGG_ORI = NVL(cp_ToDate(_read_.IDORIGIN),cp_NullValue(_read_.IDORIGIN))
      this.w_IDLORIFIL = NVL(cp_ToDate(_read_.IDORIFIL),cp_NullValue(_read_.IDORIFIL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_CDMODALL="S" 
      * --- A seconda che l'indice appena fatto si copia o collegamento il file generato � memorizzato in campi diversi
      this.w_LORIFIL = Alltrim(w_IDORIFIL)
    else
      this.w_LORIFIL = Alltrim(this.w_PATALL)+Alltrim(this.w_NOMFIL)
    endif
    if cp_FILEEXIST(Alltrim(this.w_LORIFIL))
      DELETE FILE (this.w_NOMEFILE)
      i_retcode = 'stop'
      i_retval = this.w_LORIFIL
      return
    else
      i_retcode = 'stop'
      i_retval = this.w_NOMEFILE
      return
    endif
    if USED((w_CURSORE))
      SELECT w_CURSORE 
 USE
    endif
  endproc
  proc Try_00E868F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
     
 DIMENSION ARPARAM[7,2] 
 ARPARAM[1,1]="DATE" 
 ARPARAM[1,2]=ALLTRIM(STR(YEAR(i_DATSYS)))+Right("00"+ALLTRIM(STR(MONTH(i_DATSYS))),2)+Right("00"+ALLTRIM(STR(DAY(i_DATSYS))),2) 
 ARPARAM[2,1]="TIME" 
 ARPARAM[2,2]=ALLTRIM(STRTRAN(TIME(),":","")) 
 ARPARAM[3,1]="DATDOC" 
 Datdoc=this.w_MVDATDOC 
 CAMPO="ALLTRIM(STR(YEAR(DATDOC)))+right('00'+ALLTRIM(STR(MONTH(DATDOC))),2)+right('00'+ALLTRIM(STR(DAY(DATDOC))),2)" 
 ARPARAM[3,2]=IIF(TYPE("&CAMPO")="C",&CAMPO,"X") 
 ARPARAM[4,1]="NUMDOC" 
 ARPARAM[4,2]=Alltrim(str(this.w_MVNUMDOC)) 
 ARPARAM[5,1]="SERIAL" 
 ARPARAM[5,2]=this.w_MVSERIAL 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=this.w_MVCODCON 
 ARPARAM[7,1]="TIPCON" 
 ARPARAM[7,2]=this.w_MVTIPCON
    this.w_NOMEFILE = CALDESPA(this.w_ALLCBI,@ARPARAM)
    if chknfile(ADDBS(Alltrim(this.w_PATHFILE)),"S")
      if Not Directory(Alltrim(ADDBS(Alltrim(this.w_PATHFILE))))
        this.w_OKDIR = Makedir(Alltrim(ADDBS(Alltrim(this.w_PATHFILE))))
        if Not this.w_OKDIR
          ah_errormsg("Errore generazione directory!")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    this.w_NOMEFILE = ADDBS(Alltrim(this.w_PATHFILE))+this.w_NOMEFILE
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pCLASSE,pARCHIVIO,pSERIAL,pCREAIND)
    this.pCLASSE=pCLASSE
    this.pARCHIVIO=pARCHIVIO
    this.pSERIAL=pSERIAL
    this.pCREAIND=pCREAIND
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='*OUTPUTMP'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='PRODINDI'
    this.cWorkTables[7]='PROMINDI'
    this.cWorkTables[8]='CONTROPA'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCLASSE,pARCHIVIO,pSERIAL,pCREAIND"
endproc
