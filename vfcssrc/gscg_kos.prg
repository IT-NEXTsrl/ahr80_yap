* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kos                                                        *
*              Riferimento documenti da rettificare                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-04-12                                                      *
* Last revis.: 2012-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kos",oParentObject))

* --- Class definition
define class tgscg_kos as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 794
  Height = 417
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-01-20"
  HelpContextID=166339945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_kos"
  cComment = "Riferimento documenti da rettificare"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PNNUMRER = 0
  w_PNCOMPET = space(4)
  w_PNDATREG = ctod('  /  /  ')
  w_PNSERIAL = space(10)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_DESCLF = space(40)
  w_SERIALEDIFILTRO = space(10)
  w_CONF_ELAB = space(1)
  w_PARAME = space(1)
  w_DATAREGISTRAZIONE = ctod('  /  /  ')
  w_PNCODVAL = space(3)
  w_ragru = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kosPag1","gscg_kos",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ragru = this.oPgFrm.Pages(1).oPag.ragru
    DoDefault()
    proc Destroy()
      this.w_ragru = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PNNUMRER=0
      .w_PNCOMPET=space(4)
      .w_PNDATREG=ctod("  /  /  ")
      .w_PNSERIAL=space(10)
      .w_PNTIPCLF=space(1)
      .w_PNCODCLF=space(15)
      .w_DESCLF=space(40)
      .w_SERIALEDIFILTRO=space(10)
      .w_CONF_ELAB=space(1)
      .w_PARAME=space(1)
      .w_DATAREGISTRAZIONE=ctod("  /  /  ")
      .w_PNCODVAL=space(3)
      .w_PNNUMRER=oParentObject.w_PNNUMRER
      .w_PNCOMPET=oParentObject.w_PNCOMPET
      .w_PNDATREG=oParentObject.w_PNDATREG
      .w_PNSERIAL=oParentObject.w_PNSERIAL
      .w_PNTIPCLF=oParentObject.w_PNTIPCLF
      .w_PNCODCLF=oParentObject.w_PNCODCLF
      .w_DESCLF=oParentObject.w_DESCLF
      .w_SERIALEDIFILTRO=oParentObject.w_SERIALEDIFILTRO
      .w_CONF_ELAB=oParentObject.w_CONF_ELAB
      .w_PARAME=oParentObject.w_PARAME
      .w_DATAREGISTRAZIONE=oParentObject.w_DATAREGISTRAZIONE
      .w_PNCODVAL=oParentObject.w_PNCODVAL
        .w_PNNUMRER = .w_ragru.getVar('PNNUMRER')
        .w_PNCOMPET = .w_ragru.getVar('PNCOMPET')
        .w_PNDATREG = .w_ragru.getVar('PNDATREG')
        .w_PNSERIAL = .w_ragru.getVar('PNSERIAL')
      .oPgFrm.Page1.oPag.ragru.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
          .DoRTCalc(5,8,.f.)
        .w_CONF_ELAB = 'S'
    endwith
    this.DoRTCalc(10,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PNNUMRER=.w_PNNUMRER
      .oParentObject.w_PNCOMPET=.w_PNCOMPET
      .oParentObject.w_PNDATREG=.w_PNDATREG
      .oParentObject.w_PNSERIAL=.w_PNSERIAL
      .oParentObject.w_PNTIPCLF=.w_PNTIPCLF
      .oParentObject.w_PNCODCLF=.w_PNCODCLF
      .oParentObject.w_DESCLF=.w_DESCLF
      .oParentObject.w_SERIALEDIFILTRO=.w_SERIALEDIFILTRO
      .oParentObject.w_CONF_ELAB=.w_CONF_ELAB
      .oParentObject.w_PARAME=.w_PARAME
      .oParentObject.w_DATAREGISTRAZIONE=.w_DATAREGISTRAZIONE
      .oParentObject.w_PNCODVAL=.w_PNCODVAL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_PNNUMRER = .w_ragru.getVar('PNNUMRER')
            .w_PNCOMPET = .w_ragru.getVar('PNCOMPET')
            .w_PNDATREG = .w_ragru.getVar('PNDATREG')
            .w_PNSERIAL = .w_ragru.getVar('PNSERIAL')
        .oPgFrm.Page1.oPag.ragru.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(5,8,.t.)
            .w_CONF_ELAB = 'S'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ragru.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gscg_kos
    THIS.CAPTION=IIF (This.w_PARAME='S',ah_msgformat("Riferimento documenti di saldo"),ah_msgformat("Riferimento documenti da rettificare"))
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ragru.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPNCODCLF_1_6.value==this.w_PNCODCLF)
      this.oPgFrm.Page1.oPag.oPNCODCLF_1_6.value=this.w_PNCODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_11.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_11.value=this.w_DESCLF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kosPag1 as StdContainer
  Width  = 790
  height = 417
  stdWidth  = 790
  stdheight = 417
  resizeXpos=303
  resizeYpos=184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPNCODCLF_1_6 as StdField with uid="TNOYPOOZHJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PNCODCLF", cQueryName = "PNCODCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 39231940,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=87, Top=6, InputMask=replicate('X',15)


  add object ragru as cp_zoombox with uid="IMYJOIBLJT",left=0, top=31, width=789,height=333,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PNT_MAST",cZoomFile="GSCG_KOS",bOptions=.f.,bAdvOptions=.f.,bRetriveAllRows=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 11657702


  add object oObj_1_8 as cp_runprogram with uid="JQNMJKTDTH",left=941, top=33, width=159,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BOS('A')",;
    cEvent = "w_ragru selected",;
    nPag=1;
    , HelpContextID = 11657702

  add object oDESCLF_1_11 as StdField with uid="GDSKXGBWMZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 249670602,;
   bGlobalFont=.t.,;
    Height=21, Width=541, Left=218, Top=6, InputMask=replicate('X',40)


  add object oBtn_1_17 as StdButton with uid="VMPFRPKBFV",left=684, top=367, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare le modifiche";
    , HelpContextID = 166311194;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="MTXNONHMHL",left=736, top=367, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 159022522;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_9 as StdString with uid="UJKYBVMJAQ",Visible=.t., Left=5, Top=9,;
    Alignment=1, Width=77, Height=18,;
    Caption="Cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_PNTIPCLF<>'C')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="KRDLCJCXTN",Visible=.t., Left=5, Top=9,;
    Alignment=1, Width=77, Height=18,;
    Caption="Fornitore:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_PNTIPCLF<>'F')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kos','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
