* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_klk                                                        *
*              Aggiornamento listini kit                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_223]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-18                                                      *
* Last revis.: 2007-07-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_klk",oParentObject))

* --- Class definition
define class tgsar_klk as StdForm
  Top    = 4
  Left   = 33

  * --- Standard Properties
  Width  = 577
  Height = 426
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-05"
  HelpContextID=51808407
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=35

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  FAM_ARTI_IDX = 0
  VOCIIVA_IDX = 0
  LISTINI_IDX = 0
  DISMBASE_IDX = 0
  cPrg = "gsar_klk"
  cComment = "Aggiornamento listini kit"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_LISTINO = space(5)
  w_DESLIS = space(40)
  w_CODVAL = space(3)
  w_LORNET = space(1)
  w_DATLISIN = ctod('  /  /  ')
  w_DATLISFI = ctod('  /  /  ')
  w_DATVAL = ctod('  /  /  ')
  w_CREALIS = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CODARTIN = space(20)
  w_DESART = space(40)
  w_CODARTFI = space(20)
  w_DESART1 = space(40)
  w_CODIVA = space(5)
  w_IVDESCR = space(35)
  w_GRUPMERC = space(5)
  w_GDESCRI = space(35)
  w_CATOMOGE = space(5)
  w_CTDESCRI = space(35)
  w_FAMIGLIA = space(5)
  w_FDESCRI = space(35)
  w_ARROT1 = 0
  w_VALORIN = 0
  w_ARROT2 = 0
  w_VALOR2IN = 0
  w_ARROT3 = 0
  w_VALOR3IN = 0
  w_ARROT4 = 0
  w_VALOFIN = 0
  w_ARTPOS1 = space(1)
  w_ARTPOS2 = space(1)
  w_FLSCON = space(1)
  w_TIPKIT = space(10)
  w_KITIMB = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_klkPag1","gsar_klk",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLISTINO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='GRUMERC'
    this.cWorkTables[3]='CATEGOMO'
    this.cWorkTables[4]='FAM_ARTI'
    this.cWorkTables[5]='VOCIIVA'
    this.cWorkTables[6]='LISTINI'
    this.cWorkTables[7]='DISMBASE'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSAR_BKL(this,"A")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsar_klk
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LISTINO=space(5)
      .w_DESLIS=space(40)
      .w_CODVAL=space(3)
      .w_LORNET=space(1)
      .w_DATLISIN=ctod("  /  /  ")
      .w_DATLISFI=ctod("  /  /  ")
      .w_DATVAL=ctod("  /  /  ")
      .w_CREALIS=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CODARTIN=space(20)
      .w_DESART=space(40)
      .w_CODARTFI=space(20)
      .w_DESART1=space(40)
      .w_CODIVA=space(5)
      .w_IVDESCR=space(35)
      .w_GRUPMERC=space(5)
      .w_GDESCRI=space(35)
      .w_CATOMOGE=space(5)
      .w_CTDESCRI=space(35)
      .w_FAMIGLIA=space(5)
      .w_FDESCRI=space(35)
      .w_ARROT1=0
      .w_VALORIN=0
      .w_ARROT2=0
      .w_VALOR2IN=0
      .w_ARROT3=0
      .w_VALOR3IN=0
      .w_ARROT4=0
      .w_VALOFIN=0
      .w_ARTPOS1=space(1)
      .w_ARTPOS2=space(1)
      .w_FLSCON=space(1)
      .w_TIPKIT=space(10)
      .w_KITIMB=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_LISTINO))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,6,.f.)
        .w_DATVAL = i_datsys
        .w_CREALIS = ''
        .w_OBTEST = i_datsys
        .DoRTCalc(10,11,.f.)
        if not(empty(.w_CODARTIN))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,13,.f.)
        if not(empty(.w_CODARTFI))
          .link_1_13('Full')
        endif
        .DoRTCalc(14,15,.f.)
        if not(empty(.w_CODIVA))
          .link_1_15('Full')
        endif
        .DoRTCalc(16,17,.f.)
        if not(empty(.w_GRUPMERC))
          .link_1_17('Full')
        endif
        .DoRTCalc(18,19,.f.)
        if not(empty(.w_CATOMOGE))
          .link_1_19('Full')
        endif
        .DoRTCalc(20,21,.f.)
        if not(empty(.w_FAMIGLIA))
          .link_1_21('Full')
        endif
          .DoRTCalc(22,29,.f.)
        .w_VALOFIN = MAX(.w_VALORIN, .w_VALOR2IN, .w_VALOR3IN)
          .DoRTCalc(31,33,.f.)
        .w_TIPKIT = 'K'
    endwith
    this.DoRTCalc(35,35,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,29,.t.)
            .w_VALOFIN = MAX(.w_VALORIN, .w_VALOR2IN, .w_VALOR3IN)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(31,35,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oVALORIN_1_27.enabled = this.oPgFrm.Page1.oPag.oVALORIN_1_27.mCond()
    this.oPgFrm.Page1.oPag.oARROT2_1_28.enabled = this.oPgFrm.Page1.oPag.oARROT2_1_28.mCond()
    this.oPgFrm.Page1.oPag.oVALOR2IN_1_29.enabled = this.oPgFrm.Page1.oPag.oVALOR2IN_1_29.mCond()
    this.oPgFrm.Page1.oPag.oARROT3_1_30.enabled = this.oPgFrm.Page1.oPag.oARROT3_1_30.mCond()
    this.oPgFrm.Page1.oPag.oVALOR3IN_1_31.enabled = this.oPgFrm.Page1.oPag.oVALOR3IN_1_31.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LISTINO
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LISTINO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LISTINO)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LISTINO))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LISTINO)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LISTINO) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLISTINO_1_1'),i_cWhere,'GSAR_ALI',"Anagrafica listini",'GSPS_MVD.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LISTINO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LISTINO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LISTINO)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LISTINO = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_CODVAL = NVL(_Link_.LSVALLIS,space(3))
      this.w_LORNET = NVL(_Link_.LSIVALIS,space(1))
      this.w_FLSCON = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LISTINO = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_CODVAL = space(3)
      this.w_LORNET = space(1)
      this.w_FLSCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODVAL=g_PERVAL
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino errato o inesistente")
        endif
        this.w_LISTINO = space(5)
        this.w_DESLIS = space(40)
        this.w_CODVAL = space(3)
        this.w_LORNET = space(1)
        this.w_FLSCON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LISTINO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODARTIN
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DISMBASE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DBCODICE like "+cp_ToStrODBC(trim(this.w_CODARTIN)+"%");

          i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDISKIT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DBCODICE',trim(this.w_CODARTIN))
          select DBCODICE,DBDESCRI,DBDISKIT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARTIN)==trim(_Link_.DBCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DBDESCRI like "+cp_ToStrODBC(trim(this.w_CODARTIN)+"%");

            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDISKIT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DBDESCRI like "+cp_ToStr(trim(this.w_CODARTIN)+"%");

            select DBCODICE,DBDESCRI,DBDISKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODARTIN) and !this.bDontReportError
            deferred_cp_zoom('DISMBASE','*','DBCODICE',cp_AbsName(oSource.parent,'oCODARTIN_1_11'),i_cWhere,'',"Elenco kit",'GSAR_MAK.DISMBASE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDISKIT";
                     +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',oSource.xKey(1))
            select DBCODICE,DBDESCRI,DBDISKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDISKIT";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_CODARTIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_CODARTIN)
            select DBCODICE,DBDESCRI,DBDISKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARTIN = NVL(_Link_.DBCODICE,space(20))
      this.w_DESART = NVL(_Link_.DBDESCRI,space(40))
      this.w_KITIMB = NVL(_Link_.DBDISKIT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODARTIN = space(20)
      endif
      this.w_DESART = space(40)
      this.w_KITIMB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_CODARTFI)) OR  (.w_CODARTIN<=.w_CODARTFI)) And .w_TIPKIT = .w_KITIMB
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo iniziale inesistente o incongruente o obsoleto o non articolo kit")
        endif
        this.w_CODARTIN = space(20)
        this.w_DESART = space(40)
        this.w_KITIMB = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODARTFI
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARTFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DISMBASE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DBCODICE like "+cp_ToStrODBC(trim(this.w_CODARTFI)+"%");

          i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDISKIT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DBCODICE',trim(this.w_CODARTFI))
          select DBCODICE,DBDESCRI,DBDISKIT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARTFI)==trim(_Link_.DBCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DBDESCRI like "+cp_ToStrODBC(trim(this.w_CODARTFI)+"%");

            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDISKIT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DBDESCRI like "+cp_ToStr(trim(this.w_CODARTFI)+"%");

            select DBCODICE,DBDESCRI,DBDISKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODARTFI) and !this.bDontReportError
            deferred_cp_zoom('DISMBASE','*','DBCODICE',cp_AbsName(oSource.parent,'oCODARTFI_1_13'),i_cWhere,'',"Elenco articoli",'GSAR_MAK.DISMBASE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDISKIT";
                     +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',oSource.xKey(1))
            select DBCODICE,DBDESCRI,DBDISKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARTFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDISKIT";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_CODARTFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_CODARTFI)
            select DBCODICE,DBDESCRI,DBDISKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARTFI = NVL(_Link_.DBCODICE,space(20))
      this.w_DESART1 = NVL(_Link_.DBDESCRI,space(40))
      this.w_KITIMB = NVL(_Link_.DBDISKIT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODARTFI = space(20)
      endif
      this.w_DESART1 = space(40)
      this.w_KITIMB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_CODARTFI>=.w_CODARTIN) or (empty(.w_CODARTIN))) And .w_TIPKIT = .w_KITIMB
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo finale inesistente o incongruente o obsoleto o non articolo kit")
        endif
        this.w_CODARTFI = space(20)
        this.w_DESART1 = space(40)
        this.w_KITIMB = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARTFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_CODIVA))
          select IVCODIVA,IVDESIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_CODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCODIVA_1_15'),i_cWhere,'GSAR_AIV',"Voci IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_IVDESCR = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_IVDESCR = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODIVA = space(5)
        this.w_IVDESCR = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPMERC
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPMERC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUPMERC)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUPMERC))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPMERC)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUPMERC) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUPMERC_1_17'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPMERC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUPMERC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUPMERC)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPMERC = NVL(_Link_.GMCODICE,space(5))
      this.w_GDESCRI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPMERC = space(5)
      endif
      this.w_GDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPMERC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATOMOGE
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATOMOGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATOMOGE)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATOMOGE))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATOMOGE)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATOMOGE) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATOMOGE_1_19'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATOMOGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATOMOGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATOMOGE)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATOMOGE = NVL(_Link_.OMCODICE,space(5))
      this.w_CTDESCRI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATOMOGE = space(5)
      endif
      this.w_CTDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATOMOGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMIGLIA
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMIGLIA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMIGLIA)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMIGLIA))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMIGLIA)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMIGLIA) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMIGLIA_1_21'),i_cWhere,'GSAR_AFA',"Famiglia articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMIGLIA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMIGLIA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMIGLIA)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMIGLIA = NVL(_Link_.FACODICE,space(5))
      this.w_FDESCRI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMIGLIA = space(5)
      endif
      this.w_FDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMIGLIA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLISTINO_1_1.value==this.w_LISTINO)
      this.oPgFrm.Page1.oPag.oLISTINO_1_1.value=this.w_LISTINO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_2.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_2.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDATLISIN_1_5.value==this.w_DATLISIN)
      this.oPgFrm.Page1.oPag.oDATLISIN_1_5.value=this.w_DATLISIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATLISFI_1_6.value==this.w_DATLISFI)
      this.oPgFrm.Page1.oPag.oDATLISFI_1_6.value=this.w_DATLISFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATVAL_1_7.value==this.w_DATVAL)
      this.oPgFrm.Page1.oPag.oDATVAL_1_7.value=this.w_DATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCREALIS_1_8.RadioValue()==this.w_CREALIS)
      this.oPgFrm.Page1.oPag.oCREALIS_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODARTIN_1_11.value==this.w_CODARTIN)
      this.oPgFrm.Page1.oPag.oCODARTIN_1_11.value=this.w_CODARTIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_12.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_12.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODARTFI_1_13.value==this.w_CODARTFI)
      this.oPgFrm.Page1.oPag.oCODARTFI_1_13.value=this.w_CODARTFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART1_1_14.value==this.w_DESART1)
      this.oPgFrm.Page1.oPag.oDESART1_1_14.value=this.w_DESART1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIVA_1_15.value==this.w_CODIVA)
      this.oPgFrm.Page1.oPag.oCODIVA_1_15.value=this.w_CODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oIVDESCR_1_16.value==this.w_IVDESCR)
      this.oPgFrm.Page1.oPag.oIVDESCR_1_16.value=this.w_IVDESCR
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUPMERC_1_17.value==this.w_GRUPMERC)
      this.oPgFrm.Page1.oPag.oGRUPMERC_1_17.value=this.w_GRUPMERC
    endif
    if not(this.oPgFrm.Page1.oPag.oGDESCRI_1_18.value==this.w_GDESCRI)
      this.oPgFrm.Page1.oPag.oGDESCRI_1_18.value=this.w_GDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATOMOGE_1_19.value==this.w_CATOMOGE)
      this.oPgFrm.Page1.oPag.oCATOMOGE_1_19.value=this.w_CATOMOGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCTDESCRI_1_20.value==this.w_CTDESCRI)
      this.oPgFrm.Page1.oPag.oCTDESCRI_1_20.value=this.w_CTDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMIGLIA_1_21.value==this.w_FAMIGLIA)
      this.oPgFrm.Page1.oPag.oFAMIGLIA_1_21.value=this.w_FAMIGLIA
    endif
    if not(this.oPgFrm.Page1.oPag.oFDESCRI_1_22.value==this.w_FDESCRI)
      this.oPgFrm.Page1.oPag.oFDESCRI_1_22.value=this.w_FDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT1_1_26.value==this.w_ARROT1)
      this.oPgFrm.Page1.oPag.oARROT1_1_26.value=this.w_ARROT1
    endif
    if not(this.oPgFrm.Page1.oPag.oVALORIN_1_27.value==this.w_VALORIN)
      this.oPgFrm.Page1.oPag.oVALORIN_1_27.value=this.w_VALORIN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT2_1_28.value==this.w_ARROT2)
      this.oPgFrm.Page1.oPag.oARROT2_1_28.value=this.w_ARROT2
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOR2IN_1_29.value==this.w_VALOR2IN)
      this.oPgFrm.Page1.oPag.oVALOR2IN_1_29.value=this.w_VALOR2IN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT3_1_30.value==this.w_ARROT3)
      this.oPgFrm.Page1.oPag.oARROT3_1_30.value=this.w_ARROT3
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOR3IN_1_31.value==this.w_VALOR3IN)
      this.oPgFrm.Page1.oPag.oVALOR3IN_1_31.value=this.w_VALOR3IN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT4_1_32.value==this.w_ARROT4)
      this.oPgFrm.Page1.oPag.oARROT4_1_32.value=this.w_ARROT4
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOFIN_1_39.value==this.w_VALOFIN)
      this.oPgFrm.Page1.oPag.oVALOFIN_1_39.value=this.w_VALOFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_LISTINO)) or not(.w_CODVAL=g_PERVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLISTINO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_LISTINO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino errato o inesistente")
          case   (empty(.w_DATVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATVAL_1_7.SetFocus()
            i_bnoObbl = !empty(.w_DATVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_CODARTFI)) OR  (.w_CODARTIN<=.w_CODARTFI)) And .w_TIPKIT = .w_KITIMB)  and not(empty(.w_CODARTIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODARTIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo iniziale inesistente o incongruente o obsoleto o non articolo kit")
          case   not(((.w_CODARTFI>=.w_CODARTIN) or (empty(.w_CODARTIN))) And .w_TIPKIT = .w_KITIMB)  and not(empty(.w_CODARTFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODARTFI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo finale inesistente o incongruente o obsoleto o non articolo kit")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODIVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODIVA_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VALOR2IN>.w_VALORIN)  and (.w_ARROT2<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVALOR2IN_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VALOR3IN>.w_VALORIN AND .w_VALOR3IN>.w_VALOR2IN)  and (.w_ARROT3<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVALOR3IN_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_klk
      if i_bRes=.t.
        do case
          case EMPTY(.w_LISTINO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = Ah_MsgFormat("Listino da aggiornare non selezionato")
          case EMPTY(.w_DATLISIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = Ah_MsgFormat("La data di inizio validit� del listino non � stata selezionata")
          case EMPTY(.w_DATLISFI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = Ah_MsgFormat("La data di fine validit� del listino non � stata selezionata")
          case .w_DATLISFI<.w_DATLISIN
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = Ah_MsgFormat("Data di fine validit� minore della data di inizio validit�")
        endcase
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_klkPag1 as StdContainer
  Width  = 573
  height = 426
  stdWidth  = 573
  stdheight = 426
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLISTINO_1_1 as StdField with uid="YNJAORDGVJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LISTINO", cQueryName = "LISTINO",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino errato o inesistente",;
    ToolTipText = "Codice di listino da generare/variare",;
    HelpContextID = 100665014,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=166, Top=10, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LISTINO"

  func oLISTINO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oLISTINO_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLISTINO_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLISTINO_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Anagrafica listini",'GSPS_MVD.LISTINI_VZM',this.parent.oContained
  endproc
  proc oLISTINO_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_LISTINO
     i_obj.ecpSave()
  endproc

  add object oDESLIS_1_2 as StdField with uid="VHVFQRRGPI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 184025654,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=230, Top=10, InputMask=replicate('X',40)

  add object oDATLISIN_1_5 as StdField with uid="PJPJOFXRWU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATLISIN", cQueryName = "DATLISIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit� listino",;
    HelpContextID = 184028804,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=166, Top=37

  add object oDATLISFI_1_6 as StdField with uid="NTEXDBNRRY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATLISFI", cQueryName = "DATLISFI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine validit� listino",;
    HelpContextID = 84406657,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=269, Top=37

  add object oDATVAL_1_7 as StdField with uid="WDCIHUWQQE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATVAL", cQueryName = "DATVAL",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di selezione del listino di riferimento componenti",;
    HelpContextID = 58854966,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=484, Top=37

  add object oCREALIS_1_8 as StdCheck with uid="OBOVIFZAZZ",rtseq=8,rtrep=.f.,left=166, top=65, caption="Solo listini esistenti",;
    ToolTipText = "Solo prezzi presenti in listino da aggiornare",;
    HelpContextID = 249811162,;
    cFormVar="w_CREALIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCREALIS_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCREALIS_1_8.GetRadio()
    this.Parent.oContained.w_CREALIS = this.RadioValue()
    return .t.
  endfunc

  func oCREALIS_1_8.SetRadio()
    this.Parent.oContained.w_CREALIS=trim(this.Parent.oContained.w_CREALIS)
    this.value = ;
      iif(this.Parent.oContained.w_CREALIS=='S',1,;
      0)
  endfunc

  add object oCODARTIN_1_11 as StdField with uid="FQICFIJNUH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODARTIN", cQueryName = "CODARTIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo iniziale inesistente o incongruente o obsoleto o non articolo kit",;
    ToolTipText = "Codice articolo/servizio di inizio selezione",;
    HelpContextID = 209460340,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=119, Top=109, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_CODARTIN"

  func oCODARTIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARTIN_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARTIN_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DISMBASE','*','DBCODICE',cp_AbsName(this.parent,'oCODARTIN_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco kit",'GSAR_MAK.DISMBASE_VZM',this.parent.oContained
  endproc

  add object oDESART_1_12 as StdField with uid="FFKDUJWTFU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209519158,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=271, Top=109, InputMask=replicate('X',40)

  add object oCODARTFI_1_13 as StdField with uid="JMOAIBQNJF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODARTFI", cQueryName = "CODARTFI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo finale inesistente o incongruente o obsoleto o non articolo kit",;
    ToolTipText = "Codice articolo/servizio di fine selezione",;
    HelpContextID = 58975121,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=119, Top=136, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_CODARTFI"

  func oCODARTFI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARTFI_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARTFI_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DISMBASE','*','DBCODICE',cp_AbsName(this.parent,'oCODARTFI_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco articoli",'GSAR_MAK.DISMBASE_VZM',this.parent.oContained
  endproc

  add object oDESART1_1_14 as StdField with uid="EUGHNFRNBK",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESART1", cQueryName = "DESART1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 58916298,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=271, Top=136, InputMask=replicate('X',40)

  add object oCODIVA_1_15 as StdField with uid="NEOEETMZSJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODIVA", cQueryName = "CODIVA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA articolo di selezione",;
    HelpContextID = 163847206,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=119, Top=163, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_CODIVA"

  func oCODIVA_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIVA_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIVA_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCODIVA_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'',this.parent.oContained
  endproc
  proc oCODIVA_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_CODIVA
     i_obj.ecpSave()
  endproc

  add object oIVDESCR_1_16 as StdField with uid="AXMSCIUMYU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_IVDESCR", cQueryName = "IVDESCR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 74439802,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=186, Top=163, InputMask=replicate('X',35)

  add object oGRUPMERC_1_17 as StdField with uid="QKUPGLZFUZ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_GRUPMERC", cQueryName = "GRUPMERC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Aggiorna i listini del gruppo merceologico selezionato",;
    HelpContextID = 46387287,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=119, Top=190, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUPMERC"

  func oGRUPMERC_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPMERC_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPMERC_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUPMERC_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oGRUPMERC_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUPMERC
     i_obj.ecpSave()
  endproc

  add object oGDESCRI_1_18 as StdField with uid="QMSLSUPIBF",rtseq=18,rtrep=.f.,;
    cFormVar = "w_GDESCRI", cQueryName = "GDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 161358182,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=186, Top=190, InputMask=replicate('X',35)

  add object oCATOMOGE_1_19 as StdField with uid="WFGZXVOVSI",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CATOMOGE", cQueryName = "CATOMOGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Aggiorna i listini della categoria omogenea selezionata",;
    HelpContextID = 147124629,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=119, Top=217, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATOMOGE"

  func oCATOMOGE_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATOMOGE_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATOMOGE_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATOMOGE_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCATOMOGE_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CATOMOGE
     i_obj.ecpSave()
  endproc

  add object oCTDESCRI_1_20 as StdField with uid="CCBBMUANCV",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CTDESCRI", cQueryName = "CTDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 74440337,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=186, Top=217, InputMask=replicate('X',35)

  add object oFAMIGLIA_1_21 as StdField with uid="EUWEFBLLOP",rtseq=21,rtrep=.f.,;
    cFormVar = "w_FAMIGLIA", cQueryName = "FAMIGLIA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Aggiorna i listini della famiglia selezionata",;
    HelpContextID = 64265879,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=119, Top=244, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMIGLIA"

  func oFAMIGLIA_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMIGLIA_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMIGLIA_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMIGLIA_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglia articoli",'',this.parent.oContained
  endproc
  proc oFAMIGLIA_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMIGLIA
     i_obj.ecpSave()
  endproc

  add object oFDESCRI_1_22 as StdField with uid="THMPCMLLGU",rtseq=22,rtrep=.f.,;
    cFormVar = "w_FDESCRI", cQueryName = "FDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 161358166,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=186, Top=244, InputMask=replicate('X',35)

  add object oARROT1_1_26 as StdField with uid="RVCKWZFISF",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ARROT1", cQueryName = "ARROT1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 162201350,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=9, Top=305, cSayPict="v_PU(13)", cGetPict="v_GU(13)"

  add object oVALORIN_1_27 as StdField with uid="MHNGXGXYBX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_VALORIN", cQueryName = "VALORIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25857878,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=119, Top=305, cSayPict="v_PU(20)", cGetPict="v_GU(20)"

  func oVALORIN_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARROT1<>0)
    endwith
   endif
  endfunc

  add object oARROT2_1_28 as StdField with uid="RHGJBSFYAZ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ARROT2", cQueryName = "ARROT2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 178978566,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=9, Top=331, cSayPict="v_PU(13)", cGetPict="v_GU(13)"

  func oARROT2_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0)
    endwith
   endif
  endfunc

  add object oVALOR2IN_1_29 as StdField with uid="NMSIQUJYRO",rtseq=26,rtrep=.f.,;
    cFormVar = "w_VALOR2IN", cQueryName = "VALOR2IN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 91582556,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=119, Top=331, cSayPict="v_PU(20)", cGetPict="v_GU(20)"

  func oVALOR2IN_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARROT2<>0)
    endwith
   endif
  endfunc

  func oVALOR2IN_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VALOR2IN>.w_VALORIN)
    endwith
    return bRes
  endfunc

  add object oARROT3_1_30 as StdField with uid="HXTPQSAAPP",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ARROT3", cQueryName = "ARROT3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 195755782,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=9, Top=357, cSayPict="v_PU(13)", cGetPict="v_GU(13)"

  func oARROT3_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0 AND .w_VALOR2IN<>0)
    endwith
   endif
  endfunc

  add object oVALOR3IN_1_31 as StdField with uid="GVJHJMJPXN",rtseq=28,rtrep=.f.,;
    cFormVar = "w_VALOR3IN", cQueryName = "VALOR3IN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 74805340,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=119, Top=357, cSayPict="v_PU(20)", cGetPict="v_GU(20)"

  func oVALOR3IN_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARROT3<>0)
    endwith
   endif
  endfunc

  func oVALOR3IN_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VALOR3IN>.w_VALORIN AND .w_VALOR3IN>.w_VALOR2IN)
    endwith
    return bRes
  endfunc

  add object oARROT4_1_32 as StdField with uid="RPOAXMKGRV",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ARROT4", cQueryName = "ARROT4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento per importi diversi dai precedenti (0 = nessun arrotondamento)",;
    HelpContextID = 212532998,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=9, Top=383, cSayPict="v_PU(13)", cGetPict="v_GU(13)"


  add object oBtn_1_34 as StdButton with uid="YSRFFHTDMW",left=466, top=370, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la generazione del listino prezzi kit";
    , HelpContextID = 51837158;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        GSAR_BKL(this.Parent.oContained,"B")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_35 as StdButton with uid="OJYROTBZFC",left=518, top=370, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 59125830;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oVALOFIN_1_39 as StdField with uid="ZZMRLWXAIF",rtseq=30,rtrep=.f.,;
    cFormVar = "w_VALOFIN", cQueryName = "VALOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 13274966,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=256, Top=384, cSayPict="v_PU(20)", cGetPict="v_GU(20)"

  add object oStr_1_23 as StdString with uid="GPCTDMFHWZ",Visible=.t., Left=4, Top=10,;
    Alignment=1, Width=158, Height=18,;
    Caption="Listino da aggiornare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="RTNQCLHVFI",Visible=.t., Left=88, Top=37,;
    Alignment=1, Width=74, Height=15,;
    Caption="Valido dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="EWACOKHGOF",Visible=.t., Left=247, Top=37,;
    Alignment=1, Width=20, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="JEFECLEULP",Visible=.t., Left=6, Top=275,;
    Alignment=2, Width=104, Height=18,;
    Caption="Arrotondamenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="MHEWZFMOQE",Visible=.t., Left=121, Top=276,;
    Alignment=0, Width=146, Height=18,;
    Caption="Per importi fino a"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="HFEZTPBADH",Visible=.t., Left=110, Top=385,;
    Alignment=1, Width=143, Height=15,;
    Caption="Per importi superiori a:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="BGZKWAZUBQ",Visible=.t., Left=9, Top=109,;
    Alignment=1, Width=108, Height=18,;
    Caption="Da kit:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="EOZTSDPVGP",Visible=.t., Left=9, Top=136,;
    Alignment=1, Width=108, Height=18,;
    Caption="Ad kit:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="FLZYVSFGSY",Visible=.t., Left=9, Top=164,;
    Alignment=1, Width=108, Height=15,;
    Caption="Cod. IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="CMKEUYLNHL",Visible=.t., Left=9, Top=190,;
    Alignment=1, Width=108, Height=15,;
    Caption="Gruppo merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="YHXJLZQZSI",Visible=.t., Left=9, Top=217,;
    Alignment=1, Width=108, Height=15,;
    Caption="Cat.omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="GOIEYXNRYG",Visible=.t., Left=9, Top=244,;
    Alignment=1, Width=108, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="PBFNYFGCDO",Visible=.t., Left=5, Top=82,;
    Alignment=0, Width=146, Height=18,;
    Caption="Selezioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="AYKMNKUBBM",Visible=.t., Left=394, Top=37,;
    Alignment=1, Width=88, Height=18,;
    Caption="Listino valido al:"  ;
  , bGlobalFont=.t.

  add object oBox_1_36 as StdBox with uid="LFYMCQCNFX",left=2, top=295, width=565,height=2

  add object oBox_1_46 as StdBox with uid="ADKUWJCUAJ",left=2, top=101, width=565,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_klk','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
