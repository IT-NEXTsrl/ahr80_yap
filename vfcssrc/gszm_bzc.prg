* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bzc                                                        *
*              Tasto destro per codici di ricerca                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-13                                                      *
* Last revis.: 2005-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bzc",oParentObject)
return(i_retval)

define class tgszm_bzc as StdBatch
  * --- Local variables
  w_CODICE = space(20)
  w_PROG = space(8)
  w_OKGEST = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apertura Codici di Ricerca
    this.w_OKGEST = .F.
    this.w_CODICE = g_oMenu.getValue("CACODICE")
    if Empty(this.w_CODICE)
      if Type("g_oMenu.oParentObject.w_CACODICE") <>"U"
        this.w_CODICE = g_oMenu.oParentObject.w_CACODICE
        this.w_OKGEST = .T.
      endif
    else
      this.w_OKGEST = .T.
    endif
    if this.w_OKGEST
      this.w_PROG = "GSMA_ACA"
      OpenGest(iif(.T.,g_oMenu.cBatchType,"S"),this.w_PROG,"CACODICE",this.w_CODICE)
    else
      this.w_CODICE = g_oMenu.getValue("ARCODART")
      if Empty(this.w_CODICE)
        if Type("g_oMenu.oParentObject.w_ARCODART") <>"U"
          this.w_CODICE = g_oMenu.oParentObject.w_ARCODART
        endif
      endif
      this.w_PROG = "GSMA_AAR"
      OpenGest(iif(.T.,g_oMenu.cBatchType,"S"),this.w_PROG,"ARCODART",this.w_CODICE)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
