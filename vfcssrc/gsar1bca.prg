* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1bca                                                        *
*              Inserimento attributo                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-22                                                      *
* Last revis.: 2012-10-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1bca",oParentObject,m.pParam)
return(i_retval)

define class tgsar1bca as StdBatch
  * --- Local variables
  pParam = space(5)
  w_ASVALATT = space(10)
  w_MASSIMO = 0
  w_MASSIMOR = 0
  w_CODFAM = space(10)
  w_NOVAL = .f.
  w_FATIPCAR = space(10)
  w_FATIPDAT = space(10)
  w_FATIPNUM = space(10)
  w_CAR = 0
  w_MESS = space(100)
  w_ZOOM1 = space(10)
  w_RECO = 0
  w_RECPOS = 0
  w_OK = .f.
  w_GSAR_MRA = .NULL.
  w_FRDIMENS = 0
  w_FRNUMDEC = 0
  w_LINPUTA = space(1)
  w_FR_TABLE = space(30)
  w_FR__ZOOM = space(254)
  w_FR_CAMPO = space(50)
  * --- WorkFile variables
  FAZDATTR_idx=0
  ASS_ATTR_idx=0
  FAM_ATTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Inserimento di un valore non presente in archivio
    * --- --DA GSAR_MRA
    this.w_GSAR_MRA = this.OparentObject
    do case
      case this.pParam="VIS"
        this.w_CODFAM = this.oParentObject.w_ASCODFAM
        this.w_ZOOM1 = this.w_GSAR_MRA.w_ATTRIBUTI
        NC = this.w_ZOOM1.cCursor
        if reccount(NC)>0
          select (NC) 
 delete FOR CPROWNUM=this.oParentObject.w_RIGA-1
          this.w_GSAR_MRA.w_ATTRIBUTI.Refresh()
          select (NC) 
 GO TOP
        endif
      case this.pParam="OBB"
        * --- --Inserimento Dati Obbligatori
        * --- --Cancellazione Transitorio
        this.w_OK = ah_YesNo("Attenzione%0gli attributi inseriti verranno sovrascritti%0si desidera proseguire?")
        if this.w_OK
          this.w_GSAR_MRA.mCalc(.T.)     
          do vq_exec with "..\exe\query\GSAR_MRA", this, "_Gruppi_"
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if reccount("_Gruppi_")>0
            this.w_GSAR_MRA.MarkPos()     
            this.w_GSAR_MRA.firstrow()     
            Delete All
            Select _Gruppi_ 
 go top
            scan
            scatter memvar
            this.w_GSAR_MRA.AddRow()     
            this.w_GSAR_MRA.w_ASMODFAM = NVL(this.oParentObject.w_GEST, SPACE(10))
            this.w_GSAR_MRA.w_ASCODATT = NVL(M.GRCODICE, SPACE(8)) 
            this.w_GSAR_MRA.w_CODGRU = NVL(M.GRCODICE, SPACE(8)) 
            this.w_GSAR_MRA.w_CODATT = NVL(M.GRCODICE, SPACE(8)) 
            this.w_GSAR_MRA.w_ASCODFAM = NVL(M.FRCODICE,SPACE(8)) 
            * --- Read from FAM_ATTR
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.FAM_ATTR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.FAM_ATTR_idx,2],.t.,this.FAM_ATTR_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "FRDIMENS,FRNUMDEC,FRINPUTA,FR_TABLE,FR__ZOOM,FR_CAMPO"+;
                " from "+i_cTable+" FAM_ATTR where ";
                    +"FRCODICE = "+cp_ToStrODBC(M.FRCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                FRDIMENS,FRNUMDEC,FRINPUTA,FR_TABLE,FR__ZOOM,FR_CAMPO;
                from (i_cTable) where;
                    FRCODICE = M.FRCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FRDIMENS = NVL(cp_ToDate(_read_.FRDIMENS),cp_NullValue(_read_.FRDIMENS))
              this.w_FRNUMDEC = NVL(cp_ToDate(_read_.FRNUMDEC),cp_NullValue(_read_.FRNUMDEC))
              this.w_LINPUTA = NVL(cp_ToDate(_read_.FRINPUTA),cp_NullValue(_read_.FRINPUTA))
              this.w_FR_TABLE = NVL(cp_ToDate(_read_.FR_TABLE),cp_NullValue(_read_.FR_TABLE))
              this.w_FR__ZOOM = NVL(cp_ToDate(_read_.FR__ZOOM),cp_NullValue(_read_.FR__ZOOM))
              this.w_FR_CAMPO = NVL(cp_ToDate(_read_.FR_CAMPO),cp_NullValue(_read_.FR_CAMPO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_GSAR_MRA.w_FRDIMENS = this.w_FRDIMENS
            this.w_GSAR_MRA.w_FRNUMDEC = this.w_FRNUMDEC
            this.w_GSAR_MRA.w_INPUTA = this.w_LINPUTA
            this.w_GSAR_MRA.w_CODFAM = NVL(M.FRCODICE,SPACE(8)) 
            this.w_GSAR_MRA.w_TIPO = NVL(M.FRTIPOLO, SPACE(1))
            this.w_GSAR_MRA.w_INPUTA = NVL(M.FRINPUTA,SPACE(1))
            this.w_GSAR_MRA.w_DEC = NVL(M.FRNUMDEC,0)
            this.w_GSAR_MRA.w_DIME = NVL(M.FRDIMENS,0)
            this.w_GSAR_MRA.w_ASVALATT = SPACE(20)
            this.w_GSAR_MRA.w_TABLE = NVL(M.FR_TABLE,SPACE(30))
            this.w_GSAR_MRA.w_CAMPO = NVL(M.FR_CAMPO,SPACE(50))
            this.w_GSAR_MRA.w_ZOOM = NVL(M.FR__ZOOM,SPACE(254))
            this.w_GSAR_MRA.w_ZOOMOZ = NVL(M.FRZOOMOZ,SPACE(20))
            this.w_GSAR_MRA.w_ATTRIBUTO = NVL(M.FRCODFAM,SPACE(10))
            this.w_GSAR_MRA.w_ATTCOL = NVL(M.FRCAMCOL,SPACE(30))
            this.w_GSAR_MRA.w_FR_TABLE = this.w_FR_TABLE
            this.w_GSAR_MRA.w_FR_CAMPO = this.w_FR_CAMPO
            this.w_GSAR_MRA.w_FR__ZOOM = this.w_FR__ZOOM
            this.w_GSAR_MRA.SaveRow()     
            endscan
            this.w_GSAR_MRA.FirstRow()     
            this.w_GSAR_MRA.RePos()     
            this.w_GSAR_MRA.Refresh()     
          endif
          if used("_Gruppi_")
            USE IN _Gruppi_
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='FAZDATTR'
    this.cWorkTables[2]='ASS_ATTR'
    this.cWorkTables[3]='FAM_ATTR'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
