* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bcv                                                        *
*              Controlli cancellazione valute                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-09-20                                                      *
* Last revis.: 2011-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bcv",oParentObject)
return(i_retval)

define class tgsar_bcv as StdBatch
  * --- Local variables
  w_TESVAL = .f.
  w_CODVAL = space(5)
  w_CODAZI = space(5)
  w_MESS = space(100)
  w_CODVAL = space(3)
  * --- WorkFile variables
  CONTROPA_idx=0
  AZIENDA_idx=0
  ESERCIZI_idx=0
  VALUTE_idx=0
  BAN_CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo cancellazione valuta da GSAR_AVL
    * --- Controlli per la cancellazione
    this.w_CODAZI = i_CODAZI
    this.w_TESVAL = .T.
    * --- Select from AZIENDA
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" AZIENDA ";
          +" where AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI)+" And ( AZVALEUR = "+cp_ToStrODBC(this.oParentObject.w_VACODVAL)+" Or AZVALLIR = "+cp_ToStrODBC(this.oParentObject.w_VACODVAL)+")";
           ,"_Curs_AZIENDA")
    else
      select * from (i_cTable);
       where AZCODAZI = this.w_CODAZI And ( AZVALEUR = this.oParentObject.w_VACODVAL Or AZVALLIR = this.oParentObject.w_VACODVAL);
        into cursor _Curs_AZIENDA
    endif
    if used('_Curs_AZIENDA')
      select _Curs_AZIENDA
      locate for 1=1
      do while not(eof())
      this.w_MESS = ah_MsgFormat("Impossibile cancellare, codice valuta presente nei dati azienda")
      this.w_TESVAL = .F.
        select _Curs_AZIENDA
        continue
      enddo
      use
    endif
    if this.w_TESVAL
      * --- Select from BAN_CONTI
      i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2],.t.,this.BAN_CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CCCODCON  from "+i_cTable+" BAN_CONTI ";
            +" where CCCODVAL="+cp_ToStrODBC(this.oParentObject.w_VACODVAL)+"";
             ,"_Curs_BAN_CONTI")
      else
        select CCCODCON from (i_cTable);
         where CCCODVAL=this.oParentObject.w_VACODVAL;
          into cursor _Curs_BAN_CONTI
      endif
      if used('_Curs_BAN_CONTI')
        select _Curs_BAN_CONTI
        locate for 1=1
        do while not(eof())
        this.w_MESS = ah_MsgFormat("Impossibile cancellare, codice valuta utilizzato nel conto corrente associato all'anagrafica")
        this.w_TESVAL = .F.
        EXIT
          select _Curs_BAN_CONTI
          continue
        enddo
        use
      endif
    endif
    if this.w_TESVAL
      * --- Select from ESERCIZI
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ESERCIZI ";
            +" where ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI)+" And ESVALNAZ = "+cp_ToStrODBC(this.oParentObject.w_VACODVAL)+"";
             ,"_Curs_ESERCIZI")
      else
        select * from (i_cTable);
         where ESCODAZI = this.w_CODAZI And ESVALNAZ = this.oParentObject.w_VACODVAL;
          into cursor _Curs_ESERCIZI
      endif
      if used('_Curs_ESERCIZI')
        select _Curs_ESERCIZI
        locate for 1=1
        do while not(eof())
        this.w_MESS = ah_MsgFormat("Impossibile cancellare, codice valuta utilizzato in esercizio: %1", Alltrim(_Curs_ESERCIZI.ESCODESE))
        this.w_TESVAL = .F.
        EXIT
          select _Curs_ESERCIZI
          continue
        enddo
        use
      endif
    endif
    if this.w_TESVAL
      * --- Select from VALUTE
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" VALUTE ";
            +" where VAUNIVAL="+cp_ToStrODBC(this.oParentObject.w_VACODVAL)+" AND VACODVAL<>"+cp_ToStrODBC(this.oParentObject.w_VACODVAL)+"";
             ,"_Curs_VALUTE")
      else
        select * from (i_cTable);
         where VAUNIVAL=this.oParentObject.w_VACODVAL AND VACODVAL<>this.oParentObject.w_VACODVAL;
          into cursor _Curs_VALUTE
      endif
      if used('_Curs_VALUTE')
        select _Curs_VALUTE
        locate for 1=1
        do while not(eof())
        this.w_MESS = ah_msgformat("Impossibile cancellare, codice presente in altre valute ")
        this.w_TESVAL = .F.
        exit
          select _Curs_VALUTE
          continue
        enddo
        use
      endif
    endif
    if Not this.w_TESVAL
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='BAN_CONTI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_AZIENDA')
      use in _Curs_AZIENDA
    endif
    if used('_Curs_BAN_CONTI')
      use in _Curs_BAN_CONTI
    endif
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_VALUTE')
      use in _Curs_VALUTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
