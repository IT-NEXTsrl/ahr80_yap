* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mec                                                        *
*              Evasione componenti                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_110]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-25                                                      *
* Last revis.: 2014-07-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mec")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mec")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mec")
  return

* --- Class definition
define class tgsar_mec as StdPCForm
  Width  = 426
  Height = 346
  Top    = 55
  Left   = 119
  cComment = "Evasione componenti"
  cPrg = "gsar_mec"
  HelpContextID=63534953
  add object cnt as tcgsar_mec
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mec as PCContext
  w_MVSERIAL = space(10)
  w_MVROWNUM = 0
  w_CPROWORD = 0
  w_MVCODICE = space(20)
  w_MVTIPRIG = space(1)
  w_MVCODART = space(20)
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_OPERA3 = space(1)
  w_DTOBS1 = space(8)
  w_UNMIS1 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_UNMIS2 = space(3)
  w_MVUNIMIS = space(3)
  w_FLFRAZ = space(1)
  w_MVQTAMOV = 0
  w_OBTEST = space(8)
  w_CAUPFI = space(10)
  w_CAUCOD = space(10)
  w_DESART = space(40)
  w_MVQTAUM1 = 0
  w_MVTIPREC = space(1)
  w_MVCODCAU = space(5)
  w_MVDATEVA = space(8)
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  w_MVQTASAL = 0
  w_MVCODMAG = space(5)
  w_MVKEYSAL = space(20)
  w_MVFLORDI = space(1)
  w_MVFLIMPE = space(1)
  w_MVFLRISE = space(1)
  w_MVFLERIF = space(1)
  w_MVDESSUP = space(10)
  w_MVMAGORI = space(5)
  w_MVMATORI = space(5)
  w_TDATEVA = space(8)
  w_FLUSEP = space(1)
  w_NOFRAZ1 = space(1)
  w_MODUM2 = space(1)
  w_MVCODCOM = space(15)
  w_MVCODATT = space(15)
  w_MVCODCEN = space(15)
  proc Save(i_oFrom)
    this.w_MVSERIAL = i_oFrom.w_MVSERIAL
    this.w_MVROWNUM = i_oFrom.w_MVROWNUM
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_MVCODICE = i_oFrom.w_MVCODICE
    this.w_MVTIPRIG = i_oFrom.w_MVTIPRIG
    this.w_MVCODART = i_oFrom.w_MVCODART
    this.w_UNMIS3 = i_oFrom.w_UNMIS3
    this.w_MOLTI3 = i_oFrom.w_MOLTI3
    this.w_OPERA3 = i_oFrom.w_OPERA3
    this.w_DTOBS1 = i_oFrom.w_DTOBS1
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_OPERAT = i_oFrom.w_OPERAT
    this.w_MOLTIP = i_oFrom.w_MOLTIP
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_MVUNIMIS = i_oFrom.w_MVUNIMIS
    this.w_FLFRAZ = i_oFrom.w_FLFRAZ
    this.w_MVQTAMOV = i_oFrom.w_MVQTAMOV
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_CAUPFI = i_oFrom.w_CAUPFI
    this.w_CAUCOD = i_oFrom.w_CAUCOD
    this.w_DESART = i_oFrom.w_DESART
    this.w_MVQTAUM1 = i_oFrom.w_MVQTAUM1
    this.w_MVTIPREC = i_oFrom.w_MVTIPREC
    this.w_MVCODCAU = i_oFrom.w_MVCODCAU
    this.w_MVDATEVA = i_oFrom.w_MVDATEVA
    this.w_MVSERRIF = i_oFrom.w_MVSERRIF
    this.w_MVROWRIF = i_oFrom.w_MVROWRIF
    this.w_MVQTASAL = i_oFrom.w_MVQTASAL
    this.w_MVCODMAG = i_oFrom.w_MVCODMAG
    this.w_MVKEYSAL = i_oFrom.w_MVKEYSAL
    this.w_MVFLORDI = i_oFrom.w_MVFLORDI
    this.w_MVFLIMPE = i_oFrom.w_MVFLIMPE
    this.w_MVFLRISE = i_oFrom.w_MVFLRISE
    this.w_MVFLERIF = i_oFrom.w_MVFLERIF
    this.w_MVDESSUP = i_oFrom.w_MVDESSUP
    this.w_MVMAGORI = i_oFrom.w_MVMAGORI
    this.w_MVMATORI = i_oFrom.w_MVMATORI
    this.w_TDATEVA = i_oFrom.w_TDATEVA
    this.w_FLUSEP = i_oFrom.w_FLUSEP
    this.w_NOFRAZ1 = i_oFrom.w_NOFRAZ1
    this.w_MODUM2 = i_oFrom.w_MODUM2
    this.w_MVCODCOM = i_oFrom.w_MVCODCOM
    this.w_MVCODATT = i_oFrom.w_MVCODATT
    this.w_MVCODCEN = i_oFrom.w_MVCODCEN
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MVSERIAL = this.w_MVSERIAL
    i_oTo.w_MVROWNUM = this.w_MVROWNUM
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_MVCODICE = this.w_MVCODICE
    i_oTo.w_MVTIPRIG = this.w_MVTIPRIG
    i_oTo.w_MVCODART = this.w_MVCODART
    i_oTo.w_UNMIS3 = this.w_UNMIS3
    i_oTo.w_MOLTI3 = this.w_MOLTI3
    i_oTo.w_OPERA3 = this.w_OPERA3
    i_oTo.w_DTOBS1 = this.w_DTOBS1
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_OPERAT = this.w_OPERAT
    i_oTo.w_MOLTIP = this.w_MOLTIP
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_MVUNIMIS = this.w_MVUNIMIS
    i_oTo.w_FLFRAZ = this.w_FLFRAZ
    i_oTo.w_MVQTAMOV = this.w_MVQTAMOV
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_CAUPFI = this.w_CAUPFI
    i_oTo.w_CAUCOD = this.w_CAUCOD
    i_oTo.w_DESART = this.w_DESART
    i_oTo.w_MVQTAUM1 = this.w_MVQTAUM1
    i_oTo.w_MVTIPREC = this.w_MVTIPREC
    i_oTo.w_MVCODCAU = this.w_MVCODCAU
    i_oTo.w_MVDATEVA = this.w_MVDATEVA
    i_oTo.w_MVSERRIF = this.w_MVSERRIF
    i_oTo.w_MVROWRIF = this.w_MVROWRIF
    i_oTo.w_MVQTASAL = this.w_MVQTASAL
    i_oTo.w_MVCODMAG = this.w_MVCODMAG
    i_oTo.w_MVKEYSAL = this.w_MVKEYSAL
    i_oTo.w_MVFLORDI = this.w_MVFLORDI
    i_oTo.w_MVFLIMPE = this.w_MVFLIMPE
    i_oTo.w_MVFLRISE = this.w_MVFLRISE
    i_oTo.w_MVFLERIF = this.w_MVFLERIF
    i_oTo.w_MVDESSUP = this.w_MVDESSUP
    i_oTo.w_MVMAGORI = this.w_MVMAGORI
    i_oTo.w_MVMATORI = this.w_MVMATORI
    i_oTo.w_TDATEVA = this.w_TDATEVA
    i_oTo.w_FLUSEP = this.w_FLUSEP
    i_oTo.w_NOFRAZ1 = this.w_NOFRAZ1
    i_oTo.w_MODUM2 = this.w_MODUM2
    i_oTo.w_MVCODCOM = this.w_MVCODCOM
    i_oTo.w_MVCODATT = this.w_MVCODATT
    i_oTo.w_MVCODCEN = this.w_MVCODCEN
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mec as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 426
  Height = 346
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-21"
  HelpContextID=63534953
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=44

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  EVA_COMP_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  TIP_DOCU_IDX = 0
  cFile = "EVA_COMP"
  cKeySelect = "MVSERIAL,MVROWNUM"
  cKeyWhere  = "MVSERIAL=this.w_MVSERIAL and MVROWNUM=this.w_MVROWNUM"
  cKeyDetail  = "MVSERIAL=this.w_MVSERIAL and MVROWNUM=this.w_MVROWNUM"
  cKeyWhereODBC = '"MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)';
      +'+" and MVROWNUM="+cp_ToStrODBC(this.w_MVROWNUM)';

  cKeyDetailWhereODBC = '"MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)';
      +'+" and MVROWNUM="+cp_ToStrODBC(this.w_MVROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"EVA_COMP.MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)';
      +'+" and EVA_COMP.MVROWNUM="+cp_ToStrODBC(this.w_MVROWNUM)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'EVA_COMP.CPROWORD '
  cPrg = "gsar_mec"
  cComment = "Evasione componenti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 15
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MVSERIAL = space(10)
  w_MVROWNUM = 0
  w_CPROWORD = 0
  w_MVCODICE = space(20)
  o_MVCODICE = space(20)
  w_MVTIPRIG = space(1)
  w_MVCODART = space(20)
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_OPERA3 = space(1)
  w_DTOBS1 = ctod('  /  /  ')
  w_UNMIS1 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_UNMIS2 = space(3)
  w_MVUNIMIS = space(3)
  o_MVUNIMIS = space(3)
  w_FLFRAZ = space(1)
  w_MVQTAMOV = 0
  o_MVQTAMOV = 0
  w_OBTEST = ctod('  /  /  ')
  w_CAUPFI = space(10)
  w_CAUCOD = space(10)
  w_DESART = space(40)
  w_MVQTAUM1 = 0
  w_MVTIPREC = space(1)
  w_MVCODCAU = space(5)
  w_MVDATEVA = ctod('  /  /  ')
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  w_MVQTASAL = 0
  w_MVCODMAG = space(5)
  w_MVKEYSAL = space(20)
  w_MVFLORDI = space(1)
  w_MVFLIMPE = space(1)
  w_MVFLRISE = space(1)
  w_MVFLERIF = space(1)
  w_MVDESSUP = space(0)
  w_MVMAGORI = space(5)
  w_MVMATORI = space(5)
  w_TDATEVA = ctod('  /  /  ')
  w_FLUSEP = space(1)
  w_NOFRAZ1 = space(1)
  w_MODUM2 = space(1)
  w_MVCODCOM = space(15)
  w_MVCODATT = space(15)
  w_MVCODCEN = space(15)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mecPag1","gsar_mec",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='TIP_DOCU'
    this.cWorkTables[5]='EVA_COMP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.EVA_COMP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.EVA_COMP_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mec'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_13_joined
    link_2_13_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from EVA_COMP where MVSERIAL=KeySet.MVSERIAL
    *                            and MVROWNUM=KeySet.MVROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsar_mec
      i_cOrder = 'order by MVTIPREC, CPROWORD '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.EVA_COMP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.EVA_COMP_IDX,2],this.bLoadRecFilter,this.EVA_COMP_IDX,"gsar_mec")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('EVA_COMP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "EVA_COMP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' EVA_COMP '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MVSERIAL',this.w_MVSERIAL  ,'MVROWNUM',this.w_MVROWNUM  )
      select * from (i_cTable) EVA_COMP where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = ctod("  /  /  ")
        .w_TDATEVA = ctod("  /  /  ")
        .w_MVSERIAL = NVL(MVSERIAL,space(10))
        .w_MVROWNUM = NVL(MVROWNUM,0)
        .w_CAUPFI = this.oparentobject .w_CAUPFI
        .w_CAUCOD = this.oparentobject .w_CAUCOD
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'EVA_COMP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_UNMIS3 = space(3)
          .w_MOLTI3 = 0
          .w_OPERA3 = space(1)
          .w_DTOBS1 = ctod("  /  /  ")
          .w_UNMIS1 = space(3)
          .w_OPERAT = space(1)
          .w_MOLTIP = 0
          .w_UNMIS2 = space(3)
          .w_FLFRAZ = space(1)
          .w_DESART = space(40)
          .w_FLUSEP = space(1)
          .w_NOFRAZ1 = space(1)
          .w_MODUM2 = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MVCODICE = NVL(MVCODICE,space(20))
          if link_2_2_joined
            this.w_MVCODICE = NVL(CACODICE202,NVL(this.w_MVCODICE,space(20)))
            this.w_DESART = NVL(CADESART202,space(40))
            this.w_MVCODART = NVL(CACODART202,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS202,space(3))
            this.w_OPERA3 = NVL(CAOPERAT202,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP202,0)
            this.w_DTOBS1 = NVL(cp_ToDate(CADTOBSO202),ctod("  /  /  "))
            this.w_MVTIPRIG = NVL(CA__TIPO202,space(1))
          else
          .link_2_2('Load')
          endif
          .w_MVTIPRIG = NVL(MVTIPRIG,space(1))
          .w_MVCODART = NVL(MVCODART,space(20))
          if link_2_4_joined
            this.w_MVCODART = NVL(ARCODART204,NVL(this.w_MVCODART,space(20)))
            this.w_MOLTIP = NVL(ARMOLTIP204,0)
            this.w_UNMIS2 = NVL(ARUNMIS2204,space(3))
            this.w_UNMIS1 = NVL(ARUNMIS1204,space(3))
            this.w_OPERAT = NVL(AROPERAT204,space(1))
            this.w_FLUSEP = NVL(ARFLUSEP204,space(1))
          else
          .link_2_4('Load')
          endif
          .link_2_9('Load')
          .w_MVUNIMIS = NVL(MVUNIMIS,space(3))
          if link_2_13_joined
            this.w_MVUNIMIS = NVL(UMCODICE213,NVL(this.w_MVUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ213,space(1))
          else
          .link_2_13('Load')
          endif
          .w_MVQTAMOV = NVL(MVQTAMOV,0)
          .w_MVQTAUM1 = NVL(MVQTAUM1,0)
          .w_MVTIPREC = NVL(MVTIPREC,space(1))
          .w_MVCODCAU = NVL(MVCODCAU,space(5))
          .w_MVDATEVA = NVL(cp_ToDate(MVDATEVA),ctod("  /  /  "))
          .w_MVSERRIF = NVL(MVSERRIF,space(10))
          .w_MVROWRIF = NVL(MVROWRIF,0)
          .w_MVQTASAL = NVL(MVQTASAL,0)
          .w_MVCODMAG = NVL(MVCODMAG,space(5))
          .w_MVKEYSAL = NVL(MVKEYSAL,space(20))
          .w_MVFLORDI = NVL(MVFLORDI,space(1))
          .w_MVFLIMPE = NVL(MVFLIMPE,space(1))
          .w_MVFLRISE = NVL(MVFLRISE,space(1))
          .w_MVFLERIF = NVL(MVFLERIF,space(1))
          .w_MVDESSUP = NVL(MVDESSUP,space(0))
          .w_MVMAGORI = NVL(MVMAGORI,space(5))
          .w_MVMATORI = NVL(MVMATORI,space(5))
          .w_MVCODCOM = NVL(MVCODCOM,space(15))
          .w_MVCODATT = NVL(MVCODATT,space(15))
          .w_MVCODCEN = NVL(MVCODCEN,space(15))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CAUPFI = this.oparentobject .w_CAUPFI
        .w_CAUCOD = this.oparentobject .w_CAUCOD
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_mec
    * --- Valorizzo w_TDATEVA
    This.w_TDATEVA = this.w_MVDATEVA
    
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MVSERIAL=space(10)
      .w_MVROWNUM=0
      .w_CPROWORD=10
      .w_MVCODICE=space(20)
      .w_MVTIPRIG=space(1)
      .w_MVCODART=space(20)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_OPERA3=space(1)
      .w_DTOBS1=ctod("  /  /  ")
      .w_UNMIS1=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_UNMIS2=space(3)
      .w_MVUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_MVQTAMOV=0
      .w_OBTEST=ctod("  /  /  ")
      .w_CAUPFI=space(10)
      .w_CAUCOD=space(10)
      .w_DESART=space(40)
      .w_MVQTAUM1=0
      .w_MVTIPREC=space(1)
      .w_MVCODCAU=space(5)
      .w_MVDATEVA=ctod("  /  /  ")
      .w_MVSERRIF=space(10)
      .w_MVROWRIF=0
      .w_MVQTASAL=0
      .w_MVCODMAG=space(5)
      .w_MVKEYSAL=space(20)
      .w_MVFLORDI=space(1)
      .w_MVFLIMPE=space(1)
      .w_MVFLRISE=space(1)
      .w_MVFLERIF=space(1)
      .w_MVDESSUP=space(0)
      .w_MVMAGORI=space(5)
      .w_MVMATORI=space(5)
      .w_TDATEVA=ctod("  /  /  ")
      .w_FLUSEP=space(1)
      .w_NOFRAZ1=space(1)
      .w_MODUM2=space(1)
      .w_MVCODCOM=space(15)
      .w_MVCODATT=space(15)
      .w_MVCODCEN=space(15)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_MVCODICE))
         .link_2_2('Full')
        endif
        .w_MVTIPRIG = 'R'
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_MVCODART))
         .link_2_4('Full')
        endif
        .DoRTCalc(7,11,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_9('Full')
        endif
        .DoRTCalc(12,14,.f.)
        .w_MVUNIMIS = .w_UNMIS1
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_MVUNIMIS))
         .link_2_13('Full')
        endif
        .DoRTCalc(16,18,.f.)
        .w_CAUPFI = this.oparentobject .w_CAUPFI
        .w_CAUCOD = this.oparentobject .w_CAUCOD
        .DoRTCalc(21,21,.f.)
        .w_MVQTAUM1 = CALQTAADV(.w_MVQTAMOV,.w_MVUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,this, "MVQTAMOV")
        .w_MVTIPREC = IIF(EMPTY(.w_CAUCOD), '1', '2')
        .w_MVCODCAU = IIF(.w_MVTIPRIG='1', .w_CAUPFI, .w_CAUCOD)
        .w_MVDATEVA = .w_TDATEVA
        .DoRTCalc(26,41,.f.)
        .w_MVCODCOM = this.oParentObject.oParentObject.oParentObject .w_MVCODCOM
        .w_MVCODATT = this.oParentObject.oParentObject.oParentObject .w_MVCODATT
        .w_MVCODCEN = this.oParentObject.oParentObject.oParentObject .w_MVCODCEN
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'EVA_COMP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'EVA_COMP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.EVA_COMP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MVSERIAL,"MVSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MVROWNUM,"MVROWNUM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_MVCODICE C(20);
      ,t_MVUNIMIS C(3);
      ,t_MVQTAMOV N(12,3);
      ,t_DESART C(40);
      ,t_MVTIPREC N(3);
      ,CPROWNUM N(10);
      ,t_MVTIPRIG C(1);
      ,t_MVCODART C(20);
      ,t_UNMIS3 C(3);
      ,t_MOLTI3 N(10,4);
      ,t_OPERA3 C(1);
      ,t_DTOBS1 D(8);
      ,t_UNMIS1 C(3);
      ,t_OPERAT C(1);
      ,t_MOLTIP N(10,4);
      ,t_UNMIS2 C(3);
      ,t_FLFRAZ C(1);
      ,t_MVQTAUM1 N(12,3);
      ,t_MVCODCAU C(5);
      ,t_MVDATEVA D(8);
      ,t_MVSERRIF C(10);
      ,t_MVROWRIF N(4);
      ,t_MVQTASAL N(12,3);
      ,t_MVCODMAG C(5);
      ,t_MVKEYSAL C(20);
      ,t_MVFLORDI C(1);
      ,t_MVFLIMPE C(1);
      ,t_MVFLRISE C(1);
      ,t_MVFLERIF C(1);
      ,t_MVDESSUP M(10);
      ,t_MVMAGORI C(5);
      ,t_MVMATORI C(5);
      ,t_FLUSEP C(1);
      ,t_NOFRAZ1 C(1);
      ,t_MODUM2 C(1);
      ,t_MVCODCOM C(15);
      ,t_MVCODATT C(15);
      ,t_MVCODCEN C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mecbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2.controlsource=this.cTrsName+'.t_MVCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMVUNIMIS_2_13.controlsource=this.cTrsName+'.t_MVUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMVQTAMOV_2_15.controlsource=this.cTrsName+'.t_MVQTAMOV'
    this.oPgFRm.Page1.oPag.oDESART_2_16.controlsource=this.cTrsName+'.t_DESART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMVTIPREC_2_18.controlsource=this.cTrsName+'.t_MVTIPREC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(58)
    this.AddVLine(200)
    this.AddVLine(240)
    this.AddVLine(320)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.EVA_COMP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.EVA_COMP_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.EVA_COMP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.EVA_COMP_IDX,2])
      *
      * insert into EVA_COMP
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'EVA_COMP')
        i_extval=cp_InsertValODBCExtFlds(this,'EVA_COMP')
        i_cFldBody=" "+;
                  "(MVSERIAL,MVROWNUM,CPROWORD,MVCODICE,MVTIPRIG"+;
                  ",MVCODART,MVUNIMIS,MVQTAMOV,MVQTAUM1,MVTIPREC"+;
                  ",MVCODCAU,MVDATEVA,MVSERRIF,MVROWRIF,MVQTASAL"+;
                  ",MVCODMAG,MVKEYSAL,MVFLORDI,MVFLIMPE,MVFLRISE"+;
                  ",MVFLERIF,MVDESSUP,MVMAGORI,MVMATORI,MVCODCOM"+;
                  ",MVCODATT,MVCODCEN,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MVSERIAL)+","+cp_ToStrODBC(this.w_MVROWNUM)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_MVCODICE)+","+cp_ToStrODBC(this.w_MVTIPRIG)+;
             ","+cp_ToStrODBCNull(this.w_MVCODART)+","+cp_ToStrODBCNull(this.w_MVUNIMIS)+","+cp_ToStrODBC(this.w_MVQTAMOV)+","+cp_ToStrODBC(this.w_MVQTAUM1)+","+cp_ToStrODBC(this.w_MVTIPREC)+;
             ","+cp_ToStrODBC(this.w_MVCODCAU)+","+cp_ToStrODBC(this.w_MVDATEVA)+","+cp_ToStrODBC(this.w_MVSERRIF)+","+cp_ToStrODBC(this.w_MVROWRIF)+","+cp_ToStrODBC(this.w_MVQTASAL)+;
             ","+cp_ToStrODBC(this.w_MVCODMAG)+","+cp_ToStrODBC(this.w_MVKEYSAL)+","+cp_ToStrODBC(this.w_MVFLORDI)+","+cp_ToStrODBC(this.w_MVFLIMPE)+","+cp_ToStrODBC(this.w_MVFLRISE)+;
             ","+cp_ToStrODBC(this.w_MVFLERIF)+","+cp_ToStrODBC(this.w_MVDESSUP)+","+cp_ToStrODBC(this.w_MVMAGORI)+","+cp_ToStrODBC(this.w_MVMATORI)+","+cp_ToStrODBC(this.w_MVCODCOM)+;
             ","+cp_ToStrODBC(this.w_MVCODATT)+","+cp_ToStrODBC(this.w_MVCODCEN)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'EVA_COMP')
        i_extval=cp_InsertValVFPExtFlds(this,'EVA_COMP')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MVSERIAL',this.w_MVSERIAL,'MVROWNUM',this.w_MVROWNUM)
        INSERT INTO (i_cTable) (;
                   MVSERIAL;
                  ,MVROWNUM;
                  ,CPROWORD;
                  ,MVCODICE;
                  ,MVTIPRIG;
                  ,MVCODART;
                  ,MVUNIMIS;
                  ,MVQTAMOV;
                  ,MVQTAUM1;
                  ,MVTIPREC;
                  ,MVCODCAU;
                  ,MVDATEVA;
                  ,MVSERRIF;
                  ,MVROWRIF;
                  ,MVQTASAL;
                  ,MVCODMAG;
                  ,MVKEYSAL;
                  ,MVFLORDI;
                  ,MVFLIMPE;
                  ,MVFLRISE;
                  ,MVFLERIF;
                  ,MVDESSUP;
                  ,MVMAGORI;
                  ,MVMATORI;
                  ,MVCODCOM;
                  ,MVCODATT;
                  ,MVCODCEN;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MVSERIAL;
                  ,this.w_MVROWNUM;
                  ,this.w_CPROWORD;
                  ,this.w_MVCODICE;
                  ,this.w_MVTIPRIG;
                  ,this.w_MVCODART;
                  ,this.w_MVUNIMIS;
                  ,this.w_MVQTAMOV;
                  ,this.w_MVQTAUM1;
                  ,this.w_MVTIPREC;
                  ,this.w_MVCODCAU;
                  ,this.w_MVDATEVA;
                  ,this.w_MVSERRIF;
                  ,this.w_MVROWRIF;
                  ,this.w_MVQTASAL;
                  ,this.w_MVCODMAG;
                  ,this.w_MVKEYSAL;
                  ,this.w_MVFLORDI;
                  ,this.w_MVFLIMPE;
                  ,this.w_MVFLRISE;
                  ,this.w_MVFLERIF;
                  ,this.w_MVDESSUP;
                  ,this.w_MVMAGORI;
                  ,this.w_MVMATORI;
                  ,this.w_MVCODCOM;
                  ,this.w_MVCODATT;
                  ,this.w_MVCODCEN;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.EVA_COMP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.EVA_COMP_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_MVCODICE)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'EVA_COMP')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'EVA_COMP')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_MVCODICE)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update EVA_COMP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'EVA_COMP')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MVCODICE="+cp_ToStrODBCNull(this.w_MVCODICE)+;
                     ",MVTIPRIG="+cp_ToStrODBC(this.w_MVTIPRIG)+;
                     ",MVCODART="+cp_ToStrODBCNull(this.w_MVCODART)+;
                     ",MVUNIMIS="+cp_ToStrODBCNull(this.w_MVUNIMIS)+;
                     ",MVQTAMOV="+cp_ToStrODBC(this.w_MVQTAMOV)+;
                     ",MVQTAUM1="+cp_ToStrODBC(this.w_MVQTAUM1)+;
                     ",MVTIPREC="+cp_ToStrODBC(this.w_MVTIPREC)+;
                     ",MVCODCAU="+cp_ToStrODBC(this.w_MVCODCAU)+;
                     ",MVDATEVA="+cp_ToStrODBC(this.w_MVDATEVA)+;
                     ",MVSERRIF="+cp_ToStrODBC(this.w_MVSERRIF)+;
                     ",MVROWRIF="+cp_ToStrODBC(this.w_MVROWRIF)+;
                     ",MVQTASAL="+cp_ToStrODBC(this.w_MVQTASAL)+;
                     ",MVCODMAG="+cp_ToStrODBC(this.w_MVCODMAG)+;
                     ",MVKEYSAL="+cp_ToStrODBC(this.w_MVKEYSAL)+;
                     ",MVFLORDI="+cp_ToStrODBC(this.w_MVFLORDI)+;
                     ",MVFLIMPE="+cp_ToStrODBC(this.w_MVFLIMPE)+;
                     ",MVFLRISE="+cp_ToStrODBC(this.w_MVFLRISE)+;
                     ",MVFLERIF="+cp_ToStrODBC(this.w_MVFLERIF)+;
                     ",MVDESSUP="+cp_ToStrODBC(this.w_MVDESSUP)+;
                     ",MVMAGORI="+cp_ToStrODBC(this.w_MVMAGORI)+;
                     ",MVMATORI="+cp_ToStrODBC(this.w_MVMATORI)+;
                     ",MVCODCOM="+cp_ToStrODBC(this.w_MVCODCOM)+;
                     ",MVCODATT="+cp_ToStrODBC(this.w_MVCODATT)+;
                     ",MVCODCEN="+cp_ToStrODBC(this.w_MVCODCEN)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'EVA_COMP')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MVCODICE=this.w_MVCODICE;
                     ,MVTIPRIG=this.w_MVTIPRIG;
                     ,MVCODART=this.w_MVCODART;
                     ,MVUNIMIS=this.w_MVUNIMIS;
                     ,MVQTAMOV=this.w_MVQTAMOV;
                     ,MVQTAUM1=this.w_MVQTAUM1;
                     ,MVTIPREC=this.w_MVTIPREC;
                     ,MVCODCAU=this.w_MVCODCAU;
                     ,MVDATEVA=this.w_MVDATEVA;
                     ,MVSERRIF=this.w_MVSERRIF;
                     ,MVROWRIF=this.w_MVROWRIF;
                     ,MVQTASAL=this.w_MVQTASAL;
                     ,MVCODMAG=this.w_MVCODMAG;
                     ,MVKEYSAL=this.w_MVKEYSAL;
                     ,MVFLORDI=this.w_MVFLORDI;
                     ,MVFLIMPE=this.w_MVFLIMPE;
                     ,MVFLRISE=this.w_MVFLRISE;
                     ,MVFLERIF=this.w_MVFLERIF;
                     ,MVDESSUP=this.w_MVDESSUP;
                     ,MVMAGORI=this.w_MVMAGORI;
                     ,MVMATORI=this.w_MVMATORI;
                     ,MVCODCOM=this.w_MVCODCOM;
                     ,MVCODATT=this.w_MVCODATT;
                     ,MVCODCEN=this.w_MVCODCEN;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.EVA_COMP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.EVA_COMP_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_MVCODICE)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete EVA_COMP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_MVCODICE)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.EVA_COMP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.EVA_COMP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
          .link_2_4('Full')
        .DoRTCalc(7,10,.t.)
          .link_2_9('Full')
        .DoRTCalc(12,14,.t.)
        if .o_MVCODICE<>.w_MVCODICE
          .w_MVUNIMIS = .w_UNMIS1
          .link_2_13('Full')
        endif
        .DoRTCalc(16,18,.t.)
          .w_CAUPFI = this.oparentobject .w_CAUPFI
          .w_CAUCOD = this.oparentobject .w_CAUCOD
        .DoRTCalc(21,21,.t.)
        if .o_MVQTAMOV<>.w_MVQTAMOV.or. .o_MVUNIMIS<>.w_MVUNIMIS
          .w_MVQTAUM1 = CALQTAADV(.w_MVQTAMOV,.w_MVUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,this, "MVQTAMOV")
        endif
        .DoRTCalc(23,23,.t.)
          .w_MVCODCAU = IIF(.w_MVTIPRIG='1', .w_CAUPFI, .w_CAUCOD)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(25,44,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MVTIPRIG with this.w_MVTIPRIG
      replace t_MVCODART with this.w_MVCODART
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_OPERA3 with this.w_OPERA3
      replace t_DTOBS1 with this.w_DTOBS1
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_OPERAT with this.w_OPERAT
      replace t_MOLTIP with this.w_MOLTIP
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_MVQTAUM1 with this.w_MVQTAUM1
      replace t_MVCODCAU with this.w_MVCODCAU
      replace t_MVDATEVA with this.w_MVDATEVA
      replace t_MVSERRIF with this.w_MVSERRIF
      replace t_MVROWRIF with this.w_MVROWRIF
      replace t_MVQTASAL with this.w_MVQTASAL
      replace t_MVCODMAG with this.w_MVCODMAG
      replace t_MVKEYSAL with this.w_MVKEYSAL
      replace t_MVFLORDI with this.w_MVFLORDI
      replace t_MVFLIMPE with this.w_MVFLIMPE
      replace t_MVFLRISE with this.w_MVFLRISE
      replace t_MVFLERIF with this.w_MVFLERIF
      replace t_MVDESSUP with this.w_MVDESSUP
      replace t_MVMAGORI with this.w_MVMAGORI
      replace t_MVMATORI with this.w_MVMATORI
      replace t_FLUSEP with this.w_FLUSEP
      replace t_NOFRAZ1 with this.w_NOFRAZ1
      replace t_MODUM2 with this.w_MODUM2
      replace t_MVCODCOM with this.w_MVCODCOM
      replace t_MVCODATT with this.w_MVCODATT
      replace t_MVCODCEN with this.w_MVCODCEN
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVCODICE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVCODICE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVUNIMIS_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVUNIMIS_2_13.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVQTAMOV_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVQTAMOV_2_15.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVTIPREC_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVTIPREC_2_18.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVCODICE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MVCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MVCODICE))
          select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oMVCODICE_2_2'),i_cWhere,'GSMA_BZA',"Elenco componenti",'GSDS_MDB.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MVCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MVCODICE)
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_DESART = NVL(_Link_.CADESART,space(40))
      this.w_MVCODART = NVL(_Link_.CACODART,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_MVTIPRIG = NVL(_Link_.CA__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODICE = space(20)
      endif
      this.w_DESART = space(40)
      this.w_MVCODART = space(20)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_DTOBS1 = ctod("  /  /  ")
      this.w_MVTIPRIG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MVTIPRIG='R' AND (.w_DTOBS1>i_DATSYS OR EMPTY(.w_DTOBS1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente oppure obsoleto")
        endif
        this.w_MVCODICE = space(20)
        this.w_DESART = space(40)
        this.w_MVCODART = space(20)
        this.w_UNMIS3 = space(3)
        this.w_OPERA3 = space(1)
        this.w_MOLTI3 = 0
        this.w_DTOBS1 = ctod("  /  /  ")
        this.w_MVTIPRIG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CACODICE as CACODICE202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CACODART as CACODART202"+ ",link_2_2.CAUNIMIS as CAUNIMIS202"+ ",link_2_2.CAOPERAT as CAOPERAT202"+ ",link_2_2.CAMOLTIP as CAMOLTIP202"+ ",link_2_2.CADTOBSO as CADTOBSO202"+ ",link_2_2.CA__TIPO as CA__TIPO202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on EVA_COMP.MVCODICE=link_2_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and EVA_COMP.MVCODICE=link_2_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MVCODART
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARMOLTIP,ARUNMIS2,ARUNMIS1,AROPERAT,ARFLUSEP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MVCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MVCODART)
            select ARCODART,ARMOLTIP,ARUNMIS2,ARUNMIS1,AROPERAT,ARFLUSEP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODART = NVL(_Link_.ARCODART,space(20))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODART = space(20)
      endif
      this.w_MOLTIP = 0
      this.w_UNMIS2 = space(3)
      this.w_UNMIS1 = space(3)
      this.w_OPERAT = space(1)
      this.w_FLUSEP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.ARCODART as ARCODART204"+ ",link_2_4.ARMOLTIP as ARMOLTIP204"+ ",link_2_4.ARUNMIS2 as ARUNMIS2204"+ ",link_2_4.ARUNMIS1 as ARUNMIS1204"+ ",link_2_4.AROPERAT as AROPERAT204"+ ",link_2_4.ARFLUSEP as ARFLUSEP204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on EVA_COMP.MVCODART=link_2_4.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and EVA_COMP.MVCODART=link_2_4.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_NOFRAZ1 = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_NOFRAZ1 = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVUNIMIS
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_MVUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_MVUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oMVUNIMIS_2_13'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSDS_MDB.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_MVUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_MVUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_MVCODICE) or CHKUNIMI(.w_MVUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_MVUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.UMCODICE as UMCODICE213"+ ",link_2_13.UMFLFRAZ as UMFLFRAZ213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on EVA_COMP.MVUNIMIS=link_2_13.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and EVA_COMP.MVUNIMIS=link_2_13.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESART_2_16.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_2_16.value=this.w_DESART
      replace t_DESART with this.oPgFrm.Page1.oPag.oDESART_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2.value==this.w_MVCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2.value=this.w_MVCODICE
      replace t_MVCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVUNIMIS_2_13.value==this.w_MVUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVUNIMIS_2_13.value=this.w_MVUNIMIS
      replace t_MVUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVUNIMIS_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVQTAMOV_2_15.value==this.w_MVQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVQTAMOV_2_15.value=this.w_MVQTAMOV
      replace t_MVQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVQTAMOV_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVTIPREC_2_18.RadioValue()==this.w_MVTIPREC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVTIPREC_2_18.SetRadio()
      replace t_MVTIPREC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVTIPREC_2_18.value
    endif
    cp_SetControlsValueExtFlds(this,'EVA_COMP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_MVTIPRIG='R' AND (.w_DTOBS1>i_DATSYS OR EMPTY(.w_DTOBS1))) and (NOT EMPTY(.oParentObject .w_MVCODICE)) and not(empty(.w_MVCODICE)) and (not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_MVCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice incongruente oppure obsoleto")
        case   (empty(.w_MVUNIMIS) or not(EMPTY(.w_MVCODICE) or CHKUNIMI(.w_MVUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3))) and (NOT EMPTY(.w_MVCODICE)) and (not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_MVCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVUNIMIS_2_13
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
        case   not(.w_MVQTAMOV<>0 AND (.w_FLFRAZ<>'S' OR .w_MVQTAMOV=INT(.w_MVQTAMOV))) and (NOT EMPTY(.w_MVCODICE)) and (not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_MVCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVQTAMOV_2_15
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
        case   (empty(.w_MVTIPREC) or not((.w_MVTIPREC='1' AND NOT EMPTY(.w_CAUPFI)) OR (.w_MVTIPREC='2' AND NOT EMPTY(.w_CAUCOD)))) and (NOT EMPTY(.w_CAUPFI) AND NOT EMPTY(.w_CAUCOD)) and (not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_MVCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVTIPREC_2_18
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_MVCODICE)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVCODICE = this.w_MVCODICE
    this.o_MVUNIMIS = this.w_MVUNIMIS
    this.o_MVQTAMOV = this.w_MVQTAMOV
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) AND NOT EMPTY(t_MVCODICE))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MVCODICE=space(20)
      .w_MVTIPRIG=space(1)
      .w_MVCODART=space(20)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_OPERA3=space(1)
      .w_DTOBS1=ctod("  /  /  ")
      .w_UNMIS1=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_UNMIS2=space(3)
      .w_MVUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_MVQTAMOV=0
      .w_DESART=space(40)
      .w_MVQTAUM1=0
      .w_MVTIPREC=space(1)
      .w_MVCODCAU=space(5)
      .w_MVDATEVA=ctod("  /  /  ")
      .w_MVSERRIF=space(10)
      .w_MVROWRIF=0
      .w_MVQTASAL=0
      .w_MVCODMAG=space(5)
      .w_MVKEYSAL=space(20)
      .w_MVFLORDI=space(1)
      .w_MVFLIMPE=space(1)
      .w_MVFLRISE=space(1)
      .w_MVFLERIF=space(1)
      .w_MVDESSUP=space(0)
      .w_MVMAGORI=space(5)
      .w_MVMATORI=space(5)
      .w_FLUSEP=space(1)
      .w_NOFRAZ1=space(1)
      .w_MODUM2=space(1)
      .w_MVCODCOM=space(15)
      .w_MVCODATT=space(15)
      .w_MVCODCEN=space(15)
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_MVCODICE))
        .link_2_2('Full')
      endif
        .w_MVTIPRIG = 'R'
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_MVCODART))
        .link_2_4('Full')
      endif
      .DoRTCalc(7,11,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_9('Full')
      endif
      .DoRTCalc(12,14,.f.)
        .w_MVUNIMIS = .w_UNMIS1
      .DoRTCalc(15,15,.f.)
      if not(empty(.w_MVUNIMIS))
        .link_2_13('Full')
      endif
      .DoRTCalc(16,21,.f.)
        .w_MVQTAUM1 = CALQTAADV(.w_MVQTAMOV,.w_MVUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,this, "MVQTAMOV")
        .w_MVTIPREC = IIF(EMPTY(.w_CAUCOD), '1', '2')
        .w_MVCODCAU = IIF(.w_MVTIPRIG='1', .w_CAUPFI, .w_CAUCOD)
        .w_MVDATEVA = .w_TDATEVA
      .DoRTCalc(26,41,.f.)
        .w_MVCODCOM = this.oParentObject.oParentObject.oParentObject .w_MVCODCOM
        .w_MVCODATT = this.oParentObject.oParentObject.oParentObject .w_MVCODATT
        .w_MVCODCEN = this.oParentObject.oParentObject.oParentObject .w_MVCODCEN
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_MVCODICE = t_MVCODICE
    this.w_MVTIPRIG = t_MVTIPRIG
    this.w_MVCODART = t_MVCODART
    this.w_UNMIS3 = t_UNMIS3
    this.w_MOLTI3 = t_MOLTI3
    this.w_OPERA3 = t_OPERA3
    this.w_DTOBS1 = t_DTOBS1
    this.w_UNMIS1 = t_UNMIS1
    this.w_OPERAT = t_OPERAT
    this.w_MOLTIP = t_MOLTIP
    this.w_UNMIS2 = t_UNMIS2
    this.w_MVUNIMIS = t_MVUNIMIS
    this.w_FLFRAZ = t_FLFRAZ
    this.w_MVQTAMOV = t_MVQTAMOV
    this.w_DESART = t_DESART
    this.w_MVQTAUM1 = t_MVQTAUM1
    this.w_MVTIPREC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVTIPREC_2_18.RadioValue(.t.)
    this.w_MVCODCAU = t_MVCODCAU
    this.w_MVDATEVA = t_MVDATEVA
    this.w_MVSERRIF = t_MVSERRIF
    this.w_MVROWRIF = t_MVROWRIF
    this.w_MVQTASAL = t_MVQTASAL
    this.w_MVCODMAG = t_MVCODMAG
    this.w_MVKEYSAL = t_MVKEYSAL
    this.w_MVFLORDI = t_MVFLORDI
    this.w_MVFLIMPE = t_MVFLIMPE
    this.w_MVFLRISE = t_MVFLRISE
    this.w_MVFLERIF = t_MVFLERIF
    this.w_MVDESSUP = t_MVDESSUP
    this.w_MVMAGORI = t_MVMAGORI
    this.w_MVMATORI = t_MVMATORI
    this.w_FLUSEP = t_FLUSEP
    this.w_NOFRAZ1 = t_NOFRAZ1
    this.w_MODUM2 = t_MODUM2
    this.w_MVCODCOM = t_MVCODCOM
    this.w_MVCODATT = t_MVCODATT
    this.w_MVCODCEN = t_MVCODCEN
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MVCODICE with this.w_MVCODICE
    replace t_MVTIPRIG with this.w_MVTIPRIG
    replace t_MVCODART with this.w_MVCODART
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_OPERA3 with this.w_OPERA3
    replace t_DTOBS1 with this.w_DTOBS1
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_OPERAT with this.w_OPERAT
    replace t_MOLTIP with this.w_MOLTIP
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_MVUNIMIS with this.w_MVUNIMIS
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_MVQTAMOV with this.w_MVQTAMOV
    replace t_DESART with this.w_DESART
    replace t_MVQTAUM1 with this.w_MVQTAUM1
    replace t_MVTIPREC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVTIPREC_2_18.ToRadio()
    replace t_MVCODCAU with this.w_MVCODCAU
    replace t_MVDATEVA with this.w_MVDATEVA
    replace t_MVSERRIF with this.w_MVSERRIF
    replace t_MVROWRIF with this.w_MVROWRIF
    replace t_MVQTASAL with this.w_MVQTASAL
    replace t_MVCODMAG with this.w_MVCODMAG
    replace t_MVKEYSAL with this.w_MVKEYSAL
    replace t_MVFLORDI with this.w_MVFLORDI
    replace t_MVFLIMPE with this.w_MVFLIMPE
    replace t_MVFLRISE with this.w_MVFLRISE
    replace t_MVFLERIF with this.w_MVFLERIF
    replace t_MVDESSUP with this.w_MVDESSUP
    replace t_MVMAGORI with this.w_MVMAGORI
    replace t_MVMATORI with this.w_MVMATORI
    replace t_FLUSEP with this.w_FLUSEP
    replace t_NOFRAZ1 with this.w_NOFRAZ1
    replace t_MODUM2 with this.w_MODUM2
    replace t_MVCODCOM with this.w_MVCODCOM
    replace t_MVCODATT with this.w_MVCODATT
    replace t_MVCODCEN with this.w_MVCODCEN
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mecPag1 as StdContainer
  Width  = 422
  height = 346
  stdWidth  = 422
  stdheight = 346
  resizeXpos=123
  resizeYpos=143
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=7, width=410,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Riga",Field2="MVCODICE",Label2="Articolo",Field3="MVUNIMIS",Label3="U.M.",Field4="MVQTAMOV",Label4="Quantit�",Field5="MVTIPREC",Label5="Causale",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 252555898

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=26,;
    width=406+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=27,width=405+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESART_2_16.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oMVCODICE_2_2
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oMVUNIMIS_2_13
    endcase
    return(oDropInto)
  EndFunc


  add object oDESART_2_16 as StdTrsField with uid="FGXYWQTMMW",rtseq=21,rtrep=.t.,;
    cFormVar="w_DESART",value=space(40),enabled=.f.,;
    HelpContextID = 174259658,;
    cTotal="", bFixedPos=.t., cQueryName = "DESART",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=211, Left=7, Top=318, InputMask=replicate('X',40)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mecBodyRow as CPBodyRowCnt
  Width=396
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="NIGLVDHCLZ",rtseq=3,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 16448874,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oMVCODICE_2_2 as StdTrsField with uid="TDIKJKDMYK",rtseq=4,rtrep=.t.,;
    cFormVar="w_MVCODICE",value=space(20),;
    ToolTipText = "Codice componente della distinta base",;
    HelpContextID = 164238347,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=140, Left=48, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_MVCODICE"

  func oMVCODICE_2_2.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.oParentObject .w_MVCODICE))
    endwith
  endfunc

  func oMVCODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODICE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMVCODICE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oMVCODICE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco componenti",'GSDS_MDB.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oMVCODICE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_MVCODICE
    i_obj.ecpSave()
  endproc

  add object oMVUNIMIS_2_13 as StdTrsField with uid="MSMNYCJKKB",rtseq=15,rtrep=.t.,;
    cFormVar="w_MVUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 236598297,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=38, Left=191, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_MVUNIMIS"

  func oMVUNIMIS_2_13.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVCODICE))
    endwith
  endfunc

  func oMVUNIMIS_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVUNIMIS_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMVUNIMIS_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oMVUNIMIS_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSDS_MDB.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oMVUNIMIS_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_MVUNIMIS
    i_obj.ecpSave()
  endproc

  add object oMVQTAMOV_2_15 as StdTrsField with uid="JIGGAFUOEN",rtseq=17,rtrep=.t.,;
    cFormVar="w_MVQTAMOV",value=0,;
    ToolTipText = "Coefficiente di impiego",;
    HelpContextID = 39848932,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=78, Left=230, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]

  func oMVQTAMOV_2_15.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVCODICE))
    endwith
  endfunc

  func oMVQTAMOV_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVQTAMOV<>0 AND (.w_FLFRAZ<>'S' OR .w_MVQTAMOV=INT(.w_MVQTAMOV)))
    endwith
    return bRes
  endfunc

  add object oMVTIPREC_2_18 as StdTrsCombo with uid="SUHQQKEDLM",rtrep=.t.,;
    cFormVar="w_MVTIPREC", RowSource=""+"Prod.finito,"+"Componente" , ;
    HelpContextID = 59057161,;
    Height=21, Width=81, Left=310, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oMVTIPREC_2_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MVTIPREC,&i_cF..t_MVTIPREC),this.value)
    return(iif(xVal =1,'1',;
    iif(xVal =2,'2',;
    space(1))))
  endfunc
  func oMVTIPREC_2_18.GetRadio()
    this.Parent.oContained.w_MVTIPREC = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPREC_2_18.ToRadio()
    this.Parent.oContained.w_MVTIPREC=trim(this.Parent.oContained.w_MVTIPREC)
    return(;
      iif(this.Parent.oContained.w_MVTIPREC=='1',1,;
      iif(this.Parent.oContained.w_MVTIPREC=='2',2,;
      0)))
  endfunc

  func oMVTIPREC_2_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oMVTIPREC_2_18.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CAUPFI) AND NOT EMPTY(.w_CAUCOD))
    endwith
  endfunc

  func oMVTIPREC_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_MVTIPREC='1' AND NOT EMPTY(.w_CAUPFI)) OR (.w_MVTIPREC='2' AND NOT EMPTY(.w_CAUCOD)))
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=14
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mec','EVA_COMP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MVSERIAL=EVA_COMP.MVSERIAL";
  +" and "+i_cAliasName2+".MVROWNUM=EVA_COMP.MVROWNUM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
