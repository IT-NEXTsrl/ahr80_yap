* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_klt                                                        *
*              Generatore looktab                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-25                                                      *
* Last revis.: 2014-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsar_klt
**Apertura della maschera dalla modifica del report
**Se la maschera � gi� stata aperta non se ne apre un altra ma gli si d� il focus
**Si cambia il nome alla maschera per poterla chiudere automaticamente all uscita della modifica del report

IF alltrim(ON ('KEY','ALT+F2'))='gsar_klt()'
local maschera
   IF WEXIST('gsar_klt') 
      ACTIVATE WINDOW gsar_klt
      return
   ELSE
      maschera = createobject("tgsar_klt",oParentObject)
      maschera.name='gsar_klt'
      return maschera
   ENDIF
ENDIF
* --- Fine Area Manuale
return(createobject("tgsar_klt",oParentObject))

* --- Class definition
define class tgsar_klt as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 716
  Height = 433+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-02"
  HelpContextID=216627049
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=51

  * --- Constant Properties
  _IDX = 0
  XDC_TABLE_IDX = 0
  XDC_FIELDS_IDX = 0
  cPrg = "gsar_klt"
  cComment = "Generatore looktab"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DESTABLE = space(20)
  o_DESTABLE = space(20)
  w_DESTABLE = space(20)
  w_NUM_KEYS = 0
  w_PK__LIST = space(254)
  w_PKFLD_01 = space(30)
  w_PKFLD_02 = space(30)
  w_PKFLD_03 = space(30)
  w_PKFLD_04 = space(30)
  w_PKFLD_05 = space(30)
  w_DESFIELD = space(30)
  o_DESFIELD = space(30)
  w_DESFIELD = space(30)
  w_VALPKC01 = space(20)
  o_VALPKC01 = space(20)
  w_FLTYPE01 = space(1)
  w_FLTYPE02 = space(1)
  w_FLTYPE03 = space(1)
  w_FLTYPE04 = space(1)
  w_FLTYPE05 = space(1)
  w_VALPKC02 = space(20)
  o_VALPKC02 = space(20)
  w_VALPKC03 = space(20)
  o_VALPKC03 = space(20)
  w_VALPKC04 = space(20)
  o_VALPKC04 = space(20)
  w_VALPKC05 = space(20)
  o_VALPKC05 = space(20)
  w_VALPKN01 = 0
  o_VALPKN01 = 0
  w_VALPKN02 = 0
  o_VALPKN02 = 0
  w_VALPKN03 = 0
  o_VALPKN03 = 0
  w_VALPKN04 = 0
  o_VALPKN04 = 0
  w_VALPKN05 = 0
  o_VALPKN05 = 0
  w_VALPKD01 = ctod('  /  /  ')
  o_VALPKD01 = ctod('  /  /  ')
  w_VALPKD02 = ctod('  /  /  ')
  o_VALPKD02 = ctod('  /  /  ')
  w_VALPKD03 = ctod('  /  /  ')
  o_VALPKD03 = ctod('  /  /  ')
  w_VALPKD04 = ctod('  /  /  ')
  o_VALPKD04 = ctod('  /  /  ')
  w_VALPKD05 = ctod('  /  /  ')
  o_VALPKD05 = ctod('  /  /  ')
  w_PRINTCUR = space(10)
  o_PRINTCUR = space(10)
  w_GETVFC01 = space(10)
  o_GETVFC01 = space(10)
  w_VALPKT01 = space(20)
  o_VALPKT01 = space(20)
  w_CURTOTRE = 0
  w_GETVFC02 = space(10)
  o_GETVFC02 = space(10)
  w_VALPKT02 = space(20)
  o_VALPKT02 = space(20)
  w_GETVFC03 = space(10)
  o_GETVFC03 = space(10)
  w_VALPKT03 = space(20)
  o_VALPKT03 = space(20)
  w_GETVFC04 = space(10)
  o_GETVFC04 = space(10)
  w_VALPKT04 = space(20)
  o_VALPKT04 = space(20)
  w_GETVFC05 = space(10)
  o_GETVFC05 = space(10)
  w_VALPKT05 = space(20)
  o_VALPKT05 = space(20)
  w_LTKEY_01 = space(254)
  w_LTKEY_02 = space(254)
  w_LTROWDAT = 0
  w_LTKEY_03 = space(254)
  w_LTKEY_04 = space(254)
  w_LTKEY_05 = space(254)
  w_LTSINTAX = space(0)
  w_LTRESULT = space(0)
  w_LBLPK_01 = .NULL.
  w_LBLPK_02 = .NULL.
  w_LBLPK_03 = .NULL.
  w_LBLPK_04 = .NULL.
  w_LBLPK_05 = .NULL.
  w_VIS__CUR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kltPag1","gsar_klt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generatore looktab")
      .Pages(2).addobject("oPag","tgsar_kltPag2","gsar_klt",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Mostra cursore")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDESTABLE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_LBLPK_01 = this.oPgFrm.Pages(1).oPag.LBLPK_01
    this.w_LBLPK_02 = this.oPgFrm.Pages(1).oPag.LBLPK_02
    this.w_LBLPK_03 = this.oPgFrm.Pages(1).oPag.LBLPK_03
    this.w_LBLPK_04 = this.oPgFrm.Pages(1).oPag.LBLPK_04
    this.w_LBLPK_05 = this.oPgFrm.Pages(1).oPag.LBLPK_05
    this.w_VIS__CUR = this.oPgFrm.Pages(2).oPag.VIS__CUR
    DoDefault()
    proc Destroy()
      this.w_LBLPK_01 = .NULL.
      this.w_LBLPK_02 = .NULL.
      this.w_LBLPK_03 = .NULL.
      this.w_LBLPK_04 = .NULL.
      this.w_LBLPK_05 = .NULL.
      this.w_VIS__CUR = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='XDC_TABLE'
    this.cWorkTables[2]='XDC_FIELDS'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DESTABLE=space(20)
      .w_DESTABLE=space(20)
      .w_NUM_KEYS=0
      .w_PK__LIST=space(254)
      .w_PKFLD_01=space(30)
      .w_PKFLD_02=space(30)
      .w_PKFLD_03=space(30)
      .w_PKFLD_04=space(30)
      .w_PKFLD_05=space(30)
      .w_DESFIELD=space(30)
      .w_DESFIELD=space(30)
      .w_VALPKC01=space(20)
      .w_FLTYPE01=space(1)
      .w_FLTYPE02=space(1)
      .w_FLTYPE03=space(1)
      .w_FLTYPE04=space(1)
      .w_FLTYPE05=space(1)
      .w_VALPKC02=space(20)
      .w_VALPKC03=space(20)
      .w_VALPKC04=space(20)
      .w_VALPKC05=space(20)
      .w_VALPKN01=0
      .w_VALPKN02=0
      .w_VALPKN03=0
      .w_VALPKN04=0
      .w_VALPKN05=0
      .w_VALPKD01=ctod("  /  /  ")
      .w_VALPKD02=ctod("  /  /  ")
      .w_VALPKD03=ctod("  /  /  ")
      .w_VALPKD04=ctod("  /  /  ")
      .w_VALPKD05=ctod("  /  /  ")
      .w_PRINTCUR=space(10)
      .w_GETVFC01=space(10)
      .w_VALPKT01=space(20)
      .w_CURTOTRE=0
      .w_GETVFC02=space(10)
      .w_VALPKT02=space(20)
      .w_GETVFC03=space(10)
      .w_VALPKT03=space(20)
      .w_GETVFC04=space(10)
      .w_VALPKT04=space(20)
      .w_GETVFC05=space(10)
      .w_VALPKT05=space(20)
      .w_LTKEY_01=space(254)
      .w_LTKEY_02=space(254)
      .w_LTROWDAT=0
      .w_LTKEY_03=space(254)
      .w_LTKEY_04=space(254)
      .w_LTKEY_05=space(254)
      .w_LTSINTAX=space(0)
      .w_LTRESULT=space(0)
          .DoRTCalc(1,2,.f.)
        .w_NUM_KEYS = OCCURS(",",ALLTRIM(cp_KeyToSQL(I_DCX.GetIdxDef(.w_DESTABLE,1))))
        .w_PK__LIST = IIF(.w_NUM_KEYS<5, ALLTRIM(cp_KeyToSQL(I_DCX.GetIdxDef(.w_DESTABLE,1))), "")
        .w_PKFLD_01 = IIF(.w_NUM_KEYS>0, LEFT(.w_PK__LIST,AT(",",.w_PK__LIST)-1), .w_PK__LIST)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PKFLD_01))
          .link_1_7('Full')
        endif
        .w_PKFLD_02 = IIF(.w_NUM_KEYS>=1, SUBSTR(.w_PK__LIST, AT(",",.w_PK__LIST,1)+1, IIF(.w_NUM_KEYS>=2, AT(",",.w_PK__LIST,2), LEN(.w_PK__LIST)+1 )-AT(",",.w_PK__LIST,1)-1), "")
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_PKFLD_02))
          .link_1_8('Full')
        endif
        .w_PKFLD_03 = IIF(.w_NUM_KEYS>=2, SUBSTR(.w_PK__LIST, AT(",",.w_PK__LIST,2)+1, IIF(.w_NUM_KEYS>=3, AT(",",.w_PK__LIST,3), LEN(.w_PK__LIST)+1 )-AT(",",.w_PK__LIST,2)-1), "")
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_PKFLD_03))
          .link_1_9('Full')
        endif
        .w_PKFLD_04 = IIF(.w_NUM_KEYS>=3, SUBSTR(.w_PK__LIST, AT(",",.w_PK__LIST,3)+1, IIF(.w_NUM_KEYS>=4, AT(",",.w_PK__LIST,4), LEN(.w_PK__LIST)+1 )-AT(",",.w_PK__LIST,3)-1), "")
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_PKFLD_04))
          .link_1_10('Full')
        endif
        .w_PKFLD_05 = IIF(.w_NUM_KEYS>=5, SUBSTR(.w_PK__LIST, AT(",",.w_PK__LIST,4)+1, IIF(.w_NUM_KEYS>=5, AT(",",.w_PK__LIST,5), LEN(.w_PK__LIST)+1 )-AT(",",.w_PK__LIST,4)-1), "")
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_PKFLD_05))
          .link_1_11('Full')
        endif
      .oPgFrm.Page1.oPag.LBLPK_01.Calculate(IIF(EMPTY(.w_PKFLD_01),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_01))))
      .oPgFrm.Page1.oPag.LBLPK_02.Calculate(IIF(EMPTY(.w_PKFLD_02),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_02))))
      .oPgFrm.Page1.oPag.LBLPK_03.Calculate(IIF(EMPTY(.w_PKFLD_03),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_03))))
      .oPgFrm.Page1.oPag.LBLPK_04.Calculate(IIF(EMPTY(.w_PKFLD_04),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_04))))
      .oPgFrm.Page1.oPag.LBLPK_05.Calculate(IIF(EMPTY(.w_PKFLD_05),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_05))))
          .DoRTCalc(10,31,.f.)
        .w_PRINTCUR = IIF(VARTYPE(g_CURCURSOR)='C', ALLTRIM(g_CURCURSOR), "")
      .oPgFrm.Page2.oPag.VIS__CUR.Calculate()
        .w_GETVFC01 = IIF(EMPTY(.w_PRINTCUR),'N', IIF(EMPTY(.w_GETVFC01), 'S', .w_GETVFC01 ))
          .DoRTCalc(34,34,.f.)
        .w_CURTOTRE = IIF(EMPTY(.w_PRINTCUR), 0, RECCOUNT((.w_PRINTCUR)))
        .w_GETVFC02 = IIF(EMPTY(.w_PRINTCUR),'N', IIF(EMPTY(.w_GETVFC02), 'S', .w_GETVFC02 ))
          .DoRTCalc(37,37,.f.)
        .w_GETVFC03 = IIF(EMPTY(.w_PRINTCUR),'N', IIF(EMPTY(.w_GETVFC03), 'S', .w_GETVFC03 ))
          .DoRTCalc(39,39,.f.)
        .w_GETVFC04 = IIF(EMPTY(.w_PRINTCUR),'N', IIF(EMPTY(.w_GETVFC04), 'S', .w_GETVFC04 ))
          .DoRTCalc(41,41,.f.)
        .w_GETVFC05 = IIF(EMPTY(.w_PRINTCUR),'N', IIF(EMPTY(.w_GETVFC05), 'S', .w_GETVFC05 ))
          .DoRTCalc(43,43,.f.)
        .w_LTKEY_01 = IIF(EMPTY(.w_PKFLD_01),"", '"'+ALLTRIM(.w_PKFLD_01)+'", '+IIF(.w_GETVFC01='S', .w_VALPKT01, ICASE(.w_FLTYPE01='C', '"'+ALLTRIM(.w_VALPKC01)+'"', .w_FLTYPE01='N', ALLTRIM(STR(.w_VALPKN01)), .w_FLTYPE01='D', '{^'+ALLTRIM(STR(YEAR(.w_VALPKD01)))+'-'+ALLTRIM(STR(MONTH(.w_VALPKD01)))+'-'+ALLTRIM(STR(DAY(.w_VALPKD01)))+'}',"")))
        .w_LTKEY_02 = IIF(EMPTY(.w_PKFLD_02),"", '"'+ALLTRIM(.w_PKFLD_02)+'", '+IIF(.w_GETVFC02='S', .w_VALPKT02, ICASE(.w_FLTYPE02='C', '"'+ALLTRIM(.w_VALPKC02)+'"', .w_FLTYPE02='N', ALLTRIM(STR(.w_VALPKN02)), .w_FLTYPE02='D', '{^'+ALLTRIM(STR(YEAR(.w_VALPKD02)))+'-'+ALLTRIM(STR(MONTH(.w_VALPKD02)))+'-'+ALLTRIM(STR(DAY(.w_VALPKD02)))+'}',"")))
        .w_LTROWDAT = 1
        .w_LTKEY_03 = IIF(EMPTY(.w_PKFLD_03),"", '"'+ALLTRIM(.w_PKFLD_03)+'", '+IIF(.w_GETVFC03='S', .w_VALPKT03, ICASE(.w_FLTYPE03='C', '"'+ALLTRIM(.w_VALPKC03)+'"', .w_FLTYPE03='N', ALLTRIM(STR(.w_VALPKN03)), .w_FLTYPE03='D', '{^'+ALLTRIM(STR(YEAR(.w_VALPKD03)))+'-'+ALLTRIM(STR(MONTH(.w_VALPKD03)))+'-'+ALLTRIM(STR(DAY(.w_VALPKD03)))+'}',"")))
        .w_LTKEY_04 = IIF(EMPTY(.w_PKFLD_04),"", '"'+ALLTRIM(.w_PKFLD_04)+'", '+IIF(.w_GETVFC04='S', .w_VALPKT04, ICASE(.w_FLTYPE04='C', '"'+ALLTRIM(.w_VALPKC04)+'"', .w_FLTYPE04='N', ALLTRIM(STR(.w_VALPKN04)), .w_FLTYPE04='D', '{^'+ALLTRIM(STR(YEAR(.w_VALPKD04)))+'-'+ALLTRIM(STR(MONTH(.w_VALPKD04)))+'-'+ALLTRIM(STR(DAY(.w_VALPKD04)))+'}',"")))
        .w_LTKEY_05 = IIF(EMPTY(.w_PKFLD_05),"", '"'+ALLTRIM(.w_PKFLD_05)+'", '+IIF(.w_GETVFC05='S', .w_VALPKT05, ICASE(.w_FLTYPE05='C', '"'+ALLTRIM(.w_VALPKC05)+'"', .w_FLTYPE05='N', ALLTRIM(STR(.w_VALPKN05)), .w_FLTYPE05='D', '{^'+ALLTRIM(STR(YEAR(.w_VALPKD05)))+'-'+ALLTRIM(STR(MONTH(.w_VALPKD05)))+'-'+ALLTRIM(STR(DAY(.w_VALPKD05)))+'}',"")))
        .w_LTSINTAX = 'LookTab("'+ALLTRIM(.w_DESTABLE)+'", "'+ALLTRIM(.w_DESFIELD)+'"'+IIF(EMPTY(.w_LTKEY_01),"", ', '+ALLTRIM(.w_LTKEY_01))+IIF(EMPTY(.w_LTKEY_02),"", ', '+ALLTRIM(.w_LTKEY_02))+IIF(EMPTY(.w_LTKEY_03),"", ', '+ALLTRIM(.w_LTKEY_03))+IIF(EMPTY(.w_LTKEY_04),"", ', '+ALLTRIM(.w_LTKEY_04))+IIF(EMPTY(.w_LTKEY_05),"", ', '+ALLTRIM(.w_LTKEY_05))+')'
    endwith
    this.DoRTCalc(51,51,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_klt
    if !EMPTY(this.w_PRINTCUR)
      Local oCtrl
      oCtrl = this.GetCtrl("w_VIS__CUR")
      oCtrl.cCursor = this.w_PRINTCUR
      oCtrl.Browse()
      oCtrl=.NULL.
      For nCbo=1 to 5
        oCtrl = this.GetCtrl("w_VALPKT0"+ALLTRIM(STR(nCbo)),1,1,"ah_CurFieldList","cFormVar")
        oCtrl.Clear()
        oCtrl.Init()
        oCtrl=.NULL.
      NEXT
    endif
    
    
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_NUM_KEYS = OCCURS(",",ALLTRIM(cp_KeyToSQL(I_DCX.GetIdxDef(.w_DESTABLE,1))))
            .w_PK__LIST = IIF(.w_NUM_KEYS<5, ALLTRIM(cp_KeyToSQL(I_DCX.GetIdxDef(.w_DESTABLE,1))), "")
            .w_PKFLD_01 = IIF(.w_NUM_KEYS>0, LEFT(.w_PK__LIST,AT(",",.w_PK__LIST)-1), .w_PK__LIST)
          .link_1_7('Full')
            .w_PKFLD_02 = IIF(.w_NUM_KEYS>=1, SUBSTR(.w_PK__LIST, AT(",",.w_PK__LIST,1)+1, IIF(.w_NUM_KEYS>=2, AT(",",.w_PK__LIST,2), LEN(.w_PK__LIST)+1 )-AT(",",.w_PK__LIST,1)-1), "")
          .link_1_8('Full')
            .w_PKFLD_03 = IIF(.w_NUM_KEYS>=2, SUBSTR(.w_PK__LIST, AT(",",.w_PK__LIST,2)+1, IIF(.w_NUM_KEYS>=3, AT(",",.w_PK__LIST,3), LEN(.w_PK__LIST)+1 )-AT(",",.w_PK__LIST,2)-1), "")
          .link_1_9('Full')
            .w_PKFLD_04 = IIF(.w_NUM_KEYS>=3, SUBSTR(.w_PK__LIST, AT(",",.w_PK__LIST,3)+1, IIF(.w_NUM_KEYS>=4, AT(",",.w_PK__LIST,4), LEN(.w_PK__LIST)+1 )-AT(",",.w_PK__LIST,3)-1), "")
          .link_1_10('Full')
            .w_PKFLD_05 = IIF(.w_NUM_KEYS>=5, SUBSTR(.w_PK__LIST, AT(",",.w_PK__LIST,4)+1, IIF(.w_NUM_KEYS>=5, AT(",",.w_PK__LIST,5), LEN(.w_PK__LIST)+1 )-AT(",",.w_PK__LIST,4)-1), "")
          .link_1_11('Full')
        .oPgFrm.Page1.oPag.LBLPK_01.Calculate(IIF(EMPTY(.w_PKFLD_01),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_01))))
        .oPgFrm.Page1.oPag.LBLPK_02.Calculate(IIF(EMPTY(.w_PKFLD_02),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_02))))
        .oPgFrm.Page1.oPag.LBLPK_03.Calculate(IIF(EMPTY(.w_PKFLD_03),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_03))))
        .oPgFrm.Page1.oPag.LBLPK_04.Calculate(IIF(EMPTY(.w_PKFLD_04),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_04))))
        .oPgFrm.Page1.oPag.LBLPK_05.Calculate(IIF(EMPTY(.w_PKFLD_05),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_05))))
        .oPgFrm.Page2.oPag.VIS__CUR.Calculate()
        .DoRTCalc(10,32,.t.)
        if .o_PRINTCUR<>.w_PRINTCUR.or. .o_DESTABLE<>.w_DESTABLE.or. .o_DESFIELD<>.w_DESFIELD
            .w_GETVFC01 = IIF(EMPTY(.w_PRINTCUR),'N', IIF(EMPTY(.w_GETVFC01), 'S', .w_GETVFC01 ))
        endif
        .DoRTCalc(34,34,.t.)
            .w_CURTOTRE = IIF(EMPTY(.w_PRINTCUR), 0, RECCOUNT((.w_PRINTCUR)))
        if .o_PRINTCUR<>.w_PRINTCUR.or. .o_DESTABLE<>.w_DESTABLE.or. .o_DESFIELD<>.w_DESFIELD
            .w_GETVFC02 = IIF(EMPTY(.w_PRINTCUR),'N', IIF(EMPTY(.w_GETVFC02), 'S', .w_GETVFC02 ))
        endif
        .DoRTCalc(37,37,.t.)
        if .o_PRINTCUR<>.w_PRINTCUR.or. .o_DESTABLE<>.w_DESTABLE.or. .o_DESFIELD<>.w_DESFIELD
            .w_GETVFC03 = IIF(EMPTY(.w_PRINTCUR),'N', IIF(EMPTY(.w_GETVFC03), 'S', .w_GETVFC03 ))
        endif
        .DoRTCalc(39,39,.t.)
        if .o_PRINTCUR<>.w_PRINTCUR.or. .o_DESTABLE<>.w_DESTABLE.or. .o_DESFIELD<>.w_DESFIELD
            .w_GETVFC04 = IIF(EMPTY(.w_PRINTCUR),'N', IIF(EMPTY(.w_GETVFC04), 'S', .w_GETVFC04 ))
        endif
        .DoRTCalc(41,41,.t.)
        if .o_PRINTCUR<>.w_PRINTCUR.or. .o_DESTABLE<>.w_DESTABLE.or. .o_DESFIELD<>.w_DESFIELD
            .w_GETVFC05 = IIF(EMPTY(.w_PRINTCUR),'N', IIF(EMPTY(.w_GETVFC05), 'S', .w_GETVFC05 ))
        endif
        .DoRTCalc(43,43,.t.)
            .w_LTKEY_01 = IIF(EMPTY(.w_PKFLD_01),"", '"'+ALLTRIM(.w_PKFLD_01)+'", '+IIF(.w_GETVFC01='S', .w_VALPKT01, ICASE(.w_FLTYPE01='C', '"'+ALLTRIM(.w_VALPKC01)+'"', .w_FLTYPE01='N', ALLTRIM(STR(.w_VALPKN01)), .w_FLTYPE01='D', '{^'+ALLTRIM(STR(YEAR(.w_VALPKD01)))+'-'+ALLTRIM(STR(MONTH(.w_VALPKD01)))+'-'+ALLTRIM(STR(DAY(.w_VALPKD01)))+'}',"")))
            .w_LTKEY_02 = IIF(EMPTY(.w_PKFLD_02),"", '"'+ALLTRIM(.w_PKFLD_02)+'", '+IIF(.w_GETVFC02='S', .w_VALPKT02, ICASE(.w_FLTYPE02='C', '"'+ALLTRIM(.w_VALPKC02)+'"', .w_FLTYPE02='N', ALLTRIM(STR(.w_VALPKN02)), .w_FLTYPE02='D', '{^'+ALLTRIM(STR(YEAR(.w_VALPKD02)))+'-'+ALLTRIM(STR(MONTH(.w_VALPKD02)))+'-'+ALLTRIM(STR(DAY(.w_VALPKD02)))+'}',"")))
        .DoRTCalc(46,46,.t.)
            .w_LTKEY_03 = IIF(EMPTY(.w_PKFLD_03),"", '"'+ALLTRIM(.w_PKFLD_03)+'", '+IIF(.w_GETVFC03='S', .w_VALPKT03, ICASE(.w_FLTYPE03='C', '"'+ALLTRIM(.w_VALPKC03)+'"', .w_FLTYPE03='N', ALLTRIM(STR(.w_VALPKN03)), .w_FLTYPE03='D', '{^'+ALLTRIM(STR(YEAR(.w_VALPKD03)))+'-'+ALLTRIM(STR(MONTH(.w_VALPKD03)))+'-'+ALLTRIM(STR(DAY(.w_VALPKD03)))+'}',"")))
            .w_LTKEY_04 = IIF(EMPTY(.w_PKFLD_04),"", '"'+ALLTRIM(.w_PKFLD_04)+'", '+IIF(.w_GETVFC04='S', .w_VALPKT04, ICASE(.w_FLTYPE04='C', '"'+ALLTRIM(.w_VALPKC04)+'"', .w_FLTYPE04='N', ALLTRIM(STR(.w_VALPKN04)), .w_FLTYPE04='D', '{^'+ALLTRIM(STR(YEAR(.w_VALPKD04)))+'-'+ALLTRIM(STR(MONTH(.w_VALPKD04)))+'-'+ALLTRIM(STR(DAY(.w_VALPKD04)))+'}',"")))
            .w_LTKEY_05 = IIF(EMPTY(.w_PKFLD_05),"", '"'+ALLTRIM(.w_PKFLD_05)+'", '+IIF(.w_GETVFC05='S', .w_VALPKT05, ICASE(.w_FLTYPE05='C', '"'+ALLTRIM(.w_VALPKC05)+'"', .w_FLTYPE05='N', ALLTRIM(STR(.w_VALPKN05)), .w_FLTYPE05='D', '{^'+ALLTRIM(STR(YEAR(.w_VALPKD05)))+'-'+ALLTRIM(STR(MONTH(.w_VALPKD05)))+'-'+ALLTRIM(STR(DAY(.w_VALPKD05)))+'}',"")))
        Local l_Dep1,l_Dep2,l_Dep3,l_Dep4,l_Dep5,l_Dep6
        l_Dep1= .o_DESTABLE<>.w_DESTABLE .or. .o_DESFIELD<>.w_DESFIELD .or. .o_VALPKC01<>.w_VALPKC01 .or. .o_VALPKC02<>.w_VALPKC02 .or. .o_VALPKC03<>.w_VALPKC03
        l_Dep2= .o_VALPKC04<>.w_VALPKC04 .or. .o_VALPKC05<>.w_VALPKC05 .or. .o_VALPKN01<>.w_VALPKN01 .or. .o_VALPKN02<>.w_VALPKN02 .or. .o_VALPKN03<>.w_VALPKN03
        l_Dep3= .o_VALPKN04<>.w_VALPKN04 .or. .o_VALPKN05<>.w_VALPKN05 .or. .o_VALPKD01<>.w_VALPKD01 .or. .o_VALPKD02<>.w_VALPKD02 .or. .o_VALPKD03<>.w_VALPKD03
        l_Dep4= .o_VALPKD04<>.w_VALPKD04 .or. .o_VALPKD05<>.w_VALPKD05 .or. .o_VALPKT01<>.w_VALPKT01 .or. .o_VALPKT02<>.w_VALPKT02 .or. .o_VALPKT03<>.w_VALPKT03
        l_Dep5= .o_VALPKT04<>.w_VALPKT04 .or. .o_VALPKT05<>.w_VALPKT05 .or. .o_GETVFC01<>.w_GETVFC01 .or. .o_GETVFC02<>.w_GETVFC02 .or. .o_GETVFC03<>.w_GETVFC03
        l_Dep6= .o_GETVFC04<>.w_GETVFC04 .or. .o_GETVFC05<>.w_GETVFC05
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3 .or. m.l_Dep4 .or. m.l_Dep5 .or. m.l_Dep6
            .w_LTSINTAX = 'LookTab("'+ALLTRIM(.w_DESTABLE)+'", "'+ALLTRIM(.w_DESFIELD)+'"'+IIF(EMPTY(.w_LTKEY_01),"", ', '+ALLTRIM(.w_LTKEY_01))+IIF(EMPTY(.w_LTKEY_02),"", ', '+ALLTRIM(.w_LTKEY_02))+IIF(EMPTY(.w_LTKEY_03),"", ', '+ALLTRIM(.w_LTKEY_03))+IIF(EMPTY(.w_LTKEY_04),"", ', '+ALLTRIM(.w_LTKEY_04))+IIF(EMPTY(.w_LTKEY_05),"", ', '+ALLTRIM(.w_LTKEY_05))+')'
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(51,51,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.LBLPK_01.Calculate(IIF(EMPTY(.w_PKFLD_01),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_01))))
        .oPgFrm.Page1.oPag.LBLPK_02.Calculate(IIF(EMPTY(.w_PKFLD_02),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_02))))
        .oPgFrm.Page1.oPag.LBLPK_03.Calculate(IIF(EMPTY(.w_PKFLD_03),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_03))))
        .oPgFrm.Page1.oPag.LBLPK_04.Calculate(IIF(EMPTY(.w_PKFLD_04),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_04))))
        .oPgFrm.Page1.oPag.LBLPK_05.Calculate(IIF(EMPTY(.w_PKFLD_05),"",ah_msgformat("Valore per campo chiave %1:", ALLTRIM(.w_PKFLD_05))))
        .oPgFrm.Page2.oPag.VIS__CUR.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDESFIELD_1_12.enabled = this.oPgFrm.Page1.oPag.oDESFIELD_1_12.mCond()
    this.oPgFrm.Page1.oPag.oDESFIELD_1_13.enabled = this.oPgFrm.Page1.oPag.oDESFIELD_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(EMPTY(this.w_PRINTCUR))
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Mostra cursore"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oVALPKC01_1_20.visible=!this.oPgFrm.Page1.oPag.oVALPKC01_1_20.mHide()
    this.oPgFrm.Page1.oPag.oVALPKC02_1_26.visible=!this.oPgFrm.Page1.oPag.oVALPKC02_1_26.mHide()
    this.oPgFrm.Page1.oPag.oVALPKC03_1_27.visible=!this.oPgFrm.Page1.oPag.oVALPKC03_1_27.mHide()
    this.oPgFrm.Page1.oPag.oVALPKC04_1_28.visible=!this.oPgFrm.Page1.oPag.oVALPKC04_1_28.mHide()
    this.oPgFrm.Page1.oPag.oVALPKC05_1_29.visible=!this.oPgFrm.Page1.oPag.oVALPKC05_1_29.mHide()
    this.oPgFrm.Page1.oPag.oVALPKN01_1_30.visible=!this.oPgFrm.Page1.oPag.oVALPKN01_1_30.mHide()
    this.oPgFrm.Page1.oPag.oVALPKN02_1_31.visible=!this.oPgFrm.Page1.oPag.oVALPKN02_1_31.mHide()
    this.oPgFrm.Page1.oPag.oVALPKN03_1_32.visible=!this.oPgFrm.Page1.oPag.oVALPKN03_1_32.mHide()
    this.oPgFrm.Page1.oPag.oVALPKN04_1_33.visible=!this.oPgFrm.Page1.oPag.oVALPKN04_1_33.mHide()
    this.oPgFrm.Page1.oPag.oVALPKN05_1_34.visible=!this.oPgFrm.Page1.oPag.oVALPKN05_1_34.mHide()
    this.oPgFrm.Page1.oPag.oVALPKD01_1_35.visible=!this.oPgFrm.Page1.oPag.oVALPKD01_1_35.mHide()
    this.oPgFrm.Page1.oPag.oVALPKD02_1_36.visible=!this.oPgFrm.Page1.oPag.oVALPKD02_1_36.mHide()
    this.oPgFrm.Page1.oPag.oVALPKD03_1_37.visible=!this.oPgFrm.Page1.oPag.oVALPKD03_1_37.mHide()
    this.oPgFrm.Page1.oPag.oVALPKD04_1_38.visible=!this.oPgFrm.Page1.oPag.oVALPKD04_1_38.mHide()
    this.oPgFrm.Page1.oPag.oVALPKD05_1_39.visible=!this.oPgFrm.Page1.oPag.oVALPKD05_1_39.mHide()
    this.oPgFrm.Page1.oPag.oGETVFC01_1_41.visible=!this.oPgFrm.Page1.oPag.oGETVFC01_1_41.mHide()
    this.oPgFrm.Page1.oPag.oVALPKT01_1_42.visible=!this.oPgFrm.Page1.oPag.oVALPKT01_1_42.mHide()
    this.oPgFrm.Page1.oPag.oCURTOTRE_1_43.visible=!this.oPgFrm.Page1.oPag.oCURTOTRE_1_43.mHide()
    this.oPgFrm.Page1.oPag.oGETVFC02_1_44.visible=!this.oPgFrm.Page1.oPag.oGETVFC02_1_44.mHide()
    this.oPgFrm.Page1.oPag.oVALPKT02_1_45.visible=!this.oPgFrm.Page1.oPag.oVALPKT02_1_45.mHide()
    this.oPgFrm.Page1.oPag.oGETVFC03_1_46.visible=!this.oPgFrm.Page1.oPag.oGETVFC03_1_46.mHide()
    this.oPgFrm.Page1.oPag.oVALPKT03_1_47.visible=!this.oPgFrm.Page1.oPag.oVALPKT03_1_47.mHide()
    this.oPgFrm.Page1.oPag.oGETVFC04_1_48.visible=!this.oPgFrm.Page1.oPag.oGETVFC04_1_48.mHide()
    this.oPgFrm.Page1.oPag.oVALPKT04_1_49.visible=!this.oPgFrm.Page1.oPag.oVALPKT04_1_49.mHide()
    this.oPgFrm.Page1.oPag.oGETVFC05_1_50.visible=!this.oPgFrm.Page1.oPag.oGETVFC05_1_50.mHide()
    this.oPgFrm.Page1.oPag.oVALPKT05_1_51.visible=!this.oPgFrm.Page1.oPag.oVALPKT05_1_51.mHide()
    this.oPgFrm.Page1.oPag.oLTROWDAT_1_54.visible=!this.oPgFrm.Page1.oPag.oLTROWDAT_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsar_klt
    If cEvent="w_DESTABLE Changed"
      Local oCtrl
      oCtrl=This.GetCtrl("w_DESFIELD")
      oCtrl.Clear()
      oCtrl.Init()
      oCtrl=This.GetCtrl("w_DESFIELD",2)
      oCtrl.Clear()
      oCtrl.Init()
      oCtrl=.NULL.
    EndIf
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.LBLPK_01.Event(cEvent)
      .oPgFrm.Page1.oPag.LBLPK_02.Event(cEvent)
      .oPgFrm.Page1.oPag.LBLPK_03.Event(cEvent)
      .oPgFrm.Page1.oPag.LBLPK_04.Event(cEvent)
      .oPgFrm.Page1.oPag.LBLPK_05.Event(cEvent)
      .oPgFrm.Page2.oPag.VIS__CUR.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsar_klt
    If cEvent='Test'
       Try
         Local L_LTRESULT, L_LTSINTAX
         Local L_RetMsgErr
         L_RetMsgErr = ah_msgformat("Errore eseguendo looktab.%0Verificare sintassi looktab e/o valori impostati")
         If !EMPTY(this.w_PRINTCUR)
           SELECT (this.w_PRINTCUR)
           GO TOP
           GO (this.w_LTROWDAT)
         endif
         L_LTSINTAX=this.w_LTSINTAX
         Local L_Res
         L_LTSINTAX="adhoc_" + STRTRAN(L_LTSINTAX,"(","(@L_Res , ", 1, 1)
         L_LTRESULT=&L_LTSINTAX
         If !EMPTY(this.w_PRINTCUR)
           SELECT (this.w_PRINTCUR)
           GO TOP
         endif
       Catch
         L_LTRESULT = L_RetMsgErr
       Finally
         this.w_LTRESULT = ah_msgformat("Risultato esecuzione looktab:%0%1", ICASE(L_res>0, ICASE(isnull(L_LTRESULT), "<NULL>", EMPTY(L_LTRESULT),"<Vuoto>",L_LTRESULT), L_Res=0,"Nessun record trovato",L_RetMsgErr))
       EndTry
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PKFLD_01
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PKFLD_01) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PKFLD_01)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_PKFLD_01);
                   +" and TBNAME="+cp_ToStrODBC(this.w_DESTABLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_DESTABLE;
                       ,'FLNAME',this.w_PKFLD_01)
            select TBNAME,FLNAME,FLTYPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PKFLD_01 = NVL(_Link_.FLNAME,space(30))
      this.w_FLTYPE01 = NVL(_Link_.FLTYPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PKFLD_01 = space(30)
      endif
      this.w_FLTYPE01 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PKFLD_01 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PKFLD_02
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PKFLD_02) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PKFLD_02)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_PKFLD_02);
                   +" and TBNAME="+cp_ToStrODBC(this.w_DESTABLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_DESTABLE;
                       ,'FLNAME',this.w_PKFLD_02)
            select TBNAME,FLNAME,FLTYPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PKFLD_02 = NVL(_Link_.FLNAME,space(30))
      this.w_FLTYPE02 = NVL(_Link_.FLTYPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PKFLD_02 = space(30)
      endif
      this.w_FLTYPE02 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PKFLD_02 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PKFLD_03
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PKFLD_03) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PKFLD_03)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_PKFLD_03);
                   +" and TBNAME="+cp_ToStrODBC(this.w_DESTABLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_DESTABLE;
                       ,'FLNAME',this.w_PKFLD_03)
            select TBNAME,FLNAME,FLTYPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PKFLD_03 = NVL(_Link_.FLNAME,space(30))
      this.w_FLTYPE03 = NVL(_Link_.FLTYPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PKFLD_03 = space(30)
      endif
      this.w_FLTYPE03 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PKFLD_03 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PKFLD_04
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PKFLD_04) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PKFLD_04)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_PKFLD_04);
                   +" and TBNAME="+cp_ToStrODBC(this.w_DESTABLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_DESTABLE;
                       ,'FLNAME',this.w_PKFLD_04)
            select TBNAME,FLNAME,FLTYPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PKFLD_04 = NVL(_Link_.FLNAME,space(30))
      this.w_FLTYPE04 = NVL(_Link_.FLTYPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PKFLD_04 = space(30)
      endif
      this.w_FLTYPE04 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PKFLD_04 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PKFLD_05
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PKFLD_05) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PKFLD_05)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_PKFLD_05);
                   +" and TBNAME="+cp_ToStrODBC(this.w_DESTABLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_DESTABLE;
                       ,'FLNAME',this.w_PKFLD_05)
            select TBNAME,FLNAME,FLTYPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PKFLD_05 = NVL(_Link_.FLNAME,space(30))
      this.w_FLTYPE05 = NVL(_Link_.FLTYPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PKFLD_05 = space(30)
      endif
      this.w_FLTYPE05 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PKFLD_05 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDESTABLE_1_1.RadioValue()==this.w_DESTABLE)
      this.oPgFrm.Page1.oPag.oDESTABLE_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTABLE_1_2.RadioValue()==this.w_DESTABLE)
      this.oPgFrm.Page1.oPag.oDESTABLE_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIELD_1_12.RadioValue()==this.w_DESFIELD)
      this.oPgFrm.Page1.oPag.oDESFIELD_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIELD_1_13.RadioValue()==this.w_DESFIELD)
      this.oPgFrm.Page1.oPag.oDESFIELD_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKC01_1_20.value==this.w_VALPKC01)
      this.oPgFrm.Page1.oPag.oVALPKC01_1_20.value=this.w_VALPKC01
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKC02_1_26.value==this.w_VALPKC02)
      this.oPgFrm.Page1.oPag.oVALPKC02_1_26.value=this.w_VALPKC02
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKC03_1_27.value==this.w_VALPKC03)
      this.oPgFrm.Page1.oPag.oVALPKC03_1_27.value=this.w_VALPKC03
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKC04_1_28.value==this.w_VALPKC04)
      this.oPgFrm.Page1.oPag.oVALPKC04_1_28.value=this.w_VALPKC04
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKC05_1_29.value==this.w_VALPKC05)
      this.oPgFrm.Page1.oPag.oVALPKC05_1_29.value=this.w_VALPKC05
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKN01_1_30.value==this.w_VALPKN01)
      this.oPgFrm.Page1.oPag.oVALPKN01_1_30.value=this.w_VALPKN01
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKN02_1_31.value==this.w_VALPKN02)
      this.oPgFrm.Page1.oPag.oVALPKN02_1_31.value=this.w_VALPKN02
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKN03_1_32.value==this.w_VALPKN03)
      this.oPgFrm.Page1.oPag.oVALPKN03_1_32.value=this.w_VALPKN03
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKN04_1_33.value==this.w_VALPKN04)
      this.oPgFrm.Page1.oPag.oVALPKN04_1_33.value=this.w_VALPKN04
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKN05_1_34.value==this.w_VALPKN05)
      this.oPgFrm.Page1.oPag.oVALPKN05_1_34.value=this.w_VALPKN05
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKD01_1_35.value==this.w_VALPKD01)
      this.oPgFrm.Page1.oPag.oVALPKD01_1_35.value=this.w_VALPKD01
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKD02_1_36.value==this.w_VALPKD02)
      this.oPgFrm.Page1.oPag.oVALPKD02_1_36.value=this.w_VALPKD02
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKD03_1_37.value==this.w_VALPKD03)
      this.oPgFrm.Page1.oPag.oVALPKD03_1_37.value=this.w_VALPKD03
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKD04_1_38.value==this.w_VALPKD04)
      this.oPgFrm.Page1.oPag.oVALPKD04_1_38.value=this.w_VALPKD04
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKD05_1_39.value==this.w_VALPKD05)
      this.oPgFrm.Page1.oPag.oVALPKD05_1_39.value=this.w_VALPKD05
    endif
    if not(this.oPgFrm.Page1.oPag.oGETVFC01_1_41.RadioValue()==this.w_GETVFC01)
      this.oPgFrm.Page1.oPag.oGETVFC01_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKT01_1_42.RadioValue()==this.w_VALPKT01)
      this.oPgFrm.Page1.oPag.oVALPKT01_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCURTOTRE_1_43.value==this.w_CURTOTRE)
      this.oPgFrm.Page1.oPag.oCURTOTRE_1_43.value=this.w_CURTOTRE
    endif
    if not(this.oPgFrm.Page1.oPag.oGETVFC02_1_44.RadioValue()==this.w_GETVFC02)
      this.oPgFrm.Page1.oPag.oGETVFC02_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKT02_1_45.RadioValue()==this.w_VALPKT02)
      this.oPgFrm.Page1.oPag.oVALPKT02_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGETVFC03_1_46.RadioValue()==this.w_GETVFC03)
      this.oPgFrm.Page1.oPag.oGETVFC03_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKT03_1_47.RadioValue()==this.w_VALPKT03)
      this.oPgFrm.Page1.oPag.oVALPKT03_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGETVFC04_1_48.RadioValue()==this.w_GETVFC04)
      this.oPgFrm.Page1.oPag.oGETVFC04_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKT04_1_49.RadioValue()==this.w_VALPKT04)
      this.oPgFrm.Page1.oPag.oVALPKT04_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGETVFC05_1_50.RadioValue()==this.w_GETVFC05)
      this.oPgFrm.Page1.oPag.oGETVFC05_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPKT05_1_51.RadioValue()==this.w_VALPKT05)
      this.oPgFrm.Page1.oPag.oVALPKT05_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLTROWDAT_1_54.value==this.w_LTROWDAT)
      this.oPgFrm.Page1.oPag.oLTROWDAT_1_54.value=this.w_LTROWDAT
    endif
    if not(this.oPgFrm.Page1.oPag.oLTSINTAX_1_58.value==this.w_LTSINTAX)
      this.oPgFrm.Page1.oPag.oLTSINTAX_1_58.value=this.w_LTSINTAX
    endif
    if not(this.oPgFrm.Page1.oPag.oLTRESULT_1_60.value==this.w_LTRESULT)
      this.oPgFrm.Page1.oPag.oLTRESULT_1_60.value=this.w_LTRESULT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_PRINTCUR) OR (.w_LTROWDAT>0 AND .w_LTROWDAT<=.w_CURTOTRE))  and not(EMPTY(.w_PRINTCUR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLTROWDAT_1_54.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero di record da utilizzare per prelevare i dati d'esempio deve essere maggiore di zero e minore o uguale al numero di record totali presnti nel cursore")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_klt
      if ah_yesno("Copiare la sintassi della looktab negli appunti?")
        _Cliptext=this.w_LTSINTAX
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DESTABLE = this.w_DESTABLE
    this.o_DESFIELD = this.w_DESFIELD
    this.o_VALPKC01 = this.w_VALPKC01
    this.o_VALPKC02 = this.w_VALPKC02
    this.o_VALPKC03 = this.w_VALPKC03
    this.o_VALPKC04 = this.w_VALPKC04
    this.o_VALPKC05 = this.w_VALPKC05
    this.o_VALPKN01 = this.w_VALPKN01
    this.o_VALPKN02 = this.w_VALPKN02
    this.o_VALPKN03 = this.w_VALPKN03
    this.o_VALPKN04 = this.w_VALPKN04
    this.o_VALPKN05 = this.w_VALPKN05
    this.o_VALPKD01 = this.w_VALPKD01
    this.o_VALPKD02 = this.w_VALPKD02
    this.o_VALPKD03 = this.w_VALPKD03
    this.o_VALPKD04 = this.w_VALPKD04
    this.o_VALPKD05 = this.w_VALPKD05
    this.o_PRINTCUR = this.w_PRINTCUR
    this.o_GETVFC01 = this.w_GETVFC01
    this.o_VALPKT01 = this.w_VALPKT01
    this.o_GETVFC02 = this.w_GETVFC02
    this.o_VALPKT02 = this.w_VALPKT02
    this.o_GETVFC03 = this.w_GETVFC03
    this.o_VALPKT03 = this.w_VALPKT03
    this.o_GETVFC04 = this.w_GETVFC04
    this.o_VALPKT04 = this.w_VALPKT04
    this.o_GETVFC05 = this.w_GETVFC05
    this.o_VALPKT05 = this.w_VALPKT05
    return

enddefine

* --- Define pages as container
define class tgsar_kltPag1 as StdContainer
  Width  = 712
  height = 433
  stdWidth  = 712
  stdheight = 433
  resizeXpos=481
  resizeYpos=289
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oDESTABLE_1_1 as StdTableCombo with uid="QEOAATVPWC",rtseq=1,rtrep=.f.,left=229,top=14,width=153,height=21;
    , ToolTipText = "Tabella da cui prelevare i dati";
    , HelpContextID = 109051269;
    , cFormVar="w_DESTABLE",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='XDC_TABLE',cKey='TBNAME',cValue='TBNAME',cOrderBy='TBNAME',xDefault=space(20);
  , bGlobalFont=.t.


  func oDESTABLE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_PKFLD_01)
        bRes2=.link_1_7('Full')
      endif
      if .not. empty(.w_PKFLD_02)
        bRes2=.link_1_8('Full')
      endif
      if .not. empty(.w_PKFLD_03)
        bRes2=.link_1_9('Full')
      endif
      if .not. empty(.w_PKFLD_04)
        bRes2=.link_1_10('Full')
      endif
      if .not. empty(.w_PKFLD_05)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oDESTABLE_1_2 as StdTableCombo with uid="TAXJMMMZKF",rtseq=2,rtrep=.f.,left=383,top=14,width=276,height=21;
    , ToolTipText = "Tabella da cui prelevare i dati";
    , HelpContextID = 109051269;
    , cFormVar="w_DESTABLE",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='XDC_TABLE',cKey='TBNAME',cValue='TBCOMMENT',cOrderBy='TBCOMMENT',xDefault=space(20);
  , bGlobalFont=.t.


  func oDESTABLE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_PKFLD_01)
        bRes2=.link_1_7('Full')
      endif
      if .not. empty(.w_PKFLD_02)
        bRes2=.link_1_8('Full')
      endif
      if .not. empty(.w_PKFLD_03)
        bRes2=.link_1_9('Full')
      endif
      if .not. empty(.w_PKFLD_04)
        bRes2=.link_1_10('Full')
      endif
      if .not. empty(.w_PKFLD_05)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oDESFIELD_1_12 as StdTableCombo with uid="VDLCLWXUNJ",rtseq=10,rtrep=.f.,left=229,top=39,width=153,height=21;
    , ToolTipText = "Campo il cui valore verr� restituito dalla looktab";
    , HelpContextID = 51248518;
    , cFormVar="w_DESFIELD",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='gsar_klt.vqr',cKey='FLNAME',cValue='FLNAME',cOrderBy='FLNAME',xDefault=space(30);
  , bGlobalFont=.t.


  func oDESFIELD_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUM_KEYS<5)
    endwith
   endif
  endfunc


  add object oDESFIELD_1_13 as StdTableCombo with uid="KAVLYSZHZE",rtseq=11,rtrep=.f.,left=383,top=39,width=276,height=21;
    , ToolTipText = "Campo il cui valore verr� restituito dalla looktab";
    , HelpContextID = 51248518;
    , cFormVar="w_DESFIELD",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='gsar_klt.vqr',cKey='FLNAME',cValue='FLCOMMEN',cOrderBy='FLCOMMEN',xDefault=space(30);
  , bGlobalFont=.t.


  func oDESFIELD_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUM_KEYS<5)
    endwith
   endif
  endfunc


  add object LBLPK_01 as cp_calclbl with uid="XJTEEIRJSA",left=24, top=91, width=358,height=19,;
    caption='LBLPK_01',;
   bGlobalFont=.t.,;
    caption="label text",Alignment=1,;
    nPag=1;
    , HelpContextID = 119246823


  add object LBLPK_02 as cp_calclbl with uid="RLMXZDGSCC",left=24, top=113, width=358,height=19,;
    caption='LBLPK_02',;
   bGlobalFont=.t.,;
    caption="label text",Alignment=1,;
    nPag=1;
    , HelpContextID = 119246824


  add object LBLPK_03 as cp_calclbl with uid="FFTJVTPEQP",left=24, top=135, width=358,height=19,;
    caption='LBLPK_03',;
   bGlobalFont=.t.,;
    caption="label text",Alignment=1,;
    nPag=1;
    , HelpContextID = 119246825


  add object LBLPK_04 as cp_calclbl with uid="QYSDZDJAGR",left=24, top=157, width=358,height=19,;
    caption='LBLPK_04',;
   bGlobalFont=.t.,;
    caption="label text",Alignment=1,;
    nPag=1;
    , HelpContextID = 119246826


  add object LBLPK_05 as cp_calclbl with uid="LPIJVZFJSX",left=24, top=179, width=358,height=19,;
    caption='LBLPK_05',;
   bGlobalFont=.t.,;
    caption="label text",Alignment=1,;
    nPag=1;
    , HelpContextID = 119246827

  add object oVALPKC01_1_20 as StdField with uid="PSPBAQJDYL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_VALPKC01", cQueryName = "VALPKC01",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 82079865,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=388, Top=91, InputMask=replicate('X',20)

  func oVALPKC01_1_20.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_01) OR .w_FLTYPE01<>'C' OR .w_GETVFC01='S')
    endwith
  endfunc

  add object oVALPKC02_1_26 as StdField with uid="BAWKFETILR",rtseq=18,rtrep=.f.,;
    cFormVar = "w_VALPKC02", cQueryName = "VALPKC02",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 82079864,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=388, Top=113, InputMask=replicate('X',20)

  func oVALPKC02_1_26.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_02) OR .w_FLTYPE02<>'C' OR .w_GETVFC02='S')
    endwith
  endfunc

  add object oVALPKC03_1_27 as StdField with uid="KZKCETMNWX",rtseq=19,rtrep=.f.,;
    cFormVar = "w_VALPKC03", cQueryName = "VALPKC03",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 82079863,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=388, Top=135, InputMask=replicate('X',20)

  func oVALPKC03_1_27.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_03) OR .w_FLTYPE03<>'C' OR .w_GETVFC03='S')
    endwith
  endfunc

  add object oVALPKC04_1_28 as StdField with uid="BXKONHSYEZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_VALPKC04", cQueryName = "VALPKC04",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 82079862,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=388, Top=157, InputMask=replicate('X',20)

  func oVALPKC04_1_28.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_04) OR .w_FLTYPE04<>'C' OR .w_GETVFC04='S')
    endwith
  endfunc

  add object oVALPKC05_1_29 as StdField with uid="WOQPGHMRGU",rtseq=21,rtrep=.f.,;
    cFormVar = "w_VALPKC05", cQueryName = "VALPKC05",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 82079861,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=388, Top=179, InputMask=replicate('X',20)

  func oVALPKC05_1_29.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_05) OR .w_FLTYPE05<>'C' OR .w_GETVFC05='S')
    endwith
  endfunc

  add object oVALPKN01_1_30 as StdField with uid="OHEFWEBRQV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_VALPKN01", cQueryName = "VALPKN01",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 102469511,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=388, Top=91

  func oVALPKN01_1_30.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_01) OR .w_FLTYPE01<>'N' OR .w_GETVFC01='S')
    endwith
  endfunc

  add object oVALPKN02_1_31 as StdField with uid="WWZDPDLNKA",rtseq=23,rtrep=.f.,;
    cFormVar = "w_VALPKN02", cQueryName = "VALPKN02",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 102469512,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=388, Top=113

  func oVALPKN02_1_31.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_02) OR .w_FLTYPE02<>'N' OR .w_GETVFC02='S')
    endwith
  endfunc

  add object oVALPKN03_1_32 as StdField with uid="VMUAXLKDPI",rtseq=24,rtrep=.f.,;
    cFormVar = "w_VALPKN03", cQueryName = "VALPKN03",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 102469513,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=388, Top=135

  func oVALPKN03_1_32.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_03) OR .w_FLTYPE03<>'N' OR .w_GETVFC03='S')
    endwith
  endfunc

  add object oVALPKN04_1_33 as StdField with uid="WVXDMOUFRU",rtseq=25,rtrep=.f.,;
    cFormVar = "w_VALPKN04", cQueryName = "VALPKN04",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 102469514,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=388, Top=157

  func oVALPKN04_1_33.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_04) OR .w_FLTYPE04<>'N' OR .w_GETVFC04='S')
    endwith
  endfunc

  add object oVALPKN05_1_34 as StdField with uid="JZQHHBSEZR",rtseq=26,rtrep=.f.,;
    cFormVar = "w_VALPKN05", cQueryName = "VALPKN05",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore da passare alla chiave della looktab",;
    HelpContextID = 102469515,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=388, Top=179

  func oVALPKN05_1_34.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_05) OR .w_FLTYPE05<>'N' OR .w_GETVFC05='S')
    endwith
  endfunc

  add object oVALPKD01_1_35 as StdField with uid="DIVGXZFXBZ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_VALPKD01", cQueryName = "VALPKD01",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Valore da passare alla chiave della looktab",;
    HelpContextID = 65302649,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=388, Top=91, bHasZoom = .t. 

  func oVALPKD01_1_35.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_01) OR .w_FLTYPE01<>'D' OR .w_GETVFC01='S')
    endwith
  endfunc

  proc oVALPKD01_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with '','*','',cp_AbsName(this.parent,'oVALPKD01_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oVALPKD02_1_36 as StdField with uid="BTMPXQJEEJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_VALPKD02", cQueryName = "VALPKD02",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Valore da passare alla chiave della looktab",;
    HelpContextID = 65302648,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=388, Top=113, bHasZoom = .t. 

  func oVALPKD02_1_36.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_02) OR .w_FLTYPE02<>'D' OR .w_GETVFC02='S')
    endwith
  endfunc

  proc oVALPKD02_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with '','*','',cp_AbsName(this.parent,'oVALPKD02_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oVALPKD03_1_37 as StdField with uid="VURYZOURJG",rtseq=29,rtrep=.f.,;
    cFormVar = "w_VALPKD03", cQueryName = "VALPKD03",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Valore da passare alla chiave della looktab",;
    HelpContextID = 65302647,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=388, Top=135, bHasZoom = .t. 

  func oVALPKD03_1_37.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_03) OR .w_FLTYPE03<>'D' OR .w_GETVFC03='S')
    endwith
  endfunc

  proc oVALPKD03_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with '','*','',cp_AbsName(this.parent,'oVALPKD03_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oVALPKD04_1_38 as StdField with uid="UJMFNZPVEY",rtseq=30,rtrep=.f.,;
    cFormVar = "w_VALPKD04", cQueryName = "VALPKD04",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Valore da passare alla chiave della looktab",;
    HelpContextID = 65302646,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=388, Top=157, bHasZoom = .t. 

  func oVALPKD04_1_38.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_04) OR .w_FLTYPE04<>'D' OR .w_GETVFC04='S')
    endwith
  endfunc

  proc oVALPKD04_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with '','*','',cp_AbsName(this.parent,'oVALPKD04_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oVALPKD05_1_39 as StdField with uid="AHRUHDJLVV",rtseq=31,rtrep=.f.,;
    cFormVar = "w_VALPKD05", cQueryName = "VALPKD05",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 65302645,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=388, Top=179, bHasZoom = .t. 

  func oVALPKD05_1_39.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_05) OR .w_FLTYPE05<>'D' OR .w_GETVFC05='S')
    endwith
  endfunc

  proc oVALPKD05_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with '','*','',cp_AbsName(this.parent,'oVALPKD05_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oGETVFC01_1_41 as StdCheck with uid="ZRJVUJYKNI",rtseq=33,rtrep=.f.,left=547, top=91, caption="Valore da cursore",;
    ToolTipText = "Se attivo il valore da passare alla chiave della tabella viene prelevato dal cursore, altrimenti sar� possibile inserire un valore manualmente",;
    HelpContextID = 86895977,;
    cFormVar="w_GETVFC01", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGETVFC01_1_41.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGETVFC01_1_41.GetRadio()
    this.Parent.oContained.w_GETVFC01 = this.RadioValue()
    return .t.
  endfunc

  func oGETVFC01_1_41.SetRadio()
    this.Parent.oContained.w_GETVFC01=trim(this.Parent.oContained.w_GETVFC01)
    this.value = ;
      iif(this.Parent.oContained.w_GETVFC01=='S',1,;
      0)
  endfunc

  func oGETVFC01_1_41.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_01) OR EMPTY(.w_PRINTCUR))
    endwith
  endfunc


  add object oVALPKT01_1_42 as ah_CurFieldList with uid="KFTIKIYHLY",rtseq=34,rtrep=.f.,left=388,top=91,width=153,height=21;
    , ToolTipText = "Campo del cursore da cui prelevare il valore da passare alla chiave della looktab";
    , HelpContextID = 203132807;
    , cFormVar="w_VALPKT01",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(20);
  , bGlobalFont=.t.


  func oVALPKT01_1_42.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_01) OR EMPTY(.w_PRINTCUR) OR .w_GETVFC01<>'S')
    endwith
  endfunc

  add object oCURTOTRE_1_43 as StdField with uid="YNFYLTOQWQ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CURTOTRE", cQueryName = "CURTOTRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 207618667,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=463, Top=207

  func oCURTOTRE_1_43.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PRINTCUR))
    endwith
  endfunc

  add object oGETVFC02_1_44 as StdCheck with uid="OFXBKZFPEJ",rtseq=36,rtrep=.f.,left=547, top=113, caption="Valore da cursore",;
    ToolTipText = "Se attivo il valore da passare alla chiave della tabella viene prelevato dal cursore, altrimenti sar� possibile inserire un valore manualmente",;
    HelpContextID = 86895976,;
    cFormVar="w_GETVFC02", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGETVFC02_1_44.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGETVFC02_1_44.GetRadio()
    this.Parent.oContained.w_GETVFC02 = this.RadioValue()
    return .t.
  endfunc

  func oGETVFC02_1_44.SetRadio()
    this.Parent.oContained.w_GETVFC02=trim(this.Parent.oContained.w_GETVFC02)
    this.value = ;
      iif(this.Parent.oContained.w_GETVFC02=='S',1,;
      0)
  endfunc

  func oGETVFC02_1_44.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_02) OR EMPTY(.w_PRINTCUR))
    endwith
  endfunc


  add object oVALPKT02_1_45 as ah_CurFieldList with uid="XYXHCHRFGP",rtseq=37,rtrep=.f.,left=388,top=113,width=153,height=21;
    , ToolTipText = "Campo del cursore da cui prelevare il valore da passare alla chiave della looktab";
    , HelpContextID = 203132808;
    , cFormVar="w_VALPKT02",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(20);
  , bGlobalFont=.t.


  func oVALPKT02_1_45.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_02) OR EMPTY(.w_PRINTCUR) OR .w_GETVFC02<>'S')
    endwith
  endfunc

  add object oGETVFC03_1_46 as StdCheck with uid="JKLRVCJSWC",rtseq=38,rtrep=.f.,left=547, top=135, caption="Valore da cursore",;
    ToolTipText = "Se attivo il valore da passare alla chiave della tabella viene prelevato dal cursore, altrimenti sar� possibile inserire un valore manualmente",;
    HelpContextID = 86895975,;
    cFormVar="w_GETVFC03", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGETVFC03_1_46.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGETVFC03_1_46.GetRadio()
    this.Parent.oContained.w_GETVFC03 = this.RadioValue()
    return .t.
  endfunc

  func oGETVFC03_1_46.SetRadio()
    this.Parent.oContained.w_GETVFC03=trim(this.Parent.oContained.w_GETVFC03)
    this.value = ;
      iif(this.Parent.oContained.w_GETVFC03=='S',1,;
      0)
  endfunc

  func oGETVFC03_1_46.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_03) OR EMPTY(.w_PRINTCUR))
    endwith
  endfunc


  add object oVALPKT03_1_47 as ah_CurFieldList with uid="VTRQLJTLZV",rtseq=39,rtrep=.f.,left=388,top=135,width=153,height=21;
    , ToolTipText = "Campo del cursore da cui prelevare il valore da passare alla chiave della looktab";
    , HelpContextID = 203132809;
    , cFormVar="w_VALPKT03",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(20);
  , bGlobalFont=.t.


  func oVALPKT03_1_47.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_03) OR EMPTY(.w_PRINTCUR) OR .w_GETVFC03<>'S')
    endwith
  endfunc

  add object oGETVFC04_1_48 as StdCheck with uid="DVXKLJQPZZ",rtseq=40,rtrep=.f.,left=547, top=157, caption="Valore da cursore",;
    ToolTipText = "Se attivo il valore da passare alla chiave della tabella viene prelevato dal cursore, altrimenti sar� possibile inserire un valore manualmente",;
    HelpContextID = 86895974,;
    cFormVar="w_GETVFC04", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGETVFC04_1_48.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGETVFC04_1_48.GetRadio()
    this.Parent.oContained.w_GETVFC04 = this.RadioValue()
    return .t.
  endfunc

  func oGETVFC04_1_48.SetRadio()
    this.Parent.oContained.w_GETVFC04=trim(this.Parent.oContained.w_GETVFC04)
    this.value = ;
      iif(this.Parent.oContained.w_GETVFC04=='S',1,;
      0)
  endfunc

  func oGETVFC04_1_48.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_04) OR EMPTY(.w_PRINTCUR))
    endwith
  endfunc


  add object oVALPKT04_1_49 as ah_CurFieldList with uid="ZTZPOYDNTU",rtseq=41,rtrep=.f.,left=388,top=157,width=153,height=21;
    , ToolTipText = "Campo del cursore da cui prelevare il valore da passare alla chiave della looktab";
    , HelpContextID = 203132810;
    , cFormVar="w_VALPKT04",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(20);
  , bGlobalFont=.t.


  func oVALPKT04_1_49.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_04) OR EMPTY(.w_PRINTCUR) OR .w_GETVFC04<>'S')
    endwith
  endfunc

  add object oGETVFC05_1_50 as StdCheck with uid="CNAUARLKSR",rtseq=42,rtrep=.f.,left=547, top=179, caption="Valore da cursore",;
    ToolTipText = "Se attivo il valore da passare alla chiave della tabella viene prelevato dal cursore, altrimenti sar� possibile inserire un valore manualmente",;
    HelpContextID = 86895973,;
    cFormVar="w_GETVFC05", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGETVFC05_1_50.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGETVFC05_1_50.GetRadio()
    this.Parent.oContained.w_GETVFC05 = this.RadioValue()
    return .t.
  endfunc

  func oGETVFC05_1_50.SetRadio()
    this.Parent.oContained.w_GETVFC05=trim(this.Parent.oContained.w_GETVFC05)
    this.value = ;
      iif(this.Parent.oContained.w_GETVFC05=='S',1,;
      0)
  endfunc

  func oGETVFC05_1_50.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_05) OR EMPTY(.w_PRINTCUR))
    endwith
  endfunc


  add object oVALPKT05_1_51 as ah_CurFieldList with uid="VMFIRXLTAI",rtseq=43,rtrep=.f.,left=388,top=179,width=153,height=21;
    , ToolTipText = "Campo del cursore da cui prelevare il valore da passare alla chiave della looktab";
    , HelpContextID = 203132811;
    , cFormVar="w_VALPKT05",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(20);
  , bGlobalFont=.t.


  func oVALPKT05_1_51.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PKFLD_05) OR EMPTY(.w_PRINTCUR) OR .w_GETVFC05<>'S')
    endwith
  endfunc

  add object oLTROWDAT_1_54 as StdField with uid="DCIPPTCZNV",rtseq=46,rtrep=.f.,;
    cFormVar = "w_LTROWDAT", cQueryName = "LTROWDAT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero di record da utilizzare per prelevare i dati d'esempio deve essere maggiore di zero e minore o uguale al numero di record totali presnti nel cursore",;
    ToolTipText = "Numero di record da cui prelevare i valori dei campi",;
    HelpContextID = 215679498,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=463, Top=235, cSayPict='"999"', cGetPict='"999"'

  func oLTROWDAT_1_54.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PRINTCUR))
    endwith
  endfunc

  func oLTROWDAT_1_54.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_PRINTCUR) OR (.w_LTROWDAT>0 AND .w_LTROWDAT<=.w_CURTOTRE))
    endwith
    return bRes
  endfunc

  add object oLTSINTAX_1_58 as StdMemo with uid="VFBGINNPKE",rtseq=50,rtrep=.f.,;
    cFormVar = "w_LTSINTAX", cQueryName = "LTSINTAX",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Sintassi della looktab",;
    HelpContextID = 205853198,;
   bGlobalFont=.t.,;
    Height=92, Width=628, Left=24, Top=260


  add object oBtn_1_59 as StdButton with uid="ISLODYFBVX",left=656, top=308, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare il test della looktab";
    , HelpContextID = 208526538;
    , Caption='\<Test';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_59.Click()
      this.parent.oContained.NotifyEvent("Test")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_59.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_NUM_KEYS<5 AND !EMPTY(.w_DESFIELD))
      endwith
    endif
  endfunc

  add object oLTRESULT_1_60 as StdMemo with uid="XURYZNSXXS",rtseq=51,rtrep=.f.,;
    cFormVar = "w_LTRESULT", cQueryName = "LTRESULT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Risultato dell'esecuzione della looktab",;
    HelpContextID = 40828406,;
   bGlobalFont=.t.,;
    Height=69, Width=576, Left=24, Top=360


  add object oBtn_1_63 as StdButton with uid="MNRBYJQCYP",left=605, top=385, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per chiudere la gestione ed esportare la sintassi della looktab";
    , HelpContextID = 216606490;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_63.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_63.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_NUM_KEYS<5 AND !EMPTY(.w_DESFIELD))
      endwith
    endif
  endfunc


  add object oBtn_1_64 as StdButton with uid="WVKCRLCZSX",left=656, top=385, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 106093318;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_64.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="VOZAGHBHAG",Visible=.t., Left=12, Top=14,;
    Alignment=1, Width=209, Height=18,;
    Caption="Archivio da leggere:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="NUEHSQXWHJ",Visible=.t., Left=12, Top=39,;
    Alignment=1, Width=209, Height=18,;
    Caption="Campo da leggere:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="RVSVLOGRDN",Visible=.t., Left=13, Top=64,;
    Alignment=0, Width=661, Height=18,;
    Caption="Tabella selezionata con chiave composta da pi� di 5 campi. Impossibile utilizzarla con la funzione looktab"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_NUM_KEYS<5)
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="GJBYWKSBJL",Visible=.t., Left=99, Top=208,;
    Alignment=1, Width=356, Height=18,;
    Caption="Record presenti nel cursore di stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PRINTCUR))
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="QYPTBOQXWO",Visible=.t., Left=63, Top=235,;
    Alignment=1, Width=392, Height=18,;
    Caption="Per la prova utilizza i dati presenti nel record numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PRINTCUR))
    endwith
  endfunc
enddefine
define class tgsar_kltPag2 as StdContainer
  Width  = 712
  height = 433
  stdWidth  = 712
  stdheight = 433
  resizeXpos=606
  resizeYpos=377
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object VIS__CUR as ah_CBrowse with uid="YOKDEIKOQV",left=7, top=13, width=698,height=414,;
    caption='VIS__CUR',;
   bGlobalFont=.t.,;
    nPag=2;
    , HelpContextID = 208340904
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_klt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_klt
DEFINE CLASS ah_CBrowse AS Grid

  cCursor=""

  * Standard
  RecordSource=""
  RecordSourceType=1   && ALIAS
  ColumnCount=0
  ReadOnly=.t.
  DeleteMark=.f.

  proc Calculate(xValue)
  endproc

  proc Event(cEvent)
    if cEvent="Browse"
       this.Browse()
    endif
  endproc

  proc Browse
    local nI,cI,oCol
    with this
       .RecordSource=''
       .ColumnCount=0
       if !empty(.cCursor)
          .ColumnCount=fcount(.cCursor)
          .recordsource=.cCursor
          for nI=1 to .ColumnCount
	    cI=alltrim(str(nI))
	    oCol=.Columns(nI)
            oCol.Header1.Caption=field(nI,.cCursor)
   	    oCol.Bound=.f.
            oCol.ReadOnly=.ReadOnly
   	    oCol.ControlSource=alltrim(.cCursor)+'.'+field(nI,.cCursor)
   	    oCol.SelectOnEntry=.f.   	
	   next
       endif
    endwith
    Grid::Refresh()
  endproc

ENDDEFINE

Define Class ah_CurFieldList As StdTableCombo
	* Ridefinita classe per recuperare i driver ODBC installati
	* sulla macchina
	Proc Init
		* --- Zucchetti Aulla - Inizio Interfaccia
		this.specialeffect = i_nSpecialEffect
		this.BorderColor = i_nBorderColor
		If Vartype(This.bNoBackColor)='U'
			This.BackColor=i_EBackColor
		Endif
		* --- Zucchetti Aulla - Fine Interfaccia
		This.ToolTipText=cp_Translate(This.ToolTipText)
		this.StatusBarText=Padr(this.ToolTipText,100)
		Local oParentObject
		oParentObject=This.Parent.Parent.Parent.Parent
		If !EMPTY(NVL(oParentObject.w_PRINTCUR,' '))
			AFIELDS(aValues, (oParentObject.w_PRINTCUR))
			Dimension This.combovalues(1)
			This.combovalues(1)=""
			For nCount = 1 To Alen(aValues,1)
				cName = aValues[nCount,1]
				vValue = .Null.
				If !Empty(This.combovalues(1))
					Dimension This.combovalues(Alen(This.combovalues)+1)
				Endif
				This.combovalues(Alen(This.combovalues))= m.cName
				This.AddItem(m.cName)
			Next
		EndIf
		This.nValues = IIF(VARTYPE(This.combovalues)='L', 0, Alen(This.combovalues))
		This.SetFont()
	Endproc
Enddefine

Procedure adhoc_looktab
lparam i_nRes, i_cTable,i_cField,i_cK1,i_xV1,i_cK2,i_xV2,i_cK3,i_xV3,i_cK4,i_xV4,i_cK5,i_xV5
local i_NT,i_nConn,i_cPhTable,i_cWhere,i,n,i_xRes,i_nOldArea,i_cLoTable
i_nOldArea=select()
i_NT=cp_OpenTable(i_cTable)
i_xRes=.NULL.
if i_NT<>0
  i_nConn = i_TableProp[i_NT,3]
  i_cLoTable = upper(i_TableProp[i_NT,1])
  i_cPhTable = cp_SetAzi(i_TableProp[i_NT,2],.t.,i_NT)
  i_cWhere=''
  for i=1 to 5
    n=alltrim(str(i))
    if type("i_xv"+n)='D' and empty(i_xv&n)
      i_xv&n=Null
    endif
    if type("i_cK"+n)='C' and not(isnull(i_xv&n))
      i_cWhere=i_cWhere+iif(empty(i_cWhere),'',' and ')+UPPER(i_cK&n)+'='+iif(i_nConn=0,cp_ToStr(i_xV&n),cp_ToStrODBC(i_xV&n))
    endif
  next
  if not(empty(i_cWhere))
    if i_nConn<>0
      i_nRes=cp_SQL(i_nConn,"select "+UPPER(i_cField)+" from "+i_cPhTable+" "+i_cLoTable+" where "+i_cWhere,'__looktab__')
    else
      select &i_cField from &i_cPhTable as &i_cLoTable where &i_cWhere nofilter into cursor __looktab__
    endif
    if used('__looktab__')
      i_xRes = cp_ToDate(&i_cField) && trasforma l' eventuale datetime in date
      use
    endif
  endif
  cp_CloseTable(i_cTable)
endif
select (i_nOldArea)
return(i_xRes)
endproc

* --- Fine Area Manuale
