* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mdf                                                        *
*              Distinta fatture CBI                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-25                                                      *
* Last revis.: 2015-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_mdf"))

* --- Class definition
define class tgsar_mdf as StdTrsForm
  Top    = 10
  Left   = 14

  * --- Standard Properties
  Width  = 569
  Height = 410+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-08"
  HelpContextID=80312169
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=48

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  DISMFAEL_IDX = 0
  DISDFAEL_IDX = 0
  DOC_MAST_IDX = 0
  ESERCIZI_IDX = 0
  COC_MAST_IDX = 0
  CONTROPA_IDX = 0
  cFile = "DISMFAEL"
  cFileDetail = "DISDFAEL"
  cKeySelect = "FANUMDIS"
  cKeyWhere  = "FANUMDIS=this.w_FANUMDIS"
  cKeyDetail  = "FANUMDIS=this.w_FANUMDIS and FARIFDOC=this.w_FARIFDOC"
  cKeyWhereODBC = '"FANUMDIS="+cp_ToStrODBC(this.w_FANUMDIS)';

  cKeyDetailWhereODBC = '"FANUMDIS="+cp_ToStrODBC(this.w_FANUMDIS)';
      +'+" and FARIFDOC="+cp_ToStrODBC(this.w_FARIFDOC)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"DISDFAEL.FANUMDIS="+cp_ToStrODBC(this.w_FANUMDIS)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DISDFAEL.CPROWORD '
  cPrg = "gsar_mdf"
  cComment = "Distinta fatture CBI"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_READAZI = space(10)
  w_TIPBAN = space(1)
  w_BANRIF = space(15)
  w_FANUMDIS = space(10)
  o_FANUMDIS = space(10)
  w_FANUMERO = 0
  w_FA__ANNO = space(4)
  w_FADATDIS = ctod('  /  /  ')
  o_FADATDIS = ctod('  /  /  ')
  w_FACODESE = space(4)
  w_CPROWORD = 0
  w_FARIFDOC = space(10)
  w_EBANRIF = space(10)
  o_EBANRIF = space(10)
  w_RBANRIF = space(10)
  o_RBANRIF = space(10)
  w_RDESBAN = space(35)
  w_FLEDIT = .F.
  w_EDESBAN = space(35)
  w_FASTATUS = space(1)
  w_FABANRIF = space(15)
  o_FABANRIF = space(15)
  w_FADATVAL = ctod('  /  /  ')
  o_FADATVAL = ctod('  /  /  ')
  w_FAFLPROV = space(1)
  w_TIPDOC = space(5)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod('  /  /  ')
  w_FANOMFIL = space(254)
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_CODBA2 = space(15)
  w_CODBAN = space(10)
  w_CODORN = space(15)
  w_CODSPE = space(3)
  w_CODDES = space(5)
  w_NUMCOR = space(25)
  w_PATCBI = space(254)
  w_STRCBI = space(10)
  w_CODSED = space(5)
  w_OLDSTAT = space(1)
  w_FLVEAC = space(1)
  w_CLADOC = space(2)
  w_PATFIL = space(254)
  w_DESBAN = space(35)
  w_ALLCBI = space(254)
  w_FILCBI = space(254)
  w_OBTEST = ctod('  /  /  ')
  w_FLPROV = space(1)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_FANUMDIS = this.W_FANUMDIS
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_FA__ANNO = this.W_FA__ANNO
  op_FANUMERO = this.W_FANUMERO
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DISMFAEL','gsar_mdf')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mdfPag1","gsar_mdf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Distinta")
      .Pages(1).HelpContextID = 264100201
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='COC_MAST'
    this.cWorkTables[4]='CONTROPA'
    this.cWorkTables[5]='DISMFAEL'
    this.cWorkTables[6]='DISDFAEL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DISMFAEL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DISMFAEL_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_FANUMDIS = NVL(FANUMDIS,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from DISMFAEL where FANUMDIS=KeySet.FANUMDIS
    *
    i_nConn = i_TableProp[this.DISMFAEL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DISMFAEL_IDX,2],this.bLoadRecFilter,this.DISMFAEL_IDX,"gsar_mdf")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DISMFAEL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DISMFAEL.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"DISDFAEL.","DISMFAEL.")
      i_cTable = i_cTable+' DISMFAEL '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FANUMDIS',this.w_FANUMDIS  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_TIPBAN = space(1)
        .w_BANRIF = space(15)
        .w_EBANRIF = IIF(g_APPLICATION<> "ADHOC REVOLUTION",.w_BANRIF,' ')
        .w_RBANRIF = IIF(g_APPLICATION="ADHOC REVOLUTION",.w_BANRIF,' ')
        .w_RDESBAN = space(35)
        .w_EDESBAN = space(35)
        .w_PATCBI = space(254)
        .w_STRCBI = space(10)
        .w_ALLCBI = space(254)
        .w_FILCBI = space(254)
        .w_READAZI = i_CODAZI
          .link_1_1('Load')
        .w_FANUMDIS = NVL(FANUMDIS,space(10))
        .op_FANUMDIS = .w_FANUMDIS
        .w_FANUMERO = NVL(FANUMERO,0)
        .op_FANUMERO = .w_FANUMERO
        .w_FA__ANNO = NVL(FA__ANNO,space(4))
        .op_FA__ANNO = .w_FA__ANNO
        .w_FADATDIS = NVL(cp_ToDate(FADATDIS),ctod("  /  /  "))
        .w_FACODESE = NVL(FACODESE,space(4))
          * evitabile
          *.link_1_8('Load')
          .link_1_10('Load')
          .link_1_11('Load')
        .w_FLEDIT = IIF(.w_FASTATUS $ '4-5' or Empty(.w_FANOMFIL),.F.,.T.)
        .w_FASTATUS = NVL(FASTATUS,space(1))
        .w_FABANRIF = NVL(FABANRIF,space(15))
        .w_FADATVAL = NVL(cp_ToDate(FADATVAL),ctod("  /  /  "))
        .w_FAFLPROV = NVL(FAFLPROV,space(1))
        .w_FANOMFIL = NVL(FANOMFIL,space(254))
        .w_OLDSTAT = .w_FASTATUS
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .w_PATFIL = ADDBS(ALLTRIM(.w_PATCBI))+ALLTRIM(IIF(.w_TIPBAN $ 'CD', iif(.w_TIPBAN='C', ALLTRIM(.w_FABANRIF) +ALLTRIM(IIF(EMPTY(.w_FABANRIF), ' ', '\')),ALLTRIM(.w_DESBAN) +ALLTRIM(IIF(EMPTY(.w_DESBAN), ' ', '\'))), ' '))
        .w_DESBAN = IIF(g_APPLICATION="ADHOC REVOLUTION",.w_RDESBAN,.w_EDESBAN)
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DISMFAEL')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from DISDFAEL where FANUMDIS=KeySet.FANUMDIS
      *                            and FARIFDOC=KeySet.FARIFDOC
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.DISDFAEL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DISDFAEL_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('DISDFAEL')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "DISDFAEL.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" DISDFAEL"
        link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'FANUMDIS',this.w_FANUMDIS  )
        select * from (i_cTable) DISDFAEL where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_TIPDOC = space(5)
          .w_NUMDOC = 0
          .w_ALFDOC = space(10)
          .w_DATDOC = ctod("  /  /  ")
          .w_CODCON = space(15)
          .w_TIPCON = space(1)
          .w_CODBA2 = space(15)
          .w_CODBAN = space(10)
          .w_CODORN = space(15)
          .w_CODSPE = space(3)
          .w_CODDES = space(5)
          .w_NUMCOR = space(25)
          .w_CODSED = space(5)
          .w_FLVEAC = space(1)
          .w_CLADOC = space(2)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_FARIFDOC = NVL(FARIFDOC,space(10))
          if link_2_2_joined
            this.w_FARIFDOC = NVL(MVSERIAL202,NVL(this.w_FARIFDOC,space(10)))
            this.w_TIPDOC = NVL(MVTIPDOC202,space(5))
            this.w_NUMDOC = NVL(MVNUMDOC202,0)
            this.w_ALFDOC = NVL(MVALFDOC202,space(10))
            this.w_DATDOC = NVL(cp_ToDate(MVDATDOC202),ctod("  /  /  "))
            this.w_CODCON = NVL(MVCODCON202,space(15))
            this.w_TIPCON = NVL(MVTIPCON202,space(1))
            this.w_CODBA2 = NVL(MVCODBA2202,space(15))
            this.w_CODBAN = NVL(MVCODBAN202,space(10))
            this.w_CODORN = NVL(MVCODORN202,space(15))
            this.w_CODSPE = NVL(MVCODSPE202,space(3))
            this.w_CODDES = NVL(MVCODDES202,space(5))
            this.w_CLADOC = NVL(MVCLADOC202,space(2))
            this.w_FLVEAC = NVL(MVFLVEAC202,space(1))
          else
          .link_2_2('Load')
          endif
        .w_FLPROV = .w_FAFLPROV
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_READAZI = i_CODAZI
        .w_FLEDIT = IIF(.w_FASTATUS $ '4-5' or Empty(.w_FANOMFIL),.F.,.T.)
        .w_OLDSTAT = .w_FASTATUS
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .w_PATFIL = ADDBS(ALLTRIM(.w_PATCBI))+ALLTRIM(IIF(.w_TIPBAN $ 'CD', iif(.w_TIPBAN='C', ALLTRIM(.w_FABANRIF) +ALLTRIM(IIF(EMPTY(.w_FABANRIF), ' ', '\')),ALLTRIM(.w_DESBAN) +ALLTRIM(IIF(EMPTY(.w_DESBAN), ' ', '\'))), ' '))
        .w_DESBAN = IIF(g_APPLICATION="ADHOC REVOLUTION",.w_RDESBAN,.w_EDESBAN)
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_25.enabled = .oPgFrm.Page1.oPag.oBtn_1_25.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_28.enabled = .oPgFrm.Page1.oPag.oBtn_1_28.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_READAZI=space(10)
      .w_TIPBAN=space(1)
      .w_BANRIF=space(15)
      .w_FANUMDIS=space(10)
      .w_FANUMERO=0
      .w_FA__ANNO=space(4)
      .w_FADATDIS=ctod("  /  /  ")
      .w_FACODESE=space(4)
      .w_CPROWORD=10
      .w_FARIFDOC=space(10)
      .w_EBANRIF=space(10)
      .w_RBANRIF=space(10)
      .w_RDESBAN=space(35)
      .w_FLEDIT=.f.
      .w_EDESBAN=space(35)
      .w_FASTATUS=space(1)
      .w_FABANRIF=space(15)
      .w_FADATVAL=ctod("  /  /  ")
      .w_FAFLPROV=space(1)
      .w_TIPDOC=space(5)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_FANOMFIL=space(254)
      .w_CODCON=space(15)
      .w_TIPCON=space(1)
      .w_CODBA2=space(15)
      .w_CODBAN=space(10)
      .w_CODORN=space(15)
      .w_CODSPE=space(3)
      .w_CODDES=space(5)
      .w_NUMCOR=space(25)
      .w_PATCBI=space(254)
      .w_STRCBI=space(10)
      .w_CODSED=space(5)
      .w_OLDSTAT=space(1)
      .w_FLVEAC=space(1)
      .w_CLADOC=space(2)
      .w_PATFIL=space(254)
      .w_DESBAN=space(35)
      .w_ALLCBI=space(254)
      .w_FILCBI=space(254)
      .w_OBTEST=ctod("  /  /  ")
      .w_FLPROV=space(1)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .w_READAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READAZI))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,5,.f.)
        .w_FA__ANNO = STR(YEAR(IIF(EMPTY(.w_FADATDIS), i_datsys, .w_FADATDIS)),4,0)
        .w_FADATDIS = i_datsys
        .w_FACODESE = CALCESER(.w_FADATVAL,g_CODESE)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_FACODESE))
         .link_1_8('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_FARIFDOC))
         .link_2_2('Full')
        endif
        .w_EBANRIF = IIF(g_APPLICATION<> "ADHOC REVOLUTION",.w_BANRIF,' ')
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_EBANRIF))
         .link_1_10('Full')
        endif
        .w_RBANRIF = IIF(g_APPLICATION="ADHOC REVOLUTION",.w_BANRIF,' ')
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_RBANRIF))
         .link_1_11('Full')
        endif
        .DoRTCalc(13,13,.f.)
        .w_FLEDIT = IIF(.w_FASTATUS $ '4-5' or Empty(.w_FANOMFIL),.F.,.T.)
        .DoRTCalc(15,15,.f.)
        .w_FASTATUS = '1'
        .w_FABANRIF = IIF(g_APPLICATION="ADHOC REVOLUTION",.w_RBANRIF,.w_EBANRIF)
        .DoRTCalc(18,18,.f.)
        .w_FAFLPROV = 'N'
        .DoRTCalc(20,35,.f.)
        .w_OLDSTAT = .w_FASTATUS
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .DoRTCalc(37,38,.f.)
        .w_PATFIL = ADDBS(ALLTRIM(.w_PATCBI))+ALLTRIM(IIF(.w_TIPBAN $ 'CD', iif(.w_TIPBAN='C', ALLTRIM(.w_FABANRIF) +ALLTRIM(IIF(EMPTY(.w_FABANRIF), ' ', '\')),ALLTRIM(.w_DESBAN) +ALLTRIM(IIF(EMPTY(.w_DESBAN), ' ', '\'))), ' '))
        .w_DESBAN = IIF(g_APPLICATION="ADHOC REVOLUTION",.w_RDESBAN,.w_EDESBAN)
        .DoRTCalc(41,42,.f.)
        .w_OBTEST = i_DATSYS
        .w_FLPROV = .w_FAFLPROV
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DISMFAEL')
    this.DoRTCalc(45,48,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oFANUMERO_1_5.enabled = i_bVal
      .Page1.oPag.oFADATDIS_1_7.enabled = i_bVal
      .Page1.oPag.oFACODESE_1_8.enabled = i_bVal
      .Page1.oPag.oEBANRIF_1_10.enabled = i_bVal
      .Page1.oPag.oRBANRIF_1_11.enabled = i_bVal
      .Page1.oPag.oFASTATUS_1_15.enabled = i_bVal
      .Page1.oPag.oBtn_1_25.enabled = .Page1.oPag.oBtn_1_25.mCond()
      .Page1.oPag.oBtn_1_27.enabled = i_bVal
      .Page1.oPag.oBtn_1_28.enabled = .Page1.oPag.oBtn_1_28.mCond()
      .Page1.oPag.oBtn_1_29.enabled = i_bVal
      .Page1.oPag.oObj_1_33.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oFANUMERO_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DISMFAEL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DISMFAEL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DISMFAEL_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"PRDIF","i_codazi,w_FANUMDIS")
      cp_AskTableProg(this,i_nConn,"PNDIF","i_codazi,w_FA__ANNO,w_FANUMERO")
      .op_codazi = .w_codazi
      .op_FANUMDIS = .w_FANUMDIS
      .op_codazi = .w_codazi
      .op_FA__ANNO = .w_FA__ANNO
      .op_FANUMERO = .w_FANUMERO
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DISMFAEL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FANUMDIS,"FANUMDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FANUMERO,"FANUMERO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FA__ANNO,"FA__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FADATDIS,"FADATDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FACODESE,"FACODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FASTATUS,"FASTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FABANRIF,"FABANRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FADATVAL,"FADATVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FAFLPROV,"FAFLPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FANOMFIL,"FANOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DISMFAEL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DISMFAEL_IDX,2])
    i_lTable = "DISMFAEL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DISMFAEL_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_TIPDOC C(5);
      ,t_NUMDOC N(15);
      ,t_ALFDOC C(10);
      ,t_DATDOC D(8);
      ,t_CODCON C(15);
      ,CPROWNUM N(10);
      ,t_FARIFDOC C(10);
      ,t_TIPCON C(1);
      ,t_CODBA2 C(15);
      ,t_CODBAN C(10);
      ,t_CODORN C(15);
      ,t_CODSPE C(3);
      ,t_CODDES C(5);
      ,t_NUMCOR C(25);
      ,t_CODSED C(5);
      ,t_FLVEAC C(1);
      ,t_CLADOC C(2);
      ,t_FLPROV C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mdfbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTIPDOC_2_3.controlsource=this.cTrsName+'.t_TIPDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_4.controlsource=this.cTrsName+'.t_NUMDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_5.controlsource=this.cTrsName+'.t_ALFDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDATDOC_2_6.controlsource=this.cTrsName+'.t_DATDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCODCON_2_7.controlsource=this.cTrsName+'.t_CODCON'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(62)
    this.AddVLine(120)
    this.AddVLine(241)
    this.AddVLine(327)
    this.AddVLine(400)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DISMFAEL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DISMFAEL_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"PRDIF","i_codazi,w_FANUMDIS")
          cp_NextTableProg(this,i_nConn,"PNDIF","i_codazi,w_FA__ANNO,w_FANUMERO")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DISMFAEL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DISMFAEL')
        i_extval=cp_InsertValODBCExtFlds(this,'DISMFAEL')
        local i_cFld
        i_cFld=" "+;
                  "(FANUMDIS,FANUMERO,FA__ANNO,FADATDIS,FACODESE"+;
                  ",FASTATUS,FABANRIF,FADATVAL,FAFLPROV,FANOMFIL"+;
                  ",UTCC,UTDC,UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_FANUMDIS)+;
                    ","+cp_ToStrODBC(this.w_FANUMERO)+;
                    ","+cp_ToStrODBC(this.w_FA__ANNO)+;
                    ","+cp_ToStrODBC(this.w_FADATDIS)+;
                    ","+cp_ToStrODBCNull(this.w_FACODESE)+;
                    ","+cp_ToStrODBC(this.w_FASTATUS)+;
                    ","+cp_ToStrODBC(this.w_FABANRIF)+;
                    ","+cp_ToStrODBC(this.w_FADATVAL)+;
                    ","+cp_ToStrODBC(this.w_FAFLPROV)+;
                    ","+cp_ToStrODBC(this.w_FANOMFIL)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DISMFAEL')
        i_extval=cp_InsertValVFPExtFlds(this,'DISMFAEL')
        cp_CheckDeletedKey(i_cTable,0,'FANUMDIS',this.w_FANUMDIS)
        INSERT INTO (i_cTable);
              (FANUMDIS,FANUMERO,FA__ANNO,FADATDIS,FACODESE,FASTATUS,FABANRIF,FADATVAL,FAFLPROV,FANOMFIL,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_FANUMDIS;
                  ,this.w_FANUMERO;
                  ,this.w_FA__ANNO;
                  ,this.w_FADATDIS;
                  ,this.w_FACODESE;
                  ,this.w_FASTATUS;
                  ,this.w_FABANRIF;
                  ,this.w_FADATVAL;
                  ,this.w_FAFLPROV;
                  ,this.w_FANOMFIL;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DISDFAEL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DISDFAEL_IDX,2])
      *
      * insert into DISDFAEL
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(FANUMDIS,CPROWORD,FARIFDOC,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_FANUMDIS)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_FARIFDOC)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'FANUMDIS',this.w_FANUMDIS,'FARIFDOC',this.w_FARIFDOC)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_FANUMDIS,this.w_CPROWORD,this.w_FARIFDOC,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.DISMFAEL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DISMFAEL_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update DISMFAEL
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'DISMFAEL')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " FANUMERO="+cp_ToStrODBC(this.w_FANUMERO)+;
             ",FA__ANNO="+cp_ToStrODBC(this.w_FA__ANNO)+;
             ",FADATDIS="+cp_ToStrODBC(this.w_FADATDIS)+;
             ",FACODESE="+cp_ToStrODBCNull(this.w_FACODESE)+;
             ",FASTATUS="+cp_ToStrODBC(this.w_FASTATUS)+;
             ",FABANRIF="+cp_ToStrODBC(this.w_FABANRIF)+;
             ",FADATVAL="+cp_ToStrODBC(this.w_FADATVAL)+;
             ",FAFLPROV="+cp_ToStrODBC(this.w_FAFLPROV)+;
             ",FANOMFIL="+cp_ToStrODBC(this.w_FANOMFIL)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'DISMFAEL')
          i_cWhere = cp_PKFox(i_cTable  ,'FANUMDIS',this.w_FANUMDIS  )
          UPDATE (i_cTable) SET;
              FANUMERO=this.w_FANUMERO;
             ,FA__ANNO=this.w_FA__ANNO;
             ,FADATDIS=this.w_FADATDIS;
             ,FACODESE=this.w_FACODESE;
             ,FASTATUS=this.w_FASTATUS;
             ,FABANRIF=this.w_FABANRIF;
             ,FADATVAL=this.w_FADATVAL;
             ,FAFLPROV=this.w_FAFLPROV;
             ,FANOMFIL=this.w_FANOMFIL;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) and not(Empty(t_FARIFDOC)) ) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.DISDFAEL_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.DISDFAEL_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from DISDFAEL
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DISDFAEL
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsar_mdf
    if not(bTrsErr)
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- gsar_mdf
    if not(bTrsErr)
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
    endif
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) and not(Empty(t_FARIFDOC)) ) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.DISDFAEL_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.DISDFAEL_IDX,2])
        *
        * delete DISDFAEL
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.DISMFAEL_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.DISMFAEL_IDX,2])
        *
        * delete DISMFAEL
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) and not(Empty(t_FARIFDOC)) ) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DISMFAEL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DISMFAEL_IDX,2])
    if i_bUpd
      with this
          .w_READAZI = i_CODAZI
          .link_1_1('Full')
        .DoRTCalc(2,5,.t.)
        if .o_FADATDIS<>.w_FADATDIS
          .w_FA__ANNO = STR(YEAR(IIF(EMPTY(.w_FADATDIS), i_datsys, .w_FADATDIS)),4,0)
        endif
        .DoRTCalc(7,7,.t.)
        if .o_FADATVAL<>.w_FADATVAL
          .w_FACODESE = CALCESER(.w_FADATVAL,g_CODESE)
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.t.)
          .link_2_2('Full')
        .DoRTCalc(11,13,.t.)
        if .o_FANUMDIS<>.w_FANUMDIS
          .w_FLEDIT = IIF(.w_FASTATUS $ '4-5' or Empty(.w_FANOMFIL),.F.,.T.)
        endif
        .DoRTCalc(15,16,.t.)
        if .o_RBANRIF<>.w_RBANRIF.or. .o_EBANRIF<>.w_EBANRIF
          .w_FABANRIF = IIF(g_APPLICATION="ADHOC REVOLUTION",.w_RBANRIF,.w_EBANRIF)
        endif
        .DoRTCalc(18,35,.t.)
        if .o_FANUMDIS<>.w_FANUMDIS
          .w_OLDSTAT = .w_FASTATUS
        endif
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .DoRTCalc(37,38,.t.)
        if .o_FABANRIF<>.w_FABANRIF
          .w_PATFIL = ADDBS(ALLTRIM(.w_PATCBI))+ALLTRIM(IIF(.w_TIPBAN $ 'CD', iif(.w_TIPBAN='C', ALLTRIM(.w_FABANRIF) +ALLTRIM(IIF(EMPTY(.w_FABANRIF), ' ', '\')),ALLTRIM(.w_DESBAN) +ALLTRIM(IIF(EMPTY(.w_DESBAN), ' ', '\'))), ' '))
        endif
        if .o_RBANRIF<>.w_RBANRIF.or. .o_EBANRIF<>.w_EBANRIF
          .w_DESBAN = IIF(g_APPLICATION="ADHOC REVOLUTION",.w_RDESBAN,.w_EDESBAN)
        endif
        .DoRTCalc(41,42,.t.)
          .w_OBTEST = i_DATSYS
          .w_FLPROV = .w_FAFLPROV
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"PRDIF","i_codazi,w_FANUMDIS")
          .op_FANUMDIS = .w_FANUMDIS
        endif
        if .op_codazi<>.w_codazi .or. .op_FA__ANNO<>.w_FA__ANNO
           cp_AskTableProg(this,i_nConn,"PNDIF","i_codazi,w_FA__ANNO,w_FANUMERO")
          .op_FANUMERO = .w_FANUMERO
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_FA__ANNO = .w_FA__ANNO
      endwith
      this.DoRTCalc(45,48,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_FARIFDOC with this.w_FARIFDOC
      replace t_TIPCON with this.w_TIPCON
      replace t_CODBA2 with this.w_CODBA2
      replace t_CODBAN with this.w_CODBAN
      replace t_CODORN with this.w_CODORN
      replace t_CODSPE with this.w_CODSPE
      replace t_CODDES with this.w_CODDES
      replace t_NUMCOR with this.w_NUMCOR
      replace t_CODSED with this.w_CODSED
      replace t_FLVEAC with this.w_FLVEAC
      replace t_CLADOC with this.w_CLADOC
      replace t_FLPROV with this.w_FLPROV
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_DYEAIUKFHP()
    with this
          * --- Assegnamento banca
          .w_RBANRIF = IIF(g_APPLICATION="ADHOC REVOLUTION",.w_FABANRIF,'')
          .link_1_11('Full')
          .w_EBANRIF = IIF(g_APPLICATION <> "ADHOC REVOLUTION",.w_FABANRIF,'')
          .link_1_10('Full')
          .w_DESBAN = IIF(g_APPLICATION="ADHOC REVOLUTION",.w_RDESBAN,.w_EDESBAN)
          .w_PATFIL = ADDBS(ALLTRIM(.w_PATCBI))+ALLTRIM(IIF(.w_TIPBAN $ 'CD', iif(.w_TIPBAN='C', ALLTRIM(.w_FABANRIF) +ALLTRIM(IIF(EMPTY(.w_FABANRIF), ' ', '\')),ALLTRIM(.w_DESBAN) +ALLTRIM(IIF(EMPTY(.w_DESBAN), ' ', '\'))), ' '))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFASTATUS_1_15.enabled = this.oPgFrm.Page1.oPag.oFASTATUS_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oEBANRIF_1_10.visible=!this.oPgFrm.Page1.oPag.oEBANRIF_1_10.mHide()
    this.oPgFrm.Page1.oPag.oRBANRIF_1_11.visible=!this.oPgFrm.Page1.oPag.oRBANRIF_1_11.mHide()
    this.oPgFrm.Page1.oPag.oRDESBAN_1_12.visible=!this.oPgFrm.Page1.oPag.oRDESBAN_1_12.mHide()
    this.oPgFrm.Page1.oPag.oEDESBAN_1_14.visible=!this.oPgFrm.Page1.oPag.oEDESBAN_1_14.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_25.visible=!this.oPgFrm.Page1.oPag.oBtn_1_25.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_27.visible=!this.oPgFrm.Page1.oPag.oBtn_1_27.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_DYEAIUKFHP()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COBANPRO,COPATCBI,COSTRCBI,COTIPBAC,COFILCBI,COALLCBI";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_READAZI)
            select COCODAZI,COBANPRO,COPATCBI,COSTRCBI,COTIPBAC,COFILCBI,COALLCBI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.COCODAZI,space(10))
      this.w_BANRIF = NVL(_Link_.COBANPRO,space(15))
      this.w_PATCBI = NVL(_Link_.COPATCBI,space(254))
      this.w_STRCBI = NVL(_Link_.COSTRCBI,space(10))
      this.w_TIPBAN = NVL(_Link_.COTIPBAC,space(1))
      this.w_FILCBI = NVL(_Link_.COFILCBI,space(254))
      this.w_ALLCBI = NVL(_Link_.COALLCBI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(10)
      endif
      this.w_BANRIF = space(15)
      this.w_PATCBI = space(254)
      this.w_STRCBI = space(10)
      this.w_TIPBAN = space(1)
      this.w_FILCBI = space(254)
      this.w_ALLCBI = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FACODESE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FACODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_FACODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_FACODESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FACODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FACODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oFACODESE_1_8'),i_cWhere,'GSAR_KES',"Elenco esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FACODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_FACODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_FACODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FACODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_FACODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FACODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FARIFDOC
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FARIFDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FARIFDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC,MVCODCON,MVTIPCON,MVCODBA2,MVCODBAN,MVCODORN,MVCODSPE,MVCODDES,MVCLADOC,MVFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_FARIFDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_FARIFDOC)
            select MVSERIAL,MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC,MVCODCON,MVTIPCON,MVCODBA2,MVCODBAN,MVCODORN,MVCODSPE,MVCODDES,MVCLADOC,MVFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FARIFDOC = NVL(_Link_.MVSERIAL,space(10))
      this.w_TIPDOC = NVL(_Link_.MVTIPDOC,space(5))
      this.w_NUMDOC = NVL(_Link_.MVNUMDOC,0)
      this.w_ALFDOC = NVL(_Link_.MVALFDOC,space(10))
      this.w_DATDOC = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
      this.w_CODCON = NVL(_Link_.MVCODCON,space(15))
      this.w_TIPCON = NVL(_Link_.MVTIPCON,space(1))
      this.w_CODBA2 = NVL(_Link_.MVCODBA2,space(15))
      this.w_CODBAN = NVL(_Link_.MVCODBAN,space(10))
      this.w_CODORN = NVL(_Link_.MVCODORN,space(15))
      this.w_CODSPE = NVL(_Link_.MVCODSPE,space(3))
      this.w_CODDES = NVL(_Link_.MVCODDES,space(5))
      this.w_CLADOC = NVL(_Link_.MVCLADOC,space(2))
      this.w_FLVEAC = NVL(_Link_.MVFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_FARIFDOC = space(10)
      endif
      this.w_TIPDOC = space(5)
      this.w_NUMDOC = 0
      this.w_ALFDOC = space(10)
      this.w_DATDOC = ctod("  /  /  ")
      this.w_CODCON = space(15)
      this.w_TIPCON = space(1)
      this.w_CODBA2 = space(15)
      this.w_CODBAN = space(10)
      this.w_CODORN = space(15)
      this.w_CODSPE = space(3)
      this.w_CODDES = space(5)
      this.w_CLADOC = space(2)
      this.w_FLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FARIFDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 14 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DOC_MAST_IDX,3] and i_nFlds+14<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.MVSERIAL as MVSERIAL202"+ ",link_2_2.MVTIPDOC as MVTIPDOC202"+ ",link_2_2.MVNUMDOC as MVNUMDOC202"+ ",link_2_2.MVALFDOC as MVALFDOC202"+ ",link_2_2.MVDATDOC as MVDATDOC202"+ ",link_2_2.MVCODCON as MVCODCON202"+ ",link_2_2.MVTIPCON as MVTIPCON202"+ ",link_2_2.MVCODBA2 as MVCODBA2202"+ ",link_2_2.MVCODBAN as MVCODBAN202"+ ",link_2_2.MVCODORN as MVCODORN202"+ ",link_2_2.MVCODSPE as MVCODSPE202"+ ",link_2_2.MVCODDES as MVCODDES202"+ ",link_2_2.MVCLADOC as MVCLADOC202"+ ",link_2_2.MVFLVEAC as MVFLVEAC202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on DISDFAEL.FARIFDOC=link_2_2.MVSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and DISDFAEL.FARIFDOC=link_2_2.MVSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EBANRIF
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EBANRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACC',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BANUMCOR like "+cp_ToStrODBC(trim(this.w_EBANRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BANUMCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BANUMCOR',trim(this.w_EBANRIF))
          select BANUMCOR,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BANUMCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EBANRIF)==trim(_Link_.BANUMCOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EBANRIF) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BANUMCOR',cp_AbsName(oSource.parent,'oEBANRIF_1_10'),i_cWhere,'GSTE_ACC',"Conti di tesoreria",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BANUMCOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BANUMCOR',oSource.xKey(1))
            select BANUMCOR,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EBANRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BANUMCOR="+cp_ToStrODBC(this.w_EBANRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BANUMCOR',this.w_EBANRIF)
            select BANUMCOR,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EBANRIF = NVL(_Link_.BANUMCOR,space(10))
      this.w_EDESBAN = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_EBANRIF = space(10)
      endif
      this.w_EDESBAN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BANUMCOR,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EBANRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RBANRIF
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RBANRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_RBANRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_RBANRIF))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RBANRIF)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RBANRIF) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oRBANRIF_1_11'),i_cWhere,'GSTE_ACB',"Conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RBANRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_RBANRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_RBANRIF)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RBANRIF = NVL(_Link_.BACODBAN,space(10))
      this.w_RDESBAN = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_RBANRIF = space(10)
      endif
      this.w_RDESBAN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RBANRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oFANUMERO_1_5.value==this.w_FANUMERO)
      this.oPgFrm.Page1.oPag.oFANUMERO_1_5.value=this.w_FANUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oFA__ANNO_1_6.value==this.w_FA__ANNO)
      this.oPgFrm.Page1.oPag.oFA__ANNO_1_6.value=this.w_FA__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oFADATDIS_1_7.value==this.w_FADATDIS)
      this.oPgFrm.Page1.oPag.oFADATDIS_1_7.value=this.w_FADATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oFACODESE_1_8.value==this.w_FACODESE)
      this.oPgFrm.Page1.oPag.oFACODESE_1_8.value=this.w_FACODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oEBANRIF_1_10.value==this.w_EBANRIF)
      this.oPgFrm.Page1.oPag.oEBANRIF_1_10.value=this.w_EBANRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oRBANRIF_1_11.value==this.w_RBANRIF)
      this.oPgFrm.Page1.oPag.oRBANRIF_1_11.value=this.w_RBANRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oRDESBAN_1_12.value==this.w_RDESBAN)
      this.oPgFrm.Page1.oPag.oRDESBAN_1_12.value=this.w_RDESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oEDESBAN_1_14.value==this.w_EDESBAN)
      this.oPgFrm.Page1.oPag.oEDESBAN_1_14.value=this.w_EDESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oFASTATUS_1_15.RadioValue()==this.w_FASTATUS)
      this.oPgFrm.Page1.oPag.oFASTATUS_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFADATVAL_1_17.value==this.w_FADATVAL)
      this.oPgFrm.Page1.oPag.oFADATVAL_1_17.value=this.w_FADATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oFAFLPROV_1_18.RadioValue()==this.w_FAFLPROV)
      this.oPgFrm.Page1.oPag.oFAFLPROV_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFANOMFIL_1_23.value==this.w_FANOMFIL)
      this.oPgFrm.Page1.oPag.oFANOMFIL_1_23.value=this.w_FANOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPDOC_2_3.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPDOC_2_3.value=this.w_TIPDOC
      replace t_TIPDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPDOC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_4.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_4.value=this.w_NUMDOC
      replace t_NUMDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_5.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_5.value=this.w_ALFDOC
      replace t_ALFDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATDOC_2_6.value==this.w_DATDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATDOC_2_6.value=this.w_DATDOC
      replace t_DATDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATDOC_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODCON_2_7.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODCON_2_7.value=this.w_CODCON
      replace t_CODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODCON_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'DISMFAEL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FACODESE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oFACODESE_1_8.SetFocus()
            i_bnoObbl = !empty(.w_FACODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (not(Empty(t_CPROWORD)) and not(Empty(t_FARIFDOC)) );
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CPROWORD)) and not(Empty(.w_FARIFDOC)) 
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FANUMDIS = this.w_FANUMDIS
    this.o_FADATDIS = this.w_FADATDIS
    this.o_EBANRIF = this.w_EBANRIF
    this.o_RBANRIF = this.w_RBANRIF
    this.o_FABANRIF = this.w_FABANRIF
    this.o_FADATVAL = this.w_FADATVAL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) and not(Empty(t_FARIFDOC)) )
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=t_FLPROV='N'
    if !i_bRes
      cp_ErrorMsg("Distinta non provvisoria impossibile eliminare!","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_FARIFDOC=space(10)
      .w_TIPDOC=space(5)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_CODCON=space(15)
      .w_TIPCON=space(1)
      .w_CODBA2=space(15)
      .w_CODBAN=space(10)
      .w_CODORN=space(15)
      .w_CODSPE=space(3)
      .w_CODDES=space(5)
      .w_NUMCOR=space(25)
      .w_CODSED=space(5)
      .w_FLVEAC=space(1)
      .w_CLADOC=space(2)
      .w_FLPROV=space(1)
      .DoRTCalc(1,10,.f.)
      if not(empty(.w_FARIFDOC))
        .link_2_2('Full')
      endif
      .DoRTCalc(11,43,.f.)
        .w_FLPROV = .w_FAFLPROV
    endwith
    this.DoRTCalc(45,48,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_FARIFDOC = t_FARIFDOC
    this.w_TIPDOC = t_TIPDOC
    this.w_NUMDOC = t_NUMDOC
    this.w_ALFDOC = t_ALFDOC
    this.w_DATDOC = t_DATDOC
    this.w_CODCON = t_CODCON
    this.w_TIPCON = t_TIPCON
    this.w_CODBA2 = t_CODBA2
    this.w_CODBAN = t_CODBAN
    this.w_CODORN = t_CODORN
    this.w_CODSPE = t_CODSPE
    this.w_CODDES = t_CODDES
    this.w_NUMCOR = t_NUMCOR
    this.w_CODSED = t_CODSED
    this.w_FLVEAC = t_FLVEAC
    this.w_CLADOC = t_CLADOC
    this.w_FLPROV = t_FLPROV
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_FARIFDOC with this.w_FARIFDOC
    replace t_TIPDOC with this.w_TIPDOC
    replace t_NUMDOC with this.w_NUMDOC
    replace t_ALFDOC with this.w_ALFDOC
    replace t_DATDOC with this.w_DATDOC
    replace t_CODCON with this.w_CODCON
    replace t_TIPCON with this.w_TIPCON
    replace t_CODBA2 with this.w_CODBA2
    replace t_CODBAN with this.w_CODBAN
    replace t_CODORN with this.w_CODORN
    replace t_CODSPE with this.w_CODSPE
    replace t_CODDES with this.w_CODDES
    replace t_NUMCOR with this.w_NUMCOR
    replace t_CODSED with this.w_CODSED
    replace t_FLVEAC with this.w_FLVEAC
    replace t_CLADOC with this.w_CLADOC
    replace t_FLPROV with this.w_FLPROV
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mdfPag1 as StdContainer
  Width  = 565
  height = 410
  stdWidth  = 565
  stdheight = 410
  resizeXpos=301
  resizeYpos=233
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFANUMERO_1_5 as StdField with uid="HJTCBPWDZA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FANUMERO", cQueryName = "FANUMERO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero distinta",;
    HelpContextID = 90222245,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=70, Top=21, cSayPict='"999999"', cGetPict='"999999"'

  add object oFA__ANNO_1_6 as StdField with uid="KWYMASECCH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FA__ANNO", cQueryName = "FANUMERO,FA__ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 39076187,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=143, Top=21, InputMask=replicate('X',4)

  add object oFADATDIS_1_7 as StdField with uid="YYTHKRDFCB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FADATDIS", cQueryName = "FADATDIS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data distinta",;
    HelpContextID = 189002071,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=224, Top=21

  add object oFACODESE_1_8 as StdField with uid="MWOWHCARAG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_FACODESE", cQueryName = "FACODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza",;
    HelpContextID = 80346779,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=506, Top=21, InputMask=replicate('X',4), bHasZoom = .t. , tabstop=.f., cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_FACODESE"

  func oFACODESE_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oFACODESE_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFACODESE_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oFACODESE_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"Elenco esercizi",'',this.parent.oContained
  endproc
  proc oFACODESE_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_CODAZI
     i_obj.w_ESCODESE=this.parent.oContained.w_FACODESE
    i_obj.ecpSave()
  endproc

  add object oEBANRIF_1_10 as StdField with uid="QBOIJOXSLZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_EBANRIF", cQueryName = "EBANRIF",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 162062150,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=81, Top=48, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACC", oKey_1_1="BANUMCOR", oKey_1_2="this.w_EBANRIF"

  func oEBANRIF_1_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
    endif
  endfunc

  func oEBANRIF_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oEBANRIF_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEBANRIF_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BANUMCOR',cp_AbsName(this.parent,'oEBANRIF_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACC',"Conti di tesoreria",'',this.parent.oContained
  endproc
  proc oEBANRIF_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BANUMCOR=this.parent.oContained.w_EBANRIF
    i_obj.ecpSave()
  endproc

  add object oRBANRIF_1_11 as StdField with uid="COOMTDDOKD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_RBANRIF", cQueryName = "RBANRIF",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 162062358,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=81, Top=48, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_RBANRIF"

  func oRBANRIF_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
    endif
  endfunc

  func oRBANRIF_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oRBANRIF_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRBANRIF_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oRBANRIF_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'',this.parent.oContained
  endproc
  proc oRBANRIF_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_RBANRIF
    i_obj.ecpSave()
  endproc

  add object oRDESBAN_1_12 as StdField with uid="ZWITLNGPEV",rtseq=13,rtrep=.f.,;
    cFormVar = "w_RDESBAN", cQueryName = "RDESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 257023466,;
   bGlobalFont=.t.,;
    Height=21, Width=348, Left=201, Top=49, InputMask=replicate('X',35)

  func oRDESBAN_1_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
    endif
  endfunc

  add object oEDESBAN_1_14 as StdField with uid="JZODFOEEAK",rtseq=15,rtrep=.f.,;
    cFormVar = "w_EDESBAN", cQueryName = "EDESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 257023674,;
   bGlobalFont=.t.,;
    Height=21, Width=348, Left=213, Top=49, InputMask=replicate('X',35)

  func oEDESBAN_1_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION = "ADHOC REVOLUTION")
    endwith
    endif
  endfunc


  add object oFASTATUS_1_15 as StdCombo with uid="DMNZMUWCQP",rtseq=16,rtrep=.f.,left=82,top=79,width=111,height=21;
    , ToolTipText = "Status del file";
    , HelpContextID = 60817065;
    , cFormVar="w_FASTATUS",RowSource=""+"Da generare,"+"Generato,"+"Inviato,"+"Accettato,"+"Rifiutato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFASTATUS_1_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FASTATUS,&i_cF..t_FASTATUS),this.value)
    return(iif(xVal =1,'1',;
    iif(xVal =2,'2',;
    iif(xVal =3,'3',;
    iif(xVal =4,'4',;
    iif(xVal =5,'5',;
    space(1)))))))
  endfunc
  func oFASTATUS_1_15.GetRadio()
    this.Parent.oContained.w_FASTATUS = this.RadioValue()
    return .t.
  endfunc

  func oFASTATUS_1_15.ToRadio()
    this.Parent.oContained.w_FASTATUS=trim(this.Parent.oContained.w_FASTATUS)
    return(;
      iif(this.Parent.oContained.w_FASTATUS=='1',1,;
      iif(this.Parent.oContained.w_FASTATUS=='2',2,;
      iif(this.Parent.oContained.w_FASTATUS=='3',3,;
      iif(this.Parent.oContained.w_FASTATUS=='4',4,;
      iif(this.Parent.oContained.w_FASTATUS=='5',5,;
      0))))))
  endfunc

  func oFASTATUS_1_15.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oFASTATUS_1_15.mCond()
    with this.Parent.oContained
      return (.w_FLEDIT)
    endwith
  endfunc

  add object oFADATVAL_1_17 as StdField with uid="ENHCDCKAQD",rtseq=18,rtrep=.f.,;
    cFormVar = "w_FADATVAL", cQueryName = "FADATVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 112987810,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=373, Top=21


  add object oFAFLPROV_1_18 as StdCombo with uid="JECYQBBFOD",rtseq=19,rtrep=.f.,left=411,top=79,width=140,height=21, enabled=.f.;
    , ToolTipText = "Stato della distinta";
    , HelpContextID = 42413740;
    , cFormVar="w_FAFLPROV",RowSource=""+"Provvisoria,"+"Confermata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFAFLPROV_1_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FAFLPROV,&i_cF..t_FAFLPROV),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oFAFLPROV_1_18.GetRadio()
    this.Parent.oContained.w_FAFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oFAFLPROV_1_18.ToRadio()
    this.Parent.oContained.w_FAFLPROV=trim(this.Parent.oContained.w_FAFLPROV)
    return(;
      iif(this.Parent.oContained.w_FAFLPROV=='N',1,;
      iif(this.Parent.oContained.w_FAFLPROV=='S',2,;
      0)))
  endfunc

  func oFAFLPROV_1_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oFANOMFIL_1_23 as StdField with uid="DRGCYTJOYO",rtseq=24,rtrep=.f.,;
    cFormVar = "w_FANOMFIL", cQueryName = "FANOMFIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome del file CBI",;
    HelpContextID = 161829214,;
   bGlobalFont=.t.,;
    Height=21, Width=434, Left=106, Top=330, InputMask=replicate('X',254)


  add object oBtn_1_25 as StdButton with uid="IYPJCAFGSD",left=346, top=362, width=48,height=45,;
    CpPicture="BMP\treeview.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire il file CBI";
    , HelpContextID = 207150142;
    , caption='Apri \<XML';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        ViewFile(.w_FANOMFIL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_FANOMFIL) OR .w_FASTATUS<>'2')
    endwith
   endif
  endfunc


  add object oBtn_1_27 as StdButton with uid="NVBXBYHAQH",left=503, top=362, width=48,height=45,;
    CpPicture="bmp\ABBINA.bmp", caption="", nPag=1;
    , ToolTipText = "Abbina documenti";
    , HelpContextID = 59117318;
    , Caption='A\<bbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        do gsar_kgd with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mCond()
    with this.Parent.oContained
      return (.w_FAFLPROV <>'S' AND .cFunction<>'Query')
    endwith
  endfunc

  func oBtn_1_27.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FAFLPROV='S')
    endwith
   endif
  endfunc


  add object oBtn_1_28 as StdButton with uid="IVUYXSPMRW",left=398, top=362, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato alla riga selezionata";
    , HelpContextID = 37515152;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_FARIFDOC, IIF(Isahe(),.w_FLVEAC+.w_CLADOC,.w_CLADOC+.w_FLVEAC))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_FARIFDOC))
    endwith
  endfunc


  add object oBtn_1_29 as StdButton with uid="EPTFZVWHSC",left=450, top=362, width=48,height=45,;
    CpPicture="bmp\GENERA.bmp", caption="", nPag=1;
    , ToolTipText = "Genera file CBI";
    , HelpContextID = 63099494;
    , Caption='G\<enera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        gsar_bdf(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    with this.Parent.oContained
      return (.w_FASTATUS<>'2' and .w_FAFLPROV='N' and .cFunction='Edit')
    endwith
  endfunc

  func oBtn_1_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FASTATUS='2')
    endwith
   endif
  endfunc


  add object oObj_1_33 as cp_runprogram with uid="RZISURDYJP",left=576, top=223, width=187,height=25,;
    caption='Gsar_Bdf(L)',;
   bGlobalFont=.t.,;
    prg="Gsar_bdf('L')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 60944204


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=12, top=111, width=533,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Riga",Field2="TIPDOC",Label2="Causale",Field3="NUMDOC",Label3="Numero",Field4="ALFDOC",Label4="Serie",Field5="DATDOC",Label5="Data",Field6="CODCON",Label6="Intestatario",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235778682

  add object oStr_1_9 as StdString with uid="MKUHFWDHQE",Visible=.t., Left=443, Top=23,;
    Alignment=1, Width=60, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="GZTPZERYYF",Visible=.t., Left=7, Top=50,;
    Alignment=1, Width=71, Height=18,;
    Caption="Banca prop.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="CJHNGURSUO",Visible=.t., Left=191, Top=23,;
    Alignment=1, Width=31, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="UIRAFPKNWW",Visible=.t., Left=10, Top=23,;
    Alignment=1, Width=57, Height=15,;
    Caption="Numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="DAFHILRDBL",Visible=.t., Left=133, Top=23,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="OYXFEIROMM",Visible=.t., Left=335, Top=80,;
    Alignment=1, Width=71, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="YNIRQGQUHQ",Visible=.t., Left=307, Top=23,;
    Alignment=1, Width=64, Height=18,;
    Caption="Data gen.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="OHKRYAMIWK",Visible=.t., Left=26, Top=331,;
    Alignment=1, Width=76, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="OJJPAXWPYJ",Visible=.t., Left=2, Top=80,;
    Alignment=1, Width=76, Height=18,;
    Caption="Stato file:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=3,top=132,;
    width=529+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=4,top=133,width=528+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mdfBodyRow as CPBodyRowCnt
  Width=519
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="PYJFWFBHQR",rtseq=9,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 268107114,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oTIPDOC_2_3 as StdTrsField with uid="FVPPQXQDZU",rtseq=20,rtrep=.t.,;
    cFormVar="w_TIPDOC",value=space(5),enabled=.f.,;
    HelpContextID = 57661238,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=49, Top=0, InputMask=replicate('X',5)

  add object oNUMDOC_2_4 as StdTrsField with uid="FXLBQWSNFT",rtseq=21,rtrep=.t.,;
    cFormVar="w_NUMDOC",value=0,enabled=.f.,;
    HelpContextID = 57651926,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=107, Top=0

  add object oALFDOC_2_5 as StdTrsField with uid="AOSTTSMPRW",rtseq=22,rtrep=.t.,;
    cFormVar="w_ALFDOC",value=space(10),enabled=.f.,;
    HelpContextID = 57620742,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=228, Top=0, InputMask=replicate('X',10)

  add object oDATDOC_2_6 as StdTrsField with uid="DTUPGEPXLN",rtseq=23,rtrep=.t.,;
    cFormVar="w_DATDOC",value=ctod("  /  /  "),enabled=.f.,;
    HelpContextID = 57675318,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=314, Top=0

  add object oCODCON_2_7 as StdTrsField with uid="PEIVVKMBNC",rtseq=25,rtrep=.t.,;
    cFormVar="w_CODCON",value=space(15),enabled=.f.,;
    HelpContextID = 242097190,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=126, Left=388, Top=0, InputMask=replicate('X',15)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mdf','DISMFAEL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FANUMDIS=DISMFAEL.FANUMDIS";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
