* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bvp                                                        *
*              Valorizza parametri                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-18                                                      *
* Last revis.: 2011-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OBJPARENT,w_PRNOMVAL,w_PRVALVAR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bvp",oParentObject,m.w_OBJPARENT,m.w_PRNOMVAL,m.w_PRVALVAR)
return(i_retval)

define class tgsut_bvp as StdBatch
  * --- Local variables
  w_OBJPARENT = .NULL.
  w_PRNOMVAL = space(20)
  w_PRVALVAR = space(30)
  w_OBJ = .NULL.
  w_COUNT = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazioni variabili (da GSJB_BSJ e GSUT_BCG)
    w_PRNOMVAL_GO=this.w_PRNOMVAL
    w_PRVALVAR_GO=this.w_PRVALVAR
    * --- Valorizza il controllo
    * --- Ricerco se associato alla w_xxx vi � un oggetto, se l'oggetto non esiste la variabile 
    *     � nascosta
    this.w_OBJ = this.w_OBJPARENT.GetCtrl( Alltrim(w_PRNOMVAL_GO) )
    if VarType( this.w_OBJ ) = "O" 
      this.w_COUNT = 1
      do while Not ISNULL(this.w_OBJ)
        * --- Valorizzo solo i controlli che sono editabili e visibili per impedire che vengano
        *     valorizzati campi che non potrei valorizzare manualmente
        * --- Gestisco la variabile w_ODES come caso particolare, se non valorizzo questa
        *     variabile dato che � popolata dal bottone ... non vedo che l'output utente
        *     � cambiato, anche se ho valorizzato w_OQRY e w_OREP. (Gestione configurazioni)
        * --- Gestisco la variabile w_Oridat come caso particolare, � popolata all'evento INIT, 
        *     se non passata non viene valorizzato il nome database nel backup database dando errore in:
        *     BackUp Database [ahr70] to DISK = 'C:\Programmi\Microsoft SQL Server\MSSQL.1\MSSQL\Backup\ahr70.BAK' WITH INIT (Gestione configurazioni)
        * --- Inserito controllo su w_PRNOMVAL_GO per evitare errori nel caso in cui 
        *     erroneamente sia presente un record vuoto.
        if !EMPTY(w_PRNOMVAL_GO) AND ((this.w_OBJ.Enabled And this.w_OBJ.Visible) Or ALLTRIM(w_PRNOMVAL_GO)="w_ODES" Or ALLTRIM(w_PRNOMVAL_GO)=="w_OriDat" )
          * --- Assegno alla w_xxx valore
          this.w_OBJPARENT.&w_PRNOMVAL_GO = &w_PRVALVAR_GO
          * --- Se controllo con un link invoco il link Full altrimenti eseguo la Valid
          *                         (se eseguissi la valid per controlli con link eseguirebbe il Link con parametro Part
          *                         e quindi eseguendo una Like sul database)
          if Not Empty( this.w_OBJ.clinkFile )
            * --- Se vi � un oggetto lancio la link ad esso legata con parametro Full.
            *                             Per farlo utilizzo una Macro costruendo il nome della procedura ricercando 
            *                             il secondo "_" a partire dalla fine (Es. _2_154 per MVCODMAG_2_154).
            *                             Quindi invoco Link_2_154 dell'oggetto maschera/anagrafica..
            *                             DI seguito invoco la changed Es w_MVQTAMOV Changed..
             
 L_Macro="This.w_OBJPARENT.link"+Right( this.w_OBJ.Name , LEN( this.w_OBJ.Name ) - RAT("_", this.w_OBJ.Name ,2)+1)+"('Full')" 
 &L_Macro 
 L_Macro="This.w_OBJPARENT.NotifyEvent('"+Alltrim( w_PRNOMVAL_GO )+" Changed')" 
 &L_Macro
            * --- Aggiorno i calcoli derivati dalla modifica della variabile
            this.w_OBJPARENT.mCalc(.T.)     
            * --- Aggiorno le o_xxx
            this.w_OBJPARENT.SaveDependsOn()     
          else
            * --- Non � un campo con un link, valorizzo il controllo con il valore ed eseguo
            *                             la valid.
            *                             SetControlsValue valorizza tutti i controlli, occorre utilizzarlo per gestire
            *                             correttamente anche i controlli di tipo Combo/Radio Group
            this.w_OBJ.bUpd = .t.
            this.w_OBJPARENT.SetControlsValue()     
            this.w_OBJ.Valid()     
          endif
        endif
        this.w_COUNT = this.w_COUNT + 1
        this.w_OBJ = this.w_OBJPARENT.GetCtrl( Alltrim(w_PRNOMVAL_GO), this.w_COUNT )
      enddo
    else
      * --- Assegno alla w_xxx valore
      this.w_OBJPARENT.&w_PRNOMVAL_GO = &w_PRVALVAR_GO
    endif
    this.w_OBJ = .NULL.
  endproc


  proc Init(oParentObject,w_OBJPARENT,w_PRNOMVAL,w_PRVALVAR)
    this.w_OBJPARENT=w_OBJPARENT
    this.w_PRNOMVAL=w_PRNOMVAL
    this.w_PRVALVAR=w_PRVALVAR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OBJPARENT,w_PRNOMVAL,w_PRVALVAR"
endproc
