* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_map                                                        *
*              Automatismi primanota                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_21]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-17                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_map")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_map")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_map")
  return

* --- Class definition
define class tgscg_map as StdPCForm
  Width  = 672
  Height = 166
  Top    = 14
  Left   = 20
  cComment = "Automatismi primanota"
  cPrg = "gscg_map"
  HelpContextID=137747095
  add object cnt as tcgscg_map
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_map as PCContext
  w_APCODCAU = space(5)
  w_APTIPCLF = space(1)
  w_APCODCLF = space(15)
  w_CAURIG = space(5)
  w_CAUPAR = space(1)
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_PARTSN = space(1)
  w_APTIPCON = space(1)
  w_APCODCON = space(15)
  w_DESCON = space(40)
  w_APFLDAVE = space(1)
  w_APFLAUTO = space(1)
  w_APFLPART = space(1)
  w_APCAURIG = space(5)
  w_TIPSOT = space(10)
  w_APCAUMOV = space(5)
  w_DESCAU = space(35)
  w_APCONBAN = space(15)
  w_TIPCAU = space(1)
  w_CCOBSO = space(8)
  w_FLCOMP = space(1)
  w_TIPCON = space(1)
  w_FLMOVC = space(1)
  proc Save(i_oFrom)
    this.w_APCODCAU = i_oFrom.w_APCODCAU
    this.w_APTIPCLF = i_oFrom.w_APTIPCLF
    this.w_APCODCLF = i_oFrom.w_APCODCLF
    this.w_CAURIG = i_oFrom.w_CAURIG
    this.w_CAUPAR = i_oFrom.w_CAUPAR
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_PARTSN = i_oFrom.w_PARTSN
    this.w_APTIPCON = i_oFrom.w_APTIPCON
    this.w_APCODCON = i_oFrom.w_APCODCON
    this.w_DESCON = i_oFrom.w_DESCON
    this.w_APFLDAVE = i_oFrom.w_APFLDAVE
    this.w_APFLAUTO = i_oFrom.w_APFLAUTO
    this.w_APFLPART = i_oFrom.w_APFLPART
    this.w_APCAURIG = i_oFrom.w_APCAURIG
    this.w_TIPSOT = i_oFrom.w_TIPSOT
    this.w_APCAUMOV = i_oFrom.w_APCAUMOV
    this.w_DESCAU = i_oFrom.w_DESCAU
    this.w_APCONBAN = i_oFrom.w_APCONBAN
    this.w_TIPCAU = i_oFrom.w_TIPCAU
    this.w_CCOBSO = i_oFrom.w_CCOBSO
    this.w_FLCOMP = i_oFrom.w_FLCOMP
    this.w_TIPCON = i_oFrom.w_TIPCON
    this.w_FLMOVC = i_oFrom.w_FLMOVC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_APCODCAU = this.w_APCODCAU
    i_oTo.w_APTIPCLF = this.w_APTIPCLF
    i_oTo.w_APCODCLF = this.w_APCODCLF
    i_oTo.w_CAURIG = this.w_CAURIG
    i_oTo.w_CAUPAR = this.w_CAUPAR
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_PARTSN = this.w_PARTSN
    i_oTo.w_APTIPCON = this.w_APTIPCON
    i_oTo.w_APCODCON = this.w_APCODCON
    i_oTo.w_DESCON = this.w_DESCON
    i_oTo.w_APFLDAVE = this.w_APFLDAVE
    i_oTo.w_APFLAUTO = this.w_APFLAUTO
    i_oTo.w_APFLPART = this.w_APFLPART
    i_oTo.w_APCAURIG = this.w_APCAURIG
    i_oTo.w_TIPSOT = this.w_TIPSOT
    i_oTo.w_APCAUMOV = this.w_APCAUMOV
    i_oTo.w_DESCAU = this.w_DESCAU
    i_oTo.w_APCONBAN = this.w_APCONBAN
    i_oTo.w_TIPCAU = this.w_TIPCAU
    i_oTo.w_CCOBSO = this.w_CCOBSO
    i_oTo.w_FLCOMP = this.w_FLCOMP
    i_oTo.w_TIPCON = this.w_TIPCON
    i_oTo.w_FLMOVC = this.w_FLMOVC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_map as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 672
  Height = 166
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=137747095
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CAUPRI_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  COC_MAST_IDX = 0
  CCC_MAST_IDX = 0
  cFile = "CAUPRI"
  cKeySelect = "APCODCAU,APTIPCLF,APCODCLF"
  cKeyWhere  = "APCODCAU=this.w_APCODCAU and APTIPCLF=this.w_APTIPCLF and APCODCLF=this.w_APCODCLF"
  cKeyDetail  = "APCODCAU=this.w_APCODCAU and APTIPCLF=this.w_APTIPCLF and APCODCLF=this.w_APCODCLF"
  cKeyWhereODBC = '"APCODCAU="+cp_ToStrODBC(this.w_APCODCAU)';
      +'+" and APTIPCLF="+cp_ToStrODBC(this.w_APTIPCLF)';
      +'+" and APCODCLF="+cp_ToStrODBC(this.w_APCODCLF)';

  cKeyDetailWhereODBC = '"APCODCAU="+cp_ToStrODBC(this.w_APCODCAU)';
      +'+" and APTIPCLF="+cp_ToStrODBC(this.w_APTIPCLF)';
      +'+" and APCODCLF="+cp_ToStrODBC(this.w_APCODCLF)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"CAUPRI.APCODCAU="+cp_ToStrODBC(this.w_APCODCAU)';
      +'+" and CAUPRI.APTIPCLF="+cp_ToStrODBC(this.w_APTIPCLF)';
      +'+" and CAUPRI.APCODCLF="+cp_ToStrODBC(this.w_APCODCLF)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CAUPRI.CPROWNUM '
  cPrg = "gscg_map"
  cComment = "Automatismi primanota"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_APCODCAU = space(5)
  w_APTIPCLF = space(1)
  w_APCODCLF = space(15)
  w_CAURIG = space(5)
  w_CAUPAR = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_PARTSN = space(1)
  w_APTIPCON = space(1)
  o_APTIPCON = space(1)
  w_APCODCON = space(15)
  o_APCODCON = space(15)
  w_DESCON = space(40)
  w_APFLDAVE = space(1)
  w_APFLAUTO = space(1)
  w_APFLPART = space(1)
  w_APCAURIG = space(5)
  w_TIPSOT = space(10)
  w_APCAUMOV = space(5)
  w_DESCAU = space(35)
  w_APCONBAN = space(15)
  w_TIPCAU = space(1)
  w_CCOBSO = ctod('  /  /  ')
  w_FLCOMP = space(1)
  w_TIPCON = space(1)
  w_FLMOVC = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mapPag1","gscg_map",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='COC_MAST'
    this.cWorkTables[4]='CCC_MAST'
    this.cWorkTables[5]='CAUPRI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAUPRI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAUPRI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_map'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_8_joined
    link_2_8_joined=.f.
    local link_2_10_joined
    link_2_10_joined=.f.
    local link_2_12_joined
    link_2_12_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CAUPRI where APCODCAU=KeySet.APCODCAU
    *                            and APTIPCLF=KeySet.APTIPCLF
    *                            and APCODCLF=KeySet.APCODCLF
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CAUPRI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAUPRI_IDX,2],this.bLoadRecFilter,this.CAUPRI_IDX,"gscg_map")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAUPRI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAUPRI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAUPRI '
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_10_joined=this.AddJoinedLink_2_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_12_joined=this.AddJoinedLink_2_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'APCODCAU',this.w_APCODCAU  ,'APTIPCLF',this.w_APTIPCLF  ,'APCODCLF',this.w_APCODCLF  )
      select * from (i_cTable) CAUPRI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_APCODCAU = NVL(APCODCAU,space(5))
        .w_APTIPCLF = NVL(APTIPCLF,space(1))
        .w_APCODCLF = NVL(APCODCLF,space(15))
        .w_CAURIG = this.oParentObject .w_CCCODICE
        .w_CAUPAR = this.oParentObject .w_CFLPART
        .w_FLMOVC = this.oParentObject .w_FLMOVC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CAUPRI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_PARTSN = space(1)
          .w_DESCON = space(40)
          .w_TIPSOT = space(10)
          .w_DESCAU = space(35)
          .w_TIPCAU = space(1)
          .w_CCOBSO = ctod("  /  /  ")
          .w_FLCOMP = space(1)
          .w_TIPCON = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_APTIPCON = NVL(APTIPCON,space(1))
          .w_APCODCON = NVL(APCODCON,space(15))
          if link_2_3_joined
            this.w_APCODCON = NVL(ANCODICE203,NVL(this.w_APCODCON,space(15)))
            this.w_DESCON = NVL(ANDESCRI203,space(40))
            this.w_PARTSN = NVL(ANPARTSN203,space(1))
            this.w_CCOBSO = NVL(cp_ToDate(ANDTOBSO203),ctod("  /  /  "))
            this.w_TIPSOT = NVL(ANTIPSOT203,space(10))
          else
          .link_2_3('Load')
          endif
          .w_APFLDAVE = NVL(APFLDAVE,space(1))
          .w_APFLAUTO = NVL(APFLAUTO,space(1))
          .w_APFLPART = NVL(APFLPART,space(1))
          .w_APCAURIG = NVL(APCAURIG,space(5))
          if link_2_8_joined
            this.w_APCAURIG = NVL(CCCODICE208,NVL(this.w_APCAURIG,space(5)))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO208),ctod("  /  /  "))
          else
          .link_2_8('Load')
          endif
          .w_APCAUMOV = NVL(APCAUMOV,space(5))
          if link_2_10_joined
            this.w_APCAUMOV = NVL(CACODICE210,NVL(this.w_APCAUMOV,space(5)))
            this.w_DESCAU = NVL(CADESCRI210,space(35))
            this.w_FLCOMP = NVL(CAFLCOMP210,space(1))
            this.w_TIPCAU = NVL(CATIPCON210,space(1))
          else
          .link_2_10('Load')
          endif
          .w_APCONBAN = NVL(APCONBAN,space(15))
          if link_2_12_joined
            this.w_APCONBAN = NVL(BACODBAN212,NVL(this.w_APCONBAN,space(15)))
            this.w_CCOBSO = NVL(cp_ToDate(BADTOBSO212),ctod("  /  /  "))
            this.w_TIPCON = NVL(BATIPCON212,space(1))
          else
          .link_2_12('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_2_14.Calculate(IIF(g_BANC<>'S' OR ( this.oParentObject .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ), '','Causale C\C:'))
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate(IIF(g_BANC<>'S' OR ( this.oParentObject .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ), '','Conto Corrente:'))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CAURIG = this.oParentObject .w_CCCODICE
        .w_CAUPAR = this.oParentObject .w_CFLPART
        .w_FLMOVC = this.oParentObject .w_FLMOVC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_APCODCAU=space(5)
      .w_APTIPCLF=space(1)
      .w_APCODCLF=space(15)
      .w_CAURIG=space(5)
      .w_CAUPAR=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_PARTSN=space(1)
      .w_APTIPCON=space(1)
      .w_APCODCON=space(15)
      .w_DESCON=space(40)
      .w_APFLDAVE=space(1)
      .w_APFLAUTO=space(1)
      .w_APFLPART=space(1)
      .w_APCAURIG=space(5)
      .w_TIPSOT=space(10)
      .w_APCAUMOV=space(5)
      .w_DESCAU=space(35)
      .w_APCONBAN=space(15)
      .w_TIPCAU=space(1)
      .w_CCOBSO=ctod("  /  /  ")
      .w_FLCOMP=space(1)
      .w_TIPCON=space(1)
      .w_FLMOVC=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_CAURIG = this.oParentObject .w_CCCODICE
        .w_CAUPAR = this.oParentObject .w_CFLPART
        .w_OBTEST = i_datsys
        .DoRTCalc(7,10,.f.)
        if not(empty(.w_APCODCON))
         .link_2_3('Full')
        endif
        .DoRTCalc(11,11,.f.)
        .w_APFLDAVE = "D"
        .w_APFLAUTO = 'N'
        .w_APFLPART = IIF(.w_PARTSN="S", .w_CAUPAR, "N")
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_APCAURIG))
         .link_2_8('Full')
        endif
        .DoRTCalc(16,16,.f.)
        .w_APCAUMOV = SPACE(5)
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_APCAUMOV))
         .link_2_10('Full')
        endif
        .DoRTCalc(18,18,.f.)
        .w_APCONBAN = SPACE(15)
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_APCONBAN))
         .link_2_12('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_2_14.Calculate(IIF(g_BANC<>'S' OR ( this.oParentObject .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ), '','Causale C\C:'))
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate(IIF(g_BANC<>'S' OR ( this.oParentObject .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ), '','Conto Corrente:'))
        .DoRTCalc(20,23,.f.)
        .w_FLMOVC = this.oParentObject .w_FLMOVC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAUPRI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oAPCAUMOV_2_10.enabled = i_bVal
      .Page1.oPag.oAPCONBAN_2_12.enabled = i_bVal
      .Page1.oPag.oObj_2_14.enabled = i_bVal
      .Page1.oPag.oObj_2_15.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CAUPRI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAUPRI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APCODCAU,"APCODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APTIPCLF,"APTIPCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_APCODCLF,"APCODCLF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_APTIPCON C(1);
      ,t_APCODCON C(15);
      ,t_DESCON C(40);
      ,t_APFLDAVE N(3);
      ,t_APFLAUTO N(3);
      ,t_APFLPART N(3);
      ,t_APCAURIG C(5);
      ,t_APCAUMOV C(5);
      ,t_DESCAU C(35);
      ,t_APCONBAN C(15);
      ,CPROWNUM N(10);
      ,t_PARTSN C(1);
      ,t_TIPSOT C(10);
      ,t_TIPCAU C(1);
      ,t_CCOBSO D(8);
      ,t_FLCOMP C(1);
      ,t_TIPCON C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mapbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAPTIPCON_2_2.controlsource=this.cTrsName+'.t_APTIPCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAPCODCON_2_3.controlsource=this.cTrsName+'.t_APCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCON_2_4.controlsource=this.cTrsName+'.t_DESCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLDAVE_2_5.controlsource=this.cTrsName+'.t_APFLDAVE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLAUTO_2_6.controlsource=this.cTrsName+'.t_APFLAUTO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLPART_2_7.controlsource=this.cTrsName+'.t_APFLPART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAPCAURIG_2_8.controlsource=this.cTrsName+'.t_APCAURIG'
    this.oPgFRm.Page1.oPag.oAPCAUMOV_2_10.controlsource=this.cTrsName+'.t_APCAUMOV'
    this.oPgFRm.Page1.oPag.oDESCAU_2_11.controlsource=this.cTrsName+'.t_DESCAU'
    this.oPgFRm.Page1.oPag.oAPCONBAN_2_12.controlsource=this.cTrsName+'.t_APCONBAN'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(36)
    this.AddVLine(175)
    this.AddVLine(426)
    this.AddVLine(486)
    this.AddVLine(522)
    this.AddVLine(582)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPTIPCON_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAUPRI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAUPRI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAUPRI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAUPRI_IDX,2])
      *
      * insert into CAUPRI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAUPRI')
        i_extval=cp_InsertValODBCExtFlds(this,'CAUPRI')
        i_cFldBody=" "+;
                  "(APCODCAU,APTIPCLF,APCODCLF,APTIPCON,APCODCON"+;
                  ",APFLDAVE,APFLAUTO,APFLPART,APCAURIG,APCAUMOV"+;
                  ",APCONBAN,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_APCODCAU)+","+cp_ToStrODBC(this.w_APTIPCLF)+","+cp_ToStrODBC(this.w_APCODCLF)+","+cp_ToStrODBC(this.w_APTIPCON)+","+cp_ToStrODBCNull(this.w_APCODCON)+;
             ","+cp_ToStrODBC(this.w_APFLDAVE)+","+cp_ToStrODBC(this.w_APFLAUTO)+","+cp_ToStrODBC(this.w_APFLPART)+","+cp_ToStrODBCNull(this.w_APCAURIG)+","+cp_ToStrODBCNull(this.w_APCAUMOV)+;
             ","+cp_ToStrODBCNull(this.w_APCONBAN)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAUPRI')
        i_extval=cp_InsertValVFPExtFlds(this,'CAUPRI')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'APCODCAU',this.w_APCODCAU,'APTIPCLF',this.w_APTIPCLF,'APCODCLF',this.w_APCODCLF)
        INSERT INTO (i_cTable) (;
                   APCODCAU;
                  ,APTIPCLF;
                  ,APCODCLF;
                  ,APTIPCON;
                  ,APCODCON;
                  ,APFLDAVE;
                  ,APFLAUTO;
                  ,APFLPART;
                  ,APCAURIG;
                  ,APCAUMOV;
                  ,APCONBAN;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_APCODCAU;
                  ,this.w_APTIPCLF;
                  ,this.w_APCODCLF;
                  ,this.w_APTIPCON;
                  ,this.w_APCODCON;
                  ,this.w_APFLDAVE;
                  ,this.w_APFLAUTO;
                  ,this.w_APFLPART;
                  ,this.w_APCAURIG;
                  ,this.w_APCAUMOV;
                  ,this.w_APCONBAN;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CAUPRI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAUPRI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_APTIPCON<>' ' ) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CAUPRI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CAUPRI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_APTIPCON<>' ' ) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CAUPRI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CAUPRI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " APTIPCON="+cp_ToStrODBC(this.w_APTIPCON)+;
                     ",APCODCON="+cp_ToStrODBCNull(this.w_APCODCON)+;
                     ",APFLDAVE="+cp_ToStrODBC(this.w_APFLDAVE)+;
                     ",APFLAUTO="+cp_ToStrODBC(this.w_APFLAUTO)+;
                     ",APFLPART="+cp_ToStrODBC(this.w_APFLPART)+;
                     ",APCAURIG="+cp_ToStrODBCNull(this.w_APCAURIG)+;
                     ",APCAUMOV="+cp_ToStrODBCNull(this.w_APCAUMOV)+;
                     ",APCONBAN="+cp_ToStrODBCNull(this.w_APCONBAN)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CAUPRI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      APTIPCON=this.w_APTIPCON;
                     ,APCODCON=this.w_APCODCON;
                     ,APFLDAVE=this.w_APFLDAVE;
                     ,APFLAUTO=this.w_APFLAUTO;
                     ,APFLPART=this.w_APFLPART;
                     ,APCAURIG=this.w_APCAURIG;
                     ,APCAUMOV=this.w_APCAUMOV;
                     ,APCONBAN=this.w_APCONBAN;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAUPRI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAUPRI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_APTIPCON<>' ' ) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CAUPRI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_APTIPCON<>' ' ) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAUPRI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAUPRI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .w_CAURIG = this.oParentObject .w_CCCODICE
          .w_CAUPAR = this.oParentObject .w_CFLPART
        .DoRTCalc(6,13,.t.)
        if .o_APTIPCON<>.w_APTIPCON.or. .o_APCODCON<>.w_APCODCON
          .w_APFLPART = IIF(.w_PARTSN="S", .w_CAUPAR, "N")
        endif
        .DoRTCalc(15,16,.t.)
        if .o_APCODCON<>.w_APCODCON
          .link_2_10('Full')
        endif
        .DoRTCalc(18,18,.t.)
        if .o_APCODCON<>.w_APCODCON
          .link_2_12('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_2_14.Calculate(IIF(g_BANC<>'S' OR ( this.oParentObject .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ), '','Causale C\C:'))
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate(IIF(g_BANC<>'S' OR ( this.oParentObject .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ), '','Conto Corrente:'))
        .DoRTCalc(20,23,.t.)
          .w_FLMOVC = this.oParentObject .w_FLMOVC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PARTSN with this.w_PARTSN
      replace t_TIPSOT with this.w_TIPSOT
      replace t_TIPCAU with this.w_TIPCAU
      replace t_CCOBSO with this.w_CCOBSO
      replace t_FLCOMP with this.w_FLCOMP
      replace t_TIPCON with this.w_TIPCON
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_14.Calculate(IIF(g_BANC<>'S' OR ( this.oParentObject .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ), '','Causale C\C:'))
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate(IIF(g_BANC<>'S' OR ( this.oParentObject .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ), '','Conto Corrente:'))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_14.Calculate(IIF(g_BANC<>'S' OR ( this.oParentObject .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ), '','Causale C\C:'))
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate(IIF(g_BANC<>'S' OR ( this.oParentObject .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ), '','Conto Corrente:'))
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAPCODCON_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAPCODCON_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAPFLPART_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAPFLPART_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oAPCAUMOV_2_10.visible=!this.oPgFrm.Page1.oPag.oAPCAUMOV_2_10.mHide()
    this.oPgFrm.Page1.oPag.oDESCAU_2_11.visible=!this.oPgFrm.Page1.oPag.oDESCAU_2_11.mHide()
    this.oPgFrm.Page1.oPag.oAPCONBAN_2_12.visible=!this.oPgFrm.Page1.oPag.oAPCONBAN_2_12.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_2_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=APCODCON
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_APCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_APCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_APTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO,ANTIPSOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_APTIPCON;
                     ,'ANCODICE',trim(this.w_APCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO,ANTIPSOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_APCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_APCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_APTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO,ANTIPSOT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_APCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_APTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_APCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAPCODCON_2_3'),i_cWhere,'GSAR_BZC',"Elenco conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_APTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO,ANTIPSOT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO,ANTIPSOT";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_APTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_APCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO,ANTIPSOT";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_APCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_APTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_APTIPCON;
                       ,'ANCODICE',this.w_APCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_APCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
      this.w_CCOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_TIPSOT = NVL(_Link_.ANTIPSOT,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_APCODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_PARTSN = space(1)
      this.w_CCOBSO = ctod("  /  /  ")
      this.w_TIPSOT = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_APCODCON = space(15)
        this.w_DESCON = space(40)
        this.w_PARTSN = space(1)
        this.w_CCOBSO = ctod("  /  /  ")
        this.w_TIPSOT = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_APCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ANCODICE as ANCODICE203"+ ",link_2_3.ANDESCRI as ANDESCRI203"+ ",link_2_3.ANPARTSN as ANPARTSN203"+ ",link_2_3.ANDTOBSO as ANDTOBSO203"+ ",link_2_3.ANTIPSOT as ANTIPSOT203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on CAUPRI.APCODCON=link_2_3.ANCODICE"+" and CAUPRI.APTIPCON=link_2_3.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and CAUPRI.APCODCON=link_2_3.ANCODICE(+)"'+'+" and CAUPRI.APTIPCON=link_2_3.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=APCAURIG
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_APCAURIG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_APCAURIG)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_APCAURIG))
          select CCCODICE,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_APCAURIG)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_APCAURIG) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oAPCAURIG_2_8'),i_cWhere,'',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_APCAURIG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_APCAURIG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_APCAURIG)
            select CCCODICE,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_APCAURIG = NVL(_Link_.CCCODICE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_APCAURIG = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente oppure obsoleta")
        endif
        this.w_APCAURIG = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_APCAURIG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.CCCODICE as CCCODICE208"+ ",link_2_8.CCDTOBSO as CCDTOBSO208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on CAUPRI.APCAURIG=link_2_8.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and CAUPRI.APCAURIG=link_2_8.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=APCAUMOV
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_lTable = "CCC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2], .t., this.CCC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_APCAUMOV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBA_MCT',True,'CCC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_APCAUMOV)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLCOMP,CATIPCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_APCAUMOV))
          select CACODICE,CADESCRI,CAFLCOMP,CATIPCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_APCAUMOV)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_APCAUMOV) and !this.bDontReportError
            deferred_cp_zoom('CCC_MAST','*','CACODICE',cp_AbsName(oSource.parent,'oAPCAUMOV_2_10'),i_cWhere,'GSBA_MCT',"Causali movimenti C\C",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLCOMP,CATIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CAFLCOMP,CATIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_APCAUMOV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLCOMP,CATIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_APCAUMOV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_APCAUMOV)
            select CACODICE,CADESCRI,CAFLCOMP,CATIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_APCAUMOV = NVL(_Link_.CACODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CADESCRI,space(35))
      this.w_FLCOMP = NVL(_Link_.CAFLCOMP,space(1))
      this.w_TIPCAU = NVL(_Link_.CATIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_APCAUMOV = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_FLCOMP = space(1)
      this.w_TIPCAU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CCC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_APCAUMOV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CCC_MAST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_10.CACODICE as CACODICE210"+ ",link_2_10.CADESCRI as CADESCRI210"+ ",link_2_10.CAFLCOMP as CAFLCOMP210"+ ",link_2_10.CATIPCON as CATIPCON210"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_10 on CAUPRI.APCAUMOV=link_2_10.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_10"
          i_cKey=i_cKey+'+" and CAUPRI.APCAUMOV=link_2_10.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=APCONBAN
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_APCONBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_APCONBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADTOBSO,BATIPCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_APCONBAN))
          select BACODBAN,BADTOBSO,BATIPCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_APCONBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_APCONBAN) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oAPCONBAN_2_12'),i_cWhere,'GSTE_ACB',"Conti correnti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADTOBSO,BATIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADTOBSO,BATIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_APCONBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADTOBSO,BATIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_APCONBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_APCONBAN)
            select BACODBAN,BADTOBSO,BATIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_APCONBAN = NVL(_Link_.BACODBAN,space(15))
      this.w_CCOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_TIPCON = NVL(_Link_.BATIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_APCONBAN = space(15)
      endif
      this.w_CCOBSO = ctod("  /  /  ")
      this.w_TIPCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF(g_BANC='S',(EMPTY(.w_APCONBAN) OR .w_FLCOMP<>'N' OR CHKNUMCR(.w_APCAUMOV, .w_TIPCAU, .w_APCONBAN, .w_TIPCON) AND (.w_CCOBSO>.w_OBTEST OR EMPTY(.w_CCOBSO))),.T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto corrente obsoleto o non congruente")
        endif
        this.w_APCONBAN = space(15)
        this.w_CCOBSO = ctod("  /  /  ")
        this.w_TIPCON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_APCONBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_12.BACODBAN as BACODBAN212"+ ",link_2_12.BADTOBSO as BADTOBSO212"+ ",link_2_12.BATIPCON as BATIPCON212"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_12 on CAUPRI.APCONBAN=link_2_12.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_12"
          i_cKey=i_cKey+'+" and CAUPRI.APCONBAN=link_2_12.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oAPCAUMOV_2_10.value==this.w_APCAUMOV)
      this.oPgFrm.Page1.oPag.oAPCAUMOV_2_10.value=this.w_APCAUMOV
      replace t_APCAUMOV with this.oPgFrm.Page1.oPag.oAPCAUMOV_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_2_11.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_2_11.value=this.w_DESCAU
      replace t_DESCAU with this.oPgFrm.Page1.oPag.oDESCAU_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oAPCONBAN_2_12.value==this.w_APCONBAN)
      this.oPgFrm.Page1.oPag.oAPCONBAN_2_12.value=this.w_APCONBAN
      replace t_APCONBAN with this.oPgFrm.Page1.oPag.oAPCONBAN_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPTIPCON_2_2.value==this.w_APTIPCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPTIPCON_2_2.value=this.w_APTIPCON
      replace t_APTIPCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPTIPCON_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPCODCON_2_3.value==this.w_APCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPCODCON_2_3.value=this.w_APCODCON
      replace t_APCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPCODCON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCON_2_4.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCON_2_4.value=this.w_DESCON
      replace t_DESCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCON_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLDAVE_2_5.RadioValue()==this.w_APFLDAVE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLDAVE_2_5.SetRadio()
      replace t_APFLDAVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLDAVE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLAUTO_2_6.RadioValue()==this.w_APFLAUTO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLAUTO_2_6.SetRadio()
      replace t_APFLAUTO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLAUTO_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLPART_2_7.RadioValue()==this.w_APFLPART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLPART_2_7.SetRadio()
      replace t_APFLPART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLPART_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPCAURIG_2_8.value==this.w_APCAURIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPCAURIG_2_8.value=this.w_APCAURIG
      replace t_APCAURIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPCAURIG_2_8.value
    endif
    cp_SetControlsValueExtFlds(this,'CAUPRI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_APTIPCON$"CFG") and (.w_APTIPCON<>' ' )
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPTIPCON_2_2
          i_bRes = .f.
          i_bnoChk = .f.
        case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)) and (.w_APTIPCON$"CFG") and not(empty(.w_APCODCON)) and (.w_APTIPCON<>' ' )
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPCODCON_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        case   not(.w_APFLDAVE$"DA") and (.w_APTIPCON<>' ' )
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLDAVE_2_5
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_APFLPART$"CSA" OR .w_CAUPAR="N" OR .w_PARTSN<>"S") and ((.w_PARTSN="S" OR .w_APCODCON=SPACE(15)) AND .w_CAUPAR<>"N") and (.w_APTIPCON<>' ' )
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLPART_2_7
          i_bRes = .f.
          i_bnoChk = .f.
        case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)) and not(empty(.w_APCAURIG)) and (.w_APTIPCON<>' ' )
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPCAURIG_2_8
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente oppure obsoleta")
        case   not(IIF(g_BANC='S',(EMPTY(.w_APCONBAN) OR .w_FLCOMP<>'N' OR CHKNUMCR(.w_APCAUMOV, .w_TIPCAU, .w_APCONBAN, .w_TIPCON) AND (.w_CCOBSO>.w_OBTEST OR EMPTY(.w_CCOBSO))),.T.)) and not(empty(.w_APCONBAN)) and (.w_APTIPCON<>' ' )
          .oNewFocus=.oPgFrm.Page1.oPag.oAPCONBAN_2_12
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Conto corrente obsoleto o non congruente")
      endcase
      if .w_APTIPCON<>' ' 
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_APTIPCON = this.w_APTIPCON
    this.o_APCODCON = this.w_APCODCON
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_APTIPCON<>' ' )
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PARTSN=space(1)
      .w_APTIPCON=space(1)
      .w_APCODCON=space(15)
      .w_DESCON=space(40)
      .w_APFLDAVE=space(1)
      .w_APFLAUTO=space(1)
      .w_APFLPART=space(1)
      .w_APCAURIG=space(5)
      .w_TIPSOT=space(10)
      .w_APCAUMOV=space(5)
      .w_DESCAU=space(35)
      .w_APCONBAN=space(15)
      .w_TIPCAU=space(1)
      .w_CCOBSO=ctod("  /  /  ")
      .w_FLCOMP=space(1)
      .w_TIPCON=space(1)
      .DoRTCalc(1,10,.f.)
      if not(empty(.w_APCODCON))
        .link_2_3('Full')
      endif
      .DoRTCalc(11,11,.f.)
        .w_APFLDAVE = "D"
        .w_APFLAUTO = 'N'
        .w_APFLPART = IIF(.w_PARTSN="S", .w_CAUPAR, "N")
      .DoRTCalc(15,15,.f.)
      if not(empty(.w_APCAURIG))
        .link_2_8('Full')
      endif
      .DoRTCalc(16,16,.f.)
        .w_APCAUMOV = SPACE(5)
      .DoRTCalc(17,17,.f.)
      if not(empty(.w_APCAUMOV))
        .link_2_10('Full')
      endif
      .DoRTCalc(18,18,.f.)
        .w_APCONBAN = SPACE(15)
      .DoRTCalc(19,19,.f.)
      if not(empty(.w_APCONBAN))
        .link_2_12('Full')
      endif
        .oPgFrm.Page1.oPag.oObj_2_14.Calculate(IIF(g_BANC<>'S' OR ( this.oParentObject .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ), '','Causale C\C:'))
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate(IIF(g_BANC<>'S' OR ( this.oParentObject .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ), '','Conto Corrente:'))
    endwith
    this.DoRTCalc(20,24,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PARTSN = t_PARTSN
    this.w_APTIPCON = t_APTIPCON
    this.w_APCODCON = t_APCODCON
    this.w_DESCON = t_DESCON
    this.w_APFLDAVE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLDAVE_2_5.RadioValue(.t.)
    this.w_APFLAUTO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLAUTO_2_6.RadioValue(.t.)
    this.w_APFLPART = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLPART_2_7.RadioValue(.t.)
    this.w_APCAURIG = t_APCAURIG
    this.w_TIPSOT = t_TIPSOT
    this.w_APCAUMOV = t_APCAUMOV
    this.w_DESCAU = t_DESCAU
    this.w_APCONBAN = t_APCONBAN
    this.w_TIPCAU = t_TIPCAU
    this.w_CCOBSO = t_CCOBSO
    this.w_FLCOMP = t_FLCOMP
    this.w_TIPCON = t_TIPCON
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PARTSN with this.w_PARTSN
    replace t_APTIPCON with this.w_APTIPCON
    replace t_APCODCON with this.w_APCODCON
    replace t_DESCON with this.w_DESCON
    replace t_APFLDAVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLDAVE_2_5.ToRadio()
    replace t_APFLAUTO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLAUTO_2_6.ToRadio()
    replace t_APFLPART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAPFLPART_2_7.ToRadio()
    replace t_APCAURIG with this.w_APCAURIG
    replace t_TIPSOT with this.w_TIPSOT
    replace t_APCAUMOV with this.w_APCAUMOV
    replace t_DESCAU with this.w_DESCAU
    replace t_APCONBAN with this.w_APCONBAN
    replace t_TIPCAU with this.w_TIPCAU
    replace t_CCOBSO with this.w_CCOBSO
    replace t_FLCOMP with this.w_FLCOMP
    replace t_TIPCON with this.w_TIPCON
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mapPag1 as StdContainer
  Width  = 668
  height = 166
  stdWidth  = 668
  stdheight = 166
  resizeXpos=333
  resizeYpos=54
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=1, top=-2, width=659,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="APTIPCON",Label1="Tipo",Field2="APCODCON",Label2="Conto",Field3="DESCON",Label3="Descrizione",Field4="APFLDAVE",Label4="Sezione",Field5="APFLAUTO",Label5="Auto.",Field6="APFLPART",Label6="Partite",Field7="APCAURIG",Label7="Causale",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 83032966

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-9,top=17,;
    width=655+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-8,top=18,width=654+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CONTI|CAU_CONT|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oAPCAUMOV_2_10.Refresh()
      this.Parent.oDESCAU_2_11.Refresh()
      this.Parent.oAPCONBAN_2_12.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oAPCODCON_2_3
      case cFile='CAU_CONT'
        oDropInto=this.oBodyCol.oRow.oAPCAURIG_2_8
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oAPCAUMOV_2_10 as StdTrsField with uid="BLBNZFZXRE",rtseq=17,rtrep=.t.,;
    cFormVar="w_APCAUMOV",value=space(5),;
    ToolTipText = "Causale movimenti di conto corrente",;
    HelpContextID = 87335076,;
    cTotal="", bFixedPos=.t., cQueryName = "APCAUMOV",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=88, Top=138, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCC_MAST", cZoomOnZoom="GSBA_MCT", oKey_1_1="CACODICE", oKey_1_2="this.w_APCAUMOV"

  func oAPCAUMOV_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_BANC<>'S' OR (  .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ))
    endwith
    endif
  endfunc

  func oAPCAUMOV_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oAPCAUMOV_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oAPCAUMOV_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCC_MAST','*','CACODICE',cp_AbsName(this.parent,'oAPCAUMOV_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBA_MCT',"Causali movimenti C\C",'',this.parent.oContained
  endproc
  proc oAPCAUMOV_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSBA_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_APCAUMOV
    i_obj.ecpSave()
  endproc

  add object oDESCAU_2_11 as StdTrsField with uid="SHWKACJVFA",rtseq=18,rtrep=.t.,;
    cFormVar="w_DESCAU",value=space(35),enabled=.f.,;
    HelpContextID = 242330570,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCAU",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=137, Top=138, InputMask=replicate('X',35)

  func oDESCAU_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_BANC<>'S' OR ( .w_FLMOVC <>'S' OR .w_TIPSOT<>'B' ))
    endwith
    endif
  endfunc

  add object oAPCONBAN_2_12 as StdTrsField with uid="SQXUTGYSHC",rtseq=19,rtrep=.t.,;
    cFormVar="w_APCONBAN",value=space(15),;
    ToolTipText = "Conto banche associato",;
    HelpContextID = 9871532,;
    cTotal="", bFixedPos=.t., cQueryName = "APCONBAN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Conto corrente obsoleto o non congruente",;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=508, Top=138, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_APCONBAN"

  func oAPCONBAN_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_BANC<>'S' OR ( .w_FLMOVC<>'S' OR .w_TIPSOT<>'B' ))
    endwith
    endif
  endfunc

  func oAPCONBAN_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oAPCONBAN_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oAPCONBAN_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oAPCONBAN_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti correnti",'',this.parent.oContained
  endproc
  proc oAPCONBAN_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_APCONBAN
    i_obj.ecpSave()
  endproc

  add object oObj_2_14 as cp_calclbl with uid="SLSVWBTAIQ",width=80,height=20,;
   left=7, top=139,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=2;
    , HelpContextID = 221126170

  add object oObj_2_15 as cp_calclbl with uid="VRPMABFPRF",width=98,height=20,;
   left=407, top=139,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=2;
    , HelpContextID = 221126170

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mapBodyRow as CPBodyRowCnt
  Width=645
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oAPTIPCON_2_2 as StdTrsField with uid="XETWQSFRKL",rtseq=9,rtrep=.t.,;
    cFormVar="w_APTIPCON",value=space(1),;
    ToolTipText = "Tipo conto riga: clienti; fornitori; generico",;
    HelpContextID = 259756204,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=33, Left=-2, Top=0, cSayPict=["!"], cGetPict=["!"], InputMask=replicate('X',1)

  func oAPTIPCON_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APTIPCON$"CFG")
      if .not. empty(.w_APCODCON)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oAPCODCON_2_3 as StdTrsField with uid="QUITIFYCON",rtseq=10,rtrep=.t.,;
    cFormVar="w_APCODCON",value=space(15),;
    ToolTipText = "Conto da riportare in automatico sulla riga di primanota",;
    HelpContextID = 3580076,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=35, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_APTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_APCODCON"

  func oAPCODCON_2_3.mCond()
    with this.Parent.oContained
      return (.w_APTIPCON$"CFG")
    endwith
  endfunc

  func oAPCODCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oAPCODCON_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oAPCODCON_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_APTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_APTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAPCODCON_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco conti",'',this.parent.oContained
  endproc
  proc oAPCODCON_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_APTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_APCODCON
    i_obj.ecpSave()
  endproc

  add object oDESCON_2_4 as StdTrsField with uid="DIGWOSOPRQ",rtseq=11,rtrep=.t.,;
    cFormVar="w_DESCON",value=space(40),enabled=.f.,;
    HelpContextID = 76655562,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=247, Left=174, Top=0, InputMask=replicate('X',40)

  add object oAPFLDAVE_2_5 as StdTrsCombo with uid="FQJWFXHGZS",rtrep=.t.,;
    cFormVar="w_APFLDAVE", RowSource=""+"Dare,"+"Avere" , ;
    ToolTipText = "D = importo sulla colonna dare; A = importo sulla colonna avere",;
    HelpContextID = 231116619,;
    Height=21, Width=56, Left=425, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oAPFLDAVE_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..APFLDAVE,&i_cF..t_APFLDAVE),this.value)
    return(iif(xVal =1,'D',;
    iif(xVal =2,'A',;
    space(1))))
  endfunc
  func oAPFLDAVE_2_5.GetRadio()
    this.Parent.oContained.w_APFLDAVE = this.RadioValue()
    return .t.
  endfunc

  func oAPFLDAVE_2_5.ToRadio()
    this.Parent.oContained.w_APFLDAVE=trim(this.Parent.oContained.w_APFLDAVE)
    return(;
      iif(this.Parent.oContained.w_APFLDAVE=='D',1,;
      iif(this.Parent.oContained.w_APFLDAVE=='A',2,;
      0)))
  endfunc

  func oAPFLDAVE_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oAPFLDAVE_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APFLDAVE$"DA")
    endwith
    return bRes
  endfunc

  add object oAPFLAUTO_2_6 as StdTrsCombo with uid="MYCRFPRUOE",rtrep=.t.,;
    cFormVar="w_APFLAUTO", RowSource=""+"+ Incrementa,"+"- Decrementa,"+"T totalizza,"+"P subtotale,"+"D subtotale+decrem.,"+"I subtotale+increm,"+"A totalizza e asseg,"+". No automatismo" , ;
    ToolTipText = "Flag automatismo associato alla riga",;
    HelpContextID = 26644309,;
    Height=21, Width=32, Left=485, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oAPFLAUTO_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..APFLAUTO,&i_cF..t_APFLAUTO),this.value)
    return(iif(xVal =1,'+',;
    iif(xVal =2,'-',;
    iif(xVal =3,'T',;
    iif(xVal =4,'P',;
    iif(xVal =5,'D',;
    iif(xVal =6,'I',;
    iif(xVal =7,'A',;
    iif(xVal =8,'N',;
    space(1))))))))))
  endfunc
  func oAPFLAUTO_2_6.GetRadio()
    this.Parent.oContained.w_APFLAUTO = this.RadioValue()
    return .t.
  endfunc

  func oAPFLAUTO_2_6.ToRadio()
    this.Parent.oContained.w_APFLAUTO=trim(this.Parent.oContained.w_APFLAUTO)
    return(;
      iif(this.Parent.oContained.w_APFLAUTO=='+',1,;
      iif(this.Parent.oContained.w_APFLAUTO=='-',2,;
      iif(this.Parent.oContained.w_APFLAUTO=='T',3,;
      iif(this.Parent.oContained.w_APFLAUTO=='P',4,;
      iif(this.Parent.oContained.w_APFLAUTO=='D',5,;
      iif(this.Parent.oContained.w_APFLAUTO=='I',6,;
      iif(this.Parent.oContained.w_APFLAUTO=='A',7,;
      iif(this.Parent.oContained.w_APFLAUTO=='N',8,;
      0)))))))))
  endfunc

  func oAPFLAUTO_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oAPFLPART_2_7 as StdTrsCombo with uid="LWPYDYHILX",rtrep=.t.,;
    cFormVar="w_APFLPART", RowSource=""+"Acconto,"+"Crea,"+"Salda,"+"No" , ;
    ToolTipText = "Riga crea, salda o acconto partita",;
    HelpContextID = 24735910,;
    Height=21, Width=56, Left=521, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oAPFLPART_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..APFLPART,&i_cF..t_APFLPART),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'C',;
    iif(xVal =3,'S',;
    iif(xVal =4,'N',;
    space(1))))))
  endfunc
  func oAPFLPART_2_7.GetRadio()
    this.Parent.oContained.w_APFLPART = this.RadioValue()
    return .t.
  endfunc

  func oAPFLPART_2_7.ToRadio()
    this.Parent.oContained.w_APFLPART=trim(this.Parent.oContained.w_APFLPART)
    return(;
      iif(this.Parent.oContained.w_APFLPART=='A',1,;
      iif(this.Parent.oContained.w_APFLPART=='C',2,;
      iif(this.Parent.oContained.w_APFLPART=='S',3,;
      iif(this.Parent.oContained.w_APFLPART=='N',4,;
      0)))))
  endfunc

  func oAPFLPART_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oAPFLPART_2_7.mCond()
    with this.Parent.oContained
      return ((.w_PARTSN="S" OR .w_APCODCON=SPACE(15)) AND .w_CAUPAR<>"N")
    endwith
  endfunc

  func oAPFLPART_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APFLPART$"CSA" OR .w_CAUPAR="N" OR .w_PARTSN<>"S")
    endwith
    return bRes
  endfunc

  add object oAPCAURIG_2_8 as StdTrsField with uid="CDLPKWWFXO",rtseq=15,rtrep=.t.,;
    cFormVar="w_APCAURIG",value=space(5),;
    ToolTipText = "Causale contabile di riga (inserire solo se diversa dalla causale principale)",;
    HelpContextID = 264986445,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente oppure obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=582, Top=0, cSayPict=['!!!!!'], cGetPict=['!!!!!'], InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", oKey_1_1="CCCODICE", oKey_1_2="this.w_APCAURIG"

  func oAPCAURIG_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oAPCAURIG_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oAPCAURIG_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oAPCAURIG_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali contabili",'',this.parent.oContained
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oAPTIPCON_2_2.When()
    return(.t.)
  proc oAPTIPCON_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oAPTIPCON_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_map','CAUPRI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".APCODCAU=CAUPRI.APCODCAU";
  +" and "+i_cAliasName2+".APTIPCLF=CAUPRI.APTIPCLF";
  +" and "+i_cAliasName2+".APCODCLF=CAUPRI.APCODCLF";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
