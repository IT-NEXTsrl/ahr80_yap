* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bsr                                                        *
*              ZOOM RISORSE                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-05                                                      *
* Last revis.: 2008-06-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bsr",oParentObject,m.pTIPOPE)
return(i_retval)

define class tgsar_bsr as StdBatch
  * --- Local variables
  pTIPOPE = space(3)
  w_DPDESCRI = space(40)
  w_DPTIPRIS = space(1)
  w_DPCODICE = space(5)
  w_CURNAME = space(10)
  w_PADRE = .NULL.
  w_RETVAL = .f.
  w_OBJCTRL = .NULL.
  w_OSOURCE = .NULL.
  w_NUMMARK = 0
  w_DPCODCOL = space(5)
  w_DPOLDCOL = space(5)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.pTIPOPE = IIF(VARTYPE(this.pTIPOPE)<>"C", "", this.pTipOpe)
    this.w_PADRE = this.oParentObject
    do case
      case EMPTY(this.pTIPOPE)
        this.w_DPTIPRIS = this.oParentObject.w_SRTIPCOL
        this.w_DPCODICE = IIF(EMPTY(this.oParentObject.w_SRCODICE), this.oParentObject.w_SRCODPAD, this.oParentObject.w_SRCODICE)
        this.w_DPOLDCOL = this.w_DPCODICE
        this.w_DPCODICE = SPACE(5)
        this.w_DPCODCOL = this.w_DPOLDCOL
        vx_exec("gsar_bsr.VZM",this)
        if !EMPTY(this.w_DPCODICE)
          * --- Assegno i valori del record selezionato
          this.oParentObject.w_SRTIPCOL = this.w_DPTIPRIS
          this.w_OBJCTRL = this.w_PADRE.GetBodyCtrl( "w_SRRISCOL" )
          if this.w_OBJCTRL.mCond()
            this.w_OSOURCE.xKey( 1 ) = this.oParentObject.w_SRTIPCOL
          endif
          this.w_OSOURCE.xKey( 2 ) = this.w_DPCODICE
          this.w_NUMMARK = this.w_PADRE.nMarkpos
          this.w_PADRE.nMarkpos = 0
          this.w_OBJCTRL.ecpDrop(this.w_OSOURCE)     
          this.w_PADRE.nMarkpos = this.w_NUMMARK
        else
          this.w_DPCODICE = this.w_DPCODCOL
        endif
      case this.pTIPOPE="CHK"
        this.w_DPDESCRI = GSAR_BTW(this, "CHK")
        if !EMPTY(this.w_DPDESCRI)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_DPDESCRI
        endif
    endcase
  endproc


  proc Init(oParentObject,pTIPOPE)
    this.pTIPOPE=pTIPOPE
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOPE"
endproc
