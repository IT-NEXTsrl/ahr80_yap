* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_asp                                                        *
*              Spedizioni                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_9]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2009-10-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_asp"))

* --- Class definition
define class tgsar_asp as StdForm
  Top    = 47
  Left   = 29

  * --- Standard Properties
  Width  = 498
  Height = 158+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-10-28"
  HelpContextID=158763159
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  MODASPED_IDX = 0
  METCALSP_IDX = 0
  cFile = "MODASPED"
  cKeySelect = "SPCODSPE"
  cKeyWhere  = "SPCODSPE=this.w_SPCODSPE"
  cKeyWhereODBC = '"SPCODSPE="+cp_ToStrODBC(this.w_SPCODSPE)';

  cKeyWhereODBCqualified = '"MODASPED.SPCODSPE="+cp_ToStrODBC(this.w_SPCODSPE)';

  cPrg = "gsar_asp"
  cComment = "Spedizioni"
  icon = "anag.ico"
  cAutoZoom = 'GSVE0ASP'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SPCODSPE = space(3)
  w_SPDESSPE = space(35)
  w_SPMODSPE = space(2)
  w_SPMCCODI = space(5)
  w_SPMCCODT = space(5)
  w_DESMCIMB = space(40)
  w_DESMCTRA = space(40)
  w_SPFLGCOM = space(1)
  w_SPFLGPRO = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MODASPED','gsar_asp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_aspPag1","gsar_asp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Spedizione")
      .Pages(1).HelpContextID = 93209995
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSPCODSPE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='METCALSP'
    this.cWorkTables[2]='MODASPED'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MODASPED_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MODASPED_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SPCODSPE = NVL(SPCODSPE,space(3))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_5_joined
    link_1_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MODASPED where SPCODSPE=KeySet.SPCODSPE
    *
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MODASPED')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MODASPED.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MODASPED '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SPCODSPE',this.w_SPCODSPE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESMCIMB = space(40)
        .w_DESMCTRA = space(40)
        .w_SPCODSPE = NVL(SPCODSPE,space(3))
        .w_SPDESSPE = NVL( cp_TransLoadField('SPDESSPE') ,space(35))
        .w_SPMODSPE = NVL(SPMODSPE,space(2))
        .w_SPMCCODI = NVL(SPMCCODI,space(5))
          if link_1_4_joined
            this.w_SPMCCODI = NVL(MSCODICE104,NVL(this.w_SPMCCODI,space(5)))
            this.w_DESMCIMB = NVL(MSDESCRI104,space(40))
          else
          .link_1_4('Load')
          endif
        .w_SPMCCODT = NVL(SPMCCODT,space(5))
          if link_1_5_joined
            this.w_SPMCCODT = NVL(MSCODICE105,NVL(this.w_SPMCCODT,space(5)))
            this.w_DESMCTRA = NVL(MSDESCRI105,space(40))
          else
          .link_1_5('Load')
          endif
        .w_SPFLGCOM = NVL(SPFLGCOM,space(1))
        .w_SPFLGPRO = NVL(SPFLGPRO,space(1))
        cp_LoadRecExtFlds(this,'MODASPED')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SPCODSPE = space(3)
      .w_SPDESSPE = space(35)
      .w_SPMODSPE = space(2)
      .w_SPMCCODI = space(5)
      .w_SPMCCODT = space(5)
      .w_DESMCIMB = space(40)
      .w_DESMCTRA = space(40)
      .w_SPFLGCOM = space(1)
      .w_SPFLGPRO = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
          if not(empty(.w_SPMCCODI))
          .link_1_4('Full')
          endif
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_SPMCCODT))
          .link_1_5('Full')
          endif
          .DoRTCalc(6,7,.f.)
        .w_SPFLGCOM = 'M'
        .w_SPFLGPRO = 'M'
      endif
    endwith
    cp_BlankRecExtFlds(this,'MODASPED')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSPCODSPE_1_1.enabled = i_bVal
      .Page1.oPag.oSPDESSPE_1_2.enabled = i_bVal
      .Page1.oPag.oSPMODSPE_1_3.enabled = i_bVal
      .Page1.oPag.oSPMCCODI_1_4.enabled = i_bVal
      .Page1.oPag.oSPMCCODT_1_5.enabled = i_bVal
      .Page1.oPag.oSPFLGCOM_1_12.enabled = i_bVal
      .Page1.oPag.oSPFLGPRO_1_13.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSPCODSPE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSPCODSPE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MODASPED',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPCODSPE,"SPCODSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPDESSPE,"SPDESSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPMODSPE,"SPMODSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPMCCODI,"SPMCCODI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPMCCODT,"SPMCCODT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPFLGCOM,"SPFLGCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SPFLGPRO,"SPFLGPRO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
    i_lTable = "MODASPED"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MODASPED_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("QUERY\GSMA_QS1.VQR,QUERY\GSMA_SAS.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MODASPED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MODASPED_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MODASPED
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MODASPED')
        i_extval=cp_InsertValODBCExtFlds(this,'MODASPED')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SPCODSPE,SPDESSPE"+cp_TransInsFldName('SPDESSPE')+",SPMODSPE,SPMCCODI,SPMCCODT"+;
                  ",SPFLGCOM,SPFLGPRO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_SPCODSPE)+;
                  ","+cp_ToStrODBC(this.w_SPDESSPE)+cp_TransInsFldValue(this.w_SPDESSPE)+;
                  ","+cp_ToStrODBC(this.w_SPMODSPE)+;
                  ","+cp_ToStrODBCNull(this.w_SPMCCODI)+;
                  ","+cp_ToStrODBCNull(this.w_SPMCCODT)+;
                  ","+cp_ToStrODBC(this.w_SPFLGCOM)+;
                  ","+cp_ToStrODBC(this.w_SPFLGPRO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MODASPED')
        i_extval=cp_InsertValVFPExtFlds(this,'MODASPED')
        cp_CheckDeletedKey(i_cTable,0,'SPCODSPE',this.w_SPCODSPE)
        INSERT INTO (i_cTable);
              (SPCODSPE,SPDESSPE,SPMODSPE,SPMCCODI,SPMCCODT,SPFLGCOM,SPFLGPRO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SPCODSPE;
                  ,this.w_SPDESSPE;
                  ,this.w_SPMODSPE;
                  ,this.w_SPMCCODI;
                  ,this.w_SPMCCODT;
                  ,this.w_SPFLGCOM;
                  ,this.w_SPFLGPRO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MODASPED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MODASPED_IDX,i_nConn)
      *
      * update MODASPED
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MODASPED')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SPDESSPE="+cp_TransUpdFldName('SPDESSPE',this.w_SPDESSPE)+;
             ",SPMODSPE="+cp_ToStrODBC(this.w_SPMODSPE)+;
             ",SPMCCODI="+cp_ToStrODBCNull(this.w_SPMCCODI)+;
             ",SPMCCODT="+cp_ToStrODBCNull(this.w_SPMCCODT)+;
             ",SPFLGCOM="+cp_ToStrODBC(this.w_SPFLGCOM)+;
             ",SPFLGPRO="+cp_ToStrODBC(this.w_SPFLGPRO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MODASPED')
        i_cWhere = cp_PKFox(i_cTable  ,'SPCODSPE',this.w_SPCODSPE  )
        UPDATE (i_cTable) SET;
              SPDESSPE=this.w_SPDESSPE;
             ,SPMODSPE=this.w_SPMODSPE;
             ,SPMCCODI=this.w_SPMCCODI;
             ,SPMCCODT=this.w_SPMCCODT;
             ,SPFLGCOM=this.w_SPFLGCOM;
             ,SPFLGPRO=this.w_SPFLGPRO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MODASPED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MODASPED_IDX,i_nConn)
      *
      * delete MODASPED
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SPCODSPE',this.w_SPCODSPE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SPMCCODI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SPMCCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_SPMCCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_SPMCCODI))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SPMCCODI)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SPMCCODI) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oSPMCCODI_1_4'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SPMCCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_SPMCCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_SPMCCODI)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SPMCCODI = NVL(_Link_.MSCODICE,space(5))
      this.w_DESMCIMB = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SPMCCODI = space(5)
      endif
      this.w_DESMCIMB = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SPMCCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.MSCODICE as MSCODICE104"+ ",link_1_4.MSDESCRI as MSDESCRI104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on MODASPED.SPMCCODI=link_1_4.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and MODASPED.SPMCCODI=link_1_4.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SPMCCODT
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SPMCCODT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_SPMCCODT)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_SPMCCODT))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SPMCCODT)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SPMCCODT) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oSPMCCODT_1_5'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SPMCCODT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_SPMCCODT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_SPMCCODT)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SPMCCODT = NVL(_Link_.MSCODICE,space(5))
      this.w_DESMCTRA = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SPMCCODT = space(5)
      endif
      this.w_DESMCTRA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SPMCCODT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.MSCODICE as MSCODICE105"+ ",link_1_5.MSDESCRI as MSDESCRI105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on MODASPED.SPMCCODT=link_1_5.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and MODASPED.SPMCCODT=link_1_5.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSPCODSPE_1_1.value==this.w_SPCODSPE)
      this.oPgFrm.Page1.oPag.oSPCODSPE_1_1.value=this.w_SPCODSPE
    endif
    if not(this.oPgFrm.Page1.oPag.oSPDESSPE_1_2.value==this.w_SPDESSPE)
      this.oPgFrm.Page1.oPag.oSPDESSPE_1_2.value=this.w_SPDESSPE
    endif
    if not(this.oPgFrm.Page1.oPag.oSPMODSPE_1_3.value==this.w_SPMODSPE)
      this.oPgFrm.Page1.oPag.oSPMODSPE_1_3.value=this.w_SPMODSPE
    endif
    if not(this.oPgFrm.Page1.oPag.oSPMCCODI_1_4.value==this.w_SPMCCODI)
      this.oPgFrm.Page1.oPag.oSPMCCODI_1_4.value=this.w_SPMCCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oSPMCCODT_1_5.value==this.w_SPMCCODT)
      this.oPgFrm.Page1.oPag.oSPMCCODT_1_5.value=this.w_SPMCCODT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMCIMB_1_10.value==this.w_DESMCIMB)
      this.oPgFrm.Page1.oPag.oDESMCIMB_1_10.value=this.w_DESMCIMB
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMCTRA_1_11.value==this.w_DESMCTRA)
      this.oPgFrm.Page1.oPag.oDESMCTRA_1_11.value=this.w_DESMCTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oSPFLGCOM_1_12.RadioValue()==this.w_SPFLGCOM)
      this.oPgFrm.Page1.oPag.oSPFLGCOM_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPFLGPRO_1_13.RadioValue()==this.w_SPFLGPRO)
      this.oPgFrm.Page1.oPag.oSPFLGPRO_1_13.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'MODASPED')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SPCODSPE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSPCODSPE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_SPCODSPE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_aspPag1 as StdContainer
  Width  = 494
  height = 158
  stdWidth  = 494
  stdheight = 158
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSPCODSPE_1_1 as StdField with uid="RQJSTMRUFK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SPCODSPE", cQueryName = "SPCODSPE",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice del mezzo di spedizione",;
    HelpContextID = 250999189,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=146, Top=7, InputMask=replicate('X',3)

  add object oSPDESSPE_1_2 as StdField with uid="RMBEGUIHPJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SPDESSPE", cQueryName = "SPDESSPE",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .t.,;
    ToolTipText = "Descrizione del mezzo di spedizione",;
    HelpContextID = 235921813,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=186, Top=7, InputMask=replicate('X',35)

  add object oSPMODSPE_1_3 as StdField with uid="LWMAHYSWBR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SPMODSPE", cQueryName = "SPMODSPE",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della tabella dei mezzi di trasporto riportata sui moduli INTRA 1/2",;
    HelpContextID = 250958229,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=146, Top=30, InputMask=replicate('X',2)

  add object oSPMCCODI_1_4 as StdField with uid="SFBCGWYHIH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SPMCCODI", cQueryName = "SPMCCODI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo spese imballo",;
    HelpContextID = 216968815,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=146, Top=53, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_SPMCCODI"

  func oSPMCCODI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSPMCCODI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSPMCCODI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oSPMCCODI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oSPMCCODI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_SPMCCODI
     i_obj.ecpSave()
  endproc

  add object oSPMCCODT_1_5 as StdField with uid="VBHFCIEJSS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SPMCCODT", cQueryName = "SPMCCODT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo spese di trasporto",;
    HelpContextID = 216968826,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=146, Top=76, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_SPMCCODT"

  func oSPMCCODT_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSPMCCODT_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSPMCCODT_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oSPMCCODT_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oSPMCCODT_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_SPMCCODT
     i_obj.ecpSave()
  endproc

  add object oDESMCIMB_1_10 as StdField with uid="TPRNVEINLT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESMCIMB", cQueryName = "DESMCIMB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 151453064,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=196, Top=53, InputMask=replicate('X',40)

  add object oDESMCTRA_1_11 as StdField with uid="DZTLRVIOWR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESMCTRA", cQueryName = "DESMCTRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 235339145,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=196, Top=76, InputMask=replicate('X',40)


  add object oSPFLGCOM_1_12 as StdCombo with uid="TQBOVDUBSU",rtseq=8,rtrep=.f.,left=146,top=104,width=232,height=21;
    , ToolTipText = "Identifica chi tra i soggetti del documento deve essere considerato come committente del trasporto";
    , HelpContextID = 248037773;
    , cFormVar="w_SPFLGCOM",RowSource=""+"Azienda mittente,"+"Azienda destinataria,"+"Azienda per conto di", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSPFLGCOM_1_12.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'D',;
    iif(this.value =3,'P',;
    space(1)))))
  endfunc
  func oSPFLGCOM_1_12.GetRadio()
    this.Parent.oContained.w_SPFLGCOM = this.RadioValue()
    return .t.
  endfunc

  func oSPFLGCOM_1_12.SetRadio()
    this.Parent.oContained.w_SPFLGCOM=trim(this.Parent.oContained.w_SPFLGCOM)
    this.value = ;
      iif(this.Parent.oContained.w_SPFLGCOM=='M',1,;
      iif(this.Parent.oContained.w_SPFLGCOM=='D',2,;
      iif(this.Parent.oContained.w_SPFLGCOM=='P',3,;
      0)))
  endfunc


  add object oSPFLGPRO_1_13 as StdCombo with uid="TVRDFZHUIN",rtseq=9,rtrep=.f.,left=146,top=127,width=232,height=21;
    , ToolTipText = "Identifica chi tra i soggetti del documento deve essere considerato come proprietario della merce oggetto del trasporto";
    , HelpContextID = 29933963;
    , cFormVar="w_SPFLGPRO",RowSource=""+"Azienda mittente,"+"Azienda destinataria,"+"Azienda per conto di", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSPFLGPRO_1_13.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'D',;
    iif(this.value =3,'P',;
    space(1)))))
  endfunc
  func oSPFLGPRO_1_13.GetRadio()
    this.Parent.oContained.w_SPFLGPRO = this.RadioValue()
    return .t.
  endfunc

  func oSPFLGPRO_1_13.SetRadio()
    this.Parent.oContained.w_SPFLGPRO=trim(this.Parent.oContained.w_SPFLGPRO)
    this.value = ;
      iif(this.Parent.oContained.w_SPFLGPRO=='M',1,;
      iif(this.Parent.oContained.w_SPFLGPRO=='D',2,;
      iif(this.Parent.oContained.w_SPFLGPRO=='P',3,;
      0)))
  endfunc

  add object oStr_1_6 as StdString with uid="WDESRYQUMG",Visible=.t., Left=26, Top=7,;
    Alignment=1, Width=118, Height=15,;
    Caption="Codice spedizione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="IUUMHMJYUB",Visible=.t., Left=26, Top=30,;
    Alignment=1, Width=118, Height=15,;
    Caption="Codice INTRA 1/2:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="AVLOTAFZOF",Visible=.t., Left=4, Top=53,;
    Alignment=1, Width=140, Height=18,;
    Caption="M. c. spese imballo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XLLGNWHQWQ",Visible=.t., Left=5, Top=76,;
    Alignment=1, Width=139, Height=18,;
    Caption="M. c. spese trasporto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="BVEOOPHOUK",Visible=.t., Left=5, Top=104,;
    Alignment=1, Width=139, Height=18,;
    Caption="Committente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="NGIKJHLKPF",Visible=.t., Left=5, Top=127,;
    Alignment=1, Width=139, Height=18,;
    Caption="Proprietario:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_asp','MODASPED','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SPCODSPE=MODASPED.SPCODSPE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
