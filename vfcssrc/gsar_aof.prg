* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_aof                                                        *
*              Suite Office                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-05                                                      *
* Last revis.: 2007-07-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_aof")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_aof")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_aof")
  return

* --- Class definition
define class tgsar_aof as StdPCForm
  Width  = 391
  Height = 115
  Top    = 10
  Left   = 10
  cComment = "Suite Office"
  cPrg = "gsar_aof"
  HelpContextID=176781161
  add object cnt as tcgsar_aof
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_aof as PCContext
  w_AZCODAZI = space(5)
  w_AZOFFICE = space(1)
  w_PRINTMERGE = space(1)
  proc Save(oFrom)
    this.w_AZCODAZI = oFrom.w_AZCODAZI
    this.w_AZOFFICE = oFrom.w_AZOFFICE
    this.w_PRINTMERGE = oFrom.w_PRINTMERGE
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_AZCODAZI = this.w_AZCODAZI
    oTo.w_AZOFFICE = this.w_AZOFFICE
    oTo.w_PRINTMERGE = this.w_PRINTMERGE
    PCContext::Load(oTo)
enddefine

define class tcgsar_aof as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 391
  Height = 115
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-19"
  HelpContextID=176781161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  AZIENDA_IDX = 0
  cFile = "AZIENDA"
  cKeySelect = "AZCODAZI"
  cKeyWhere  = "AZCODAZI=this.w_AZCODAZI"
  cKeyWhereODBC = '"AZCODAZI="+cp_ToStrODBC(this.w_AZCODAZI)';

  cKeyWhereODBCqualified = '"AZIENDA.AZCODAZI="+cp_ToStrODBC(this.w_AZCODAZI)';

  cPrg = "gsar_aof"
  cComment = "Suite Office"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZCODAZI = space(5)
  o_AZCODAZI = space(5)
  w_AZOFFICE = space(1)
  w_PRINTMERGE = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsar_aof
  * --- Disabilita il Caricamento e la Cancellazione sulla Toolbar
  proc SetCPToolBar()
          doDefault()
          oCpToolBar.b3.enabled=.f.
          oCpToolBar.b5.enabled=.f.
  endproc
  * ---- Disattiva i metodi Load e Delete (posso solo variare)
  proc ecpLoad()
      * ----
  endproc
  proc ecpDelete()
      * ----
  endproc
  proc ecpEdit()
        * ----
        DoDefault()
        this.oPgFrm.Pages(2).caption=''
        this.oPgFrm.Pages(2).enabled=.f.
  endproc
  proc ecpQuery()
        * ----
        DoDefault()
        this.oPgFrm.Pages(2).caption=''
        this.oPgFrm.Pages(2).enabled=.f.
  endproc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_aofPag1","gsar_aof",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 118333706
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAZOFFICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='AZIENDA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AZIENDA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AZIENDA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_aof'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- gsar_aof
    this.w_azcodazi=i_codazi
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from AZIENDA where AZCODAZI=KeySet.AZCODAZI
    *
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AZIENDA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AZIENDA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AZIENDA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PRINTMERGE = IIF( TYPE('g_PRINTMERGE')='C',g_PRINTMERGE, '2')
        .w_AZCODAZI = NVL(AZCODAZI,space(5))
        .w_AZOFFICE = NVL(AZOFFICE,space(1))
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        cp_LoadRecExtFlds(this,'AZIENDA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_aof
    g_OFFICE=this.w_AZOFFICE
    if this.w_AZCODAZI<>i_codazi and not empty(this.w_AZCODAZI)
     this.blankrec()
     this.w_AZCODAZI=""
     AH_ERRORMSG("Manutenzione consentita alla sola azienda corrente (%1)",48,'',i_CODAZI)
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_AZCODAZI = space(5)
      .w_AZOFFICE = space(1)
      .w_PRINTMERGE = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_AZOFFICE = 'M'
        .w_PRINTMERGE = IIF( TYPE('g_PRINTMERGE')='C',g_PRINTMERGE, '2')
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'AZIENDA')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oAZOFFICE_1_2.enabled = i_bVal
      .Page1.oPag.oPRINTMERGE_1_3.enabled_(i_bVal)
      .Page1.oPag.oBtn_1_7.enabled = .Page1.oPag.oBtn_1_7.mCond()
      .Page1.oPag.oBtn_1_8.enabled = .Page1.oPag.oBtn_1_8.mCond()
      .Page1.oPag.oObj_1_6.enabled = i_bVal
      .Page1.oPag.oObj_1_9.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'AZIENDA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODAZI,"AZCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZOFFICE,"AZOFFICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AZIENDA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.AZIENDA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into AZIENDA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AZIENDA')
        i_extval=cp_InsertValODBCExtFlds(this,'AZIENDA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AZCODAZI,AZOFFICE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AZCODAZI)+;
                  ","+cp_ToStrODBC(this.w_AZOFFICE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AZIENDA')
        i_extval=cp_InsertValVFPExtFlds(this,'AZIENDA')
        cp_CheckDeletedKey(i_cTable,0,'AZCODAZI',this.w_AZCODAZI)
        INSERT INTO (i_cTable);
              (AZCODAZI,AZOFFICE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AZCODAZI;
                  ,this.w_AZOFFICE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.AZIENDA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.AZIENDA_IDX,i_nConn)
      *
      * update AZIENDA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'AZIENDA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " AZOFFICE="+cp_ToStrODBC(this.w_AZOFFICE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'AZIENDA')
        i_cWhere = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
        UPDATE (i_cTable) SET;
              AZOFFICE=this.w_AZOFFICE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsar_aof
    g_OFFICE=this.w_AZOFFICE
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AZIENDA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.AZIENDA_IDX,i_nConn)
      *
      * delete AZIENDA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPRINTMERGE_1_3.enabled_(this.oPgFrm.Page1.oPag.oPRINTMERGE_1_3.mCond())
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPRINTMERGE_1_3.visible=!this.oPgFrm.Page1.oPag.oPRINTMERGE_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAZOFFICE_1_2.RadioValue()==this.w_AZOFFICE)
      this.oPgFrm.Page1.oPag.oAZOFFICE_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRINTMERGE_1_3.RadioValue()==this.w_PRINTMERGE)
      this.oPgFrm.Page1.oPag.oPRINTMERGE_1_3.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'AZIENDA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsar_aof
      .NotifyEvent('Salva')
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AZCODAZI = this.w_AZCODAZI
    return

enddefine

* --- Define pages as container
define class tgsar_aofPag1 as StdContainer
  Width  = 387
  height = 115
  stdWidth  = 387
  stdheight = 115
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oAZOFFICE_1_2 as StdCombo with uid="IBRFTCJPES",value=3,rtseq=2,rtrep=.f.,left=99,top=11,width=280,height=21;
    , HelpContextID = 52549451;
    , cFormVar="w_AZOFFICE",RowSource=""+"Microsoft Office,"+"OpenOffice/StarOffice,"+"Non definita", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAZOFFICE_1_2.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'O',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oAZOFFICE_1_2.GetRadio()
    this.Parent.oContained.w_AZOFFICE = this.RadioValue()
    return .t.
  endfunc

  func oAZOFFICE_1_2.SetRadio()
    this.Parent.oContained.w_AZOFFICE=trim(this.Parent.oContained.w_AZOFFICE)
    this.value = ;
      iif(this.Parent.oContained.w_AZOFFICE=='M',1,;
      iif(this.Parent.oContained.w_AZOFFICE=='O',2,;
      iif(this.Parent.oContained.w_AZOFFICE=='',3,;
      0)))
  endfunc

  proc oAZOFFICE_1_2.mBefore
    with this.Parent.oContained
      this.parent.oContained.bUpdated=.T.
    endwith
  endproc

  add object oPRINTMERGE_1_3 as StdRadio with uid="RXBBDWMQJK",rtseq=3,rtrep=.f.,left=99, top=41, width=144,height=23;
    , ToolTipText = "Permette di inviare direttamente alla stampante i file di OpenOffice generati con il Mailmerge";
    , cFormVar="w_PRINTMERGE", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPRINTMERGE_1_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="File"
      this.Buttons(1).HelpContextID = 134855096
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("File","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Stampante"
      this.Buttons(2).HelpContextID = 134855096
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Stampante","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Permette di inviare direttamente alla stampante i file di OpenOffice generati con il Mailmerge")
      StdRadio::init()
    endproc

  func oPRINTMERGE_1_3.RadioValue()
    return(iif(this.value =1,'2',;
    iif(this.value =2,'1',;
    space(1))))
  endfunc
  func oPRINTMERGE_1_3.GetRadio()
    this.Parent.oContained.w_PRINTMERGE = this.RadioValue()
    return .t.
  endfunc

  func oPRINTMERGE_1_3.SetRadio()
    this.Parent.oContained.w_PRINTMERGE=trim(this.Parent.oContained.w_PRINTMERGE)
    this.value = ;
      iif(this.Parent.oContained.w_PRINTMERGE=='2',1,;
      iif(this.Parent.oContained.w_PRINTMERGE=='1',2,;
      0))
  endfunc

  func oPRINTMERGE_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZOFFICE='O')
    endwith
   endif
  endfunc

  func oPRINTMERGE_1_3.mHide()
    with this.Parent.oContained
      return (.w_AZOFFICE<>'O')
    endwith
  endfunc


  add object oObj_1_5 as cp_runprogram with uid="QNFMWJINQX",left=256, top=128, width=153,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BAM("modifica")',;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 1216486


  add object oObj_1_6 as cp_runprogram with uid="VRVKXIGHFQ",left=256, top=155, width=153,height=24,;
    caption='GSUT_BLD(S)',;
   bGlobalFont=.t.,;
    prg="GSUT_BLD('S',IIF(type('g_CODPEN')<>'U',g_CODPEN,' '),IIF(type('g_CODCAS')<>'U',g_CODCAS,' '),IIF(type('g_CODNEG')<>'U',g_CODNEG,''),w_AZOFFICE, w_PRINTMERGE,iif(type('g_PRINTERARCHI')<>'U',g_PRINTERARCHI,' ') )",;
    cEvent = "Salva",;
    nPag=1;
    , HelpContextID = 37546454


  add object oBtn_1_7 as StdButton with uid="RPHQSXWHBW",left=270, top=62, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 176760602;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="WMKMIZTHAU",left=323, top=61, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 176760602;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_9 as cp_runprogram with uid="FZAYKELAJD",left=256, top=182, width=153,height=24,;
    caption='GSUT_BLD(L)',;
   bGlobalFont=.t.,;
    prg="GSUT_BLD('L',' ',' ',' ',' ',' ')",;
    cEvent = "Inizio",;
    nPag=1;
    , ToolTipText = "Legge file di configurazione legato alla macchina";
    , HelpContextID = 37548246

  add object oStr_1_4 as StdString with uid="YOICNZHBVP",Visible=.t., Left=25, Top=14,;
    Alignment=1, Width=71, Height=18,;
    Caption="Suite Office:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="FRUQICREFR",Visible=.t., Left=-1, Top=41,;
    Alignment=1, Width=97, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_AZOFFICE<>'O')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_aof','AZIENDA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AZCODAZI=AZIENDA.AZCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
