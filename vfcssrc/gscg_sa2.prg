* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sa2                                                        *
*              Riepilogo acconto IVA                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_75]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-27                                                      *
* Last revis.: 2008-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sa2",oParentObject))

* --- Class definition
define class tgscg_sa2 as StdForm
  Top    = 12
  Left   = 13

  * --- Standard Properties
  Width  = 674
  Height = 472
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-10"
  HelpContextID=124396905
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=36

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_sa2"
  cComment = "Riepilogo acconto IVA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_VPIMPO7A = 0
  w_VPIMPO7B = 0
  w_VPIMPO7C = 0
  w_IMPDEB8 = 0
  w_IMPCRE8 = 0
  w_VIMPOR8 = 0
  w_IMPDEB9 = 0
  w_IMPCRE9 = 0
  w_VIMPOR9 = 0
  w_IMPDEB10 = 0
  w_IMPCRE10 = 0
  w_VIMPO10 = 0
  w_VIMPO11 = 0
  w_VIMPO12 = 0
  w_VIMPO13 = 0
  w_NUMPER = 0
  w_VIMPO16 = 0
  o_VIMPO16 = 0
  w_PEAIVA1 = 0
  w_VIMPO15 = 0
  w_ACCSTO = 0
  w_CREPRI = 0
  w_CREDOC = 0
  w_DEBPRI = 0
  w_DEBDOC = 0
  w_DEBEFF = 0
  w_CRERES = 0
  w_MACIVA1 = 0
  w_SIMVA1 = space(5)
  w_METOD = space(1)
  o_METOD = space(1)
  w_ACCONTO = 0
  w_F2DEFI = space(1)
  w_VDICSOC = space(1)
  w_MTIPLIQ = space(1)
  w_TIPSTA = space(1)
  w_ANNO = ctod('  /  /  ')
  w_OK = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sa2Pag1","gscg_sa2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIMPDEB8_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VPIMPO7A=0
      .w_VPIMPO7B=0
      .w_VPIMPO7C=0
      .w_IMPDEB8=0
      .w_IMPCRE8=0
      .w_VIMPOR8=0
      .w_IMPDEB9=0
      .w_IMPCRE9=0
      .w_VIMPOR9=0
      .w_IMPDEB10=0
      .w_IMPCRE10=0
      .w_VIMPO10=0
      .w_VIMPO11=0
      .w_VIMPO12=0
      .w_VIMPO13=0
      .w_NUMPER=0
      .w_VIMPO16=0
      .w_PEAIVA1=0
      .w_VIMPO15=0
      .w_ACCSTO=0
      .w_CREPRI=0
      .w_CREDOC=0
      .w_DEBPRI=0
      .w_DEBDOC=0
      .w_DEBEFF=0
      .w_CRERES=0
      .w_MACIVA1=0
      .w_SIMVA1=space(5)
      .w_METOD=space(1)
      .w_ACCONTO=0
      .w_F2DEFI=space(1)
      .w_VDICSOC=space(1)
      .w_MTIPLIQ=space(1)
      .w_TIPSTA=space(1)
      .w_ANNO=ctod("  /  /  ")
      .w_OK=.f.
      .w_VPIMPO7A=oParentObject.w_VPIMPO7A
      .w_VPIMPO7B=oParentObject.w_VPIMPO7B
      .w_VPIMPO7C=oParentObject.w_VPIMPO7C
      .w_VIMPOR8=oParentObject.w_VIMPOR8
      .w_VIMPOR9=oParentObject.w_VIMPOR9
      .w_VIMPO10=oParentObject.w_VIMPO10
      .w_VIMPO11=oParentObject.w_VIMPO11
      .w_VIMPO12=oParentObject.w_VIMPO12
      .w_VIMPO13=oParentObject.w_VIMPO13
      .w_NUMPER=oParentObject.w_NUMPER
      .w_VIMPO16=oParentObject.w_VIMPO16
      .w_PEAIVA1=oParentObject.w_PEAIVA1
      .w_VIMPO15=oParentObject.w_VIMPO15
      .w_ACCSTO=oParentObject.w_ACCSTO
      .w_CRERES=oParentObject.w_CRERES
      .w_MACIVA1=oParentObject.w_MACIVA1
      .w_SIMVA1=oParentObject.w_SIMVA1
      .w_METOD=oParentObject.w_METOD
      .w_ACCONTO=oParentObject.w_ACCONTO
      .w_F2DEFI=oParentObject.w_F2DEFI
      .w_VDICSOC=oParentObject.w_VDICSOC
      .w_MTIPLIQ=oParentObject.w_MTIPLIQ
      .w_ANNO=oParentObject.w_ANNO
      .w_OK=oParentObject.w_OK
          .DoRTCalc(1,5,.f.)
        .w_VIMPOR8 = .w_IMPDEB8-.w_IMPCRE8
          .DoRTCalc(7,8,.f.)
        .w_VIMPOR9 = .w_IMPDEB9-.w_IMPCRE9
        .w_IMPDEB10 = IIF( .w_VIMPO10>0,  .w_VIMPO10 ,0)
        .w_IMPCRE10 = IIF( .w_VIMPO10<0 ,  ABS(.w_VIMPO10) ,0)
        .w_VIMPO10 = .w_IMPDEB10-.w_IMPCRE10
          .DoRTCalc(13,13,.f.)
        .w_VIMPO12 = (.w_VPIMPO7A+.w_VPIMPO7B+.w_VPIMPO7C+.w_VIMPOR8+.w_VIMPOR9+.w_VIMPO10)-IIF(.w_VIMPO11<=.w_CRERES And .w_VIMPO11>=0,.w_VIMPO11,0)
          .DoRTCalc(15,16,.f.)
        .w_VIMPO16 = IIF(((.w_VIMPO12 ) - (.w_VIMPO13))<0, 0, (.w_VIMPO12) - (.w_VIMPO13))
          .DoRTCalc(18,18,.f.)
        .w_VIMPO15 = .w_VIMPO16
          .DoRTCalc(20,20,.f.)
        .w_CREPRI = IIF( .w_VPIMPO7A<0 ,  ABS(.w_VPIMPO7A) ,0 )
        .w_CREDOC = IIF( .w_VPIMPO7B<0 ,  ABS(.w_VPIMPO7B) ,0 )
        .w_DEBPRI = IIF( .w_VPIMPO7A>0 ,  .w_VPIMPO7A, 0)
        .w_DEBDOC = IIF( .w_VPIMPO7B>0 ,  .w_VPIMPO7B, 0)
        .w_DEBEFF = .w_VPIMPO7C
          .DoRTCalc(26,29,.f.)
        .w_ACCONTO = IIF(.w_METOD='O',.w_VIMPO16,.w_ACCSTO)
      .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
          .DoRTCalc(31,33,.f.)
        .w_TIPSTA = IIF(.w_METOD='O','O','S')
      .oPgFrm.Page1.oPag.oObj_1_62.Calculate(IIF(g_TIPDEN='M','01/12/','01/10/')+ALLTRIM(.w_ANNO)+AH_Msgformat(' al')+' 20/12/'+ALLTRIM(.w_ANNO))
      .oPgFrm.Page1.oPag.oObj_1_63.Calculate(IIF(g_TIPDEN='M','01/12/','01/10/')+ALLTRIM(.w_ANNO)+AH_Msgformat(' al')+' 20/12/'+ALLTRIM(.w_ANNO))
      .oPgFrm.Page1.oPag.oObj_1_64.Calculate('01/11/'+ALLTRIM(.w_ANNO)+AH_Msgformat(' al')+' 20/12/'+ALLTRIM(.w_ANNO))
          .DoRTCalc(35,35,.f.)
        .w_OK = .T.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_VPIMPO7A=.w_VPIMPO7A
      .oParentObject.w_VPIMPO7B=.w_VPIMPO7B
      .oParentObject.w_VPIMPO7C=.w_VPIMPO7C
      .oParentObject.w_VIMPOR8=.w_VIMPOR8
      .oParentObject.w_VIMPOR9=.w_VIMPOR9
      .oParentObject.w_VIMPO10=.w_VIMPO10
      .oParentObject.w_VIMPO11=.w_VIMPO11
      .oParentObject.w_VIMPO12=.w_VIMPO12
      .oParentObject.w_VIMPO13=.w_VIMPO13
      .oParentObject.w_NUMPER=.w_NUMPER
      .oParentObject.w_VIMPO16=.w_VIMPO16
      .oParentObject.w_PEAIVA1=.w_PEAIVA1
      .oParentObject.w_VIMPO15=.w_VIMPO15
      .oParentObject.w_ACCSTO=.w_ACCSTO
      .oParentObject.w_CRERES=.w_CRERES
      .oParentObject.w_MACIVA1=.w_MACIVA1
      .oParentObject.w_SIMVA1=.w_SIMVA1
      .oParentObject.w_METOD=.w_METOD
      .oParentObject.w_ACCONTO=.w_ACCONTO
      .oParentObject.w_F2DEFI=.w_F2DEFI
      .oParentObject.w_VDICSOC=.w_VDICSOC
      .oParentObject.w_MTIPLIQ=.w_MTIPLIQ
      .oParentObject.w_ANNO=.w_ANNO
      .oParentObject.w_OK=.w_OK
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
            .w_VIMPOR8 = .w_IMPDEB8-.w_IMPCRE8
        .DoRTCalc(7,8,.t.)
            .w_VIMPOR9 = .w_IMPDEB9-.w_IMPCRE9
        .DoRTCalc(10,11,.t.)
            .w_VIMPO10 = .w_IMPDEB10-.w_IMPCRE10
        .DoRTCalc(13,13,.t.)
            .w_VIMPO12 = (.w_VPIMPO7A+.w_VPIMPO7B+.w_VPIMPO7C+.w_VIMPOR8+.w_VIMPOR9+.w_VIMPO10)-IIF(.w_VIMPO11<=.w_CRERES And .w_VIMPO11>=0,.w_VIMPO11,0)
        .DoRTCalc(15,16,.t.)
            .w_VIMPO16 = IIF(((.w_VIMPO12 ) - (.w_VIMPO13))<0, 0, (.w_VIMPO12) - (.w_VIMPO13))
        .DoRTCalc(18,18,.t.)
        if .o_VIMPO16<>.w_VIMPO16
            .w_VIMPO15 = .w_VIMPO16
        endif
        .DoRTCalc(20,20,.t.)
            .w_CREPRI = IIF( .w_VPIMPO7A<0 ,  ABS(.w_VPIMPO7A) ,0 )
            .w_CREDOC = IIF( .w_VPIMPO7B<0 ,  ABS(.w_VPIMPO7B) ,0 )
            .w_DEBPRI = IIF( .w_VPIMPO7A>0 ,  .w_VPIMPO7A, 0)
            .w_DEBDOC = IIF( .w_VPIMPO7B>0 ,  .w_VPIMPO7B, 0)
            .w_DEBEFF = .w_VPIMPO7C
        .DoRTCalc(26,29,.t.)
        if .o_METOD<>.w_METOD.or. .o_VIMPO16<>.w_VIMPO16
            .w_ACCONTO = IIF(.w_METOD='O',.w_VIMPO16,.w_ACCSTO)
        endif
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .DoRTCalc(31,33,.t.)
        if .o_METOD<>.w_METOD
            .w_TIPSTA = IIF(.w_METOD='O','O','S')
        endif
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate(IIF(g_TIPDEN='M','01/12/','01/10/')+ALLTRIM(.w_ANNO)+AH_Msgformat(' al')+' 20/12/'+ALLTRIM(.w_ANNO))
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(IIF(g_TIPDEN='M','01/12/','01/10/')+ALLTRIM(.w_ANNO)+AH_Msgformat(' al')+' 20/12/'+ALLTRIM(.w_ANNO))
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate('01/11/'+ALLTRIM(.w_ANNO)+AH_Msgformat(' al')+' 20/12/'+ALLTRIM(.w_ANNO))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(35,36,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate(IIF(g_TIPDEN='M','01/12/','01/10/')+ALLTRIM(.w_ANNO)+AH_Msgformat(' al')+' 20/12/'+ALLTRIM(.w_ANNO))
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(IIF(g_TIPDEN='M','01/12/','01/10/')+ALLTRIM(.w_ANNO)+AH_Msgformat(' al')+' 20/12/'+ALLTRIM(.w_ANNO))
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate('01/11/'+ALLTRIM(.w_ANNO)+AH_Msgformat(' al')+' 20/12/'+ALLTRIM(.w_ANNO))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oIMPDEB8_1_4.enabled = this.oPgFrm.Page1.oPag.oIMPDEB8_1_4.mCond()
    this.oPgFrm.Page1.oPag.oIMPCRE8_1_5.enabled = this.oPgFrm.Page1.oPag.oIMPCRE8_1_5.mCond()
    this.oPgFrm.Page1.oPag.oIMPDEB9_1_7.enabled = this.oPgFrm.Page1.oPag.oIMPDEB9_1_7.mCond()
    this.oPgFrm.Page1.oPag.oIMPCRE9_1_8.enabled = this.oPgFrm.Page1.oPag.oIMPCRE9_1_8.mCond()
    this.oPgFrm.Page1.oPag.oIMPDEB10_1_10.enabled = this.oPgFrm.Page1.oPag.oIMPDEB10_1_10.mCond()
    this.oPgFrm.Page1.oPag.oIMPCRE10_1_11.enabled = this.oPgFrm.Page1.oPag.oIMPCRE10_1_11.mCond()
    this.oPgFrm.Page1.oPag.oVIMPO13_1_15.enabled = this.oPgFrm.Page1.oPag.oVIMPO13_1_15.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCREPRI_1_30.visible=!this.oPgFrm.Page1.oPag.oCREPRI_1_30.mHide()
    this.oPgFrm.Page1.oPag.oCREDOC_1_31.visible=!this.oPgFrm.Page1.oPag.oCREDOC_1_31.mHide()
    this.oPgFrm.Page1.oPag.oDEBPRI_1_32.visible=!this.oPgFrm.Page1.oPag.oDEBPRI_1_32.mHide()
    this.oPgFrm.Page1.oPag.oDEBDOC_1_33.visible=!this.oPgFrm.Page1.oPag.oDEBDOC_1_33.mHide()
    this.oPgFrm.Page1.oPag.oDEBEFF_1_34.visible=!this.oPgFrm.Page1.oPag.oDEBEFF_1_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIMPDEB8_1_4.value==this.w_IMPDEB8)
      this.oPgFrm.Page1.oPag.oIMPDEB8_1_4.value=this.w_IMPDEB8
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE8_1_5.value==this.w_IMPCRE8)
      this.oPgFrm.Page1.oPag.oIMPCRE8_1_5.value=this.w_IMPCRE8
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDEB9_1_7.value==this.w_IMPDEB9)
      this.oPgFrm.Page1.oPag.oIMPDEB9_1_7.value=this.w_IMPDEB9
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE9_1_8.value==this.w_IMPCRE9)
      this.oPgFrm.Page1.oPag.oIMPCRE9_1_8.value=this.w_IMPCRE9
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDEB10_1_10.value==this.w_IMPDEB10)
      this.oPgFrm.Page1.oPag.oIMPDEB10_1_10.value=this.w_IMPDEB10
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE10_1_11.value==this.w_IMPCRE10)
      this.oPgFrm.Page1.oPag.oIMPCRE10_1_11.value=this.w_IMPCRE10
    endif
    if not(this.oPgFrm.Page1.oPag.oVIMPO11_1_13.value==this.w_VIMPO11)
      this.oPgFrm.Page1.oPag.oVIMPO11_1_13.value=this.w_VIMPO11
    endif
    if not(this.oPgFrm.Page1.oPag.oVIMPO13_1_15.value==this.w_VIMPO13)
      this.oPgFrm.Page1.oPag.oVIMPO13_1_15.value=this.w_VIMPO13
    endif
    if not(this.oPgFrm.Page1.oPag.oPEAIVA1_1_18.value==this.w_PEAIVA1)
      this.oPgFrm.Page1.oPag.oPEAIVA1_1_18.value=this.w_PEAIVA1
    endif
    if not(this.oPgFrm.Page1.oPag.oVIMPO15_1_19.value==this.w_VIMPO15)
      this.oPgFrm.Page1.oPag.oVIMPO15_1_19.value=this.w_VIMPO15
    endif
    if not(this.oPgFrm.Page1.oPag.oACCSTO_1_21.value==this.w_ACCSTO)
      this.oPgFrm.Page1.oPag.oACCSTO_1_21.value=this.w_ACCSTO
    endif
    if not(this.oPgFrm.Page1.oPag.oCREPRI_1_30.value==this.w_CREPRI)
      this.oPgFrm.Page1.oPag.oCREPRI_1_30.value=this.w_CREPRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCREDOC_1_31.value==this.w_CREDOC)
      this.oPgFrm.Page1.oPag.oCREDOC_1_31.value=this.w_CREDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDEBPRI_1_32.value==this.w_DEBPRI)
      this.oPgFrm.Page1.oPag.oDEBPRI_1_32.value=this.w_DEBPRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDEBDOC_1_33.value==this.w_DEBDOC)
      this.oPgFrm.Page1.oPag.oDEBDOC_1_33.value=this.w_DEBDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDEBEFF_1_34.value==this.w_DEBEFF)
      this.oPgFrm.Page1.oPag.oDEBEFF_1_34.value=this.w_DEBEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oCRERES_1_39.value==this.w_CRERES)
      this.oPgFrm.Page1.oPag.oCRERES_1_39.value=this.w_CRERES
    endif
    if not(this.oPgFrm.Page1.oPag.oMACIVA1_1_46.value==this.w_MACIVA1)
      this.oPgFrm.Page1.oPag.oMACIVA1_1_46.value=this.w_MACIVA1
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVA1_1_49.value==this.w_SIMVA1)
      this.oPgFrm.Page1.oPag.oSIMVA1_1_49.value=this.w_SIMVA1
    endif
    if not(this.oPgFrm.Page1.oPag.oMETOD_1_50.RadioValue()==this.w_METOD)
      this.oPgFrm.Page1.oPag.oMETOD_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oACCONTO_1_54.value==this.w_ACCONTO)
      this.oPgFrm.Page1.oPag.oACCONTO_1_54.value=this.w_ACCONTO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_IMPDEB8>=0)  and (.w_IMPCRE8=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPDEB8_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPCRE8>=0)  and (.w_IMPDEB8=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPCRE8_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPDEB9>=0)  and (.w_IMPCRE9=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPDEB9_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPCRE9>=0)  and (.w_IMPDEB9=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPCRE9_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPDEB10>=0)  and (.w_F2DEFI<>'S' AND .w_IMPCRE10=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPDEB10_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPCRE10>=0)  and (.w_F2DEFI<>'S' AND .w_IMPDEB10=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPCRE10_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VIMPO11<=.w_CRERES And .w_VIMPO11>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVIMPO11_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il credito deve essere minore del credito disponibile e maggiore di zero")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_sa2
      * --- Esegue la Stampa
      if i_bRes = .t.
         this.NotifyEvent('Conferma')
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VIMPO16 = this.w_VIMPO16
    this.o_METOD = this.w_METOD
    return

enddefine

* --- Define pages as container
define class tgscg_sa2Pag1 as StdContainer
  Width  = 670
  height = 472
  stdWidth  = 670
  stdheight = 472
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIMPDEB8_1_4 as StdField with uid="CSFNYRMAWZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_IMPDEB8", cQueryName = "IMPDEB8",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Variazioni d'imposta non comprese nell'IVA esigibile/detraibile sopra indicata",;
    HelpContextID = 254749830,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=359, Top=173, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPDEB8_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPCRE8=0)
    endwith
   endif
  endfunc

  func oIMPDEB8_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPDEB8>=0)
    endwith
    return bRes
  endfunc

  add object oIMPCRE8_1_5 as StdField with uid="EHOLMASDFC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_IMPCRE8", cQueryName = "IMPCRE8",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Variazioni d'imposta non comprese nei righi VP10 e VP11",;
    HelpContextID = 50211974,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=510, Top=173, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPCRE8_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB8=0)
    endwith
   endif
  endfunc

  func oIMPCRE8_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPCRE8>=0)
    endwith
    return bRes
  endfunc

  add object oIMPDEB9_1_7 as StdField with uid="HWBRXQYOHA",rtseq=7,rtrep=.f.,;
    cFormVar = "w_IMPDEB9", cQueryName = "IMPDEB9",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA non versata gi� indicata nell'importo da versare di una dichiarazione precedente",;
    HelpContextID = 254749830,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=359, Top=195, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPDEB9_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPCRE9=0)
    endwith
   endif
  endfunc

  func oIMPDEB9_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPDEB9>=0)
    endwith
    return bRes
  endfunc

  add object oIMPCRE9_1_8 as StdField with uid="WNWUBHSMJK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_IMPCRE9", cQueryName = "IMPCRE9",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA versata in eccesso gi� indicata nell'importo da versare di una dichiarazione precedente",;
    HelpContextID = 50211974,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=510, Top=195, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPCRE9_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB9=0)
    endwith
   endif
  endfunc

  func oIMPCRE9_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPCRE9>=0)
    endwith
    return bRes
  endfunc

  add object oIMPDEB10_1_10 as StdField with uid="KRAZPMOFLH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_IMPDEB10", cQueryName = "IMPDEB10",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Debito riportato dal periodo precedente",;
    HelpContextID = 13685578,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=359, Top=217, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPDEB10_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_F2DEFI<>'S' AND .w_IMPCRE10=0)
    endwith
   endif
  endfunc

  func oIMPDEB10_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPDEB10>=0)
    endwith
    return bRes
  endfunc

  add object oIMPCRE10_1_11 as StdField with uid="KTOBCQHFAQ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_IMPCRE10", cQueryName = "IMPCRE10",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Credito riportato dal periodo precedente",;
    HelpContextID = 218223434,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=510, Top=217, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPCRE10_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_F2DEFI<>'S' AND .w_IMPDEB10=0)
    endwith
   endif
  endfunc

  func oIMPCRE10_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPCRE10>=0)
    endwith
    return bRes
  endfunc

  add object oVIMPO11_1_13 as StdField with uid="GRJCBCYOES",rtseq=13,rtrep=.f.,;
    cFormVar = "w_VIMPO11", cQueryName = "VIMPO11",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il credito deve essere minore del credito disponibile e maggiore di zero",;
    ToolTipText = "Credito IVA dell'anno precedente compensato nel periodo",;
    HelpContextID = 19203754,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=510, Top=239, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  proc oVIMPO11_1_13.mBefore
      with this.Parent.oContained
        GSCG_BA2(this.Parent.oContained,"_BEFOREVP11")
      endwith
  endproc

  proc oVIMPO11_1_13.mAfter
      with this.Parent.oContained
        GSCG_BA2(this.Parent.oContained,"_AFTERVP11")
      endwith
  endproc

  func oVIMPO11_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VIMPO11<=.w_CRERES And .w_VIMPO11>=0)
    endwith
    return bRes
  endfunc

  add object oVIMPO13_1_15 as StdField with uid="ALXFZKRLSF",rtseq=15,rtrep=.f.,;
    cFormVar = "w_VIMPO13", cQueryName = "VIMPO13",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il credito non pu� superare il debito per il periodo",;
    ToolTipText = "Ammontare dei particolari crediti di imposta utilizzati nel periodo",;
    HelpContextID = 19203754,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=510, Top=261, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVIMPO13_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not (g_TIPDEN='T' And .w_NUMPER=4) And .w_VDICSOC<>'S')
    endwith
   endif
  endfunc

  add object oPEAIVA1_1_18 as StdField with uid="SEORQSEEDE",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PEAIVA1", cQueryName = "PEAIVA1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di legge",;
    HelpContextID = 12372746,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=597, Top=35, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oVIMPO15_1_19 as StdField with uid="ONJGXRIWMI",rtseq=19,rtrep=.f.,;
    cFormVar = "w_VIMPO15", cQueryName = "VIMPO15",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Acconto da versare calcolato con metodo operazioni effettuate",;
    HelpContextID = 249231702,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=149, Left=359, Top=283, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oACCSTO_1_21 as StdField with uid="IFLSHYHORC",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ACCSTO", cQueryName = "ACCSTO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Acconto IVA calcolato con metodo storico",;
    HelpContextID = 47361530,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=149, Left=108, Top=34, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oCREPRI_1_30 as StdField with uid="YUQTPHCCRE",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CREPRI", cQueryName = "CREPRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA a credito su operazioni registrate",;
    HelpContextID = 150306522,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=510, Top=107, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oCREPRI_1_30.mHide()
    with this.Parent.oContained
      return (.w_VPIMPO7A>=0)
    endwith
  endfunc

  add object oCREDOC_1_31 as StdField with uid="UXDTYPWAPH",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CREDOC", cQueryName = "CREDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA a credito su operazioni da registrare",;
    HelpContextID = 254901978,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=510, Top=129, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oCREDOC_1_31.mHide()
    with this.Parent.oContained
      return (.w_VPIMPO7B>=0)
    endwith
  endfunc

  add object oDEBPRI_1_32 as StdField with uid="YGDZMAAPDI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DEBPRI", cQueryName = "DEBPRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA a debito su operazioni registrate",;
    HelpContextID = 150322122,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=359, Top=107, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oDEBPRI_1_32.mHide()
    with this.Parent.oContained
      return (.w_VPIMPO7A<0)
    endwith
  endfunc

  add object oDEBDOC_1_33 as StdField with uid="YHLDHDDVBH",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DEBDOC", cQueryName = "DEBDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA a debito su operazioni da registrate",;
    HelpContextID = 254917578,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=359, Top=129, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oDEBDOC_1_33.mHide()
    with this.Parent.oContained
      return (.w_VPIMPO7B<0)
    endwith
  endfunc

  add object oDEBEFF_1_34 as StdField with uid="FMFDJNYUFM",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DEBEFF", cQueryName = "DEBEFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA a debito su operazioni effettuate",;
    HelpContextID = 213957578,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=359, Top=151, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oDEBEFF_1_34.mHide()
    with this.Parent.oContained
      return (.w_VPIMPO7C<0)
    endwith
  endfunc

  add object oCRERES_1_39 as StdField with uid="BOEQBQAHYJ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CRERES", cQueryName = "CRERES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Credito IVA dell'anno precedente ancora utilizzabile",;
    HelpContextID = 264470234,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=510, Top=305, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oMACIVA1_1_46 as StdField with uid="LFHPCODUEQ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MACIVA1", cQueryName = "MACIVA1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Versamento minimo",;
    HelpContextID = 12365626,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=138, Top=361, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oSIMVA1_1_49 as StdField with uid="EAAGAZXWSU",rtseq=28,rtrep=.f.,;
    cFormVar = "w_SIMVA1", cQueryName = "SIMVA1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 33490650,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=299, Top=361, InputMask=replicate('X',5)


  add object oMETOD_1_50 as StdCombo with uid="UXXHHJOYVD",rtseq=29,rtrep=.f.,left=524,top=361,width=135,height=21;
    , ToolTipText = "Selezionare il metodo per il quale si vuole eseguire la stampa";
    , HelpContextID = 47553338;
    , cFormVar="w_METOD",RowSource=""+"Storico,"+"Operazioni effettuate", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMETOD_1_50.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'O',;
    space(1))))
  endfunc
  func oMETOD_1_50.GetRadio()
    this.Parent.oContained.w_METOD = this.RadioValue()
    return .t.
  endfunc

  func oMETOD_1_50.SetRadio()
    this.Parent.oContained.w_METOD=trim(this.Parent.oContained.w_METOD)
    this.value = ;
      iif(this.Parent.oContained.w_METOD=='S',1,;
      iif(this.Parent.oContained.w_METOD=='O',2,;
      0))
  endfunc


  add object oBtn_1_52 as StdButton with uid="OBZRZEORUH",left=565, top=423, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare i risultati";
    , HelpContextID = 124368154;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_52.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_53 as StdButton with uid="FWTZHDZJVZ",left=617, top=423, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare l'elaborazione";
    , HelpContextID = 117079482;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_53.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oACCONTO_1_54 as StdField with uid="BDLZBCEIJU",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ACCONTO", cQueryName = "ACCONTO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Importo acconto IVA inferiore al versamento minimo",;
    ToolTipText = "Importo da versare o da trasferire",;
    HelpContextID = 238464506,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=149, Left=510, Top=388, cSayPict="v_PV(20)", cGetPict="v_GV(20)"


  add object oObj_1_57 as cp_runprogram with uid="UJDUSKJAVW",left=749, top=290, width=161,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BA2('L')",;
    cEvent = "Conferma",;
    nPag=1;
    , HelpContextID = 53600742


  add object oObj_1_62 as cp_calclbl with uid="PNTNKTJKQG",left=191, top=108, width=152,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 53600742


  add object oObj_1_63 as cp_calclbl with uid="ZLDAZZBEOS",left=205, top=130, width=152,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 53600742


  add object oObj_1_64 as cp_calclbl with uid="ILTRGCYNAS",left=190, top=152, width=152,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 53600742

  add object oStr_1_20 as StdString with uid="GUPFZRBLIQ",Visible=.t., Left=427, Top=36,;
    Alignment=1, Width=167, Height=15,;
    Caption="Percentuale di legge:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="JHSUHWZTID",Visible=.t., Left=11, Top=35,;
    Alignment=1, Width=95, Height=15,;
    Caption="Acconto IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="COYMYXQWGR",Visible=.t., Left=10, Top=9,;
    Alignment=0, Width=276, Height=18,;
    Caption="Metodo storico"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="KNDDTZHIMY",Visible=.t., Left=10, Top=71,;
    Alignment=0, Width=276, Height=18,;
    Caption="Metodo operazioni effettuate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="GYFWXVPMHJ",Visible=.t., Left=9, Top=108,;
    Alignment=1, Width=179, Height=18,;
    Caption="IVA su oper. registrate dal"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="QRXBNAAZGA",Visible=.t., Left=9, Top=130,;
    Alignment=1, Width=193, Height=18,;
    Caption="IVA su oper. da registrare dal"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="NMFONZLNCX",Visible=.t., Left=6, Top=152,;
    Alignment=1, Width=181, Height=18,;
    Caption="IVA su oper. effettuate dal"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="ZBMNXRNWAF",Visible=.t., Left=12, Top=218,;
    Alignment=0, Width=194, Height=18,;
    Caption="Debito/credito riportato"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="UVPQJQHTSV",Visible=.t., Left=12, Top=262,;
    Alignment=0, Width=194, Height=18,;
    Caption="Crediti speciali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_37 as StdString with uid="HHCDJUDDZE",Visible=.t., Left=12, Top=284,;
    Alignment=0, Width=194, Height=18,;
    Caption="Acconto IVA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="WKEWYLEUTZ",Visible=.t., Left=10, Top=335,;
    Alignment=0, Width=276, Height=18,;
    Caption="Acconto da versare"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="HUYVXDWZNZ",Visible=.t., Left=510, Top=287,;
    Alignment=0, Width=149, Height=15,;
    Caption="Credito utilizzabile"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="UKZSBRPEOI",Visible=.t., Left=12, Top=196,;
    Alignment=0, Width=194, Height=18,;
    Caption="IVA non versata/in eccesso"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="WVYPKEBXKK",Visible=.t., Left=12, Top=240,;
    Alignment=0, Width=194, Height=18,;
    Caption="Credito IVA compensabile"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_43 as StdString with uid="PIHTZHANIP",Visible=.t., Left=12, Top=174,;
    Alignment=0, Width=194, Height=18,;
    Caption="Variazioni di imposta"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="EBYUUGFJVL",Visible=.t., Left=322, Top=390,;
    Alignment=1, Width=184, Height=18,;
    Caption="Acconto IVA da versare:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_47 as StdString with uid="FHUBBCNIYM",Visible=.t., Left=11, Top=361,;
    Alignment=1, Width=125, Height=15,;
    Caption="Versamento minimo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="ZMAZDZTSBB",Visible=.t., Left=275, Top=361,;
    Alignment=1, Width=23, Height=18,;
    Caption="in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="FGXIBPIGCL",Visible=.t., Left=402, Top=360,;
    Alignment=1, Width=118, Height=18,;
    Caption="Metodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="HKMKOHZPOO",Visible=.t., Left=359, Top=91,;
    Alignment=2, Width=149, Height=18,;
    Caption="DEBITI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_61 as StdString with uid="PWAOWUBGCY",Visible=.t., Left=511, Top=91,;
    Alignment=2, Width=149, Height=18,;
    Caption="CREDITI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_24 as StdBox with uid="DTCCLUEMPD",left=4, top=27, width=660,height=43

  add object oBox_1_26 as StdBox with uid="RLYDQYUZJF",left=4, top=87, width=660,height=244

  add object oBox_1_44 as StdBox with uid="EPNZRNSLSW",left=4, top=355, width=660,height=66
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sa2','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
