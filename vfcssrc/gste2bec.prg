* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste2bec                                                        *
*              Ricerca data filtro documenti (CPZ)                             *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-28                                                      *
* Last revis.: 2005-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste2bec",oParentObject)
return(i_retval)

define class tgste2bec as StdBatch
  * --- Local variables
  * --- WorkFile variables
  ZMPOARCH_idx=0
  ZFILTQRY_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- -- Generazione file XML per Estratto Conto --  (Invocata dalla routine GSTE_BEC {Pag.5})
    *     
    *     Routine per l'estrazione della data associata al parametro "w_INIDATE", relativo all'azione "Document".
    ah_msg( "Ricerca data filtro pubblicazioni documenti" )
    * --- Leggo la sorgente dati assegnata all'azione  "Document"
    * --- Read from ZMPOARCH
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ZMPOARCH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZMPOARCH_idx,2],.t.,this.ZMPOARCH_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARSRCCOD"+;
        " from "+i_cTable+" ZMPOARCH where ";
            +"ARCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICEAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARSRCCOD;
        from (i_cTable) where;
            ARCODICE = this.oParentObject.w_CODICEAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_CODSORG = NVL(cp_ToDate(_read_.ARSRCCOD),cp_NullValue(_read_.ARSRCCOD))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Leggo il valore assegnato al parametro "w_INIDATE", per la sorgente dati letta
    * --- Read from ZFILTQRY
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ZFILTQRY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZFILTQRY_idx,2],.t.,this.ZFILTQRY_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "FQVALORE"+;
        " from "+i_cTable+" ZFILTQRY where ";
            +"FQCODSOU = "+cp_ToStrODBC(this.oParentObject.w_CODSORG);
            +" and FQNOMPAR = "+cp_ToStrODBC(this.oParentObject.w_PARAMETR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        FQVALORE;
        from (i_cTable) where;
            FQCODSOU = this.oParentObject.w_CODSORG;
            and FQNOMPAR = this.oParentObject.w_PARAMETR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_VALFILTER = NVL(cp_ToDate(_read_.FQVALORE),cp_NullValue(_read_.FQVALORE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Estraggo la data
    if i_rows<>0
      this.oParentObject.w_DATAFILT = eval(w_VALFILTER)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ZMPOARCH'
    this.cWorkTables[2]='ZFILTQRY'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
