* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bnu                                                        *
*              Numerazione automatica codice                                   *
*                                                                              *
*      Author: ZUCCHETTI SPA -CS                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-21                                                      *
* Last revis.: 2014-02-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bnu",oParentObject,m.pTIPO)
return(i_retval)

define class tgsar_bnu as StdBatch
  * --- Local variables
  pTIPO = space(1)
  w_oPADRE = .NULL.
  w_oDETTFLD = .NULL.
  w_oDETTEXP = .NULL.
  w_LENGHT = 0
  w_MEMEXP = space(0)
  w_TIPO = space(1)
  w_TIPON = 0
  w_RESULT = space(15)
  w_OK = .f.
  w_EXPVAL = space(250)
  w_TBPROG = space(10)
  w_TBSTR = space(250)
  w_CODICE = space(15)
  w_TMPVAR = space(30)
  i = 0
  L_FirstPos = 0
  L_LastPos = 0
  w_RECNO = 0
  w_COUNT = 0
  w_CODLEN = 0
  w_NUMAUTO = 0
  w_TIPRIG = space(14)
  w_VARTOSTORE = space(30)
  w_VARTYPE = space(1)
  w_VARLENGHT = 0
  w_VARDECIMA = 0
  w_PROG = .NULL.
  w_TIPCON = space(1)
  w_PROG = space(20)
  w_STRAUTO = space(50)
  w_SELECT = space(254)
  w_WHERE = space(254)
  w_WHERE2 = space(254)
  w_FIELDS = space(250)
  w_FIECUR = space(250)
  w_GROUP = space(254)
  w_AUTON = space(254)
  w_TABLE = space(20)
  w_CHIAVE = space(20)
  w_INIPOS = 0
  w_ENDPOS = 0
  w_FIRSTWHERE = .f.
  w_POINT = 0
  w_STRTMP = space(250)
  w_FSUBSTR = space(20)
  w_FLEN = space(6)
  w_FCONCAT = space(2)
  w_LENSTR = 0
  w_CP_DBTYPE = space(10)
  * --- WorkFile variables
  XDC_FIELDS_idx=0
  TMPVARS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiamato dalla gestione di numerazione automatica (GSAR_ANU) con parametro pTIPO:
    *     'F' = Seleziona file
    *     'E' = Check lunghezza espressione
    *     'C' = Calcolo codice
    *     'A' = Visualizzo nello zoom di pag.3 gli autonumber da generare...
    *     'U' = Aggiornamento autonumber
    *     'X' = Chiusura cursori
    *     'N' = Verifica autonumber
    *     'V' = Zoom per elenco varibili/campi per valorizzazione
    this.w_oPADRE = this.oParentObject
    do case
      case this.pTIPO= "F"
        this.oParentObject.w_NANOMPRO = GetFile("fxp")
      case this.pTIPO= "E"
        * --- Tutti i codici sono di 15 caratteri.
        *     Si deve verificare che la somma delle lunghezze non sia maggiore di 15
        NC=this.w_oPADRE.cTrsName
        SELECT (NC)
        SUM t_NAEXPLEN to this.w_LENGHT
        if this.w_LENGHT>15
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg="La lunghezza supera la dimensione massima di 15 caratteri!"
          i_retcode = 'stop'
          return
        endif
      case this.pTIPO$"C-K"
        * --- Calcolo la prova del codice direttamente: non posso chiamare la funzione calnumauto
        *     perch� utilizzo la valorizzazione delle variabili dal transitorio
        this.w_oDETTFLD = this.w_oPADRE.GSAR_MNU
        NC=this.w_oDETTFLD.cTrsName
        SELECT (NC)
        SCAN
        Var = "w_"+t_NA_CAMPO
        &Var = iif(t_NA__TIPO=2, cp_ROUND(VAL(t_NAVALORE),t_NADECIMA),t_NAVALORE)
        ENDSCAN
        this.oParentObject.w_EXPRESULT = REPL("*",90)+chr(13)
        this.oParentObject.w_EXPRESULT = this.oParentObject.w_EXPRESULT+REPL(" ",24)+"VERIFICA REGOLE DI CODIFICA"+chr(13)
        this.oParentObject.w_EXPRESULT = this.oParentObject.w_EXPRESULT+REPL("*",90)+chr(13)+chr(13)
        NUMERR=0
        this.w_CODICE = ""
        OLDERR = ON("ERROR")
        ON ERROR NUMERR=ERROR()
        this.w_oDETTEXP = this.w_oPADRE.GSAR_MNV
        NC=this.w_oDETTEXP.cTrsName
        SELECT (NC)
        this.w_RECNO = RECCOUNT(NC)
        Dimension ARRFIELDS(this.w_RECNO,4)
        SCAN
        this.w_EXPVAL = STRTRAN(t_NA__EXPR,CHR(13)+CHR(10),"")
        this.w_TBSTR = this.w_EXPVAL
        if MOD(OCCURS("?",this.w_EXPVAL),2) <> 0
          * --- Se i '?' sono dispari allora la sintassi dell'espressione non � corretta
          NUMERR=1
        endif
        this.i = 0
        do while 1 <= OCCURS( "?", this.w_EXPVAL ) and NUMERR=0
          * --- Estraggo la variabile cercando "?...?"
          this.i = this.i + 1
          this.L_FirstPos = AT( "?", this.w_EXPVAL ) + 1
          this.L_LastPos = AT( "?", this.w_EXPVAL, 2 )
          this.w_TMPVAR = SUBSTR( this.w_EXPVAL, this.L_FirstPos, this.L_LastPos - this.L_FirstPos )
          this.w_TBSTR = STRTRAN( this.w_TBSTR, "?"+this.w_TMPVAR+"?", "")
          this.w_TBSTR = STRTRAN( this.w_TBSTR, " ", "")
          this.w_EXPVAL = STRTRAN( this.w_EXPVAL, "?"+this.w_TMPVAR+"?", this.w_TMPVAR)
        enddo
        ARRFIELDS[RECNO(),1] = this.w_EXPVAL
        ARRFIELDS[RECNO(),2] = t_NATIPEXP
        ARRFIELDS[RECNO(),3] = t_NAEXPLEN
        ARRFIELDS[RECNO(),4] = t_NAFLTRIM
        if t_NATIPEXP<>3
          this.w_EXPVAL = evaluate(STRTRAN(this.w_EXPVAL,CHR(13)+CHR(10),""))
        else
          * --- Non posso chiamare la cp_AskTableProg quindi per i tipi AUTONUMBER
          *     verifico la sintassi e la valorizzazione delle variabili, il valore progressivo sar� sempre 7
          if LEN(this.w_TBSTR)<>OCCURS(",",this.w_TBSTR)
            NUMERR=1
          else
            this.w_EXPVAL = REPL("0",t_NAEXPLEN-1)+"7"
          endif
        endif
        this.w_TIPRIG = iif(t_NATIPEXP=1,"[VALORE FISSO]",iif(t_NATIPEXP=2,"[ESPRESSIONE]","[AUTONUMBER]"))
        if NUMERR<>0
          this.oParentObject.w_EXPRESULT = this.oParentObject.w_EXPRESULT+"Riga "+ALLTRIM(STR(CPROWNUM))+" "+this.w_TIPRIG+": "+"Errore nell'espressione: "+t_NA__EXPR+"."+chr(13)
        else
          this.oParentObject.w_EXPRESULT = this.oParentObject.w_EXPRESULT+"Riga "+ALLTRIM(STR(CPROWNUM))+" "+this.w_TIPRIG+": "+iif(t_NAFLTRIM=1,LEFT(this.w_EXPVAL,t_NAEXPLEN),RIGHT(this.w_EXPVAL,t_NAEXPLEN))+chr(13)
          this.w_CODICE = this.w_CODICE+ALLTRIM(iif(t_NAFLTRIM=1,LEFT(this.w_EXPVAL,t_NAEXPLEN),RIGHT(this.w_EXPVAL,t_NAEXPLEN)))
        endif
        this.w_CODLEN = this.w_CODLEN+NVL(t_NAEXPLEN,0)
        if NUMERR<>0
          EXIT
        endif
        ENDSCAN
        if (this.oParentObject.w_NATIPGES="AG" and this.w_CODLEN>5) OR (this.oParentObject.w_NATIPGES$"AR-AS" and this.w_CODLEN>20) or (!this.oParentObject.w_NATIPGES$"AR-AS" and this.w_CODLEN>15)
          NUMERR=-1
        endif
        if NUMERR=0
          this.oParentObject.w_EXPRESULT = this.oParentObject.w_EXPRESULT+chr(13)+"RISULTATO: "+this.w_CODICE
        else
          if NUMERR=-1
            this.oParentObject.w_EXPRESULT = this.oParentObject.w_EXPRESULT+chr(13)+"Il codice risultante � pi� lungo della dimensione massima consentita."
          else
            this.oParentObject.w_EXPRESULT = this.oParentObject.w_EXPRESULT+chr(13)+"Non � stato possibile verificare il codice!"
          endif
        endif
        if this.pTipo="K"
          i_retcode = 'stop'
          i_retval = NUMERR
          return
        endif
        NUMERR=0
        this.w_oPADRE.oPgFrm.ActivePage = 2
      case this.pTIPO="A"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTIPO="S"
        * --- Seleziona/Deseleziona tutto
        ND = this.w_oPADRE.w_ZOOMAUT.cCursor
        SELECT (ND) 
 UPDATE (ND) SET XCHK=IIF(this.oParentObject.w_SELEZI="S",1,0)
        GOTO 1
      case this.pTIPO="U"
        this.w_oDETTEXP = this.w_oPADRE.GSAR_MNV
        NC=this.w_oDETTEXP.cTrsName
        SELECT (NC)
        this.w_RECNO = RECCOUNT(NC)
        Dimension ARRFIELDS(this.w_RECNO,4)
        SCAN
        this.w_EXPVAL = STRTRAN(t_NA__EXPR,CHR(13)+CHR(10),"")
        this.w_TBSTR = this.w_EXPVAL
        this.i = 0
        do while 1 <= OCCURS( "?", this.w_EXPVAL )
          * --- Estraggo la variabile cercando "?...?"
          this.i = this.i + 1
          this.L_FirstPos = AT( "?", this.w_EXPVAL ) + 1
          this.L_LastPos = AT( "?", this.w_EXPVAL, 2 )
          this.w_TMPVAR = SUBSTR( this.w_EXPVAL, this.L_FirstPos, this.L_LastPos - this.L_FirstPos )
          this.w_TBSTR = STRTRAN( this.w_TBSTR, "?"+this.w_TMPVAR+"?", "")
          this.w_TBSTR = STRTRAN( this.w_TBSTR, " ", "")
          this.w_EXPVAL = STRTRAN( this.w_EXPVAL, "?"+this.w_TMPVAR+"?", this.w_TMPVAR)
        enddo
        ARRFIELDS[RECNO(),1] = this.w_EXPVAL
        ARRFIELDS[RECNO(),2] = t_NATIPEXP
        ARRFIELDS[RECNO(),3] = t_NAEXPLEN
        ARRFIELDS[RECNO(),4] = t_NAFLTRIM
        ENDSCAN
        FOR this.i=1 to ALEN(ARRFIELDS,1)
        this.w_ENDPOS = ARRFIELDS(this.i,3)
        if ARRFIELDS(this.i,2) = 3
          * --- Autonumber
          this.w_AUTON = iif(EMPTY(ARRFIELDS(this.i,1)),"i_CODAZI",ARRFIELDS(this.i,1))
        endif
        ENDFOR
        NC = this.w_oPadre.w_ZOOMAUT.cCursor
        this.w_COUNT = RECCOUNT("CursVal")
        if this.w_COUNT=0
          ah_ErrorMsg("Non ci sono dati da elaborare.")
          this.oParentObject.w_PROGUPD = "S"
          i_retcode = 'stop'
          return
        else
          this.w_COUNT = 0
        endif
        Select * from "CursVal" where CHIAVE in (Select CHIAVE from (NC) where Xchk=1) into curs "Elabora"
        param=createobject("cParamObjClass") 
 SCATTER NAME param ADDITIVE
        retval = afields(ARRSTRU)
        Select "Elabora"
        GO TOP 
 SCAN
        this.w_CHIAVE = NVL(CHIAVE,"")
        for this.i=1 to ALEN(ARRSTRU,1)
        if !ALLTRIM(ARRSTRU(this.i,1))$"MAXCODE-CHIAVE-XCHK"
          comm ="ADDPROPERTY(param, 'w_"+ALLTRIM(ARRSTRU(this.i,1))+"',  NVL("+ALLTRIM(ARRSTRU(this.i,1))+",''))"
          &comm
        else
          if ALLTRIM(ARRSTRU(this.i,1))="MAXCODE"
            comm="ADDPROPERTY(param,'w_PROG',NVL("+MAXCODE+",''))" 
 &comm
          endif
        endif
        ENDFOR
        comm ="ADDPROPERTY(param, 'i_CODAZI',  i_CODAZI)"
        &comm
        this.w_STRAUTO = this.w_AUTON+",w_PROG" 
         i_Conn=i_TableProp[this.XDC_FIELDS_IDX, 3] 
 res = cp_NextTableProg(param, i_Conn, "AUTOCODE\"+alltrim(this.oParentObject.w_NATIPGES), this.w_STRAUTO)
        this.w_COUNT = this.w_COUNT+1
        ah_msg( "Processando %1",.t.,.f.,.f.,this.w_CHIAVE)
        ENDSCAN
        ah_ErrorMsg("Elaborati %1 records.",48,"",alltrim(str(this.w_COUNT)))
        this.oParentObject.w_PROGUPD = "S"
      case this.pTIPO="X"
        if USED("Elabora")
          Select Elabora 
 USE
        endif
        if USED("CursVal")
          Select CursVal 
 USE
        endif
        if USED("TMP")
          Select TMP 
 USE
        endif
      case this.pTipo="N"
        NUMERR=0
        * --- Verifico nel caso in cui ci sia un autonumber se � stato effettuato l'aggiornamento.
        this.w_oDETTEXP = this.w_oPADRE.GSAR_MNV
        NC=this.w_oDETTEXP.cTrsName
        SELECT (NC)
        COUNT FOR t_NATIPEXP=3 to this.w_NUMAUTO
        if this.w_NUMAUTO>0
          if this.oParentObject.w_PROGUPD="C"
            NUMERR=1
          else
            if this.oParentObject.w_PROGUPD="N"
              NUMERR=2
            endif
          endif
        endif
        i_retcode = 'stop'
        i_retval = NUMERR
        return
      case this.pTipo="V"
        * --- Create temporary table TMPVARS
        i_nIdx=cp_AddTableDef('TMPVARS') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPVARS_proto';
              )
        this.TMPVARS_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        NC=this.oParentObject.oParentObject.GSAR_MNV.cTrsName
        SELECT (NC)
        SCAN
        this.w_EXPVAL = STRTRAN(t_NA__EXPR,CHR(13)+CHR(10),"")
        if MOD(OCCURS("?",this.w_EXPVAL),2) = 0
          this.i = 0
          do while 1 <= OCCURS( "?", this.w_EXPVAL )
            * --- Estraggo la variabile cercando "?...?"
            this.i = this.i + 1
            this.L_FirstPos = AT( "?", this.w_EXPVAL ) + 1
            this.L_LastPos = AT( "?", this.w_EXPVAL, 2 )
            this.w_TMPVAR = SUBSTR( this.w_EXPVAL, this.L_FirstPos, this.L_LastPos - this.L_FirstPos )
            if UPPER(LEFT(this.w_TMPVAR,2))="W_"
              this.w_VARTOSTORE = SUBSTR( this.w_TMPVAR,3, LEN(ALLTRIM(this.w_TMPVAR))-2)
              * --- Insert into TMPVARS
              i_nConn=i_TableProp[this.TMPVARS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPVARS_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPVARS_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"CODVAR"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_VARTOSTORE),'TMPVARS','CODVAR');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'CODVAR',this.w_VARTOSTORE)
                insert into (i_cTable) (CODVAR &i_ccchkf. );
                   values (;
                     this.w_VARTOSTORE;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
            this.w_EXPVAL = STRTRAN( this.w_EXPVAL, "?"+this.w_TMPVAR+"?", "")
          enddo
        endif
        ENDSCAN
        vx_exec("STD\GSAR_BNU.VZM",this)
        this.oParentObject.w_NA_CAMPO=this.w_VARTOSTORE
        this.oParentObject.w_NA__TIPO=NVL(this.w_VARTYPE, " ")
        this.oParentObject.w_NALENGHT=NVL(this.w_VARLENGHT, 0)
        this.oParentObject.w_NADECIMA=NVL(this.w_VARDECIMA, 0)
        * --- Drop temporary table TMPVARS
        i_nIdx=cp_GetTableDefIdx('TMPVARS')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVARS')
        endif
      case this.pTipo="G"
        NUMERR=0
        * --- Verifico nel caso in cui ci sia un autonumber se sono presenti delle espressioni fox....
        this.w_oDETTEXP = this.w_oPADRE.GSAR_MNV
        NC=this.w_oDETTEXP.cTrsName
        SELECT (NC)
        COUNT FOR t_NATIPEXP=3 to this.w_NUMAUTO
        if this.w_NUMAUTO>0
          SELECT (NC) 
 GO TOP 
 SCAN FOR t_NATIPEXP=2
          this.w_EXPVAL = alltrim(STRTRAN(t_NA__EXPR,CHR(13)+CHR(10),""))
          if right(this.w_EXPVAL,1)<>"?" or left(this.w_EXPVAL,1)<>"?"
            NUMERR=1
          endif
          ENDSCAN
        endif
        i_retcode = 'stop'
        i_retval = NUMERR
        return
      case this.pTipo="O"
        do case
          case this.oParentObject.w_NATIPGES="CG"
            this.w_PROG = GSAR_API()
            this.w_TIPCON = "G"
          case this.oParentObject.w_NATIPGES="MG"
            this.w_PROG = GSAR_AMC()
          case this.oParentObject.w_NATIPGES="CA"
            this.w_PROG = GSCA_AVC()
          case this.oParentObject.w_NATIPGES="MA"
            this.w_PROG = GSCA_AMV()
          case this.oParentObject.w_NATIPGES="CL"
            this.w_PROG = GSAR_ACL()
            this.w_TIPCON = "C"
          case this.oParentObject.w_NATIPGES="FO"
            this.w_PROG = GSAR_AFR()
            this.w_TIPCON = "F"
          case this.oParentObject.w_NATIPGES="AG"
            this.w_PROG = GSAR_AGE()
          case this.oParentObject.w_NATIPGES="PR"
            this.w_PROG = GSPR_ACN()
          case this.oParentObject.w_NATIPGES="AR"
            this.w_PROG = GSMA_AAR()
          case this.oParentObject.w_NATIPGES="AS"
            this.w_PROG = GSMA_AAS()
        endcase
        if this.w_PROG.bSEC1
          do case
            case this.oParentObject.w_NATIPGES$"CG-CL-FO"
              this.w_PROG.w_ANCODICE = this.oParentObject.w_CHIAVEARC
              this.w_PROG.w_ANTIPCON = this.w_TIPCON
              this.w_PROG.QueryKeySet("ANTIPCON='"+this.w_TIPCON+"' AND ANCODICE='"+this.oParentObject.w_CHIAVEARC+"'")     
              this.w_PROG.LoadRecWarn()     
            case this.oParentObject.w_NATIPGES$"MG-MA"
              this.w_PROG.w_MCCODICE = this.oParentObject.w_CHIAVEARC
              this.w_PROG.QueryKeySet("MCCODICE='"+this.oParentObject.w_CHIAVEARC+"'")     
              this.w_PROG.LoadRecWarn()     
            case this.oParentObject.w_NATIPGES="CA"
              this.w_PROG.w_VCCODICE = this.oParentObject.w_CHIAVEARC
              this.w_PROG.QueryKeySet("VCCODICE='"+this.oParentObject.w_CHIAVEARC+"'")     
              this.w_PROG.LoadRecWarn()     
            case this.oParentObject.w_NATIPGES="AG"
              this.w_PROG.w_AGCODAGE = this.oParentObject.w_CHIAVEARC
              this.w_PROG.QueryKeySet("AGCODAGE='"+this.oParentObject.w_CHIAVEARC+"'")     
              this.w_PROG.LoadRecWarn()     
            case this.oParentObject.w_NATIPGES="PR"
              this.w_PROG.w_CNCODCAN = this.oParentObject.w_CHIAVEARC
              this.w_PROG.QueryKeySet("CNCODCAN='"+this.oParentObject.w_CHIAVEARC+"'")     
              this.w_PROG.LoadRecWarn()     
            case this.oParentObject.w_NATIPGES$"AR-AS"
              this.w_PROG.w_ARCODART = this.oParentObject.w_CHIAVEARC
              this.w_PROG.QueryKeySet("ARCODART='"+this.oParentObject.w_CHIAVEARC+"'")     
              this.w_PROG.LoadRecWarn()     
          endcase
        endif
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTIPO= "L"
        NUMERR=0
        * --- Controlla che la lunghezza delle espressioni sia >0
        this.w_oDETTFLD = this.w_oPADRE.GSAR_MNV
        NC=this.w_oDETTFLD.cTrsName
        SELECT (NC)
        SCAN
        if t_NAEXPLEN <= 0
          NUMERR=1
          exit
        endif
        ENDSCAN
        i_retcode = 'stop'
        i_retval = NUMERR
        return
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CP_DBTYPE = UPPER(CP_DBTYPE)
    if USED("Elabora")
      Select Elabora 
 USE
    endif
    if USED("CursVal")
      Select CursVal 
 USE
    endif
    if USED("TMP")
      Select TMP 
 USE
    endif
    this.w_oDETTEXP = this.w_oPADRE.GSAR_MNV
    NC=this.w_oDETTEXP.cTrsName
    SELECT (NC)
    * --- Verifico nel caso in cui ci sia un autonumber se sono presenti delle espressioni fox....
    NUMERR=0
    this.w_oDETTEXP = this.w_oPADRE.GSAR_MNV
    NC=this.w_oDETTEXP.cTrsName
    SELECT (NC)
    COUNT FOR t_NATIPEXP=3 to this.w_NUMAUTO
    if this.w_NUMAUTO>0
      SELECT (NC) 
 GO TOP 
 SCAN FOR t_NATIPEXP=2
      this.w_EXPVAL = alltrim(STRTRAN(t_NA__EXPR,CHR(13)+CHR(10),""))
      if right(this.w_EXPVAL,1)<>"?" or left(this.w_EXPVAL,1)<>"?"
        NUMERR=1
      endif
      ENDSCAN
    endif
    if NUMERR>0
      ah_errormsg("Attenzione: non e' possibile effettuare la verifica degli autonumber in presenza di espressioni contenenti funzioni Fox.")
      i_retcode = 'stop'
      return
    endif
    SELECT (NC)
    this.w_RECNO = RECCOUNT(NC)
    Dimension ARRFIELDS(this.w_RECNO,4)
    SCAN
    this.w_EXPVAL = STRTRAN(t_NA__EXPR,CHR(13)+CHR(10),"")
    this.w_TBSTR = this.w_EXPVAL
    this.i = 0
    do while 1 <= OCCURS( "?", this.w_EXPVAL )
      * --- Estraggo la variabile cercando "?...?"
      this.i = this.i + 1
      this.L_FirstPos = AT( "?", this.w_EXPVAL ) + 1
      this.L_LastPos = AT( "?", this.w_EXPVAL, 2 )
      this.w_TMPVAR = SUBSTR( this.w_EXPVAL, this.L_FirstPos, this.L_LastPos - this.L_FirstPos )
      if UPPER(LEFT(this.w_TMPVAR,2))<>"W_"
         
 L_errsav=on("ERROR") 
 VarMsg="" 
 ON ERROR VarMsg=message()
        a=EVAL(this.w_TMPVAR)
        if !EMPTY(VarMsg)
          ah_ErrorMsg( "Attenzione: le variabili definite in alcune espressioni contengono errori di sintassi.%0Impossibile effettuare la verifica dei progressivi",48,"")
          i_retcode = 'stop'
          return
        endif
        on error &L_errsav
        this.w_EXPVAL = STRTRAN( this.w_EXPVAL," ", "")
        this.w_EXPVAL = STRTRAN( this.w_EXPVAL, "?"+this.w_TMPVAR+"?", "")
        if RIGHT(ALLTRIM(this.w_EXPVAL),1)=","
          this.w_EXPVAL = SUBSTR(this.w_EXPVAL,1,LEN(ALLTRIM(this.w_EXPVAL))-1)
        endif
        if LEFT(ALLTRIM(this.w_EXPVAL),1)=","
          this.w_EXPVAL = SUBSTR(this.w_EXPVAL,2,LEN(ALLTRIM(this.w_EXPVAL)))
        endif
      else
        this.w_EXPVAL = STRTRAN( this.w_EXPVAL, "?"+this.w_TMPVAR+"?", this.w_TMPVAR)
      endif
    enddo
    ARRFIELDS[RECNO(),1] = iif(EMPTY(this.w_EXPVAL),i_CODAZI,this.w_EXPVAL)
    ARRFIELDS[RECNO(),2] = t_NATIPEXP
    ARRFIELDS[RECNO(),3] = t_NAEXPLEN
    ARRFIELDS[RECNO(),4] = t_NAFLTRIM
    ENDSCAN
    * --- Costruisco la frase di select sulla base dell'array...
    do case
      case this.oParentObject.w_NATIPGES="CG"
        this.w_TABLE = ALLTRIM(i_CODAZI)+"CONTI"
        this.w_CHIAVE = "ANCODICE"
        this.w_WHERE2 = " ANTIPCON='G'"
      case this.oParentObject.w_NATIPGES="MG"
        this.w_TABLE = ALLTRIM(i_CODAZI)+"MASTRI"
        this.w_CHIAVE = "MCCODICE"
      case this.oParentObject.w_NATIPGES="CA"
        this.w_TABLE = ALLTRIM(i_CODAZI)+"VOC_COST"
        this.w_CHIAVE = "VCCODICE"
      case this.oParentObject.w_NATIPGES="MA"
        this.w_TABLE = ALLTRIM(i_CODAZI)+"MASTVOCI"
        this.w_CHIAVE = "MCCODICE"
      case this.oParentObject.w_NATIPGES="CL"
        this.w_TABLE = ALLTRIM(i_CODAZI)+"CONTI"
        this.w_CHIAVE = "ANCODICE"
        this.w_WHERE2 = " ANTIPCON='C'"
      case this.oParentObject.w_NATIPGES="FO"
        this.w_TABLE = ALLTRIM(i_CODAZI)+"CONTI"
        this.w_CHIAVE = "ANCODICE"
        this.w_WHERE2 = " ANTIPCON='F'"
      case this.oParentObject.w_NATIPGES="AG"
        this.w_TABLE = ALLTRIM(i_CODAZI)+"AGENTI"
        this.w_CHIAVE = "AGCODAGE"
      case this.oParentObject.w_NATIPGES="PR"
        this.w_TABLE = ALLTRIM(i_CODAZI)+"CAN_TIER"
        this.w_CHIAVE = "CNCODCAN"
      case this.oParentObject.w_NATIPGES="AR" 
        this.w_TABLE = ALLTRIM(i_CODAZI)+"ART_ICOL"
        this.w_CHIAVE = "ARCODART"
        this.w_WHERE2 = " ARTIPART in ('PF','SE','MP','MC','MA','IM','PH')"
      case this.oParentObject.w_NATIPGES="AS" 
        this.w_TABLE = ALLTRIM(i_CODAZI)+"ART_ICOL"
        this.w_CHIAVE = "ARCODART"
        this.w_WHERE2 = " ARTIPART in ('FM','FO','DE')"
    endcase
    this.w_FSUBSTR = iif(inlist(this.w_CP_DBTYPE,"SQLSERVER","POSTGRESQL"), "SUBSTRING","SUBSTR")
    this.w_FLEN = icase(this.w_CP_DBTYPE="SQLSERVER", "LEN", this.w_CP_DBTYPE="POSTGRESQL", "CHAR_LENGTH", "LENGTH")
    this.w_FCONCAT = iif(this.w_CP_DBTYPE ="SQLSERVER","+","||")
    this.w_INIPOS = 1
    this.w_ENDPOS = 1
    this.w_FIRSTWHERE = .T.
    * --- Determino la posizione dell'autonumber all'interno del codice...
    FOR this.i=1 to ALEN(ARRFIELDS,1)
    this.w_ENDPOS = this.w_ENDPOS+ARRFIELDS(this.i,3)
    if ARRFIELDS(this.i,2) <> 3
      if inlist(this.w_CP_DBTYPE,"SQLSERVER","POSTGRESQL")
        this.w_WHERE = this.w_WHERE+iif(this.w_FIRSTWHERE,"",this.w_FCONCAT)+"LTRIM(RTRIM("+iif(ARRFIELDS(this.i,4)=1,"LEFT(","RIGHT(")+iif(ALLTRIM(STRTRAN(ARRFIELDS(this.i,1),"w_","",1,-1,1))=ALLTRIM(i_CODAZI),"'"+STRTRAN(ARRFIELDS(this.i,1),"w_","",1,-1,1)+"'",STRTRAN(ARRFIELDS(this.i,1),"w_","",1,-1,1))+","+ALLTRIM(STR(ARRFIELDS(this.i,3)))+")))"
      else
        * --- Nel caso il database sia ORACLE devo sostituire la right con una SUBSTR
        if ARRFIELDS(this.i,4)=1
          this.w_WHERE = this.w_WHERE+iif(this.w_FIRSTWHERE,"",this.w_FCONCAT)+" LTRIM(RTRIM(SUBSTR("+iif(ALLTRIM(STRTRAN(ARRFIELDS(this.i,1),"w_","",1,-1,1))=ALLTRIM(i_CODAZI),"'"+STRTRAN(ARRFIELDS(this.i,1),"w_","",1,-1,1)+"'",STRTRAN(ARRFIELDS(this.i,1),"w_","",1,-1,1))+",1,"+ALLTRIM(STR(ARRFIELDS(this.i,3)))+")))"
        else
          this.w_WHERE = this.w_WHERE+iif(this.w_FIRSTWHERE,"",this.w_FCONCAT)+" LTRIM(RTRIM(SUBSTR(LTRIM(RTRIM("+iif(ALLTRIM(STRTRAN(ARRFIELDS(this.i,1),"w_","",1,-1,1))=ALLTRIM(i_CODAZI),"'"+STRTRAN(ARRFIELDS(this.i,1),"w_","",1,-1,1)+"'",STRTRAN(ARRFIELDS(this.i,1),"w_","",1,-1,1))+")),-"+ALLTRIM(STR(ARRFIELDS(this.i,3)))+")))"
        endif
      endif
      this.w_FIRSTWHERE = .F.
    else
      this.w_EXPVAL = ARRFIELDS(this.i,1)
      do while 1 <= OCCURS( ",", this.w_EXPVAL )
        * --- Estraggo la variabile cercando "?...?"
        this.L_LastPos = AT( ",", this.w_EXPVAL)
        this.w_TMPVAR = SUBSTR( this.w_EXPVAL, 1, this.L_LastPos - 1 )
        if OCCURS("w_",lower(this.w_TMPVAR))>0
          this.w_TMPVAR = STRTRAN(lower(this.w_TMPVAR),"w_","")
          this.w_FIELDS = this.w_FIELDS+iif(!EMPTY(this.w_FIELDS),",","")+this.w_TMPVAR+" as "+this.w_TMPVAR
          this.w_FIECUR = this.w_FIECUR+iif(!EMPTY(this.w_FIECUR),",","")+"MAX("+this.w_TMPVAR+") as "+this.w_TMPVAR
        endif
        this.w_EXPVAL = SUBSTR(this.w_EXPVAL,this.L_LastPos+1,LEN(this.w_EXPVAL)-this.L_LastPos)
      enddo
      if OCCURS("w_",lower(this.w_EXPVAL))>0
        this.w_EXPVAL = STRTRAN(lower(this.w_EXPVAL),"w_","")
        this.w_FIELDS = this.w_FIELDS+iif(!EMPTY(this.w_FIELDS),",","")+this.w_EXPVAL+" as "+this.w_EXPVAL
        this.w_FIECUR = this.w_FIECUR+iif(!EMPTY(this.w_FIECUR),",","")+"MAX("+this.w_EXPVAL+") as "+this.w_EXPVAL
      endif
    endif
    this.w_INIPOS = this.w_INIPOS+ARRFIELDS(this.i,3)
    ENDFOR
    * --- Autonumber
    this.w_SELECT = "SELECT "+this.w_FSUBSTR+"("+this.w_CHIAVE+","+this.w_FLEN+"("+alltrim(this.w_WHERE)+")"+"+1,"+ALLTRIM(STR(this.w_ENDPOS-1))
    this.w_SELECT = this.w_SELECT+") AS MAXCODE, "+this.w_CHIAVE+" as CHIAVE,1 as Xchk "+iif(!EMPTY(this.w_FIELDS),","+this.w_FIELDS,"")
    this.w_GROUP = this.w_WHERE
    this.w_SELECT = this.w_SELECT+", "+alltrim(this.w_GROUP)+" as Gruppo FROM "+this.w_TABLE+" WHERE " 
    this.w_WHERE = this.w_FSUBSTR+"("+this.w_CHIAVE+",1,"+this.w_FLEN+"("+alltrim(this.w_WHERE)+"))="+alltrim(this.w_WHERE)
    this.w_SELECT = this.w_SELECT+" "+ALLTRIM(this.w_WHERE)+iif(!EMPTY(this.w_WHERE2)," AND "+this.w_WHERE2,"")
    SQLEXEC(1,this.w_SELECT,"TMP")
    if USED("TMP") and RECCOUNT("TMP")>0
      * --- Elimino eventuali valori non numerici
      Select TMP 
 Select * from TMP where VAL(NVL(MAXCODE,""))<>0 into cursor TMP 
 appo="Select MAX(maxcode) as maxcode, max(chiave) as chiave, max(Xchk) as Xchk,gruppo as gruppo"+iif(!EMPTY(this.w_FIECUR),","+this.w_FIECUR,"")+" from TMP group by gruppo  into cursor CursVal" 
 &appo
    endif
    SELECT ( this.w_oPadre.w_ZOOMAUT.cCursor )
    ZAP
    if USED("CursVal") and RECCOUNT("CursVal")>0
      * --- Inserisco il risultato del cursore Visu nello zoom
      Select CursVal
      Go Top
      Scan
      Scatter Memvar
      Select ( this.w_oPadre.w_ZOOMAUT.cCursor )
      Append blank
      Gather memvar
      Endscan
      this.oParentObject.opgfrm.page3.enabled=.t.
      this.w_oPADRE.oPgFrm.activepage = 3
      this.w_oPADRE.oPgFrm.click()     
      SELECT ( this.w_oPadre.w_ZOOMAUT.cCursor )
      NUMREC=RECCOUNT()
      GO TOP
      if NUMREC=0
        this.oParentObject.w_PROGUPD = "S"
      else
        this.oParentObject.w_PROGUPD = "C"
      endif
      SELECT ( this.w_oPadre.w_ZOOMAUT.cCursor ) 
 go top
    else
      ah_ErrorMsg(ah_MsgFormat("Non ci sono autonumber da aggiornare"),"")
      this.oParentObject.w_PROGUPD = "S"
    endif
    this.w_oPADRE.w_ZOOMAUT.refresh()     
  endproc


  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='XDC_FIELDS'
    this.cWorkTables[2]='*TMPVARS'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- gsar_bnu
  enddefine
  
  define class cParamObjClass as custom
    proc SetControlsValue()
    return
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
