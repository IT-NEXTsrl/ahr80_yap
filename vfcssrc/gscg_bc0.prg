* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bc0                                                        *
*              Ricalcola importi alla cancellazione                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_9]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2000-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bc0",oParentObject)
return(i_retval)

define class tgscg_bc0 as StdBatch
  * --- Local variables
  w_RECO = 0
  w_APPO = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola importi sul Temporaneo alla cancellazione di una riga (da GSCG_MCA)
    * --- Cicla sul Temporaneo...
    SELECT (this.oParentObject.cTrsName)
    this.w_RECO = recno()
    * --- Ricalcola Totale Importi
    SUM t_MRTOTIMP*IIF(t_MR_SEGNO="D",1,-1) ;
    FOR t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) TO this.w_APPO
    this.oParentObject.w_TOTVIS = ABS(this.w_APPO)
    this.oParentObject.w_TOTRIG = this.w_APPO
    this.oParentObject.w_SEGTOT = IIF(this.oParentObject.w_TOTRIG<0, "A","D")
    GOTO this.w_RECO
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
