* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bil                                                        *
*              Installazione componenti                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-14                                                      *
* Last revis.: 2016-06-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFROMWIZARD,bSilentInstall
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bil",oParentObject,m.pFROMWIZARD,m.bSilentInstall)
return(i_retval)

define class tgsut_bil as StdBatch
  * --- Local variables
  pFROMWIZARD = .f.
  bSilentInstall = .f.
  w_SYSPATH = space(254)
  w_DLLPATH = space(254)
  w_LibName = space(100)
  w_RegistraLib = .f.
  w_ProcessaLib = .f.
  w_LicenzaLib = space(254)
  w_nComponents = 0
  w_Counter = 0
  w_bUpdated = .f.
  w_CopiaLib = .f.
  w_MSG = space(254)
  w_IS64BITOS = .f.
  w_SYSWOW64 = space(1)
  w_ERR_REG_DLL = space(250)
  w_objFSO = .NULL.
  w_LibNameFile = space(10)
  w_VERSIONE_INSTALLATA = space(15)
  w_VERSIONE_ADHOC = space(15)
  w_NuovaVer = .f.
  w_INST = 0
  w_ADHOC = 0
  w_xCont = 0
  w_VINS = space(10)
  w_VADH = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.pFROMWIZARD = Iif(Vartype(this.pFROMWIZARD)<>"L", .F., this.pFROMWIZARD)
    this.bSilentInstall = Iif(Vartype(this.bSilentInstall)<>"L", .F., this.bSilentInstall)
    if not cp_IsAdminWin()
      this.w_MSG = "Eseguire l'applicazione come amministratore.%0Installazione componenti annullata!"
      do case
        case this.pFROMWIZARD
          this.UpdateParent("LOG",this.w_MSG)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          i_retval = "ko: " + this.w_MSG
          return
        case this.bSilentInstall
          i_retcode = 'stop'
          i_retval = ah_MsgFormat(this.w_MSG)
          return
        otherwise
          if ah_YesNo(MSG_RESTART_AS_ADMIN_QP)
            AdHocRunAs()
          endif
          i_retcode = 'stop'
          return
      endcase
    endif
    * --- Verifico se sono su un sistema a 64bit
    if Directory(GETENV("WINDIR")+ "\SysWOW64\")
      * --- Ricavo il path della cartella SYSWOW64
      this.w_SYSPATH=ADDBS( ADDBS(GETENV("WINDIR"))+ "SysWOW64")
    else
      * --- Ricavo il path della cartella SYSTEM32
      this.w_SYSPATH=ADDBS( ADDBS(GETENV("WINDIR"))+ "SYSTEM32")
    endif
    * --- =====================================================
    * --- -- funzione definita nella SSFALIBRARY
    this.w_IS64BITOS = is64bitOS()
    * --- =====================================================
    this.w_SYSWOW64 = GETENV("WINDIR") + "\SysWOW64\"
    * --- =====================================================
    this.w_DLLPATH = ".\"
    * --- =====================================================
    this.w_objFSO=createobject("Scripting.FileSystemObject")
    * --- Colonne array:
    *     1=nome libreria
    *     2=richiesta registrazione regsvr32
    *     3=eventuale file di licenza da copiare
    *     4=da processare
    *     5=copia nella System32 di windows, altrimenti nella exe
    Dimension aComponents(1,5)
    this.w_nComponents = 1
    * --- Definizione componenti
    aComponents(this.w_nComponents,1)="libeay32.dll"
    aComponents(this.w_nComponents,2)=.F.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= .T.
    aComponents(this.w_nComponents,5)= .T.
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,5)
    aComponents(this.w_nComponents,1)="ssleay32.dll"
    aComponents(this.w_nComponents,2)=.F.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= .T.
    aComponents(this.w_nComponents,5)= .T.
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,5)
    aComponents(this.w_nComponents,1)="zlib1.dll"
    aComponents(this.w_nComponents,2)=.F.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= .T.
    aComponents(this.w_nComponents,5)= .T.
    * --- MSVS 2012
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,5)
    aComponents(this.w_nComponents,1)="atl110.dll"
    aComponents(this.w_nComponents,2)=.F.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= .T.
    aComponents(this.w_nComponents,5)= .T.
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,5)
    aComponents(this.w_nComponents,1)="msvcp110.dll"
    aComponents(this.w_nComponents,2)=.F.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= .T.
    aComponents(this.w_nComponents,5)= .T.
    * --- --
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,5)
    aComponents(this.w_nComponents,1)="msvcr110.dll"
    aComponents(this.w_nComponents,2)=.F.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= .T.
    aComponents(this.w_nComponents,5)= .T.
    * --- Protocollo https supportato
    this.w_nComponents = this.w_nComponents+1
    Dimension aComponents(this.w_nComponents,5)
    aComponents(this.w_nComponents,1)="SyncroDllClient.dll"
    aComponents(this.w_nComponents,2)=.T.
    aComponents(this.w_nComponents,3)=""
    aComponents(this.w_nComponents,4)= .T.
    aComponents(this.w_nComponents,5)= .T.
    * --- ====== Controllo installazione componenti =====================================================
    if !this.bSilentInstall
      this.w_bUpdated = .T.
      this.w_Counter = 1
      do while this.w_bUpdated And this.w_Counter<=this.w_nComponents
        this.w_LibName = aComponents(this.w_Counter,1)
        this.w_CopiaLib = aComponents(this.w_Counter,5)
        * --- Verifico se il file esiste gi� e se i checksum delle dei due file corrispondono
        if !cp_FileExist( ForcePath(this.w_LibName, Iif(this.w_CopiaLib, this.w_SYSPATH, cHomeDir)) ) Or Sys(2007, FileToStr(ForcePath(this.w_LibName, this.w_DLLPATH)))<>Sys(2007, FileToStr(ForcePath(this.w_LibName, Iif(this.w_CopiaLib, this.w_SYSPATH, cHomeDir))))
          this.w_bUpdated = .F.
        endif
        * --- Aggiorna contatore
        this.w_Counter = this.w_Counter+1
      enddo
      if this.w_bUpdated
        * --- OCX e DLL gi� installati
        if not (ah_YesNo("Attenzione! Componenti gi� installati. Procedere con l'aggiornamento?","!"))
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- ====== Registrazione componenti ==================================================================
    this.w_Counter = 1
    do while this.w_Counter<=this.w_nComponents
      * --- Libreria corrente
      this.w_LibName = aComponents(this.w_Counter,1)
      this.w_RegistraLib = aComponents(this.w_Counter,2)
      this.w_LicenzaLib = aComponents(this.w_Counter,3)
      this.w_ProcessaLib = aComponents(this.w_Counter,4)
      this.w_CopiaLib = aComponents(this.w_Counter,5)
      this.w_LibNameFile = IIF(this.w_CopiaLib, this.w_SYSPATH, cHomeDir)+this.w_LibName
      this.w_ERR_REG_DLL = ""
      this.w_VERSIONE_INSTALLATA = ""
      this.w_VERSIONE_ADHOC = ""
      if this.w_ProcessaLib
        * --- Se il file � gi� presente, cancella, deregistra, rimette e registra (se necessario)
        if File(this.w_LibNameFile)
          if FILE(this.w_DLLPATH+this.w_LibName) and FILE(this.w_SYSPATH+this.w_LibName)
            * --- Aggiorna le librerie di sistema solo se la versione da installare � pi� recente
            this.w_VERSIONE_INSTALLATA = this.w_objFSO.GetFileVersion(this.w_SYSPATH+this.w_LibName)
            this.w_VERSIONE_ADHOC = this.w_objFSO.GetFileVersion(this.w_DLLPATH+this.w_LibName)
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Not(File(this.w_LibNameFile))
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      * --- Aggiorna contatore
      this.w_Counter = this.w_Counter+1
    enddo
    this.w_objFSO = .NULL.
    if !Empty(this.w_ERR_REG_DLL)
      this.w_MSG = ah_MsgFormat("Errori in installazione componenti%0%0%1", Alltrim(this.w_ERR_REG_DLL))
    else
      this.w_MSG = "Installazione componenti effettuata"
    endif
    if this.pFROMWIZARD
      this.UpdateParent("ESITO",.T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if !this.bSilentInstall
      Ah_ErrorMsg(this.w_MSG,64,"INSTALLAZIONE COMPONENTI")
    else
      i_retcode = 'stop'
      i_retval = Alltrim(this.w_ERR_REG_DLL)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messaggio
    if !this.bSilentInstall
      ah_Msg("Disinstallo %1",.t.,.f.,.f.,this.w_LibName)
    endif
    * --- Gestione errori
    L_OLDERR=ON("ERROR")
    L_Err=.F.
    ON ERROR L_Err=.T.
    * --- Cancellazione fisica libreria
    if File(this.w_LibNameFile)
      * --- Eventuale deregistrazione libreria
      if this.w_RegistraLib and this.NuovaVersione()
        local cPathExe, oWSShell 
 cPathExe=this.w_SYSPATH+ "REGSVR32.exe /U /S " + this.w_LibNameFile 
 oWSShell = CREATEOBJECT("WScript.Shell")
         
 retValue=oWSShell.Run(cPathExe,0,.T.) 
 release cPathExe 
 release oWSShell
        if retValue <> 0
          this.w_ERR_REG_DLL = this.w_ERR_REG_DLL + "Si sono verificati errori in fase di disinstallazione della dll "+alltrim(this.w_LibName)+chr(10)+Chr(13)
        endif
      endif
      w_FILEORIG = this.w_LibNameFile 
 DELETE FILE &w_FILEORIG RECYCLE
      if Not(Empty(this.w_LicenzaLib)) and File(IIF(this.w_CopiaLib, this.w_SYSPATH, cHomeDir) + this.w_LicenzaLib)
        w_FILEORIG = IIF(this.w_CopiaLib, this.w_SYSPATH, cHomeDir) + this.w_LicenzaLib 
 DELETE FILE &w_FILEORIG RECYCLE
      endif
    endif
    ON ERROR &L_OLDERR
    if L_Err
      this.w_MSG = ah_MsgFormat("[2] Libreria %1 in uso: chiudere tutti i programmi attualmente aperti e ripetere l'installazione componenti%0%0Errore: %2", this.w_LibName, Message())
      do case
        case this.pFROMWIZARD
          this.UpdateParent("LOG",this.w_MSG)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          i_retval = "ko: " + this.w_MSG
          return
        case this.bSilentInstall
          i_retcode = 'stop'
          i_retval = this.w_MSG
          return
        otherwise
          ah_ErrorMsg(this.w_MSG,48)
          i_retcode = 'stop'
          return
      endcase
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if FILE(this.w_DLLPATH+this.w_LibName)
      if !this.bSilentInstall
        ah_Msg("Installo %1",.t.,.f.,.f.,this.w_LibName)
      endif
      * --- Errori
      L_OLDERR=ON("ERROR")
      L_Err=.F.
      ON ERROR L_Err=.T.
      * --- Copia ------
      w_FILEORIG = this.w_DLLPATH+this.w_LibName
      w_FILEDEST = this.w_LibNameFile
      COPY FILE &w_FILEORIG TO &w_FILEDEST 
 release &w_FILEDEST
      * --- Copia eventuale file di licenza
      if Not(L_Err) and Not(Empty(this.w_LicenzaLib))
        w_FILEORIG = this.w_DLLPATH+this.w_LicenzaLib
        if File(w_FILEORIG)
          w_FILEDEST = IIF(this.w_CopiaLib, this.w_SYSPATH, cHomeDir) + this.w_LicenzaLib
          COPY FILE &w_FILEORIG TO &w_FILEDEST
        endif
      endif
      ON ERROR &L_OLDERR
      * --- Gestione errori
      if L_Err
        this.w_MSG = ah_MsgFormat("[3] Libreria %1 in uso: chiudere tutti i programmi attualmente aperti e ripetere l'installazione componenti%0%0Errore: %2", this.w_LibName, Message())
        do case
          case this.pFROMWIZARD
            this.UpdateParent("LOG",this.w_MSG)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            i_retval = "ko: " + this.w_MSG
            return
          case this.bSilentInstall
            i_retcode = 'stop'
            i_retval = this.w_MSG
            return
          otherwise
            ah_ErrorMsg(this.w_MSG,48)
            i_retcode = 'stop'
            return
        endcase
        i_retcode = 'stop'
        return
      endif
      if this.w_RegistraLib
        local cPathExe, oWSShell 
 cPathExe=IIF(this.w_IS64BITOS, this.w_SYSWOW64,"") + "REGSVR32.exe /S " + this.w_LibNameFile 
 oWSShell = CREATEOBJECT("WScript.Shell")
         
 retValue=oWSShell.Run(cPathExe,0,.T.) 
 release cPathExe 
 release oWSShell
        if retValue <> 0
          this.w_ERR_REG_DLL = this.w_ERR_REG_DLL + "Si sono verificati errori in fase di installazione della dll "+alltrim(this.w_LibName)+chr(10)+Chr(13)
        endif
      endif
    else
      this.w_MSG = ah_MsgFormat("[3] Libreria %1 non trovata nella cartella %2 dell' installazione, operazione sospesa", this.w_LibName, IIF(this.w_DLLPATH=".\","EXE",this.w_DLLPATH))
      do case
        case this.pFROMWIZARD
          this.UpdateParent("LOG",this.w_MSG)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          i_retval = "ko: " + this.w_MSG
          return
        case this.bSilentInstall
          i_retcode = 'stop'
          i_retval = this.w_MSG
          return
        otherwise
          ah_ErrorMsg(this.w_MSG,48)
          i_retcode = 'stop'
          return
      endcase
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure NuovaVersione
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se la versione da installare � pi� recente
    this.w_NuovaVer = empty(this.w_VERSIONE_INSTALLATA) or empty(this.w_VERSIONE_ADHOC) 
    if not this.w_NuovaVer
      if "." $ this.w_VERSIONE_INSTALLATA
        this.w_INST = ALINES(A_INST,this.w_VERSIONE_INSTALLATA,0,".")
        this.w_ADHOC = ALINES(A_ADHOC,this.w_VERSIONE_ADHOC,0,".")
        this.w_xCont = 1
        do while this.w_xCont<=MIN(this.w_INST,this.w_ADHOC)
          this.w_VINS = A_INST[this.w_xCont]
          this.w_VADH = A_ADHOC[this.w_xCont]
          if Empty(Chrtran(this.w_VINS,"1234567890","")) and Empty(Chrtran(this.w_VADH,"1234567890",""))
            * --- la versione contiene solo caratteri numerici
            if val(this.w_VINS)<>val(this.w_VADH)
              this.w_NuovaVer = val(this.w_VINS)<val(this.w_VADH)
              this.w_xCont = MAX(this.w_INST,this.w_ADHOC)
            endif
          else
            if this.w_VINS<>this.w_VADH
              this.w_NuovaVer = this.w_VINS<this.w_VADH
              this.w_xCont = MAX(this.w_INST,this.w_ADHOC)
            endif
          endif
          this.w_xCont = this.w_xCont+1
        enddo
        RELEASE A_INST, A_ADHOC
      else
        this.w_NuovaVer = this.w_VERSIONE_INSTALLATA<this.w_VERSIONE_ADHOC
      endif
    endif
    return(this.w_NuovaVer)
  endproc


  procedure UpdateParent
    param pType,pMsg
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    private oWizard
    m.oWizard=createobject("")
    m.pType = Alltrim(Upper(m.pType))
    m.pMsg = Iif( Vartype(m.pMsg)="C", Alltrim(m.pMsg), m.pMsg)
    m.oWizard = .Null.
    * --- Determina puntatore
    if VarType(this.oParentObject)="O" 
      if At("gsdi_bkwizard", Lower(this.oParentObject.Class))>0
        m.oWizard = This.oParentObject
      else
        m.oWizard = This.oParentObject.oParentObject
      endif
    endif
    * --- Aggiornamento
    if Vartype(m.oWizard)="O"
      do case
        case m.pType=="LOG"
          m.oWizard.UpdLog(m.pMsg)
        case m.pType=="ESITO"
          m.oWizard.wz_Esito = m.pMsg
      endcase
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pFROMWIZARD,bSilentInstall)
    this.pFROMWIZARD=pFROMWIZARD
    this.bSilentInstall=bSilentInstall
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFROMWIZARD,bSilentInstall"
endproc
