* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sdi                                                        *
*              Elaborazione liquidazione periodica IVA                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_125]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-30                                                      *
* Last revis.: 2014-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sdi",oParentObject))

* --- Class definition
define class tgscg_sdi as StdForm
  Top    = 34
  Left   = 71

  * --- Standard Properties
  Width  = 521
  Height = 241
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-02-20"
  HelpContextID=74065257
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  _IDX = 0
  ATTIMAST_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gscg_sdi"
  cComment = "Elaborazione liquidazione periodica IVA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_ANNO = space(4)
  o_ANNO = space(4)
  w_NUMPER = 0
  o_NUMPER = 0
  w_DESCRI = space(30)
  w_MTIPLIQ = space(1)
  o_MTIPLIQ = space(1)
  w_MTIPDIC = space(1)
  w_CODATT = space(5)
  w_DESATT = space(35)
  w_INTLIG = space(1)
  w_PRPARI = 0
  w_PREFIS = space(20)
  w_FLDEFI = space(1)
  w_STAREG = space(1)
  w_MFLTEST = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DENMAG = 0
  w_IVACOF = space(16)
  w_IVACAR = space(1)
  w_MCOFAZI = space(16)
  w_MPIVAZI = space(12)
  w_MCONCES = space(3)
  w_ATNUMREG = 0
  w_ATTIPREG = space(1)
  w_STOPELA = .F.
  w_GIRIVA = space(2)
  w_NEWIVA = space(1)
  w_TIPDEN = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sdiPag1","gscg_sdi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANNO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='ATTIMAST'
    this.cWorkTables[2]='AZIENDA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCG_BDI with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_sdi
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_ANNO=space(4)
      .w_NUMPER=0
      .w_DESCRI=space(30)
      .w_MTIPLIQ=space(1)
      .w_MTIPDIC=space(1)
      .w_CODATT=space(5)
      .w_DESATT=space(35)
      .w_INTLIG=space(1)
      .w_PRPARI=0
      .w_PREFIS=space(20)
      .w_FLDEFI=space(1)
      .w_STAREG=space(1)
      .w_MFLTEST=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DENMAG=0
      .w_IVACOF=space(16)
      .w_IVACAR=space(1)
      .w_MCOFAZI=space(16)
      .w_MPIVAZI=space(12)
      .w_MCONCES=space(3)
      .w_ATNUMREG=0
      .w_ATTIPREG=space(1)
      .w_STOPELA=.f.
      .w_GIRIVA=space(2)
      .w_NEWIVA=space(1)
      .w_TIPDEN=space(1)
        .w_CODAZI = I_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_ANNO = ALLTRIM(STR(YEAR(i_datsys)))
          .DoRTCalc(3,3,.f.)
        .w_DESCRI = IIF(.w_NUMPER>0, CALCPER(.w_NUMPER, .w_TIPDEN), SPACE(30))
        .w_MTIPLIQ = 'S'
          .DoRTCalc(6,6,.f.)
        .w_CODATT = IIF(.w_MTIPLIQ<>'R', g_CATAZI, SPACE(5))
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODATT))
          .link_1_7('Full')
        endif
          .DoRTCalc(8,11,.f.)
        .w_FLDEFI = 'N'
          .DoRTCalc(13,13,.f.)
        .w_MFLTEST = ' '
        .w_OBTEST = i_INIDAT
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
          .DoRTCalc(16,23,.f.)
        .w_STOPELA = .T.
    endwith
    this.DoRTCalc(25,27,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_ANNO<>.w_ANNO
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.t.)
        if .o_NUMPER<>.w_NUMPER
            .w_DESCRI = IIF(.w_NUMPER>0, CALCPER(.w_NUMPER, .w_TIPDEN), SPACE(30))
        endif
        .DoRTCalc(5,6,.t.)
        if .o_MTIPLIQ<>.w_MTIPLIQ
            .w_CODATT = IIF(.w_MTIPLIQ<>'R', g_CATAZI, SPACE(5))
          .link_1_7('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .DoRTCalc(8,23,.t.)
            .w_STOPELA = .T.
        * --- Area Manuale = Calculate
        * --- gscg_sdi
        **aggiorna valore globale della periodicita iva
        g_TIPDEN = IIF (not EMPTY (.w_TIPDEN),.w_TIPDEN,g_TIPDEN)
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(25,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMTIPLIQ_1_5.enabled = this.oPgFrm.Page1.oPag.oMTIPLIQ_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_7.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_7.mCond()
    this.oPgFrm.Page1.oPag.oPRPARI_1_10.enabled = this.oPgFrm.Page1.oPag.oPRPARI_1_10.mCond()
    this.oPgFrm.Page1.oPag.oMFLTEST_1_14.enabled = this.oPgFrm.Page1.oPag.oMFLTEST_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODATT_1_7.visible=!this.oPgFrm.Page1.oPag.oCODATT_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDESATT_1_8.visible=!this.oPgFrm.Page1.oPag.oDESATT_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZDENMAG,AZIVACAR,AZIVACOF,AZCOFAZI,AZPIVAZI,AZIVACON,AZTIPDIC,AZSTAREG,AZGIRIVA,AZNEWIVA,AZTIPDEN";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZDENMAG,AZIVACAR,AZIVACOF,AZCOFAZI,AZPIVAZI,AZIVACON,AZTIPDIC,AZSTAREG,AZGIRIVA,AZNEWIVA,AZTIPDEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_DENMAG = NVL(_Link_.AZDENMAG,0)
      this.w_IVACAR = NVL(_Link_.AZIVACAR,space(1))
      this.w_IVACOF = NVL(_Link_.AZIVACOF,space(16))
      this.w_MCOFAZI = NVL(_Link_.AZCOFAZI,space(16))
      this.w_MPIVAZI = NVL(_Link_.AZPIVAZI,space(12))
      this.w_MCONCES = NVL(_Link_.AZIVACON,space(3))
      this.w_MTIPDIC = NVL(_Link_.AZTIPDIC,space(1))
      this.w_STAREG = NVL(_Link_.AZSTAREG,space(1))
      this.w_GIRIVA = NVL(_Link_.AZGIRIVA,space(2))
      this.w_NEWIVA = NVL(_Link_.AZNEWIVA,space(1))
      this.w_TIPDEN = NVL(_Link_.AZTIPDEN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_DENMAG = 0
      this.w_IVACAR = space(1)
      this.w_IVACOF = space(16)
      this.w_MCOFAZI = space(16)
      this.w_MPIVAZI = space(12)
      this.w_MCONCES = space(3)
      this.w_MTIPDIC = space(1)
      this.w_STAREG = space(1)
      this.w_GIRIVA = space(2)
      this.w_NEWIVA = space(1)
      this.w_TIPDEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODATT,ATDESATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIMAST','*','ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_7'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(5))
      this.w_DESATT = NVL(_Link_.ATDESATT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(5)
      endif
      this.w_DESATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANNO_1_2.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_2.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPER_1_3.value==this.w_NUMPER)
      this.oPgFrm.Page1.oPag.oNUMPER_1_3.value=this.w_NUMPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_4.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_4.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMTIPLIQ_1_5.RadioValue()==this.w_MTIPLIQ)
      this.oPgFrm.Page1.oPag.oMTIPLIQ_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_7.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_7.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_8.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_8.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPARI_1_10.value==this.w_PRPARI)
      this.oPgFrm.Page1.oPag.oPRPARI_1_10.value=this.w_PRPARI
    endif
    if not(this.oPgFrm.Page1.oPag.oPREFIS_1_11.value==this.w_PREFIS)
      this.oPgFrm.Page1.oPag.oPREFIS_1_11.value=this.w_PREFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDEFI_1_12.RadioValue()==this.w_FLDEFI)
      this.oPgFrm.Page1.oPag.oFLDEFI_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAREG_1_13.RadioValue()==this.w_STAREG)
      this.oPgFrm.Page1.oPag.oSTAREG_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMFLTEST_1_14.RadioValue()==this.w_MFLTEST)
      this.oPgFrm.Page1.oPag.oMFLTEST_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ANNO)) or not(Val(.w_ANNO)>1900))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NUMPER)) or not(.w_NUMPER<5 OR (.w_NUMPER<13 AND .w_TIPDEN="M")))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMPER_1_3.SetFocus()
            i_bnoObbl = !empty(.w_NUMPER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero periodo non congruente")
          case   (empty(.w_CODATT))  and not(.w_MTIPLIQ='R')  and (g_ATTIVI='S' AND .w_MTIPLIQ<>'R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATT_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CODATT)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ANNO = this.w_ANNO
    this.o_NUMPER = this.w_NUMPER
    this.o_MTIPLIQ = this.w_MTIPLIQ
    return

enddefine

* --- Define pages as container
define class tgscg_sdiPag1 as StdContainer
  Width  = 517
  height = 241
  stdWidth  = 517
  stdheight = 241
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANNO_1_2 as StdField with uid="SVOOWJDNJU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento del periodo di liquidazione",;
    HelpContextID = 68547322,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=98, Top=13, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Val(.w_ANNO)>1900)
    endwith
    return bRes
  endfunc

  add object oNUMPER_1_3 as StdField with uid="UALDNSWUXR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NUMPER", cQueryName = "NUMPER",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero periodo non congruente",;
    ToolTipText = "Periodo: 1..4 per trimestrale; 1..12 per mensile",;
    HelpContextID = 37422294,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=217, Top=13, cSayPict='"99"', cGetPict='"99"'

  func oNUMPER_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMPER<5 OR (.w_NUMPER<13 AND .w_TIPDEN="M"))
    endwith
    return bRes
  endfunc

  add object oDESCRI_1_4 as StdField with uid="BHYHEPIOIM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 167662646,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=257, Top=13, InputMask=replicate('X',30)


  add object oMTIPLIQ_1_5 as StdCombo with uid="JQOGWFIUJG",rtseq=5,rtrep=.f.,left=98,top=45,width=134,height=21;
    , ToolTipText = "Tipo liquidazione riepilogativa o per singola attivit�";
    , HelpContextID = 162186182;
    , cFormVar="w_MTIPLIQ",RowSource=""+"Per singola attivit�,"+"Riepilogativa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMTIPLIQ_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oMTIPLIQ_1_5.GetRadio()
    this.Parent.oContained.w_MTIPLIQ = this.RadioValue()
    return .t.
  endfunc

  func oMTIPLIQ_1_5.SetRadio()
    this.Parent.oContained.w_MTIPLIQ=trim(this.Parent.oContained.w_MTIPLIQ)
    this.value = ;
      iif(this.Parent.oContained.w_MTIPLIQ=='S',1,;
      iif(this.Parent.oContained.w_MTIPLIQ=='R',2,;
      0))
  endfunc

  func oMTIPLIQ_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ATTIVI='S')
    endwith
   endif
  endfunc

  add object oCODATT_1_7 as StdField with uid="MKQDHCLUOQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 85683750,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=98, Top=75, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ATTIMAST", oKey_1_1="ATCODATT", oKey_1_2="this.w_CODATT"

  func oCODATT_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ATTIVI='S' AND .w_MTIPLIQ<>'R')
    endwith
   endif
  endfunc

  func oCODATT_1_7.mHide()
    with this.Parent.oContained
      return (.w_MTIPLIQ='R')
    endwith
  endfunc

  func oCODATT_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ATTIMAST','*','ATCODATT',cp_AbsName(this.parent,'oCODATT_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object oDESATT_1_8 as StdField with uid="FBAHBADWOH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attivit�",;
    HelpContextID = 85742646,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=166, Top=75, InputMask=replicate('X',35)

  func oDESATT_1_8.mHide()
    with this.Parent.oContained
      return (.w_MTIPLIQ='R')
    endwith
  endfunc

  add object oPRPARI_1_10 as StdField with uid="GZVSBCMEQD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PRPARI", cQueryName = "PRPARI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultima pagina stampata nel corrispondente R.I.",;
    HelpContextID = 167522806,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=98, Top=107, cSayPict='"9999999"', cGetPict='"9999999"'

  func oPRPARI_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INTLIG='S')
    endwith
   endif
  endfunc

  add object oPREFIS_1_11 as StdField with uid="SJBUMXHJGU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PREFIS", cQueryName = "PREFIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso che comparir� nella stampa prima del numero di pagina",;
    HelpContextID = 57704950,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=309, Top=107, InputMask=replicate('X',20)


  add object oFLDEFI_1_12 as StdCombo with uid="JCSARPBJAQ",rtseq=12,rtrep=.f.,left=98,top=139,width=122,height=21;
    , ToolTipText = "Tipo di elaborazione: se definitiva, aggiorna i dati della liquidazione periodica IVA";
    , HelpContextID = 155151190;
    , cFormVar="w_FLDEFI",RowSource=""+"Definitiva,"+"In prova,"+"Ristampa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLDEFI_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oFLDEFI_1_12.GetRadio()
    this.Parent.oContained.w_FLDEFI = this.RadioValue()
    return .t.
  endfunc

  func oFLDEFI_1_12.SetRadio()
    this.Parent.oContained.w_FLDEFI=trim(this.Parent.oContained.w_FLDEFI)
    this.value = ;
      iif(this.Parent.oContained.w_FLDEFI=='S',1,;
      iif(this.Parent.oContained.w_FLDEFI=='N',2,;
      iif(this.Parent.oContained.w_FLDEFI=='R',3,;
      0)))
  endfunc


  add object oSTAREG_1_13 as StdCombo with uid="ODCBMECLIH",rtseq=13,rtrep=.f.,left=341,top=139,width=122,height=21;
    , ToolTipText = "Formato dei moduli di stampa";
    , HelpContextID = 121390118;
    , cFormVar="w_STAREG",RowSource=""+"Orizzontale,"+"Verticale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTAREG_1_13.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'V',;
    space(1))))
  endfunc
  func oSTAREG_1_13.GetRadio()
    this.Parent.oContained.w_STAREG = this.RadioValue()
    return .t.
  endfunc

  func oSTAREG_1_13.SetRadio()
    this.Parent.oContained.w_STAREG=trim(this.Parent.oContained.w_STAREG)
    this.value = ;
      iif(this.Parent.oContained.w_STAREG=='O',1,;
      iif(this.Parent.oContained.w_STAREG=='V',2,;
      0))
  endfunc

  add object oMFLTEST_1_14 as StdCheck with uid="FBCCFMYYLU",rtseq=14,rtrep=.f.,left=98, top=174, caption="Stampa su modulo continuo",;
    ToolTipText = "Se attivo: stampa su modulo continuo",;
    HelpContextID = 213981754,;
    cFormVar="w_MFLTEST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMFLTEST_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMFLTEST_1_14.GetRadio()
    this.Parent.oContained.w_MFLTEST = this.RadioValue()
    return .t.
  endfunc

  func oMFLTEST_1_14.SetRadio()
    this.Parent.oContained.w_MFLTEST=trim(this.Parent.oContained.w_MFLTEST)
    this.value = ;
      iif(this.Parent.oContained.w_MFLTEST=='S',1,;
      0)
  endfunc

  func oMFLTEST_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_ATTIVI='S' AND .w_MTIPLIQ<>'R') OR VAL(.w_ANNO)>2001)
    endwith
   endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="IVCYUSQARH",left=411, top=189, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "premere per iniziare l'elaborazione";
    , HelpContextID = 74036506;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        do GSCG_BDI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_NUMPER >0)
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="IAKHGCJIPR",left=461, top=189, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 66747834;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_31 as cp_runprogram with uid="YKIXWMKGBT",left=2, top=253, width=449,height=19,;
    caption='GSCG_BD1',;
   bGlobalFont=.t.,;
    prg="GSCG_BD1",;
    cEvent = "w_MTIPLIQ Changed,w_CODATT Changed,Blank,w_ANNO Changed",;
    nPag=1;
    , HelpContextID = 204381545

  add object oStr_1_15 as StdString with uid="POUJUIROUE",Visible=.t., Left=14, Top=75,;
    Alignment=1, Width=82, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_MTIPLIQ='R')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="YZUGOQHTAC",Visible=.t., Left=14, Top=13,;
    Alignment=1, Width=82, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="XYYGGYEUUU",Visible=.t., Left=14, Top=45,;
    Alignment=1, Width=82, Height=15,;
    Caption="Liquidazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="TYAULEOLNX",Visible=.t., Left=151, Top=13,;
    Alignment=1, Width=64, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="PNZSMSPBCF",Visible=.t., Left=7, Top=139,;
    Alignment=1, Width=89, Height=15,;
    Caption="Elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="FWNKOATCCI",Visible=.t., Left=12, Top=107,;
    Alignment=1, Width=84, Height=15,;
    Caption="Ultima pagina:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="QBIQLOVEJE",Visible=.t., Left=197, Top=107,;
    Alignment=1, Width=110, Height=15,;
    Caption="Prefisso num. pag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="IZCTKFJGKB",Visible=.t., Left=250, Top=139,;
    Alignment=1, Width=89, Height=15,;
    Caption="Modulo stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sdi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
