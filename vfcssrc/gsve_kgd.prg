* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kgd                                                        *
*              Generazione distinte su file                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_131]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-25                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kgd",oParentObject))

* --- Class definition
define class tgsve_kgd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 537
  Height = 211
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=32125545
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  PAR_PROV_IDX = 0
  cPrg = "gsve_kgd"
  cComment = "Generazione distinte su file"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_TRIPRE = space(1)
  w_DIR = space(254)
  o_DIR = space(254)
  w_GENFIR = space(1)
  w_NUMTR1 = space(4)
  o_NUMTR1 = space(4)
  w_NUMTR2 = space(4)
  o_NUMTR2 = space(4)
  w_NUMTR3 = space(4)
  o_NUMTR3 = space(4)
  w_NUMTR4 = space(4)
  o_NUMTR4 = space(4)
  w_NUMTR5 = space(4)
  o_NUMTR5 = space(4)
  w_TIPGEN = space(1)
  w_TIPDIS = space(1)
  o_TIPDIS = space(1)
  w_TIPDIS1 = space(1)
  w_ANNO = space(4)
  w_ANNOF = space(4)
  o_ANNOF = space(4)
  w_ANFIRR = space(4)
  o_ANFIRR = space(4)
  w_TRIRIF = space(1)
  o_TRIRIF = space(1)
  w_ANPREV = space(4)
  o_ANPREV = space(4)
  w_PATH = space(254)
  o_PATH = space(254)
  w_NOMEFILE = space(8)
  o_NOMEFILE = space(8)
  w_DIRGEN = space(254)
  w_SEPREC = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_PROVEN = space(1)
  w_DATA = ctod('  /  /  ')
  w_DATEFIR = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kgdPag1","gsve_kgd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPGEN_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PAR_PROV'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_TRIPRE=space(1)
      .w_DIR=space(254)
      .w_GENFIR=space(1)
      .w_NUMTR1=space(4)
      .w_NUMTR2=space(4)
      .w_NUMTR3=space(4)
      .w_NUMTR4=space(4)
      .w_NUMTR5=space(4)
      .w_TIPGEN=space(1)
      .w_TIPDIS=space(1)
      .w_TIPDIS1=space(1)
      .w_ANNO=space(4)
      .w_ANNOF=space(4)
      .w_ANFIRR=space(4)
      .w_TRIRIF=space(1)
      .w_ANPREV=space(4)
      .w_PATH=space(254)
      .w_NOMEFILE=space(8)
      .w_DIRGEN=space(254)
      .w_SEPREC=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_PROVEN=space(1)
      .w_DATA=ctod("  /  /  ")
      .w_DATEFIR=ctod("  /  /  ")
        .w_CODAZI = I_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,9,.f.)
        .w_TIPGEN = 'P'
        .w_TIPDIS = 'P'
        .w_TIPDIS1 = 'R'
          .DoRTCalc(13,14,.f.)
        .w_ANFIRR = IIF(NOT EMPTY(.w_ANNOF), ALLTRIM(STR(VAL(.w_ANNOF)+1)), SPACE(4))
        .w_TRIRIF = IIF(.w_TIPDIS='F', '4', IIF(.w_TRIPRE='4','1',ALLTRIM(STR(VAL(.w_TRIPRE)+1))))
        .w_ANPREV = IIF(.w_TIPDIS='P', IIF(.w_TRIPRE='4', ALLTRIM(STR(VAL(.w_ANNO)+1)), ALLTRIM(.w_ANNO)), IIF(NOT EMPTY(.w_ANNOF), ALLTRIM(STR(VAL(.w_ANNOF)+1)), SPACE(4)))
          .DoRTCalc(18,18,.f.)
        .w_NOMEFILE = iif(.w_TRIRIF='4' ,ALLTRIM(STR(VAL(.w_ANPREV)+1)), ALLTRIM(.w_ANPREV))+IIF(.w_TIPDIS='F', .w_NUMTR5, IIF(.w_TRIRIF='1',.w_NUMTR1,IIF(.w_TRIRIF='2',.w_NUMTR2,IIF(.w_TRIRIF='3',.w_NUMTR3,IIF(.w_TRIRIF='4',.w_NUMTR4,'')))))
        .w_DIRGEN = IIF(NOT EMPTY(.w_PATH), ALLTRIM(.w_PATH)+ALLTRIM(.w_NOMEFILE), ALLTRIM(.w_DIR)+ALLTRIM(.w_NOMEFILE))
        .w_SEPREC = 'S'
        .w_DATINI = IIF(.w_TIPDIS='F', cp_CharToDate( '01-01' + '-' + .w_ANPREV ), cp_CharToDate( '01-' + STR((( VAL( .w_TRIRIF ) - 1) * 3) + 1) + '-' + .w_ANPREV))
        .w_DATFIN = IIF(.w_TRIRIF='4', cp_CharToDate('31-12-'+' ' + IIF(.w_TIPDIS='F', .w_ANPREV,.w_ANPREV)), cp_CharToDate('01-'+ STR(((VAL(.w_TRIRIF))*3)+1)+ '-' + IIF(.w_TIPDIS='F', .w_ANPREV, .w_ANPREV)) -1)
        .w_PROVEN = IIF(.w_GENFIR='S', .w_TIPDIS, IIF(.w_TRIRIF='4','','P'))
        .w_DATA = cp_CharToDate('01-'+STR(((VAL(.w_TRIRIF)-1)*3)+1)+ '-' + IIF(.w_TIPDIS='F', .w_ANFIRR,.w_ANPREV))
        .w_DATEFIR = IIF(.w_TIPDIS1='R', cp_CharToDate( '01-01' + '-' + .w_ANPREV ),'')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,15,.t.)
        if .o_TIPDIS<>.w_TIPDIS
            .w_TRIRIF = IIF(.w_TIPDIS='F', '4', IIF(.w_TRIPRE='4','1',ALLTRIM(STR(VAL(.w_TRIPRE)+1))))
        endif
        if .o_TIPDIS<>.w_TIPDIS
            .w_ANPREV = IIF(.w_TIPDIS='P', IIF(.w_TRIPRE='4', ALLTRIM(STR(VAL(.w_ANNO)+1)), ALLTRIM(.w_ANNO)), IIF(NOT EMPTY(.w_ANNOF), ALLTRIM(STR(VAL(.w_ANNOF)+1)), SPACE(4)))
        endif
        .DoRTCalc(18,18,.t.)
        Local l_Dep1,l_Dep2
        l_Dep1= .o_TRIRIF<>.w_TRIRIF .or. .o_ANFIRR<>.w_ANFIRR .or. .o_ANPREV<>.w_ANPREV .or. .o_TIPDIS<>.w_TIPDIS .or. .o_NUMTR1<>.w_NUMTR1
        l_Dep2= .o_NUMTR2<>.w_NUMTR2 .or. .o_NUMTR3<>.w_NUMTR3 .or. .o_NUMTR4<>.w_NUMTR4 .or. .o_NUMTR5<>.w_NUMTR5 .or. .o_ANNOF<>.w_ANNOF
        if m.l_Dep1 .or. m.l_Dep2
            .w_NOMEFILE = iif(.w_TRIRIF='4' ,ALLTRIM(STR(VAL(.w_ANPREV)+1)), ALLTRIM(.w_ANPREV))+IIF(.w_TIPDIS='F', .w_NUMTR5, IIF(.w_TRIRIF='1',.w_NUMTR1,IIF(.w_TRIRIF='2',.w_NUMTR2,IIF(.w_TRIRIF='3',.w_NUMTR3,IIF(.w_TRIRIF='4',.w_NUMTR4,'')))))
        endif
        if .o_NOMEFILE<>.w_NOMEFILE.or. .o_DIR<>.w_DIR.or. .o_PATH<>.w_PATH
            .w_DIRGEN = IIF(NOT EMPTY(.w_PATH), ALLTRIM(.w_PATH)+ALLTRIM(.w_NOMEFILE), ALLTRIM(.w_DIR)+ALLTRIM(.w_NOMEFILE))
        endif
        .DoRTCalc(21,21,.t.)
        if .o_ANPREV<>.w_ANPREV.or. .o_TRIRIF<>.w_TRIRIF.or. .o_TIPDIS<>.w_TIPDIS
            .w_DATINI = IIF(.w_TIPDIS='F', cp_CharToDate( '01-01' + '-' + .w_ANPREV ), cp_CharToDate( '01-' + STR((( VAL( .w_TRIRIF ) - 1) * 3) + 1) + '-' + .w_ANPREV))
        endif
        if .o_TRIRIF<>.w_TRIRIF.or. .o_TIPDIS<>.w_TIPDIS.or. .o_ANPREV<>.w_ANPREV
            .w_DATFIN = IIF(.w_TRIRIF='4', cp_CharToDate('31-12-'+' ' + IIF(.w_TIPDIS='F', .w_ANPREV,.w_ANPREV)), cp_CharToDate('01-'+ STR(((VAL(.w_TRIRIF))*3)+1)+ '-' + IIF(.w_TIPDIS='F', .w_ANPREV, .w_ANPREV)) -1)
        endif
            .w_PROVEN = IIF(.w_GENFIR='S', .w_TIPDIS, IIF(.w_TRIRIF='4','','P'))
            .w_DATA = cp_CharToDate('01-'+STR(((VAL(.w_TRIRIF)-1)*3)+1)+ '-' + IIF(.w_TIPDIS='F', .w_ANFIRR,.w_ANPREV))
        if .o_ANPREV<>.w_ANPREV
            .w_DATEFIR = IIF(.w_TIPDIS1='R', cp_CharToDate( '01-01' + '-' + .w_ANPREV ),'')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTRIRIF_1_16.enabled = this.oPgFrm.Page1.oPag.oTRIRIF_1_16.mCond()
    this.oPgFrm.Page1.oPag.oANPREV_1_17.enabled = this.oPgFrm.Page1.oPag.oANPREV_1_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPDIS_1_11.visible=!this.oPgFrm.Page1.oPag.oTIPDIS_1_11.mHide()
    this.oPgFrm.Page1.oPag.oTIPDIS1_1_12.visible=!this.oPgFrm.Page1.oPag.oTIPDIS1_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROV_IDX,3]
    i_lTable = "PAR_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2], .t., this.PAR_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODAZI,PPANPREV,PPTRIPRE,PPANFIRR,PPDIRGEN,PPNUMTR1,PPNUMTR2,PPNUMTR3,PPNUMTR4,PPNUMTR5,PPGENFIR";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODAZI',this.w_CODAZI)
            select PPCODAZI,PPANPREV,PPTRIPRE,PPANFIRR,PPDIRGEN,PPNUMTR1,PPNUMTR2,PPNUMTR3,PPNUMTR4,PPNUMTR5,PPGENFIR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PPCODAZI,space(5))
      this.w_ANNO = NVL(_Link_.PPANPREV,space(4))
      this.w_TRIPRE = NVL(_Link_.PPTRIPRE,space(1))
      this.w_ANNOF = NVL(_Link_.PPANFIRR,space(4))
      this.w_DIR = NVL(_Link_.PPDIRGEN,space(254))
      this.w_NUMTR1 = NVL(_Link_.PPNUMTR1,space(4))
      this.w_NUMTR2 = NVL(_Link_.PPNUMTR2,space(4))
      this.w_NUMTR3 = NVL(_Link_.PPNUMTR3,space(4))
      this.w_NUMTR4 = NVL(_Link_.PPNUMTR4,space(4))
      this.w_NUMTR5 = NVL(_Link_.PPNUMTR5,space(4))
      this.w_GENFIR = NVL(_Link_.PPGENFIR,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_ANNO = space(4)
      this.w_TRIPRE = space(1)
      this.w_ANNOF = space(4)
      this.w_DIR = space(254)
      this.w_NUMTR1 = space(4)
      this.w_NUMTR2 = space(4)
      this.w_NUMTR3 = space(4)
      this.w_NUMTR4 = space(4)
      this.w_NUMTR5 = space(4)
      this.w_GENFIR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2])+'\'+cp_ToStr(_Link_.PPCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPGEN_1_10.RadioValue()==this.w_TIPGEN)
      this.oPgFrm.Page1.oPag.oTIPGEN_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDIS_1_11.RadioValue()==this.w_TIPDIS)
      this.oPgFrm.Page1.oPag.oTIPDIS_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDIS1_1_12.RadioValue()==this.w_TIPDIS1)
      this.oPgFrm.Page1.oPag.oTIPDIS1_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIRIF_1_16.RadioValue()==this.w_TRIRIF)
      this.oPgFrm.Page1.oPag.oTRIRIF_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANPREV_1_17.value==this.w_ANPREV)
      this.oPgFrm.Page1.oPag.oANPREV_1_17.value=this.w_ANPREV
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEFILE_1_19.value==this.w_NOMEFILE)
      this.oPgFrm.Page1.oPag.oNOMEFILE_1_19.value=this.w_NOMEFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oDIRGEN_1_20.value==this.w_DIRGEN)
      this.oPgFrm.Page1.oPag.oDIRGEN_1_20.value=this.w_DIRGEN
    endif
    if not(this.oPgFrm.Page1.oPag.oSEPREC_1_21.RadioValue()==this.w_SEPREC)
      this.oPgFrm.Page1.oPag.oSEPREC_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_30.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_30.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_32.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_32.value=this.w_DATFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(val(.w_ANPREV)>=1996  and val(.w_ANPREV)<=2050)  and (.w_TIPGEN='E')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANPREV_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un anno compreso fra 1996 e 2050")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DIR = this.w_DIR
    this.o_NUMTR1 = this.w_NUMTR1
    this.o_NUMTR2 = this.w_NUMTR2
    this.o_NUMTR3 = this.w_NUMTR3
    this.o_NUMTR4 = this.w_NUMTR4
    this.o_NUMTR5 = this.w_NUMTR5
    this.o_TIPDIS = this.w_TIPDIS
    this.o_ANNOF = this.w_ANNOF
    this.o_ANFIRR = this.w_ANFIRR
    this.o_TRIRIF = this.w_TRIRIF
    this.o_ANPREV = this.w_ANPREV
    this.o_PATH = this.w_PATH
    this.o_NOMEFILE = this.w_NOMEFILE
    return

enddefine

* --- Define pages as container
define class tgsve_kgdPag1 as StdContainer
  Width  = 533
  height = 211
  stdWidth  = 533
  stdheight = 211
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPGEN_1_10 as StdCombo with uid="TTAOWECYWZ",rtseq=10,rtrep=.f.,left=147,top=14,width=99,height=21;
    , ToolTipText = "Tipo di generazione";
    , HelpContextID = 11672630;
    , cFormVar="w_TIPGEN",RowSource=""+"Simulata,"+"Definitiva,"+"Rigenera file", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPGEN_1_10.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oTIPGEN_1_10.GetRadio()
    this.Parent.oContained.w_TIPGEN = this.RadioValue()
    return .t.
  endfunc

  func oTIPGEN_1_10.SetRadio()
    this.Parent.oContained.w_TIPGEN=trim(this.Parent.oContained.w_TIPGEN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPGEN=='P',1,;
      iif(this.Parent.oContained.w_TIPGEN=='D',2,;
      iif(this.Parent.oContained.w_TIPGEN=='E',3,;
      0)))
  endfunc


  add object oTIPDIS_1_11 as StdCombo with uid="CYVAWKBCIL",rtseq=11,rtrep=.f.,left=401,top=14,width=107,height=21;
    , HelpContextID = 99556406;
    , cFormVar="w_TIPDIS",RowSource=""+"Prev./ass.,"+"FIRR", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDIS_1_11.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPDIS_1_11.GetRadio()
    this.Parent.oContained.w_TIPDIS = this.RadioValue()
    return .t.
  endfunc

  func oTIPDIS_1_11.SetRadio()
    this.Parent.oContained.w_TIPDIS=trim(this.Parent.oContained.w_TIPDIS)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDIS=='P',1,;
      iif(this.Parent.oContained.w_TIPDIS=='F',2,;
      0))
  endfunc

  func oTIPDIS_1_11.mHide()
    with this.Parent.oContained
      return (.w_GENFIR<>'S')
    endwith
  endfunc


  add object oTIPDIS1_1_12 as StdCombo with uid="KIHXIZOHTE",rtseq=12,rtrep=.f.,left=402,top=14,width=107,height=21, enabled=.f.;
    , ToolTipText = "Tipo distinta";
    , HelpContextID = 99556406;
    , cFormVar="w_TIPDIS1",RowSource=""+"Prev./ass.+FIRR", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDIS1_1_12.RadioValue()
    return(iif(this.value =1,'R',;
    space(1)))
  endfunc
  func oTIPDIS1_1_12.GetRadio()
    this.Parent.oContained.w_TIPDIS1 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDIS1_1_12.SetRadio()
    this.Parent.oContained.w_TIPDIS1=trim(this.Parent.oContained.w_TIPDIS1)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDIS1=='R',1,;
      0)
  endfunc

  func oTIPDIS1_1_12.mHide()
    with this.Parent.oContained
      return (.w_GENFIR='S')
    endwith
  endfunc


  add object oTRIRIF_1_16 as StdCombo with uid="GROMUVGOIW",rtseq=16,rtrep=.f.,left=147,top=43,width=99,height=21;
    , ToolTipText = "Trimestre di riferimento";
    , HelpContextID = 117656266;
    , cFormVar="w_TRIRIF",RowSource=""+"1� Trimestre,"+"2� Trimestre,"+"3� Trimestre,"+"4� Trimestre", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRIRIF_1_16.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    space(1))))))
  endfunc
  func oTRIRIF_1_16.GetRadio()
    this.Parent.oContained.w_TRIRIF = this.RadioValue()
    return .t.
  endfunc

  func oTRIRIF_1_16.SetRadio()
    this.Parent.oContained.w_TRIRIF=trim(this.Parent.oContained.w_TRIRIF)
    this.value = ;
      iif(this.Parent.oContained.w_TRIRIF=='1',1,;
      iif(this.Parent.oContained.w_TRIRIF=='2',2,;
      iif(this.Parent.oContained.w_TRIRIF=='3',3,;
      iif(this.Parent.oContained.w_TRIRIF=='4',4,;
      0))))
  endfunc

  func oTRIRIF_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGEN='E' AND .w_TIPDIS='P')
    endwith
   endif
  endfunc

  add object oANPREV_1_17 as StdField with uid="QVVCDWCDOP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ANPREV", cQueryName = "ANPREV",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 146612230,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=147, Top=72, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  func oANPREV_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGEN='E')
    endwith
   endif
  endfunc

  func oANPREV_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (val(.w_ANPREV)>=1996  and val(.w_ANPREV)<=2050)
    endwith
    return bRes
  endfunc

  add object oNOMEFILE_1_19 as StdField with uid="HCOSYOBUMO",rtseq=19,rtrep=.f.,;
    cFormVar = "w_NOMEFILE", cQueryName = "NOMEFILE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Nome del file",;
    HelpContextID = 71306725,;
   bGlobalFont=.t.,;
    Height=21, Width=89, Left=147, Top=101, InputMask=replicate('X',8)

  add object oDIRGEN_1_20 as StdField with uid="CJWNMGAIFQ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DIRGEN", cQueryName = "DIRGEN",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Directory di destinazione",;
    HelpContextID = 11680566,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=148, Top=130, InputMask=replicate('X',254), readonly=.t.


  add object oSEPREC_1_21 as StdCombo with uid="JBRPDLYBVK",rtseq=21,rtrep=.f.,left=148,top=160,width=130,height=21;
    , ToolTipText = "Caratteri utilizzati come separatori dei records";
    , HelpContextID = 172156890;
    , cFormVar="w_SEPREC",RowSource=""+"Cr + linefeed,"+"Carrige return", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSEPREC_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oSEPREC_1_21.GetRadio()
    this.Parent.oContained.w_SEPREC = this.RadioValue()
    return .t.
  endfunc

  func oSEPREC_1_21.SetRadio()
    this.Parent.oContained.w_SEPREC=trim(this.Parent.oContained.w_SEPREC)
    this.value = ;
      iif(this.Parent.oContained.w_SEPREC=='S',1,;
      iif(this.Parent.oContained.w_SEPREC=='C',2,;
      0))
  endfunc


  add object oBtn_1_28 as StdButton with uid="APIWEFNJEL",left=426, top=160, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 32096282;
    , Caption='\<OK';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        do GSVE_BGD with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="ZJIUAZYUNY",left=477, top=160, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 24808122;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDATINI_1_30 as StdField with uid="RKMNLVWXUJ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 62631114,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=316, Top=43

  add object oDATFIN_1_32 as StdField with uid="EKZKYHYSYU",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 15815478,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=439, Top=43


  add object oBtn_1_35 as StdButton with uid="DFZTGVXEZM",left=505, top=131, width=22,height=22,;
    caption="...", nPag=1;
    , HelpContextID = 31924522;
  , bGlobalFont=.t.

    proc oBtn_1_35.Click()
      with this.Parent.oContained
        .w_PATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(40),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_22 as StdString with uid="XXESYOYHMZ",Visible=.t., Left=44, Top=14,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo generazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="PHHKYDYBFC",Visible=.t., Left=326, Top=14,;
    Alignment=1, Width=72, Height=18,;
    Caption="Tipo distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="GRCVTEXFRA",Visible=.t., Left=33, Top=72,;
    Alignment=1, Width=111, Height=18,;
    Caption="Anno di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="TMKXXLZAJL",Visible=.t., Left=8, Top=43,;
    Alignment=1, Width=136, Height=18,;
    Caption="Trimestre di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="KEOHHSNEVQ",Visible=.t., Left=5, Top=130,;
    Alignment=1, Width=139, Height=18,;
    Caption="Directory di destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="SFQQTAKDNP",Visible=.t., Left=75, Top=101,;
    Alignment=1, Width=69, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="LKOAIPRYDV",Visible=.t., Left=254, Top=43,;
    Alignment=1, Width=61, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="MRUVTVWJLH",Visible=.t., Left=392, Top=43,;
    Alignment=1, Width=46, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="LZFVOQHZFL",Visible=.t., Left=23, Top=160,;
    Alignment=1, Width=122, Height=15,;
    Caption="Separatore records:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kgd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
