* ---------------------------------------------------------------------------- *
* #%&%#Build:0000
*                                                                              *
*   Procedure: TX_FACZU                                                        *
*              Fattura Accompagnatoria Zucche                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 31/5/01                                                         *
* Last revis.: 31/5/13                                                         *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
private i_formh13,i_formh14
private i_brk,i_quit,i_row,i_pag,i_oldarea,i_oldrows
private i_rdvars,i_rdkvars,i_rdkey
private w_s,i_rec,i_wait,i_modal
private i_usr_brk          && .T. se l'utente ha interrotto la stampa
private i_frm_rpr          && .T. se � stata stampata la testata del report
private Sm                 && Variabile di appoggio per formattazione memo
private i_form_ph,i_form_phh,i_form_phg,i_form_pf,i_saveh13,i_exec

* --- Variabili per configurazione stampante
private w_t_stdevi,w_t_stmsin,w_t_stnrig
private w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
private w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
private w_t_stpica,w_t_stelit
private w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
private w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
private w_stdesc
store " " to w_t_stdevi
store " " to w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
store " " to w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
store " " to w_t_stpica,w_t_stelit, i_rdkey
store " " to w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
store " " to w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
store ""  to Sm
store 0 to i_formh13,i_formh14,i_form_phh,i_form_phg,i_saveh13
store space(20) to w_stdesc
i_form_ph = 12
i_form_pf = 13
i_wait = 1
i_modal = .t.
i_oldrows = 0
store .F. to i_usr_brk, i_frm_rpr, w_s
i_exec = ""

dimension i_rdvars[41,2],i_rdkvars[6,2],i_zoompars[3]
store 0 to i_rdvars[1,1],i_rdkvars[1,1]
private w_StComp,w_NOMDES,w_INDI,w_DATITEST2,w_CNAZ
private w_IVAFIS,w_NUMDOC,w_DATDOC,w_DESCRI,w_INDIRI
private w_DATITEST,w_DESSPE,w_CAUTRA,w_RATPAG,w_TIPPAG
private w_CNAZ,w_IVAFIS,w_IVAFIS,w_CODPAG,w_SERIAL
private CONAI,w_StComp,w_StComp,w_NOMDES,w_INDI
private w_DATITEST2,w_CNAZ,w_IVAFIS,w_NUMDOC,w_DATDOC
private w_DESCRI,w_INDIRI,w_DATITEST,w_DESSPE,w_CAUTRA
private w_RATPAG,w_TIPPAG,w_CNAZ,w_IVAFIS,w_IVAFIS
private w_CODPAG,w_SERIAL,w_StComp,w_CODICE,w_MVDESART
private pMVVALRIG,w_SCRDESSUP,w_SCRDESSUP,w_PREZZO,SCONTO1
private SCONT2,w_VALRIG,w_IVA,SCONT3,SCONT4
private w_DESAR2,IMP,w_TXT1,w_TXT2,w_IMPIVA
private IMPOSTA,w_IMPIVA,TOTALEA,P_CAMBIO,FLIVDF
private w_StComp,w_SETBOL,w_ESIGIB,w_SETBOL,w_StComp
private w_ARTCONAI1,w_IMPCONAI1,w_ARTCONAI2,w_IMPCONAI2,w_ARTCONAI3
private w_IMPCONAI3,w_IMPCONAI4,w_ARTCONAI4,w_IMPCONAI5,w_ARTCONAI5
private w_IMPCONAI6,w_ARTCONAI6,w_IMPCONAITOT,w_SUMOK1,TOTALEDOC
private IMPIV1,CORRIS,L_DESDIC,L_NOTE,w_VETDAT2
private w_PROVET2,w_VETDAT1,w_PROVE1,NONCORRIS,SETRIG
private univanaz,VALSIM,VALSIM,w_TOTALE,w_SUMOK
private w_SCONTI,w_SPEIMB,w_MVAIMPN1,w_AIMPS1,w_AIMPS21
private w_AIMPN21,w_MVAIMPN2,w_AIMPS2,w_AIMPS22,w_AIMPN22
private w_MVAIMPN3,w_AIMPS3,w_AIMPS23,w_AIMPN23,w_MVSPETRA
private w_SPEIMB,w_SPEBOLL,w_MVAIMPN4,w_AIMPS4,w_AIMPS24
private w_AIMPN24,w_MVAIMPN5,w_AIMPS5,w_AIMPS25,w_AIMPN25
private w_MVAIMPN6,w_AIMPS6,w_AIMPS26,w_AIMPN26,w_ACCONT
private w_DOC_RA1,w_DOCRA1,w_DOCRAT1,w_IMPIV1,w_IMPOSTA
private w_TOTDOC,w_DOC_RA2,w_DOCRA2,w_DOCRAT2,w_DOC_RA3
private w_DOCRA3,w_DOCRAT3,w_MVSPEINC,w_DOC_RA4,w_DOCRA4
private w_DOCRAT4,w_DOC_RA5,w_DOCRA5,w_DOCRAT5,w_DOC_RA6
private w_DOCRA6,w_DOCRAT6,TOTALE,w_DESBAN,w_DESBAN1
private w_MVQTACOL,w_QTAPES,w_QTALOR,w_DESPOR,w_DATTRA
private w_DESVET,w_VETDAT,TOTALE,CONAI
w_StComp = space(2)
w_NOMDES = space(35)
w_INDI = space(35)
w_DATITEST2 = space(35)
w_CNAZ = space(4)
w_IVAFIS = space(20)
w_NUMDOC = space(10)
w_DATDOC = ctod("  /  /  ")
w_DESCRI = space(35)
w_INDIRI = space(35)
w_DATITEST = space(35)
w_DESSPE = space(20)
w_CAUTRA = space(30)
w_RATPAG = space(10)
w_TIPPAG = space(2)
w_CNAZ = space(4)
w_IVAFIS = space(20)
w_IVAFIS = space(20)
w_CODPAG = space(30)
w_SERIAL = space(10)
CONAI = 0
w_StComp = space(2)
w_StComp = space(2)
w_NOMDES = space(35)
w_INDI = space(35)
w_DATITEST2 = space(35)
w_CNAZ = space(4)
w_IVAFIS = space(20)
w_NUMDOC = space(10)
w_DATDOC = ctod("  /  /  ")
w_DESCRI = space(35)
w_INDIRI = space(35)
w_DATITEST = space(35)
w_DESSPE = space(20)
w_CAUTRA = space(30)
w_RATPAG = space(10)
w_TIPPAG = space(2)
w_CNAZ = space(4)
w_IVAFIS = space(20)
w_IVAFIS = space(20)
w_CODPAG = space(30)
w_SERIAL = space(10)
w_StComp = space(2)
w_CODICE = space(17)
w_MVDESART = space(40)
pMVVALRIG = 0
w_SCRDESSUP = .f.
w_SCRDESSUP = .f.
w_PREZZO = 0
SCONTO1 = 0
SCONT2 = 0
w_VALRIG = space(16)
w_IVA = space(5)
SCONT3 = 0
SCONT4 = 0
w_DESAR2 = space(0)
IMP = 0
w_TXT1 = space(0)
w_TXT2 = space(0)
w_IMPIVA = 0
IMPOSTA = 0
w_IMPIVA = 0
TOTALEA = 0
P_CAMBIO = 0
FLIVDF = space(1)
w_StComp = space(2)
w_SETBOL = space(1)
w_ESIGIB = space(45)
w_SETBOL = space(1)
w_StComp = space(2)
w_ARTCONAI1 = space(0)
w_IMPCONAI1 = 0
w_ARTCONAI2 = space(0)
w_IMPCONAI2 = 0
w_ARTCONAI3 = space(0)
w_IMPCONAI3 = 0
w_IMPCONAI4 = 0
w_ARTCONAI4 = space(0)
w_IMPCONAI5 = 0
w_ARTCONAI5 = space(0)
w_IMPCONAI6 = 0
w_ARTCONAI6 = space(0)
w_IMPCONAITOT = 0
w_SUMOK1 = 0
TOTALEDOC = 0
IMPIV1 = 0
CORRIS = 0
L_DESDIC = space(40)
L_NOTE = space(40)
w_VETDAT2 = space(30)
w_PROVET2 = space(30)
w_VETDAT1 = space(30)
w_PROVE1 = space(30)
NONCORRIS = 0
SETRIG = 0
univanaz = space(10)
VALSIM = space(5)
VALSIM = space(5)
w_TOTALE = space(18)
w_SUMOK = 0
w_SCONTI = space(13)
w_SPEIMB = space(13)
w_MVAIMPN1 = 0
w_AIMPS1 = 0
w_AIMPS21 = 0
w_AIMPN21 = 0
w_MVAIMPN2 = 0
w_AIMPS2 = 0
w_AIMPS22 = 0
w_AIMPN22 = 0
w_MVAIMPN3 = 0
w_AIMPS3 = 0
w_AIMPS23 = 0
w_AIMPN23 = 0
w_MVSPETRA = space(13)
w_SPEIMB = space(13)
w_SPEBOLL = 0
w_MVAIMPN4 = 0
w_AIMPS4 = 0
w_AIMPS24 = 0
w_AIMPN24 = 0
w_MVAIMPN5 = 0
w_AIMPS5 = 0
w_AIMPS25 = 0
w_AIMPN25 = 0
w_MVAIMPN6 = 0
w_AIMPS6 = 0
w_AIMPS26 = 0
w_AIMPN26 = 0
w_ACCONT = space(35)
w_DOC_RA1 = space(13)
w_DOCRA1 = space(10)
w_DOCRAT1 = space(10)
w_IMPIV1 = space(20)
w_IMPOSTA = 0
w_TOTDOC = 0
w_DOC_RA2 = space(13)
w_DOCRA2 = space(10)
w_DOCRAT2 = space(10)
w_DOC_RA3 = space(13)
w_DOCRA3 = space(10)
w_DOCRAT3 = space(10)
w_MVSPEINC = space(13)
w_DOC_RA4 = space(13)
w_DOCRA4 = space(10)
w_DOCRAT4 = space(10)
w_DOC_RA5 = space(13)
w_DOCRA5 = space(10)
w_DOCRAT5 = space(10)
w_DOC_RA6 = space(13)
w_DOCRA6 = space(10)
w_DOCRAT6 = space(10)
TOTALE = 0
w_DESBAN = space(10)
w_DESBAN1 = space(10)
w_MVQTACOL = space(5)
w_QTAPES = space(8)
w_QTALOR = space(8)
w_DESPOR = space(20)
w_DATTRA = space(20)
w_DESVET = space(40)
w_VETDAT = space(40)
TOTALE = 0
CONAI = 0

i_oldarea = select()
select __tmp__
go top

w_t_stnrig = 65
w_t_stmsin = 0
  
  
* --- Inizializza Variabili per configurazione stampante da CP_CHPRN
w_t_stdevi = cFileStampa+'.prn'
w_t_stlung = ts_ForPag
w_t_stnrig = ts_RowOk 
w_t_strese = ts_Reset+ts_Inizia
w_t_st10 = ts_10Cpi
w_t_st12 = ts_12Cpi
w_t_st15 = ts_15Cpi
w_t_stcomp = ts_Comp
w_t_stnorm = ts_RtComp
w_t_stbold = ts_StBold
w_t_stwide = ts_StDoub
w_t_stital = ts_StItal
w_t_stunde = ts_StUnde
w_t_stbol_ = ts_FiBold
w_t_stwid_ = ts_FiDoub
w_t_stita_ = ts_FiItal
w_t_stund_ = ts_FiUnde
* --- non definiti
*w_t_stmsin
*w_t_stnlq
*w_t_stdraf
*w_t_stpica
*w_t_stelit

i_row = 0
i_pag = 1
*---------------------------------------
wait wind "Generazione file appoggio..." nowait
*----------------------------------------
activate screen
* --- Settaggio stampante
set printer to &w_t_stdevi
set device to printer
set margin to w_t_stmsin
if len(trim(w_t_strese))>0
  @ 0,0 say &w_t_strese
endif
if len(trim(w_t_stlung))>0
  @ 0,0 say &w_t_stlung
endif
* --- Inizio stampa
do TX_F4CZU with 1, 0
if i_frm_rpr .and. .not. i_usr_brk
  * stampa il piede del report
  do TX_F4CZU with 14, 0
endif
if i_row<>0 
  @ prow(),pcol() say chr(12)
endif
set device to screen
set printer off
set printer to
if .not. i_frm_rpr
  do cplu_erm with "Non ci sono dati da stampare"
endif
* --- Fine
if .not.(empty(wontop()))
  activate  window (wontop())
endif
i_warea = alltrim(str(i_oldarea))
select (i_oldarea)
return


procedure TX_F4CZU
* === Procedure TX_F4CZU
parameters i_form_id, i_height

private i_currec, i_prevrec, i_formh
private i_frm_brk    && flag che indica il verificarsi di un break interform
                     && anche se la specifica condizione non � soddisfatta
private i_break, i_cond1, i_cond2

do case
  case i_form_id=1
    select __tmp__
    i_warea = '__tmp__'
    * --- inizializza le condizioni dei break interform
    i_cond1 = (MVSERIAL)
    i_cond2 = (MVSERIAL)
    i_frm_brk = .T.
    do while .not. eof() 
     wait wind "Elabora riga:"+str(recno()) +"/"+str(reccount()) nowait
      if .not. i_frm_rpr
        * --- stampa l'intestazione del report
        do TX_F4CZU with 11, 0
        i_frm_rpr = .T.
      endif
      if i_cond1<>(MVSERIAL) .or. i_frm_brk
        i_cond1 = (MVSERIAL)
        i_frm_brk = .T.
        do TX_F5CZU with 1.00, 22
      endif
      if i_cond2<>(MVSERIAL) .or. i_frm_brk
        i_cond2 = (MVSERIAL)
        do TX_F4CZU with 98
        i_form_ph = 1.01
        i_form_phh = 22
        i_form_phg = 422
      endif
      i_frm_brk = .F.
      * stampa del dettaglio
        do TX_F5CZU with 1.02, 0
      if (CTRL_A() or CTRL_D()) and not empty(nvl(mvcodice,' ')) AND CTRL_COAC()
        do TX_F5CZU with 1.03, 1
      endif
      if Ctrl2() AND CTRL_COAC()
        do TX_F5CZU with 1.04, 1
      endif
      if Ctrl1() AND Fscrdessup() AND NOT EMPTY(ALLTRIM(NVL(mvdessup,' '))) AND g_PERSDA='S' AND CTRL_COAC()
        do TX_F5CZU with 1.05, 1
      endif
      if .not. empty(L_DESDIC)  AND ultrig='S'
        do TX_F5CZU with 1.06, 1
      endif
      if ultrig='S' AND not empty(nvl(L_NOTE,''))
        do TX_F5CZU with 1.07, 1
      endif
      if ultrig='S' AND CONAI<>0
        do TX_F5CZU with 1.08, 1
      endif
      if ultrig='S'
        do TX_F5CZU with 1.09, 1
      endif
      if NVL(ANFLESIG,' ')='S' AND ULTRIG='S'
        do TX_F5CZU with 1.10, 1
      endif
      if g_coac='S' and ultrig='S' and not empty(acont_acc(1,1))
        do TX_F5CZU with 1.11, 2
      endif
      if g_coac='S' and ultrig='S' and not empty(acont_acc(2,1))
        do TX_F5CZU with 1.12, 1
      endif
      if g_coac='S' and ultrig='S' and not empty(acont_acc(3,1))
        do TX_F5CZU with 1.13, 1
      endif
      if g_coac='S' and ultrig='S' and not empty(acont_acc(4,1))
        do TX_F5CZU with 1.14, 1
      endif
      if g_coac='S' and ultrig='S' and not empty(acont_acc(5,1))
        do TX_F5CZU with 1.15, 1
      endif
      if g_coac='S' and ultrig='S' and not empty(acont_acc(6,1))
        do TX_F5CZU with 1.16, 1
      endif
      if g_coac='S' and ultrig='S' and not empty(acont_acc(1,1))
        do TX_F5CZU with 1.17, 1
      endif
      if i_usr_brk
        exit
      endif
      * --- passa al record successivo
      i_prevrec = recno()
      if .not. eof()
        skip
      endif
      i_currec = iif(eof(), -1, recno())
      if eof()  .or. i_cond2<>(MVSERIAL) .or. i_cond1<>(MVSERIAL)      
        i_form_ph = 12
        i_form_pf = 13
        i_formh13 = i_saveh13
      endif
      if eof()  .or. i_cond1<>(MVSERIAL)      
        go i_prevrec
        do TX_F5CZU with 1.19, 24
        do cplu_go with i_currec
      endif
    enddo
  case i_form_id=99
    * --- controllo per il salto pagina
    if inkey()=27
      i_usr_brk = .T.
    else
      if i_row+i_height+i_formh13>w_t_stnrig
        * --- stampa il piede di pagina
        i_row = w_t_stnrig-i_formh13
        if i_form_pf=13
          do TX_F4CZU with 13, 0
        else
          do TX_F5CZU with -i_form_pf,i_formh13
        endif
        i_row = 0
        i_pag = i_pag+1
        @ prow(),pcol() say chr(12)+chr(13)
        w_s = 0
        * --- stampa l'intestazione di pagina
        if i_form_ph=12
          do TX_F4CZU with 12, 0
        else
          do TX_F5CZU with -i_form_ph,i_form_phh
        endif
      endif
    endif
  case i_form_id=98
    i_form_pf = 1.18
    i_saveh13 = i_formh13
    i_formh13 = 24
endcase
return

procedure TX_F5CZU
* === Procedure TX_F5CZU
parameters i_form_id, i_form_h

* --- controllo per il salto pagina
if i_form_id<11 .and. i_form_id>0
  do TX_F4CZU with 99, i_form_h
  if i_usr_brk
    return
  endif
endif
if i_form_id<0
  i_form_id = -i_form_id
endif
do case
  * --- 1� form
  case i_form_id=1.0
    do frm1_0
  case i_form_id=1.01
    do frm1_01
  case i_form_id=1.02
    do frm1_02
  case i_form_id=1.03
    do frm1_03
  case i_form_id=1.04
    do frm1_04
  case i_form_id=1.05
    do frm1_05
  case i_form_id=1.06
    do frm1_06
  case i_form_id=1.07
    do frm1_07
  case i_form_id=1.08
    do frm1_08
  case i_form_id=1.09
    do frm1_09
  case i_form_id=1.10
    do frm1_10
  case i_form_id=1.11
    do frm1_11
  case i_form_id=1.12
    do frm1_12
  case i_form_id=1.13
    do frm1_13
  case i_form_id=1.14
    do frm1_14
  case i_form_id=1.15
    do frm1_15
  case i_form_id=1.16
    do frm1_16
  case i_form_id=1.17
    do frm1_17
  case i_form_id=1.18
    do frm1_18
  case i_form_id=1.19
    do frm1_19
endcase
i_row = i_row+i_form_h
return


* --- 1� form
procedure frm1_0
  w_StComp = ' '
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(105-0),37,at_x(300),transform(w_StComp,""),i_fn
  if NVL(ddtiprif,'')<>'FA'
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(121-0),40,at_x(324),transform(ANCODICE,""),i_fn
  endif
  w_NOMDES = IIF(NVL(ddtiprif,'')='FA',ddnomdes,andescri)
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(140-0),40,at_x(324),transform(w_NOMDES,""),i_fn
  w_INDI = IIF(NVL(ddtiprif,'')='FA',ddindiri,anindiri)
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(159-0),40,at_x(324),transform(w_INDI,""),i_fn
  w_DATITEST2 = left(Fdatitest2(),35)
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(178-0),40,at_x(324),transform(w_DATITEST2,""),i_fn
  w_CNAZ = LTRIM(nvl(looktab('nazioni','nacodiso','nacodnaz',annazion),''))
  if !empty(nvl(anpariva,'')) and NVL(ddtiprif,'')<>'FA'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(197-0),40,at_x(324),transform(w_CNAZ,""),i_fn
  endif
  w_IVAFIS = ltrim(iif(empty(anpariva),ancodfis,anpariva))
  if NVL(ddtiprif,'')<>'FA'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(197-0),45,at_x(364),transform(w_IVAFIS,""),i_fn
  endif
  w_NUMDOC = Alltrim(str(mvnumdoc,15))+iif(empty(mvalfdoc),'','/'+Alltrim(mvalfdoc))
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(233-0),1,at_x(13),transform(w_NUMDOC,""),i_fn
  i_PAG=IIF(w_SERIAL<>MVSERIAL,1,i_PAG)
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(233-0),22,at_x(183),transform(i_pag,"9999"),i_fn
  w_DATDOC = CP_TODATE(MVDATDOC)
  i_fn = ""
  do F_Say with i_row+13,i_row+at_y(252-0),3,at_x(31),transform(w_DATDOC,""),i_fn
  w_DESCRI = IIF(empty(nvl(coddes,'')),ANDESCRI,NOMDES)
  i_fn = ""
  do F_Say with i_row+13,i_row+at_y(252-0),40,at_x(326),transform(w_DESCRI,""),i_fn
  w_INDIRI = IIF(empty(nvl(coddes,'')),ANINDIRI,INDIRI)
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(271-0),40,at_x(326),transform(w_INDIRI,""),i_fn
  w_DATITEST = left(Fdatitest(),35)
  i_fn = ""
  do F_Say with i_row+15,i_row+at_y(290-0),40,at_x(326),transform(w_DATITEST,""),i_fn
  w_DESSPE = left(translat(MVTIPCON,MVCODCON,'TRADSPED','lgdescri','LGCODSPE','lgcodlin','MODASPED', 'SPDESSPE','SPCODSPE',mvcodspe),20)
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(323-0),1,at_x(8),transform(w_DESSPE,""),i_fn
  w_CAUTRA = nvl(mvtcamag,'')+' '+translat(mvtipcon,mvcodcon,'tradcama','lgdescod','lgcodice','lgcodlin','cam_agaz','cmdescri','cmcodice')
  if (TDCATDOC='FA'  and TDFLACCO='S')
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(323-0),21,at_x(172),transform(w_CAUTRA,Repl('X',30)),i_fn
  endif
  w_RATPAG = NVL(LOOKTAB('DOC_RATE','RSMODPAG','RSSERIAL',MVSERIAL,'RSNUMRAT',1),' ')
  IF .F.
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(328-0),47,at_x(380),transform(w_RATPAG,""),i_fn
   ENDIF
  w_TIPPAG = NVL(LOOKTAB('MOD_PAGA','MPTIPPAG','MPCODICE',w_RATPAG),' ')
  IF .F.
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(328-0),54,at_x(435),transform(w_TIPPAG,""),i_fn
   ENDIF
  w_CNAZ = LTRIM(nvl(looktab('nazioni','nacodiso','nacodnaz',annazion),''))
  if !empty(nvl(anpariva,''))
  i_fn = ""
  do F_Say with i_row+19,i_row+at_y(361-0),1,at_x(8),transform(w_CNAZ,""),i_fn
  endif
  w_IVAFIS = ltrim(iif(empty(anpariva),ancodfis,anpariva))
  i_fn = ""
  do F_Say with i_row+19,i_row+at_y(361-0),5,at_x(40),transform(w_IVAFIS,""),i_fn
  w_IVAFIS = ltrim(ancodfis)
  i_fn = ""
  do F_Say with i_row+19,i_row+at_y(361-0),26,at_x(208),transform(w_IVAFIS,""),i_fn
  w_CODPAG = Caldespag()
  i_fn = ""
  do F_Say with i_row+20,i_row+at_y(384-0),0,at_x(7),transform(w_CODPAG,""),i_fn
   Ambdoc()
  w_SERIAL = MVSERIAL
  IF .F.
  i_fn = ""
  do F_Say with i_row+20,i_row+at_y(384-0),34,at_x(278),transform(w_SERIAL,""),i_fn
   ENDIF
  CONAI = 0
  IF .F.
  i_fn = ""
  do F_Say with i_row+20,i_row+at_y(384-0),41,at_x(332),transform(CONAI,""),i_fn
   ENDIF
  w_StComp = ' '
  i_fn = ""
  do F_Say with i_row+20,i_row+at_y(384-0),78,at_x(624),transform(w_StComp,""),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
return

* --- frm1_01
procedure frm1_01
  w_StComp = ' '
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(540-436),37,at_x(300),transform(w_StComp,""),i_fn
  if NVL(ddtiprif,'')<>'FA'
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(556-436),40,at_x(324),transform(ANCODICE,""),i_fn
  endif
  w_NOMDES = IIF(NVL(ddtiprif,'')='FA',ddnomdes,andescri)
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(575-436),40,at_x(324),transform(w_NOMDES,""),i_fn
  w_INDI = IIF(NVL(ddtiprif,'')='FA',ddindiri,anindiri)
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(594-436),40,at_x(324),transform(w_INDI,""),i_fn
  w_DATITEST2 = left(Fdatitest2(),35)
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(613-436),40,at_x(324),transform(w_DATITEST2,""),i_fn
  w_CNAZ = LTRIM(nvl(looktab('nazioni','nacodiso','nacodnaz',annazion),''))
  if !empty(nvl(anpariva,'')) and NVL(ddtiprif,'')<>'FA'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(632-436),40,at_x(324),transform(w_CNAZ,""),i_fn
  endif
  w_IVAFIS = ltrim(iif(empty(anpariva),ancodfis,anpariva))
  if NVL(ddtiprif,'')<>'FA'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(632-436),45,at_x(364),transform(w_IVAFIS,""),i_fn
  endif
  w_NUMDOC = Alltrim(str(mvnumdoc,15))+iif(empty(mvalfdoc),'','/'+Alltrim(mvalfdoc))
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(668-436),1,at_x(13),transform(w_NUMDOC,""),i_fn
  i_PAG=IIF(w_SERIAL<>MVSERIAL,1,i_PAG)
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(668-436),22,at_x(183),transform(i_pag,"9999"),i_fn
  w_DATDOC = CP_TODATE(MVDATDOC)
  i_fn = ""
  do F_Say with i_row+13,i_row+at_y(687-436),3,at_x(31),transform(w_DATDOC,""),i_fn
  w_DESCRI = IIF(empty(nvl(coddes,'')),ANDESCRI,NOMDES)
  i_fn = ""
  do F_Say with i_row+13,i_row+at_y(687-436),40,at_x(326),transform(w_DESCRI,""),i_fn
  w_INDIRI = IIF(empty(nvl(coddes,'')),ANINDIRI,INDIRI)
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(706-436),40,at_x(326),transform(w_INDIRI,""),i_fn
  w_DATITEST = left(Fdatitest(),35)
  i_fn = ""
  do F_Say with i_row+15,i_row+at_y(725-436),40,at_x(326),transform(w_DATITEST,""),i_fn
  w_DESSPE = left(translat(MVTIPCON,MVCODCON,'TRADSPED','lgdescri','LGCODSPE','lgcodlin','MODASPED', 'SPDESSPE','SPCODSPE',mvcodspe),20)
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(766-436),0,at_x(7),transform(w_DESSPE,""),i_fn
  w_CAUTRA = nvl(mvtcamag,'')+' '+translat(mvtipcon,mvcodcon,'tradcama','lgdescod','lgcodice','lgcodlin','cam_agaz','cmdescri','cmcodice')
  if (TDCATDOC='FA'  and TDFLACCO='S')
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(766-436),21,at_x(171),transform(w_CAUTRA,Repl('X',30)),i_fn
  endif
  w_RATPAG = NVL(LOOKTAB('DOC_RATE','RSMODPAG','RSSERIAL',MVSERIAL,'RSNUMRAT',1),' ')
  IF .F.
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(766-436),46,at_x(370),transform(w_RATPAG,""),i_fn
   ENDIF
  w_TIPPAG = NVL(LOOKTAB('MOD_PAGA','MPTIPPAG','MPCODICE',w_RATPAG),' ')
  IF .F.
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(766-436),53,at_x(425),transform(w_TIPPAG,""),i_fn
   ENDIF
  w_CNAZ = LTRIM(nvl(looktab('nazioni','nacodiso','nacodnaz',annazion),''))
  if !empty(nvl(anpariva,''))
  i_fn = ""
  do F_Say with i_row+19,i_row+at_y(798-436),1,at_x(8),transform(w_CNAZ,""),i_fn
  endif
  w_IVAFIS = ltrim(iif(empty(anpariva),ancodfis,anpariva))
  i_fn = ""
  do F_Say with i_row+19,i_row+at_y(798-436),5,at_x(40),transform(w_IVAFIS,""),i_fn
  w_IVAFIS = ltrim(ancodfis)
  i_fn = ""
  do F_Say with i_row+19,i_row+at_y(798-436),25,at_x(200),transform(w_IVAFIS,""),i_fn
  w_CODPAG = Caldespag()
  i_fn = ""
  do F_Say with i_row+20,i_row+at_y(819-436),0,at_x(7),transform(w_CODPAG,""),i_fn
   Ambdoc()
  w_SERIAL = MVSERIAL
  IF .F.
  i_fn = ""
  do F_Say with i_row+20,i_row+at_y(819-436),34,at_x(273),transform(w_SERIAL,""),i_fn
   ENDIF
  w_StComp = ' '
  i_fn = ""
  do F_Say with i_row+20,i_row+at_y(819-436),78,at_x(624),transform(w_StComp,""),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
return

* --- frm1_02
procedure frm1_02
return

* --- frm1_03
procedure frm1_03
  w_CODICE = iif(nvl(anflcodi,' ')='S',left(mvcodice,17),left(mvcodart,17))
  if nvl(arstacod,' ')<>'S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(897-896),0,at_x(5),transform(w_CODICE,""),i_fn
   CONAI=CONAI+iif(arflcona='S' or arflcon2='S',1,0)
  endif
  w_MVDESART = left(alltrim(MVDESART),40)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(897-896),18,at_x(148),transform(w_MVDESART,""),i_fn
  pMVVALRIG = IIF(MVFLOMAG='X',MVVALRIG,0)
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(897-896),54,at_x(439),transform(pMVVALRIG,""),i_fn
   ENDIF
   TOTALE = TOTALE+pMVVALRIG
  w_SCRDESSUP = .f.
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(897-896),57,at_x(459),transform(w_SCRDESSUP,""),i_fn
   endif
  w_SCRDESSUP = 'S'=nvl(looktab('ART_ICOL','ARSTASUP','ARCODART',MVCODART),'')
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(897-896),59,at_x(475),transform(w_SCRDESSUP,""),i_fn
   endif
  if FCODART()
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(897-896),62,at_x(502),transform(MVUNIMIS,Repl('X',3)),i_fn
  endif
  if mvtiprig<>'D' and fcodart()
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(897-896),67,at_x(537),transform(MVQTAMOV,'@Z '+v_PQ(11)),i_fn
  endif
  w_PREZZO = iif(mvrifkit<>0,'',TRAN(mvprezzo,'@Z '+V_PV[38+(20*VADECUNI)]))
  if mvtiprig<>'D' and FCODART()
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(897-896),78,at_x(628),transform(w_PREZZO,""),i_fn
  endif
  SCONTO1 = iif(mvrifkit<>0,0,mvscont1)
  if Ctrl3() .and. sconto1<>0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(897-896),96,at_x(775),transform(SCONTO1,'999.99'),i_fn
  endif
  SCONT2 = iif(mvrifkit<>0,0,mvscont2)
  if Ctrl4() .and. scont2<>0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(897-896),102,at_x(818),transform(SCONT2,'999.99'),i_fn
  endif
  w_VALRIG = right(space(16)+alltrim(FValrig()),16)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(897-896),111,at_x(892),transform(w_VALRIG,""),i_fn
  w_IVA = left(alltrim(FIva())+Space(5),5)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(897-896),129,at_x(1037),transform(w_IVA,Repl('X',5)),i_fn
return

* --- frm1_04
procedure frm1_04
  SCONT3 = iif(mvrifkit<>0,0,mvscont3)
  if scont3<>0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(935-934),96,at_x(774),transform(SCONT3,'999.99'),i_fn
  endif
  SCONT4 = iif(mvrifkit<>0,0,mvscont4)
  if scont4<>0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(935-934),102,at_x(818),transform(SCONT4,'999.99'),i_fn
  endif
return

* --- frm1_05
procedure frm1_05
  w_DESAR2 = MVDESSUP
  i_fn = ""
  w_s = 36
  set memowidth to w_s
  Sm = mline(w_DESAR2,1)+space(w_s-len(mline(w_DESAR2,1)))
  do F_Say with i_row+0,i_row+at_y(973-972),18,at_x(148),alltrim(Sm),i_fn
  i_ml = 0
  w_s = 36
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_DESAR2))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_F4CZU with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 36
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_DESAR2,i_i)+space(w_s-len(mline(w_DESAR2,i_i)))
    do F_Say with i_row,i_row,18,at_x(148),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_06
procedure frm1_06
  IMP = TRAN(L_IMPONI,'@Z '+V_PV[20*(VADECTOT+2)])
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1010-1009),3,at_x(27),transform(IMP,""),i_fn
   ENDIF
  w_TXT1 = L_DESDIC
  if .not. empty(L_DESDIC)  and ultrig='S'
  i_fn = ""
  w_s = 36
  set memowidth to w_s
  Sm = mline(w_TXT1,1)+space(w_s-len(mline(w_TXT1,1)))
  do F_Say with i_row+0,i_row+at_y(1010-1009),18,at_x(148),alltrim(Sm),i_fn
  endif
  i_ml = 0
  w_s = 36
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_TXT1))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_F4CZU with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 36
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_TXT1,i_i)+space(w_s-len(mline(w_TXT1,i_i)))
    do F_Say with i_row,i_row,18,at_x(148),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_07
procedure frm1_07
  w_TXT2 = L_NOTE
  if ultrig='S'
  i_fn = ""
  w_s = 36
  set memowidth to w_s
  Sm = mline(w_TXT2,1)+space(w_s-len(mline(w_TXT2,1)))
  do F_Say with i_row+0,i_row+at_y(1048-1047),18,at_x(148),alltrim(Sm),i_fn
  endif
  i_ml = 0
  w_s = 36
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_TXT2))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_F4CZU with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 36
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_TXT2,i_i)+space(w_s-len(mline(w_TXT2,i_i)))
    do F_Say with i_row,i_row,18,at_x(148),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_08
procedure frm1_08
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1086-1085),18,at_x(148),"Contributo CONAI assolto",i_fn
return

* --- frm1_09
procedure frm1_09
  w_IMPIVA = FImpiva()
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1124-1123),0,at_x(4),transform(w_IMPIVA,""),i_fn
   ENDIF
  IMPOSTA = FImposta()
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1124-1123),3,at_x(24),transform(IMPOSTA,""),i_fn
   endif
  w_IMPIVA = FImpiva()
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1124-1123),5,at_x(46),transform(w_IMPIVA,""),i_fn
   ENDIF
  TOTALEA = w_impiva+imposta+mvimparr
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1124-1123),8,at_x(66),transform(TOTALEA,""),i_fn
   ENDIF
  P_CAMBIO = L_CAMBIO
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1124-1123),11,at_x(91),transform(P_CAMBIO,""),i_fn
   endif
return

* --- frm1_10
procedure frm1_10
  FLIVDF = nvl(looktab('cau_cont','ccflivdf','cccodice',mvcaucon),'')
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1161-1160),2,at_x(21),transform(FLIVDF,""),i_fn
   ENDIF
  w_StComp = ' '
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1161-1160),9,at_x(72),transform(w_StComp,""),i_fn
  IF .F.
  if len(trim('&w_t_stbold'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stbold,""
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1161-1160),12,at_x(101),transform(w_SETBOL,""),i_fn
   ENDIF
  w_ESIGIB = IIF(FLIVDF='S','Fattura ad esigibilita'+"'"+' Differita','Fattura ad esigibilita'+"'"+' Immediata')
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1161-1160),18,at_x(150),transform(w_ESIGIB,""),i_fn
  IF .F.
  if len(trim('&w_t_stbol_'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stbol_,""
  endif
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1161-1160),65,at_x(520),transform(w_SETBOL,""),i_fn
   ENDIF
  w_StComp = ' '
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1161-1160),67,at_x(540),transform(w_StComp,""),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
return

* --- frm1_11
procedure frm1_11
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1199-1197),18,at_x(149),"RIEPILOGO CONTRIBUTI:",i_fn
  w_ARTCONAI1 = acont_acc(1,1)
  if g_coac='S' and ultrig='S' and not empty(acont_acc(1,1))
  i_fn = ""
  w_s = 36
  set memowidth to w_s
  Sm = mline(w_ARTCONAI1,1)+space(w_s-len(mline(w_ARTCONAI1,1)))
  do F_Say with i_row+1,i_row+at_y(1217-1197),18,at_x(149),alltrim(Sm),i_fn
  endif
  w_IMPCONAI1 = TRAN(acont_acc(1,2),'@Z '+V_PV[38+(20*VADECUNI)])
  if g_coac='S' and ultrig='S' and not empty(acont_acc(1,1))
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(1218-1197),78,at_x(628),transform(w_IMPCONAI1,""),i_fn
  endif
  i_ml = 0
  w_s = 36
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_ARTCONAI1))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_F4CZU with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 36
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_ARTCONAI1,i_i)+space(w_s-len(mline(w_ARTCONAI1,i_i)))
    do F_Say with i_row,i_row,18,at_x(149),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_12
procedure frm1_12
  w_ARTCONAI2 = acont_acc(2,1)
  if g_coac='S' and ultrig='S' and not empty(acont_acc(2,1))
  i_fn = ""
  w_s = 36
  set memowidth to w_s
  Sm = mline(w_ARTCONAI2,1)+space(w_s-len(mline(w_ARTCONAI2,1)))
  do F_Say with i_row+0,i_row+at_y(1256-1254),18,at_x(148),alltrim(Sm),i_fn
  endif
  w_IMPCONAI2 = TRAN(acont_acc(2,2),'@Z '+V_PV[38+(20*VADECUNI)])
  if g_coac='S' and ultrig='S' and not empty(acont_acc(2,1))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1256-1254),78,at_x(628),transform(w_IMPCONAI2,""),i_fn
  endif
  i_ml = 0
  w_s = 36
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_ARTCONAI2))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_F4CZU with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 36
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_ARTCONAI2,i_i)+space(w_s-len(mline(w_ARTCONAI2,i_i)))
    do F_Say with i_row,i_row,18,at_x(148),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_13
procedure frm1_13
  w_ARTCONAI3 = acont_acc(3,1)
  if g_coac='S' and ultrig='S' and not empty(acont_acc(3,1))
  i_fn = ""
  w_s = 36
  set memowidth to w_s
  Sm = mline(w_ARTCONAI3,1)+space(w_s-len(mline(w_ARTCONAI3,1)))
  do F_Say with i_row+0,i_row+at_y(1294-1292),18,at_x(148),alltrim(Sm),i_fn
  endif
  w_IMPCONAI3 = TRAN(acont_acc(3,2),'@Z '+V_PV[38+(20*VADECUNI)])
  if g_coac='S' and ultrig='S' and not empty(acont_acc(3,1))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1294-1292),78,at_x(628),transform(w_IMPCONAI3,""),i_fn
  endif
  i_ml = 0
  w_s = 36
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_ARTCONAI3))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_F4CZU with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 36
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_ARTCONAI3,i_i)+space(w_s-len(mline(w_ARTCONAI3,i_i)))
    do F_Say with i_row,i_row,18,at_x(148),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_14
procedure frm1_14
  w_IMPCONAI4 = TRAN(acont_acc(4,2),'@Z '+V_PV[38+(20*VADECUNI)])
  if g_coac='S' and ultrig='S' and not empty(acont_acc(4,1))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1332-1330),78,at_x(628),transform(w_IMPCONAI4,""),i_fn
  endif
  w_ARTCONAI4 = acont_acc(4,1)
  if g_coac='S' and ultrig='S' and not empty(acont_acc(4,1))
  i_fn = ""
  w_s = 36
  set memowidth to w_s
  Sm = mline(w_ARTCONAI4,1)+space(w_s-len(mline(w_ARTCONAI4,1)))
  do F_Say with i_row+0,i_row+at_y(1333-1330),18,at_x(147),alltrim(Sm),i_fn
  endif
  i_ml = 0
  w_s = 36
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_ARTCONAI4))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_F4CZU with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 36
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_ARTCONAI4,i_i)+space(w_s-len(mline(w_ARTCONAI4,i_i)))
    do F_Say with i_row,i_row,18,at_x(147),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_15
procedure frm1_15
  w_IMPCONAI5 = TRAN(acont_acc(5,2),'@Z '+V_PV[38+(20*VADECUNI)])
  if g_coac='S' and ultrig='S' and not empty(acont_acc(5,1))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1370-1368),78,at_x(628),transform(w_IMPCONAI5,""),i_fn
  endif
  w_ARTCONAI5 = acont_acc(5,1)
  if g_coac='S' and ultrig='S' and not empty(acont_acc(5,1))
  i_fn = ""
  w_s = 36
  set memowidth to w_s
  Sm = mline(w_ARTCONAI5,1)+space(w_s-len(mline(w_ARTCONAI5,1)))
  do F_Say with i_row+0,i_row+at_y(1371-1368),18,at_x(146),alltrim(Sm),i_fn
  endif
  i_ml = 0
  w_s = 36
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_ARTCONAI5))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_F4CZU with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 36
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_ARTCONAI5,i_i)+space(w_s-len(mline(w_ARTCONAI5,i_i)))
    do F_Say with i_row,i_row,18,at_x(146),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_16
procedure frm1_16
  w_IMPCONAI6 = TRAN(acont_acc(6,2),'@Z '+V_PV[38+(20*VADECUNI)])
  if g_coac='S' and ultrig='S' and not empty(acont_acc(6,1))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1408-1406),78,at_x(628),transform(w_IMPCONAI6,""),i_fn
  endif
  w_ARTCONAI6 = acont_acc(5,1)
  if g_coac='S' and ultrig='S' and not empty(acont_acc(5,1))
  i_fn = ""
  w_s = 36
  set memowidth to w_s
  Sm = mline(w_ARTCONAI6,1)+space(w_s-len(mline(w_ARTCONAI6,1)))
  do F_Say with i_row+0,i_row+at_y(1409-1406),18,at_x(147),alltrim(Sm),i_fn
  endif
  i_ml = 0
  w_s = 36
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_ARTCONAI6))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_F4CZU with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 36
    set memowidth to w_s
      i_fn = ""
    Sm = mline(w_ARTCONAI6,i_i)+space(w_s-len(mline(w_ARTCONAI6,i_i)))
    do F_Say with i_row,i_row,18,at_x(147),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_17
procedure frm1_17
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1444-1444),112,at_x(902),"SEGUE =======>",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1445-1444),18,at_x(149),"TOTALE CONTRIBUTI:",i_fn
  w_IMPCONAITOT = TRAN(acont_acc(1,2)+acont_acc(2,2)+acont_acc(3,2)+acont_acc(4,2)+acont_acc(5,2)+acont_acc(6,2),'@Z '+V_PV[38+(20*VADECUNI)])
  if g_coac='S' and ultrig='S' and not empty(acont_acc(1,1))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1446-1444),78,at_x(628),transform(w_IMPCONAITOT,""),i_fn
  endif
return

* --- frm1_18
procedure frm1_18
return

* --- frm1_19
procedure frm1_19
  w_SUMOK1 = ROUND(((((TOTALE-(-MVSCONTI))-TOTALE)/TOTALE)*100),2)
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),2,at_x(16),transform(w_SUMOK1,""),i_fn
   ENDIF
  TOTALEDOC = w_impiva+imposta+mvimparr
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),5,at_x(40),transform(TOTALEDOC,""),i_fn
   ENDIF
  IMPIV1 = nvl(mvaimpn1,0)+nvl(mvaimpn2,0)+nvl(mvaimpn3,0)+nvl(mvaimpn4,0)+nvl(mvaimpn5,0)+nvl(mvaimpn6,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),8,at_x(69),transform(IMPIV1,""),i_fn
   endif
  CORRIS = iif(MVACCONT=0,totaledoc,MVACCONT)
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),11,at_x(89),transform(CORRIS,""),i_fn
   ENDIF
  L_DESDIC = ' '
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),13,at_x(109),transform(L_DESDIC,""),i_fn
   ENDIF
  L_NOTE = ' '
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),16,at_x(129),transform(L_NOTE,""),i_fn
   ENDIF
  w_VETDAT2 = FVetdat2()
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),18,at_x(150),transform(w_VETDAT2,""),i_fn
   ENDIF
  w_PROVET2 = NVL(looktab('vettori','vtprovet','vtcodvet',mvcodve2),'')
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),21,at_x(171),transform(w_PROVET2,""),i_fn
   ENDIF
  w_VETDAT1 = FVetdat1()
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),23,at_x(191),transform(w_VETDAT1,""),i_fn
   ENDIF
  w_PROVE1 = NVL(looktab('vettori','vtprovet','vtcodvet',mvcodvet),'')
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),26,at_x(210),transform(w_PROVE1,""),i_fn
   ENDIF
  NONCORRIS = iif(MVACCONT=0,0,totaledoc-corris)
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),29,at_x(234),transform(NONCORRIS,""),i_fn
   ENDIF
  SETRIG = ' '
  I_ROW=46
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),32,at_x(258),transform(SETRIG,""),i_fn
  univanaz = getvalut(g_codlir, "vaunival")
  if .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(1957-1956),45,at_x(360),transform(univanaz,""),i_fn
   endif
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(1976-1956),98,at_x(785),transform(VASIMVAL,""),i_fn
  VALSIM = getvalut(univanaz, "vasimval")
  if mvcodval<>g_codeur
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(1976-1956),118,at_x(950),transform(VALSIM,""),i_fn
  endif
  VALSIM = getvalut(univanaz, "vasimval")
  if mvcodval<>g_codeur
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(1976-1956),139,at_x(1115),transform(VALSIM,""),i_fn
  endif
  w_TOTALE = TRAN(totale,'@Z '+V_PV[18*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(1995-1956),0,at_x(0),transform(w_TOTALE,""),i_fn
   w_TOTALE=0
  w_SUMOK = IIF(MVSCOCL2<>0 OR(MVSCOCL1<>0 AND MVSCOPAG<>0),w_SUMOK1,IIF(MVSCOCL1<>0,MVSCOCL1,IIF(NOT EMPTY(MVSCOPAG),MVSCOPAG,MVSCONTI)))
  if NOT EMPTY(MVSCONTI)
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(1995-1956),18,at_x(149),transform(w_SUMOK,'999.99'),i_fn
  endif
  w_SCONTI = TRAN(MVSCONTI,'@Z '+V_PV[33+(20*VADECTOT)])
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(1995-1956),26,at_x(210),transform(w_SCONTI,""),i_fn
  w_SPEIMB = TRAN(MVSPEIMB,'@Z '+V_PV[33+(20*VADECTOT)])
  if PIVAIMB<>0
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(1995-1956),39,at_x(319),transform(w_SPEIMB,""),i_fn
  endif
  w_MVAIMPN1 = TRAN(mvaimpn1,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(1995-1956),55,at_x(440),transform(w_MVAIMPN1,""),i_fn
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(1995-1956),75,at_x(605),transform(MVACIVA1,""),i_fn
  w_AIMPS1 = TRAN(mvaimps1,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(1995-1956),81,at_x(650),transform(w_AIMPS1,""),i_fn
  w_AIMPS21 = TRAN(round(mvaimps1/mvcaoval,2),'@Z '+V_PV[20*(G_PERPVL+2)])
  if mvcodval<>g_codeur AND NOT empty(iva(1,1))
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(1995-1956),101,at_x(815),transform(w_AIMPS21,""),i_fn
  endif
  w_AIMPN21 = TRAN(round(mvaimpn1/mvcaoval,2),'@Z '+V_PV[20*(G_PERPVL+2)])
  if mvcodval<>g_codeur AND NOT empty(mvaimpn1)
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(1995-1956),122,at_x(980),transform(w_AIMPN21,""),i_fn
  endif
  w_MVAIMPN2 = TRAN(mvaimpn2,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(2014-1956),55,at_x(440),transform(w_MVAIMPN2,""),i_fn
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(2014-1956),75,at_x(605),transform(MVACIVA2,""),i_fn
  w_AIMPS2 = TRAN(mvaimps2,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(2014-1956),81,at_x(650),transform(w_AIMPS2,""),i_fn
  w_AIMPS22 = TRAN(round(mvaimps2/mvcaoval,2),'@Z '+V_PV[20*(G_PERPVL+2)])
  if mvcodval<>g_codeur AND NOT empty(iva(2,1))
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(2014-1956),101,at_x(815),transform(w_AIMPS22,""),i_fn
  endif
  w_AIMPN22 = TRAN(round(mvaimpn2/mvcaoval,2),'@Z '+V_PV[20*(G_PERPVL+2)])
  if mvcodval<>g_codeur  AND NOT empty(mvaimpn2)
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(2014-1956),122,at_x(980),transform(w_AIMPN22,""),i_fn
  endif
  w_MVAIMPN3 = TRAN(mvaimpn3,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(2033-1956),55,at_x(440),transform(w_MVAIMPN3,""),i_fn
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(2033-1956),75,at_x(605),transform(MVACIVA3,""),i_fn
  w_AIMPS3 = TRAN(mvaimps3,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(2033-1956),81,at_x(650),transform(w_AIMPS3,""),i_fn
  w_AIMPS23 = TRAN(round(mvaimps3/mvcaoval,2),'@Z '+V_PV[20*(G_PERPVL+2)])
  if mvcodval<>g_codeur
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(2033-1956),101,at_x(815),transform(w_AIMPS23,""),i_fn
  endif
  w_AIMPN23 = TRAN(round(mvaimpN3/mvcaoval,2),'@Z '+V_PV[20*(G_PERPVL+2)])
  if mvcodval<>g_codeur AND  NOT empty(mvaimpn3)
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(2033-1956),122,at_x(980),transform(w_AIMPN23,""),i_fn
  endif
  w_MVSPETRA = TRAN(mvspetra,'@Z '+V_PV[33+(20*VADECTOT)])
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(2052-1956),0,at_x(2),transform(w_MVSPETRA,""),i_fn
  w_SPEIMB = TRAN(MVSPEIMB,'@Z '+V_PV[33+(20*VADECTOT)])
  if PIVAIMB=0
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(2052-1956),15,at_x(127),transform(w_SPEIMB,""),i_fn
  endif
  w_SPEBOLL = TRAN(mvspebol,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(2052-1956),29,at_x(236),transform(w_SPEBOLL,""),i_fn
  w_MVAIMPN4 = TRAN(mvaimpn4,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(2052-1956),55,at_x(440),transform(w_MVAIMPN4,""),i_fn
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(2052-1956),75,at_x(605),transform(MVACIVA4,""),i_fn
  w_AIMPS4 = TRAN(mvaimps4,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(2052-1956),81,at_x(650),transform(w_AIMPS4,""),i_fn
  w_AIMPS24 = TRAN(round(mvaimps4/mvcaoval,2),'@Z '+V_PV[20*(G_PERPVL+2)])
  if mvcodval<>g_codeur AND NOT empty(iva(4,1))
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(2052-1956),101,at_x(815),transform(w_AIMPS24,""),i_fn
  endif
  w_AIMPN24 = TRAN(round(mvaimpn4/mvcaoval,2),'@Z '+V_PV[20*(G_PERPVL+2)])
  if mvcodval<>g_codeur AND  NOT empty(mvaimpn4)
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(2052-1956),122,at_x(980),transform(w_AIMPN24,""),i_fn
  endif
  w_MVAIMPN5 = TRAN(mvaimpn5,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(2071-1956),55,at_x(440),transform(w_MVAIMPN5,""),i_fn
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(2071-1956),75,at_x(605),transform(MVACIVA5,""),i_fn
  w_AIMPS5 = TRAN(mvaimps5,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(2071-1956),81,at_x(650),transform(w_AIMPS5,""),i_fn
  w_AIMPS25 = TRAN(round(mvaimps5/mvcaoval,2),'@Z '+V_PV[20*(G_PERPVL+2)])
  if mvcodval<>g_codeur AND NOT empty(iva(5,1))
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(2071-1956),101,at_x(815),transform(w_AIMPS25,""),i_fn
  endif
  w_AIMPN25 = TRAN(round(mvaimpn5/mvcaoval,2),'@Z '+V_PV[20*(G_PERPVL+2)])
  if mvcodval<>g_codeur AND  NOT empty(mvaimpn5)
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(2071-1956),122,at_x(980),transform(w_AIMPN25,""),i_fn
  endif
  w_MVAIMPN6 = TRAN(mvaimpn6,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(2090-1956),55,at_x(440),transform(w_MVAIMPN6,""),i_fn
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(2090-1956),75,at_x(605),transform(MVACIVA6,""),i_fn
  w_AIMPS6 = TRAN(mvaimps6,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(2090-1956),81,at_x(650),transform(w_AIMPS6,""),i_fn
  w_AIMPS26 = TRAN(round(mvaimps6/mvcaoval,2),'@Z '+V_PV[20*(G_PERPVL+2)])
  if mvcodval<>g_codeur AND NOT empty(iva(6,1))
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(2090-1956),101,at_x(815),transform(w_AIMPS26,""),i_fn
  endif
  w_AIMPN26 = TRAN(round(mvaimpn6/mvcaoval,2),'@Z '+V_PV[20*(G_PERPVL+2)])
  if mvcodval<>g_codeur and  NOT empty(mvaimpn6)
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(2090-1956),122,at_x(980),transform(w_AIMPN26,""),i_fn
  endif
  w_ACCONT = 'Acconto Versato '+TRAN(MVACCONT,'@Z '+V_PV[20*(VADECTOT+2)])
  if not empty(nvl(mvaccont,0))
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(2109-1956),1,at_x(8),transform(w_ACCONT,""),i_fn
  endif
  w_DOC_RA1 = RIGHT(SPACE(13)+TRAN(doc_rate(1,3),'@Z '+V_PV[20*(VADECTOT+2)]),13)
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(2128-1956),1,at_x(8),transform(w_DOC_RA1,""),i_fn
  if not empty(w_DOC_RA1)
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(2128-1956),14,at_x(112),"/",i_fn
  endif
  w_DOCRA1 = CP_TODATE(doc_rate(1,1))
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(2128-1956),15,at_x(123),transform(w_DOCRA1,""),i_fn
  if not empty(w_DOCRA1)
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(2128-1956),26,at_x(210),"-",i_fn
  endif
  w_DOCRAT1 = NVL(translat(MVTIPCON,MVCODCON,'TRADMODP','lgdescri','lgtippag','lgcodlin','MOD_PAGA','MPDESCRI','MPCODICE',doc_rate(1,2)),'')
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(2128-1956),28,at_x(226),transform(w_DOCRAT1,""),i_fn
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(2128-1956),49,at_x(393),transform(VACODVAL,""),i_fn
  w_IMPIV1 = TRAN(impiv1,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(2128-1956),55,at_x(440),transform(w_IMPIV1,""),i_fn
  w_IMPOSTA = TRAN(imposta,'@Z '+V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(2128-1956),85,at_x(681),transform(w_IMPOSTA,""),i_fn
  w_TOTDOC = TRAN(totaledoc,V_PV[20*(VADECTOT+2)])
  i_fn = ""
  do F_Say with i_row+9,i_row+at_y(2128-1956),107,at_x(856),transform(w_TOTDOC,""),i_fn
  w_DOC_RA2 = RIGHT(SPACE(13)+TRAN(doc_rate(2,3),'@Z '+V_PV[20*(VADECTOT+2)]),13)
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(2147-1956),1,at_x(8),transform(w_DOC_RA2,""),i_fn
  if not empty(w_DOC_RA2)
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(2147-1956),14,at_x(112),"/",i_fn
  endif
  w_DOCRA2 = doc_rate(2,1)
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(2147-1956),15,at_x(123),transform(w_DOCRA2,""),i_fn
  if not empty(w_DOCRA2)
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(2147-1956),26,at_x(210),"-",i_fn
  endif
  w_DOCRAT2 = NVL(translat(MVTIPCON,MVCODCON,'TRADMODP','lgdescri','lgtippag','lgcodlin','MOD_PAGA','MPDESCRI','MPCODICE',doc_rate(2,2)),'')
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(2147-1956),28,at_x(226),transform(w_DOCRAT2,""),i_fn
  w_DOC_RA3 = RIGHT(SPACE(13)+TRAN(doc_rate(3,3),'@Z '+V_PV[20*(VADECTOT+2)]),13)
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(2166-1956),1,at_x(8),transform(w_DOC_RA3,""),i_fn
  if not empty(w_DOC_RA3)
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(2166-1956),14,at_x(112),"/",i_fn
  endif
  w_DOCRA3 = CP_TODATE(doc_rate(3,1))
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(2166-1956),15,at_x(123),transform(w_DOCRA3,""),i_fn
  if not empty(w_DOCRA3)
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(2166-1956),26,at_x(210),"-",i_fn
  endif
  w_DOCRAT3 = NVL(translat(MVTIPCON,MVCODCON,'TRADMODP','lgdescri','lgtippag','lgcodlin','MOD_PAGA','MPDESCRI','MPCODICE',doc_rate(3,2)),'')
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(2166-1956),28,at_x(226),transform(w_DOCRAT3,""),i_fn
  if not empty(nvl(mvspeinc,0))
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(2166-1956),70,at_x(560),"Spese D'Incasso:",i_fn
  endif
  w_MVSPEINC = TRAN(MVSPEINC,'@Z '+V_PV[33+(20*VADECTOT)])
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(2166-1956),92,at_x(737),transform(w_MVSPEINC,""),i_fn
  w_DOC_RA4 = RIGHT(SPACE(13)+TRAN(doc_rate(4,3),'@Z '+V_PV[20*(VADECTOT+2)]),13)
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(2185-1956),1,at_x(8),transform(w_DOC_RA4,""),i_fn
  if not empty(w_DOC_RA4)
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(2185-1956),14,at_x(112),"/",i_fn
  endif
  w_DOCRA4 = CP_TODATE(doc_rate(4,1))
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(2185-1956),15,at_x(123),transform(w_DOCRA4,""),i_fn
  if not empty(w_DOCRA4)
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(2185-1956),26,at_x(210),"-",i_fn
  endif
  w_DOCRAT4 = NVL(translat(MVTIPCON,MVCODCON,'TRADMODP','lgdescri','lgtippag','lgcodlin','MOD_PAGA','MPDESCRI','MPCODICE',doc_rate(4,2)),'')
  i_fn = ""
  do F_Say with i_row+12,i_row+at_y(2185-1956),28,at_x(226),transform(w_DOCRAT4,""),i_fn
  w_DOC_RA5 = RIGHT(SPACE(13)+TRAN(doc_rate(5,3),'@Z '+V_PV[20*(VADECTOT+2)]),13)
  i_fn = ""
  do F_Say with i_row+13,i_row+at_y(2204-1956),1,at_x(8),transform(w_DOC_RA5,""),i_fn
  if not empty(w_DOC_RA5)
  i_fn = ""
  do F_Say with i_row+13,i_row+at_y(2204-1956),14,at_x(112),"/",i_fn
  endif
  w_DOCRA5 = CP_TODATE(doc_rate(5,1))
  i_fn = ""
  do F_Say with i_row+13,i_row+at_y(2204-1956),15,at_x(123),transform(w_DOCRA5,""),i_fn
  if not empty(w_DOCRA5)
  i_fn = ""
  do F_Say with i_row+13,i_row+at_y(2204-1956),26,at_x(210),"-",i_fn
  endif
  w_DOCRAT5 = NVL(translat(MVTIPCON,MVCODCON,'TRADMODP','lgdescri','lgtippag','lgcodlin','MOD_PAGA','MPDESCRI','MPCODICE',doc_rate(5,2)),'')
  i_fn = ""
  do F_Say with i_row+13,i_row+at_y(2204-1956),28,at_x(226),transform(w_DOCRAT5,""),i_fn
  w_DOC_RA6 = RIGHT(SPACE(13)+TRAN(doc_rate(6,3),'@Z '+V_PV[20*(VADECTOT+2)]),13)
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(2223-1956),1,at_x(8),transform(w_DOC_RA6,""),i_fn
  if not empty(w_DOC_RA6)
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(2223-1956),14,at_x(112),"/",i_fn
  endif
  w_DOCRA6 = CP_TODATE(doc_rate(6,1))
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(2223-1956),15,at_x(123),transform(w_DOCRA6,""),i_fn
  if not empty(w_DOCRA6)
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(2223-1956),26,at_x(210),"-",i_fn
  endif
  w_DOCRAT6 = NVL(translat(MVTIPCON,MVCODCON,'TRADMODP','lgdescri','lgtippag','lgcodlin','MOD_PAGA','MPDESCRI','MPCODICE',doc_rate(6,2)),'')
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(2223-1956),28,at_x(226),transform(w_DOCRAT6,""),i_fn
  TOTALE = 0
  IF .F.
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(2223-1956),38,at_x(306),transform(TOTALE,""),i_fn
   ENDIF
  w_DESBAN = nvl(looktab('ban_che','badesban','bacodban',mvcodban),'')
  if w_TIPPAG<>'MA' AND w_TIPPAG<>'BO'
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(2223-1956),47,at_x(376),transform(w_DESBAN,""),i_fn
  endif
  w_DESBAN1 = nvl(looktab('coc_mast','badescri','bacodban',mvcodba2),'')
  if w_TIPPAG='MA' OR w_TIPPAG='BO'
  i_fn = ""
  do F_Say with i_row+14,i_row+at_y(2223-1956),47,at_x(376),transform(w_DESBAN1,""),i_fn
  endif
  if NVL(TDFLACCO,' ')='S'
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(2280-1956),1,at_x(8),transform(MVASPEST,""),i_fn
  endif
  w_MVQTACOL = TRAN(mvqtacol,'@KZ '+'99999')
  if nvl(tdflpack,'')='S'
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(2280-1956),70,at_x(560),transform(w_MVQTACOL,""),i_fn
  endif
  w_QTAPES = TRAN(mvqtapes,'@KZ '+v_PV(91))
  if NOT EMPTY(NVL(MVQTAPES,'')) and nvl(tdflpack,'')='S'
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(2280-1956),91,at_x(728),transform(w_QTAPES,""),i_fn
  endif
  w_QTALOR = TRAN(mvqtalor,'@KZ '+v_PV(91))
  if NOT EMPTY(NVL(MVQTALOR,'')) and nvl(tdflpack,'')='S'
  i_fn = ""
  do F_Say with i_row+17,i_row+at_y(2280-1956),111,at_x(888),transform(w_QTALOR,""),i_fn
  endif
  w_DESPOR = NVL(translat(MVTIPCON,MVCODCON,'TRADPORT','lgdescri','LGCODPOR','lgcodlin','PORTI', 'PODESPOR','POCODPOR',mvcodpor),'')
  i_fn = ""
  do F_Say with i_row+19,i_row+at_y(2318-1956),1,at_x(8),transform(w_DESPOR,""),i_fn
  w_DATTRA = dtoc(mvdattra)+'          '+iif(not empty(nvl(mvoratra,0)) and not empty(nvl(mvmintra,0)),mvoratra+':'+mvmintra,space(5))
  if TDFLACCO='S' and not empty(nvl(dtoc(mvdattra),' '))
  i_fn = ""
  do F_Say with i_row+19,i_row+at_y(2318-1956),66,at_x(528),transform(w_DATTRA,""),i_fn
  endif
  w_DESVET = nvl(looktab('vettori','vtdesvet','vtcodvet',mvcodvet),'')
  i_fn = ""
  do F_Say with i_row+21,i_row+at_y(2356-1956),3,at_x(24),transform(w_DESVET,Repl('X',40)),i_fn
  w_VETDAT = w_VETDAT1+alltrim(w_PROVE1)
  i_fn = ""
  do F_Say with i_row+21,i_row+at_y(2356-1956),42,at_x(336),transform(w_VETDAT,Repl('X',40)),i_fn
  TOTALE = 0
  IF .F.
  i_fn = ""
  do F_Say with i_row+21,i_row+at_y(2356-1956),108,at_x(864),transform(TOTALE,""),i_fn
   ENDIF
  CONAI = 0
  IF .F.
  i_fn = ""
  do F_Say with i_row+21,i_row+at_y(2356-1956),112,at_x(896),transform(CONAI,""),i_fn
   ENDIF
  i_fn = ""
  do F_Say with i_row+23,i_row+at_y(2394-1956),3,at_x(24),transform(MVNOTAGG,""),i_fn
return

function at_x
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/6
  return i_pos

function at_y
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/13
  return i_pos

procedure F_Say
  parameter y,py,x,px,s,f
  * --- Questa funzione corregge un errore del driver "Generica solo testo"
  *     di Windows che aggiunge spazi oltre la 89 colonna
  *     Inoltre in Windows sostituisce il carattere 196 con un '-'
  if y=-1
    y = prow()
    x = pcol()
  endif
  @ y,x say s
  return

PROCEDURE CPLU_GO
parameter i_recpos

if i_recpos<=0 .or. i_recpos>reccount()
  if reccount()<>0
    goto bottom
    if .not. eof()
      skip
    endif
  endif  
else
  goto i_recpos
endif
return

* --- Area Manuale = Functions & Procedures 
* --- TX_FACZU
FUNCTION CTRL_A
Private w_RET
w_RET=.f.

if empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ') and mvcodcla<>nvl(tdstacl2,' ');
   and mvcodcla<>nvl(tdstacl3,' ') and mvcodcla<>nvl(tdstacl4,' ') and ;
   mvcodcla<>nvl(tdstacl5,' '))
        w_RET=.T.
endif

Return (w_RET)
endfunc

FUNCTION CTRL_D
Private w_RET
w_RET=.f.

if !empty(mvdesart) and ((empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ') and ;
mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ') ;
and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' '))))
   w_RET=.T.
endif
Return (w_RET)
endfunc

FUNCTION CTRL1
Private w_RET
w_RET=.f.

if (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ') and mvcodcla<>nvl(tdstacl2,' ');
   and mvcodcla<>nvl(tdstacl3,' ') and mvcodcla<>nvl(tdstacl4,' ');
    and mvcodcla<>nvl(tdstacl5,' ')))
       w_RET=.t.
endif
Return (w_RET)
endfunc

FUNCTION CTRL2
Private w_RET
w_RET=.f.

if (mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and not empty(mvscont3) or not empty(mvscont4);
   and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
   and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
   and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' ')))
       w_RET=.t.
endif
Return (w_RET)
endfunc


FUNCTION CTRL4
Private w_RET
w_RET=.f.

if (mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and not empty(mvscont2);
   and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
   and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
   and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' ')))
       w_RET=.t.
endif
Return (w_RET)
endfunc

FUNCTION CTRL3
Private w_RET
w_RET=.f.

if ((mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and not empty(mvscont1);
    and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
    and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
    and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' '))))
       w_RET=.t.
endif
Return (w_RET)
endfunc


Function FValrig
private w_VALRIG 
w_VALRIG=iif(mvrifkit<>0,'',iif(mvflomag='X',TRAN(mvvalrig,'@Z '+V_PV[18*(VADECTOT+2)]),;
                  iif(mvflomag='S','Sconto Merce',iif(mvflomag='I','Omaggio Imponibile',;
                  iif(mvflomag='E','Omaggio Imp.+IVA','')))))
Return(w_VALRIG)
endfunc

Function FIva
private w_IVA
w_IVA=iif(mvrifkit<>0 or mvtiprig='D','',iif(empty(nvl(mvcodive,' ')) or nvl(ivperiva,0)=0,mvcodiva,mvcodive))
Return (w_IVA)
endfunc

Function Fscrdessup
Private w_RET
w_RET=.f.
if 'S'=nvl(looktab('ART_ICOL','ARSTASUP','ARCODART',MVCODART),'')
    w_RET=.t.
endif
Return (w_RET)
endfunc

Function Fdatitest
private w_DATITEST
IF empty(nvl(coddes,''))
            w_DATITEST=alltrim(an___cap)+iif(empty(anlocali),'',' '+alltrim(anlocali ))+;
            iif(empty(alltrim(anprovin)),'',+' ( ' +anprovin+' ) ')+iif(empty(alltrim(nvl(annazion,''))),;
            '',+' ( ' +nvl(annazion,'')+' ) ')
else 
            w_DATITEST=alltrim(cap)+iif(empty(locali),'',' '+alltrim(locali ))+iif(empty(alltrim(provin)),'',+;
            ' ( ' +provin+' ) ')+iif(empty(alltrim(nvl(codnaz,''))),'',+' ( ' +nvl(codnaz,'')+' ) ')       
endif
Return (w_DATITEST)
endfunc

Function Fdatitest2
private w_DATITEST2
IF NVL(ddtiprif,'')='FA'
       w_DATITEST2=alltrim(dd___cap)+iif(empty(ddlocali),'',' '+alltrim(ddlocali ));
              +iif(empty(alltrim(ddprovin)),'',+' ( ' +ddprovin+' ) ');
              +iif(empty(alltrim(nvl(ddcodnaz,''))),'',+' ( ' +nvl(ddcodnaz,'')+' ) ')
ELSE
        w_DATITEST2=alltrim(an___cap)+iif(empty(anlocali),'',' '+alltrim(anlocali ))+;
              iif(empty(alltrim(anprovin)),'',+' ( ' +anprovin+' ) ')+;
              iif(empty(alltrim(nvl(annazion,''))),'',+' ( ' +nvl(annazion,'')+' ) ')
ENDIF
Return (w_DATITEST2)
endfunc

Function FTxt1
Private w_TXT1
w_TXT1='Dichiarazione di intento n� ' + alltrim(str(L_NDIC));
                + ' del ' +dtoc(L_DDIC)+' Art. Es. '+alltrim(L_ARTESE);
                +' del D.P.R. 633/72'+iif(L_TIPOPER='D','  Valida dal '+DTOC(L_DATINI);
                + ' al '+DTOC(L_DATFIN),IIF(L_TIPOPER='I',' Limite es: '+alltrim(IMP),''))
Return (w_TXT1)
Endfunc

Function FImposta
Private imposta
imposta=iif(mvaflom1$'XI',mvaimps1,0)+iif(mvaflom2$'XI',mvaimps2,0);
               +iif(mvaflom3$'XI',mvaimps3,0)+iif(mvaflom4$'XI',mvaimps4,0);
                +iif(mvaflom5$'XI',mvaimps5,0)+iif(mvaflom6$'XI',mvaimps6,0)
Return(imposta)
Endfunc

FUNCTIO FVETDAT1
private w_VETDAT1
w_VETDAT1=alltrim(NVL(looktab('vettori','vtindvet','vtcodvet',mvcodvet),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtvetcap','vtcodvet',mvcodvet),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtlocvet','vtcodvet',mvcodvet),''))+'   '
RETURN (w_VETDAT1)
ENDFUNC

FUNCTION FVETDAT2
private w_VETDAT2
w_VETDAT2=alltrim(NVL(looktab('vettori','vtindvet','vtcodvet',mvcodve2),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtvetcap','vtcodvet',mvcodve2),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtlocvet','vtcodvet',mvcodve3),''))+'   '
RETURN(w_VETDAT2)
ENDFUNC

FUNCTION FIMPIVA
PRIVATE w_IMPIVA
w_IMPIVA=iif(mvaflom1='X',mvaimpn1,0)+iif(mvaflom2='X',mvaimpn2,0);
                 +iif(mvaflom3='X',mvaimpn3,0)+iif(mvaflom4='X',mvaimpn4,0);
                 +iif(mvaflom5='X',mvaimpn5,0)+iif(mvaflom6='X',mvaimpn6,0)
RETURN(w_IMPIVA)
ENDFUNC

FUNCTION FCODART
Private w_RET
w_RET=.f.

if (not empty(mvqtamov) or not empty(mvprezzo) or empty(mvcodcla)) or (mvcodcla<>nvl(tdstacl1,' ') and mvcodcla<>nvl(tdstacl2,' ');
   and mvcodcla<>nvl(tdstacl3,' ') and mvcodcla<>nvl(tdstacl4,' ');
   and mvcodcla<>nvl(tdstacl5,' '))
       w_RET=.t.
endif
Return (w_RET)
endfunc

FUNCTION FDESART
Private w_RET
w_RET=.f.

if mvtiprig<>'D' and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
   and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
   and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' ')))
       w_RET=.t.
endif
Return (w_RET)
endfunc

* ----------------------------------------------------------------------
* Funzione : Pari_A
* ----------------------------------------------------------------------
*  Esegue traduzione importo documento in EURO o LIRE
* ----------------------------------------------------------------------
Function Pari_a
parameter Valuta
private w_ImpRet,w_TotPar
do case
   case MVCODVAL=g_CODEUR
        w_TotPar =  ROUND(( TOTALEA*p_Cambio),0)
        w_ImpRet = TRAN(w_TotPar,+V_PV[(40)])
   case MVCODVAL=g_CODLIR
         w_TotPar =  ROUND((TOTALEA/p_Cambio),2)
         w_ImpRet = TRAN(w_TotPar,V_PV[(80)])
   otherwise
   if mvdatdoc>=g_dateur and vacodval<>g_codeur
        w_TotPar = ROUND((TOTALEA/mvcaoval),2)
        w_ImpRet = TRAN(w_TotPar,V_PV[(80)])
   else
        w_TotPar = ROUND((TOTALEA*p_cambio),0)
        w_ImpRet = TRAN(w_TotPar,V_PV[(80)])
   endif
endcase

return (w_ImpRet)

Func Caldespag()
    Private w_Ret
    w_Ret = mvcodpag+' '+NVL(translat(mvtipcon,mvcodcon,'tradpaga','lgdescod','lgcodice','lgcodlin','pag_amen','padescri','pacodice',mvcodpag),'')
    Return (w_Ret)
Endfunc

FUNCTION CTRL_COAC
Private w_RET
w_RET=.t.
* INDICA SE STAMPARE LA RIGA DI CONTRIBUTO ACCESSORIO
if G_COAC = "S"
  if VARTYPE(MVCACONT) = "C"
    if NVL( LOOKTAB( "TIP_DOCU" , "TDNOSTCO"  , "TDTIPDOC" , TDTIPDOC ) , "N" ) = "S" and !EMPTY(MVCACONT)
      * se il flag sulla causale � attivo e siamo su una riga di contributo accessorio,
      * la riga non viene stampata
      w_RET=.f.
    else
      w_RET=.t.
    endif
  else
    w_RET=.t.
  endif
else
  w_RET=.t.
endif

Return (w_RET)
endfunc

* --- Fine Area Manuale 
