* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bpa                                                        *
*              Menu contestuale partite\scadenze                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_35]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-28                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFUNZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bpa",oParentObject,m.pFUNZ)
return(i_retval)

define class tgszm_bpa as StdBatch
  * --- Local variables
  pFUNZ = space(1)
  w_OBJECT = .NULL.
  w_PROG = .NULL.
  w_BOTTONE = .NULL.
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  * --- WorkFile variables
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine men� contestuale intestatario su Manutenzione Partite
    * --- Assegno i valori alle variabili
    this.w_PTSERIAL = g_oMenu.getbyindexkeyvalue(1)
    this.w_PTROWORD = g_oMenu.getbyindexkeyvalue(2)
    if g_oMenu.nkeycount=3
      * --- Se esiste la terza chiave nella gestione
      this.w_CPROWNUM = g_oMenu.getbyindexkeyvalue(3)
    else
      * --- Se esiste la variabile relativa alla terza chiave
      this.w_CPROWNUM = iif(Type("g_oMenu.oParentobject.w_CPROWNUM")="N",g_oMenu.oParentobject.w_CPROWNUM,1)
    endif
    * --- Read from PAR_TITE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PTCODCON,PTTIPCON"+;
        " from "+i_cTable+" PAR_TITE where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
            +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PTCODCON,PTTIPCON;
        from (i_cTable) where;
            PTSERIAL = this.w_PTSERIAL;
            and PTROWORD = this.w_PTROWORD;
            and CPROWNUM = this.w_CPROWNUM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ANCODICE = NVL(cp_ToDate(_read_.PTCODCON),cp_NullValue(_read_.PTCODCON))
      this.w_ANTIPCON = NVL(cp_ToDate(_read_.PTTIPCON),cp_NullValue(_read_.PTTIPCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pFUNZ = "1"
        * --- Anagrafica Clienti\Conti\Fornitori
        this.w_OBJECT = iif(this.w_ANTIPCON="F",GSAR_AFR(),iif(this.w_ANTIPCON="C",GSAR_ACL(),GSAR_API()))
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_ANTIPCON = this.w_ANTIPCON
        this.w_OBJECT.w_ANCODICE = this.w_ANCODICE
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
      case this.pFUNZ = "2"
        * --- Stampa Partite
        this.w_OBJECT = GSTE_SIS()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_TIPCON = this.w_ANTIPCON
        this.w_OBJECT.w_CODCON = this.w_ANCODICE
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODCON")
        this.w_PROG.Check()     
        this.w_OBJECT.mCalc(.t.)     
      case this.pFUNZ = "3"
        * --- Stampa Estratto Conto
        this.w_OBJECT = GSTE_KSC()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_TIPCLF = this.w_ANTIPCON
        this.w_OBJECT.w_CODINI = this.w_ANCODICE
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODINI")
        this.w_PROG.Check()     
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODFIN")
        this.w_OBJECT.w_CODFIN = this.w_ANCODICE
        this.w_PROG.Check()     
        this.w_OBJECT.mCalc(.t.)     
      case this.pFUNZ = "4"
        * --- Stampa Analisi dello Scaduto
        this.w_OBJECT = GSTE_SCA()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_TIPCON = this.w_ANTIPCON
        this.w_OBJECT.w_CODCON = this.w_ANCODICE
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODCON")
        this.w_PROG.Check()     
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODFIN")
        this.w_OBJECT.w_CODFIN = this.w_ANCODICE
        this.w_PROG.Check()     
        this.w_OBJECT.mCalc(.t.)     
      case this.pFUNZ = "5"
        * --- Stampa Brigliaccio Effetti
        this.w_OBJECT = GSTE_SBE()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_TIPCON = this.w_ANTIPCON
        this.w_OBJECT.w_CODCON = this.w_ANCODICE
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODCON")
        this.w_PROG.Check()     
        this.w_OBJECT.mCalc(.t.)     
      case this.pFUNZ = "6"
        * --- Esposizione Finanziaria
        this.w_OBJECT = GSTE_KEF()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_TIPO = this.w_ANTIPCON
        if this.w_ANTIPCON="C"
          this.w_OBJECT.w_CLIENTE = this.w_ANCODICE
          this.w_PROG = this.w_OBJECT.GetcTRL("w_CLIENTE")
        else
          this.w_OBJECT.w_FORNITORE = this.w_ANCODICE
          this.w_PROG = this.w_OBJECT.GetcTRL("w_FORNITORE")
        endif
        this.w_PROG.Check()     
        this.w_OBJECT.mCalc(.t.)     
      case this.pFUNZ = "7"
        * --- Esposizione Finanziaria
        this.w_OBJECT = GSTE_KCF()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        do case
          case this.w_ANTIPCON="C"
            this.w_OBJECT.w_TIPCLI = this.w_ANTIPCON
            this.w_OBJECT.w_CODCLI = this.w_ANCODICE
            this.w_OBJECT.w_TIPGEN = " "
            this.w_OBJECT.w_TIPFOR = " "
            this.w_PROG = this.w_OBJECT.GetcTRL("w_CODCLI")
            this.w_PROG.Value = this.w_ANCODICE
            this.w_PROG.Check()     
          case this.w_ANTIPCON="F"
            this.w_OBJECT.w_TIPFOR = this.w_ANTIPCON
            this.w_OBJECT.w_TIPGEN = " "
            this.w_OBJECT.w_TIPCLI = " "
            this.w_OBJECT.w_CODFOR = this.w_ANCODICE
            this.w_PROG = this.w_OBJECT.GetcTRL("w_CODFOR")
            this.w_PROG.Check()     
          case this.w_ANTIPCON="G"
            this.w_OBJECT.w_TIPGEN = this.w_ANTIPCON
            this.w_OBJECT.w_TIPCLI = " "
            this.w_OBJECT.w_TIPFOR = " "
        endcase
        this.w_OBJECT.mCalc(.t.)     
    endcase
    this.w_PROG = .NULL.
    this.w_OBJECT = .NULL.
  endproc


  proc Init(oParentObject,pFUNZ)
    this.pFUNZ=pFUNZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_TITE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFUNZ"
endproc
