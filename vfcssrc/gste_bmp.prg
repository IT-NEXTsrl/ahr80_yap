* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bmp                                                        *
*              Eventi da manutenzione partite                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_161]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-28                                                      *
* Last revis.: 2012-03-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bmp",oParentObject,m.pOper)
return(i_retval)

define class tgste_bmp as StdBatch
  * --- Local variables
  pOper = space(4)
  w_SERDIS = space(10)
  w_IMPSCA = 0
  w_APPO = 0
  w_TIPSCA = space(1)
  w_MESS = space(10)
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_OK = .f.
  w_RIGPAR = 0
  w_MODPAG = space(10)
  w_TOTPAR = 0
  w_MAXDEC = 0
  w_APPO1 = space(10)
  w_LSERRIF = space(10)
  w_LORDRIF = 0
  w_LNUMRIF = 0
  w_RIFCON = space(10)
  w_FLINDI = space(1)
  w_FLIMPE = space(1)
  w_PADRE = .NULL.
  w_TOTIMP = 0
  w_LROWNUM = 0
  * --- WorkFile variables
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  SCA_VARI_idx=0
  VALUTE_idx=0
  DIS_TINT_idx=0
  CON_INDI_idx=0
  PAR_TITE_idx=0
  VALDATPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue il Programma Padre associato alla Partita selezionata (da GSTE_MMP)
    * --- chiave della Prima Nota
    this.w_PADRE = this.oparentobject
    do case
      case this.pOper="LOAD"
        this.w_PADRE.Markpos()     
        SELECT (this.w_PADRE.cTrsName)
        GO TOP
        this.w_MAXDEC = 0
        this.w_SERDIS = SPACE(10)
        this.oParentObject.w_NUMIND = 0
        this.oParentObject.w_DATIND = cp_CharToDate("  -  -  ")
        this.oParentObject.w_ESEIND = SPACE(4)
        SCAN
        this.w_MAXDEC = MAX(this.w_MAXDEC, NVL(t_DECTOT,0))
        ENDSCAN
        VVL = 20*this.w_MAXDEC
        this.oParentObject.w_VALNAZ = g_PERVAL
        this.oParentObject.w_TOTDOC = 0
        this.w_TOTIMP = 0
        do case
          case this.oParentObject.w_PTROWORD > 0
            * --- PrimaNota
            * --- Read from PNT_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PNT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PNNUMDOC,PNALFDOC,PNDESSUP,PNNUMRER,PNDATREG,PNVALNAZ,PNCODVAL,PNCAOVAL,PNRIFDIS,PNDATDOC,PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05,PNAGG_06"+;
                " from "+i_cTable+" PNT_MAST where ";
                    +"PNSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PTSERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PNNUMDOC,PNALFDOC,PNDESSUP,PNNUMRER,PNDATREG,PNVALNAZ,PNCODVAL,PNCAOVAL,PNRIFDIS,PNDATDOC,PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05,PNAGG_06;
                from (i_cTable) where;
                    PNSERIAL = this.oParentObject.w_PTSERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_RIFNUM = NVL(cp_ToDate(_read_.PNNUMDOC),cp_NullValue(_read_.PNNUMDOC))
              this.oParentObject.w_RIFALF = NVL(cp_ToDate(_read_.PNALFDOC),cp_NullValue(_read_.PNALFDOC))
              this.oParentObject.w_DESCRI = NVL(cp_ToDate(_read_.PNDESSUP),cp_NullValue(_read_.PNDESSUP))
              this.oParentObject.w_NUMRER = NVL(cp_ToDate(_read_.PNNUMRER),cp_NullValue(_read_.PNNUMRER))
              this.oParentObject.w_DATREG = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
              this.oParentObject.w_VALNAZ = NVL(cp_ToDate(_read_.PNVALNAZ),cp_NullValue(_read_.PNVALNAZ))
              this.oParentObject.w_CODVAL = NVL(cp_ToDate(_read_.PNCODVAL),cp_NullValue(_read_.PNCODVAL))
              this.oParentObject.w_CAOVAL = NVL(cp_ToDate(_read_.PNCAOVAL),cp_NullValue(_read_.PNCAOVAL))
              this.w_SERDIS = NVL(cp_ToDate(_read_.PNRIFDIS),cp_NullValue(_read_.PNRIFDIS))
              this.oParentObject.w_DATDOC = NVL(cp_ToDate(_read_.PNDATDOC),cp_NullValue(_read_.PNDATDOC))
              this.oParentObject.w_PNAGG_01 = NVL(cp_ToDate(_read_.PNAGG_01),cp_NullValue(_read_.PNAGG_01))
              this.oParentObject.w_PNAGG_02 = NVL(cp_ToDate(_read_.PNAGG_02),cp_NullValue(_read_.PNAGG_02))
              this.oParentObject.w_PNAGG_03 = NVL(cp_ToDate(_read_.PNAGG_03),cp_NullValue(_read_.PNAGG_03))
              this.oParentObject.w_PNAGG_04 = NVL(cp_ToDate(_read_.PNAGG_04),cp_NullValue(_read_.PNAGG_04))
              this.oParentObject.w_PNAGG_05 = NVL(cp_ToDate(_read_.PNAGG_05),cp_NullValue(_read_.PNAGG_05))
              this.oParentObject.w_PNAGG_06 = NVL(cp_ToDate(_read_.PNAGG_06),cp_NullValue(_read_.PNAGG_06))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from PNT_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PNT_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2],.t.,this.PNT_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PNIMPDAR,PNIMPAVE,PNCAURIG,PNDESRIG"+;
                " from "+i_cTable+" PNT_DETT where ";
                    +"PNSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PTSERIAL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_PTROWORD);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PNIMPDAR,PNIMPAVE,PNCAURIG,PNDESRIG;
                from (i_cTable) where;
                    PNSERIAL = this.oParentObject.w_PTSERIAL;
                    and CPROWNUM = this.oParentObject.w_PTROWORD;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_IMPDAR = NVL(cp_ToDate(_read_.PNIMPDAR),cp_NullValue(_read_.PNIMPDAR))
              this.w_IMPAVE = NVL(cp_ToDate(_read_.PNIMPAVE),cp_NullValue(_read_.PNIMPAVE))
              this.oParentObject.w_CODCAU = NVL(cp_ToDate(_read_.PNCAURIG),cp_NullValue(_read_.PNCAURIG))
              this.oParentObject.w_DESRIG = NVL(cp_ToDate(_read_.PNDESRIG),cp_NullValue(_read_.PNDESRIG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.oParentObject.w_FLDAVE = IIF(this.w_IMPAVE<>0, "A", "D")
            this.oParentObject.w_IMPRIG = IIF(this.oParentObject.w_FLDAVE="A", this.w_IMPAVE, this.w_IMPDAR)
            * --- Read from VALUTE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VALUTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VACAOVAL,VADECTOT"+;
                " from "+i_cTable+" VALUTE where ";
                    +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALNAZ);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VACAOVAL,VADECTOT;
                from (i_cTable) where;
                    VACODVAL = this.oParentObject.w_VALNAZ;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_CAONAZ = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
              this.oParentObject.w_DECTOP = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_SERDIS = IIF(VAL(this.w_SERDIS)<0, "0"+RIGHT(this.w_SERDIS, 9), this.w_SERDIS) 
            if NOT EMPTY(this.w_SERDIS)
              SELECT (this.w_PADRE.cTrsName)
              GO TOP
              SCAN FOR t_PTTOTIMP<>0 AND NOT EMPTY(t_PTDATSCA) AND NOT EMPTY(t_PTNUMPAR)
              this.w_PADRE.WorkFromTrs()     
              this.w_APPO1 = SPACE(10)
              if not empty(this.oParentObject.w_PTSERRIF)
                * --- Leggo riferimenti Partita con PTROWORD=-3 da contabilizzazione indiretta effetti
                * --- Read from PAR_TITE
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PTSERIAL,PTROWORD,CPROWNUM"+;
                    " from "+i_cTable+" PAR_TITE where ";
                        +"PTDATSCA = "+cp_ToStrODBC(this.oParentObject.w_PTDATSCA);
                        +" and PTNUMPAR = "+cp_ToStrODBC(this.oParentObject.w_PTNUMPAR);
                        +" and PTTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PTTIPCON);
                        +" and PTCODCON = "+cp_ToStrODBC(this.oParentObject.w_PTCODCON);
                        +" and PTCODVAL = "+cp_ToStrODBC(this.oParentObject.w_PTCODVAL);
                        +" and PTROWORD = "+cp_ToStrODBC(-3);
                        +" and PTFLINDI = "+cp_ToStrODBC("S");
                        +" and PTSERRIF = "+cp_ToStrODBC(this.oParentObject.w_PTSERRIF);
                        +" and PTORDRIF = "+cp_ToStrODBC(this.oParentObject.w_PTORDRIF);
                        +" and PTNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_PTNUMRIF);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PTSERIAL,PTROWORD,CPROWNUM;
                    from (i_cTable) where;
                        PTDATSCA = this.oParentObject.w_PTDATSCA;
                        and PTNUMPAR = this.oParentObject.w_PTNUMPAR;
                        and PTTIPCON = this.oParentObject.w_PTTIPCON;
                        and PTCODCON = this.oParentObject.w_PTCODCON;
                        and PTCODVAL = this.oParentObject.w_PTCODVAL;
                        and PTROWORD = -3;
                        and PTFLINDI = "S";
                        and PTSERRIF = this.oParentObject.w_PTSERRIF;
                        and PTORDRIF = this.oParentObject.w_PTORDRIF;
                        and PTNUMRIF = this.oParentObject.w_PTNUMRIF;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_LSERRIF = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
                  this.w_LORDRIF = NVL(cp_ToDate(_read_.PTROWORD),cp_NullValue(_read_.PTROWORD))
                  this.w_LNUMRIF = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Leggo riferimento e numero effetti dalla partita della distinta
                this.w_FLINDI = "S"
                if I_rows=0
                  this.w_LSERRIF = this.oParentObject.w_PTSERRIF
                  this.w_LORDRIF = this.oParentObject.w_PTORDRIF
                  this.w_LNUMRIF = this.oParentObject.w_PTNUMRIF
                  this.w_FLINDI = " "
                endif
                * --- Read from PAR_TITE
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PTSERIAL,PTNUMEFF,PTFLIMPE,PTRIFIND"+;
                    " from "+i_cTable+" PAR_TITE where ";
                        +"PTDATSCA = "+cp_ToStrODBC(this.oParentObject.w_PTDATSCA);
                        +" and PTNUMPAR = "+cp_ToStrODBC(this.oParentObject.w_PTNUMPAR);
                        +" and PTTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PTTIPCON);
                        +" and PTCODCON = "+cp_ToStrODBC(this.oParentObject.w_PTCODCON);
                        +" and PTCODVAL = "+cp_ToStrODBC(this.oParentObject.w_PTCODVAL);
                        +" and PTROWORD = "+cp_ToStrODBC(-2);
                        +" and PTFLINDI = "+cp_ToStrODBC(this.w_FLINDI);
                        +" and PTSERRIF = "+cp_ToStrODBC(this.w_LSERRIF);
                        +" and PTORDRIF = "+cp_ToStrODBC(this.w_LORDRIF);
                        +" and PTNUMRIF = "+cp_ToStrODBC(this.w_LNUMRIF);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PTSERIAL,PTNUMEFF,PTFLIMPE,PTRIFIND;
                    from (i_cTable) where;
                        PTDATSCA = this.oParentObject.w_PTDATSCA;
                        and PTNUMPAR = this.oParentObject.w_PTNUMPAR;
                        and PTTIPCON = this.oParentObject.w_PTTIPCON;
                        and PTCODCON = this.oParentObject.w_PTCODCON;
                        and PTCODVAL = this.oParentObject.w_PTCODVAL;
                        and PTROWORD = -2;
                        and PTFLINDI = this.w_FLINDI;
                        and PTSERRIF = this.w_LSERRIF;
                        and PTORDRIF = this.w_LORDRIF;
                        and PTNUMRIF = this.w_LNUMRIF;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_APPO1 = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
                  this.oParentObject.w_NUMEFF = NVL(cp_ToDate(_read_.PTNUMEFF),cp_NullValue(_read_.PTNUMEFF))
                  this.w_FLIMPE = NVL(cp_ToDate(_read_.PTFLIMPE),cp_NullValue(_read_.PTFLIMPE))
                  this.w_RIFCON = NVL(cp_ToDate(_read_.PTRIFIND),cp_NullValue(_read_.PTRIFIND))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              else
                * --- Se pregresso non ho i riferimenti
                * --- Read from PAR_TITE
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PTSERIAL,PTNUMEFF,PTFLIMPE,PTRIFIND"+;
                    " from "+i_cTable+" PAR_TITE where ";
                        +"PTDATSCA = "+cp_ToStrODBC(this.oParentObject.w_PTDATSCA);
                        +" and PTNUMPAR = "+cp_ToStrODBC(this.oParentObject.w_PTNUMPAR);
                        +" and PTTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PTTIPCON);
                        +" and PTCODCON = "+cp_ToStrODBC(this.oParentObject.w_PTCODCON);
                        +" and PTCODVAL = "+cp_ToStrODBC(this.oParentObject.w_PTCODVAL);
                        +" and PTROWORD = "+cp_ToStrODBC(-2);
                        +" and PTFLINDI = "+cp_ToStrODBC(this.w_FLINDI);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PTSERIAL,PTNUMEFF,PTFLIMPE,PTRIFIND;
                    from (i_cTable) where;
                        PTDATSCA = this.oParentObject.w_PTDATSCA;
                        and PTNUMPAR = this.oParentObject.w_PTNUMPAR;
                        and PTTIPCON = this.oParentObject.w_PTTIPCON;
                        and PTCODCON = this.oParentObject.w_PTCODCON;
                        and PTCODVAL = this.oParentObject.w_PTCODVAL;
                        and PTROWORD = -2;
                        and PTFLINDI = this.w_FLINDI;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_APPO1 = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
                  this.oParentObject.w_NUMEFF = NVL(cp_ToDate(_read_.PTNUMEFF),cp_NullValue(_read_.PTNUMEFF))
                  this.w_FLIMPE = NVL(cp_ToDate(_read_.PTFLIMPE),cp_NullValue(_read_.PTFLIMPE))
                  this.w_RIFCON = NVL(cp_ToDate(_read_.PTRIFIND),cp_NullValue(_read_.PTRIFIND))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              if NOT EMPTY(this.w_APPO1)
                this.oParentObject.w_PTNUMDIS = this.w_APPO1
                * --- Manutenzione Distinte
                * --- Read from DIS_TINT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DIS_TINT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DINUMERO,DI__ANNO,DIDATDIS,DITIPDIS,DINUMDIS"+;
                    " from "+i_cTable+" DIS_TINT where ";
                        +"DINUMDIS = "+cp_ToStrODBC(this.w_APPO1);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DINUMERO,DI__ANNO,DIDATDIS,DITIPDIS,DINUMDIS;
                    from (i_cTable) where;
                        DINUMDIS = this.w_APPO1;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_NUMDIS = NVL(cp_ToDate(_read_.DINUMERO),cp_NullValue(_read_.DINUMERO))
                  this.oParentObject.w_ANNDIS = NVL(cp_ToDate(_read_.DI__ANNO),cp_NullValue(_read_.DI__ANNO))
                  this.oParentObject.w_DATDIS = NVL(cp_ToDate(_read_.DIDATDIS),cp_NullValue(_read_.DIDATDIS))
                  this.oParentObject.w_TIPDIS = NVL(cp_ToDate(_read_.DITIPDIS),cp_NullValue(_read_.DITIPDIS))
                  this.oParentObject.w_CODDIS = NVL(cp_ToDate(_read_.DINUMDIS),cp_NullValue(_read_.DINUMDIS))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if NOT EMPTY(this.w_RIFCON)
                  * --- Read from PNT_MAST
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PNT_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "PNNUMDOC,PNALFDOC,PNDESSUP,PNNUMRER,PNDATREG,PNVALNAZ,PNCODVAL,PNCAOVAL,PNRIFDIS,PNDATDOC,PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05,PNAGG_06"+;
                      " from "+i_cTable+" PNT_MAST where ";
                          +"PNSERIAL = "+cp_ToStrODBC(this.w_RIFCON);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      PNNUMDOC,PNALFDOC,PNDESSUP,PNNUMRER,PNDATREG,PNVALNAZ,PNCODVAL,PNCAOVAL,PNRIFDIS,PNDATDOC,PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05,PNAGG_06;
                      from (i_cTable) where;
                          PNSERIAL = this.w_RIFCON;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.oParentObject.w_RIFNUM = NVL(cp_ToDate(_read_.PNNUMDOC),cp_NullValue(_read_.PNNUMDOC))
                    this.oParentObject.w_RIFALF = NVL(cp_ToDate(_read_.PNALFDOC),cp_NullValue(_read_.PNALFDOC))
                    this.oParentObject.w_DESCRI = NVL(cp_ToDate(_read_.PNDESSUP),cp_NullValue(_read_.PNDESSUP))
                    this.oParentObject.w_NUMRER = NVL(cp_ToDate(_read_.PNNUMRER),cp_NullValue(_read_.PNNUMRER))
                    this.oParentObject.w_DATREG = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
                    this.oParentObject.w_VALNAZ = NVL(cp_ToDate(_read_.PNVALNAZ),cp_NullValue(_read_.PNVALNAZ))
                    this.oParentObject.w_CODVAL = NVL(cp_ToDate(_read_.PNCODVAL),cp_NullValue(_read_.PNCODVAL))
                    this.oParentObject.w_CAOVAL = NVL(cp_ToDate(_read_.PNCAOVAL),cp_NullValue(_read_.PNCAOVAL))
                    this.w_SERDIS = NVL(cp_ToDate(_read_.PNRIFDIS),cp_NullValue(_read_.PNRIFDIS))
                    this.oParentObject.w_DATDOC = NVL(cp_ToDate(_read_.PNDATDOC),cp_NullValue(_read_.PNDATDOC))
                    this.oParentObject.w_PNAGG_01 = NVL(cp_ToDate(_read_.PNAGG_01),cp_NullValue(_read_.PNAGG_01))
                    this.oParentObject.w_PNAGG_02 = NVL(cp_ToDate(_read_.PNAGG_02),cp_NullValue(_read_.PNAGG_02))
                    this.oParentObject.w_PNAGG_03 = NVL(cp_ToDate(_read_.PNAGG_03),cp_NullValue(_read_.PNAGG_03))
                    this.oParentObject.w_PNAGG_04 = NVL(cp_ToDate(_read_.PNAGG_04),cp_NullValue(_read_.PNAGG_04))
                    this.oParentObject.w_PNAGG_05 = NVL(cp_ToDate(_read_.PNAGG_05),cp_NullValue(_read_.PNAGG_05))
                    this.oParentObject.w_PNAGG_06 = NVL(cp_ToDate(_read_.PNAGG_06),cp_NullValue(_read_.PNAGG_06))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
              else
                * --- Distinta Contab.Indiretta
                * --- Read from CON_INDI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CON_INDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CON_INDI_idx,2],.t.,this.CON_INDI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CINUMERO,CICODESE,CIDATREG"+;
                    " from "+i_cTable+" CON_INDI where ";
                        +"CISERIAL = "+cp_ToStrODBC(this.w_SERDIS);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CINUMERO,CICODESE,CIDATREG;
                    from (i_cTable) where;
                        CISERIAL = this.w_SERDIS;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_NUMIND = NVL(cp_ToDate(_read_.CINUMERO),cp_NullValue(_read_.CINUMERO))
                  this.oParentObject.w_ESEIND = NVL(cp_ToDate(_read_.CICODESE),cp_NullValue(_read_.CICODESE))
                  this.oParentObject.w_DATIND = NVL(cp_ToDate(_read_.CIDATREG),cp_NullValue(_read_.CIDATREG))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              SELECT (this.w_PADRE.cTrsName)
              this.w_PADRE.TrsFromWork()     
              ENDSCAN
            endif
          case this.oParentObject.w_PTROWORD = -1
            this.w_TIPSCA = "C"
            * --- Read from SCA_VARI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.SCA_VARI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2],.t.,this.SCA_VARI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "SCNUMDOC,SCALFDOC,SCIMPSCA,SCCODICE,SCDESCRI,SCTIPSCA,SCVALNAZ,SCCODVAL,SCCAOVAL,SCDATVAL,SCDATDOC"+;
                " from "+i_cTable+" SCA_VARI where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PTSERIAL);
                    +" and SCCODSEC = "+cp_ToStrODBC(this.oParentObject.w_PTROWORD);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                SCNUMDOC,SCALFDOC,SCIMPSCA,SCCODICE,SCDESCRI,SCTIPSCA,SCVALNAZ,SCCODVAL,SCCAOVAL,SCDATVAL,SCDATDOC;
                from (i_cTable) where;
                    SCCODICE = this.oParentObject.w_PTSERIAL;
                    and SCCODSEC = this.oParentObject.w_PTROWORD;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_RIFNUM = NVL(cp_ToDate(_read_.SCNUMDOC),cp_NullValue(_read_.SCNUMDOC))
              this.oParentObject.w_RIFALF = NVL(cp_ToDate(_read_.SCALFDOC),cp_NullValue(_read_.SCALFDOC))
              this.oParentObject.w_IMPRIG = NVL(cp_ToDate(_read_.SCIMPSCA),cp_NullValue(_read_.SCIMPSCA))
              this.oParentObject.w_CODICE = NVL(cp_ToDate(_read_.SCCODICE),cp_NullValue(_read_.SCCODICE))
              this.oParentObject.w_DESCRI = NVL(cp_ToDate(_read_.SCDESCRI),cp_NullValue(_read_.SCDESCRI))
              this.w_TIPSCA = NVL(cp_ToDate(_read_.SCTIPSCA),cp_NullValue(_read_.SCTIPSCA))
              this.oParentObject.w_VALNAZ = NVL(cp_ToDate(_read_.SCVALNAZ),cp_NullValue(_read_.SCVALNAZ))
              this.oParentObject.w_CODVAL = NVL(cp_ToDate(_read_.SCCODVAL),cp_NullValue(_read_.SCCODVAL))
              this.oParentObject.w_CAOVAL = NVL(cp_ToDate(_read_.SCCAOVAL),cp_NullValue(_read_.SCCAOVAL))
              this.oParentObject.w_DATREG = NVL(cp_ToDate(_read_.SCDATVAL),cp_NullValue(_read_.SCDATVAL))
              this.oParentObject.w_DATDOC = NVL(cp_ToDate(_read_.SCDATDOC),cp_NullValue(_read_.SCDATDOC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.oParentObject.w_DESRIG = this.oParentObject.w_DESCRI
            this.oParentObject.w_FLDAVE = IIF(this.w_TIPSCA="C", "D", "A")
          case this.oParentObject.w_PTROWORD = -2
            * --- Manutenzione Distinte
            * --- Read from DIS_TINT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DIS_TINT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DIDATVAL,DINUMERO,DIBANRIF"+;
                " from "+i_cTable+" DIS_TINT where ";
                    +"DINUMDIS = "+cp_ToStrODBC(this.oParentObject.w_PTSERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DIDATVAL,DINUMERO,DIBANRIF;
                from (i_cTable) where;
                    DINUMDIS = this.oParentObject.w_PTSERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_DATREG = NVL(cp_ToDate(_read_.DIDATVAL),cp_NullValue(_read_.DIDATVAL))
              this.oParentObject.w_NUMRER = NVL(cp_ToDate(_read_.DINUMERO),cp_NullValue(_read_.DINUMERO))
              this.oParentObject.w_BANRIF = NVL(cp_ToDate(_read_.DIBANRIF),cp_NullValue(_read_.DIBANRIF))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.oParentObject.w_NUMEFF = this.oParentObject.w_PTNUMEFF
            * --- Valuta nazionale dell'esercizio in corso..
        endcase
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VACAOVAL,VADECTOT,VASIMVAL"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALNAZ);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VACAOVAL,VADECTOT,VASIMVAL;
            from (i_cTable) where;
                VACODVAL = this.oParentObject.w_VALNAZ;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_CAONAZ = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
          this.oParentObject.w_DECTOP = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          this.oParentObject.w_SIMNAZ = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Devo Calcolarmi il totale partite esclusa la scadenza che sto modificando
        if this.oParentObject.w_ROWNUM>0
          * --- Variabile dichiarata nell'area Manuale Declaire Variabiles
          * --- Select from PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select PTTOTIMP,PT_SEGNO,PTCODVAL,PTCAOAPE,PTDATAPE  from "+i_cTable+" PAR_TITE ";
                +" where PTSERIAL="+cp_ToStrODBC(this.oParentObject.w_PTSERIAL)+" AND PTROWORD="+cp_ToStrODBC(this.oParentObject.w_PTROWORD)+" AND "+cp_ToStrODBC(this.oParentObject.w_ROWNUM)+"<>CPROWNUM";
                 ,"_Curs_PAR_TITE")
          else
            select PTTOTIMP,PT_SEGNO,PTCODVAL,PTCAOAPE,PTDATAPE from (i_cTable);
             where PTSERIAL=this.oParentObject.w_PTSERIAL AND PTROWORD=this.oParentObject.w_PTROWORD AND this.oParentObject.w_ROWNUM<>CPROWNUM;
              into cursor _Curs_PAR_TITE
          endif
          if used('_Curs_PAR_TITE')
            select _Curs_PAR_TITE
            locate for 1=1
            do while not(eof())
            * --- Converte in Moneta di Conto (va fatto riga x riga in quanto, sebbene della stessa valuta,  il cambio di apertura puo' differire)
            this.w_TOTIMP = Nvl(_Curs_PAR_TITE.PTTOTIMP,0)
            if _Curs_PAR_TITE.PTCODVAL<>this.oParentObject.w_VALNAZ AND this.oParentObject.w_PTROWORD > 0
              this.w_TOTIMP = VAL2MON(this.w_TOTIMP, Nvl(_Curs_PAR_TITE.PTCAOAPE,0), this.oParentObject.w_CAONAZ, _Curs_PAR_TITE.PTDATAPE, this.oParentObject.w_VALNAZ)
            endif
            this.oParentObject.w_TOTDOC = this.oParentObject.w_TOTDOC + this.w_TOTIMP* iif(_Curs_PAR_TITE.PT_SEGNO="D",1,-1)
              select _Curs_PAR_TITE
              continue
            enddo
            use
          endif
        endif
        SELECT (this.w_PADRE.cTrsName)
        GO TOP
        this.oParentObject.w_TOTALE = 0
        this.oParentObject.w_TOTRIG = 0
        SCAN FOR t_PTTOTIMP<>0 AND NOT EMPTY(t_PTDATSCA) AND NOT EMPTY(t_PTNUMPAR)
        this.w_PADRE.WorkFromTrs()     
        this.w_LROWNUM = CPROWNUM
        * --- Read from VALDATPA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALDATPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALDATPA_idx,2],.t.,this.VALDATPA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DVSERIAL"+;
            " from "+i_cTable+" VALDATPA where ";
                +"DVPASERI = "+cp_ToStrODBC(this.oParentObject.w_PTSERIAL);
                +" and DVROWORD = "+cp_ToStrODBC(this.oParentObject.w_PTROWORD);
                +" and DVROWNUM = "+cp_ToStrODBC(this.w_LROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DVSERIAL;
            from (i_cTable) where;
                DVPASERI = this.oParentObject.w_PTSERIAL;
                and DVROWORD = this.oParentObject.w_PTROWORD;
                and DVROWNUM = this.w_LROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DVSERIAL = NVL(cp_ToDate(_read_.DVSERIAL),cp_NullValue(_read_.DVSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.w_SERDIS)
          this.w_APPO1 = SPACE(10)
          if not empty(this.oParentObject.w_PTSERRIF)
            * --- Leggo riferimenti Partita con PTROWORD=-3 da contabilizzazione indiretta effetti
            * --- Read from PAR_TITE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PTSERIAL,PTROWORD,CPROWNUM"+;
                " from "+i_cTable+" PAR_TITE where ";
                    +"PTDATSCA = "+cp_ToStrODBC(this.oParentObject.w_PTDATSCA);
                    +" and PTNUMPAR = "+cp_ToStrODBC(this.oParentObject.w_PTNUMPAR);
                    +" and PTTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PTTIPCON);
                    +" and PTCODCON = "+cp_ToStrODBC(this.oParentObject.w_PTCODCON);
                    +" and PTCODVAL = "+cp_ToStrODBC(this.oParentObject.w_PTCODVAL);
                    +" and PTROWORD = "+cp_ToStrODBC(-3);
                    +" and PTFLINDI = "+cp_ToStrODBC("S");
                    +" and PTSERRIF = "+cp_ToStrODBC(this.oParentObject.w_PTSERRIF);
                    +" and PTORDRIF = "+cp_ToStrODBC(this.oParentObject.w_PTORDRIF);
                    +" and PTNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_PTNUMRIF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PTSERIAL,PTROWORD,CPROWNUM;
                from (i_cTable) where;
                    PTDATSCA = this.oParentObject.w_PTDATSCA;
                    and PTNUMPAR = this.oParentObject.w_PTNUMPAR;
                    and PTTIPCON = this.oParentObject.w_PTTIPCON;
                    and PTCODCON = this.oParentObject.w_PTCODCON;
                    and PTCODVAL = this.oParentObject.w_PTCODVAL;
                    and PTROWORD = -3;
                    and PTFLINDI = "S";
                    and PTSERRIF = this.oParentObject.w_PTSERRIF;
                    and PTORDRIF = this.oParentObject.w_PTORDRIF;
                    and PTNUMRIF = this.oParentObject.w_PTNUMRIF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_LSERRIF = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
              this.w_LORDRIF = NVL(cp_ToDate(_read_.PTROWORD),cp_NullValue(_read_.PTROWORD))
              this.w_LNUMRIF = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Leggo riferimento e numero effetti dalla partita della distinta
            this.w_FLINDI = "S"
            if I_rows=0
              this.w_LSERRIF = this.oParentObject.w_PTSERRIF
              this.w_LORDRIF = this.oParentObject.w_PTORDRIF
              this.w_LNUMRIF = this.oParentObject.w_PTNUMRIF
              this.w_FLINDI = " "
            endif
            * --- Read from PAR_TITE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PTSERIAL,PTNUMEFF,PTFLIMPE,PTRIFIND"+;
                " from "+i_cTable+" PAR_TITE where ";
                    +"PTDATSCA = "+cp_ToStrODBC(this.oParentObject.w_PTDATSCA);
                    +" and PTNUMPAR = "+cp_ToStrODBC(this.oParentObject.w_PTNUMPAR);
                    +" and PTTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PTTIPCON);
                    +" and PTCODCON = "+cp_ToStrODBC(this.oParentObject.w_PTCODCON);
                    +" and PTCODVAL = "+cp_ToStrODBC(this.oParentObject.w_PTCODVAL);
                    +" and PTROWORD = "+cp_ToStrODBC(-2);
                    +" and PTFLINDI = "+cp_ToStrODBC(this.w_FLINDI);
                    +" and PTSERRIF = "+cp_ToStrODBC(this.w_LSERRIF);
                    +" and PTORDRIF = "+cp_ToStrODBC(this.w_LORDRIF);
                    +" and PTNUMRIF = "+cp_ToStrODBC(this.w_LNUMRIF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PTSERIAL,PTNUMEFF,PTFLIMPE,PTRIFIND;
                from (i_cTable) where;
                    PTDATSCA = this.oParentObject.w_PTDATSCA;
                    and PTNUMPAR = this.oParentObject.w_PTNUMPAR;
                    and PTTIPCON = this.oParentObject.w_PTTIPCON;
                    and PTCODCON = this.oParentObject.w_PTCODCON;
                    and PTCODVAL = this.oParentObject.w_PTCODVAL;
                    and PTROWORD = -2;
                    and PTFLINDI = this.w_FLINDI;
                    and PTSERRIF = this.w_LSERRIF;
                    and PTORDRIF = this.w_LORDRIF;
                    and PTNUMRIF = this.w_LNUMRIF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_APPO1 = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
              this.oParentObject.w_NUMEFF = NVL(cp_ToDate(_read_.PTNUMEFF),cp_NullValue(_read_.PTNUMEFF))
              this.w_FLIMPE = NVL(cp_ToDate(_read_.PTFLIMPE),cp_NullValue(_read_.PTFLIMPE))
              this.w_RIFCON = NVL(cp_ToDate(_read_.PTRIFIND),cp_NullValue(_read_.PTRIFIND))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            * --- Se pregresso non ho i riferimenti
            * --- Read from PAR_TITE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PTSERIAL,PTNUMEFF,PTFLIMPE,PTRIFIND"+;
                " from "+i_cTable+" PAR_TITE where ";
                    +"PTDATSCA = "+cp_ToStrODBC(this.oParentObject.w_PTDATSCA);
                    +" and PTNUMPAR = "+cp_ToStrODBC(this.oParentObject.w_PTNUMPAR);
                    +" and PTTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PTTIPCON);
                    +" and PTCODCON = "+cp_ToStrODBC(this.oParentObject.w_PTCODCON);
                    +" and PTCODVAL = "+cp_ToStrODBC(this.oParentObject.w_PTCODVAL);
                    +" and PTROWORD = "+cp_ToStrODBC(-2);
                    +" and PTFLINDI = "+cp_ToStrODBC(this.w_FLINDI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PTSERIAL,PTNUMEFF,PTFLIMPE,PTRIFIND;
                from (i_cTable) where;
                    PTDATSCA = this.oParentObject.w_PTDATSCA;
                    and PTNUMPAR = this.oParentObject.w_PTNUMPAR;
                    and PTTIPCON = this.oParentObject.w_PTTIPCON;
                    and PTCODCON = this.oParentObject.w_PTCODCON;
                    and PTCODVAL = this.oParentObject.w_PTCODVAL;
                    and PTROWORD = -2;
                    and PTFLINDI = this.w_FLINDI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_APPO1 = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
              this.oParentObject.w_NUMEFF = NVL(cp_ToDate(_read_.PTNUMEFF),cp_NullValue(_read_.PTNUMEFF))
              this.w_FLIMPE = NVL(cp_ToDate(_read_.PTFLIMPE),cp_NullValue(_read_.PTFLIMPE))
              this.w_RIFCON = NVL(cp_ToDate(_read_.PTRIFIND),cp_NullValue(_read_.PTRIFIND))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if NOT EMPTY(this.w_APPO1)
            this.oParentObject.w_PTNUMDIS = this.w_APPO1
            * --- Manutenzione Distinte
            * --- Read from DIS_TINT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DIS_TINT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DINUMERO,DI__ANNO,DIDATDIS,DITIPDIS,DINUMDIS"+;
                " from "+i_cTable+" DIS_TINT where ";
                    +"DINUMDIS = "+cp_ToStrODBC(this.w_APPO1);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DINUMERO,DI__ANNO,DIDATDIS,DITIPDIS,DINUMDIS;
                from (i_cTable) where;
                    DINUMDIS = this.w_APPO1;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_NUMDIS = NVL(cp_ToDate(_read_.DINUMERO),cp_NullValue(_read_.DINUMERO))
              this.oParentObject.w_ANNDIS = NVL(cp_ToDate(_read_.DI__ANNO),cp_NullValue(_read_.DI__ANNO))
              this.oParentObject.w_DATDIS = NVL(cp_ToDate(_read_.DIDATDIS),cp_NullValue(_read_.DIDATDIS))
              this.oParentObject.w_TIPDIS = NVL(cp_ToDate(_read_.DITIPDIS),cp_NullValue(_read_.DITIPDIS))
              this.oParentObject.w_CODDIS = NVL(cp_ToDate(_read_.DINUMDIS),cp_NullValue(_read_.DINUMDIS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if NOT EMPTY(this.w_RIFCON)
              * --- Read from PNT_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PNT_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PNNUMDOC,PNALFDOC,PNDESSUP,PNNUMRER,PNDATREG,PNVALNAZ,PNCODVAL,PNCAOVAL,PNRIFDIS,PNDATDOC,PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05,PNAGG_06"+;
                  " from "+i_cTable+" PNT_MAST where ";
                      +"PNSERIAL = "+cp_ToStrODBC(this.w_RIFCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PNNUMDOC,PNALFDOC,PNDESSUP,PNNUMRER,PNDATREG,PNVALNAZ,PNCODVAL,PNCAOVAL,PNRIFDIS,PNDATDOC,PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05,PNAGG_06;
                  from (i_cTable) where;
                      PNSERIAL = this.w_RIFCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_RIFNUM = NVL(cp_ToDate(_read_.PNNUMDOC),cp_NullValue(_read_.PNNUMDOC))
                this.oParentObject.w_RIFALF = NVL(cp_ToDate(_read_.PNALFDOC),cp_NullValue(_read_.PNALFDOC))
                this.oParentObject.w_DESCRI = NVL(cp_ToDate(_read_.PNDESSUP),cp_NullValue(_read_.PNDESSUP))
                this.oParentObject.w_NUMRER = NVL(cp_ToDate(_read_.PNNUMRER),cp_NullValue(_read_.PNNUMRER))
                this.oParentObject.w_DATREG = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
                this.oParentObject.w_VALNAZ = NVL(cp_ToDate(_read_.PNVALNAZ),cp_NullValue(_read_.PNVALNAZ))
                this.oParentObject.w_CODVAL = NVL(cp_ToDate(_read_.PNCODVAL),cp_NullValue(_read_.PNCODVAL))
                this.oParentObject.w_CAOVAL = NVL(cp_ToDate(_read_.PNCAOVAL),cp_NullValue(_read_.PNCAOVAL))
                this.w_SERDIS = NVL(cp_ToDate(_read_.PNRIFDIS),cp_NullValue(_read_.PNRIFDIS))
                this.oParentObject.w_DATDOC = NVL(cp_ToDate(_read_.PNDATDOC),cp_NullValue(_read_.PNDATDOC))
                this.oParentObject.w_PNAGG_01 = NVL(cp_ToDate(_read_.PNAGG_01),cp_NullValue(_read_.PNAGG_01))
                this.oParentObject.w_PNAGG_02 = NVL(cp_ToDate(_read_.PNAGG_02),cp_NullValue(_read_.PNAGG_02))
                this.oParentObject.w_PNAGG_03 = NVL(cp_ToDate(_read_.PNAGG_03),cp_NullValue(_read_.PNAGG_03))
                this.oParentObject.w_PNAGG_04 = NVL(cp_ToDate(_read_.PNAGG_04),cp_NullValue(_read_.PNAGG_04))
                this.oParentObject.w_PNAGG_05 = NVL(cp_ToDate(_read_.PNAGG_05),cp_NullValue(_read_.PNAGG_05))
                this.oParentObject.w_PNAGG_06 = NVL(cp_ToDate(_read_.PNAGG_06),cp_NullValue(_read_.PNAGG_06))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          else
            * --- Distinta Contab.Indiretta
            * --- Read from CON_INDI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CON_INDI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CON_INDI_idx,2],.t.,this.CON_INDI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CINUMERO,CICODESE,CIDATREG"+;
                " from "+i_cTable+" CON_INDI where ";
                    +"CISERIAL = "+cp_ToStrODBC(this.w_SERDIS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CINUMERO,CICODESE,CIDATREG;
                from (i_cTable) where;
                    CISERIAL = this.w_SERDIS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_NUMIND = NVL(cp_ToDate(_read_.CINUMERO),cp_NullValue(_read_.CINUMERO))
              this.oParentObject.w_ESEIND = NVL(cp_ToDate(_read_.CICODESE),cp_NullValue(_read_.CICODESE))
              this.oParentObject.w_DATIND = NVL(cp_ToDate(_read_.CIDATREG),cp_NullValue(_read_.CIDATREG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
        this.oParentObject.w_RIG = IIF(this.oParentObject.w_PT_SEGNO="A",1,-1 )*IIF( this.oParentObject.w_PTCODVAL=this.oParentObject.w_VALNAZ , this.oParentObject.w_PTTOTIMP , VAL2MON(this.oParentObject.w_PTTOTIMP, this.oParentObject.w_PTCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_PTDATSCA, this.oParentObject.w_VALNAZ, this.oParentObject.w_DECTOP)) 
        this.oParentObject.w_TOTRIG = this.oParentObject.w_TOTRIG + this.oParentObject.w_RIG
        this.oParentObject.w_OTOTIMP = this.oParentObject.w_PTTOTIMP
        this.oParentObject.w_DATSCA = this.oParentObject.w_PTDATSCA
        SELECT (this.w_PADRE.cTrsName)
        this.w_PADRE.TrsFromWork()     
        ENDSCAN
        this.w_PADRE.Repos()     
      case this.pOper="UPDA"
        this.w_OK = .T.
        this.w_MESS = " "
        if this.oParentObject.w_PTROWORD=-2
          this.w_MESS = ah_Msgformat("Scadenza associata ad una distinta%0Non modificabile")
          this.w_OK = .F.
        else
          this.w_PADRE.Markpos()     
          SELECT (this.w_PADRE.cTrsName)
          * --- devo verificare anche le eventuali Partite Cancellate e non Ripristinate
          OLDSET=SET("DELETED")
          SET DELETED OFF
          GO TOP
          SCAN FOR t_PTTOTIMP<>0 AND NOT EMPTY(t_PTDATSCA) AND NOT EMPTY(t_PTNUMPAR) 
          if i_SRV="U" OR DELETED()
            do case
              case EMPTY(NVL(t_PTMODPAG," ")) AND NOT DELETED()
                this.w_MESS = ah_Msgformat("Scadenza del %1%0Inserire codice tipo pagamento", DTOC(t_DATSCA) )
                this.w_OK = .F.
              case NOT EMPTY(NVL(t_PTNUMDIS," "))
                if DELETED()
                  this.w_MESS = ah_Msgformat("Scadenza del %1 associata ad una distinta non eliminabile", DTOC(t_DATSCA) )
                else
                  this.w_MESS = ah_Msgformat("Scadenza del %1 associata ad una distinta non modificabile", DTOC(t_DATSCA) )
                endif
                this.w_OK = .F.
              case CHKPARD(t_PTDATSCA, t_PTNUMPAR, this.oParentObject.w_PTTIPCON, this.oParentObject.w_PTCODCON, t_PTCODVAL,Nvl(t_PTSERRIF," "),Nvl(t_PTORDRIF,0),Nvl(t_PTNUMRIF,0))>0
                if DELETED()
                  this.w_MESS = ah_Msgformat("Scadenza del %1 associata ad una distinta non eliminabile", DTOC(t_DATSCA) )
                else
                  this.w_MESS = ah_Msgformat("Scadenza del %1 associata ad una distinta non modificabile", DTOC(t_DATSCA) )
                endif
                this.w_OK = .F.
            endcase
            SELECT (this.w_PADRE.cTrsName)
            if Not this.w_OK
              EXIT
            endif
          endif
          if NOT DELETED()
            this.w_RIGPAR = NVL(t_PTTOTIMP,0)
            * --- Converte in Moneta di Conto (va fatto riga x riga in quanto, sebbene della stessa valuta,  il cambio di apertura puo' differire)
            if t_PTCODVAL<>this.oParentObject.w_VALNAZ AND this.oParentObject.w_PTROWORD > 0
              this.w_RIGPAR = VAL2MON(this.w_RIGPAR, t_PTCAOAPE, this.oParentObject.w_CAONAZ, t_PTDATAPE, this.oParentObject.w_VALNAZ)
              SELECT (this.w_PADRE.cTrsName)
            endif
            this.w_TOTPAR = this.w_TOTPAR + (this.w_RIGPAR * IIF(NVL(t_PT_SEGNO," ")="D", 1, -1))
            if EMPTY(NVL(t_PTCODVAL,"   ")) 
              this.w_MESS = ah_Msgformat("Valuta scadenza del %1 inesistente", DTOC(t_DATSCA) )
              this.w_OK = .F.
              EXIT
            endif
            if NVL(t_TIPPAG,SPACE(2))="BO" and Empty(Nvl(t_PTNUMCOR," ")) and NVL(t_PT_SEGNO," ")="A"
              this.w_MESS = ah_Msgformat("Pagamento di tipo bonifico:%0Inserire il conto corrente")
              this.w_OK = .F.
              EXIT
            endif
          endif
          ENDSCAN
          SET DELETED &OLDSET
          this.w_PADRE.Repos()     
          * --- Aggiungo righe non considerate nel dettaglio partite
          this.w_TOTPAR = this.oParentObject.w_TOTDOC+this.w_TOTPAR
          * --- Controlla solo se da Primanota o Scadenze Diverse
          if (this.oParentObject.w_PTROWORD>0 OR this.oParentObject.w_PTROWORD=-1) and Empty(this.oParentObject.w_DVSERIAL)
            this.w_APPO = this.oParentObject.w_IMPRIG * IIF(this.oParentObject.w_FLDAVE="D", 1,-1)
            if this.w_OK=.T. AND cp_ROUND(this.w_APPO, this.oParentObject.w_DECTOP) <> cp_ROUND(this.w_TOTPAR, this.oParentObject.w_DECTOP)
              this.w_MESS = ah_Msgformat("Importi partite errati%0Riga P.N.: %1%0%3Partite: %2", ALLTRIM(STR(this.oParentObject.w_IMPRIG,18,this.oParentObject.w_DECTOP)), ALLTRIM(STR(ABS(this.w_TOTPAR),18,this.oParentObject.w_DECTOP)),SPACE(5) ) 
              this.w_OK = .F.
            endif
          endif
        endif
        if this.w_OK=.F.
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='PNT_DETT'
    this.cWorkTables[3]='SCA_VARI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='DIS_TINT'
    this.cWorkTables[6]='CON_INDI'
    this.cWorkTables[7]='PAR_TITE'
    this.cWorkTables[8]='VALDATPA'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
