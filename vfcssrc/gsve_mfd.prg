* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_mfd                                                        *
*              Dettaglio fatture differite                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_40]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2012-10-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsve_mfd")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsve_mfd")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsve_mfd")
  return

* --- Class definition
define class tgsve_mfd as StdPCForm
  Width  = 792
  Height = 383
  Top    = 60
  Left   = 17
  cComment = "Dettaglio fatture differite"
  cPrg = "gsve_mfd"
  HelpContextID=46805609
  add object cnt as tcgsve_mfd
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsve_mfd as PCContext
  w_DPSERIAL = space(10)
  w_CPROWORD = 0
  w_DPSERDOC = space(10)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = space(8)
  w_DPNUMDOC = 0
  w_DPALFDOC = space(10)
  w_DPDATDOC = space(8)
  w_DPTIPCLF = space(1)
  w_DPCODCLF = space(15)
  w_DESCRI = space(40)
  w_GENPRO = space(1)
  w_FLCONT = space(1)
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_TIPFAT = space(1)
  w_FLSPERIP = space(1)
  w_RIFDCO = space(10)
  proc Save(i_oFrom)
    this.w_DPSERIAL = i_oFrom.w_DPSERIAL
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_DPSERDOC = i_oFrom.w_DPSERDOC
    this.w_NUMDOC = i_oFrom.w_NUMDOC
    this.w_ALFDOC = i_oFrom.w_ALFDOC
    this.w_DATDOC = i_oFrom.w_DATDOC
    this.w_DPNUMDOC = i_oFrom.w_DPNUMDOC
    this.w_DPALFDOC = i_oFrom.w_DPALFDOC
    this.w_DPDATDOC = i_oFrom.w_DPDATDOC
    this.w_DPTIPCLF = i_oFrom.w_DPTIPCLF
    this.w_DPCODCLF = i_oFrom.w_DPCODCLF
    this.w_DESCRI = i_oFrom.w_DESCRI
    this.w_GENPRO = i_oFrom.w_GENPRO
    this.w_FLCONT = i_oFrom.w_FLCONT
    this.w_CODVAL = i_oFrom.w_CODVAL
    this.w_CAOVAL = i_oFrom.w_CAOVAL
    this.w_TIPFAT = i_oFrom.w_TIPFAT
    this.w_FLSPERIP = i_oFrom.w_FLSPERIP
    this.w_RIFDCO = i_oFrom.w_RIFDCO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DPSERIAL = this.w_DPSERIAL
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_DPSERDOC = this.w_DPSERDOC
    i_oTo.w_NUMDOC = this.w_NUMDOC
    i_oTo.w_ALFDOC = this.w_ALFDOC
    i_oTo.w_DATDOC = this.w_DATDOC
    i_oTo.w_DPNUMDOC = this.w_DPNUMDOC
    i_oTo.w_DPALFDOC = this.w_DPALFDOC
    i_oTo.w_DPDATDOC = this.w_DPDATDOC
    i_oTo.w_DPTIPCLF = this.w_DPTIPCLF
    i_oTo.w_DPCODCLF = this.w_DPCODCLF
    i_oTo.w_DESCRI = this.w_DESCRI
    i_oTo.w_GENPRO = this.w_GENPRO
    i_oTo.w_FLCONT = this.w_FLCONT
    i_oTo.w_CODVAL = this.w_CODVAL
    i_oTo.w_CAOVAL = this.w_CAOVAL
    i_oTo.w_TIPFAT = this.w_TIPFAT
    i_oTo.w_FLSPERIP = this.w_FLSPERIP
    i_oTo.w_RIFDCO = this.w_RIFDCO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsve_mfd as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 792
  Height = 383
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-17"
  HelpContextID=46805609
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DET_DIFF_IDX = 0
  CONTI_IDX = 0
  DOC_MAST_IDX = 0
  cFile = "DET_DIFF"
  cKeySelect = "DPSERIAL"
  cKeyWhere  = "DPSERIAL=this.w_DPSERIAL"
  cKeyDetail  = "DPSERIAL=this.w_DPSERIAL"
  cKeyWhereODBC = '"DPSERIAL="+cp_ToStrODBC(this.w_DPSERIAL)';

  cKeyDetailWhereODBC = '"DPSERIAL="+cp_ToStrODBC(this.w_DPSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DET_DIFF.DPSERIAL="+cp_ToStrODBC(this.w_DPSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DET_DIFF.CPROWORD '
  cPrg = "gsve_mfd"
  cComment = "Dettaglio fatture differite"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 16
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DPSERIAL = space(10)
  w_CPROWORD = 0
  w_DPSERDOC = space(10)
  o_DPSERDOC = space(10)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod('  /  /  ')
  w_DPNUMDOC = 0
  w_DPALFDOC = space(10)
  w_DPDATDOC = ctod('  /  /  ')
  w_DPTIPCLF = space(1)
  w_DPCODCLF = space(15)
  w_DESCRI = space(40)
  w_GENPRO = space(1)
  w_FLCONT = space(1)
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_TIPFAT = space(1)
  w_FLSPERIP = space(1)
  w_RIFDCO = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsve_mfd
  procedure F6()
     IF this.w_FLCONT = "S" or this.w_GENPRO = "S"
       IF this.w_FLCONT = "S"
         AH_ERRORMSG("Questo documento � contabilizzato e non pu� essere eliminato","!","")
       ELSE
         IF this.w_GENPRO = "S"
           AH_ERRORMSG("Questo documento ha generato provvigioni e non pu� essere eliminato","!","")
         ENDIF
       ENDIF
     ELSE
       DoDefault()
     ENDIF
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_mfdPag1","gsve_mfd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='DET_DIFF'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DET_DIFF_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DET_DIFF_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsve_mfd'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_10_joined
    link_2_10_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DET_DIFF where DPSERIAL=KeySet.DPSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DET_DIFF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_DIFF_IDX,2],this.bLoadRecFilter,this.DET_DIFF_IDX,"gsve_mfd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DET_DIFF')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DET_DIFF.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DET_DIFF '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_10_joined=this.AddJoinedLink_2_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DPSERIAL',this.w_DPSERIAL  )
      select * from (i_cTable) DET_DIFF where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DPSERIAL = NVL(DPSERIAL,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DET_DIFF')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_NUMDOC = 0
          .w_ALFDOC = space(10)
          .w_DATDOC = ctod("  /  /  ")
          .w_DESCRI = space(40)
          .w_GENPRO = space(1)
          .w_FLCONT = space(1)
          .w_CODVAL = space(3)
          .w_CAOVAL = 0
          .w_TIPFAT = space(1)
          .w_RIFDCO = space(10)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DPSERDOC = NVL(DPSERDOC,space(10))
          if link_2_2_joined
            this.w_DPSERDOC = NVL(MVSERIAL202,NVL(this.w_DPSERDOC,space(10)))
            this.w_NUMDOC = NVL(MVNUMDOC202,0)
            this.w_ALFDOC = NVL(MVALFDOC202,space(10))
            this.w_DATDOC = NVL(cp_ToDate(MVDATDOC202),ctod("  /  /  "))
            this.w_GENPRO = NVL(MVGENPRO202,space(1))
            this.w_FLCONT = NVL(MVFLCONT202,space(1))
            this.w_CODVAL = NVL(MVCODVAL202,space(3))
            this.w_CAOVAL = NVL(MVCAOVAL202,0)
            this.w_RIFDCO = NVL(MVRIFDCO202,space(10))
          else
          .link_2_2('Load')
          endif
          .w_DPNUMDOC = NVL(DPNUMDOC,0)
          .w_DPALFDOC = NVL(DPALFDOC,space(10))
          .w_DPDATDOC = NVL(cp_ToDate(DPDATDOC),ctod("  /  /  "))
          .w_DPTIPCLF = NVL(DPTIPCLF,space(1))
          .w_DPCODCLF = NVL(DPCODCLF,space(15))
          if link_2_10_joined
            this.w_DPCODCLF = NVL(ANCODICE210,NVL(this.w_DPCODCLF,space(15)))
            this.w_DESCRI = NVL(ANDESCRI210,space(40))
            this.w_TIPFAT = NVL(ANTIPFAT210,space(1))
          else
          .link_2_10('Load')
          endif
        .w_FLSPERIP = IIF( Not Empty( CHKRIPSPE( .w_DPSERDOC ) ) , 'S' , 'N')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_12.enabled = .oPgFrm.Page1.oPag.oBtn_2_12.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DPSERIAL=space(10)
      .w_CPROWORD=10
      .w_DPSERDOC=space(10)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_DPNUMDOC=0
      .w_DPALFDOC=space(10)
      .w_DPDATDOC=ctod("  /  /  ")
      .w_DPTIPCLF=space(1)
      .w_DPCODCLF=space(15)
      .w_DESCRI=space(40)
      .w_GENPRO=space(1)
      .w_FLCONT=space(1)
      .w_CODVAL=space(3)
      .w_CAOVAL=0
      .w_TIPFAT=space(1)
      .w_FLSPERIP=space(1)
      .w_RIFDCO=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_DPSERDOC))
         .link_2_2('Full')
        endif
        .DoRTCalc(4,11,.f.)
        if not(empty(.w_DPCODCLF))
         .link_2_10('Full')
        endif
        .DoRTCalc(12,17,.f.)
        .w_FLSPERIP = IIF( Not Empty( CHKRIPSPE( .w_DPSERDOC ) ) , 'S' , 'N')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DET_DIFF')
    this.DoRTCalc(19,19,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_12.enabled = this.oPgFrm.Page1.oPag.oBtn_2_12.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_2_12.enabled = .Page1.oPag.oBtn_2_12.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DET_DIFF',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DET_DIFF_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPSERIAL,"DPSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_NUMDOC N(15);
      ,t_ALFDOC C(10);
      ,t_DATDOC D(8);
      ,t_DPCODCLF C(15);
      ,t_DESCRI C(40);
      ,CPROWNUM N(10);
      ,t_DPSERDOC C(10);
      ,t_DPNUMDOC N(15);
      ,t_DPALFDOC C(10);
      ,t_DPDATDOC D(8);
      ,t_DPTIPCLF C(1);
      ,t_GENPRO C(1);
      ,t_FLCONT C(1);
      ,t_CODVAL C(3);
      ,t_CAOVAL N(12,7);
      ,t_TIPFAT C(1);
      ,t_FLSPERIP C(1);
      ,t_RIFDCO C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsve_mfdbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_3.controlsource=this.cTrsName+'.t_NUMDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_4.controlsource=this.cTrsName+'.t_ALFDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDATDOC_2_5.controlsource=this.cTrsName+'.t_DATDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDPCODCLF_2_10.controlsource=this.cTrsName+'.t_DPCODCLF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_11.controlsource=this.cTrsName+'.t_DESCRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(63)
    this.AddVLine(185)
    this.AddVLine(271)
    this.AddVLine(353)
    this.AddVLine(485)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DET_DIFF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_DIFF_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DET_DIFF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_DIFF_IDX,2])
      *
      * insert into DET_DIFF
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DET_DIFF')
        i_extval=cp_InsertValODBCExtFlds(this,'DET_DIFF')
        i_cFldBody=" "+;
                  "(DPSERIAL,CPROWORD,DPSERDOC,DPNUMDOC,DPALFDOC"+;
                  ",DPDATDOC,DPTIPCLF,DPCODCLF,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DPSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_DPSERDOC)+","+cp_ToStrODBC(this.w_DPNUMDOC)+","+cp_ToStrODBC(this.w_DPALFDOC)+;
             ","+cp_ToStrODBC(this.w_DPDATDOC)+","+cp_ToStrODBC(this.w_DPTIPCLF)+","+cp_ToStrODBCNull(this.w_DPCODCLF)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DET_DIFF')
        i_extval=cp_InsertValVFPExtFlds(this,'DET_DIFF')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DPSERIAL',this.w_DPSERIAL)
        INSERT INTO (i_cTable) (;
                   DPSERIAL;
                  ,CPROWORD;
                  ,DPSERDOC;
                  ,DPNUMDOC;
                  ,DPALFDOC;
                  ,DPDATDOC;
                  ,DPTIPCLF;
                  ,DPCODCLF;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DPSERIAL;
                  ,this.w_CPROWORD;
                  ,this.w_DPSERDOC;
                  ,this.w_DPNUMDOC;
                  ,this.w_DPALFDOC;
                  ,this.w_DPDATDOC;
                  ,this.w_DPTIPCLF;
                  ,this.w_DPCODCLF;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DET_DIFF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_DIFF_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CPROWORD>0 AND (NOT EMPTY(t_DPSERDOC) OR t_DPNUMDOC<>0)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DET_DIFF')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DET_DIFF')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD>0 AND (NOT EMPTY(t_DPSERDOC) OR t_DPNUMDOC<>0)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DET_DIFF
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DET_DIFF')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DPSERDOC="+cp_ToStrODBCNull(this.w_DPSERDOC)+;
                     ",DPNUMDOC="+cp_ToStrODBC(this.w_DPNUMDOC)+;
                     ",DPALFDOC="+cp_ToStrODBC(this.w_DPALFDOC)+;
                     ",DPDATDOC="+cp_ToStrODBC(this.w_DPDATDOC)+;
                     ",DPTIPCLF="+cp_ToStrODBC(this.w_DPTIPCLF)+;
                     ",DPCODCLF="+cp_ToStrODBCNull(this.w_DPCODCLF)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DET_DIFF')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,DPSERDOC=this.w_DPSERDOC;
                     ,DPNUMDOC=this.w_DPNUMDOC;
                     ,DPALFDOC=this.w_DPALFDOC;
                     ,DPDATDOC=this.w_DPDATDOC;
                     ,DPTIPCLF=this.w_DPTIPCLF;
                     ,DPCODCLF=this.w_DPCODCLF;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
    this.Calculate_EGGTMWNDOB()
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DET_DIFF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_DIFF_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD>0 AND (NOT EMPTY(t_DPSERDOC) OR t_DPNUMDOC<>0)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DET_DIFF
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD>0 AND (NOT EMPTY(t_DPSERDOC) OR t_DPNUMDOC<>0)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
    this.Calculate_BNNADBMEBU()
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DET_DIFF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_DIFF_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_2_2('Full')
        .DoRTCalc(4,10,.t.)
          .link_2_10('Full')
        .DoRTCalc(12,17,.t.)
        if .o_DPSERDOC<>.w_DPSERDOC
          .w_FLSPERIP = IIF( Not Empty( CHKRIPSPE( .w_DPSERDOC ) ) , 'S' , 'N')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DPSERDOC with this.w_DPSERDOC
      replace t_DPNUMDOC with this.w_DPNUMDOC
      replace t_DPALFDOC with this.w_DPALFDOC
      replace t_DPDATDOC with this.w_DPDATDOC
      replace t_DPTIPCLF with this.w_DPTIPCLF
      replace t_GENPRO with this.w_GENPRO
      replace t_FLCONT with this.w_FLCONT
      replace t_CODVAL with this.w_CODVAL
      replace t_CAOVAL with this.w_CAOVAL
      replace t_TIPFAT with this.w_TIPFAT
      replace t_FLSPERIP with this.w_FLSPERIP
      replace t_RIFDCO with this.w_RIFDCO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_BNNADBMEBU()
    with this
          * --- Cancella tutti i documenti - gsve_bf3 (T)
          GSVE_BF3(this;
              ,'T';
             )
    endwith
  endproc
  proc Calculate_EGGTMWNDOB()
    with this
          * --- Cancella singolo documento - gsve_bf3 (R)
          GSVE_BF3(this;
              ,'R';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBtn_2_12.enabled =this.oPgFrm.Page1.oPag.oBtn_2_12.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DPSERDOC
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPSERDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPSERDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVNUMDOC,MVALFDOC,MVDATDOC,MVGENPRO,MVFLCONT,MVCODVAL,MVCAOVAL,MVRIFDCO";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_DPSERDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_DPSERDOC)
            select MVSERIAL,MVNUMDOC,MVALFDOC,MVDATDOC,MVGENPRO,MVFLCONT,MVCODVAL,MVCAOVAL,MVRIFDCO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPSERDOC = NVL(_Link_.MVSERIAL,space(10))
      this.w_NUMDOC = NVL(_Link_.MVNUMDOC,0)
      this.w_ALFDOC = NVL(_Link_.MVALFDOC,space(10))
      this.w_DATDOC = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
      this.w_GENPRO = NVL(_Link_.MVGENPRO,space(1))
      this.w_FLCONT = NVL(_Link_.MVFLCONT,space(1))
      this.w_CODVAL = NVL(_Link_.MVCODVAL,space(3))
      this.w_CAOVAL = NVL(_Link_.MVCAOVAL,0)
      this.w_RIFDCO = NVL(_Link_.MVRIFDCO,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_DPSERDOC = space(10)
      endif
      this.w_NUMDOC = 0
      this.w_ALFDOC = space(10)
      this.w_DATDOC = ctod("  /  /  ")
      this.w_GENPRO = space(1)
      this.w_FLCONT = space(1)
      this.w_CODVAL = space(3)
      this.w_CAOVAL = 0
      this.w_RIFDCO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPSERDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DOC_MAST_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.MVSERIAL as MVSERIAL202"+ ",link_2_2.MVNUMDOC as MVNUMDOC202"+ ",link_2_2.MVALFDOC as MVALFDOC202"+ ",link_2_2.MVDATDOC as MVDATDOC202"+ ",link_2_2.MVGENPRO as MVGENPRO202"+ ",link_2_2.MVFLCONT as MVFLCONT202"+ ",link_2_2.MVCODVAL as MVCODVAL202"+ ",link_2_2.MVCAOVAL as MVCAOVAL202"+ ",link_2_2.MVRIFDCO as MVRIFDCO202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on DET_DIFF.DPSERDOC=link_2_2.MVSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and DET_DIFF.DPSERDOC=link_2_2.MVSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODCLF
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPFAT";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DPCODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DPTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DPTIPCLF;
                       ,'ANCODICE',this.w_DPCODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPFAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPFAT = NVL(_Link_.ANTIPFAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODCLF = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_TIPFAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_10.ANCODICE as ANCODICE210"+ ",link_2_10.ANDESCRI as ANDESCRI210"+ ",link_2_10.ANTIPFAT as ANTIPFAT210"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_10 on DET_DIFF.DPCODCLF=link_2_10.ANCODICE"+" and DET_DIFF.DPTIPCLF=link_2_10.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_10"
          i_cKey=i_cKey+'+" and DET_DIFF.DPCODCLF=link_2_10.ANCODICE(+)"'+'+" and DET_DIFF.DPTIPCLF=link_2_10.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_3.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_3.value=this.w_NUMDOC
      replace t_NUMDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_4.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_4.value=this.w_ALFDOC
      replace t_ALFDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATDOC_2_5.value==this.w_DATDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATDOC_2_5.value=this.w_DATDOC
      replace t_DATDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATDOC_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPCODCLF_2_10.value==this.w_DPCODCLF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPCODCLF_2_10.value=this.w_DPCODCLF
      replace t_DPCODCLF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPCODCLF_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_11.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_11.value=this.w_DESCRI
      replace t_DESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_11.value
    endif
    cp_SetControlsValueExtFlds(this,'DET_DIFF')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .w_CPROWORD>0 AND (NOT EMPTY(.w_DPSERDOC) OR .w_DPNUMDOC<>0)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DPSERDOC = this.w_DPSERDOC
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD>0 AND (NOT EMPTY(t_DPSERDOC) OR t_DPNUMDOC<>0))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_DPSERDOC=space(10)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_DPNUMDOC=0
      .w_DPALFDOC=space(10)
      .w_DPDATDOC=ctod("  /  /  ")
      .w_DPTIPCLF=space(1)
      .w_DPCODCLF=space(15)
      .w_DESCRI=space(40)
      .w_GENPRO=space(1)
      .w_FLCONT=space(1)
      .w_CODVAL=space(3)
      .w_CAOVAL=0
      .w_TIPFAT=space(1)
      .w_FLSPERIP=space(1)
      .w_RIFDCO=space(10)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_DPSERDOC))
        .link_2_2('Full')
      endif
      .DoRTCalc(4,11,.f.)
      if not(empty(.w_DPCODCLF))
        .link_2_10('Full')
      endif
      .DoRTCalc(12,17,.f.)
        .w_FLSPERIP = IIF( Not Empty( CHKRIPSPE( .w_DPSERDOC ) ) , 'S' , 'N')
    endwith
    this.DoRTCalc(19,19,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_DPSERDOC = t_DPSERDOC
    this.w_NUMDOC = t_NUMDOC
    this.w_ALFDOC = t_ALFDOC
    this.w_DATDOC = t_DATDOC
    this.w_DPNUMDOC = t_DPNUMDOC
    this.w_DPALFDOC = t_DPALFDOC
    this.w_DPDATDOC = t_DPDATDOC
    this.w_DPTIPCLF = t_DPTIPCLF
    this.w_DPCODCLF = t_DPCODCLF
    this.w_DESCRI = t_DESCRI
    this.w_GENPRO = t_GENPRO
    this.w_FLCONT = t_FLCONT
    this.w_CODVAL = t_CODVAL
    this.w_CAOVAL = t_CAOVAL
    this.w_TIPFAT = t_TIPFAT
    this.w_FLSPERIP = t_FLSPERIP
    this.w_RIFDCO = t_RIFDCO
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DPSERDOC with this.w_DPSERDOC
    replace t_NUMDOC with this.w_NUMDOC
    replace t_ALFDOC with this.w_ALFDOC
    replace t_DATDOC with this.w_DATDOC
    replace t_DPNUMDOC with this.w_DPNUMDOC
    replace t_DPALFDOC with this.w_DPALFDOC
    replace t_DPDATDOC with this.w_DPDATDOC
    replace t_DPTIPCLF with this.w_DPTIPCLF
    replace t_DPCODCLF with this.w_DPCODCLF
    replace t_DESCRI with this.w_DESCRI
    replace t_GENPRO with this.w_GENPRO
    replace t_FLCONT with this.w_FLCONT
    replace t_CODVAL with this.w_CODVAL
    replace t_CAOVAL with this.w_CAOVAL
    replace t_TIPFAT with this.w_TIPFAT
    replace t_FLSPERIP with this.w_FLSPERIP
    replace t_RIFDCO with this.w_RIFDCO
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsve_mfdPag1 as StdContainer
  Width  = 788
  height = 383
  stdWidth  = 788
  stdheight = 383
  resizeXpos=452
  resizeYpos=209
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=0, width=776,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Riga",Field2="NUMDOC",Label2="Doc.N.",Field3="ALFDOC",Label3="Ser.",Field4="DATDOC",Label4="Del",Field5="DPCODCLF",Label5="Cliente",Field6="DESCRI",Label6="Descrizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 849786

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=19,;
    width=772+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*16*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=20,width=771+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*16*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_12 as StdButton with uid="BIGHRONQVF",width=48,height=45,;
   left=10, top=329,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedre al documento selezionato";
    , HelpContextID = 87984225;
    , caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_12.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_DPSERDOC, -20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_12.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DPSERDOC))
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsve_mfdBodyRow as CPBodyRowCnt
  Width=762
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="DQGSDZSDWC",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Numero progressivo di riga",;
    HelpContextID = 33178218,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oNUMDOC_2_3 as StdTrsField with uid="LBIKUYVLDZ",rtseq=4,rtrep=.t.,;
    cFormVar="w_NUMDOC",value=0,enabled=.f.,;
    HelpContextID = 177276970,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=53, Top=0

  add object oALFDOC_2_4 as StdTrsField with uid="OLTXJDNWTR",rtseq=5,rtrep=.t.,;
    cFormVar="w_ALFDOC",value=space(10),enabled=.f.,;
    HelpContextID = 177308154,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=175, Top=0, InputMask=replicate('X',10)

  add object oDATDOC_2_5 as StdTrsField with uid="MWDJYUVWND",rtseq=6,rtrep=.t.,;
    cFormVar="w_DATDOC",value=ctod("  /  /  "),enabled=.f.,;
    HelpContextID = 177253578,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=261, Top=0

  add object oDPCODCLF_2_10 as StdTrsField with uid="EVUUXWAFSM",rtseq=11,rtrep=.t.,;
    cFormVar="w_DPCODCLF",value=space(15),enabled=.f.,;
    HelpContextID = 188132740,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=343, Top=0, cSayPict=[p_CLI], cGetPict=[p_CLI], InputMask=replicate('X',15), cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DPTIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_DPCODCLF"

  func oDPCODCLF_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCRI_2_11 as StdTrsField with uid="EQUCXLAZUR",rtseq=12,rtrep=.t.,;
    cFormVar="w_DESCRI",value=space(40),enabled=.f.,;
    HelpContextID = 73513162,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=282, Left=475, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=15
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_mfd','DET_DIFF','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DPSERIAL=DET_DIFF.DPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
