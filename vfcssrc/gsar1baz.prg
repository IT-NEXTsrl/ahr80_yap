* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1baz                                                        *
*              Creazione azienda                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_58]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-13                                                      *
* Last revis.: 2015-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1baz",oParentObject)
return(i_retval)

define class tgsar1baz as StdBatch
  * --- Local variables
  w_CODAZI = space(5)
  w_C1 = 0
  w_C2 = 0
  w_C3 = 0
  w_C4 = 0
  w_C5 = 0
  w_C6 = 0
  w_C7 = 0
  w_C8 = ctod("  /  /  ")
  w_C9 = ctod("  /  /  ")
  w_C10 = space(4)
  w_C11 = space(30)
  w_C12 = space(30)
  w_C13 = space(30)
  w_C14 = space(30)
  w_C15 = space(30)
  w_C16 = space(1)
  * --- WorkFile variables
  PAR_DISB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato sotto transazione da GSUT_BCA (Creazione Azienda)
    * --- Archivi
    *      Distinta Base
    this.w_CODAZI = this.oparentObject.oParentObject.w_CODAZI
    * --- Read from PAR_DISB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_DISB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2],.t.,this.PAR_DISB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDCAUIMP,PDCAUORD,PDMAGIMP,PDMAGORD,PDCAUCAR,PDCAUSCA,PDMAGCAR,PDMAGSCA,PDCAUCA2,PDCAUSC2,PDFLVERI,PDCRIVAL,PDESEINV,PDNUMINV,PDCODLIS,PDCOSPAR"+;
        " from "+i_cTable+" PAR_DISB where ";
            +"PDCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDCAUIMP,PDCAUORD,PDMAGIMP,PDMAGORD,PDCAUCAR,PDCAUSCA,PDMAGCAR,PDMAGSCA,PDCAUCA2,PDCAUSC2,PDFLVERI,PDCRIVAL,PDESEINV,PDNUMINV,PDCODLIS,PDCOSPAR;
        from (i_cTable) where;
            PDCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_C1 = NVL(cp_ToDate(_read_.PDCAUIMP),cp_NullValue(_read_.PDCAUIMP))
      this.w_C2 = NVL(cp_ToDate(_read_.PDCAUORD),cp_NullValue(_read_.PDCAUORD))
      this.w_C3 = NVL(cp_ToDate(_read_.PDMAGIMP),cp_NullValue(_read_.PDMAGIMP))
      this.w_C4 = NVL(cp_ToDate(_read_.PDMAGORD),cp_NullValue(_read_.PDMAGORD))
      this.w_C5 = NVL(cp_ToDate(_read_.PDCAUCAR),cp_NullValue(_read_.PDCAUCAR))
      this.w_C6 = NVL(cp_ToDate(_read_.PDCAUSCA),cp_NullValue(_read_.PDCAUSCA))
      this.w_C7 = NVL(cp_ToDate(_read_.PDMAGCAR),cp_NullValue(_read_.PDMAGCAR))
      this.w_C8 = NVL(cp_ToDate(_read_.PDMAGSCA),cp_NullValue(_read_.PDMAGSCA))
      this.w_C9 = NVL(cp_ToDate(_read_.PDCAUCA2),cp_NullValue(_read_.PDCAUCA2))
      this.w_C10 = NVL(cp_ToDate(_read_.PDCAUSC2),cp_NullValue(_read_.PDCAUSC2))
      this.w_C11 = NVL(cp_ToDate(_read_.PDFLVERI),cp_NullValue(_read_.PDFLVERI))
      this.w_C12 = NVL(cp_ToDate(_read_.PDCRIVAL),cp_NullValue(_read_.PDCRIVAL))
      this.w_C13 = NVL(cp_ToDate(_read_.PDESEINV),cp_NullValue(_read_.PDESEINV))
      this.w_C14 = NVL(cp_ToDate(_read_.PDNUMINV),cp_NullValue(_read_.PDNUMINV))
      this.w_C15 = NVL(cp_ToDate(_read_.PDCODLIS),cp_NullValue(_read_.PDCODLIS))
      this.w_C16 = NVL(cp_ToDate(_read_.PDCOSPAR),cp_NullValue(_read_.PDCOSPAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows<>0
      * --- Try
      local bErr_03C3E540
      bErr_03C3E540=bTrsErr
      this.Try_03C3E540()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03C3E540
      * --- End
      * --- Try
      local bErr_03C3D940
      bErr_03C3D940=bTrsErr
      this.Try_03C3D940()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Impossibile registrare i parametri distinta base: inserire manualmente",,"")
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03C3D940
      * --- End
    endif
    this.oParentObject.VABENE = TRASFARC(i_CODAZI,this.w_codazi,"COE_IMPI")
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"TIP_DIBA"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"TAB_RISO"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"PAR_VARI"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"TABMCICL"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"TAB_CICL"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"DISMBASE"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"DISTBASE"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"COM_VARI"),.F.)
  endproc
  proc Try_03C3E540()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_DISB
    i_nConn=i_TableProp[this.PAR_DISB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_DISB_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PDCODAZI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'PAR_DISB','PDCODAZI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PDCODAZI',this.w_CODAZI)
      insert into (i_cTable) (PDCODAZI &i_ccchkf. );
         values (;
           this.w_CODAZI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03C3D940()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_DISB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_DISB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_DISB_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PDCAUIMP ="+cp_NullLink(cp_ToStrODBC(this.w_C1),'PAR_DISB','PDCAUIMP');
      +",PDCAUORD ="+cp_NullLink(cp_ToStrODBC(this.w_C2),'PAR_DISB','PDCAUORD');
      +",PDMAGIMP ="+cp_NullLink(cp_ToStrODBC(this.w_C3),'PAR_DISB','PDMAGIMP');
      +",PDMAGORD ="+cp_NullLink(cp_ToStrODBC(this.w_C4),'PAR_DISB','PDMAGORD');
      +",PDCAUCAR ="+cp_NullLink(cp_ToStrODBC(this.w_C5),'PAR_DISB','PDCAUCAR');
      +",PDCAUSCA ="+cp_NullLink(cp_ToStrODBC(this.w_C6),'PAR_DISB','PDCAUSCA');
      +",PDMAGCAR ="+cp_NullLink(cp_ToStrODBC(this.w_C7),'PAR_DISB','PDMAGCAR');
      +",PDMAGSCA ="+cp_NullLink(cp_ToStrODBC(this.w_C8),'PAR_DISB','PDMAGSCA');
      +",PDCAUCA2 ="+cp_NullLink(cp_ToStrODBC(this.w_C9),'PAR_DISB','PDCAUCA2');
      +",PDCAUSC2 ="+cp_NullLink(cp_ToStrODBC(this.w_C10),'PAR_DISB','PDCAUSC2');
      +",PDFLVERI ="+cp_NullLink(cp_ToStrODBC(this.w_C11),'PAR_DISB','PDFLVERI');
      +",PDCRIVAL ="+cp_NullLink(cp_ToStrODBC(this.w_C12),'PAR_DISB','PDCRIVAL');
      +",PDCOSPAR ="+cp_NullLink(cp_ToStrODBC(this.w_C16),'PAR_DISB','PDCOSPAR');
          +i_ccchkf ;
      +" where ";
          +"PDCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          PDCAUIMP = this.w_C1;
          ,PDCAUORD = this.w_C2;
          ,PDMAGIMP = this.w_C3;
          ,PDMAGORD = this.w_C4;
          ,PDCAUCAR = this.w_C5;
          ,PDCAUSCA = this.w_C6;
          ,PDMAGCAR = this.w_C7;
          ,PDMAGSCA = this.w_C8;
          ,PDCAUCA2 = this.w_C9;
          ,PDCAUSC2 = this.w_C10;
          ,PDFLVERI = this.w_C11;
          ,PDCRIVAL = this.w_C12;
          ,PDCOSPAR = this.w_C16;
          &i_ccchkf. ;
       where;
          PDCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_DISB'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
