* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kpn                                                        *
*              Prima nota dati aggiuntivi                                      *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_98]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-20                                                      *
* Last revis.: 2008-07-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kpn",oParentObject))

* --- Class definition
define class tgscg_kpn as StdForm
  Top    = 183
  Left   = 115

  * --- Standard Properties
  Width  = 559
  Height = 207
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-07"
  HelpContextID=118872727
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Constant Properties
  _IDX = 0
  PAG_AMEN_IDX = 0
  CAU_CONT_IDX = 0
  AGENTI_IDX = 0
  cPrg = "gscg_kpn"
  cComment = "Prima nota dati aggiuntivi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PNFLPROV = space(1)
  w_FLSALI = space(1)
  w_FLSALF = space(1)
  w_SEZB = space(1)
  w_CODAZI = space(5)
  w_PARTSN = space(10)
  w_PNTIPCLF = space(1)
  w_PNTIPCON = space(1)
  w_DESCON = space(50)
  w_DESCO2 = space(40)
  w_PNDESRIG = space(50)
  w_PNCAURIG = space(5)
  o_PNCAURIG = space(5)
  w_PNCODAGE = space(5)
  w_RIGPAR = space(1)
  w_FLRITE = space(1)
  w_CAUDES = space(35)
  w_PNINICOM = ctod('  /  /  ')
  o_PNINICOM = ctod('  /  /  ')
  w_PNFINCOM = ctod('  /  /  ')
  w_PNFLZERO = space(1)
  w_PNCODPAG = space(5)
  w_PAGDES = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_PNIMPIND = 0
  w_PNFLSALD = space(1)
  w_PNFLSALI = space(1)
  w_PNFLSALF = space(1)
  w_PNFLPART = space(1)
  w_DESAGE = space(35)
  w_PNFLVABD = space(1)
  w_GESRIT = space(1)
  w_PNCODCON = space(15)
  w_VARPAR = space(1)
  w_VARCEN = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kpnPag1","gscg_kpn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPNDESRIG_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='PAG_AMEN'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='AGENTI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PNFLPROV=space(1)
      .w_FLSALI=space(1)
      .w_FLSALF=space(1)
      .w_SEZB=space(1)
      .w_CODAZI=space(5)
      .w_PARTSN=space(10)
      .w_PNTIPCLF=space(1)
      .w_PNTIPCON=space(1)
      .w_DESCON=space(50)
      .w_DESCO2=space(40)
      .w_PNDESRIG=space(50)
      .w_PNCAURIG=space(5)
      .w_PNCODAGE=space(5)
      .w_RIGPAR=space(1)
      .w_FLRITE=space(1)
      .w_CAUDES=space(35)
      .w_PNINICOM=ctod("  /  /  ")
      .w_PNFINCOM=ctod("  /  /  ")
      .w_PNFLZERO=space(1)
      .w_PNCODPAG=space(5)
      .w_PAGDES=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_PNIMPIND=0
      .w_PNFLSALD=space(1)
      .w_PNFLSALI=space(1)
      .w_PNFLSALF=space(1)
      .w_PNFLPART=space(1)
      .w_DESAGE=space(35)
      .w_PNFLVABD=space(1)
      .w_GESRIT=space(1)
      .w_PNCODCON=space(15)
      .w_VARPAR=space(1)
      .w_VARCEN=space(1)
      .w_PNFLPROV=oParentObject.w_PNFLPROV
      .w_FLSALI=oParentObject.w_FLSALI
      .w_FLSALF=oParentObject.w_FLSALF
      .w_SEZB=oParentObject.w_SEZB
      .w_CODAZI=oParentObject.w_CODAZI
      .w_PARTSN=oParentObject.w_PARTSN
      .w_PNTIPCLF=oParentObject.w_PNTIPCLF
      .w_PNTIPCON=oParentObject.w_PNTIPCON
      .w_DESCON=oParentObject.w_DESCON
      .w_DESCO2=oParentObject.w_DESCO2
      .w_PNDESRIG=oParentObject.w_PNDESRIG
      .w_PNCAURIG=oParentObject.w_PNCAURIG
      .w_PNCODAGE=oParentObject.w_PNCODAGE
      .w_RIGPAR=oParentObject.w_RIGPAR
      .w_CAUDES=oParentObject.w_CAUDES
      .w_PNINICOM=oParentObject.w_PNINICOM
      .w_PNFINCOM=oParentObject.w_PNFINCOM
      .w_PNFLZERO=oParentObject.w_PNFLZERO
      .w_PNCODPAG=oParentObject.w_PNCODPAG
      .w_PNIMPIND=oParentObject.w_PNIMPIND
      .w_PNFLSALD=oParentObject.w_PNFLSALD
      .w_PNFLSALI=oParentObject.w_PNFLSALI
      .w_PNFLSALF=oParentObject.w_PNFLSALF
      .w_PNFLPART=oParentObject.w_PNFLPART
      .w_PNFLVABD=oParentObject.w_PNFLVABD
      .w_GESRIT=oParentObject.w_GESRIT
      .w_PNCODCON=oParentObject.w_PNCODCON
      .w_VARPAR=oParentObject.w_VARPAR
      .w_VARCEN=oParentObject.w_VARCEN
        .DoRTCalc(1,12,.f.)
        if not(empty(.w_PNCAURIG))
          .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_PNCODAGE))
          .link_1_13('Full')
        endif
          .DoRTCalc(14,19,.f.)
        .w_PNCODPAG = .w_PNCODPAG
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_PNCODPAG))
          .link_1_21('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
          .DoRTCalc(21,21,.f.)
        .w_OBTEST = THIS.OPARENTOBJECT.w_PNDATREG
          .DoRTCalc(23,24,.f.)
        .w_PNFLSALD = IIF(.w_PNFLPROV='S', ' ', '+')
        .w_PNFLSALI = IIF(.w_FLSALI='S' AND .w_PNFLPROV<>'S' AND .w_SEZB<>'T', '+', ' ')
        .w_PNFLSALF = IIF(.w_FLSALF='S' AND .w_PNFLPROV<>'S' AND .w_SEZB<>'T', '+', ' ')
        .w_PNFLPART = IIF(.w_PARTSN="S", .w_RIGPAR, "N")
      .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
    endwith
    this.DoRTCalc(29,34,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPNDESRIG_1_11.enabled = i_bVal
      .Page1.oPag.oPNCAURIG_1_12.enabled = i_bVal
      .Page1.oPag.oPNCODAGE_1_13.enabled = i_bVal
      .Page1.oPag.oPNINICOM_1_17.enabled = i_bVal
      .Page1.oPag.oPNFINCOM_1_18.enabled = i_bVal
      .Page1.oPag.oPNFLZERO_1_19.enabled = i_bVal
      .Page1.oPag.oPNFLVABD_1_38.enabled = i_bVal
      .Page1.oPag.oObj_1_27.enabled = i_bVal
      .Page1.oPag.oObj_1_43.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PNFLPROV=.w_PNFLPROV
      .oParentObject.w_FLSALI=.w_FLSALI
      .oParentObject.w_FLSALF=.w_FLSALF
      .oParentObject.w_SEZB=.w_SEZB
      .oParentObject.w_CODAZI=.w_CODAZI
      .oParentObject.w_PARTSN=.w_PARTSN
      .oParentObject.w_PNTIPCLF=.w_PNTIPCLF
      .oParentObject.w_PNTIPCON=.w_PNTIPCON
      .oParentObject.w_DESCON=.w_DESCON
      .oParentObject.w_DESCO2=.w_DESCO2
      .oParentObject.w_PNDESRIG=.w_PNDESRIG
      .oParentObject.w_PNCAURIG=.w_PNCAURIG
      .oParentObject.w_PNCODAGE=.w_PNCODAGE
      .oParentObject.w_RIGPAR=.w_RIGPAR
      .oParentObject.w_CAUDES=.w_CAUDES
      .oParentObject.w_PNINICOM=.w_PNINICOM
      .oParentObject.w_PNFINCOM=.w_PNFINCOM
      .oParentObject.w_PNFLZERO=.w_PNFLZERO
      .oParentObject.w_PNCODPAG=.w_PNCODPAG
      .oParentObject.w_PNIMPIND=.w_PNIMPIND
      .oParentObject.w_PNFLSALD=.w_PNFLSALD
      .oParentObject.w_PNFLSALI=.w_PNFLSALI
      .oParentObject.w_PNFLSALF=.w_PNFLSALF
      .oParentObject.w_PNFLPART=.w_PNFLPART
      .oParentObject.w_PNFLVABD=.w_PNFLVABD
      .oParentObject.w_GESRIT=.w_GESRIT
      .oParentObject.w_PNCODCON=.w_PNCODCON
      .oParentObject.w_VARPAR=.w_VARPAR
      .oParentObject.w_VARCEN=.w_VARCEN
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,11,.t.)
        if .o_PNCAURIG<>.w_PNCAURIG
            .w_PNCAURIG = .w_PNCAURIG
          .link_1_12('Full')
        endif
        .DoRTCalc(13,17,.t.)
        if .o_PNINICOM<>.w_PNINICOM
            .w_PNFINCOM = iif(empty(.w_pnfincom)and not empty(.w_pninicom),.w_pninicom,iif(not empty(.w_PNFINCOM)and empty(.w_PNINICOM),.w_pninicom,.w_pnfincom))
        endif
        .DoRTCalc(19,19,.t.)
          .link_1_21('Full')
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .DoRTCalc(21,24,.t.)
            .w_PNFLSALD = IIF(.w_PNFLPROV='S', ' ', '+')
            .w_PNFLSALI = IIF(.w_FLSALI='S' AND .w_PNFLPROV<>'S' AND .w_SEZB<>'T', '+', ' ')
            .w_PNFLSALF = IIF(.w_FLSALF='S' AND .w_PNFLPROV<>'S' AND .w_SEZB<>'T', '+', ' ')
        if .o_PNCAURIG<>.w_PNCAURIG
            .w_PNFLPART = IIF(.w_PARTSN="S", .w_RIGPAR, "N")
        endif
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(29,34,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPNCODAGE_1_13.enabled = this.oPgFrm.Page1.oPag.oPNCODAGE_1_13.mCond()
    this.oPgFrm.Page1.oPag.oPNINICOM_1_17.enabled = this.oPgFrm.Page1.oPag.oPNINICOM_1_17.mCond()
    this.oPgFrm.Page1.oPag.oPNFINCOM_1_18.enabled = this.oPgFrm.Page1.oPag.oPNFINCOM_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPNIMPIND_1_30.visible=!this.oPgFrm.Page1.oPag.oPNIMPIND_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PNCAURIG
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCAURIG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_PNCAURIG)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCFLRITE,CCFLPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_PNCAURIG))
          select CCCODICE,CCDESCRI,CCDTOBSO,CCFLRITE,CCFLPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PNCAURIG)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PNCAURIG) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oPNCAURIG_1_12'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCFLRITE,CCFLPART";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO,CCFLRITE,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCAURIG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCFLRITE,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_PNCAURIG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_PNCAURIG)
            select CCCODICE,CCDESCRI,CCDTOBSO,CCFLRITE,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCAURIG = NVL(_Link_.CCCODICE,space(5))
      this.w_CAUDES = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_FLRITE = NVL(_Link_.CCFLRITE,space(1))
      this.w_RIGPAR = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PNCAURIG = space(5)
      endif
      this.w_CAUDES = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLRITE = space(1)
      this.w_RIGPAR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente oppure obsoleta")
        endif
        this.w_PNCAURIG = space(5)
        this.w_CAUDES = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLRITE = space(1)
        this.w_RIGPAR = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCAURIG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNCODAGE
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_PNCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_PNCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PNCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PNCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oPNCODAGE_1_13'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PNCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PNCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente inesistente oppure obsoleto")
        endif
        this.w_PNCODAGE = space(5)
        this.w_DESAGE = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNCODPAG
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_PNCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_PNCODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_PAGDES = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODPAG = space(5)
      endif
      this.w_PAGDES = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_PNCODPAG = space(5)
        this.w_PAGDES = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_9.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_9.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCO2_1_10.value==this.w_DESCO2)
      this.oPgFrm.Page1.oPag.oDESCO2_1_10.value=this.w_DESCO2
    endif
    if not(this.oPgFrm.Page1.oPag.oPNDESRIG_1_11.value==this.w_PNDESRIG)
      this.oPgFrm.Page1.oPag.oPNDESRIG_1_11.value=this.w_PNDESRIG
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCAURIG_1_12.value==this.w_PNCAURIG)
      this.oPgFrm.Page1.oPag.oPNCAURIG_1_12.value=this.w_PNCAURIG
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCODAGE_1_13.value==this.w_PNCODAGE)
      this.oPgFrm.Page1.oPag.oPNCODAGE_1_13.value=this.w_PNCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUDES_1_16.value==this.w_CAUDES)
      this.oPgFrm.Page1.oPag.oCAUDES_1_16.value=this.w_CAUDES
    endif
    if not(this.oPgFrm.Page1.oPag.oPNINICOM_1_17.value==this.w_PNINICOM)
      this.oPgFrm.Page1.oPag.oPNINICOM_1_17.value=this.w_PNINICOM
    endif
    if not(this.oPgFrm.Page1.oPag.oPNFINCOM_1_18.value==this.w_PNFINCOM)
      this.oPgFrm.Page1.oPag.oPNFINCOM_1_18.value=this.w_PNFINCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oPNFLZERO_1_19.RadioValue()==this.w_PNFLZERO)
      this.oPgFrm.Page1.oPag.oPNFLZERO_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPNIMPIND_1_30.value==this.w_PNIMPIND)
      this.oPgFrm.Page1.oPag.oPNIMPIND_1_30.value=this.w_PNIMPIND
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_36.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_36.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oPNFLVABD_1_38.RadioValue()==this.w_PNFLVABD)
      this.oPgFrm.Page1.oPag.oPNFLVABD_1_38.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_PNCAURIG)) or not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPNCAURIG_1_12.SetFocus()
            i_bnoObbl = !empty(.w_PNCAURIG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente oppure obsoleta")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_PNTIPCON= 'C')  and not(empty(.w_PNCODAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPNCODAGE_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice agente inesistente oppure obsoleto")
          case   ((empty(.w_PNFINCOM)) or not( .w_PNINICOM<=.w_PNFINCOM))  and (.w_SEZB $ 'CR' AND NOT EMPTY(.w_PNINICOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPNFINCOM_1_18.SetFocus()
            i_bnoObbl = !empty(.w_PNFINCOM)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data fine competenza incongruente")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PNCODPAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPNCODPAG_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PNCAURIG = this.w_PNCAURIG
    this.o_PNINICOM = this.w_PNINICOM
    return

enddefine

* --- Define pages as container
define class tgscg_kpnPag1 as StdContainer
  Width  = 555
  height = 207
  stdWidth  = 555
  stdheight = 207
  resizeXpos=387
  resizeYpos=201
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESCON_1_9 as StdField with uid="GLZNRACNPR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 95529930,;
   bGlobalFont=.t.,;
    Height=21, Width=373, Left=159, Top=15, InputMask=replicate('X',50)

  add object oDESCO2_1_10 as StdField with uid="OKCWOMUOUJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCO2", cQueryName = "DESCO2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 28421066,;
   bGlobalFont=.t.,;
    Height=21, Width=318, Left=159, Top=41, InputMask=replicate('X',40)

  add object oPNDESRIG_1_11 as StdField with uid="CTHVXFCKTV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PNDESRIG", cQueryName = "PNDESRIG",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione supplementare di riga",;
    HelpContextID = 244280893,;
   bGlobalFont=.t.,;
    Height=21, Width=387, Left=159, Top=67, InputMask=replicate('X',50)

  add object oPNCAURIG_1_12 as StdField with uid="HEXLCVCFUT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PNCAURIG", cQueryName = "PNCAURIG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente oppure obsoleta",;
    ToolTipText = "Causale contabili di riga",;
    HelpContextID = 246111805,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=159, Top=93, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_PNCAURIG"

  func oPNCAURIG_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPNCAURIG_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPNCAURIG_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oPNCAURIG_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oPNCAURIG_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_PNCAURIG
     i_obj.ecpSave()
  endproc

  add object oPNCODAGE_1_13 as StdField with uid="FSCIIPSPHE",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PNCODAGE", cQueryName = "PNCODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente inesistente oppure obsoleto",;
    ToolTipText = "Codice agente",;
    HelpContextID = 212426299,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=159, Top=119, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_PNCODAGE"

  func oPNCODAGE_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIPCON= 'C')
    endwith
   endif
  endfunc

  func oPNCODAGE_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPNCODAGE_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPNCODAGE_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oPNCODAGE_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oPNCODAGE_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_PNCODAGE
     i_obj.ecpSave()
  endproc

  add object oCAUDES_1_16 as StdField with uid="GZBDGGJRZO",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CAUDES", cQueryName = "CAUDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 22056922,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=219, Top=93, InputMask=replicate('X',35)

  add object oPNINICOM_1_17 as StdField with uid="AFSAGPRLMO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PNINICOM", cQueryName = "PNINICOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data inizio competenza incongruente",;
    ToolTipText = "Data di inizio competenza; l'intervallo deve essere vuoto oppure finito",;
    HelpContextID = 17252797,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=159, Top=145

  func oPNINICOM_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZB $ 'CR')
    endwith
   endif
  endfunc

  add object oPNFINCOM_1_18 as StdField with uid="TJLCZJNWVX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PNFINCOM", cQueryName = "PNFINCOM",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data fine competenza incongruente",;
    ToolTipText = "Data di fine competenza contabile",;
    HelpContextID = 12349885,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=274, Top=145

  func oPNFINCOM_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZB $ 'CR' AND NOT EMPTY(.w_PNINICOM))
    endwith
   endif
  endfunc

  func oPNFINCOM_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ( .w_PNINICOM<=.w_PNFINCOM)
    endwith
    return bRes
  endfunc

  add object oPNFLZERO_1_19 as StdCheck with uid="KXAYZHKPGR",rtseq=19,rtrep=.f.,left=404, top=145, caption="Importi a zero",;
    ToolTipText = "Se attivo: la riga di primanota accetter� anche importi (dare+avere) =0",;
    HelpContextID = 234451387,;
    cFormVar="w_PNFLZERO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPNFLZERO_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPNFLZERO_1_19.GetRadio()
    this.Parent.oContained.w_PNFLZERO = this.RadioValue()
    return .t.
  endfunc

  func oPNFLZERO_1_19.SetRadio()
    this.Parent.oContained.w_PNFLZERO=trim(this.Parent.oContained.w_PNFLZERO)
    this.value = ;
      iif(this.Parent.oContained.w_PNFLZERO=='S',1,;
      0)
  endfunc


  add object oObj_1_27 as cp_runprogram with uid="RDPAHEQHXR",left=524, top=234, width=250,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCG_BKG',;
    cEvent = "w_PNCODPAG Changed",;
    nPag=1;
    , HelpContextID = 240000538

  add object oPNIMPIND_1_30 as StdField with uid="XEBVVOQOJL",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PNIMPIND", cQueryName = "PNIMPIND",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo non imponibile ai fini della generazione ritenute",;
    HelpContextID = 177750470,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=159, Top=171, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oPNIMPIND_1_30.mHide()
    with this.Parent.oContained
      return (.w_PNTIPCLF<>'F' OR .w_PNTIPCON<>'G' OR .w_GESRIT<>'S')
    endwith
  endfunc

  add object oDESAGE_1_36 as StdField with uid="GSFSAWJWII",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 255044554,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=219, Top=119, InputMask=replicate('X',35)

  add object oPNFLVABD_1_38 as StdCheck with uid="BJRLERBNOE",rtseq=30,rtrep=.f.,left=404, top=171, caption="Beni deperibili",;
    ToolTipText = "Flag beni deperibili",;
    HelpContextID = 37319110,;
    cFormVar="w_PNFLVABD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPNFLVABD_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPNFLVABD_1_38.GetRadio()
    this.Parent.oContained.w_PNFLVABD = this.RadioValue()
    return .t.
  endfunc

  func oPNFLVABD_1_38.SetRadio()
    this.Parent.oContained.w_PNFLVABD=trim(this.Parent.oContained.w_PNFLVABD)
    this.value = ;
      iif(this.Parent.oContained.w_PNFLVABD=='S',1,;
      0)
  endfunc


  add object oObj_1_43 as cp_runprogram with uid="FWOKZBLYVP",left=524, top=251, width=250,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BTK('R')",;
    cEvent = "w_PNCAURIG Changed",;
    nPag=1;
    , HelpContextID = 240000538

  add object oStr_1_20 as StdString with uid="GPTRUVZQUV",Visible=.t., Left=5, Top=67,;
    Alignment=1, Width=153, Height=15,;
    Caption="Descrizione aggiuntiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="RVMFCXILMF",Visible=.t., Left=5, Top=93,;
    Alignment=1, Width=153, Height=15,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="VJWRRNBKNP",Visible=.t., Left=5, Top=15,;
    Alignment=1, Width=153, Height=15,;
    Caption="Descrizione conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="UOEJZPKORP",Visible=.t., Left=5, Top=145,;
    Alignment=1, Width=153, Height=15,;
    Caption="Compet.contabile dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="ORXCYVNRWU",Visible=.t., Left=246, Top=145,;
    Alignment=1, Width=23, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="IOGEAIDZWC",Visible=.t., Left=5, Top=171,;
    Alignment=1, Width=153, Height=15,;
    Caption="Importo non imponibile:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_PNTIPCLF<>'F' OR .w_PNTIPCON<>'G' OR .w_GESRIT<>'S')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="EZNIGBSYWU",Visible=.t., Left=5, Top=117,;
    Alignment=1, Width=153, Height=18,;
    Caption="Codice agente:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kpn','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
