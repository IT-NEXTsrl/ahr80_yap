* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_bmu                                                        *
*              Aggiornamento utenti controllo flussi                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-03-11                                                      *
* Last revis.: 2013-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscf_bmu",oParentObject,m.pOPER)
return(i_retval)

define class tgscf_bmu as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_ZSERIAL = space(10)
  w_ZROWNUM = 0
  w_MESS = space(10)
  * --- WorkFile variables
  REF_MAST_idx=0
  ASSREGUT_idx=0
  OPAGGREG_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento utenti controllo flussi (da GSCF_KMU)
    do case
      case this.pOPER="BLANK"
        * --- Insert into REF_MAST
        i_nConn=i_TableProp[this.REF_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.REF_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REF_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"RESERIAL"+",REBUSOBJ"+",RETABMST"+",RETABDTL"+",REFLENAB"+",RESAVEDB"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RESERIAL),'REF_MAST','RESERIAL');
          +","+cp_NullLink(cp_ToStrODBC("."),'REF_MAST','REBUSOBJ');
          +","+cp_NullLink(cp_ToStrODBC("."),'REF_MAST','RETABMST');
          +","+cp_NullLink(cp_ToStrODBC("."),'REF_MAST','RETABDTL');
          +","+cp_NullLink(cp_ToStrODBC("N"),'REF_MAST','REFLENAB');
          +","+cp_NullLink(cp_ToStrODBC("N"),'REF_MAST','RESAVEDB');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'RESERIAL',this.oParentObject.w_RESERIAL,'REBUSOBJ',".",'RETABMST',".",'RETABDTL',".",'REFLENAB',"N",'RESAVEDB',"N")
          insert into (i_cTable) (RESERIAL,REBUSOBJ,RETABMST,RETABDTL,REFLENAB,RESAVEDB &i_ccchkf. );
             values (;
               this.oParentObject.w_RESERIAL;
               ,".";
               ,".";
               ,".";
               ,"N";
               ,"N";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.pOPER="DONE"
        * --- Controllo se sono stati inseriti dei record ASSREGUT
        this.w_ZROWNUM = 0
        * --- Select from ASSREGUT
        i_nConn=i_TableProp[this.ASSREGUT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ASSREGUT_idx,2],.t.,this.ASSREGUT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM  from "+i_cTable+" ASSREGUT ";
              +" where RUSERIAL="+cp_ToStrODBC(this.oParentObject.w_RESERIAL)+"";
               ,"_Curs_ASSREGUT")
        else
          select MAX(CPROWNUM) AS CPROWNUM from (i_cTable);
           where RUSERIAL=this.oParentObject.w_RESERIAL;
            into cursor _Curs_ASSREGUT
        endif
        if used('_Curs_ASSREGUT')
          select _Curs_ASSREGUT
          locate for 1=1
          do while not(eof())
          this.w_ZROWNUM = NVL(_Curs_ASSREGUT.CPROWNUM,0)
            select _Curs_ASSREGUT
            continue
          enddo
          use
        endif
        if this.w_ZROWNUM>0
          this.w_ZROWNUM = 0
          Select (this.oParentObject.w_ZOOM.cCursor)
          SCAN FOR XCHK=1
          this.w_ZSERIAL = RESERIAL
          if this.oParentObject.w_CRITAGG="E"
            * --- Elimino i dati esistenti
            * --- Delete from ASSREGUT
            i_nConn=i_TableProp[this.ASSREGUT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ASSREGUT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"RUSERIAL = "+cp_ToStrODBC(this.w_ZSERIAL);
                     )
            else
              delete from (i_cTable) where;
                    RUSERIAL = this.w_ZSERIAL;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          else
            this.w_ZROWNUM = 0
            * --- PRENDO IL CPROWNUM MASSIMO
            * --- Select from ASSREGUT
            i_nConn=i_TableProp[this.ASSREGUT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ASSREGUT_idx,2],.t.,this.ASSREGUT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM  from "+i_cTable+" ASSREGUT ";
                  +" where RUSERIAL="+cp_ToStrODBC(this.w_ZSERIAL)+"";
                   ,"_Curs_ASSREGUT")
            else
              select MAX(CPROWNUM) AS CPROWNUM from (i_cTable);
               where RUSERIAL=this.w_ZSERIAL;
                into cursor _Curs_ASSREGUT
            endif
            if used('_Curs_ASSREGUT')
              select _Curs_ASSREGUT
              locate for 1=1
              do while not(eof())
              this.w_ZROWNUM = NVL(_Curs_ASSREGUT.CPROWNUM,0)
                select _Curs_ASSREGUT
                continue
              enddo
              use
            endif
          endif
          * --- Insert into ASSREGUT
          i_nConn=i_TableProp[this.ASSREGUT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ASSREGUT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCF_BMU",this.ASSREGUT_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          ENDSCAN
          Select (this.oParentObject.w_ZOOM.cCursor)
          * --- Delete from ASSREGUT
          i_nConn=i_TableProp[this.ASSREGUT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ASSREGUT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"RUSERIAL = "+cp_ToStrODBC(this.oParentObject.w_RESERIAL);
                   )
          else
            delete from (i_cTable) where;
                  RUSERIAL = this.oParentObject.w_RESERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Elimino i record doppi
          * --- Delete from ASSREGUT
          i_nConn=i_TableProp[this.ASSREGUT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ASSREGUT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".RUSERIAL = "+i_cQueryTable+".RUSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
          
            do vq_exec with 'gscf_bmu3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          this.w_MESS = ah_msgformat("Aggiornamento completato")
        endif
        * --- Controllo se sono stati inseriti dei record OPAGGREG
        this.w_ZROWNUM = 0
        * --- Select from OPAGGREG
        i_nConn=i_TableProp[this.OPAGGREG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OPAGGREG_idx,2],.t.,this.OPAGGREG_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM  from "+i_cTable+" OPAGGREG ";
              +" where OASERIAL="+cp_ToStrODBC(this.oParentObject.w_RESERIAL)+"";
               ,"_Curs_OPAGGREG")
        else
          select MAX(CPROWNUM) AS CPROWNUM from (i_cTable);
           where OASERIAL=this.oParentObject.w_RESERIAL;
            into cursor _Curs_OPAGGREG
        endif
        if used('_Curs_OPAGGREG')
          select _Curs_OPAGGREG
          locate for 1=1
          do while not(eof())
          this.w_ZROWNUM = NVL(_Curs_OPAGGREG.CPROWNUM,0)
            select _Curs_OPAGGREG
            continue
          enddo
          use
        endif
        if this.w_ZROWNUM>0
          this.w_ZROWNUM = 0
          Select (this.oParentObject.w_ZOOM.cCursor)
          SCAN FOR XCHK=1
          this.w_ZSERIAL = RESERIAL
          if this.oParentObject.w_CRITAGG="E"
            * --- Elimino i dati esistenti
            * --- Delete from OPAGGREG
            i_nConn=i_TableProp[this.OPAGGREG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OPAGGREG_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"OASERIAL = "+cp_ToStrODBC(this.w_ZSERIAL);
                     )
            else
              delete from (i_cTable) where;
                    OASERIAL = this.w_ZSERIAL;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          else
            this.w_ZROWNUM = 0
            * --- PRENDO IL CPROWNUM MASSIMO
            * --- Select from OPAGGREG
            i_nConn=i_TableProp[this.OPAGGREG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OPAGGREG_idx,2],.t.,this.OPAGGREG_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM  from "+i_cTable+" OPAGGREG ";
                  +" where OASERIAL="+cp_ToStrODBC(this.w_ZSERIAL)+"";
                   ,"_Curs_OPAGGREG")
            else
              select MAX(CPROWNUM) AS CPROWNUM from (i_cTable);
               where OASERIAL=this.w_ZSERIAL;
                into cursor _Curs_OPAGGREG
            endif
            if used('_Curs_OPAGGREG')
              select _Curs_OPAGGREG
              locate for 1=1
              do while not(eof())
              this.w_ZROWNUM = NVL(_Curs_OPAGGREG.CPROWNUM,0)
                select _Curs_OPAGGREG
                continue
              enddo
              use
            endif
          endif
          * --- Insert into OPAGGREG
          i_nConn=i_TableProp[this.OPAGGREG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OPAGGREG_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCF_BMU1",this.OPAGGREG_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          ENDSCAN
          Select (this.oParentObject.w_ZOOM.cCursor)
          * --- Delete from OPAGGREG
          i_nConn=i_TableProp[this.OPAGGREG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OPAGGREG_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"OASERIAL = "+cp_ToStrODBC(this.oParentObject.w_RESERIAL);
                   )
          else
            delete from (i_cTable) where;
                  OASERIAL = this.oParentObject.w_RESERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          this.w_MESS = ah_msgformat("Aggiornamento completato")
        endif
        * --- Delete from REF_MAST
        i_nConn=i_TableProp[this.REF_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.REF_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"RESERIAL = "+cp_ToStrODBC(this.oParentObject.w_RESERIAL);
                 )
        else
          delete from (i_cTable) where;
                RESERIAL = this.oParentObject.w_RESERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        if not empty(this.w_MESS)
          ah_ErrorMsg(this.w_MESS)
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='REF_MAST'
    this.cWorkTables[2]='ASSREGUT'
    this.cWorkTables[3]='OPAGGREG'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_ASSREGUT')
      use in _Curs_ASSREGUT
    endif
    if used('_Curs_ASSREGUT')
      use in _Curs_ASSREGUT
    endif
    if used('_Curs_OPAGGREG')
      use in _Curs_OPAGGREG
    endif
    if used('_Curs_OPAGGREG')
      use in _Curs_OPAGGREG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
