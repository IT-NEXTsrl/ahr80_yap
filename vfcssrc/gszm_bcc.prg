* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bcc                                                        *
*              Tasto destro esegue routine eseguita al click                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-14                                                      *
* Last revis.: 2012-06-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bcc",oParentObject)
return(i_retval)

define class tgszm_bcc as StdBatch
  * --- Local variables
  w_GEST = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa Routine esegue quanto laniato al doppio click in una qualsiasi visualizzazione
    *     risalendo all'evento specifico di selezione
    *     con il vincolo che le routine eseguita apra una gestione
    *     visto che successivamente si utilizza la variabile globale  i_curform
    *     per aprire la gestione  in modifica o caricamento
     
 NC="w_" + lower(Alltrim(G_OMENU.OCONTAINED.NAME)) + " selected" 
 g_oMenu.oparentobject.Notifyevent((nc))
    this.w_GEST = i_curform
    if Type("This.w_GEST")="O" 
      do case
        case g_oMenu.cBatchType = "M"
          if this.w_GEST.HasCpEvents("ecpEdit")
            this.w_GEST.ecpEdit()     
          endif
        case g_oMenu.cBatchType = "L"
          if this.w_GEST.HasCpEvents("ecpLoad")
            this.w_GEST.ecpLoad()     
          endif
      endcase
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
