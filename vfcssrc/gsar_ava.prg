* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ava                                                        *
*              Contropartite ven./acq.                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_7]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2014-05-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_ava"))

* --- Class definition
define class tgsar_ava as StdForm
  Top    = 22
  Left   = 28

  * --- Standard Properties
  Width  = 586
  Height = 297+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-05-30"
  HelpContextID=59340649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Constant Properties
  CONTVEAC_IDX = 0
  CONTI_IDX = 0
  CACOARTI_IDX = 0
  CACOCLFO_IDX = 0
  cFile = "CONTVEAC"
  cKeySelect = "CVCODART,CVCODCLI"
  cKeyWhere  = "CVCODART=this.w_CVCODART and CVCODCLI=this.w_CVCODCLI"
  cKeyWhereODBC = '"CVCODART="+cp_ToStrODBC(this.w_CVCODART)';
      +'+" and CVCODCLI="+cp_ToStrODBC(this.w_CVCODCLI)';

  cKeyWhereODBCqualified = '"CONTVEAC.CVCODART="+cp_ToStrODBC(this.w_CVCODART)';
      +'+" and CONTVEAC.CVCODCLI="+cp_ToStrODBC(this.w_CVCODCLI)';

  cPrg = "gsar_ava"
  cComment = "Contropartite ven./acq."
  icon = "anag.ico"
  cAutoZoom = 'GSAR0AVA'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CVCODART = space(5)
  w_DESC1 = space(35)
  w_CVCODCLI = space(5)
  w_DESC2 = space(35)
  w_CVTIPCON = space(1)
  w_CVCONRIC = space(15)
  w_DESPI1 = space(40)
  w_CVCONOMV = space(15)
  w_DESPI2 = space(40)
  w_CVIVAOMA = space(15)
  w_DESPI5 = space(40)
  w_CVCONNCC = space(15)
  w_DESPI6 = space(40)
  w_CVCONACQ = space(15)
  w_DESPI3 = space(40)
  w_CVCONOMA = space(15)
  w_DESPI4 = space(40)
  w_CVIVAOMC = space(15)
  w_DESPI8 = space(40)
  w_CVCONNCF = space(15)
  w_DESPI7 = space(40)
  w_TIPSO1 = space(1)
  w_TIPSO2 = space(1)
  w_TIPSO3 = space(1)
  w_TIPSO4 = space(1)
  w_TIPSO5 = space(1)
  w_TIPSO6 = space(1)
  w_TIPSO7 = space(1)
  w_TIPSO8 = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CONTVEAC','gsar_ava')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_avaPag1","gsar_ava",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Contropartite")
      .Pages(1).HelpContextID = 165472167
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCVCODART_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsar_ava
    WITH THIS.PARENT
    If IsAlt()
       .cComment = CP_TRANSLATE('Contropartite parcellazione')
       .cAutoZoom = 'GSAR_AVA'
    Else
       .cComment = CP_TRANSLATE('Contropartite ven./acq.')
       .cAutoZoom = 'GSAR0AVA'
    Endif
    * --- nMaxFieldsJoin di default vale 200
    .nMaxFieldsJoin = iif(lower(i_ServerConn[1,6])="oracle", 30, .nMaxFieldsJoin)
    Endwith
    
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CACOARTI'
    this.cWorkTables[3]='CACOCLFO'
    this.cWorkTables[4]='CONTVEAC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTVEAC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTVEAC_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CVCODART = NVL(CVCODART,space(5))
      .w_CVCODCLI = NVL(CVCODCLI,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_3_joined
    link_1_3_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONTVEAC where CVCODART=KeySet.CVCODART
    *                            and CVCODCLI=KeySet.CVCODCLI
    *
    i_nConn = i_TableProp[this.CONTVEAC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTVEAC_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTVEAC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTVEAC.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTVEAC '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CVCODART',this.w_CVCODART  ,'CVCODCLI',this.w_CVCODCLI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESC1 = space(35)
        .w_DESC2 = space(35)
        .w_DESPI1 = space(40)
        .w_DESPI2 = space(40)
        .w_DESPI5 = space(40)
        .w_DESPI6 = space(40)
        .w_DESPI3 = space(40)
        .w_DESPI4 = space(40)
        .w_DESPI8 = space(40)
        .w_DESPI7 = space(40)
        .w_TIPSO1 = space(1)
        .w_TIPSO2 = space(1)
        .w_TIPSO3 = space(1)
        .w_TIPSO4 = space(1)
        .w_TIPSO5 = space(1)
        .w_TIPSO6 = space(1)
        .w_TIPSO7 = space(1)
        .w_TIPSO8 = space(1)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_CVCODART = NVL(CVCODART,space(5))
          if link_1_1_joined
            this.w_CVCODART = NVL(C1CODICE101,NVL(this.w_CVCODART,space(5)))
            this.w_DESC1 = NVL(C1DESCRI101,space(35))
          else
          .link_1_1('Load')
          endif
        .w_CVCODCLI = NVL(CVCODCLI,space(5))
          if link_1_3_joined
            this.w_CVCODCLI = NVL(C2CODICE103,NVL(this.w_CVCODCLI,space(5)))
            this.w_DESC2 = NVL(C2DESCRI103,space(35))
          else
          .link_1_3('Load')
          endif
        .w_CVTIPCON = NVL(CVTIPCON,space(1))
        .w_CVCONRIC = NVL(CVCONRIC,space(15))
          if link_1_6_joined
            this.w_CVCONRIC = NVL(ANCODICE106,NVL(this.w_CVCONRIC,space(15)))
            this.w_DESPI1 = NVL(ANDESCRI106,space(40))
            this.w_TIPSO1 = NVL(ANTIPSOT106,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO106),ctod("  /  /  "))
          else
          .link_1_6('Load')
          endif
        .w_CVCONOMV = NVL(CVCONOMV,space(15))
          if link_1_8_joined
            this.w_CVCONOMV = NVL(ANCODICE108,NVL(this.w_CVCONOMV,space(15)))
            this.w_DESPI2 = NVL(ANDESCRI108,space(40))
            this.w_TIPSO2 = NVL(ANTIPSOT108,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO108),ctod("  /  /  "))
          else
          .link_1_8('Load')
          endif
        .w_CVIVAOMA = NVL(CVIVAOMA,space(15))
          if link_1_10_joined
            this.w_CVIVAOMA = NVL(ANCODICE110,NVL(this.w_CVIVAOMA,space(15)))
            this.w_DESPI5 = NVL(ANDESCRI110,space(40))
            this.w_TIPSO5 = NVL(ANTIPSOT110,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO110),ctod("  /  /  "))
          else
          .link_1_10('Load')
          endif
        .w_CVCONNCC = NVL(CVCONNCC,space(15))
          if link_1_12_joined
            this.w_CVCONNCC = NVL(ANCODICE112,NVL(this.w_CVCONNCC,space(15)))
            this.w_DESPI6 = NVL(ANDESCRI112,space(40))
            this.w_TIPSO6 = NVL(ANTIPSOT112,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO112),ctod("  /  /  "))
          else
          .link_1_12('Load')
          endif
        .w_CVCONACQ = NVL(CVCONACQ,space(15))
          if link_1_14_joined
            this.w_CVCONACQ = NVL(ANCODICE114,NVL(this.w_CVCONACQ,space(15)))
            this.w_DESPI3 = NVL(ANDESCRI114,space(40))
            this.w_TIPSO3 = NVL(ANTIPSOT114,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO114),ctod("  /  /  "))
          else
          .link_1_14('Load')
          endif
        .w_CVCONOMA = NVL(CVCONOMA,space(15))
          if link_1_16_joined
            this.w_CVCONOMA = NVL(ANCODICE116,NVL(this.w_CVCONOMA,space(15)))
            this.w_DESPI4 = NVL(ANDESCRI116,space(40))
            this.w_TIPSO4 = NVL(ANTIPSOT116,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO116),ctod("  /  /  "))
          else
          .link_1_16('Load')
          endif
        .w_CVIVAOMC = NVL(CVIVAOMC,space(15))
          if link_1_18_joined
            this.w_CVIVAOMC = NVL(ANCODICE118,NVL(this.w_CVIVAOMC,space(15)))
            this.w_DESPI8 = NVL(ANDESCRI118,space(40))
            this.w_TIPSO8 = NVL(ANTIPSOT118,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO118),ctod("  /  /  "))
          else
          .link_1_18('Load')
          endif
        .w_CVCONNCF = NVL(CVCONNCF,space(15))
          if link_1_20_joined
            this.w_CVCONNCF = NVL(ANCODICE120,NVL(this.w_CVCONNCF,space(15)))
            this.w_DESPI7 = NVL(ANDESCRI120,space(40))
            this.w_TIPSO7 = NVL(ANTIPSOT120,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO120),ctod("  /  /  "))
          else
          .link_1_20('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF(IsAlt(), "Codice categoria contabile prestazione", "Codice categoria contabile articolo"))
        cp_LoadRecExtFlds(this,'CONTVEAC')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CVCODART = space(5)
      .w_DESC1 = space(35)
      .w_CVCODCLI = space(5)
      .w_DESC2 = space(35)
      .w_CVTIPCON = space(1)
      .w_CVCONRIC = space(15)
      .w_DESPI1 = space(40)
      .w_CVCONOMV = space(15)
      .w_DESPI2 = space(40)
      .w_CVIVAOMA = space(15)
      .w_DESPI5 = space(40)
      .w_CVCONNCC = space(15)
      .w_DESPI6 = space(40)
      .w_CVCONACQ = space(15)
      .w_DESPI3 = space(40)
      .w_CVCONOMA = space(15)
      .w_DESPI4 = space(40)
      .w_CVIVAOMC = space(15)
      .w_DESPI8 = space(40)
      .w_CVCONNCF = space(15)
      .w_DESPI7 = space(40)
      .w_TIPSO1 = space(1)
      .w_TIPSO2 = space(1)
      .w_TIPSO3 = space(1)
      .w_TIPSO4 = space(1)
      .w_TIPSO5 = space(1)
      .w_TIPSO6 = space(1)
      .w_TIPSO7 = space(1)
      .w_TIPSO8 = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CVCODART))
          .link_1_1('Full')
          endif
        .DoRTCalc(2,3,.f.)
          if not(empty(.w_CVCODCLI))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,4,.f.)
        .w_CVTIPCON = 'G'
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_CVCONRIC))
          .link_1_6('Full')
          endif
        .DoRTCalc(7,8,.f.)
          if not(empty(.w_CVCONOMV))
          .link_1_8('Full')
          endif
        .DoRTCalc(9,10,.f.)
          if not(empty(.w_CVIVAOMA))
          .link_1_10('Full')
          endif
        .DoRTCalc(11,12,.f.)
          if not(empty(.w_CVCONNCC))
          .link_1_12('Full')
          endif
        .DoRTCalc(13,14,.f.)
          if not(empty(.w_CVCONACQ))
          .link_1_14('Full')
          endif
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_CVCONOMA))
          .link_1_16('Full')
          endif
        .DoRTCalc(17,18,.f.)
          if not(empty(.w_CVIVAOMC))
          .link_1_18('Full')
          endif
        .DoRTCalc(19,20,.f.)
          if not(empty(.w_CVCONNCF))
          .link_1_20('Full')
          endif
          .DoRTCalc(21,29,.f.)
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF(IsAlt(), "Codice categoria contabile prestazione", "Codice categoria contabile articolo"))
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTVEAC')
    this.DoRTCalc(31,31,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCVCODART_1_1.enabled = i_bVal
      .Page1.oPag.oCVCODCLI_1_3.enabled = i_bVal
      .Page1.oPag.oCVCONRIC_1_6.enabled = i_bVal
      .Page1.oPag.oCVCONOMV_1_8.enabled = i_bVal
      .Page1.oPag.oCVIVAOMA_1_10.enabled = i_bVal
      .Page1.oPag.oCVCONNCC_1_12.enabled = i_bVal
      .Page1.oPag.oCVCONACQ_1_14.enabled = i_bVal
      .Page1.oPag.oCVCONOMA_1_16.enabled = i_bVal
      .Page1.oPag.oCVIVAOMC_1_18.enabled = i_bVal
      .Page1.oPag.oCVCONNCF_1_20.enabled = i_bVal
      .Page1.oPag.oObj_1_43.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCVCODART_1_1.enabled = .f.
        .Page1.oPag.oCVCODCLI_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCVCODART_1_1.enabled = .t.
        .Page1.oPag.oCVCODCLI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CONTVEAC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTVEAC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCODART,"CVCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCODCLI,"CVCODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVTIPCON,"CVTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCONRIC,"CVCONRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCONOMV,"CVCONOMV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVIVAOMA,"CVIVAOMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCONNCC,"CVCONNCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCONACQ,"CVCONACQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCONOMA,"CVCONOMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVIVAOMC,"CVIVAOMC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCONNCF,"CVCONNCF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONTVEAC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTVEAC_IDX,2])
    i_lTable = "CONTVEAC"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CONTVEAC_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SCP with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTVEAC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTVEAC_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONTVEAC_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONTVEAC
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTVEAC')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTVEAC')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CVCODART,CVCODCLI,CVTIPCON,CVCONRIC,CVCONOMV"+;
                  ",CVIVAOMA,CVCONNCC,CVCONACQ,CVCONOMA,CVIVAOMC"+;
                  ",CVCONNCF "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_CVCODART)+;
                  ","+cp_ToStrODBCNull(this.w_CVCODCLI)+;
                  ","+cp_ToStrODBC(this.w_CVTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_CVCONRIC)+;
                  ","+cp_ToStrODBCNull(this.w_CVCONOMV)+;
                  ","+cp_ToStrODBCNull(this.w_CVIVAOMA)+;
                  ","+cp_ToStrODBCNull(this.w_CVCONNCC)+;
                  ","+cp_ToStrODBCNull(this.w_CVCONACQ)+;
                  ","+cp_ToStrODBCNull(this.w_CVCONOMA)+;
                  ","+cp_ToStrODBCNull(this.w_CVIVAOMC)+;
                  ","+cp_ToStrODBCNull(this.w_CVCONNCF)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTVEAC')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTVEAC')
        cp_CheckDeletedKey(i_cTable,0,'CVCODART',this.w_CVCODART,'CVCODCLI',this.w_CVCODCLI)
        INSERT INTO (i_cTable);
              (CVCODART,CVCODCLI,CVTIPCON,CVCONRIC,CVCONOMV,CVIVAOMA,CVCONNCC,CVCONACQ,CVCONOMA,CVIVAOMC,CVCONNCF  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CVCODART;
                  ,this.w_CVCODCLI;
                  ,this.w_CVTIPCON;
                  ,this.w_CVCONRIC;
                  ,this.w_CVCONOMV;
                  ,this.w_CVIVAOMA;
                  ,this.w_CVCONNCC;
                  ,this.w_CVCONACQ;
                  ,this.w_CVCONOMA;
                  ,this.w_CVIVAOMC;
                  ,this.w_CVCONNCF;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONTVEAC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTVEAC_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONTVEAC_IDX,i_nConn)
      *
      * update CONTVEAC
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONTVEAC')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CVTIPCON="+cp_ToStrODBC(this.w_CVTIPCON)+;
             ",CVCONRIC="+cp_ToStrODBCNull(this.w_CVCONRIC)+;
             ",CVCONOMV="+cp_ToStrODBCNull(this.w_CVCONOMV)+;
             ",CVIVAOMA="+cp_ToStrODBCNull(this.w_CVIVAOMA)+;
             ",CVCONNCC="+cp_ToStrODBCNull(this.w_CVCONNCC)+;
             ",CVCONACQ="+cp_ToStrODBCNull(this.w_CVCONACQ)+;
             ",CVCONOMA="+cp_ToStrODBCNull(this.w_CVCONOMA)+;
             ",CVIVAOMC="+cp_ToStrODBCNull(this.w_CVIVAOMC)+;
             ",CVCONNCF="+cp_ToStrODBCNull(this.w_CVCONNCF)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONTVEAC')
        i_cWhere = cp_PKFox(i_cTable  ,'CVCODART',this.w_CVCODART  ,'CVCODCLI',this.w_CVCODCLI  )
        UPDATE (i_cTable) SET;
              CVTIPCON=this.w_CVTIPCON;
             ,CVCONRIC=this.w_CVCONRIC;
             ,CVCONOMV=this.w_CVCONOMV;
             ,CVIVAOMA=this.w_CVIVAOMA;
             ,CVCONNCC=this.w_CVCONNCC;
             ,CVCONACQ=this.w_CVCONACQ;
             ,CVCONOMA=this.w_CVCONOMA;
             ,CVIVAOMC=this.w_CVIVAOMC;
             ,CVCONNCF=this.w_CVCONNCF;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTVEAC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTVEAC_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONTVEAC_IDX,i_nConn)
      *
      * delete CONTVEAC
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CVCODART',this.w_CVCODART  ,'CVCODCLI',this.w_CVCODCLI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTVEAC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTVEAC_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF(IsAlt(), "Codice categoria contabile prestazione", "Codice categoria contabile articolo"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF(IsAlt(), "Codice categoria contabile prestazione", "Codice categoria contabile articolo"))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCVCONACQ_1_14.visible=!this.oPgFrm.Page1.oPag.oCVCONACQ_1_14.mHide()
    this.oPgFrm.Page1.oPag.oDESPI3_1_15.visible=!this.oPgFrm.Page1.oPag.oDESPI3_1_15.mHide()
    this.oPgFrm.Page1.oPag.oCVCONOMA_1_16.visible=!this.oPgFrm.Page1.oPag.oCVCONOMA_1_16.mHide()
    this.oPgFrm.Page1.oPag.oDESPI4_1_17.visible=!this.oPgFrm.Page1.oPag.oDESPI4_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCVIVAOMC_1_18.visible=!this.oPgFrm.Page1.oPag.oCVIVAOMC_1_18.mHide()
    this.oPgFrm.Page1.oPag.oDESPI8_1_19.visible=!this.oPgFrm.Page1.oPag.oDESPI8_1_19.mHide()
    this.oPgFrm.Page1.oPag.oCVCONNCF_1_20.visible=!this.oPgFrm.Page1.oPag.oCVCONNCF_1_20.mHide()
    this.oPgFrm.Page1.oPag.oDESPI7_1_21.visible=!this.oPgFrm.Page1.oPag.oDESPI7_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CVCODART
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_lTable = "CACOARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2], .t., this.CACOARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC1',True,'CACOARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_CVCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_CVCODART))
          select C1CODICE,C1DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCODART)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CVCODART) and !this.bDontReportError
            deferred_cp_zoom('CACOARTI','*','C1CODICE',cp_AbsName(oSource.parent,'oCVCODART_1_1'),i_cWhere,'GSAR_AC1',"Categorie contabili prestazioni",'GSAR_AVA.CACOARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_CVCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_CVCODART)
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCODART = NVL(_Link_.C1CODICE,space(5))
      this.w_DESC1 = NVL(_Link_.C1DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CVCODART = space(5)
      endif
      this.w_DESC1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.C1CODICE as C1CODICE101"+ ",link_1_1.C1DESCRI as C1DESCRI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on CONTVEAC.CVCODART=link_1_1.C1CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and CONTVEAC.CVCODART=link_1_1.C1CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CVCODCLI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CVCODCLI)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CVCODCLI))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCODCLI)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CVCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCVCODCLI_1_3'),i_cWhere,'GSAR_AC2',"Categorie contabili cli/for",'GSAR_AVA.CACOCLFO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CVCODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CVCODCLI)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCODCLI = NVL(_Link_.C2CODICE,space(5))
      this.w_DESC2 = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CVCODCLI = space(5)
      endif
      this.w_DESC2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOCLFO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.C2CODICE as C2CODICE103"+ ",link_1_3.C2DESCRI as C2DESCRI103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on CONTVEAC.CVCODCLI=link_1_3.C2CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and CONTVEAC.CVCODCLI=link_1_3.C2CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CVCONRIC
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCONRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CVCONRIC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CVTIPCON;
                     ,'ANCODICE',trim(this.w_CVCONRIC))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCONRIC)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CVCONRIC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CVCONRIC)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CVCONRIC) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCVCONRIC_1_6'),i_cWhere,'GSAR_BZC',"Conti contropartita",'GSAR_AVA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo vendite oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCONRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CVCONRIC);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CVTIPCON;
                       ,'ANCODICE',this.w_CVCONRIC)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCONRIC = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPI1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSO1 = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CVCONRIC = space(15)
      endif
      this.w_DESPI1 = space(40)
      this.w_TIPSO1 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSO1$'VM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo vendite oppure obsoleto")
        endif
        this.w_CVCONRIC = space(15)
        this.w_DESPI1 = space(40)
        this.w_TIPSO1 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCONRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.ANCODICE as ANCODICE106"+ ",link_1_6.ANDESCRI as ANDESCRI106"+ ",link_1_6.ANTIPSOT as ANTIPSOT106"+ ",link_1_6.ANDTOBSO as ANDTOBSO106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on CONTVEAC.CVCONRIC=link_1_6.ANCODICE"+" and CONTVEAC.CVTIPCON=link_1_6.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and CONTVEAC.CVCONRIC=link_1_6.ANCODICE(+)"'+'+" and CONTVEAC.CVTIPCON=link_1_6.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CVCONOMV
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCONOMV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CVCONOMV)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CVTIPCON;
                     ,'ANCODICE',trim(this.w_CVCONOMV))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCONOMV)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CVCONOMV)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CVCONOMV)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CVCONOMV) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCVCONOMV_1_8'),i_cWhere,'GSAR_BZC',"Conti contropartita",'GSAR_AVA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo vendite oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCONOMV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CVCONOMV);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CVTIPCON;
                       ,'ANCODICE',this.w_CVCONOMV)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCONOMV = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPI2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSO2 = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CVCONOMV = space(15)
      endif
      this.w_DESPI2 = space(40)
      this.w_TIPSO2 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSO2$'VM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo vendite oppure obsoleto")
        endif
        this.w_CVCONOMV = space(15)
        this.w_DESPI2 = space(40)
        this.w_TIPSO2 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCONOMV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.ANCODICE as ANCODICE108"+ ",link_1_8.ANDESCRI as ANDESCRI108"+ ",link_1_8.ANTIPSOT as ANTIPSOT108"+ ",link_1_8.ANDTOBSO as ANDTOBSO108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on CONTVEAC.CVCONOMV=link_1_8.ANCODICE"+" and CONTVEAC.CVTIPCON=link_1_8.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and CONTVEAC.CVCONOMV=link_1_8.ANCODICE(+)"'+'+" and CONTVEAC.CVTIPCON=link_1_8.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CVIVAOMA
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVIVAOMA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CVIVAOMA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CVTIPCON;
                     ,'ANCODICE',trim(this.w_CVIVAOMA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVIVAOMA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CVIVAOMA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CVIVAOMA)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CVIVAOMA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCVIVAOMA_1_10'),i_cWhere,'GSAR_BZC',"Conti contropartita",'GSAR_AVA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo vendite oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVIVAOMA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CVIVAOMA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CVTIPCON;
                       ,'ANCODICE',this.w_CVIVAOMA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVIVAOMA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPI5 = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSO5 = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CVIVAOMA = space(15)
      endif
      this.w_DESPI5 = space(40)
      this.w_TIPSO5 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSO5$'VM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo vendite oppure obsoleto")
        endif
        this.w_CVIVAOMA = space(15)
        this.w_DESPI5 = space(40)
        this.w_TIPSO5 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVIVAOMA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.ANCODICE as ANCODICE110"+ ",link_1_10.ANDESCRI as ANDESCRI110"+ ",link_1_10.ANTIPSOT as ANTIPSOT110"+ ",link_1_10.ANDTOBSO as ANDTOBSO110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on CONTVEAC.CVIVAOMA=link_1_10.ANCODICE"+" and CONTVEAC.CVTIPCON=link_1_10.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and CONTVEAC.CVIVAOMA=link_1_10.ANCODICE(+)"'+'+" and CONTVEAC.CVTIPCON=link_1_10.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CVCONNCC
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCONNCC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CVCONNCC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CVTIPCON;
                     ,'ANCODICE',trim(this.w_CVCONNCC))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCONNCC)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CVCONNCC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CVCONNCC)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CVCONNCC) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCVCONNCC_1_12'),i_cWhere,'GSAR_BZC',"Conti contropartita",'GSAR_AVA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo vendite oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCONNCC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CVCONNCC);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CVTIPCON;
                       ,'ANCODICE',this.w_CVCONNCC)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCONNCC = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPI6 = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSO6 = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CVCONNCC = space(15)
      endif
      this.w_DESPI6 = space(40)
      this.w_TIPSO6 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSO6$'VM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo vendite oppure obsoleto")
        endif
        this.w_CVCONNCC = space(15)
        this.w_DESPI6 = space(40)
        this.w_TIPSO6 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCONNCC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.ANCODICE as ANCODICE112"+ ",link_1_12.ANDESCRI as ANDESCRI112"+ ",link_1_12.ANTIPSOT as ANTIPSOT112"+ ",link_1_12.ANDTOBSO as ANDTOBSO112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on CONTVEAC.CVCONNCC=link_1_12.ANCODICE"+" and CONTVEAC.CVTIPCON=link_1_12.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and CONTVEAC.CVCONNCC=link_1_12.ANCODICE(+)"'+'+" and CONTVEAC.CVTIPCON=link_1_12.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CVCONACQ
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCONACQ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CVCONACQ)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CVTIPCON;
                     ,'ANCODICE',trim(this.w_CVCONACQ))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCONACQ)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CVCONACQ)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CVCONACQ)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CVCONACQ) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCVCONACQ_1_14'),i_cWhere,'GSAR_BZC',"Conti contropartita",'GSAR1AVA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo acquisti oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCONACQ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CVCONACQ);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CVTIPCON;
                       ,'ANCODICE',this.w_CVCONACQ)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCONACQ = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPI3 = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSO3 = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CVCONACQ = space(15)
      endif
      this.w_DESPI3 = space(40)
      this.w_TIPSO3 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSO3$'AM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo acquisti oppure obsoleto")
        endif
        this.w_CVCONACQ = space(15)
        this.w_DESPI3 = space(40)
        this.w_TIPSO3 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCONACQ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.ANCODICE as ANCODICE114"+ ",link_1_14.ANDESCRI as ANDESCRI114"+ ",link_1_14.ANTIPSOT as ANTIPSOT114"+ ",link_1_14.ANDTOBSO as ANDTOBSO114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on CONTVEAC.CVCONACQ=link_1_14.ANCODICE"+" and CONTVEAC.CVTIPCON=link_1_14.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and CONTVEAC.CVCONACQ=link_1_14.ANCODICE(+)"'+'+" and CONTVEAC.CVTIPCON=link_1_14.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CVCONOMA
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCONOMA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CVCONOMA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CVTIPCON;
                     ,'ANCODICE',trim(this.w_CVCONOMA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCONOMA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CVCONOMA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CVCONOMA)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CVCONOMA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCVCONOMA_1_16'),i_cWhere,'GSAR_BZC',"Conti contropartita",'GSAR1AVA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo acquisti oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCONOMA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CVCONOMA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CVTIPCON;
                       ,'ANCODICE',this.w_CVCONOMA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCONOMA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPI4 = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSO4 = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CVCONOMA = space(15)
      endif
      this.w_DESPI4 = space(40)
      this.w_TIPSO4 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSO4$'AM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo acquisti oppure obsoleto")
        endif
        this.w_CVCONOMA = space(15)
        this.w_DESPI4 = space(40)
        this.w_TIPSO4 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCONOMA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.ANCODICE as ANCODICE116"+ ",link_1_16.ANDESCRI as ANDESCRI116"+ ",link_1_16.ANTIPSOT as ANTIPSOT116"+ ",link_1_16.ANDTOBSO as ANDTOBSO116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on CONTVEAC.CVCONOMA=link_1_16.ANCODICE"+" and CONTVEAC.CVTIPCON=link_1_16.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and CONTVEAC.CVCONOMA=link_1_16.ANCODICE(+)"'+'+" and CONTVEAC.CVTIPCON=link_1_16.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CVIVAOMC
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVIVAOMC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CVIVAOMC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CVTIPCON;
                     ,'ANCODICE',trim(this.w_CVIVAOMC))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVIVAOMC)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CVIVAOMC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CVIVAOMC)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CVIVAOMC) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCVIVAOMC_1_18'),i_cWhere,'GSAR_BZC',"Conti contropartita",'GSAR1AVA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo acquisti oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVIVAOMC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CVIVAOMC);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CVTIPCON;
                       ,'ANCODICE',this.w_CVIVAOMC)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVIVAOMC = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPI8 = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSO8 = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CVIVAOMC = space(15)
      endif
      this.w_DESPI8 = space(40)
      this.w_TIPSO8 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSO8$'AM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo acquisti oppure obsoleto")
        endif
        this.w_CVIVAOMC = space(15)
        this.w_DESPI8 = space(40)
        this.w_TIPSO8 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVIVAOMC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.ANCODICE as ANCODICE118"+ ",link_1_18.ANDESCRI as ANDESCRI118"+ ",link_1_18.ANTIPSOT as ANTIPSOT118"+ ",link_1_18.ANDTOBSO as ANDTOBSO118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on CONTVEAC.CVIVAOMC=link_1_18.ANCODICE"+" and CONTVEAC.CVTIPCON=link_1_18.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and CONTVEAC.CVIVAOMC=link_1_18.ANCODICE(+)"'+'+" and CONTVEAC.CVTIPCON=link_1_18.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CVCONNCF
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCONNCF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CVCONNCF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CVTIPCON;
                     ,'ANCODICE',trim(this.w_CVCONNCF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCONNCF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CVCONNCF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CVCONNCF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CVCONNCF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCVCONNCF_1_20'),i_cWhere,'GSAR_BZC',"Conti contropartita",'GSAR1AVA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo acquisti oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCONNCF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CVCONNCF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CVTIPCON;
                       ,'ANCODICE',this.w_CVCONNCF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCONNCF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPI7 = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSO7 = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CVCONNCF = space(15)
      endif
      this.w_DESPI7 = space(40)
      this.w_TIPSO7 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSO7$'AM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, non tipo acquisti oppure obsoleto")
        endif
        this.w_CVCONNCF = space(15)
        this.w_DESPI7 = space(40)
        this.w_TIPSO7 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCONNCF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.ANCODICE as ANCODICE120"+ ",link_1_20.ANDESCRI as ANDESCRI120"+ ",link_1_20.ANTIPSOT as ANTIPSOT120"+ ",link_1_20.ANDTOBSO as ANDTOBSO120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on CONTVEAC.CVCONNCF=link_1_20.ANCODICE"+" and CONTVEAC.CVTIPCON=link_1_20.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and CONTVEAC.CVCONNCF=link_1_20.ANCODICE(+)"'+'+" and CONTVEAC.CVTIPCON=link_1_20.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCVCODART_1_1.value==this.w_CVCODART)
      this.oPgFrm.Page1.oPag.oCVCODART_1_1.value=this.w_CVCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC1_1_2.value==this.w_DESC1)
      this.oPgFrm.Page1.oPag.oDESC1_1_2.value=this.w_DESC1
    endif
    if not(this.oPgFrm.Page1.oPag.oCVCODCLI_1_3.value==this.w_CVCODCLI)
      this.oPgFrm.Page1.oPag.oCVCODCLI_1_3.value=this.w_CVCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC2_1_4.value==this.w_DESC2)
      this.oPgFrm.Page1.oPag.oDESC2_1_4.value=this.w_DESC2
    endif
    if not(this.oPgFrm.Page1.oPag.oCVCONRIC_1_6.value==this.w_CVCONRIC)
      this.oPgFrm.Page1.oPag.oCVCONRIC_1_6.value=this.w_CVCONRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPI1_1_7.value==this.w_DESPI1)
      this.oPgFrm.Page1.oPag.oDESPI1_1_7.value=this.w_DESPI1
    endif
    if not(this.oPgFrm.Page1.oPag.oCVCONOMV_1_8.value==this.w_CVCONOMV)
      this.oPgFrm.Page1.oPag.oCVCONOMV_1_8.value=this.w_CVCONOMV
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPI2_1_9.value==this.w_DESPI2)
      this.oPgFrm.Page1.oPag.oDESPI2_1_9.value=this.w_DESPI2
    endif
    if not(this.oPgFrm.Page1.oPag.oCVIVAOMA_1_10.value==this.w_CVIVAOMA)
      this.oPgFrm.Page1.oPag.oCVIVAOMA_1_10.value=this.w_CVIVAOMA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPI5_1_11.value==this.w_DESPI5)
      this.oPgFrm.Page1.oPag.oDESPI5_1_11.value=this.w_DESPI5
    endif
    if not(this.oPgFrm.Page1.oPag.oCVCONNCC_1_12.value==this.w_CVCONNCC)
      this.oPgFrm.Page1.oPag.oCVCONNCC_1_12.value=this.w_CVCONNCC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPI6_1_13.value==this.w_DESPI6)
      this.oPgFrm.Page1.oPag.oDESPI6_1_13.value=this.w_DESPI6
    endif
    if not(this.oPgFrm.Page1.oPag.oCVCONACQ_1_14.value==this.w_CVCONACQ)
      this.oPgFrm.Page1.oPag.oCVCONACQ_1_14.value=this.w_CVCONACQ
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPI3_1_15.value==this.w_DESPI3)
      this.oPgFrm.Page1.oPag.oDESPI3_1_15.value=this.w_DESPI3
    endif
    if not(this.oPgFrm.Page1.oPag.oCVCONOMA_1_16.value==this.w_CVCONOMA)
      this.oPgFrm.Page1.oPag.oCVCONOMA_1_16.value=this.w_CVCONOMA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPI4_1_17.value==this.w_DESPI4)
      this.oPgFrm.Page1.oPag.oDESPI4_1_17.value=this.w_DESPI4
    endif
    if not(this.oPgFrm.Page1.oPag.oCVIVAOMC_1_18.value==this.w_CVIVAOMC)
      this.oPgFrm.Page1.oPag.oCVIVAOMC_1_18.value=this.w_CVIVAOMC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPI8_1_19.value==this.w_DESPI8)
      this.oPgFrm.Page1.oPag.oDESPI8_1_19.value=this.w_DESPI8
    endif
    if not(this.oPgFrm.Page1.oPag.oCVCONNCF_1_20.value==this.w_CVCONNCF)
      this.oPgFrm.Page1.oPag.oCVCONNCF_1_20.value=this.w_CVCONNCF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPI7_1_21.value==this.w_DESPI7)
      this.oPgFrm.Page1.oPag.oDESPI7_1_21.value=this.w_DESPI7
    endif
    cp_SetControlsValueExtFlds(this,'CONTVEAC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CVCODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCODART_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CVCODART)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CVCODCLI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCODCLI_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CVCODCLI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPSO1$'VM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CVCONRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCONRIC_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, non tipo vendite oppure obsoleto")
          case   not(.w_TIPSO2$'VM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CVCONOMV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCONOMV_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, non tipo vendite oppure obsoleto")
          case   not(.w_TIPSO5$'VM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CVIVAOMA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVIVAOMA_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, non tipo vendite oppure obsoleto")
          case   not(.w_TIPSO6$'VM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CVCONNCC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCONNCC_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, non tipo vendite oppure obsoleto")
          case   not(.w_TIPSO3$'AM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(IsAlt())  and not(empty(.w_CVCONACQ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCONACQ_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, non tipo acquisti oppure obsoleto")
          case   not(.w_TIPSO4$'AM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(IsAlt())  and not(empty(.w_CVCONOMA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCONOMA_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, non tipo acquisti oppure obsoleto")
          case   not(.w_TIPSO8$'AM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(IsAlt())  and not(empty(.w_CVIVAOMC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVIVAOMC_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, non tipo acquisti oppure obsoleto")
          case   not(.w_TIPSO7$'AM' and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(IsAlt())  and not(empty(.w_CVCONNCF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCONNCF_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, non tipo acquisti oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_avaPag1 as StdContainer
  Width  = 582
  height = 297
  stdWidth  = 582
  stdheight = 297
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCVCODART_1_1 as StdField with uid="SHIYKHNDPI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CVCODART", cQueryName = "CVCODART",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria contabile articolo",;
    HelpContextID = 234220678,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=173, Top=11, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOARTI", cZoomOnZoom="GSAR_AC1", oKey_1_1="C1CODICE", oKey_1_2="this.w_CVCODART"

  func oCVCODART_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCODART_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVCODART_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOARTI','*','C1CODICE',cp_AbsName(this.parent,'oCVCODART_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC1',"Categorie contabili prestazioni",'GSAR_AVA.CACOARTI_VZM',this.parent.oContained
  endproc
  proc oCVCODART_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_CVCODART
     i_obj.ecpSave()
  endproc

  add object oDESC1_1_2 as StdField with uid="JJPEPCHKDL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESC1", cQueryName = "DESC1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 3210698,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=239, Top=11, InputMask=replicate('X',35)

  add object oCVCODCLI_1_3 as StdField with uid="TQVJKTSNZP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CVCODCLI", cQueryName = "CVCODART,CVCODCLI",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria contabile cliente/fornitore",;
    HelpContextID = 200666257,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=173, Top=38, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_CVCODCLI"

  func oCVCODCLI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCODCLI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVCODCLI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCVCODCLI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili cli/for",'GSAR_AVA.CACOCLFO_VZM',this.parent.oContained
  endproc
  proc oCVCODCLI_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_CVCODCLI
     i_obj.ecpSave()
  endproc

  add object oDESC2_1_4 as StdField with uid="SXDTIICXTF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESC2", cQueryName = "DESC2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 2162122,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=239, Top=38, InputMask=replicate('X',35)

  add object oCVCONRIC_1_6 as StdField with uid="JMZARUBFHH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CVCONRIC", cQueryName = "CVCONRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, non tipo vendite oppure obsoleto",;
    ToolTipText = "Conto ricavi utilizzato nella contabilizzazione ciclo attivo",;
    HelpContextID = 61477737,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=173, Top=75, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CVCONRIC"

  func oCVCONRIC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCONRIC_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVCONRIC_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCVCONRIC_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'GSAR_AVA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCVCONRIC_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CVCONRIC
     i_obj.ecpSave()
  endproc

  add object oDESPI1_1_7 as StdField with uid="CKPGJLPUUH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESPI1", cQueryName = "DESPI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 228851146,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=312, Top=75, InputMask=replicate('X',40)

  add object oCVCONOMV_1_8 as StdField with uid="GWBHPQBJRI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CVCONOMV", cQueryName = "CVCONOMV",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, non tipo vendite oppure obsoleto",;
    ToolTipText = "Conto omaggi utilizzato nella contabilizzazione ciclo attivo",;
    HelpContextID = 257289348,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=173, Top=103, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CVCONOMV"

  func oCVCONOMV_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCONOMV_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVCONOMV_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCVCONOMV_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'GSAR_AVA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCVCONOMV_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CVCONOMV
     i_obj.ecpSave()
  endproc

  add object oDESPI2_1_9 as StdField with uid="XPABBVOGNN",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESPI2", cQueryName = "DESPI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 212073930,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=312, Top=103, InputMask=replicate('X',40)

  add object oCVIVAOMA_1_10 as StdField with uid="IKCQSHTAHU",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CVIVAOMA", cQueryName = "CVIVAOMA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, non tipo vendite oppure obsoleto",;
    ToolTipText = "Conto per addebitare l'IVA su omaggi vendite",;
    HelpContextID = 2002073,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=174, Top=131, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CVIVAOMA"

  func oCVIVAOMA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVIVAOMA_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVIVAOMA_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCVIVAOMA_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'GSAR_AVA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCVIVAOMA_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CVIVAOMA
     i_obj.ecpSave()
  endproc

  add object oDESPI5_1_11 as StdField with uid="QAXHLAKMFJ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESPI5", cQueryName = "DESPI5",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 161742282,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=312, Top=131, InputMask=replicate('X',40)

  add object oCVCONNCC_1_12 as StdField with uid="AGEAWCTIED",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CVCONNCC", cQueryName = "CVCONNCC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, non tipo vendite oppure obsoleto",;
    ToolTipText = "Conto note di credito a clienti",;
    HelpContextID = 262804329,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=173, Top=159, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CVCONNCC"

  func oCVCONNCC_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCONNCC_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVCONNCC_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCVCONNCC_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'GSAR_AVA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCVCONNCC_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CVCONNCC
     i_obj.ecpSave()
  endproc

  add object oDESPI6_1_13 as StdField with uid="RHNWWXDWMQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESPI6", cQueryName = "DESPI6",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 144965066,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=312, Top=159, InputMask=replicate('X',40)

  add object oCVCONACQ_1_14 as StdField with uid="QQTXMEADMA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CVCONACQ", cQueryName = "CVCONACQ",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, non tipo acquisti oppure obsoleto",;
    ToolTipText = "Conto acquisti utilizzato nella contabilizzazione ciclo passivo",;
    HelpContextID = 44700535,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=173, Top=187, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CVCONACQ"

  func oCVCONACQ_1_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCVCONACQ_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCONACQ_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVCONACQ_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCVCONACQ_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'GSAR1AVA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCVCONACQ_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CVCONACQ
     i_obj.ecpSave()
  endproc

  add object oDESPI3_1_15 as StdField with uid="UYICGGZBJH",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESPI3", cQueryName = "DESPI3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 195296714,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=312, Top=187, InputMask=replicate('X',40)

  func oDESPI3_1_15.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCVCONOMA_1_16 as StdField with uid="LZAVAIFBTD",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CVCONOMA", cQueryName = "CVCONOMA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, non tipo acquisti oppure obsoleto",;
    ToolTipText = "Conto omaggi utilizzato nella contabilizzazione ciclo passivo",;
    HelpContextID = 257289369,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=173, Top=215, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CVCONOMA"

  func oCVCONOMA_1_16.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCVCONOMA_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCONOMA_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVCONOMA_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCVCONOMA_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'GSAR1AVA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCVCONOMA_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CVCONOMA
     i_obj.ecpSave()
  endproc

  add object oDESPI4_1_17 as StdField with uid="RAKGLEUNLL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESPI4", cQueryName = "DESPI4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 178519498,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=312, Top=215, InputMask=replicate('X',40)

  func oDESPI4_1_17.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCVIVAOMC_1_18 as StdField with uid="XMRLDYKTCP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CVIVAOMC", cQueryName = "CVIVAOMC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, non tipo acquisti oppure obsoleto",;
    ToolTipText = "Conto per accreditare l'IVA su omaggi negli acquisti",;
    HelpContextID = 2002071,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=173, Top=243, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CVIVAOMC"

  func oCVIVAOMC_1_18.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCVIVAOMC_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVIVAOMC_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVIVAOMC_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCVIVAOMC_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'GSAR1AVA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCVIVAOMC_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CVIVAOMC
     i_obj.ecpSave()
  endproc

  add object oDESPI8_1_19 as StdField with uid="WSZSFVTJKW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESPI8", cQueryName = "DESPI8",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 111410634,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=312, Top=243, InputMask=replicate('X',40)

  func oDESPI8_1_19.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCVCONNCF_1_20 as StdField with uid="BVKHSVNSQV",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CVCONNCF", cQueryName = "CVCONNCF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, non tipo acquisti oppure obsoleto",;
    ToolTipText = "Conto note di credito da fornitori",;
    HelpContextID = 262804332,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=173, Top=271, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CVCONNCF"

  func oCVCONNCF_1_20.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCVCONNCF_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCONNCF_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVCONNCF_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCVCONNCF_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'GSAR1AVA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCVCONNCF_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CVCONNCF
     i_obj.ecpSave()
  endproc

  add object oDESPI7_1_21 as StdField with uid="IRWTQJFKVL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESPI7", cQueryName = "DESPI7",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 128187850,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=312, Top=271, InputMask=replicate('X',40)

  func oDESPI7_1_21.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oObj_1_43 as cp_setobjprop with uid="WBUOCTVRDO",left=173, top=342, width=209,height=21,;
    caption='ToolTip di w_CVCODART',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_CVCODART",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 97929783

  add object oStr_1_26 as StdString with uid="AWYLMSAQBB",Visible=.t., Left=34, Top=38,;
    Alignment=1, Width=137, Height=15,;
    Caption="Categoria cli/for:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="MSAVGYMRXO",Visible=.t., Left=34, Top=78,;
    Alignment=1, Width=137, Height=15,;
    Caption="Conto ricavi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="ZMWZDDZDQP",Visible=.t., Left=34, Top=190,;
    Alignment=1, Width=137, Height=15,;
    Caption="Conto acquisti:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="VTTAOFPDET",Visible=.t., Left=29, Top=218,;
    Alignment=1, Width=142, Height=15,;
    Caption="Omaggi su acquisti:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="EKYMLOWLYS",Visible=.t., Left=34, Top=162,;
    Alignment=1, Width=137, Height=15,;
    Caption="Note di credito a clienti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="YLOLNTRFZL",Visible=.t., Left=28, Top=274,;
    Alignment=1, Width=143, Height=15,;
    Caption="Note di credito da fornitori:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="BKICVUXCLI",Visible=.t., Left=2, Top=134,;
    Alignment=1, Width=169, Height=15,;
    Caption="IVA omaggi su vendite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="YSCODCTCLO",Visible=.t., Left=3, Top=246,;
    Alignment=1, Width=168, Height=15,;
    Caption="IVA omaggi su acquisti:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="PMWPYJGMCW",Visible=.t., Left=29, Top=106,;
    Alignment=1, Width=142, Height=15,;
    Caption="Omaggi su vendite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="ZGEQZYGTBK",Visible=.t., Left=34, Top=11,;
    Alignment=1, Width=137, Height=15,;
    Caption="Categoria prestazione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="SUUIPMGWHI",Visible=.t., Left=34, Top=11,;
    Alignment=1, Width=137, Height=15,;
    Caption="Categoria articolo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ava','CONTVEAC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CVCODART=CONTVEAC.CVCODART";
  +" and "+i_cAliasName2+".CVCODCLI=CONTVEAC.CVCODCLI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
