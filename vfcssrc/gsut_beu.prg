* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_beu                                                        *
*              Gestione configurazione interfaccia                             *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-04                                                      *
* Last revis.: 2017-02-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- gsut_beu
#Define TABLEPROPDIM 8

If cp_ascan(@i_TableProp,'myg_mast',ALEN(i_TableProp))=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='myg_mast'
		i_TableProp[i_nTables,2]='MYG_MAST'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		i_TableProp[i_nTables,8]=-1
Endif

If cp_ascan(@i_TableProp,'conf_int',ALEN(i_TableProp))=0
		i_nTables=i_nTables+1
		Dimension i_TableProp[i_nTables,TABLEPROPDIM]
		i_TableProp[i_nTables,1]='conf_int'
		i_TableProp[i_nTables,2]='CONF_INT'
		i_TableProp[i_nTables,3]=-1 && non connesso
		i_TableProp[i_nTables,4]=0
		i_TableProp[i_nTables,5]=1
		i_TableProp[i_nTables,6]=0
		i_TableProp[i_nTables,7]=-1
		i_TableProp[i_nTables,8]=-1
Endif

* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_beu",oParentObject,m.pTipo)
return(i_retval)

define class tgsut_beu as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_PADRE = .NULL.
  w_NoUserScheduler = .f.
  w_CODUTE = 0
  w_TEST = 0
  w_READFROMDB = .f.
  w_CIMONFRA = space(1)
  cStyle = space(25)
  cDefault = space(25)
  cFileConfig = space(100)
  oXML = .NULL.
  oRootNode = .NULL.
  cRootTagName = space(50)
  cParentName = space(50)
  cName = space(50)
  oNodeList = .NULL.
  oNode = .NULL.
  bHasChild = .f.
  oFatherNodeList = .NULL.
  oFatherNode = .NULL.
  nFatherLen = 0
  nFatherPass = 0
  oChildNodeList = .NULL.
  nChildLen = 0
  nPass = 0
  oChildNode = .NULL.
  cTextData = space(100)
  nTextDataLen = 0
  nNumNodes = 0
  nType = 0
  i_Rows = 0
  w_PROGBAR = space(1)
  w_MDIFORM = space(1)
  w_WNDMEN = space(1)
  w_DSKBAR = space(1)
  w_CPTBAR = space(1)
  w_TOOLMN = space(1)
  w_STABAR = space(1)
  w_LOADBUTT = space(1)
  w_PAGEBUTT = space(1)
  w_SHWMEN = space(1)
  w_SETCEN = space(1)
  w_SETDAT = space(1)
  w_SETMRK = space(1)
  w_SETPNT = space(1)
  w_SETSEP = space(1)
  w_SYSFRM = space(1)
  w_DispColon = space(1)
  w_CILBLFNM = space(50)
  w_CITXTFNM = space(50)
  w_CICBXFNM = space(50)
  w_CIBTNFNM = space(50)
  w_CIGRDFNM = space(50)
  w_CIPAGFNM = space(50)
  w_CILBLFSZ = 0
  w_CITXTFSZ = 0
  w_CICBXFSZ = 0
  w_CIBTNFSZ = 0
  w_CIGRDFSZ = 0
  w_CIPAGFSZ = 0
  w_CITBSIZE = 0
  w_CISEARMN = space(2)
  w_CIMENFIX = space(1)
  w_CINOBTNI = space(1)
  w_CIDSKRSS = space(1)
  w_CIOPNGST = space(50)
  w_WAITWND = space(1)
  w_CILBLFIT = space(1)
  w_CITXTFIT = space(1)
  w_CICBXFIT = space(1)
  w_CIBTNFIT = space(1)
  w_CIGRDFIT = space(1)
  w_CIPAGFIT = space(1)
  w_CILBLFBO = space(1)
  w_CITXTFBO = space(1)
  w_CICBXFBO = space(1)
  w_CIBTNFBO = space(1)
  w_CIGRDFBO = space(1)
  w_CIPAGFBO = space(1)
  w_CIMENIMM = space(1)
  w_CIZOOMSC = space(1)
  w_CISEARNT = space(1)
  w_COPOSREC = space(1)
  w_CINRECEN = 0
  w_CINEWPRI = space(1)
  w_CICTRGRD = space(1)
  w_CIZOESPR = space(1)
  w_CITOOBDS = space(1)
  w_CISTASCR = space(1)
  w_CISAVFRM = space(1)
  w_NoReloadMenu = .f.
  w_VisualTheme = 0
  w_oTheme = .NULL.
  w_cPathTheme = space(100)
  w_MENUTOOLBARVISIBLE = .f.
  w_BorderColor = 0
  w_MG_START = space(1)
  w_GSWD_KWD = space(10)
  w_TotGest = 0
  w_GestAtt = 0
  * --- WorkFile variables
  CONF_INT_idx=0
  MYG_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per rendere attive le impostazioni relative all'interfaccia
    *     Invocato da
    *     'Avvio' - CP3START - Legge le impostazioni d'installazione (utente=-1)
    *     'Utente' - CP_LOGIN - Legge se presenti le impostazioni dell'utente appena entrato
    *     'Setup' - GSUT_KCI - Cambia le impostazioni in essere e non chiude la maschera
    *     'Applica' - GSUT_KCI - Cambia le impostazioni in essere
    *     'Open' - CP3START - Apre gestioni all'apertura (non usare Utente o avvio perch� si chiude la NavBar)
    this.w_PADRE = oParentObject
    this.w_NoUserScheduler = vartype(g_UserScheduler)<>"L" or not g_UserScheduler
    if this.pTipo="Open"
      if this.w_NoUserScheduler
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      if this.pTipo="Applica" Or this.pTipo="Eliminato"
        * --- Salvo il contenuto della maschera sul database (non eseguo una ECPSAVE perch� voglio mantenere la finestra aperta)
        this.w_PADRE.Save_GSUT_ACU(.f.)     
        this.w_PADRE.mReplace(.T.)     
        if this.oParentObject.w_CODUTE=IIF( i_bMobileMode, -10, 1)*i_CODUTE or Empty(this.oParentObject.w_CODUTE2) and inlist(this.oParentObject.w_CODUTE,-1,-2) and this.pTipo<>"Eliminato"
          ah_ERRORMSG("� necessario uscire e rientrare dalla procedura per rendere effettivi i cambiamenti apportati.", "i")
        endif
        * --- Ricarico il record per aggiornare il cpccchk (altrimenti al successivo applica
        *     riceverei record modificato da altro utente)
        this.w_PADRE.GSUT_ACU.LoadRec()     
        this.w_PADRE.GSUT_ACU.w_CICODUTE = this.w_PADRE.w_CODUTE
        i_retcode = 'stop'
        return
      endif
      * --- Se padre del batch � Cp3start (Null) allora leggo le impostazioni di default
      if this.pTipo="Avvio" AND g_CODUTE=0
        * --- Se codice utente a 0 allora sono all'apertura (da CP3START) leggo le impostazioni a livello di installazione
        this.w_TEST = -1 + IIF( vartype(i_bMobileMode)<>"U" and i_bMobileMode , -1, 0 )
        this.w_CODUTE = -2
      else
        if VARTYPE(i_ThemesManager)="O" AND VARTYPE(this.w_PADRE)="O" AND VARTYPE(this.w_PADRE.w_FLDELCAC)="C" AND this.w_PADRE.w_FLDELCAC="S"
          L_FH = FCREATE(ADDBS(ALLTRIM(i_ThemesManager.Imgpath))+"bDelGraphics.fla" )
          FCLOSE(L_FH)
        endif
        * --- Leggo per l'utente attivo le informazioni memorizzate sul database
        this.w_TEST = iif(this.pTipo="Avvio" AND g_CODUTE<>0, g_CODUTE, i_CODUTE)
        this.w_CODUTE = iif(this.pTipo="Avvio" AND g_CODUTE<>0, g_CODUTE, i_CODUTE) * IIF( vartype(i_bMobileMode)<>"U" and i_bMobileMode , -10, 1 )
        * --- Read from CONF_INT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONF_INT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2],.t.,this.CONF_INT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CICODUTE"+;
            " from "+i_cTable+" CONF_INT where ";
                +"CICODUTE = "+cp_ToStrODBC(this.w_CODUTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CICODUTE;
            from (i_cTable) where;
                CICODUTE = this.w_CODUTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TEST = NVL(cp_ToDate(_read_.CICODUTE),cp_NullValue(_read_.CICODUTE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_READFROMDB = .F.
      if this.w_TEST = this.w_CODUTE
        this.w_READFROMDB = .T.
        * --- Read from CONF_INT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONF_INT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2],.t.,this.CONF_INT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CIMONFRA,CINEWPRI"+;
            " from "+i_cTable+" CONF_INT where ";
                +"CICODUTE = "+cp_ToStrODBC(-1);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CIMONFRA,CINEWPRI;
            from (i_cTable) where;
                CICODUTE = -1;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CIMONFRA = NVL(cp_ToDate(_read_.CIMONFRA),cp_NullValue(_read_.CIMONFRA))
          this.w_CINEWPRI = NVL(cp_ToDate(_read_.CINEWPRI),cp_NullValue(_read_.CINEWPRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if not i_MonitorFramework AND NVL(this.w_CIMONFRA, " ")="S"
          i_MonitorFramework=.t. 
 cp_SetFWMonitor()
        endif
      else
        * --- Cerco le impostazioni legate all'installazione (Utente -1)
        this.w_TEST = -1 + IIF( vartype(i_bMobileMode)<>"U" and i_bMobileMode , -1, 0 )
        this.w_CODUTE = -1 + IIF( vartype(i_bMobileMode)<>"U" and i_bMobileMode , -1, 0 )
        * --- Read from CONF_INT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONF_INT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2],.t.,this.CONF_INT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CICODUTE,CIMONFRA,CINEWPRI,CI_ILIKE"+;
            " from "+i_cTable+" CONF_INT where ";
                +"CICODUTE = "+cp_ToStrODBC(this.w_CODUTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CICODUTE,CIMONFRA,CINEWPRI,CI_ILIKE;
            from (i_cTable) where;
                CICODUTE = this.w_CODUTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TEST = NVL(cp_ToDate(_read_.CICODUTE),cp_NullValue(_read_.CICODUTE))
          this.w_CIMONFRA = NVL(cp_ToDate(_read_.CIMONFRA),cp_NullValue(_read_.CIMONFRA))
          this.w_CINEWPRI = NVL(cp_ToDate(_read_.CINEWPRI),cp_NullValue(_read_.CINEWPRI))
          g_USE_ILIKE = NVL(cp_ToDate(_read_.CI_ILIKE),cp_NullValue(_read_.CI_ILIKE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if not i_MonitorFramework AND NVL(this.w_CIMONFRA, " ")="S"
          i_MonitorFramework=.t. 
 cp_SetFWMonitor()
        endif
        if this.w_TEST <> this.w_CODUTE
          * --- Valori di default
          *     Se non ho impostazioni ne per l'utente ne per l'installazione 
          *     inserisco i dati per installazione
          * --- Aggiorno la tabella CONF_INT con i valori di default del file XML
          * --- Insert into CONF_INT
          i_nConn=i_TableProp[this.CONF_INT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONF_INT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CICODUTE"+",CIVTHEME"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(-1),'CONF_INT','CICODUTE');
            +","+cp_NullLink(cp_ToStrODBC(i_ThemesManager.ActiveTheme),'CONF_INT','CIVTHEME');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CICODUTE',-1,'CIVTHEME',i_ThemesManager.ActiveTheme)
            insert into (i_cTable) (CICODUTE,CIVTHEME &i_ccchkf. );
               values (;
                 -1;
                 ,i_ThemesManager.ActiveTheme;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.cDefault = iif(Not i_bMobileMode, "Predefinita", "Mobile_Predefinita")
          this.cFileConfig = cPathVfcsim+"\Themes\Default_"+IIF(IsAhr(),"ahr",IIF(IsAhe(),"AHE","AETOP"))+".xml"
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Con DB vuoto abilito la Tool Menu e disabilito men� windows e desktop (come precedente comportamento) nonostante i dati del file di default
          this.w_TOOLMN = "S"
          * --- Write into CONF_INT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CONF_INT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CONF_INT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CITOOLMN ="+cp_NullLink(cp_ToStrODBC(this.w_TOOLMN),'CONF_INT','CITOOLMN');
            +",COPOSREC ="+cp_NullLink(cp_ToStrODBC("0"),'CONF_INT','COPOSREC');
                +i_ccchkf ;
            +" where ";
                +"CICODUTE = "+cp_ToStrODBC(-1);
                   )
          else
            update (i_cTable) set;
                CITOOLMN = this.w_TOOLMN;
                ,COPOSREC = "0";
                &i_ccchkf. ;
             where;
                CICODUTE = -1;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        this.w_READFROMDB = .T.
      endif
      if CP_DBTYPE<>"PostgreSQL"
        g_USE_ILIKE="N"
      endif
      i_bNewPrintSystem= NVL (this.w_CINEWPRI,"@@")="S"
      if NOT i_bNewPrintSystem
        if VARTYPE(g_xfrxpreview)="U"
          public g_xfrxpreview
        endif
        g_xfrxpreview = .f.
      endif
      if this.w_READFROMDB
        if vartype(g_UseProgBar)="U"
           public g_UseProgBar
        endif
        if vartype(g_AttUFore)="U"
          * --- Parametri agenda
           
 public g_Colon 
 public g_DispCnt 
 public g_RicContColor 
 public g_AttUFore 
 public g_AttTFore 
 public g_AttCFore 
 public g_AttUBack 
 public g_AttTBack 
 public g_AttCBack 
 public g_OraLavIni 
 public g_OraLavFin 
 public g_OraStaIni 
 public g_OraStaFin 
 public g_AgeVis 
 public g_ColPrime 
 public g_ColNnPrm 
 public g_ColSelect 
 public g_ColHeader 
 public g_ColAttConf 
 public g_GioStdFontName 
 public g_GioStdFontSize 
 public g_bGioStdFontItalic 
 public g_bGioStdFontBold 
 public g_GioImpFontName 
 public g_GioImpFontSize 
 public g_bGioImpFontItalic 
 public g_bGioImpFontBold 
 public g_AgeDayHeightOffset
        endif
        if Vartype(g_typebalagen)="U"
          public g_typebalagen 
 g_typebalagen="N"
        endif
        if Vartype(g_typecalendar)="U"
          public g_typecalendar 
 g_typecalendar = "S"
        endif
        if Vartype(g_bDskRSS)="U"
          Public g_bDskRSS 
 g_bDskRSS = .F.
        endif
         
 public i_cWindowManagerFontName 
 public i_nWindowManagerFontSize
         
 oldWindowMenu = i_bWindowMenu 
 oldVisualTheme = i_VisualTheme 
 oldDisableMenuImage =g_DisableMenuImage
        public g_PostInColor
        public g_Age_FlComp
        public i_cWarnType
        public i_cZoomMode
        public g_cMaskStartupDimension
        public g_nMaskFixedWidth
        public g_nMaskFixedHeight
        * --- Impostazioni Internazionali
        * --- Read from CONF_INT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONF_INT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2],.t.,this.CONF_INT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" CONF_INT where ";
                +"CICODUTE = "+cp_ToStrODBC(this.w_CODUTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                CICODUTE = this.w_CODUTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          i_nGridLineColor = NVL(cp_ToDate(_read_.CIGRIDCL),cp_NullValue(_read_.CIGRIDCL))
          i_nBackColor = NVL(cp_ToDate(_read_.CISELECL),cp_NullValue(_read_.CISELECL))
          i_nEBackColor = NVL(cp_ToDate(_read_.CIEDTCOL),cp_NullValue(_read_.CIEDTCOL))
          i_nOBLCOLOR = NVL(cp_ToDate(_read_.CIOBLCOL),cp_NullValue(_read_.CIOBLCOL))
          i_udisabledforecolor = NVL(cp_ToDate(_read_.CIDSBLFC),cp_NullValue(_read_.CIDSBLFC))
          i_udisabledbackcolor = NVL(cp_ToDate(_read_.CIDSBLBC),cp_NullValue(_read_.CIDSBLBC))
          this.w_STABAR = NVL(cp_ToDate(_read_.CISTABAR),cp_NullValue(_read_.CISTABAR))
          i_nXPTheme = NVL(cp_ToDate(_read_.CIXPTHEM),cp_NullValue(_read_.CIXPTHEM))
          this.w_PROGBAR = NVL(cp_ToDate(_read_.CIPROBAR),cp_NullValue(_read_.CIPROBAR))
          i_cViewMode = NVL(cp_ToDate(_read_.CIMDIFRM),cp_NullValue(_read_.CIMDIFRM))
          g_nNumRecent = NVL(cp_ToDate(_read_.CINUMREC),cp_NullValue(_read_.CINUMREC))
          this.w_WNDMEN = NVL(cp_ToDate(_read_.CIWNDMEN),cp_NullValue(_read_.CIWNDMEN))
          this.w_DSKBAR = NVL(cp_ToDate(_read_.CIDSKBAR),cp_NullValue(_read_.CIDSKBAR))
          this.w_CPTBAR = NVL(cp_ToDate(_read_.CICPTBAR),cp_NullValue(_read_.CICPTBAR))
          i_nScreenColor = NVL(cp_ToDate(_read_.CISCRCOL),cp_NullValue(_read_.CISCRCOL))
          this.w_TOOLMN = NVL(cp_ToDate(_read_.CITOOLMN),cp_NullValue(_read_.CITOOLMN))
          this.w_SHWMEN = NVL(cp_ToDate(_read_.CISHWMEN),cp_NullValue(_read_.CISHWMEN))
          i_cZBtnShw = NVL(cp_ToDate(_read_.CISHWBTN),cp_NullValue(_read_.CISHWBTN))
          i_nDtlRowClr = NVL(cp_ToDate(_read_.CIDTLCLR),cp_NullValue(_read_.CIDTLCLR))
          this.w_LOADBUTT = NVL(cp_ToDate(_read_.CIFRMBUT),cp_NullValue(_read_.CIFRMBUT))
          this.w_PAGEBUTT = NVL(cp_ToDate(_read_.CIFRMPAG),cp_NullValue(_read_.CIFRMPAG))
          this.w_SYSFRM = NVL(cp_ToDate(_read_.CISYSFRM),cp_NullValue(_read_.CISYSFRM))
          this.w_SETDAT = NVL(cp_ToDate(_read_.CISETDAT),cp_NullValue(_read_.CISETDAT))
          this.w_SETCEN = NVL(cp_ToDate(_read_.CISETCEN),cp_NullValue(_read_.CISETCEN))
          this.w_SETPNT = NVL(cp_ToDate(_read_.CISETPNT),cp_NullValue(_read_.CISETPNT))
          this.w_SETSEP = NVL(cp_ToDate(_read_.CISETSEP),cp_NullValue(_read_.CISETSEP))
          this.w_SETMRK = NVL(cp_ToDate(_read_.CISETMRK),cp_NullValue(_read_.CISETMRK))
          this.w_DispColon = NVL(cp_ToDate(_read_.CI_COLON),cp_NullValue(_read_.CI_COLON))
          g_DispCnt = NVL(cp_ToDate(_read_.CIDSPCNT),cp_NullValue(_read_.CIDSPCNT))
          g_RicContColor = NVL(cp_ToDate(_read_.CIRICCOL),cp_NullValue(_read_.CIRICCOL))
          g_AttUFore = NVL(cp_ToDate(_read_.CIATUCOL),cp_NullValue(_read_.CIATUCOL))
          g_AttTFore = NVL(cp_ToDate(_read_.CIATTCOL),cp_NullValue(_read_.CIATTCOL))
          g_AttCFore = NVL(cp_ToDate(_read_.CIATCCOL),cp_NullValue(_read_.CIATCCOL))
          i_nEviRigaZoom = NVL(cp_ToDate(_read_.CIEVIZOM),cp_NullValue(_read_.CIEVIZOM))
          i_nZoomColor = NVL(cp_ToDate(_read_.CICOLZOM),cp_NullValue(_read_.CICOLZOM))
          g_OraLavIni = NVL(cp_ToDate(_read_.CIORAINI),cp_NullValue(_read_.CIORAINI))
          g_OraLavFin = NVL(cp_ToDate(_read_.CIORAFIN),cp_NullValue(_read_.CIORAFIN))
          g_AgeVis = NVL(cp_ToDate(_read_.CIAGEVIS),cp_NullValue(_read_.CIAGEVIS))
          g_ColPrime = NVL(cp_ToDate(_read_.CICOLPRM),cp_NullValue(_read_.CICOLPRM))
          g_ColNnPrm = NVL(cp_ToDate(_read_.CICOLNNP),cp_NullValue(_read_.CICOLNNP))
          g_ColSelect = NVL(cp_ToDate(_read_.CICOLSEL),cp_NullValue(_read_.CICOLSEL))
          g_ColHeader = NVL(cp_ToDate(_read_.CICOLHDR),cp_NullValue(_read_.CICOLHDR))
          g_OraStaIni = NVL(cp_ToDate(_read_.CISTAINI),cp_NullValue(_read_.CISTAINI))
          g_OraStaFin = NVL(cp_ToDate(_read_.CISTAFIN),cp_NullValue(_read_.CISTAFIN))
          this.w_VisualTheme = NVL(cp_ToDate(_read_.CIVTHEME),cp_NullValue(_read_.CIVTHEME))
          i_cMenuTab = NVL(cp_ToDate(_read_.CITABMEN),cp_NullValue(_read_.CITABMEN))
          w_CIBCKGRD = NVL(cp_ToDate(_read_.CIBCKGRD),cp_NullValue(_read_.CIBCKGRD))
          w_CIMRKGRD = NVL(cp_ToDate(_read_.CIMRKGRD),cp_NullValue(_read_.CIMRKGRD))
          i_cDeskMenu = NVL(cp_ToDate(_read_.CIDSKMEN),cp_NullValue(_read_.CIDSKMEN))
          this.w_CILBLFNM = NVL(cp_ToDate(_read_.CILBLFNM),cp_NullValue(_read_.CILBLFNM))
          this.w_CITXTFNM = NVL(cp_ToDate(_read_.CITXTFNM),cp_NullValue(_read_.CITXTFNM))
          this.w_CICBXFNM = NVL(cp_ToDate(_read_.CICBXFNM),cp_NullValue(_read_.CICBXFNM))
          this.w_CIBTNFNM = NVL(cp_ToDate(_read_.CIBTNFNM),cp_NullValue(_read_.CIBTNFNM))
          this.w_CIGRDFNM = NVL(cp_ToDate(_read_.CIGRDFNM),cp_NullValue(_read_.CIGRDFNM))
          this.w_CIPAGFNM = NVL(cp_ToDate(_read_.CIPAGFNM),cp_NullValue(_read_.CIPAGFNM))
          this.w_CILBLFSZ = NVL(cp_ToDate(_read_.CILBLFSZ),cp_NullValue(_read_.CILBLFSZ))
          this.w_CITXTFSZ = NVL(cp_ToDate(_read_.CITXTFSZ),cp_NullValue(_read_.CITXTFSZ))
          this.w_CICBXFSZ = NVL(cp_ToDate(_read_.CICBXFSZ),cp_NullValue(_read_.CICBXFSZ))
          this.w_CIBTNFSZ = NVL(cp_ToDate(_read_.CIBTNFSZ),cp_NullValue(_read_.CIBTNFSZ))
          this.w_CIGRDFSZ = NVL(cp_ToDate(_read_.CIGRDFSZ),cp_NullValue(_read_.CIGRDFSZ))
          this.w_CIPAGFSZ = NVL(cp_ToDate(_read_.CIPAGFSZ),cp_NullValue(_read_.CIPAGFSZ))
          g_ColAttConf = NVL(cp_ToDate(_read_.CICOLACF),cp_NullValue(_read_.CICOLACF))
          i_cMenuNavFontName = NVL(cp_ToDate(_read_.CIMNAFNM),cp_NullValue(_read_.CIMNAFNM))
          i_nMenuNavFontSize = NVL(cp_ToDate(_read_.CIMNAFSZ),cp_NullValue(_read_.CIMNAFSZ))
          i_cWindowManagerFontName = NVL(cp_ToDate(_read_.CIWMAFNM),cp_NullValue(_read_.CIWMAFNM))
          i_nWindowManagerFontSize = NVL(cp_ToDate(_read_.CIWMAFSZ),cp_NullValue(_read_.CIWMAFSZ))
          i_nDeskMenuMaxButton = NVL(cp_ToDate(_read_.CINAVNUB),cp_NullValue(_read_.CINAVNUB))
          i_cDeskMenuStatus = NVL(cp_ToDate(_read_.CINAVSTA),cp_NullValue(_read_.CINAVSTA))
          i_nDeskMenuInitDim = NVL(cp_ToDate(_read_.CINAVDIM),cp_NullValue(_read_.CINAVDIM))
          this.w_CITBSIZE = NVL(cp_ToDate(_read_.CITBSIZE),cp_NullValue(_read_.CITBSIZE))
          this.w_CISEARMN = NVL(cp_ToDate(_read_.CISEARMN),cp_NullValue(_read_.CISEARMN))
          this.w_CIMENFIX = NVL(cp_ToDate(_read_.CIMENFIX),cp_NullValue(_read_.CIMENFIX))
          g_RicPerCont = NVL(cp_ToDate(_read_.CIRICONT),cp_NullValue(_read_.CIRICONT))
          g_MinutiPostIN = NVL(cp_ToDate(_read_.CIMINPST),cp_NullValue(_read_.CIMINPST))
          g_ModoPostIN = NVL(cp_ToDate(_read_.CIMODPST),cp_NullValue(_read_.CIMODPST))
          this.w_CINOBTNI = NVL(cp_ToDate(_read_.CINOBTNI),cp_NullValue(_read_.CINOBTNI))
          g_PostInColor = NVL(cp_ToDate(_read_.CICOLPST),cp_NullValue(_read_.CICOLPST))
          g_Age_FlComp = NVL(cp_ToDate(_read_.CIFLCOMP),cp_NullValue(_read_.CIFLCOMP))
          this.w_CIDSKRSS = NVL(cp_ToDate(_read_.CIDSKRSS),cp_NullValue(_read_.CIDSKRSS))
          this.w_WAITWND = NVL(cp_ToDate(_read_.CIWAITWD),cp_NullValue(_read_.CIWAITWD))
          this.w_CILBLFIT = NVL(cp_ToDate(_read_.CILBLFIT),cp_NullValue(_read_.CILBLFIT))
          this.w_CITXTFIT = NVL(cp_ToDate(_read_.CITXTFIT),cp_NullValue(_read_.CITXTFIT))
          this.w_CICBXFIT = NVL(cp_ToDate(_read_.CICBXFIT),cp_NullValue(_read_.CICBXFIT))
          this.w_CIBTNFIT = NVL(cp_ToDate(_read_.CIBTNFIT),cp_NullValue(_read_.CIBTNFIT))
          this.w_CIGRDFIT = NVL(cp_ToDate(_read_.CIGRDFIT),cp_NullValue(_read_.CIGRDFIT))
          this.w_CIPAGFIT = NVL(cp_ToDate(_read_.CIPAGFIT),cp_NullValue(_read_.CIPAGFIT))
          this.w_CILBLFBO = NVL(cp_ToDate(_read_.CILBLFBO),cp_NullValue(_read_.CILBLFBO))
          this.w_CITXTFBO = NVL(cp_ToDate(_read_.CITXTFBO),cp_NullValue(_read_.CITXTFBO))
          this.w_CICBXFBO = NVL(cp_ToDate(_read_.CICBXFBO),cp_NullValue(_read_.CICBXFBO))
          this.w_CIBTNFBO = NVL(cp_ToDate(_read_.CIBTNFBO),cp_NullValue(_read_.CIBTNFBO))
          this.w_CIGRDFBO = NVL(cp_ToDate(_read_.CIGRDFBO),cp_NullValue(_read_.CIGRDFBO))
          this.w_CIPAGFBO = NVL(cp_ToDate(_read_.CIPAGFBO),cp_NullValue(_read_.CIPAGFBO))
          this.w_CIMENIMM = NVL(cp_ToDate(_read_.CIMENIMM),cp_NullValue(_read_.CIMENIMM))
          this.w_CICTRGRD = NVL(cp_ToDate(_read_.CICTRGRD),cp_NullValue(_read_.CICTRGRD))
          i_nHeaderHeight = NVL(cp_ToDate(_read_.CIHEHEZO),cp_NullValue(_read_.CIHEHEZO))
          i_nRowHeight = NVL(cp_ToDate(_read_.CIHEROZO),cp_NullValue(_read_.CIHEROZO))
          i_cWarnType = NVL(cp_ToDate(_read_.CIWARTYP),cp_NullValue(_read_.CIWARTYP))
          i_cZoomMode = NVL(cp_ToDate(_read_.CIZOOMMO),cp_NullValue(_read_.CIZOOMMO))
          g_GioStdFontName = NVL(cp_ToDate(_read_.CIAGSFNM),cp_NullValue(_read_.CIAGSFNM))
          g_GioStdFontSize = NVL(cp_ToDate(_read_.CIAGSFSZ),cp_NullValue(_read_.CIAGSFSZ))
          g_bGioStdFontItalic = NVL(cp_ToDate(_read_.CIAGSFIT),cp_NullValue(_read_.CIAGSFIT))
          g_bGioStdFontBold = NVL(cp_ToDate(_read_.CIAGSFBO),cp_NullValue(_read_.CIAGSFBO))
          g_GioImpFontName = NVL(cp_ToDate(_read_.CIAGIFNM),cp_NullValue(_read_.CIAGIFNM))
          g_GioImpFontSize = NVL(cp_ToDate(_read_.CIAGIFSZ),cp_NullValue(_read_.CIAGIFSZ))
          g_bGioImpFontItalic = NVL(cp_ToDate(_read_.CIAGIFIT),cp_NullValue(_read_.CIAGIFIT))
          g_bGioImpFontBold = NVL(cp_ToDate(_read_.CIAGIFBO),cp_NullValue(_read_.CIAGIFBO))
          g_AgeDayHeightOffset = NVL(cp_ToDate(_read_.CIGIORHO),cp_NullValue(_read_.CIGIORHO))
          this.w_CIZOOMSC = NVL(cp_ToDate(_read_.CIZOOMSC),cp_NullValue(_read_.CIZOOMSC))
          g_cMaskStartupDimension = NVL(cp_ToDate(_read_.CIDIMFRM),cp_NullValue(_read_.CIDIMFRM))
          g_nMaskFixedWidth = NVL(cp_ToDate(_read_.CIWIDFRM),cp_NullValue(_read_.CIWIDFRM))
          g_nMaskFixedHeight = NVL(cp_ToDate(_read_.CIHEIFRM),cp_NullValue(_read_.CIHEIFRM))
          this.w_CISEARNT = NVL(cp_ToDate(_read_.CISEARNT),cp_NullValue(_read_.CISEARNT))
          this.w_COPOSREC = NVL(cp_ToDate(_read_.COPOSREC),cp_NullValue(_read_.COPOSREC))
          this.w_CINRECEN = NVL(cp_ToDate(_read_.CINRECEN),cp_NullValue(_read_.CINRECEN))
          this.w_CIZOESPR = NVL(cp_ToDate(_read_.CIZOESPR),cp_NullValue(_read_.CIZOESPR))
          g_typebalagen = NVL(cp_ToDate(_read_.CITYPBAL),cp_NullValue(_read_.CITYPBAL))
          g_typecalendar = NVL(cp_ToDate(_read_.CITYPCAL                ),cp_NullValue(_read_.CITYPCAL                ))
          this.w_CITOOBDS = NVL(cp_ToDate(_read_.CITOOBDS),cp_NullValue(_read_.CITOOBDS))
          this.w_CISTASCR = NVL(cp_ToDate(_read_.CISTASCR),cp_NullValue(_read_.CISTASCR))
          this.w_CISAVFRM = NVL(cp_ToDate(_read_.CISAVFRM),cp_NullValue(_read_.CISAVFRM))
          g_AB_VIEWBUTTON = NVL(cp_ToDate(_read_.CITBVWBT),cp_NullValue(_read_.CITBVWBT))
          g_cGadgetInterface = NVL(cp_ToDate(_read_.CIGADGET),cp_NullValue(_read_.CIGADGET))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        i_cDeskMenu = iif(this.w_NoUserScheduler And g_cGadgetInterface<>"S", i_cDeskMenu, "H")
        * --- Controllo se scelta interfaccia interfaccia Windows 8
        public i_bWindows8
        if .f.
          if .f.
            i_bWindows8=.t.
            _screen.TitleBar= 0 
 _screen.WindowState= 2
          endif
        else
          _screen.TitleBar= 1
          i_bWindows8=.f.
        endif
        * --- --
        i_cConfSavePosForm = NVL(this.w_CISAVFRM," ")
        if TYPE("i_bShowStartScreen")<>"L"
          public i_bShowStartScreen
        endif
        i_bShowStartScreen = NVL(this.w_CISTASCR," ")="S"
        * --- --
        if this.w_SYSFRM="S"
          * --- Setto impostazioni S.O.
          SET SYSFORMATS ON
        else
          * --- Impostazioni Ad Hoc
          SET SYSFORMATS OFF
          if this.w_SETCEN="S"
            SET CENTURY ON
          else
            SET CENTURY OFF
          endif
          do case
            case this.w_SETDAT="A"
              SET DATE AMERICAN
            case this.w_SETDAT="N"
              SET DATE ANSI
            case this.w_SETDAT="I"
              SET DATE ITALIAN
            case this.w_SETDAT="B"
              SET DATE BRITISH
            case this.w_SETDAT="G"
              SET DATE GERMAN
            case this.w_SETDAT="J"
              SET DATE JAPAN
            case this.w_SETDAT="T"
              SET DATE TAIWAN
            case this.w_SETDAT="U"
              SET DATE USA
            case this.w_SETDAT="M"
              SET DATE MDY
            case this.w_SETDAT="D"
              SET DATE DMY
            case this.w_SETDAT="Y"
              SET DATE YMD
            case this.w_SETDAT="S"
              SET DATE SHORT
            case this.w_SETDAT="L"
              SET DATE LONG
            otherwise
              SET DATE ITALIAN
          endcase
           
 macro='SET MARK TO "'+IIF(!EMPTY(this.w_SETMRK), this.w_SETMRK, "-")+'"' 
 &macro 
 macro='SET POINT TO "'+IIF(!EMPTY(this.w_SETPNT), this.w_SETPNT, ",")+'"' 
 &macro 
 macro='SET SEPARATOR TO "'+IIF(!EMPTY(this.w_SETSEP), this.w_SETSEP, ".")+'"' 
 &macro
        endif
         
 i_cFontName = Alltrim(this.w_CITXTFNM) 
 i_cCboxFontName = Alltrim(this.w_CICBXFNM) 
 i_cBtnFontName =Alltrim(this.w_CIBTNFNM) 
 i_cPageFontName = Alltrim(this.w_CIPAGFNM) 
 i_cGrdFontName = Alltrim(this.w_CIGRDFNM) 
 i_cLblFontName = Alltrim(this.w_CILBLFNM)
         
 i_nFontSize = this.w_CITXTFSZ 
 i_nPageFontSize = this.w_CIPAGFSZ 
 i_nCboxFontSize = this.w_CICBXFSZ 
 i_nBtnFontSize = this.w_CIBTNFSZ 
 i_nGrdFontSize = this.w_CIGRDFSZ 
 i_nLblFontSize = this.w_CILBLFSZ
         
 *-- Font Italic 
 i_bLblFontItalic = this.w_CILBLFIT="S" 
 i_bFontItalic = this.w_CITXTFIT="S" 
 i_bCboxFontItalic = this.w_CICBXFIT="S" 
 i_bBtnFontItalic = this.w_CIBTNFIT="S" 
 i_bGrdFontItalic = this.w_CIGRDFIT="S" 
 i_bPageFontItalic = this.w_CIPAGFIT="S" 
 
 *-- Font Bold 
 i_bLblFontBold = this.w_CILBLFBO="S" 
 i_bFontBold = this.w_CITXTFBO="S" 
 i_bCboxFontBold = this.w_CICBXFBO="S" 
 i_bBtnFontBold = this.w_CIBTNFBO="S" 
 i_bGrdFontBold = this.w_CIGRDFBO="S" 
 i_bPageFontBold = this.w_CIPAGFBO="S"
        g_GioStdFontName = Alltrim(g_GioStdFontName) 
 g_bGioStdFontItalic = g_bGioStdFontItalic = "S" 
 g_bGioStdFontBold = g_bGioStdFontBold = "S" 
 g_GioImpFontName = Alltrim(g_GioImpFontName) 
 g_bGioImpFontItalic = g_bGioImpFontItalic = "S" 
 g_bGioImpFontBold = g_bGioImpFontBold = "S"
        * --- Cambio tema 
        this.w_NoReloadMenu = i_ThemesManager.ThemeNumber = this.w_VisualTheme
         
 i_nTBTNW=this.w_CITBSIZE 
 i_nTBTNH=this.w_CITBSIZE
         
 g_UseProgBar = this.w_PROGBAR="S" 
 i_bShowCPToolBar = this.w_CPTBAR <> "N" 
 i_bShowDeskTopBar = not this.w_NoUserScheduler or this.w_DSKBAR <> "N" 
 i_nCPToolBarPos = ICASE( this.w_CPTBAR = "L", 1, this.w_CPTBAR = "R", 2, this.w_CPTBAR = "D", 3, this.w_CPTBAR = "F", -1, 0 ) 
 i_nDeskTopBarPos = ICASE( this.w_DSKBAR = "L", 1, this.w_DSKBAR = "R", 2, this.w_DSKBAR = "D", 3, this.w_DSKBAR = "F", -1, 0 ) 
 i_bToolBarDisappear=this.w_CITOOBDS="S" 
 i_bWindowMenu = this.w_NoUserScheduler and this.w_WNDMEN = "S" 
 i_bShowToolMenu = this.w_NoUserScheduler and this.w_TOOLMN = "S" 
 g_bShowMenu = this.w_NoUserScheduler and this.w_SHWMEN = "S" 
 i_bLoadFuncButton = this.w_LOADBUTT = "S" 
 i_bPageFrmButton = this.w_PAGEBUTT = "S" 
 g_COLON = !(this.w_DispColon = "N") 
 g_NoButtonImage = this.w_CINOBTNI = "S"
         
 i_bGradientBck = w_CIBCKGRD = "S" 
 i_bRecordMark = w_CIMRKGRD = "S" 
 i_bSearchMenu = SUBSTR(this.w_CISEARMN,1,1) = "S" 
 i_bSearchMenuDesk = NVL(SUBSTR(this.w_CISEARMN,2,1)," ") = "S" 
 i_bMenuFix = this.w_CIMENFIX = "S" 
 g_bDskRSS = this.w_CIDSKRSS = "S" 
 i_bWaitWindowTheme = this.w_WAITWND="S" 
 g_DisableMenuImage = this.w_CIMENIMM = "S" 
 i_AdvancedHeaderZoom = this.w_CICTRGRD="S" 
 i_bExpandZoomParameter = this.w_CIZOESPR="S" 
 i_nHeaderHeight = iif(i_nHeaderHeight=0, 19, i_nHeaderHeight) 
 i_nRowHeight = iif(i_nRowHeight=0, 19, i_nRowHeight) 
 i_bRecentMenu_navbar = this.w_CISEARNT="S" and this.w_COPOSREC $ "2367NE" and this.w_NoUserScheduler 
 i_bRecentMenu_toolbar = this.w_CISEARNT="S" and this.w_COPOSREC $ "4567TE" and this.w_NoUserScheduler 
 i_bRecentMenu_windows = this.w_CISEARNT="S" and this.w_COPOSREC $ "1357NTE" and this.w_NoUserScheduler 
 g_nRecRecent = this.w_CINRECEN
        if vartype(cCursRecen)="C" and used(cCursRecen)
          select (cCursRecen)
          zap
          GSUT_BMR( "C" )
        endif
        if this.pTipo="Setup" or this.pTipo="Applica"
          this.w_oTheme = this.w_Padre.Gsut_Acu.GetCtrl("w_CIVTHEME")
          this.w_cPathTheme = this.w_oTheme.GetPath()
          i_ThemesManager.ReadXMLTheme(this.w_cPathTheme)
        endif
        this.w_cPathTheme = This.ThemePath(this.w_VisualTheme)
        if Not Empty (this.w_cPathTheme)
          i_ThemesManager.ReadXMLTheme(this.w_cPathTheme)
        endif
        if this.pTipo<>"Avvio" Or Not Empty (this.w_cPathTheme)
          i_ThemesManager.ThemeNumber = this.w_VisualTheme
          i_VisualTheme = this.w_VisualTheme
        else
          i_VisualTheme = -1
        endif
        * --- Lettura variabili pubblice relative ai moduli
        if this.pTipo="Utente" and i_CODUTE<>0
          do GSUT_BRP with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Ricarico il menu con o senza il menu Window
        if oldWindowMenu <> i_bWindowMenu OR oldDisableMenuImage <>g_DisableMenuImage
          * --- controllo che sia iniziliazzata i_CUR_MENU:
          *     Il controllo � necessario perch� tale variabile � inizializzata all'avvio
          *     della cp_menu, cosa che avviene dopo questa procedura.
          if this.pTipo="Setup" AND TYPE( "i_CUR_MENU" )="C" And (!this.w_NoReloadMenu OR oldDisableMenuImage <>g_DisableMenuImage)
             
 USE IN SELECT( i_CUR_MENU ) 
 CP_MENU()
          endif
        endif
        * --- Controllo cambio tema
        *     Se passo da std a nuovo o viceversa devo ricreare le toolbar e menu
        *     oldVisualTheme contiene il vecchio valore del tema
        *     -1 = std
        if (oldVisualTheme = -1 And i_VisualTheme <> - 1) Or (oldVisualTheme <> -1 And i_VisualTheme = - 1) Or oldVisualTheme <> i_VisualTheme
          cp_CreateCacheFile()
          * --- Se cambio da nuova a standard devo sgangiare le toolbar in modo tale da non avere pi� visibile la dockarea (poich� resta colorata)
          oCpToolBar.Visible=.F.
          if VARTYPE(oDesktopBar)="O"
            oDesktopBar.Visible= .F.
          endif
          if OldVisualTheme <> -1 AND VARTYPE(i_MenuToolbar) = "O"
            i_MenuToolbar.Visible=.F.
          endif
          * --- ToolBar sistema
           
 oCpToolBar.Destroy() 
 oCpToolBar = .null. 
 If Type("OCPTOOLBAR")="O" 
 Release oCpToolBar 
 Endif 
 Public oCpToolBar 
 oCpToolBar=Createobject("CPToolBar", i_VisualTheme<>-1 And Vartype(_Screen.cp_themesmanager)=="O") 
 oCPToolBar.Dock(i_nCPToolBarPos)
          * --- Application bar
          if VARTYPE(oDesktopBar)="O"
             
 oDesktopBar.Destroy() 
 oDesktopBar = .Null. 
 cp_desk() 
 oDesktopBar.SetAfterCpToolbar()
          endif
          * --- Menu
          if OldVisualTheme <> -1 AND VARTYPE(i_MenuToolbar) = "O"
             
 i_MenuToolbar.Visible=.F. 
 cb_Destroy(i_MenuToolbar.hWnd) 
 i_MenuToolbar.oPopupMenu.oParentObject=.Null. 
 i_MenuToolbar.Destroy() 
 i_MenuToolbar = .null. 
 RELEASE i_MenuToolbar
          else
            HIDE MENU _MSYSMENU
          endif
          if this.pTipo<>"Avvio" And VARTYPE(i_CUR_MENU)="C" And USED(i_CUR_MENU)
            CP_MENU()
          endif
        endif
        * --- Status Bar
        if this.w_STABAR="S"
           
 SET STATUS BAR ON 
 SET STATUS OFF
          if i_VisualTheme <> -1
             
 i_oStatusBar = CreateObject("cp_StatusBar") 
 i_oStatusBar.addpanel(0,100,.t.) 
 i_oStatusBar.addpanel(3) &&OVR 
 i_oStatusBar.addpanel(1) &&NUM 
 i_oStatusBar.addpanel(2) &&CAPS
          endif
        else
          if i_VisualTheme <> -1
            i_oStatusBar = .null.
          endif
           
 SET STATUS BAR OFF
        endif
        * --- Mostra / Nasconde Menu 
        if this.w_SHWMEN = "S" and this.w_NoUserScheduler
          if i_VisualTheme <> -1
            if Type("i_MenuToolbar")<>"U"
              i_MenuToolbar.Visible = .T.
            endif
          else
            if this.pTipo<>"Avvio"
              SHOW MENU _MSYSMENU
            endif
          endif
          g_bShowHideMenu = .T.
        else
          if i_VisualTheme <> -1
            if Type("i_MenuToolbar")<>"U"
              i_MenuToolbar.Visible = .F.
            endif
          else
            HIDE MENU _MSYSMENU
          endif
          g_bShowHideMenu = .F.
        endif
        public i_bZoomSingleClickSelMode
        i_bZoomSingleClickSelMode = this.w_CIZOOMSC = "S"
        * --- Abilito ToolBar e setto screen color
        oCPToolBar.Dock( i_nCPToolBarPos ) 
         
 oCPToolBar.visible = i_bShowCPToolBar and !i_bToolBarDisappear and i_CODUTE<>0 
 oCPToolBar.Customize = i_bShowCPToolBar
      endif
      SYS(2700,i_nXPTheme)
      if (i_nXPTheme=1 and i_nZBtnWidth<14) or this.w_CODUTE<-1
        * --- Se tema Xp Attivo il bottone contestuale al mimimo deve essere largo 14 pixel 
        *     altrimenti risulta tagliato
        *     Se premo applica oppure cambia impostazione tra installazioen e utente,
        *      non funziona da Xp attivo a disattivo, per farlo funzionare 
        *     occorre memorizzare in un'ulteriore var. il valore prima della modifica..
        Public i_OldZBtnWidth 
 i_OldZBtnWidth=i_nZBtnWidth 
 i_nZBtnWidth=IIF(this.w_CODUTE<-1, 22 ,14)
        * --- Rimuovo la classe del bottoncino nel caso in cui le impostazioni
        *     relative al tema Xp differiscono tra Installazione e utente...
        Clear Class btnZoom
      endif
      if i_nXPTheme=0 And i_nZBtnWidth=14 And VarType(i_OldZBtnWidth)="N" And i_nZBtnWidth<>i_OldZBtnWidth
        i_nZBtnWidth=i_OldZBtnWidth
        Clear Class btnZoom
      endif
      * --- Desktop Menu
      if VarType(i_oDeskmenu)="O"
         
 i_oDeskmenu.ChangeSettings()
      endif
      * --- Toolbar
      if VarType(ocptoolbar)="O"
         
 ocptoolbar.ChangeSettings()
      endif
      * --- Application bar
      if VarType(oDesktopBar)="O"
         
 oDesktopBar.ChangeSettings()
      endif
      if this.pTipo<>"Avvio"
        if VarType(i_oDeskmenu)="O"
          _Screen.RemoveObject("NavBar") 
 Release i_oDeskmenu 
 _Screen.RemoveObject("ImgBackground") 
 _Screen.RemoveObject("TBMDI")
        endif
        if i_cDeskMenu<>"N" and this.w_NoUserScheduler
           
 Do cp_NavBar
        endif
        if VarType(i_oDeskmenu)="O" And i_cDeskMenu="O" and (this.pTipo<>"Utente" or (this.pTipo="Utente" and !g_INIZ))
          * --- Lo lancio due volte, la prima crea l'oggetto la seconda carica il menu
           
 Do cp_NavBar
          i_oDeskmenu.Visible=.F. 
 i_oDeskmenu.ChangeSettings() 
 i_oDeskmenu.Visible=.T.
        endif
        * --- Ricerca voci menu
        if i_VisualTheme <> -1
          if i_bSearchMenu
            if TYPE("i_MenuToolbar.oSEARCHMENU") <> "O" and VARTYPE(i_MenuToolbar) = "O"
              this.w_MENUTOOLBARVISIBLE = i_MenuToolbar.Visible
              i_MenuToolbar.Visible=.F. 
              i_MenuToolbar.Addobject("oSearchMenu", "SearchMenu", i_MenuToolbar.oPopupMenu.cCursorName, "CERCA") 
 i_MenuToolbar.oSearchMenu.Visible=.T. 
 i_MenuToolbar.Rearrange()
              i_MenuToolbar.Visible = this.w_MENUTOOLBARVISIBLE
            endif
          else
            if TYPE("i_MenuToolbar.oSEARCHMENU") = "O" and VARTYPE(i_MenuToolbar) = "O"
              this.w_MENUTOOLBARVISIBLE = i_MenuToolbar.Visible
              i_MenuToolbar.Visible=.F. 
              i_MenuToolbar.RemoveObject("oSearchMenu") 
 i_MenuToolbar.Rearrange()
              i_MenuToolbar.Visible = this.w_MENUTOOLBARVISIBLE
            endif
          endif
          * --- Menu in posizione fissa
          if VARTYPE(i_MenuToolbar) = "O"
             
 i_MenuToolbar.Dock(0,-1,0) 
 i_MenuToolbar.Movable = !i_bMenuFix 
 i_MenuToolbar.Gripper = !i_bMenuFix
          endif
          * --- ricerca dei recenti della toolbar
          if i_bRecentMenu_toolbar and VARTYPE(odesktopbar)="O"
            if TYPE("odesktopbar.oRecentMenu") <> "O"
              this.w_MENUTOOLBARVISIBLE = odesktopbar.Visible
              if VARTYPE(cCursRecen)<>"C"
                PUBLIC cCursRecen
              endif
              if EMPTY(cCursRecen)
                cCursRecen = SYS(2015)
              endif
              odesktopbar.Visible=.F. 
              odesktopbar.Addobject("oRecentMenu", "SearchRecent", cCursRecen, "TOOLBAR")
              odesktopbar.oRecentMenu.Visible=.T. 
              odesktopbar.Visible = this.w_MENUTOOLBARVISIBLE
              GSUT_BMR( "C" )
            endif
          else
            if TYPE("odesktopbar.oRecentMenu") = "O"
              this.w_MENUTOOLBARVISIBLE = odesktopbar.Visible
              odesktopbar.Visible=.F. 
              odesktopbar.RemoveObject("oRecentMenu")
              odesktopbar.Visible = this.w_MENUTOOLBARVISIBLE
            endif
          endif
          * --- Colore Bordi Controlli
          this.w_BorderColor = i_ThemesManager.GetProp(122)
          if this.w_BorderColor<>-1
            i_nBorderColor = this.w_BorderColor
          else
            i_nBorderColor = i_ThemesManager.oThemesManagerSetup.i_nBorderColor
          endif
        endif
        if TYPE("_screen.bckImg")="O"
          UNBINDEVENTS(_SCREEN,"Resize",_screen.BCKIMG,"DesktopResize")
          _Screen.RemoveObject("bckimg")
        endif
        if TYPE("_screen.ahlogo")="O"
          _Screen.RemoveObject("ahlogo")
        endif
        if i_nScreenColor=RGB(255,255,255) and empty(_screen.picture) and (isahr() or isahe()) and not g_iniz and NOT g_TERMINALSS
          if At("vm_lib",Lower(Set("proc")))=0
            Set Proc To vm_lib Additive
          endif
          _Screen.AddObject("bckimg","vm_frm_bck_image")
          _screen.bckImg.Width=_screen.Width 
 _screen.bckImg.Height=_screen.height
          _SCREEN.bckImg.Setup("", "S", RGB(255,255,255), iif(i_ThemesManager.GetSingleProp(54, 2)<i_ThemesManager.GetSingleProp(54, 3),i_ThemesManager.GetSingleProp(54, 2),i_ThemesManager.GetSingleProp(54, 3)), 1, _screen.Width, _screen.Height)
          _Screen.AddObject("ahlogo","image")
          if isahr()
            _Screen.ahlogo.Width=307 
 _Screen.ahlogo.Height=193 
 _Screen.ahlogo.Left=(_screen.Width-307)/2 
 _Screen.ahlogo.Top=(_screen.Height-193)/2 
 _Screen.ahlogo.Picture="bmp\ahr_logo.png"
          else
            _Screen.ahlogo.Width=338 
 _Screen.ahlogo.Left=(_screen.Width-338)/2
            if g_DEMO
              _Screen.ahlogo.Height=297 
 _Screen.ahlogo.Top=(_screen.Height-297)/2 
 _Screen.ahlogo.Picture="bmp\verdim.png"
            else
              _Screen.ahlogo.Height=200 
 _Screen.ahlogo.Top=(_screen.Height-200)/2 
 _Screen.ahlogo.Picture="bmp\ahe_logo.png"
            endif
          endif
          _Screen.ahlogo.visible=.t.
          _SCREEN.ahlogo.ZOrder(1)
          _SCREEN.bckImg.ZOrder(1)
          BINDEVENT(_SCREEN,"Resize",_screen.BCKIMG,"DesktopResize")
        else
          _screen.backcolor = i_nScreenColor
        endif
        if i_ThemesManager.GetProp(108)=1
          if At("cp_button",Lower(Set("proc")))=0
            if this.pTipo<>"Setup"
              CLEAR WINDOW
            endif
            cProcedure=SET("proc") 
 RELEASE PROCEDURE &cProcedure 
 cProcedure=STRTRAN(cProcedure, FULLPATH(cPathVfcsim)+"\CP_CTRLS.FXP,", FULLPATH(cPathVfcsim)+"\CP_BUTTON.FXP, "+FULLPATH(cPathVfcsim)+"\CP_CTRLS.FXP,",-1,-1,3) 
 Set Proc To &cProcedure
          endif
        else
          if At("cp_button",Lower(Set("proc")))<>0
            if this.pTipo<>"Setup"
              CLEAR WINDOW
            endif
            cProcedure=cPathVfcsim+"\CP_BUTTON.fxp" 
 RELEASE PROCEDURE &cProcedure
          endif
        endif
      endif
      * --- Apertura MyGadget
      * --- Read from MYG_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MYG_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MYG_MAST_idx,2],.t.,this.MYG_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MG_START"+;
          " from "+i_cTable+" MYG_MAST where ";
              +"MGCODUTE = "+cp_ToStrODBC(i_CODUTE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MG_START;
          from (i_cTable) where;
              MGCODUTE = i_CODUTE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MG_START = NVL(cp_ToDate(_read_.MG_START),cp_NullValue(_read_.MG_START))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      Public i_bGadgetStartup 
 i_bGadgetStartup = (this.w_MG_START == "S")
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestioni da aprire all'avvio
    this.w_CODUTE = i_CODUTE * IIF( vartype(i_bMobileMode)<>"U" and i_bMobileMode , -10, 1 )
    * --- Read from CONF_INT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONF_INT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2],.t.,this.CONF_INT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CIOPNGST"+;
        " from "+i_cTable+" CONF_INT where ";
            +"CICODUTE = "+cp_ToStrODBC(this.w_CODUTE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CIOPNGST;
        from (i_cTable) where;
            CICODUTE = this.w_CODUTE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CIOPNGST = NVL(cp_ToDate(_read_.CIOPNGST),cp_NullValue(_read_.CIOPNGST))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(NVL(this.w_CIOPNGST, " "))
      this.w_CODUTE = -1 - IIF( vartype(i_bMobileMode)<>"U" and i_bMobileMode , 1, 0 )
      * --- Read from CONF_INT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONF_INT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2],.t.,this.CONF_INT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CIOPNGST"+;
          " from "+i_cTable+" CONF_INT where ";
              +"CICODUTE = "+cp_ToStrODBC(this.w_CODUTE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CIOPNGST;
          from (i_cTable) where;
              CICODUTE = this.w_CODUTE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CIOPNGST = NVL(cp_ToDate(_read_.CIOPNGST),cp_NullValue(_read_.CIOPNGST))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if g_APPLICATION="ad hoc ENTERPRISE" And Type("g_UFLD")="C" And g_UFLD="S" And i_codute<>0
      * --- Lancia mascherone Flussi e Autorizzazioni
      this.w_GSWD_KWD = GEST_KWD()
      if not empty(this.w_GSWD_KWD)
        this.w_CIOPNGST = alltrim(this.w_CIOPNGST) + iif(empty(this.w_CIOPNGST), "", ";") + alltrim(this.w_GSWD_KWD)
      endif
    endif
    * --- Gestione da aprire all'avvio
    if !EMPTY(NVL(this.w_CIOPNGST, " "))
      this.w_TotGest = 0
      this.w_TotGest = ALINES(L_ArrGestToOpen, this.w_CIOPNGST, 5, ";")
      this.w_GestAtt = 1
      do while this.w_GestAtt<=this.w_TotGest
        L_Macro = STRTRAN(L_ArrGestToOpen(this.w_GestAtt), "#", " with ")
        do mhit in cp_menu with [&L_Macro]
        this.w_GestAtt = this.w_GestAtt + 1
      enddo
      release L_ArrGestToOpen
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura file XML
    this.oXML = CREATEOBJECT("MSXML2.DomDocument")
    this.oXML.ASYNC = .F.
    this.oXML.Load(this.cFileConfig)     
    * --- Verifico che il file sia ben formato
    if Type("this.oXML.documentElement")="O" AND Not IsNull(this.oXML.documentElement)
      this.oRootNode = this.oXML.documentElement
      * --- Prendo solo la lista dei nodi relativi al tag richiesto
      this.oNodeList = this.oRootNode.getElementsByTagName(this.cDefault)
      this.nNumNodes = this.oNodeList.LENGTH
       For nPos = 0 To (this.nNumNodes-1) Step 1
      this.oNode = this.oNodeList.ITEM(nPos)
      this.cParentName = this.oNode.nodeName
      this.bHasChild = this.oNode.hasChildNodes()
      this.nType = this.oNode.nodeType
      if this.nType=1
        if this.bHasChild
          this.oFatherNodeList = this.oNode.childNodes
          this.nFatherLen = this.oFatherNodeList.LENGTH
          FOR this.nFatherPass = 0 TO (this.nFatherLen-1) STEP 1
          this.oFatherNode = this.oFatherNodeList.ITEM(this.nFatherPass)
          this.oChildNodeList = this.oFatherNode.childNodes
          this.nChildLen = this.oChildNodeList.LENGTH
          FOR this.nPass = 0 TO (this.nChildLen-1) STEP 1
          * --- Aggiorno il campo nella tabella
          * --- Accesso al nome del campo da aggiornare (nella propriet� cTagName)
          cTagName = this.oFatherNode.tagName
          this.oChildNode = this.oChildNodeList.ITEM(this.nPass)
          this.nType = this.oChildNode.nodeType
          this.bHasChild = this.oChildNode.hasChildNodes()
          this.cTextData = this.oChildNode.DATA
          this.nTextDataLen = this.oChildNode.LENGTH
          this.cTextData = StrTran(this.cTextData,"&lt;","<")
          if IsDigit(this.cTextData)
            * --- Numero
            InsData = Int(Val(this.cTextData))
          else
            * --- Colori, condizioni, spazi, booleani e espressioni forzate a stringa
            if Upper(Left(this.cTextData,3)) == "RGB" or Upper(Left(this.cTextData,3)) == "IIF" or Upper(Left(this.cTextData,5)) == "SPACE" or Upper(Left(this.cTextData,3)) == ".T." or Upper(Left(this.cTextData,3)) == ".F." or Left(this.cTextData,1) == "'"
              InsData = Evaluate(this.cTextData)
            else
              * --- Carattere
              InsData = this.cTextData
            endif
          endif
          * --- Nel file sono presenti anche variabili della maschera GSUT_ACU, con l'Accept Error evito il sollevarsi di errori di transazione
          * --- Try
          local bErr_05B37CF8
          bErr_05B37CF8=bTrsErr
          this.Try_05B37CF8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_05B37CF8
          * --- End
          Next &&FOR this.nPass = 0 TO (this.nChildLen-1) STEP 1
          Next &&FOR this.nFatherPass = 0 TO (this.nFatherLen-1) STEP 1
        endif
      endif
      Next
    endif
    this.oXML = .Null.
  endproc
  proc Try_05B37CF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_nConn=i_TableProp[this.CONF_INT_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2])
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CONF_INT_idx,i_nConn)
    this.i_Rows = cp_SqlExec( i_nConn,"update "+i_cTable+" set "+cTagname+" ="+cp_NullLink(cp_ToStrODBC(InsData),"CONF_INT",cTagname)+i_ccchkf+" where CICODUTE = "+cp_ToStrODBC(-1) )
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONF_INT'
    this.cWorkTables[2]='MYG_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- gsut_beu
  Func ThemePath(nValue)
  
  		    If IsAhe() Or IsAhr() Or IsAlt()
  		      local i,fname,cPath,cFile, cThemePath
  			    cThemePath = cPathVfcsim+'\Themes\'
  			    ADIR(aDirectories, cThemePath+'*','D')
  			    i=1
  			    Do While m.i<=ALEN(aDirectories,1)
  			    	fname = aDirectories[i,1]
  		     		bApplication = (IsAlt() And Not 'AHR'$Upper(fname) And Not 'AHE'$Upper(fname)) Or (IsAhe() And Not 'AHR'$Upper(fname) And Not 'AETOP'$Upper(fname)) or (IsAhr() And Not 'AHE'$Upper(fname) And Not 'AETOP'$Upper(fname))
  					  If m.bApplication And m.fname<>'.' and m.fname<>'..' and 'D'$aDirectories[m.i,5] and IsDigit(m.fname) And Alltrim(Str(nValue)) == Left(m.fname, At('.', m.fname)-1)
  				        cPath = m.cThemePath+aDirectories[m.i,1]
  				        cFile = Alltrim(Substr(m.fname, At('.', m.fname)+1))
  				        If cp_fileexist(m.cPath+"\"+m.cFile+".xml")
                    Return m.cPath+"\"+m.cFile+".xml"
  				        Endif
  			     	Endif
  				    i=m.i+1
  			    EndDo
  		    Endif
          Return ''
  Endfunc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
