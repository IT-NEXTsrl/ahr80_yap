* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_snm                                                        *
*              Stampa nomenclature                                             *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_7]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2007-07-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_snm",oParentObject))

* --- Class definition
define class tgsar_snm as StdForm
  Top    = 42
  Left   = 35

  * --- Standard Properties
  Width  = 534
  Height = 157
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-11"
  HelpContextID=93751447
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  NOMENCLA_IDX = 0
  cPrg = "gsar_snm"
  cComment = "Stampa nomenclature"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODINI = space(8)
  w_DESINI = space(51)
  w_CODFIN = space(8)
  w_DESFIN = space(51)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_snmPag1","gsar_snm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='NOMENCLA'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsar_snm
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODINI=space(8)
      .w_DESINI=space(51)
      .w_CODFIN=space(8)
      .w_DESFIN=space(51)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODINI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_CODFIN))
          .link_1_3('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
    this.DoRTCalc(4,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
    i_lTable = "NOMENCLA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2], .t., this.NOMENCLA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANM',True,'NOMENCLA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NMCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NMCODICE',trim(this.w_CODINI))
          select NMCODICE,NMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.NMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('NOMENCLA','*','NMCODICE',cp_AbsName(oSource.parent,'oCODINI_1_1'),i_cWhere,'GSAR_ANM',"Nomenclature",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',oSource.xKey(1))
            select NMCODICE,NMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',this.w_CODINI)
            select NMCODICE,NMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.NMCODICE,space(8))
      this.w_DESINI = NVL(_Link_.NMDESCRI,space(51))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(8)
      endif
      this.w_DESINI = space(51)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_CODFIN)) OR  (.w_CODINI<=.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_CODINI = space(8)
        this.w_DESINI = space(51)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])+'\'+cp_ToStr(_Link_.NMCODICE,1)
      cp_ShowWarn(i_cKey,this.NOMENCLA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
    i_lTable = "NOMENCLA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2], .t., this.NOMENCLA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANM',True,'NOMENCLA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NMCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NMCODICE',trim(this.w_CODFIN))
          select NMCODICE,NMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.NMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('NOMENCLA','*','NMCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_3'),i_cWhere,'GSAR_ANM',"Nomenclature",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',oSource.xKey(1))
            select NMCODICE,NMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',this.w_CODFIN)
            select NMCODICE,NMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.NMCODICE,space(8))
      this.w_DESFIN = NVL(_Link_.NMDESCRI,space(51))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(8)
      endif
      this.w_DESFIN = space(51)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODFIN>=.w_CODINI) or (empty(.w_CODINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_CODFIN = space(8)
        this.w_DESFIN = space(51)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])+'\'+cp_ToStr(_Link_.NMCODICE,1)
      cp_ShowWarn(i_cKey,this.NOMENCLA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_1.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_1.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_2.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_2.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_3.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_3.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_4.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_4.value=this.w_DESFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_CODFIN)) OR  (.w_CODINI<=.w_CODFIN))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not((.w_CODFIN>=.w_CODINI) or (empty(.w_CODINI)))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_snmPag1 as StdContainer
  Width  = 530
  height = 157
  stdWidth  = 530
  stdheight = 157
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_1 as StdField with uid="GXGWTBVBHA",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Nomenclatura di inizio selezione",;
    HelpContextID = 205251546,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=122, Top=17, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="NOMENCLA", cZoomOnZoom="GSAR_ANM", oKey_1_1="NMCODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NOMENCLA','*','NMCODICE',cp_AbsName(this.parent,'oCODINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANM',"Nomenclature",'',this.parent.oContained
  endproc
  proc oCODINI_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NMCODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_2 as StdField with uid="JEEBKWGYOX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(51), bMultilanguage =  .f.,;
    HelpContextID = 205192650,;
   bGlobalFont=.t.,;
    Height=21, Width=310, Left=211, Top=17, InputMask=replicate('X',51)

  add object oCODFIN_1_3 as StdField with uid="ABAGUKQDCQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Nomenclatura di fine selezione",;
    HelpContextID = 126804954,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=122, Top=41, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="NOMENCLA", cZoomOnZoom="GSAR_ANM", oKey_1_1="NMCODICE", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NOMENCLA','*','NMCODICE',cp_AbsName(this.parent,'oCODFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANM',"Nomenclature",'',this.parent.oContained
  endproc
  proc oCODFIN_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NMCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_4 as StdField with uid="ZDAKRECMBP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(51), bMultilanguage =  .f.,;
    HelpContextID = 126746058,;
   bGlobalFont=.t.,;
    Height=21, Width=310, Left=211, Top=41, InputMask=replicate('X',51)


  add object oObj_1_5 as cp_outputCombo with uid="ZLERXIVPWT",left=122, top=74, width=399,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 265121818


  add object oBtn_1_6 as StdButton with uid="SLZWOHNFQA",left=419, top=105, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 32894426;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="CUUVHKOEDW",left=474, top=105, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 101068870;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_8 as StdString with uid="OJDGGGWWHQ",Visible=.t., Left=15, Top=74,;
    Alignment=1, Width=105, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="BIYMGKQFNA",Visible=.t., Left=7, Top=17,;
    Alignment=1, Width=113, Height=15,;
    Caption="Da nomenclatura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="VVMHQZNBMQ",Visible=.t., Left=15, Top=41,;
    Alignment=1, Width=105, Height=15,;
    Caption="A nomenclatura:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_snm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
