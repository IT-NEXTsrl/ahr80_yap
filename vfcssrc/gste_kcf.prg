* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kcf                                                        *
*              Cash flow                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_182]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-29                                                      *
* Last revis.: 2014-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kcf",oParentObject))

* --- Class definition
define class tgste_kcf as StdForm
  Top    = 17
  Left   = 22

  * --- Standard Properties
  Width  = 655
  Height = 413+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-30"
  HelpContextID=99234921
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=39

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  PNT_MAST_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gste_kcf"
  cComment = "Cash flow"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPCER = space(1)
  w_TIPEFF = space(1)
  w_TIPATT = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_TIPPER = space(1)
  w_SELEZI = space(1)
  w_PAGRD = space(2)
  w_PAGBO = space(2)
  w_PAGRB = space(2)
  w_PAGRI = space(2)
  w_PAGCA = space(2)
  w_PAGMA = space(2)
  w_TIPCLI = space(1)
  o_TIPCLI = space(1)
  w_TIPFOR = space(1)
  o_TIPFOR = space(1)
  w_TIPGEN = space(1)
  w_CODCLI = space(15)
  w_CODFOR = space(15)
  w_CDESCRI = space(40)
  w_FDESCRI = space(40)
  w_NDOCIN = 0
  w_ADOCIN = space(10)
  w_DDOCIN = ctod('  /  /  ')
  w_NDOCFI = 0
  w_ADOCFI = space(10)
  w_DDOCFI = ctod('  /  /  ')
  w_CODVAL = space(3)
  w_VADESCRI = space(35)
  w_TIPVAL = space(1)
  o_TIPVAL = space(1)
  w_VALRAP = space(3)
  o_VALRAP = space(3)
  w_CAMBIO = 0
  w_OBTEST = ctod('  /  /  ')
  w_CAOVAL = 0
  w_DECTOT = 0
  w_DATCAM = ctod('  /  /  ')
  w_CALCPIC = 0
  w_TIPCONC = space(1)
  w_TIPCONF = space(1)
  w_SIMVAL = space(5)
  w_Zoomcf = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kcfPag1","gste_kcf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgste_kcfPag2","gste_kcf",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri aggiuntivi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPCER_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoomcf = this.oPgFrm.Pages(1).oPag.Zoomcf
    DoDefault()
    proc Destroy()
      this.w_Zoomcf = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='VALUTE'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCER=space(1)
      .w_TIPEFF=space(1)
      .w_TIPATT=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_TIPPER=space(1)
      .w_SELEZI=space(1)
      .w_PAGRD=space(2)
      .w_PAGBO=space(2)
      .w_PAGRB=space(2)
      .w_PAGRI=space(2)
      .w_PAGCA=space(2)
      .w_PAGMA=space(2)
      .w_TIPCLI=space(1)
      .w_TIPFOR=space(1)
      .w_TIPGEN=space(1)
      .w_CODCLI=space(15)
      .w_CODFOR=space(15)
      .w_CDESCRI=space(40)
      .w_FDESCRI=space(40)
      .w_NDOCIN=0
      .w_ADOCIN=space(10)
      .w_DDOCIN=ctod("  /  /  ")
      .w_NDOCFI=0
      .w_ADOCFI=space(10)
      .w_DDOCFI=ctod("  /  /  ")
      .w_CODVAL=space(3)
      .w_VADESCRI=space(35)
      .w_TIPVAL=space(1)
      .w_VALRAP=space(3)
      .w_CAMBIO=0
      .w_OBTEST=ctod("  /  /  ")
      .w_CAOVAL=0
      .w_DECTOT=0
      .w_DATCAM=ctod("  /  /  ")
      .w_CALCPIC=0
      .w_TIPCONC=space(1)
      .w_TIPCONF=space(1)
      .w_SIMVAL=space(5)
        .w_TIPCER = 'S'
          .DoRTCalc(2,3,.f.)
        .w_DATINI = i_datsys
        .w_DATFIN = i_datsys+30
        .w_TIPPER = 'S'
        .w_SELEZI = 'S'
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .w_PAGRD = 'RD'
        .w_PAGBO = 'BO'
        .w_PAGRB = 'RB'
        .w_PAGRI = 'RI'
        .w_PAGCA = 'CA'
        .w_PAGMA = 'MA'
        .w_TIPCLI = 'C'
        .w_TIPFOR = 'F'
        .w_TIPGEN = 'G'
        .w_CODCLI = ' '
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODCLI))
          .link_2_14('Full')
        endif
        .w_CODFOR = ' '
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CODFOR))
          .link_2_15('Full')
        endif
        .DoRTCalc(19,27,.f.)
        if not(empty(.w_CODVAL))
          .link_2_31('Full')
        endif
          .DoRTCalc(28,28,.f.)
        .w_TIPVAL = 'C'
        .w_VALRAP = ALLTR(g_PERVAL)
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_VALRAP))
          .link_2_38('Full')
        endif
        .w_CAMBIO = GETCAM(.w_VALRAP,.w_DATCAM,7)
      .oPgFrm.Page1.oPag.Zoomcf.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .w_OBTEST = i_DATSYS
          .DoRTCalc(33,34,.f.)
        .w_DATCAM = i_datsys
        .w_CALCPIC = DEFPIC(.w_DECTOT)
    endwith
    this.DoRTCalc(37,39,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .DoRTCalc(1,16,.t.)
        if .o_TIPCLI<>.w_TIPCLI
            .w_CODCLI = ' '
          .link_2_14('Full')
        endif
        if .o_TIPFOR<>.w_TIPFOR
            .w_CODFOR = ' '
          .link_2_15('Full')
        endif
        .DoRTCalc(19,29,.t.)
        if .o_TIPVAL<>.w_TIPVAL
            .w_VALRAP = ALLTR(g_PERVAL)
          .link_2_38('Full')
        endif
        if .o_VALRAP<>.w_VALRAP
            .w_CAMBIO = GETCAM(.w_VALRAP,.w_DATCAM,7)
        endif
        .oPgFrm.Page1.oPag.Zoomcf.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .DoRTCalc(32,35,.t.)
            .w_CALCPIC = DEFPIC(.w_DECTOT)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(37,39,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.Zoomcf.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oCODCLI_2_14.enabled = this.oPgFrm.Page2.oPag.oCODCLI_2_14.mCond()
    this.oPgFrm.Page2.oPag.oCODFOR_2_15.enabled = this.oPgFrm.Page2.oPag.oCODFOR_2_15.mCond()
    this.oPgFrm.Page2.oPag.oVALRAP_2_38.enabled = this.oPgFrm.Page2.oPag.oVALRAP_2_38.mCond()
    this.oPgFrm.Page2.oPag.oCAMBIO_2_40.enabled = this.oPgFrm.Page2.oPag.oCAMBIO_2_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oStr_2_39.visible=!this.oPgFrm.Page2.oPag.oStr_2_39.mHide()
    this.oPgFrm.Page2.oPag.oCAMBIO_2_40.visible=!this.oPgFrm.Page2.oPag.oCAMBIO_2_40.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.Zoomcf.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCLI
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLI;
                     ,'ANCODICE',trim(this.w_CODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCLI);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLI_2_14'),i_cWhere,'GSAR_BZC',"Elenco clienti",'gsar_scl.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLI;
                       ,'ANCODICE',this.w_CODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_CDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPCONC = NVL(_Link_.ANTIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_CDESCRI = space(40)
      this.w_TIPCONC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFOR
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPFOR;
                     ,'ANCODICE',trim(this.w_CODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPFOR);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFOR_2_15'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'gsar_sfr.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPFOR;
                       ,'ANCODICE',this.w_CODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_FDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPCONF = NVL(_Link_.ANTIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODFOR = space(15)
      endif
      this.w_FDESCRI = space(40)
      this.w_TIPCONF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPCONF='F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso")
        endif
        this.w_CODFOR = space(15)
        this.w_FDESCRI = space(40)
        this.w_TIPCONF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL_2_31'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_VADESCRI = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_VADESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALRAP
  func Link_2_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALRAP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VALRAP)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VALRAP))
          select VACODVAL,VACAOVAL,VADECTOT,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VALRAP)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VALRAP) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVALRAP_2_38'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VACAOVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALRAP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALRAP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALRAP)
            select VACODVAL,VACAOVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALRAP = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALRAP = space(3)
      endif
      this.w_CAOVAL = 0
      this.w_DECTOT = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALRAP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPCER_1_1.RadioValue()==this.w_TIPCER)
      this.oPgFrm.Page1.oPag.oTIPCER_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPEFF_1_2.RadioValue()==this.w_TIPEFF)
      this.oPgFrm.Page1.oPag.oTIPEFF_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPATT_1_3.RadioValue()==this.w_TIPATT)
      this.oPgFrm.Page1.oPag.oTIPATT_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_5.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_5.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_6.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_6.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPPER_1_7.RadioValue()==this.w_TIPPER)
      this.oPgFrm.Page1.oPag.oTIPPER_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_11.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGRD_2_1.RadioValue()==this.w_PAGRD)
      this.oPgFrm.Page2.oPag.oPAGRD_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGBO_2_2.RadioValue()==this.w_PAGBO)
      this.oPgFrm.Page2.oPag.oPAGBO_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGRB_2_3.RadioValue()==this.w_PAGRB)
      this.oPgFrm.Page2.oPag.oPAGRB_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGRI_2_5.RadioValue()==this.w_PAGRI)
      this.oPgFrm.Page2.oPag.oPAGRI_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGCA_2_6.RadioValue()==this.w_PAGCA)
      this.oPgFrm.Page2.oPag.oPAGCA_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGMA_2_7.RadioValue()==this.w_PAGMA)
      this.oPgFrm.Page2.oPag.oPAGMA_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPCLI_2_8.RadioValue()==this.w_TIPCLI)
      this.oPgFrm.Page2.oPag.oTIPCLI_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPFOR_2_9.RadioValue()==this.w_TIPFOR)
      this.oPgFrm.Page2.oPag.oTIPFOR_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPGEN_2_10.RadioValue()==this.w_TIPGEN)
      this.oPgFrm.Page2.oPag.oTIPGEN_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCLI_2_14.value==this.w_CODCLI)
      this.oPgFrm.Page2.oPag.oCODCLI_2_14.value=this.w_CODCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFOR_2_15.value==this.w_CODFOR)
      this.oPgFrm.Page2.oPag.oCODFOR_2_15.value=this.w_CODFOR
    endif
    if not(this.oPgFrm.Page2.oPag.oCDESCRI_2_16.value==this.w_CDESCRI)
      this.oPgFrm.Page2.oPag.oCDESCRI_2_16.value=this.w_CDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oFDESCRI_2_17.value==this.w_FDESCRI)
      this.oPgFrm.Page2.oPag.oFDESCRI_2_17.value=this.w_FDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oNDOCIN_2_18.value==this.w_NDOCIN)
      this.oPgFrm.Page2.oPag.oNDOCIN_2_18.value=this.w_NDOCIN
    endif
    if not(this.oPgFrm.Page2.oPag.oADOCIN_2_19.value==this.w_ADOCIN)
      this.oPgFrm.Page2.oPag.oADOCIN_2_19.value=this.w_ADOCIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDDOCIN_2_20.value==this.w_DDOCIN)
      this.oPgFrm.Page2.oPag.oDDOCIN_2_20.value=this.w_DDOCIN
    endif
    if not(this.oPgFrm.Page2.oPag.oNDOCFI_2_21.value==this.w_NDOCFI)
      this.oPgFrm.Page2.oPag.oNDOCFI_2_21.value=this.w_NDOCFI
    endif
    if not(this.oPgFrm.Page2.oPag.oADOCFI_2_22.value==this.w_ADOCFI)
      this.oPgFrm.Page2.oPag.oADOCFI_2_22.value=this.w_ADOCFI
    endif
    if not(this.oPgFrm.Page2.oPag.oDDOCFI_2_23.value==this.w_DDOCFI)
      this.oPgFrm.Page2.oPag.oDDOCFI_2_23.value=this.w_DDOCFI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODVAL_2_31.value==this.w_CODVAL)
      this.oPgFrm.Page2.oPag.oCODVAL_2_31.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oVADESCRI_2_32.value==this.w_VADESCRI)
      this.oPgFrm.Page2.oPag.oVADESCRI_2_32.value=this.w_VADESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPVAL_2_36.RadioValue()==this.w_TIPVAL)
      this.oPgFrm.Page2.oPag.oTIPVAL_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVALRAP_2_38.value==this.w_VALRAP)
      this.oPgFrm.Page2.oPag.oVALRAP_2_38.value=this.w_VALRAP
    endif
    if not(this.oPgFrm.Page2.oPag.oCAMBIO_2_40.value==this.w_CAMBIO)
      this.oPgFrm.Page2.oPag.oCAMBIO_2_40.value=this.w_CAMBIO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DATINI<=.w_DATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore di data finale")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore di data finale")
          case   not(.w_TIPCONF='F')  and (.w_TIPFOR='F')  and not(empty(.w_CODFOR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODFOR_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso")
          case   (empty(.w_VALRAP))  and (.w_TIPVAL='A')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVALRAP_2_38.SetFocus()
            i_bnoObbl = !empty(.w_VALRAP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMBIO))  and not(.w_TIPVAL<>'A' or .w_CAOVAL<>0 or Empty(nvl(.w_VALRAP,' ')))  and (.w_CAOVAL=0 AND .w_TIPVAL='A')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCAMBIO_2_40.SetFocus()
            i_bnoObbl = !empty(.w_CAMBIO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cambio di riferimento")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCLI = this.w_TIPCLI
    this.o_TIPFOR = this.w_TIPFOR
    this.o_TIPVAL = this.w_TIPVAL
    this.o_VALRAP = this.w_VALRAP
    return

enddefine

* --- Define pages as container
define class tgste_kcfPag1 as StdContainer
  Width  = 651
  height = 413
  stdWidth  = 651
  stdheight = 413
  resizeXpos=470
  resizeYpos=255
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPCER_1_1 as StdCheck with uid="WAVDVSMHDV",rtseq=1,rtrep=.f.,left=62, top=20, caption="Certo",;
    ToolTipText = "Dati derivanti da distinte effetti contabilizzabili non ancora contabilizzate",;
    HelpContextID = 11409974,;
    cFormVar="w_TIPCER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPCER_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPCER_1_1.GetRadio()
    this.Parent.oContained.w_TIPCER = this.RadioValue()
    return .t.
  endfunc

  func oTIPCER_1_1.SetRadio()
    this.Parent.oContained.w_TIPCER=trim(this.Parent.oContained.w_TIPCER)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCER=='S',1,;
      0)
  endfunc

  add object oTIPEFF_1_2 as StdCheck with uid="BVXSSKGXKD",rtseq=2,rtrep=.f.,left=62, top=42, caption="Effettivo",;
    ToolTipText = "Dati derivanti da partite associate a primanota e a scadenze diverse",;
    HelpContextID = 79698486,;
    cFormVar="w_TIPEFF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPEFF_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPEFF_1_2.GetRadio()
    this.Parent.oContained.w_TIPEFF = this.RadioValue()
    return .t.
  endfunc

  func oTIPEFF_1_2.SetRadio()
    this.Parent.oContained.w_TIPEFF=trim(this.Parent.oContained.w_TIPEFF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPEFF=='S',1,;
      0)
  endfunc

  add object oTIPATT_1_3 as StdCheck with uid="ZNARTWEVGE",rtseq=3,rtrep=.f.,left=62, top=64, caption="Atteso",;
    ToolTipText = "Dati derivanti da rate scadenze associate a documenti",;
    HelpContextID = 60561974,;
    cFormVar="w_TIPATT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPATT_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPATT_1_3.GetRadio()
    this.Parent.oContained.w_TIPATT = this.RadioValue()
    return .t.
  endfunc

  func oTIPATT_1_3.SetRadio()
    this.Parent.oContained.w_TIPATT=trim(this.Parent.oContained.w_TIPATT)
    this.value = ;
      iif(this.Parent.oContained.w_TIPATT=='S',1,;
      0)
  endfunc

  add object oDATINI_1_5 as StdField with uid="PMTTMHPITH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore di data finale",;
    ToolTipText = "Data inizio selezione",;
    HelpContextID = 138694966,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=225, Top=20

  func oDATINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_6 as StdField with uid="OTCGWMGFYS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore di data finale",;
    ToolTipText = "Data fine selezione",;
    HelpContextID = 217141558,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=341, Top=20

  func oDATFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc


  add object oTIPPER_1_7 as StdCombo with uid="LXNCDCDAHL",rtseq=6,rtrep=.f.,left=225,top=55,width=147,height=21;
    , ToolTipText = "Periodo di raggruppamento selezionato";
    , HelpContextID = 12261942;
    , cFormVar="w_TIPPER",RowSource=""+"Giornaliero,"+"Settimanale,"+"Quindicinale,"+"Mensile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPPER_1_7.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'S',;
    iif(this.value =3,'Q',;
    iif(this.value =4,'M',;
    space(1))))))
  endfunc
  func oTIPPER_1_7.GetRadio()
    this.Parent.oContained.w_TIPPER = this.RadioValue()
    return .t.
  endfunc

  func oTIPPER_1_7.SetRadio()
    this.Parent.oContained.w_TIPPER=trim(this.Parent.oContained.w_TIPPER)
    this.value = ;
      iif(this.Parent.oContained.w_TIPPER=='G',1,;
      iif(this.Parent.oContained.w_TIPPER=='S',2,;
      iif(this.Parent.oContained.w_TIPPER=='Q',3,;
      iif(this.Parent.oContained.w_TIPPER=='M',4,;
      0))))
  endfunc

  add object oSELEZI_1_11 as StdRadio with uid="RDFUYVXOXW",rtseq=7,rtrep=.f.,left=11, top=346, width=140,height=35;
    , ToolTipText = "Seleziona/deseleziona tutti i conti banca";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_11.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 150984230
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 150984230
      this.Buttons(2).Top=16
      this.SetAll("Width",138)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutti i conti banca")
      StdRadio::init()
    endproc

  func oSELEZI_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_11.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_11.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_13 as StdButton with uid="FMNRJPCTLS",left=548, top=368, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare il cash flow";
    , HelpContextID = 42554662;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSTE_BFL(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_OREP))
      endwith
    endif
  endfunc


  add object oObj_1_14 as cp_outputCombo with uid="ZLERXIVPWT",left=267, top=343, width=371,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 78762726


  add object oBtn_1_15 as StdButton with uid="LZNQFGWXIN",left=598, top=368, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91917498;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object Zoomcf as cp_szoombox with uid="IWTIQPTJGQ",left=2, top=87, width=646,height=249,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSTE_KCF",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cTable="COC_MAST",cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 78762726


  add object oObj_1_17 as cp_runprogram with uid="HWXSNDJZQK",left=-4, top=434, width=237,height=18,;
    caption='GSTE_BSB',;
   bGlobalFont=.t.,;
    prg="GSTE_BSB",;
    cEvent = "w_SELEZI Changed,Blank",;
    nPag=1;
    , HelpContextID = 38822824

  add object oStr_1_4 as StdString with uid="SCBVFRZZEG",Visible=.t., Left=25, Top=20,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="SMODERLOEY",Visible=.t., Left=184, Top=20,;
    Alignment=1, Width=34, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="WSESJLGWYK",Visible=.t., Left=313, Top=20,;
    Alignment=1, Width=23, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="QPXBZBNSLQ",Visible=.t., Left=162, Top=55,;
    Alignment=1, Width=56, Height=18,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="EUIDGZENRM",Visible=.t., Left=166, Top=346,;
    Alignment=1, Width=97, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine
define class tgste_kcfPag2 as StdContainer
  Width  = 651
  height = 413
  stdWidth  = 651
  stdheight = 413
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPAGRD_2_1 as StdCheck with uid="YXPZLKEHVK",rtseq=8,rtrep=.f.,left=129, top=15, caption="Rimessa diretta",;
    ToolTipText = "Se attivo filtra pagamenti rimessa diretta",;
    HelpContextID = 22248970,;
    cFormVar="w_PAGRD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGRD_2_1.RadioValue()
    return(iif(this.value =1,'RD',;
    ' '))
  endfunc
  func oPAGRD_2_1.GetRadio()
    this.Parent.oContained.w_PAGRD = this.RadioValue()
    return .t.
  endfunc

  func oPAGRD_2_1.SetRadio()
    this.Parent.oContained.w_PAGRD=trim(this.Parent.oContained.w_PAGRD)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRD=='RD',1,;
      0)
  endfunc

  add object oPAGBO_2_2 as StdCheck with uid="LNFVRFNFDE",rtseq=9,rtrep=.f.,left=129, top=33, caption="Bonifico",;
    ToolTipText = "Se attivo filtra pagamenti bonifico",;
    HelpContextID = 11763210,;
    cFormVar="w_PAGBO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGBO_2_2.RadioValue()
    return(iif(this.value =1,'BO',;
    ' '))
  endfunc
  func oPAGBO_2_2.GetRadio()
    this.Parent.oContained.w_PAGBO = this.RadioValue()
    return .t.
  endfunc

  func oPAGBO_2_2.SetRadio()
    this.Parent.oContained.w_PAGBO=trim(this.Parent.oContained.w_PAGBO)
    this.value = ;
      iif(this.Parent.oContained.w_PAGBO=='BO',1,;
      0)
  endfunc

  add object oPAGRB_2_3 as StdCheck with uid="KQBHYVCHXH",rtseq=10,rtrep=.f.,left=129, top=51, caption="Ric.bancaria/Ri.Ba.",;
    ToolTipText = "Se attivo filtra pagamenti ric.bancaria",;
    HelpContextID = 24346122,;
    cFormVar="w_PAGRB", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGRB_2_3.RadioValue()
    return(iif(this.value =1,'RB',;
    ' '))
  endfunc
  func oPAGRB_2_3.GetRadio()
    this.Parent.oContained.w_PAGRB = this.RadioValue()
    return .t.
  endfunc

  func oPAGRB_2_3.SetRadio()
    this.Parent.oContained.w_PAGRB=trim(this.Parent.oContained.w_PAGRB)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRB=='RB',1,;
      0)
  endfunc

  add object oPAGRI_2_5 as StdCheck with uid="QOFHPAWNSJ",rtseq=11,rtrep=.f.,left=316, top=15, caption="R.I.D.",;
    ToolTipText = "Se attivo filtra pagamenti R.I.D.",;
    HelpContextID = 17006090,;
    cFormVar="w_PAGRI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGRI_2_5.RadioValue()
    return(iif(this.value =1,'RI',;
    ' '))
  endfunc
  func oPAGRI_2_5.GetRadio()
    this.Parent.oContained.w_PAGRI = this.RadioValue()
    return .t.
  endfunc

  func oPAGRI_2_5.SetRadio()
    this.Parent.oContained.w_PAGRI=trim(this.Parent.oContained.w_PAGRI)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRI=='RI',1,;
      0)
  endfunc

  add object oPAGCA_2_6 as StdCheck with uid="FXFINXWFBL",rtseq=12,rtrep=.f.,left=316, top=33, caption="Cambiale",;
    ToolTipText = "Se attivo filtra pagamenti cambiale",;
    HelpContextID = 26377738,;
    cFormVar="w_PAGCA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGCA_2_6.RadioValue()
    return(iif(this.value =1,'CA',;
    ' '))
  endfunc
  func oPAGCA_2_6.GetRadio()
    this.Parent.oContained.w_PAGCA = this.RadioValue()
    return .t.
  endfunc

  func oPAGCA_2_6.SetRadio()
    this.Parent.oContained.w_PAGCA=trim(this.Parent.oContained.w_PAGCA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGCA=='CA',1,;
      0)
  endfunc

  add object oPAGMA_2_7 as StdCheck with uid="JERCZZLYHZ",rtseq=13,rtrep=.f.,left=316, top=51, caption="M.AV.",;
    ToolTipText = "Se attivo filtra pagamenti M.AV.",;
    HelpContextID = 25722378,;
    cFormVar="w_PAGMA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGMA_2_7.RadioValue()
    return(iif(this.value =1,'MA',;
    ' '))
  endfunc
  func oPAGMA_2_7.GetRadio()
    this.Parent.oContained.w_PAGMA = this.RadioValue()
    return .t.
  endfunc

  func oPAGMA_2_7.SetRadio()
    this.Parent.oContained.w_PAGMA=trim(this.Parent.oContained.w_PAGMA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGMA=='MA',1,;
      0)
  endfunc

  add object oTIPCLI_2_8 as StdCheck with uid="IRJWPVXOBO",rtseq=14,rtrep=.f.,left=129, top=88, caption="Clienti",;
    ToolTipText = "Se attivo filtra gli intestatari clienti",;
    HelpContextID = 136190518,;
    cFormVar="w_TIPCLI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTIPCLI_2_8.RadioValue()
    return(iif(this.value =1,'C',;
    ' '))
  endfunc
  func oTIPCLI_2_8.GetRadio()
    this.Parent.oContained.w_TIPCLI = this.RadioValue()
    return .t.
  endfunc

  func oTIPCLI_2_8.SetRadio()
    this.Parent.oContained.w_TIPCLI=trim(this.Parent.oContained.w_TIPCLI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCLI=='C',1,;
      0)
  endfunc

  func oTIPCLI_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCLI)
        bRes2=.link_2_14('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oTIPFOR_2_9 as StdCheck with uid="ETIYEZTRXP",rtseq=15,rtrep=.f.,left=129, top=107, caption="Fornitori",;
    ToolTipText = "Se attivo filtra gli intestatari fornitori",;
    HelpContextID = 22092342,;
    cFormVar="w_TIPFOR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTIPFOR_2_9.RadioValue()
    return(iif(this.value =1,'F',;
    ' '))
  endfunc
  func oTIPFOR_2_9.GetRadio()
    this.Parent.oContained.w_TIPFOR = this.RadioValue()
    return .t.
  endfunc

  func oTIPFOR_2_9.SetRadio()
    this.Parent.oContained.w_TIPFOR=trim(this.Parent.oContained.w_TIPFOR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPFOR=='F',1,;
      0)
  endfunc

  func oTIPFOR_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODFOR)
        bRes2=.link_2_15('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oTIPGEN_2_10 as StdCheck with uid="KFTABRHETU",rtseq=16,rtrep=.f.,left=129, top=126, caption="Altro",;
    ToolTipText = "Se attivo filtra i conti generici",;
    HelpContextID = 212998710,;
    cFormVar="w_TIPGEN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTIPGEN_2_10.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oTIPGEN_2_10.GetRadio()
    this.Parent.oContained.w_TIPGEN = this.RadioValue()
    return .t.
  endfunc

  func oTIPGEN_2_10.SetRadio()
    this.Parent.oContained.w_TIPGEN=trim(this.Parent.oContained.w_TIPGEN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPGEN=='G',1,;
      0)
  endfunc

  add object oCODCLI_2_14 as StdField with uid="DRRQATAJPY",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODCLI", cQueryName = "CODCLI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Codice cliente",;
    HelpContextID = 136142630,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=129, Top=163, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLI", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLI"

  func oCODCLI_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLI='C')
    endwith
   endif
  endfunc

  func oCODCLI_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLI_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLI_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLI)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLI_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti",'gsar_scl.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCLI_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCLI
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCLI
     i_obj.ecpSave()
  endproc

  add object oCODFOR_2_15 as StdField with uid="JFSPSQZVIK",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODFOR", cQueryName = "CODFOR",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Codice fornitore",;
    HelpContextID = 22044454,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=129, Top=196, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFOR"

  func oCODFOR_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPFOR='F')
    endwith
   endif
  endfunc

  func oCODFOR_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFOR_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFOR_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFOR_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'gsar_sfr.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODFOR_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPFOR
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFOR
     i_obj.ecpSave()
  endproc

  add object oCDESCRI_2_16 as StdField with uid="PNEILFOEAK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CDESCRI", cQueryName = "CDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cliente",;
    HelpContextID = 258120666,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=273, Top=163, InputMask=replicate('X',40)

  add object oFDESCRI_2_17 as StdField with uid="PRILNFCFVV",rtseq=20,rtrep=.f.,;
    cFormVar = "w_FDESCRI", cQueryName = "FDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione fornitore",;
    HelpContextID = 258120618,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=273, Top=196, InputMask=replicate('X',40)

  add object oNDOCIN_2_18 as StdField with uid="ZBNZTFUKTZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_NDOCIN", cQueryName = "",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Num. documento iniziale",;
    HelpContextID = 216925398,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=129, Top=229, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOCIN_2_19 as StdField with uid="IQDHLHPBVF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ADOCIN", cQueryName = "ADOCIN",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento iniziale",;
    HelpContextID = 216925190,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=275, Top=229, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDDOCIN_2_20 as StdField with uid="KTUFDNNKXZ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DDOCIN", cQueryName = "",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data emissione del documento iniziale",;
    HelpContextID = 216925238,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=392, Top=229

  add object oNDOCFI_2_21 as StdField with uid="GDBIXIPPHU",rtseq=24,rtrep=.f.,;
    cFormVar = "w_NDOCFI", cQueryName = "",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Num.documento finale",;
    HelpContextID = 129893590,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=129, Top=262, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOCFI_2_22 as StdField with uid="HFHATSWFWM",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ADOCFI", cQueryName = "ADOCFI",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento finale",;
    HelpContextID = 129893382,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=275, Top=262, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDDOCFI_2_23 as StdField with uid="VNSTMBLDUE",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DDOCFI", cQueryName = "",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data emissione del documento finale",;
    HelpContextID = 129893430,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=392, Top=262

  add object oCODVAL_2_31 as StdField with uid="TMMMDEZAEX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta, se vuota tutte",;
    HelpContextID = 176185126,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=129, Top=292, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL_2_31.ecpDrop(oSource)
    this.Parent.oContained.link_2_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAL_2_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCODVAL_2_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oCODVAL_2_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CODVAL
     i_obj.ecpSave()
  endproc

  add object oVADESCRI_2_32 as StdField with uid="GFEMTIBHLK",rtseq=28,rtrep=.f.,;
    cFormVar = "w_VADESCRI", cQueryName = "VADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione valuta",;
    HelpContextID = 42947231,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=186, Top=292, InputMask=replicate('X',35)


  add object oTIPVAL_2_36 as StdCombo with uid="BHKRADOIRX",rtseq=29,rtrep=.f.,left=119,top=364,width=118,height=21;
    , ToolTipText = "Valuta di valorizzazione dei dati sulla stampa";
    , HelpContextID = 176233014;
    , cFormVar="w_TIPVAL",RowSource=""+"Altra valuta,"+"Valuta di conto", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPVAL_2_36.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oTIPVAL_2_36.GetRadio()
    this.Parent.oContained.w_TIPVAL = this.RadioValue()
    return .t.
  endfunc

  func oTIPVAL_2_36.SetRadio()
    this.Parent.oContained.w_TIPVAL=trim(this.Parent.oContained.w_TIPVAL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPVAL=='A',1,;
      iif(this.Parent.oContained.w_TIPVAL=='C',2,;
      0))
  endfunc

  add object oVALRAP_2_38 as StdField with uid="PUWQWLEJNX",rtseq=30,rtrep=.f.,;
    cFormVar = "w_VALRAP", cQueryName = "VALRAP",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 243061334,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=338, Top=364, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALRAP"

  func oVALRAP_2_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPVAL='A')
    endwith
   endif
  endfunc

  func oVALRAP_2_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oVALRAP_2_38.ecpDrop(oSource)
    this.Parent.oContained.link_2_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVALRAP_2_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oVALRAP_2_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oVALRAP_2_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_VALRAP
     i_obj.ecpSave()
  endproc

  add object oCAMBIO_2_40 as StdField with uid="VKYBHEINIG",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CAMBIO", cQueryName = "CAMBIO",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cambio di riferimento",;
    ToolTipText = "Cambio della valuta selezionata",;
    HelpContextID = 233627942,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=497, Top=364

  func oCAMBIO_2_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL=0 AND .w_TIPVAL='A')
    endwith
   endif
  endfunc

  func oCAMBIO_2_40.mHide()
    with this.Parent.oContained
      return (.w_TIPVAL<>'A' or .w_CAOVAL<>0 or Empty(nvl(.w_VALRAP,' ')))
    endwith
  endfunc

  add object oStr_2_4 as StdString with uid="STVTDNLYIV",Visible=.t., Left=55, Top=15,;
    Alignment=1, Width=67, Height=18,;
    Caption="Pagamenti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="QTLQPGDOXA",Visible=.t., Left=43, Top=88,;
    Alignment=1, Width=79, Height=18,;
    Caption="Intestatari:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="POLHOZCPTF",Visible=.t., Left=64, Top=165,;
    Alignment=1, Width=60, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="OSATJKHCVR",Visible=.t., Left=63, Top=197,;
    Alignment=1, Width=61, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="DKPVJOYCGC",Visible=.t., Left=12, Top=229,;
    Alignment=1, Width=112, Height=18,;
    Caption="Da n.documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="XCVVRPDOFL",Visible=.t., Left=27, Top=262,;
    Alignment=1, Width=97, Height=18,;
    Caption="A n. documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="DLGWUZZOTK",Visible=.t., Left=365, Top=229,;
    Alignment=1, Width=26, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="JGWMBFEVVL",Visible=.t., Left=365, Top=262,;
    Alignment=1, Width=26, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="YAAATJHSEK",Visible=.t., Left=262, Top=229,;
    Alignment=2, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="BJWEUSWIMR",Visible=.t., Left=262, Top=262,;
    Alignment=2, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="HCEVWHRVQD",Visible=.t., Left=28, Top=294,;
    Alignment=1, Width=96, Height=18,;
    Caption="Codice valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="QBPFDNGPPQ",Visible=.t., Left=16, Top=333,;
    Alignment=0, Width=54, Height=18,;
    Caption="Valuta"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_35 as StdString with uid="HINAHNVGPK",Visible=.t., Left=50, Top=364,;
    Alignment=1, Width=64, Height=18,;
    Caption="Stampa in:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="HKZDJTXSKN",Visible=.t., Left=276, Top=364,;
    Alignment=1, Width=55, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="XLOHVSIDIH",Visible=.t., Left=426, Top=364,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_2_39.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0 OR .w_TIPVAL<>'A' or Empty(nvl(.w_VALRAP,' ')))
    endwith
  endfunc

  add object oBox_2_34 as StdBox with uid="CIQVCQHASG",left=16, top=352, width=619,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kcf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
