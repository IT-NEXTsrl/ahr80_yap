* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_mvf                                                        *
*              Versamenti FIRR                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_36]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-16                                                      *
* Last revis.: 2014-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_mvf"))

* --- Class definition
define class tgsve_mvf as StdTrsForm
  Top    = 3
  Left   = 12

  * --- Standard Properties
  Width  = 655
  Height = 343+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-30"
  HelpContextID=46805609
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=44

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  VFI_MAST_IDX = 0
  VFI_DETT_IDX = 0
  ESERCIZI_IDX = 0
  PRO_LIQU_IDX = 0
  VALUTE_IDX = 0
  AGENTI_IDX = 0
  PAR_PROV_IDX = 0
  cFile = "VFI_MAST"
  cFileDetail = "VFI_DETT"
  cKeySelect = "VFSERIAL"
  cKeyWhere  = "VFSERIAL=this.w_VFSERIAL"
  cKeyDetail  = "VFSERIAL=this.w_VFSERIAL"
  cKeyWhereODBC = '"VFSERIAL="+cp_ToStrODBC(this.w_VFSERIAL)';

  cKeyDetailWhereODBC = '"VFSERIAL="+cp_ToStrODBC(this.w_VFSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"VFI_DETT.VFSERIAL="+cp_ToStrODBC(this.w_VFSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'VFI_DETT.CPROWNUM '
  cPrg = "gsve_mvf"
  cComment = "Versamenti FIRR"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_VFSERIAL = space(10)
  w_VFNUMREG = 0
  w_VF__ANNO = space(4)
  o_VF__ANNO = space(4)
  w_VFDATREG = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_VFCODVAL = space(3)
  o_VFCODVAL = space(3)
  w_VFDATINI = ctod('  /  /  ')
  w_VFDATFIN = ctod('  /  /  ')
  o_VFDATFIN = ctod('  /  /  ')
  w_DESAPP = space(35)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_VFCODAGE = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_VFFLORMI = 0
  w_VFIMPLIQ = 0
  w_VFIMPFI1 = 0
  o_VFIMPFI1 = 0
  w_VFPERFI1 = 0
  o_VFPERFI1 = 0
  w_VFCONFI1 = 0
  o_VFCONFI1 = 0
  w_VFIMPFI2 = 0
  o_VFIMPFI2 = 0
  w_VFPERFI2 = 0
  o_VFPERFI2 = 0
  w_VFCONFI2 = 0
  o_VFCONFI2 = 0
  w_VFIMPFI3 = 0
  o_VFIMPFI3 = 0
  w_VFPERFI3 = 0
  o_VFPERFI3 = 0
  w_VFCONFI3 = 0
  o_VFCONFI3 = 0
  w_IMPFIRR = 0
  w_desage = space(35)
  w_TOTFIRR = 0
  w_VFDATVER = ctod('  /  /  ')
  w_VFNUMVER = 0
  w_VFALFVER = space(2)
  w_DTOBSO = ctod('  /  /  ')
  w_VALUTA = space(3)
  w_SIMVAL = space(5)
  w_CODAZ2 = space(5)
  w_ANFIRR = space(4)
  w_GENFIR = space(1)
  w_ANPREV = space(4)
  w_TRIPRE = space(1)
  w_AGINIENA = ctod('  /  /  ')
  w_AGFINENA = ctod('  /  /  ')
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_VFSERIAL = this.W_VFSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_VF__ANNO = this.W_VF__ANNO
  op_VFNUMREG = this.W_VFNUMREG
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'VFI_MAST','gsve_mvf')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_mvfPag1","gsve_mvf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Versamento FIRR")
      .Pages(1).HelpContextID = 127454980
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='PRO_LIQU'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='AGENTI'
    this.cWorkTables[5]='PAR_PROV'
    this.cWorkTables[6]='VFI_MAST'
    this.cWorkTables[7]='VFI_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VFI_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VFI_MAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_VFSERIAL = NVL(VFSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from VFI_MAST where VFSERIAL=KeySet.VFSERIAL
    *
    i_nConn = i_TableProp[this.VFI_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VFI_MAST_IDX,2],this.bLoadRecFilter,this.VFI_MAST_IDX,"gsve_mvf")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VFI_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VFI_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"VFI_DETT.","VFI_MAST.")
      i_cTable = i_cTable+' VFI_MAST '
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'VFSERIAL',this.w_VFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_DESAPP = space(35)
        .w_DECTOT = 0
        .w_TOTFIRR = 0
        .w_DTOBSO = ctod("  /  /  ")
        .w_SIMVAL = space(5)
        .w_CODAZ2 = i_CODAZI
        .w_ANFIRR = space(4)
        .w_GENFIR = space(1)
        .w_ANPREV = space(4)
        .w_TRIPRE = space(1)
        .w_VFSERIAL = NVL(VFSERIAL,space(10))
        .op_VFSERIAL = .w_VFSERIAL
        .w_VFNUMREG = NVL(VFNUMREG,0)
        .op_VFNUMREG = .w_VFNUMREG
        .w_VF__ANNO = NVL(VF__ANNO,space(4))
        .op_VF__ANNO = .w_VF__ANNO
        .w_VFDATREG = NVL(cp_ToDate(VFDATREG),ctod("  /  /  "))
        .w_VFCODVAL = NVL(VFCODVAL,space(3))
          if link_1_6_joined
            this.w_VFCODVAL = NVL(VACODVAL106,NVL(this.w_VFCODVAL,space(3)))
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO106),ctod("  /  /  "))
            this.w_DECTOT = NVL(VADECTOT106,0)
            this.w_SIMVAL = NVL(VASIMVAL106,space(5))
          else
          .link_1_6('Load')
          endif
        .w_VFDATINI = NVL(cp_ToDate(VFDATINI),ctod("  /  /  "))
        .w_VFDATFIN = NVL(cp_ToDate(VFDATFIN),ctod("  /  /  "))
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .w_VFDATVER = NVL(cp_ToDate(VFDATVER),ctod("  /  /  "))
        .w_VFNUMVER = NVL(VFNUMVER,0)
        .w_VFALFVER = NVL(VFALFVER,space(2))
        .w_VALUTA = .w_VFCODVAL
          .link_1_28('Load')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'VFI_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from VFI_DETT where VFSERIAL=KeySet.VFSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.VFI_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VFI_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('VFI_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "VFI_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" VFI_DETT"
        link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'VFSERIAL',this.w_VFSERIAL  )
        select * from (i_cTable) VFI_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTFIRR = 0
      scan
        with this
          .w_DATOBSO = ctod("  /  /  ")
          .w_desage = space(35)
          .w_AGINIENA = ctod("  /  /  ")
          .w_AGFINENA = ctod("  /  /  ")
          .w_CPROWNUM = CPROWNUM
        .w_OBTEST = .w_VFDATREG
          .w_VFCODAGE = NVL(VFCODAGE,space(5))
          if link_2_1_joined
            this.w_VFCODAGE = NVL(AGCODAGE201,NVL(this.w_VFCODAGE,space(5)))
            this.w_desage = NVL(AGDESAGE201,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(AGDTOBSO201),ctod("  /  /  "))
          else
          .link_2_1('Load')
          endif
          .w_VFFLORMI = NVL(VFFLORMI,0)
          .w_VFIMPLIQ = NVL(VFIMPLIQ,0)
          .w_VFIMPFI1 = NVL(VFIMPFI1,0)
          .w_VFPERFI1 = NVL(VFPERFI1,0)
          .w_VFCONFI1 = NVL(VFCONFI1,0)
          .w_VFIMPFI2 = NVL(VFIMPFI2,0)
          .w_VFPERFI2 = NVL(VFPERFI2,0)
          .w_VFCONFI2 = NVL(VFCONFI2,0)
          .w_VFIMPFI3 = NVL(VFIMPFI3,0)
          .w_VFPERFI3 = NVL(VFPERFI3,0)
          .w_VFCONFI3 = NVL(VFCONFI3,0)
        .w_IMPFIRR = cp_ROUND (.w_VFCONFI1+.w_VFCONFI2+.w_VFCONFI3,.w_DECTOT)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTFIRR = .w_TOTFIRR+.w_IMPFIRR
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_OBTEST = .w_VFDATREG
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .w_VALUTA = .w_VFCODVAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_VFSERIAL=space(10)
      .w_VFNUMREG=0
      .w_VF__ANNO=space(4)
      .w_VFDATREG=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_VFCODVAL=space(3)
      .w_VFDATINI=ctod("  /  /  ")
      .w_VFDATFIN=ctod("  /  /  ")
      .w_DESAPP=space(35)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_VFCODAGE=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_VFFLORMI=0
      .w_VFIMPLIQ=0
      .w_VFIMPFI1=0
      .w_VFPERFI1=0
      .w_VFCONFI1=0
      .w_VFIMPFI2=0
      .w_VFPERFI2=0
      .w_VFCONFI2=0
      .w_VFIMPFI3=0
      .w_VFPERFI3=0
      .w_VFCONFI3=0
      .w_IMPFIRR=0
      .w_desage=space(35)
      .w_TOTFIRR=0
      .w_VFDATVER=ctod("  /  /  ")
      .w_VFNUMVER=0
      .w_VFALFVER=space(2)
      .w_DTOBSO=ctod("  /  /  ")
      .w_VALUTA=space(3)
      .w_SIMVAL=space(5)
      .w_CODAZ2=space(5)
      .w_ANFIRR=space(4)
      .w_GENFIR=space(1)
      .w_ANPREV=space(4)
      .w_TRIPRE=space(1)
      .w_AGINIENA=ctod("  /  /  ")
      .w_AGFINENA=ctod("  /  /  ")
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_VF__ANNO = IIF(EMPTY(.w_VFDATFIN), STR(YEAR(i_DATSYS),4,0), STR(YEAR(.w_VFDATFIN),4,0))
        .w_VFDATREG = i_datsys
        .w_OBTEST = .w_VFDATREG
        .w_VFCODVAL = iif(g_perval=g_codeur,g_codeur,g_codlir)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_VFCODVAL))
         .link_1_6('Full')
        endif
        .w_VFDATINI = cp_CharToDate('01-01-'+str(val(.w_VF__ANNO)-1,4,0))
        .w_VFDATFIN = cp_CharToDate('31-12-'+str(val(.w_VF__ANNO)-1,4,0))
        .DoRTCalc(9,10,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_VFCODAGE))
         .link_2_1('Full')
        endif
        .DoRTCalc(13,17,.f.)
        .w_VFCONFI1 = cp_ROUND (.w_VFIMPFI1*.w_VFPERFI1/100,.w_DECTOT)
        .DoRTCalc(19,20,.f.)
        .w_VFCONFI2 = cp_ROUND (.w_VFIMPFI2*.w_VFPERFI2/100,.w_DECTOT)
        .DoRTCalc(22,23,.f.)
        .w_VFCONFI3 = cp_ROUND (.w_VFIMPFI3*.w_VFPERFI3/100,.w_DECTOT)
        .w_IMPFIRR = cp_ROUND (.w_VFCONFI1+.w_VFCONFI2+.w_VFCONFI3,.w_DECTOT)
        .DoRTCalc(26,31,.f.)
        .w_VALUTA = .w_VFCODVAL
        .DoRTCalc(33,33,.f.)
        .w_CODAZ2 = i_CODAZI
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_CODAZ2))
         .link_1_28('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'VFI_MAST')
    this.DoRTCalc(35,44,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oVFNUMREG_1_2.enabled = i_bVal
      .Page1.oPag.oVF__ANNO_1_3.enabled = i_bVal
      .Page1.oPag.oVFDATREG_1_4.enabled = i_bVal
      .Page1.oPag.oVFCODVAL_1_6.enabled = i_bVal
      .Page1.oPag.oVFDATINI_1_7.enabled = i_bVal
      .Page1.oPag.oVFDATFIN_1_8.enabled = i_bVal
      .Page1.oPag.oVFIMPFI1_2_5.enabled = i_bVal
      .Page1.oPag.oVFPERFI1_2_6.enabled = i_bVal
      .Page1.oPag.oVFIMPFI2_2_8.enabled = i_bVal
      .Page1.oPag.oVFPERFI2_2_9.enabled = i_bVal
      .Page1.oPag.oVFIMPFI3_2_11.enabled = i_bVal
      .Page1.oPag.oVFPERFI3_2_12.enabled = i_bVal
      .Page1.oPag.oVFDATVER_3_3.enabled = i_bVal
      .Page1.oPag.oVFNUMVER_3_4.enabled = i_bVal
      .Page1.oPag.oVFALFVER_3_8.enabled = i_bVal
      .Page1.oPag.oBtn_1_22.enabled = i_bVal
      .Page1.oPag.oObj_1_23.enabled = i_bVal
      .Page1.oPag.oObj_1_24.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oVFNUMREG_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'VFI_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VFI_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VFI_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEVFI","i_codazi,w_VFSERIAL")
      cp_AskTableProg(this,i_nConn,"PRVFI","i_codazi,w_VF__ANNO,w_VFNUMREG")
      .op_codazi = .w_codazi
      .op_VFSERIAL = .w_VFSERIAL
      .op_codazi = .w_codazi
      .op_VF__ANNO = .w_VF__ANNO
      .op_VFNUMREG = .w_VFNUMREG
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VFI_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFSERIAL,"VFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFNUMREG,"VFNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VF__ANNO,"VF__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFDATREG,"VFDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFCODVAL,"VFCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFDATINI,"VFDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFDATFIN,"VFDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFDATVER,"VFDATVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFNUMVER,"VFNUMVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFALFVER,"VFALFVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VFI_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VFI_MAST_IDX,2])
    i_lTable = "VFI_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.VFI_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_VFCODAGE C(5);
      ,t_VFIMPLIQ N(18,4);
      ,t_VFIMPFI1 N(18,4);
      ,t_VFPERFI1 N(5,2);
      ,t_VFCONFI1 N(18,4);
      ,t_VFIMPFI2 N(18,4);
      ,t_VFPERFI2 N(5,2);
      ,t_VFCONFI2 N(18,4);
      ,t_VFIMPFI3 N(18,4);
      ,t_VFPERFI3 N(5,2);
      ,t_VFCONFI3 N(18,4);
      ,t_IMPFIRR N(18,4);
      ,t_desage C(35);
      ,t_AGINIENA D(8);
      ,t_AGFINENA D(8);
      ,CPROWNUM N(10);
      ,t_OBTEST D(8);
      ,t_DATOBSO D(8);
      ,t_VFFLORMI N(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsve_mvfbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVFCODAGE_2_1.controlsource=this.cTrsName+'.t_VFCODAGE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVFIMPLIQ_2_4.controlsource=this.cTrsName+'.t_VFIMPLIQ'
    this.oPgFRm.Page1.oPag.oVFIMPFI1_2_5.controlsource=this.cTrsName+'.t_VFIMPFI1'
    this.oPgFRm.Page1.oPag.oVFPERFI1_2_6.controlsource=this.cTrsName+'.t_VFPERFI1'
    this.oPgFRm.Page1.oPag.oVFCONFI1_2_7.controlsource=this.cTrsName+'.t_VFCONFI1'
    this.oPgFRm.Page1.oPag.oVFIMPFI2_2_8.controlsource=this.cTrsName+'.t_VFIMPFI2'
    this.oPgFRm.Page1.oPag.oVFPERFI2_2_9.controlsource=this.cTrsName+'.t_VFPERFI2'
    this.oPgFRm.Page1.oPag.oVFCONFI2_2_10.controlsource=this.cTrsName+'.t_VFCONFI2'
    this.oPgFRm.Page1.oPag.oVFIMPFI3_2_11.controlsource=this.cTrsName+'.t_VFIMPFI3'
    this.oPgFRm.Page1.oPag.oVFPERFI3_2_12.controlsource=this.cTrsName+'.t_VFPERFI3'
    this.oPgFRm.Page1.oPag.oVFCONFI3_2_13.controlsource=this.cTrsName+'.t_VFCONFI3'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIMPFIRR_2_14.controlsource=this.cTrsName+'.t_IMPFIRR'
    this.oPgFRm.Page1.oPag.odesage_2_15.controlsource=this.cTrsName+'.t_desage'
    this.oPgFRm.Page1.oPag.oAGINIENA_2_17.controlsource=this.cTrsName+'.t_AGINIENA'
    this.oPgFRm.Page1.oPag.oAGFINENA_2_18.controlsource=this.cTrsName+'.t_AGFINENA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(70)
    this.AddVLine(195)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVFCODAGE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VFI_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VFI_MAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEVFI","i_codazi,w_VFSERIAL")
          cp_NextTableProg(this,i_nConn,"PRVFI","i_codazi,w_VF__ANNO,w_VFNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into VFI_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VFI_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'VFI_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(VFSERIAL,VFNUMREG,VF__ANNO,VFDATREG,VFCODVAL"+;
                  ",VFDATINI,VFDATFIN,VFDATVER,VFNUMVER,VFALFVER"+;
                  ",UTCC,UTDC,UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_VFSERIAL)+;
                    ","+cp_ToStrODBC(this.w_VFNUMREG)+;
                    ","+cp_ToStrODBC(this.w_VF__ANNO)+;
                    ","+cp_ToStrODBC(this.w_VFDATREG)+;
                    ","+cp_ToStrODBCNull(this.w_VFCODVAL)+;
                    ","+cp_ToStrODBC(this.w_VFDATINI)+;
                    ","+cp_ToStrODBC(this.w_VFDATFIN)+;
                    ","+cp_ToStrODBC(this.w_VFDATVER)+;
                    ","+cp_ToStrODBC(this.w_VFNUMVER)+;
                    ","+cp_ToStrODBC(this.w_VFALFVER)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VFI_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'VFI_MAST')
        cp_CheckDeletedKey(i_cTable,0,'VFSERIAL',this.w_VFSERIAL)
        INSERT INTO (i_cTable);
              (VFSERIAL,VFNUMREG,VF__ANNO,VFDATREG,VFCODVAL,VFDATINI,VFDATFIN,VFDATVER,VFNUMVER,VFALFVER,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_VFSERIAL;
                  ,this.w_VFNUMREG;
                  ,this.w_VF__ANNO;
                  ,this.w_VFDATREG;
                  ,this.w_VFCODVAL;
                  ,this.w_VFDATINI;
                  ,this.w_VFDATFIN;
                  ,this.w_VFDATVER;
                  ,this.w_VFNUMVER;
                  ,this.w_VFALFVER;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VFI_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VFI_DETT_IDX,2])
      *
      * insert into VFI_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(VFSERIAL,VFCODAGE,VFFLORMI,VFIMPLIQ,VFIMPFI1"+;
                  ",VFPERFI1,VFCONFI1,VFIMPFI2,VFPERFI2,VFCONFI2"+;
                  ",VFIMPFI3,VFPERFI3,VFCONFI3,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_VFSERIAL)+","+cp_ToStrODBCNull(this.w_VFCODAGE)+","+cp_ToStrODBC(this.w_VFFLORMI)+","+cp_ToStrODBC(this.w_VFIMPLIQ)+","+cp_ToStrODBC(this.w_VFIMPFI1)+;
             ","+cp_ToStrODBC(this.w_VFPERFI1)+","+cp_ToStrODBC(this.w_VFCONFI1)+","+cp_ToStrODBC(this.w_VFIMPFI2)+","+cp_ToStrODBC(this.w_VFPERFI2)+","+cp_ToStrODBC(this.w_VFCONFI2)+;
             ","+cp_ToStrODBC(this.w_VFIMPFI3)+","+cp_ToStrODBC(this.w_VFPERFI3)+","+cp_ToStrODBC(this.w_VFCONFI3)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'VFSERIAL',this.w_VFSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_VFSERIAL,this.w_VFCODAGE,this.w_VFFLORMI,this.w_VFIMPLIQ,this.w_VFIMPFI1"+;
                ",this.w_VFPERFI1,this.w_VFCONFI1,this.w_VFIMPFI2,this.w_VFPERFI2,this.w_VFCONFI2"+;
                ",this.w_VFIMPFI3,this.w_VFPERFI3,this.w_VFCONFI3,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.VFI_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VFI_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update VFI_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'VFI_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " VFNUMREG="+cp_ToStrODBC(this.w_VFNUMREG)+;
             ",VF__ANNO="+cp_ToStrODBC(this.w_VF__ANNO)+;
             ",VFDATREG="+cp_ToStrODBC(this.w_VFDATREG)+;
             ",VFCODVAL="+cp_ToStrODBCNull(this.w_VFCODVAL)+;
             ",VFDATINI="+cp_ToStrODBC(this.w_VFDATINI)+;
             ",VFDATFIN="+cp_ToStrODBC(this.w_VFDATFIN)+;
             ",VFDATVER="+cp_ToStrODBC(this.w_VFDATVER)+;
             ",VFNUMVER="+cp_ToStrODBC(this.w_VFNUMVER)+;
             ",VFALFVER="+cp_ToStrODBC(this.w_VFALFVER)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'VFI_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'VFSERIAL',this.w_VFSERIAL  )
          UPDATE (i_cTable) SET;
              VFNUMREG=this.w_VFNUMREG;
             ,VF__ANNO=this.w_VF__ANNO;
             ,VFDATREG=this.w_VFDATREG;
             ,VFCODVAL=this.w_VFCODVAL;
             ,VFDATINI=this.w_VFDATINI;
             ,VFDATFIN=this.w_VFDATFIN;
             ,VFDATVER=this.w_VFDATVER;
             ,VFNUMVER=this.w_VFNUMVER;
             ,VFALFVER=this.w_VFALFVER;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_VFCODAGE) AND t_VFIMPLIQ<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.VFI_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.VFI_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from VFI_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update VFI_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " VFCODAGE="+cp_ToStrODBCNull(this.w_VFCODAGE)+;
                     ",VFFLORMI="+cp_ToStrODBC(this.w_VFFLORMI)+;
                     ",VFIMPLIQ="+cp_ToStrODBC(this.w_VFIMPLIQ)+;
                     ",VFIMPFI1="+cp_ToStrODBC(this.w_VFIMPFI1)+;
                     ",VFPERFI1="+cp_ToStrODBC(this.w_VFPERFI1)+;
                     ",VFCONFI1="+cp_ToStrODBC(this.w_VFCONFI1)+;
                     ",VFIMPFI2="+cp_ToStrODBC(this.w_VFIMPFI2)+;
                     ",VFPERFI2="+cp_ToStrODBC(this.w_VFPERFI2)+;
                     ",VFCONFI2="+cp_ToStrODBC(this.w_VFCONFI2)+;
                     ",VFIMPFI3="+cp_ToStrODBC(this.w_VFIMPFI3)+;
                     ",VFPERFI3="+cp_ToStrODBC(this.w_VFPERFI3)+;
                     ",VFCONFI3="+cp_ToStrODBC(this.w_VFCONFI3)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      VFCODAGE=this.w_VFCODAGE;
                     ,VFFLORMI=this.w_VFFLORMI;
                     ,VFIMPLIQ=this.w_VFIMPLIQ;
                     ,VFIMPFI1=this.w_VFIMPFI1;
                     ,VFPERFI1=this.w_VFPERFI1;
                     ,VFCONFI1=this.w_VFCONFI1;
                     ,VFIMPFI2=this.w_VFIMPFI2;
                     ,VFPERFI2=this.w_VFPERFI2;
                     ,VFCONFI2=this.w_VFCONFI2;
                     ,VFIMPFI3=this.w_VFIMPFI3;
                     ,VFPERFI3=this.w_VFPERFI3;
                     ,VFCONFI3=this.w_VFCONFI3;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_VFCODAGE) AND t_VFIMPLIQ<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.VFI_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.VFI_DETT_IDX,2])
        *
        * delete VFI_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.VFI_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.VFI_MAST_IDX,2])
        *
        * delete VFI_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_VFCODAGE) AND t_VFIMPLIQ<>0) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VFI_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VFI_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_VFDATFIN<>.w_VFDATFIN
          .w_VF__ANNO = IIF(EMPTY(.w_VFDATFIN), STR(YEAR(i_DATSYS),4,0), STR(YEAR(.w_VFDATFIN),4,0))
        endif
        .DoRTCalc(4,4,.t.)
          .w_OBTEST = .w_VFDATREG
        .DoRTCalc(6,6,.t.)
        if .o_VF__ANNO<>.w_VF__ANNO
          .w_VFDATINI = cp_CharToDate('01-01-'+str(val(.w_VF__ANNO)-1,4,0))
        endif
        if .o_VF__ANNO<>.w_VF__ANNO
          .w_VFDATFIN = cp_CharToDate('31-12-'+str(val(.w_VF__ANNO)-1,4,0))
        endif
        .DoRTCalc(9,10,.t.)
        if .o_VFCODVAL<>.w_VFCODVAL
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .DoRTCalc(12,17,.t.)
        if .o_VFIMPFI1<>.w_VFIMPFI1.or. .o_VFPERFI1<>.w_VFPERFI1
          .w_VFCONFI1 = cp_ROUND (.w_VFIMPFI1*.w_VFPERFI1/100,.w_DECTOT)
        endif
        .DoRTCalc(19,20,.t.)
        if .o_VFIMPFI2<>.w_VFIMPFI2.or. .o_VFPERFI2<>.w_VFPERFI2
          .w_VFCONFI2 = cp_ROUND (.w_VFIMPFI2*.w_VFPERFI2/100,.w_DECTOT)
        endif
        .DoRTCalc(22,23,.t.)
        if .o_VFIMPFI3<>.w_VFIMPFI3.or. .o_VFPERFI3<>.w_VFPERFI3
          .w_VFCONFI3 = cp_ROUND (.w_VFIMPFI3*.w_VFPERFI3/100,.w_DECTOT)
        endif
        if .o_VFCONFI1<>.w_VFCONFI1.or. .o_VFCONFI2<>.w_VFCONFI2.or. .o_VFCONFI3<>.w_VFCONFI3
          .w_TOTFIRR = .w_TOTFIRR-.w_impfirr
          .w_IMPFIRR = cp_ROUND (.w_VFCONFI1+.w_VFCONFI2+.w_VFCONFI3,.w_DECTOT)
          .w_TOTFIRR = .w_TOTFIRR+.w_impfirr
        endif
        .DoRTCalc(26,31,.t.)
          .w_VALUTA = .w_VFCODVAL
        .DoRTCalc(33,33,.t.)
          .link_1_28('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEVFI","i_codazi,w_VFSERIAL")
          .op_VFSERIAL = .w_VFSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_VF__ANNO<>.w_VF__ANNO
           cp_AskTableProg(this,i_nConn,"PRVFI","i_codazi,w_VF__ANNO,w_VFNUMREG")
          .op_VFNUMREG = .w_VFNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_VF__ANNO = .w_VF__ANNO
      endwith
      this.DoRTCalc(35,44,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_OBTEST with this.w_OBTEST
      replace t_DATOBSO with this.w_DATOBSO
      replace t_VFFLORMI with this.w_VFFLORMI
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_PCJBSJJXMN()
    with this
          * --- Ricalcolo anno pagamento provvigioni
          .w_VF__ANNO = IIF(EMPTY(.w_VFDATFIN), STR(YEAR(i_DATSYS),4,0), STR(YEAR(.w_VFDATFIN),4,0))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oVFNUMVER_3_4.enabled = this.oPgFrm.Page1.oPag.oVFNUMVER_3_4.mCond()
    this.oPgFrm.Page1.oPag.oVFALFVER_3_8.enabled = this.oPgFrm.Page1.oPag.oVFALFVER_3_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVFIMPLIQ_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVFIMPLIQ_2_4.mCond()
    this.oPgFrm.Page1.oPag.oVFIMPFI1_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oVFIMPFI1_2_5.mCond()
    this.oPgFrm.Page1.oPag.oVFPERFI1_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oVFPERFI1_2_6.mCond()
    this.oPgFrm.Page1.oPag.oVFIMPFI2_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oVFIMPFI2_2_8.mCond()
    this.oPgFrm.Page1.oPag.oVFPERFI2_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oVFPERFI2_2_9.mCond()
    this.oPgFrm.Page1.oPag.oVFIMPFI3_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oVFIMPFI3_2_11.mCond()
    this.oPgFrm.Page1.oPag.oVFPERFI3_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oVFPERFI3_2_12.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_VFDATFIN Changed")
          .Calculate_PCJBSJJXMN()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=VFCODVAL
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VFCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VFCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VADECTOT,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VFCODVAL))
          select VACODVAL,VADTOBSO,VADECTOT,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VFCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VFCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVFCODVAL_1_6'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VADECTOT,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADTOBSO,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VFCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VFCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VFCODVAL)
            select VACODVAL,VADTOBSO,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VFCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VFCODVAL = space(3)
      endif
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_DECTOT = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
        endif
        this.w_VFCODVAL = space(3)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_DECTOT = 0
        this.w_SIMVAL = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VFCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.VACODVAL as VACODVAL106"+ ",link_1_6.VADTOBSO as VADTOBSO106"+ ",link_1_6.VADECTOT as VADECTOT106"+ ",link_1_6.VASIMVAL as VASIMVAL106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on VFI_MAST.VFCODVAL=link_1_6.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and VFI_MAST.VFCODVAL=link_1_6.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VFCODAGE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VFCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_VFCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_VFCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VFCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_VFCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_VFCODAGE)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_VFCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oVFCODAGE_2_1'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VFCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_VFCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_VFCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VFCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_desage = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_VFCODAGE = space(5)
      endif
      this.w_desage = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Agente inesistente oppure obsoleto")
        endif
        this.w_VFCODAGE = space(5)
        this.w_desage = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VFCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.AGCODAGE as AGCODAGE201"+ ",link_2_1.AGDESAGE as AGDESAGE201"+ ",link_2_1.AGDTOBSO as AGDTOBSO201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on VFI_DETT.VFCODAGE=link_2_1.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and VFI_DETT.VFCODAGE=link_2_1.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODAZ2
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROV_IDX,3]
    i_lTable = "PAR_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2], .t., this.PAR_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZ2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZ2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODAZI,PPANFIRR,PPGENFIR,PPANPREV,PPTRIPRE";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODAZI="+cp_ToStrODBC(this.w_CODAZ2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODAZI',this.w_CODAZ2)
            select PPCODAZI,PPANFIRR,PPGENFIR,PPANPREV,PPTRIPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZ2 = NVL(_Link_.PPCODAZI,space(5))
      this.w_ANFIRR = NVL(_Link_.PPANFIRR,space(4))
      this.w_GENFIR = NVL(_Link_.PPGENFIR,space(1))
      this.w_ANPREV = NVL(_Link_.PPANPREV,space(4))
      this.w_TRIPRE = NVL(_Link_.PPTRIPRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZ2 = space(5)
      endif
      this.w_ANFIRR = space(4)
      this.w_GENFIR = space(1)
      this.w_ANPREV = space(4)
      this.w_TRIPRE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2])+'\'+cp_ToStr(_Link_.PPCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZ2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oVFNUMREG_1_2.value==this.w_VFNUMREG)
      this.oPgFrm.Page1.oPag.oVFNUMREG_1_2.value=this.w_VFNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oVF__ANNO_1_3.value==this.w_VF__ANNO)
      this.oPgFrm.Page1.oPag.oVF__ANNO_1_3.value=this.w_VF__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oVFDATREG_1_4.value==this.w_VFDATREG)
      this.oPgFrm.Page1.oPag.oVFDATREG_1_4.value=this.w_VFDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oVFCODVAL_1_6.RadioValue()==this.w_VFCODVAL)
      this.oPgFrm.Page1.oPag.oVFCODVAL_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVFDATINI_1_7.value==this.w_VFDATINI)
      this.oPgFrm.Page1.oPag.oVFDATINI_1_7.value=this.w_VFDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oVFDATFIN_1_8.value==this.w_VFDATFIN)
      this.oPgFrm.Page1.oPag.oVFDATFIN_1_8.value=this.w_VFDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oVFIMPFI1_2_5.value==this.w_VFIMPFI1)
      this.oPgFrm.Page1.oPag.oVFIMPFI1_2_5.value=this.w_VFIMPFI1
      replace t_VFIMPFI1 with this.oPgFrm.Page1.oPag.oVFIMPFI1_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oVFPERFI1_2_6.value==this.w_VFPERFI1)
      this.oPgFrm.Page1.oPag.oVFPERFI1_2_6.value=this.w_VFPERFI1
      replace t_VFPERFI1 with this.oPgFrm.Page1.oPag.oVFPERFI1_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oVFCONFI1_2_7.value==this.w_VFCONFI1)
      this.oPgFrm.Page1.oPag.oVFCONFI1_2_7.value=this.w_VFCONFI1
      replace t_VFCONFI1 with this.oPgFrm.Page1.oPag.oVFCONFI1_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oVFIMPFI2_2_8.value==this.w_VFIMPFI2)
      this.oPgFrm.Page1.oPag.oVFIMPFI2_2_8.value=this.w_VFIMPFI2
      replace t_VFIMPFI2 with this.oPgFrm.Page1.oPag.oVFIMPFI2_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oVFPERFI2_2_9.value==this.w_VFPERFI2)
      this.oPgFrm.Page1.oPag.oVFPERFI2_2_9.value=this.w_VFPERFI2
      replace t_VFPERFI2 with this.oPgFrm.Page1.oPag.oVFPERFI2_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oVFCONFI2_2_10.value==this.w_VFCONFI2)
      this.oPgFrm.Page1.oPag.oVFCONFI2_2_10.value=this.w_VFCONFI2
      replace t_VFCONFI2 with this.oPgFrm.Page1.oPag.oVFCONFI2_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oVFIMPFI3_2_11.value==this.w_VFIMPFI3)
      this.oPgFrm.Page1.oPag.oVFIMPFI3_2_11.value=this.w_VFIMPFI3
      replace t_VFIMPFI3 with this.oPgFrm.Page1.oPag.oVFIMPFI3_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oVFPERFI3_2_12.value==this.w_VFPERFI3)
      this.oPgFrm.Page1.oPag.oVFPERFI3_2_12.value=this.w_VFPERFI3
      replace t_VFPERFI3 with this.oPgFrm.Page1.oPag.oVFPERFI3_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oVFCONFI3_2_13.value==this.w_VFCONFI3)
      this.oPgFrm.Page1.oPag.oVFCONFI3_2_13.value=this.w_VFCONFI3
      replace t_VFCONFI3 with this.oPgFrm.Page1.oPag.oVFCONFI3_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.odesage_2_15.value==this.w_desage)
      this.oPgFrm.Page1.oPag.odesage_2_15.value=this.w_desage
      replace t_desage with this.oPgFrm.Page1.oPag.odesage_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTFIRR_3_2.value==this.w_TOTFIRR)
      this.oPgFrm.Page1.oPag.oTOTFIRR_3_2.value=this.w_TOTFIRR
    endif
    if not(this.oPgFrm.Page1.oPag.oVFDATVER_3_3.value==this.w_VFDATVER)
      this.oPgFrm.Page1.oPag.oVFDATVER_3_3.value=this.w_VFDATVER
    endif
    if not(this.oPgFrm.Page1.oPag.oVFNUMVER_3_4.value==this.w_VFNUMVER)
      this.oPgFrm.Page1.oPag.oVFNUMVER_3_4.value=this.w_VFNUMVER
    endif
    if not(this.oPgFrm.Page1.oPag.oVFALFVER_3_8.value==this.w_VFALFVER)
      this.oPgFrm.Page1.oPag.oVFALFVER_3_8.value=this.w_VFALFVER
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_27.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_27.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oAGINIENA_2_17.value==this.w_AGINIENA)
      this.oPgFrm.Page1.oPag.oAGINIENA_2_17.value=this.w_AGINIENA
      replace t_AGINIENA with this.oPgFrm.Page1.oPag.oAGINIENA_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oAGFINENA_2_18.value==this.w_AGFINENA)
      this.oPgFrm.Page1.oPag.oAGFINENA_2_18.value=this.w_AGFINENA
      replace t_AGFINENA with this.oPgFrm.Page1.oPag.oAGFINENA_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVFCODAGE_2_1.value==this.w_VFCODAGE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVFCODAGE_2_1.value=this.w_VFCODAGE
      replace t_VFCODAGE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVFCODAGE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVFIMPLIQ_2_4.value==this.w_VFIMPLIQ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVFIMPLIQ_2_4.value=this.w_VFIMPLIQ
      replace t_VFIMPLIQ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVFIMPLIQ_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMPFIRR_2_14.value==this.w_IMPFIRR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMPFIRR_2_14.value=this.w_IMPFIRR
      replace t_IMPFIRR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMPFIRR_2_14.value
    endif
    cp_SetControlsValueExtFlds(this,'VFI_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(iif(.w_GENFIR<>'S',IIF(.w_TRIPRE='4',.w_ANPREV<substr(DTOC(.w_VFDATFIN),7,4),(VAL(.w_ANPREV)-1)<VAL(substr(DTOC(.w_VFDATFIN),7,4))),.w_ANFIRR<substr(DTOC(.w_VFDATFIN),7,4)) )
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Impossibile salvare: generata distinta su file in data successiva o uguale ai movimenti."))
          case   (empty(.w_VF__ANNO))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oVF__ANNO_1_3.SetFocus()
            i_bnoObbl = !empty(.w_VF__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_VFDATREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oVFDATREG_1_4.SetFocus()
            i_bnoObbl = !empty(.w_VFDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_VFCODVAL) or not(CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oVFCODVAL_1_6.SetFocus()
            i_bnoObbl = !empty(.w_VFCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
          case   (empty(.w_VFDATINI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oVFDATINI_1_7.SetFocus()
            i_bnoObbl = !empty(.w_VFDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_VFDATFIN))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oVFDATFIN_1_8.SetFocus()
            i_bnoObbl = !empty(.w_VFDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)) and not(empty(.w_VFCODAGE)) and (NOT EMPTY(.w_VFCODAGE) AND .w_VFIMPLIQ<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVFCODAGE_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Agente inesistente oppure obsoleto")
      endcase
      if NOT EMPTY(.w_VFCODAGE) AND .w_VFIMPLIQ<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VF__ANNO = this.w_VF__ANNO
    this.o_VFCODVAL = this.w_VFCODVAL
    this.o_VFDATFIN = this.w_VFDATFIN
    this.o_VFIMPFI1 = this.w_VFIMPFI1
    this.o_VFPERFI1 = this.w_VFPERFI1
    this.o_VFCONFI1 = this.w_VFCONFI1
    this.o_VFIMPFI2 = this.w_VFIMPFI2
    this.o_VFPERFI2 = this.w_VFPERFI2
    this.o_VFCONFI2 = this.w_VFCONFI2
    this.o_VFIMPFI3 = this.w_VFIMPFI3
    this.o_VFPERFI3 = this.w_VFPERFI3
    this.o_VFCONFI3 = this.w_VFCONFI3
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_VFCODAGE) AND t_VFIMPLIQ<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_VFCODAGE=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_VFFLORMI=0
      .w_VFIMPLIQ=0
      .w_VFIMPFI1=0
      .w_VFPERFI1=0
      .w_VFCONFI1=0
      .w_VFIMPFI2=0
      .w_VFPERFI2=0
      .w_VFCONFI2=0
      .w_VFIMPFI3=0
      .w_VFPERFI3=0
      .w_VFCONFI3=0
      .w_IMPFIRR=0
      .w_desage=space(35)
      .w_AGINIENA=ctod("  /  /  ")
      .w_AGFINENA=ctod("  /  /  ")
      .DoRTCalc(1,4,.f.)
        .w_OBTEST = .w_VFDATREG
      .DoRTCalc(6,12,.f.)
      if not(empty(.w_VFCODAGE))
        .link_2_1('Full')
      endif
      .DoRTCalc(13,17,.f.)
        .w_VFCONFI1 = cp_ROUND (.w_VFIMPFI1*.w_VFPERFI1/100,.w_DECTOT)
      .DoRTCalc(19,20,.f.)
        .w_VFCONFI2 = cp_ROUND (.w_VFIMPFI2*.w_VFPERFI2/100,.w_DECTOT)
      .DoRTCalc(22,23,.f.)
        .w_VFCONFI3 = cp_ROUND (.w_VFIMPFI3*.w_VFPERFI3/100,.w_DECTOT)
        .w_IMPFIRR = cp_ROUND (.w_VFCONFI1+.w_VFCONFI2+.w_VFCONFI3,.w_DECTOT)
    endwith
    this.DoRTCalc(26,44,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_OBTEST = t_OBTEST
    this.w_VFCODAGE = t_VFCODAGE
    this.w_DATOBSO = t_DATOBSO
    this.w_VFFLORMI = t_VFFLORMI
    this.w_VFIMPLIQ = t_VFIMPLIQ
    this.w_VFIMPFI1 = t_VFIMPFI1
    this.w_VFPERFI1 = t_VFPERFI1
    this.w_VFCONFI1 = t_VFCONFI1
    this.w_VFIMPFI2 = t_VFIMPFI2
    this.w_VFPERFI2 = t_VFPERFI2
    this.w_VFCONFI2 = t_VFCONFI2
    this.w_VFIMPFI3 = t_VFIMPFI3
    this.w_VFPERFI3 = t_VFPERFI3
    this.w_VFCONFI3 = t_VFCONFI3
    this.w_IMPFIRR = t_IMPFIRR
    this.w_desage = t_desage
    this.w_AGINIENA = t_AGINIENA
    this.w_AGFINENA = t_AGFINENA
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_OBTEST with this.w_OBTEST
    replace t_VFCODAGE with this.w_VFCODAGE
    replace t_DATOBSO with this.w_DATOBSO
    replace t_VFFLORMI with this.w_VFFLORMI
    replace t_VFIMPLIQ with this.w_VFIMPLIQ
    replace t_VFIMPFI1 with this.w_VFIMPFI1
    replace t_VFPERFI1 with this.w_VFPERFI1
    replace t_VFCONFI1 with this.w_VFCONFI1
    replace t_VFIMPFI2 with this.w_VFIMPFI2
    replace t_VFPERFI2 with this.w_VFPERFI2
    replace t_VFCONFI2 with this.w_VFCONFI2
    replace t_VFIMPFI3 with this.w_VFIMPFI3
    replace t_VFPERFI3 with this.w_VFPERFI3
    replace t_VFCONFI3 with this.w_VFCONFI3
    replace t_IMPFIRR with this.w_IMPFIRR
    replace t_desage with this.w_desage
    replace t_AGINIENA with this.w_AGINIENA
    replace t_AGFINENA with this.w_AGFINENA
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTFIRR = .w_TOTFIRR-.w_impfirr
        .SetControlsValue()
      endwith
  EndProc
  func CanEdit()
    local i_res
    i_res=iif(this.w_GENFIR<>'S',IIF(this.w_TRIPRE='4',this.w_ANPREV<substr(DTOC(this.w_VFDATFIN),7,4),(VAL(this.w_ANPREV)-1)<VAL(substr(DTOC(this.w_VFDATFIN),7,4))),this.w_ANFIRR<substr(DTOC(this.w_VFDATFIN),7,4)) 
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile modificare: generata distinta su file in data successiva o uguale ai movimenti."))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=iif(this.w_GENFIR<>'S',IIF(this.w_TRIPRE='4',this.w_ANPREV<substr(DTOC(this.w_VFDATFIN),7,4),(VAL(this.w_ANPREV)-1)<VAL(substr(DTOC(this.w_VFDATFIN),7,4))),this.w_ANFIRR<substr(DTOC(this.w_VFDATFIN),7,4)) 
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile cancellare: generata distinta su file in data successiva o uguale ai movimenti."))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsve_mvfPag1 as StdContainer
  Width  = 651
  height = 343
  stdWidth  = 651
  stdheight = 343
  resizeYpos=247
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVFNUMREG_1_2 as StdField with uid="JOVYYUUAPT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_VFNUMREG", cQueryName = "VFNUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 195036771,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=121, Top=12, cSayPict='"999999"', cGetPict='"999999"'

  add object oVF__ANNO_1_3 as StdField with uid="WHJWHUGTDH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VF__ANNO", cQueryName = "VF__ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di pagamento delle provvigioni",;
    HelpContextID = 262867365,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=199, Top=12, InputMask=replicate('X',4)

  add object oVFDATREG_1_4 as StdField with uid="NGBMKRTQPQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_VFDATREG", cQueryName = "VFDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione",;
    HelpContextID = 189048419,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=279, Top=12


  add object oVFCODVAL_1_6 as StdCombo with uid="TJYCSNEXDQ",rtseq=6,rtrep=.f.,left=121,top=44,width=122,height=21;
    , ToolTipText = "Codice valuta del versamento";
    , HelpContextID = 130632098;
    , cFormVar="w_VFCODVAL",RowSource=""+"Valuta di conto,"+"Valuta nazionale", bObbl = .t. , nPag = 1;
    , sErrorMsg = "Valuta inesistente o incongruente o obsoleta";
    , cLinkFile="VALUTE";
  , bGlobalFont=.t.


  func oVFCODVAL_1_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..VFCODVAL,&i_cF..t_VFCODVAL),this.value)
    return(iif(xVal =1,g_codeur,;
    iif(xVal =2,g_codlir,;
    space(3))))
  endfunc
  func oVFCODVAL_1_6.GetRadio()
    this.Parent.oContained.w_VFCODVAL = this.RadioValue()
    return .t.
  endfunc

  func oVFCODVAL_1_6.ToRadio()
    this.Parent.oContained.w_VFCODVAL=trim(this.Parent.oContained.w_VFCODVAL)
    return(;
      iif(this.Parent.oContained.w_VFCODVAL==g_codeur,1,;
      iif(this.Parent.oContained.w_VFCODVAL==g_codlir,2,;
      0)))
  endfunc

  func oVFCODVAL_1_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oVFCODVAL_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oVFCODVAL_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oVFDATINI_1_7 as StdField with uid="XQXGUSKULC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_VFDATINI", cQueryName = "VFDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale pagamento provvigioni",;
    HelpContextID = 196827551,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=462, Top=12

  add object oVFDATFIN_1_8 as StdField with uid="PWTPPURXRO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_VFDATFIN", cQueryName = "VFDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data finale pagamento provvigioni",;
    HelpContextID = 121939548,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=462, Top=44


  add object oBtn_1_22 as StdButton with uid="BJOTKXVRGC",left=596, top=12, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare gli agenti";
    , HelpContextID = 130116630;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        do GSVE_BVF with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc


  add object oObj_1_23 as cp_runprogram with uid="PZVOCBJUZG",left=237, top=357, width=142,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSVE_BAP(0)',;
    cEvent = "Insert end",;
    nPag=1;
    , HelpContextID = 131192038


  add object oObj_1_24 as cp_runprogram with uid="XELIVUCXWY",left=383, top=358, width=157,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSVE_BAP(1)',;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 131192038

  add object oSIMVAL_1_27 as StdField with uid="JISJPNODRN",rtseq=33,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 228650022,;
   bGlobalFont=.t.,;
    Height=19, Width=55, Left=279, Top=44, InputMask=replicate('X',5)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=75, width=325,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="VFCODAGE",Label1="Agente",Field2="VFIMPLIQ",Label2="Provv.liquidate",Field3="IMPFIRR",Label3="Importo FIRR",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 849786

  add object oStr_1_12 as StdString with uid="JEAIHUWMKU",Visible=.t., Left=2, Top=12,;
    Alignment=1, Width=117, Height=15,;
    Caption="Registrazione n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="ZWBGYUMQXY",Visible=.t., Left=185, Top=12,;
    Alignment=2, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="TWCGOQPOED",Visible=.t., Left=245, Top=12,;
    Alignment=1, Width=32, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="OWRVFEOJJT",Visible=.t., Left=361, Top=12,;
    Alignment=1, Width=99, Height=15,;
    Caption="Provvigioni dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="BBHMGCQRWB",Visible=.t., Left=366, Top=44,;
    Alignment=1, Width=94, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="JRILPQAFOO",Visible=.t., Left=11, Top=44,;
    Alignment=1, Width=108, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="GXVUUHOBZI",Visible=.t., Left=344, Top=79,;
    Alignment=2, Width=114, Height=15,;
    Caption="Imponibile"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="DOOOAYTLLO",Visible=.t., Left=469, Top=79,;
    Alignment=2, Width=45, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="VARTDBKIJA",Visible=.t., Left=525, Top=79,;
    Alignment=2, Width=111, Height=15,;
    Caption="Contributo FIRR"  ;
  , bGlobalFont=.t.

  add object oBox_1_18 as StdBox with uid="DWFBDFESFM",left=342, top=75, width=298,height=20

  add object oBox_1_35 as StdBox with uid="WFVLMFMEJF",left=342, top=75, width=298,height=212

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=94,;
    width=321+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=95,width=320+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='AGENTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oVFIMPFI1_2_5.Refresh()
      this.Parent.oVFPERFI1_2_6.Refresh()
      this.Parent.oVFCONFI1_2_7.Refresh()
      this.Parent.oVFIMPFI2_2_8.Refresh()
      this.Parent.oVFPERFI2_2_9.Refresh()
      this.Parent.oVFCONFI2_2_10.Refresh()
      this.Parent.oVFIMPFI3_2_11.Refresh()
      this.Parent.oVFPERFI3_2_12.Refresh()
      this.Parent.oVFCONFI3_2_13.Refresh()
      this.Parent.odesage_2_15.Refresh()
      this.Parent.oAGINIENA_2_17.Refresh()
      this.Parent.oAGFINENA_2_18.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='AGENTI'
        oDropInto=this.oBodyCol.oRow.oVFCODAGE_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oVFIMPFI1_2_5 as StdTrsField with uid="PFWUEANBZO",rtseq=16,rtrep=.t.,;
    cFormVar="w_VFIMPFI1",value=0,;
    HelpContextID = 125326969,;
    cTotal="", bFixedPos=.t., cQueryName = "VFIMPFI1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=346, Top=96, cSayPict=[v_PV(36+VVL)], cGetPict=[v_GV(36+VVL)]

  func oVFIMPFI1_2_5.mCond()
    with this.Parent.oContained
      return (.w_VFFLORMI=0)
    endwith
  endfunc

  add object oVFPERFI1_2_6 as StdTrsField with uid="GNDMHMPZXK",rtseq=17,rtrep=.t.,;
    cFormVar="w_VFPERFI1",value=0,;
    HelpContextID = 123725433,;
    cTotal="", bFixedPos=.t., cQueryName = "VFPERFI1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=467, Top=96, cSayPict=["99.99"], cGetPict=["99.99"]

  func oVFPERFI1_2_6.mCond()
    with this.Parent.oContained
      return (.w_VFFLORMI=0)
    endwith
  endfunc

  add object oVFCONFI1_2_7 as StdTrsField with uid="XAFCVFXHWA",rtseq=18,rtrep=.t.,;
    cFormVar="w_VFCONFI1",value=0,enabled=.f.,;
    HelpContextID = 127317625,;
    cTotal="", bFixedPos=.t., cQueryName = "VFCONFI1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=522, Top=96, cSayPict=[v_PV(36+VVL)], cGetPict=[v_GV(36+VVL)]

  add object oVFIMPFI2_2_8 as StdTrsField with uid="NBOMADKAHY",rtseq=19,rtrep=.t.,;
    cFormVar="w_VFIMPFI2",value=0,;
    HelpContextID = 125326968,;
    cTotal="", bFixedPos=.t., cQueryName = "VFIMPFI2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=346, Top=128, cSayPict=[v_PV(36+VVL)], cGetPict=[v_GV(36+VVL)]

  func oVFIMPFI2_2_8.mCond()
    with this.Parent.oContained
      return (.w_VFFLORMI=0)
    endwith
  endfunc

  add object oVFPERFI2_2_9 as StdTrsField with uid="KPFCCPQFSK",rtseq=20,rtrep=.t.,;
    cFormVar="w_VFPERFI2",value=0,;
    HelpContextID = 123725432,;
    cTotal="", bFixedPos=.t., cQueryName = "VFPERFI2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=467, Top=128, cSayPict=["99.99"], cGetPict=["99.99"]

  func oVFPERFI2_2_9.mCond()
    with this.Parent.oContained
      return (.w_VFFLORMI=0)
    endwith
  endfunc

  add object oVFCONFI2_2_10 as StdTrsField with uid="RTFTFDIMQC",rtseq=21,rtrep=.t.,;
    cFormVar="w_VFCONFI2",value=0,enabled=.f.,;
    HelpContextID = 127317624,;
    cTotal="", bFixedPos=.t., cQueryName = "VFCONFI2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=522, Top=128, cSayPict=[v_PV(36+VVL)], cGetPict=[v_GV(36+VVL)]

  add object oVFIMPFI3_2_11 as StdTrsField with uid="VECUMJPMOC",rtseq=22,rtrep=.t.,;
    cFormVar="w_VFIMPFI3",value=0,;
    HelpContextID = 125326967,;
    cTotal="", bFixedPos=.t., cQueryName = "VFIMPFI3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=346, Top=160, cSayPict=[v_PV(36+VVL)], cGetPict=[v_GV(36+VVL)]

  func oVFIMPFI3_2_11.mCond()
    with this.Parent.oContained
      return (.w_VFFLORMI=0)
    endwith
  endfunc

  add object oVFPERFI3_2_12 as StdTrsField with uid="TKFNHVYKOS",rtseq=23,rtrep=.t.,;
    cFormVar="w_VFPERFI3",value=0,;
    HelpContextID = 123725431,;
    cTotal="", bFixedPos=.t., cQueryName = "VFPERFI3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=468, Top=160, cSayPict=["99.99"], cGetPict=["99.99"]

  func oVFPERFI3_2_12.mCond()
    with this.Parent.oContained
      return (.w_VFFLORMI=0)
    endwith
  endfunc

  add object oVFCONFI3_2_13 as StdTrsField with uid="NXYVIPZLJV",rtseq=24,rtrep=.t.,;
    cFormVar="w_VFCONFI3",value=0,enabled=.f.,;
    HelpContextID = 127317623,;
    cTotal="", bFixedPos=.t., cQueryName = "VFCONFI3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=522, Top=160, cSayPict=[v_PV(36+VVL)], cGetPict=[v_GV(36+VVL)]

  add object odesage_2_15 as StdTrsField with uid="LOROLZWAWT",rtseq=26,rtrep=.t.,;
    cFormVar="w_desage",value=space(35),enabled=.f.,;
    HelpContextID = 151939382,;
    cTotal="", bFixedPos=.t., cQueryName = "desage",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=253, Left=67, Top=294, InputMask=replicate('X',35)

  add object oAGINIENA_2_17 as StdTrsField with uid="HGHYYWKUDT",rtseq=39,rtrep=.t.,;
    cFormVar="w_AGINIENA",value=ctod("  /  /  "),enabled=.f.,;
    HelpContextID = 119056711,;
    cTotal="", bFixedPos=.t., cQueryName = "AGINIENA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=468, Top=192

  add object oAGFINENA_2_18 as StdTrsField with uid="SFQBTOELXT",rtseq=40,rtrep=.t.,;
    cFormVar="w_AGFINENA",value=ctod("  /  /  "),enabled=.f.,;
    HelpContextID = 123959623,;
    cTotal="", bFixedPos=.t., cQueryName = "AGFINENA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=468, Top=224

  add object oStr_2_16 as StdString with uid="XIBUOFMIAY",Visible=.t., Left=2, Top=294,;
    Alignment=1, Width=65, Height=15,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="ATHJFJXQBO",Visible=.t., Left=347, Top=192,;
    Alignment=1, Width=119, Height=18,;
    Caption="Data inizio mandato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="AQBDMTYRQF",Visible=.t., Left=362, Top=224,;
    Alignment=1, Width=104, Height=18,;
    Caption="Data fine mandato:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTFIRR_3_2 as StdField with uid="UGEGHQHLRH",rtseq=27,rtrep=.f.,;
    cFormVar="w_TOTFIRR",value=0,enabled=.f.,;
    HelpContextID = 68248118,;
    cQueryName = "TOTFIRR",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=199, Top=319, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oVFDATVER_3_3 as StdField with uid="KWIUMVFOSQ",rtseq=28,rtrep=.f.,;
    cFormVar="w_VFDATVER",value=ctod("  /  /  "),;
    ToolTipText = "Data versamento FIRR",;
    HelpContextID = 121939544,;
    cQueryName = "VFDATVER",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=364, Top=319

  add object oVFNUMVER_3_4 as StdField with uid="GAWRQFOCEI",rtseq=29,rtrep=.f.,;
    cFormVar="w_VFNUMVER",value=0,;
    ToolTipText = "Numero versamento FIRR",;
    HelpContextID = 127927896,;
    cQueryName = "VFNUMVER",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=468, Top=319

  func oVFNUMVER_3_4.mCond()
    with this.Parent.oContained
      return (not empty(.w_VFDATVER))
    endwith
  endfunc

  add object oVFALFVER_3_8 as StdField with uid="CBMBILHADK",rtseq=30,rtrep=.f.,;
    cFormVar="w_VFALFVER",value=space(2),;
    ToolTipText = "Alfa versamento",;
    HelpContextID = 135911000,;
    cQueryName = "VFALFVER",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=554, Top=319, InputMask=replicate('X',2)

  func oVFALFVER_3_8.mCond()
    with this.Parent.oContained
      return (not empty (.w_VFDATVER))
    endwith
  endfunc

  add object oStr_3_1 as StdString with uid="RGGYLNXNNL",Visible=.t., Left=1, Top=319,;
    Alignment=1, Width=195, Height=15,;
    Caption="Totale FIRR da versare:"  ;
  , bGlobalFont=.t.

  add object oStr_3_5 as StdString with uid="TWNUNNNDMK",Visible=.t., Left=541, Top=319,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_3_6 as StdString with uid="ANXNRFOHGO",Visible=.t., Left=364, Top=296,;
    Alignment=0, Width=96, Height=15,;
    Caption="Data versamento"  ;
  , bGlobalFont=.t.

  add object oStr_3_7 as StdString with uid="MFRCCZKQEM",Visible=.t., Left=468, Top=296,;
    Alignment=0, Width=107, Height=15,;
    Caption="Num.versamento"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsve_mvfBodyRow as CPBodyRowCnt
  Width=311
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oVFCODAGE_2_1 as StdTrsField with uid="PVDCHYYRWP",rtseq=12,rtrep=.t.,;
    cFormVar="w_VFCODAGE",value=space(5),;
    ToolTipText = "Codice agente",;
    HelpContextID = 221689445,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Agente inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_VFCODAGE"

  func oVFCODAGE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oVFCODAGE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oVFCODAGE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oVFCODAGE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oVFCODAGE_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_VFCODAGE
    i_obj.ecpSave()
  endproc

  add object oVFIMPLIQ_2_4 as StdTrsField with uid="QINLDFLXGA",rtseq=15,rtrep=.t.,;
    cFormVar="w_VFIMPLIQ",value=0,;
    ToolTipText = "Importo provv.liquidate",;
    HelpContextID = 24663641,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=121, Left=60, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oVFIMPLIQ_2_4.mCond()
    with this.Parent.oContained
      return (.w_VFFLORMI=0)
    endwith
  endfunc

  add object oIMPFIRR_2_14 as StdTrsField with uid="WUDPPVVYNS",rtseq=25,rtrep=.t.,;
    cFormVar="w_IMPFIRR",value=0,enabled=.f.,;
    ToolTipText = "Importo FIRR",;
    HelpContextID = 68231046,;
    cTotal = "this.Parent.oContained.w_totfirr", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=121, Left=185, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oVFCODAGE_2_1.When()
    return(.t.)
  proc oVFCODAGE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oVFCODAGE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_mvf','VFI_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".VFSERIAL=VFI_MAST.VFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
