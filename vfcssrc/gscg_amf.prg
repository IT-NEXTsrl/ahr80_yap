* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_amf                                                        *
*              Modello pagamento F24/2001                                      *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [116] [VRS_1000]                                                *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-08                                                      *
* Last revis.: 2011-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_amf"))

* --- Class definition
define class tgscg_amf as StdForm
  Top    = 4
  Left   = 19

  * --- Standard Properties
  Width  = 735
  Height = 390+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-09"
  HelpContextID=210380137
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=176

  * --- Constant Properties
  MOD_PAG_IDX = 0
  TRI_BUTI_IDX = 0
  AZIENDA_IDX = 0
  TITOLARI_IDX = 0
  COD_TRIB_IDX = 0
  CODI_UFF_IDX = 0
  SED_INPS_IDX = 0
  CAU_INPS_IDX = 0
  REG_PROV_IDX = 0
  ENTI_LOC_IDX = 0
  SE_INAIL_IDX = 0
  COD_PREV_IDX = 0
  SED_AEN_IDX = 0
  CAU_AEN_IDX = 0
  MODVPAG_IDX = 0
  MODCPAG_IDX = 0
  MODEPAG_IDX = 0
  cFile = "MOD_PAG"
  cKeySelect = "MFSERIAL"
  cKeyWhere  = "MFSERIAL=this.w_MFSERIAL"
  cKeyWhereODBC = '"MFSERIAL="+cp_ToStrODBC(this.w_MFSERIAL)';

  cKeyWhereODBCqualified = '"MOD_PAG.MFSERIAL="+cp_ToStrODBC(this.w_MFSERIAL)';

  cPrg = "gscg_amf"
  cComment = "Modello pagamento F24/2001"
  icon = "anag.ico"
  cAutoZoom = 'GSCG_AMF'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MFSERIAL = space(10)
  w_MFMESRIF = space(2)
  w_MFANNRIF = space(4)
  w_MFVALUTA = space(3)
  w_APPCOIN = space(1)
  o_APPCOIN = space(1)
  w_MF_COINC = space(1)
  w_MFSERIAL = space(10)
  w_APPOIMP = 0
  w_MFCODUFF = space(3)
  w_MFCODATT = space(11)
  w_MESE = space(2)
  w_ANNO = space(4)
  w_MFSERIAL = space(10)
  w_MFCDSED1 = space(5)
  o_MFCDSED1 = space(5)
  w_MFCCONT1 = space(5)
  o_MFCCONT1 = space(5)
  w_MFMINPS1 = space(22)
  w_APP1 = .F.
  o_APP1 = .F.
  w_MFDAMES1 = space(2)
  w_MFDAANN1 = space(4)
  w_MF_AMES1 = space(2)
  w_MFAN1 = space(4)
  w_MFIMPSD1 = 0
  w_MFIMPSC1 = 0
  w_MFCDSED2 = space(5)
  o_MFCDSED2 = space(5)
  w_MFCCONT2 = space(5)
  o_MFCCONT2 = space(5)
  w_MFMINPS2 = space(22)
  w_APP2 = .F.
  o_APP2 = .F.
  w_MFDAMES2 = space(2)
  w_MFDAANN2 = space(4)
  w_MF_AMES2 = space(2)
  w_MFAN2 = space(4)
  w_MFIMPSD2 = 0
  w_MFIMPSC2 = 0
  w_MFCDSED3 = space(5)
  o_MFCDSED3 = space(5)
  w_MFCCONT3 = space(5)
  o_MFCCONT3 = space(5)
  w_MFMINPS3 = space(22)
  w_APP3 = .F.
  o_APP3 = .F.
  w_MFDAMES3 = space(2)
  w_MFDAANN3 = space(4)
  w_MF_AMES3 = space(2)
  w_MFAN3 = space(4)
  w_MFIMPSD3 = 0
  w_MFIMPSC3 = 0
  w_MFCDSED4 = space(5)
  o_MFCDSED4 = space(5)
  w_MFCCONT4 = space(5)
  o_MFCCONT4 = space(5)
  w_MFMINPS4 = space(22)
  w_APP4 = .F.
  o_APP4 = .F.
  w_MFDAMES4 = space(2)
  w_MFDAANN4 = space(4)
  w_MF_AMES4 = space(2)
  w_MFAN4 = space(4)
  w_MFIMPSD4 = 0
  w_MFIMPSC4 = 0
  w_MFTOTDPS = 0
  w_MFTOTCPS = 0
  w_MFSALDPS = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_MFSERIAL = space(10)
  w_MFCODRE1 = space(2)
  o_MFCODRE1 = space(2)
  w_MFTRIRE1 = space(5)
  o_MFTRIRE1 = space(5)
  w_MFRATRE1 = space(4)
  w_MFANNRE1 = space(4)
  w_MFIMDRE1 = 0
  w_MFIMCRE1 = 0
  w_MFCODRE2 = space(2)
  o_MFCODRE2 = space(2)
  w_MFTRIRE2 = space(5)
  o_MFTRIRE2 = space(5)
  w_MFRATRE2 = space(4)
  w_MFANNRE2 = space(4)
  w_MFIMDRE2 = 0
  w_MFIMCRE2 = 0
  w_MFCODRE3 = space(2)
  o_MFCODRE3 = space(2)
  w_MFTRIRE3 = space(5)
  o_MFTRIRE3 = space(5)
  w_MFRATRE3 = space(4)
  w_MFANNRE3 = space(4)
  w_MFIMDRE3 = 0
  w_MFIMCRE3 = 0
  w_MFCODRE4 = space(2)
  o_MFCODRE4 = space(2)
  w_MFTRIRE4 = space(5)
  o_MFTRIRE4 = space(5)
  w_MFRATRE4 = space(4)
  w_MFANNRE4 = space(4)
  w_MFIMDRE4 = 0
  w_MFIMCRE4 = 0
  w_MFTOTDRE = 0
  w_MFTOTCRE = 0
  w_MFSALDRE = 0
  w_MFCODEL1 = space(2)
  o_MFCODEL1 = space(2)
  w_MFTRIEL1 = space(5)
  w_MFRATEL1 = space(4)
  w_MFANNEL1 = space(4)
  w_MFIMDEL1 = 0
  w_MFIMCEL1 = 0
  w_MFCODEL2 = space(2)
  o_MFCODEL2 = space(2)
  w_MFTRIEL2 = space(5)
  w_MFRATEL2 = space(4)
  w_MFANNEL2 = space(4)
  w_MFIMDEL2 = 0
  w_MFIMCEL2 = 0
  w_MFCODEL3 = space(2)
  o_MFCODEL3 = space(2)
  w_MFTRIEL3 = space(5)
  w_MFRATEL3 = space(4)
  w_MFANNEL3 = space(4)
  w_MFIMDEL3 = 0
  w_MFIMCEL3 = 0
  w_MFTOTDEL = 0
  w_MFTOTCEL = 0
  w_MFSALDEL = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_MFSERIAL = space(10)
  w_MFSINAI1 = space(5)
  o_MFSINAI1 = space(5)
  w_MF_NPOS1 = space(8)
  w_MF_PACC1 = space(2)
  w_MF_NRIF1 = space(7)
  w_MFCAUSA1 = space(2)
  w_MFIMDIL1 = 0
  w_MFIMCIL1 = 0
  w_MFSINAI2 = space(5)
  o_MFSINAI2 = space(5)
  w_MF_NPOS2 = space(8)
  w_MF_PACC2 = space(2)
  w_MF_NRIF2 = space(7)
  w_MFCAUSA2 = space(2)
  w_MFIMDIL2 = 0
  w_MFIMCIL2 = 0
  w_MFSINAI3 = space(5)
  o_MFSINAI3 = space(5)
  w_MF_NPOS3 = space(8)
  w_MF_PACC3 = space(2)
  w_MF_NRIF3 = space(7)
  w_MFCAUSA3 = space(2)
  w_MFIMDIL3 = 0
  w_MFIMCIL3 = 0
  w_MFTDINAI = 0
  w_MFTCINAI = 0
  w_MFSALINA = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_MFSERIAL = space(10)
  w_MFCDENTE = space(5)
  o_MFCDENTE = space(5)
  w_MFSDENT1 = space(5)
  o_MFSDENT1 = space(5)
  w_MFCCOAE1 = space(5)
  o_MFCCOAE1 = space(5)
  w_MFCDPOS1 = space(12)
  w_MFMSINE1 = space(2)
  w_MFANINE1 = space(4)
  w_MFMSFIE1 = space(2)
  w_MFANF1 = space(4)
  w_MFIMDAE1 = 0
  w_MFIMCAE1 = 0
  w_MFSDENT2 = space(5)
  o_MFSDENT2 = space(5)
  w_MFCCOAE2 = space(5)
  o_MFCCOAE2 = space(5)
  w_MFCDPOS2 = space(12)
  w_MFMSINE2 = space(2)
  w_MFANINE2 = space(4)
  w_MFMSFIE2 = space(2)
  w_MFANF2 = space(4)
  w_MFIMDAE2 = 0
  w_MFIMCAE2 = 0
  w_MFSDENT3 = space(5)
  o_MFSDENT3 = space(5)
  w_MFCCOAE3 = space(5)
  o_MFCCOAE3 = space(5)
  w_MFCDPOS3 = space(12)
  w_MFMSINE3 = space(2)
  w_MFANINE3 = space(4)
  w_MFMSFIE3 = space(2)
  w_MFANF3 = space(4)
  w_MFIMDAE3 = 0
  w_MFIMCAE3 = 0
  w_MFTDAENT = 0
  w_MFTCAENT = 0
  w_MFSALAEN = 0
  w_DESAENTE = space(30)
  w_MESE = space(2)
  w_ANNO = space(4)
  w_MFSERIAL = space(10)
  w_MFSALFIN = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_TIPOMOD = space(3)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_MFSERIAL = this.W_MFSERIAL

  * --- Children pointers
  GSCG_ACF = .NULL.
  GSCG_AEF = .NULL.
  GSCG_AVF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=8, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOD_PAG','gscg_amf')
    stdPageFrame::Init()
    *set procedure to GSCG_ACF additive
    with this
      .Pages(1).addobject("oPag","tgscg_amfPag1","gscg_amf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Contribuente")
      .Pages(1).HelpContextID = 75358955
      .Pages(2).addobject("oPag","tgscg_amfPag2","gscg_amf",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Erario")
      .Pages(2).HelpContextID = 159277382
      .Pages(3).addobject("oPag","tgscg_amfPag3","gscg_amf",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("INPS")
      .Pages(3).HelpContextID = 204591738
      .Pages(4).addobject("oPag","tgscg_amfPag4","gscg_amf",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Regioni enti locali")
      .Pages(4).HelpContextID = 122514425
      .Pages(5).addobject("oPag","tgscg_amfPag5","gscg_amf",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("INAIL")
      .Pages(5).HelpContextID = 125616762
      .Pages(6).addobject("oPag","tgscg_amfPag6","gscg_amf",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Altri enti")
      .Pages(6).HelpContextID = 176159668
      .Pages(7).addobject("oPag","tgscg_amfPag7","gscg_amf",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Estr.versamento")
      .Pages(7).HelpContextID = 136039144
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMFSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCG_ACF
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[17]
    this.cWorkTables[1]='TRI_BUTI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='TITOLARI'
    this.cWorkTables[4]='COD_TRIB'
    this.cWorkTables[5]='CODI_UFF'
    this.cWorkTables[6]='SED_INPS'
    this.cWorkTables[7]='CAU_INPS'
    this.cWorkTables[8]='REG_PROV'
    this.cWorkTables[9]='ENTI_LOC'
    this.cWorkTables[10]='SE_INAIL'
    this.cWorkTables[11]='COD_PREV'
    this.cWorkTables[12]='SED_AEN'
    this.cWorkTables[13]='CAU_AEN'
    this.cWorkTables[14]='MODVPAG'
    this.cWorkTables[15]='MODCPAG'
    this.cWorkTables[16]='MODEPAG'
    this.cWorkTables[17]='MOD_PAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(17))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_PAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_PAG_IDX,3]
  return

  function CreateChildren()
    this.GSCG_ACF = CREATEOBJECT('stdDynamicChild',this,'GSCG_ACF',this.oPgFrm.Page1.oPag.oLinkPC_1_7)
    this.GSCG_ACF.createrealchild()
    this.GSCG_AEF = CREATEOBJECT('stdDynamicChild',this,'GSCG_AEF',this.oPgFrm.Page2.oPag.oLinkPC_2_2)
    this.GSCG_AEF.createrealchild()
    this.GSCG_AVF = CREATEOBJECT('stdDynamicChild',this,'GSCG_AVF',this.oPgFrm.Page7.oPag.oLinkPC_7_3)
    this.GSCG_AVF.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_ACF)
      this.GSCG_ACF.DestroyChildrenChain()
      this.GSCG_ACF=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_7')
    if !ISNULL(this.GSCG_AEF)
      this.GSCG_AEF.DestroyChildrenChain()
      this.GSCG_AEF=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_2')
    if !ISNULL(this.GSCG_AVF)
      this.GSCG_AVF.DestroyChildrenChain()
      this.GSCG_AVF=.NULL.
    endif
    this.oPgFrm.Page7.oPag.RemoveObject('oLinkPC_7_3')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_ACF.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_AEF.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_AVF.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_ACF.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_AEF.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_AVF.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_ACF.NewDocument()
    this.GSCG_AEF.NewDocument()
    this.GSCG_AVF.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_ACF.SetKey(;
            .w_MFSERIAL,"CFSERIAL";
            )
      this.GSCG_AEF.SetKey(;
            .w_MFSERIAL,"EFSERIAL";
            )
      this.GSCG_AVF.SetKey(;
            .w_MFSERIAL,"VFSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_ACF.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"CFSERIAL";
             )
      .GSCG_AEF.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"EFSERIAL";
             )
      .GSCG_AVF.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"VFSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_ACF)
        i_f=.GSCG_ACF.BuildFilter()
        if !(i_f==.GSCG_ACF.cQueryFilter)
          i_fnidx=.GSCG_ACF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_ACF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_ACF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_ACF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_ACF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_AEF)
        i_f=.GSCG_AEF.BuildFilter()
        if !(i_f==.GSCG_AEF.cQueryFilter)
          i_fnidx=.GSCG_AEF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AEF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AEF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AEF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AEF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_AVF)
        i_f=.GSCG_AVF.BuildFilter()
        if !(i_f==.GSCG_AVF.cQueryFilter)
          i_fnidx=.GSCG_AVF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AVF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AVF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AVF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AVF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MFSERIAL = NVL(MFSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_6_3_joined
    link_6_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOD_PAG where MFSERIAL=KeySet.MFSERIAL
    *
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_PAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_PAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_PAG '
      link_6_3_joined=this.AddJoinedLink_6_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_APPCOIN = space(1)
        .w_APPOIMP = 0
        .w_DESAENTE = space(30)
        .w_TIPOMOD = 'OLD'
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFMESRIF = NVL(MFMESRIF,space(2))
        .w_MFANNRIF = NVL(MFANNRIF,space(4))
        .w_MFVALUTA = NVL(MFVALUTA,space(3))
        .w_MF_COINC = NVL(MF_COINC,space(1))
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCODUFF = NVL(MFCODUFF,space(3))
          * evitabile
          *.link_2_4('Load')
        .w_MFCODATT = NVL(MFCODATT,space(11))
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCDSED1 = NVL(MFCDSED1,space(5))
          * evitabile
          *.link_3_2('Load')
        .w_MFCCONT1 = NVL(MFCCONT1,space(5))
          * evitabile
          *.link_3_3('Load')
        .w_MFMINPS1 = NVL(MFMINPS1,space(22))
        .w_APP1 = !empty(.w_MFCCONT1) and alltrim(.w_MFCCONT1)$ 'ACON-DCON-DPC-DM10-DMRA-LAAP-PCFP-PCFS-PESC' and ! (alltrim(.w_MFCCONT1)$ 'CFP-AP')
        .w_MFDAMES1 = NVL(MFDAMES1,space(2))
        .w_MFDAANN1 = NVL(MFDAANN1,space(4))
        .w_MF_AMES1 = NVL(MF_AMES1,space(2))
        .w_MFAN1 = NVL(MFAN1,space(4))
        .w_MFIMPSD1 = NVL(MFIMPSD1,0)
        .w_MFIMPSC1 = NVL(MFIMPSC1,0)
        .w_MFCDSED2 = NVL(MFCDSED2,space(5))
          * evitabile
          *.link_3_12('Load')
        .w_MFCCONT2 = NVL(MFCCONT2,space(5))
          * evitabile
          *.link_3_13('Load')
        .w_MFMINPS2 = NVL(MFMINPS2,space(22))
        .w_APP2 = !empty(.w_MFCCONT2) and alltrim(.w_MFCCONT2)$ 'ACON-DCON-DPC-DM10-DMRA-LAAP-PCFP-PCFS-PESC' and ! (alltrim(.w_MFCCONT2)$ 'CFP-AP')
        .w_MFDAMES2 = NVL(MFDAMES2,space(2))
        .w_MFDAANN2 = NVL(MFDAANN2,space(4))
        .w_MF_AMES2 = NVL(MF_AMES2,space(2))
        .w_MFAN2 = NVL(MFAN2,space(4))
        .w_MFIMPSD2 = NVL(MFIMPSD2,0)
        .w_MFIMPSC2 = NVL(MFIMPSC2,0)
        .w_MFCDSED3 = NVL(MFCDSED3,space(5))
          * evitabile
          *.link_3_22('Load')
        .w_MFCCONT3 = NVL(MFCCONT3,space(5))
          * evitabile
          *.link_3_23('Load')
        .w_MFMINPS3 = NVL(MFMINPS3,space(22))
        .w_APP3 = !empty(.w_MFCCONT3) and alltrim(.w_MFCCONT3)$ 'ACON-DCON-DPC-DM10-DMRA-LAAP-PCFP-PCFS-PESC' and ! (alltrim(.w_MFCCONT3)$ 'CFP-AP')
        .w_MFDAMES3 = NVL(MFDAMES3,space(2))
        .w_MFDAANN3 = NVL(MFDAANN3,space(4))
        .w_MF_AMES3 = NVL(MF_AMES3,space(2))
        .w_MFAN3 = NVL(MFAN3,space(4))
        .w_MFIMPSD3 = NVL(MFIMPSD3,0)
        .w_MFIMPSC3 = NVL(MFIMPSC3,0)
        .w_MFCDSED4 = NVL(MFCDSED4,space(5))
          * evitabile
          *.link_3_32('Load')
        .w_MFCCONT4 = NVL(MFCCONT4,space(5))
          * evitabile
          *.link_3_33('Load')
        .w_MFMINPS4 = NVL(MFMINPS4,space(22))
        .w_APP4 = !empty(.w_MFCCONT4) and alltrim(.w_MFCCONT4)$ 'ACON-DCON-DPC-DM10-DMRA-LAAP-PCFP-PCFS-PESC' and ! (alltrim(.w_MFCCONT4)$ 'CFP-AP')
        .w_MFDAMES4 = NVL(MFDAMES4,space(2))
        .w_MFDAANN4 = NVL(MFDAANN4,space(4))
        .w_MF_AMES4 = NVL(MF_AMES4,space(2))
        .w_MFAN4 = NVL(MFAN4,space(4))
        .w_MFIMPSD4 = NVL(MFIMPSD4,0)
        .w_MFIMPSC4 = NVL(MFIMPSC4,0)
        .w_MFTOTDPS = NVL(MFTOTDPS,0)
        .w_MFTOTCPS = NVL(MFTOTCPS,0)
        .w_MFSALDPS = NVL(MFSALDPS,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCODRE1 = NVL(MFCODRE1,space(2))
          * evitabile
          *.link_4_2('Load')
        .w_MFTRIRE1 = NVL(MFTRIRE1,space(5))
          * evitabile
          *.link_4_5('Load')
        .w_MFRATRE1 = NVL(MFRATRE1,space(4))
        .w_MFANNRE1 = NVL(MFANNRE1,space(4))
        .w_MFIMDRE1 = NVL(MFIMDRE1,0)
        .w_MFIMCRE1 = NVL(MFIMCRE1,0)
        .w_MFCODRE2 = NVL(MFCODRE2,space(2))
          * evitabile
          *.link_4_17('Load')
        .w_MFTRIRE2 = NVL(MFTRIRE2,space(5))
          * evitabile
          *.link_4_18('Load')
        .w_MFRATRE2 = NVL(MFRATRE2,space(4))
        .w_MFANNRE2 = NVL(MFANNRE2,space(4))
        .w_MFIMDRE2 = NVL(MFIMDRE2,0)
        .w_MFIMCRE2 = NVL(MFIMCRE2,0)
        .w_MFCODRE3 = NVL(MFCODRE3,space(2))
          * evitabile
          *.link_4_23('Load')
        .w_MFTRIRE3 = NVL(MFTRIRE3,space(5))
          * evitabile
          *.link_4_24('Load')
        .w_MFRATRE3 = NVL(MFRATRE3,space(4))
        .w_MFANNRE3 = NVL(MFANNRE3,space(4))
        .w_MFIMDRE3 = NVL(MFIMDRE3,0)
        .w_MFIMCRE3 = NVL(MFIMCRE3,0)
        .w_MFCODRE4 = NVL(MFCODRE4,space(2))
          * evitabile
          *.link_4_29('Load')
        .w_MFTRIRE4 = NVL(MFTRIRE4,space(5))
          * evitabile
          *.link_4_30('Load')
        .w_MFRATRE4 = NVL(MFRATRE4,space(4))
        .w_MFANNRE4 = NVL(MFANNRE4,space(4))
        .w_MFIMDRE4 = NVL(MFIMDRE4,0)
        .w_MFIMCRE4 = NVL(MFIMCRE4,0)
        .w_MFTOTDRE = NVL(MFTOTDRE,0)
        .w_MFTOTCRE = NVL(MFTOTCRE,0)
        .w_MFSALDRE = NVL(MFSALDRE,0)
        .w_MFCODEL1 = NVL(MFCODEL1,space(2))
          * evitabile
          *.link_4_50('Load')
        .w_MFTRIEL1 = NVL(MFTRIEL1,space(5))
          * evitabile
          *.link_4_51('Load')
        .w_MFRATEL1 = NVL(MFRATEL1,space(4))
        .w_MFANNEL1 = NVL(MFANNEL1,space(4))
        .w_MFIMDEL1 = NVL(MFIMDEL1,0)
        .w_MFIMCEL1 = NVL(MFIMCEL1,0)
        .w_MFCODEL2 = NVL(MFCODEL2,space(2))
          * evitabile
          *.link_4_56('Load')
        .w_MFTRIEL2 = NVL(MFTRIEL2,space(5))
          * evitabile
          *.link_4_57('Load')
        .w_MFRATEL2 = NVL(MFRATEL2,space(4))
        .w_MFANNEL2 = NVL(MFANNEL2,space(4))
        .w_MFIMDEL2 = NVL(MFIMDEL2,0)
        .w_MFIMCEL2 = NVL(MFIMCEL2,0)
        .w_MFCODEL3 = NVL(MFCODEL3,space(2))
          * evitabile
          *.link_4_62('Load')
        .w_MFTRIEL3 = NVL(MFTRIEL3,space(5))
          * evitabile
          *.link_4_63('Load')
        .w_MFRATEL3 = NVL(MFRATEL3,space(4))
        .w_MFANNEL3 = NVL(MFANNEL3,space(4))
        .w_MFIMDEL3 = NVL(MFIMDEL3,0)
        .w_MFIMCEL3 = NVL(MFIMCEL3,0)
        .w_MFTOTDEL = NVL(MFTOTDEL,0)
        .w_MFTOTCEL = NVL(MFTOTCEL,0)
        .w_MFSALDEL = NVL(MFSALDEL,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFSINAI1 = NVL(MFSINAI1,space(5))
          * evitabile
          *.link_5_2('Load')
        .w_MF_NPOS1 = NVL(MF_NPOS1,space(8))
        .w_MF_PACC1 = NVL(MF_PACC1,space(2))
        .w_MF_NRIF1 = NVL(MF_NRIF1,space(7))
        .w_MFCAUSA1 = NVL(MFCAUSA1,space(2))
        .w_MFIMDIL1 = NVL(MFIMDIL1,0)
        .w_MFIMCIL1 = NVL(MFIMCIL1,0)
        .w_MFSINAI2 = NVL(MFSINAI2,space(5))
          * evitabile
          *.link_5_9('Load')
        .w_MF_NPOS2 = NVL(MF_NPOS2,space(8))
        .w_MF_PACC2 = NVL(MF_PACC2,space(2))
        .w_MF_NRIF2 = NVL(MF_NRIF2,space(7))
        .w_MFCAUSA2 = NVL(MFCAUSA2,space(2))
        .w_MFIMDIL2 = NVL(MFIMDIL2,0)
        .w_MFIMCIL2 = NVL(MFIMCIL2,0)
        .w_MFSINAI3 = NVL(MFSINAI3,space(5))
          * evitabile
          *.link_5_16('Load')
        .w_MF_NPOS3 = NVL(MF_NPOS3,space(8))
        .w_MF_PACC3 = NVL(MF_PACC3,space(2))
        .w_MF_NRIF3 = NVL(MF_NRIF3,space(7))
        .w_MFCAUSA3 = NVL(MFCAUSA3,space(2))
        .w_MFIMDIL3 = NVL(MFIMDIL3,0)
        .w_MFIMCIL3 = NVL(MFIMCIL3,0)
        .w_MFTDINAI = NVL(MFTDINAI,0)
        .w_MFTCINAI = NVL(MFTCINAI,0)
        .w_MFSALINA = NVL(MFSALINA,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCDENTE = NVL(MFCDENTE,space(5))
          if link_6_3_joined
            this.w_MFCDENTE = NVL(CPCODICE603,NVL(this.w_MFCDENTE,space(5)))
            this.w_DESAENTE = NVL(CPDESCRI603,space(30))
          else
          .link_6_3('Load')
          endif
        .w_MFSDENT1 = NVL(MFSDENT1,space(5))
          * evitabile
          *.link_6_4('Load')
        .w_MFCCOAE1 = NVL(MFCCOAE1,space(5))
          * evitabile
          *.link_6_5('Load')
        .w_MFCDPOS1 = NVL(MFCDPOS1,space(12))
        .w_MFMSINE1 = NVL(MFMSINE1,space(2))
        .w_MFANINE1 = NVL(MFANINE1,space(4))
        .w_MFMSFIE1 = NVL(MFMSFIE1,space(2))
        .w_MFANF1 = NVL(MFANF1,space(4))
        .w_MFIMDAE1 = NVL(MFIMDAE1,0)
        .w_MFIMCAE1 = NVL(MFIMCAE1,0)
        .w_MFSDENT2 = NVL(MFSDENT2,space(5))
          * evitabile
          *.link_6_13('Load')
        .w_MFCCOAE2 = NVL(MFCCOAE2,space(5))
          * evitabile
          *.link_6_14('Load')
        .w_MFCDPOS2 = NVL(MFCDPOS2,space(12))
        .w_MFMSINE2 = NVL(MFMSINE2,space(2))
        .w_MFANINE2 = NVL(MFANINE2,space(4))
        .w_MFMSFIE2 = NVL(MFMSFIE2,space(2))
        .w_MFANF2 = NVL(MFANF2,space(4))
        .w_MFIMDAE2 = NVL(MFIMDAE2,0)
        .w_MFIMCAE2 = NVL(MFIMCAE2,0)
        .w_MFSDENT3 = NVL(MFSDENT3,space(5))
          * evitabile
          *.link_6_22('Load')
        .w_MFCCOAE3 = NVL(MFCCOAE3,space(5))
          * evitabile
          *.link_6_23('Load')
        .w_MFCDPOS3 = NVL(MFCDPOS3,space(12))
        .w_MFMSINE3 = NVL(MFMSINE3,space(2))
        .w_MFANINE3 = NVL(MFANINE3,space(4))
        .w_MFMSFIE3 = NVL(MFMSFIE3,space(2))
        .w_MFANF3 = NVL(MFANF3,space(4))
        .w_MFIMDAE3 = NVL(MFIMDAE3,0)
        .w_MFIMCAE3 = NVL(MFIMCAE3,0)
        .w_MFTDAENT = NVL(MFTDAENT,0)
        .w_MFTCAENT = NVL(MFTCAENT,0)
        .w_MFSALAEN = NVL(MFSALAEN,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFSALFIN = NVL(MFSALFIN,0)
        .oPgFrm.Page7.oPag.oObj_7_8.Calculate()
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'MOD_PAG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page7.oPag.oBtn_7_14.enabled = this.oPgFrm.Page7.oPag.oBtn_7_14.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_amf
    if this.GSCG_AVF.w_VFDTPRES > cp_CharToDate('31-12-2001')
      this.BlankRec()
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MFSERIAL = space(10)
      .w_MFMESRIF = space(2)
      .w_MFANNRIF = space(4)
      .w_MFVALUTA = space(3)
      .w_APPCOIN = space(1)
      .w_MF_COINC = space(1)
      .w_MFSERIAL = space(10)
      .w_APPOIMP = 0
      .w_MFCODUFF = space(3)
      .w_MFCODATT = space(11)
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_MFSERIAL = space(10)
      .w_MFCDSED1 = space(5)
      .w_MFCCONT1 = space(5)
      .w_MFMINPS1 = space(22)
      .w_APP1 = .f.
      .w_MFDAMES1 = space(2)
      .w_MFDAANN1 = space(4)
      .w_MF_AMES1 = space(2)
      .w_MFAN1 = space(4)
      .w_MFIMPSD1 = 0
      .w_MFIMPSC1 = 0
      .w_MFCDSED2 = space(5)
      .w_MFCCONT2 = space(5)
      .w_MFMINPS2 = space(22)
      .w_APP2 = .f.
      .w_MFDAMES2 = space(2)
      .w_MFDAANN2 = space(4)
      .w_MF_AMES2 = space(2)
      .w_MFAN2 = space(4)
      .w_MFIMPSD2 = 0
      .w_MFIMPSC2 = 0
      .w_MFCDSED3 = space(5)
      .w_MFCCONT3 = space(5)
      .w_MFMINPS3 = space(22)
      .w_APP3 = .f.
      .w_MFDAMES3 = space(2)
      .w_MFDAANN3 = space(4)
      .w_MF_AMES3 = space(2)
      .w_MFAN3 = space(4)
      .w_MFIMPSD3 = 0
      .w_MFIMPSC3 = 0
      .w_MFCDSED4 = space(5)
      .w_MFCCONT4 = space(5)
      .w_MFMINPS4 = space(22)
      .w_APP4 = .f.
      .w_MFDAMES4 = space(2)
      .w_MFDAANN4 = space(4)
      .w_MF_AMES4 = space(2)
      .w_MFAN4 = space(4)
      .w_MFIMPSD4 = 0
      .w_MFIMPSC4 = 0
      .w_MFTOTDPS = 0
      .w_MFTOTCPS = 0
      .w_MFSALDPS = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_MFSERIAL = space(10)
      .w_MFCODRE1 = space(2)
      .w_MFTRIRE1 = space(5)
      .w_MFRATRE1 = space(4)
      .w_MFANNRE1 = space(4)
      .w_MFIMDRE1 = 0
      .w_MFIMCRE1 = 0
      .w_MFCODRE2 = space(2)
      .w_MFTRIRE2 = space(5)
      .w_MFRATRE2 = space(4)
      .w_MFANNRE2 = space(4)
      .w_MFIMDRE2 = 0
      .w_MFIMCRE2 = 0
      .w_MFCODRE3 = space(2)
      .w_MFTRIRE3 = space(5)
      .w_MFRATRE3 = space(4)
      .w_MFANNRE3 = space(4)
      .w_MFIMDRE3 = 0
      .w_MFIMCRE3 = 0
      .w_MFCODRE4 = space(2)
      .w_MFTRIRE4 = space(5)
      .w_MFRATRE4 = space(4)
      .w_MFANNRE4 = space(4)
      .w_MFIMDRE4 = 0
      .w_MFIMCRE4 = 0
      .w_MFTOTDRE = 0
      .w_MFTOTCRE = 0
      .w_MFSALDRE = 0
      .w_MFCODEL1 = space(2)
      .w_MFTRIEL1 = space(5)
      .w_MFRATEL1 = space(4)
      .w_MFANNEL1 = space(4)
      .w_MFIMDEL1 = 0
      .w_MFIMCEL1 = 0
      .w_MFCODEL2 = space(2)
      .w_MFTRIEL2 = space(5)
      .w_MFRATEL2 = space(4)
      .w_MFANNEL2 = space(4)
      .w_MFIMDEL2 = 0
      .w_MFIMCEL2 = 0
      .w_MFCODEL3 = space(2)
      .w_MFTRIEL3 = space(5)
      .w_MFRATEL3 = space(4)
      .w_MFANNEL3 = space(4)
      .w_MFIMDEL3 = 0
      .w_MFIMCEL3 = 0
      .w_MFTOTDEL = 0
      .w_MFTOTCEL = 0
      .w_MFSALDEL = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_MFSERIAL = space(10)
      .w_MFSINAI1 = space(5)
      .w_MF_NPOS1 = space(8)
      .w_MF_PACC1 = space(2)
      .w_MF_NRIF1 = space(7)
      .w_MFCAUSA1 = space(2)
      .w_MFIMDIL1 = 0
      .w_MFIMCIL1 = 0
      .w_MFSINAI2 = space(5)
      .w_MF_NPOS2 = space(8)
      .w_MF_PACC2 = space(2)
      .w_MF_NRIF2 = space(7)
      .w_MFCAUSA2 = space(2)
      .w_MFIMDIL2 = 0
      .w_MFIMCIL2 = 0
      .w_MFSINAI3 = space(5)
      .w_MF_NPOS3 = space(8)
      .w_MF_PACC3 = space(2)
      .w_MF_NRIF3 = space(7)
      .w_MFCAUSA3 = space(2)
      .w_MFIMDIL3 = 0
      .w_MFIMCIL3 = 0
      .w_MFTDINAI = 0
      .w_MFTCINAI = 0
      .w_MFSALINA = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_MFSERIAL = space(10)
      .w_MFCDENTE = space(5)
      .w_MFSDENT1 = space(5)
      .w_MFCCOAE1 = space(5)
      .w_MFCDPOS1 = space(12)
      .w_MFMSINE1 = space(2)
      .w_MFANINE1 = space(4)
      .w_MFMSFIE1 = space(2)
      .w_MFANF1 = space(4)
      .w_MFIMDAE1 = 0
      .w_MFIMCAE1 = 0
      .w_MFSDENT2 = space(5)
      .w_MFCCOAE2 = space(5)
      .w_MFCDPOS2 = space(12)
      .w_MFMSINE2 = space(2)
      .w_MFANINE2 = space(4)
      .w_MFMSFIE2 = space(2)
      .w_MFANF2 = space(4)
      .w_MFIMDAE2 = 0
      .w_MFIMCAE2 = 0
      .w_MFSDENT3 = space(5)
      .w_MFCCOAE3 = space(5)
      .w_MFCDPOS3 = space(12)
      .w_MFMSINE3 = space(2)
      .w_MFANINE3 = space(4)
      .w_MFMSFIE3 = space(2)
      .w_MFANF3 = space(4)
      .w_MFIMDAE3 = 0
      .w_MFIMCAE3 = 0
      .w_MFTDAENT = 0
      .w_MFTCAENT = 0
      .w_MFSALAEN = 0
      .w_DESAENTE = space(30)
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_MFSERIAL = space(10)
      .w_MFSALFIN = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_TIPOMOD = space(3)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_MFMESRIF = ALLTRIM(STR(MONTH(i_datsys)))
        .w_MFANNRIF = ALLTRIM(STR(YEAR(i_datsys)))
        .w_MFVALUTA = g_PERVAL
          .DoRTCalc(5,5,.f.)
        .w_MF_COINC = .w_APPCOIN
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .DoRTCalc(7,9,.f.)
          if not(empty(.w_MFCODUFF))
          .link_2_4('Full')
          endif
          .DoRTCalc(10,10,.f.)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .DoRTCalc(13,14,.f.)
          if not(empty(.w_MFCDSED1))
          .link_3_2('Full')
          endif
        .w_MFCCONT1 = iif(empty(.w_MFCDSED1),' ',.w_MFCCONT1)
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_MFCCONT1))
          .link_3_3('Full')
          endif
        .w_MFMINPS1 = iif(empty(.w_MFCDSED1),' ',.w_MFMINPS1)
        .w_APP1 = !empty(.w_MFCCONT1) and alltrim(.w_MFCCONT1)$ 'ACON-DCON-DPC-DM10-DMRA-LAAP-PCFP-PCFS-PESC' and ! (alltrim(.w_MFCCONT1)$ 'CFP-AP')
        .w_MFDAMES1 = iif(empty(.w_MFCDSED1),' ',iif(alltrim(.w_MFCCONT1) $ 'ACON-DCON','0',.w_MFDAMES1))
        .w_MFDAANN1 = iif(empty(.w_MFCDSED1),' ',iif(alltrim(.w_MFCCONT1) $ 'ACON-DCON','0',.w_MFDAANN1))
        .w_MF_AMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,'0',.w_MF_AMES1))
        .w_MFAN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,'0',.w_MFAN1))
        .w_MFIMPSD1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSD1)
        .w_MFIMPSC1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSC1)
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_MFCDSED2))
          .link_3_12('Full')
          endif
        .w_MFCCONT2 = iif(empty(.w_MFCDSED2),' ',.w_MFCCONT2)
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_MFCCONT2))
          .link_3_13('Full')
          endif
        .w_MFMINPS2 = iif(empty(.w_MFCDSED2),' ',.w_MFMINPS2)
        .w_APP2 = !empty(.w_MFCCONT2) and alltrim(.w_MFCCONT2)$ 'ACON-DCON-DPC-DM10-DMRA-LAAP-PCFP-PCFS-PESC' and ! (alltrim(.w_MFCCONT2)$ 'CFP-AP')
        .w_MFDAMES2 = iif(empty(.w_MFCDSED2),' ',iif(alltrim(.w_MFCCONT2) $ 'ACON-DCON','0',.w_MFDAMES2))
        .w_MFDAANN2 = iif(empty(.w_MFCDSED2),' ',iif(alltrim(.w_MFCCONT2) $ 'ACON-DCON','0',.w_MFDAANN2))
        .w_MF_AMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,'0',.w_MF_AMES2))
        .w_MFAN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,'0',.w_MFAN2))
        .w_MFIMPSD2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSD2)
        .w_MFIMPSC2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSC2)
        .DoRTCalc(34,34,.f.)
          if not(empty(.w_MFCDSED3))
          .link_3_22('Full')
          endif
        .w_MFCCONT3 = iif(empty(.w_MFCDSED3),' ',.w_MFCCONT3)
        .DoRTCalc(35,35,.f.)
          if not(empty(.w_MFCCONT3))
          .link_3_23('Full')
          endif
        .w_MFMINPS3 = iif(empty(.w_MFCDSED3),' ',.w_MFMINPS3)
        .w_APP3 = !empty(.w_MFCCONT3) and alltrim(.w_MFCCONT3)$ 'ACON-DCON-DPC-DM10-DMRA-LAAP-PCFP-PCFS-PESC' and ! (alltrim(.w_MFCCONT3)$ 'CFP-AP')
        .w_MFDAMES3 = iif(empty(.w_MFCDSED3),' ',iif(alltrim(.w_MFCCONT3) $ 'ACON-DCON','0',.w_MFDAMES3))
        .w_MFDAANN3 = iif(empty(.w_MFCDSED3),' ',iif(alltrim(.w_MFCCONT3) $ 'ACON-DCON','0',.w_MFDAANN3))
        .w_MF_AMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,'0',.w_MF_AMES3))
        .w_MFAN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,'0',.w_MFAN3))
        .w_MFIMPSD3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSD3)
        .w_MFIMPSC3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSC3)
        .DoRTCalc(44,44,.f.)
          if not(empty(.w_MFCDSED4))
          .link_3_32('Full')
          endif
        .w_MFCCONT4 = iif(empty(.w_MFCDSED4),' ',.w_MFCCONT4)
        .DoRTCalc(45,45,.f.)
          if not(empty(.w_MFCCONT4))
          .link_3_33('Full')
          endif
        .w_MFMINPS4 = iif(empty(.w_MFCDSED4),' ',.w_MFMINPS4)
        .w_APP4 = !empty(.w_MFCCONT4) and alltrim(.w_MFCCONT4)$ 'ACON-DCON-DPC-DM10-DMRA-LAAP-PCFP-PCFS-PESC' and ! (alltrim(.w_MFCCONT4)$ 'CFP-AP')
        .w_MFDAMES4 = iif(empty(.w_MFCDSED4),' ',iif(alltrim(.w_MFCCONT4) $ 'ACON-DCON','0',.w_MFDAMES4))
        .w_MFDAANN4 = iif(empty(.w_MFCDSED4),' ',iif(alltrim(.w_MFCCONT4) $ 'ACON-DCON','0',.w_MFDAANN4))
        .w_MF_AMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,'0',.w_MF_AMES4))
        .w_MFAN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,'0',.w_MFAN4))
        .w_MFIMPSD4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSD4)
        .w_MFIMPSC4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSC4)
        .w_MFTOTDPS = .w_MFIMPSD1+.w_MFIMPSD2+.w_MFIMPSD3+.w_MFIMPSD4
        .w_MFTOTCPS = .w_MFIMPSC1+.w_MFIMPSC2+.w_MFIMPSC3+.w_MFIMPSC4
        .w_MFSALDPS = .w_MFTOTDPS-.w_MFTOTCPS
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .DoRTCalc(59,60,.f.)
          if not(empty(.w_MFCODRE1))
          .link_4_2('Full')
          endif
        .w_MFTRIRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFTRIRE1)
        .DoRTCalc(61,61,.f.)
          if not(empty(.w_MFTRIRE1))
          .link_4_5('Full')
          endif
        .w_MFRATRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFRATRE1)
        .w_MFANNRE1 = iif(empty(.w_MFCODRE1),' ',iif(.w_MFTRIRE1= '3805','0',.w_MFANNRE1))
        .w_MFIMDRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMDRE1)
        .w_MFIMCRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMCRE1)
        .DoRTCalc(66,66,.f.)
          if not(empty(.w_MFCODRE2))
          .link_4_17('Full')
          endif
        .w_MFTRIRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFTRIRE2)
        .DoRTCalc(67,67,.f.)
          if not(empty(.w_MFTRIRE2))
          .link_4_18('Full')
          endif
        .w_MFRATRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFRATRE2)
        .w_MFANNRE2 = iif(empty(.w_MFCODRE2),' ',iif(.w_MFTRIRE2 ='3805','0',.w_MFANNRE2))
        .w_MFIMDRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMDRE2)
        .w_MFIMCRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMCRE2)
        .DoRTCalc(72,72,.f.)
          if not(empty(.w_MFCODRE3))
          .link_4_23('Full')
          endif
        .w_MFTRIRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFTRIRE3)
        .DoRTCalc(73,73,.f.)
          if not(empty(.w_MFTRIRE3))
          .link_4_24('Full')
          endif
        .w_MFRATRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFRATRE3)
        .w_MFANNRE3 = iif(empty(.w_MFCODRE3),' ',iif(.w_MFTRIRE3 = '3805','0',.w_MFANNRE3))
        .w_MFIMDRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMDRE3)
        .w_MFIMCRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMCRE3)
        .DoRTCalc(78,78,.f.)
          if not(empty(.w_MFCODRE4))
          .link_4_29('Full')
          endif
        .w_MFTRIRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFTRIRE4)
        .DoRTCalc(79,79,.f.)
          if not(empty(.w_MFTRIRE4))
          .link_4_30('Full')
          endif
        .w_MFRATRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFRATRE4)
        .w_MFANNRE4 = iif(empty(.w_MFCODRE4),' ',iif( .w_MFTRIRE4 = '3805','0',.w_MFANNRE4))
        .w_MFIMDRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMDRE4)
        .w_MFIMCRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMCRE4)
        .w_MFTOTDRE = .w_MFIMDRE1+.w_MFIMDRE2+.w_MFIMDRE3+.w_MFIMDRE4
        .w_MFTOTCRE = .w_MFIMCRE1+.w_MFIMCRE2+.w_MFIMCRE3+.w_MFIMCRE4
        .w_MFSALDRE = .w_MFTOTDRE-.w_MFTOTCRE
        .DoRTCalc(87,87,.f.)
          if not(empty(.w_MFCODEL1))
          .link_4_50('Full')
          endif
        .w_MFTRIEL1 = iif(empty(.w_MFCODEL1),' ',.w_MFTRIEL1)
        .DoRTCalc(88,88,.f.)
          if not(empty(.w_MFTRIEL1))
          .link_4_51('Full')
          endif
        .w_MFRATEL1 = iif(empty(.w_MFCODEL1),' ',.w_MFRATEL1)
        .w_MFANNEL1 = iif(empty(.w_MFCODEL1),' ',.w_MFANNEL1)
        .w_MFIMDEL1 = iif(empty(.w_MFCODEL1),0,.w_MFIMDEL1)
        .w_MFIMCEL1 = iif(empty(.w_MFCODEL1),0,.w_MFIMCEL1)
        .DoRTCalc(93,93,.f.)
          if not(empty(.w_MFCODEL2))
          .link_4_56('Full')
          endif
        .w_MFTRIEL2 = iif(empty(.w_MFCODEL2),' ',.w_MFTRIEL2)
        .DoRTCalc(94,94,.f.)
          if not(empty(.w_MFTRIEL2))
          .link_4_57('Full')
          endif
        .w_MFRATEL2 = iif(empty(.w_MFCODEL2),' ',.w_MFRATEL2)
        .w_MFANNEL2 = iif(empty(.w_MFCODEL2),' ',.w_MFANNEL2)
        .w_MFIMDEL2 = iif(empty(.w_MFCODEL2),0,.w_MFIMDEL2)
        .w_MFIMCEL2 = iif(empty(.w_MFCODEL2),0,.w_MFIMCEL2)
        .DoRTCalc(99,99,.f.)
          if not(empty(.w_MFCODEL3))
          .link_4_62('Full')
          endif
        .w_MFTRIEL3 = iif(empty(.w_MFCODEL3),' ',.w_MFTRIEL3)
        .DoRTCalc(100,100,.f.)
          if not(empty(.w_MFTRIEL3))
          .link_4_63('Full')
          endif
        .w_MFRATEL3 = iif(empty(.w_MFCODEL3),' ',.w_MFRATEL3)
        .w_MFANNEL3 = iif(empty(.w_MFCODEL3),' ',.w_MFANNEL3)
        .w_MFIMDEL3 = iif(empty(.w_MFCODEL3),0,.w_MFIMDEL3)
        .w_MFIMCEL3 = iif(empty(.w_MFCODEL3),0,.w_MFIMCEL3)
        .w_MFTOTDEL = .w_MFIMDEL1+.w_MFIMDEL2+.w_MFIMDEL3
        .w_MFTOTCEL = .w_MFIMCEL1+.w_MFIMCEL2+.w_MFIMCEL3
        .w_MFSALDEL = .w_MFTOTDEL-.w_MFTOTCEL
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .DoRTCalc(110,111,.f.)
          if not(empty(.w_MFSINAI1))
          .link_5_2('Full')
          endif
        .w_MF_NPOS1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NPOS1)
        .w_MF_PACC1 = iif(empty(.w_MFSINAI1),' ',.w_MF_PACC1)
        .w_MF_NRIF1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NRIF1)
        .w_MFCAUSA1 = iif(empty(.w_MFSINAI1),' ',.w_MFCAUSA1)
        .w_MFIMDIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMDIL1)
        .w_MFIMCIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMCIL1)
        .DoRTCalc(118,118,.f.)
          if not(empty(.w_MFSINAI2))
          .link_5_9('Full')
          endif
        .w_MF_NPOS2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NPOS2)
        .w_MF_PACC2 = iif(empty(.w_MFSINAI2),' ',.w_MF_PACC2)
        .w_MF_NRIF2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NRIF2)
        .w_MFCAUSA2 = iif(empty(.w_MFSINAI2),' ',.w_MFCAUSA2)
        .w_MFIMDIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMDIL2)
        .w_MFIMCIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMCIL2)
        .DoRTCalc(125,125,.f.)
          if not(empty(.w_MFSINAI3))
          .link_5_16('Full')
          endif
        .w_MF_NPOS3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NPOS3)
        .w_MF_PACC3 = iif(empty(.w_MFSINAI3),' ',.w_MF_PACC3)
        .w_MF_NRIF3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NRIF3)
        .w_MFCAUSA3 = iif(empty(.w_MFSINAI3),' ',.w_MFCAUSA3)
        .w_MFIMDIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMDIL3)
        .w_MFIMCIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMCIL3)
        .w_MFTDINAI = .w_MFIMDIL1+.w_MFIMDIL2+.w_MFIMDIL3
        .w_MFTCINAI = .w_MFIMCIL1+.w_MFIMCIL2+.w_MFIMCIL3
        .w_MFSALINA = .w_MFTDINAI-.w_MFTCINAI
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .DoRTCalc(137,138,.f.)
          if not(empty(.w_MFCDENTE))
          .link_6_3('Full')
          endif
        .w_MFSDENT1 = iif(empty(.w_MFCDENTE),' ',.w_MFSDENT1)
        .DoRTCalc(139,139,.f.)
          if not(empty(.w_MFSDENT1))
          .link_6_4('Full')
          endif
        .w_MFCCOAE1 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT1),' ',.w_MFCCOAE1)
        .DoRTCalc(140,140,.f.)
          if not(empty(.w_MFCCOAE1))
          .link_6_5('Full')
          endif
        .w_MFCDPOS1 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT1),' ',.w_MFCDPOS1)
        .w_MFMSINE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR','0',.w_MFMSINE1))
        .w_MFANINE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR','0',.w_MFANINE1))
        .w_MFMSFIE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'CCSP-CCLS-RS-RC-PR','0',.w_MFMSFIE1))
        .w_MFANF1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'CCSP-CCLS-RS-RC-PR','0',.w_MFANF1))
        .w_MFIMDAE1 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT1),0,.w_MFIMDAE1)
        .w_MFIMCAE1 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT1),0,.w_MFIMCAE1)
        .w_MFSDENT2 = iif(empty(.w_MFCDENTE),' ',.w_MFSDENT2)
        .DoRTCalc(148,148,.f.)
          if not(empty(.w_MFSDENT2))
          .link_6_13('Full')
          endif
        .w_MFCCOAE2 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT2),' ',.w_MFCCOAE2)
        .DoRTCalc(149,149,.f.)
          if not(empty(.w_MFCCOAE2))
          .link_6_14('Full')
          endif
        .w_MFCDPOS2 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT2),' ',.w_MFCDPOS2)
        .w_MFMSINE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR','0',.w_MFMSINE2))
        .w_MFANINE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR','0',.w_MFANINE2))
        .w_MFMSFIE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'CCSP-CCLS-RS-RC-PR','0',.w_MFMSFIE2))
        .w_MFANF2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'CCSP-CCLS-RS-RC-PR','0',.w_MFANF2))
        .w_MFIMDAE2 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT2),0,.w_MFIMDAE2)
        .w_MFIMCAE2 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT2),0,.w_MFIMCAE2)
        .w_MFSDENT3 = iif(empty(.w_MFCDENTE),' ',.w_MFSDENT3)
        .DoRTCalc(157,157,.f.)
          if not(empty(.w_MFSDENT3))
          .link_6_22('Full')
          endif
        .w_MFCCOAE3 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT3),' ',.w_MFCCOAE3)
        .DoRTCalc(158,158,.f.)
          if not(empty(.w_MFCCOAE3))
          .link_6_23('Full')
          endif
        .w_MFCDPOS3 = iif(empty(.w_MFCDENTE)  or empty(.w_MFSDENT3),' ',.w_MFCDPOS3)
        .w_MFMSINE3 = iif(empty(.w_MFCCOAE3),' ',iif(alltrim(.w_MFCCOAE3) $ 'RS-RC-PR','0',.w_MFMSINE3))
        .w_MFANINE3 = iif(empty(.w_MFCCOAE3),' ',iif(alltrim(.w_MFCCOAE3) $ 'RS-RC-PR','0',.w_MFANINE3))
        .w_MFMSFIE3 = iif(empty(.w_MFCCOAE3),' ',iif(alltrim(.w_MFCCOAE3) $ 'CCSP-CCLS-RS-RC-PR','0',.w_MFMSFIE3))
        .w_MFANF3 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE3) $ 'CCSP-CCLS-RS-RC-PR','0',.w_MFANF3))
        .w_MFIMDAE3 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT3),0,.w_MFIMDAE3)
        .w_MFIMCAE3 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT3),0,.w_MFIMCAE3)
        .w_MFTDAENT = .w_MFIMDAE1+.w_MFIMDAE2+.w_MFIMDAE3
        .w_MFTCAENT = .w_MFIMCAE1+.w_MFIMCAE2+.w_MFIMCAE3
        .w_MFSALAEN = .w_MFTDAENT-.w_MFTCAENT
          .DoRTCalc(169,169,.f.)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
          .DoRTCalc(172,172,.f.)
        .w_MFSALFIN = .w_APPOIMP + .w_MFSALDPS + .w_MFSALDRE + .w_MFSALDEL + .w_MFSALINA + .w_MFSALAEN
        .oPgFrm.Page7.oPag.oObj_7_8.Calculate()
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .w_TIPOMOD = 'OLD'
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_PAG')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page7.oPag.oBtn_7_14.enabled = this.oPgFrm.Page7.oPag.oBtn_7_14.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"MODEL","i_CODAZI,w_MFSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_MFSERIAL = .w_MFSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMFSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oMFMESRIF_1_2.enabled = i_bVal
      .Page1.oPag.oMFANNRIF_1_3.enabled = i_bVal
      .Page1.oPag.oMFVALUTA_1_4.enabled = i_bVal
      .Page1.oPag.oMF_COINC_1_6.enabled = i_bVal
      .Page2.oPag.oMFCODUFF_2_4.enabled = i_bVal
      .Page2.oPag.oMFCODATT_2_5.enabled = i_bVal
      .Page3.oPag.oMFCDSED1_3_2.enabled = i_bVal
      .Page3.oPag.oMFCCONT1_3_3.enabled = i_bVal
      .Page3.oPag.oMFMINPS1_3_4.enabled = i_bVal
      .Page3.oPag.oMFDAMES1_3_6.enabled = i_bVal
      .Page3.oPag.oMFDAANN1_3_7.enabled = i_bVal
      .Page3.oPag.oMF_AMES1_3_8.enabled = i_bVal
      .Page3.oPag.oMFAN1_3_9.enabled = i_bVal
      .Page3.oPag.oMFIMPSD1_3_10.enabled = i_bVal
      .Page3.oPag.oMFIMPSC1_3_11.enabled = i_bVal
      .Page3.oPag.oMFCDSED2_3_12.enabled = i_bVal
      .Page3.oPag.oMFCCONT2_3_13.enabled = i_bVal
      .Page3.oPag.oMFMINPS2_3_14.enabled = i_bVal
      .Page3.oPag.oMFDAMES2_3_16.enabled = i_bVal
      .Page3.oPag.oMFDAANN2_3_17.enabled = i_bVal
      .Page3.oPag.oMF_AMES2_3_18.enabled = i_bVal
      .Page3.oPag.oMFAN2_3_19.enabled = i_bVal
      .Page3.oPag.oMFIMPSD2_3_20.enabled = i_bVal
      .Page3.oPag.oMFIMPSC2_3_21.enabled = i_bVal
      .Page3.oPag.oMFCDSED3_3_22.enabled = i_bVal
      .Page3.oPag.oMFCCONT3_3_23.enabled = i_bVal
      .Page3.oPag.oMFMINPS3_3_24.enabled = i_bVal
      .Page3.oPag.oMFDAMES3_3_26.enabled = i_bVal
      .Page3.oPag.oMFDAANN3_3_27.enabled = i_bVal
      .Page3.oPag.oMF_AMES3_3_28.enabled = i_bVal
      .Page3.oPag.oMFAN3_3_29.enabled = i_bVal
      .Page3.oPag.oMFIMPSD3_3_30.enabled = i_bVal
      .Page3.oPag.oMFIMPSC3_3_31.enabled = i_bVal
      .Page3.oPag.oMFCDSED4_3_32.enabled = i_bVal
      .Page3.oPag.oMFCCONT4_3_33.enabled = i_bVal
      .Page3.oPag.oMFMINPS4_3_34.enabled = i_bVal
      .Page3.oPag.oMFDAMES4_3_36.enabled = i_bVal
      .Page3.oPag.oMFDAANN4_3_37.enabled = i_bVal
      .Page3.oPag.oMF_AMES4_3_38.enabled = i_bVal
      .Page3.oPag.oMFAN4_3_39.enabled = i_bVal
      .Page3.oPag.oMFIMPSD4_3_40.enabled = i_bVal
      .Page3.oPag.oMFIMPSC4_3_41.enabled = i_bVal
      .Page4.oPag.oMFCODRE1_4_2.enabled = i_bVal
      .Page4.oPag.oMFTRIRE1_4_5.enabled = i_bVal
      .Page4.oPag.oMFRATRE1_4_6.enabled = i_bVal
      .Page4.oPag.oMFANNRE1_4_7.enabled = i_bVal
      .Page4.oPag.oMFIMDRE1_4_8.enabled = i_bVal
      .Page4.oPag.oMFIMCRE1_4_9.enabled = i_bVal
      .Page4.oPag.oMFCODRE2_4_17.enabled = i_bVal
      .Page4.oPag.oMFTRIRE2_4_18.enabled = i_bVal
      .Page4.oPag.oMFRATRE2_4_19.enabled = i_bVal
      .Page4.oPag.oMFANNRE2_4_20.enabled = i_bVal
      .Page4.oPag.oMFIMDRE2_4_21.enabled = i_bVal
      .Page4.oPag.oMFIMCRE2_4_22.enabled = i_bVal
      .Page4.oPag.oMFCODRE3_4_23.enabled = i_bVal
      .Page4.oPag.oMFTRIRE3_4_24.enabled = i_bVal
      .Page4.oPag.oMFRATRE3_4_25.enabled = i_bVal
      .Page4.oPag.oMFANNRE3_4_26.enabled = i_bVal
      .Page4.oPag.oMFIMDRE3_4_27.enabled = i_bVal
      .Page4.oPag.oMFIMCRE3_4_28.enabled = i_bVal
      .Page4.oPag.oMFCODRE4_4_29.enabled = i_bVal
      .Page4.oPag.oMFTRIRE4_4_30.enabled = i_bVal
      .Page4.oPag.oMFRATRE4_4_31.enabled = i_bVal
      .Page4.oPag.oMFANNRE4_4_32.enabled = i_bVal
      .Page4.oPag.oMFIMDRE4_4_33.enabled = i_bVal
      .Page4.oPag.oMFIMCRE4_4_34.enabled = i_bVal
      .Page4.oPag.oMFCODEL1_4_50.enabled = i_bVal
      .Page4.oPag.oMFTRIEL1_4_51.enabled = i_bVal
      .Page4.oPag.oMFRATEL1_4_52.enabled = i_bVal
      .Page4.oPag.oMFANNEL1_4_53.enabled = i_bVal
      .Page4.oPag.oMFIMDEL1_4_54.enabled = i_bVal
      .Page4.oPag.oMFIMCEL1_4_55.enabled = i_bVal
      .Page4.oPag.oMFCODEL2_4_56.enabled = i_bVal
      .Page4.oPag.oMFTRIEL2_4_57.enabled = i_bVal
      .Page4.oPag.oMFRATEL2_4_58.enabled = i_bVal
      .Page4.oPag.oMFANNEL2_4_59.enabled = i_bVal
      .Page4.oPag.oMFIMDEL2_4_60.enabled = i_bVal
      .Page4.oPag.oMFIMCEL2_4_61.enabled = i_bVal
      .Page4.oPag.oMFCODEL3_4_62.enabled = i_bVal
      .Page4.oPag.oMFTRIEL3_4_63.enabled = i_bVal
      .Page4.oPag.oMFRATEL3_4_64.enabled = i_bVal
      .Page4.oPag.oMFANNEL3_4_65.enabled = i_bVal
      .Page4.oPag.oMFIMDEL3_4_66.enabled = i_bVal
      .Page4.oPag.oMFIMCEL3_4_67.enabled = i_bVal
      .Page5.oPag.oMFSINAI1_5_2.enabled = i_bVal
      .Page5.oPag.oMF_NPOS1_5_3.enabled = i_bVal
      .Page5.oPag.oMF_PACC1_5_4.enabled = i_bVal
      .Page5.oPag.oMF_NRIF1_5_5.enabled = i_bVal
      .Page5.oPag.oMFCAUSA1_5_6.enabled = i_bVal
      .Page5.oPag.oMFIMDIL1_5_7.enabled = i_bVal
      .Page5.oPag.oMFIMCIL1_5_8.enabled = i_bVal
      .Page5.oPag.oMFSINAI2_5_9.enabled = i_bVal
      .Page5.oPag.oMF_NPOS2_5_10.enabled = i_bVal
      .Page5.oPag.oMF_PACC2_5_11.enabled = i_bVal
      .Page5.oPag.oMF_NRIF2_5_12.enabled = i_bVal
      .Page5.oPag.oMFCAUSA2_5_13.enabled = i_bVal
      .Page5.oPag.oMFIMDIL2_5_14.enabled = i_bVal
      .Page5.oPag.oMFIMCIL2_5_15.enabled = i_bVal
      .Page5.oPag.oMFSINAI3_5_16.enabled = i_bVal
      .Page5.oPag.oMF_NPOS3_5_17.enabled = i_bVal
      .Page5.oPag.oMF_PACC3_5_18.enabled = i_bVal
      .Page5.oPag.oMF_NRIF3_5_19.enabled = i_bVal
      .Page5.oPag.oMFCAUSA3_5_20.enabled = i_bVal
      .Page5.oPag.oMFIMDIL3_5_21.enabled = i_bVal
      .Page5.oPag.oMFIMCIL3_5_22.enabled = i_bVal
      .Page6.oPag.oMFCDENTE_6_3.enabled = i_bVal
      .Page6.oPag.oMFSDENT1_6_4.enabled = i_bVal
      .Page6.oPag.oMFCCOAE1_6_5.enabled = i_bVal
      .Page6.oPag.oMFCDPOS1_6_6.enabled = i_bVal
      .Page6.oPag.oMFMSINE1_6_7.enabled = i_bVal
      .Page6.oPag.oMFANINE1_6_8.enabled = i_bVal
      .Page6.oPag.oMFMSFIE1_6_9.enabled = i_bVal
      .Page6.oPag.oMFANF1_6_10.enabled = i_bVal
      .Page6.oPag.oMFIMDAE1_6_11.enabled = i_bVal
      .Page6.oPag.oMFIMCAE1_6_12.enabled = i_bVal
      .Page6.oPag.oMFSDENT2_6_13.enabled = i_bVal
      .Page6.oPag.oMFCCOAE2_6_14.enabled = i_bVal
      .Page6.oPag.oMFCDPOS2_6_15.enabled = i_bVal
      .Page6.oPag.oMFMSINE2_6_16.enabled = i_bVal
      .Page6.oPag.oMFANINE2_6_17.enabled = i_bVal
      .Page6.oPag.oMFMSFIE2_6_18.enabled = i_bVal
      .Page6.oPag.oMFANF2_6_19.enabled = i_bVal
      .Page6.oPag.oMFIMDAE2_6_20.enabled = i_bVal
      .Page6.oPag.oMFIMCAE2_6_21.enabled = i_bVal
      .Page6.oPag.oMFSDENT3_6_22.enabled = i_bVal
      .Page6.oPag.oMFCCOAE3_6_23.enabled = i_bVal
      .Page6.oPag.oMFCDPOS3_6_24.enabled = i_bVal
      .Page6.oPag.oMFMSINE3_6_25.enabled = i_bVal
      .Page6.oPag.oMFANINE3_6_26.enabled = i_bVal
      .Page6.oPag.oMFMSFIE3_6_27.enabled = i_bVal
      .Page6.oPag.oMFANF3_6_28.enabled = i_bVal
      .Page6.oPag.oMFIMDAE3_6_29.enabled = i_bVal
      .Page6.oPag.oMFIMCAE3_6_30.enabled = i_bVal
      .Page7.oPag.oBtn_7_13.enabled = i_bVal
      .Page7.oPag.oBtn_7_14.enabled = .Page7.oPag.oBtn_7_14.mCond()
      .Page1.oPag.oObj_1_12.enabled = i_bVal
      .Page7.oPag.oObj_7_8.enabled = i_bVal
      .Page1.oPag.oObj_1_13.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMFSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMFSERIAL_1_1.enabled = .t.
        .Page1.oPag.oMFMESRIF_1_2.enabled = .t.
        .Page1.oPag.oMFANNRIF_1_3.enabled = .t.
      endif
    endwith
    this.GSCG_ACF.SetStatus(i_cOp)
    this.GSCG_AEF.SetStatus(i_cOp)
    this.GSCG_AVF.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'MOD_PAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_ACF.SetChildrenStatus(i_cOp)
  *  this.GSCG_AEF.SetChildrenStatus(i_cOp)
  *  this.GSCG_AVF.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMESRIF,"MFMESRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRIF,"MFANNRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFVALUTA,"MFVALUTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_COINC,"MF_COINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODUFF,"MFCODUFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODATT,"MFCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED1,"MFCDSED1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT1,"MFCCONT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS1,"MFMINPS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES1,"MFDAMES1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN1,"MFDAANN1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES1,"MF_AMES1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN1,"MFAN1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD1,"MFIMPSD1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC1,"MFIMPSC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED2,"MFCDSED2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT2,"MFCCONT2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS2,"MFMINPS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES2,"MFDAMES2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN2,"MFDAANN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES2,"MF_AMES2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN2,"MFAN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD2,"MFIMPSD2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC2,"MFIMPSC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED3,"MFCDSED3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT3,"MFCCONT3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS3,"MFMINPS3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES3,"MFDAMES3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN3,"MFDAANN3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES3,"MF_AMES3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN3,"MFAN3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD3,"MFIMPSD3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC3,"MFIMPSC3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED4,"MFCDSED4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT4,"MFCCONT4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS4,"MFMINPS4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES4,"MFDAMES4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN4,"MFDAANN4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES4,"MF_AMES4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN4,"MFAN4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD4,"MFIMPSD4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC4,"MFIMPSC4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTDPS,"MFTOTDPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTCPS,"MFTOTCPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALDPS,"MFSALDPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE1,"MFCODRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE1,"MFTRIRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE1,"MFRATRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE1,"MFANNRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE1,"MFIMDRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE1,"MFIMCRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE2,"MFCODRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE2,"MFTRIRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE2,"MFRATRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE2,"MFANNRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE2,"MFIMDRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE2,"MFIMCRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE3,"MFCODRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE3,"MFTRIRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE3,"MFRATRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE3,"MFANNRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE3,"MFIMDRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE3,"MFIMCRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE4,"MFCODRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE4,"MFTRIRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE4,"MFRATRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE4,"MFANNRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE4,"MFIMDRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE4,"MFIMCRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTDRE,"MFTOTDRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTCRE,"MFTOTCRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALDRE,"MFSALDRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODEL1,"MFCODEL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIEL1,"MFTRIEL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATEL1,"MFRATEL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNEL1,"MFANNEL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDEL1,"MFIMDEL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCEL1,"MFIMCEL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODEL2,"MFCODEL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIEL2,"MFTRIEL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATEL2,"MFRATEL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNEL2,"MFANNEL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDEL2,"MFIMDEL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCEL2,"MFIMCEL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODEL3,"MFCODEL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIEL3,"MFTRIEL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATEL3,"MFRATEL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNEL3,"MFANNEL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDEL3,"MFIMDEL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCEL3,"MFIMCEL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTDEL,"MFTOTDEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTCEL,"MFTOTCEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALDEL,"MFSALDEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSINAI1,"MFSINAI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NPOS1,"MF_NPOS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PACC1,"MF_PACC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NRIF1,"MF_NRIF1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUSA1,"MFCAUSA1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDIL1,"MFIMDIL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCIL1,"MFIMCIL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSINAI2,"MFSINAI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NPOS2,"MF_NPOS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PACC2,"MF_PACC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NRIF2,"MF_NRIF2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUSA2,"MFCAUSA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDIL2,"MFIMDIL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCIL2,"MFIMCIL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSINAI3,"MFSINAI3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NPOS3,"MF_NPOS3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PACC3,"MF_PACC3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NRIF3,"MF_NRIF3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUSA3,"MFCAUSA3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDIL3,"MFIMDIL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCIL3,"MFIMCIL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTDINAI,"MFTDINAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTCINAI,"MFTCINAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALINA,"MFSALINA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDENTE,"MFCDENTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSDENT1,"MFSDENT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCOAE1,"MFCCOAE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDPOS1,"MFCDPOS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSINE1,"MFMSINE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANINE1,"MFANINE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSFIE1,"MFMSFIE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANF1,"MFANF1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDAE1,"MFIMDAE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCAE1,"MFIMCAE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSDENT2,"MFSDENT2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCOAE2,"MFCCOAE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDPOS2,"MFCDPOS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSINE2,"MFMSINE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANINE2,"MFANINE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSFIE2,"MFMSFIE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANF2,"MFANF2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDAE2,"MFIMDAE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCAE2,"MFIMCAE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSDENT3,"MFSDENT3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCOAE3,"MFCCOAE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDPOS3,"MFCDPOS3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSINE3,"MFMSINE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANINE3,"MFANINE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSFIE3,"MFMSFIE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANF3,"MFANF3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDAE3,"MFIMDAE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCAE3,"MFIMCAE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTDAENT,"MFTDAENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTCAENT,"MFTCAENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALAEN,"MFSALAEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALFIN,"MFSALFIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    i_lTable = "MOD_PAG"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOD_PAG_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        do GSCG_BMF with this
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOD_PAG_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"MODEL","i_CODAZI,w_MFSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOD_PAG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_PAG')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_PAG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MFSERIAL,MFMESRIF,MFANNRIF,MFVALUTA,MF_COINC"+;
                  ",MFCODUFF,MFCODATT,MFCDSED1,MFCCONT1,MFMINPS1"+;
                  ",MFDAMES1,MFDAANN1,MF_AMES1,MFAN1,MFIMPSD1"+;
                  ",MFIMPSC1,MFCDSED2,MFCCONT2,MFMINPS2,MFDAMES2"+;
                  ",MFDAANN2,MF_AMES2,MFAN2,MFIMPSD2,MFIMPSC2"+;
                  ",MFCDSED3,MFCCONT3,MFMINPS3,MFDAMES3,MFDAANN3"+;
                  ",MF_AMES3,MFAN3,MFIMPSD3,MFIMPSC3,MFCDSED4"+;
                  ",MFCCONT4,MFMINPS4,MFDAMES4,MFDAANN4,MF_AMES4"+;
                  ",MFAN4,MFIMPSD4,MFIMPSC4,MFTOTDPS,MFTOTCPS"+;
                  ",MFSALDPS,MFCODRE1,MFTRIRE1,MFRATRE1,MFANNRE1"+;
                  ",MFIMDRE1,MFIMCRE1,MFCODRE2,MFTRIRE2,MFRATRE2"+;
                  ",MFANNRE2,MFIMDRE2,MFIMCRE2,MFCODRE3,MFTRIRE3"+;
                  ",MFRATRE3,MFANNRE3,MFIMDRE3,MFIMCRE3,MFCODRE4"+;
                  ",MFTRIRE4,MFRATRE4,MFANNRE4,MFIMDRE4,MFIMCRE4"+;
                  ",MFTOTDRE,MFTOTCRE,MFSALDRE,MFCODEL1,MFTRIEL1"+;
                  ",MFRATEL1,MFANNEL1,MFIMDEL1,MFIMCEL1,MFCODEL2"+;
                  ",MFTRIEL2,MFRATEL2,MFANNEL2,MFIMDEL2,MFIMCEL2"+;
                  ",MFCODEL3,MFTRIEL3,MFRATEL3,MFANNEL3,MFIMDEL3"+;
                  ",MFIMCEL3,MFTOTDEL,MFTOTCEL,MFSALDEL,MFSINAI1"+;
                  ",MF_NPOS1,MF_PACC1,MF_NRIF1,MFCAUSA1,MFIMDIL1"+;
                  ",MFIMCIL1,MFSINAI2,MF_NPOS2,MF_PACC2,MF_NRIF2"+;
                  ",MFCAUSA2,MFIMDIL2,MFIMCIL2,MFSINAI3,MF_NPOS3"+;
                  ",MF_PACC3,MF_NRIF3,MFCAUSA3,MFIMDIL3,MFIMCIL3"+;
                  ",MFTDINAI,MFTCINAI,MFSALINA,MFCDENTE,MFSDENT1"+;
                  ",MFCCOAE1,MFCDPOS1,MFMSINE1,MFANINE1,MFMSFIE1"+;
                  ",MFANF1,MFIMDAE1,MFIMCAE1,MFSDENT2,MFCCOAE2"+;
                  ",MFCDPOS2,MFMSINE2,MFANINE2,MFMSFIE2,MFANF2"+;
                  ",MFIMDAE2,MFIMCAE2,MFSDENT3,MFCCOAE3,MFCDPOS3"+;
                  ",MFMSINE3,MFANINE3,MFMSFIE3,MFANF3,MFIMDAE3"+;
                  ",MFIMCAE3,MFTDAENT,MFTCAENT,MFSALAEN,MFSALFIN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MFSERIAL)+;
                  ","+cp_ToStrODBC(this.w_MFMESRIF)+;
                  ","+cp_ToStrODBC(this.w_MFANNRIF)+;
                  ","+cp_ToStrODBC(this.w_MFVALUTA)+;
                  ","+cp_ToStrODBC(this.w_MF_COINC)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODUFF)+;
                  ","+cp_ToStrODBC(this.w_MFCODATT)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT1)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS1)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES1)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN1)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES1)+;
                  ","+cp_ToStrODBC(this.w_MFAN1)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD1)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT2)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS2)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES2)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN2)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES2)+;
                  ","+cp_ToStrODBC(this.w_MFAN2)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD2)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT3)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS3)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES3)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN3)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES3)+;
                  ","+cp_ToStrODBC(this.w_MFAN3)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD3)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED4)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT4)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS4)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES4)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN4)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES4)+;
                  ","+cp_ToStrODBC(this.w_MFAN4)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD4)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC4)+;
                  ","+cp_ToStrODBC(this.w_MFTOTDPS)+;
                  ","+cp_ToStrODBC(this.w_MFTOTCPS)+;
                  ","+cp_ToStrODBC(this.w_MFSALDPS)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE1)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE1)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE1)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE1)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE1)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE2)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE2)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE2)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE2)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE2)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE3)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE3)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE3)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE3)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE3)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE4)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE4)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE4)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE4)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE4)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE4)+;
                  ","+cp_ToStrODBC(this.w_MFTOTDRE)+;
                  ","+cp_ToStrODBC(this.w_MFTOTCRE)+;
                  ","+cp_ToStrODBC(this.w_MFSALDRE)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODEL1)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIEL1)+;
                  ","+cp_ToStrODBC(this.w_MFRATEL1)+;
                  ","+cp_ToStrODBC(this.w_MFANNEL1)+;
                  ","+cp_ToStrODBC(this.w_MFIMDEL1)+;
                  ","+cp_ToStrODBC(this.w_MFIMCEL1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODEL2)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIEL2)+;
                  ","+cp_ToStrODBC(this.w_MFRATEL2)+;
                  ","+cp_ToStrODBC(this.w_MFANNEL2)+;
                  ","+cp_ToStrODBC(this.w_MFIMDEL2)+;
                  ","+cp_ToStrODBC(this.w_MFIMCEL2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODEL3)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIEL3)+;
                  ","+cp_ToStrODBC(this.w_MFRATEL3)+;
                  ","+cp_ToStrODBC(this.w_MFANNEL3)+;
                  ","+cp_ToStrODBC(this.w_MFIMDEL3)+;
                  ","+cp_ToStrODBC(this.w_MFIMCEL3)+;
                  ","+cp_ToStrODBC(this.w_MFTOTDEL)+;
                  ","+cp_ToStrODBC(this.w_MFTOTCEL)+;
                  ","+cp_ToStrODBC(this.w_MFSALDEL)+;
                  ","+cp_ToStrODBCNull(this.w_MFSINAI1)+;
                  ","+cp_ToStrODBC(this.w_MF_NPOS1)+;
                  ","+cp_ToStrODBC(this.w_MF_PACC1)+;
                  ","+cp_ToStrODBC(this.w_MF_NRIF1)+;
                  ","+cp_ToStrODBC(this.w_MFCAUSA1)+;
                  ","+cp_ToStrODBC(this.w_MFIMDIL1)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBC(this.w_MFIMCIL1)+;
                  ","+cp_ToStrODBCNull(this.w_MFSINAI2)+;
                  ","+cp_ToStrODBC(this.w_MF_NPOS2)+;
                  ","+cp_ToStrODBC(this.w_MF_PACC2)+;
                  ","+cp_ToStrODBC(this.w_MF_NRIF2)+;
                  ","+cp_ToStrODBC(this.w_MFCAUSA2)+;
                  ","+cp_ToStrODBC(this.w_MFIMDIL2)+;
                  ","+cp_ToStrODBC(this.w_MFIMCIL2)+;
                  ","+cp_ToStrODBCNull(this.w_MFSINAI3)+;
                  ","+cp_ToStrODBC(this.w_MF_NPOS3)+;
                  ","+cp_ToStrODBC(this.w_MF_PACC3)+;
                  ","+cp_ToStrODBC(this.w_MF_NRIF3)+;
                  ","+cp_ToStrODBC(this.w_MFCAUSA3)+;
                  ","+cp_ToStrODBC(this.w_MFIMDIL3)+;
                  ","+cp_ToStrODBC(this.w_MFIMCIL3)+;
                  ","+cp_ToStrODBC(this.w_MFTDINAI)+;
                  ","+cp_ToStrODBC(this.w_MFTCINAI)+;
                  ","+cp_ToStrODBC(this.w_MFSALINA)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDENTE)+;
                  ","+cp_ToStrODBCNull(this.w_MFSDENT1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCOAE1)+;
                  ","+cp_ToStrODBC(this.w_MFCDPOS1)+;
                  ","+cp_ToStrODBC(this.w_MFMSINE1)+;
                  ","+cp_ToStrODBC(this.w_MFANINE1)+;
                  ","+cp_ToStrODBC(this.w_MFMSFIE1)+;
                  ","+cp_ToStrODBC(this.w_MFANF1)+;
                  ","+cp_ToStrODBC(this.w_MFIMDAE1)+;
                  ","+cp_ToStrODBC(this.w_MFIMCAE1)+;
                  ","+cp_ToStrODBCNull(this.w_MFSDENT2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCOAE2)+;
                  ","+cp_ToStrODBC(this.w_MFCDPOS2)+;
                  ","+cp_ToStrODBC(this.w_MFMSINE2)+;
                  ","+cp_ToStrODBC(this.w_MFANINE2)+;
                  ","+cp_ToStrODBC(this.w_MFMSFIE2)+;
                  ","+cp_ToStrODBC(this.w_MFANF2)+;
                  ","+cp_ToStrODBC(this.w_MFIMDAE2)+;
                  ","+cp_ToStrODBC(this.w_MFIMCAE2)+;
                  ","+cp_ToStrODBCNull(this.w_MFSDENT3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCOAE3)+;
                  ","+cp_ToStrODBC(this.w_MFCDPOS3)+;
                  ","+cp_ToStrODBC(this.w_MFMSINE3)+;
                  ","+cp_ToStrODBC(this.w_MFANINE3)+;
                  ","+cp_ToStrODBC(this.w_MFMSFIE3)+;
                  ","+cp_ToStrODBC(this.w_MFANF3)+;
                  ","+cp_ToStrODBC(this.w_MFIMDAE3)+;
                  ","+cp_ToStrODBC(this.w_MFIMCAE3)+;
                  ","+cp_ToStrODBC(this.w_MFTDAENT)+;
                  ","+cp_ToStrODBC(this.w_MFTCAENT)+;
                  ","+cp_ToStrODBC(this.w_MFSALAEN)+;
                  ","+cp_ToStrODBC(this.w_MFSALFIN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_PAG')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_PAG')
        cp_CheckDeletedKey(i_cTable,0,'MFSERIAL',this.w_MFSERIAL)
        INSERT INTO (i_cTable);
              (MFSERIAL,MFMESRIF,MFANNRIF,MFVALUTA,MF_COINC,MFCODUFF,MFCODATT,MFCDSED1,MFCCONT1,MFMINPS1,MFDAMES1,MFDAANN1,MF_AMES1,MFAN1,MFIMPSD1,MFIMPSC1,MFCDSED2,MFCCONT2,MFMINPS2,MFDAMES2,MFDAANN2,MF_AMES2,MFAN2,MFIMPSD2,MFIMPSC2,MFCDSED3,MFCCONT3,MFMINPS3,MFDAMES3,MFDAANN3,MF_AMES3,MFAN3,MFIMPSD3,MFIMPSC3,MFCDSED4,MFCCONT4,MFMINPS4,MFDAMES4,MFDAANN4,MF_AMES4,MFAN4,MFIMPSD4,MFIMPSC4,MFTOTDPS,MFTOTCPS,MFSALDPS,MFCODRE1,MFTRIRE1,MFRATRE1,MFANNRE1,MFIMDRE1,MFIMCRE1,MFCODRE2,MFTRIRE2,MFRATRE2,MFANNRE2,MFIMDRE2,MFIMCRE2,MFCODRE3,MFTRIRE3,MFRATRE3,MFANNRE3,MFIMDRE3,MFIMCRE3,MFCODRE4,MFTRIRE4,MFRATRE4,MFANNRE4,MFIMDRE4,MFIMCRE4,MFTOTDRE,MFTOTCRE,MFSALDRE,MFCODEL1,MFTRIEL1,MFRATEL1,MFANNEL1,MFIMDEL1,MFIMCEL1,MFCODEL2,MFTRIEL2,MFRATEL2,MFANNEL2,MFIMDEL2,MFIMCEL2,MFCODEL3,MFTRIEL3,MFRATEL3,MFANNEL3,MFIMDEL3,MFIMCEL3,MFTOTDEL,MFTOTCEL,MFSALDEL,MFSINAI1,MF_NPOS1,MF_PACC1,MF_NRIF1,MFCAUSA1,MFIMDIL1,MFIMCIL1,MFSINAI2,MF_NPOS2,MF_PACC2,MF_NRIF2,MFCAUSA2,MFIMDIL2,MFIMCIL2,MFSINAI3,MF_NPOS3,MF_PACC3,MF_NRIF3,MFCAUSA3,MFIMDIL3,MFIMCIL3,MFTDINAI,MFTCINAI,MFSALINA,MFCDENTE,MFSDENT1,MFCCOAE1,MFCDPOS1,MFMSINE1,MFANINE1,MFMSFIE1,MFANF1,MFIMDAE1,MFIMCAE1,MFSDENT2,MFCCOAE2,MFCDPOS2,MFMSINE2,MFANINE2,MFMSFIE2,MFANF2,MFIMDAE2,MFIMCAE2,MFSDENT3,MFCCOAE3,MFCDPOS3,MFMSINE3,MFANINE3,MFMSFIE3,MFANF3,MFIMDAE3,MFIMCAE3,MFTDAENT,MFTCAENT,MFSALAEN,MFSALFIN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MFSERIAL;
                  ,this.w_MFMESRIF;
                  ,this.w_MFANNRIF;
                  ,this.w_MFVALUTA;
                  ,this.w_MF_COINC;
                  ,this.w_MFCODUFF;
                  ,this.w_MFCODATT;
                  ,this.w_MFCDSED1;
                  ,this.w_MFCCONT1;
                  ,this.w_MFMINPS1;
                  ,this.w_MFDAMES1;
                  ,this.w_MFDAANN1;
                  ,this.w_MF_AMES1;
                  ,this.w_MFAN1;
                  ,this.w_MFIMPSD1;
                  ,this.w_MFIMPSC1;
                  ,this.w_MFCDSED2;
                  ,this.w_MFCCONT2;
                  ,this.w_MFMINPS2;
                  ,this.w_MFDAMES2;
                  ,this.w_MFDAANN2;
                  ,this.w_MF_AMES2;
                  ,this.w_MFAN2;
                  ,this.w_MFIMPSD2;
                  ,this.w_MFIMPSC2;
                  ,this.w_MFCDSED3;
                  ,this.w_MFCCONT3;
                  ,this.w_MFMINPS3;
                  ,this.w_MFDAMES3;
                  ,this.w_MFDAANN3;
                  ,this.w_MF_AMES3;
                  ,this.w_MFAN3;
                  ,this.w_MFIMPSD3;
                  ,this.w_MFIMPSC3;
                  ,this.w_MFCDSED4;
                  ,this.w_MFCCONT4;
                  ,this.w_MFMINPS4;
                  ,this.w_MFDAMES4;
                  ,this.w_MFDAANN4;
                  ,this.w_MF_AMES4;
                  ,this.w_MFAN4;
                  ,this.w_MFIMPSD4;
                  ,this.w_MFIMPSC4;
                  ,this.w_MFTOTDPS;
                  ,this.w_MFTOTCPS;
                  ,this.w_MFSALDPS;
                  ,this.w_MFCODRE1;
                  ,this.w_MFTRIRE1;
                  ,this.w_MFRATRE1;
                  ,this.w_MFANNRE1;
                  ,this.w_MFIMDRE1;
                  ,this.w_MFIMCRE1;
                  ,this.w_MFCODRE2;
                  ,this.w_MFTRIRE2;
                  ,this.w_MFRATRE2;
                  ,this.w_MFANNRE2;
                  ,this.w_MFIMDRE2;
                  ,this.w_MFIMCRE2;
                  ,this.w_MFCODRE3;
                  ,this.w_MFTRIRE3;
                  ,this.w_MFRATRE3;
                  ,this.w_MFANNRE3;
                  ,this.w_MFIMDRE3;
                  ,this.w_MFIMCRE3;
                  ,this.w_MFCODRE4;
                  ,this.w_MFTRIRE4;
                  ,this.w_MFRATRE4;
                  ,this.w_MFANNRE4;
                  ,this.w_MFIMDRE4;
                  ,this.w_MFIMCRE4;
                  ,this.w_MFTOTDRE;
                  ,this.w_MFTOTCRE;
                  ,this.w_MFSALDRE;
                  ,this.w_MFCODEL1;
                  ,this.w_MFTRIEL1;
                  ,this.w_MFRATEL1;
                  ,this.w_MFANNEL1;
                  ,this.w_MFIMDEL1;
                  ,this.w_MFIMCEL1;
                  ,this.w_MFCODEL2;
                  ,this.w_MFTRIEL2;
                  ,this.w_MFRATEL2;
                  ,this.w_MFANNEL2;
                  ,this.w_MFIMDEL2;
                  ,this.w_MFIMCEL2;
                  ,this.w_MFCODEL3;
                  ,this.w_MFTRIEL3;
                  ,this.w_MFRATEL3;
                  ,this.w_MFANNEL3;
                  ,this.w_MFIMDEL3;
                  ,this.w_MFIMCEL3;
                  ,this.w_MFTOTDEL;
                  ,this.w_MFTOTCEL;
                  ,this.w_MFSALDEL;
                  ,this.w_MFSINAI1;
                  ,this.w_MF_NPOS1;
                  ,this.w_MF_PACC1;
                  ,this.w_MF_NRIF1;
                  ,this.w_MFCAUSA1;
                  ,this.w_MFIMDIL1;
                  ,this.w_MFIMCIL1;
                  ,this.w_MFSINAI2;
                  ,this.w_MF_NPOS2;
                  ,this.w_MF_PACC2;
                  ,this.w_MF_NRIF2;
                  ,this.w_MFCAUSA2;
                  ,this.w_MFIMDIL2;
                  ,this.w_MFIMCIL2;
                  ,this.w_MFSINAI3;
                  ,this.w_MF_NPOS3;
                  ,this.w_MF_PACC3;
                  ,this.w_MF_NRIF3;
                  ,this.w_MFCAUSA3;
                  ,this.w_MFIMDIL3;
                  ,this.w_MFIMCIL3;
                  ,this.w_MFTDINAI;
                  ,this.w_MFTCINAI;
                  ,this.w_MFSALINA;
                  ,this.w_MFCDENTE;
                  ,this.w_MFSDENT1;
                  ,this.w_MFCCOAE1;
                  ,this.w_MFCDPOS1;
                  ,this.w_MFMSINE1;
                  ,this.w_MFANINE1;
                  ,this.w_MFMSFIE1;
                  ,this.w_MFANF1;
                  ,this.w_MFIMDAE1;
                  ,this.w_MFIMCAE1;
                  ,this.w_MFSDENT2;
                  ,this.w_MFCCOAE2;
                  ,this.w_MFCDPOS2;
                  ,this.w_MFMSINE2;
                  ,this.w_MFANINE2;
                  ,this.w_MFMSFIE2;
                  ,this.w_MFANF2;
                  ,this.w_MFIMDAE2;
                  ,this.w_MFIMCAE2;
                  ,this.w_MFSDENT3;
                  ,this.w_MFCCOAE3;
                  ,this.w_MFCDPOS3;
                  ,this.w_MFMSINE3;
                  ,this.w_MFANINE3;
                  ,this.w_MFMSFIE3;
                  ,this.w_MFANF3;
                  ,this.w_MFIMDAE3;
                  ,this.w_MFIMCAE3;
                  ,this.w_MFTDAENT;
                  ,this.w_MFTCAENT;
                  ,this.w_MFSALAEN;
                  ,this.w_MFSALFIN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- gscg_amf
    * Forza aggiornamento del database
    this.bupdated=.t.
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOD_PAG_IDX,i_nConn)
      *
      * update MOD_PAG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_PAG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MFMESRIF="+cp_ToStrODBC(this.w_MFMESRIF)+;
             ",MFANNRIF="+cp_ToStrODBC(this.w_MFANNRIF)+;
             ",MFVALUTA="+cp_ToStrODBC(this.w_MFVALUTA)+;
             ",MF_COINC="+cp_ToStrODBC(this.w_MF_COINC)+;
             ",MFCODUFF="+cp_ToStrODBCNull(this.w_MFCODUFF)+;
             ",MFCODATT="+cp_ToStrODBC(this.w_MFCODATT)+;
             ",MFCDSED1="+cp_ToStrODBCNull(this.w_MFCDSED1)+;
             ",MFCCONT1="+cp_ToStrODBCNull(this.w_MFCCONT1)+;
             ",MFMINPS1="+cp_ToStrODBC(this.w_MFMINPS1)+;
             ",MFDAMES1="+cp_ToStrODBC(this.w_MFDAMES1)+;
             ",MFDAANN1="+cp_ToStrODBC(this.w_MFDAANN1)+;
             ",MF_AMES1="+cp_ToStrODBC(this.w_MF_AMES1)+;
             ",MFAN1="+cp_ToStrODBC(this.w_MFAN1)+;
             ",MFIMPSD1="+cp_ToStrODBC(this.w_MFIMPSD1)+;
             ",MFIMPSC1="+cp_ToStrODBC(this.w_MFIMPSC1)+;
             ",MFCDSED2="+cp_ToStrODBCNull(this.w_MFCDSED2)+;
             ",MFCCONT2="+cp_ToStrODBCNull(this.w_MFCCONT2)+;
             ",MFMINPS2="+cp_ToStrODBC(this.w_MFMINPS2)+;
             ",MFDAMES2="+cp_ToStrODBC(this.w_MFDAMES2)+;
             ",MFDAANN2="+cp_ToStrODBC(this.w_MFDAANN2)+;
             ",MF_AMES2="+cp_ToStrODBC(this.w_MF_AMES2)+;
             ",MFAN2="+cp_ToStrODBC(this.w_MFAN2)+;
             ",MFIMPSD2="+cp_ToStrODBC(this.w_MFIMPSD2)+;
             ",MFIMPSC2="+cp_ToStrODBC(this.w_MFIMPSC2)+;
             ",MFCDSED3="+cp_ToStrODBCNull(this.w_MFCDSED3)+;
             ",MFCCONT3="+cp_ToStrODBCNull(this.w_MFCCONT3)+;
             ",MFMINPS3="+cp_ToStrODBC(this.w_MFMINPS3)+;
             ",MFDAMES3="+cp_ToStrODBC(this.w_MFDAMES3)+;
             ",MFDAANN3="+cp_ToStrODBC(this.w_MFDAANN3)+;
             ",MF_AMES3="+cp_ToStrODBC(this.w_MF_AMES3)+;
             ",MFAN3="+cp_ToStrODBC(this.w_MFAN3)+;
             ",MFIMPSD3="+cp_ToStrODBC(this.w_MFIMPSD3)+;
             ",MFIMPSC3="+cp_ToStrODBC(this.w_MFIMPSC3)+;
             ",MFCDSED4="+cp_ToStrODBCNull(this.w_MFCDSED4)+;
             ",MFCCONT4="+cp_ToStrODBCNull(this.w_MFCCONT4)+;
             ",MFMINPS4="+cp_ToStrODBC(this.w_MFMINPS4)+;
             ",MFDAMES4="+cp_ToStrODBC(this.w_MFDAMES4)+;
             ",MFDAANN4="+cp_ToStrODBC(this.w_MFDAANN4)+;
             ",MF_AMES4="+cp_ToStrODBC(this.w_MF_AMES4)+;
             ",MFAN4="+cp_ToStrODBC(this.w_MFAN4)+;
             ",MFIMPSD4="+cp_ToStrODBC(this.w_MFIMPSD4)+;
             ",MFIMPSC4="+cp_ToStrODBC(this.w_MFIMPSC4)+;
             ",MFTOTDPS="+cp_ToStrODBC(this.w_MFTOTDPS)+;
             ",MFTOTCPS="+cp_ToStrODBC(this.w_MFTOTCPS)+;
             ",MFSALDPS="+cp_ToStrODBC(this.w_MFSALDPS)+;
             ",MFCODRE1="+cp_ToStrODBCNull(this.w_MFCODRE1)+;
             ",MFTRIRE1="+cp_ToStrODBCNull(this.w_MFTRIRE1)+;
             ",MFRATRE1="+cp_ToStrODBC(this.w_MFRATRE1)+;
             ",MFANNRE1="+cp_ToStrODBC(this.w_MFANNRE1)+;
             ",MFIMDRE1="+cp_ToStrODBC(this.w_MFIMDRE1)+;
             ",MFIMCRE1="+cp_ToStrODBC(this.w_MFIMCRE1)+;
             ",MFCODRE2="+cp_ToStrODBCNull(this.w_MFCODRE2)+;
             ",MFTRIRE2="+cp_ToStrODBCNull(this.w_MFTRIRE2)+;
             ",MFRATRE2="+cp_ToStrODBC(this.w_MFRATRE2)+;
             ",MFANNRE2="+cp_ToStrODBC(this.w_MFANNRE2)+;
             ",MFIMDRE2="+cp_ToStrODBC(this.w_MFIMDRE2)+;
             ",MFIMCRE2="+cp_ToStrODBC(this.w_MFIMCRE2)+;
             ",MFCODRE3="+cp_ToStrODBCNull(this.w_MFCODRE3)+;
             ",MFTRIRE3="+cp_ToStrODBCNull(this.w_MFTRIRE3)+;
             ",MFRATRE3="+cp_ToStrODBC(this.w_MFRATRE3)+;
             ",MFANNRE3="+cp_ToStrODBC(this.w_MFANNRE3)+;
             ",MFIMDRE3="+cp_ToStrODBC(this.w_MFIMDRE3)+;
             ",MFIMCRE3="+cp_ToStrODBC(this.w_MFIMCRE3)+;
             ",MFCODRE4="+cp_ToStrODBCNull(this.w_MFCODRE4)+;
             ",MFTRIRE4="+cp_ToStrODBCNull(this.w_MFTRIRE4)+;
             ",MFRATRE4="+cp_ToStrODBC(this.w_MFRATRE4)+;
             ",MFANNRE4="+cp_ToStrODBC(this.w_MFANNRE4)+;
             ",MFIMDRE4="+cp_ToStrODBC(this.w_MFIMDRE4)+;
             ",MFIMCRE4="+cp_ToStrODBC(this.w_MFIMCRE4)+;
             ",MFTOTDRE="+cp_ToStrODBC(this.w_MFTOTDRE)+;
             ",MFTOTCRE="+cp_ToStrODBC(this.w_MFTOTCRE)+;
             ",MFSALDRE="+cp_ToStrODBC(this.w_MFSALDRE)+;
             ",MFCODEL1="+cp_ToStrODBCNull(this.w_MFCODEL1)+;
             ",MFTRIEL1="+cp_ToStrODBCNull(this.w_MFTRIEL1)+;
             ",MFRATEL1="+cp_ToStrODBC(this.w_MFRATEL1)+;
             ",MFANNEL1="+cp_ToStrODBC(this.w_MFANNEL1)+;
             ",MFIMDEL1="+cp_ToStrODBC(this.w_MFIMDEL1)+;
             ",MFIMCEL1="+cp_ToStrODBC(this.w_MFIMCEL1)+;
             ",MFCODEL2="+cp_ToStrODBCNull(this.w_MFCODEL2)+;
             ",MFTRIEL2="+cp_ToStrODBCNull(this.w_MFTRIEL2)+;
             ",MFRATEL2="+cp_ToStrODBC(this.w_MFRATEL2)+;
             ",MFANNEL2="+cp_ToStrODBC(this.w_MFANNEL2)+;
             ",MFIMDEL2="+cp_ToStrODBC(this.w_MFIMDEL2)+;
             ",MFIMCEL2="+cp_ToStrODBC(this.w_MFIMCEL2)+;
             ",MFCODEL3="+cp_ToStrODBCNull(this.w_MFCODEL3)+;
             ",MFTRIEL3="+cp_ToStrODBCNull(this.w_MFTRIEL3)+;
             ",MFRATEL3="+cp_ToStrODBC(this.w_MFRATEL3)+;
             ",MFANNEL3="+cp_ToStrODBC(this.w_MFANNEL3)+;
             ",MFIMDEL3="+cp_ToStrODBC(this.w_MFIMDEL3)+;
             ",MFIMCEL3="+cp_ToStrODBC(this.w_MFIMCEL3)+;
             ",MFTOTDEL="+cp_ToStrODBC(this.w_MFTOTDEL)+;
             ",MFTOTCEL="+cp_ToStrODBC(this.w_MFTOTCEL)+;
             ",MFSALDEL="+cp_ToStrODBC(this.w_MFSALDEL)+;
             ",MFSINAI1="+cp_ToStrODBCNull(this.w_MFSINAI1)+;
             ",MF_NPOS1="+cp_ToStrODBC(this.w_MF_NPOS1)+;
             ",MF_PACC1="+cp_ToStrODBC(this.w_MF_PACC1)+;
             ",MF_NRIF1="+cp_ToStrODBC(this.w_MF_NRIF1)+;
             ",MFCAUSA1="+cp_ToStrODBC(this.w_MFCAUSA1)+;
             ",MFIMDIL1="+cp_ToStrODBC(this.w_MFIMDIL1)+;
             ",MFIMCIL1="+cp_ToStrODBC(this.w_MFIMCIL1)+;
             ""
             i_nnn=i_nnn+;
             ",MFSINAI2="+cp_ToStrODBCNull(this.w_MFSINAI2)+;
             ",MF_NPOS2="+cp_ToStrODBC(this.w_MF_NPOS2)+;
             ",MF_PACC2="+cp_ToStrODBC(this.w_MF_PACC2)+;
             ",MF_NRIF2="+cp_ToStrODBC(this.w_MF_NRIF2)+;
             ",MFCAUSA2="+cp_ToStrODBC(this.w_MFCAUSA2)+;
             ",MFIMDIL2="+cp_ToStrODBC(this.w_MFIMDIL2)+;
             ",MFIMCIL2="+cp_ToStrODBC(this.w_MFIMCIL2)+;
             ",MFSINAI3="+cp_ToStrODBCNull(this.w_MFSINAI3)+;
             ",MF_NPOS3="+cp_ToStrODBC(this.w_MF_NPOS3)+;
             ",MF_PACC3="+cp_ToStrODBC(this.w_MF_PACC3)+;
             ",MF_NRIF3="+cp_ToStrODBC(this.w_MF_NRIF3)+;
             ",MFCAUSA3="+cp_ToStrODBC(this.w_MFCAUSA3)+;
             ",MFIMDIL3="+cp_ToStrODBC(this.w_MFIMDIL3)+;
             ",MFIMCIL3="+cp_ToStrODBC(this.w_MFIMCIL3)+;
             ",MFTDINAI="+cp_ToStrODBC(this.w_MFTDINAI)+;
             ",MFTCINAI="+cp_ToStrODBC(this.w_MFTCINAI)+;
             ",MFSALINA="+cp_ToStrODBC(this.w_MFSALINA)+;
             ",MFCDENTE="+cp_ToStrODBCNull(this.w_MFCDENTE)+;
             ",MFSDENT1="+cp_ToStrODBCNull(this.w_MFSDENT1)+;
             ",MFCCOAE1="+cp_ToStrODBCNull(this.w_MFCCOAE1)+;
             ",MFCDPOS1="+cp_ToStrODBC(this.w_MFCDPOS1)+;
             ",MFMSINE1="+cp_ToStrODBC(this.w_MFMSINE1)+;
             ",MFANINE1="+cp_ToStrODBC(this.w_MFANINE1)+;
             ",MFMSFIE1="+cp_ToStrODBC(this.w_MFMSFIE1)+;
             ",MFANF1="+cp_ToStrODBC(this.w_MFANF1)+;
             ",MFIMDAE1="+cp_ToStrODBC(this.w_MFIMDAE1)+;
             ",MFIMCAE1="+cp_ToStrODBC(this.w_MFIMCAE1)+;
             ",MFSDENT2="+cp_ToStrODBCNull(this.w_MFSDENT2)+;
             ",MFCCOAE2="+cp_ToStrODBCNull(this.w_MFCCOAE2)+;
             ",MFCDPOS2="+cp_ToStrODBC(this.w_MFCDPOS2)+;
             ",MFMSINE2="+cp_ToStrODBC(this.w_MFMSINE2)+;
             ",MFANINE2="+cp_ToStrODBC(this.w_MFANINE2)+;
             ",MFMSFIE2="+cp_ToStrODBC(this.w_MFMSFIE2)+;
             ",MFANF2="+cp_ToStrODBC(this.w_MFANF2)+;
             ",MFIMDAE2="+cp_ToStrODBC(this.w_MFIMDAE2)+;
             ",MFIMCAE2="+cp_ToStrODBC(this.w_MFIMCAE2)+;
             ",MFSDENT3="+cp_ToStrODBCNull(this.w_MFSDENT3)+;
             ",MFCCOAE3="+cp_ToStrODBCNull(this.w_MFCCOAE3)+;
             ",MFCDPOS3="+cp_ToStrODBC(this.w_MFCDPOS3)+;
             ",MFMSINE3="+cp_ToStrODBC(this.w_MFMSINE3)+;
             ",MFANINE3="+cp_ToStrODBC(this.w_MFANINE3)+;
             ",MFMSFIE3="+cp_ToStrODBC(this.w_MFMSFIE3)+;
             ",MFANF3="+cp_ToStrODBC(this.w_MFANF3)+;
             ",MFIMDAE3="+cp_ToStrODBC(this.w_MFIMDAE3)+;
             ",MFIMCAE3="+cp_ToStrODBC(this.w_MFIMCAE3)+;
             ",MFTDAENT="+cp_ToStrODBC(this.w_MFTDAENT)+;
             ",MFTCAENT="+cp_ToStrODBC(this.w_MFTCAENT)+;
             ",MFSALAEN="+cp_ToStrODBC(this.w_MFSALAEN)+;
             ",MFSALFIN="+cp_ToStrODBC(this.w_MFSALFIN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_PAG')
        i_cWhere = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
        UPDATE (i_cTable) SET;
              MFMESRIF=this.w_MFMESRIF;
             ,MFANNRIF=this.w_MFANNRIF;
             ,MFVALUTA=this.w_MFVALUTA;
             ,MF_COINC=this.w_MF_COINC;
             ,MFCODUFF=this.w_MFCODUFF;
             ,MFCODATT=this.w_MFCODATT;
             ,MFCDSED1=this.w_MFCDSED1;
             ,MFCCONT1=this.w_MFCCONT1;
             ,MFMINPS1=this.w_MFMINPS1;
             ,MFDAMES1=this.w_MFDAMES1;
             ,MFDAANN1=this.w_MFDAANN1;
             ,MF_AMES1=this.w_MF_AMES1;
             ,MFAN1=this.w_MFAN1;
             ,MFIMPSD1=this.w_MFIMPSD1;
             ,MFIMPSC1=this.w_MFIMPSC1;
             ,MFCDSED2=this.w_MFCDSED2;
             ,MFCCONT2=this.w_MFCCONT2;
             ,MFMINPS2=this.w_MFMINPS2;
             ,MFDAMES2=this.w_MFDAMES2;
             ,MFDAANN2=this.w_MFDAANN2;
             ,MF_AMES2=this.w_MF_AMES2;
             ,MFAN2=this.w_MFAN2;
             ,MFIMPSD2=this.w_MFIMPSD2;
             ,MFIMPSC2=this.w_MFIMPSC2;
             ,MFCDSED3=this.w_MFCDSED3;
             ,MFCCONT3=this.w_MFCCONT3;
             ,MFMINPS3=this.w_MFMINPS3;
             ,MFDAMES3=this.w_MFDAMES3;
             ,MFDAANN3=this.w_MFDAANN3;
             ,MF_AMES3=this.w_MF_AMES3;
             ,MFAN3=this.w_MFAN3;
             ,MFIMPSD3=this.w_MFIMPSD3;
             ,MFIMPSC3=this.w_MFIMPSC3;
             ,MFCDSED4=this.w_MFCDSED4;
             ,MFCCONT4=this.w_MFCCONT4;
             ,MFMINPS4=this.w_MFMINPS4;
             ,MFDAMES4=this.w_MFDAMES4;
             ,MFDAANN4=this.w_MFDAANN4;
             ,MF_AMES4=this.w_MF_AMES4;
             ,MFAN4=this.w_MFAN4;
             ,MFIMPSD4=this.w_MFIMPSD4;
             ,MFIMPSC4=this.w_MFIMPSC4;
             ,MFTOTDPS=this.w_MFTOTDPS;
             ,MFTOTCPS=this.w_MFTOTCPS;
             ,MFSALDPS=this.w_MFSALDPS;
             ,MFCODRE1=this.w_MFCODRE1;
             ,MFTRIRE1=this.w_MFTRIRE1;
             ,MFRATRE1=this.w_MFRATRE1;
             ,MFANNRE1=this.w_MFANNRE1;
             ,MFIMDRE1=this.w_MFIMDRE1;
             ,MFIMCRE1=this.w_MFIMCRE1;
             ,MFCODRE2=this.w_MFCODRE2;
             ,MFTRIRE2=this.w_MFTRIRE2;
             ,MFRATRE2=this.w_MFRATRE2;
             ,MFANNRE2=this.w_MFANNRE2;
             ,MFIMDRE2=this.w_MFIMDRE2;
             ,MFIMCRE2=this.w_MFIMCRE2;
             ,MFCODRE3=this.w_MFCODRE3;
             ,MFTRIRE3=this.w_MFTRIRE3;
             ,MFRATRE3=this.w_MFRATRE3;
             ,MFANNRE3=this.w_MFANNRE3;
             ,MFIMDRE3=this.w_MFIMDRE3;
             ,MFIMCRE3=this.w_MFIMCRE3;
             ,MFCODRE4=this.w_MFCODRE4;
             ,MFTRIRE4=this.w_MFTRIRE4;
             ,MFRATRE4=this.w_MFRATRE4;
             ,MFANNRE4=this.w_MFANNRE4;
             ,MFIMDRE4=this.w_MFIMDRE4;
             ,MFIMCRE4=this.w_MFIMCRE4;
             ,MFTOTDRE=this.w_MFTOTDRE;
             ,MFTOTCRE=this.w_MFTOTCRE;
             ,MFSALDRE=this.w_MFSALDRE;
             ,MFCODEL1=this.w_MFCODEL1;
             ,MFTRIEL1=this.w_MFTRIEL1;
             ,MFRATEL1=this.w_MFRATEL1;
             ,MFANNEL1=this.w_MFANNEL1;
             ,MFIMDEL1=this.w_MFIMDEL1;
             ,MFIMCEL1=this.w_MFIMCEL1;
             ,MFCODEL2=this.w_MFCODEL2;
             ,MFTRIEL2=this.w_MFTRIEL2;
             ,MFRATEL2=this.w_MFRATEL2;
             ,MFANNEL2=this.w_MFANNEL2;
             ,MFIMDEL2=this.w_MFIMDEL2;
             ,MFIMCEL2=this.w_MFIMCEL2;
             ,MFCODEL3=this.w_MFCODEL3;
             ,MFTRIEL3=this.w_MFTRIEL3;
             ,MFRATEL3=this.w_MFRATEL3;
             ,MFANNEL3=this.w_MFANNEL3;
             ,MFIMDEL3=this.w_MFIMDEL3;
             ,MFIMCEL3=this.w_MFIMCEL3;
             ,MFTOTDEL=this.w_MFTOTDEL;
             ,MFTOTCEL=this.w_MFTOTCEL;
             ,MFSALDEL=this.w_MFSALDEL;
             ,MFSINAI1=this.w_MFSINAI1;
             ,MF_NPOS1=this.w_MF_NPOS1;
             ,MF_PACC1=this.w_MF_PACC1;
             ,MF_NRIF1=this.w_MF_NRIF1;
             ,MFCAUSA1=this.w_MFCAUSA1;
             ,MFIMDIL1=this.w_MFIMDIL1;
             ,MFIMCIL1=this.w_MFIMCIL1;
             ,MFSINAI2=this.w_MFSINAI2;
             ,MF_NPOS2=this.w_MF_NPOS2;
             ,MF_PACC2=this.w_MF_PACC2;
             ,MF_NRIF2=this.w_MF_NRIF2;
             ,MFCAUSA2=this.w_MFCAUSA2;
             ,MFIMDIL2=this.w_MFIMDIL2;
             ,MFIMCIL2=this.w_MFIMCIL2;
             ,MFSINAI3=this.w_MFSINAI3;
             ,MF_NPOS3=this.w_MF_NPOS3;
             ,MF_PACC3=this.w_MF_PACC3;
             ,MF_NRIF3=this.w_MF_NRIF3;
             ,MFCAUSA3=this.w_MFCAUSA3;
             ,MFIMDIL3=this.w_MFIMDIL3;
             ,MFIMCIL3=this.w_MFIMCIL3;
             ,MFTDINAI=this.w_MFTDINAI;
             ,MFTCINAI=this.w_MFTCINAI;
             ,MFSALINA=this.w_MFSALINA;
             ,MFCDENTE=this.w_MFCDENTE;
             ,MFSDENT1=this.w_MFSDENT1;
             ,MFCCOAE1=this.w_MFCCOAE1;
             ,MFCDPOS1=this.w_MFCDPOS1;
             ,MFMSINE1=this.w_MFMSINE1;
             ,MFANINE1=this.w_MFANINE1;
             ,MFMSFIE1=this.w_MFMSFIE1;
             ,MFANF1=this.w_MFANF1;
             ,MFIMDAE1=this.w_MFIMDAE1;
             ,MFIMCAE1=this.w_MFIMCAE1;
             ,MFSDENT2=this.w_MFSDENT2;
             ,MFCCOAE2=this.w_MFCCOAE2;
             ,MFCDPOS2=this.w_MFCDPOS2;
             ,MFMSINE2=this.w_MFMSINE2;
             ,MFANINE2=this.w_MFANINE2;
             ,MFMSFIE2=this.w_MFMSFIE2;
             ,MFANF2=this.w_MFANF2;
             ,MFIMDAE2=this.w_MFIMDAE2;
             ,MFIMCAE2=this.w_MFIMCAE2;
             ,MFSDENT3=this.w_MFSDENT3;
             ,MFCCOAE3=this.w_MFCCOAE3;
             ,MFCDPOS3=this.w_MFCDPOS3;
             ,MFMSINE3=this.w_MFMSINE3;
             ,MFANINE3=this.w_MFANINE3;
             ,MFMSFIE3=this.w_MFMSFIE3;
             ,MFANF3=this.w_MFANF3;
             ,MFIMDAE3=this.w_MFIMDAE3;
             ,MFIMCAE3=this.w_MFIMCAE3;
             ,MFTDAENT=this.w_MFTDAENT;
             ,MFTCAENT=this.w_MFTCAENT;
             ,MFSALAEN=this.w_MFSALAEN;
             ,MFSALFIN=this.w_MFSALFIN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCG_ACF : Saving
      this.GSCG_ACF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"CFSERIAL";
             )
      this.GSCG_ACF.mReplace()
      * --- GSCG_AEF : Saving
      this.GSCG_AEF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"EFSERIAL";
             )
      this.GSCG_AEF.mReplace()
      * --- GSCG_AVF : Saving
      this.GSCG_AVF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"VFSERIAL";
             )
      this.GSCG_AVF.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCG_ACF : Deleting
    this.GSCG_ACF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"CFSERIAL";
           )
    this.GSCG_ACF.mDelete()
    * --- GSCG_AEF : Deleting
    this.GSCG_AEF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"EFSERIAL";
           )
    this.GSCG_AEF.mDelete()
    * --- GSCG_AVF : Deleting
    this.GSCG_AVF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"VFSERIAL";
           )
    this.GSCG_AVF.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOD_PAG_IDX,i_nConn)
      *
      * delete MOD_PAG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_APPCOIN<>.w_APPCOIN
            .w_MF_COINC = .w_APPCOIN
        endif
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .DoRTCalc(7,10,.t.)
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(13,14,.t.)
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFCCONT1 = iif(empty(.w_MFCDSED1),' ',.w_MFCCONT1)
          .link_3_3('Full')
        endif
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFMINPS1 = iif(empty(.w_MFCDSED1),' ',.w_MFMINPS1)
        endif
        if .o_MFCCONT1<>.w_MFCCONT1
            .w_APP1 = !empty(.w_MFCCONT1) and alltrim(.w_MFCCONT1)$ 'ACON-DCON-DPC-DM10-DMRA-LAAP-PCFP-PCFS-PESC' and ! (alltrim(.w_MFCCONT1)$ 'CFP-AP')
        endif
        if .o_MFCCONT1<>.w_MFCCONT1.or. .o_MFCDSED1<>.w_MFCDSED1
            .w_MFDAMES1 = iif(empty(.w_MFCDSED1),' ',iif(alltrim(.w_MFCCONT1) $ 'ACON-DCON','0',.w_MFDAMES1))
        endif
        if .o_MFCCONT1<>.w_MFCCONT1.or. .o_MFCDSED1<>.w_MFCDSED1
            .w_MFDAANN1 = iif(empty(.w_MFCDSED1),' ',iif(alltrim(.w_MFCCONT1) $ 'ACON-DCON','0',.w_MFDAANN1))
        endif
        if .o_MFCDSED1<>.w_MFCDSED1.or. .o_APP1<>.w_APP1
            .w_MF_AMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,'0',.w_MF_AMES1))
        endif
        if .o_MFCDSED1<>.w_MFCDSED1.or. .o_APP1<>.w_APP1
            .w_MFAN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,'0',.w_MFAN1))
        endif
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFIMPSD1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSD1)
        endif
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFIMPSC1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSC1)
        endif
        .DoRTCalc(24,24,.t.)
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFCCONT2 = iif(empty(.w_MFCDSED2),' ',.w_MFCCONT2)
          .link_3_13('Full')
        endif
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFMINPS2 = iif(empty(.w_MFCDSED2),' ',.w_MFMINPS2)
        endif
        if .o_MFCCONT2<>.w_MFCCONT2
            .w_APP2 = !empty(.w_MFCCONT2) and alltrim(.w_MFCCONT2)$ 'ACON-DCON-DPC-DM10-DMRA-LAAP-PCFP-PCFS-PESC' and ! (alltrim(.w_MFCCONT2)$ 'CFP-AP')
        endif
        if .o_MFCCONT2<>.w_MFCCONT2.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MFDAMES2 = iif(empty(.w_MFCDSED2),' ',iif(alltrim(.w_MFCCONT2) $ 'ACON-DCON','0',.w_MFDAMES2))
        endif
        if .o_MFCCONT2<>.w_MFCCONT2.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MFDAANN2 = iif(empty(.w_MFCDSED2),' ',iif(alltrim(.w_MFCCONT2) $ 'ACON-DCON','0',.w_MFDAANN2))
        endif
        if .o_APP2<>.w_APP2.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MF_AMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,'0',.w_MF_AMES2))
        endif
        if .o_APP2<>.w_APP2.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MFAN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,'0',.w_MFAN2))
        endif
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFIMPSD2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSD2)
        endif
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFIMPSC2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSC2)
        endif
        .DoRTCalc(34,34,.t.)
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFCCONT3 = iif(empty(.w_MFCDSED3),' ',.w_MFCCONT3)
          .link_3_23('Full')
        endif
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFMINPS3 = iif(empty(.w_MFCDSED3),' ',.w_MFMINPS3)
        endif
        if .o_MFCCONT3<>.w_MFCCONT3
            .w_APP3 = !empty(.w_MFCCONT3) and alltrim(.w_MFCCONT3)$ 'ACON-DCON-DPC-DM10-DMRA-LAAP-PCFP-PCFS-PESC' and ! (alltrim(.w_MFCCONT3)$ 'CFP-AP')
        endif
        if .o_MFCCONT3<>.w_MFCCONT3.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MFDAMES3 = iif(empty(.w_MFCDSED3),' ',iif(alltrim(.w_MFCCONT3) $ 'ACON-DCON','0',.w_MFDAMES3))
        endif
        if .o_MFCCONT3<>.w_MFCCONT3.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MFDAANN3 = iif(empty(.w_MFCDSED3),' ',iif(alltrim(.w_MFCCONT3) $ 'ACON-DCON','0',.w_MFDAANN3))
        endif
        if .o_APP3<>.w_APP3.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MF_AMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,'0',.w_MF_AMES3))
        endif
        if .o_APP3<>.w_APP3.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MFAN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,'0',.w_MFAN3))
        endif
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFIMPSD3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSD3)
        endif
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFIMPSC3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSC3)
        endif
        .DoRTCalc(44,44,.t.)
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFCCONT4 = iif(empty(.w_MFCDSED4),' ',.w_MFCCONT4)
          .link_3_33('Full')
        endif
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFMINPS4 = iif(empty(.w_MFCDSED4),' ',.w_MFMINPS4)
        endif
        if .o_MFCCONT4<>.w_MFCCONT4
            .w_APP4 = !empty(.w_MFCCONT4) and alltrim(.w_MFCCONT4)$ 'ACON-DCON-DPC-DM10-DMRA-LAAP-PCFP-PCFS-PESC' and ! (alltrim(.w_MFCCONT4)$ 'CFP-AP')
        endif
        if .o_MFCCONT4<>.w_MFCCONT4.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MFDAMES4 = iif(empty(.w_MFCDSED4),' ',iif(alltrim(.w_MFCCONT4) $ 'ACON-DCON','0',.w_MFDAMES4))
        endif
        if .o_MFCCONT4<>.w_MFCCONT4.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MFDAANN4 = iif(empty(.w_MFCDSED4),' ',iif(alltrim(.w_MFCCONT4) $ 'ACON-DCON','0',.w_MFDAANN4))
        endif
        if .o_APP4<>.w_APP4.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MF_AMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,'0',.w_MF_AMES4))
        endif
        if .o_APP4<>.w_APP4.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MFAN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,'0',.w_MFAN4))
        endif
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFIMPSD4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSD4)
        endif
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFIMPSC4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSC4)
        endif
            .w_MFTOTDPS = .w_MFIMPSD1+.w_MFIMPSD2+.w_MFIMPSD3+.w_MFIMPSD4
            .w_MFTOTCPS = .w_MFIMPSC1+.w_MFIMPSC2+.w_MFIMPSC3+.w_MFIMPSC4
            .w_MFSALDPS = .w_MFTOTDPS-.w_MFTOTCPS
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(59,60,.t.)
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFTRIRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFTRIRE1)
          .link_4_5('Full')
        endif
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFRATRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFRATRE1)
        endif
        if .o_MFCODRE1<>.w_MFCODRE1.or. .o_MFTRIRE1<>.w_MFTRIRE1
            .w_MFANNRE1 = iif(empty(.w_MFCODRE1),' ',iif(.w_MFTRIRE1= '3805','0',.w_MFANNRE1))
        endif
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFIMDRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMDRE1)
        endif
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFIMCRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMCRE1)
        endif
        .DoRTCalc(66,66,.t.)
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFTRIRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFTRIRE2)
          .link_4_18('Full')
        endif
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFRATRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFRATRE2)
        endif
        if .o_MFCODRE2<>.w_MFCODRE2.or. .o_MFTRIRE2<>.w_MFTRIRE2
            .w_MFANNRE2 = iif(empty(.w_MFCODRE2),' ',iif(.w_MFTRIRE2 ='3805','0',.w_MFANNRE2))
        endif
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFIMDRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMDRE2)
        endif
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFIMCRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMCRE2)
        endif
        .DoRTCalc(72,72,.t.)
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFTRIRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFTRIRE3)
          .link_4_24('Full')
        endif
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFRATRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFRATRE3)
        endif
        if .o_MFCODRE3<>.w_MFCODRE3.or. .o_MFTRIRE3<>.w_MFTRIRE3
            .w_MFANNRE3 = iif(empty(.w_MFCODRE3),' ',iif(.w_MFTRIRE3 = '3805','0',.w_MFANNRE3))
        endif
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFIMDRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMDRE3)
        endif
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFIMCRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMCRE3)
        endif
        .DoRTCalc(78,78,.t.)
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFTRIRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFTRIRE4)
          .link_4_30('Full')
        endif
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFRATRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFRATRE4)
        endif
        if .o_MFCODRE4<>.w_MFCODRE4.or. .o_MFTRIRE4<>.w_MFTRIRE4
            .w_MFANNRE4 = iif(empty(.w_MFCODRE4),' ',iif( .w_MFTRIRE4 = '3805','0',.w_MFANNRE4))
        endif
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFIMDRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMDRE4)
        endif
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFIMCRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMCRE4)
        endif
            .w_MFTOTDRE = .w_MFIMDRE1+.w_MFIMDRE2+.w_MFIMDRE3+.w_MFIMDRE4
            .w_MFTOTCRE = .w_MFIMCRE1+.w_MFIMCRE2+.w_MFIMCRE3+.w_MFIMCRE4
            .w_MFSALDRE = .w_MFTOTDRE-.w_MFTOTCRE
        .DoRTCalc(87,87,.t.)
        if .o_MFCODEL1<>.w_MFCODEL1
            .w_MFTRIEL1 = iif(empty(.w_MFCODEL1),' ',.w_MFTRIEL1)
          .link_4_51('Full')
        endif
        if .o_MFCODEL1<>.w_MFCODEL1
            .w_MFRATEL1 = iif(empty(.w_MFCODEL1),' ',.w_MFRATEL1)
        endif
        if .o_MFCODEL1<>.w_MFCODEL1
            .w_MFANNEL1 = iif(empty(.w_MFCODEL1),' ',.w_MFANNEL1)
        endif
        if .o_MFCODEL1<>.w_MFCODEL1
            .w_MFIMDEL1 = iif(empty(.w_MFCODEL1),0,.w_MFIMDEL1)
        endif
        if .o_MFCODEL1<>.w_MFCODEL1
            .w_MFIMCEL1 = iif(empty(.w_MFCODEL1),0,.w_MFIMCEL1)
        endif
        .DoRTCalc(93,93,.t.)
        if .o_MFCODEL2<>.w_MFCODEL2
            .w_MFTRIEL2 = iif(empty(.w_MFCODEL2),' ',.w_MFTRIEL2)
          .link_4_57('Full')
        endif
        if .o_MFCODEL2<>.w_MFCODEL2
            .w_MFRATEL2 = iif(empty(.w_MFCODEL2),' ',.w_MFRATEL2)
        endif
        if .o_MFCODEL2<>.w_MFCODEL2
            .w_MFANNEL2 = iif(empty(.w_MFCODEL2),' ',.w_MFANNEL2)
        endif
        if .o_MFCODEL2<>.w_MFCODEL2
            .w_MFIMDEL2 = iif(empty(.w_MFCODEL2),0,.w_MFIMDEL2)
        endif
        if .o_MFCODEL2<>.w_MFCODEL2
            .w_MFIMCEL2 = iif(empty(.w_MFCODEL2),0,.w_MFIMCEL2)
        endif
        .DoRTCalc(99,99,.t.)
        if .o_MFCODEL3<>.w_MFCODEL3
            .w_MFTRIEL3 = iif(empty(.w_MFCODEL3),' ',.w_MFTRIEL3)
          .link_4_63('Full')
        endif
        if .o_MFCODEL3<>.w_MFCODEL3
            .w_MFRATEL3 = iif(empty(.w_MFCODEL3),' ',.w_MFRATEL3)
        endif
        if .o_MFCODEL3<>.w_MFCODEL3
            .w_MFANNEL3 = iif(empty(.w_MFCODEL3),' ',.w_MFANNEL3)
        endif
        if .o_MFCODEL3<>.w_MFCODEL3
            .w_MFIMDEL3 = iif(empty(.w_MFCODEL3),0,.w_MFIMDEL3)
        endif
        if .o_MFCODEL3<>.w_MFCODEL3
            .w_MFIMCEL3 = iif(empty(.w_MFCODEL3),0,.w_MFIMCEL3)
        endif
            .w_MFTOTDEL = .w_MFIMDEL1+.w_MFIMDEL2+.w_MFIMDEL3
            .w_MFTOTCEL = .w_MFIMCEL1+.w_MFIMCEL2+.w_MFIMCEL3
            .w_MFSALDEL = .w_MFTOTDEL-.w_MFTOTCEL
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(110,111,.t.)
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MF_NPOS1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NPOS1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MF_PACC1 = iif(empty(.w_MFSINAI1),' ',.w_MF_PACC1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MF_NRIF1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NRIF1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MFCAUSA1 = iif(empty(.w_MFSINAI1),' ',.w_MFCAUSA1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MFIMDIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMDIL1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MFIMCIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMCIL1)
        endif
        .DoRTCalc(118,118,.t.)
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MF_NPOS2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NPOS2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MF_PACC2 = iif(empty(.w_MFSINAI2),' ',.w_MF_PACC2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MF_NRIF2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NRIF2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MFCAUSA2 = iif(empty(.w_MFSINAI2),' ',.w_MFCAUSA2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MFIMDIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMDIL2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MFIMCIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMCIL2)
        endif
        .DoRTCalc(125,125,.t.)
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MF_NPOS3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NPOS3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MF_PACC3 = iif(empty(.w_MFSINAI3),' ',.w_MF_PACC3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MF_NRIF3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NRIF3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MFCAUSA3 = iif(empty(.w_MFSINAI3),' ',.w_MFCAUSA3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MFIMDIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMDIL3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MFIMCIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMCIL3)
        endif
            .w_MFTDINAI = .w_MFIMDIL1+.w_MFIMDIL2+.w_MFIMDIL3
            .w_MFTCINAI = .w_MFIMCIL1+.w_MFIMCIL2+.w_MFIMCIL3
            .w_MFSALINA = .w_MFTDINAI-.w_MFTCINAI
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(137,138,.t.)
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_MFSDENT1 = iif(empty(.w_MFCDENTE),' ',.w_MFSDENT1)
          .link_6_4('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_MFSDENT1<>.w_MFSDENT1
            .w_MFCCOAE1 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT1),' ',.w_MFCCOAE1)
          .link_6_5('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_MFSDENT1<>.w_MFSDENT1
            .w_MFCDPOS1 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT1),' ',.w_MFCDPOS1)
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1.or. .o_MFSDENT1<>.w_MFSDENT1
            .w_MFMSINE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR','0',.w_MFMSINE1))
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1.or. .o_MFSDENT1<>.w_MFSDENT1
            .w_MFANINE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR','0',.w_MFANINE1))
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1.or. .o_MFSDENT1<>.w_MFSDENT1
            .w_MFMSFIE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'CCSP-CCLS-RS-RC-PR','0',.w_MFMSFIE1))
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1.or. .o_MFSDENT1<>.w_MFSDENT1
            .w_MFANF1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'CCSP-CCLS-RS-RC-PR','0',.w_MFANF1))
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_MFSDENT1<>.w_MFSDENT1
            .w_MFIMDAE1 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT1),0,.w_MFIMDAE1)
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_MFSDENT1<>.w_MFSDENT1
            .w_MFIMCAE1 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT1),0,.w_MFIMCAE1)
        endif
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_MFSDENT2 = iif(empty(.w_MFCDENTE),' ',.w_MFSDENT2)
          .link_6_13('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_MFSDENT2<>.w_MFSDENT2
            .w_MFCCOAE2 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT2),' ',.w_MFCCOAE2)
          .link_6_14('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_MFSDENT2<>.w_MFSDENT2
            .w_MFCDPOS2 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT2),' ',.w_MFCDPOS2)
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2.or. .o_MFSDENT2<>.w_MFSDENT2
            .w_MFMSINE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR','0',.w_MFMSINE2))
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2.or. .o_MFSDENT2<>.w_MFSDENT2
            .w_MFANINE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR','0',.w_MFANINE2))
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2.or. .o_MFSDENT2<>.w_MFSDENT2
            .w_MFMSFIE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'CCSP-CCLS-RS-RC-PR','0',.w_MFMSFIE2))
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2.or. .o_MFSDENT2<>.w_MFSDENT2
            .w_MFANF2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'CCSP-CCLS-RS-RC-PR','0',.w_MFANF2))
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_MFSDENT2<>.w_MFSDENT2
            .w_MFIMDAE2 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT2),0,.w_MFIMDAE2)
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_MFSDENT2<>.w_MFSDENT2
            .w_MFIMCAE2 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT2),0,.w_MFIMCAE2)
        endif
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_MFSDENT3 = iif(empty(.w_MFCDENTE),' ',.w_MFSDENT3)
          .link_6_22('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_MFSDENT3<>.w_MFSDENT3
            .w_MFCCOAE3 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT3),' ',.w_MFCCOAE3)
          .link_6_23('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_MFSDENT3<>.w_MFSDENT3
            .w_MFCDPOS3 = iif(empty(.w_MFCDENTE)  or empty(.w_MFSDENT3),' ',.w_MFCDPOS3)
        endif
        if .o_MFCCOAE3<>.w_MFCCOAE3.or. .o_MFSDENT3<>.w_MFSDENT3
            .w_MFMSINE3 = iif(empty(.w_MFCCOAE3),' ',iif(alltrim(.w_MFCCOAE3) $ 'RS-RC-PR','0',.w_MFMSINE3))
        endif
        if .o_MFCCOAE3<>.w_MFCCOAE3.or. .o_MFSDENT3<>.w_MFSDENT3
            .w_MFANINE3 = iif(empty(.w_MFCCOAE3),' ',iif(alltrim(.w_MFCCOAE3) $ 'RS-RC-PR','0',.w_MFANINE3))
        endif
        if .o_MFCCOAE3<>.w_MFCCOAE3.or. .o_MFSDENT3<>.w_MFSDENT3
            .w_MFMSFIE3 = iif(empty(.w_MFCCOAE3),' ',iif(alltrim(.w_MFCCOAE3) $ 'CCSP-CCLS-RS-RC-PR','0',.w_MFMSFIE3))
        endif
        if .o_MFCCOAE3<>.w_MFCCOAE3.or. .o_MFSDENT3<>.w_MFSDENT3
            .w_MFANF3 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE3) $ 'CCSP-CCLS-RS-RC-PR','0',.w_MFANF3))
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_MFSDENT3<>.w_MFSDENT3
            .w_MFIMDAE3 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT3),0,.w_MFIMDAE3)
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_MFSDENT3<>.w_MFSDENT3
            .w_MFIMCAE3 = iif(empty(.w_MFCDENTE) or empty(.w_MFSDENT3),0,.w_MFIMCAE3)
        endif
            .w_MFTDAENT = .w_MFIMDAE1+.w_MFIMDAE2+.w_MFIMDAE3
            .w_MFTCAENT = .w_MFIMCAE1+.w_MFIMCAE2+.w_MFIMCAE3
            .w_MFSALAEN = .w_MFTDAENT-.w_MFTCAENT
        .DoRTCalc(169,169,.t.)
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(172,172,.t.)
            .w_MFSALFIN = .w_APPOIMP + .w_MFSALDPS + .w_MFSALDRE + .w_MFSALDEL + .w_MFSALINA + .w_MFSALAEN
        .oPgFrm.Page7.oPag.oObj_7_8.Calculate()
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"MODEL","i_CODAZI,w_MFSERIAL")
          .op_MFSERIAL = .w_MFSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(176,176,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page7.oPag.oObj_7_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.enabled = this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.enabled = this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES1_3_6.enabled = this.oPgFrm.Page3.oPag.oMFDAMES1_3_6.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN1_3_7.enabled = this.oPgFrm.Page3.oPag.oMFDAANN1_3_7.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES1_3_8.enabled = this.oPgFrm.Page3.oPag.oMF_AMES1_3_8.mCond()
    this.oPgFrm.Page3.oPag.oMFAN1_3_9.enabled = this.oPgFrm.Page3.oPag.oMFAN1_3_9.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD1_3_10.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD1_3_10.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC1_3_11.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC1_3_11.mCond()
    this.oPgFrm.Page3.oPag.oMFCCONT2_3_13.enabled = this.oPgFrm.Page3.oPag.oMFCCONT2_3_13.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS2_3_14.enabled = this.oPgFrm.Page3.oPag.oMFMINPS2_3_14.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES2_3_16.enabled = this.oPgFrm.Page3.oPag.oMFDAMES2_3_16.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN2_3_17.enabled = this.oPgFrm.Page3.oPag.oMFDAANN2_3_17.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES2_3_18.enabled = this.oPgFrm.Page3.oPag.oMF_AMES2_3_18.mCond()
    this.oPgFrm.Page3.oPag.oMFAN2_3_19.enabled = this.oPgFrm.Page3.oPag.oMFAN2_3_19.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD2_3_20.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD2_3_20.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC2_3_21.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC2_3_21.mCond()
    this.oPgFrm.Page3.oPag.oMFCCONT3_3_23.enabled = this.oPgFrm.Page3.oPag.oMFCCONT3_3_23.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS3_3_24.enabled = this.oPgFrm.Page3.oPag.oMFMINPS3_3_24.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES3_3_26.enabled = this.oPgFrm.Page3.oPag.oMFDAMES3_3_26.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN3_3_27.enabled = this.oPgFrm.Page3.oPag.oMFDAANN3_3_27.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES3_3_28.enabled = this.oPgFrm.Page3.oPag.oMF_AMES3_3_28.mCond()
    this.oPgFrm.Page3.oPag.oMFAN3_3_29.enabled = this.oPgFrm.Page3.oPag.oMFAN3_3_29.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD3_3_30.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD3_3_30.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC3_3_31.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC3_3_31.mCond()
    this.oPgFrm.Page3.oPag.oMFCCONT4_3_33.enabled = this.oPgFrm.Page3.oPag.oMFCCONT4_3_33.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS4_3_34.enabled = this.oPgFrm.Page3.oPag.oMFMINPS4_3_34.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES4_3_36.enabled = this.oPgFrm.Page3.oPag.oMFDAMES4_3_36.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN4_3_37.enabled = this.oPgFrm.Page3.oPag.oMFDAANN4_3_37.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES4_3_38.enabled = this.oPgFrm.Page3.oPag.oMF_AMES4_3_38.mCond()
    this.oPgFrm.Page3.oPag.oMFAN4_3_39.enabled = this.oPgFrm.Page3.oPag.oMFAN4_3_39.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD4_3_40.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD4_3_40.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC4_3_41.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC4_3_41.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE1_4_5.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE1_4_5.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE1_4_6.enabled = this.oPgFrm.Page4.oPag.oMFRATRE1_4_6.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE1_4_7.enabled = this.oPgFrm.Page4.oPag.oMFANNRE1_4_7.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE1_4_8.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE1_4_8.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE1_4_9.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE1_4_9.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE2_4_18.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE2_4_18.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE2_4_19.enabled = this.oPgFrm.Page4.oPag.oMFRATRE2_4_19.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE2_4_20.enabled = this.oPgFrm.Page4.oPag.oMFANNRE2_4_20.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE2_4_21.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE2_4_21.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE2_4_22.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE2_4_22.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE3_4_24.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE3_4_24.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE3_4_25.enabled = this.oPgFrm.Page4.oPag.oMFRATRE3_4_25.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE3_4_26.enabled = this.oPgFrm.Page4.oPag.oMFANNRE3_4_26.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE3_4_27.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE3_4_27.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE3_4_28.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE3_4_28.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE4_4_30.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE4_4_30.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE4_4_31.enabled = this.oPgFrm.Page4.oPag.oMFRATRE4_4_31.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE4_4_32.enabled = this.oPgFrm.Page4.oPag.oMFANNRE4_4_32.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE4_4_33.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE4_4_33.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE4_4_34.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE4_4_34.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIEL1_4_51.enabled = this.oPgFrm.Page4.oPag.oMFTRIEL1_4_51.mCond()
    this.oPgFrm.Page4.oPag.oMFRATEL1_4_52.enabled = this.oPgFrm.Page4.oPag.oMFRATEL1_4_52.mCond()
    this.oPgFrm.Page4.oPag.oMFANNEL1_4_53.enabled = this.oPgFrm.Page4.oPag.oMFANNEL1_4_53.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDEL1_4_54.enabled = this.oPgFrm.Page4.oPag.oMFIMDEL1_4_54.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCEL1_4_55.enabled = this.oPgFrm.Page4.oPag.oMFIMCEL1_4_55.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIEL2_4_57.enabled = this.oPgFrm.Page4.oPag.oMFTRIEL2_4_57.mCond()
    this.oPgFrm.Page4.oPag.oMFRATEL2_4_58.enabled = this.oPgFrm.Page4.oPag.oMFRATEL2_4_58.mCond()
    this.oPgFrm.Page4.oPag.oMFANNEL2_4_59.enabled = this.oPgFrm.Page4.oPag.oMFANNEL2_4_59.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDEL2_4_60.enabled = this.oPgFrm.Page4.oPag.oMFIMDEL2_4_60.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCEL2_4_61.enabled = this.oPgFrm.Page4.oPag.oMFIMCEL2_4_61.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIEL3_4_63.enabled = this.oPgFrm.Page4.oPag.oMFTRIEL3_4_63.mCond()
    this.oPgFrm.Page4.oPag.oMFRATEL3_4_64.enabled = this.oPgFrm.Page4.oPag.oMFRATEL3_4_64.mCond()
    this.oPgFrm.Page4.oPag.oMFANNEL3_4_65.enabled = this.oPgFrm.Page4.oPag.oMFANNEL3_4_65.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDEL3_4_66.enabled = this.oPgFrm.Page4.oPag.oMFIMDEL3_4_66.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCEL3_4_67.enabled = this.oPgFrm.Page4.oPag.oMFIMCEL3_4_67.mCond()
    this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.enabled = this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.mCond()
    this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.enabled = this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.mCond()
    this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.enabled = this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.mCond()
    this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.enabled = this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.mCond()
    this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.enabled = this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.mCond()
    this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.enabled = this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.mCond()
    this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.enabled = this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.mCond()
    this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.enabled = this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.mCond()
    this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.enabled = this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.mCond()
    this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.enabled = this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.mCond()
    this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.enabled = this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.mCond()
    this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.enabled = this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.mCond()
    this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.enabled = this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.mCond()
    this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.enabled = this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.mCond()
    this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.enabled = this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.mCond()
    this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.enabled = this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.mCond()
    this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.enabled = this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.mCond()
    this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.enabled = this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.mCond()
    this.oPgFrm.Page6.oPag.oMFSDENT1_6_4.enabled = this.oPgFrm.Page6.oPag.oMFSDENT1_6_4.mCond()
    this.oPgFrm.Page6.oPag.oMFCCOAE1_6_5.enabled = this.oPgFrm.Page6.oPag.oMFCCOAE1_6_5.mCond()
    this.oPgFrm.Page6.oPag.oMFCDPOS1_6_6.enabled = this.oPgFrm.Page6.oPag.oMFCDPOS1_6_6.mCond()
    this.oPgFrm.Page6.oPag.oMFMSINE1_6_7.enabled = this.oPgFrm.Page6.oPag.oMFMSINE1_6_7.mCond()
    this.oPgFrm.Page6.oPag.oMFANINE1_6_8.enabled = this.oPgFrm.Page6.oPag.oMFANINE1_6_8.mCond()
    this.oPgFrm.Page6.oPag.oMFMSFIE1_6_9.enabled = this.oPgFrm.Page6.oPag.oMFMSFIE1_6_9.mCond()
    this.oPgFrm.Page6.oPag.oMFANF1_6_10.enabled = this.oPgFrm.Page6.oPag.oMFANF1_6_10.mCond()
    this.oPgFrm.Page6.oPag.oMFIMDAE1_6_11.enabled = this.oPgFrm.Page6.oPag.oMFIMDAE1_6_11.mCond()
    this.oPgFrm.Page6.oPag.oMFIMCAE1_6_12.enabled = this.oPgFrm.Page6.oPag.oMFIMCAE1_6_12.mCond()
    this.oPgFrm.Page6.oPag.oMFSDENT2_6_13.enabled = this.oPgFrm.Page6.oPag.oMFSDENT2_6_13.mCond()
    this.oPgFrm.Page6.oPag.oMFCCOAE2_6_14.enabled = this.oPgFrm.Page6.oPag.oMFCCOAE2_6_14.mCond()
    this.oPgFrm.Page6.oPag.oMFCDPOS2_6_15.enabled = this.oPgFrm.Page6.oPag.oMFCDPOS2_6_15.mCond()
    this.oPgFrm.Page6.oPag.oMFMSINE2_6_16.enabled = this.oPgFrm.Page6.oPag.oMFMSINE2_6_16.mCond()
    this.oPgFrm.Page6.oPag.oMFANINE2_6_17.enabled = this.oPgFrm.Page6.oPag.oMFANINE2_6_17.mCond()
    this.oPgFrm.Page6.oPag.oMFMSFIE2_6_18.enabled = this.oPgFrm.Page6.oPag.oMFMSFIE2_6_18.mCond()
    this.oPgFrm.Page6.oPag.oMFANF2_6_19.enabled = this.oPgFrm.Page6.oPag.oMFANF2_6_19.mCond()
    this.oPgFrm.Page6.oPag.oMFIMDAE2_6_20.enabled = this.oPgFrm.Page6.oPag.oMFIMDAE2_6_20.mCond()
    this.oPgFrm.Page6.oPag.oMFIMCAE2_6_21.enabled = this.oPgFrm.Page6.oPag.oMFIMCAE2_6_21.mCond()
    this.oPgFrm.Page6.oPag.oMFSDENT3_6_22.enabled = this.oPgFrm.Page6.oPag.oMFSDENT3_6_22.mCond()
    this.oPgFrm.Page6.oPag.oMFCCOAE3_6_23.enabled = this.oPgFrm.Page6.oPag.oMFCCOAE3_6_23.mCond()
    this.oPgFrm.Page6.oPag.oMFCDPOS3_6_24.enabled = this.oPgFrm.Page6.oPag.oMFCDPOS3_6_24.mCond()
    this.oPgFrm.Page6.oPag.oMFMSINE3_6_25.enabled = this.oPgFrm.Page6.oPag.oMFMSINE3_6_25.mCond()
    this.oPgFrm.Page6.oPag.oMFANINE3_6_26.enabled = this.oPgFrm.Page6.oPag.oMFANINE3_6_26.mCond()
    this.oPgFrm.Page6.oPag.oMFMSFIE3_6_27.enabled = this.oPgFrm.Page6.oPag.oMFMSFIE3_6_27.mCond()
    this.oPgFrm.Page6.oPag.oMFANF3_6_28.enabled = this.oPgFrm.Page6.oPag.oMFANF3_6_28.mCond()
    this.oPgFrm.Page6.oPag.oMFIMDAE3_6_29.enabled = this.oPgFrm.Page6.oPag.oMFIMDAE3_6_29.mCond()
    this.oPgFrm.Page6.oPag.oMFIMCAE3_6_30.enabled = this.oPgFrm.Page6.oPag.oMFIMCAE3_6_30.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page3.oPag.oStr_3_65.visible=!this.oPgFrm.Page3.oPag.oStr_3_65.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_66.visible=!this.oPgFrm.Page3.oPag.oStr_3_66.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_67.visible=!this.oPgFrm.Page3.oPag.oStr_3_67.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_68.visible=!this.oPgFrm.Page3.oPag.oStr_3_68.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_69.visible=!this.oPgFrm.Page3.oPag.oStr_3_69.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_70.visible=!this.oPgFrm.Page3.oPag.oStr_3_70.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_71.visible=!this.oPgFrm.Page3.oPag.oStr_3_71.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_72.visible=!this.oPgFrm.Page3.oPag.oStr_3_72.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_73.visible=!this.oPgFrm.Page3.oPag.oStr_3_73.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_74.visible=!this.oPgFrm.Page3.oPag.oStr_3_74.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_75.visible=!this.oPgFrm.Page3.oPag.oStr_3_75.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_76.visible=!this.oPgFrm.Page3.oPag.oStr_3_76.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_77.visible=!this.oPgFrm.Page3.oPag.oStr_3_77.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_78.visible=!this.oPgFrm.Page3.oPag.oStr_3_78.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_79.visible=!this.oPgFrm.Page3.oPag.oStr_3_79.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_80.visible=!this.oPgFrm.Page3.oPag.oStr_3_80.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_81.visible=!this.oPgFrm.Page3.oPag.oStr_3_81.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_82.visible=!this.oPgFrm.Page3.oPag.oStr_3_82.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_83.visible=!this.oPgFrm.Page3.oPag.oStr_3_83.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_84.visible=!this.oPgFrm.Page3.oPag.oStr_3_84.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_85.visible=!this.oPgFrm.Page3.oPag.oStr_3_85.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_86.visible=!this.oPgFrm.Page3.oPag.oStr_3_86.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_82.visible=!this.oPgFrm.Page4.oPag.oStr_4_82.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_83.visible=!this.oPgFrm.Page4.oPag.oStr_4_83.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_84.visible=!this.oPgFrm.Page4.oPag.oStr_4_84.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_85.visible=!this.oPgFrm.Page4.oPag.oStr_4_85.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_86.visible=!this.oPgFrm.Page4.oPag.oStr_4_86.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_87.visible=!this.oPgFrm.Page4.oPag.oStr_4_87.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_88.visible=!this.oPgFrm.Page4.oPag.oStr_4_88.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_89.visible=!this.oPgFrm.Page4.oPag.oStr_4_89.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_90.visible=!this.oPgFrm.Page4.oPag.oStr_4_90.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_91.visible=!this.oPgFrm.Page4.oPag.oStr_4_91.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_92.visible=!this.oPgFrm.Page4.oPag.oStr_4_92.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_93.visible=!this.oPgFrm.Page4.oPag.oStr_4_93.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_94.visible=!this.oPgFrm.Page4.oPag.oStr_4_94.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_95.visible=!this.oPgFrm.Page4.oPag.oStr_4_95.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_96.visible=!this.oPgFrm.Page4.oPag.oStr_4_96.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_97.visible=!this.oPgFrm.Page4.oPag.oStr_4_97.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_98.visible=!this.oPgFrm.Page4.oPag.oStr_4_98.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_99.visible=!this.oPgFrm.Page4.oPag.oStr_4_99.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_100.visible=!this.oPgFrm.Page4.oPag.oStr_4_100.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_101.visible=!this.oPgFrm.Page4.oPag.oStr_4_101.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_102.visible=!this.oPgFrm.Page4.oPag.oStr_4_102.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_103.visible=!this.oPgFrm.Page4.oPag.oStr_4_103.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_104.visible=!this.oPgFrm.Page4.oPag.oStr_4_104.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_105.visible=!this.oPgFrm.Page4.oPag.oStr_4_105.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_106.visible=!this.oPgFrm.Page4.oPag.oStr_4_106.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_107.visible=!this.oPgFrm.Page4.oPag.oStr_4_107.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_108.visible=!this.oPgFrm.Page4.oPag.oStr_4_108.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_109.visible=!this.oPgFrm.Page4.oPag.oStr_4_109.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_110.visible=!this.oPgFrm.Page4.oPag.oStr_4_110.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_111.visible=!this.oPgFrm.Page4.oPag.oStr_4_111.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_112.visible=!this.oPgFrm.Page4.oPag.oStr_4_112.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_113.visible=!this.oPgFrm.Page4.oPag.oStr_4_113.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_114.visible=!this.oPgFrm.Page4.oPag.oStr_4_114.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_115.visible=!this.oPgFrm.Page4.oPag.oStr_4_115.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_116.visible=!this.oPgFrm.Page4.oPag.oStr_4_116.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_117.visible=!this.oPgFrm.Page4.oPag.oStr_4_117.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_118.visible=!this.oPgFrm.Page4.oPag.oStr_4_118.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_119.visible=!this.oPgFrm.Page4.oPag.oStr_4_119.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_120.visible=!this.oPgFrm.Page4.oPag.oStr_4_120.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_121.visible=!this.oPgFrm.Page4.oPag.oStr_4_121.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_44.visible=!this.oPgFrm.Page5.oPag.oStr_5_44.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_45.visible=!this.oPgFrm.Page5.oPag.oStr_5_45.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_46.visible=!this.oPgFrm.Page5.oPag.oStr_5_46.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_47.visible=!this.oPgFrm.Page5.oPag.oStr_5_47.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_48.visible=!this.oPgFrm.Page5.oPag.oStr_5_48.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_49.visible=!this.oPgFrm.Page5.oPag.oStr_5_49.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_50.visible=!this.oPgFrm.Page5.oPag.oStr_5_50.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_51.visible=!this.oPgFrm.Page5.oPag.oStr_5_51.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_52.visible=!this.oPgFrm.Page5.oPag.oStr_5_52.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_53.visible=!this.oPgFrm.Page5.oPag.oStr_5_53.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_54.visible=!this.oPgFrm.Page5.oPag.oStr_5_54.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_55.visible=!this.oPgFrm.Page5.oPag.oStr_5_55.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_56.visible=!this.oPgFrm.Page5.oPag.oStr_5_56.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_57.visible=!this.oPgFrm.Page5.oPag.oStr_5_57.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_58.visible=!this.oPgFrm.Page5.oPag.oStr_5_58.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_59.visible=!this.oPgFrm.Page5.oPag.oStr_5_59.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_60.visible=!this.oPgFrm.Page5.oPag.oStr_5_60.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_61.visible=!this.oPgFrm.Page5.oPag.oStr_5_61.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_56.visible=!this.oPgFrm.Page6.oPag.oStr_6_56.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_57.visible=!this.oPgFrm.Page6.oPag.oStr_6_57.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_58.visible=!this.oPgFrm.Page6.oPag.oStr_6_58.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_59.visible=!this.oPgFrm.Page6.oPag.oStr_6_59.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_60.visible=!this.oPgFrm.Page6.oPag.oStr_6_60.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_61.visible=!this.oPgFrm.Page6.oPag.oStr_6_61.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_62.visible=!this.oPgFrm.Page6.oPag.oStr_6_62.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_63.visible=!this.oPgFrm.Page6.oPag.oStr_6_63.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_64.visible=!this.oPgFrm.Page6.oPag.oStr_6_64.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_65.visible=!this.oPgFrm.Page6.oPag.oStr_6_65.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_66.visible=!this.oPgFrm.Page6.oPag.oStr_6_66.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_67.visible=!this.oPgFrm.Page6.oPag.oStr_6_67.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_68.visible=!this.oPgFrm.Page6.oPag.oStr_6_68.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_69.visible=!this.oPgFrm.Page6.oPag.oStr_6_69.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_70.visible=!this.oPgFrm.Page6.oPag.oStr_6_70.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_71.visible=!this.oPgFrm.Page6.oPag.oStr_6_71.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_72.visible=!this.oPgFrm.Page6.oPag.oStr_6_72.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_73.visible=!this.oPgFrm.Page6.oPag.oStr_6_73.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_11.visible=!this.oPgFrm.Page7.oPag.oStr_7_11.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_12.visible=!this.oPgFrm.Page7.oPag.oStr_7_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
      .oPgFrm.Page7.oPag.oObj_7_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MFCODUFF
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CODI_UFF_IDX,3]
    i_lTable = "CODI_UFF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CODI_UFF_IDX,2], .t., this.CODI_UFF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CODI_UFF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODUFF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AUF',True,'CODI_UFF')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UFCODICE like "+cp_ToStrODBC(trim(this.w_MFCODUFF)+"%");

          i_ret=cp_SQL(i_nConn,"select UFCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UFCODICE',trim(this.w_MFCODUFF))
          select UFCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODUFF)==trim(_Link_.UFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODUFF) and !this.bDontReportError
            deferred_cp_zoom('CODI_UFF','*','UFCODICE',cp_AbsName(oSource.parent,'oMFCODUFF_2_4'),i_cWhere,'GSCG_AUF',"Codici ufficio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',oSource.xKey(1))
            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODUFF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(this.w_MFCODUFF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',this.w_MFCODUFF)
            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODUFF = NVL(_Link_.UFCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODUFF = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CODI_UFF_IDX,2])+'\'+cp_ToStr(_Link_.UFCODICE,1)
      cp_ShowWarn(i_cKey,this.CODI_UFF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODUFF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED1
  func Link_3_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED1)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED1))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED1)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED1) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED1_3_2'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED1)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED1 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT1
  func Link_3_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT1))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT1)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT1) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT1_3_3'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT1)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT1 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED2
  func Link_3_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED2)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED2))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED2)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED2) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED2_3_12'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED2)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED2 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT2
  func Link_3_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT2)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT2))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT2)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT2) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT2_3_13'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT2)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT2 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED3
  func Link_3_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED3)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED3))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED3)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED3) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED3_3_22'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED3)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED3 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT3
  func Link_3_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT3)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT3))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT3)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT3) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT3_3_23'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT3)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT3 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED4
  func Link_3_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED4)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED4))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED4)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED4) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED4_3_32'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED4)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED4 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED4 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT4
  func Link_3_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT4)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT4))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT4)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT4) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT4_3_33'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT4)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT4 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT4 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCODRE1
  func Link_4_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE1)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE1))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE1)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE1) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE1_4_2'),i_cWhere,'GSCG_ARP',"Codici regione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE1)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE1 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE1 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE1
  func Link_4_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE1))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE1)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE1) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE1_4_5'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE1)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE1 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCODRE2
  func Link_4_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE2)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE2))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE2)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE2) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE2_4_17'),i_cWhere,'GSCG_ARP',"Codici regione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE2)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE2 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE2 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE2
  func Link_4_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE2))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE2)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE2) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE2_4_18'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE2)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE2 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCODRE3
  func Link_4_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE3)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE3))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE3)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE3) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE3_4_23'),i_cWhere,'GSCG_ARP',"Codici regione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE3)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE3 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE3 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE3
  func Link_4_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE3)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE3))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE3)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE3) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE3_4_24'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE3)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE3 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCODRE4
  func Link_4_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE4)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE4))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE4)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE4) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE4_4_29'),i_cWhere,'GSCG_ARP',"Codici regione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE4)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE4 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE4 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE4
  func Link_4_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE4)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE4))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE4)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE4) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE4_4_30'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE4)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE4 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE4 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCODEL1
  func Link_4_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_LOC_IDX,3]
    i_lTable = "ENTI_LOC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_LOC_IDX,2], .t., this.ENTI_LOC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_LOC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODEL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AEL',True,'ENTI_LOC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ELCODICE like "+cp_ToStrODBC(trim(this.w_MFCODEL1)+"%");

          i_ret=cp_SQL(i_nConn,"select ELCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ELCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ELCODICE',trim(this.w_MFCODEL1))
          select ELCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ELCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODEL1)==trim(_Link_.ELCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODEL1) and !this.bDontReportError
            deferred_cp_zoom('ENTI_LOC','*','ELCODICE',cp_AbsName(oSource.parent,'oMFCODEL1_4_50'),i_cWhere,'GSCG_AEL',"Codici enti locali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODICE',oSource.xKey(1))
            select ELCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODEL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_MFCODEL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODICE',this.w_MFCODEL1)
            select ELCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODEL1 = NVL(_Link_.ELCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODEL1 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_LOC_IDX,2])+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_LOC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODEL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIEL1
  func Link_4_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIEL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIEL1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIEL1))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIEL1)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIEL1) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIEL1_4_51'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIEL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIEL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIEL1)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIEL1 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIEL1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIEL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCODEL2
  func Link_4_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_LOC_IDX,3]
    i_lTable = "ENTI_LOC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_LOC_IDX,2], .t., this.ENTI_LOC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_LOC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODEL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AEL',True,'ENTI_LOC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ELCODICE like "+cp_ToStrODBC(trim(this.w_MFCODEL2)+"%");

          i_ret=cp_SQL(i_nConn,"select ELCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ELCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ELCODICE',trim(this.w_MFCODEL2))
          select ELCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ELCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODEL2)==trim(_Link_.ELCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODEL2) and !this.bDontReportError
            deferred_cp_zoom('ENTI_LOC','*','ELCODICE',cp_AbsName(oSource.parent,'oMFCODEL2_4_56'),i_cWhere,'GSCG_AEL',"Codici enti locali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODICE',oSource.xKey(1))
            select ELCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODEL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_MFCODEL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODICE',this.w_MFCODEL2)
            select ELCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODEL2 = NVL(_Link_.ELCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODEL2 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_LOC_IDX,2])+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_LOC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODEL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIEL2
  func Link_4_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIEL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIEL2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIEL2))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIEL2)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIEL2) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIEL2_4_57'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIEL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIEL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIEL2)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIEL2 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIEL2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIEL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCODEL3
  func Link_4_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_LOC_IDX,3]
    i_lTable = "ENTI_LOC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_LOC_IDX,2], .t., this.ENTI_LOC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_LOC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODEL3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AEL',True,'ENTI_LOC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ELCODICE like "+cp_ToStrODBC(trim(this.w_MFCODEL3)+"%");

          i_ret=cp_SQL(i_nConn,"select ELCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ELCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ELCODICE',trim(this.w_MFCODEL3))
          select ELCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ELCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODEL3)==trim(_Link_.ELCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODEL3) and !this.bDontReportError
            deferred_cp_zoom('ENTI_LOC','*','ELCODICE',cp_AbsName(oSource.parent,'oMFCODEL3_4_62'),i_cWhere,'GSCG_AEL',"Codici enti locali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODICE',oSource.xKey(1))
            select ELCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODEL3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_MFCODEL3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODICE',this.w_MFCODEL3)
            select ELCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODEL3 = NVL(_Link_.ELCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODEL3 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_LOC_IDX,2])+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_LOC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODEL3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIEL3
  func Link_4_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIEL3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIEL3)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIEL3))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIEL3)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIEL3) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIEL3_4_63'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIEL3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIEL3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIEL3)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIEL3 = NVL(_Link_.TRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIEL3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIEL3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFSINAI1
  func Link_5_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_lTable = "SE_INAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2], .t., this.SE_INAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSINAI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ASI',True,'SE_INAIL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SICODICE like "+cp_ToStrODBC(trim(this.w_MFSINAI1)+"%");

          i_ret=cp_SQL(i_nConn,"select SICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SICODICE',trim(this.w_MFSINAI1))
          select SICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSINAI1)==trim(_Link_.SICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSINAI1) and !this.bDontReportError
            deferred_cp_zoom('SE_INAIL','*','SICODICE',cp_AbsName(oSource.parent,'oMFSINAI1_5_2'),i_cWhere,'GSCG_ASI',"Codici sede INAIL",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',oSource.xKey(1))
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSINAI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(this.w_MFSINAI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',this.w_MFSINAI1)
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSINAI1 = NVL(_Link_.SICODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSINAI1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])+'\'+cp_ToStr(_Link_.SICODICE,1)
      cp_ShowWarn(i_cKey,this.SE_INAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSINAI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFSINAI2
  func Link_5_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_lTable = "SE_INAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2], .t., this.SE_INAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSINAI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ASI',True,'SE_INAIL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SICODICE like "+cp_ToStrODBC(trim(this.w_MFSINAI2)+"%");

          i_ret=cp_SQL(i_nConn,"select SICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SICODICE',trim(this.w_MFSINAI2))
          select SICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSINAI2)==trim(_Link_.SICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSINAI2) and !this.bDontReportError
            deferred_cp_zoom('SE_INAIL','*','SICODICE',cp_AbsName(oSource.parent,'oMFSINAI2_5_9'),i_cWhere,'GSCG_ASI',"Codici sede INAIL",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',oSource.xKey(1))
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSINAI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(this.w_MFSINAI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',this.w_MFSINAI2)
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSINAI2 = NVL(_Link_.SICODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSINAI2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])+'\'+cp_ToStr(_Link_.SICODICE,1)
      cp_ShowWarn(i_cKey,this.SE_INAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSINAI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFSINAI3
  func Link_5_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_lTable = "SE_INAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2], .t., this.SE_INAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSINAI3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ASI',True,'SE_INAIL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SICODICE like "+cp_ToStrODBC(trim(this.w_MFSINAI3)+"%");

          i_ret=cp_SQL(i_nConn,"select SICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SICODICE',trim(this.w_MFSINAI3))
          select SICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSINAI3)==trim(_Link_.SICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSINAI3) and !this.bDontReportError
            deferred_cp_zoom('SE_INAIL','*','SICODICE',cp_AbsName(oSource.parent,'oMFSINAI3_5_16'),i_cWhere,'GSCG_ASI',"Codici sede INAIL",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',oSource.xKey(1))
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSINAI3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(this.w_MFSINAI3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',this.w_MFSINAI3)
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSINAI3 = NVL(_Link_.SICODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSINAI3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])+'\'+cp_ToStr(_Link_.SICODICE,1)
      cp_ShowWarn(i_cKey,this.SE_INAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSINAI3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDENTE
  func Link_6_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_PREV_IDX,3]
    i_lTable = "COD_PREV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2], .t., this.COD_PREV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACP',True,'COD_PREV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CPCODICE like "+cp_ToStrODBC(trim(this.w_MFCDENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select CPCODICE,CPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CPCODICE',trim(this.w_MFCDENTE))
          select CPCODICE,CPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDENTE)==trim(_Link_.CPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDENTE) and !this.bDontReportError
            deferred_cp_zoom('COD_PREV','*','CPCODICE',cp_AbsName(oSource.parent,'oMFCDENTE_6_3'),i_cWhere,'GSCG_ACP',"Codici enti previdenziali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPCODICE,CPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPCODICE',oSource.xKey(1))
            select CPCODICE,CPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPCODICE,CPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CPCODICE="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPCODICE',this.w_MFCDENTE)
            select CPCODICE,CPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDENTE = NVL(_Link_.CPCODICE,space(5))
      this.w_DESAENTE = NVL(_Link_.CPDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDENTE = space(5)
      endif
      this.w_DESAENTE = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2])+'\'+cp_ToStr(_Link_.CPCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_PREV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_PREV_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_3.CPCODICE as CPCODICE603"+ ",link_6_3.CPDESCRI as CPDESCRI603"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_3 on MOD_PAG.MFCDENTE=link_6_3.CPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_3"
          i_cKey=i_cKey+'+" and MOD_PAG.MFCDENTE=link_6_3.CPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFSDENT1
  func Link_6_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_AEN_IDX,3]
    i_lTable = "SED_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2], .t., this.SED_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSDENT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MSE',True,'SED_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODSED like "+cp_ToStrODBC(trim(this.w_MFSDENT1)+"%");
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SECODENT,SECODSED","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SECODENT',this.w_MFCDENTE;
                     ,'SECODSED',trim(this.w_MFSDENT1))
          select SECODENT,SECODSED;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SECODENT,SECODSED into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSDENT1)==trim(_Link_.SECODSED) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSDENT1) and !this.bDontReportError
            deferred_cp_zoom('SED_AEN','*','SECODENT,SECODSED',cp_AbsName(oSource.parent,'oMFSDENT1_6_4'),i_cWhere,'GSCG_MSE',"Codici sede",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',oSource.xKey(1);
                       ,'SECODSED',oSource.xKey(2))
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSDENT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                   +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(this.w_MFSDENT1);
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',this.w_MFCDENTE;
                       ,'SECODSED',this.w_MFSDENT1)
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSDENT1 = NVL(_Link_.SECODSED,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSDENT1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])+'\'+cp_ToStr(_Link_.SECODENT,1)+'\'+cp_ToStr(_Link_.SECODSED,1)
      cp_ShowWarn(i_cKey,this.SED_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSDENT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCOAE1
  func Link_6_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_AEN_IDX,3]
    i_lTable = "CAU_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2], .t., this.CAU_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCOAE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MAE',True,'CAU_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AECAUSEN like "+cp_ToStrODBC(trim(this.w_MFCCOAE1)+"%");
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AECODENT,AECAUSEN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AECODENT',this.w_MFCDENTE;
                     ,'AECAUSEN',trim(this.w_MFCCOAE1))
          select AECODENT,AECAUSEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AECODENT,AECAUSEN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCOAE1)==trim(_Link_.AECAUSEN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCOAE1) and !this.bDontReportError
            deferred_cp_zoom('CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(oSource.parent,'oMFCCOAE1_6_5'),i_cWhere,'GSCG_MAE',"Causali contributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(oSource.xKey(2));
                     +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',oSource.xKey(1);
                       ,'AECAUSEN',oSource.xKey(2))
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCOAE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                   +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(this.w_MFCCOAE1);
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',this.w_MFCDENTE;
                       ,'AECAUSEN',this.w_MFCCOAE1)
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCOAE1 = NVL(_Link_.AECAUSEN,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCOAE1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])+'\'+cp_ToStr(_Link_.AECODENT,1)+'\'+cp_ToStr(_Link_.AECAUSEN,1)
      cp_ShowWarn(i_cKey,this.CAU_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCOAE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFSDENT2
  func Link_6_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_AEN_IDX,3]
    i_lTable = "SED_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2], .t., this.SED_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSDENT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MSE',True,'SED_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODSED like "+cp_ToStrODBC(trim(this.w_MFSDENT2)+"%");
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SECODENT,SECODSED","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SECODENT',this.w_MFCDENTE;
                     ,'SECODSED',trim(this.w_MFSDENT2))
          select SECODENT,SECODSED;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SECODENT,SECODSED into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSDENT2)==trim(_Link_.SECODSED) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSDENT2) and !this.bDontReportError
            deferred_cp_zoom('SED_AEN','*','SECODENT,SECODSED',cp_AbsName(oSource.parent,'oMFSDENT2_6_13'),i_cWhere,'GSCG_MSE',"Codici sede",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',oSource.xKey(1);
                       ,'SECODSED',oSource.xKey(2))
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSDENT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                   +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(this.w_MFSDENT2);
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',this.w_MFCDENTE;
                       ,'SECODSED',this.w_MFSDENT2)
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSDENT2 = NVL(_Link_.SECODSED,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSDENT2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])+'\'+cp_ToStr(_Link_.SECODENT,1)+'\'+cp_ToStr(_Link_.SECODSED,1)
      cp_ShowWarn(i_cKey,this.SED_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSDENT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCOAE2
  func Link_6_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_AEN_IDX,3]
    i_lTable = "CAU_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2], .t., this.CAU_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCOAE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MAE',True,'CAU_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AECAUSEN like "+cp_ToStrODBC(trim(this.w_MFCCOAE2)+"%");
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AECODENT,AECAUSEN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AECODENT',this.w_MFCDENTE;
                     ,'AECAUSEN',trim(this.w_MFCCOAE2))
          select AECODENT,AECAUSEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AECODENT,AECAUSEN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCOAE2)==trim(_Link_.AECAUSEN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCOAE2) and !this.bDontReportError
            deferred_cp_zoom('CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(oSource.parent,'oMFCCOAE2_6_14'),i_cWhere,'GSCG_MAE',"Causali contributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(oSource.xKey(2));
                     +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',oSource.xKey(1);
                       ,'AECAUSEN',oSource.xKey(2))
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCOAE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                   +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(this.w_MFCCOAE2);
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',this.w_MFCDENTE;
                       ,'AECAUSEN',this.w_MFCCOAE2)
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCOAE2 = NVL(_Link_.AECAUSEN,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCOAE2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])+'\'+cp_ToStr(_Link_.AECODENT,1)+'\'+cp_ToStr(_Link_.AECAUSEN,1)
      cp_ShowWarn(i_cKey,this.CAU_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCOAE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFSDENT3
  func Link_6_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_AEN_IDX,3]
    i_lTable = "SED_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2], .t., this.SED_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSDENT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MSE',True,'SED_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODSED like "+cp_ToStrODBC(trim(this.w_MFSDENT3)+"%");
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SECODENT,SECODSED","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SECODENT',this.w_MFCDENTE;
                     ,'SECODSED',trim(this.w_MFSDENT3))
          select SECODENT,SECODSED;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SECODENT,SECODSED into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSDENT3)==trim(_Link_.SECODSED) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSDENT3) and !this.bDontReportError
            deferred_cp_zoom('SED_AEN','*','SECODENT,SECODSED',cp_AbsName(oSource.parent,'oMFSDENT3_6_22'),i_cWhere,'GSCG_MSE',"Codici sede",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',oSource.xKey(1);
                       ,'SECODSED',oSource.xKey(2))
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSDENT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                   +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(this.w_MFSDENT3);
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',this.w_MFCDENTE;
                       ,'SECODSED',this.w_MFSDENT3)
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSDENT3 = NVL(_Link_.SECODSED,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSDENT3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])+'\'+cp_ToStr(_Link_.SECODENT,1)+'\'+cp_ToStr(_Link_.SECODSED,1)
      cp_ShowWarn(i_cKey,this.SED_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSDENT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCOAE3
  func Link_6_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_AEN_IDX,3]
    i_lTable = "CAU_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2], .t., this.CAU_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCOAE3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MAE',True,'CAU_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AECAUSEN like "+cp_ToStrODBC(trim(this.w_MFCCOAE3)+"%");
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AECODENT,AECAUSEN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AECODENT',this.w_MFCDENTE;
                     ,'AECAUSEN',trim(this.w_MFCCOAE3))
          select AECODENT,AECAUSEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AECODENT,AECAUSEN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCOAE3)==trim(_Link_.AECAUSEN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCOAE3) and !this.bDontReportError
            deferred_cp_zoom('CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(oSource.parent,'oMFCCOAE3_6_23'),i_cWhere,'GSCG_MAE',"Causali contributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(oSource.xKey(2));
                     +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',oSource.xKey(1);
                       ,'AECAUSEN',oSource.xKey(2))
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCOAE3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                   +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(this.w_MFCCOAE3);
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',this.w_MFCDENTE;
                       ,'AECAUSEN',this.w_MFCCOAE3)
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCOAE3 = NVL(_Link_.AECAUSEN,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCOAE3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])+'\'+cp_ToStr(_Link_.AECODENT,1)+'\'+cp_ToStr(_Link_.AECAUSEN,1)
      cp_ShowWarn(i_cKey,this.CAU_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCOAE3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMFSERIAL_1_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page1.oPag.oMFSERIAL_1_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMFMESRIF_1_2.value==this.w_MFMESRIF)
      this.oPgFrm.Page1.oPag.oMFMESRIF_1_2.value=this.w_MFMESRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oMFANNRIF_1_3.value==this.w_MFANNRIF)
      this.oPgFrm.Page1.oPag.oMFANNRIF_1_3.value=this.w_MFANNRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oMFVALUTA_1_4.RadioValue()==this.w_MFVALUTA)
      this.oPgFrm.Page1.oPag.oMFVALUTA_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMF_COINC_1_6.RadioValue()==this.w_MF_COINC)
      this.oPgFrm.Page1.oPag.oMF_COINC_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMFSERIAL_2_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page2.oPag.oMFSERIAL_2_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page2.oPag.oMFCODUFF_2_4.value==this.w_MFCODUFF)
      this.oPgFrm.Page2.oPag.oMFCODUFF_2_4.value=this.w_MFCODUFF
    endif
    if not(this.oPgFrm.Page2.oPag.oMFCODATT_2_5.value==this.w_MFCODATT)
      this.oPgFrm.Page2.oPag.oMFCODATT_2_5.value=this.w_MFCODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oMESE_2_8.value==this.w_MESE)
      this.oPgFrm.Page2.oPag.oMESE_2_8.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page2.oPag.oANNO_2_9.value==this.w_ANNO)
      this.oPgFrm.Page2.oPag.oANNO_2_9.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page3.oPag.oMFSERIAL_3_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page3.oPag.oMFSERIAL_3_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED1_3_2.value==this.w_MFCDSED1)
      this.oPgFrm.Page3.oPag.oMFCDSED1_3_2.value=this.w_MFCDSED1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.value==this.w_MFCCONT1)
      this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.value=this.w_MFCCONT1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.value==this.w_MFMINPS1)
      this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.value=this.w_MFMINPS1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES1_3_6.value==this.w_MFDAMES1)
      this.oPgFrm.Page3.oPag.oMFDAMES1_3_6.value=this.w_MFDAMES1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN1_3_7.value==this.w_MFDAANN1)
      this.oPgFrm.Page3.oPag.oMFDAANN1_3_7.value=this.w_MFDAANN1
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES1_3_8.value==this.w_MF_AMES1)
      this.oPgFrm.Page3.oPag.oMF_AMES1_3_8.value=this.w_MF_AMES1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN1_3_9.value==this.w_MFAN1)
      this.oPgFrm.Page3.oPag.oMFAN1_3_9.value=this.w_MFAN1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD1_3_10.value==this.w_MFIMPSD1)
      this.oPgFrm.Page3.oPag.oMFIMPSD1_3_10.value=this.w_MFIMPSD1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC1_3_11.value==this.w_MFIMPSC1)
      this.oPgFrm.Page3.oPag.oMFIMPSC1_3_11.value=this.w_MFIMPSC1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED2_3_12.value==this.w_MFCDSED2)
      this.oPgFrm.Page3.oPag.oMFCDSED2_3_12.value=this.w_MFCDSED2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT2_3_13.value==this.w_MFCCONT2)
      this.oPgFrm.Page3.oPag.oMFCCONT2_3_13.value=this.w_MFCCONT2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS2_3_14.value==this.w_MFMINPS2)
      this.oPgFrm.Page3.oPag.oMFMINPS2_3_14.value=this.w_MFMINPS2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES2_3_16.value==this.w_MFDAMES2)
      this.oPgFrm.Page3.oPag.oMFDAMES2_3_16.value=this.w_MFDAMES2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN2_3_17.value==this.w_MFDAANN2)
      this.oPgFrm.Page3.oPag.oMFDAANN2_3_17.value=this.w_MFDAANN2
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES2_3_18.value==this.w_MF_AMES2)
      this.oPgFrm.Page3.oPag.oMF_AMES2_3_18.value=this.w_MF_AMES2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN2_3_19.value==this.w_MFAN2)
      this.oPgFrm.Page3.oPag.oMFAN2_3_19.value=this.w_MFAN2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD2_3_20.value==this.w_MFIMPSD2)
      this.oPgFrm.Page3.oPag.oMFIMPSD2_3_20.value=this.w_MFIMPSD2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC2_3_21.value==this.w_MFIMPSC2)
      this.oPgFrm.Page3.oPag.oMFIMPSC2_3_21.value=this.w_MFIMPSC2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED3_3_22.value==this.w_MFCDSED3)
      this.oPgFrm.Page3.oPag.oMFCDSED3_3_22.value=this.w_MFCDSED3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT3_3_23.value==this.w_MFCCONT3)
      this.oPgFrm.Page3.oPag.oMFCCONT3_3_23.value=this.w_MFCCONT3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS3_3_24.value==this.w_MFMINPS3)
      this.oPgFrm.Page3.oPag.oMFMINPS3_3_24.value=this.w_MFMINPS3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES3_3_26.value==this.w_MFDAMES3)
      this.oPgFrm.Page3.oPag.oMFDAMES3_3_26.value=this.w_MFDAMES3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN3_3_27.value==this.w_MFDAANN3)
      this.oPgFrm.Page3.oPag.oMFDAANN3_3_27.value=this.w_MFDAANN3
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES3_3_28.value==this.w_MF_AMES3)
      this.oPgFrm.Page3.oPag.oMF_AMES3_3_28.value=this.w_MF_AMES3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN3_3_29.value==this.w_MFAN3)
      this.oPgFrm.Page3.oPag.oMFAN3_3_29.value=this.w_MFAN3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD3_3_30.value==this.w_MFIMPSD3)
      this.oPgFrm.Page3.oPag.oMFIMPSD3_3_30.value=this.w_MFIMPSD3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC3_3_31.value==this.w_MFIMPSC3)
      this.oPgFrm.Page3.oPag.oMFIMPSC3_3_31.value=this.w_MFIMPSC3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED4_3_32.value==this.w_MFCDSED4)
      this.oPgFrm.Page3.oPag.oMFCDSED4_3_32.value=this.w_MFCDSED4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT4_3_33.value==this.w_MFCCONT4)
      this.oPgFrm.Page3.oPag.oMFCCONT4_3_33.value=this.w_MFCCONT4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS4_3_34.value==this.w_MFMINPS4)
      this.oPgFrm.Page3.oPag.oMFMINPS4_3_34.value=this.w_MFMINPS4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES4_3_36.value==this.w_MFDAMES4)
      this.oPgFrm.Page3.oPag.oMFDAMES4_3_36.value=this.w_MFDAMES4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN4_3_37.value==this.w_MFDAANN4)
      this.oPgFrm.Page3.oPag.oMFDAANN4_3_37.value=this.w_MFDAANN4
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES4_3_38.value==this.w_MF_AMES4)
      this.oPgFrm.Page3.oPag.oMF_AMES4_3_38.value=this.w_MF_AMES4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN4_3_39.value==this.w_MFAN4)
      this.oPgFrm.Page3.oPag.oMFAN4_3_39.value=this.w_MFAN4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD4_3_40.value==this.w_MFIMPSD4)
      this.oPgFrm.Page3.oPag.oMFIMPSD4_3_40.value=this.w_MFIMPSD4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC4_3_41.value==this.w_MFIMPSC4)
      this.oPgFrm.Page3.oPag.oMFIMPSC4_3_41.value=this.w_MFIMPSC4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFTOTDPS_3_55.value==this.w_MFTOTDPS)
      this.oPgFrm.Page3.oPag.oMFTOTDPS_3_55.value=this.w_MFTOTDPS
    endif
    if not(this.oPgFrm.Page3.oPag.oMFTOTCPS_3_56.value==this.w_MFTOTCPS)
      this.oPgFrm.Page3.oPag.oMFTOTCPS_3_56.value=this.w_MFTOTCPS
    endif
    if not(this.oPgFrm.Page3.oPag.oMFSALDPS_3_57.value==this.w_MFSALDPS)
      this.oPgFrm.Page3.oPag.oMFSALDPS_3_57.value=this.w_MFSALDPS
    endif
    if not(this.oPgFrm.Page3.oPag.oMESE_3_63.value==this.w_MESE)
      this.oPgFrm.Page3.oPag.oMESE_3_63.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page3.oPag.oANNO_3_64.value==this.w_ANNO)
      this.oPgFrm.Page3.oPag.oANNO_3_64.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page4.oPag.oMFSERIAL_4_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page4.oPag.oMFSERIAL_4_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE1_4_2.value==this.w_MFCODRE1)
      this.oPgFrm.Page4.oPag.oMFCODRE1_4_2.value=this.w_MFCODRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE1_4_5.value==this.w_MFTRIRE1)
      this.oPgFrm.Page4.oPag.oMFTRIRE1_4_5.value=this.w_MFTRIRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE1_4_6.value==this.w_MFRATRE1)
      this.oPgFrm.Page4.oPag.oMFRATRE1_4_6.value=this.w_MFRATRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE1_4_7.value==this.w_MFANNRE1)
      this.oPgFrm.Page4.oPag.oMFANNRE1_4_7.value=this.w_MFANNRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE1_4_8.value==this.w_MFIMDRE1)
      this.oPgFrm.Page4.oPag.oMFIMDRE1_4_8.value=this.w_MFIMDRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE1_4_9.value==this.w_MFIMCRE1)
      this.oPgFrm.Page4.oPag.oMFIMCRE1_4_9.value=this.w_MFIMCRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE2_4_17.value==this.w_MFCODRE2)
      this.oPgFrm.Page4.oPag.oMFCODRE2_4_17.value=this.w_MFCODRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE2_4_18.value==this.w_MFTRIRE2)
      this.oPgFrm.Page4.oPag.oMFTRIRE2_4_18.value=this.w_MFTRIRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE2_4_19.value==this.w_MFRATRE2)
      this.oPgFrm.Page4.oPag.oMFRATRE2_4_19.value=this.w_MFRATRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE2_4_20.value==this.w_MFANNRE2)
      this.oPgFrm.Page4.oPag.oMFANNRE2_4_20.value=this.w_MFANNRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE2_4_21.value==this.w_MFIMDRE2)
      this.oPgFrm.Page4.oPag.oMFIMDRE2_4_21.value=this.w_MFIMDRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE2_4_22.value==this.w_MFIMCRE2)
      this.oPgFrm.Page4.oPag.oMFIMCRE2_4_22.value=this.w_MFIMCRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE3_4_23.value==this.w_MFCODRE3)
      this.oPgFrm.Page4.oPag.oMFCODRE3_4_23.value=this.w_MFCODRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE3_4_24.value==this.w_MFTRIRE3)
      this.oPgFrm.Page4.oPag.oMFTRIRE3_4_24.value=this.w_MFTRIRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE3_4_25.value==this.w_MFRATRE3)
      this.oPgFrm.Page4.oPag.oMFRATRE3_4_25.value=this.w_MFRATRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE3_4_26.value==this.w_MFANNRE3)
      this.oPgFrm.Page4.oPag.oMFANNRE3_4_26.value=this.w_MFANNRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE3_4_27.value==this.w_MFIMDRE3)
      this.oPgFrm.Page4.oPag.oMFIMDRE3_4_27.value=this.w_MFIMDRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE3_4_28.value==this.w_MFIMCRE3)
      this.oPgFrm.Page4.oPag.oMFIMCRE3_4_28.value=this.w_MFIMCRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE4_4_29.value==this.w_MFCODRE4)
      this.oPgFrm.Page4.oPag.oMFCODRE4_4_29.value=this.w_MFCODRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE4_4_30.value==this.w_MFTRIRE4)
      this.oPgFrm.Page4.oPag.oMFTRIRE4_4_30.value=this.w_MFTRIRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE4_4_31.value==this.w_MFRATRE4)
      this.oPgFrm.Page4.oPag.oMFRATRE4_4_31.value=this.w_MFRATRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE4_4_32.value==this.w_MFANNRE4)
      this.oPgFrm.Page4.oPag.oMFANNRE4_4_32.value=this.w_MFANNRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE4_4_33.value==this.w_MFIMDRE4)
      this.oPgFrm.Page4.oPag.oMFIMDRE4_4_33.value=this.w_MFIMDRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE4_4_34.value==this.w_MFIMCRE4)
      this.oPgFrm.Page4.oPag.oMFIMCRE4_4_34.value=this.w_MFIMCRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTOTDRE_4_38.value==this.w_MFTOTDRE)
      this.oPgFrm.Page4.oPag.oMFTOTDRE_4_38.value=this.w_MFTOTDRE
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTOTCRE_4_39.value==this.w_MFTOTCRE)
      this.oPgFrm.Page4.oPag.oMFTOTCRE_4_39.value=this.w_MFTOTCRE
    endif
    if not(this.oPgFrm.Page4.oPag.oMFSALDRE_4_40.value==this.w_MFSALDRE)
      this.oPgFrm.Page4.oPag.oMFSALDRE_4_40.value=this.w_MFSALDRE
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODEL1_4_50.value==this.w_MFCODEL1)
      this.oPgFrm.Page4.oPag.oMFCODEL1_4_50.value=this.w_MFCODEL1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIEL1_4_51.value==this.w_MFTRIEL1)
      this.oPgFrm.Page4.oPag.oMFTRIEL1_4_51.value=this.w_MFTRIEL1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATEL1_4_52.value==this.w_MFRATEL1)
      this.oPgFrm.Page4.oPag.oMFRATEL1_4_52.value=this.w_MFRATEL1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNEL1_4_53.value==this.w_MFANNEL1)
      this.oPgFrm.Page4.oPag.oMFANNEL1_4_53.value=this.w_MFANNEL1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDEL1_4_54.value==this.w_MFIMDEL1)
      this.oPgFrm.Page4.oPag.oMFIMDEL1_4_54.value=this.w_MFIMDEL1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCEL1_4_55.value==this.w_MFIMCEL1)
      this.oPgFrm.Page4.oPag.oMFIMCEL1_4_55.value=this.w_MFIMCEL1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODEL2_4_56.value==this.w_MFCODEL2)
      this.oPgFrm.Page4.oPag.oMFCODEL2_4_56.value=this.w_MFCODEL2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIEL2_4_57.value==this.w_MFTRIEL2)
      this.oPgFrm.Page4.oPag.oMFTRIEL2_4_57.value=this.w_MFTRIEL2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATEL2_4_58.value==this.w_MFRATEL2)
      this.oPgFrm.Page4.oPag.oMFRATEL2_4_58.value=this.w_MFRATEL2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNEL2_4_59.value==this.w_MFANNEL2)
      this.oPgFrm.Page4.oPag.oMFANNEL2_4_59.value=this.w_MFANNEL2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDEL2_4_60.value==this.w_MFIMDEL2)
      this.oPgFrm.Page4.oPag.oMFIMDEL2_4_60.value=this.w_MFIMDEL2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCEL2_4_61.value==this.w_MFIMCEL2)
      this.oPgFrm.Page4.oPag.oMFIMCEL2_4_61.value=this.w_MFIMCEL2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODEL3_4_62.value==this.w_MFCODEL3)
      this.oPgFrm.Page4.oPag.oMFCODEL3_4_62.value=this.w_MFCODEL3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIEL3_4_63.value==this.w_MFTRIEL3)
      this.oPgFrm.Page4.oPag.oMFTRIEL3_4_63.value=this.w_MFTRIEL3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATEL3_4_64.value==this.w_MFRATEL3)
      this.oPgFrm.Page4.oPag.oMFRATEL3_4_64.value=this.w_MFRATEL3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNEL3_4_65.value==this.w_MFANNEL3)
      this.oPgFrm.Page4.oPag.oMFANNEL3_4_65.value=this.w_MFANNEL3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDEL3_4_66.value==this.w_MFIMDEL3)
      this.oPgFrm.Page4.oPag.oMFIMDEL3_4_66.value=this.w_MFIMDEL3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCEL3_4_67.value==this.w_MFIMCEL3)
      this.oPgFrm.Page4.oPag.oMFIMCEL3_4_67.value=this.w_MFIMCEL3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTOTDEL_4_68.value==this.w_MFTOTDEL)
      this.oPgFrm.Page4.oPag.oMFTOTDEL_4_68.value=this.w_MFTOTDEL
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTOTCEL_4_69.value==this.w_MFTOTCEL)
      this.oPgFrm.Page4.oPag.oMFTOTCEL_4_69.value=this.w_MFTOTCEL
    endif
    if not(this.oPgFrm.Page4.oPag.oMFSALDEL_4_70.value==this.w_MFSALDEL)
      this.oPgFrm.Page4.oPag.oMFSALDEL_4_70.value=this.w_MFSALDEL
    endif
    if not(this.oPgFrm.Page4.oPag.oMESE_4_80.value==this.w_MESE)
      this.oPgFrm.Page4.oPag.oMESE_4_80.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page4.oPag.oANNO_4_81.value==this.w_ANNO)
      this.oPgFrm.Page4.oPag.oANNO_4_81.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSERIAL_5_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page5.oPag.oMFSERIAL_5_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSINAI1_5_2.value==this.w_MFSINAI1)
      this.oPgFrm.Page5.oPag.oMFSINAI1_5_2.value=this.w_MFSINAI1
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.value==this.w_MF_NPOS1)
      this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.value=this.w_MF_NPOS1
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.value==this.w_MF_PACC1)
      this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.value=this.w_MF_PACC1
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.value==this.w_MF_NRIF1)
      this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.value=this.w_MF_NRIF1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.value==this.w_MFCAUSA1)
      this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.value=this.w_MFCAUSA1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.value==this.w_MFIMDIL1)
      this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.value=this.w_MFIMDIL1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.value==this.w_MFIMCIL1)
      this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.value=this.w_MFIMCIL1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSINAI2_5_9.value==this.w_MFSINAI2)
      this.oPgFrm.Page5.oPag.oMFSINAI2_5_9.value=this.w_MFSINAI2
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.value==this.w_MF_NPOS2)
      this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.value=this.w_MF_NPOS2
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.value==this.w_MF_PACC2)
      this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.value=this.w_MF_PACC2
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.value==this.w_MF_NRIF2)
      this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.value=this.w_MF_NRIF2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.value==this.w_MFCAUSA2)
      this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.value=this.w_MFCAUSA2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.value==this.w_MFIMDIL2)
      this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.value=this.w_MFIMDIL2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.value==this.w_MFIMCIL2)
      this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.value=this.w_MFIMCIL2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSINAI3_5_16.value==this.w_MFSINAI3)
      this.oPgFrm.Page5.oPag.oMFSINAI3_5_16.value=this.w_MFSINAI3
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.value==this.w_MF_NPOS3)
      this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.value=this.w_MF_NPOS3
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.value==this.w_MF_PACC3)
      this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.value=this.w_MF_PACC3
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.value==this.w_MF_NRIF3)
      this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.value=this.w_MF_NRIF3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.value==this.w_MFCAUSA3)
      this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.value=this.w_MFCAUSA3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.value==this.w_MFIMDIL3)
      this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.value=this.w_MFIMDIL3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.value==this.w_MFIMCIL3)
      this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.value=this.w_MFIMCIL3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFTDINAI_5_32.value==this.w_MFTDINAI)
      this.oPgFrm.Page5.oPag.oMFTDINAI_5_32.value=this.w_MFTDINAI
    endif
    if not(this.oPgFrm.Page5.oPag.oMFTCINAI_5_33.value==this.w_MFTCINAI)
      this.oPgFrm.Page5.oPag.oMFTCINAI_5_33.value=this.w_MFTCINAI
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSALINA_5_34.value==this.w_MFSALINA)
      this.oPgFrm.Page5.oPag.oMFSALINA_5_34.value=this.w_MFSALINA
    endif
    if not(this.oPgFrm.Page5.oPag.oMESE_5_42.value==this.w_MESE)
      this.oPgFrm.Page5.oPag.oMESE_5_42.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page5.oPag.oANNO_5_43.value==this.w_ANNO)
      this.oPgFrm.Page5.oPag.oANNO_5_43.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page6.oPag.oMFSERIAL_6_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page6.oPag.oMFSERIAL_6_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCDENTE_6_3.value==this.w_MFCDENTE)
      this.oPgFrm.Page6.oPag.oMFCDENTE_6_3.value=this.w_MFCDENTE
    endif
    if not(this.oPgFrm.Page6.oPag.oMFSDENT1_6_4.value==this.w_MFSDENT1)
      this.oPgFrm.Page6.oPag.oMFSDENT1_6_4.value=this.w_MFSDENT1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCCOAE1_6_5.value==this.w_MFCCOAE1)
      this.oPgFrm.Page6.oPag.oMFCCOAE1_6_5.value=this.w_MFCCOAE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCDPOS1_6_6.value==this.w_MFCDPOS1)
      this.oPgFrm.Page6.oPag.oMFCDPOS1_6_6.value=this.w_MFCDPOS1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSINE1_6_7.value==this.w_MFMSINE1)
      this.oPgFrm.Page6.oPag.oMFMSINE1_6_7.value=this.w_MFMSINE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANINE1_6_8.value==this.w_MFANINE1)
      this.oPgFrm.Page6.oPag.oMFANINE1_6_8.value=this.w_MFANINE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSFIE1_6_9.value==this.w_MFMSFIE1)
      this.oPgFrm.Page6.oPag.oMFMSFIE1_6_9.value=this.w_MFMSFIE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANF1_6_10.value==this.w_MFANF1)
      this.oPgFrm.Page6.oPag.oMFANF1_6_10.value=this.w_MFANF1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMDAE1_6_11.value==this.w_MFIMDAE1)
      this.oPgFrm.Page6.oPag.oMFIMDAE1_6_11.value=this.w_MFIMDAE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMCAE1_6_12.value==this.w_MFIMCAE1)
      this.oPgFrm.Page6.oPag.oMFIMCAE1_6_12.value=this.w_MFIMCAE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFSDENT2_6_13.value==this.w_MFSDENT2)
      this.oPgFrm.Page6.oPag.oMFSDENT2_6_13.value=this.w_MFSDENT2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCCOAE2_6_14.value==this.w_MFCCOAE2)
      this.oPgFrm.Page6.oPag.oMFCCOAE2_6_14.value=this.w_MFCCOAE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCDPOS2_6_15.value==this.w_MFCDPOS2)
      this.oPgFrm.Page6.oPag.oMFCDPOS2_6_15.value=this.w_MFCDPOS2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSINE2_6_16.value==this.w_MFMSINE2)
      this.oPgFrm.Page6.oPag.oMFMSINE2_6_16.value=this.w_MFMSINE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANINE2_6_17.value==this.w_MFANINE2)
      this.oPgFrm.Page6.oPag.oMFANINE2_6_17.value=this.w_MFANINE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSFIE2_6_18.value==this.w_MFMSFIE2)
      this.oPgFrm.Page6.oPag.oMFMSFIE2_6_18.value=this.w_MFMSFIE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANF2_6_19.value==this.w_MFANF2)
      this.oPgFrm.Page6.oPag.oMFANF2_6_19.value=this.w_MFANF2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMDAE2_6_20.value==this.w_MFIMDAE2)
      this.oPgFrm.Page6.oPag.oMFIMDAE2_6_20.value=this.w_MFIMDAE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMCAE2_6_21.value==this.w_MFIMCAE2)
      this.oPgFrm.Page6.oPag.oMFIMCAE2_6_21.value=this.w_MFIMCAE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFSDENT3_6_22.value==this.w_MFSDENT3)
      this.oPgFrm.Page6.oPag.oMFSDENT3_6_22.value=this.w_MFSDENT3
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCCOAE3_6_23.value==this.w_MFCCOAE3)
      this.oPgFrm.Page6.oPag.oMFCCOAE3_6_23.value=this.w_MFCCOAE3
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCDPOS3_6_24.value==this.w_MFCDPOS3)
      this.oPgFrm.Page6.oPag.oMFCDPOS3_6_24.value=this.w_MFCDPOS3
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSINE3_6_25.value==this.w_MFMSINE3)
      this.oPgFrm.Page6.oPag.oMFMSINE3_6_25.value=this.w_MFMSINE3
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANINE3_6_26.value==this.w_MFANINE3)
      this.oPgFrm.Page6.oPag.oMFANINE3_6_26.value=this.w_MFANINE3
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSFIE3_6_27.value==this.w_MFMSFIE3)
      this.oPgFrm.Page6.oPag.oMFMSFIE3_6_27.value=this.w_MFMSFIE3
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANF3_6_28.value==this.w_MFANF3)
      this.oPgFrm.Page6.oPag.oMFANF3_6_28.value=this.w_MFANF3
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMDAE3_6_29.value==this.w_MFIMDAE3)
      this.oPgFrm.Page6.oPag.oMFIMDAE3_6_29.value=this.w_MFIMDAE3
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMCAE3_6_30.value==this.w_MFIMCAE3)
      this.oPgFrm.Page6.oPag.oMFIMCAE3_6_30.value=this.w_MFIMCAE3
    endif
    if not(this.oPgFrm.Page6.oPag.oMFTDAENT_6_44.value==this.w_MFTDAENT)
      this.oPgFrm.Page6.oPag.oMFTDAENT_6_44.value=this.w_MFTDAENT
    endif
    if not(this.oPgFrm.Page6.oPag.oMFTCAENT_6_45.value==this.w_MFTCAENT)
      this.oPgFrm.Page6.oPag.oMFTCAENT_6_45.value=this.w_MFTCAENT
    endif
    if not(this.oPgFrm.Page6.oPag.oMFSALAEN_6_46.value==this.w_MFSALAEN)
      this.oPgFrm.Page6.oPag.oMFSALAEN_6_46.value=this.w_MFSALAEN
    endif
    if not(this.oPgFrm.Page6.oPag.oDESAENTE_6_51.value==this.w_DESAENTE)
      this.oPgFrm.Page6.oPag.oDESAENTE_6_51.value=this.w_DESAENTE
    endif
    if not(this.oPgFrm.Page6.oPag.oMESE_6_54.value==this.w_MESE)
      this.oPgFrm.Page6.oPag.oMESE_6_54.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page6.oPag.oANNO_6_55.value==this.w_ANNO)
      this.oPgFrm.Page6.oPag.oANNO_6_55.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page7.oPag.oMFSERIAL_7_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page7.oPag.oMFSERIAL_7_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page7.oPag.oMFSALFIN_7_2.value==this.w_MFSALFIN)
      this.oPgFrm.Page7.oPag.oMFSALFIN_7_2.value=this.w_MFSALFIN
    endif
    if not(this.oPgFrm.Page7.oPag.oMESE_7_9.value==this.w_MESE)
      this.oPgFrm.Page7.oPag.oMESE_7_9.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page7.oPag.oANNO_7_10.value==this.w_ANNO)
      this.oPgFrm.Page7.oPag.oANNO_7_10.value=this.w_ANNO
    endif
    cp_SetControlsValueExtFlds(this,'MOD_PAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(not empty(.w_MFMESRIF) and val(.w_MFMESRIF)>= 1 and val(.w_MFMESRIF)<13)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMFMESRIF_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mese di riferimento non valido")
          case   not(not empty(.w_MFANNRIF) and val(.w_MFANNRIF)>1950 and val(.w_MFANNRIF)<2050)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMFANNRIF_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un anno compreso fra il 1950 e 2050")
          case   (empty(.w_MFCCONT1))  and (not empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT1_3_3.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!empty(.w_MFDAMES1) and 0<=val(.w_MFDAMES1) and val(.w_MFDAMES1)<13)  and (not empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES1_3_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(!empty(.w_MFDAANN1) and (val(.w_MFDAANN1)>=1996  and val(.w_MFDAANN1)<=2050 or val(.w_MFDAANN1)=0))  and (!empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN1_3_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(!empty(.w_MF_AMES1)  and 0<=val(.w_MF_AMES1) and val(.w_MF_AMES1)<13)  and (!empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES1_3_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(val(.w_MFAN1)=0 or val(.w_MFAN1)<=2050 and(val(.w_MFDAANN1)<val(.w_MFAN1)or .w_MFDAANN1=.w_MFAN1 and val(.w_MFDAMES1)<=val(.w_MF_AMES1)))  and (!empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN1_3_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCONT2))  and (not empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT2_3_13.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!empty(.w_MFDAMES2)  and 0<=val(.w_MFDAMES2) and val(.w_MFDAMES2)<13)  and (not empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES2_3_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(!empty(.w_MFDAANN2) and (val(.w_MFDAANN2)>=1996  and val(.w_MFDAANN2)<=2050  or val(.w_MFDAANN2)=0))  and (!empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN2_3_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(!empty(.w_MF_AMES2)  and 0<=val(.w_MF_AMES2) and val(.w_MF_AMES2)<13)  and (!empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES2_3_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(val(.w_MFAN2)=0 or val(.w_MFAN2)<=2050 and(val(.w_MFDAANN2)<val(.w_MFAN2)or .w_MFDAANN2=.w_MFAN2 and val(.w_MFDAMES2)<=val(.w_MF_AMES2)))  and (!empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN2_3_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCONT3))  and (not empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT3_3_23.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!empty(.w_MFDAMES3)  and 0<=val(.w_MFDAMES3) and val(.w_MFDAMES3)<13)  and (not empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES3_3_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(!empty(.w_MFDAANN3) and (val(.w_MFDAANN3)>=1996  and val(.w_MFDAANN3)<=2050  or val(.w_MFDAANN3)=0))  and (!empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN3_3_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(!empty(.w_MF_AMES3)  and 0<=val(.w_MF_AMES3) and val(.w_MF_AMES3)<13)  and (!empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES3_3_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(val(.w_MFAN3)=0 or val(.w_MFAN3)<=2050 and(val(.w_MFDAANN3)<val(.w_MFAN3)or .w_MFDAANN3=.w_MFAN3 and val(.w_MFDAMES3)<=val(.w_MF_AMES3)))  and (!empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN3_3_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCONT4))  and (not empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT4_3_33.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT4)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!empty(.w_MFDAMES4)  and 0<=val(.w_MFDAMES4) and val(.w_MFDAMES4)<13)  and (not empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES4_3_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(!empty(.w_MFDAANN4) and (val(.w_MFDAANN4)>=1996  and val(.w_MFDAANN4)<=2050 or val(.w_MFDAANN4)=0))  and (!empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN4_3_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(!empty(.w_MF_AMES4)  and 0<=val(.w_MF_AMES4) and val(.w_MF_AMES4)<13)  and (!empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES4_3_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(val(.w_MFAN4)=0 or val(.w_MFAN4)<=2050 and(val(.w_MFDAANN4)<val(.w_MFAN4)or .w_MFDAANN4=.w_MFAN4 and val(.w_MFDAMES4)<=val(.w_MF_AMES4)))  and (!empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN4_3_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFTRIRE1))  and (not empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE1_4_5.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_MFRATRE1) or left(.w_MFRATRE1,2)<=right(.w_MFRATRE1,2) and len(alltrim(.w_MFRATRE1))=4)  and (not empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE1_4_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNRE1) and (val(.w_MFANNRE1)>=1996  and val(.w_MFANNRE1)<=2050  or val(.w_MFANNRE1)=0))  and (!empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE1_4_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050")
          case   (empty(.w_MFTRIRE2))  and (not empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE2_4_18.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_MFRATRE2) or left(.w_MFRATRE2,2)<=right(.w_MFRATRE2,2) and len(alltrim(.w_MFRATRE2))=4)  and (not empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE2_4_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNRE2) and (val(.w_MFANNRE2)>=1996  and val(.w_MFANNRE2)<=2050   or val(.w_MFANNRE2)=0))  and (!empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE2_4_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050")
          case   (empty(.w_MFTRIRE3))  and (not empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE3_4_24.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_MFRATRE3) or left(.w_MFRATRE3,2)<=right(.w_MFRATRE3,2) and len(alltrim(.w_MFRATRE3))=4)  and (not empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE3_4_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNRE3) and (val(.w_MFANNRE3)>=1996  and val(.w_MFANNRE3)<=2050  or val(.w_MFANNRE3)=0))  and (!empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE3_4_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050")
          case   (empty(.w_MFTRIRE4))  and (not empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE4_4_30.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE4)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_MFRATRE4) or left(.w_MFRATRE4,2)<=right(.w_MFRATRE4,2) and len(alltrim(.w_MFRATRE4))=4)  and (not empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE4_4_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNRE4) and (val(.w_MFANNRE4)>=1996  and val(.w_MFANNRE4)<=2050   or val(.w_MFANNRE4)=0))  and (!empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE4_4_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050")
          case   (empty(.w_MFTRIEL1))  and (not empty(.w_MFCODEL1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIEL1_4_51.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIEL1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_MFRATEL1) or left(.w_MFRATEL1,2)<=right(.w_MFRATEL1,2) and len(alltrim(.w_MFRATEL1))=4)  and (not empty(.w_MFCODEL1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATEL1_4_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNEL1) and (val(.w_MFANNEL1)>=1996  and val(.w_MFANNEL1)<=2050 or val(.w_MFANNEL1)=0))  and (not empty(.w_MFCODEL1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNEL1_4_53.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: selezionare un anno compreso fra 1996 e 2050")
          case   (empty(.w_MFTRIEL2))  and (not empty(.w_MFCODEL2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIEL2_4_57.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIEL2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_MFRATEL2) or left(.w_MFRATEL2,2)<=right(.w_MFRATEL2,2) and len(alltrim(.w_MFRATEL2))=4)  and (not empty(.w_MFCODEL2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATEL2_4_58.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNEL2) and (val(.w_MFANNEL2)>=1996  and val(.w_MFANNEL2)<=2050  or val(.w_MFANNEL2)=0))  and (not empty(.w_MFCODEL2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNEL2_4_59.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: selezionare un anno compreso fra 1996 e 2050")
          case   (empty(.w_MFTRIEL3))  and (not empty(.w_MFCODEL3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIEL3_4_63.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIEL3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_MFRATEL3) or left(.w_MFRATEL3,2)<=right(.w_MFRATEL3,2) and len(alltrim(.w_MFRATEL3))=4)  and (not empty(.w_MFCODEL3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATEL3_4_64.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNEL3) and (val(.w_MFANNEL3)>=1996  and val(.w_MFANNEL3)<=2050  or val(.w_MFANNEL3)=0))  and (not empty(.w_MFCODEL3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNEL3_4_65.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: selezionare un anno compreso fra 1996 e 2050")
          case   (empty(.w_MFCCOAE1))  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFCCOAE1_6_5.SetFocus()
            i_bnoObbl = !empty(.w_MFCCOAE1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!empty(.w_MFMSINE1) and 0<= val(.w_MFMSINE1) and val(.w_MFMSINE1) < 13)  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSINE1_6_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(!empty(.w_MFANINE1) and (val(.w_MFANINE1)>=1996  and  val(.w_MFANINE1)<=2050 or val(.w_MFANINE1)=0))  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANINE1_6_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un anno compreso fra 1996 e 2050")
          case   not(!empty(.w_MFMSFIE1) and 0<=val(.w_MFMSFIE1) and val(.w_MFMSFIE1)<13)  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSFIE1_6_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(!empty(.w_MFANF1)and(val(.w_MFANF1)=0or(val(.w_MFANINE1)<val(.w_MFANF1)or .w_MFANINE1=.w_MFANF1 and val(.w_MFMSINE1)<=val(.w_MFMSFIE1))))  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANF1_6_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCOAE2))  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFCCOAE2_6_14.SetFocus()
            i_bnoObbl = !empty(.w_MFCCOAE2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!empty(.w_MFMSINE2) and 0<= val(.w_MFMSINE2) and val(.w_MFMSINE2) < 13)  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSINE2_6_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(!empty(.w_MFANINE2)  and (val(.w_MFANINE2)>=1996  and val(.w_MFANINE2)<=2050  or val(.w_MFANINE2)=0))  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANINE2_6_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un anno compreso fra 1996 e 2050")
          case   not(!empty(.w_MFMSFIE2)  and 0<=val(.w_MFMSFIE2) and val(.w_MFMSFIE2)<13)  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSFIE2_6_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(!empty(.w_MFANF2)and(val(.w_MFANF2)=0or(val(.w_MFANINE2)<val(.w_MFANF2)or .w_MFANINE2=.w_MFANF2 and val(.w_MFMSINE2)<=val(.w_MFMSFIE2))))  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANF2_6_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCOAE3))  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFCCOAE3_6_23.SetFocus()
            i_bnoObbl = !empty(.w_MFCCOAE3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!empty(.w_MFMSINE3)and 0<= val(.w_MFMSINE3) and val(.w_MFMSINE3) < 13)  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSINE3_6_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(!empty(.w_MFANINE3)  and (val(.w_MFANINE3)>=1996  and val(.w_MFANINE3)<=2050  or val(.w_MFANINE3)=0))  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANINE3_6_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un anno compreso fra 1996 e 2050")
          case   not(!empty(.w_MFMSFIE3) and 0<=val(.w_MFMSFIE3) and val(.w_MFMSFIE3)<13)  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSFIE3_6_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(!empty(.w_MFANF3)and(val(.w_MFANF3)=0or(val(.w_MFANINE3)<val(.w_MFANF3)or .w_MFANINE3=.w_MFANF3 and val(.w_MFMSINE3)<=val(.w_MFMSFIE3))))  and (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANF3_6_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCG_ACF.CheckForm()
      if i_bres
        i_bres=  .GSCG_ACF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_AEF.CheckForm()
      if i_bres
        i_bres=  .GSCG_AEF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_AVF.CheckForm()
      if i_bres
        i_bres=  .GSCG_AVF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=7
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_APPCOIN = this.w_APPCOIN
    this.o_MFCDSED1 = this.w_MFCDSED1
    this.o_MFCCONT1 = this.w_MFCCONT1
    this.o_APP1 = this.w_APP1
    this.o_MFCDSED2 = this.w_MFCDSED2
    this.o_MFCCONT2 = this.w_MFCCONT2
    this.o_APP2 = this.w_APP2
    this.o_MFCDSED3 = this.w_MFCDSED3
    this.o_MFCCONT3 = this.w_MFCCONT3
    this.o_APP3 = this.w_APP3
    this.o_MFCDSED4 = this.w_MFCDSED4
    this.o_MFCCONT4 = this.w_MFCCONT4
    this.o_APP4 = this.w_APP4
    this.o_MFCODRE1 = this.w_MFCODRE1
    this.o_MFTRIRE1 = this.w_MFTRIRE1
    this.o_MFCODRE2 = this.w_MFCODRE2
    this.o_MFTRIRE2 = this.w_MFTRIRE2
    this.o_MFCODRE3 = this.w_MFCODRE3
    this.o_MFTRIRE3 = this.w_MFTRIRE3
    this.o_MFCODRE4 = this.w_MFCODRE4
    this.o_MFTRIRE4 = this.w_MFTRIRE4
    this.o_MFCODEL1 = this.w_MFCODEL1
    this.o_MFCODEL2 = this.w_MFCODEL2
    this.o_MFCODEL3 = this.w_MFCODEL3
    this.o_MFSINAI1 = this.w_MFSINAI1
    this.o_MFSINAI2 = this.w_MFSINAI2
    this.o_MFSINAI3 = this.w_MFSINAI3
    this.o_MFCDENTE = this.w_MFCDENTE
    this.o_MFSDENT1 = this.w_MFSDENT1
    this.o_MFCCOAE1 = this.w_MFCCOAE1
    this.o_MFSDENT2 = this.w_MFSDENT2
    this.o_MFCCOAE2 = this.w_MFCCOAE2
    this.o_MFSDENT3 = this.w_MFSDENT3
    this.o_MFCCOAE3 = this.w_MFCCOAE3
    * --- GSCG_ACF : Depends On
    this.GSCG_ACF.SaveDependsOn()
    * --- GSCG_AEF : Depends On
    this.GSCG_AEF.SaveDependsOn()
    * --- GSCG_AVF : Depends On
    this.GSCG_AVF.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_amfPag1 as StdContainer
  Width  = 731
  height = 390
  stdWidth  = 731
  stdheight = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_1_1 as StdField with uid="INGQIDSUEM",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero modello",;
    HelpContextID = 31479314,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=116, Top=19, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFMESRIF_1_2 as StdField with uid="HFOOEBHDWN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MFMESRIF", cQueryName = "MFMESRIF",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Mese di riferimento non valido",;
    HelpContextID = 84937204,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=366, Top=19, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMESRIF_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_MFMESRIF) and val(.w_MFMESRIF)>= 1 and val(.w_MFMESRIF)<13)
    endwith
    return bRes
  endfunc

  add object oMFANNRIF_1_3 as StdField with uid="SPMPJCSSZI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MFANNRIF", cQueryName = "MFANNRIF",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un anno compreso fra il 1950 e 2050",;
    HelpContextID = 89639412,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=391, Top=19, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRIF_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_MFANNRIF) and val(.w_MFANNRIF)>1950 and val(.w_MFANNRIF)<2050)
    endwith
    return bRes
  endfunc


  add object oMFVALUTA_1_4 as StdCombo with uid="PYOBZQACID",rtseq=4,rtrep=.f.,left=597,top=20,width=111,height=21;
    , HelpContextID = 226264583;
    , cFormVar="w_MFVALUTA",RowSource=""+"Valuta di conto,"+"Valuta nazionale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMFVALUTA_1_4.RadioValue()
    return(iif(this.value =1,g_CODEUR,;
    iif(this.value =2,g_CODLIR,;
    space(3))))
  endfunc
  func oMFVALUTA_1_4.GetRadio()
    this.Parent.oContained.w_MFVALUTA = this.RadioValue()
    return .t.
  endfunc

  func oMFVALUTA_1_4.SetRadio()
    this.Parent.oContained.w_MFVALUTA=trim(this.Parent.oContained.w_MFVALUTA)
    this.value = ;
      iif(this.Parent.oContained.w_MFVALUTA==g_CODEUR,1,;
      iif(this.Parent.oContained.w_MFVALUTA==g_CODLIR,2,;
      0))
  endfunc

  add object oMF_COINC_1_6 as StdCheck with uid="RKATVGKMBB",rtseq=6,rtrep=.f.,left=655, top=49, caption="",;
    HelpContextID = 28251657,;
    cFormVar="w_MF_COINC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMF_COINC_1_6.RadioValue()
    return(iif(this.value =1,'N',;
    ' '))
  endfunc
  func oMF_COINC_1_6.GetRadio()
    this.Parent.oContained.w_MF_COINC = this.RadioValue()
    return .t.
  endfunc

  func oMF_COINC_1_6.SetRadio()
    this.Parent.oContained.w_MF_COINC=trim(this.Parent.oContained.w_MF_COINC)
    this.value = ;
      iif(this.Parent.oContained.w_MF_COINC=='N',1,;
      0)
  endfunc


  add object oLinkPC_1_7 as stdDynamicChildContainer with uid="VFEXZQDZFH",left=7, top=69, width=722, height=310, bOnScreen=.t.;



  add object oObj_1_12 as cp_runprogram with uid="APLGIFIYWP",left=157, top=406, width=201,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BIM('M','VAL')",;
    cEvent = "w_MFVALUTA Changed",;
    nPag=1;
    , HelpContextID = 236052966


  add object oObj_1_13 as cp_runprogram with uid="PLXBYEFNSH",left=4, top=406, width=152,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BIM('M','ANN')",;
    cEvent = "New record",;
    nPag=1;
    , HelpContextID = 236052966

  add object oStr_1_8 as StdString with uid="RIMPGFGUUC",Visible=.t., Left=359, Top=52,;
    Alignment=1, Width=288, Height=15,;
    Caption="Anno di imposta non coincidente con anno solare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="EFRBFXDZBK",Visible=.t., Left=8, Top=19,;
    Alignment=1, Width=104, Height=15,;
    Caption="Numero modello:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="DNRJNKBPHF",Visible=.t., Left=207, Top=19,;
    Alignment=1, Width=154, Height=15,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="IWKPFCUQCR",Visible=.t., Left=465, Top=20,;
    Alignment=1, Width=124, Height=15,;
    Caption="Valuta di riferimento:"  ;
  , bGlobalFont=.t.
enddefine
define class tgscg_amfPag2 as StdContainer
  Width  = 731
  height = 390
  stdWidth  = 731
  stdheight = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_2_1 as StdField with uid="ODPEQKOEGB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero modello",;
    HelpContextID = 31479314,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=137, Top=18, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)


  add object oLinkPC_2_2 as stdDynamicChildContainer with uid="LPQACJIYQL",left=5, top=44, width=698, height=297, bOnScreen=.t.;


  add object oMFCODUFF_2_4 as StdField with uid="RYJFAXKYGF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MFCODUFF", cQueryName = "MFCODUFF",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice ufficio",;
    HelpContextID = 49719796,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=137, Top=353, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CODI_UFF", cZoomOnZoom="GSCG_AUF", oKey_1_1="UFCODICE", oKey_1_2="this.w_MFCODUFF"

  func oMFCODUFF_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODUFF_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODUFF_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CODI_UFF','*','UFCODICE',cp_AbsName(this.parent,'oMFCODUFF_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AUF',"Codici ufficio",'',this.parent.oContained
  endproc
  proc oMFCODUFF_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AUF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UFCODICE=this.parent.oContained.w_MFCODUFF
     i_obj.ecpSave()
  endproc

  add object oMFCODATT_2_5 as StdField with uid="TNODRPVFFT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MFCODATT", cQueryName = "MFCODATT",;
    bObbl = .f. , nPag = 2, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice atto",;
    HelpContextID = 151606810,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=356, Top=353, InputMask=replicate('X',11)

  add object oMESE_2_8 as StdField with uid="KPWCJAHHDA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 205499194,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=356, Top=19, InputMask=replicate('X',2)

  add object oANNO_2_9 as StdField with uid="QEFDSTIJJN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 204862202,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=381, Top=19, InputMask=replicate('X',4)

  add object oStr_2_6 as StdString with uid="DUVZRBXUKS",Visible=.t., Left=9, Top=19,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="EWLCKETTJZ",Visible=.t., Left=226, Top=20,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="NFEBJOXPXM",Visible=.t., Left=43, Top=353,;
    Alignment=1, Width=91, Height=15,;
    Caption="Codice ufficio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="LRITFRLPUR",Visible=.t., Left=265, Top=353,;
    Alignment=1, Width=87, Height=15,;
    Caption="Codice atto:"  ;
  , bGlobalFont=.t.
enddefine
define class tgscg_amfPag3 as StdContainer
  Width  = 731
  height = 390
  stdWidth  = 731
  stdheight = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_3_1 as StdField with uid="UMTAXXYHVZ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 31479314,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=132, Top=26, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFCDSED1_3_2 as StdField with uid="CKABJGIONW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MFCDSED1", cQueryName = "MFCDSED1",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 233723383,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=108, Top=131, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED1"

  func oMFCDSED1_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED1_3_2.ecpDrop(oSource)
    this.Parent.oContained.link_3_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED1_3_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED1_3_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED1_3_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED1
     i_obj.ecpSave()
  endproc

  add object oMFCCONT1_3_3 as StdField with uid="SPGALMZAEZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MFCCONT1", cQueryName = "MFCCONT1",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 112023031,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=180, Top=131, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT1"

  func oMFCCONT1_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFCCONT1_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT1_3_3.ecpDrop(oSource)
    this.Parent.oContained.link_3_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT1_3_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT1_3_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT1_3_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT1
     i_obj.ecpSave()
  endproc

  add object oMFMINPS1_3_4 as StdField with uid="UZPQGAZZEX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MFMINPS1", cQueryName = "MFMINPS1",;
    bObbl = .f. , nPag = 3, value=space(22), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 144963063,;
   bGlobalFont=.t.,;
    Height=21, Width=200, Left=289, Top=131, InputMask=replicate('X',22)

  func oMFMINPS1_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  add object oMFDAMES1_3_6 as StdField with uid="LNKWVLBIEA",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MFDAMES1", cQueryName = "MFDAMES1",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 227239415,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=529, Top=131, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES1_3_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFDAMES1_3_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFDAMES1) and 0<=val(.w_MFDAMES1) and val(.w_MFDAMES1)<13)
    endwith
    return bRes
  endfunc

  add object oMFDAANN1_3_7 as StdField with uid="AKJYRQPYXF",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MFDAANN1", cQueryName = "MFDAANN1",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 97215991,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=557, Top=131, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN1_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFDAANN1_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFDAANN1) and (val(.w_MFDAANN1)>=1996  and val(.w_MFDAANN1)<=2050 or val(.w_MFDAANN1)=0))
    endwith
    return bRes
  endfunc

  add object oMF_AMES1_3_8 as StdField with uid="UGNEEGCWGA",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MF_AMES1", cQueryName = "MF_AMES1",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 227350007,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=625, Top=131, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES1_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMF_AMES1_3_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MF_AMES1)  and 0<=val(.w_MF_AMES1) and val(.w_MF_AMES1)<13)
    endwith
    return bRes
  endfunc

  add object oMFAN1_3_9 as StdField with uid="HMOIVJLGQJ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MFAN1", cQueryName = "MFAN1",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 153602618,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=654, Top=131, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN1_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFAN1_3_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (val(.w_MFAN1)=0 or val(.w_MFAN1)<=2050 and(val(.w_MFDAANN1)<val(.w_MFAN1)or .w_MFDAANN1=.w_MFAN1 and val(.w_MFDAMES1)<=val(.w_MF_AMES1)))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD1_3_10 as StdField with uid="TCTJSLGIBA",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MFIMPSD1", cQueryName = "MFIMPSD1",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 197637623,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=108, Top=254, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMPSD1_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  add object oMFIMPSC1_3_11 as StdField with uid="FPDWLHKRZP",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MFIMPSC1", cQueryName = "MFIMPSC1",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 197637623,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=289, Top=254, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMPSC1_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  add object oMFCDSED2_3_12 as StdField with uid="SXZYRVNBKS",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MFCDSED2", cQueryName = "MFCDSED2",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 233723384,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=108, Top=152, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED2"

  func oMFCDSED2_3_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED2_3_12.ecpDrop(oSource)
    this.Parent.oContained.link_3_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED2_3_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED2_3_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED2_3_12.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED2
     i_obj.ecpSave()
  endproc

  add object oMFCCONT2_3_13 as StdField with uid="AVTFDROKIG",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MFCCONT2", cQueryName = "MFCCONT2",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 112023032,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=180, Top=152, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT2"

  func oMFCCONT2_3_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFCCONT2_3_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT2_3_13.ecpDrop(oSource)
    this.Parent.oContained.link_3_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT2_3_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT2_3_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT2_3_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT2
     i_obj.ecpSave()
  endproc

  add object oMFMINPS2_3_14 as StdField with uid="TLRSSESNPI",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MFMINPS2", cQueryName = "MFMINPS2",;
    bObbl = .f. , nPag = 3, value=space(22), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 144963064,;
   bGlobalFont=.t.,;
    Height=21, Width=200, Left=289, Top=152, InputMask=replicate('X',22)

  func oMFMINPS2_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  add object oMFDAMES2_3_16 as StdField with uid="HZGQCKOMWJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MFDAMES2", cQueryName = "MFDAMES2",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 227239416,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=529, Top=152, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES2_3_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFDAMES2_3_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFDAMES2)  and 0<=val(.w_MFDAMES2) and val(.w_MFDAMES2)<13)
    endwith
    return bRes
  endfunc

  add object oMFDAANN2_3_17 as StdField with uid="WBAUDZABUC",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MFDAANN2", cQueryName = "MFDAANN2",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 97215992,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=557, Top=152, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN2_3_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFDAANN2_3_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFDAANN2) and (val(.w_MFDAANN2)>=1996  and val(.w_MFDAANN2)<=2050  or val(.w_MFDAANN2)=0))
    endwith
    return bRes
  endfunc

  add object oMF_AMES2_3_18 as StdField with uid="XRSTGKETUN",rtseq=30,rtrep=.f.,;
    cFormVar = "w_MF_AMES2", cQueryName = "MF_AMES2",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 227350008,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=625, Top=152, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES2_3_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMF_AMES2_3_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MF_AMES2)  and 0<=val(.w_MF_AMES2) and val(.w_MF_AMES2)<13)
    endwith
    return bRes
  endfunc

  add object oMFAN2_3_19 as StdField with uid="LQXSVVFIUC",rtseq=31,rtrep=.f.,;
    cFormVar = "w_MFAN2", cQueryName = "MFAN2",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 152554042,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=654, Top=152, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN2_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFAN2_3_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (val(.w_MFAN2)=0 or val(.w_MFAN2)<=2050 and(val(.w_MFDAANN2)<val(.w_MFAN2)or .w_MFDAANN2=.w_MFAN2 and val(.w_MFDAMES2)<=val(.w_MF_AMES2)))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD2_3_20 as StdField with uid="DPPNTFIXZF",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MFIMPSD2", cQueryName = "MFIMPSD2",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 197637624,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=108, Top=274, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMPSD2_3_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  add object oMFIMPSC2_3_21 as StdField with uid="DKBHHCVJQC",rtseq=33,rtrep=.f.,;
    cFormVar = "w_MFIMPSC2", cQueryName = "MFIMPSC2",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 197637624,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=289, Top=274, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMPSC2_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  add object oMFCDSED3_3_22 as StdField with uid="PMUMNAHIUI",rtseq=34,rtrep=.f.,;
    cFormVar = "w_MFCDSED3", cQueryName = "MFCDSED3",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 233723385,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=108, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED3"

  func oMFCDSED3_3_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED3_3_22.ecpDrop(oSource)
    this.Parent.oContained.link_3_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED3_3_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED3_3_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED3_3_22.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED3
     i_obj.ecpSave()
  endproc

  add object oMFCCONT3_3_23 as StdField with uid="MUKHFMMIGC",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MFCCONT3", cQueryName = "MFCCONT3",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 112023033,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=180, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT3"

  func oMFCCONT3_3_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFCCONT3_3_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT3_3_23.ecpDrop(oSource)
    this.Parent.oContained.link_3_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT3_3_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT3_3_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT3_3_23.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT3
     i_obj.ecpSave()
  endproc

  add object oMFMINPS3_3_24 as StdField with uid="CPVWKREMYW",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MFMINPS3", cQueryName = "MFMINPS3",;
    bObbl = .f. , nPag = 3, value=space(22), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 144963065,;
   bGlobalFont=.t.,;
    Height=21, Width=200, Left=289, Top=173, InputMask=replicate('X',22)

  func oMFMINPS3_3_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  add object oMFDAMES3_3_26 as StdField with uid="UCGDBCLFJK",rtseq=38,rtrep=.f.,;
    cFormVar = "w_MFDAMES3", cQueryName = "MFDAMES3",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 227239417,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=529, Top=173, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES3_3_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFDAMES3_3_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFDAMES3)  and 0<=val(.w_MFDAMES3) and val(.w_MFDAMES3)<13)
    endwith
    return bRes
  endfunc

  add object oMFDAANN3_3_27 as StdField with uid="PYVGITCREO",rtseq=39,rtrep=.f.,;
    cFormVar = "w_MFDAANN3", cQueryName = "MFDAANN3",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 97215993,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=557, Top=173, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN3_3_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFDAANN3_3_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFDAANN3) and (val(.w_MFDAANN3)>=1996  and val(.w_MFDAANN3)<=2050  or val(.w_MFDAANN3)=0))
    endwith
    return bRes
  endfunc

  add object oMF_AMES3_3_28 as StdField with uid="QTOOIBGIKI",rtseq=40,rtrep=.f.,;
    cFormVar = "w_MF_AMES3", cQueryName = "MF_AMES3",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 227350009,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=625, Top=173, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES3_3_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMF_AMES3_3_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MF_AMES3)  and 0<=val(.w_MF_AMES3) and val(.w_MF_AMES3)<13)
    endwith
    return bRes
  endfunc

  add object oMFAN3_3_29 as StdField with uid="FWZATATEDM",rtseq=41,rtrep=.f.,;
    cFormVar = "w_MFAN3", cQueryName = "MFAN3",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 151505466,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=654, Top=173, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN3_3_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFAN3_3_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (val(.w_MFAN3)=0 or val(.w_MFAN3)<=2050 and(val(.w_MFDAANN3)<val(.w_MFAN3)or .w_MFDAANN3=.w_MFAN3 and val(.w_MFDAMES3)<=val(.w_MF_AMES3)))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD3_3_30 as StdField with uid="NRTFEMYREN",rtseq=42,rtrep=.f.,;
    cFormVar = "w_MFIMPSD3", cQueryName = "MFIMPSD3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 197637625,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=108, Top=294, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMPSD3_3_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  add object oMFIMPSC3_3_31 as StdField with uid="RMAERXBXYN",rtseq=43,rtrep=.f.,;
    cFormVar = "w_MFIMPSC3", cQueryName = "MFIMPSC3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 197637625,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=289, Top=294, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMPSC3_3_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  add object oMFCDSED4_3_32 as StdField with uid="RENIINYJRG",rtseq=44,rtrep=.f.,;
    cFormVar = "w_MFCDSED4", cQueryName = "MFCDSED4",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 233723386,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=108, Top=194, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED4"

  func oMFCDSED4_3_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED4_3_32.ecpDrop(oSource)
    this.Parent.oContained.link_3_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED4_3_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED4_3_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED4_3_32.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED4
     i_obj.ecpSave()
  endproc

  add object oMFCCONT4_3_33 as StdField with uid="ZYWRGKWSCK",rtseq=45,rtrep=.f.,;
    cFormVar = "w_MFCCONT4", cQueryName = "MFCCONT4",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 112023034,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=180, Top=194, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT4"

  func oMFCCONT4_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFCCONT4_3_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT4_3_33.ecpDrop(oSource)
    this.Parent.oContained.link_3_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT4_3_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT4_3_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT4_3_33.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT4
     i_obj.ecpSave()
  endproc

  add object oMFMINPS4_3_34 as StdField with uid="YUDKXKBRDK",rtseq=46,rtrep=.f.,;
    cFormVar = "w_MFMINPS4", cQueryName = "MFMINPS4",;
    bObbl = .f. , nPag = 3, value=space(22), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 144963066,;
   bGlobalFont=.t.,;
    Height=21, Width=200, Left=289, Top=194, InputMask=replicate('X',22)

  func oMFMINPS4_3_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  add object oMFDAMES4_3_36 as StdField with uid="IUDRVUKOUP",rtseq=48,rtrep=.f.,;
    cFormVar = "w_MFDAMES4", cQueryName = "MFDAMES4",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 227239418,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=529, Top=194, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES4_3_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFDAMES4_3_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFDAMES4)  and 0<=val(.w_MFDAMES4) and val(.w_MFDAMES4)<13)
    endwith
    return bRes
  endfunc

  add object oMFDAANN4_3_37 as StdField with uid="HJXJWHWGKV",rtseq=49,rtrep=.f.,;
    cFormVar = "w_MFDAANN4", cQueryName = "MFDAANN4",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 97215994,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=557, Top=194, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN4_3_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFDAANN4_3_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFDAANN4) and (val(.w_MFDAANN4)>=1996  and val(.w_MFDAANN4)<=2050 or val(.w_MFDAANN4)=0))
    endwith
    return bRes
  endfunc

  add object oMF_AMES4_3_38 as StdField with uid="RGSVDGJKVT",rtseq=50,rtrep=.f.,;
    cFormVar = "w_MF_AMES4", cQueryName = "MF_AMES4",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 227350010,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=625, Top=194, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES4_3_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMF_AMES4_3_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MF_AMES4)  and 0<=val(.w_MF_AMES4) and val(.w_MF_AMES4)<13)
    endwith
    return bRes
  endfunc

  add object oMFAN4_3_39 as StdField with uid="CFHZNDFFLJ",rtseq=51,rtrep=.f.,;
    cFormVar = "w_MFAN4", cQueryName = "MFAN4",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 150456890,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=654, Top=194, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN4_3_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFAN4_3_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (val(.w_MFAN4)=0 or val(.w_MFAN4)<=2050 and(val(.w_MFDAANN4)<val(.w_MFAN4)or .w_MFDAANN4=.w_MFAN4 and val(.w_MFDAMES4)<=val(.w_MF_AMES4)))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD4_3_40 as StdField with uid="MNPGVFYFDQ",rtseq=52,rtrep=.f.,;
    cFormVar = "w_MFIMPSD4", cQueryName = "MFIMPSD4",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 197637626,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=108, Top=314, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMPSD4_3_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  add object oMFIMPSC4_3_41 as StdField with uid="UYJTGTJJGS",rtseq=53,rtrep=.f.,;
    cFormVar = "w_MFIMPSC4", cQueryName = "MFIMPSC4",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 197637626,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=289, Top=314, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMPSC4_3_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  add object oMFTOTDPS_3_55 as StdField with uid="XAUJQDXFLK",rtseq=54,rtrep=.f.,;
    cFormVar = "w_MFTOTDPS", cQueryName = "MFTOTDPS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 218785305,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=108, Top=340, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMFTOTCPS_3_56 as StdField with uid="MGBYMLHXPA",rtseq=55,rtrep=.f.,;
    cFormVar = "w_MFTOTCPS", cQueryName = "MFTOTCPS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 202008089,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=289, Top=340, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMFSALDPS_3_57 as StdField with uid="JXAPRSKVXL",rtseq=56,rtrep=.f.,;
    cFormVar = "w_MFSALDPS", cQueryName = "MFSALDPS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 209475097,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=445, Top=340, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMESE_3_63 as StdField with uid="BKZRGNZFUB",rtseq=57,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 205499194,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=367, Top=26, InputMask=replicate('X',2)

  add object oANNO_3_64 as StdField with uid="MJMYMEQJVT",rtseq=58,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 204862202,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=392, Top=26, InputMask=replicate('X',4)

  add object oStr_3_42 as StdString with uid="AYUEVOPXGG",Visible=.t., Left=108, Top=88,;
    Alignment=0, Width=42, Height=15,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_3_43 as StdString with uid="NBVWNRXNAQ",Visible=.t., Left=108, Top=105,;
    Alignment=0, Width=41, Height=15,;
    Caption="Sede"  ;
  , bGlobalFont=.t.

  add object oStr_3_44 as StdString with uid="RLQSQDFBLN",Visible=.t., Left=180, Top=88,;
    Alignment=0, Width=73, Height=15,;
    Caption="Causale"  ;
  , bGlobalFont=.t.

  add object oStr_3_45 as StdString with uid="VIPSVEHZFB",Visible=.t., Left=180, Top=105,;
    Alignment=0, Width=60, Height=15,;
    Caption="Contributo"  ;
  , bGlobalFont=.t.

  add object oStr_3_46 as StdString with uid="IYEELTZMSP",Visible=.t., Left=289, Top=89,;
    Alignment=0, Width=156, Height=15,;
    Caption="Matricola INPS/codice INPS/"  ;
  , bGlobalFont=.t.

  add object oStr_3_47 as StdString with uid="YEXOAXJKLC",Visible=.t., Left=289, Top=105,;
    Alignment=0, Width=104, Height=15,;
    Caption="Filiale azienda"  ;
  , bGlobalFont=.t.

  add object oStr_3_48 as StdString with uid="MFUFBDQZKN",Visible=.t., Left=550, Top=89,;
    Alignment=0, Width=123, Height=15,;
    Caption="Periodo di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_3_49 as StdString with uid="BOQOXBLNLK",Visible=.t., Left=529, Top=107,;
    Alignment=0, Width=85, Height=15,;
    Caption="da mm/aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_3_50 as StdString with uid="YZBSWXWFTX",Visible=.t., Left=625, Top=107,;
    Alignment=0, Width=68, Height=15,;
    Caption="a mm/aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_3_51 as StdString with uid="SYTXHRSHZV",Visible=.t., Left=108, Top=231,;
    Alignment=0, Width=172, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_3_52 as StdString with uid="HOACILSNNJ",Visible=.t., Left=289, Top=231,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_3_53 as StdString with uid="LGLKGDJLHM",Visible=.t., Left=8, Top=340,;
    Alignment=1, Width=94, Height=15,;
    Caption="Totale C"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_54 as StdString with uid="TZCTEEYMBI",Visible=.t., Left=272, Top=340,;
    Alignment=0, Width=15, Height=15,;
    Caption="D"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_58 as StdString with uid="XBSXKBBESY",Visible=.t., Left=458, Top=316,;
    Alignment=0, Width=79, Height=15,;
    Caption="Saldo (C-D)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_59 as StdString with uid="ASFKUCRSCQ",Visible=.t., Left=4, Top=26,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_61 as StdString with uid="KPNHKKCGVH",Visible=.t., Left=2, Top=52,;
    Alignment=0, Width=82, Height=18,;
    Caption="Sezione INPS"  ;
  , bGlobalFont=.t.

  add object oStr_3_62 as StdString with uid="IZXDXVWWJG",Visible=.t., Left=236, Top=27,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_65 as StdString with uid="IUWJYNNUUH",Visible=.t., Left=201, Top=258,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_65.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_3_66 as StdString with uid="PAIYKIBAII",Visible=.t., Left=201, Top=278,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_66.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_3_67 as StdString with uid="GTHUXQWWWV",Visible=.t., Left=201, Top=298,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_67.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_3_68 as StdString with uid="AVAWJLFTDQ",Visible=.t., Left=201, Top=318,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_68.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_3_69 as StdString with uid="NCUSYGMFKG",Visible=.t., Left=201, Top=344,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_69.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_3_70 as StdString with uid="JDDYBRKEGH",Visible=.t., Left=538, Top=343,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_70.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_3_71 as StdString with uid="BFSIPDSZXW",Visible=.t., Left=201, Top=258,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_3_71.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_3_72 as StdString with uid="SPRXVQXOBB",Visible=.t., Left=201, Top=278,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_3_72.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_3_73 as StdString with uid="UTNAKIBMCA",Visible=.t., Left=201, Top=298,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_3_73.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_3_74 as StdString with uid="EORLKXMXAA",Visible=.t., Left=201, Top=318,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_3_74.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_3_75 as StdString with uid="OYSMMGNXOZ",Visible=.t., Left=201, Top=344,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_3_75.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_3_76 as StdString with uid="OUXNHCDSPZ",Visible=.t., Left=384, Top=257,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_76.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_3_77 as StdString with uid="WMTFHTYBFH",Visible=.t., Left=384, Top=277,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_77.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_3_78 as StdString with uid="HZANTMOHNM",Visible=.t., Left=384, Top=297,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_78.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_3_79 as StdString with uid="XJYMGXECXR",Visible=.t., Left=384, Top=317,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_79.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_3_80 as StdString with uid="AMBMHUPOPU",Visible=.t., Left=384, Top=343,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_80.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_3_81 as StdString with uid="WXKGDWWBLR",Visible=.t., Left=384, Top=257,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_3_81.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_3_82 as StdString with uid="UUFHTUTULD",Visible=.t., Left=384, Top=277,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_3_82.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_3_83 as StdString with uid="NXAXPNFOLA",Visible=.t., Left=384, Top=297,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_3_83.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_3_84 as StdString with uid="QQHODHRTMM",Visible=.t., Left=384, Top=317,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_3_84.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_3_85 as StdString with uid="QMCWAMZTMU",Visible=.t., Left=384, Top=343,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_3_85.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_3_86 as StdString with uid="BFBEMXLCQA",Visible=.t., Left=538, Top=343,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_3_86.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oBox_3_60 as StdBox with uid="DBOPZLDODS",left=4, top=73, width=725,height=1
enddefine
define class tgscg_amfPag4 as StdContainer
  Width  = 731
  height = 390
  stdWidth  = 731
  stdheight = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_4_1 as StdField with uid="WUJQAZLJAD",rtseq=59,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 31479314,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=137, Top=22, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFCODRE1_4_2 as StdField with uid="AIZVRMHYSY",rtseq=60,rtrep=.f.,;
    cFormVar = "w_MFCODRE1", cQueryName = "MFCODRE1",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 168383991,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=32, Left=10, Top=113, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE1"

  func oMFCODRE1_4_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE1_4_2.ecpDrop(oSource)
    this.Parent.oContained.link_4_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE1_4_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE1_4_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regione",'',this.parent.oContained
  endproc
  proc oMFCODRE1_4_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE1
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE1_4_5 as StdField with uid="EOPPRNCXED",rtseq=61,rtrep=.f.,;
    cFormVar = "w_MFTRIRE1", cQueryName = "MFTRIRE1",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 173893111,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=79, Top=113, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE1"

  func oMFTRIRE1_4_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFTRIRE1_4_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE1_4_5.ecpDrop(oSource)
    this.Parent.oContained.link_4_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE1_4_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE1_4_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE1_4_5.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE1
     i_obj.ecpSave()
  endproc

  add object oMFRATRE1_4_6 as StdField with uid="WJSZNHKTPL",rtseq=62,rtrep=.f.,;
    cFormVar = "w_MFRATRE1", cQueryName = "MFRATRE1",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 184305143,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=128, Top=113, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE1_4_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFRATRE1_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE1) or left(.w_MFRATRE1,2)<=right(.w_MFRATRE1,2) and len(alltrim(.w_MFRATRE1))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNRE1_4_7 as StdField with uid="MUARMFNAEO",rtseq=63,rtrep=.f.,;
    cFormVar = "w_MFANNRE1", cQueryName = "MFANNRE1",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 178796023,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=225, Top=113, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE1_4_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFANNRE1_4_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE1) and (val(.w_MFANNRE1)>=1996  and val(.w_MFANNRE1)<=2050  or val(.w_MFANNRE1)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE1_4_8 as StdField with uid="DHWEMFLHFR",rtseq=64,rtrep=.f.,;
    cFormVar = "w_MFIMDRE1", cQueryName = "MFIMDRE1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168277495,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=298, Top=113, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDRE1_4_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  add object oMFIMCRE1_4_9 as StdField with uid="UKNUQBICLX",rtseq=65,rtrep=.f.,;
    cFormVar = "w_MFIMCRE1", cQueryName = "MFIMCRE1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167228919,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=479, Top=113, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCRE1_4_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  add object oMFCODRE2_4_17 as StdField with uid="ZYXFOZVGTH",rtseq=66,rtrep=.f.,;
    cFormVar = "w_MFCODRE2", cQueryName = "MFCODRE2",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 168383992,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=10, Top=133, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE2"

  func oMFCODRE2_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE2_4_17.ecpDrop(oSource)
    this.Parent.oContained.link_4_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE2_4_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE2_4_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regione",'',this.parent.oContained
  endproc
  proc oMFCODRE2_4_17.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE2
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE2_4_18 as StdField with uid="FKBLLTEVWV",rtseq=67,rtrep=.f.,;
    cFormVar = "w_MFTRIRE2", cQueryName = "MFTRIRE2",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 173893112,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=79, Top=133, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE2"

  func oMFTRIRE2_4_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFTRIRE2_4_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE2_4_18.ecpDrop(oSource)
    this.Parent.oContained.link_4_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE2_4_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE2_4_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE2_4_18.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE2
     i_obj.ecpSave()
  endproc

  add object oMFRATRE2_4_19 as StdField with uid="UQVLTAPSHS",rtseq=68,rtrep=.f.,;
    cFormVar = "w_MFRATRE2", cQueryName = "MFRATRE2",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 184305144,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=128, Top=133, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE2_4_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFRATRE2_4_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE2) or left(.w_MFRATRE2,2)<=right(.w_MFRATRE2,2) and len(alltrim(.w_MFRATRE2))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNRE2_4_20 as StdField with uid="HEIMJNJWRR",rtseq=69,rtrep=.f.,;
    cFormVar = "w_MFANNRE2", cQueryName = "MFANNRE2",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 178796024,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=225, Top=133, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE2_4_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFANNRE2_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE2) and (val(.w_MFANNRE2)>=1996  and val(.w_MFANNRE2)<=2050   or val(.w_MFANNRE2)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE2_4_21 as StdField with uid="XOVRSESBNM",rtseq=70,rtrep=.f.,;
    cFormVar = "w_MFIMDRE2", cQueryName = "MFIMDRE2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168277496,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=298, Top=133, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDRE2_4_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  add object oMFIMCRE2_4_22 as StdField with uid="RCLUETTAKO",rtseq=71,rtrep=.f.,;
    cFormVar = "w_MFIMCRE2", cQueryName = "MFIMCRE2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167228920,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=479, Top=133, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCRE2_4_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  add object oMFCODRE3_4_23 as StdField with uid="ZVBRNRSCQE",rtseq=72,rtrep=.f.,;
    cFormVar = "w_MFCODRE3", cQueryName = "MFCODRE3",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 168383993,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=10, Top=153, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE3"

  func oMFCODRE3_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE3_4_23.ecpDrop(oSource)
    this.Parent.oContained.link_4_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE3_4_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE3_4_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regione",'',this.parent.oContained
  endproc
  proc oMFCODRE3_4_23.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE3
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE3_4_24 as StdField with uid="YCEJPTYMRK",rtseq=73,rtrep=.f.,;
    cFormVar = "w_MFTRIRE3", cQueryName = "MFTRIRE3",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 173893113,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=79, Top=153, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE3"

  func oMFTRIRE3_4_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFTRIRE3_4_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE3_4_24.ecpDrop(oSource)
    this.Parent.oContained.link_4_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE3_4_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE3_4_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE3_4_24.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE3
     i_obj.ecpSave()
  endproc

  add object oMFRATRE3_4_25 as StdField with uid="TWMNLHYLWV",rtseq=74,rtrep=.f.,;
    cFormVar = "w_MFRATRE3", cQueryName = "MFRATRE3",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 184305145,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=128, Top=153, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE3_4_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFRATRE3_4_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE3) or left(.w_MFRATRE3,2)<=right(.w_MFRATRE3,2) and len(alltrim(.w_MFRATRE3))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNRE3_4_26 as StdField with uid="SUHDLNHBSQ",rtseq=75,rtrep=.f.,;
    cFormVar = "w_MFANNRE3", cQueryName = "MFANNRE3",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 178796025,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=225, Top=153, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE3_4_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFANNRE3_4_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE3) and (val(.w_MFANNRE3)>=1996  and val(.w_MFANNRE3)<=2050  or val(.w_MFANNRE3)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE3_4_27 as StdField with uid="CYOULTQZVW",rtseq=76,rtrep=.f.,;
    cFormVar = "w_MFIMDRE3", cQueryName = "MFIMDRE3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168277497,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=298, Top=153, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDRE3_4_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  add object oMFIMCRE3_4_28 as StdField with uid="QQOFTRRZLZ",rtseq=77,rtrep=.f.,;
    cFormVar = "w_MFIMCRE3", cQueryName = "MFIMCRE3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167228921,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=479, Top=153, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCRE3_4_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  add object oMFCODRE4_4_29 as StdField with uid="MBZNNKESOD",rtseq=78,rtrep=.f.,;
    cFormVar = "w_MFCODRE4", cQueryName = "MFCODRE4",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 168383994,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=10, Top=173, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE4"

  func oMFCODRE4_4_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE4_4_29.ecpDrop(oSource)
    this.Parent.oContained.link_4_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE4_4_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE4_4_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regione",'',this.parent.oContained
  endproc
  proc oMFCODRE4_4_29.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE4
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE4_4_30 as StdField with uid="MJAPDUZSJV",rtseq=79,rtrep=.f.,;
    cFormVar = "w_MFTRIRE4", cQueryName = "MFTRIRE4",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 173893114,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=79, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE4"

  func oMFTRIRE4_4_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFTRIRE4_4_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE4_4_30.ecpDrop(oSource)
    this.Parent.oContained.link_4_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE4_4_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE4_4_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE4_4_30.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE4
     i_obj.ecpSave()
  endproc

  add object oMFRATRE4_4_31 as StdField with uid="NDAJMIEPRZ",rtseq=80,rtrep=.f.,;
    cFormVar = "w_MFRATRE4", cQueryName = "MFRATRE4",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 184305146,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=128, Top=173, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE4_4_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFRATRE4_4_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE4) or left(.w_MFRATRE4,2)<=right(.w_MFRATRE4,2) and len(alltrim(.w_MFRATRE4))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNRE4_4_32 as StdField with uid="BIEFIFIRQX",rtseq=81,rtrep=.f.,;
    cFormVar = "w_MFANNRE4", cQueryName = "MFANNRE4",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 178796026,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=225, Top=173, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE4_4_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFANNRE4_4_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE4) and (val(.w_MFANNRE4)>=1996  and val(.w_MFANNRE4)<=2050   or val(.w_MFANNRE4)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE4_4_33 as StdField with uid="BCINHDFTDU",rtseq=82,rtrep=.f.,;
    cFormVar = "w_MFIMDRE4", cQueryName = "MFIMDRE4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 168277498,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=298, Top=173, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDRE4_4_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  add object oMFIMCRE4_4_34 as StdField with uid="GJHJZPGKWU",rtseq=83,rtrep=.f.,;
    cFormVar = "w_MFIMCRE4", cQueryName = "MFIMCRE4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167228922,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=479, Top=173, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCRE4_4_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  add object oMFTOTDRE_4_38 as StdField with uid="RWVYQDEOZY",rtseq=84,rtrep=.f.,;
    cFormVar = "w_MFTOTDRE", cQueryName = "MFTOTDRE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 218785291,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=298, Top=201, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMFTOTCRE_4_39 as StdField with uid="NEFFOASBPH",rtseq=85,rtrep=.f.,;
    cFormVar = "w_MFTOTCRE", cQueryName = "MFTOTCRE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 202008075,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=479, Top=201, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMFSALDRE_4_40 as StdField with uid="ZPSTXWEYRU",rtseq=86,rtrep=.f.,;
    cFormVar = "w_MFSALDRE", cQueryName = "MFSALDRE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 209475083,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=605, Top=201, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMFCODEL1_4_50 as StdField with uid="OWHRAGUTTI",rtseq=87,rtrep=.f.,;
    cFormVar = "w_MFCODEL1", cQueryName = "MFCODEL1",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente locale",;
    HelpContextID = 49719817,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=10, Top=281, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="ENTI_LOC", cZoomOnZoom="GSCG_AEL", oKey_1_1="ELCODICE", oKey_1_2="this.w_MFCODEL1"

  func oMFCODEL1_4_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_50('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODEL1_4_50.ecpDrop(oSource)
    this.Parent.oContained.link_4_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODEL1_4_50.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_LOC','*','ELCODICE',cp_AbsName(this.parent,'oMFCODEL1_4_50'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AEL',"Codici enti locali",'',this.parent.oContained
  endproc
  proc oMFCODEL1_4_50.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AEL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ELCODICE=this.parent.oContained.w_MFCODEL1
     i_obj.ecpSave()
  endproc

  add object oMFTRIEL1_4_51 as StdField with uid="YWULUYCAVU",rtseq=88,rtrep=.f.,;
    cFormVar = "w_MFTRIEL1", cQueryName = "MFTRIEL1",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 44210697,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=79, Top=282, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIEL1"

  func oMFTRIEL1_4_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL1))
    endwith
   endif
  endfunc

  func oMFTRIEL1_4_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_51('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIEL1_4_51.ecpDrop(oSource)
    this.Parent.oContained.link_4_51('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIEL1_4_51.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIEL1_4_51'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIEL1_4_51.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIEL1
     i_obj.ecpSave()
  endproc

  add object oMFRATEL1_4_52 as StdField with uid="CINHCWXKTX",rtseq=89,rtrep=.f.,;
    cFormVar = "w_MFRATEL1", cQueryName = "MFRATEL1",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 33798665,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=128, Top=282, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATEL1_4_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL1))
    endwith
   endif
  endfunc

  func oMFRATEL1_4_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATEL1) or left(.w_MFRATEL1,2)<=right(.w_MFRATEL1,2) and len(alltrim(.w_MFRATEL1))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNEL1_4_53 as StdField with uid="EUVLCLSTWA",rtseq=90,rtrep=.f.,;
    cFormVar = "w_MFANNEL1", cQueryName = "MFANNEL1",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 39307785,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=225, Top=282, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNEL1_4_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL1))
    endwith
   endif
  endfunc

  func oMFANNEL1_4_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNEL1) and (val(.w_MFANNEL1)>=1996  and val(.w_MFANNEL1)<=2050 or val(.w_MFANNEL1)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDEL1_4_54 as StdField with uid="XKUTIFTYDZ",rtseq=91,rtrep=.f.,;
    cFormVar = "w_MFIMDEL1", cQueryName = "MFIMDEL1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 49826313,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=298, Top=282, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDEL1_4_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL1))
    endwith
   endif
  endfunc

  add object oMFIMCEL1_4_55 as StdField with uid="XKVQMDBRXD",rtseq=92,rtrep=.f.,;
    cFormVar = "w_MFIMCEL1", cQueryName = "MFIMCEL1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 50874889,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=479, Top=282, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCEL1_4_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL1))
    endwith
   endif
  endfunc

  add object oMFCODEL2_4_56 as StdField with uid="PTOXXPRYPG",rtseq=93,rtrep=.f.,;
    cFormVar = "w_MFCODEL2", cQueryName = "MFCODEL2",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente locale",;
    HelpContextID = 49719816,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=10, Top=301, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="ENTI_LOC", cZoomOnZoom="GSCG_AEL", oKey_1_1="ELCODICE", oKey_1_2="this.w_MFCODEL2"

  func oMFCODEL2_4_56.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_56('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODEL2_4_56.ecpDrop(oSource)
    this.Parent.oContained.link_4_56('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODEL2_4_56.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_LOC','*','ELCODICE',cp_AbsName(this.parent,'oMFCODEL2_4_56'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AEL',"Codici enti locali",'',this.parent.oContained
  endproc
  proc oMFCODEL2_4_56.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AEL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ELCODICE=this.parent.oContained.w_MFCODEL2
     i_obj.ecpSave()
  endproc

  add object oMFTRIEL2_4_57 as StdField with uid="CWWCMTJXHJ",rtseq=94,rtrep=.f.,;
    cFormVar = "w_MFTRIEL2", cQueryName = "MFTRIEL2",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 44210696,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=79, Top=302, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIEL2"

  func oMFTRIEL2_4_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL2))
    endwith
   endif
  endfunc

  func oMFTRIEL2_4_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_57('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIEL2_4_57.ecpDrop(oSource)
    this.Parent.oContained.link_4_57('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIEL2_4_57.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIEL2_4_57'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIEL2_4_57.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIEL2
     i_obj.ecpSave()
  endproc

  add object oMFRATEL2_4_58 as StdField with uid="YQTOAEZSKO",rtseq=95,rtrep=.f.,;
    cFormVar = "w_MFRATEL2", cQueryName = "MFRATEL2",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 33798664,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=128, Top=302, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATEL2_4_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL2))
    endwith
   endif
  endfunc

  func oMFRATEL2_4_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATEL2) or left(.w_MFRATEL2,2)<=right(.w_MFRATEL2,2) and len(alltrim(.w_MFRATEL2))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNEL2_4_59 as StdField with uid="XQNJZHRXSR",rtseq=96,rtrep=.f.,;
    cFormVar = "w_MFANNEL2", cQueryName = "MFANNEL2",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 39307784,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=225, Top=302, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNEL2_4_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL2))
    endwith
   endif
  endfunc

  func oMFANNEL2_4_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNEL2) and (val(.w_MFANNEL2)>=1996  and val(.w_MFANNEL2)<=2050  or val(.w_MFANNEL2)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDEL2_4_60 as StdField with uid="JXREHKAWZX",rtseq=97,rtrep=.f.,;
    cFormVar = "w_MFIMDEL2", cQueryName = "MFIMDEL2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 49826312,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=298, Top=302, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDEL2_4_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL2))
    endwith
   endif
  endfunc

  add object oMFIMCEL2_4_61 as StdField with uid="KUOXMSJXNF",rtseq=98,rtrep=.f.,;
    cFormVar = "w_MFIMCEL2", cQueryName = "MFIMCEL2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 50874888,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=479, Top=302, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCEL2_4_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL2))
    endwith
   endif
  endfunc

  add object oMFCODEL3_4_62 as StdField with uid="ONWMPBUXNL",rtseq=99,rtrep=.f.,;
    cFormVar = "w_MFCODEL3", cQueryName = "MFCODEL3",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente locale",;
    HelpContextID = 49719815,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=10, Top=321, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="ENTI_LOC", cZoomOnZoom="GSCG_AEL", oKey_1_1="ELCODICE", oKey_1_2="this.w_MFCODEL3"

  func oMFCODEL3_4_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_62('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODEL3_4_62.ecpDrop(oSource)
    this.Parent.oContained.link_4_62('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODEL3_4_62.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_LOC','*','ELCODICE',cp_AbsName(this.parent,'oMFCODEL3_4_62'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AEL',"Codici enti locali",'',this.parent.oContained
  endproc
  proc oMFCODEL3_4_62.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AEL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ELCODICE=this.parent.oContained.w_MFCODEL3
     i_obj.ecpSave()
  endproc

  add object oMFTRIEL3_4_63 as StdField with uid="EZLDQOIGDH",rtseq=100,rtrep=.f.,;
    cFormVar = "w_MFTRIEL3", cQueryName = "MFTRIEL3",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 44210695,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=79, Top=322, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIEL3"

  func oMFTRIEL3_4_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL3))
    endwith
   endif
  endfunc

  func oMFTRIEL3_4_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_63('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIEL3_4_63.ecpDrop(oSource)
    this.Parent.oContained.link_4_63('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIEL3_4_63.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIEL3_4_63'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIEL3_4_63.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIEL3
     i_obj.ecpSave()
  endproc

  add object oMFRATEL3_4_64 as StdField with uid="TMOXVNBKKN",rtseq=101,rtrep=.f.,;
    cFormVar = "w_MFRATEL3", cQueryName = "MFRATEL3",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 33798663,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=128, Top=322, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATEL3_4_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL3))
    endwith
   endif
  endfunc

  func oMFRATEL3_4_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATEL3) or left(.w_MFRATEL3,2)<=right(.w_MFRATEL3,2) and len(alltrim(.w_MFRATEL3))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNEL3_4_65 as StdField with uid="SZXNAMQCGT",rtseq=102,rtrep=.f.,;
    cFormVar = "w_MFANNEL3", cQueryName = "MFANNEL3",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 39307783,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=225, Top=322, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNEL3_4_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL3))
    endwith
   endif
  endfunc

  func oMFANNEL3_4_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNEL3) and (val(.w_MFANNEL3)>=1996  and val(.w_MFANNEL3)<=2050  or val(.w_MFANNEL3)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDEL3_4_66 as StdField with uid="GPEJSBGZGU",rtseq=103,rtrep=.f.,;
    cFormVar = "w_MFIMDEL3", cQueryName = "MFIMDEL3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 49826311,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=298, Top=322, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDEL3_4_66.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL3))
    endwith
   endif
  endfunc

  add object oMFIMCEL3_4_67 as StdField with uid="ARMYKADZBN",rtseq=104,rtrep=.f.,;
    cFormVar = "w_MFIMCEL3", cQueryName = "MFIMCEL3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 50874887,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=479, Top=322, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCEL3_4_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODEL3))
    endwith
   endif
  endfunc

  add object oMFTOTDEL_4_68 as StdField with uid="TUTWDVKUWK",rtseq=105,rtrep=.f.,;
    cFormVar = "w_MFTOTDEL", cQueryName = "MFTOTDEL",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 218785298,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=298, Top=350, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMFTOTCEL_4_69 as StdField with uid="VEHYDMMZRC",rtseq=106,rtrep=.f.,;
    cFormVar = "w_MFTOTCEL", cQueryName = "MFTOTCEL",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 202008082,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=479, Top=350, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMFSALDEL_4_70 as StdField with uid="QMUWYTXWWP",rtseq=107,rtrep=.f.,;
    cFormVar = "w_MFSALDEL", cQueryName = "MFSALDEL",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 209475090,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=605, Top=350, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMESE_4_80 as StdField with uid="QDHEDGHHIW",rtseq=108,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 205499194,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=374, Top=23, InputMask=replicate('X',2)

  add object oANNO_4_81 as StdField with uid="EBJMOQWTXY",rtseq=109,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 204862202,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=399, Top=23, InputMask=replicate('X',4)

  add object oStr_4_3 as StdString with uid="JMYELXYYLS",Visible=.t., Left=10, Top=76,;
    Alignment=0, Width=42, Height=15,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_4_4 as StdString with uid="QOAKWPNUWO",Visible=.t., Left=10, Top=92,;
    Alignment=0, Width=47, Height=15,;
    Caption="Regione"  ;
  , bGlobalFont=.t.

  add object oStr_4_10 as StdString with uid="AZYSIRFDIY",Visible=.t., Left=79, Top=77,;
    Alignment=0, Width=42, Height=15,;
    Caption="Cod."  ;
  , bGlobalFont=.t.

  add object oStr_4_11 as StdString with uid="UJCCQXXPKY",Visible=.t., Left=79, Top=92,;
    Alignment=0, Width=45, Height=15,;
    Caption="Tributo"  ;
  , bGlobalFont=.t.

  add object oStr_4_12 as StdString with uid="YQGQUJLBET",Visible=.t., Left=128, Top=92,;
    Alignment=0, Width=89, Height=15,;
    Caption="Rateazione"  ;
  , bGlobalFont=.t.

  add object oStr_4_13 as StdString with uid="TVCKYQGKBA",Visible=.t., Left=225, Top=75,;
    Alignment=0, Width=41, Height=15,;
    Caption="Anno di"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="ETADKKPXVC",Visible=.t., Left=225, Top=91,;
    Alignment=0, Width=64, Height=15,;
    Caption="Riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_4_15 as StdString with uid="MVBKBDZKBM",Visible=.t., Left=298, Top=90,;
    Alignment=0, Width=172, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="NHHSLKGDGU",Visible=.t., Left=479, Top=90,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_4_35 as StdString with uid="KEBVMVBZNJ",Visible=.t., Left=201, Top=203,;
    Alignment=1, Width=93, Height=15,;
    Caption="Totale E"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_36 as StdString with uid="FIWKHDGIJY",Visible=.t., Left=620, Top=176,;
    Alignment=0, Width=76, Height=15,;
    Caption="Saldo (E-F)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_37 as StdString with uid="WDIGBVHLVI",Visible=.t., Left=450, Top=202,;
    Alignment=0, Width=12, Height=15,;
    Caption="F"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_41 as StdString with uid="TGTDJGUXMP",Visible=.t., Left=10, Top=246,;
    Alignment=0, Width=42, Height=15,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_4_42 as StdString with uid="BDFHYYPANL",Visible=.t., Left=79, Top=247,;
    Alignment=0, Width=42, Height=15,;
    Caption="Cod."  ;
  , bGlobalFont=.t.

  add object oStr_4_43 as StdString with uid="JMHMIVRGRY",Visible=.t., Left=79, Top=262,;
    Alignment=0, Width=45, Height=15,;
    Caption="Tributo"  ;
  , bGlobalFont=.t.

  add object oStr_4_44 as StdString with uid="CIJLWALMQA",Visible=.t., Left=128, Top=262,;
    Alignment=0, Width=89, Height=15,;
    Caption="Rateazione"  ;
  , bGlobalFont=.t.

  add object oStr_4_45 as StdString with uid="WWXINBJEYD",Visible=.t., Left=225, Top=245,;
    Alignment=0, Width=41, Height=15,;
    Caption="Anno di"  ;
  , bGlobalFont=.t.

  add object oStr_4_46 as StdString with uid="IQJFHZJSGK",Visible=.t., Left=225, Top=261,;
    Alignment=0, Width=64, Height=15,;
    Caption="Riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_4_47 as StdString with uid="ZISLZJKDPJ",Visible=.t., Left=298, Top=260,;
    Alignment=0, Width=172, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_4_48 as StdString with uid="SLLANVSIZO",Visible=.t., Left=479, Top=260,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_4_49 as StdString with uid="WCRKUKLJYO",Visible=.t., Left=10, Top=262,;
    Alignment=0, Width=66, Height=15,;
    Caption="Ente loc."  ;
  , bGlobalFont=.t.

  add object oStr_4_71 as StdString with uid="DDEDDRYFQY",Visible=.t., Left=200, Top=352,;
    Alignment=1, Width=94, Height=15,;
    Caption="Totale G"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_72 as StdString with uid="CBEVQCIWID",Visible=.t., Left=630, Top=326,;
    Alignment=0, Width=79, Height=15,;
    Caption="Saldo (G-H)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_73 as StdString with uid="YVEZCPMINK",Visible=.t., Left=450, Top=352,;
    Alignment=0, Width=12, Height=15,;
    Caption="H"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_76 as StdString with uid="WRQXUQEJZJ",Visible=.t., Left=9, Top=23,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_77 as StdString with uid="XBBOUQJYEG",Visible=.t., Left=243, Top=25,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_78 as StdString with uid="DORQAGMARM",Visible=.t., Left=6, Top=45,;
    Alignment=0, Width=106, Height=18,;
    Caption="Sezione regioni"  ;
  , bGlobalFont=.t.

  add object oStr_4_79 as StdString with uid="WCXQOPNHBO",Visible=.t., Left=7, Top=216,;
    Alignment=0, Width=151, Height=18,;
    Caption="Sezione enti locali"  ;
  , bGlobalFont=.t.

  add object oStr_4_82 as StdString with uid="PVGWHDBXSU",Visible=.t., Left=393, Top=116,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_82.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_83 as StdString with uid="EDUGFTWRRG",Visible=.t., Left=393, Top=136,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_83.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_84 as StdString with uid="MYIBPYHILJ",Visible=.t., Left=393, Top=156,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_84.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_85 as StdString with uid="LYJCBDUKKO",Visible=.t., Left=393, Top=176,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_85.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_86 as StdString with uid="VMOBLCOOFW",Visible=.t., Left=393, Top=116,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_86.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_87 as StdString with uid="XHXYEEXYBI",Visible=.t., Left=393, Top=136,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_87.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_88 as StdString with uid="OQLSWKBQWR",Visible=.t., Left=393, Top=156,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_88.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_89 as StdString with uid="KWDDCIWCQS",Visible=.t., Left=393, Top=176,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_89.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_90 as StdString with uid="QROGXMVFMA",Visible=.t., Left=575, Top=116,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_90.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_91 as StdString with uid="IEBMZTGHDQ",Visible=.t., Left=575, Top=136,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_91.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_92 as StdString with uid="XSIYYXRRFY",Visible=.t., Left=575, Top=156,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_92.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_93 as StdString with uid="NOIYTRUWXK",Visible=.t., Left=575, Top=176,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_93.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_94 as StdString with uid="VXBNZFQCBA",Visible=.t., Left=575, Top=116,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_94.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_95 as StdString with uid="VNPGNBWJOR",Visible=.t., Left=575, Top=136,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_95.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_96 as StdString with uid="KEPOMKWOUX",Visible=.t., Left=575, Top=156,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_96.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_97 as StdString with uid="NIQLOYGLUW",Visible=.t., Left=575, Top=176,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_97.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_98 as StdString with uid="JJSYQRJBVH",Visible=.t., Left=393, Top=204,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_98.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_99 as StdString with uid="SUFHJDCLQW",Visible=.t., Left=393, Top=204,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_99.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_100 as StdString with uid="GKMWROMCDG",Visible=.t., Left=575, Top=203,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_100.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_101 as StdString with uid="UUYKVZJDFP",Visible=.t., Left=575, Top=203,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_101.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_102 as StdString with uid="RBOIEVFCSY",Visible=.t., Left=699, Top=203,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_102.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_103 as StdString with uid="QEYLSOHOYF",Visible=.t., Left=699, Top=203,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_103.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_104 as StdString with uid="DCFAEKWRYN",Visible=.t., Left=393, Top=352,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_104.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_105 as StdString with uid="IUGGWKZAON",Visible=.t., Left=393, Top=352,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_105.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_106 as StdString with uid="HNBVCQATLW",Visible=.t., Left=393, Top=285,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_106.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_107 as StdString with uid="ZZSXCILBWT",Visible=.t., Left=393, Top=305,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_107.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_108 as StdString with uid="TGBATOJRUX",Visible=.t., Left=393, Top=325,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_108.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_109 as StdString with uid="GPNWOTUFCY",Visible=.t., Left=393, Top=285,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_109.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_110 as StdString with uid="DRERJHUXUS",Visible=.t., Left=393, Top=305,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_110.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_111 as StdString with uid="ZPWKVLOYAI",Visible=.t., Left=393, Top=325,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_111.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_112 as StdString with uid="NAILRMUQOH",Visible=.t., Left=575, Top=285,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_112.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_113 as StdString with uid="PTIZBAWUVE",Visible=.t., Left=575, Top=305,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_113.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_114 as StdString with uid="BZUQPFTVCS",Visible=.t., Left=575, Top=325,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_114.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_115 as StdString with uid="AXREDNDXMV",Visible=.t., Left=575, Top=285,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_115.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_116 as StdString with uid="SSJMEUXGIF",Visible=.t., Left=575, Top=305,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_116.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_117 as StdString with uid="YRHSNHQHSO",Visible=.t., Left=575, Top=325,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_117.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_118 as StdString with uid="MAASSDWRCV",Visible=.t., Left=575, Top=352,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_118.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_119 as StdString with uid="JRCCJUHYAE",Visible=.t., Left=575, Top=352,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_119.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_4_120 as StdString with uid="BVLFJALQFS",Visible=.t., Left=699, Top=353,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_4_120.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_4_121 as StdString with uid="PWQPJBBNGU",Visible=.t., Left=699, Top=353,;
    Alignment=0, Width=30, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_4_121.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oBox_4_74 as StdBox with uid="KHJPYHBFQV",left=3, top=65, width=722,height=1

  add object oBox_4_75 as StdBox with uid="QWPUGCRDON",left=5, top=236, width=722,height=1
enddefine
define class tgscg_amfPag5 as StdContainer
  Width  = 731
  height = 390
  stdWidth  = 731
  stdheight = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_5_1 as StdField with uid="UHIVYKYYLY",rtseq=110,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 5, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 31479314,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=141, Top=24, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFSINAI1_5_2 as StdField with uid="VJCJNRLRZM",rtseq=111,rtrep=.f.,;
    cFormVar = "w_MFSINAI1", cQueryName = "MFSINAI1",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INAIL",;
    HelpContextID = 106670601,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=7, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SE_INAIL", cZoomOnZoom="GSCG_ASI", oKey_1_1="SICODICE", oKey_1_2="this.w_MFSINAI1"

  func oMFSINAI1_5_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSINAI1_5_2.ecpDrop(oSource)
    this.Parent.oContained.link_5_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSINAI1_5_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SE_INAIL','*','SICODICE',cp_AbsName(this.parent,'oMFSINAI1_5_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ASI',"Codici sede INAIL",'',this.parent.oContained
  endproc
  proc oMFSINAI1_5_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ASI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SICODICE=this.parent.oContained.w_MFSINAI1
     i_obj.ecpSave()
  endproc

  add object oMF_NPOS1_5_3 as StdField with uid="IOZFIFMCFH",rtseq=112,rtrep=.f.,;
    cFormVar = "w_MF_NPOS1", cQueryName = "MF_NPOS1",;
    bObbl = .f. , nPag = 5, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Numero posizione assicurativa",;
    HelpContextID = 130684407,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=84, Top=173, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8)

  func oMF_NPOS1_5_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMF_PACC1_5_4 as StdField with uid="ZRBNXSIDVP",rtseq=113,rtrep=.f.,;
    cFormVar = "w_MF_PACC1", cQueryName = "MF_PACC1",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "C.C. posizione assicurativa",;
    HelpContextID = 182195703,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=156, Top=173, InputMask=replicate('X',2)

  func oMF_PACC1_5_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMF_NRIF1_5_5 as StdField with uid="MNCHMZYNTX",rtseq=114,rtrep=.f.,;
    cFormVar = "w_MF_NRIF1", cQueryName = "MF_NRIF1",;
    bObbl = .f. , nPag = 5, value=space(7), bMultilanguage =  .f.,;
    ToolTipText = "Numero di riferimento",;
    HelpContextID = 236317193,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=194, Top=173, cSayPict='"9999999"', cGetPict='"9999999"', InputMask=replicate('X',7)

  func oMF_NRIF1_5_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFCAUSA1_5_6 as StdField with uid="UYLFQZKBVP",rtseq=115,rtrep=.f.,;
    cFormVar = "w_MFCAUSA1", cQueryName = "MFCAUSA1",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Causale",;
    HelpContextID = 202069495,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=260, Top=173, InputMask=replicate('X',2)

  func oMFCAUSA1_5_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFIMDIL1_5_7 as StdField with uid="KHMXTBZGND",rtseq=116,rtrep=.f.,;
    cFormVar = "w_MFIMDIL1", cQueryName = "MFIMDIL1",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 251152905,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=312, Top=173, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDIL1_5_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFIMCIL1_5_8 as StdField with uid="LCWCGXKFZS",rtseq=117,rtrep=.f.,;
    cFormVar = "w_MFIMCIL1", cQueryName = "MFIMCIL1",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 252201481,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=487, Top=173, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCIL1_5_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFSINAI2_5_9 as StdField with uid="CRJUFFVUOV",rtseq=118,rtrep=.f.,;
    cFormVar = "w_MFSINAI2", cQueryName = "MFSINAI2",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INAIL",;
    HelpContextID = 106670600,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=7, Top=193, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SE_INAIL", cZoomOnZoom="GSCG_ASI", oKey_1_1="SICODICE", oKey_1_2="this.w_MFSINAI2"

  func oMFSINAI2_5_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSINAI2_5_9.ecpDrop(oSource)
    this.Parent.oContained.link_5_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSINAI2_5_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SE_INAIL','*','SICODICE',cp_AbsName(this.parent,'oMFSINAI2_5_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ASI',"Codici sede INAIL",'',this.parent.oContained
  endproc
  proc oMFSINAI2_5_9.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ASI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SICODICE=this.parent.oContained.w_MFSINAI2
     i_obj.ecpSave()
  endproc

  add object oMF_NPOS2_5_10 as StdField with uid="FUEQFYRIPH",rtseq=119,rtrep=.f.,;
    cFormVar = "w_MF_NPOS2", cQueryName = "MF_NPOS2",;
    bObbl = .f. , nPag = 5, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Numero posizione assicurativa",;
    HelpContextID = 130684408,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=84, Top=193, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8)

  func oMF_NPOS2_5_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMF_PACC2_5_11 as StdField with uid="TUSBHCGAVC",rtseq=120,rtrep=.f.,;
    cFormVar = "w_MF_PACC2", cQueryName = "MF_PACC2",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "C.C. posizione assicurativa",;
    HelpContextID = 182195704,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=156, Top=193, InputMask=replicate('X',2)

  func oMF_PACC2_5_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMF_NRIF2_5_12 as StdField with uid="DJFDGNQMDU",rtseq=121,rtrep=.f.,;
    cFormVar = "w_MF_NRIF2", cQueryName = "MF_NRIF2",;
    bObbl = .f. , nPag = 5, value=space(7), bMultilanguage =  .f.,;
    ToolTipText = "Numero di riferimento",;
    HelpContextID = 236317192,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=194, Top=193, cSayPict='"9999999"', cGetPict='"9999999"', InputMask=replicate('X',7)

  func oMF_NRIF2_5_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFCAUSA2_5_13 as StdField with uid="HNWHAKLLNT",rtseq=122,rtrep=.f.,;
    cFormVar = "w_MFCAUSA2", cQueryName = "MFCAUSA2",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Causale",;
    HelpContextID = 202069496,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=260, Top=193, InputMask=replicate('X',2)

  func oMFCAUSA2_5_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFIMDIL2_5_14 as StdField with uid="KEXJJEVGQG",rtseq=123,rtrep=.f.,;
    cFormVar = "w_MFIMDIL2", cQueryName = "MFIMDIL2",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 251152904,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=312, Top=193, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDIL2_5_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFIMCIL2_5_15 as StdField with uid="HAECOJYPAK",rtseq=124,rtrep=.f.,;
    cFormVar = "w_MFIMCIL2", cQueryName = "MFIMCIL2",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 252201480,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=487, Top=193, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCIL2_5_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFSINAI3_5_16 as StdField with uid="XBUILCQVSF",rtseq=125,rtrep=.f.,;
    cFormVar = "w_MFSINAI3", cQueryName = "MFSINAI3",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INAIL",;
    HelpContextID = 106670599,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=7, Top=213, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SE_INAIL", cZoomOnZoom="GSCG_ASI", oKey_1_1="SICODICE", oKey_1_2="this.w_MFSINAI3"

  func oMFSINAI3_5_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSINAI3_5_16.ecpDrop(oSource)
    this.Parent.oContained.link_5_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSINAI3_5_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SE_INAIL','*','SICODICE',cp_AbsName(this.parent,'oMFSINAI3_5_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ASI',"Codici sede INAIL",'',this.parent.oContained
  endproc
  proc oMFSINAI3_5_16.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ASI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SICODICE=this.parent.oContained.w_MFSINAI3
     i_obj.ecpSave()
  endproc

  add object oMF_NPOS3_5_17 as StdField with uid="KYNIYEUWFN",rtseq=126,rtrep=.f.,;
    cFormVar = "w_MF_NPOS3", cQueryName = "MF_NPOS3",;
    bObbl = .f. , nPag = 5, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Numero posizione assicurativa",;
    HelpContextID = 130684409,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=84, Top=213, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8)

  func oMF_NPOS3_5_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMF_PACC3_5_18 as StdField with uid="RMJFRMESCX",rtseq=127,rtrep=.f.,;
    cFormVar = "w_MF_PACC3", cQueryName = "MF_PACC3",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "C.C. posizione assicurativa",;
    HelpContextID = 182195705,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=156, Top=213, InputMask=replicate('X',2)

  func oMF_PACC3_5_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMF_NRIF3_5_19 as StdField with uid="PAMPHMQGFW",rtseq=128,rtrep=.f.,;
    cFormVar = "w_MF_NRIF3", cQueryName = "MF_NRIF3",;
    bObbl = .f. , nPag = 5, value=space(7), bMultilanguage =  .f.,;
    ToolTipText = "Numero di riferimento",;
    HelpContextID = 236317191,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=194, Top=213, cSayPict='"9999999"', cGetPict='"9999999"', InputMask=replicate('X',7)

  func oMF_NRIF3_5_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFCAUSA3_5_20 as StdField with uid="BWTVBLXRGC",rtseq=129,rtrep=.f.,;
    cFormVar = "w_MFCAUSA3", cQueryName = "MFCAUSA3",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Causale",;
    HelpContextID = 202069497,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=260, Top=213, InputMask=replicate('X',2)

  func oMFCAUSA3_5_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFIMDIL3_5_21 as StdField with uid="ZSLZCWMSTL",rtseq=130,rtrep=.f.,;
    cFormVar = "w_MFIMDIL3", cQueryName = "MFIMDIL3",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 251152903,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=312, Top=213, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDIL3_5_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFIMCIL3_5_22 as StdField with uid="RWHHSNNRDK",rtseq=131,rtrep=.f.,;
    cFormVar = "w_MFIMCIL3", cQueryName = "MFIMCIL3",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 252201479,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=487, Top=213, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCIL3_5_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFTDINAI_5_32 as StdField with uid="PSBGPKLIAA",rtseq=132,rtrep=.f.,;
    cFormVar = "w_MFTDINAI", cQueryName = "MFTDINAI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 105866767,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=312, Top=242, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMFTCINAI_5_33 as StdField with uid="XTGTLEVZXZ",rtseq=133,rtrep=.f.,;
    cFormVar = "w_MFTCINAI", cQueryName = "MFTCINAI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 105801231,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=487, Top=242, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMFSALINA_5_34 as StdField with uid="DZWWWGTXFX",rtseq=134,rtrep=.f.,;
    cFormVar = "w_MFSALINA", cQueryName = "MFSALINA",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 24925703,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=611, Top=242, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMESE_5_42 as StdField with uid="MLVBGKPOHZ",rtseq=135,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 205499194,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=379, Top=25, InputMask=replicate('X',2)

  add object oANNO_5_43 as StdField with uid="JTXVHPAOPB",rtseq=136,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 204862202,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=404, Top=25, InputMask=replicate('X',4)

  add object oStr_5_23 as StdString with uid="TZTHCVPSZI",Visible=.t., Left=84, Top=151,;
    Alignment=0, Width=51, Height=15,;
    Caption="Numero"  ;
  , bGlobalFont=.t.

  add object oStr_5_24 as StdString with uid="FGFSRZKDQK",Visible=.t., Left=156, Top=153,;
    Alignment=0, Width=24, Height=15,;
    Caption="C.C."  ;
  , bGlobalFont=.t.

  add object oStr_5_25 as StdString with uid="XMJKQNQCQQ",Visible=.t., Left=312, Top=153,;
    Alignment=0, Width=172, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_5_26 as StdString with uid="QPZQHUEURM",Visible=.t., Left=487, Top=153,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_5_27 as StdString with uid="YCITJFFMQZ",Visible=.t., Left=7, Top=151,;
    Alignment=0, Width=71, Height=15,;
    Caption="Cod.sede"  ;
  , bGlobalFont=.t.

  add object oStr_5_28 as StdString with uid="DUQPKICCWN",Visible=.t., Left=84, Top=135,;
    Alignment=0, Width=109, Height=15,;
    Caption="Posiz.assicurativa"  ;
  , bGlobalFont=.t.

  add object oStr_5_29 as StdString with uid="AZKLFKKYGI",Visible=.t., Left=194, Top=135,;
    Alignment=0, Width=73, Height=15,;
    Caption="Numero di"  ;
  , bGlobalFont=.t.

  add object oStr_5_30 as StdString with uid="OPXQMQDPMJ",Visible=.t., Left=194, Top=153,;
    Alignment=0, Width=59, Height=15,;
    Caption="riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_5_31 as StdString with uid="GWKFEEEWGD",Visible=.t., Left=260, Top=153,;
    Alignment=0, Width=45, Height=15,;
    Caption="Caus."  ;
  , bGlobalFont=.t.

  add object oStr_5_35 as StdString with uid="CAXNCWDVNP",Visible=.t., Left=255, Top=244,;
    Alignment=0, Width=89, Height=15,;
    Caption="Totale I"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_36 as StdString with uid="YQYWHXKWQX",Visible=.t., Left=625, Top=220,;
    Alignment=0, Width=73, Height=15,;
    Caption="Saldo (I-L)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_37 as StdString with uid="HTBHHGCOXH",Visible=.t., Left=467, Top=244,;
    Alignment=0, Width=17, Height=15,;
    Caption="L"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_38 as StdString with uid="UICRRZTZSD",Visible=.t., Left=7, Top=66,;
    Alignment=0, Width=302, Height=18,;
    Caption="Altri enti previdenziali ed assicurativi - INAIL"  ;
  , bGlobalFont=.t.

  add object oStr_5_40 as StdString with uid="VXPRYYUHZU",Visible=.t., Left=13, Top=25,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_41 as StdString with uid="HJKKMLLUEV",Visible=.t., Left=247, Top=27,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_5_44 as StdString with uid="VPDHINRQAN",Visible=.t., Left=406, Top=244,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_5_44.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_5_45 as StdString with uid="DZZAQTGPOI",Visible=.t., Left=406, Top=244,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_5_45.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_5_46 as StdString with uid="JYFKZYQLYG",Visible=.t., Left=581, Top=244,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_5_46.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_5_47 as StdString with uid="KWDQOKVAZE",Visible=.t., Left=581, Top=244,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_5_47.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_5_48 as StdString with uid="KUUGIUTQAZ",Visible=.t., Left=704, Top=245,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_5_48.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_5_49 as StdString with uid="YWQFOUCVXB",Visible=.t., Left=704, Top=245,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_5_49.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_5_50 as StdString with uid="TKBEVAOPLH",Visible=.t., Left=581, Top=176,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_5_50.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_5_51 as StdString with uid="WUQMYXZLGS",Visible=.t., Left=581, Top=196,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_5_51.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_5_52 as StdString with uid="SRACREJDOA",Visible=.t., Left=581, Top=216,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_5_52.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_5_53 as StdString with uid="PQSKTMHCHV",Visible=.t., Left=581, Top=176,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_5_53.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_5_54 as StdString with uid="DNYGAVXAOB",Visible=.t., Left=581, Top=196,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_5_54.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_5_55 as StdString with uid="FLHBLMVERV",Visible=.t., Left=581, Top=216,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_5_55.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_5_56 as StdString with uid="ZAYEOZEXWP",Visible=.t., Left=406, Top=176,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_5_56.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_5_57 as StdString with uid="EEUNFFVSZP",Visible=.t., Left=406, Top=196,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_5_57.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_5_58 as StdString with uid="POXOJYPLVW",Visible=.t., Left=406, Top=216,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_5_58.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_5_59 as StdString with uid="XPKNUJVLOJ",Visible=.t., Left=406, Top=176,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_5_59.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_5_60 as StdString with uid="DNCCELNZUA",Visible=.t., Left=406, Top=196,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_5_60.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_5_61 as StdString with uid="SOSKXAOCLA",Visible=.t., Left=406, Top=216,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_5_61.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oBox_5_39 as StdBox with uid="DLSNHYHKXJ",left=5, top=86, width=716,height=1
enddefine
define class tgscg_amfPag6 as StdContainer
  Width  = 731
  height = 390
  stdWidth  = 731
  stdheight = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_6_1 as StdField with uid="BOVCFDHJYO",rtseq=137,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 6, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 31479314,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=137, Top=26, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFCDENTE_6_3 as StdField with uid="ZPTITZNOZB",rtseq=138,rtrep=.f.,;
    cFormVar = "w_MFCDENTE", cQueryName = "MFCDENTE",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente previdenziale",;
    HelpContextID = 101602827,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=87, Top=114, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_PREV", cZoomOnZoom="GSCG_ACP", oKey_1_1="CPCODICE", oKey_1_2="this.w_MFCDENTE"

  func oMFCDENTE_6_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_3('Part',this)
      if .not. empty(.w_MFSDENT1)
        bRes2=.link_6_4('Full')
      endif
      if .not. empty(.w_MFCCOAE1)
        bRes2=.link_6_5('Full')
      endif
      if .not. empty(.w_MFSDENT2)
        bRes2=.link_6_13('Full')
      endif
      if .not. empty(.w_MFCCOAE2)
        bRes2=.link_6_14('Full')
      endif
      if .not. empty(.w_MFSDENT3)
        bRes2=.link_6_22('Full')
      endif
      if .not. empty(.w_MFCCOAE3)
        bRes2=.link_6_23('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMFCDENTE_6_3.ecpDrop(oSource)
    this.Parent.oContained.link_6_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDENTE_6_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_PREV','*','CPCODICE',cp_AbsName(this.parent,'oMFCDENTE_6_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACP',"Codici enti previdenziali",'',this.parent.oContained
  endproc
  proc oMFCDENTE_6_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CPCODICE=this.parent.oContained.w_MFCDENTE
     i_obj.ecpSave()
  endproc

  add object oMFSDENT1_6_4 as StdField with uid="ANDZMYNXGS",rtseq=139,rtrep=.f.,;
    cFormVar = "w_MFSDENT1", cQueryName = "MFSDENT1",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 101668343,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=451, Top=144, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_AEN", cZoomOnZoom="GSCG_MSE", oKey_1_1="SECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="SECODSED", oKey_2_2="this.w_MFSDENT1"

  func oMFSDENT1_6_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oMFSDENT1_6_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSDENT1_6_4.ecpDrop(oSource)
    this.Parent.oContained.link_6_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSDENT1_6_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SED_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'SED_AEN','*','SECODENT,SECODSED',cp_AbsName(this.parent,'oMFSDENT1_6_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MSE',"Codici sede",'',this.parent.oContained
  endproc
  proc oMFSDENT1_6_4.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MSE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SECODENT=w_MFCDENTE
     i_obj.w_SECODSED=this.parent.oContained.w_MFSDENT1
     i_obj.ecpSave()
  endproc

  add object oMFCCOAE1_6_5 as StdField with uid="QKCNTKXASE",rtseq=140,rtrep=.f.,;
    cFormVar = "w_MFCCOAE1", cQueryName = "MFCCOAE1",;
    bObbl = .t. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo",;
    HelpContextID = 162354679,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=512, Top=144, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_AEN", cZoomOnZoom="GSCG_MAE", oKey_1_1="AECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="AECAUSEN", oKey_2_2="this.w_MFCCOAE1"

  func oMFCCOAE1_6_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
    endwith
   endif
  endfunc

  func oMFCCOAE1_6_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCOAE1_6_5.ecpDrop(oSource)
    this.Parent.oContained.link_6_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCOAE1_6_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAU_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(this.parent,'oMFCCOAE1_6_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MAE',"Causali contributo",'',this.parent.oContained
  endproc
  proc oMFCCOAE1_6_5.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MAE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.AECODENT=w_MFCDENTE
     i_obj.w_AECAUSEN=this.parent.oContained.w_MFCCOAE1
     i_obj.ecpSave()
  endproc

  add object oMFCDPOS1_6_6 as StdField with uid="FVFMMSXZYE",rtseq=141,rtrep=.f.,;
    cFormVar = "w_MFCDPOS1", cQueryName = "MFCDPOS1",;
    bObbl = .f. , nPag = 6, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Codice posizione",;
    HelpContextID = 129914359,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=10, Top=247, InputMask=replicate('X',12)

  func oMFCDPOS1_6_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
    endwith
   endif
  endfunc

  add object oMFMSINE1_6_7 as StdField with uid="CQUCVMEFND",rtseq=142,rtrep=.f.,;
    cFormVar = "w_MFMSINE1", cQueryName = "MFMSINE1",;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 106821111,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=115, Top=247, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSINE1_6_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
    endwith
   endif
  endfunc

  func oMFMSINE1_6_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFMSINE1) and 0<= val(.w_MFMSINE1) and val(.w_MFMSINE1) < 13)
    endwith
    return bRes
  endfunc

  add object oMFANINE1_6_8 as StdField with uid="BSCZHMAMEK",rtseq=143,rtrep=.f.,;
    cFormVar = "w_MFANINE1", cQueryName = "MFANINE1",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 106444279,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=144, Top=247, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANINE1_6_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
    endwith
   endif
  endfunc

  func oMFANINE1_6_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANINE1) and (val(.w_MFANINE1)>=1996  and  val(.w_MFANINE1)<=2050 or val(.w_MFANINE1)=0))
    endwith
    return bRes
  endfunc

  add object oMFMSFIE1_6_9 as StdField with uid="NJWCQBUUWE",rtseq=144,rtrep=.f.,;
    cFormVar = "w_MFMSFIE1", cQueryName = "MFMSFIE1",;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 19789303,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=207, Top=247, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSFIE1_6_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
    endwith
   endif
  endfunc

  func oMFMSFIE1_6_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFMSFIE1) and 0<=val(.w_MFMSFIE1) and val(.w_MFMSFIE1)<13)
    endwith
    return bRes
  endfunc

  add object oMFANF1_6_10 as StdField with uid="SCAIVPDBNL",rtseq=145,rtrep=.f.,;
    cFormVar = "w_MFANF1", cQueryName = "MFANF1",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 153630150,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=235, Top=247, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANF1_6_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
    endwith
   endif
  endfunc

  func oMFANF1_6_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANF1)and(val(.w_MFANF1)=0or(val(.w_MFANINE1)<val(.w_MFANF1)or .w_MFANINE1=.w_MFANF1 and val(.w_MFMSINE1)<=val(.w_MFMSFIE1))))
    endwith
    return bRes
  endfunc

  add object oMFIMDAE1_6_11 as StdField with uid="SVMKLLZVBI",rtseq=146,rtrep=.f.,;
    cFormVar = "w_MFIMDAE1", cQueryName = "MFIMDAE1",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 151500279,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=284, Top=247, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDAE1_6_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
    endwith
   endif
  endfunc

  add object oMFIMCAE1_6_12 as StdField with uid="WGFGYVXVVM",rtseq=147,rtrep=.f.,;
    cFormVar = "w_MFIMCAE1", cQueryName = "MFIMCAE1",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 150451703,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=461, Top=247, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCAE1_6_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT1))
    endwith
   endif
  endfunc

  add object oMFSDENT2_6_13 as StdField with uid="TZPIAHWJQU",rtseq=148,rtrep=.f.,;
    cFormVar = "w_MFSDENT2", cQueryName = "MFSDENT2",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 101668344,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=451, Top=164, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_AEN", cZoomOnZoom="GSCG_MSE", oKey_1_1="SECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="SECODSED", oKey_2_2="this.w_MFSDENT2"

  func oMFSDENT2_6_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oMFSDENT2_6_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSDENT2_6_13.ecpDrop(oSource)
    this.Parent.oContained.link_6_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSDENT2_6_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SED_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'SED_AEN','*','SECODENT,SECODSED',cp_AbsName(this.parent,'oMFSDENT2_6_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MSE',"Codici sede",'',this.parent.oContained
  endproc
  proc oMFSDENT2_6_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MSE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SECODENT=w_MFCDENTE
     i_obj.w_SECODSED=this.parent.oContained.w_MFSDENT2
     i_obj.ecpSave()
  endproc

  add object oMFCCOAE2_6_14 as StdField with uid="NOTHDZQOTX",rtseq=149,rtrep=.f.,;
    cFormVar = "w_MFCCOAE2", cQueryName = "MFCCOAE2",;
    bObbl = .t. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo",;
    HelpContextID = 162354680,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=512, Top=164, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_AEN", cZoomOnZoom="GSCG_MAE", oKey_1_1="AECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="AECAUSEN", oKey_2_2="this.w_MFCCOAE2"

  func oMFCCOAE2_6_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
    endwith
   endif
  endfunc

  func oMFCCOAE2_6_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCOAE2_6_14.ecpDrop(oSource)
    this.Parent.oContained.link_6_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCOAE2_6_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAU_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(this.parent,'oMFCCOAE2_6_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MAE',"Causali contributo",'',this.parent.oContained
  endproc
  proc oMFCCOAE2_6_14.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MAE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.AECODENT=w_MFCDENTE
     i_obj.w_AECAUSEN=this.parent.oContained.w_MFCCOAE2
     i_obj.ecpSave()
  endproc

  add object oMFCDPOS2_6_15 as StdField with uid="YTUJHCWFYN",rtseq=150,rtrep=.f.,;
    cFormVar = "w_MFCDPOS2", cQueryName = "MFCDPOS2",;
    bObbl = .f. , nPag = 6, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Codice posizione",;
    HelpContextID = 129914360,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=10, Top=267, InputMask=replicate('X',12)

  func oMFCDPOS2_6_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
    endwith
   endif
  endfunc

  add object oMFMSINE2_6_16 as StdField with uid="SHSFTEZLCU",rtseq=151,rtrep=.f.,;
    cFormVar = "w_MFMSINE2", cQueryName = "MFMSINE2",;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 106821112,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=115, Top=267, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSINE2_6_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
    endwith
   endif
  endfunc

  func oMFMSINE2_6_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFMSINE2) and 0<= val(.w_MFMSINE2) and val(.w_MFMSINE2) < 13)
    endwith
    return bRes
  endfunc

  add object oMFANINE2_6_17 as StdField with uid="MCAXPQUTQA",rtseq=152,rtrep=.f.,;
    cFormVar = "w_MFANINE2", cQueryName = "MFANINE2",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 106444280,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=144, Top=267, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANINE2_6_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
    endwith
   endif
  endfunc

  func oMFANINE2_6_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANINE2)  and (val(.w_MFANINE2)>=1996  and val(.w_MFANINE2)<=2050  or val(.w_MFANINE2)=0))
    endwith
    return bRes
  endfunc

  add object oMFMSFIE2_6_18 as StdField with uid="YMMTXOEUQT",rtseq=153,rtrep=.f.,;
    cFormVar = "w_MFMSFIE2", cQueryName = "MFMSFIE2",;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 19789304,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=207, Top=267, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSFIE2_6_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
    endwith
   endif
  endfunc

  func oMFMSFIE2_6_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFMSFIE2)  and 0<=val(.w_MFMSFIE2) and val(.w_MFMSFIE2)<13)
    endwith
    return bRes
  endfunc

  add object oMFANF2_6_19 as StdField with uid="CXTDVEYWGH",rtseq=154,rtrep=.f.,;
    cFormVar = "w_MFANF2", cQueryName = "MFANF2",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 170407366,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=234, Top=267, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANF2_6_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
    endwith
   endif
  endfunc

  func oMFANF2_6_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANF2)and(val(.w_MFANF2)=0or(val(.w_MFANINE2)<val(.w_MFANF2)or .w_MFANINE2=.w_MFANF2 and val(.w_MFMSINE2)<=val(.w_MFMSFIE2))))
    endwith
    return bRes
  endfunc

  add object oMFIMDAE2_6_20 as StdField with uid="JSQAGPZNHA",rtseq=155,rtrep=.f.,;
    cFormVar = "w_MFIMDAE2", cQueryName = "MFIMDAE2",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 151500280,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=284, Top=267, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDAE2_6_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
    endwith
   endif
  endfunc

  add object oMFIMCAE2_6_21 as StdField with uid="NZPTLZBXME",rtseq=156,rtrep=.f.,;
    cFormVar = "w_MFIMCAE2", cQueryName = "MFIMCAE2",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 150451704,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=461, Top=267, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCAE2_6_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT2))
    endwith
   endif
  endfunc

  add object oMFSDENT3_6_22 as StdField with uid="AXMESYVVWG",rtseq=157,rtrep=.f.,;
    cFormVar = "w_MFSDENT3", cQueryName = "MFSDENT3",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 101668345,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=451, Top=184, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_AEN", cZoomOnZoom="GSCG_MSE", oKey_1_1="SECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="SECODSED", oKey_2_2="this.w_MFSDENT3"

  func oMFSDENT3_6_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oMFSDENT3_6_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSDENT3_6_22.ecpDrop(oSource)
    this.Parent.oContained.link_6_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSDENT3_6_22.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SED_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'SED_AEN','*','SECODENT,SECODSED',cp_AbsName(this.parent,'oMFSDENT3_6_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MSE',"Codici sede",'',this.parent.oContained
  endproc
  proc oMFSDENT3_6_22.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MSE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SECODENT=w_MFCDENTE
     i_obj.w_SECODSED=this.parent.oContained.w_MFSDENT3
     i_obj.ecpSave()
  endproc

  add object oMFCCOAE3_6_23 as StdField with uid="ELMAQZBORI",rtseq=158,rtrep=.f.,;
    cFormVar = "w_MFCCOAE3", cQueryName = "MFCCOAE3",;
    bObbl = .t. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo",;
    HelpContextID = 162354681,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=512, Top=184, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_AEN", cZoomOnZoom="GSCG_MAE", oKey_1_1="AECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="AECAUSEN", oKey_2_2="this.w_MFCCOAE3"

  func oMFCCOAE3_6_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
    endwith
   endif
  endfunc

  func oMFCCOAE3_6_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCOAE3_6_23.ecpDrop(oSource)
    this.Parent.oContained.link_6_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCOAE3_6_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAU_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(this.parent,'oMFCCOAE3_6_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MAE',"Causali contributo",'',this.parent.oContained
  endproc
  proc oMFCCOAE3_6_23.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MAE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.AECODENT=w_MFCDENTE
     i_obj.w_AECAUSEN=this.parent.oContained.w_MFCCOAE3
     i_obj.ecpSave()
  endproc

  add object oMFCDPOS3_6_24 as StdField with uid="AFOHWSESVA",rtseq=159,rtrep=.f.,;
    cFormVar = "w_MFCDPOS3", cQueryName = "MFCDPOS3",;
    bObbl = .f. , nPag = 6, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Codice posizione",;
    HelpContextID = 129914361,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=10, Top=287, InputMask=replicate('X',12)

  func oMFCDPOS3_6_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
    endwith
   endif
  endfunc

  add object oMFMSINE3_6_25 as StdField with uid="APZRDTGVPV",rtseq=160,rtrep=.f.,;
    cFormVar = "w_MFMSINE3", cQueryName = "MFMSINE3",;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 106821113,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=115, Top=287, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSINE3_6_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
    endwith
   endif
  endfunc

  func oMFMSINE3_6_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFMSINE3)and 0<= val(.w_MFMSINE3) and val(.w_MFMSINE3) < 13)
    endwith
    return bRes
  endfunc

  add object oMFANINE3_6_26 as StdField with uid="PYOZBLAJXU",rtseq=161,rtrep=.f.,;
    cFormVar = "w_MFANINE3", cQueryName = "MFANINE3",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 106444281,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=144, Top=287, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANINE3_6_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
    endwith
   endif
  endfunc

  func oMFANINE3_6_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANINE3)  and (val(.w_MFANINE3)>=1996  and val(.w_MFANINE3)<=2050  or val(.w_MFANINE3)=0))
    endwith
    return bRes
  endfunc

  add object oMFMSFIE3_6_27 as StdField with uid="UMDQHQSZXO",rtseq=162,rtrep=.f.,;
    cFormVar = "w_MFMSFIE3", cQueryName = "MFMSFIE3",;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 19789305,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=207, Top=287, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSFIE3_6_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
    endwith
   endif
  endfunc

  func oMFMSFIE3_6_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFMSFIE3) and 0<=val(.w_MFMSFIE3) and val(.w_MFMSFIE3)<13)
    endwith
    return bRes
  endfunc

  add object oMFANF3_6_28 as StdField with uid="PDMYDRQCRI",rtseq=163,rtrep=.f.,;
    cFormVar = "w_MFANF3", cQueryName = "MFANF3",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 187184582,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=235, Top=287, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANF3_6_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
    endwith
   endif
  endfunc

  func oMFANF3_6_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANF3)and(val(.w_MFANF3)=0or(val(.w_MFANINE3)<val(.w_MFANF3)or .w_MFANINE3=.w_MFANF3 and val(.w_MFMSINE3)<=val(.w_MFMSFIE3))))
    endwith
    return bRes
  endfunc

  add object oMFIMDAE3_6_29 as StdField with uid="BKUMWHYTAI",rtseq=164,rtrep=.f.,;
    cFormVar = "w_MFIMDAE3", cQueryName = "MFIMDAE3",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 151500281,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=284, Top=287, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMDAE3_6_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
    endwith
   endif
  endfunc

  add object oMFIMCAE3_6_30 as StdField with uid="YYFSODGOKL",rtseq=165,rtrep=.f.,;
    cFormVar = "w_MFIMCAE3", cQueryName = "MFIMCAE3",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 150451705,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=461, Top=287, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  func oMFIMCAE3_6_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE) and not empty(.w_MFSDENT3))
    endwith
   endif
  endfunc

  add object oMFTDAENT_6_44 as StdField with uid="UBZHZRCDWP",rtseq=166,rtrep=.f.,;
    cFormVar = "w_MFTDAENT", cQueryName = "MFTDAENT",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 53516774,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=284, Top=318, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMFTCAENT_6_45 as StdField with uid="VGJIBWUKLL",rtseq=167,rtrep=.f.,;
    cFormVar = "w_MFTCAENT", cQueryName = "MFTCAENT",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 53582310,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=461, Top=318, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oMFSALAEN_6_46 as StdField with uid="NWKWVKNQGD",rtseq=168,rtrep=.f.,;
    cFormVar = "w_MFSALAEN", cQueryName = "MFSALAEN",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 159143444,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=592, Top=318, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"

  add object oDESAENTE_6_51 as StdField with uid="MHNJWSROFX",rtseq=169,rtrep=.f.,;
    cFormVar = "w_DESAENTE", cQueryName = "DESAENTE",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 101471355,;
   bGlobalFont=.t.,;
    Height=21, Width=185, Left=226, Top=114, InputMask=replicate('X',30)

  add object oMESE_6_54 as StdField with uid="BPSADYMFGH",rtseq=170,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 205499194,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=366, Top=27, InputMask=replicate('X',2)

  add object oANNO_6_55 as StdField with uid="XFUVSGDJBX",rtseq=171,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 204862202,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=391, Top=27, InputMask=replicate('X',4)

  add object oStr_6_2 as StdString with uid="XUFRHUWVCA",Visible=.t., Left=12, Top=114,;
    Alignment=1, Width=70, Height=18,;
    Caption="Codice ente:"  ;
  , bGlobalFont=.t.

  add object oStr_6_31 as StdString with uid="QFXBNLJTNB",Visible=.t., Left=512, Top=108,;
    Alignment=0, Width=73, Height=15,;
    Caption="Causale"  ;
  , bGlobalFont=.t.

  add object oStr_6_32 as StdString with uid="WHBZJRPMBM",Visible=.t., Left=512, Top=124,;
    Alignment=0, Width=62, Height=15,;
    Caption="Contributo"  ;
  , bGlobalFont=.t.

  add object oStr_6_33 as StdString with uid="EQPYVSBGGO",Visible=.t., Left=10, Top=211,;
    Alignment=0, Width=42, Height=15,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_6_34 as StdString with uid="IYBABYHCRZ",Visible=.t., Left=451, Top=108,;
    Alignment=0, Width=42, Height=15,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_6_35 as StdString with uid="BODVKKPKSH",Visible=.t., Left=451, Top=123,;
    Alignment=0, Width=41, Height=15,;
    Caption="Sede"  ;
  , bGlobalFont=.t.

  add object oStr_6_36 as StdString with uid="UYRKEQZZXQ",Visible=.t., Left=10, Top=227,;
    Alignment=0, Width=54, Height=15,;
    Caption="Posizione"  ;
  , bGlobalFont=.t.

  add object oStr_6_37 as StdString with uid="QDZVJIFSLW",Visible=.t., Left=130, Top=211,;
    Alignment=0, Width=123, Height=15,;
    Caption="Periodo di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_6_38 as StdString with uid="WFWPRZSWDC",Visible=.t., Left=115, Top=227,;
    Alignment=0, Width=85, Height=15,;
    Caption="da mm/aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_6_39 as StdString with uid="UMJFKCLLIF",Visible=.t., Left=207, Top=227,;
    Alignment=0, Width=68, Height=15,;
    Caption="a mm/aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_6_40 as StdString with uid="SAKCHWIMZS",Visible=.t., Left=282, Top=227,;
    Alignment=0, Width=172, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_6_41 as StdString with uid="ISMZUWQKMW",Visible=.t., Left=461, Top=227,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_6_42 as StdString with uid="TXOVKRQMIL",Visible=.t., Left=183, Top=320,;
    Alignment=1, Width=96, Height=15,;
    Caption="Totale M"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_43 as StdString with uid="ZHSIEZUFVM",Visible=.t., Left=439, Top=320,;
    Alignment=0, Width=15, Height=15,;
    Caption="N"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_47 as StdString with uid="LTURFGEZTI",Visible=.t., Left=618, Top=293,;
    Alignment=0, Width=81, Height=15,;
    Caption="Saldo (M-N)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_48 as StdString with uid="DDIXCLCYBG",Visible=.t., Left=10, Top=27,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_49 as StdString with uid="CTXZAWNSVI",Visible=.t., Left=3, Top=67,;
    Alignment=0, Width=263, Height=18,;
    Caption="Altri enti previdenziali ed assicurativi"  ;
  , bGlobalFont=.t.

  add object oStr_6_52 as StdString with uid="RVEZJNDSJX",Visible=.t., Left=140, Top=114,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_6_53 as StdString with uid="JWKSCNPXLY",Visible=.t., Left=235, Top=28,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_6_56 as StdString with uid="IIHFVRWXVM",Visible=.t., Left=378, Top=250,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_6_56.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_6_57 as StdString with uid="GCWYPPENVY",Visible=.t., Left=378, Top=270,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_6_57.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_6_58 as StdString with uid="ELMDEZJMSA",Visible=.t., Left=378, Top=290,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_6_58.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_6_59 as StdString with uid="RFIYBVQGBK",Visible=.t., Left=378, Top=250,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_6_59.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_6_60 as StdString with uid="MJGIDVXUEY",Visible=.t., Left=378, Top=270,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_6_60.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_6_61 as StdString with uid="BOLVPANORE",Visible=.t., Left=378, Top=290,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_6_61.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_6_62 as StdString with uid="OFEOICWCBE",Visible=.t., Left=555, Top=250,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_6_62.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_6_63 as StdString with uid="VXSAFKUFJA",Visible=.t., Left=555, Top=270,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_6_63.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_6_64 as StdString with uid="GZFGTWAEXO",Visible=.t., Left=555, Top=290,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_6_64.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_6_65 as StdString with uid="HFTBGLCRZG",Visible=.t., Left=555, Top=250,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_6_65.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_6_66 as StdString with uid="MZASHKMJDK",Visible=.t., Left=555, Top=270,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_6_66.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_6_67 as StdString with uid="DKMSGRSBCJ",Visible=.t., Left=555, Top=290,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_6_67.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_6_68 as StdString with uid="RRFEBRBBEQ",Visible=.t., Left=378, Top=322,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_6_68.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_6_69 as StdString with uid="SDTXJUQWYY",Visible=.t., Left=378, Top=322,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_6_69.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_6_70 as StdString with uid="QMSBYKHKOL",Visible=.t., Left=555, Top=321,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_6_70.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_6_71 as StdString with uid="KGSKYPWCMP",Visible=.t., Left=555, Top=321,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_6_71.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oStr_6_72 as StdString with uid="IWZLOIEKBY",Visible=.t., Left=686, Top=320,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_6_72.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_6_73 as StdString with uid="BNPPZBQILJ",Visible=.t., Left=686, Top=320,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
  , bGlobalFont=.t.

  func oStr_6_73.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oBox_6_50 as StdBox with uid="OSEQIEOXBC",left=13, top=89, width=681,height=0
enddefine
define class tgscg_amfPag7 as StdContainer
  Width  = 731
  height = 390
  stdWidth  = 731
  stdheight = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_7_1 as StdField with uid="FWNKVSYQIO",rtseq=172,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 7, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 31479314,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=143, Top=26, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFSALFIN_7_2 as StdField with uid="XPRIIKCUAV",rtseq=173,rtrep=.f.,;
    cFormVar = "w_MFSALFIN", cQueryName = "MFSALFIN",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25405932,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=93, Left=582, Top=31, cSayPict="'@Z '+ v_PV(32)", cGetPict="'@Z '+ v_GV(32)"


  add object oLinkPC_7_3 as stdDynamicChildContainer with uid="CDRKTLRCVN",left=5, top=60, width=695, height=271, bOnScreen=.t.;



  add object oObj_7_8 as cp_runprogram with uid="GFQIHJVWOZ",left=3, top=403, width=265,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BMF",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=7;
    , HelpContextID = 236052966

  add object oMESE_7_9 as StdField with uid="VBABMPRSRE",rtseq=174,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 205499194,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=369, Top=27, InputMask=replicate('X',2)

  add object oANNO_7_10 as StdField with uid="DRPQPJTNKA",rtseq=175,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 204862202,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=394, Top=27, InputMask=replicate('X',4)


  add object oBtn_7_13 as StdButton with uid="BKXUQHPGVI",left=627, top=342, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=7;
    , ToolTipText = "Premere per salvare i risultati e stampare il modello";
    , HelpContextID = 210351386;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_13.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_7_14 as StdButton with uid="IKRZAGEXKT",left=679, top=342, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=7;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 203062714;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_7_4 as StdString with uid="VKEGRLLTGP",Visible=.t., Left=475, Top=31,;
    Alignment=1, Width=100, Height=15,;
    Caption="SALDO FINALE:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_5 as StdString with uid="BYCBSRREFL",Visible=.t., Left=14, Top=28,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_6 as StdString with uid="SCVOUDRHJZ",Visible=.t., Left=239, Top=28,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_7_11 as StdString with uid="XSSKDZKBIL",Visible=.t., Left=677, Top=33,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_7_11.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODEUR)
    endwith
  endfunc

  add object oStr_7_12 as StdString with uid="NUCVUZZUNW",Visible=.t., Left=677, Top=33,;
    Alignment=0, Width=24, Height=15,;
    Caption=",00"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_7_12.mHide()
    with this.Parent.oContained
      return (.w_MFVALUTA=g_CODLIR)
    endwith
  endfunc

  add object oBox_7_7 as StdBox with uid="VHFAPMTUTR",left=492, top=55, width=205,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_amf','MOD_PAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MFSERIAL=MOD_PAG.MFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
