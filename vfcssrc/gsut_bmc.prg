* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bmc                                                        *
*              CREAZIONE CACHE IMMAGINI                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-06-10                                                      *
* Last revis.: 2008-09-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pUSEDTHEME
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bmc",oParentObject,m.pUSEDTHEME)
return(i_retval)

define class tgsut_bmc as StdBatch
  * --- Local variables
  pUSEDTHEME = 0
  w_ORIGDIR = space(254)
  w_NIMM = 0
  w_IDXATT = 0
  w_FILEATT = space(254)
  w_NTHEMES = 0
  w_NPROPS = 0
  w_GARBAGE = space(1)
  w_GRAYBMP = space(1)
  w_THEMEATT = 0
  w_NDIMATT = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_NTHEMES = i_ThemesManager.GetMaxThemes()
    this.w_NPROPS = i_ThemesManager.GetMaxProps()
    * --- && ###start remove from setup###
    if g_ADHOCONE
      this.w_ORIGDIR = FULLPATH("..\..")
    else
      * --- && ###end remove from setup###
      this.w_ORIGDIR = FULLPATH("..")
      * --- && ###start remove from setup###
    endif
    * --- && ###end remove from setup###
    * --- Costruzione immagini array temi del tema attuale
    this.w_IDXATT = 1
    i_ThemesManager.theMENUMBER = this.pUSEDTHEME
    do while this.w_IDXATT<=this.w_NPROPS
      this.w_GARBAGE = i_ThemesManager.GetProp( this.w_IDXATT )
    enddo
    * --- Costruzione immagini array temi degli altri temi
    this.w_THEMEATT = 1
    do while this.w_THEMEATT<=this.w_NTHEMES
      if this.w_THEMEATT<>this.pUSEDTHEME
        this.w_IDXATT = 1
        do while this.w_IDXATT<=this.w_NPROPS
          i_ThemesManager.theMENUMBER = this.w_THEMEATT
          this.w_GARBAGE = i_ThemesManager.GetProp( this.w_IDXATT )
        enddo
      endif
    enddo
    DIMENSION L_FILES(1)
    VisitaDir(this.w_ORIGDIR,"*.ico",@L_FILES,.T.)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    RELEASE L_FILES
    DIMENSION L_FILES(1)
    VisitaDir(this.w_ORIGDIR,"*.bmp",@L_FILES,.T.)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_NIMM = ALEN(L_FILES, 1)
    this.w_IDXATT = 1
    do while this.w_IDXATT<=this.w_NIMM
      if "A" $ L_FILES[ this.w_IDXATT, 5 ]
        this.w_FILEATT = ALLTRIM( L_ICONS[ this.w_IDXATT, 1 ] )
        * --- Genero le varie versioni dell'immagine
        this.w_NDIMATT = 16
        do while this.w_NDIMATT<=32
          this.w_GARBAGE = i_ThemesManager.RetBmpFromIco( this.w_FILEATT, this.w_NDIMATT)
          this.w_GRAYBMP = i_ThemesManager.GrayScaleBmp( this.w_GARBAGE, this.w_NDIMATT)
          this.w_NDIMATT = this.w_NDIMATT + 8
        enddo
      endif
      this.w_IDXATT = this.w_IDXATT + 1
    enddo
  endproc


  proc Init(oParentObject,pUSEDTHEME)
    this.pUSEDTHEME=pUSEDTHEME
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pUSEDTHEME"
endproc
