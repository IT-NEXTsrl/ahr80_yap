* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kgf                                                        *
*              Generazione file IVA fisco azienda                              *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_34]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-02                                                      *
* Last revis.: 2012-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kgf",oParentObject))

* --- Class definition
define class tgscg_kgf as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 634
  Height = 186
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-07-04"
  HelpContextID=32122217
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  CONTROPA_IDX = 0
  ATTIMAST_IDX = 0
  cPrg = "gscg_kgf"
  cComment = "Generazione file IVA fisco azienda"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ANNO = 0
  o_ANNO = 0
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CODATT = space(5)
  o_CODATT = space(5)
  w_RIFFAZ = space(3)
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_CODIDE = space(5)
  w_CODIDA = space(4)
  w_PROATT = space(3)
  w_PATH = space(150)
  w_FILE = space(150)
  w_PERIODO = 0
  w_TICODICE = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kgfPag1","gscg_kgf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANNO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='ATTIMAST'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ANNO=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CODATT=space(5)
      .w_RIFFAZ=space(3)
      .w_CODAZI=space(5)
      .w_CODIDE=space(5)
      .w_CODIDA=space(4)
      .w_PROATT=space(3)
      .w_PATH=space(150)
      .w_FILE=space(150)
      .w_PERIODO=0
      .w_TICODICE=space(10)
        .w_ANNO = Year(i_datsys)-1
        .w_DATINI = IIF(.w_anno>0,cp_chartodate('01-01-'+alltrim(str(.w_ANNO))),cp_chartodate('01-01-'+alltrim(str(year(i_datsys)-1))))
        .w_DATFIN = IIF(.w_anno>0,cp_chartodate('31-12-'+alltrim(str(.w_ANNO))),cp_chartodate('31-12-'+alltrim(str(year(i_datsys)-1))))
        .w_CODATT = g_catazi
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODATT))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODAZI))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,10,.f.)
        .w_FILE = 'IIVA'+Alltrim(.w_CODIDE)+'.'+IIF(Not Empty(Alltrim(.w_RIFFAZ)),Alltrim(.w_RIFFAZ),Alltrim(.w_PROATT))
    endwith
    this.DoRTCalc(12,13,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_ANNO<>.w_ANNO
            .w_DATINI = IIF(.w_anno>0,cp_chartodate('01-01-'+alltrim(str(.w_ANNO))),cp_chartodate('01-01-'+alltrim(str(year(i_datsys)-1))))
        endif
        if .o_ANNO<>.w_ANNO
            .w_DATFIN = IIF(.w_anno>0,cp_chartodate('31-12-'+alltrim(str(.w_ANNO))),cp_chartodate('31-12-'+alltrim(str(year(i_datsys)-1))))
        endif
        .DoRTCalc(4,5,.t.)
        if .o_CODAZI<>.w_CODAZI
          .link_1_6('Full')
        endif
        .DoRTCalc(7,10,.t.)
        if .o_CODATT<>.w_CODATT
            .w_FILE = 'IIVA'+Alltrim(.w_CODIDE)+'.'+IIF(Not Empty(Alltrim(.w_RIFFAZ)),Alltrim(.w_RIFFAZ),Alltrim(.w_PROATT))
        endif
        if .o_datini<>.w_datini
          .Calculate_UUDLMEQCPJ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_UUDLMEQCPJ()
    with this
          * --- Modifica data finale
          .w_datfin = iif(.w_datini>.w_datfin and not empty(.w_datfin),.w_datini,.w_datfin)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODATT
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MAT',True,'ATTIMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATRIFFAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODATT,ATRIFFAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIMAST','*','ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_4'),i_cWhere,'GSAR_MAT',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATRIFFAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATRIFFAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATRIFFAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODATT,ATRIFFAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(5))
      this.w_RIFFAZ = NVL(_Link_.ATRIFFAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(5)
      endif
      this.w_RIFFAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COPATHFA,COCODIDE,COCODIDA,COPROATT";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COPATHFA,COCODIDE,COCODIDA,COPROATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_PATH = NVL(_Link_.COPATHFA,space(150))
      this.w_CODIDE = NVL(_Link_.COCODIDE,space(5))
      this.w_CODIDA = NVL(_Link_.COCODIDA,space(4))
      this.w_PROATT = NVL(_Link_.COPROATT,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_PATH = space(150)
      this.w_CODIDE = space(5)
      this.w_CODIDA = space(4)
      this.w_PROATT = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANNO_1_1.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_1.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_2.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_2.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_3.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_3.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_4.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_4.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oPATH_1_10.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_10.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oFILE_1_12.value==this.w_FILE)
      this.oPgFrm.Page1.oPag.oFILE_1_12.value=this.w_FILE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ANNO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_datfin) or .w_datini<=.w_datfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale minore della data finale")
          case   (empty(.w_CODATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATT_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CODATT)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ANNO = this.w_ANNO
    this.o_DATINI = this.w_DATINI
    this.o_CODATT = this.w_CODATT
    this.o_CODAZI = this.w_CODAZI
    return

enddefine

* --- Define pages as container
define class tgscg_kgfPag1 as StdContainer
  Width  = 630
  height = 186
  stdWidth  = 630
  stdheight = 186
  resizeXpos=244
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANNO_1_1 as StdField with uid="HJOSAYXVON",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di selezione",;
    HelpContextID = 26604282,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=81, Top=16, cSayPict="'9999'", cGetPict="'9999'"

  add object oDATINI_1_2 as StdField with uid="JDCJQCAYAT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data competenza IVA iniziale",;
    HelpContextID = 205807670,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=333, Top=16

  add object oDATFIN_1_3 as StdField with uid="AMYQXXYTLL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale minore della data finale",;
    ToolTipText = "Data competenza IVA finale",;
    HelpContextID = 15818806,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=333, Top=45

  func oDATFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_datfin) or .w_datini<=.w_datfin)
    endwith
    return bRes
  endfunc

  add object oCODATT_1_4 as StdField with uid="YEPRNSZADD",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 127626790,;
   bGlobalFont=.t.,;
    Height=21, Width=85, Left=511, Top=16, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ATTIMAST", cZoomOnZoom="GSAR_MAT", oKey_1_1="ATCODATT", oKey_1_2="this.w_CODATT"

  func oCODATT_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ATTIMAST','*','ATCODATT',cp_AbsName(this.parent,'oCODATT_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MAT',"Attivit�",'',this.parent.oContained
  endproc
  proc oCODATT_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MAT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ATCODATT=this.parent.oContained.w_CODATT
     i_obj.ecpSave()
  endproc

  add object oPATH_1_10 as StdField with uid="HBNCUVPVRI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Inserisci il path file da generare (impostabile di default in contropartite e parametri)",;
    HelpContextID = 27041546,;
   bGlobalFont=.t.,;
    Height=21, Width=521, Left=76, Top=88, InputMask=replicate('X',150)


  add object oBtn_1_11 as StdButton with uid="PSACVHWWYM",left=599, top=89, width=22,height=20,;
    caption="...", nPag=1;
    , HelpContextID = 31921194;
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .w_PATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(40),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFILE_1_12 as StdField with uid="KDKEYWXRRW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FILE", cQueryName = "FILE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Inserisci il file da generare",;
    HelpContextID = 27269034,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=76, Top=117, InputMask=replicate('X',150)


  add object oBtn_1_13 as StdButton with uid="PWCUCFWQQP",left=520, top=134, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 32093466;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSCG_BGF(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_PATH ) AND Not Empty(.w_CODATT))
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="LNMWHHBLEZ",left=572, top=134, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 24804794;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_15 as StdString with uid="EHJMGLHYKT",Visible=.t., Left=9, Top=88,;
    Alignment=1, Width=65, Height=18,;
    Caption="Percorso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="BFSLHDKDFF",Visible=.t., Left=15, Top=117,;
    Alignment=1, Width=59, Height=18,;
    Caption="File:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="KGWPNIHETR",Visible=.t., Left=18, Top=17,;
    Alignment=1, Width=60, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="BMSKIRKBZP",Visible=.t., Left=171, Top=17,;
    Alignment=1, Width=160, Height=18,;
    Caption="Data comp. IVA. iniziale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="YARCQNTTTC",Visible=.t., Left=179, Top=45,;
    Alignment=1, Width=152, Height=18,;
    Caption="Data comp. IVA. finale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="VPUIFOGJTG",Visible=.t., Left=414, Top=17,;
    Alignment=1, Width=96, Height=18,;
    Caption="Codice attivit�:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kgf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
