* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bsa                                                        *
*              Carica partite da chiudere                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_207]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-24                                                      *
* Last revis.: 2014-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bsa",oParentObject,m.pOPER)
return(i_retval)

define class tgscg_bsa as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_OLDREC = 0
  w_CNTREC = 0
  w_APPO = space(10)
  w_TIPPAG = space(2)
  w_PNOTA = .NULL.
  w_IDRIGA = 0
  w_CREAROWNUM = 0
  w_Tmpc = space(10)
  w_TOTRIP = 0
  w_OKRIP = .f.
  w_PADRE = .NULL.
  w_TOTIMP = 0
  w_SCTIPGES = space(1)
  w_TIPCON = space(1)
  w_DREG = ctod("  /  /  ")
  w_CODCON = space(15)
  w_DDOC = ctod("  /  /  ")
  w_VALDIF = 0
  w_VAL1 = 0
  w_VAL2 = 0
  w_ROWORD = 0
  w_OREC = 0
  w_FLVARI = .f.
  w_ROWABB = 0
  w_FLDELE = .f.
  w_SERIAL = space(10)
  w_ROWNUM = 0
  w_CODCON1 = space(15)
  w_INIT = .f.
  w_BANAPP = space(10)
  w_BANNOS = space(15)
  w_CODPAGAM = space(5)
  w_LIMITE = 0
  w_OKSAL = .f.
  w_CLASS = space(10)
  w_FLAGAC = space(1)
  * --- WorkFile variables
  PAR_TITE_idx=0
  VALUTE_idx=0
  MOD_PAGA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Partite/Scadenze da Chiudere in Primanota (da GSCG_MSP)
    if this.oParentObject.cFunction<>"Load"
      i_retcode = 'stop'
      return
    endif
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ah_Msg("Lettura partite da saldare...",.T.)
    * --- Legge le Partite gestibili sul Saldaconto in Base alle selezioni
    * --- ATTENZIONE: non considero le partite della riga di P.N., i valoro verranno riportati sul Saldato/Abbuono
    * --- Creo il temporaneo con l'elenco delle partite non saldate
    GSTE_BPA (this, this.oParentObject.w_SCAINI , this.oParentObject.w_SCAFIN , this.w_TIPCON , this.w_CODCON , this.w_CODCON1,this.oParentObject.w_CODVAL , "", "RB","BO","RD","RI","MA","CA", "GSCG_BSA","N", cp_CharToDate("  -  -  ") )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if UPPER(this.w_CLASS)="TGSCG_MSC"
      vq_exec("query\GSCG_QSC.VQR",this,"SALDPAR1")
    else
      vq_exec("query\GSCG_QSP.VQR",this,"SALDPAR1")
    endif
    * --- Drop temporary table TMP_PART_APE
    i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART_APE')
    endif
    * --- Applicare filtro su eventuali elementi in lista su saldpar1
    if used("selcon")
      if RECCOUNT("selcon") > 0
        select SALDPAR1.* from SALDPAR1 inner join selcon on (SALDPAR1.TIPCON=selcon.antipcon and SALDPAR1.CODCON=selcon.ancodice) ; 
 order by 1,2,3,4,5 into cursor SALDPAR2
         
 Use in Select("SALDPAR1") 
 select * from SALDPAR2 into cursor SALDPAR1
        = WrCursor("SALDPAR1")
      endif
       USE IN SELECT("selcon")
       USE IN SELECT("SALDPAR2")
    endif
    if this.w_OKSAL
      * --- Se sono in Caricamento verifico se esistono su altre righe partite che fanno riferimento allo stesso cliente (ma non ancora registrate)
      * --- Riferimento alla Prima Nota....
      this.w_PNOTA = this.oParentObject.w_PUNTAT.oParentObject
      this.w_IDRIGA = this.w_PNOTA.cRowID
      this.w_PNOTA.MarkPos()     
      * --- Nome archivio di appoggio su cui salver� eventuali partite gia presenti in
      *     prima nota e ancora da salvare sul database....
      *     Statement per problemi con macro
      L_TMPRIGPAR = SYS(2015)
       
 Select ( this.w_PNOTA.cTrsName ) 
 Go Top
      * --- Cicla sulle eventuali righe di Primanota interessate alla ricerca
      SCAN FOR t_PNTIPCON=this.w_TIPCON AND t_PNCODCON=this.w_CODCON AND CPROWNUM <> this.w_ROWNUM
      * --- Lancia il metodo di Caricamento delle Partite sul Transitorio
      this.w_CREAROWNUM = CPROWNUM
      this.w_PNOTA.GSCG_MPA.cnt.LoadAllTrs( this.w_IDRIGA , recno(), L_TMPRIGPAR , .F., "PAR_TITE", this.w_PNOTA.w_PNSERIAL, "PTSERIAL", CPROWNUM,"PTROWORD")     
      * --- marco in t_PTORDRIF il numero di riga della partita per impostare il PTROWORD al
      *     corretto valore di riga.
      *     Utilizzo il campo t_PTORDRIF perch� nel caso di partite chiuse contestualmente non
      *     � mai valorizzato
      if USED( L_TMPRIGPAR )
         
 Select ( L_TMPRIGPAR ) 
 Go Top 
 Replace t_PTORDRIF with this.w_CREAROWNUM FOR Nvl(t_PTORDRIF,0)=0
      endif
      Select ( this.w_PNOTA.cTrsName )
      ENDSCAN
      * --- Riposiziona il Puntatore dettaglio di Primanota
      this.w_PNOTA.RePos()     
      if USED( L_TMPRIGPAR )
        * --- Cicla sulle righe inserite Nuove e le aggiunge al cursore SALDPAR1
        this.w_CNTREC = 0
         
 Select ( L_TMPRIGPAR ) 
 Go Top
        SCAN FOR t_PTTIPCON=this.w_TIPCON AND t_PTCODCON=this.w_CODCON
        if (t_PTCODVAL=this.oParentObject.w_CODVAL OR EMPTY(this.oParentObject.w_CODVAL)) AND (t_PTDATSCA>=this.oParentObject.w_SCAINI OR EMPTY(this.oParentObject.w_SCAINI)) AND (t_PTDATSCA<=this.oParentObject.w_SCAFIN OR EMPTY(this.oParentObject.w_SCAFIN))
          if EMPTY(NVL(t_PTFLIMPE,"")) AND EMPTY(NVL(t_PTFLINDI,"")) AND i_SRV="A" AND i_TRSDEL<>"D" AND NOT DELETED()
            * --- Solo Righe non cancellate e Aggiunte e no da Contab.Indiretta o Impegnate (questi ultimi 3 probabilmente superflui)
            this.w_CNTREC = this.w_CNTREC + 1
            * --- Aggiunge le Righe
             
 Select SALDPAR1 
 Append Blank
             REPLACE TIPPA2 WITH this.w_TIPPAG, CONTA WITH 2 
 REPLACE TIPPAG WITH this.w_TIPPAG 
 REPLACE CODAGE WITH &L_TMPRIGPAR..t_PTCODAGE, TIPCON WITH &L_TMPRIGPAR..t_PTTIPCON, CODCON WITH &L_TMPRIGPAR..t_PTCODCON 
 REPLACE FLINDI WITH LEN(ALLTRIM(&L_TMPRIGPAR..t_PTFLINDI)) 
 REPLACE PTSERIAL WITH this.oParentObject.w_PUNTAT.oParentObject.w_PNSERIAL, PTROWORD WITH &L_TMPRIGPAR..t_PTORDRIF, CPROWNUM WITH &L_TMPRIGPAR..CPROWNUM 
 REPLACE FLCRSA WITH &L_TMPRIGPAR..t_PTFLCRSA, BANNOS WITH &L_TMPRIGPAR..t_PTBANNOS, BANAPP WITH &L_TMPRIGPAR..t_PTBANAPP 
 REPLACE BANFIL WITH SPACE(10), PTFLRAGG WITH LEN(ALLTRIM(&L_TMPRIGPAR..t_PTFLRAGG)), SEGNO WITH &L_TMPRIGPAR..t_PT_SEGNO 
 REPLACE IMPDAR WITH 0, IMPAVE WITH 0, PTSERRIF WITH IIF(&L_TMPRIGPAR..t_PTFLCRSA="C"," ",Nvl(&L_TMPRIGPAR..t_PTSERRIF," ")),; 
 PTORDRIF WITH IIF(&L_TMPRIGPAR..t_PTFLCRSA="C",0,Nvl(&L_TMPRIGPAR..t_PTORDRIF,0)), ; 
 PTNUMRIF WITH IIF(&L_TMPRIGPAR..t_PTFLCRSA="C",0,Nvl(&L_TMPRIGPAR..t_PTNUMRIF,0)) 
 REPLACE DATREG WITH this.oParentObject.w_PUNTAT.oParentObject.w_PNDATREG, NUMPRO WITH &L_TMPRIGPAR..t_PTNUMPRO, PTFLVABD WITH &L_TMPRIGPAR..t_PTFLVABD 
 
            * --- ATTENZIONE: Considera anche gli Abbuoni
             
 REPLACE TOTIMP WITH (&L_TMPRIGPAR..t_PTTOTIMP+&L_TMPRIGPAR..t_PTTOTABB) * IIF(&L_TMPRIGPAR..t_PT_SEGNO="D", 1, -1) 
 REPLACE FLSOSP WITH IIF(&L_TMPRIGPAR..t_PTFLCRSA="C", IIF(&L_TMPRIGPAR..t_PTFLSOSP=0, " ", "S"), " ") 
 REPLACE IMPSAL WITH 0, IMPABB WITH 0, FLSALD WITH " ", CAOVAL WITH &L_TMPRIGPAR..t_PTCAOVAL 
 REPLACE DESRIG WITH IIF(&L_TMPRIGPAR..t_PTFLCRSA="C", &L_TMPRIGPAR..t_PTDESRIG, " ") 
 
            this.w_Tmpc = NVL(&L_TMPRIGPAR..t_PTMODPAG, SPACE(10))
            this.w_TIPPAG = " "
            if NOT EMPTY(this.w_Tmpc)
              * --- Read from MOD_PAGA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2],.t.,this.MOD_PAGA_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MPTIPPAG"+;
                  " from "+i_cTable+" MOD_PAGA where ";
                      +"MPCODICE = "+cp_ToStrODBC(this.w_Tmpc);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MPTIPPAG;
                  from (i_cTable) where;
                      MPCODICE = this.w_Tmpc;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_TIPPAG = NVL(cp_ToDate(_read_.MPTIPPAG),cp_NullValue(_read_.MPTIPPAG))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- Le partite create da scadenze create contestualmente devono essere sempre 
            *     valutate da GSTE_BCP, quindi valorizzo CONTA con 2
             
 REPLACE DATSCA WITH &L_TMPRIGPAR..t_PTDATSCA 
 REPLACE NUMPAR WITH &L_TMPRIGPAR..t_PTNUMPAR 
 REPLACE MODPAG WITH &L_TMPRIGPAR..t_PTMODPAG 
 REPLACE CODVAL WITH &L_TMPRIGPAR..t_PTCODVAL 
 REPLACE DATDOC WITH &L_TMPRIGPAR..t_PTDATDOC 
 REPLACE NUMDOC WITH &L_TMPRIGPAR..t_PTNUMDOC 
 REPLACE ALFDOC WITH &L_TMPRIGPAR..t_PTALFDOC 
 REPLACE TIPPA2 WITH this.w_TIPPAG, CONTA WITH 2 
 REPLACE TIPPAG WITH this.w_TIPPAG 
 REPLACE CODAGE WITH &L_TMPRIGPAR..t_PTCODAGE, TIPCON WITH &L_TMPRIGPAR..t_PTTIPCON, CODCON WITH &L_TMPRIGPAR..t_PTCODCON 
 REPLACE FLINDI WITH LEN(ALLTRIM(&L_TMPRIGPAR..t_PTFLINDI)) 
 REPLACE PTSERIAL WITH this.oParentObject.w_PUNTAT.oParentObject.w_PNSERIAL, PTROWORD WITH &L_TMPRIGPAR..t_PTORDRIF, CPROWNUM WITH &L_TMPRIGPAR..CPROWNUM 
 REPLACE FLCRSA WITH &L_TMPRIGPAR..t_PTFLCRSA, BANNOS WITH &L_TMPRIGPAR..t_PTBANNOS, BANAPP WITH &L_TMPRIGPAR..t_PTBANAPP 
 REPLACE BANFIL WITH SPACE(10), PTFLRAGG WITH LEN(ALLTRIM(&L_TMPRIGPAR..t_PTFLRAGG)), SEGNO WITH &L_TMPRIGPAR..t_PT_SEGNO 
 REPLACE DATREG WITH this.oParentObject.w_PUNTAT.oParentObject.w_PNDATREG, NUMPRO WITH &L_TMPRIGPAR..t_PTNUMPRO, PTFLVABD WITH &L_TMPRIGPAR..t_PTFLVABD
          endif
        endif
        Select ( L_TMPRIGPAR )
        ENDSCAN
        * --- Distruggo la tabella temporanea appena creata
         
 Select ( L_TMPRIGPAR ) 
 Use 
 Erase L_TMPRIGPAR+".*"
        if this.w_CNTREC>0
           
 Select SALDPAR1 
 Go Top
          SELECT * FROM SALDPAR1 INTO CURSOR RIGPAR nofilter ORDER BY 1,2,3,4,5
          * --- Riaggiorna SALDPAR1 e rende il cursore riscrivibile
           
 SELECT SALDPAR1 
 Use
          SELECT * FROM RIGPAR INTO CURSOR SALDPAR1
          Cur=WRCURSOR("SALDPAR1")
          SELECT RIGPAR 
 Use
        endif
      endif
    endif
    * --- Applico routine per calcolare importo residuo
    GSTE_BCP(this,"A","SALDPAR1"," ")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_FLVARI = .F.
    if Not this.w_OKSAL 
      * --- Se sono in Variazione Calcolo l'importo chiuso dalla Partita corrente, nonche' l'Abbuono
      * --- Leggo direttamente dalla Tabella, non posso fidarmi dell'importo della movimentazione xche' nel frattempo potrei averlo variato
      vq_exec("query\GSCG1QSP.VQR",this,"SALDPAR2")
      this.w_FLVARI = IIF(RECCOUNT("SALDPAR2")>0, .T., .F.)
    endif
    * --- Ulteriore Filtro su Flag Sospeso e Pagamento,Flag Solo Acconti (non riesco a farlo dalla Query)
    SELECT DATSCA, NUMPAR, CODVAL, NUMDOC, ALFDOC, DATDOC, IMPDOC,CODCON,TIPCON,ANDESCRI, ; 
 IIF(EMPTY(CP_TODATE(DATAPE)), DATSCA, DATAPE) AS DATAPE, ; 
 IIF(EMPTY(NVL(CAOAPE,0)), CAOVAL, CAOAPE) AS CAOAPE, ; 
 MODPAG, IIF(EMPTY(NVL(TIPPAG,"")), TIPPA2, TIPPAG) AS TIPPAG, ; 
 TOTIMP, FLSOSP, IMPSAL, IMPABB, FLSALD, DESRIG,CODAGE,PTSERIAL,PTROWORD,CPROWNUM,SEGNO,PTSERRIF,PTORDRIF,PTNUMRIF,PTFLVABD,RIFDIS,PTFLRAGG,BANAPP,BANNOS ; 
 FROM SALDPAR1 ; 
 WHERE Nvl(FLSOSP," ")<>"S" AND ; 
 ((this.w_SCTIPGES="B" AND (this.oParentObject.w_RIFDIS=NVL(RIFDIS,SPACE(10)) AND Not Empty(this.oParentObject.w_RIFDIS)) OR this.oParentObject.w_DINUMERO=0 ) OR this.w_SCTIPGES<>"B") AND ; 
 ((EMPTY(NVL(TIPPAG,"  ")) AND EMPTY(NVL(TIPPA2,"  "))) OR IIF(EMPTY(NVL(TIPPAG,"")), TIPPA2, TIPPAG) IN (this.oParentObject.w_PAGRD,this.oParentObject.w_PAGBO,this.oParentObject.w_PAGRB,this.oParentObject.w_PAGMA,this.oParentObject.w_PAGRI,this.oParentObject.w_PAGCA)) ; 
 AND ((this.oParentObject.w_FLSACC="S" AND Nvl(FLCRSA," ")="A") or Empty(this.oParentObject.w_FLSACC)) AND IIF(!EMPTY(this.w_BANNOS),NVL(BANNOS,"  ")=this.w_BANNOS,1=1) ; 
 AND IIF(!EMPTY(this.w_BANAPP),NVL(BANAPP,"  ")=this.w_BANAPP,1=1) AND IIF(!EMPTY(this.w_CODPAGAM),NVL(CODPAG,"  ")=this.w_CODPAGAM,1=1) ; 
 AND IIF(this.w_LIMITE<>0,ABS(TOTIMP)<=this.w_LIMITE,1=1) ; 
 INTO CURSOR SALDPART ORDER BY DATSCA, NUMPAR, CODVAL NOFILTER
    if used("Saldpar1")
      select Saldpar1 
 Use
    endif
    this.w_OKRIP = .T.
    this.w_INIT = .T.
    * --- Azzera Totalizzatori
    this.oParentObject.w_TOTSAL = 0
    this.oParentObject.w_TOTRES = 0
    this.oParentObject.w_VALSAL = 0
    this.oParentObject.w_VALRES = 0
    this.w_PADRE = this.oParentObject
    this.w_PADRE.Firstrow()     
    this.w_PADRE.Setrow(1)     
    this.w_PADRE.MarkPos()     
    * --- Cicla sul Risultato della Query
    SELECT SALDPART
    GO TOP
    SCAN FOR NVL(TOTIMP,0)<>0
    * --- Carica Detail Saldaconto
    SELECT (this.oParentObject.cTrsName)
    if this.w_INIT
      * --- Azzera il Transitorio
      if reccount()<>0
        ZAP
      endif
      this.w_INIT = .F.
    endif
    * --- Nuova Riga del Temporaneo
    this.oParentObject.InitRow()
    SELECT SALDPART
    this.oParentObject.w_SADATSCA = CP_TODATE(DATSCA)
    this.oParentObject.w_SANUMPAR = NVL(NUMPAR,SPACE(31))
    this.oParentObject.w_SA_SEGNO = NVL(SEGNO," ")
    this.oParentObject.w_SACAOAPE = NVL(CAOAPE,0)
    this.oParentObject.w_SADATAPE = IIF(EMPTY(CP_TODATE(DATAPE)), this.oParentObject.w_DATRIF, CP_TODATE(DATAPE))
    this.oParentObject.w_SAMODPAG = Nvl(MODPAG,Space(10))
    this.oParentObject.w_SATOTIMP = ABS(TOTIMP)
    this.oParentObject.w_SACODVAL = NVL(CODVAL, " ")
    this.oParentObject.w_SACODAGE = NVL(CODAGE, "     ")
    this.oParentObject.w_FLRAGG = NVL(PTFLRAGG, "     ")
    this.oParentObject.w_SAFLVABD = NVL(PTFLVABD, " ")
    this.oParentObject.w_SASERRIF = NVL(PTSERIAL,SPACE(10))
    this.oParentObject.w_SAORDRIF = NVL(PTROWORD,0)
    this.oParentObject.w_SANUMRIF = NVL(CPROWNUM,0)
    this.oParentObject.w_SABANAPP = NVL(BANAPP,SPACE(10))
    this.oParentObject.w_SABANNOS = NVL(BANNOS,SPACE(15))
    this.oParentObject.w_SACAOVAL = this.oParentObject.w_CAOVAL
    if this.w_OKRIP
      * --- Solo alla prima Interazione
      if this.oParentObject.w_CODNAZ<>this.oParentObject.w_SACODVAL
        this.w_TOTRIP = Abs(this.oParentObject.w_TOTDAO)
      else
        this.w_TOTRIP = Abs(this.oParentObject.w_TOTDAV)
      endif
      this.w_OKRIP = .F.
    endif
    * --- Assegnamenti Utilizzati nel Saldaconto
    *     =========================================================
    this.oParentObject.w_SACODCON = NVL(CODCON,SPACE(15))
    this.oParentObject.w_SATIPCON = NVL(TIPCON," ")
    this.oParentObject.w_DESCON1 = NVL(ANDESCRI,SPACE(40))
    * --- ==========================================================
    this.oParentObject.w_EURVAL = this.oParentObject.w_PUNTAT.oParentObject.w_CAOVAL
    this.oParentObject.w_DECRIG = this.oParentObject.w_PUNTAT.oParentObject.w_DECTOT
    this.oParentObject.w_CODVAO = this.oParentObject.w_PUNTAT.oParentObject.w_PNCODVAL
    * --- Se valuta differente da Primanota Calcola il cambio
    if this.w_SCTIPGES<>"B" 
      if this.oParentObject.w_SACODVAL<>this.oParentObject.w_VALDEF
        this.oParentObject.w_SACAOVAL = GETCAM(this.oParentObject.w_SACODVAL, this.oParentObject.w_DATRIF)
        this.oParentObject.w_EURVAL = GETCAM(g_PERVAL,this.oParentObject.w_DATRIF)
        this.oParentObject.w_DECRIG = g_PERPVL
      endif
    else
      * --- Se storno SBF forzo cambio di apertura
      this.oParentObject.w_SACAOVAL = this.oParentObject.w_SACAOAPE
    endif
    * --- Per gli Importi Partite prende il numero decimali max delle valute lette
    VVL = MAX(this.oParentObject.w_DECRIG * 20, VVL)
    this.oParentObject.w_SAIMPSAL = 0
    this.oParentObject.w_SAFLSALD = " "
    this.oParentObject.w_SAIMPABB = 0
    if Not this.w_OKSAL AND UPPER(this.w_CLASS)<>"TGSCG_MSC"
      * --- A questo punto vedo se esistono sulla riga partite che fanno riferimento a quella in esame...
      if USED("SALDPAR2")
        SELECT SALDPAR2
        GO TOP
        LOCATE FOR CP_TODATE(DATSCA)=this.oParentObject.w_SADATSCA AND NUMPAR=this.oParentObject.w_SANUMPAR AND CODVAL=this.oParentObject.w_SACODVAL
        if FOUND()
          this.oParentObject.w_SAIMPSAL = ABS(NVL(IMPSAL,0))
          this.oParentObject.w_SAIMPABB = ABS(NVL(IMPABB,0))
          this.oParentObject.w_SATOTIMP = this.oParentObject.w_SATOTIMP + this.oParentObject.w_SAIMPABB
          this.oParentObject.w_SAFLSALD = IIF(this.oParentObject.w_SAIMPABB<>0 OR this.oParentObject.w_SAIMPSAL>=this.oParentObject.w_SATOTIMP, "S", " ")
          this.oParentObject.w_SACAOVAL = NVL(CAOVAL, 0)
        endif
        SELECT SALDPART
      endif
    endif
    this.oParentObject.w_SANUMDOC = NVL(NUMDOC,0)
    this.oParentObject.w_SAALFDOC = NVL(ALFDOC, "  ")
    this.oParentObject.w_SADATDOC = CP_TODATE(DATDOC)
    this.oParentObject.w_SADESRIG = NVL(DESRIG, SPACE(50))
    this.oParentObject.w_FLSALD = this.oParentObject.w_SAFLSALD
    this.oParentObject.w_DARSAL = (this.oParentObject.w_SAIMPSAL+ this.oParentObject.w_SAIMPABB) * IIF(this.oParentObject.w_SEGSAL=this.oParentObject.w_SA_SEGNO, 1, -1)
    this.oParentObject.w_DARSAL1 = this.oParentObject.w_DARSAL
    this.oParentObject.w_DIFCAM = 0
    if this.oParentObject.w_SACODVAL<>this.oParentObject.w_CODNAZ AND (this.oParentObject.w_SAFLSALD="S" OR this.oParentObject.w_SAIMPSAL<>0)
      * --- Differenza Cambi
      if this.oParentObject.w_EURVAL=0 OR this.oParentObject.w_DATRIF<GETVALUT(g_PERVAL, "VADATEUR")
        * --- Fase Pre EURO o Valuta non EURO; Calcola Differenze Cambi
        this.w_VALDIF = MIN(ABS(this.oParentObject.w_SATOTIMP), ABS(this.oParentObject.w_SAIMPSAL))
        this.w_VAL1 = VAL2MON(this.w_VALDIF, this.oParentObject.w_SACAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_DATRIF, this.oParentObject.w_CODNAZ)
        this.w_VAL2 = VAL2MON(this.w_VALDIF, this.oParentObject.w_SACAOAPE, this.oParentObject.w_CAONAZ, this.oParentObject.w_SADATAPE, this.oParentObject.w_CODNAZ)
        this.oParentObject.w_DIFCAM = cp_ROUND(this.w_VAL1 - this.w_VAL2, this.oParentObject.w_DECTOP)
      endif
      this.oParentObject.w_DARSAL1 = VAL2MON(this.oParentObject.w_DARSAL, this.oParentObject.w_SACAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_DATRIF, this.oParentObject.w_CODNAZ)
    endif
    if  this.w_TOTRIP>0
      if this.w_FLAGAC="S" 
        * --- Valorizzo Nel caso di accorpamento acconti righe in base all'importo di riga della fattura
        this.w_TOTIMP = this.oParentObject.w_SATOTIMP
        if this.w_TOTIMP<=this.w_TOTRIP
          this.oParentObject.w_SAIMPSAL = this.oParentObject.w_SATOTIMP
          this.oParentObject.w_SAFLSALD = "S"
          this.w_PADRE.NotifyEvent("w_SAFLSALD Changed")     
          this.w_TOTRIP = this.w_TOTRIP-this.w_TOTIMP
        else
          this.oParentObject.w_SAIMPSAL = this.w_TOTRIP
          this.w_PADRE.NotifyEvent("w_SAIMPSAL Changed")     
          this.w_TOTRIP = 0
        endif
      else
        this.oParentObject.w_VALSAL = this.oParentObject.w_VALSAL + this.oParentObject.w_DARSAL
        this.oParentObject.w_TOTSAL = this.oParentObject.w_TOTSAL + this.oParentObject.w_DARSAL1
      endif
    endif
    this.oParentObject.w_IMPDOC = NVL(IMPDOC,0)
    * --- Carica il Temporaneo Saldaconto dei Dati
    this.oParentObject.TrsFromWork()
    SELECT SALDPART
    * --- Skippa al record successivo
    ENDSCAN
    this.oParentObject.w_TOTRES = IIF(this.oParentObject.w_TOTDAV=0, 0, this.oParentObject.w_TOTDAV-this.oParentObject.w_TOTSAL)
    this.oParentObject.w_TOTDAO = IIF(this.oParentObject.w_CODNAZ=this.oParentObject.w_CODVAO, 0, cp_ROUND(MON2VAL(this.oParentObject.w_TOTDAV, this.oParentObject.w_CAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_DATRIF, this.oParentObject.w_CODNAZ), this.oParentObject.w_DECRIG))
    this.oParentObject.w_VALRES = IIF(this.oParentObject.w_TOTDAO=0, 0, this.oParentObject.w_TOTDAO - this.oParentObject.w_VALSAL)
    if used("SALDPAR2")
      select SALDPAR2
      USE
    endif
    WAIT CLEAR
    if used("SALDPART")
      SELECT SALDPART
      USE
    endif
    * --- Verifica se vi sono piu' valute
    this.oParentObject.w_NUMVAL = 0
    SELECT t_SACODVAL FROM (this.oParentObject.cTrsName) ;
    GROUP BY t_SACODVAL WHERE NOT EMPTY(NVL(t_SACODVAL,"")) INTO CURSOR SALDPART
    if used("SALDPART")
      SELECT SALDPART
      this.oParentObject.w_NUMVAL = RECCOUNT()
      USE
    endif
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    if this.w_INIT
      if reccount()<>0
        ZAP
      endif
      ah_ErrorMsg("Non ci sono dati da elaborare",,"")
      this.oParentObject.InitRow()
    endif
    this.w_PADRE.RePos(.T.)     
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza Variabili Globali e Locali
    this.oParentObject.w_FLACCESS = .F.
    if Type("this.oParentObject.w_PUNTAT.OPARENTOBJECT")="O"
      this.w_CLASS = this.oParentObject.w_PUNTAT.OPARENTOBJECT.CLASS
      * --- Inizializza con i valori provenienti dalla Movimentazione (GSCG_MSP)
      if UPPER(this.w_CLASS)="TGSCG_MSC"
        * --- Elaborazione da Saldaconto
        With this.oParentObject.w_PUNTAT.oParentobject
        this.w_TIPCON = .w_SCTIPCON
        this.w_CODCON = .w_SCCONINI
        this.w_CODCON1 = .w_SCCONFIN
        this.oParentObject.w_CAOVAL = .w_SCCAOVAL
        this.w_SCTIPGES = .w_SCTIPGES
        this.w_BANAPP = .w_SCBANAPP
        this.w_BANNOS = .w_SCBANNOS
        this.w_CODPAGAM = .w_CODPAG
        this.w_LIMITE = .w_IMPLIMITE
        if this.pOPER="A"
          this.oParentObject.w_SCAINI = .w_SCDATINI
          this.oParentObject.w_SCAFIN = .w_SCDATFIN
          this.oParentObject.w_CODVAL = .w_SCCODVAL
          this.oParentObject.w_PAGRD = .w_SCPAGRDR
          this.oParentObject.w_PAGBO = .w_SCPAGBON
          this.oParentObject.w_PAGRB = .w_SCPAGRBA
          this.oParentObject.w_PAGMA = .w_SCPAGMAV
          this.oParentObject.w_PAGRI = .w_SCPAGRID
          this.oParentObject.w_PAGCA = .w_SCPAGCAM
        endif
        if this.w_SCTIPGES="B" AND this.oParentObject.w_SCAFIN>.w_SCDATREG
          ah_ErrorMsg("Attenzione, la data di fine valuta deve essere minore o uguale alla data registrazione",,"")
          i_retcode = 'stop'
          return
        endif
        EndWith
        this.w_OKSAL = .F.
      else
        * --- Puntatore alla Primanota
        this.w_FLAGAC = this.oParentObject.w_PUNTAT.oParentObject.w_FLAGAC
        this.w_TIPCON = IIF(this.oParentObject.pTipGes = "A",this.oParentObject.w_PUNTAT.oParentObject.w_PNTIPCLF,this.oParentObject.w_TIPCON)
        this.w_CODCON = IIF(this.oParentObject.pTipGes = "A",this.oParentObject.w_PUNTAT.oParentObject.w_PNCODCLF,this.oParentObject.w_CODCON)
        this.w_CODCON1 = IIF(this.oParentObject.pTipGes = "A",this.oParentObject.w_PUNTAT.oParentObject.w_PNCODCLF,this.oParentObject.w_CODCON)
        this.w_SERIAL = this.oParentObject.w_PUNTAT.oParentObject.w_PNSERIAL
        this.w_ROWNUM = this.oParentObject.w_PUNTAT.oParentObject.w_CPROWNUM
        this.w_OKSAL = IIF(this.oParentObject.w_PUNTAT.oParentObject.cFunction="Load",.T.,.F.)
        this.oParentObject.w_CODVAL = iif(this.w_FLAGAC="S",this.oParentObject.w_PUNTAT.oParentObject.w_PNCODVAL,this.oParentObject.w_CODVAL)
        this.oParentObject.w_CAOVAL = iif(this.w_FLAGAC="S",this.oParentObject.w_PUNTAT.oParentObject.w_PNCAOVAL,this.oParentObject.w_CAOVAL)
        this.w_SCTIPGES = "P"
      endif
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='MOD_PAGA'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
