* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_spm                                                        *
*              Stampa provvigioni maturate                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_25]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-14                                                      *
* Last revis.: 2014-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_spm",oParentObject))

* --- Class definition
define class tgsve_spm as StdForm
  Top    = 50
  Left   = 93

  * --- Standard Properties
  Width  = 441
  Height = 292
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-18"
  HelpContextID=127258007
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  AGENTI_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsve_spm"
  cComment = "Stampa provvigioni maturate"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PARAMETRO = space(10)
  w_NOMPRG = space(10)
  w_CODAZE = space(5)
  w_INIDATA = ctod('  /  /  ')
  w_FINDATA = ctod('  /  /  ')
  w_AGEINI = space(5)
  o_AGEINI = space(5)
  w_DESAGE = space(35)
  w_CAPOINI = space(5)
  o_CAPOINI = space(5)
  w_CAPODES = space(35)
  w_AGEFIN = space(5)
  w_DESAG2 = space(35)
  w_CAPOFIN = space(5)
  w_CAPODE2 = space(35)
  w_DATA1 = ctod('  /  /  ')
  o_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_VALUTA = space(3)
  w_FLLIQUID = space(1)
  w_FLMATUR = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPOAG = space(1)
  w_DESAPP = space(35)
  w_ONUME = 0
  * --- Area Manuale = Declare Variables
  * --- gsve_spm
  * --- Per gestione sicurezza (a seconda del parametro)
  * --- gestisco e mostro all'utente un nome di maschera diverso..
  func getSecuritycode()
     return(lower(IIF( this.oParentObject $ "LMR" , "GSVE_SP"+this.oParentObject , .cPrg)))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_spmPag1","gsve_spm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAGEINI_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AGENTI'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARAMETRO=space(10)
      .w_NOMPRG=space(10)
      .w_CODAZE=space(5)
      .w_INIDATA=ctod("  /  /  ")
      .w_FINDATA=ctod("  /  /  ")
      .w_AGEINI=space(5)
      .w_DESAGE=space(35)
      .w_CAPOINI=space(5)
      .w_CAPODES=space(35)
      .w_AGEFIN=space(5)
      .w_DESAG2=space(35)
      .w_CAPOFIN=space(5)
      .w_CAPODE2=space(35)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_VALUTA=space(3)
      .w_FLLIQUID=space(1)
      .w_FLMATUR=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPOAG=space(1)
      .w_DESAPP=space(35)
      .w_ONUME=0
        .w_PARAMETRO = .oParentObject
        .w_NOMPRG = UPPER(IIF( .oParentObject $ "LMR" , "GSVE_SP"+.oParentObject , .cPrg))
        .w_CODAZE = i_codazi
        .w_INIDATA = MESESKIP(i_datsys,-1,'I')
        .w_FINDATA = MESESKIP(i_datsys,-1,'F')
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_AGEINI))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_CAPOINI))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_AGEFIN = .w_AGEINI
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_AGEFIN))
          .link_1_10('Full')
        endif
          .DoRTCalc(11,11,.f.)
        .w_CAPOFIN = .w_CAPOINI
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CAPOFIN))
          .link_1_12('Full')
        endif
          .DoRTCalc(13,13,.f.)
        .w_DATA1 = .w_INIDATA
        .w_DATA2 = .w_FINDATA
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_VALUTA))
          .link_1_16('Full')
        endif
        .w_FLLIQUID = 'E'
        .w_FLMATUR = 'E'
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate(.w_NOMPRG)
        .w_OBTEST = .w_DATA1
          .DoRTCalc(20,20,.f.)
        .w_TIPOAG = 'C'
    endwith
    this.DoRTCalc(22,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_AGEINI<>.w_AGEINI
            .w_AGEFIN = .w_AGEINI
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.t.)
        if .o_CAPOINI<>.w_CAPOINI
            .w_CAPOFIN = .w_CAPOINI
          .link_1_12('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(.w_NOMPRG)
        .DoRTCalc(13,18,.t.)
        if .o_DATA1<>.w_DATA1
            .w_OBTEST = .w_DATA1
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        this.Calculate_OVBGVWXVMA()
      endwith
      this.DoRTCalc(20,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(.w_NOMPRG)
    endwith
  return

  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .cComment = IIF( .oParentObject = "L" , AH_MSGFORMAT("Stampa provvigioni liquidate") , IIF( .oParentObject = "M",AH_MSGFORMAT("Stampa provvigioni maturate"),AH_MSGFORMAT("Stampa provvigioni calcolate")))
          .Caption = .cComment
    endwith
  endproc
  proc Calculate_OVBGVWXVMA()
    with this
          * --- Cambia cprg
          .cPrg = lower(.w_NOMPRG)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAPOINI_1_8.visible=!this.oPgFrm.Page1.oPag.oCAPOINI_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCAPODES_1_9.visible=!this.oPgFrm.Page1.oPag.oCAPODES_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCAPOFIN_1_12.visible=!this.oPgFrm.Page1.oPag.oCAPOFIN_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCAPODE2_1_13.visible=!this.oPgFrm.Page1.oPag.oCAPODE2_1_13.mHide()
    this.oPgFrm.Page1.oPag.oFLLIQUID_1_17.visible=!this.oPgFrm.Page1.oPag.oFLLIQUID_1_17.mHide()
    this.oPgFrm.Page1.oPag.oFLMATUR_1_18.visible=!this.oPgFrm.Page1.oPag.oFLMATUR_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AGEINI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGEINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_AGEINI)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_AGEINI))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGEINI)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_AGEINI)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_AGEINI)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AGEINI) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oAGEINI_1_6'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGEINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_AGEINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_AGEINI)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGEINI = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AGEINI = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_agefin) or (.w_agefin>=.w_ageini)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Agente incongruente oppure obsoleto")
        endif
        this.w_AGEINI = space(5)
        this.w_DESAGE = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGEINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAPOINI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAPOINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CAPOINI)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CAPOINI))
          select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAPOINI)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_CAPOINI)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_CAPOINI)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAPOINI) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCAPOINI_1_8'),i_cWhere,'GSAR_AGE',"Agenti",'GSAR1AGE.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAPOINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CAPOINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CAPOINI)
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAPOINI = NVL(_Link_.AGCODAGE,space(5))
      this.w_CAPODES = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_TIPOAG = NVL(_Link_.AGTIPAGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAPOINI = space(5)
      endif
      this.w_CAPODES = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPOAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPOAG='C') AND (empty(.w_capofin) or (.w_capofin>=.w_capoini)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Capo area incongruente oppure obsoleto")
        endif
        this.w_CAPOINI = space(5)
        this.w_CAPODES = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPOAG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAPOINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AGEFIN
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGEFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_AGEFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_AGEFIN))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGEFIN)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_AGEFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_AGEFIN)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AGEFIN) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oAGEFIN_1_10'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGEFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_AGEFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_AGEFIN)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGEFIN = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAG2 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AGEFIN = space(5)
      endif
      this.w_DESAG2 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_agefin) or (.w_agefin>=.w_ageini)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Agente incongruente oppure obsoleto")
        endif
        this.w_AGEFIN = space(5)
        this.w_DESAG2 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGEFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAPOFIN
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAPOFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CAPOFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CAPOFIN))
          select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAPOFIN)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_CAPOFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_CAPOFIN)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAPOFIN) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCAPOFIN_1_12'),i_cWhere,'GSAR_AGE',"Agenti",'GSAR1AGE.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAPOFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CAPOFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CAPOFIN)
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAPOFIN = NVL(_Link_.AGCODAGE,space(5))
      this.w_CAPODE2 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_TIPOAG = NVL(_Link_.AGTIPAGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAPOFIN = space(5)
      endif
      this.w_CAPODE2 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPOAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPOAG='C') AND (empty(.w_capofin) or (.w_capofin>=.w_capoini)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Capo area incongruente oppure obsoleto")
        endif
        this.w_CAPOFIN = space(5)
        this.w_CAPODE2 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPOAG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAPOFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VALUTA))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VALUTA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VALUTA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVALUTA_1_16'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DESAPP = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAGEINI_1_6.value==this.w_AGEINI)
      this.oPgFrm.Page1.oPag.oAGEINI_1_6.value=this.w_AGEINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_7.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_7.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCAPOINI_1_8.value==this.w_CAPOINI)
      this.oPgFrm.Page1.oPag.oCAPOINI_1_8.value=this.w_CAPOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCAPODES_1_9.value==this.w_CAPODES)
      this.oPgFrm.Page1.oPag.oCAPODES_1_9.value=this.w_CAPODES
    endif
    if not(this.oPgFrm.Page1.oPag.oAGEFIN_1_10.value==this.w_AGEFIN)
      this.oPgFrm.Page1.oPag.oAGEFIN_1_10.value=this.w_AGEFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAG2_1_11.value==this.w_DESAG2)
      this.oPgFrm.Page1.oPag.oDESAG2_1_11.value=this.w_DESAG2
    endif
    if not(this.oPgFrm.Page1.oPag.oCAPOFIN_1_12.value==this.w_CAPOFIN)
      this.oPgFrm.Page1.oPag.oCAPOFIN_1_12.value=this.w_CAPOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAPODE2_1_13.value==this.w_CAPODE2)
      this.oPgFrm.Page1.oPag.oCAPODE2_1_13.value=this.w_CAPODE2
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_14.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_14.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_15.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_15.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_16.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_16.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLLIQUID_1_17.RadioValue()==this.w_FLLIQUID)
      this.oPgFrm.Page1.oPag.oFLLIQUID_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMATUR_1_18.RadioValue()==this.w_FLMATUR)
      this.oPgFrm.Page1.oPag.oFLMATUR_1_18.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_agefin) or (.w_agefin>=.w_ageini)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_AGEINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGEINI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Agente incongruente oppure obsoleto")
          case   not((.w_TIPOAG='C') AND (empty(.w_capofin) or (.w_capofin>=.w_capoini)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_ONUME=1 or .w_ONUME=2)  and not(empty(.w_CAPOINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAPOINI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Capo area incongruente oppure obsoleto")
          case   not((empty(.w_agefin) or (.w_agefin>=.w_ageini)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_AGEFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGEFIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Agente incongruente oppure obsoleto")
          case   not((.w_TIPOAG='C') AND (empty(.w_capofin) or (.w_capofin>=.w_capoini)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_ONUME=1 or .w_ONUME=2)  and not(empty(.w_CAPOFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAPOFIN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Capo area incongruente oppure obsoleto")
          case   ((empty(.w_DATA1)) or not(empty(.w_DATA2) or .w_DATA2>=.w_DATA1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_14.SetFocus()
            i_bnoObbl = !empty(.w_DATA1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date errato")
          case   ((empty(.w_DATA2)) or not(empty(.w_DATA1) or .w_DATA2>=.w_DATA1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_15.SetFocus()
            i_bnoObbl = !empty(.w_DATA2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date errato")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AGEINI = this.w_AGEINI
    this.o_CAPOINI = this.w_CAPOINI
    this.o_DATA1 = this.w_DATA1
    return

enddefine

* --- Define pages as container
define class tgsve_spmPag1 as StdContainer
  Width  = 437
  height = 292
  stdWidth  = 437
  stdheight = 292
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAGEINI_1_6 as StdField with uid="QDSFOPECGD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AGEINI", cQueryName = "AGEINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Agente incongruente oppure obsoleto",;
    ToolTipText = "Agente iniziale selezionato",;
    HelpContextID = 171742970,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=14, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_AGEINI"

  func oAGEINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGEINI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGEINI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oAGEINI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oAGEINI_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_AGEINI
     i_obj.ecpSave()
  endproc

  add object oDESAGE_1_7 as StdField with uid="LJAWKIMKFJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 246659274,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=170, Top=14, InputMask=replicate('X',35)

  add object oCAPOINI_1_8 as StdField with uid="OJSLZCLHGW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CAPOINI", cQueryName = "CAPOINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Capo area incongruente oppure obsoleto",;
    ToolTipText = "Capo area iniziale selezionato",;
    HelpContextID = 175772454,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=69, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CAPOINI"

  func oCAPOINI_1_8.mHide()
    with this.Parent.oContained
      return (.w_ONUME=1 or .w_ONUME=2)
    endwith
  endfunc

  func oCAPOINI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAPOINI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAPOINI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCAPOINI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'GSAR1AGE.AGENTI_VZM',this.parent.oContained
  endproc
  proc oCAPOINI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_CAPOINI
     i_obj.ecpSave()
  endproc

  add object oCAPODES_1_9 as StdField with uid="BYOEZVDIRI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CAPODES", cQueryName = "CAPODES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 248900826,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=170, Top=69, InputMask=replicate('X',35)

  func oCAPODES_1_9.mHide()
    with this.Parent.oContained
      return (.w_ONUME=1 or .w_ONUME=2)
    endwith
  endfunc

  add object oAGEFIN_1_10 as StdField with uid="XLNTBCWJFK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_AGEFIN", cQueryName = "AGEFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Agente incongruente oppure obsoleto",;
    ToolTipText = "Agente finale selezionato",;
    HelpContextID = 93296378,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_AGEFIN"

  func oAGEFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGEFIN_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGEFIN_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oAGEFIN_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oAGEFIN_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_AGEFIN
     i_obj.ecpSave()
  endproc

  add object oDESAG2_1_11 as StdField with uid="XUOZMFQCJD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESAG2", cQueryName = "DESAG2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 239879990,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=170, Top=42, InputMask=replicate('X',35)

  add object oCAPOFIN_1_12 as StdField with uid="LBLLTSOVAV",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CAPOFIN", cQueryName = "CAPOFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Capo area incongruente oppure obsoleto",;
    ToolTipText = "Capo area finale selezionato",;
    HelpContextID = 88740646,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=97, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CAPOFIN"

  func oCAPOFIN_1_12.mHide()
    with this.Parent.oContained
      return (.w_ONUME=1 or .w_ONUME=2)
    endwith
  endfunc

  func oCAPOFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAPOFIN_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAPOFIN_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCAPOFIN_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'GSAR1AGE.AGENTI_VZM',this.parent.oContained
  endproc
  proc oCAPOFIN_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_CAPOFIN
     i_obj.ecpSave()
  endproc

  add object oCAPODE2_1_13 as StdField with uid="TVBONSSAAJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CAPODE2", cQueryName = "CAPODE2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 248900826,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=170, Top=97, InputMask=replicate('X',35)

  func oCAPODE2_1_13.mHide()
    with this.Parent.oContained
      return (.w_ONUME=1 or .w_ONUME=2)
    endwith
  endfunc

  add object oDATA1_1_14 as StdField with uid="AOGUHVDJZE",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date errato",;
    ToolTipText = "Data di maturazione di inizio stampa",;
    HelpContextID = 183259958,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=109, Top=130

  func oDATA1_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATA2) or .w_DATA2>=.w_DATA1)
    endwith
    return bRes
  endfunc

  add object oDATA2_1_15 as StdField with uid="UHSXOQRPWF",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date errato",;
    ToolTipText = "Data di maturazione di fine stampa",;
    HelpContextID = 184308534,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=109, Top=158

  func oDATA2_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATA1) or .w_DATA2>=.w_DATA1)
    endwith
    return bRes
  endfunc

  add object oVALUTA_1_16 as StdField with uid="WXJYQQABLI",rtseq=16,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta di selezione (spazio=no selezione)",;
    HelpContextID = 30419882,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=109, Top=186, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oVALUTA_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVALUTA_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oVALUTA_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oVALUTA_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_VALUTA
     i_obj.ecpSave()
  endproc


  add object oFLLIQUID_1_17 as StdCombo with uid="QHMLSFRKAN",rtseq=17,rtrep=.f.,left=315,top=130,width=109,height=21;
    , ToolTipText = "Liquidate, da liquidare, o entrambe";
    , HelpContextID = 32759450;
    , cFormVar="w_FLLIQUID",RowSource=""+"Liquidate,"+"Da liquidare,"+"Entrambe", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLLIQUID_1_17.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'D',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oFLLIQUID_1_17.GetRadio()
    this.Parent.oContained.w_FLLIQUID = this.RadioValue()
    return .t.
  endfunc

  func oFLLIQUID_1_17.SetRadio()
    this.Parent.oContained.w_FLLIQUID=trim(this.Parent.oContained.w_FLLIQUID)
    this.value = ;
      iif(this.Parent.oContained.w_FLLIQUID=='L',1,;
      iif(this.Parent.oContained.w_FLLIQUID=='D',2,;
      iif(this.Parent.oContained.w_FLLIQUID=='E',3,;
      0)))
  endfunc

  func oFLLIQUID_1_17.mHide()
    with this.Parent.oContained
      return (.w_PARAMETRO <> "M")
    endwith
  endfunc


  add object oFLMATUR_1_18 as StdCombo with uid="YTPHGHNQWT",rtseq=18,rtrep=.f.,left=315,top=130,width=109,height=21;
    , ToolTipText = "Maturate, da maturare, o entrambe";
    , HelpContextID = 233050538;
    , cFormVar="w_FLMATUR",RowSource=""+"Maturate,"+"Da maturare,"+"Entrambe", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLMATUR_1_18.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'D',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oFLMATUR_1_18.GetRadio()
    this.Parent.oContained.w_FLMATUR = this.RadioValue()
    return .t.
  endfunc

  func oFLMATUR_1_18.SetRadio()
    this.Parent.oContained.w_FLMATUR=trim(this.Parent.oContained.w_FLMATUR)
    this.value = ;
      iif(this.Parent.oContained.w_FLMATUR=='M',1,;
      iif(this.Parent.oContained.w_FLMATUR=='D',2,;
      iif(this.Parent.oContained.w_FLMATUR=='E',3,;
      0)))
  endfunc

  func oFLMATUR_1_18.mHide()
    with this.Parent.oContained
      return (.w_PARAMETRO <> "R")
    endwith
  endfunc


  add object oObj_1_21 as cp_outputCombo with uid="ZLERXIVPWT",left=109, top=215, width=319,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 231615258


  add object oBtn_1_22 as StdButton with uid="MYUPCFETDS",left=329, top=242, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 267823322;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_data1)) and (not empty(.w_data2)))
      endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="LOHROJBSGE",left=380, top=242, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 134575430;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_19 as StdString with uid="RTNVZSFCHK",Visible=.t., Left=26, Top=130,;
    Alignment=1, Width=81, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="WTZKSAXHWL",Visible=.t., Left=26, Top=158,;
    Alignment=1, Width=81, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="HUESCZHNQK",Visible=.t., Left=26, Top=186,;
    Alignment=1, Width=81, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="QBPCRYZEHI",Visible=.t., Left=15, Top=14,;
    Alignment=1, Width=93, Height=15,;
    Caption="Da agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="FOKJNYNOSC",Visible=.t., Left=15, Top=42,;
    Alignment=1, Width=93, Height=15,;
    Caption="A agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="BAPHMPQUQY",Visible=.t., Left=15, Top=69,;
    Alignment=1, Width=93, Height=15,;
    Caption="Da capo area:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_ONUME=1 or .w_ONUME=2)
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="AKXMFBDBLR",Visible=.t., Left=8, Top=97,;
    Alignment=1, Width=100, Height=15,;
    Caption="A capo area:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_ONUME=1 or .w_ONUME=2)
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="EESBYCEWQN",Visible=.t., Left=191, Top=130,;
    Alignment=1, Width=121, Height=18,;
    Caption="Stato provvigioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_PARAMETRO = "L")
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="HRTFFGQQPR",Visible=.t., Left=14, Top=215,;
    Alignment=1, Width=93, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_spm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
